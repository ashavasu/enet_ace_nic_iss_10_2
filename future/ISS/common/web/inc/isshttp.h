/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: isshttp.h,v 1.26 2017/01/24 13:18:15 siva Exp $
 *
 * Description:  macros,typedefs and prototypes for ISS web Module 
 *******************************************************************/


#ifndef _ISSHTTPH
#define _ISSHTTPH
/* HTML Tags */

typedef struct REDIRECTPAGE
{
    const char *pi1Name;
    void       (*pRedirectPage)(tHttp *); 
}tHtmlRedirectPage;

extern VOID IssRedirectMacFilterPage (tHttp *pHttp);
extern VOID IssRedirectDiffSrvPage (tHttp *pHttp);
extern UINT1 ISS_RT_TABLE_FLAG[] ;
extern UINT1 ISS_RT_PARAM_STOP_FLAG[] ;
extern UINT1 ISS_RT_TABLE_STOP_FLAG[] ;

VOID IssRedirectHtmlPage (tHttp *pHttp);
VOID IssRedirectRateCtrlPage (tHttp *pHttp);
#ifdef IGS_WANTED
extern INT1 IgsGetForwardingType (VOID);
#endif

#ifdef _ISS_WEB_C_
UINT1 VLAN_PORT_ROW_TAG[] = "<tr><td><input type=\"text\" name=\"PortNo\" value=\"%d\" readonly></td>";

UINT1 VLAN_UNTAGGED_PORT_TAG [] = "<td><input type=\"checkbox\" name=\"UntaggedPort\" value = \"%d\" checked></td>";

UINT1 VLAN_TAGGED_PORT_TAG [] = "<td><input type=\"checkbox\" name=\"TaggedPort\" value=\"%d\"></td>";

UINT1 VLAN_PORT_CHECKED_TAG [] = "<td><input type=\"radio\" name=\"radio%d\" value = \"%d\" checked></td>";

UINT1 VLAN_PORT_UNCHECKED_TAG [] = "<td><input type=\"radio\" name=\"radio%d\" value=\"%d\"></td>";

UINT1  ISS_ERROR_START_STR [ ] =  "\r\n<HTML>\n<HEAD> <link rel=\"stylesheet\" type=\"text/css\" HREF=\"/style.css\">\n <meta HTTP-EQUIV=\"Pragma\" content =\"no-cache\"> \n<script type=\"text/javascript\" src=\"/script.js\"> </script>\n </HEAD><body topmargin=i\"0\" leftmargin=\"0\" marginwidth=\"0\" marginheight=\"0\"  class=\"DataFrameBGColor\" onLoad='selectOption();parent.frames[\"SystemInformationFrame\"].location.href=\"/iss/homeframe.html?Gambit=GAMBIT\"'><font color=red size=+2>\n <br><br><hr>\n";

UINT1  ISS_GAMBIT_STR [] = "<form><input type=hidden name=Gambit value=\"GAMBIT\">\n</form>";
UINT1  ISS_ERROR_BACK_STR [] =  "<form name=\"buttonForm\">\n <center>\n <input type=\"button\" value=\"Back\" onClick=\"ReloadfromISS(document.forms[1].elements[1].value,document.forms[0].Gambit.value);\">\n<input type=\"hidden\" value=\""; 

UINT1  ISS_ERROR_BACK_SPECIFIC_STR [] =  "<form name=\"buttonForm\">\n <center>\n <input type=\"button\" value=\"Back\" onClick=location.href=\"%s?Gambit=GAMBIT\" >\n<input type=\"hidden\" value=\"";

UINT1  ISS_ERROR_BACK_STR_CONT [] =  "\"></center></form>\n";
UINT1  ISS_CENTER_END [] =  "</center>\n";

UINT1  ISS_ERROR_END_STR1 [] =  "</font><br><hr>\n";
UINT1  ISS_ERROR_END_STR2 [] =  "</html>\n";

UINT1  ISS_ERROR_END_STR [] =  "</font><br><hr>\n </html>\n"; 

UINT1 VLAN_ROW_START_TAG[] = "<! ROW START>";
UINT1 ISS_PARAM_STOP_FLAG[] = "<! PARAM STOP>";
#ifdef MPLS_L3VPN_WANTED
UINT1 ISS_RT_PARAM_STOP_FLAG[] = "<! RT PARAM STOP>";
#endif
UINT1 ISS_TABLE_FLAG[] = "<! TABLE INSERT>";
UINT1 ISS_RT_TABLE_FLAG[] = "<! RT TABLE INSERT>";
UINT1 ISS_MI_FLAG[] = "<! MI PAGE>";
UINT1 ISS_TABLE_INDEX[] ="TABLE_INDEX";
UINT1 ISS_TABLE_STOP_FLAG[] = "<! TABLE STOP>";
UINT1 ISS_RT_TABLE_STOP_FLAG[] = "<! RT TABLE STOP>";
UINT1 ISS_ACTION[]="ACTION";
UINT1 ISS_ADD[]="Add";
UINT1 ISS_APPLY[]="Apply";
UINT1 ISS_DELETE[]="Delete";
UINT1 ISS_MODIFY[]="Modify";
UINT1 ISS_MIB_SAVE_FAIL[]="<font color=red size=+2>configuration save Failed";
UINT1 ISS_MIB_SAVE_SUCCESS[]="<font color=red size=+2>Configuration Save was successful";
UINT1 ISS_MIB_SAVE_PROGRESS[]="<font color=red size=+2>Save is in progress";
UINT1 ISS_SAVE_NOT_INITIATED[]="<font color=red size=+2>Save Not Initiated yet";
UINT1 ISS_PORT_CREATE_SUCCESS[]="<font color=red size=+2><center><h2>Port Creation Successful</h2></center></font>";
UINT1 ISS_PORT_DELETE_SUCCESS[]="<font color=red size=+2><center><h2>Port Deletion Successful</h2></center></font>";
UINT1 ISS_MIB_LOG_SAVE_FAIL[]="<font color=red size=+2>Log Transfer Failed";
UINT1 ISS_MIB_LOG_SAVE_SUCCESS[]="<font color=red size=+2>Log Transfer was successful";
UINT1 ISS_MIB_LOG_SAVE_PROGRESS[]="<font color=red size=+2>Log Transfer is in progress";
UINT1 ISS_LOG_SAVE_NOT_INITIATED[]="<font color=red size=+2>Log Transfer is not yet initiated";

UINT1 ISS_IMG_DOWNLOAD_FAIL[]="<font color=red size=+2>Image Download Failed !";
UINT1 ISS_IMG_DOWNLOAD_SUCCESS[]="<font color=red size=+2>Image Download Successful";
UINT1 ISS_IMG_DOWNLOAD_PROGRESS[]="<font color=red size=+2>Image Download In Progress...";
UINT1 ISS_IMG_DOWNLOAD_INITIATED[]="<font color=red size=+2>Image Download Not Initiated";
UINT1 ISS_IMG_DOWNLOAD_ABORTED[]="<font color=red size=+2>Image Download Aborted";
                                                                                                                             
UINT1 ISS_FILE_TRANSFER_FAIL[]="<font color=red size=+2>File transfer failed !";
UINT1 ISS_FILE_TRANSFER_SUCCESS[]="<font color=red size=+2>File transfer successful";
UINT1 ISS_FILE_TRANSFER_PROGRESS[]="<font color=red size=+2>File transfer in progress...";
UINT1 ISS_FILE_TRANSFER_INITIATED[]="<font color=red size=+2>File transfer not initiated";


#ifdef IGS_WANTED
VOID 
IssRedirectIgsFwdTablePage (tHttp *pHttp);
VOID 
IssRedirectIgsMcastRecvPage (tHttp *pHttp);
VOID 
IssRedirectIgsIntfConfPage (tHttp *pHttp);
#endif

#ifdef MLDS_WANTED
VOID 
IssRedirectMldsFwdTablePage (tHttp *pHttp);
VOID 
IssRedirectMldsMcastRecvPage (tHttp *pHttp);
VOID 
IssRedirectMldsIntfConfPage (tHttp *pHttp);
#endif
VOID IssRedirectVlanSubnetPerPortEnablePage (tHttp * pHttp);

VOID IssRedirectVlanSubnetEnablePage(tHttp *pHttp);
tHtmlRedirectPage phtmlRedirect[] ={
#ifdef IGS_WANTED
{"igs_mcasttable.html", IssRedirectIgsFwdTablePage},
{"igs_recvtable.html", IssRedirectIgsMcastRecvPage},
{"igs_intfconf.html", IssRedirectIgsIntfConfPage},
#endif
#ifdef MLDS_WANTED
{"mlds_mcasttable.html", IssRedirectMldsFwdTablePage},
{"mlds_recvtable.html", IssRedirectMldsMcastRecvPage},
{"mlds_intfconf.html", IssRedirectMldsIntfConfPage},
#endif
#if defined(SWC) || defined (BCMX_WANTED) || defined(DX285) || defined(XCAT) || defined(MRVLLS) || defined(LION) || defined(LION_DB) || defined(XCAT3)
{"mac_filterconf.html", IssRedirectMacFilterPage},
{"port_ratectrl.html", IssRedirectRateCtrlPage},
#if defined(SWC) || defined (BCMX_WANTED) || defined(DX285) || defined(XCAT) || defined(LION) || defined(LION_DB) || defined(XCAT3)
{"dfs_globalconf.html", IssRedirectDiffSrvPage},
#ifdef BCMX_WANTED
{"vlan_pvidsetting.html", IssRedirectVlanSubnetEnablePage},
{"vlan_subnetipmap.html", IssRedirectVlanSubnetPerPortEnablePage},
#endif
#endif
#else
{"mac_filterconf.html", IssRedirectMacFilterPage},
#endif
{0,0}
};
#else
extern UINT1 VLAN_PORT_ROW_TAG[]; 
extern UINT1 VLAN_UNTAGGED_PORT_TAG [];
extern UINT1 VLAN_TAGGED_PORT_TAG [];
extern UINT1 VLAN_PORT_CHECKED_TAG [];
extern UINT1 VLAN_PORT_UNCHECKED_TAG [];
extern UINT1  ISS_ERROR_START_STR [ ];
extern UINT1  ISS_ERROR_END_STR [];
extern UINT1 ISS_ERROR_BACK_STR [];
extern UINT1 ISS_GAMBIT_STR [];
extern UINT1 ISS_ERROR_BACK_STR_CONT [];
extern UINT1 ISS_ERROR_END_STR1 [];
extern UINT1 ISS_ERROR_END_STR2 [];
extern UINT1 VLAN_ROW_START_TAG[];
extern UINT1 ISS_TABLE_INDEX[]; 
extern UINT1 ISS_ACTION[];
extern UINT1 ISS_ADD[];
extern UINT1 ISS_APPLY[];
extern UINT1 ISS_DELETE[];
extern UINT1 ISS_MIB_SAVE_FAIL[];
extern UINT1 ISS_MIB_SAVE_SUCCESS[];
extern UINT1 ISS_MIB_SAVE_PROGRESS[];
extern UINT1 ISS_SAVE_NOT_INITIATED[];
extern tHtmlRedirectPage phtmlRedirect[];
#endif


#endif /* _ISSHTTPH */

