
/********************************************************************
 *   Copyright (C) 2009 Aricent Inc . All Rights Reserved]
 *   
 *   $Id: dhcp6web.h,v 1.5 2014/06/30 11:03:40 siva Exp $
 *
 *   Description: This file contains routines for DHCP6 web Module
 *       
 * ******************************************************************/

#ifndef _DHCP6_WEB_H_
#define _DHCP6_WEB_H_

#define DHCP6_IP6_ADDRESS_SIZE_MAX    16
#define DHCP6_DUID_SIZE_MAX           128
#define DHCP6_REALM_SIZE_MAX          128
#define DHCP6_KEY_SIZE_MAX            128
#define DHCP6_STATUS_CODE_OPTION_TYPE 13
#define DHCP6_ORO_DNS_SERVERS         23
#define DHCP6_ORO_SIP_SERVER_A        22

#ifdef DHCP6_SRV_WANTED
#define DHCP6_SRV_POOL_NAME_MAX       65
#define DHCP6_SRV_KEY_SIZE_MAX        64
#define DHCP6_SRV_DUID_SIZE_MAX       128
#define DHCP6_SRV_OPTION_LENGTH_MAX   0xFF
#endif

#define DHCP6_CRITICAL_TRC  0x00000100

#define DHCP6_ALL_TRC (INIT_SHUT_TRC | MGMT_TRC | \
                       CONTROL_PLANE_TRC | DUMP_TRC | OS_RESOURCE_TRC | \
                       ALL_FAILURE_TRC | BUFFER_TRC | DHCP6_CRITICAL_TRC)


#ifdef DHCP6_CLNT_WANTED 
extern INT1
nmhGetFsDhcp6ClntTrapAdminControl ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhGetFsDhcp6ClntDebugTrace ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsDhcp6ClntSysLogAdminStatus ARG_LIST((INT4 *));
extern INT1 nmhGetFsDhcp6ClntListenPort ARG_LIST((INT4 *));
extern INT1 nmhGetFsDhcp6ClntTransmitPort ARG_LIST((INT4 *));

extern INT1
nmhSetFsDhcp6ClntTrapAdminControl ARG_LIST((tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsDhcp6ClntDebugTrace ARG_LIST((tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsDhcp6ClntSysLogAdminStatus ARG_LIST((INT4 ));
extern INT1 nmhSetFsDhcp6ClntListenPort ARG_LIST((INT4 ));
extern INT1 nmhSetFsDhcp6ClntTransmitPort ARG_LIST((INT4 ));

extern INT1 
nmhTestv2FsDhcp6ClntTrapAdminControl ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2FsDhcp6ClntDebugTrace ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsDhcp6ClntSysLogAdminStatus ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FsDhcp6ClntListenPort ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FsDhcp6ClntTransmitPort ARG_LIST((UINT4 *  ,INT4 ));


extern INT1 nmhGetFirstIndexFsDhcp6ClntIfTable ARG_LIST((INT4 *));
extern INT1 nmhGetNextIndexFsDhcp6ClntIfTable ARG_LIST((INT4 , INT4 *));

extern INT1 
nmhGetFsDhcp6ClntIfSrvAddress ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsDhcp6ClntIfDuidType ARG_LIST((INT4 ,INT4 *));
extern INT1 
nmhGetFsDhcp6ClntIfDuid ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsDhcp6ClntIfDuidIfIndex ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsDhcp6ClntIfMaxRetCount ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsDhcp6ClntIfMaxRetDelay ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsDhcp6ClntIfMaxRetTime ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsDhcp6ClntIfInitRetTime ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsDhcp6ClntIfCurrRetTime ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsDhcp6ClntIfMinRefreshTime ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsDhcp6ClntIfCurrRefreshTime ARG_LIST((INT4 ,UINT4 *));
extern INT1 
nmhGetFsDhcp6ClntIfRealmName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsDhcp6ClntIfKey ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsDhcp6ClntIfKeyId ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsDhcp6ClntIfInformOut ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsDhcp6ClntIfReplyIn ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsDhcp6ClntIfInvalidPktIn ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsDhcp6ClntIfHmacFailCount ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsDhcp6ClntIfCounterRest ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsDhcp6ClntIfRowStatus ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhSetFsDhcp6ClntIfDuidType ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsDhcp6ClntIfDuidIfIndex ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsDhcp6ClntIfMaxRetCount ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsDhcp6ClntIfMaxRetDelay ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsDhcp6ClntIfMaxRetTime ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsDhcp6ClntIfInitRetTime ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsDhcp6ClntIfMinRefreshTime ARG_LIST((INT4  ,UINT4 ));
extern INT1 
nmhSetFsDhcp6ClntIfRealmName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsDhcp6ClntIfKey ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsDhcp6ClntIfKeyId ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhSetFsDhcp6ClntIfCounterRest ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsDhcp6ClntIfRowStatus ARG_LIST((INT4  ,INT4 ));
 
extern INT1 nmhTestv2FsDhcp6ClntIfDuidType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 
nmhTestv2FsDhcp6ClntIfDuidIfIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 
nmhTestv2FsDhcp6ClntIfMaxRetCount ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 
nmhTestv2FsDhcp6ClntIfMaxRetDelay ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsDhcp6ClntIfMaxRetTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 
nmhTestv2FsDhcp6ClntIfInitRetTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 
nmhTestv2FsDhcp6ClntIfMinRefreshTime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
extern INT1 
nmhTestv2FsDhcp6ClntIfRealmName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2FsDhcp6ClntIfKey ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsDhcp6ClntIfKeyId ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
extern INT1 
nmhTestv2FsDhcp6ClntIfCounterRest ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsDhcp6ClntIfRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhGetFirstIndexFsDhcp6ClntOptionTable ARG_LIST((INT4 * , INT4 *));
extern INT1 
nmhGetNextIndexFsDhcp6ClntOptionTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));
extern INT1 nmhGetFsDhcp6ClntOptionLength ARG_LIST((INT4  , INT4 ,INT4 *));
extern INT1 
nmhGetFsDhcp6ClntOptionValue ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));



extern INT4 FsDhcp6ClntTrapAdminControlGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntDebugTraceGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntSysLogAdminStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntListenPortGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntTransmitPortGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntTrapAdminControlSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntDebugTraceSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntSysLogAdminStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntListenPortSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntTransmitPortSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntTrapAdminControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntDebugTraceTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntSysLogAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntListenPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntTransmitPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntTrapAdminControlDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
extern INT4 FsDhcp6ClntDebugTraceDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
extern INT4 FsDhcp6ClntSysLogAdminStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
extern INT4 FsDhcp6ClntListenPortDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
extern INT4 FsDhcp6ClntTransmitPortDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
extern INT4 GetNextIndexFsDhcp6ClntIfTable(tSnmpIndex *, tSnmpIndex *);
extern INT4 FsDhcp6ClntIfSrvAddressGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfDuidTypeGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfDuidGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfDuidIfIndexGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfMaxRetCountGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfMaxRetDelayGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfMaxRetTimeGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfInitRetTimeGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfCurrRetTimeGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfMinRefreshTimeGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfCurrRefreshTimeGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfRealmNameGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfKeyGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfKeyIdGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfInformOutGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfReplyInGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfInvalidPktInGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfHmacFailCountGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfCounterRestGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfRowStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfDuidTypeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfDuidIfIndexSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfMaxRetCountSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfMaxRetDelaySet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfMaxRetTimeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfInitRetTimeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfMinRefreshTimeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfRealmNameSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfKeySet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfKeyIdSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfCounterRestSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfDuidTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfDuidIfIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfMaxRetCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfMaxRetDelayTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfMaxRetTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfInitRetTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfMinRefreshTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfRealmNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfKeyTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfKeyIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfCounterRestTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntIfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
extern INT4 GetNextIndexFsDhcp6ClntOptionTable(tSnmpIndex *, tSnmpIndex *);
extern INT4 FsDhcp6ClntOptionLengthGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6ClntOptionValueGet(tSnmpIndex *, tRetVal *);
#endif

#ifdef DHCP6_RLY_WANTED 
extern INT1 nmhGetFsDhcp6RlyDebugTrace ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
extern INT1 
nmhGetFsDhcp6RlyTrapAdminControl ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsDhcp6RlySysLogAdminStatus ARG_LIST((INT4 *));
extern INT1 nmhGetFsDhcp6RlyListenPort ARG_LIST((INT4 *));
extern INT1 nmhGetFsDhcp6RlyClientTransmitPort ARG_LIST((INT4 *));
extern INT1 nmhGetFsDhcp6RlyServerTransmitPort ARG_LIST((INT4 *));
extern INT1 nmhGetFsDhcp6RlyOption37Control ARG_LIST((INT4 *));
extern INT1 nmhGetFsDhcp6RlyPDRouteControl ARG_LIST((INT4 *));

extern INT1 nmhSetFsDhcp6RlyDebugTrace ARG_LIST((tSNMP_OCTET_STRING_TYPE *));
extern INT1 
nmhSetFsDhcp6RlyTrapAdminControl ARG_LIST((tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsDhcp6RlySysLogAdminStatus ARG_LIST((INT4 ));
extern INT1 nmhSetFsDhcp6RlyListenPort ARG_LIST((INT4 ));
extern INT1 nmhSetFsDhcp6RlyClientTransmitPort ARG_LIST((INT4 ));
extern INT1 nmhSetFsDhcp6RlyServerTransmitPort ARG_LIST((INT4 ));
extern INT1 nmhSetFsDhcp6RlyOption37Control ARG_LIST((INT4 ));
extern INT1 nmhSetFsDhcp6RlyPDRouteControl ARG_LIST((INT4 ));

extern INT1 
nmhTestv2FsDhcp6RlyDebugTrace ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2FsDhcp6RlyTrapAdminControl ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2FsDhcp6RlySysLogAdminStatus ARG_LIST((UINT4 *  ,INT4 ));
extern INT1
nmhTestv2FsDhcp6RlyListenPort ARG_LIST((UINT4 *  ,INT4 ));
extern INT1
nmhTestv2FsDhcp6RlyClientTransmitPort ARG_LIST((UINT4 *  ,INT4 ));
extern INT1
nmhTestv2FsDhcp6RlyServerTransmitPort ARG_LIST((UINT4 *  ,INT4 ));
extern INT1
nmhTestv2FsDhcp6RlyOption37Control ARG_LIST((UINT4 *  ,INT4 ));
extern INT1
nmhTestv2FsDhcp6RlyPDRouteControl ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

extern INT1 nmhGetFirstIndexFsDhcp6RlyIfTable ARG_LIST((INT4 *));
extern INT1 nmhGetNextIndexFsDhcp6RlyIfTable ARG_LIST((INT4 , INT4 *));
extern INT1 nmhGetFsDhcp6RlyIfHopThreshold ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsDhcp6RlyIfCounterRest ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsDhcp6RlyIfRemoteIdOption ARG_LIST((INT4 ,INT4 *));
extern INT1 
nmhGetFsDhcp6RlyIfRemoteIdDUID ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhGetFsDhcp6RlyIfRemoteIdOptionValue ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhGetFsDhcp6RlyIfRemoteIdUserDefined ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsDhcp6RlyIfRowStatus ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhSetFsDhcp6RlyIfHopThreshold ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsDhcp6RlyIfCounterRest ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsDhcp6RlyIfRemoteIdOption ARG_LIST((INT4  ,INT4 ));
extern INT1
nmhSetFsDhcp6RlyIfRemoteIdDUID ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsDhcp6RlyIfRowStatus ARG_LIST((INT4  ,INT4 ));

extern INT1 
nmhTestv2FsDhcp6RlyIfHopThreshold ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 
nmhTestv2FsDhcp6RlyIfCounterRest ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 
nmhTestv2FsDhcp6RlyIfRemoteIdOption ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1
nmhTestv2FsDhcp6RlyIfRemoteIdDUID ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2FsDhcp6RlyIfRemoteIdUserDefined ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhSetFsDhcp6RlyIfRemoteIdUserDefined ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 
nmhTestv2FsDhcp6RlyIfRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));


extern INT1
nmhGetFirstIndexFsDhcp6RlySrvAddressTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));
extern INT1 
nmhGetNextIndexFsDhcp6RlySrvAddressTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));
extern INT1 
nmhGetFsDhcp6RlySrvAddressRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1 
nmhSetFsDhcp6RlySrvAddressRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 
nmhTestv2FsDhcp6RlySrvAddressRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));


extern INT1
nmhGetFirstIndexFsDhcp6RlyOutIfTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));
extern INT1
nmhGetNextIndexFsDhcp6RlyOutIfTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));
extern INT1
nmhGetFsDhcp6RlyOutIfRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));
extern INT1
nmhSetFsDhcp6RlyOutIfRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));
extern INT1
nmhTestv2FsDhcp6RlyOutIfRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));


extern INT4 FsDhcp6RlyDebugTraceGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyTrapAdminControlGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlySysLogAdminStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyListenPortGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyClientTransmitPortGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyServerTransmitPortGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyOption37ControlGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyDebugTraceSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyTrapAdminControlSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlySysLogAdminStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyListenPortSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyClientTransmitPortSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyServerTransmitPortSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyOption37ControlSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyPDRouteControlSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyDebugTraceTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyTrapAdminControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlySysLogAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyListenPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyClientTransmitPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyServerTransmitPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyOption37ControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyDebugTraceDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
extern INT4 FsDhcp6RlyTrapAdminControlDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
extern INT4 FsDhcp6RlySysLogAdminStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
extern INT4 FsDhcp6RlyListenPortDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
extern INT4 FsDhcp6RlyClientTransmitPortDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
extern INT4 FsDhcp6RlyServerTransmitPortDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
extern INT4 FsDhcp6RlyOption37ControlDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
extern INT4 GetNextIndexFsDhcp6RlyIfTable(tSnmpIndex *, tSnmpIndex *);
extern INT4 FsDhcp6RlyIfHopThresholdGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyIfInformInGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyIfRelayForwInGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyIfRelayReplyInGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyIfInvalidPktInGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyIfCounterRestGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyIfRemoteIdOptionGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyIfRemoteIdDUIDGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyIfRemoteIdOptionValueGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyIfRowStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyIfHopThresholdSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyIfCounterRestSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyIfRemoteIdOptionSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyIfRemoteIdDUIDSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyIfRemoteIdUserDefinedSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyIfRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyIfHopThresholdTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyIfCounterRestTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyIfRemoteIdOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyIfRemoteIdDUIDTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyIfRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyIfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
extern INT4 GetNextIndexFsDhcp6RlySrvAddressTable(tSnmpIndex *, tSnmpIndex *);
extern INT4 FsDhcp6RlySrvAddressRowStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlySrvAddressRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlySrvAddressRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlySrvAddressTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
extern INT4 GetNextIndexFsDhcp6RlyOutIfTable(tSnmpIndex *, tSnmpIndex *);
extern INT4 FsDhcp6RlyOutIfRowStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyOutIfRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyOutIfRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6RlyOutIfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif

#ifdef DHCP6_SRV_WANTED 
extern INT1 nmhGetFsDhcp6SrvDebugTrace ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsDhcp6SrvRealmTableNextIndex ARG_LIST((UINT4 *));
extern INT1 nmhGetFsDhcp6SrvClientTableNextIndex ARG_LIST((UINT4 *));
extern INT1 nmhGetFsDhcp6SrvPoolTableNextIndex ARG_LIST((UINT4 *));
extern INT1 nmhGetFsDhcp6SrvIncludePrefixTableNextIndex ARG_LIST((UINT4 *));
extern INT1 
nmhGetFsDhcp6SrvTrapAdminControl ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsDhcp6SrvSysLogAdminStatus ARG_LIST((INT4 *));
extern INT1 nmhGetFsDhcp6SrvListenPort ARG_LIST((INT4 *));
extern INT1 nmhGetFsDhcp6SrvClientTransmitPort ARG_LIST((INT4 *));
extern INT1 nmhGetFsDhcp6SrvRelayTransmitPort ARG_LIST((INT4 *));

extern INT1 nmhSetFsDhcp6SrvDebugTrace ARG_LIST((tSNMP_OCTET_STRING_TYPE *));
extern INT1 
nmhSetFsDhcp6SrvTrapAdminControl ARG_LIST((tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsDhcp6SrvSysLogAdminStatus ARG_LIST((INT4 ));
extern INT1 nmhSetFsDhcp6SrvListenPort ARG_LIST((INT4 ));
extern INT1 nmhSetFsDhcp6SrvClientTransmitPort ARG_LIST((INT4 ));
extern INT1 nmhSetFsDhcp6SrvRelayTransmitPort ARG_LIST((INT4 ));

extern INT1
nmhTestv2FsDhcp6SrvDebugTrace ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2FsDhcp6SrvTrapAdminControl ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2FsDhcp6SrvSysLogAdminStatus ARG_LIST((UINT4 *  ,INT4 ));
extern INT1
nmhTestv2FsDhcp6SrvListenPort ARG_LIST((UINT4 *  ,INT4 ));
extern INT1
nmhTestv2FsDhcp6SrvClientTransmitPort ARG_LIST((UINT4 *  ,INT4 ));
extern INT1
nmhTestv2FsDhcp6SrvRelayTransmitPort ARG_LIST((UINT4 *  ,INT4 ));

extern INT1 nmhGetFirstIndexFsDhcp6SrvPoolTable ARG_LIST((UINT4 *));
extern INT1 nmhGetNextIndexFsDhcp6SrvPoolTable ARG_LIST((UINT4 , UINT4 *));
extern INT1 
nmhGetFsDhcp6SrvPoolName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsDhcp6SrvPoolPreference ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhGetFsDhcp6SrvPoolDuidType ARG_LIST((UINT4 ,INT4 *));
extern INT1 
nmhGetFsDhcp6SrvPoolDuid ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhGetFsDhcp6SrvPoolDuidIfIndex ARG_LIST((UINT4 ,INT4 *));
extern INT1
nmhGetFsDhcp6SrvPoolOptionTableNextIndex ARG_LIST((UINT4 ,UINT4 *));
extern INT1
nmhGetFsDhcp6SrvPoolRowStatus ARG_LIST((UINT4 ,INT4 *));

extern INT1
nmhSetFsDhcp6SrvPoolName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsDhcp6SrvPoolPreference ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhSetFsDhcp6SrvPoolDuidType ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhSetFsDhcp6SrvPoolDuidIfIndex ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhSetFsDhcp6SrvPoolRowStatus ARG_LIST((UINT4  ,INT4 ));

extern INT1
nmhTestv2FsDhcp6SrvPoolName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2FsDhcp6SrvPoolPreference ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1
nmhTestv2FsDhcp6SrvPoolDuidType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1
nmhTestv2FsDhcp6SrvPoolDuidIfIndex ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1
nmhTestv2FsDhcp6SrvPoolRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1 nmhGetFirstIndexFsDhcp6SrvClientTable ARG_LIST((UINT4 *));
extern INT1 nmhGetNextIndexFsDhcp6SrvClientTable ARG_LIST((UINT4 , UINT4 *));
extern INT1 
nmhGetFsDhcp6SrvClientId ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsDhcp6SrvClientIdType ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhGetFsDhcp6SrvClientRealm ARG_LIST((UINT4 ,UINT4 *));
extern INT1 nmhGetFsDhcp6SrvClientRowStatus ARG_LIST((UINT4 ,INT4 *));
extern INT1 
nmhSetFsDhcp6SrvClientId ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsDhcp6SrvClientIdType ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhSetFsDhcp6SrvClientRealm ARG_LIST((UINT4  ,UINT4 ));
extern INT1 nmhSetFsDhcp6SrvClientRowStatus ARG_LIST((UINT4  ,INT4 ));
extern INT1
nmhTestv2FsDhcp6SrvClientId ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsDhcp6SrvClientIdType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhTestv2FsDhcp6SrvClientRealm ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));
extern INT1 
nmhTestv2FsDhcp6SrvClientRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhGetFirstIndexFsDhcp6SrvIfTable ARG_LIST((INT4 *));
extern INT1 nmhGetNextIndexFsDhcp6SrvIfTable ARG_LIST((INT4 , INT4 *));


extern INT1 nmhGetFirstIndexFsDhcp6SrvRealmTable ARG_LIST((UINT4 *));
extern INT1 nmhGetNextIndexFsDhcp6SrvRealmTable ARG_LIST((UINT4 , UINT4 *));
extern INT1 
nmhGetFsDhcp6SrvRealmName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsDhcp6SrvRealmKeyTableNextIndex ARG_LIST((UINT4 ,UINT4 *));
extern INT1 nmhGetFsDhcp6SrvRealmRowStatus ARG_LIST((UINT4 ,INT4 *));
extern INT1 
nmhSetFsDhcp6SrvRealmName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsDhcp6SrvRealmRowStatus ARG_LIST((UINT4  ,INT4 ));
extern INT1
nmhTestv2FsDhcp6SrvRealmName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2FsDhcp6SrvRealmRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1 nmhGetFirstIndexFsDhcp6SrvKeyTable ARG_LIST((UINT4 * , UINT4 *));
extern INT1 
nmhGetNextIndexFsDhcp6SrvKeyTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));
extern INT1 
nmhGetFsDhcp6SrvKey ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsDhcp6SrvKeyRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));
extern INT1
nmhSetFsDhcp6SrvKey ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsDhcp6SrvKeyRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));
extern INT1 
nmhTestv2FsDhcp6SrvKey ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2FsDhcp6SrvKeyRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

extern INT1 nmhGetFsDhcp6SrvIfPool ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsDhcp6SrvIfCounterReset ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsDhcp6SrvIfRowStatus ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhSetFsDhcp6SrvIfPool ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsDhcp6SrvIfCounterReset ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsDhcp6SrvIfRowStatus ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhTestv2FsDhcp6SrvIfPool ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 
nmhTestv2FsDhcp6SrvIfCounterReset ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsDhcp6SrvIfRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhGetFirstIndexFsDhcp6SrvIncludePrefixTable ARG_LIST((INT4 * , INT4 *));
extern INT1
nmhGetNextIndexFsDhcp6SrvIncludePrefixTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));
extern INT1
nmhGetFsDhcp6SrvIncludePrefix ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhGetFsDhcp6SrvIncludePrefixPool ARG_LIST((INT4  , INT4 ,INT4 *));
extern INT1
nmhGetFsDhcp6SrvIncludePrefixRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));
extern INT1
nmhSetFsDhcp6SrvIncludePrefix ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhSetFsDhcp6SrvIncludePrefixPool ARG_LIST((INT4  , INT4  ,INT4 ));
extern INT1
nmhSetFsDhcp6SrvIncludePrefixRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));
extern INT1
nmhTestv2FsDhcp6SrvIncludePrefix ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2FsDhcp6SrvIncludePrefixPool ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));
extern INT1
nmhTestv2FsDhcp6SrvIncludePrefixRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

extern INT1
nmhGetFirstIndexFsDhcp6SrvOptionTable ARG_LIST((UINT4 * , UINT4 *));
extern INT1
nmhGetNextIndexFsDhcp6SrvOptionTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));
extern INT1
nmhGetFsDhcp6SrvOptionType ARG_LIST((UINT4  , UINT4 ,INT4 *));
extern INT1
nmhGetFsDhcp6SrvOptionLength ARG_LIST((UINT4  , UINT4 ,INT4 *));
extern INT1
nmhGetFsDhcp6SrvOptionValue ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhGetFsDhcp6SrvOptionPreference ARG_LIST((UINT4  , UINT4 ,INT4 *));
extern INT1
nmhGetFsDhcp6SrvOptionRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));
extern INT1
nmhSetFsDhcp6SrvOptionType ARG_LIST((UINT4  , UINT4  ,INT4 ));
extern INT1
nmhSetFsDhcp6SrvOptionLength ARG_LIST((UINT4  , UINT4  ,INT4 ));
extern INT1
nmhSetFsDhcp6SrvOptionValue ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhSetFsDhcp6SrvOptionPreference ARG_LIST((UINT4  , UINT4  ,INT4 ));
extern INT1
nmhSetFsDhcp6SrvOptionRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));
extern INT1
nmhTestv2FsDhcp6SrvOptionType ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));
extern INT1
nmhTestv2FsDhcp6SrvOptionLength ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));
extern INT1
nmhTestv2FsDhcp6SrvOptionValue ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2FsDhcp6SrvOptionPreference ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));
extern INT1
nmhTestv2FsDhcp6SrvOptionRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));


extern INT1
nmhGetFirstIndexFsDhcp6SrvSubOptionTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));
extern INT1
nmhGetNextIndexFsDhcp6SrvSubOptionTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));
extern INT1
nmhGetFsDhcp6SrvSubOptionLength ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));
extern INT1
nmhGetFsDhcp6SrvSubOptionValue ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhGetFsDhcp6SrvSubOptionRowStatus ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));
extern INT1
nmhSetFsDhcp6SrvSubOptionLength ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1
nmhSetFsDhcp6SrvSubOptionValue ARG_LIST((UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhSetFsDhcp6SrvSubOptionRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

extern INT1
nmhTestv2FsDhcp6SrvSubOptionLength ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1
nmhTestv2FsDhcp6SrvSubOptionValue ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2FsDhcp6SrvSubOptionRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));



extern INT4 FsDhcp6SrvDebugTraceGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvRealmTableNextIndexGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvClientTableNextIndexGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvPoolTableNextIndexGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvIncludePrefixTableNextIndexGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvTrapAdminControlGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvSysLogAdminStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvListenPortGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvClientTransmitPortGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvRelayTransmitPortGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvDebugTraceSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvTrapAdminControlSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvSysLogAdminStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvListenPortSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvClientTransmitPortSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvRelayTransmitPortSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvDebugTraceTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvTrapAdminControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvSysLogAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvListenPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvClientTransmitPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvRelayTransmitPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvDebugTraceDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
extern INT4 FsDhcp6SrvTrapAdminControlDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
extern INT4 FsDhcp6SrvSysLogAdminStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
extern INT4 FsDhcp6SrvListenPortDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
extern INT4 FsDhcp6SrvClientTransmitPortDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
extern INT4 FsDhcp6SrvRelayTransmitPortDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
extern INT4 GetNextIndexFsDhcp6SrvPoolTable(tSnmpIndex *, tSnmpIndex *);
extern INT4 FsDhcp6SrvPoolNameGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvPoolPreferenceGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvPoolDuidTypeGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvPoolDuidGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvPoolDuidIfIndexGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvPoolOptionTableNextIndexGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvPoolRowStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvPoolNameSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvPoolPreferenceSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvPoolDuidTypeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvPoolDuidIfIndexSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvPoolRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvPoolNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvPoolPreferenceTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvPoolDuidTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvPoolDuidIfIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvPoolRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvPoolTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
extern INT4 GetNextIndexFsDhcp6SrvIncludePrefixTable(tSnmpIndex *, tSnmpIndex *);
extern INT4 FsDhcp6SrvIncludePrefixGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvIncludePrefixPoolGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvIncludePrefixRowStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvIncludePrefixSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvIncludePrefixPoolSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvIncludePrefixRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvIncludePrefixTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvIncludePrefixPoolTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvIncludePrefixRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvIncludePrefixTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
extern INT4 GetNextIndexFsDhcp6SrvOptionTable(tSnmpIndex *, tSnmpIndex *);
extern INT4 FsDhcp6SrvOptionTypeGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvOptionLengthGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvOptionValueGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvOptionPreferenceGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvOptionRowStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvOptionTypeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvOptionLengthSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvOptionValueSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvOptionPreferenceSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvOptionRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvOptionTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvOptionLengthTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvOptionValueTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvOptionPreferenceTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvOptionRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvOptionTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
extern INT4 GetNextIndexFsDhcp6SrvSubOptionTable(tSnmpIndex *, tSnmpIndex *);
extern INT4 FsDhcp6SrvSubOptionLengthGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvSubOptionValueGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvSubOptionRowStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvSubOptionLengthSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvSubOptionValueSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvSubOptionRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvSubOptionLengthTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvSubOptionValueTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvSubOptionRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvSubOptionTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
extern INT4 GetNextIndexFsDhcp6SrvClientTable(tSnmpIndex *, tSnmpIndex *);
extern INT4 FsDhcp6SrvClientIdGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvClientIdTypeGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvClientRealmGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvClientRowStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvClientIdSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvClientIdTypeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvClientRealmSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvClientRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvClientIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvClientIdTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvClientRealmTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvClientRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvClientTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
extern INT4 GetNextIndexFsDhcp6SrvRealmTable(tSnmpIndex *, tSnmpIndex *);
extern INT4 FsDhcp6SrvRealmNameGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvRealmKeyTableNextIndexGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvRealmRowStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvRealmNameSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvRealmRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvRealmNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvRealmRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvRealmTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
extern INT4 GetNextIndexFsDhcp6SrvKeyTable(tSnmpIndex *, tSnmpIndex *);
extern INT4 FsDhcp6SrvKeyGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvKeyRowStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvKeySet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvKeyRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvKeyTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvKeyRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvKeyTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
extern INT4 GetNextIndexFsDhcp6SrvIfTable(tSnmpIndex *, tSnmpIndex *);
extern INT4 FsDhcp6SrvIfPoolGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvIfInformInGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvIfRelayForwInGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvIfReplyOutGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvIfRelayReplyOutGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvIfInvalidPktInGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvIfUnknownTlvTypeGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvIfHmacFailCountGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvIfCounterResetGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvIfRowStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvIfPoolSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvIfCounterResetSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvIfRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvIfPoolTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvIfCounterResetTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvIfRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsDhcp6SrvIfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif


#endif 
