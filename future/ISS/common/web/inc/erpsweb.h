/***************************************************************************
 *  * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 *   $Id: erpsweb.h,v 1.7 2016/01/11 13:15:11 siva Exp $
 * ********************************************************************/

#ifndef _ERPSWEB_H
#define _ERPSWEB_H
#ifdef ERPS_WANTED
/* List of vlans to be added */
extern UINT1               gau1ErpsVlanList[VLAN_LIST_SIZE];
/* List of vlans to be deleted */
extern UINT1               gau1ErpsVlanDelList[VLAN_LIST_SIZE];
/* GET FIRST Routines */
extern INT1
nmhGetFirstIndexFsErpsRingTable (UINT4 * , UINT4 *);

/* GET NEXT Routines */
extern INT1
nmhGetNextIndexFsErpsRingTable (UINT4 , UINT4 * , UINT4 , UINT4 *);
extern INT1
nmhGetNextIndexFsErpsRingTcPropTable (UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *);

extern INT1
nmhGetFirstIndexFsErpsVlanGroupTable (UINT4 *, INT4 *, INT4 *);
extern INT1
nmhGetNextIndexFsErpsVlanGroupTable (UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *);

extern INT1
nmhGetFirstIndexFsErpsContextTable (UINT4 *);
extern INT1
nmhGetNextIndexFsErpsContextTable (UINT4 , UINT4 *);


/* GET Routines */
extern INT1
nmhGetFsErpsRingName (UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * );
extern INT1
nmhGetFsErpsRingVlanId (UINT4  , UINT4 ,INT4 *);
extern INT1
nmhGetFsErpsRingPort1 (UINT4  , UINT4 ,INT4 *);
extern INT1
nmhGetFsErpsRingPort2 (UINT4  , UINT4 ,INT4 *);
extern INT1
nmhGetFsErpsRingRplPort (UINT4  , UINT4 ,INT4 *);
extern INT1
nmhGetFsErpsRingNodeType (UINT4  , UINT4 ,INT4 *);
extern INT1
nmhGetFsErpsRingOperatingMode (UINT4  , UINT4 ,INT4 *);
extern INT1
nmhGetFsErpsRingPort1Status (UINT4  , UINT4 ,INT4 *);
extern INT1
nmhGetFsErpsRingPort2Status (UINT4  , UINT4 ,INT4 *);
extern INT1
nmhGetFsErpsRingSemState (UINT4  , UINT4 ,INT4 *);
extern INT1
nmhGetFsErpsRingNodeStatus (UINT4  , UINT4 ,INT4 *);
extern INT1
nmhGetFsErpsRingRowStatus (UINT4  , UINT4 ,INT4 *);
extern INT1
nmhGetFsErpsRingMEG1 (UINT4  , UINT4 ,UINT4 *);
extern INT1
nmhGetFsErpsRingCfmME1 (UINT4  , UINT4 ,UINT4 *);
extern INT1
nmhGetFsErpsRingCfmMEP1 (UINT4  , UINT4 ,UINT4 *);
extern INT1
nmhGetFsErpsRingMEG2 (UINT4  , UINT4 ,UINT4 *);
extern INT1
nmhGetFsErpsRingCfmME2 (UINT4  , UINT4 ,UINT4 *);
extern INT1
nmhGetFsErpsRingCfmMEP2 (UINT4  , UINT4 ,UINT4 *);
extern INT1
nmhGetFsErpsRingCfmRowStatus (UINT4  , UINT4 ,INT4 *);
extern INT1
nmhGetFsErpsRingConfigHoldOffTime (UINT4  , UINT4 ,UINT4 *);
extern INT1
nmhGetFsErpsRingConfigGuardTime (UINT4  , UINT4 ,UINT4 *);
extern INT1
nmhGetFsErpsRingConfigWTRTime (UINT4  , UINT4 ,UINT4 *);
extern INT1
nmhGetFsErpsRingConfigPeriodicTime (UINT4  , UINT4 ,UINT4 *);
extern INT1
nmhGetFsErpsRingConfigSwitchPort (UINT4  , UINT4 ,INT4 *);
extern INT1
nmhGetFsErpsRingConfigSwitchCmd (UINT4  , UINT4 ,INT4 *);
extern INT1
nmhGetFsErpsRingConfigRecoveryMethod (UINT4  , UINT4 ,INT4 *);
extern INT1
nmhGetFsErpsRingConfigPropagateTC (UINT4  , UINT4 ,INT4 *);
extern INT1
nmhGetFsErpsRingTcPropRowStatus (UINT4  , UINT4  , UINT4 ,INT4 *);
extern INT1
nmhGetFsErpsRingPortBlockingOnVcRecovery (UINT4  , UINT4 ,INT4 *);
extern INT1
nmhGetFsErpsRingRAPSCompatibleVersion (UINT4 , UINT4 ,INT4 *);
extern INT1
nmhGetFsErpsRingRplNeighbourPort (UINT4  , UINT4 ,INT4 *);
extern INT1
nmhGetFsErpsRingRplNextNeighbourPort (UINT4  , UINT4 ,INT4 *);
extern INT1
nmhGetFsErpsRingMacId (UINT4  , UINT4 ,INT4 *);
extern INT1
nmhGetFsErpsRingProtectedVlanGroupId (UINT4  , UINT4 ,INT4 *);
extern INT1
nmhGetFsErpsRingProtectionType (UINT4  , UINT4 ,INT4 *);
extern INT1
nmhGetFsErpsRingRAPSSubRingWithoutVC (UINT4  , UINT4 ,INT4 *);
extern INT1
nmhGetFsErpsRingConfigExtVCRecoveryPeriodicTime (UINT4  , UINT4 ,INT4 *);
extern INT1
nmhGetFsErpsRingConfigClear(UINT4 ,UINT4 ,INT4 *);
extern INT1
nmhGetFsErpsVlanGroupRowStatus (UINT4, INT4, INT4,INT4 *);

extern INT1
nmhGetFsErpsRingPwVcId1 (UINT4 , UINT4, UINT4 *);
extern INT1
nmhGetFsErpsRingPwVcId2 (UINT4 , UINT4, UINT4 *);
extern INT1
nmhSetFsErpsRingPwVcId1 (UINT4  , UINT4  ,UINT4 );
extern INT1
nmhSetFsErpsRingPwVcId2 (UINT4  , UINT4  ,UINT4 );
extern INT1
nmhSetFsErpsRingMplsRowStatus (UINT4  , UINT4  ,INT4 );
extern INT1
nmhTestv2FsErpsRingPwVcId1 (UINT4 *  ,UINT4  , UINT4  ,UINT4 );
extern INT1
nmhTestv2FsErpsRingPwVcId2 (UINT4 *  ,UINT4  , UINT4  ,UINT4 );
extern INT1
nmhTestv2FsErpsRingMplsRowStatus (UINT4 *  ,UINT4  , UINT4  ,INT4 );
extern INT1
nmhGetFsErpsRingMonitorMechanism (UINT4  , UINT4 ,INT4 *);

/* SET Routines */

extern INT1
nmhSetFsErpsRingVlanId (UINT4  , UINT4  ,INT4 );
extern INT1
nmhSetFsErpsRingName (UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *);
extern INT1
nmhSetFsErpsRingPort1 (UINT4  , UINT4  ,INT4 );
extern INT1
nmhSetFsErpsRingPort2 (UINT4  , UINT4  ,INT4 );
extern INT1
nmhSetFsErpsRingRplPort (UINT4  , UINT4  ,INT4 );
extern INT1
nmhSetFsErpsRingOperatingMode (UINT4  , UINT4  ,INT4 );
extern INT1
nmhSetFsErpsRingRowStatus (UINT4  , UINT4  ,INT4 );
extern INT1
nmhSetFsErpsRingMEG1 (UINT4  , UINT4  ,UINT4 );
extern INT1
nmhSetFsErpsRingCfmME1 (UINT4  , UINT4  ,UINT4 );
extern INT1
nmhSetFsErpsRingCfmMEP1 (UINT4  , UINT4  ,UINT4 );
extern INT1
nmhSetFsErpsRingMEG2 (UINT4  , UINT4  ,UINT4 );
extern INT1
nmhSetFsErpsRingCfmME2 (UINT4  , UINT4  ,UINT4 );
extern INT1
nmhSetFsErpsRingCfmMEP2 (UINT4  , UINT4  ,UINT4 );
extern INT1
nmhSetFsErpsRingCfmRowStatus (UINT4  , UINT4  ,INT4 );
extern INT1
nmhSetFsErpsRingConfigHoldOffTime (UINT4  , UINT4  ,UINT4 );
extern INT1
nmhSetFsErpsRingConfigGuardTime (UINT4  , UINT4  ,UINT4 );
extern INT1
nmhSetFsErpsRingConfigWTRTime (UINT4  , UINT4  ,UINT4 );
extern INT1
nmhSetFsErpsRingConfigPeriodicTime (UINT4  , UINT4  ,UINT4 );
extern INT1
nmhSetFsErpsRingConfigSwitchPort (UINT4  , UINT4  ,INT4 );
extern INT1
nmhSetFsErpsRingConfigSwitchCmd (UINT4  , UINT4  ,INT4 );
extern INT1
nmhSetFsErpsRingConfigRecoveryMethod (UINT4  , UINT4  ,INT4 );
extern INT1
nmhSetFsErpsRingConfigPropagateTC (UINT4  , UINT4  ,INT4 );
extern INT1
nmhSetFsErpsRingTcPropRowStatus (UINT4  , UINT4  , UINT4  ,INT4 );
extern INT1
nmhSetFsErpsRingPortBlockingOnVcRecovery (UINT4  , UINT4  ,INT4 );
extern INT1
nmhSetFsErpsRingRAPSCompatibleVersion  (UINT4  , UINT4  ,INT4 );
extern INT1
nmhSetFsErpsRingRplNeighbourPort (UINT4  , UINT4  ,INT4 );
extern INT1
nmhSetFsErpsRingRplNextNeighbourPort (UINT4  , UINT4  ,INT4 );
extern INT1
nmhSetFsErpsRingMacId (UINT4  , UINT4  ,INT4 );
extern INT1
nmhSetFsErpsRingProtectedVlanGroupId (UINT4  , UINT4  ,INT4 );
extern INT1
nmhSetFsErpsRingProtectionType (UINT4  , UINT4  ,INT4 );
extern INT1
nmhSetFsErpsRingRAPSSubRingWithoutVC (UINT4  , UINT4  ,INT4 );
extern INT1
nmhSetFsErpsVlanGroupRowStatus (UINT4  , INT4  , INT4  ,INT4 );
extern INT1
nmhSetFsErpsRingConfigClear(UINT4 ,UINT4 ,INT4 );


/*Test Routines */

extern INT1
nmhTestv2FsErpsRingVlanId (UINT4 *  ,UINT4  , UINT4  ,INT4 );
extern INT1
nmhTestv2FsErpsRingName (UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *);
extern INT1
nmhTestv2FsErpsRingPort1 (UINT4 *  ,UINT4  , UINT4  ,INT4 );
extern INT1
nmhTestv2FsErpsRingPort2 (UINT4 *  ,UINT4  , UINT4  ,INT4 );
extern INT1
nmhTestv2FsErpsRingRplPort (UINT4 *  ,UINT4  , UINT4  ,INT4 );
extern INT1
nmhTestv2FsErpsRingOperatingMode (UINT4 *  ,UINT4  , UINT4  ,INT4 );
extern INT1
nmhTestv2FsErpsRingRowStatus (UINT4 *  ,UINT4  , UINT4  ,INT4 );
extern INT1
nmhTestv2FsErpsRingMEG1 (UINT4 *  ,UINT4  , UINT4  ,UINT4 );
extern INT1
nmhTestv2FsErpsRingCfmME1 (UINT4 *  ,UINT4  , UINT4  ,UINT4 );
extern INT1
nmhTestv2FsErpsRingCfmMEP1 (UINT4 *  ,UINT4  , UINT4  ,UINT4 );
extern INT1
nmhTestv2FsErpsRingMEG2 (UINT4 *  ,UINT4  , UINT4  ,UINT4 );
extern INT1
nmhTestv2FsErpsRingCfmME2 (UINT4 *  ,UINT4  , UINT4  ,UINT4 );
extern INT1
nmhTestv2FsErpsRingCfmMEP2 (UINT4 *  ,UINT4  , UINT4  ,UINT4 );
extern INT1
nmhTestv2FsErpsRingCfmRowStatus (UINT4 *  ,UINT4  , UINT4  ,INT4 );
extern INT1
nmhTestv2FsErpsRingConfigHoldOffTime (UINT4 *  ,UINT4  , UINT4  ,UINT4 );
extern INT1
nmhTestv2FsErpsRingConfigGuardTime (UINT4 *  ,UINT4  , UINT4  ,UINT4 );
extern INT1
nmhTestv2FsErpsRingConfigWTRTime (UINT4 *  ,UINT4  , UINT4  ,UINT4 );
extern INT1
nmhTestv2FsErpsRingConfigPeriodicTime (UINT4 *  ,UINT4  , UINT4  ,UINT4 );
extern INT1
nmhTestv2FsErpsRingConfigSwitchPort (UINT4 *  ,UINT4  , UINT4  ,INT4 );
extern INT1
nmhTestv2FsErpsRingConfigSwitchCmd (UINT4 *  ,UINT4  , UINT4  ,INT4 );
extern INT1
nmhTestv2FsErpsRingConfigRecoveryMethod (UINT4 *  ,UINT4  , UINT4  ,INT4 );
extern INT1
nmhTestv2FsErpsRingConfigPropagateTC (UINT4 *  ,UINT4  , UINT4  ,INT4 );
extern INT1
nmhTestv2FsErpsRingTcPropRowStatus (UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 );
extern INT1
nmhTestv2FsErpsRingPortBlockingOnVcRecovery (UINT4 *  ,UINT4  , UINT4  ,INT4 );
extern INT1
nmhTestv2FsErpsRingRplNeighbourPort (UINT4 *  ,UINT4  , UINT4  ,INT4 );
extern INT1
nmhTestv2FsErpsRingRplNextNeighbourPort (UINT4 *  ,UINT4  , UINT4  ,INT4 );
extern INT1
nmhTestv2FsErpsRingMacId (UINT4 *  ,UINT4  , UINT4  ,INT4 );
extern INT1
nmhTestv2FsErpsRingProtectedVlanGroupId (UINT4 *  ,UINT4  , UINT4  ,INT4 );
extern INT1
nmhTestv2FsErpsRingProtectionType (UINT4 *  ,UINT4  , UINT4  ,INT4 );
extern INT1
nmhTestv2FsErpsRingRAPSCompatibleVersion  (UINT4 *  ,UINT4  , UINT4  ,INT4 );
extern INT1
nmhTestv2FsErpsRingRAPSSubRingWithoutVC (UINT4 *  ,UINT4  , UINT4  ,INT4 );
extern INT1
nmhTestv2FsErpsVlanGroupRowStatus (UINT4 *  ,UINT4 ,INT4 ,INT4 ,INT4);
extern INT1
nmhTestv2FsErpsRingConfigClear(UINT4 * ,UINT4 , UINT4 ,INT4 );
extern INT1
nmhValidateIndexInstanceFsErpsContextTable(UINT4);
#endif
#endif


