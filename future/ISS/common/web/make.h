#!/bin/csh
# $Id: make.h,v 1.14
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : GNU make                                      |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 27/04/2003                                    |
# |                                                                          |
# |   DESCRIPTION            : Specifies the options and modules to be       |
# |                            including for building the FutureISS web      |
# |                            module.                                       |
# |                                                                          |
# +--------------------------------------------------------------------------+
#
#     CHANGE RECORD :
# +--------------------------------------------------------------------------+
# | VERSION | AUTHOR/    | DESCRIPTION OF CHANGE                             |
# |         | DATE       |                                                   |
# +---------|------------|---------------------------------------------------+
# |   1     | DIFFSERV   | Creation of makefile                              |
# |         | 27/04/2003 |                                                   |
# +--------------------------------------------------------------------------+
# |   2     | 04/08/2004 | LR/make.h added to PROJECT_DEPENDENCIES           |
# +--------------------------------------------------------------------------+


# Set the PROJ_BASE_DIR as the directory where you untar the project files
PROJECT_NAME  = ISSWebint
PROJECT_BASE_DIR = ${BASE_DIR}/ISS/common/web
PROJECT_SOURCE_DIR = ${PROJECT_BASE_DIR}/src
PROJECT_INCLUDE_DIR = ${PROJECT_BASE_DIR}/inc
PROJECT_OBJECT_DIR = ${PROJECT_BASE_DIR}/obj

FUTURE_INC_DIR     = $(BASE_DIR)/inc
WEBNM_INC_DIR        = $(BASE_DIR)/webnm
WEBNM_UTIL_INC_DIR   = $(BASE_DIR)/util/webnm/
COMMON_SYSTEM_INC_DIR = ${BASE_DIR}/ISS/common/system/inc
VPN_INC_DIR           = ${BASE_DIR}/vpn/inc
RSNA_DIR               = ${BASE_DIR}/wss/rsna/inc
WPS_DIR                = ${BASE_DIR}/wss/wps/inc
FUTURE_INC_CLI_DIR     =  $(BASE_DIR)/inc/cli
PNAC_DIR               = $(BASE_DIR)/pnac/inc

ifneq (,$(filter YES ,${WLC} ${WTP}))
CAPWAP_INC_DIR        = ${BASE_DIR}/wss/capwap/inc
DTLS_INC_DIR        = ${BASE_DIR}/wss/dtls/inc
WSSCFG_INC_DIR        = ${BASE_DIR}/wss/wsscfg/inc
WSSIF_INC_DIR        = ${BASE_DIR}/wss/wssif/inc
WSSSTA_INC_DIR        = $(BASE_DIR)/wss/wsssta/inc
RADIOIF_INC_DIR        = ${BASE_DIR}/wss/radioif/inc
WSSPM_INC_DIR          = ${BASE_DIR}/wss/wsspm/inc
RFMGMT_INC_DIR        = ${BASE_DIR}/wss/rfmgmt/inc
USERROLE_INC_DIR      = ${BASE_DIR}/wss/wssuser/inc
endif

# Specify the project include directories and dependencies
PROJECT_INCLUDE_FILES  = $(PROJECT_INCLUDE_DIR)/webiss.h \
    $(PROJECT_INCLUDE_DIR)/ptpweb.h \
                         $(PROJECT_INCLUDE_DIR)/mplsweb.h \
                         $(PROJECT_INCLUDE_DIR)/rmapweb.h \
                         $(PROJECT_INCLUDE_DIR)/tacweb.h \
                         $(PROJECT_INCLUDE_DIR)/ecfmweb.h \
                         $(PROJECT_INCLUDE_DIR)/erpsweb.h \
                         $(PROJECT_INCLUDE_DIR)/fwlweb.h \
                         $(PROJECT_INCLUDE_DIR)/lldpweb.h\
                         

PROJECT_FINAL_INCLUDES_DIRS =  -I$(PROJECT_INCLUDE_DIR) \
                                 -I$(WEBNM_INC_DIR) \
                                 -I$(WEBNM_UTIL_INC_DIR) \
                                 -I$(COMMON_SYSTEM_INC_DIR) \
                                   $(COMMON_INCLUDE_DIRS) \
                                  -I$(FUTURE_INC_DIR)\
                                  -I$(RSNA_DIR)\
                                  -I$(WPS_DIR)\
                                  -I$(FUTURE_INC_CLI_DIR)\
                                  -I$(PNAC_DIR)
                                  

ifneq (,$(filter YES ,${WLC} ${WTP}))
PROJECT_FINAL_INCLUDES_DIRS += -I$(CAPWAP_INC_DIR) \
                               -I$(DTLS_INC_DIR)\
                               -I$(WSSCFG_INC_DIR)\
                               -I$(RADIOIF_INC_DIR)\
                               -I$(WSSIF_INC_DIR) \
                               -I$(WSSSTA_INC_DIR) \
                               -I$(WSSPM_INC_DIR) \
                               -I$(RFMGMT_INC_DIR)\
                               -I$(USERROLE_INC_DIR)
endif



PROJECT_FINAL_INCLUDE_FILES    +=  $(PROJECT_INCLUDE_FILES)

PROJECT_DEPENDENCIES =       $(PROJECT_FINAL_INCLUDE_FILES) \
    $(PROJECT_BASE_DIR)/Makefile \
    $(PROJECT_BASE_DIR)/make.h \
    $(COMMON_DEPENDENCIES)
