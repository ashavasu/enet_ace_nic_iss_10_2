/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved]
 *
 * $Id: elpsweb.c,v 1.8 2013/12/10 10:19:35 siva Exp $
 *
 * Description: This file contains routines for ELPS web Module
 *
 ********************************************************************/

#ifndef _ELPS_WEB_C_
#define _ELPS_WEB_C_

#ifdef WEBNM_WANTED
#include "webiss.h"
#include "webinc.h"
#include "isshttp.h"
#include "elps.h"
#include "fselpscli.h"

/*********************************************************************
*  Function Name : IssPrintAvailablePgId
*  Description   : This function prints available PgId.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssPrintAvailablePgId (tHttp * pHttp)
{
    UINT4               u4ContextId = 0;
    UINT4               u4PgId = 0;
    UINT4               u4NextContextId = 0;
    UINT4               u4NextPgId = 0;
    INT4                i4OutCome;

    i4OutCome = nmhGetFirstIndexFsElpsPgConfigTable (&u4NextContextId,
                                                     &u4NextPgId);
    /* No need to Take the Lock as it will be taken in the api called 
     * within the function */

    if (i4OutCome == SNMP_FAILURE)
    {
        return;
    }

    STRCPY (pHttp->au1KeyString, "<! PG_ID>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString,
                 "<option value = \"%u\" selected>%u \n", u4NextPgId,
                 u4NextPgId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        u4ContextId = u4NextContextId;
        u4PgId = u4NextPgId;

    }
    while (nmhGetNextIndexFsElpsPgConfigTable (u4ContextId,
                                               &u4NextContextId,
                                               u4PgId,
                                               &u4NextPgId) == SNMP_SUCCESS);
}

/*********************************************************************
*  Function Name : IssProcessElpsPGinfoPage
*  Description   : This function processes the request coming for the 
*                  PG information page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessElpsPGinfoPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessElpsPGinfoPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessElpsPGinfoPageSet (pHttp);
    }
    return;
}

/*******************************************************************************
 *  Function Name : IssProcessElpsPGinfoPageGet
 *  Description   : This function processes the get request for the Elps
 *                  Protection group configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessElpsPGinfoPageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE ConfigName;
    UINT1               au1ConfigName[MAX_ADDRESS_BUFFER_VLAN + 1];
    UINT4               u4ContextId = 0;
    UINT4               u4PgId = 0;
    UINT4               u4NextContextId = 0;
    UINT4               u4NextPgId = 0;
    UINT4               u4Temp = 0;
    UINT4               u4RetVal = 0;
    INT4                i4RetVal = 0;

    MEMSET (au1ConfigName, 0, sizeof (au1ConfigName));

    ConfigName.pu1_OctetList = au1ConfigName;
    ConfigName.i4_Length = 0;

    WebnmRegisterLock (pHttp, ElpsApiLock, ElpsApiUnLock);
    ELPS_LOCK ();

    pHttp->i4Write = 0;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    if (nmhGetFirstIndexFsElpsPgConfigTable (&u4ContextId,
                                             &u4PgId) == SNMP_FAILURE)
    {
        ELPS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;

    u4ContextId = 0;
    u4PgId = 0;

    while (nmhGetNextIndexFsElpsPgConfigTable (u4ContextId, &u4NextContextId,
                                               u4PgId,
                                               &u4NextPgId) == SNMP_SUCCESS)
    {
        pHttp->i4Write = (INT4) u4Temp;

        /* Context ID */
        STRCPY (pHttp->au1KeyString, "CONTEXT_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4NextContextId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Protection Group ID */
        STRCPY (pHttp->au1KeyString, "PROTECTION_GID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4NextPgId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Config Type */
        STRCPY (pHttp->au1KeyString, "CONFIG_TYPE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsElpsPgConfigType (u4NextContextId, u4NextPgId,
                                      &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Service Type */
        STRCPY (pHttp->au1KeyString, "SERVICE_TYPE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsElpsPgConfigServiceType (u4NextContextId, u4NextPgId,
                                             &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Monitor Mechanism */
        STRCPY (pHttp->au1KeyString, "MONITOR_MECHANISM_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsElpsPgConfigMonitorMechanism (u4NextContextId, u4NextPgId,
                                                  &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Ingress Port */
        STRCPY (pHttp->au1KeyString, "INGRESS_PORT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsElpsPgConfigIngressPort (u4NextContextId, u4NextPgId,
                                             &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Working Port */
        STRCPY (pHttp->au1KeyString, "WORKING_PORT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsElpsPgConfigWorkingPort (u4NextContextId, u4NextPgId,
                                             &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Protection Port */
        STRCPY (pHttp->au1KeyString, "PROTECTION_PORT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsElpsPgConfigProtectionPort (u4NextContextId, u4NextPgId,
                                                &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Working Service Value */
        STRCPY (pHttp->au1KeyString, "WSERVICE_VALUE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4RetVal = 0;
        if (nmhGetFsElpsPgConfigWorkingServiceValue
            (u4NextContextId, u4NextPgId, &u4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Protection Service Value */
        STRCPY (pHttp->au1KeyString, "PSERVICE_VALUE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4RetVal = 0;
        if (nmhGetFsElpsPgConfigProtectionServiceValue (u4NextContextId,
                                                        u4NextPgId,
                                                        &u4RetVal)
            == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Oper Type */
        STRCPY (pHttp->au1KeyString, "OPER_TYPE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsElpsPgConfigOperType (u4NextContextId, u4NextPgId,
                                          &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Prot Type */
        STRCPY (pHttp->au1KeyString, "PROT_TYPE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsElpsPgConfigProtType (u4NextContextId, u4NextPgId,
                                          &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Config Name */
        STRCPY (pHttp->au1KeyString, "CONFIG_NAME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetFsElpsPgConfigName (u4NextContextId, u4NextPgId,
                                      &ConfigName) == SNMP_SUCCESS)
        {
            if (ConfigName.i4_Length != 0)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         ConfigName.pu1_OctetList);
            }
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        u4ContextId = u4NextContextId;
        u4PgId = u4NextPgId;
    }

    ELPS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*******************************************************************************
*  Function Name : IssProcessElpsPGinfoPageSet
*  Description   : This function processes the HTTP set request coming for the
*                  Elps Protection Group configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None.
*******************************************************************************/
VOID
IssProcessElpsPGinfoPageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE ConfigName;
    UINT1               au1ConfigName[MAX_ADDRESS_BUFFER_VLAN];
    UINT4               u4ContextId = 0;
    UINT4               u4PgId = 0;
    UINT4               u4ErrValue = 0;
    UINT4               u4WorkServiceValue = 0;
    UINT4               u4ProtServiceValue = 0;
    UINT4               u4RetVal = 0;
    INT4                i4SystemControl = 0;
    INT4                i4ConfigType = 0;
    INT4                i4ServiceType = 0;
    INT4                i4MonitorMechanism = 0;
    INT4                i4IngressPort = 0;
    INT4                i4WorkingPort = 0;
    INT4                i4ProtectionPort = 0;
    INT4                i4OperType = 0;
    INT4                i4ProtType = 0;
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = 0;

    MEMSET (au1ConfigName, 0, sizeof (au1ConfigName));

    ConfigName.pu1_OctetList = au1ConfigName;
    ConfigName.i4_Length = 0;

    WebnmRegisterLock (pHttp, ElpsApiLock, ElpsApiUnLock);
    ELPS_LOCK ();

    STRCPY (pHttp->au1Name, "CONTEXT_ID");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        u4ContextId = (UINT4) ATOI (pHttp->au1Value);
    }

    nmhGetFsElpsContextSystemControl (u4ContextId, &i4SystemControl);
    if (i4SystemControl != ELPS_START)
    {
        ELPS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Elps is shutdown. No configuration possible");
        return;
    }

    STRCPY (pHttp->au1Name, "PROTECTION_GID");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        u4PgId = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "CONFIG_TYPE");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4ConfigType = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "SERVICE_TYPE");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4ServiceType = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "MONITOR_MECHANISM");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4MonitorMechanism = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "INGRESS_PORT");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4IngressPort = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "WORKING_PORT");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4WorkingPort = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "PROTECTION_PORT");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4ProtectionPort = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "WSERVICE_VALUE");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        u4WorkServiceValue = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "PSERVICE_VALUE");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        u4ProtServiceValue = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "OPER_TYPE");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4OperType = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "PROT_TYPE");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4ProtType = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "CONFIG_NAME");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        ConfigName.i4_Length = (INT4) STRLEN (pHttp->au1Value);
        MEMCPY (ConfigName.pu1_OctetList, pHttp->au1Value,
                MEM_MAX_BYTES (ConfigName.i4_Length, MAX_ADDRESS_BUFFER_VLAN));
    }

    STRCPY (pHttp->au1Name, "ACTION");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        if (STRCMP (pHttp->au1Value, "Apply") == 0)
        {

            nmhGetFsElpsPgConfigRowStatus (u4ContextId, u4PgId, &i4RowStatus);

            if (i4RowStatus == ACTIVE)
            {
                if (nmhTestv2FsElpsPgConfigRowStatus (&u4ErrValue, u4ContextId,
                                                      u4PgId, NOT_IN_SERVICE)
                    == SNMP_FAILURE)
                {
                    ELPS_UNLOCK ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                                  "needed to activate Protection Group are not applied");
                    return;
                }
                if (nmhSetFsElpsPgConfigRowStatus (u4ContextId, u4PgId,
                                                   NOT_IN_SERVICE)
                    == SNMP_FAILURE)
                {
                    ELPS_UNLOCK ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Entry creation failed");
                    return;
                }
            }
        }
        if (STRCMP (pHttp->au1Value, "Delete") == 0)
        {
            /* Delete the PgCfm Rowstatus */
            if (nmhTestv2FsElpsPgCfmRowStatus (&u4ErrValue, u4ContextId, u4PgId,
                                               DESTROY) == SNMP_FAILURE)
            {
                ELPS_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                              "needed to activate Protection Group are not applied");
                return;
            }
            if (nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId,
                                            DESTROY) == SNMP_FAILURE)
            {
                ELPS_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Entry creation failed");
                return;
            }

            /* Delete the PgConfig Rowstatus */
            if (nmhTestv2FsElpsPgConfigRowStatus (&u4ErrValue, u4ContextId,
                                                  u4PgId,
                                                  DESTROY) == SNMP_FAILURE)
            {
                ELPS_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                              "needed to activate Protection Group are not applied");
                return;
            }
            if (nmhSetFsElpsPgConfigRowStatus (u4ContextId, u4PgId,
                                               DESTROY) == SNMP_FAILURE)
            {
                ELPS_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *) "Failed in deleting the entry");
                return;
            }
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssProcessElpsPGinfoPageGet (pHttp);
            return;
        }
    }
    else
    {
        ELPS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Input");
        return;
    }

    i4RetVal = 0;
    nmhGetFsElpsPgConfigType (u4ContextId, u4PgId, &i4RetVal);
    if (i4RetVal != i4ConfigType)
    {
        if (nmhTestv2FsElpsPgConfigType (&u4ErrValue, u4ContextId, u4PgId,
                                         i4ConfigType) == SNMP_FAILURE)
        {
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure config type");
            return;
        }

        if (nmhSetFsElpsPgConfigType (u4ContextId, u4PgId,
                                      i4ConfigType) == SNMP_FAILURE)
        {
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set config type");
            return;
        }
    }

    i4RetVal = 0;
    nmhGetFsElpsPgConfigServiceType (u4ContextId, u4PgId, &i4RetVal);
    if (i4RetVal != i4ServiceType)
    {
        if (nmhTestv2FsElpsPgConfigServiceType (&u4ErrValue, u4ContextId,
                                                u4PgId,
                                                i4ServiceType) == SNMP_FAILURE)
        {
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure service type");
            return;
        }

        if (nmhSetFsElpsPgConfigServiceType (u4ContextId, u4PgId,
                                             i4ServiceType) == SNMP_FAILURE)
        {
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set service type");
            return;
        }
    }

    i4RetVal = 0;
    nmhGetFsElpsPgConfigMonitorMechanism (u4ContextId, u4PgId, &i4RetVal);
    if (i4RetVal != i4MonitorMechanism)
    {
        if (nmhTestv2FsElpsPgConfigMonitorMechanism (&u4ErrValue, u4ContextId,
                                                     u4PgId, i4MonitorMechanism)
            == SNMP_FAILURE)
        {
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure monitor mechanism");
            return;
        }

        if (nmhSetFsElpsPgConfigMonitorMechanism (u4ContextId, u4PgId,
                                                  i4MonitorMechanism)
            == SNMP_FAILURE)
        {
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set monitor mechanism");
            return;
        }
    }

    i4RetVal = 0;
    nmhGetFsElpsPgConfigIngressPort (u4ContextId, u4PgId, &i4RetVal);
    if (i4RetVal != i4IngressPort)
    {
        if (nmhTestv2FsElpsPgConfigIngressPort (&u4ErrValue, u4ContextId,
                                                u4PgId,
                                                i4IngressPort) == SNMP_FAILURE)
        {
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure ingress port");
            return;
        }

        if (nmhSetFsElpsPgConfigIngressPort (u4ContextId, u4PgId,
                                             i4IngressPort) == SNMP_FAILURE)
        {
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set ingress port");
            return;
        }
    }

    i4RetVal = 0;
    nmhGetFsElpsPgConfigWorkingPort (u4ContextId, u4PgId, &i4RetVal);
    if (i4RetVal != i4WorkingPort)
    {
        if (nmhTestv2FsElpsPgConfigWorkingPort (&u4ErrValue, u4ContextId,
                                                u4PgId,
                                                i4WorkingPort) == SNMP_FAILURE)
        {
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure working port");
            return;
        }

        if (nmhSetFsElpsPgConfigWorkingPort (u4ContextId, u4PgId,
                                             i4WorkingPort) == SNMP_FAILURE)
        {
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set working port");
            return;
        }
    }

    i4RetVal = 0;
    nmhGetFsElpsPgConfigProtectionPort (u4ContextId, u4PgId, &i4RetVal);
    if (i4RetVal != i4ProtectionPort)
    {
        if (nmhTestv2FsElpsPgConfigProtectionPort (&u4ErrValue, u4ContextId,
                                                   u4PgId, i4ProtectionPort)
            == SNMP_FAILURE)
        {
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure protection port");
            return;
        }

        if (nmhSetFsElpsPgConfigProtectionPort (u4ContextId, u4PgId,
                                                i4ProtectionPort)
            == SNMP_FAILURE)
        {
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set protection port");
            return;
        }
    }

    u4RetVal = 0;
    nmhGetFsElpsPgConfigWorkingServiceValue (u4ContextId, u4PgId, &u4RetVal);
    if (u4RetVal != u4WorkServiceValue)
    {
        if (nmhTestv2FsElpsPgConfigWorkingServiceValue (&u4ErrValue,
                                                        u4ContextId, u4PgId,
                                                        u4WorkServiceValue)
            == SNMP_FAILURE)
        {
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure working Service Value");
            return;
        }

        if (nmhSetFsElpsPgConfigWorkingServiceValue (u4ContextId, u4PgId,
                                                     u4WorkServiceValue)
            == SNMP_FAILURE)
        {
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set working Service Value");
            return;
        }
    }

    u4RetVal = 0;
    nmhGetFsElpsPgConfigProtectionServiceValue (u4ContextId, u4PgId, &u4RetVal);
    if (u4RetVal != u4ProtServiceValue)
    {
        if (nmhTestv2FsElpsPgConfigProtectionServiceValue (&u4ErrValue,
                                                           u4ContextId, u4PgId,
                                                           u4ProtServiceValue)
            == SNMP_FAILURE)
        {
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure protection Service Value");
            return;
        }

        if (nmhSetFsElpsPgConfigProtectionServiceValue (u4ContextId, u4PgId,
                                                        u4ProtServiceValue)
            == SNMP_FAILURE)
        {
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set protection Service Value");
            return;
        }
    }

    i4RetVal = 0;
    nmhGetFsElpsPgConfigOperType (u4ContextId, u4PgId, &i4RetVal);
    if (i4RetVal != i4OperType)
    {
        if (nmhTestv2FsElpsPgConfigOperType (&u4ErrValue, u4ContextId, u4PgId,
                                             i4OperType) == SNMP_FAILURE)
        {
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure oper type");
            return;
        }

        if (nmhSetFsElpsPgConfigOperType (u4ContextId, u4PgId, i4OperType)
            == SNMP_FAILURE)
        {
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set oper type Service Value");
            return;
        }
    }

    i4RetVal = 0;
    nmhGetFsElpsPgConfigProtType (u4ContextId, u4PgId, &i4RetVal);
    if (i4RetVal != i4ProtType)
    {
        if (nmhTestv2FsElpsPgConfigProtType (&u4ErrValue, u4ContextId, u4PgId,
                                             i4ProtType) == SNMP_FAILURE)
        {
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure prot type");
            return;
        }

        if (nmhSetFsElpsPgConfigProtType (u4ContextId, u4PgId, i4ProtType)
            == SNMP_FAILURE)
        {
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set oper type prot Value");
            return;
        }
    }

    if (nmhTestv2FsElpsPgConfigName (&u4ErrValue, u4ContextId, u4PgId,
                                     &ConfigName) == SNMP_FAILURE)
    {
        ELPS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to configure name");
        return;
    }

    if (nmhSetFsElpsPgConfigName (u4ContextId, u4PgId, &ConfigName)
        == SNMP_FAILURE)
    {
        ELPS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set config name");
        return;
    }

    i4RetVal = 0;
    nmhGetFsElpsPgConfigRowStatus (u4ContextId, u4PgId, &i4RetVal);
    if (i4RetVal != i4RowStatus)
    {
        if (nmhTestv2FsElpsPgConfigRowStatus (&u4ErrValue, u4ContextId,
                                              u4PgId, ACTIVE) == SNMP_FAILURE)
        {
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                          "needed to activate Protection Group are not applied");
            return;
        }
        if (nmhSetFsElpsPgConfigRowStatus (u4ContextId, u4PgId,
                                           ACTIVE) == SNMP_FAILURE)
        {
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Entry creation failed");
            return;
        }
    }

    ELPS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    IssProcessElpsPGinfoPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssProcessElpsPgCfmPage
*  Description   : This function processes the request coming for the 
*                  PG CFM information page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessElpsPgCfmPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessElpsPgCfmPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessElpsPgCfmPageSet (pHttp);
    }
    return;
}

/*******************************************************************************
 *  Function Name : IssProcessElpsPgCfmPageGet
 *  Description   : This function processes the get request for the Elps
 *                  Protection group CFM configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessElpsPgCfmPageGet (tHttp * pHttp)
{
    UINT4               u4ContextId = 0;
    UINT4               u4PgId = 0;
    UINT4               u4NextContextId = 0;
    UINT4               u4NextPgId = 0;
    UINT4               u4Temp = 0;
    UINT4               u4RetVal = 0;

    WebnmRegisterLock (pHttp, ElpsApiLock, ElpsApiUnLock);
    ELPS_LOCK ();

    pHttp->i4Write = 0;

    IssPrintAvailableContextID (pHttp);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    if (nmhGetFirstIndexFsElpsPgCfmTable (&u4ContextId,
                                          &u4PgId) == SNMP_FAILURE)
    {
        ELPS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;

    u4ContextId = 0;
    u4PgId = 0;

    while (nmhGetNextIndexFsElpsPgCfmTable (u4ContextId, &u4NextContextId,
                                            u4PgId,
                                            &u4NextPgId) == SNMP_SUCCESS)
    {
        pHttp->i4Write = (INT4) u4Temp;

        /* Context ID */
        STRCPY (pHttp->au1KeyString, "CONTEXT_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4NextContextId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Protection Group ID */
        STRCPY (pHttp->au1KeyString, "PROTECTION_GID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4NextPgId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Working MEG */
        STRCPY (pHttp->au1KeyString, "WORKING_MEG_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4RetVal = 0;
        if (nmhGetFsElpsPgCfmWorkingMEG (u4NextContextId, u4NextPgId,
                                         &u4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Working ME */
        STRCPY (pHttp->au1KeyString, "WORKING_ME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4RetVal = 0;
        if (nmhGetFsElpsPgCfmWorkingME (u4NextContextId, u4NextPgId,
                                        &u4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Working MEP */
        STRCPY (pHttp->au1KeyString, "WORKING_MEP_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4RetVal = 0;
        if (nmhGetFsElpsPgCfmWorkingMEP (u4NextContextId, u4NextPgId,
                                         &u4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Protection MEG */
        STRCPY (pHttp->au1KeyString, "PROTECTION_MEG_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4RetVal = 0;
        if (nmhGetFsElpsPgCfmProtectionMEG (u4NextContextId, u4NextPgId,
                                            &u4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Protection ME */
        STRCPY (pHttp->au1KeyString, "PROTECTION_ME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4RetVal = 0;
        if (nmhGetFsElpsPgCfmProtectionME (u4NextContextId, u4NextPgId,
                                           &u4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Protection MEP */
        STRCPY (pHttp->au1KeyString, "PROTECTION_MEP_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4RetVal = 0;
        if (nmhGetFsElpsPgCfmProtectionMEP (u4NextContextId, u4NextPgId,
                                            &u4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        /* Protection MEP */
        STRCPY (pHttp->au1KeyString, "VERSION_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4RetVal = 0;
        if (nmhGetFsElpsPgPscVersion (u4NextContextId, u4NextPgId,
                                      &u4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4ContextId = u4NextContextId;
        u4PgId = u4NextPgId;
    }

    ELPS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*******************************************************************************
*  Function Name : IssProcessElpsPgCfmPageSet
*  Description   : This function processes the HTTP set request coming for the
*                  Elps Protection Group CFM configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None.
*******************************************************************************/
VOID
IssProcessElpsPgCfmPageSet (tHttp * pHttp)
{
    UINT4               u4ContextId = 0;
    UINT4               u4PgId = 0;
    UINT4               u4ErrValue = 0;
    UINT4               u4WorkingMEG = 0;
    UINT4               u4WorkingME = 0;
    UINT4               u4WorkingMEP = 0;
    UINT4               u4ProtectionMEG = 0;
    UINT4               u4ProtectionME = 0;
    UINT4               u4ProtectionMEP = 0;
    UINT4               u4PscVer = 0;
    UINT4               u4RetVal = 0;
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = 0;
    INT4                i4SystemControl = 0;

    WebnmRegisterLock (pHttp, ElpsApiLock, ElpsApiUnLock);
    ELPS_LOCK ();

    STRCPY (pHttp->au1Name, "CONTEXT_ID");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        u4ContextId = (UINT4) ATOI (pHttp->au1Value);
    }

    nmhGetFsElpsContextSystemControl (u4ContextId, &i4SystemControl);
    if (i4SystemControl != ELPS_START)
    {
        ELPS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Elps is shutdown. No configuration possible");
        return;
    }

    STRCPY (pHttp->au1Name, "PROTECTION_GID");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        u4PgId = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "WORKING_MEG");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        u4WorkingMEG = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "WORKING_ME");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        u4WorkingME = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "WORKING_MEP");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        u4WorkingMEP = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "PROTECTION_MEG");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        u4ProtectionMEG = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "PROTECTION_ME");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        u4ProtectionME = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "PROTECTION_MEP");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        u4ProtectionMEP = (UINT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "VERSION");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        u4PscVer = (UINT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "ACTION");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        /* Three types of ACTION will be performed.(ADD and APPLY)
         * Actions other than this will result in a failure and will throw 
         * error */
        if (STRCMP (pHttp->au1Value, "Add") == 0)
        {
            /* Set PG Congfig table rowstatus as Create And Wait */
            i4RetVal = nmhGetFsElpsPgConfigRowStatus (u4ContextId, u4PgId,
                                                      &i4RowStatus);
            if (i4RetVal != SNMP_SUCCESS)
            {
                if (nmhTestv2FsElpsPgConfigRowStatus (&u4ErrValue, u4ContextId,
                                                      u4PgId, CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, DESTROY);
                    ELPS_UNLOCK ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                                  "needed to activate Protection Group are not applied");
                    return;
                }
                if (nmhSetFsElpsPgConfigRowStatus (u4ContextId, u4PgId,
                                                   CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, DESTROY);
                    ELPS_UNLOCK ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Entry creation failed");
                    return;
                }
            }
            else
            {
                ELPS_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Entry Already Exists");
                return;
            }

            i4RetVal = nmhGetFsElpsPgCfmRowStatus (u4ContextId, u4PgId,
                                                   &i4RowStatus);
            if (i4RetVal != SNMP_SUCCESS)
            {
                if (nmhTestv2FsElpsPgCfmRowStatus (&u4ErrValue, u4ContextId,
                                                   u4PgId, CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, DESTROY);
                    ELPS_UNLOCK ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                                  "needed to activate Protection Group are not applied");
                    return;
                }
                if (nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId,
                                                CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, DESTROY);
                    ELPS_UNLOCK ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Entry creation failed");
                    return;
                }
            }
            else
            {
                ELPS_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Entry Already Exists");
                return;
            }
        }
        if (STRCMP (pHttp->au1Value, "Apply") == 0)
        {
            if (nmhTestv2FsElpsPgCfmRowStatus (&u4ErrValue, u4ContextId,
                                               u4PgId, NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, DESTROY);
                ELPS_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                              "needed to activate Protection Group are not applied");
                return;
            }
            if (nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId,
                                            NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, DESTROY);
                ELPS_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Entry creation failed");
                return;
            }
        }
    }
    else
    {
        ELPS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Input");
        return;
    }

    u4RetVal = 0;
    nmhGetFsElpsPgCfmWorkingMEG (u4ContextId, u4PgId, &u4RetVal);
    if (u4RetVal != u4WorkingMEG)
    {
        if (nmhTestv2FsElpsPgCfmWorkingMEG (&u4ErrValue, u4ContextId, u4PgId,
                                            u4WorkingMEG) == SNMP_FAILURE)
        {
            nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, DESTROY);
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure Working MEG");
            return;
        }

        if (nmhSetFsElpsPgCfmWorkingMEG (u4ContextId, u4PgId,
                                         u4WorkingMEG) == SNMP_FAILURE)
        {
            nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, DESTROY);
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set Working MEG");
            return;
        }
    }

    u4RetVal = 0;
    nmhGetFsElpsPgCfmWorkingME (u4ContextId, u4PgId, &u4RetVal);
    if (u4RetVal != u4WorkingME)
    {
        if (nmhTestv2FsElpsPgCfmWorkingME (&u4ErrValue, u4ContextId, u4PgId,
                                           u4WorkingME) == SNMP_FAILURE)
        {
            nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, DESTROY);
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure Working ME");
            return;
        }

        if (nmhSetFsElpsPgCfmWorkingME (u4ContextId, u4PgId,
                                        u4WorkingME) == SNMP_FAILURE)
        {
            nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, DESTROY);
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set Working ME");
            return;
        }
    }

    u4RetVal = 0;
    nmhGetFsElpsPgCfmWorkingMEP (u4ContextId, u4PgId, &u4RetVal);
    if (u4RetVal != u4WorkingMEP)
    {
        if (nmhTestv2FsElpsPgCfmWorkingMEP (&u4ErrValue, u4ContextId, u4PgId,
                                            u4WorkingMEP) == SNMP_FAILURE)
        {
            nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, DESTROY);
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure Working MEP");
            return;
        }

        if (nmhSetFsElpsPgCfmWorkingMEP (u4ContextId, u4PgId,
                                         u4WorkingMEP) == SNMP_FAILURE)
        {
            nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, DESTROY);
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set Working MEP");
            return;
        }
    }

    u4RetVal = 0;
    nmhGetFsElpsPgCfmProtectionMEG (u4ContextId, u4PgId, &u4RetVal);
    if (u4RetVal != u4ProtectionMEG)
    {
        if (nmhTestv2FsElpsPgCfmProtectionMEG (&u4ErrValue, u4ContextId, u4PgId,
                                               u4ProtectionMEG) == SNMP_FAILURE)
        {
            nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, DESTROY);
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure Protection MEG");
            return;
        }

        if (nmhSetFsElpsPgCfmProtectionMEG (u4ContextId, u4PgId,
                                            u4ProtectionMEG) == SNMP_FAILURE)
        {
            nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, DESTROY);
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set Protection MEG");
            return;
        }
    }

    u4RetVal = 0;
    nmhGetFsElpsPgCfmProtectionME (u4ContextId, u4PgId, &u4RetVal);
    if (u4RetVal != u4ProtectionME)
    {
        if (nmhTestv2FsElpsPgCfmProtectionME (&u4ErrValue, u4ContextId, u4PgId,
                                              u4ProtectionME) == SNMP_FAILURE)
        {
            nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, DESTROY);
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure Protection ME");
            return;
        }

        if (nmhSetFsElpsPgCfmProtectionME (u4ContextId, u4PgId,
                                           u4ProtectionME) == SNMP_FAILURE)
        {
            nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, DESTROY);
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set Protection MEG");
            return;
        }
    }

    u4RetVal = 0;
    nmhGetFsElpsPgCfmProtectionMEP (u4ContextId, u4PgId, &u4RetVal);
    if (u4RetVal != u4ProtectionMEP)
    {
        if (nmhTestv2FsElpsPgCfmProtectionMEP (&u4ErrValue, u4ContextId, u4PgId,
                                               u4ProtectionMEP) == SNMP_FAILURE)
        {
            nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, DESTROY);
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure Protection MEP");
            return;
        }

        if (nmhSetFsElpsPgCfmProtectionMEP (u4ContextId, u4PgId,
                                            u4ProtectionMEP) == SNMP_FAILURE)
        {
            nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, DESTROY);
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set Protection MEP");
            return;
        }
    }
    u4RetVal = 0;
    nmhGetFsElpsPgPscVersion (u4ContextId, u4PgId, &u4RetVal);
    if (u4RetVal != u4PscVer)
    {
        if (nmhTestv2FsElpsPgPscVersion (&u4ErrValue, u4ContextId, u4PgId,
                                         u4PscVer) == SNMP_FAILURE)
        {
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set the version");
            return;
        }

        if (nmhSetFsElpsPgPscVersion (u4ContextId, u4PgId,
                                      u4PscVer) == SNMP_FAILURE)
        {
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set the version");
            return;
        }
    }
    if (nmhTestv2FsElpsPgCfmRowStatus (&u4ErrValue, u4ContextId, u4PgId,
                                       ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, DESTROY);
        ELPS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                      "needed to activate Protection Group are not applied");
        return;
    }
    if (nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId,
                                    ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, DESTROY);
        ELPS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Entry creation failed");
        return;
    }

    ELPS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    IssProcessElpsPgCfmPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssProcessElpsPGServicePage
*  Description   : This function processes the request coming for the 
*                  PG information page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessElpsPGServicePage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessElpsPGServicePageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessElpsPGServicePageSet (pHttp);
    }
    return;
}

/*******************************************************************************
 *  Function Name : IssProcessElpsPGServicePageGet
 *  Description   : This function processes the get request for the Elps
 *                  Protection group configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessElpsPGServicePageGet (tHttp * pHttp)
{
    UINT4               u4ContextId = 0;
    UINT4               u4PgId = 0;
    UINT4               u4NextContextId = 0;
    UINT4               u4NextPgId = 0;
    UINT4               u4NextSListValue = 0;
    UINT4               u4SListValue = 0;
    UINT4               u4Temp = 0;

    WebnmRegisterLock (pHttp, ElpsApiLock, ElpsApiUnLock);
    ELPS_LOCK ();

    pHttp->i4Write = 0;

    /* Display available context ID */
    IssPrintAvailableContextID (pHttp);

    /* Display available PG ID */
    IssPrintAvailablePgId (pHttp);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    if (nmhGetFirstIndexFsElpsPgServiceListTable (&u4ContextId, &u4PgId,
                                                  &u4SListValue)
        == SNMP_FAILURE)
    {
        ELPS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;

    u4ContextId = 0;
    u4PgId = 0;

    while (nmhGetNextIndexFsElpsPgServiceListTable (u4ContextId,
                                                    &u4NextContextId,
                                                    u4PgId, &u4NextPgId,
                                                    u4SListValue,
                                                    &u4NextSListValue)
           == SNMP_SUCCESS)
    {
        pHttp->i4Write = (INT4) u4Temp;

        /* Context ID */
        STRCPY (pHttp->au1KeyString, "CONTEXT_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4NextContextId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Protection Group ID */
        STRCPY (pHttp->au1KeyString, "PROTECTION_GID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4NextPgId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Service List Value */
        STRCPY (pHttp->au1KeyString, "SERVICE_LIST_VALUE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4NextSListValue);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        u4ContextId = u4NextContextId;
        u4PgId = u4NextPgId;
        u4SListValue = u4NextSListValue;
    }
    ELPS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*******************************************************************************
*  Function Name : IssProcessElpsPGServicePageSet
*  Description   : This function processes the HTTP set request coming for the
*                  Elps Protection Group Service configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None.
*******************************************************************************/
VOID
IssProcessElpsPGServicePageSet (tHttp * pHttp)
{
    UINT4               u4ContextId = 0;
    UINT4               u4NextContextId = 0;
    UINT4               u4PgId = 0;
    UINT4               u4NextPgId = 0;
    UINT4               u4ServiceId = 0;
    UINT4               u4NextServiceId = 0;
    UINT4               u4ErrorCode = 0;
    UINT2               u2ByteIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT1              *pu1SrvList = NULL;
    UINT1               u1SrvFlag = 0;
    INT4                i4SystemControl = 0;
    INT4                i4CfgType = 0;
    INT4                i4RowStatus = 0;

    pu1SrvList = UtlShMemAllocVlanList ();

    if (pu1SrvList == NULL)
    {
        return;
    }

    WebnmRegisterLock (pHttp, ElpsApiLock, ElpsApiUnLock);
    ELPS_LOCK ();

    MEMSET (pu1SrvList, 0, ELPS_SRV_LIST_SIZE);
    STRCPY (pHttp->au1Name, "CONTEXT_ID");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        u4ContextId = (UINT4) ATOI (pHttp->au1Value);
    }

    nmhGetFsElpsContextSystemControl (u4ContextId, &i4SystemControl);
    if (i4SystemControl != ELPS_START)
    {
        UtlShMemFreeVlanList (pu1SrvList);
        ELPS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Elps is shutdown. No configuration possible");
        return;
    }

    STRCPY (pHttp->au1Name, "PROTECTION_GID");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        u4PgId = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "SERVICE_LIST_VALUE");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        if (ConvertStrToPortList (pHttp->au1Value, pu1SrvList,
                                  ELPS_SRV_LIST_SIZE,
                                  CFA_L2VLAN) == PORT_FAILURE)
        {
            UtlShMemFreeVlanList (pu1SrvList);
            ELPS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Invalid port list.");
            return;
        }
    }

    nmhGetFsElpsPgConfigType (u4ContextId, u4PgId, &i4CfgType);

    if (i4CfgType != ELPS_PG_TYPE_LIST)
    {
        UtlShMemFreeVlanList (pu1SrvList);
        ELPS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Service List.");
        return;
    }
    else
    {
        nmhGetFsElpsPgConfigRowStatus (u4ContextId, u4PgId, &i4RowStatus);

        if (i4RowStatus == ACTIVE)
        {
            if (nmhTestv2FsElpsPgConfigRowStatus (&u4ErrorCode, u4ContextId,
                                                  u4PgId, NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                UtlShMemFreeVlanList (pu1SrvList);
                ELPS_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                              "needed to activate Protection Group are not applied");
                return;
            }
            if (nmhSetFsElpsPgConfigRowStatus (u4ContextId, u4PgId,
                                               NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                UtlShMemFreeVlanList (pu1SrvList);
                ELPS_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Entry creation failed");
                return;
            }
        }
    }

    if (nmhGetFirstIndexFsElpsPgServiceListTable (&u4NextContextId,
                                                  &u4NextPgId,
                                                  &u4NextServiceId)
        == SNMP_SUCCESS)
    {
        do
        {
            if ((u4ContextId != (UINT4) u4NextContextId) &&
                (u4PgId != u4NextPgId))
            {
                break;
            }

            if (nmhTestv2FsElpsPgServiceListRowStatus
                (&u4ErrorCode, u4ContextId, u4NextPgId, u4NextServiceId,
                 DESTROY) == SNMP_FAILURE)
            {
                UtlShMemFreeVlanList (pu1SrvList);
                ELPS_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Row Status fail.");
                return;
            }

            if (nmhSetFsElpsPgServiceListRowStatus (u4ContextId, u4NextPgId,
                                                    u4NextServiceId,
                                                    DESTROY) == SNMP_FAILURE)
            {
                UtlShMemFreeVlanList (pu1SrvList);
                ELPS_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Setting Row Status fail.");
                return;
            }

            /* u4NextServiceId is deleted now from the service list */

            u4ServiceId = u4NextServiceId;
        }
        while (nmhGetNextIndexFsElpsPgServiceListTable (u4ContextId,
                                                        &u4NextContextId,
                                                        u4PgId, &u4NextPgId,
                                                        u4ServiceId,
                                                        &u4NextServiceId)
               == SNMP_SUCCESS);
    }

    /* Now set the new service list values */
    for (u2ByteIndex = 0; u2ByteIndex < ELPS_SRV_LIST_SIZE; u2ByteIndex++)
    {
        if (pu1SrvList[u2ByteIndex] != 0)
        {
            u1SrvFlag = pu1SrvList[u2ByteIndex];

            for (u2BitIndex = 0;
                 ((u2BitIndex < WEB_PORTS_PER_BYTE) && (u1SrvFlag != 0));
                 u2BitIndex++)
            {
                if ((u1SrvFlag & ISS_WEB_BIT8) != 0)
                {
                    u4ServiceId =
                        (UINT4) ((u2ByteIndex * WEB_PORTS_PER_BYTE) +
                                 u2BitIndex + 1);

                    if (nmhGetFsElpsPgServiceListRowStatus
                        (u4ContextId, u4PgId,
                         u4ServiceId, &i4RowStatus) == SNMP_SUCCESS)
                    {
                        if (i4RowStatus == ACTIVE)
                        {
                            u1SrvFlag = (UINT1) (u1SrvFlag << 1);
                            continue;
                        }
                    }

                    if (nmhTestv2FsElpsPgServiceListRowStatus
                        (&u4ErrorCode, u4ContextId, u4PgId, u4ServiceId,
                         CREATE_AND_GO) == SNMP_FAILURE)
                    {
                        u1SrvFlag = (UINT1) (u1SrvFlag << 1);
                        continue;
                    }

                    if (nmhSetFsElpsPgServiceListRowStatus
                        (u4ContextId, u4PgId, u4ServiceId,
                         CREATE_AND_GO) == SNMP_FAILURE)
                    {
                        UtlShMemFreeVlanList (pu1SrvList);
                        ELPS_UNLOCK ();
                        WebnmUnRegisterLock (pHttp);
                        IssSendError (pHttp, (CONST INT1 *)
                                      "Setting Row Status fail.");
                        return;
                    }
                }
                u1SrvFlag = (UINT1) (u1SrvFlag << 1);
            }
        }
    }

    if (nmhTestv2FsElpsPgConfigRowStatus (&u4ErrorCode, u4ContextId,
                                          u4PgId, ACTIVE) == SNMP_FAILURE)
    {
        UtlShMemFreeVlanList (pu1SrvList);
        ELPS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                      "needed to activate Protection Group are not applied");
        return;
    }
    if (nmhSetFsElpsPgConfigRowStatus (u4ContextId, u4PgId,
                                       ACTIVE) == SNMP_FAILURE)
    {
        UtlShMemFreeVlanList (pu1SrvList);
        ELPS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Entry creation failed");
        return;
    }

    UtlShMemFreeVlanList (pu1SrvList);
    ELPS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    IssProcessElpsPGServicePageGet (pHttp);
}

#endif /* WEBNM_WANTED */
#endif
