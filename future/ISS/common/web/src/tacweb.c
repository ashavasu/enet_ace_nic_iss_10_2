/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: tacweb.c,v 1.13 2013/07/03 12:20:47 siva Exp $
 * 
 *******************************************************************************/

#ifndef _TAC_WEB_C
#define _TAC_WEB_C

#include "webiss.h"
#include "isshttp.h"
#include "cfa.h"
#include "tac.h"
#include "tacweb.h"
#include "webinc.h"
#include"snmputil.h"
/*******************************************************************************
*  Function Name : IssProcessTacProfilePage 
*  Description   : This function processes the request coming for the TAC       
*                  Creation Page.                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssProcessTacProfilePage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessTacProfileGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessTacProfileSet (pHttp);
    }
}

/*******************************************************************************
*  Function Name : IssProcessTacProfileGet 
*  Description   : This function gets and displays all the Profiles created 
*                  enables the user to delete or modify any Profile
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/

VOID
IssProcessTacProfileGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE profileDescription;
    UINT4               u4ProfileId = 0;
    UINT4               u4NextProfileId = 0;
    UINT4               u4PortRefCnt = 0;
    UINT4               u4VlanRefCnt = 0;
    UINT4               u4Temp = 0;
    INT4                i4PrfAction = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    INT4                i4AddrType = 0;
    INT4                i4NextAddrType = 0;
    INT4                i4Status = 0;
    UINT1               au1Description[TACM_MAX_DESCR_ARRAY_LEN];

    pHttp->i4Write = 0;

    /* Display the web page till the Table Flag */
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    /* Register the TAC Lock with Webnm Module and Take the Lock */
    WebnmRegisterLock (pHttp, TacLock, TacUnLock);
    TACM_LOCK ();

    /* Allocate the memory fot octet string and initialise */
    MEMSET (&profileDescription, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1Description, 0, TACM_MAX_DESCR_ARRAY_LEN);
    profileDescription.pu1_OctetList = au1Description;

    i4RetVal = nmhGetFirstIndexFsTacMcastProfileTable
        (&u4NextProfileId, &i4NextAddrType);

    if (i4RetVal == SNMP_FAILURE)
    {
        TACM_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;

    do
    {
        u4ProfileId = u4NextProfileId;
        i4AddrType = i4NextAddrType;

        MEMSET (au1Description, 0, TACM_MAX_DESCR_ARRAY_LEN);
        profileDescription.i4_Length = 0;

        if (nmhGetFsTacMcastProfileDescription (u4ProfileId,
                                                i4AddrType,
                                                &profileDescription)
            == SNMP_FAILURE)
        {
            continue;
        }

        if (nmhGetFsTacMcastProfileAction (u4ProfileId,
                                           i4AddrType,
                                           &i4PrfAction) == SNMP_FAILURE)
        {
            continue;
        }

        if (nmhGetFsTacMcastPrfStatsPortRefCnt (u4ProfileId,
                                                i4AddrType,
                                                &u4PortRefCnt) == SNMP_FAILURE)
        {
            continue;
        }

        if (nmhGetFsTacMcastPrfStatsVlanRefCnt (u4ProfileId,
                                                i4AddrType,
                                                &u4VlanRefCnt) == SNMP_FAILURE)
        {
            continue;
        }

        if (nmhGetFsTacMcastProfileStatus (u4ProfileId,
                                           i4AddrType,
                                           &i4Status) == SNMP_FAILURE)
        {
            continue;
        }

        pHttp->i4Write = (INT4) u4Temp;

        STRCPY (pHttp->au1KeyString, "Profile_Id");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4ProfileId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "Internet_Address_Type");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "IPv4");
        }
        else if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "IPv6");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "Profile_Description");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                 profileDescription.pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        profileDescription.i4_Length);
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "Profile_Action");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4PrfAction);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "Port_Ref_Count");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4PortRefCnt);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "Vlan_Ref_Count");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4VlanRefCnt);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "Profile_Status");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4Status == ISS_ACTIVE)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "1");
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "2");
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
        }
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    }
    while (nmhGetNextIndexFsTacMcastProfileTable
           (u4ProfileId, &u4NextProfileId, i4AddrType, &i4NextAddrType)
           == SNMP_SUCCESS);

    /* All Entries Displayed. So, Release the Lock. Unregister the Lock from
     * Webnm Module. */
    TACM_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    /* Display all the information in the html page after the Table stop flag */
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/******************************************************************************
*  Function Name : IssProcessTacProfileSet 
*  Description   : This function gets the user input from the web page and 
*                  creates or modifies a Profile.                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
******************************************************************************/

VOID
IssProcessTacProfileSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE profileDescription;
    UINT4               u4ProfileId = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4PrfAction = 0;
    INT4                i4PrfStatus = 0;
    INT4                i4ProfileStatus = 0;
    INT4                i4AddrType = 0;
    UINT1               au1Description[TACM_MAX_DESCR_ARRAY_LEN];

    MEMSET (&profileDescription, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1Description, 0, TACM_MAX_DESCR_ARRAY_LEN);
    profileDescription.pu1_OctetList = au1Description;

    /* Get the Profile Id */
    STRCPY (pHttp->au1Name, "PROFILE_ID");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4ProfileId = (UINT4) ATOI (pHttp->au1Value);
    }

    /* Get the Address type */
    STRCPY (pHttp->au1Name, "INET_ADDRESS_TYPE");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4AddrType = (INT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "PROFILE_STATUS");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4PrfStatus = ATOI (pHttp->au1Value);

        if ((i4PrfStatus == 1) || (i4PrfStatus == 2))
        {

            /* Get the profile description */
            STRCPY (pHttp->au1Name, "PROFILE_DESCRIPTION");
            if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery) == ENM_SUCCESS)
            {
                issDecodeSpecialChar (pHttp->au1Value);
                MEMSET (au1Description, 0, TACM_MAX_DESCR_ARRAY_LEN);
                STRNCPY (au1Description, pHttp->au1Value,
                         (TACM_MAX_DESCR_ARRAY_LEN - 1));
                profileDescription.i4_Length = (INT4) STRLEN (au1Description);
            }

            /* Get the profile action */
            STRCPY (pHttp->au1Name, "PROFILE_ACTION");
            if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery) == ENM_SUCCESS)
            {
                i4PrfAction = ATOI (pHttp->au1Value);
            }

            /* Not In Service Case
             * Make the Table Not In Service, Set the values.
             */
            if (i4PrfStatus != 2)
            {
                if ((nmhGetFsTacMcastProfileStatus (u4ProfileId, i4AddrType,
                                                    &i4ProfileStatus) ==
                     SNMP_SUCCESS) && (i4ProfileStatus == ACTIVE))
                {
                    IssSendError (pHttp, (CONST INT1 *)
                                  "Profile Entry is Active !!!");
                    return;
                }
            }

            TACM_LOCK ();

            if (nmhTestv2FsTacMcastProfileStatus (&u4ErrorCode, u4ProfileId,
                                                  i4AddrType,
                                                  ISS_NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "Profile is mapped to some port or vlan "
                              "in other modules");
                TACM_UNLOCK ();
                return;
            }

            if (nmhSetFsTacMcastProfileStatus (u4ProfileId, i4AddrType,
                                               ISS_NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to change the row status");
                TACM_UNLOCK ();
                return;
            }

            if (nmhTestv2FsTacMcastProfileDescription
                (&u4ErrorCode, u4ProfileId, i4AddrType, &profileDescription)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "Profile description validation failed");
                TACM_UNLOCK ();
                return;
            }

            if (nmhTestv2FsTacMcastProfileAction (&u4ErrorCode, u4ProfileId,
                                                  i4AddrType, i4PrfAction)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "Pofile action validation failed");
                TACM_UNLOCK ();
                return;
            }

            /* Set the profile description */
            if (nmhSetFsTacMcastProfileDescription
                (u4ProfileId, i4AddrType, &profileDescription) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to change the profile description");
                TACM_UNLOCK ();
                return;
            }

            /* Set the profile action */
            if (nmhSetFsTacMcastProfileAction (u4ProfileId, i4AddrType,
                                               i4PrfAction) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to change the profile action");
                TACM_UNLOCK ();
                return;
            }

            if (i4PrfStatus == 1)
            {
                if (nmhTestv2FsTacMcastProfileStatus (&u4ErrorCode,
                                                      u4ProfileId, i4AddrType,
                                                      ISS_ACTIVE)
                    == SNMP_FAILURE)
                {
                    IssSendError (pHttp, (CONST INT1 *)
                                  "Profile is mapped to some port or vlan "
                                  "in other modules");
                    TACM_UNLOCK ();
                    return;
                }

                if (nmhSetFsTacMcastProfileStatus (u4ProfileId, i4AddrType,
                                                   ISS_ACTIVE) == SNMP_FAILURE)
                {
                    IssSendError (pHttp, (CONST INT1 *)
                                  "Unable to change the row status");
                    TACM_UNLOCK ();
                    return;
                }
            }

            TACM_UNLOCK ();
        }
        else if (i4PrfStatus == 6)
        {
            /* Destroy Case.
             * Destroy the Table. */

            TACM_LOCK ();

            if (nmhTestv2FsTacMcastProfileStatus (&u4ErrorCode, u4ProfileId,
                                                  i4AddrType,
                                                  ISS_DESTROY) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "Profile is mapped to some port or vlan "
                              "in other modules");
                TACM_UNLOCK ();
                return;
            }

            if (nmhSetFsTacMcastProfileStatus (u4ProfileId, i4AddrType,
                                               ISS_DESTROY) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to destroy the profile");
                TACM_UNLOCK ();
                return;
            }

            TACM_UNLOCK ();
        }
        else
        {
            /*  Creation Case.
             *  Create the Table.
             */

            TACM_LOCK ();

            if (nmhTestv2FsTacMcastProfileStatus (&u4ErrorCode, u4ProfileId,
                                                  i4AddrType,
                                                  ISS_CREATE_AND_WAIT)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "Invalid profile Id and address type");
                TACM_UNLOCK ();
                return;
            }

            if (nmhSetFsTacMcastProfileStatus (u4ProfileId, i4AddrType,
                                               ISS_CREATE_AND_WAIT)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to create the profile");
                TACM_UNLOCK ();
                return;
            }

            TACM_UNLOCK ();
        }
    }

    /* Call IssProcessTacProfileGet to refresh the web page with
     * newly added/deleted/modified entry. */
    IssProcessTacProfileGet (pHttp);
}

/*******************************************************************************
*  Function Name : IssProcessTacPrfFilterPage
*  Description   : This function processes the request coming for the Filter 
*                  Creation Page.                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssProcessTacPrfFilterPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessTacPrfFilterGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessTacPrfFilterSet (pHttp);
    }
}

/*******************************************************************************
*  Function Name : IssProcessTacPrfFilterGet
*  Description   : This function gets and displays all the filters created 
*                  and enables the user to delete or modify any Filter
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/

VOID
IssProcessTacPrfFilterGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE *pGrpStartAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pGrpEndAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pSrcStartAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pSrcEndAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pGrpNextStartAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pGrpNextEndAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pSrcNextStartAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pSrcNextEndAddr = NULL;
    UINT4               u4ProfileId = 0;
    UINT4               u4NextProfileId = 0;
    UINT4               u4Temp = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    INT4                i4AddrType = 0;
    INT4                i4NextAddrType = 0;
    INT4                i4FilterMode = 0;
    UINT1               u1AddrLen = 0;

    pHttp->i4Write = 0;

    /* Register the TAC Lock with Webnm Module and Take the Lock */
    WebnmRegisterLock (pHttp, TacLock, TacUnLock);
    TACM_LOCK ();

    /* Allocate the memory fot octet string and initialise */
    pGrpStartAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);
    if (pGrpStartAddr == NULL)
    {
        TACM_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    pGrpEndAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);
    if (pGrpEndAddr == NULL)
    {
        free_octetstring (pGrpStartAddr);

        TACM_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    pSrcStartAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);
    if (pSrcStartAddr == NULL)
    {
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpEndAddr);

        TACM_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    pSrcEndAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);
    if (pSrcEndAddr == NULL)
    {
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpEndAddr);
        free_octetstring (pSrcStartAddr);

        TACM_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    pGrpNextStartAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);
    if (pGrpNextStartAddr == NULL)
    {
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpEndAddr);
        free_octetstring (pSrcStartAddr);
        free_octetstring (pSrcEndAddr);

        TACM_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    pGrpNextEndAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);
    if (pGrpNextEndAddr == NULL)
    {
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpNextStartAddr);
        free_octetstring (pGrpEndAddr);
        free_octetstring (pSrcStartAddr);
        free_octetstring (pSrcEndAddr);

        TACM_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    pSrcNextStartAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);
    if (pSrcNextStartAddr == NULL)
    {
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpNextStartAddr);
        free_octetstring (pGrpEndAddr);
        free_octetstring (pGrpNextEndAddr);
        free_octetstring (pSrcStartAddr);
        free_octetstring (pSrcEndAddr);

        TACM_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    pSrcNextEndAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);
    if (pSrcNextEndAddr == NULL)
    {
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpNextStartAddr);
        free_octetstring (pGrpEndAddr);
        free_octetstring (pGrpNextEndAddr);
        free_octetstring (pSrcStartAddr);
        free_octetstring (pSrcNextStartAddr);
        free_octetstring (pSrcEndAddr);

        TACM_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    MEMSET (pGrpStartAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    pGrpStartAddr->i4_Length = 0;

    MEMSET (pGrpEndAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    pGrpEndAddr->i4_Length = 0;

    MEMSET (pSrcStartAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    pSrcStartAddr->i4_Length = 0;

    MEMSET (pSrcEndAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    pSrcEndAddr->i4_Length = 0;

    MEMSET (pGrpNextStartAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    pGrpNextStartAddr->i4_Length = 0;

    MEMSET (pGrpNextEndAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    pGrpNextEndAddr->i4_Length = 0;

    MEMSET (pSrcNextStartAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    pSrcNextStartAddr->i4_Length = 0;

    MEMSET (pSrcNextEndAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    pSrcNextEndAddr->i4_Length = 0;

    /* Provide the created profile Id in the page */
    u4ProfileId = 0;
    i4AddrType = 0;

    STRCPY (pHttp->au1KeyString, "Filter_Profile_Options");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "<option value = \"0\" >%s\n", "-");
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    while (nmhGetNextIndexFsTacMcastProfileTable
           (u4ProfileId, &u4NextProfileId, i4AddrType, &i4NextAddrType)
           == SNMP_SUCCESS)
    {
        u4ProfileId = u4NextProfileId;
        i4AddrType = i4NextAddrType;

        SPRINTF ((CHR1 *) pHttp->au1DataString,
                 "<option value = \"%u\">%u\n", u4ProfileId, u4ProfileId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

    }

    /* Display the web page till the Table Flag */
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    i4RetVal = nmhGetFirstIndexFsTacMcastPrfFilterTable
        (&u4NextProfileId, &i4NextAddrType, pGrpNextStartAddr,
         pGrpNextEndAddr, pSrcNextStartAddr, pSrcNextEndAddr);

    if (i4RetVal == SNMP_FAILURE)
    {
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpNextStartAddr);
        free_octetstring (pGrpEndAddr);
        free_octetstring (pGrpNextEndAddr);
        free_octetstring (pSrcStartAddr);
        free_octetstring (pSrcNextStartAddr);
        free_octetstring (pSrcEndAddr);
        free_octetstring (pSrcNextEndAddr);

        TACM_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;

    do
    {
        u4ProfileId = u4NextProfileId;
        i4AddrType = i4NextAddrType;

        MEMSET (pGrpStartAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
        MEMSET (pGrpEndAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
        MEMSET (pSrcStartAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
        MEMSET (pSrcEndAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);

        if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            u1AddrLen = (UINT1) IPVX_IPV4_ADDR_LEN;
        }

        MEMCPY (pGrpStartAddr->pu1_OctetList,
                pGrpNextStartAddr->pu1_OctetList, u1AddrLen);
        MEMCPY (pGrpEndAddr->pu1_OctetList,
                pGrpNextEndAddr->pu1_OctetList, u1AddrLen);
        MEMCPY (pSrcStartAddr->pu1_OctetList,
                pSrcNextStartAddr->pu1_OctetList, u1AddrLen);
        MEMCPY (pSrcEndAddr->pu1_OctetList,
                pSrcNextEndAddr->pu1_OctetList, u1AddrLen);

        pGrpStartAddr->i4_Length = pGrpNextStartAddr->i4_Length;
        pGrpEndAddr->i4_Length = pGrpNextEndAddr->i4_Length;
        pSrcStartAddr->i4_Length = pSrcNextStartAddr->i4_Length;
        pSrcEndAddr->i4_Length = pSrcNextStartAddr->i4_Length;

        if (nmhGetFsTacMcastPrfFilterMode
            (u4ProfileId, i4AddrType, pGrpStartAddr,
             pGrpEndAddr, pSrcStartAddr, pSrcEndAddr, &i4FilterMode)
            == SNMP_FAILURE)
        {
            continue;
        }

        pHttp->i4Write = (INT4) u4Temp;

        STRCPY (pHttp->au1KeyString, "Filter_Profile_Id");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4ProfileId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "Filter_Internet_Address_Type");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "IPv4");
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "IPv6");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "Group_Start_Address");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d.%d.%d.%d",
                 pGrpStartAddr->pu1_OctetList[0],
                 pGrpStartAddr->pu1_OctetList[1],
                 pGrpStartAddr->pu1_OctetList[2],
                 pGrpStartAddr->pu1_OctetList[3]);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "Group_End_Address");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d.%d.%d.%d",
                 pGrpEndAddr->pu1_OctetList[0],
                 pGrpEndAddr->pu1_OctetList[1],
                 pGrpEndAddr->pu1_OctetList[2], pGrpEndAddr->pu1_OctetList[3]);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "Source_Start_Address");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d.%d.%d.%d",
                 pSrcStartAddr->pu1_OctetList[0],
                 pSrcStartAddr->pu1_OctetList[1],
                 pSrcStartAddr->pu1_OctetList[2],
                 pSrcStartAddr->pu1_OctetList[3]);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "Source_End_Address");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d.%d.%d.%d",
                 pSrcEndAddr->pu1_OctetList[0],
                 pSrcEndAddr->pu1_OctetList[1],
                 pSrcEndAddr->pu1_OctetList[2], pSrcEndAddr->pu1_OctetList[3]);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "Filter_Mode");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4FilterMode);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    }
    while (nmhGetNextIndexFsTacMcastPrfFilterTable
           (u4ProfileId, &u4NextProfileId, i4AddrType, &i4NextAddrType,
            pGrpStartAddr, pGrpNextStartAddr, pGrpEndAddr, pGrpNextEndAddr,
            pSrcStartAddr, pSrcNextStartAddr, pSrcEndAddr, pSrcNextEndAddr)
           == SNMP_SUCCESS);

    free_octetstring (pGrpStartAddr);
    free_octetstring (pGrpNextStartAddr);
    free_octetstring (pGrpEndAddr);
    free_octetstring (pGrpNextEndAddr);
    free_octetstring (pSrcStartAddr);
    free_octetstring (pSrcNextStartAddr);
    free_octetstring (pSrcEndAddr);
    free_octetstring (pSrcNextEndAddr);

    /* All Entries Displayed. So, Release the Lock. Unregister the Lock from
     * Webnm Module. */
    TACM_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    /* Display all the information in the html page after the Table stop flag */
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/******************************************************************************
*  Function Name : IssProcessTacPrfFilterSet
*  Description   : This function gets the user input from the web page and 
*                  creates or modifies a filter rule.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
******************************************************************************/

VOID
IssProcessTacPrfFilterSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE *pGrpStartAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pGrpEndAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pSrcStartAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pSrcEndAddr = NULL;
    tUtlIn6Addr         utlIn6Addr;
    UINT4               u4ProfileId = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4IpAddr = 0;
    INT4                i4AddrType = 0;
    INT4                i4FilterMode = 0;
    INT4                i4Status = 0;
    UINT1               u1AddrLen = 0;

    /* Get the Profile Id */
    STRCPY (pHttp->au1Name, "FILTER_PROFILE_ID");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4ProfileId = (UINT4) ATOI (pHttp->au1Value);
    }

    /* Get the Address type */
    STRCPY (pHttp->au1Name, "FILTER_INET_ADDRESS_TYPE");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4AddrType = (INT4) ATOI (pHttp->au1Value);
    }

    /* Do not modify the filter if the porfile is active */
    if (nmhGetFsTacMcastProfileStatus (u4ProfileId, i4AddrType,
                                       &i4Status) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Unable to get the profile status");
        TACM_UNLOCK ();
        return;
    }

    if (i4Status != ISS_NOT_IN_SERVICE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Profile is active");
        TACM_UNLOCK ();
        return;
    }

    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        u1AddrLen = IPVX_IPV4_ADDR_LEN;
    }
    else
    {
        u1AddrLen = IPVX_IPV6_ADDR_LEN;
    }

    /* Allocate the memory fot octet string and initialise */

    pGrpStartAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);

    if (pGrpStartAddr == NULL)
    {
        return;
    }

    pGrpEndAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);

    if (pGrpEndAddr == NULL)
    {
        free_octetstring (pGrpStartAddr);
        return;
    }

    pSrcStartAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);

    if (pSrcStartAddr == NULL)
    {
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpEndAddr);
        return;
    }

    pSrcEndAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);

    if (pSrcEndAddr == NULL)
    {
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpEndAddr);
        free_octetstring (pSrcStartAddr);
        return;
    }
    MEMSET (pGrpStartAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    pGrpStartAddr->i4_Length = 0;

    MEMSET (pGrpEndAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    pGrpEndAddr->i4_Length = 0;

    MEMSET (pSrcStartAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    pSrcStartAddr->i4_Length = 0;

    MEMSET (pSrcEndAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    pSrcEndAddr->i4_Length = 0;

    /* Get the Group start address */
    STRCPY (pHttp->au1Name, "GRP_START_ADDR");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery) == ENM_SUCCESS)
        && (ATOI (pHttp->au1Value) != 0))
    {
        if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            u4IpAddr = INET_ADDR ((CHR1 *) pHttp->au1Value);
            MEMCPY (pGrpStartAddr->pu1_OctetList, &u4IpAddr,
                    IPVX_IPV4_ADDR_LEN);
        }
        else
        {
            MEMSET (&utlIn6Addr, 0, sizeof (tUtlIn6Addr));
            INET_ATON6 (pHttp->au1Value, &utlIn6Addr);
            MEMCPY (pGrpStartAddr->pu1_OctetList, utlIn6Addr.u1addr, u1AddrLen);
        }
        pGrpStartAddr->i4_Length = u1AddrLen;
    }

    /* Get the Group end address */
    STRCPY (pHttp->au1Name, "GRP_END_ADDR");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery) == ENM_SUCCESS)
        && (ATOI (pHttp->au1Value) != 0))
    {
        if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            u4IpAddr = INET_ADDR ((CHR1 *) pHttp->au1Value);
            MEMCPY (pGrpEndAddr->pu1_OctetList, &u4IpAddr, IPVX_IPV4_ADDR_LEN);
        }
        else
        {
            MEMSET (&utlIn6Addr, 0, sizeof (tUtlIn6Addr));
            INET_ATON6 (pHttp->au1Value, &utlIn6Addr);
            MEMCPY (pGrpEndAddr->pu1_OctetList, utlIn6Addr.u1addr, u1AddrLen);
        }
        pGrpEndAddr->i4_Length = u1AddrLen;
    }
    else
    {
        MEMCPY (pGrpEndAddr->pu1_OctetList, pGrpStartAddr->pu1_OctetList,
                u1AddrLen);
        pGrpEndAddr->i4_Length = u1AddrLen;

    }

    /* Get the Source start address */
    STRCPY (pHttp->au1Name, "SRC_START_ADDR");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery) == ENM_SUCCESS)
        && (ATOI (pHttp->au1Value) != 0))
    {

        if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            u4IpAddr = INET_ADDR ((CHR1 *) pHttp->au1Value);
            MEMCPY (pSrcStartAddr->pu1_OctetList, &u4IpAddr,
                    IPVX_IPV4_ADDR_LEN);
        }
        else
        {
            MEMSET (&utlIn6Addr, 0, sizeof (tUtlIn6Addr));
            INET_ATON6 (pHttp->au1Value, &utlIn6Addr);
            MEMCPY (pSrcStartAddr->pu1_OctetList, utlIn6Addr.u1addr, u1AddrLen);
        }
        pSrcStartAddr->i4_Length = u1AddrLen;
    }

    /* Get the Source end address */
    STRCPY (pHttp->au1Name, "SRC_END_ADDR");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery) == ENM_SUCCESS)
        && (ATOI (pHttp->au1Value) != 0))
    {
        if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            u4IpAddr = INET_ADDR ((CHR1 *) pHttp->au1Value);
            MEMCPY (pSrcEndAddr->pu1_OctetList, &u4IpAddr, IPVX_IPV4_ADDR_LEN);
        }
        else
        {
            MEMSET (&utlIn6Addr, 0, sizeof (tUtlIn6Addr));
            INET_ATON6 (pHttp->au1Value, &utlIn6Addr);
            MEMCPY (pSrcEndAddr->pu1_OctetList, utlIn6Addr.u1addr, u1AddrLen);
        }
        pSrcEndAddr->i4_Length = u1AddrLen;
    }

    STRCPY (pHttp->au1Name, "FILTER_STATUS");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        if (STRCMP (pHttp->au1Value, "2") == 0)
        {
            /* Not In Service Case
             * Make the Table Not In Service, Set the values.
             */

            /* Get the Filter mode */
            STRCPY (pHttp->au1Name, "FILTER_MODE");
            if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery) == ENM_SUCCESS)
            {
                i4FilterMode = ATOI (pHttp->au1Value);
            }

            TACM_LOCK ();

            if (nmhTestv2FsTacMcastPrfFilterStatus
                (&u4ErrorCode, u4ProfileId, i4AddrType, pGrpStartAddr,
                 pGrpEndAddr, pSrcStartAddr, pSrcEndAddr, ISS_NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                free_octetstring (pGrpStartAddr);
                free_octetstring (pGrpEndAddr);
                free_octetstring (pSrcStartAddr);
                free_octetstring (pSrcEndAddr);

                IssSendError (pHttp, (CONST INT1 *) "Filter validation failed");
                TACM_UNLOCK ();
                return;
            }

            if (nmhSetFsTacMcastPrfFilterStatus
                (u4ProfileId, i4AddrType, pGrpStartAddr,
                 pGrpEndAddr, pSrcStartAddr, pSrcEndAddr, ISS_NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                free_octetstring (pGrpStartAddr);
                free_octetstring (pGrpEndAddr);
                free_octetstring (pSrcStartAddr);
                free_octetstring (pSrcEndAddr);

                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to change the filter status");
                TACM_UNLOCK ();
                return;
            }

            /* Set the filter mode */
            if (nmhTestv2FsTacMcastPrfFilterMode
                (&u4ErrorCode, u4ProfileId, i4AddrType, pGrpStartAddr,
                 pGrpEndAddr, pSrcStartAddr, pSrcEndAddr, i4FilterMode)
                == SNMP_FAILURE)
            {
                free_octetstring (pGrpStartAddr);
                free_octetstring (pGrpEndAddr);
                free_octetstring (pSrcStartAddr);
                free_octetstring (pSrcEndAddr);

                IssSendError (pHttp, (CONST INT1 *)
                              "Filter mode validation failed");
                TACM_UNLOCK ();
                return;
            }

            if (nmhSetFsTacMcastPrfFilterMode
                (u4ProfileId, i4AddrType, pGrpStartAddr,
                 pGrpEndAddr, pSrcStartAddr, pSrcEndAddr, i4FilterMode)
                == SNMP_FAILURE)
            {
                free_octetstring (pGrpStartAddr);
                free_octetstring (pGrpEndAddr);
                free_octetstring (pSrcStartAddr);
                free_octetstring (pSrcEndAddr);

                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to change the filter mode");
                TACM_UNLOCK ();
                return;
            }

            if (nmhTestv2FsTacMcastPrfFilterStatus
                (&u4ErrorCode, u4ProfileId, i4AddrType, pGrpStartAddr,
                 pGrpEndAddr, pSrcStartAddr, pSrcEndAddr, ISS_ACTIVE)
                == SNMP_FAILURE)
            {
                free_octetstring (pGrpStartAddr);
                free_octetstring (pGrpEndAddr);
                free_octetstring (pSrcStartAddr);
                free_octetstring (pSrcEndAddr);

                IssSendError (pHttp, (CONST INT1 *)
                              "Filter status validation failed");
                TACM_UNLOCK ();
                return;
            }

            if (nmhSetFsTacMcastPrfFilterStatus
                (u4ProfileId, i4AddrType, pGrpStartAddr,
                 pGrpEndAddr, pSrcStartAddr, pSrcEndAddr, ISS_ACTIVE)
                == SNMP_FAILURE)
            {
                free_octetstring (pGrpStartAddr);
                free_octetstring (pGrpEndAddr);
                free_octetstring (pSrcStartAddr);
                free_octetstring (pSrcEndAddr);

                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to activate the filter rule");
                TACM_UNLOCK ();
                return;
            }

            TACM_UNLOCK ();
        }
        else if (STRCMP (pHttp->au1Value, "6") == 0)
        {
            /* Destroy Case.
             * Destroy the Table. */

            TACM_LOCK ();

            if (nmhTestv2FsTacMcastPrfFilterStatus
                (&u4ErrorCode, u4ProfileId, i4AddrType, pGrpStartAddr,
                 pGrpEndAddr, pSrcStartAddr, pSrcEndAddr, ISS_DESTROY)
                == SNMP_FAILURE)
            {
                free_octetstring (pGrpStartAddr);
                free_octetstring (pGrpEndAddr);
                free_octetstring (pSrcStartAddr);
                free_octetstring (pSrcEndAddr);

                IssSendError (pHttp, (CONST INT1 *)
                              "Filter status validation failed");
                TACM_UNLOCK ();
                return;
            }

            if (nmhSetFsTacMcastPrfFilterStatus
                (u4ProfileId, i4AddrType, pGrpStartAddr,
                 pGrpEndAddr, pSrcStartAddr, pSrcEndAddr, ISS_DESTROY)
                == SNMP_FAILURE)
            {
                free_octetstring (pGrpStartAddr);
                free_octetstring (pGrpEndAddr);
                free_octetstring (pSrcStartAddr);
                free_octetstring (pSrcEndAddr);

                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to destroy the filter rule");
                TACM_UNLOCK ();
                return;
            }

            TACM_UNLOCK ();
        }
        else
        {
            /*  Creation Case.
             *  Create the Table.
             */

            TACM_LOCK ();

            if (nmhTestv2FsTacMcastPrfFilterStatus
                (&u4ErrorCode, u4ProfileId, i4AddrType, pGrpStartAddr,
                 pGrpEndAddr, pSrcStartAddr, pSrcEndAddr, ISS_CREATE_AND_GO)
                == SNMP_FAILURE)
            {
                free_octetstring (pGrpStartAddr);
                free_octetstring (pGrpEndAddr);
                free_octetstring (pSrcStartAddr);
                free_octetstring (pSrcEndAddr);

                IssSendError (pHttp, (CONST INT1 *)
                              "Filter status validation failed");
                TACM_UNLOCK ();
                return;
            }

            if (nmhSetFsTacMcastPrfFilterStatus
                (u4ProfileId, i4AddrType, pGrpStartAddr,
                 pGrpEndAddr, pSrcStartAddr, pSrcEndAddr, ISS_CREATE_AND_GO)
                == SNMP_FAILURE)
            {
                free_octetstring (pGrpStartAddr);
                free_octetstring (pGrpEndAddr);
                free_octetstring (pSrcStartAddr);
                free_octetstring (pSrcEndAddr);

                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to create a filter rule");
                TACM_UNLOCK ();
                return;
            }

            TACM_UNLOCK ();
        }
    }

    free_octetstring (pGrpStartAddr);
    free_octetstring (pGrpEndAddr);
    free_octetstring (pSrcStartAddr);
    free_octetstring (pSrcEndAddr);

    /* Call IssProcessTacPrfFilterGet to refresh the web page with
     * newly added/deleted/modified entry. */
    IssProcessTacPrfFilterGet (pHttp);
}

#endif
