/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pvlanweb.c,v 1.9.40.1 2018/03/15 12:59:01 siva Exp $
 *
 * Description:  This file contains web access routines  for   
 *               processing the web page requests for Private  
 *               VLAN. 
 *
 ********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/*****************************************************************************/
/*    FILE  NAME            : pvlanweb.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : PVLAN                                          */
/*    MODULE NAME           : ISS                                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Linux                                          */
/*    DATE OF FIRST RELEASE : 16 July 2010                                   */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains web access routines  for    */
/*                            processing the web page requests for Private   */
/*                            VLAN.                                          */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    16 July 2010           Initial Creation                        */
/*---------------------------------------------------------------------------*/

#ifndef  PVLANWEB_C
#define  PVLANWEB_C
#include "webiss.h"
#include "vcm.h"
#include "isshttp.h"

/*****************************************************************************/
/*  Function Name   : IssProcessPortIsolationPage                            */
/*  Description     : This function does the following                       */
/*                    Process the request for Port Isolation Page.           */
/*  Input(s)        : pHttp - pointer to http connection information.        */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
VOID
IssProcessPortIsolationPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessPortIsolationPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessPortIsolationPageSet (pHttp);
        IssProcessPortIsolationPageGet (pHttp);
    }
    return;
}

/*****************************************************************************/
/*  Function Name   : IssProcessPortIsolationPageGet                         */
/*  Description     : This function does the following                       */
/*                    Process the GET request for Port Isolation Page.       */
/*  Input(s)        : pHttp - pointer to http connection information.        */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
VOID
IssProcessPortIsolationPageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE IfEgressPorts;
    tSNMP_OCTET_STRING_TYPE IfIngressPorts;
    tSNMP_MULTI_DATA_TYPE *pu1DataString = NULL;
    tPortList          *pInPortList = NULL;
    tPortList          *pOutPortList = NULL;
    INT4                i4Temp = 0;
    INT4                i4IngressIndex = 0;
    INT4                i4StorageType = 0;
    INT4                i4NextIngressIndex = 0;
    INT4                i4VlanId = 0;
    INT4                i4NextVlanId = 0;
    INT4                i4EgressPort = 0;
    INT4                i4NextEgressPort = 0;
    INT1                i1RetVal = 0;

    MEMSET (&IfEgressPorts, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&IfIngressPorts, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    IssPrintAvailableL2Ports (pHttp);
    if ((pu1DataString = WebnmAllocMultiData ()) == NULL)
    {
        IssSendError (pHttp, (INT1 *) "Insufficient Memory ");
        return;
    }

    pInPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));
    if (pInPortList == NULL)
    {
        IssSendError (pHttp, (INT1 *) "Memory allocation failed. \r\n");
        return;
    }

    pOutPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));
    if (pOutPortList == NULL)
    {
        FsUtilReleaseBitList ((UINT1 *) pInPortList);
        IssSendError (pHttp, (INT1 *) "Memory allocation failed. \r\n");
        return;
    }

    MEMSET (*pInPortList, 0, sizeof (tPortList));
    MEMSET (*pOutPortList, 0, sizeof (tPortList));

    IfIngressPorts.pu1_OctetList = *pInPortList;
    IfEgressPorts.pu1_OctetList = *pOutPortList;
    IfEgressPorts.i4_Length = sizeof (tPortList);
    IfIngressPorts.i4_Length = sizeof (tPortList);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    WebnmRegisterLock (pHttp, IssLock, IssUnLock);
    ISS_LOCK ();

    if (nmhGetFirstIndexIssPortIsolationTable (&i4IngressIndex,
                                               &i4VlanId,
                                               &i4EgressPort) == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        FsUtilReleaseBitList ((UINT1 *) pInPortList);
        FsUtilReleaseBitList ((UINT1 *) pOutPortList);
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    i4Temp = pHttp->i4Write;

    while (1)
    {

        i1RetVal = nmhGetNextIndexIssPortIsolationTable (i4IngressIndex,
                                                         &i4NextIngressIndex,
                                                         i4VlanId,
                                                         &i4NextVlanId,
                                                         i4EgressPort,
                                                         &i4NextEgressPort);

        if ((i4IngressIndex != i4NextIngressIndex) ||
            (i4VlanId != i4NextVlanId))
        {
            pHttp->i4Write = i4Temp;

            IssPrintAvailableL2Ports (pHttp);

            STRCPY (pHttp->au1KeyString, "INGRESS_PORT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4IngressIndex);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "VLAN_ID_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (0 == i4VlanId)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
            }
            else
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4VlanId);
            }

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetIssPortIsolationStorageType (i4IngressIndex,
                                               i4VlanId,
                                               i4EgressPort, &i4StorageType);
            STRCPY (pHttp->au1KeyString, "STORAGE_TYPE_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4StorageType);

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            ISS_SET_MEMBER_PORT ((*pOutPortList), i4EgressPort);
            pu1DataString->pOctetStrValue->i4_Length = VLAN_PORT_LIST_SIZE;
            MEMCPY (pu1DataString->pOctetStrValue->pu1_OctetList,
                    *pOutPortList, pu1DataString->pOctetStrValue->i4_Length);
            WebnmSendToSocket (pHttp, "EGRESS_PORT_KEY", pu1DataString,
                               ENM_PORT_LIST);
            MEMSET (pu1DataString->pOctetStrValue->pu1_OctetList, 0,
                    pu1DataString->pOctetStrValue->i4_Length);

            MEMSET (*pOutPortList, 0, sizeof (tPortList));
            MEMSET (*pInPortList, 0, sizeof (tPortList));
        }
        else
        {
            ISS_SET_MEMBER_PORT ((*pOutPortList), i4EgressPort);
        }

        if (SNMP_FAILURE == i1RetVal)
        {
            break;
        }

        i4IngressIndex = i4NextIngressIndex;
        i4VlanId = i4NextVlanId;
        i4EgressPort = i4NextEgressPort;

        i4NextIngressIndex = i4NextVlanId = i4NextEgressPort = 0;

    }                            /* End of while */

    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    FsUtilReleaseBitList ((UINT1 *) pInPortList);
    FsUtilReleaseBitList ((UINT1 *) pOutPortList);
    ISS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    return;
}

/*****************************************************************************/
/*  Function Name   : IssProcessPortIsolationPageSet                         */
/*  Description     : This function does the following                       */
/*                    Process the SET request for Port Isolation Page.       */
/*  Input(s)        : pHttp - pointer to http connection information.        */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
VOID
IssProcessPortIsolationPageSet (tHttp * pHttp)
{
    tSNMP_MULTI_DATA_TYPE *pu1MultiData = NULL;
    tPortList          *pPortList = NULL;
    UINT4               au4EgressPorts[ISS_MAX_UPLINK_PORTS + 1];
    UINT4               u4ErrorCode = 0;
    UINT4               u4PortScan = 0;
    INT4                i4IngressPort = 0;
    INT4                i4VlanId = 0;

    MEMSET (au4EgressPorts, 0, ISS_MAX_UPLINK_PORTS);

    WebnmRegisterLock (pHttp, IssLock, IssUnLock);
    ISS_LOCK ();

    STRCPY (pHttp->au1Name, "IngressPort");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4IngressPort = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "VlanId");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4VlanId = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "EgressList");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if ((pu1MultiData = WebnmReadFromSocket (pHttp, "EgressList",
                                             ENM_DISPLAYSTRING)) != NULL)
    {
        issDecodeSpecialChar (pu1MultiData->pOctetStrValue->pu1_OctetList);
        pu1MultiData->pOctetStrValue->pu1_OctetList
            [pu1MultiData->pOctetStrValue->i4_Length] = '\0';
        pPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

        if (pPortList == NULL)
        {
            ISS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            return;
        }
        MEMSET (pPortList, 0, sizeof (tPortList));

        /* Convert the Port Information from OctetList to  PortList format */
        if (ENM_FAILURE ==
            IssPortStringParser (pu1MultiData->pOctetStrValue->pu1_OctetList,
                                 *pPortList))
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Member port List,"
                          "Enter valid ports i.e gi1/1-12,Gi1/1,Ex1/1,Fa1/1");
            ISS_UNLOCK ();
            FsUtilReleaseBitList ((UINT1 *) pPortList);
            WebnmUnRegisterLock (pHttp);
            return;

        }

        /* Convert the Port Information from PortList to Array of Ports
         * format. 
         */
        if (ISS_FAILURE ==
            PvlanWebConvertStrToPortArray (*pPortList, au4EgressPorts))
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Maximum of ISS_MAX_UPLINK_PORTS \
                          are allowed as Egress ports\r\n");
            ISS_UNLOCK ();
            FsUtilReleaseBitList ((UINT1 *) pPortList);
            WebnmUnRegisterLock (pHttp);
            return;

        }
        FsUtilReleaseBitList ((UINT1 *) pPortList);
    }
    else
    {
        IssSendError (pHttp, (INT1 *) "Invalid Egress Port list\r\n");
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    /* Processing block if the Port Isolation entries are to be added. */
    if (STRCMP (pHttp->au1Value, "Add") == 0)
    {
        while (au4EgressPorts[u4PortScan] != 0)
        {
            if (nmhTestv2IssPortIsolationRowStatus (&u4ErrorCode,
                                                    i4IngressPort,
                                                    i4VlanId,
                                                    (INT4)
                                                    au4EgressPorts[u4PortScan],
                                                    CREATE_AND_GO) ==
                SNMP_FAILURE)
            {
                if (i4IngressPort == (INT4) au4EgressPorts[u4PortScan])
                {
                    IssSendError (pHttp, (INT1 *) "Ingress and Egress ports \
                                    cannot be the same\r\n");
                }
                else
                {
                    IssSendError (pHttp, (INT1 *) "Entry already exists\r\n");
                }
                ISS_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                return;
            }

            if (nmhSetIssPortIsolationRowStatus (i4IngressPort,
                                                 i4VlanId,
                                                 (INT4)
                                                 au4EgressPorts[u4PortScan],
                                                 CREATE_AND_GO) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (INT1 *) "Port Isolation entry Addition\
                                                 failed.\r\n");
                ISS_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                return;
            }
            u4PortScan++;

            if ((u4PortScan == ISS_MAX_UPLINK_PORTS))
            {
                break;
            }
        }                        /* End of while */
    }

    /* Processing block if the Port Isolation entries are to be deleted. */
    if ((STRCMP (pHttp->au1Value, "Delete") == 0) ||
        (STRCMP (pHttp->au1Value, "DELETE") == 0))
    {
        u4PortScan = 0;
        while (au4EgressPorts[u4PortScan] != 0)
        {
            if (nmhTestv2IssPortIsolationRowStatus (&u4ErrorCode,
                                                    i4IngressPort,
                                                    i4VlanId,
                                                    (INT4)
                                                    au4EgressPorts[u4PortScan],
                                                    DESTROY) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (INT1 *) "Volatile Port Isolation entries \
                                             cannot be deleted.\r\n");
                ISS_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                return;
            }
            if (nmhSetIssPortIsolationRowStatus (i4IngressPort,
                                                 i4VlanId,
                                                 (INT4)
                                                 au4EgressPorts[u4PortScan],
                                                 DESTROY) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (INT1 *) "Port Isolation entry deletion\
                                                 failed.\r\n");
                ISS_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                return;
            }

            u4PortScan++;

            if (u4PortScan == ISS_MAX_UPLINK_PORTS)
            {
                break;
            }
        }                        /* End of while */
    }

    ISS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    return;
}

/*****************************************************************************/
/*  Function Name   : IssProcessIvrPvlanMappingPage                          */
/*  Description     : This function does the following                       */
/*                    Process the request for IVR PVLAN Mapping  Page.       */
/*  Input(s)        : pHttp - pointer to http connection information.        */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
VOID
IssProcessIvrPvlanMappingPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessIvrPvlanMappingPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessIvrPvlanMappingPageSet (pHttp);
        IssProcessIvrPvlanMappingPageGet (pHttp);
    }
    return;
}

/*****************************************************************************/
/*  Function Name   : IssProcessIvrPvlanMappingPageGet                       */
/*  Description     : This function does the following                       */
/*                    Process the GET request for IVR PVLAN Mapping Page.    */
/*  Input(s)        : pHttp - pointer to http connection information.        */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
VOID
IssProcessIvrPvlanMappingPageGet (tHttp * pHttp)
{
    UINT1              *pVlanBitList = NULL;
    tSNMP_MULTI_DATA_TYPE *pu1DataString = NULL;
    UINT1               au1VcAlias[VCM_ALIAS_MAX_LEN];
    UINT4               u4TempIndex = 0;
    INT4                i4Temp = 0;
    UINT4               u4StringPos = 0;
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4VlanId = 0;
    INT4                i4NextVlanId = 0;
    INT4                i4L2ContextId = 0;
    tVlanId             u2VlanIndex = 0;
    tVlanId             u2LastVlanIndex = 0;
    tVlanIfaceVlanId    u2VlanId = 0;
    INT1                i1RetVal = 0;
    INT1                i1RetValue = 0;
    BOOL1               bResult = OSIX_FALSE;

    pVlanBitList = UtlShMemAllocVlanList ();
    if (pVlanBitList == NULL)
    {
        return;
    }

    if ((pu1DataString = WebnmAllocMultiData ()) == NULL)
    {
        IssSendError (pHttp, (CONST INT1 *) "Insufficient memory!!!");
        UtlShMemFreeVlanList (pVlanBitList);
        return;
    }

    pHttp->i4Write = 0;

    IssPrintAvailableContexts (pHttp);

    MEMSET (pVlanBitList, 0, VLAN_DEV_VLAN_LIST_SIZE);
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    i4Temp = pHttp->i4Write;

    WebnmRegisterLock (pHttp, CfaLock, CfaUnlock);
    CFA_LOCK ();

    if (nmhGetFirstIndexIfIvrMappingTable (&i4IfIndex,
                                           &i4VlanId) == SNMP_FAILURE)
    {
        UtlShMemFreeVlanList (pVlanBitList);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        CFA_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    while (1)
    {
        i1RetVal = nmhGetNextIndexIfIvrMappingTable (i4IfIndex,
                                                     &i4NextIfIndex,
                                                     i4VlanId, &i4NextVlanId);
        OSIX_BITLIST_SET_BIT (pVlanBitList, i4VlanId, CFA_MGMT_VLAN_LIST_SIZE);
        if ((i4IfIndex != i4NextIfIndex) || (i1RetVal == SNMP_FAILURE))
        {
            pHttp->i4Write = i4Temp;
            STRCPY (pHttp->au1KeyString, "INTERFACE_NO_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            i1RetValue = CfaGetVlanId ((UINT4) i4IfIndex, &u2VlanId);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u2VlanId);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            if (nmhGetFsVcL2ContextId (i4IfIndex, &i4L2ContextId) ==
                SNMP_SUCCESS)
            {

                MEMSET (au1VcAlias, 0, VCM_ALIAS_MAX_LEN);
                VcmGetAliasName ((UINT4) i4L2ContextId, au1VcAlias);

                SNPRINTF ((CHR1 *) pu1DataString->pOctetStrValue->pu1_OctetList,
                          VCM_ALIAS_MAX_LEN, "%s ", au1VcAlias);
                pu1DataString->pOctetStrValue->i4_Length = VCM_ALIAS_MAX_LEN;
                MEMSET (pHttp->au1DataString, 0, ENM_MAX_NAME_LEN);
                WebnmSendToSocket (pHttp, "L2CONTEXT_KEY", pu1DataString,
                                   ENM_DISPLAYSTRING);

            }

            MEMSET (pHttp->au1DataString, 0, ENM_MAX_NAME_LEN);
            STRCPY (pHttp->au1KeyString, "ASSOCIATED_VLAN_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            for (u2VlanIndex = VLAN_MIN_VLAN_ID;
                 u2VlanIndex <= VLAN_MAX_VLAN_ID; u2VlanIndex++)
            {
                OSIX_BITLIST_IS_BIT_SET (pVlanBitList, u2VlanIndex,
                                         VLAN_LIST_SIZE, bResult);

                if (OSIX_TRUE == bResult)
                {
                    u4StringPos = STRLEN (pHttp->au1DataString);

                    SPRINTF ((CHR1 *) (pHttp->au1DataString + u4StringPos),
                             "%d, ", u2VlanIndex);
                    u4TempIndex++;
                    u2LastVlanIndex = u2VlanIndex;
                }
            }                    /* End of for */

            SPRINTF ((CHR1 *) (pHttp->au1DataString + u4StringPos), "%d ",
                     u2LastVlanIndex);

            MEMSET (pVlanBitList, 0, VLAN_DEV_VLAN_LIST_SIZE);
            u4TempIndex = 0;
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            if (SNMP_FAILURE == i1RetVal)
            {
                break;
            }
        }

        i4IfIndex = i4NextIfIndex;
        i4VlanId = i4NextVlanId;

        i4NextIfIndex = i4NextVlanId = 0;

    }                            /* End of while */
    UtlShMemFreeVlanList (pVlanBitList);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    UNUSED_PARAM (i1RetValue);
    CFA_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    return;
}

/*****************************************************************************/
/*  Function Name   : IssProcessIvrPvlanMappingPageSet                       */
/*  Description     : This function does the following                       */
/*                    Process the SET request for IVR PVLAN Mapping page.    */
/*  Input(s)        : pHttp - pointer to http connection information.        */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
VOID
IssProcessIvrPvlanMappingPageSet (tHttp * pHttp)
{
    UINT1              *pVlanBitList = NULL;
    UINT1               au1String[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4ErrorCode = 0;
    UINT4               u4VlanIndex = 0;
    INT4                i4IfIndex = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4L2Context = 0;
    BOOL1               bResult = OSIX_FALSE;

    pVlanBitList = UtilVlanAllocVlanListSize (sizeof (tVlanList));

    if (pVlanBitList == NULL)
    {
        return;
    }

    MEMSET (pVlanBitList, 0, sizeof (VLAN_DEV_VLAN_LIST_SIZE));
    MEMSET (au1String, 0, (sizeof (UINT1) * CFA_MAX_PORT_NAME_LENGTH));

    STRCPY (pHttp->au1Name, "VLAN_INTERFACE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4IfIndex = ATOI (pHttp->au1Value);
    SPRINTF ((CHR1 *) au1String, "vlan%d", i4IfIndex);

    /*Changed for VRF Support */
    STRCPY (pHttp->au1Name, "L2CONTEXT");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    VcmIsSwitchExist (pHttp->au1Value, &u4L2Context);

    if (CfaGetInterfaceIndexFromNameInCxt (u4L2Context, au1String,
                                           &u4IfIndex) == OSIX_FAILURE)
    {
        UtilVlanReleaseVlanListSize (pVlanBitList);
        IssSendError (pHttp, (INT1 *) "Interface does not exists\r\n");
        return;
    }

    STRCPY (pHttp->au1Name, "ASSOCIATED_VLAN");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);
    PvlanWebConvertVlanListToArray (pHttp->au1Value, pVlanBitList);

    WebnmRegisterLock (pHttp, CfaLock, CfaUnlock);
    CFA_LOCK ();

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Value, "Add") == 0)
    {
        for (u4VlanIndex = VLAN_MIN_VLAN_ID;
             u4VlanIndex <= VLAN_MAX_VLAN_ID; u4VlanIndex++)
        {
            OSIX_BITLIST_IS_BIT_SET (pVlanBitList, u4VlanIndex, VLAN_LIST_SIZE,
                                     bResult);

            if (OSIX_TRUE == bResult)
            {
                if (nmhTestv2IfIvrMappingRowStatus (&u4ErrorCode,
                                                    (INT4) u4IfIndex,
                                                    (INT4) u4VlanIndex,
                                                    CREATE_AND_GO) ==
                    SNMP_FAILURE)
                {
                    UtilVlanReleaseVlanListSize (pVlanBitList);
                    IssSendError (pHttp,
                                  (INT1 *) "Invalid indices passed to Create\
                                          IVR  Mapping entry \r\n");
                    CFA_UNLOCK ();
                    WebnmUnRegisterLock (pHttp);
                    return;
                }

                if (nmhSetIfIvrMappingRowStatus ((INT4) u4IfIndex,
                                                 (INT4) u4VlanIndex,
                                                 CREATE_AND_GO) == SNMP_FAILURE)
                {
                    UtilVlanReleaseVlanListSize (pVlanBitList);
                    IssSendError (pHttp, (INT1 *) "IVR Mapping entry creation \
                                          failed\r\n");
                    CFA_UNLOCK ();
                    WebnmUnRegisterLock (pHttp);
                    return;
                }
            }
        }                        /* End of while */
    }

    if ((STRCMP (pHttp->au1Value, "Delete") == 0))
    {
        for (u4VlanIndex = VLAN_MIN_VLAN_ID;
             u4VlanIndex <= VLAN_MAX_VLAN_ID; u4VlanIndex++)
        {
            OSIX_BITLIST_IS_BIT_SET (pVlanBitList, u4VlanIndex, VLAN_LIST_SIZE,
                                     bResult);

            if (OSIX_TRUE == bResult)
            {

                if (nmhTestv2IfIvrMappingRowStatus (&u4ErrorCode,
                                                    (INT4) u4IfIndex,
                                                    (INT4) u4VlanIndex,
                                                    DESTROY) == SNMP_FAILURE)
                {
                    UtilVlanReleaseVlanListSize (pVlanBitList);
                    IssSendError (pHttp,
                                  (INT1 *) "Invalid indices passed to Delete\
                                         IVR Mapping entry \r\n");
                    CFA_UNLOCK ();
                    WebnmUnRegisterLock (pHttp);
                    return;
                }

                if (nmhSetIfIvrMappingRowStatus ((INT4) u4IfIndex,
                                                 (INT4) u4VlanIndex,
                                                 DESTROY) == SNMP_FAILURE)
                {
                    UtilVlanReleaseVlanListSize (pVlanBitList);
                    IssSendError (pHttp,
                                  (INT1 *)
                                  "IVR Mapping entry deletion failed\r\n");
                    CFA_UNLOCK ();
                    WebnmUnRegisterLock (pHttp);
                    return;
                }
            }

        }                        /* End of while */
    }

    UtilVlanReleaseVlanListSize (pVlanBitList);
    CFA_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    return;
}

/*****************************************************************************/
/*  Function Name   : PvlanWebConvertVlanListToArray                         */
/*  Description     : This function does the following                       */
/*                    Converts the list of vlan ids to array of vlan ids     */
/*  Input(s)        : pu1Str - list of vlan ids seperated by commas .        */
/*                    pu1VlanList - Bitlist of vlan ids                      */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
VOID
PvlanWebConvertVlanListToArray (UINT1 *pu1Str, tVlanList au1VlanList)
{
    UINT1              *pu1TmpStr = NULL;
    INT4                i4VlanIndex = 0;

    pu1TmpStr = (UINT1 *) STRTOK (pu1Str, ",");

    while (pu1TmpStr != NULL)
    {
        i4VlanIndex = ATOI (pu1TmpStr);
        OSIX_BITLIST_SET_BIT (au1VlanList, i4VlanIndex,
                              CFA_MGMT_VLAN_LIST_SIZE);
        pu1TmpStr = (UINT1 *) STRTOK (NULL, ",");
    }                            /* End of while */

    return;
}

/*****************************************************************************/
/*  Function Name   : IssPvlanSetVlanTypeandPrimaryId                        */
/*  Description     : This function does the following                       */
/*                    Configures the vlan type and primary vlan id.          */
/*  Input(s)        : pHttp - pointer to Http                                */
/*                    u2VlanId - Vlan Id                                     */
/*                    u1VlanType - Vlan Type                                 */
/*                    u2Primary VlanId - Primary Vlan Id.                    */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
INT1
IssPvlanSetVlanTypeandPrimaryId (tHttp * pHttp,
                                 UINT2 u2VlanId,
                                 UINT1 u1VlanType, UINT2 u2PrimaryVlanId)
{
    UNUSED_PARAM (pHttp);
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (u1VlanType);
    UNUSED_PARAM (u2PrimaryVlanId);
    return ISS_SUCCESS;
}                                /* End of IssPvlanSetVlanTypeandPrimaryId */

/*****************************************************************************/
/*  Function Name   :  PvlanWebConvertStrToPortArray                         */
/*  Description     : This function does the following                       */
/*                    (i) It converts the interface list to interface array  */
/*                        (e.g) The interface list Gi0/1,Gi0/2,Gi0/10 is     */
/*                         converted to interface indices and stored         */
/*                         sequentially in the array like say 0,2,10.        */
/*  Input(s)        : pu1IfaceList - pointer to interface list               */
/*                    pu4Array     - pointer to interface array              */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  ISS_FAILURE - when the number of ifindices*/
/*                                 exceeds ISS_MAX_UPLINK_PORTS.             */
/*                                 ISS_SUCCESS otherwise.                    */
/*****************************************************************************/
INT4
PvlanWebConvertStrToPortArray (tPortList PortList, UINT4 *pu4Array)
{
    UINT4               u4Counter = 0;
    UINT4               u4IfIndex = 0;
    BOOL1               bResult = OSIX_FALSE;

    for (u4IfIndex = 0; u4IfIndex <= BRG_NUM_PHY_PLUS_LOG_PORTS; u4IfIndex++)
    {
        OSIX_BITLIST_IS_BIT_SET (PortList, u4IfIndex, BRG_PORT_LIST_SIZE,
                                 bResult);

        if (u4Counter > ISS_MAX_UPLINK_PORTS)
        {
            /* Only upto a maximum of ISS_MAX_UPLINK_PORTS are allowed to be
             * configured as egress ports. Hence return failure if the number of
             * egress ports exceeds ISS_MAX_UPLINK_PORTS. 
             */
            return ISS_FAILURE;
        }
        if (OSIX_TRUE == bResult)
        {
            pu4Array[u4Counter++] = u4IfIndex;
        }
    }                            /* End of for */

    return ISS_SUCCESS;
}

#endif /* PVLANWEB_C */
