/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pvrstweb.c,v 1.25 2014/07/19 12:53:50 siva Exp $  
 *
 * Description: This file contains the routines required for 
 *              PVRST web Module 
 *******************************************************************/

#ifndef _PVRST_WEB_C_
#define _PVRST_WEB_C_

#ifdef WEBNM_WANTED
#include "webiss.h"
#include "isshttp.h"
#include "vcm.h"
#include "pvrst.h"
#include "snmputil.h"
/*******************************************************************************
 *  Function Name : IssProcessPvrstInstPortStatusPage
 *  Description   : This function processes the request coming for the Pvrst
 *                  Instance Port Status page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessPvrstInstPortStatusPage (tHttp * pHttp)
{
    INT4                i4CurrInst = 0;
    INT4                i4CurrPort = 0;
    INT4                i4NextInst = 0;
    INT4                i4NextPort = 0;
    INT4                i4RetVal = 0;
    INT4                i4OutCome = SNMP_FAILURE;
    INT4                i4EnabledStatus = 0;
    UINT4               u4Temp = 0;
    UINT4               u4InContext = 0;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;
    UINT1               u1OperStatus = 0;
    tSNMP_OCTET_STRING_TYPE *DesigRoot = NULL;
    tSNMP_OCTET_STRING_TYPE *DesigBridge = NULL;
    tSNMP_OCTET_STRING_TYPE *DesigPort = NULL;

    pHttp->i4Write = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    DesigRoot = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PVRST_MAX_BRIDGEID_BUFFER);
    if (DesigRoot == NULL)
    {
        return;
    }

    DesigBridge = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PVRST_MAX_BRIDGEID_BUFFER);
    if (DesigBridge == NULL)
    {
        free_octetstring (DesigRoot);
        return;
    }

    DesigPort = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PVRST_MAX_PORTID_BUFFER);
    if (DesigPort == NULL)
    {
        free_octetstring (DesigRoot);
        free_octetstring (DesigBridge);
        return;
    }

    WebnmRegisterLock (pHttp, AstLock, AstUnLock);
    AST_LOCK ();
    i4OutCome = (INT4) (nmhGetNextIndexFsMIPvrstInstPortTable (i4CurrInst,
                                                               &i4NextInst,
                                                               i4CurrPort,
                                                               &i4NextPort));

    if (i4OutCome == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        free_octetstring (DesigRoot);
        free_octetstring (DesigBridge);
        free_octetstring (DesigPort);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;

    while (nmhGetNextIndexFsMIPvrstInstPortTable (i4CurrInst,
                                                  &i4NextInst, i4CurrPort,
                                                  &i4NextPort) == SNMP_SUCCESS)
    {
        pHttp->i4Write = (INT4) u4Temp;

        VcmGetContextInfoFromIfIndex ((UINT4) i4NextPort, &u4InContext,
                                      &u2LocalPort);

        /* Get the Context for the Session */
        i4OutCome = WebnmApiGetContextId (pHttp, &u4ContextId);
        if (i4OutCome != ENM_SUCCESS)
        {
            AST_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) ("Context Selection Failed \n"));
            free_octetstring (DesigRoot);
            free_octetstring (DesigBridge);
            free_octetstring (DesigPort);
            return;
        }
        /* Get the port oper status for PVRST */
        AstL2IwfGetPortOperStatus (STP_MODULE, (UINT2) i4NextPort,
                                   &u1OperStatus);

        if ((u1OperStatus != CFA_IF_DOWN) && (u4ContextId == u4InContext))
        {
            /* Get the instance id value */
            STRCPY (pHttp->au1KeyString, "INSTANCE_ID_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextInst);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            /* Get the Port index value */
            STRCPY (pHttp->au1KeyString, "PORT_INDEX_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextPort);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            /* Get the Designated root value corresp. to current instance and port */
            MEMSET (DesigRoot->pu1_OctetList, 0, ISS_PVRST_MAX_BRIDGEID_BUFFER);
            STRCPY (pHttp->au1KeyString, "DESIG_ROOT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetFsMIPvrstInstPortDesignatedRoot (i4NextInst, i4NextPort,
                                                   DesigRoot);
            IssConvertOctetToString (DesigRoot->pu1_OctetList,
                                     (UINT4) DesigRoot->i4_Length,
                                     pHttp->au1DataString);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            /* Get the Designated bridge value corresp. to current instance and port */
            MEMSET (DesigBridge->pu1_OctetList, 0,
                    ISS_PVRST_MAX_BRIDGEID_BUFFER);
            STRCPY (pHttp->au1KeyString, "DESIG_BRIDGE_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetFsMIPvrstInstPortDesignatedBridge (i4NextInst, i4NextPort,
                                                     DesigBridge);
            IssConvertOctetToString (DesigBridge->pu1_OctetList,
                                     (UINT4) DesigBridge->i4_Length,
                                     pHttp->au1DataString);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            /* Get the Designated port value corresp. to current instance and port */
            MEMSET (DesigPort->pu1_OctetList, 0, ISS_PVRST_MAX_PORTID_BUFFER);
            STRCPY (pHttp->au1KeyString, "DESIG_PORT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetFsMIPvrstInstPortDesignatedPort (i4NextInst, i4NextPort,
                                                   DesigPort);
            IssConvertOctetToString (DesigPort->pu1_OctetList,
                                     (UINT4) DesigPort->i4_Length,
                                     pHttp->au1DataString);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            /* Get the port state value corresp. to current instance and port */
            STRCPY (pHttp->au1KeyString, "PORT_STATE_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetFsMIPvrstInstPortState (i4NextInst, i4NextPort, &i4RetVal);
            switch (i4RetVal)
            {
                case 1:
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Disabled");
                    break;

                case 2:
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Discarding");
                    break;

                case 4:
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Learning");
                    break;

                case 5:
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Forwarding");
                    break;
                default:
                    break;
            }
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            /* Get the port role value corresp. to current instance and port */
            STRCPY (pHttp->au1KeyString, "PORT_ROLE_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetFsMIPvrstInstPortRole (i4NextInst, i4NextPort, &i4RetVal);
            nmhGetFsMIPvrstPortEnabledStatus (i4NextPort, &i4EnabledStatus);
            if (i4EnabledStatus != SNMP_SUCCESS)
            {
                i4RetVal = 0;
            }

            switch (i4RetVal)
            {
                case 0:
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Disabled");
                    break;

                case 1:
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Alternate");
                    break;

                case 2:
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Backup");
                    break;

                case 3:
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Root");
                    break;

                case 4:
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Designated");
                    break;

                default:
                    break;
            }
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);
        }
        else
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        }

        i4CurrInst = i4NextInst;
        i4CurrPort = i4NextPort;
    }
    AST_UNLOCK ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    free_octetstring (DesigRoot);
    free_octetstring (DesigPort);
    free_octetstring (DesigBridge);
}

/*******************************************************************************
 *  Function Name : IssProcessPvrstInstPortConfPage
 *  Description   : This function processes the HTTP request coming for the
 *                  Pvrst Instance Port configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None.
 *******************************************************************************/
VOID
IssProcessPvrstInstPortConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessPvrstInstPortConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessPvrstInstPortConfPageSet (pHttp);
    }
    return;
}

/*******************************************************************************
 *  Function Name : IssProcessPvrstInstPortConfPageGet
 *  Description   : This function processes the get request for the Pvrst
 *                  Instance Port configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessPvrstInstPortConfPageGet (tHttp * pHttp)
{
    INT4                i4CurrPort = 0;
    INT4                i4NextPort = 0;
    INT4                i4CurrInst = 0;
    INT4                i4NextInst = 0;
    INT4                i4RetVal = 0;
    INT4                i4OutCome = SNMP_FAILURE;
    UINT4               u4Temp = 0;
    UINT4               u4InContext = 0;
    UINT4               u4ContextId = 0;
    UINT1               u1OperStatus = 0;
    UINT2               u2LocalPort = 0;

    pHttp->i4Write = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    WebnmRegisterLock (pHttp, AstLock, AstUnLock);
    AST_LOCK ();
    i4OutCome = (INT4) (nmhGetNextIndexFsMIPvrstInstPortTable (i4CurrInst,
                                                               &i4NextInst,
                                                               i4CurrPort,
                                                               &i4NextPort));

    if (i4OutCome == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;

    while (nmhGetNextIndexFsMIPvrstInstPortTable (i4CurrInst, &i4NextInst,
                                                  i4CurrPort,
                                                  &i4NextPort) == SNMP_SUCCESS)
    {
        pHttp->i4Write = (INT4) u4Temp;

        VcmGetContextInfoFromIfIndex ((UINT4) i4NextPort, &u4InContext,
                                      &u2LocalPort);
        /* Get the Context for the Session */
        i4OutCome = WebnmApiGetContextId (pHttp, &u4ContextId);
        if (i4OutCome != ENM_SUCCESS)
        {
            AST_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) ("Context Selection Failed \n"));
            return;
        }
        /* Get the port oper status for PVRST */
        AstL2IwfGetPortOperStatus (STP_MODULE, (UINT2) i4NextPort,
                                   &u1OperStatus);

        if ((u1OperStatus != CFA_IF_DOWN) && (u4ContextId == u4InContext))
        {
            /* Print the Instance */
            STRCPY (pHttp->au1KeyString, "INSTANCE_ID_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextInst);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            /* Print the Port index */
            STRCPY (pHttp->au1KeyString, "PORT_INDEX_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextPort);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            /* Print the Module status value */
            STRCPY (pHttp->au1KeyString, "MOD_STATUS_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (nmhGetFsMIPvrstInstPortEnableStatus (i4NextInst, i4NextPort,
                                                     &i4RetVal) == SNMP_SUCCESS)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            }
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            /* Print the port path cost value */
            STRCPY (pHttp->au1KeyString, "PATH_COST_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (nmhGetFsMIPvrstInstPortPathCost
                (i4NextInst, i4NextPort, &i4RetVal) == SNMP_SUCCESS)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            }
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            /* Print the port priority value */
            STRCPY (pHttp->au1KeyString, "PRIORITY_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (nmhGetFsMIPvrstInstPortPriority
                (i4NextInst, i4NextPort, &i4RetVal) == SNMP_SUCCESS)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            }

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);
        }
        else
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        }

        i4CurrPort = i4NextPort;
        i4CurrInst = i4NextInst;
    }

    AST_UNLOCK ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*******************************************************************************
 *  Function Name : IssProcessPvrstInstPortConfPageSet
 *  Description   : This function processes the set request for the Pvrst port
 *                  configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessPvrstInstPortConfPageSet (tHttp * pHttp)
{
    INT4                i4PageNo = 0;
    INT4                i4PortNo = 0;
    INT4                i4Instance = 0;
    INT4                i4ModStatus = 0;
    INT4                i4Priority = 0;
    INT4                i4PathCost = 0;
    UINT4               u4ErrValue = 0;
    UINT1               au1String[WEB_MAX_STR_LEN];

    STRCPY (pHttp->au1Name, "INSTANCE_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Instance = ATOI (pHttp->au1Value);
    STRCPY (pHttp->au1Name, "PORT_INDEX");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PortNo = ATOI (pHttp->au1Value);
    STRCPY (pHttp->au1Name, "MOD_STATUS");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4ModStatus = ATOI (pHttp->au1Value);
    STRCPY (pHttp->au1Name, "PATH_COST");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PathCost = ATOI (pHttp->au1Value);
    STRCPY (pHttp->au1Name, "PRIORITY");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Priority = ATOI (pHttp->au1Value);

    WebnmRegisterLock (pHttp, AstLock, AstUnLock);
    AST_LOCK ();

    if (nmhTestv2FsMIPvrstInstPortEnableStatus
        (&u4ErrValue, i4Instance, i4PortNo, i4ModStatus) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid value for module status");
        return;
    }
    if (nmhSetFsMIPvrstInstPortEnableStatus (i4Instance, i4PortNo, i4ModStatus)
        == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the module status");
        return;
    }

    if (nmhTestv2FsMIPvrstInstPortAdminPathCost
        (&u4ErrValue, i4Instance, i4PortNo, i4PathCost) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid value for path cost");
        return;
    }
    if (nmhSetFsMIPvrstInstPortAdminPathCost (i4Instance, i4PortNo, i4PathCost)
        == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the path cost");
        return;
    }

    if (nmhTestv2FsMIPvrstInstPortPriority (&u4ErrValue, i4Instance, i4PortNo,
                                            i4Priority) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid value for port priority");
        return;
    }
    if (nmhSetFsMIPvrstInstPortPriority (i4Instance, i4PortNo, i4Priority) ==
        SNMP_FAILURE)
    {
        AST_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the priority");
        return;
    }

    i4PageNo = ((i4PortNo + ISS_PORTS_PER_PAGE - 1) / ISS_PORTS_PER_PAGE) - 1;
    SPRINTF ((CHR1 *) au1String, "?PAGENO=%d", i4PageNo);
    STRNCAT (pHttp->au1PostQuery, au1String,
             (ENM_MAX_NAME_LEN - STRLEN (pHttp->au1PostQuery)));

    AST_UNLOCK ();
    WebnmUnRegisterLock (pHttp);

    IssProcessPvrstInstPortConfPageGet (pHttp);
}

/*******************************************************************************
 *  Function Name : IssProcessPvrstInstBrigConfPage 
 *  Description   : This function processes the request for the PVRST bridge 
 *                  configuration page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessPvrstInstBrigConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessPvrstInstBrigConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessPvrstInstBrigConfPageSet (pHttp);
    }
    return;
}

/*******************************************************************************
 *  Function Name : IssProcessPvrstInstBrigConfPageSet
 *  Description   : This function processes the set request for the Pvrst bridge
 *                  configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessPvrstInstBrigConfPageSet (tHttp * pHttp)
{
    INT4                i4VlanId = 0;
    INT4                i4MaxAge = 0;
    INT4                i4HelloTime = 0;
    INT4                i4FwdDelay = 0;
    INT4                i4TxHoldCount = 0;
    INT4                i4BrigPri = 0;
    INT4                i4ContextId = 0;
    UINT4               u4ErrCode = 0;
    INT4                OldPriority = 0;
    INT4                i4PageNo = 0;
    INT4                i4RetVal = 0;
    UINT1               au1String[WEB_MAX_STR_LEN];

    STRCPY (pHttp->au1Name, "CONTEXT_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4ContextId = ATOI (pHttp->au1Value);

    /* Check If context is default and PVRST should be running in
     * that context */
    if ((i4ContextId != ISS_PVRST_DEFAULT_CONTEXT) ||
        (!AstIsPvrstStartedInContext ((UINT4) i4ContextId)))
    {
        IssSendError (pHttp,
                      (CONST INT1 *) "PVRST is not running in DEFAULT context");
        return;
    }

    STRCPY (pHttp->au1Name, "INSTANCE_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4VlanId = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "MAX_AGE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4MaxAge = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "HELLO_TIME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4HelloTime = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "FWD_DELAY");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4FwdDelay = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "TX_HOLD_COUNT");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4TxHoldCount = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "BRIG_PRI");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4BrigPri = ATOI (pHttp->au1Value);

    WebnmRegisterLock (pHttp, AstLock, AstUnLock);
    AST_LOCK ();

    i4RetVal = 0;
    nmhGetFsMIPvrstInstBridgeMaxAge (i4ContextId, i4VlanId, &i4RetVal);
    if (i4RetVal != (i4MaxAge * HUNDEREDTH_OF_SEC))
    {
        if (nmhTestv2FsMIPvrstInstBridgeMaxAge (&u4ErrCode, i4ContextId,
                                                i4VlanId,
                                                i4MaxAge * HUNDEREDTH_OF_SEC) ==
            SNMP_FAILURE)
        {
            AST_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Invalid value for Max age");
            return;
        }

        if (nmhSetFsMIPvrstInstBridgeMaxAge
            (i4ContextId, i4VlanId,
             i4MaxAge * HUNDEREDTH_OF_SEC) == SNMP_FAILURE)
        {
            AST_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set the Max age value");
            return;
        }
    }
    i4RetVal = 0;
    nmhGetFsMIPvrstInstBridgeHelloTime (i4ContextId, i4VlanId, &i4RetVal);
    if (i4RetVal != (i4HelloTime * HUNDEREDTH_OF_SEC))
    {
        if (nmhTestv2FsMIPvrstInstBridgeHelloTime
            (&u4ErrCode, i4ContextId, i4VlanId,
             i4HelloTime * HUNDEREDTH_OF_SEC) == SNMP_FAILURE)
        {
            AST_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Invalid value for Hello Time");
            return;
        }

        if (nmhSetFsMIPvrstInstBridgeHelloTime (i4ContextId, i4VlanId,
                                                i4HelloTime *
                                                HUNDEREDTH_OF_SEC) ==
            SNMP_FAILURE)
        {
            AST_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set the Hello Time value");
            return;
        }
    }
    i4RetVal = 0;
    nmhGetFsMIPvrstInstBridgeForwardDelay (i4ContextId, i4VlanId, &i4RetVal);
    if (i4RetVal != (i4FwdDelay * HUNDEREDTH_OF_SEC))
    {
        if (nmhTestv2FsMIPvrstInstBridgeForwardDelay
            (&u4ErrCode, i4ContextId, i4VlanId,
             i4FwdDelay * HUNDEREDTH_OF_SEC) == SNMP_FAILURE)
        {
            AST_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Invalid value for Fwd Delay");
            return;
        }

        if (nmhSetFsMIPvrstInstBridgeForwardDelay
            (i4ContextId, i4VlanId,
             i4FwdDelay * HUNDEREDTH_OF_SEC) == SNMP_FAILURE)
        {
            AST_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set the Fwd Delay value");
            return;
        }
    }
    i4RetVal = 0;
    nmhGetFsMIPvrstInstTxHoldCount (i4ContextId, i4VlanId, &i4RetVal);
    if (i4RetVal != i4TxHoldCount)
    {
        if (nmhTestv2FsMIPvrstInstTxHoldCount
            (&u4ErrCode, i4ContextId, i4VlanId, i4TxHoldCount) == SNMP_FAILURE)
        {
            AST_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Invalid value for Hold Count");
            return;
        }
        if (nmhSetFsMIPvrstInstTxHoldCount
            (i4ContextId, i4VlanId, i4TxHoldCount) == SNMP_FAILURE)
        {
            AST_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set the "
                          "Hold Count value");
            return;
        }
    }

    nmhGetFsMIPvrstInstBridgePriority (i4ContextId, i4VlanId, &OldPriority);

    /* If the Priority Value same as OldPriority Value,
     * No need to call the  Test and Set Routines Again */
    if (OldPriority != i4BrigPri)
    {
        if (nmhTestv2FsMIPvrstInstBridgePriority
            (&u4ErrCode, i4ContextId, i4VlanId, i4BrigPri) == SNMP_FAILURE)
        {
            AST_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Bridge Priority must be in "
                          "increments of 4096 and can be upto 61440");
            return;
        }

        if (nmhSetFsMIPvrstInstBridgePriority
            (i4ContextId, i4VlanId, i4BrigPri) == SNMP_FAILURE)
        {
            AST_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Bridge Priority must be in "
                          "increments of 4096 and can be upto 61440");
            return;
        }
    }
    i4PageNo = ((i4VlanId + ISS_INSTANCES_PER_PAGE - 1) /
                ISS_INSTANCES_PER_PAGE) - 1;
    SPRINTF ((CHR1 *) au1String, "?PAGENO=%d", i4PageNo);
    STRNCAT (pHttp->au1PostQuery, au1String,
             (ENM_MAX_NAME_LEN - STRLEN (pHttp->au1PostQuery)));
    AST_UNLOCK ();
    WebnmUnRegisterLock (pHttp);

    IssProcessPvrstInstBrigConfPageGet (pHttp);
}

/*******************************************************************************
 *  Function Name : IssProcessPvrstInstBrigConfPageGet
 *  Description   : This function processes the get request for the Pvrst 
 *                  instance bridge configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessPvrstInstBrigConfPageGet (tHttp * pHttp)
{
    INT4                i4RetVal = 0;
    INT4                i4ContextId = 0;
    INT4                i4CurrInst = 0;
    INT4                i4NextInst = 0;
    INT4                i4NextContextId = 0;
    INT4                i4OutCome;
    UINT4               u4Temp = 0;
    INT4                i4DispCnt = 0;

    pHttp->i4Write = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    WebnmRegisterLock (pHttp, AstLock, AstUnLock);
    AST_LOCK ();
    i4OutCome = (INT4) (nmhGetNextIndexFsMIPvrstInstBridgeTable (i4ContextId,
                                                                 &i4NextContextId,
                                                                 i4CurrInst,
                                                                 &i4NextInst));
    if (i4OutCome == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;

    while (nmhGetNextIndexFsMIPvrstInstBridgeTable (i4ContextId,
                                                    &i4NextContextId,
                                                    i4CurrInst,
                                                    &i4NextInst) ==
           SNMP_SUCCESS)
    {
        if (AstIsPvrstStartedInContext ((UINT4) i4NextContextId)
            && (i4NextInst != 0))
        {
            pHttp->i4Write = (INT4) u4Temp;

            /* Context Id */
            STRCPY (pHttp->au1KeyString, "CONTEXT_ID_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextContextId);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            /* Instance Id */
            STRCPY (pHttp->au1KeyString, "INSTANCE_ID_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextInst);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            /* Max Age */
            STRCPY (pHttp->au1KeyString, "MAX_AGE_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (nmhGetFsMIPvrstInstBridgeMaxAge
                (i4NextContextId, i4NextInst, &i4RetVal) == SNMP_SUCCESS)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         i4RetVal / HUNDEREDTH_OF_SEC);
            }
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            /* Hello Time */
            STRCPY (pHttp->au1KeyString, "HELLO_TIME_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (nmhGetFsMIPvrstInstBridgeHelloTime
                (i4NextContextId, i4NextInst, &i4RetVal) == SNMP_SUCCESS)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         i4RetVal / HUNDEREDTH_OF_SEC);
            }
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            /* Forward Delay */
            STRCPY (pHttp->au1KeyString, "FWD_DELAY_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (nmhGetFsMIPvrstInstBridgeForwardDelay
                (i4NextContextId, i4NextInst, &i4RetVal) == SNMP_SUCCESS)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         i4RetVal / HUNDEREDTH_OF_SEC);
            }
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            /* Tx Hold Count */
            STRCPY (pHttp->au1KeyString, "TX_HOLD_COUNT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (nmhGetFsMIPvrstInstTxHoldCount
                (i4NextContextId, i4NextInst, &i4RetVal) == SNMP_SUCCESS)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            }
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            /* Bridge Priority */
            STRCPY (pHttp->au1KeyString, "BRIG_PRI_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (nmhGetFsMIPvrstInstBridgePriority
                (i4NextContextId, i4NextInst, &i4RetVal) == SNMP_SUCCESS)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            }
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            i4DispCnt++;
        }
        i4ContextId = i4NextContextId;
        i4CurrInst = i4NextInst;
    }
    if (i4DispCnt == 0)
    {
        AST_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }
    AST_UNLOCK ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

}

/*******************************************************************************
 *  Function Name : IssProcessPvrstInfoPage
 *  Description   : This function processes the request coming for the Pvrst
 *                  basic information page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessPvrstInfoPage (tHttp * pHttp)
{
    INT4                i4ContextId = 0;
    UINT1               au1String[20];
    tMacAddr            BrgAddr;

    IssPrintPvrstCurrentContext (pHttp);

    WebnmRegisterLock (pHttp, AstLock, AstUnLock);
    AST_LOCK ();

    /* Context Id */
    STRCPY (pHttp->au1Name, "CONTEXT_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4ContextId = ATOI (pHttp->au1Value);

    /* Bridge Mac Address */
    STRCPY (pHttp->au1KeyString, "BRG_ADDR_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsMIPvrstBrgAddress (i4ContextId, (tMacAddr *) & BrgAddr);
    IssPrintMacAddress ((UINT1 *) BrgAddr, au1String);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    AST_UNLOCK ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*******************************************************************************
 *  Function Name : IssProcessPvrstInstInfoPage
 *  Description   : This function processes the request coming for the PVRST 
 *                  Instance Information page. 
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessPvrstInstInfoPage (tHttp * pHttp)
{
    INT4                i4RetVal = 0;
    INT4                i4PrevInstId = 0;
    INT4                i4PrevContextId = 0;
    INT4                i4InstId = 0;
    INT4                i4ContextId = 0;
    UINT4               u4RetVal = 0;
    UINT4               u4Temp = 0;
    tSNMP_OCTET_STRING_TYPE *DesigRoot = NULL;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    DesigRoot = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PVRST_MAX_BRIDGEID_BUFFER);

    if (DesigRoot == NULL)
    {
        return;
    }

    WebnmRegisterLock (pHttp, AstLock, AstUnLock);
    AST_LOCK ();

    if (nmhGetNextIndexFsMIPvrstInstBridgeTable (i4PrevContextId, &i4ContextId,
                                                 i4PrevInstId,
                                                 &i4InstId) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        free_octetstring (DesigRoot);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;
    while (nmhGetNextIndexFsMIPvrstInstBridgeTable
           (i4PrevContextId, &i4ContextId, i4PrevInstId,
            &i4InstId) == SNMP_SUCCESS)
    {
        pHttp->i4Write = (INT4) u4Temp;
        /* Context Id */
        STRCPY (pHttp->au1KeyString, "CONTEXT_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4ContextId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* Instance Id */
        STRCPY (pHttp->au1KeyString, "INSTANCE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4InstId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* Designated Root */
        MEMSET (DesigRoot->pu1_OctetList, 0, ISS_PVRST_MAX_BRIDGEID_BUFFER);
        STRCPY (pHttp->au1KeyString, "DESIG_ROOT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsMIPvrstInstDesignatedRoot (i4ContextId, i4InstId, DesigRoot);
        IssConvertOctetToString (DesigRoot->pu1_OctetList,
                                 (UINT4) DesigRoot->i4_Length,
                                 pHttp->au1DataString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* Root Cost */
        STRCPY (pHttp->au1KeyString, "ROOT_COST_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsMIPvrstInstRootCost (i4ContextId, i4InstId, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* Root Port */
        STRCPY (pHttp->au1KeyString, "ROOT_PORT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsMIPvrstInstRootPort (i4ContextId, i4InstId, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* Max Age */
        STRCPY (pHttp->au1KeyString, "MAX_AGE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsMIPvrstInstBridgeMaxAge (i4ContextId, i4InstId, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                 i4RetVal / HUNDEREDTH_OF_SEC);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* Hello Time */
        STRCPY (pHttp->au1KeyString, "HELLO_TIME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsMIPvrstInstBridgeHelloTime (i4ContextId, i4InstId, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                 i4RetVal / HUNDEREDTH_OF_SEC);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* Hold Time */
        STRCPY (pHttp->au1KeyString, "HOLD_TIME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsMIPvrstInstHoldTime (i4ContextId, i4InstId, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                 i4RetVal / HUNDEREDTH_OF_SEC);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* Forward Delay */
        STRCPY (pHttp->au1KeyString, "FWD_DELAY_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsMIPvrstInstBridgeForwardDelay (i4ContextId, i4InstId,
                                               &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                 i4RetVal / HUNDEREDTH_OF_SEC);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* Time Since Top. Change */
        STRCPY (pHttp->au1KeyString, "TIME_TOP_CHANGE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4RetVal = 0;
        nmhGetFsMIPvrstInstTimeSinceTopologyChange (i4ContextId, i4InstId,
                                                    &u4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* Top. Change */
        STRCPY (pHttp->au1KeyString, "TOP_CHANGE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4RetVal = 0;
        nmhGetFsMIPvrstInstTopChanges (i4ContextId, i4InstId, &u4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        i4PrevContextId = i4ContextId;
        i4PrevInstId = i4InstId;
    }

    AST_UNLOCK ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    free_octetstring (DesigRoot);
}

/*******************************************************************************
 *  Function Name : IssProcessPvrstPortInfoPage
 *  Description   : This function processes the request coming for the Pvrst
 *                  Port Statistics page. 
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessPvrstPortInfoPage (tHttp * pHttp)
{
    INT4                i4OutCome = SNMP_FAILURE;
    INT4                i4CurrentPort = 0;
    INT4                i4NextPort = 0;
    INT4                i4PageNo = 0;
    INT4                i4DispCnt = 0;
    UINT4               u4Temp = 0;
    UINT4               u4RetVal = 0;
    UINT4               u4InContext = 0;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;

    pHttp->i4Write = 0;

    IssAddLinksToPage (pHttp);

    STRCPY (pHttp->au1Name, "PAGENO");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PageNo = ATOI (pHttp->au1Value);
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    WebnmRegisterLock (pHttp, AstLock, AstUnLock);
    AST_LOCK ();

    i4OutCome = (INT4) (nmhGetFirstIndexFsMIFuturePvrstPortTable (&i4NextPort));
    if (i4OutCome == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    i4CurrentPort = i4PageNo * ISS_PORTS_PER_PAGE;
    if (nmhGetNextIndexFsMIFuturePvrstPortTable (i4CurrentPort, &i4NextPort) ==
        SNMP_FAILURE)
    {
        AST_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;
    do
    {
        pHttp->i4Write = (INT4) u4Temp;
        VcmGetContextInfoFromIfIndex ((UINT4) i4NextPort, &u4InContext,
                                      &u2LocalPort);

        /* Get the Context for the Session */
        i4OutCome = WebnmApiGetContextId (pHttp, &u4ContextId);
        if (i4OutCome != ENM_SUCCESS)
        {
            AST_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) ("Context Selection Failed \n"));
            return;
        }
        if (u4ContextId == u4InContext)
        {
            /* Port Id */
            STRCPY (pHttp->au1KeyString, "PORT_INDEX_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextPort);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            /* Invalid Bpdus rcvd */
            STRCPY (pHttp->au1KeyString, "RX_INVD_BPDUS_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            u4RetVal = 0;
            nmhGetFsMIPvrstPortInvalidBpdusRcvd (i4NextPort, &u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            /* Invalid Configuration BPDUs rcvd */
            STRCPY (pHttp->au1KeyString, "RX_INVD_CONFIG_BPDUS_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            u4RetVal = 0;
            nmhGetFsMIPvrstPortInvalidConfigBpduRxCount (i4NextPort, &u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            /* Invalid TCN BPDUs rcvd */
            STRCPY (pHttp->au1KeyString, "RX_INVD_TCN_BPDUS_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            u4RetVal = 0;
            nmhGetFsMIPvrstPortInvalidTcnBpduRxCount (i4NextPort, &u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        }
        else
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        }

        i4DispCnt++;
        i4CurrentPort = i4NextPort;
    }
    while ((nmhGetNextIndexFsMIFuturePvrstPortTable (i4CurrentPort, &i4NextPort)
            == SNMP_SUCCESS) && (i4DispCnt < ISS_PORTS_PER_PAGE));

    AST_UNLOCK ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

}

/*******************************************************************************
 *  Function Name : IssProcessPvrstInstPortInfoPage
 *  Description   : This function processes the request coming for the Pvrst 
 *                  Instance Port Statistics page. 
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessPvrstInstPortInfoPage (tHttp * pHttp)
{
    INT4                i4CurrPort = 0;
    INT4                i4NextPort = 0;
    INT4                i4CurrInst = 0;
    INT4                i4NextInst = 0;
    INT4                i4OutCome = SNMP_FAILURE;
    UINT4               u4RetVal = 0;
    UINT4               u4Temp = 0;
    UINT4               u4InContext = 0;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;

    pHttp->i4Write = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    WebnmRegisterLock (pHttp, AstLock, AstUnLock);
    AST_LOCK ();

    i4OutCome =
        (INT4) (nmhGetNextIndexFsMIPvrstInstPortTable
                (i4CurrInst, &i4NextInst, i4CurrPort, &i4NextPort));

    if (i4OutCome == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;

    while (nmhGetNextIndexFsMIPvrstInstPortTable (i4CurrInst,
                                                  &i4NextInst, i4CurrPort,
                                                  &i4NextPort) == SNMP_SUCCESS)
    {
        pHttp->i4Write = (INT4) u4Temp;
        VcmGetContextInfoFromIfIndex ((UINT4) i4NextPort, &u4InContext,
                                      &u2LocalPort);

        /* Get the Context for the Session */
        i4OutCome = WebnmApiGetContextId (pHttp, &u4ContextId);
        if (i4OutCome != ENM_SUCCESS)
        {
            AST_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) ("Context Selection Failed \n"));
            return;
        }
        if (u4ContextId == u4InContext)
        {
            /* Instance Id */
            STRCPY (pHttp->au1KeyString, "INSTANCE_ID_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextInst);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            /* Port Index */
            STRCPY (pHttp->au1KeyString, "PORT_INDEX_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextPort);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            /* Pvrst BPDUs rcvd */
            STRCPY (pHttp->au1KeyString, "RX_BPDUS_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            u4RetVal = 0;
            nmhGetFsMIPvrstInstPortReceivedBpdus (i4NextInst, i4NextPort,
                                                  &u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            /* Config BPDUs rcvd */
            STRCPY (pHttp->au1KeyString, "RX_CONFIG_BPDUS_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            u4RetVal = 0;
            nmhGetFsMIPvrstInstPortRxConfigBpduCount (i4NextInst, i4NextPort,
                                                      &u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            /* TCN BPDUs rcvd */
            STRCPY (pHttp->au1KeyString, "RX_TCN_BPDUS_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            u4RetVal = 0;
            nmhGetFsMIPvrstInstPortRxTcnBpduCount (i4NextInst, i4NextPort,
                                                   &u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            /* Pvrst BPDUs txd */
            STRCPY (pHttp->au1KeyString, "TX_BPDUS_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            u4RetVal = 0;
            nmhGetFsMIPvrstInstPortTransmittedBpdus (i4NextInst, i4NextPort,
                                                     &u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            /* Config BPDUs txd */
            STRCPY (pHttp->au1KeyString, "TX_CONFIG_BPDUS_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            u4RetVal = 0;
            nmhGetFsMIPvrstInstPortTxConfigBpduCount (i4NextInst, i4NextPort,
                                                      &u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            /* TCN BPDUs txd */
            STRCPY (pHttp->au1KeyString, "TX_TCN_BPDUS_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            u4RetVal = 0;
            nmhGetFsMIPvrstInstPortTxTcnBpduCount (i4NextInst, i4NextPort,
                                                   &u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            /* Protocol migration count */
            STRCPY (pHttp->au1KeyString, "PRO_MIG_COUNT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            u4RetVal = 0;
            nmhGetFsMIPvrstInstProtocolMigrationCount (i4NextInst, i4NextPort,
                                                       &u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);
        }
        else
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        }

        i4CurrInst = i4NextInst;
        i4CurrPort = i4NextPort;
    }

    AST_UNLOCK ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*******************************************************************************
 *  Function Name : IssProcessPvrstPortConfPage
 *  Description   : This function processes the HTTP request coming for the
 *                  Pvrst Port configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None.
 *******************************************************************************/
VOID
IssProcessPvrstPortConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessPvrstPortConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessPvrstPortConfPageSet (pHttp);
    }
    return;
}

/*******************************************************************************
 *  Function Name : IssProcessPvrstPortConfPageSet
 *  Description   : This function processes the HTTP set request coming for the
 *                  Pvrst Port configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None.
 *******************************************************************************/
VOID
IssProcessPvrstPortConfPageSet (tHttp * pHttp)
{
    UINT1               u1OperStatus = 0;
    INT4                i4PortNo = 0;
    INT4                i4Status = 0;
    INT4                i4P2P = 0;
    INT4                i4RootGuard = 0;
    INT4                i4BpduGuard = 0;
    INT4                i4LoopGuard = 0;
    INT4                i4Encap = 0;
    INT4                i4RetVal = 0;
    UINT4               u4ErrValue = 0;

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    WebnmRegisterLock (pHttp, AstLock, AstUnLock);
    AST_LOCK ();

    if (STRCMP (pHttp->au1Value, "Add") == 0)
    {

        STRCPY (pHttp->au1Name, "PORT_NUMBER");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4PortNo = ATOI (pHttp->au1Value);

        AstL2IwfGetPortOperStatus (STP_MODULE, (UINT2) i4PortNo, &u1OperStatus);
        if (u1OperStatus == CFA_IF_DOWN)
        {
            AST_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Interface cannot be added");
            return;
        }
        if (VlanValidatePortEntry ((UINT4) i4PortNo) == SNMP_FAILURE)
        {
            AST_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Interface is not mapped with vlan");
            return;
        }

        if (nmhTestv2FsMIPvrstPortRowStatus
            (&u4ErrValue, i4PortNo, ISS_CREATE_AND_GO) == SNMP_FAILURE)
        {
            AST_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Invalid value for port status");
            return;

        }
        if (nmhSetFsMIPvrstPortRowStatus (i4PortNo, ISS_CREATE_AND_GO) ==
            SNMP_FAILURE)
        {
            AST_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Invalid value for port status");
            return;
        }

        AST_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssProcessPvrstPortConfPageGet (pHttp);
        return;
    }

    STRCPY (pHttp->au1Name, "PORT_INDEX");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PortNo = ATOI (pHttp->au1Value);
    STRCPY (pHttp->au1Name, "MOD_STATUS");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Status = ATOI (pHttp->au1Value);
    STRCPY (pHttp->au1Name, "P_2_P");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4P2P = ATOI (pHttp->au1Value);
    STRCPY (pHttp->au1Name, "ROOT_GAURD");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4RootGuard = ATOI (pHttp->au1Value);
    STRCPY (pHttp->au1Name, "BPDU_GAURD");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4BpduGuard = ATOI (pHttp->au1Value);
    STRCPY (pHttp->au1Name, "ENCAP");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Encap = ATOI (pHttp->au1Value);
    STRCPY (pHttp->au1Name, "LOOP_GAURD");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4LoopGuard = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {

        if (nmhTestv2FsMIPvrstPortRowStatus (&u4ErrValue, i4PortNo, ISS_DESTROY)
            == SNMP_FAILURE)
        {
            AST_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Invalid value for port status");
            return;

        }
        if (nmhSetFsMIPvrstPortRowStatus (i4PortNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            AST_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Invalid value for port status");
            return;
        }

        AST_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssProcessPvrstPortConfPageGet (pHttp);
        return;
    }

    if (STRCMP (pHttp->au1Value, "Apply") == 0)
    {

        if (nmhTestv2FsMIPvrstPortEnabledStatus (&u4ErrValue, i4PortNo,
                                                 i4Status) == SNMP_FAILURE)
        {
            AST_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Invalid value for port status");
            return;
        }
        if (nmhSetFsMIPvrstPortEnabledStatus (i4PortNo, i4Status) ==
            SNMP_FAILURE)
        {
            AST_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set the port status");
            return;
        }

        if (nmhTestv2FsMIPvrstPortAdminPointToPoint (&u4ErrValue, i4PortNo,
                                                     i4P2P) == SNMP_FAILURE)
        {
            AST_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Invalid value for admin point "
                          "to point");
            return;
        }
        if (nmhSetFsMIPvrstPortAdminPointToPoint (i4PortNo, i4P2P) ==
            SNMP_FAILURE)
        {
            AST_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set the admin point "
                          "to point");
            return;
        }

        nmhGetFsMIPvrstRootGuard (i4PortNo, &i4RetVal);
        if (i4RetVal != i4RootGuard)
        {
            if (nmhTestv2FsMIPvrstRootGuard (&u4ErrValue, i4PortNo,
                                             i4RootGuard) == SNMP_FAILURE)
            {
                AST_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *) "Invalid value for Root Guard, "
                              "Check if the port is trunk or not");
                return;
            }
            if (nmhSetFsMIPvrstRootGuard (i4PortNo, i4RootGuard) ==
                SNMP_FAILURE)
            {
                AST_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *) "Unable to set the Root Guard, "
                              "Check if the port is trunk or not");
                return;
            }
        }

        nmhGetFsMIPvrstBpduGuard (i4PortNo, &i4RetVal);
        if (i4RetVal != i4BpduGuard)
        {
            if (nmhTestv2FsMIPvrstBpduGuard (&u4ErrValue, i4PortNo, i4BpduGuard)
                == SNMP_FAILURE)
            {
                AST_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *) "Invalid value for Bpdu Guard, "
                              "Check if the port is trunk or not");
                return;
            }
            if (nmhSetFsMIPvrstBpduGuard (i4PortNo, i4BpduGuard) ==
                SNMP_FAILURE)
            {
                AST_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *) "Unable to set the Bpdu Guard, "
                              "Check if the port is trunk or not");
                return;
            }
        }
        nmhGetFsMIPvrstPortLoopGuard (i4PortNo, &i4RetVal);
        if (i4RetVal != i4LoopGuard)
        {
            if (nmhTestv2FsMIPvrstPortLoopGuard (&u4ErrValue, i4PortNo,
                                                 i4LoopGuard) == SNMP_FAILURE)
            {
                AST_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *) "Invalid value for Loop Guard, "
                              "Check if the port is trunk or not");
                return;
            }
            if (nmhSetFsMIPvrstPortLoopGuard (i4PortNo, i4LoopGuard) ==
                SNMP_FAILURE)
            {
                AST_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *) "Unable to set the Loop Guard, "
                              "Check if the port is trunk or not");
                return;
            }
        }

        nmhGetFsMIPvrstEncapType (i4PortNo, &i4RetVal);
        if (i4RetVal != i4Encap)
        {
            if (nmhTestv2FsMIPvrstEncapType (&u4ErrValue, i4PortNo, i4Encap) ==
                SNMP_FAILURE)
            {
                AST_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Invalid value for Encap, "
                              "Check if the port is trunk or not");
                return;
            }
            if (nmhSetFsMIPvrstEncapType (i4PortNo, i4Encap) == SNMP_FAILURE)
            {
                AST_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *) "Unable to set the Encap value, "
                              "Check if the port is trunk or not");
                return;
            }
        }

        AST_UNLOCK ();
        WebnmUnRegisterLock (pHttp);

        IssProcessPvrstPortConfPageGet (pHttp);
    }

}

/*******************************************************************************
 *  Function Name : IssProcessPvrstPortConfPageGet
 *  Description   : This function processes the get request for the Pvrst
 *                  Port configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessPvrstPortConfPageGet (tHttp * pHttp)
{
    INT4                i4CurrPort = 0;
    INT4                i4NextPort = 0;
    INT4                i4RetVal = 0;
    INT4                i4OutCome = SNMP_FAILURE;
    UINT4               u4Temp = 0;
    UINT4               u4InContext = 0;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;
    UINT1               u1OperStatus = 0;
    INT1               *piIfName = NULL;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

    piIfName = (INT1 *) &au1IfName[0];

    pHttp->i4Write = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    WebnmRegisterLock (pHttp, AstLock, AstUnLock);
    AST_LOCK ();

    i4OutCome = (INT4) (nmhGetFirstIndexFsMIFuturePvrstPortTable (&i4NextPort));
    if (i4OutCome == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;

    while (i4OutCome == SNMP_SUCCESS)
    {
        pHttp->i4Write = (INT4) u4Temp;
        VcmGetContextInfoFromIfIndex ((UINT4) i4NextPort, &u4InContext,
                                      &u2LocalPort);
        CfaCliGetIfName ((UINT4) i4NextPort, piIfName);
        /* Get the Context for the Session */
        i4OutCome = WebnmApiGetContextId (pHttp, &u4ContextId);
        if (i4OutCome != ENM_SUCCESS)
        {
            AST_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) ("Context Selection Failed \n"));
            return;
        }
        /* Get the port oper status for PVRST */
        AstL2IwfGetPortOperStatus (STP_MODULE, (UINT2) i4NextPort,
                                   &u1OperStatus);

        if ((u1OperStatus != CFA_IF_DOWN) && (u4ContextId == u4InContext))
        {
            /* Port Type */
            STRCPY (pHttp->au1KeyString, "PORT_NO_KEY_NAME");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", (CHR1 *) piIfName);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            /* Module status value */
            STRCPY (pHttp->au1KeyString, "MOD_STATUS_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (nmhGetFsMIPvrstPortEnabledStatus (i4NextPort, &i4RetVal)
                == SNMP_SUCCESS)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            }
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            /* P2P value */
            STRCPY (pHttp->au1KeyString, "P_2_P_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (nmhGetFsMIPvrstPortAdminPointToPoint (i4NextPort, &i4RetVal)
                == SNMP_SUCCESS)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            }
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            /* Root guard value */
            STRCPY (pHttp->au1KeyString, "ROOT_GAURD_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (nmhGetFsMIPvrstRootGuard (i4NextPort, &i4RetVal)
                == SNMP_SUCCESS)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            }

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            /* Bpdu guard value */
            STRCPY (pHttp->au1KeyString, "BPDU_GAURD_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (nmhGetFsMIPvrstBpduGuard (i4NextPort, &i4RetVal)
                == SNMP_SUCCESS)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            }
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            /* Encap value */
            STRCPY (pHttp->au1KeyString, "ENCAP_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (nmhGetFsMIPvrstEncapType (i4NextPort, &i4RetVal) ==
                SNMP_SUCCESS)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            }

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            STRCPY (pHttp->au1KeyString, "ROW_STATUS_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (nmhGetFsMIPvrstPortRowStatus (i4NextPort, &i4RetVal) ==
                SNMP_SUCCESS)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            }

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            /* Loop guard value */
            STRCPY (pHttp->au1KeyString, "LOOP_GAURD_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (nmhGetFsMIPvrstPortLoopGuard (i4NextPort, &i4RetVal)
                == SNMP_SUCCESS)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            }

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

        }
        else
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        }

        i4CurrPort = i4NextPort;
        i4OutCome =
            nmhGetNextIndexFsMIFuturePvrstPortTable (i4CurrPort, &i4NextPort);
    }

    AST_UNLOCK ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/************************************************************************
 *  Function Name : IssPrintPvrstCurrentContext
 *  Description   : This function sends the current context configured
 *                  in the system.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *************************************************************************/
VOID
IssPrintPvrstCurrentContext (tHttp * pHttp)
{
    INT4                i4ContextId = 0;
    INT4                i4OutCome = ENM_FAILURE;

    STRCPY (pHttp->au1KeyString, "CONTEXT_ID_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    i4OutCome = WebnmApiGetContextId (pHttp, (UINT4 *) &i4ContextId);
    if (i4OutCome == ENM_SUCCESS)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4ContextId);
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
}

/*******************************************************************************
*  Function Name : IssProcessPvrstBasicConfPage
*  Description   : This function processes the HTTP request coming for the
*                  Pvrst basic configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None.
*******************************************************************************/
VOID
IssProcessPvrstBasicConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessPvrstBasicConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessPvrstBasicConfPageSet (pHttp);
    }
    return;
}

/*******************************************************************************
*  Function Name : IssProcessPvrstBasicConfPageSet
*  Description   : This function processes the HTTP set request coming for the
*                  Pvrst basic configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None.
*******************************************************************************/
VOID
IssProcessPvrstBasicConfPageSet (tHttp * pHttp)
{
    INT4                i4ContextId = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4SystemStatus = 0;
    INT4                i4ModuleStatus = 0;
    INT4                i4RetVal = 0;
    INT4                i4Val = 0;
    STRCPY (pHttp->au1Name, "CONTEXT_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4ContextId = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "SYSTEM_STATUS");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4SystemStatus = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "MODULE_STATUS");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4ModuleStatus = ATOI (pHttp->au1Value);

    WebnmRegisterLock (pHttp, AstLock, AstUnLock);
    AST_LOCK ();
    if (i4SystemStatus != 0)
    {
        if (nmhTestv2FsMIPvrstSystemControl (&u4ErrorCode,
                                             i4ContextId,
                                             i4SystemStatus) == SNMP_FAILURE)
        {
            AST_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set System "
                          "Control");
            return;
        }
        if (nmhSetFsMIPvrstSystemControl (i4ContextId, i4SystemStatus) ==
            SNMP_FAILURE)
        {
            AST_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set System "
                          "Control");
            return;
        }
    }
    i4Val = 0;
    nmhGetFsMIPvrstSystemControl (i4ContextId, &i4Val);
    /* Set only when System Control Status status is start */
    if (i4Val == 1)
    {
        i4RetVal = 0;
        nmhGetFsMIPvrstModuleStatus (i4ContextId, &i4RetVal);
        if ((i4ModuleStatus != 0) && (i4RetVal != i4ModuleStatus))
        {
            /* Test if this can be set */
            if (nmhTestv2FsMIPvrstModuleStatus (&u4ErrorCode,
                                                i4ContextId,
                                                i4ModuleStatus) == SNMP_FAILURE)
            {
                AST_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Unable to set Module "
                              "Status");
                return;
            }
            /* Then Set the status */
            if (nmhSetFsMIPvrstModuleStatus (i4ContextId,
                                             i4ModuleStatus) == SNMP_FAILURE)
            {
                AST_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Unable to set Module "
                              "Status");
                return;
            }
        }
    }
    AST_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    IssProcessPvrstBasicConfPageGet (pHttp);
}

/*******************************************************************************
 *  Function Name : IssProcessPvrstBasicConfPageGet
 *  Description   : This function processes the get request for the Pvrst
 *                  basic configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessPvrstBasicConfPageGet (tHttp * pHttp)
{
    INT4                i4CurrContextId = 0;
    INT4                i4NextContextId = 0;
    UINT4               u4Temp = 0;
    INT4                i4RetVal = 0;
    INT4                i4Val = 0;
    INT4                i4OutCome = SNMP_FAILURE;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    WebnmRegisterLock (pHttp, AstLock, AstUnLock);
    AST_LOCK ();

    u4Temp = (UINT4) pHttp->i4Write;
    i4OutCome = nmhGetFirstIndexFsMIFuturePvrstTable (&i4CurrContextId);
    while (i4OutCome == SNMP_SUCCESS)
    {
        pHttp->i4Write = (INT4) u4Temp;

        /* Context Id */
        STRCPY (pHttp->au1KeyString, "CONTEXT_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4CurrContextId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* System Control status */
        STRCPY (pHttp->au1KeyString, "SYSTEM_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsMIPvrstSystemControl (i4CurrContextId, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Module status */
        STRCPY (pHttp->au1KeyString, "MODULE_STATUS_KEY");
        nmhGetFsMIPvrstSystemControl (i4CurrContextId, &i4Val);
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4Val == 1)
        {
            i4RetVal = 0;
            nmhGetFsMIPvrstModuleStatus (i4CurrContextId, &i4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        else
        {
            /* If system control is shutdown, make it disabled */
            i4RetVal = 2;
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        i4OutCome =
            nmhGetNextIndexFsMIFuturePvrstTable (i4CurrContextId,
                                                 &i4NextContextId);

        if (i4OutCome == SNMP_SUCCESS)
        {
            i4CurrContextId = i4NextContextId;
        }
    }

    AST_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

#endif
#endif
