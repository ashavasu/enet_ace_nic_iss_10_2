/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: rmapweb.c,v 1.21 2014/08/28 11:16:24 siva Exp $
 *
 * Description: Routines for ISS web Module
 *******************************************************************/
#include "webiss.h"
#include "isshttp.h"
#include "cfa.h"
#include "rtm.h"
#include "rmap.h"
#include "rmapweb.h"
#include "webinc.h"
#include "utilipvx.h"

/*******************************************************************************
*  Function Name : IssProcessRMapCreationPage
*  Description   : This function processes the request coming for the Route Map
*                  Creation Page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssProcessRMapCreationPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessRMapCreationGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessRMapCreationSet (pHttp);
    }
}

/*******************************************************************************
*  Function Name : IssProcessRMapMatchPage
*  Description   : This function processes the request coming for the Route Map
*                  Match Page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssProcessRMapMatchPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessRMapMatchTableGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessRMapMatchTableSet (pHttp);
    }
}

/*******************************************************************************
*  Function Name : IssProcessRMapMatchPage
*  Description   : This function processes the request coming for the Route Map
*                  Set Page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssProcessRMapSetPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessRMapSetTableGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessRMapSetTableSet (pHttp);
    }
}

/*******************************************************************************
*  Function Name : IssProcessRMapIpPrefixPage
*  Description   : This function processes the request coming for the 
*                  Ip Prefix list page
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssProcessRMapIpPrefixPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessRMapIpPrefixPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessRMapIpPrefixPageSet (pHttp);
    }
}

/*******************************************************************************
*  Function Name : IssProcessRMapCreationGet
*  Description   : This function gets and displays all the Route Maps created
*                  and the associated access to Route Map and enables the user
*                  to delete or modify any Route Map.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/

VOID
IssProcessRMapCreationGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    UINT1               au1RouteMapName[RMAP_MAX_NAME_LEN + 4] = { 0 };

    UINT1              *pu1String = NULL;

    UINT4               u4Temp = RMAP_ZERO;
    UINT4               u4RouteMapSeqNo = RMAP_ONE;

    INT4                i4Access = RMAP_ZERO;
    INT4                i4RowStatus = RMAP_ZERO;
    INT4                i4IsIpPrefix = 0;

    RouteMapName.pu1_OctetList = au1RouteMapName;
    RouteMapName.i4_Length = 0;

    pu1String = au1RouteMapName;

    pHttp->i4Write = 0;

    /* Display the web page till the Table Flag */
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    /* Register the Route Map Lock with Webnm Module and Take the Lock */
    WebnmRegisterLock (pHttp, RMapLockWrite, RMapUnLockWrite);
    RMapLockWrite ();

    if (nmhGetFirstIndexFsRMapTable (&RouteMapName, &u4RouteMapSeqNo)
        == SNMP_FAILURE)
    {
        RMapUnLockWrite ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;

    do                            /* START OF DO..WHILE */
    {
        if (nmhGetFsRMapAccess (&RouteMapName, u4RouteMapSeqNo,
                                &i4Access) == SNMP_FAILURE)
        {
            continue;
        }

        if (nmhGetFsRMapRowStatus (&RouteMapName, u4RouteMapSeqNo,
                                   &i4RowStatus) == SNMP_FAILURE)
        {
            continue;
        }

        if (RMAP_ACTIVE != i4RowStatus)
        {
            continue;
        }
        if (nmhGetFsRMapIsIpPrefixList (&RouteMapName, u4RouteMapSeqNo,
                                        &i4IsIpPrefix) == SNMP_FAILURE)
        {
            continue;
        }
        if (i4IsIpPrefix == TRUE)
        {
            continue;
        }

        pHttp->i4Write = (INT4) u4Temp;

        STRCPY (pu1String, RouteMapName.pu1_OctetList);
        STRCPY (pHttp->au1KeyString, "fsRouteMapName_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, RMAP_MAX_NAME_LEN + 1, "%s",
                  pu1String);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRouteMapSeqNum_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 3, "%d", u4RouteMapSeqNo);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRouteMapAccess_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 2, "%d", i4Access);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    while (nmhGetNextIndexFsRMapTable (&RouteMapName, &RouteMapName, u4RouteMapSeqNo, &u4RouteMapSeqNo) != SNMP_FAILURE);    /* END OF DO..WHILE */

    /* All Entries Displayed. So, Release the Lock. Unregister the Lock from
     * Webnm Module. */
    RMapUnLockWrite ();
    WebnmUnRegisterLock (pHttp);
    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    /* Display all the information in the html page after the Table stop flag */
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/******************************************************************************
*  Function Name : IssProcessRMapCreationSet
*  Description   : This function gets the user input from the web page and
*                  creates Route Map with access required.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
******************************************************************************/

VOID
IssProcessRMapCreationSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    UINT1               au1RouteMapName[RMAP_MAX_NAME_LEN + 4] = { 0 };

    UINT4               u4ErrorCode = RMAP_ZERO;
    UINT4               u4RouteMapSeqNo = RMAP_ONE;

    INT4                i4Access = RMAP_ZERO;
    INT4                i4PrevAccess = RMAP_ZERO;
    INT4                i4RowStatus = RMAP_ZERO;

    BOOL1               b1NeedWebUpdate = FALSE;

    RouteMapName.pu1_OctetList = au1RouteMapName;
    RouteMapName.i4_Length = RMAP_ZERO;

    /* Get the Route Map Name, Sequence Number and Access to be set
     * from the form it */
    STRCPY (pHttp->au1Name, "fsRouteMapName");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        RouteMapName.i4_Length = (INT4) STRLEN (pHttp->au1Value);
        if (RouteMapName.i4_Length >= (RMAP_MAX_NAME_LEN + 4))
        {
            RouteMapName.i4_Length = RMAP_MAX_NAME_LEN + 3;
        }
        MEMCPY (RouteMapName.pu1_OctetList, pHttp->au1Value,
                RouteMapName.i4_Length);
        RouteMapName.pu1_OctetList[RouteMapName.i4_Length] = 0;
    }

    STRCPY (pHttp->au1Name, "fsRouteMapSeqNum");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4RouteMapSeqNo = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "fsRouteMapAccess");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4Access = (INT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "fsRouteMapRowStatus");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4RowStatus = (INT4) ATOI (pHttp->au1Value);
    }

    if (6 == i4RowStatus)
    {
        /*DESTROY BRANCH */
        RMapLockWrite ();
        if (nmhGetFsRMapRowStatus (&RouteMapName,
                                   u4RouteMapSeqNo,
                                   &i4RowStatus) == SNMP_SUCCESS)
        {
            if (nmhSetFsRMapRowStatus (&RouteMapName,
                                       u4RouteMapSeqNo,
                                       RMAP_NOT_IN_SERVICE) == SNMP_SUCCESS)
            {
                if (nmhSetFsRMapRowStatus (&RouteMapName,
                                           u4RouteMapSeqNo,
                                           RMAP_DESTROY) == SNMP_SUCCESS)
                {
                    b1NeedWebUpdate = TRUE;
                }
            }
        }
        RMapUnLockWrite ();
        if (FALSE == b1NeedWebUpdate)
        {
            IssSendError (pHttp, (CONST INT1 *) "Unable to delete");
        }
    }
    else if (2 == i4RowStatus)
    {
        /*MODIFY BRANCH */
        RMapLockWrite ();

        if (nmhGetFsRMapAccess (&RouteMapName,
                                u4RouteMapSeqNo, &i4PrevAccess) == SNMP_SUCCESS)
        {
            if (nmhSetFsRMapRowStatus (&RouteMapName,
                                       u4RouteMapSeqNo,
                                       RMAP_NOT_IN_SERVICE) != SNMP_FAILURE)
            {
                if (nmhSetFsRMapAccess (&RouteMapName,
                                        u4RouteMapSeqNo,
                                        i4Access) == SNMP_SUCCESS)
                {
                    if (nmhSetFsRMapRowStatus (&RouteMapName,
                                               u4RouteMapSeqNo,
                                               RMAP_ACTIVE) != SNMP_FAILURE)
                    {
                        b1NeedWebUpdate = TRUE;
                    }
                }
            }
        }
        if (FALSE == b1NeedWebUpdate)
        {
            IssSendError (pHttp, (CONST INT1 *) "Unable to modify");
        }
        RMapUnLockWrite ();
    }
    else
    {
        /*ADD BRANCH */
        RMapLockWrite ();

        if (nmhTestv2FsRMapRowStatus (&u4ErrorCode,
                                      &RouteMapName,
                                      u4RouteMapSeqNo,
                                      RMAP_CREATE_AND_WAIT) != SNMP_FAILURE)
        {
            if (nmhSetFsRMapRowStatus (&RouteMapName,
                                       u4RouteMapSeqNo,
                                       RMAP_CREATE_AND_WAIT) != SNMP_FAILURE)
            {
                if (nmhSetFsRMapAccess (&RouteMapName,
                                        u4RouteMapSeqNo,
                                        i4Access) == SNMP_SUCCESS)
                {
                    if (nmhTestv2FsRMapRowStatus (&u4ErrorCode,
                                                  &RouteMapName,
                                                  u4RouteMapSeqNo,
                                                  RMAP_ACTIVE) != SNMP_FAILURE)
                    {
                        if (nmhSetFsRMapRowStatus (&RouteMapName,
                                                   u4RouteMapSeqNo,
                                                   RMAP_ACTIVE) != SNMP_FAILURE)
                        {
                            b1NeedWebUpdate = TRUE;
                        }
                    }
                }
            }
        }
        if (FALSE == b1NeedWebUpdate)
        {
            IssSendError (pHttp, (CONST INT1 *) "Unable to add");
        }
        RMapUnLockWrite ();
    }

    if (TRUE == b1NeedWebUpdate)
    {
        IssProcessRMapCreationGet (pHttp);
    }
    return;
}

/*******************************************************************************
*  Function Name : IssProcessRMapMatchTableGet
*  Description   : This function gets and displays all the entries created in
*                  Route Map Match Table and enables the user to delete
*                  any entry in Route Map Match Table.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssProcessRMapMatchTableGet (tHttp * pHttp)
{
    /*RouteMap name variable storage */
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    UINT1               au1RouteMapName[RMAP_MAX_NAME_LEN + 4] = { 0 };
    UINT1               au1RouteMapNameCache[RMAP_MAX_NAME_LEN + 4] = { 0 };
    UINT1              *pu1String = NULL;

    /*to HTML processing current position keeper */
    UINT4               u4Temp = RMAP_ZERO;

    /*MatchTable columns */
    UINT4               u4RouteMapSeqNo = RMAP_ONE;

    INT4                i4RowStatus = RMAP_ZERO;

    UINT1               au4DestInetAddress[IPVX_IPV6_ADDR_LEN];
    UINT1               au4SourceInetAddress[IPVX_IPV6_ADDR_LEN];
    UINT1               au4NextHopInetAddr[IPVX_IPV6_ADDR_LEN];

    INT4                i4DestInetType;
    tSNMP_OCTET_STRING_TYPE DestInetAddress;
    UINT4               u4DestInetPrefix;

    INT4                i4SourceInetType;
    tSNMP_OCTET_STRING_TYPE SourceInetAddress;
    UINT4               u4SourceInetPrefix;

    INT4                i4NextHopInetType;
    tSNMP_OCTET_STRING_TYPE NextHopInetAddr;

    INT4                i4Interface;
    INT4                i4Metric;
    UINT4               u4Tag;
    INT4                i4MetricType;
    INT4                i4RouteType;
    UINT4               u4ASPathTag;
    UINT4               u4Community;
    INT4                i4LocalPref;
    INT4                i4Origin;

    UINT1               au1DestInetAddress[50];
    UINT1               au1SourceInetAddress[50];
    UINT1               au1NextHopInetAddr[50];

    UINT4               u4DestInetAddress;
    UINT4               u4SourceInetAddress;
    UINT4               u4NextHopInetAddr;
    UINT1              *pu1IfName = NULL;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT4                i4IsIpPrefix = 0;
    UINT4               u4Len = 0;

    tUtlInAddr          IpAddrTemp;
    RouteMapName.pu1_OctetList = au1RouteMapName;
    RouteMapName.i4_Length = 0;

    DestInetAddress.pu1_OctetList = au4DestInetAddress;
    DestInetAddress.i4_Length = 0;

    SourceInetAddress.pu1_OctetList = au4SourceInetAddress;
    SourceInetAddress.i4_Length = 0;

    NextHopInetAddr.pu1_OctetList = au4NextHopInetAddr;
    NextHopInetAddr.i4_Length = 0;

    pu1String = au1RouteMapName;

/*FILLING OUT MATCH CREATION COMBOBOXES BY RMAP NAMES AND SEQ NUMBERS*/

    WebnmRegisterLock (pHttp, RMapLockWrite, RMapUnLockWrite);
    RMapLockWrite ();

    if (nmhGetFirstIndexFsRMapTable (&RouteMapName, &u4RouteMapSeqNo) ==
        SNMP_FAILURE)
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        RMapUnLockWrite ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    pHttp->i4Write = 0;

    STRCPY (pHttp->au1KeyString, "<! RMAP_MAP_NAME>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {

        if (nmhGetFsRMapRowStatus (&RouteMapName,
                                   u4RouteMapSeqNo,
                                   &i4RowStatus) == SNMP_FAILURE)
        {
            continue;
        }

        if (RMAP_ACTIVE != i4RowStatus)
        {
            continue;
        }
        if (nmhGetFsRMapIsIpPrefixList (&RouteMapName, u4RouteMapSeqNo,
                                        &i4IsIpPrefix) == SNMP_FAILURE)
        {
            continue;
        }
        if (i4IsIpPrefix == TRUE)
        {
            continue;
        }

        if (0 != STRCMP (au1RouteMapNameCache, au1RouteMapName))
        {
            SNPRINTF ((CHR1 *) pHttp->au1DataString, 60,
                      "<option value = \"%s\">%s \n",
                      RouteMapName.pu1_OctetList, RouteMapName.pu1_OctetList);

            STRCPY (au1RouteMapNameCache, au1RouteMapName);

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
        }

    }
    while (nmhGetNextIndexFsRMapTable (&RouteMapName,
                                       &RouteMapName,
                                       u4RouteMapSeqNo,
                                       &u4RouteMapSeqNo) != SNMP_FAILURE);

    RMapUnLockWrite ();
    WebnmUnRegisterLock (pHttp);

/*FILLING OUT TABLE OF EXISTING MATCH NODES*/

    /* Register the Route Map Lock with Webnm Module and Take the Lock */
    WebnmRegisterLock (pHttp, RMapLockWrite, RMapUnLockWrite);
    RMapLockWrite ();

    /* Display all the L3 Interfaces available in the drop down box. */
    IssPrintIpInterfaces (pHttp, 0, 0);

    /* Display the web page till the Table Flag */
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    if (nmhGetFirstIndexFsRMapMatchTable (&RouteMapName,
                                          &u4RouteMapSeqNo,
                                          &i4DestInetType,
                                          &DestInetAddress,
                                          &u4DestInetPrefix,
                                          &i4SourceInetType,
                                          &SourceInetAddress,
                                          &u4SourceInetPrefix,
                                          &i4NextHopInetType,
                                          &NextHopInetAddr,
                                          &i4Interface,
                                          &i4Metric,
                                          &u4Tag,
                                          &i4MetricType,
                                          &i4RouteType,
                                          &u4ASPathTag,
                                          &u4Community,
                                          &i4LocalPref,
                                          &i4Origin) == SNMP_FAILURE)
    {
        RMapUnLockWrite ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;

    do
    {
        if (nmhGetFsRMapIsIpPrefixList (&RouteMapName, u4RouteMapSeqNo,
                                        &i4IsIpPrefix) == SNMP_FAILURE)
        {
            continue;
        }
        if (i4IsIpPrefix == TRUE)
        {
            continue;
        }

        if (IPVX_ADDR_FMLY_IPV4 == i4DestInetType)
        {

            u4DestInetAddress =
                OSIX_NTOHL (*(UINT4 *) (VOID *)
                            (DestInetAddress.pu1_OctetList));
            MEMCPY (&IpAddrTemp, &u4DestInetAddress, sizeof (tUtlInAddr));
            u4Len =
                ((STRLEN (INET_NTOA (IpAddrTemp)) <
                  (sizeof (au1DestInetAddress) -
                   1)) ? STRLEN (INET_NTOA (IpAddrTemp))
                 : (sizeof (au1DestInetAddress) - 1));

            STRNCPY (au1DestInetAddress, INET_NTOA (IpAddrTemp), u4Len);
            au1DestInetAddress[u4Len] = '\0';
        }
        else if (IPVX_ADDR_FMLY_IPV6 == i4DestInetType)    /* IPVX_ADDR_FMLY_IPV6 */
        {
            u4Len =
                (((STRLEN
                   (INET_NTOA6
                    (*(tUtlIn6Addr *) DestInetAddress.pu1_OctetList))) <
                  (sizeof (au1DestInetAddress) -
                   1)) ? STRLEN (STRLEN (INET_NTOA6 (*(tUtlIn6Addr *)
                                                     DestInetAddress.
                                                     pu1_OctetList)))
                 : (sizeof (au1DestInetAddress) - 1));

            STRNCPY (au1DestInetAddress,
                     INET_NTOA6 (*(tUtlIn6Addr *) DestInetAddress.
                                 pu1_OctetList), u4Len);
            au1DestInetAddress[u4Len] = '\0';

        }
        else
        {
            au1DestInetAddress[0] = 0;
        }

        if (IPVX_ADDR_FMLY_IPV4 == i4SourceInetType)
        {
            u4SourceInetAddress =
                OSIX_NTOHL (*(UINT4 *) (VOID *)
                            (SourceInetAddress.pu1_OctetList));
            MEMCPY (&IpAddrTemp, &u4SourceInetAddress, sizeof (tUtlInAddr));
            STRNCPY (au1SourceInetAddress, INET_NTOA (IpAddrTemp),
                     sizeof (au1SourceInetAddress));
        }
        else if (IPVX_ADDR_FMLY_IPV6 == i4SourceInetType)    /* IPVX_ADDR_FMLY_IPV6 */
        {
            STRNCPY (au1SourceInetAddress,
                     INET_NTOA6 (*(tUtlIn6Addr *) SourceInetAddress.
                                 pu1_OctetList), sizeof (au1SourceInetAddress));
        }
        else
        {
            au1SourceInetAddress[0] = 0;
        }

        if (IPVX_ADDR_FMLY_IPV4 == i4NextHopInetType)
        {
            u4NextHopInetAddr =
                OSIX_NTOHL (*(UINT4 *) (VOID *)
                            (NextHopInetAddr.pu1_OctetList));
            MEMCPY (&IpAddrTemp, &u4NextHopInetAddr, sizeof (tUtlInAddr));
            STRNCPY (au1NextHopInetAddr, INET_NTOA (IpAddrTemp),
                     sizeof (au1NextHopInetAddr));
        }
        else if (IPVX_ADDR_FMLY_IPV6 == i4NextHopInetType)    /* IPVX_ADDR_FMLY_IPV6 */
        {
            STRNCPY (au1NextHopInetAddr,
                     INET_NTOA6 (*(tUtlIn6Addr *) NextHopInetAddr.
                                 pu1_OctetList), sizeof (au1NextHopInetAddr));
        }
        else
        {
            au1NextHopInetAddr[0] = 0;
        }

        pHttp->i4Write = (INT4) u4Temp;

        STRCPY (pHttp->au1KeyString, "fsRMapName_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, RMAP_MAX_NAME_LEN + 1, "%s",
                  pu1String);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapSeqNum_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 2, "%d", u4RouteMapSeqNo);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapMatchDestInetType_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 2, "%d", i4DestInetType);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapMatchDestInetAddress_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 50, "%s", au1DestInetAddress);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapMatchDestInetPrefix_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 3, "%d", u4DestInetPrefix);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapMatchSourceInetType_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 2, "%d", i4SourceInetType);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapMatchSourceInetAddress_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 50, "%s",
                  au1SourceInetAddress);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapMatchSourceInetPrefix_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 3, "%d", u4SourceInetPrefix);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapMatchNextHopInetType_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 2, "%d", i4NextHopInetType);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapMatchNextHopInetAddr_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 50, "%s", au1NextHopInetAddr);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "IPINTERFACES_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        pu1IfName = &au1IfName[0];

        if (i4Interface != RMAP_ZERO)
        {
            CfaCliGetIfName ((UINT4) i4Interface, (INT1 *) pu1IfName);
            SNPRINTF ((CHR1 *) pHttp->au1DataString,
                      STRLEN (pu1IfName) + 1, "%s", pu1IfName);
        }
        else
        {
            SNPRINTF ((CHR1 *) pHttp->au1DataString, 2, "0");
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapMatchMetric_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 11, "%d", i4Metric);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapMatchTag_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 11, "%d", u4Tag);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapMatchMetricType_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 2, "%d", i4MetricType);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapMatchRouteType_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 2, "%d", i4RouteType);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapMatchASPathTag_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 11, "%d", u4ASPathTag);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapMatchCommunity_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 2, "%d", u4Community);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapMatchLocalPref_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 11, "%d", i4LocalPref);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapMatchOrigin_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 2, "%d", i4Origin);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRmapInterface_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Interface);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    while (nmhGetNextIndexFsRMapMatchTable (&RouteMapName,
                                            &RouteMapName,
                                            u4RouteMapSeqNo,
                                            &u4RouteMapSeqNo,
                                            i4DestInetType,
                                            &i4DestInetType,
                                            &DestInetAddress,
                                            &DestInetAddress,
                                            u4DestInetPrefix,
                                            &u4DestInetPrefix,
                                            i4SourceInetType,
                                            &i4SourceInetType,
                                            &SourceInetAddress,
                                            &SourceInetAddress,
                                            u4SourceInetPrefix,
                                            &u4SourceInetPrefix,
                                            i4NextHopInetType,
                                            &i4NextHopInetType,
                                            &NextHopInetAddr,
                                            &NextHopInetAddr,
                                            i4Interface,
                                            &i4Interface,
                                            i4Metric,
                                            &i4Metric,
                                            u4Tag,
                                            &u4Tag,
                                            i4MetricType,
                                            &i4MetricType,
                                            i4RouteType,
                                            &i4RouteType,
                                            u4ASPathTag,
                                            &u4ASPathTag,
                                            u4Community,
                                            &u4Community,
                                            i4LocalPref,
                                            &i4LocalPref,
                                            i4Origin,
                                            &i4Origin) != SNMP_FAILURE);

    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    /* Display all the information in the html page after the Table stop flag */
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    RMapUnLockWrite ();
    WebnmUnRegisterLock (pHttp);

    return;
}

/******************************************************************************
*  Function Name : IssProcessRMapMatchTableSet
*  Description   : This function gets the user input from the web page and
*                  creates an entry in Route Map Match Table.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
******************************************************************************/
INT1               *
str_replace (INT1 *income, INT1 *find, INT1 *replace)
{
    static INT1         buf[4096];
    INT1               *p;
    if (!(p = (INT1 *) STRSTR (income, find)))
        return income;
    STRNCPY (buf, income, p - income);
    buf[p - income] = 0;
    SPRINTF ((CHR1 *) (buf + (p - income)), "%s%s", (CHR1 *) replace,
             (CHR1 *) (p + STRLEN (find)));
    return str_replace (buf, find, replace);
}

VOID
IssProcessRMapMatchTableSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    UINT1               au1RouteMapName[RMAP_MAX_NAME_LEN + 4] = { 0 };
    UINT4               u4RouteMapSeqNo = RMAP_ONE;

    INT4                i4RowStatus = RMAP_ZERO;

    UINT1               au4DestInetAddress[IPVX_IPV6_ADDR_LEN];
    UINT1               au4SourceInetAddress[IPVX_IPV6_ADDR_LEN];
    UINT1               au4NextHopInetAddr[IPVX_IPV6_ADDR_LEN];

    INT4                i4DestInetType = 0;
    tSNMP_OCTET_STRING_TYPE DestInetAddress;
    UINT4               u4DestInetPrefix = 0;

    INT4                i4SourceInetType = 0;
    tSNMP_OCTET_STRING_TYPE SourceInetAddress;
    UINT4               u4SourceInetPrefix = 0;

    INT4                i4NextHopInetType = 0;
    tSNMP_OCTET_STRING_TYPE NextHopInetAddr;

    INT4                i4Interface = 0;
    INT4                i4Metric = 0;
    UINT4               u4Tag = 0;
    INT4                i4MetricType = 0;
    INT4                i4RouteType = 0;
    UINT4               u4ASPathTag = 0;
    UINT4               u4Community = 0;
    UINT4               u4Retval = 0;
    INT4                i4LocalPref = 0;
    INT4                i4Origin = 0;

    UINT1               au1DestInetAddress[70];
    UINT1               au1SourceInetAddress[70];
    UINT1               au1NextHopInetAddr[70];

    UINT4               u4DestInetAddress;
    UINT4               u4SourceInetAddress;
    UINT4               u4NextHopInetAddr;

    UINT4               u4IfaceIndex = RMAP_ZERO;
    BOOL1               b1NeedWebUpdate = FALSE;

    UINT4               u4ErrorCode = SNMP_ERR_NO_CREATION;

    RouteMapName.pu1_OctetList = au1RouteMapName;
    RouteMapName.i4_Length = 0;

    DestInetAddress.pu1_OctetList = au4DestInetAddress;
    DestInetAddress.i4_Length = 0;

    SourceInetAddress.pu1_OctetList = au4SourceInetAddress;
    SourceInetAddress.i4_Length = 0;

    NextHopInetAddr.pu1_OctetList = au4NextHopInetAddr;
    NextHopInetAddr.i4_Length = 0;

    STRCPY (pHttp->au1Name, "fsRMapName");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        RouteMapName.i4_Length = (INT4) STRLEN (pHttp->au1Value);
        if (RouteMapName.i4_Length >= (RMAP_MAX_NAME_LEN + 4))
        {
            RouteMapName.i4_Length = RMAP_MAX_NAME_LEN + 3;
        }
        MEMCPY (RouteMapName.pu1_OctetList, pHttp->au1Value,
                RouteMapName.i4_Length);
        RouteMapName.pu1_OctetList[RouteMapName.i4_Length] = 0;
    }

    STRCPY (pHttp->au1Name, "fsRMapSeqNum");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4RouteMapSeqNo = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "fsRMapMatchDestInetType");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4DestInetType = (INT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "fsRMapMatchDestInetAddress");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        if (STRLEN (pHttp->au1Value) >= 70)
        {
            pHttp->au1Value[69] = '\0';
        }
        STRCPY (au1DestInetAddress, pHttp->au1Value);

        if (IPVX_ADDR_FMLY_IPV4 == i4DestInetType)
        {
            u4DestInetAddress = INET_ADDR (au1DestInetAddress);
            u4DestInetAddress = OSIX_HTONL (u4DestInetAddress);
            DestInetAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
            MEMCPY (DestInetAddress.pu1_OctetList, &u4DestInetAddress,
                    DestInetAddress.i4_Length);
        }
        else if (IPVX_ADDR_FMLY_IPV6 == i4DestInetType)    /* IPVX_ADDR_FMLY_IPV6 */
        {
            STRNCPY (au1DestInetAddress,
                     str_replace ((INT1 *) au1DestInetAddress, (INT1 *) "%3A",
                                  (INT1 *) ":"), sizeof (au1DestInetAddress));
            INET_ATON6 (au1DestInetAddress, DestInetAddress.pu1_OctetList);
            DestInetAddress.i4_Length = IPVX_IPV6_ADDR_LEN;
        }
        else
        {
            DestInetAddress.i4_Length = 0;
            DestInetAddress.pu1_OctetList[0] = 0;
        }
    }

    STRCPY (pHttp->au1Name, "fsRMapMatchDestInetPrefix");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4DestInetPrefix = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "fsRMapMatchSourceInetType");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4SourceInetType = (INT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "fsRMapMatchSourceInetAddress");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        if (STRLEN (pHttp->au1Value) >= 70)
        {
            pHttp->au1Value[69] = '\0';
        }
        STRCPY (au1SourceInetAddress, pHttp->au1Value);

        if (IPVX_ADDR_FMLY_IPV4 == i4SourceInetType)
        {
            u4SourceInetAddress = INET_ADDR (au1SourceInetAddress);
            u4SourceInetAddress = OSIX_HTONL (u4SourceInetAddress);
            SourceInetAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
            MEMCPY (SourceInetAddress.pu1_OctetList, &u4SourceInetAddress,
                    SourceInetAddress.i4_Length);
        }
        else if (IPVX_ADDR_FMLY_IPV6 == i4SourceInetType)    /* IPVX_ADDR_FMLY_IPV6 */
        {
            STRNCPY (au1SourceInetAddress,
                     str_replace ((INT1 *) au1SourceInetAddress, (INT1 *) "%3A",
                                  (INT1 *) ":"), sizeof (au1SourceInetAddress));
            INET_ATON6 (au1SourceInetAddress, SourceInetAddress.pu1_OctetList);
            SourceInetAddress.i4_Length = IPVX_IPV6_ADDR_LEN;
        }
        else
        {
            SourceInetAddress.i4_Length = 0;
            SourceInetAddress.pu1_OctetList[0] = 0;
        }
    }

    STRCPY (pHttp->au1Name, "fsRMapMatchSourceInetPrefix");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4SourceInetPrefix = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "fsRMapMatchNextHopInetType");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4NextHopInetType = (INT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "fsRMapMatchNextHopInetAddr");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        if (STRLEN (pHttp->au1Value) >= 70)
        {
            pHttp->au1Value[69] = '\0';
        }
        STRCPY (au1NextHopInetAddr, pHttp->au1Value);

        if (IPVX_ADDR_FMLY_IPV4 == i4NextHopInetType)
        {
            u4NextHopInetAddr = INET_ADDR (au1NextHopInetAddr);
            u4NextHopInetAddr = OSIX_HTONL (u4NextHopInetAddr);
            NextHopInetAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
            MEMCPY (NextHopInetAddr.pu1_OctetList, &u4NextHopInetAddr,
                    NextHopInetAddr.i4_Length);
        }
        else if (IPVX_ADDR_FMLY_IPV6 == i4NextHopInetType)    /* IPVX_ADDR_FMLY_IPV6 */
        {
            STRNCPY (au1NextHopInetAddr,
                     str_replace ((INT1 *) au1NextHopInetAddr, (INT1 *) "%3A",
                                  (INT1 *) ":"), sizeof (au1NextHopInetAddr));
            INET_ATON6 (au1NextHopInetAddr, NextHopInetAddr.pu1_OctetList);
            NextHopInetAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
        }
        else
        {
            NextHopInetAddr.i4_Length = 0;
            NextHopInetAddr.pu1_OctetList[0] = 0;
        }
    }

    STRCPY (pHttp->au1Name, "fsRMapMatchMetric");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4Metric = (INT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "fsRMapMatchTag");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4Tag = (UINT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "fsRMapMatchMetricType");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4MetricType = (INT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "fsRMapMatchRouteType");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4RouteType = (INT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "fsRMapMatchASPathTag");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4ASPathTag = (UINT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "fsRMapMatchCommunity");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4Community = (UINT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "fsRMapMatchLocalPref");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4LocalPref = (INT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "fsRMapMatchOrigin");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4Origin = (INT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "fsRMapMatchRowStatus");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4RowStatus = (INT4) ATOI (pHttp->au1Value);
    }

    if (i4RowStatus == 1)
    {
        STRCPY (pHttp->au1Name, "fsRMapMatchInterface");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4Interface = (INT4) ATOI (pHttp->au1Value);
        }

    }
    else
    {
        STRCPY (pHttp->au1Name, "fsRMapMatchInterface");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            CFA_LOCK ();
            u4Retval =
                CfaGetInterfaceIndexFromName (pHttp->au1Value, &u4IfaceIndex);
            UNUSED_PARAM (u4Retval);
            CFA_UNLOCK ();
            i4Interface = (INT4) u4IfaceIndex;
        }

    }
    RMapLockWrite ();

#define PARAMLIST   &RouteMapName,\
                    u4RouteMapSeqNo,\
                    i4DestInetType,\
                    &DestInetAddress,\
                    u4DestInetPrefix,\
                    i4SourceInetType,\
                    &SourceInetAddress,\
                    u4SourceInetPrefix,\
                    i4NextHopInetType,\
                    &NextHopInetAddr,\
                    i4Interface,\
                    i4Metric,\
                    u4Tag,\
                    i4MetricType,\
                    i4RouteType,\
                    u4ASPathTag,\
                    u4Community,\
                    i4LocalPref,\
                    i4Origin

    if (1 == i4RowStatus)
    {
        /*ADD BRANCH */
        if (nmhGetFsRMapMatchRowStatus (PARAMLIST,
                                        &i4RowStatus) == SNMP_FAILURE)
        {
            if (nmhTestv2FsRMapMatchRowStatus (&u4ErrorCode,
                                               PARAMLIST,
                                               RMAP_CREATE_AND_WAIT) ==
                SNMP_SUCCESS)
            {
                if (nmhSetFsRMapMatchRowStatus (&RouteMapName, u4RouteMapSeqNo,
                                                i4DestInetType,
                                                &DestInetAddress,
                                                u4DestInetPrefix,
                                                i4SourceInetType,
                                                &SourceInetAddress,
                                                u4SourceInetPrefix,
                                                i4NextHopInetType,
                                                &NextHopInetAddr, i4Interface,
                                                i4Metric, u4Tag, i4MetricType,
                                                i4RouteType, u4ASPathTag,
                                                u4Community, i4LocalPref,
                                                i4Origin,
                                                RMAP_CREATE_AND_WAIT) ==
                    SNMP_SUCCESS)
                {
                    if (nmhTestv2FsRMapMatchRowStatus (&u4ErrorCode,
                                                       PARAMLIST,
                                                       RMAP_ACTIVE) ==
                        SNMP_SUCCESS)
                    {
                        if (nmhSetFsRMapMatchRowStatus (&RouteMapName,
                                                        u4RouteMapSeqNo,
                                                        i4DestInetType,
                                                        &DestInetAddress,
                                                        u4DestInetPrefix,
                                                        i4SourceInetType,
                                                        &SourceInetAddress,
                                                        u4SourceInetPrefix,
                                                        i4NextHopInetType,
                                                        &NextHopInetAddr,
                                                        i4Interface, i4Metric,
                                                        u4Tag, i4MetricType,
                                                        i4RouteType,
                                                        u4ASPathTag,
                                                        u4Community,
                                                        i4LocalPref, i4Origin,
                                                        RMAP_ACTIVE) ==
                            SNMP_SUCCESS)
                        {
                            b1NeedWebUpdate = TRUE;
                        }
                    }
                }
            }
            if (FALSE == b1NeedWebUpdate)
            {
                IssSendError (pHttp, (CONST INT1 *) "Unable to add");
            }
        }
    }
    else if (6 == i4RowStatus)
    {
        /*DESTROY BRANCH */
        if (nmhGetFsRMapMatchRowStatus (PARAMLIST,
                                        &i4RowStatus) == SNMP_SUCCESS)
        {
            if (nmhSetFsRMapMatchRowStatus (&RouteMapName, u4RouteMapSeqNo,
                                            i4DestInetType, &DestInetAddress,
                                            u4DestInetPrefix, i4SourceInetType,
                                            &SourceInetAddress,
                                            u4SourceInetPrefix,
                                            i4NextHopInetType, &NextHopInetAddr,
                                            i4Interface, i4Metric, u4Tag,
                                            i4MetricType, i4RouteType,
                                            u4ASPathTag, u4Community,
                                            i4LocalPref, i4Origin,
                                            RMAP_NOT_IN_SERVICE) ==
                SNMP_SUCCESS)
            {
                if (nmhSetFsRMapMatchRowStatus (&RouteMapName, u4RouteMapSeqNo,
                                                i4DestInetType,
                                                &DestInetAddress,
                                                u4DestInetPrefix,
                                                i4SourceInetType,
                                                &SourceInetAddress,
                                                u4SourceInetPrefix,
                                                i4NextHopInetType,
                                                &NextHopInetAddr, i4Interface,
                                                i4Metric, u4Tag, i4MetricType,
                                                i4RouteType, u4ASPathTag,
                                                u4Community, i4LocalPref,
                                                i4Origin,
                                                RMAP_DESTROY) == SNMP_SUCCESS)
                {
                    b1NeedWebUpdate = TRUE;
                }
            }

            if (FALSE == b1NeedWebUpdate)
            {
                IssSendError (pHttp, (CONST INT1 *) "Unable to delete");
            }
        }
    }

    RMapUnLockWrite ();

    if (TRUE == b1NeedWebUpdate)
    {
        IssProcessRMapMatchTableGet (pHttp);
    }

    return;
}

/*******************************************************************************
*  Function Name : IssProcessRMapSetTableGet
*  Description   : This function gets and displays all the entries created in
*                  Route Map Match Table and enables the user to delete
*                  any entry in Route Map Match Table.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssProcessRMapSetTableGet (tHttp * pHttp)
{

    /*RouteMap name variable storage */
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    UINT1               au1RouteMapName[RMAP_MAX_NAME_LEN + 4] = { 0 };
    UINT1               au1RouteMapNameCache[RMAP_MAX_NAME_LEN + 4] = { 0 };
    UINT1              *pu1String = NULL;

    /*to HTML processing current position keeper */
    UINT4               u4Temp = RMAP_ZERO;

    /*MatchTable columns */
    UINT4               u4RouteMapSeqNo = RMAP_ONE;

    INT4                i4RowStatus = RMAP_ZERO;

    UINT1               au4NextHopInetAddr[IPVX_IPV6_ADDR_LEN];

    INT4                i4NextHopInetType;
    tSNMP_OCTET_STRING_TYPE NextHopInetAddr;

    INT4                i4Interface;
    INT4                i4Metric;
    UINT4               u4Tag;
    INT4                i4RouteType;

    UINT4               u4ASPathTag;
    UINT4               u4Community;
    INT4                i4LocalPref;
    INT4                i4Origin;

    UINT4               u4Weight;
    INT4                i4EnableAutoTag;
    INT4                i4Level;
    UINT4               u4ExtCommId;
    UINT4               u4ExtCommCost;

    UINT1               au1NextHopInetAddr[50];

    UINT4               u4NextHopInetAddr;

    tUtlInAddr          IpAddrTemp;
    INT4                i4IsIpPrefix = 0;
    UINT1              *pu1IfName = NULL;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    RouteMapName.pu1_OctetList = au1RouteMapName;
    RouteMapName.i4_Length = 0;

    NextHopInetAddr.pu1_OctetList = au4NextHopInetAddr;
    NextHopInetAddr.i4_Length = 0;

    pu1String = au1RouteMapName;

/*FILLING OUT SET CREATION COMBOBOXES BY RMAP NAMES AND SEQ NUMBERS*/
    /* Display all the L3 Interfaces available in the drop down box. */
    WebnmRegisterLock (pHttp, RMapLockWrite, RMapUnLockWrite);
    RMapLockWrite ();

    if (nmhGetFirstIndexFsRMapTable (&RouteMapName, &u4RouteMapSeqNo) ==
        SNMP_FAILURE)
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        RMapUnLockWrite ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    pHttp->i4Write = 0;

    STRCPY (pHttp->au1KeyString, "<! RMAP_MAP_NAME>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {

        if (nmhGetFsRMapRowStatus (&RouteMapName,
                                   u4RouteMapSeqNo,
                                   &i4RowStatus) == SNMP_FAILURE)
        {
            continue;
        }

        if (RMAP_ACTIVE != i4RowStatus)
        {
            continue;
        }
        if (nmhGetFsRMapIsIpPrefixList (&RouteMapName, u4RouteMapSeqNo,
                                        &i4IsIpPrefix) == SNMP_FAILURE)
        {
            continue;
        }
        if (i4IsIpPrefix == TRUE)
        {
            continue;
        }

        if (0 != STRCMP (au1RouteMapNameCache, au1RouteMapName))
        {
            SNPRINTF ((CHR1 *) pHttp->au1DataString, 60,
                      "<option value = \"%s\">%s \n",
                      RouteMapName.pu1_OctetList, RouteMapName.pu1_OctetList);

            STRCPY (au1RouteMapNameCache, au1RouteMapName);

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
        }

    }
    while (nmhGetNextIndexFsRMapTable (&RouteMapName,
                                       &RouteMapName,
                                       u4RouteMapSeqNo,
                                       &u4RouteMapSeqNo) != SNMP_FAILURE);

/*FILLING OUT TABLE OF EXISTING SET NODES*/

    /* Register the Route Map Lock with Webnm Module and Take the Lock */
    /* Display the web page till the Table Flag */
    IssPrintIpInterfaces (pHttp, 0, 0);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    if (nmhGetFirstIndexFsRMapSetTable (&RouteMapName, &u4RouteMapSeqNo) ==
        SNMP_FAILURE)
    {
        RMapUnLockWrite ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;

    do
    {
        if (nmhGetFsRMapSetRowStatus (&RouteMapName,
                                      u4RouteMapSeqNo,
                                      &i4RowStatus) == SNMP_FAILURE)
        {
            continue;
        }

        if (RMAP_ACTIVE != i4RowStatus)
        {
            continue;
        }

        if (nmhGetFsRMapSetNextHopInetType (&RouteMapName,
                                            u4RouteMapSeqNo,
                                            &i4NextHopInetType) == SNMP_FAILURE)
        {
            continue;
        }
        if (nmhGetFsRMapSetNextHopInetAddr (&RouteMapName,
                                            u4RouteMapSeqNo,
                                            &NextHopInetAddr) == SNMP_FAILURE)
        {
            continue;
        }
        else
        {
            if (IPVX_ADDR_FMLY_IPV4 == i4NextHopInetType)
            {
                u4NextHopInetAddr =
                    OSIX_NTOHL (*(UINT4 *) (VOID *)
                                (NextHopInetAddr.pu1_OctetList));
                MEMCPY (&IpAddrTemp, &u4NextHopInetAddr, sizeof (tUtlInAddr));
                STRCPY (au1NextHopInetAddr, INET_NTOA (IpAddrTemp));
            }
            else if (IPVX_ADDR_FMLY_IPV6 == i4NextHopInetType)    /* IPVX_ADDR_FMLY_IPV6 */
            {
                STRCPY (au1NextHopInetAddr,
                        INET_NTOA6 (*(tUtlIn6Addr *) NextHopInetAddr.
                                    pu1_OctetList));
            }
            else
            {
                au1NextHopInetAddr[0] = 0;
            }
        }
        if (nmhGetFsRMapSetInterface (&RouteMapName,
                                      u4RouteMapSeqNo,
                                      &i4Interface) == SNMP_FAILURE)
        {
            continue;
        }
        if (nmhGetFsRMapSetMetric (&RouteMapName,
                                   u4RouteMapSeqNo, &i4Metric) == SNMP_FAILURE)
        {
            continue;
        }
        if (nmhGetFsRMapSetTag (&RouteMapName,
                                u4RouteMapSeqNo, &u4Tag) == SNMP_FAILURE)
        {
            continue;
        }
        if (nmhGetFsRMapSetRouteType (&RouteMapName,
                                      u4RouteMapSeqNo,
                                      &i4RouteType) == SNMP_FAILURE)
        {
            continue;
        }
        if (nmhGetFsRMapSetASPathTag (&RouteMapName,
                                      u4RouteMapSeqNo,
                                      &u4ASPathTag) == SNMP_FAILURE)
        {
            continue;
        }
        if (nmhGetFsRMapSetCommunity (&RouteMapName,
                                      u4RouteMapSeqNo,
                                      &u4Community) == SNMP_FAILURE)
        {
            continue;
        }
        if (nmhGetFsRMapSetLocalPref (&RouteMapName,
                                      u4RouteMapSeqNo,
                                      &i4LocalPref) == SNMP_FAILURE)
        {
            continue;
        }
        if (nmhGetFsRMapSetOrigin (&RouteMapName,
                                   u4RouteMapSeqNo, &i4Origin) == SNMP_FAILURE)
        {
            continue;
        }
        if (nmhGetFsRMapSetWeight (&RouteMapName,
                                   u4RouteMapSeqNo, &u4Weight) == SNMP_FAILURE)
        {
            continue;
        }
        if (nmhGetFsRMapSetEnableAutoTag (&RouteMapName,
                                          u4RouteMapSeqNo,
                                          &i4EnableAutoTag) == SNMP_FAILURE)
        {
            continue;
        }
        if (nmhGetFsRMapSetLevel (&RouteMapName,
                                  u4RouteMapSeqNo, &i4Level) == SNMP_FAILURE)
        {
            continue;
        }

        if (nmhGetFsRMapSetExtCommId (&RouteMapName,
                                      u4RouteMapSeqNo,
                                      &u4ExtCommId) == SNMP_FAILURE)
        {
            continue;
        }

        if (nmhGetFsRMapSetExtCommCost (&RouteMapName,
                                        u4RouteMapSeqNo,
                                        &u4ExtCommCost) == SNMP_FAILURE)
        {
            continue;
        }

        pHttp->i4Write = (INT4) u4Temp;

        STRCPY (pHttp->au1KeyString, "fsRMapName_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, RMAP_MAX_NAME_LEN + 1, "%s",
                  pu1String);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapSeqNum_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 2, "%d", u4RouteMapSeqNo);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapSetNextHopInetType_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 2, "%d", i4NextHopInetType);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapSetNextHopInetAddr_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 50, "%s", au1NextHopInetAddr);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "IPINTERFACES_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        pu1IfName = &au1IfName[0];

        if (i4Interface != RMAP_ZERO)
        {
            CfaCliGetIfName ((UINT4) i4Interface, (INT1 *) pu1IfName);
            SNPRINTF ((CHR1 *) pHttp->au1DataString,
                      STRLEN (pu1IfName) + 1, "%s", pu1IfName);
        }
        else
        {
            SNPRINTF ((CHR1 *) pHttp->au1DataString, 2, "0");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapSetMetric_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 11, "%d", i4Metric);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapSetTag_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 11, "%d", u4Tag);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapSetRouteType_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 2, "%d", i4RouteType);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapSetASPathTag_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 11, "%d", u4ASPathTag);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapSetCommunity_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 2, "%d", u4Community);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapSetLocalPref_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 11, "%d", i4LocalPref);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapSetOrigin_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 2, "%d", i4Origin);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapSetWeight_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 6, "%d", u4Weight);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapSetEnableAutoTag_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 2, "%d", i4EnableAutoTag);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapSetLevel_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 2, "%d", i4Level);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapSetExtCommId_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4ExtCommId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsRMapSetExtCommCost_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4ExtCommCost);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

    }
    while (nmhGetNextIndexFsRMapSetTable (&RouteMapName,
                                          &RouteMapName,
                                          u4RouteMapSeqNo,
                                          &u4RouteMapSeqNo) != SNMP_FAILURE);

    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    /* Display all the information in the html page after the Table stop flag */
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    RMapUnLockWrite ();
    WebnmUnRegisterLock (pHttp);

    return;
}

/******************************************************************************
*  Function Name : IssProcessRMapSetTableSet
*  Description   : This function gets the user input from the web page and
*                  creates an entry in Route Map Set Table.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
******************************************************************************/
VOID
IssProcessRMapSetTableSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    UINT1               au1RouteMapName[RMAP_MAX_NAME_LEN + 4] = { 0 };
    UINT4               u4RouteMapSeqNo = RMAP_ONE;

    INT4                i4RowStatus = RMAP_ZERO;

    INT4                i4NextHopInetType;

    UINT1               au4NextHopInetAddr[IPVX_IPV6_ADDR_LEN];
    UINT1               au1NextHopInetAddr[70];

    tSNMP_OCTET_STRING_TYPE NextHopInetAddr;
    UINT4               u4NextHopInetAddr;

    INT4                i4Interface = 0;
    INT4                i4Metric = 0;
    UINT4               u4Tag = 0;
    INT4                i4RouteType = 0;

    UINT4               u4ASPathTag = 0;
    UINT4               u4Community = 0;
    INT4                i4LocalPref = 0;
    INT4                i4Origin = 0;

    UINT4               u4Weight = 0;
    INT4                i4EnableAutoTag = 0;
    INT4                i4Level = 0;

    BOOL1               b1NeedWebUpdate = FALSE;
    UINT2               u4ExtCommId = 0;
    UINT4               u4ExtCommCost = 0;
    UINT4               u4ErrorCode = SNMP_ERR_NO_CREATION;

    INT4                iIndex = 0;

#define SETTESTV2FUNAMOUNT  16
    static tSetTestv2FsRMapSetStruct arSetTestv2Functions[SETTESTV2FUNAMOUNT] = {
        {{(tpnmhSetFsRMapSetINT) nmhSetFsRMapSetNextHopInetType},
         {(tpnmhTestv2FsRMapSetINT) nmhTestv2FsRMapSetNextHopInetType}},
        {{(tpnmhSetFsRMapSetINT) nmhSetFsRMapSetNextHopInetAddr},
         {(tpnmhTestv2FsRMapSetINT) nmhTestv2FsRMapSetNextHopInetAddr}},
        {{(tpnmhSetFsRMapSetINT) nmhSetFsRMapSetInterface},
         {(tpnmhTestv2FsRMapSetINT) nmhTestv2FsRMapSetInterface}},
        {{(tpnmhSetFsRMapSetINT) nmhSetFsRMapSetMetric},
         {(tpnmhTestv2FsRMapSetINT) nmhTestv2FsRMapSetMetric}},
        {{(tpnmhSetFsRMapSetINT) nmhSetFsRMapSetTag},
         {(tpnmhTestv2FsRMapSetINT) nmhTestv2FsRMapSetTag}},
        {{(tpnmhSetFsRMapSetINT) nmhSetFsRMapSetRouteType},
         {(tpnmhTestv2FsRMapSetINT) nmhTestv2FsRMapSetRouteType}},
        {{(tpnmhSetFsRMapSetINT) nmhSetFsRMapSetASPathTag},
         {(tpnmhTestv2FsRMapSetINT) nmhTestv2FsRMapSetASPathTag}},
        {{(tpnmhSetFsRMapSetINT) nmhSetFsRMapSetCommunity},
         {(tpnmhTestv2FsRMapSetINT) nmhTestv2FsRMapSetCommunity}},
        {{(tpnmhSetFsRMapSetINT) nmhSetFsRMapSetLocalPref},
         {(tpnmhTestv2FsRMapSetINT) nmhTestv2FsRMapSetLocalPref}},
        {{(tpnmhSetFsRMapSetINT) nmhSetFsRMapSetOrigin},
         {(tpnmhTestv2FsRMapSetINT) nmhTestv2FsRMapSetOrigin}},
        {{(tpnmhSetFsRMapSetINT) nmhSetFsRMapSetWeight},
         {(tpnmhTestv2FsRMapSetINT) nmhTestv2FsRMapSetWeight}},
        {{(tpnmhSetFsRMapSetINT) nmhSetFsRMapSetEnableAutoTag},
         {(tpnmhTestv2FsRMapSetINT) nmhTestv2FsRMapSetEnableAutoTag}},
        {{(tpnmhSetFsRMapSetINT) nmhSetFsRMapSetLevel},
         {(tpnmhTestv2FsRMapSetINT) nmhTestv2FsRMapSetLevel}},
        {{(tpnmhSetFsRMapSetINT) nmhSetFsRMapSetExtCommId},
         {(tpnmhTestv2FsRMapSetINT) nmhTestv2FsRMapSetExtCommId}},
        {{(tpnmhSetFsRMapSetINT) nmhSetFsRMapSetExtCommCost},
         {(tpnmhTestv2FsRMapSetINT) nmhTestv2FsRMapSetExtCommCost}},
        {{(tpnmhSetFsRMapSetINT) nmhSetFsRMapSetRowStatus},
         {(tpnmhTestv2FsRMapSetINT) nmhTestv2FsRMapSetRowStatus}}
    };

    static void        *arSetTestv2Arguments[SETTESTV2FUNAMOUNT];

    arSetTestv2Arguments[0] = &i4NextHopInetType;
    arSetTestv2Arguments[1] = &NextHopInetAddr;
    arSetTestv2Arguments[2] = &i4Interface;
    arSetTestv2Arguments[3] = &i4Metric;
    arSetTestv2Arguments[4] = &u4Tag;
    arSetTestv2Arguments[5] = &i4RouteType;
    arSetTestv2Arguments[6] = &u4ASPathTag;
    arSetTestv2Arguments[7] = &u4Community;
    arSetTestv2Arguments[8] = &i4LocalPref;
    arSetTestv2Arguments[9] = &i4Origin;
    arSetTestv2Arguments[10] = &u4Weight;
    arSetTestv2Arguments[11] = &i4EnableAutoTag;
    arSetTestv2Arguments[12] = &i4Level;
    arSetTestv2Arguments[13] = &u4ExtCommId;
    arSetTestv2Arguments[14] = &u4ExtCommCost;
    arSetTestv2Arguments[15] = &i4RowStatus;

    RouteMapName.pu1_OctetList = au1RouteMapName;
    RouteMapName.i4_Length = 0;

    NextHopInetAddr.pu1_OctetList = au4NextHopInetAddr;
    NextHopInetAddr.i4_Length = 0;

    STRCPY (pHttp->au1Name, "fsRMapName");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        RouteMapName.i4_Length = (INT4) STRLEN (pHttp->au1Value);
        if (RouteMapName.i4_Length >= (RMAP_MAX_NAME_LEN + 4))
        {
            RouteMapName.i4_Length = RMAP_MAX_NAME_LEN + 3;
        }
        MEMCPY (RouteMapName.pu1_OctetList, pHttp->au1Value,
                RouteMapName.i4_Length);
        RouteMapName.pu1_OctetList[RouteMapName.i4_Length] = 0;
    }

    STRCPY (pHttp->au1Name, "fsRMapSeqNum");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4RouteMapSeqNo = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "fsRMapSetNextHopInetType");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4NextHopInetType = (INT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "fsRMapSetNextHopInetAddr");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        if (STRLEN (pHttp->au1Value) >= 70)
        {
            pHttp->au1Value[69] = '\0';
        }
        STRCPY (au1NextHopInetAddr, pHttp->au1Value);

        if (IPVX_ADDR_FMLY_IPV4 == i4NextHopInetType)
        {
            u4NextHopInetAddr = INET_ADDR (au1NextHopInetAddr);
            u4NextHopInetAddr = OSIX_HTONL (u4NextHopInetAddr);
            NextHopInetAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
            MEMCPY (NextHopInetAddr.pu1_OctetList, &u4NextHopInetAddr,
                    NextHopInetAddr.i4_Length);
        }
        else if (IPVX_ADDR_FMLY_IPV6 == i4NextHopInetType)    /* IPVX_ADDR_FMLY_IPV6 */
        {
            STRNCPY (au1NextHopInetAddr,
                     str_replace ((INT1 *) au1NextHopInetAddr, (INT1 *) "%3A",
                                  (INT1 *) ":"), sizeof (au1NextHopInetAddr));
            INET_ATON6 (au1NextHopInetAddr, NextHopInetAddr.pu1_OctetList);
            NextHopInetAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
        }
        else
        {
            NextHopInetAddr.i4_Length = 0;
            NextHopInetAddr.pu1_OctetList[0] = 0;
        }
    }

    STRCPY (pHttp->au1Name, "fsRMapSetMetric");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4Metric = (INT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "fsRMapSetTag");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4Tag = (UINT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "fsRMapSetRouteType");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4RouteType = (INT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "fsRMapSetASPathTag");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4ASPathTag = (UINT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "fsRMapSetCommunity");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4Community = (UINT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "fsRMapSetLocalPref");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4LocalPref = (INT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "fsRMapSetOrigin");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4Origin = (INT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "fsRMapSetWeight");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4Weight = (UINT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "fsRMapSetEnableAutoTag");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4EnableAutoTag = (INT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "fsRMapSetLevel");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4Level = (INT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "fsRMapSetExtCommId");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4ExtCommId = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "fsRMapSetExtCommCost");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4ExtCommCost = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "fsRMapSetRowStatus");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4RowStatus = (INT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "fsRMapSetInterface");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4Interface = (INT4) ATOI (pHttp->au1Value);
    }
    RMapLockWrite ();
    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Add") == 0)
    {
        /*ADD BRANCH */
        b1NeedWebUpdate = TRUE;
        u4ErrorCode = RMAP_ZERO;

        if (nmhGetFsRMapSetRowStatus
            (&RouteMapName, u4RouteMapSeqNo, &i4RowStatus) == SNMP_SUCCESS)
        {
            if (nmhSetFsRMapSetRowStatus (&RouteMapName,
                                          u4RouteMapSeqNo,
                                          RMAP_NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                b1NeedWebUpdate = FALSE;
            }
        }
        else
        {
            if (nmhSetFsRMapSetRowStatus (&RouteMapName,
                                          u4RouteMapSeqNo,
                                          RMAP_CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                b1NeedWebUpdate = FALSE;
            }
        }

        if (TRUE == b1NeedWebUpdate)
        {
            for (iIndex = 0; iIndex < SETTESTV2FUNAMOUNT - 1; ++iIndex)
            {
                if (1 != iIndex)
                {
                    if ((iIndex == 6) && (u4ASPathTag == 0))
                    {
                        continue;
                    }
                    if ((iIndex == 7) && (u4Community == 0))
                    {
                        continue;
                    }
                    if ((iIndex == 10) && (u4Weight == 0))
                    {
                        continue;
                    }
                    if ((iIndex == 13) && (u4ExtCommId == 0))
                    {
                        continue;
                    }
                    if (arSetTestv2Functions[iIndex].
                        pnmhTestv2FsRMapSet.
                        pnmhTestv2FsRMapSetINT (&u4ErrorCode,
                                                &RouteMapName,
                                                u4RouteMapSeqNo,
                                                *(INT4 *)
                                                arSetTestv2Arguments[iIndex]) ==
                        SNMP_FAILURE)
                    {
                        b1NeedWebUpdate = FALSE;
                    }

                    if (arSetTestv2Functions[iIndex].
                        pnmhSetFsRMapSet.
                        pnmhSetFsRMapSetINT (&RouteMapName,
                                             u4RouteMapSeqNo,
                                             *(INT4 *)
                                             arSetTestv2Arguments[iIndex]) ==
                        SNMP_FAILURE)
                    {
                        b1NeedWebUpdate = FALSE;
                    }
                }
                else
                {
                    if (NextHopInetAddr.i4_Length != 0)
                    {
                        if (arSetTestv2Functions[iIndex].
                            pnmhTestv2FsRMapSet.
                            pnmhTestv2FsRMapSetOST (&u4ErrorCode,
                                                    &RouteMapName,
                                                    u4RouteMapSeqNo,
                                                    (tSNMP_OCTET_STRING_TYPE *)
                                                    arSetTestv2Arguments
                                                    [iIndex]) == SNMP_FAILURE)
                        {
                            b1NeedWebUpdate = FALSE;
                        }

                        if (arSetTestv2Functions[iIndex].
                            pnmhSetFsRMapSet.
                            pnmhSetFsRMapSetOST (&RouteMapName,
                                                 u4RouteMapSeqNo,
                                                 (tSNMP_OCTET_STRING_TYPE *)
                                                 arSetTestv2Arguments[iIndex])
                            == SNMP_FAILURE)
                        {
                            b1NeedWebUpdate = FALSE;
                        }
                    }
                }
            }
        }

        if (TRUE == b1NeedWebUpdate)
        {
            if (nmhSetFsRMapSetRowStatus (&RouteMapName,
                                          u4RouteMapSeqNo,
                                          RMAP_ACTIVE) == SNMP_FAILURE)
            {
                b1NeedWebUpdate = FALSE;
            }
        }

        if (TRUE != b1NeedWebUpdate)
        {
            IssSendError (pHttp, (CONST INT1 *) "Unable to add");
        }
    }
    else if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {
        for (iIndex = 0; iIndex < SETTESTV2FUNAMOUNT - 1; ++iIndex)
        {
            if (arSetTestv2Functions[iIndex].
                pnmhSetFsRMapSet.
                pnmhSetFsRMapSetINT (&RouteMapName,
                                     u4RouteMapSeqNo, 0) != SNMP_FAILURE)
            {
                b1NeedWebUpdate |= TRUE;
            }
        }

        if (nmhSetFsRMapSetRowStatus (&RouteMapName,
                                      u4RouteMapSeqNo,
                                      RMAP_NOT_IN_SERVICE) != SNMP_FAILURE)
        {
            b1NeedWebUpdate |= TRUE;
        }
        else
        {
            b1NeedWebUpdate = FALSE;
        }

        if (FALSE == b1NeedWebUpdate)
        {
            IssSendError (pHttp, (CONST INT1 *) "Unable to delete");
        }
    }

    RMapUnLockWrite ();

    if (TRUE == b1NeedWebUpdate)
    {
        IssProcessRMapSetTableGet (pHttp);
    }

    return;
}

/*******************************************************************************
*  Function Name : IssProcessRMapIpPrefixPageGet
*  Description   : This function gets and displays all the entries created in
*                  Ip Prefix list table and enables the user to delete
*                  any entry in Ip Prefix list Table.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssProcessRMapIpPrefixPageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE IpPrefixName;
    tSNMP_OCTET_STRING_TYPE DestInetAddressOct;
    tSNMP_OCTET_STRING_TYPE TmpOct;
    tUtlInAddr          IpAddrTmp;
    UINT1               au1IpPrefixName[RMAP_MAX_NAME_LEN + 4] = { 0 };
    UINT1               au1Tmp[50] = { 0 };
    UINT1               au1InetAddress[50];
    UINT1               au1Address[50];
    UINT4               u4SeqNo = 0;
    UINT4               u4MinLen = 0;
    UINT4               u4MaxLen = 0;
    UINT4               u4PrefixLen = 0;
    UINT4               u4TmpVal = 0;
    UINT4               u4InetAddress = 0;
    INT4                i4Temp = 0;
    INT4                i4DestInetType = 0;
    INT4                i4RowStatus = 0;
    INT4                i4IsIpPrefix = 0;
    INT4                i4Access = 0;
    INT4                i4TmpVal = 0;

    MEMSET (&IpPrefixName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&DestInetAddressOct, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&TmpOct, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&IpAddrTmp, 0, sizeof (tUtlInAddr));

    MEMSET (au1Address, 0, sizeof (au1Address));
    MEMSET (au1InetAddress, 0, sizeof (au1InetAddress));

    IpPrefixName.pu1_OctetList = au1IpPrefixName;
    DestInetAddressOct.pu1_OctetList = au1Address;
    TmpOct.pu1_OctetList = au1Tmp;

    WebnmRegisterLock (pHttp, RMapLockWrite, RMapUnLockWrite);
    RMapLockWrite ();

    pHttp->i4Write = 0;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    if (nmhGetFirstIndexFsRMapTable (&IpPrefixName, &u4SeqNo) == SNMP_FAILURE)
    {
        RMapUnLockWrite ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    i4Temp = pHttp->i4Write;
    do
    {
        if (nmhGetFsRMapRowStatus (&IpPrefixName,
                                   u4SeqNo, &i4RowStatus) == SNMP_FAILURE)
        {
            continue;
        }

        if (RMAP_ACTIVE != i4RowStatus)
        {
            continue;
        }

        if (nmhGetFsRMapIsIpPrefixList (&IpPrefixName, u4SeqNo, &i4IsIpPrefix)
            == SNMP_FAILURE)
        {
            continue;
        }
        if (i4IsIpPrefix != TRUE)
        {
            continue;
        }
        if (nmhGetFsRMapAccess (&IpPrefixName, u4SeqNo, &i4Access)
            == SNMP_FAILURE)
        {
            RMapUnLockWrite ();
            WebnmUnRegisterLock (pHttp);
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            return;
        }
        /* Only the first match rule present in the route map node is used for 
         * IP Prefix filter. So the variables are re-initialized every time while fetching
         * the first match rule (Ip prefix filter) from the RMAP node*/
        MEMSET (au1Address, 0, sizeof (au1Address));
        DestInetAddressOct.i4_Length = 0;
        i4DestInetType = 0;
        u4PrefixLen = 0;
        if (nmhGetNextIndexFsRMapMatchTable (&IpPrefixName, &IpPrefixName,
                                             u4SeqNo, &u4SeqNo,
                                             i4DestInetType, &i4DestInetType,
                                             &DestInetAddressOct,
                                             &DestInetAddressOct, u4PrefixLen,
                                             &u4PrefixLen, i4TmpVal, &i4TmpVal,
                                             &TmpOct, &TmpOct, u4TmpVal,
                                             &u4TmpVal, i4TmpVal, &i4TmpVal,
                                             &TmpOct, &TmpOct, i4TmpVal,
                                             &i4TmpVal, i4TmpVal, &i4TmpVal,
                                             u4TmpVal, &u4TmpVal, i4TmpVal,
                                             &i4TmpVal, i4TmpVal, &i4TmpVal,
                                             u4TmpVal, &u4TmpVal, u4TmpVal,
                                             &u4TmpVal, i4TmpVal, &i4TmpVal,
                                             i4TmpVal,
                                             &i4TmpVal) == SNMP_FAILURE)
        {
            RMapUnLockWrite ();
            WebnmUnRegisterLock (pHttp);
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            return;
        }

        if (nmhGetFsRMapMatchDestMinPrefixLen (&IpPrefixName, u4SeqNo,
                                               i4DestInetType,
                                               &DestInetAddressOct,
                                               u4PrefixLen, 0, 0, 0, 0, 0,
                                               0, 0, 0, 0, 0, 0, 0, 0, 0,
                                               &u4MinLen) == SNMP_FAILURE)
        {
            RMapUnLockWrite ();
            WebnmUnRegisterLock (pHttp);
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            return;
        }

        if (nmhGetFsRMapMatchDestMaxPrefixLen (&IpPrefixName, u4SeqNo,
                                               i4DestInetType,
                                               &DestInetAddressOct,
                                               u4PrefixLen, 0, 0, 0, 0, 0,
                                               0, 0, 0, 0, 0, 0, 0, 0, 0,
                                               &u4MaxLen) == SNMP_FAILURE)
        {
            RMapUnLockWrite ();
            WebnmUnRegisterLock (pHttp);
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            return;
        }

        pHttp->i4Write = i4Temp;

        STRCPY (pHttp->au1KeyString, "fsIpPrefixName_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 30, "%s",
                  IpPrefixName.pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsIpPrefixSeqNum_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4SeqNo);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsIpPrefixAddress_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (IPVX_ADDR_FMLY_IPV4 == i4DestInetType)
        {
            u4InetAddress = (*(UINT4 *) (VOID *)
                             (DestInetAddressOct.pu1_OctetList));
            MEMCPY (&IpAddrTmp, &u4InetAddress, sizeof (UINT4));
            STRCPY (au1InetAddress, INET_NTOA (IpAddrTmp));
        }
        else if (IPVX_ADDR_FMLY_IPV6 == i4DestInetType)    /* IPVX_ADDR_FMLY_IPV6 */
        {
            STRCPY (au1InetAddress,
                    INET_NTOA6 (*(tUtlIn6Addr *) DestInetAddressOct.
                                pu1_OctetList));
        }
        else
        {
            au1InetAddress[0] = 0;
        }

        SNPRINTF ((CHR1 *) pHttp->au1DataString, 40, "%s", au1InetAddress);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsIpPrefixLength_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4PrefixLen);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsIpPrefixMinLength_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4MinLen);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsIpPrefixMaxLength_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4MaxLen);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsIpPrefixAction_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4Access == RMAP_PERMIT)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Permit");
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Deny");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsIpPrefixAddrType_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4DestInetType);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    while (nmhGetNextIndexFsRMapTable (&IpPrefixName,
                                       &IpPrefixName,
                                       u4SeqNo, &u4SeqNo) != SNMP_FAILURE);

    RMapUnLockWrite ();
    WebnmUnRegisterLock (pHttp);
    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/******************************************************************************
*  Function Name : IssProcessRMapIpPrefixPageSet
*  Description   : This function gets the user input from the web page and
*                  creates an entry in IP Prefix Table.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
******************************************************************************/
VOID
IssProcessRMapIpPrefixPageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE IpPrefixName;
    tSNMP_OCTET_STRING_TYPE InetAddress;
    UINT1               au1IpPrefixName[RMAP_MAX_NAME_LEN + 4] = { 0 };
    UINT1               au1IpAddress[IPVX_IPV6_ADDR_LEN];
    UINT1               au1Address[70];
    UINT4               u4SeqNo = 0;
    UINT4               u4PrefixLen = 0;
    UINT4               u4Address = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u1Access = 0;
    UINT4               u1MinLen = 0;
    UINT4               u1MaxLen = 0;
    INT4                i4AddrType = 0;

    MEMSET (au1IpPrefixName, 0, sizeof (au1IpPrefixName));
    MEMSET (au1Address, 0, sizeof (au1Address));
    MEMSET (au1IpAddress, 0, sizeof (au1IpAddress));
    IpPrefixName.pu1_OctetList = au1IpPrefixName;
    IpPrefixName.i4_Length = 0;

    InetAddress.pu1_OctetList = au1IpAddress;
    InetAddress.i4_Length = 0;

    WebnmRegisterLock (pHttp, RMapLockWrite, RMapUnLockWrite);
    RMapLockWrite ();

    STRCPY (pHttp->au1Name, "fsIpPrefixName");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        IpPrefixName.i4_Length = (INT4) STRLEN (pHttp->au1Value);
        if (IpPrefixName.i4_Length >= (RMAP_MAX_NAME_LEN + 4))
        {
            IpPrefixName.i4_Length = RMAP_MAX_NAME_LEN + 3;
        }
        MEMCPY (IpPrefixName.pu1_OctetList, pHttp->au1Value,
                IpPrefixName.i4_Length);
        IpPrefixName.pu1_OctetList[IpPrefixName.i4_Length] = 0;
    }

    STRCPY (pHttp->au1Name, "fsIpPrefixSeqNum");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4SeqNo = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "fsIpPrefixAddrType");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4AddrType = (INT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "fsIpPrefixAddress");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        if (STRLEN (pHttp->au1Value) >= 70)
        {
            pHttp->au1Value[69] = '\0';
        }
        STRCPY (au1Address, pHttp->au1Value);

        if (IPVX_ADDR_FMLY_IPV4 == i4AddrType)
        {
            u4Address = INET_ADDR (au1Address);
            InetAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
            MEMCPY (InetAddress.pu1_OctetList, &u4Address,
                    InetAddress.i4_Length);
        }
        else if (IPVX_ADDR_FMLY_IPV6 == i4AddrType)    /* IPVX_ADDR_FMLY_IPV6 */
        {
            STRNCPY (au1Address,
                     str_replace ((INT1 *) au1Address, (INT1 *) "%3A",
                                  (INT1 *) ":"), sizeof (au1Address));
            INET_ATON6 (au1Address, InetAddress.pu1_OctetList);
            InetAddress.i4_Length = IPVX_IPV6_ADDR_LEN;
        }
        else
        {
            InetAddress.i4_Length = 0;
            InetAddress.pu1_OctetList[0] = 0;
        }
    }

    STRCPY (pHttp->au1Name, "fsIpPrefixLength");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4PrefixLen = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "fsIpPrefixMinLength");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u1MinLen = (UINT1) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "fsIpPrefixMaxLength");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u1MaxLen = (UINT1) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "fsIpPrefixAction");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u1Access = (UINT1) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Add") == 0)
    {
        if (nmhTestv2FsRMapRowStatus (&u4ErrCode, &IpPrefixName, u4SeqNo,
                                      RMAP_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            RMapUnLockWrite ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to add the Ip Prefix entry");
            return;
        }
        if (nmhSetFsRMapRowStatus (&IpPrefixName, u4SeqNo, RMAP_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            RMapUnLockWrite ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to add the Ip Prefix entry");
            return;
        }

        if (nmhTestv2FsRMapIsIpPrefixList (&u4ErrCode, &IpPrefixName, u4SeqNo,
                                           TRUE) == SNMP_FAILURE)
        {
            nmhSetFsRMapRowStatus (&IpPrefixName, u4SeqNo, RMAP_DESTROY);
            RMapUnLockWrite ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to add the Ip Prefix entry");
            return;
        }
        if (nmhSetFsRMapIsIpPrefixList (&IpPrefixName, u4SeqNo,
                                        TRUE) == SNMP_FAILURE)
        {
            nmhSetFsRMapRowStatus (&IpPrefixName, u4SeqNo, RMAP_DESTROY);
            RMapUnLockWrite ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to add the Ip Prefix entry");
            return;
        }

        if (nmhTestv2FsRMapAccess (&u4ErrCode, &IpPrefixName, u4SeqNo,
                                   (INT4) u1Access) == SNMP_FAILURE)
        {
            nmhSetFsRMapRowStatus (&IpPrefixName, u4SeqNo, RMAP_DESTROY);
            RMapUnLockWrite ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to add the Ip Prefix entry");
            return;
        }
        if (nmhSetFsRMapAccess (&IpPrefixName, u4SeqNo,
                                (INT4) u1Access) == SNMP_FAILURE)
        {
            nmhSetFsRMapRowStatus (&IpPrefixName, u4SeqNo, RMAP_DESTROY);
            RMapUnLockWrite ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to add the Ip Prefix entry");
            return;
        }

        if (nmhTestv2FsRMapMatchRowStatus (&u4ErrCode, &IpPrefixName, u4SeqNo,
                                           i4AddrType,
                                           &InetAddress, u4PrefixLen, 0, 0, 0,
                                           0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                           RMAP_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            nmhSetFsRMapRowStatus (&IpPrefixName, u4SeqNo, RMAP_DESTROY);
            RMapUnLockWrite ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to add the Ip Prefix entry");
            return;
        }
        if (nmhSetFsRMapMatchRowStatus (&IpPrefixName, u4SeqNo,
                                        i4AddrType,
                                        &InetAddress, u4PrefixLen, 0, 0, 0, 0,
                                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                        RMAP_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            nmhSetFsRMapRowStatus (&IpPrefixName, u4SeqNo, RMAP_DESTROY);
            RMapUnLockWrite ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to add the Ip Prefix entry");
            return;
        }

        if (u1MinLen != 0)
        {
            if (nmhTestv2FsRMapMatchDestMinPrefixLen
                (&u4ErrCode, &IpPrefixName, u4SeqNo,
                 i4AddrType, &InetAddress, u4PrefixLen,
                 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, u1MinLen)
                == SNMP_FAILURE)
            {
                nmhSetFsRMapMatchRowStatus (&IpPrefixName, u4SeqNo,
                                            i4AddrType,
                                            &InetAddress, u4PrefixLen, 0, 0, 0,
                                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                            RMAP_DESTROY);
                nmhSetFsRMapRowStatus (&IpPrefixName, u4SeqNo, RMAP_DESTROY);
                RMapUnLockWrite ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to add the Ip Prefix entry");
                return;
            }

            if (nmhSetFsRMapMatchDestMinPrefixLen (&IpPrefixName, u4SeqNo,
                                                   i4AddrType, &InetAddress,
                                                   u4PrefixLen, 0, 0, 0, 0, 0,
                                                   0, 0, 0, 0, 0, 0, 0, 0, 0,
                                                   u1MinLen) == SNMP_FAILURE)
            {
                nmhSetFsRMapMatchRowStatus (&IpPrefixName, u4SeqNo,
                                            i4AddrType,
                                            &InetAddress, u4PrefixLen, 0, 0, 0,
                                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                            RMAP_DESTROY);
                nmhSetFsRMapRowStatus (&IpPrefixName, u4SeqNo, RMAP_DESTROY);
                RMapUnLockWrite ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to add the Ip Prefix entry");
                return;
            }
        }
        if (u1MaxLen != 0)
        {
            if (nmhTestv2FsRMapMatchDestMaxPrefixLen
                (&u4ErrCode, &IpPrefixName, u4SeqNo,
                 i4AddrType, &InetAddress, u4PrefixLen,
                 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 u1MaxLen) == SNMP_FAILURE)
            {
                nmhSetFsRMapMatchRowStatus (&IpPrefixName, u4SeqNo,
                                            i4AddrType,
                                            &InetAddress, u4PrefixLen, 0, 0, 0,
                                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                            RMAP_DESTROY);
                nmhSetFsRMapRowStatus (&IpPrefixName, u4SeqNo, RMAP_DESTROY);
                RMapUnLockWrite ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to add the Ip Prefix entry");
                return;
            }

            if (nmhSetFsRMapMatchDestMaxPrefixLen (&IpPrefixName, u4SeqNo,
                                                   i4AddrType,
                                                   &InetAddress,
                                                   u4PrefixLen, 0, 0, 0, 0, 0,
                                                   0, 0, 0, 0, 0, 0, 0, 0, 0,
                                                   u1MaxLen) == SNMP_FAILURE)
            {
                nmhSetFsRMapMatchRowStatus (&IpPrefixName, u4SeqNo,
                                            i4AddrType,
                                            &InetAddress, u4PrefixLen, 0, 0, 0,
                                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                            RMAP_DESTROY);
                nmhSetFsRMapRowStatus (&IpPrefixName, u4SeqNo, RMAP_DESTROY);
                RMapUnLockWrite ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to add the Ip Prefix entry");
                return;
            }
        }

        if (nmhTestv2FsRMapMatchRowStatus (&u4ErrCode, &IpPrefixName, u4SeqNo,
                                           i4AddrType,
                                           &InetAddress, u4PrefixLen, 0, 0, 0,
                                           0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                           RMAP_ACTIVE) == SNMP_FAILURE)
        {
            nmhSetFsRMapMatchRowStatus (&IpPrefixName, u4SeqNo,
                                        i4AddrType,
                                        &InetAddress, u4PrefixLen, 0, 0, 0, 0,
                                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                        RMAP_DESTROY);
            nmhSetFsRMapRowStatus (&IpPrefixName, u4SeqNo, RMAP_DESTROY);
            RMapUnLockWrite ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to add the Ip Prefix entry");
            return;
        }
        if (nmhSetFsRMapMatchRowStatus (&IpPrefixName, u4SeqNo,
                                        i4AddrType,
                                        &InetAddress, u4PrefixLen, 0, 0, 0, 0,
                                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                        RMAP_ACTIVE) == SNMP_FAILURE)
        {
            nmhSetFsRMapMatchRowStatus (&IpPrefixName, u4SeqNo,
                                        i4AddrType,
                                        &InetAddress, u4PrefixLen, 0, 0, 0, 0,
                                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                        RMAP_DESTROY);
            nmhSetFsRMapRowStatus (&IpPrefixName, u4SeqNo, RMAP_DESTROY);
            RMapUnLockWrite ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to add the Ip Prefix entry");
            return;
        }

        if (nmhTestv2FsRMapRowStatus (&u4ErrCode, &IpPrefixName, u4SeqNo,
                                      RMAP_ACTIVE) == SNMP_FAILURE)
        {
            nmhSetFsRMapMatchRowStatus (&IpPrefixName, u4SeqNo,
                                        i4AddrType,
                                        &InetAddress, u4PrefixLen, 0, 0, 0, 0,
                                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                        RMAP_DESTROY);
            nmhSetFsRMapRowStatus (&IpPrefixName, u4SeqNo, RMAP_DESTROY);
            RMapUnLockWrite ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to add the Ip Prefix entry");
            return;
        }
        if (nmhSetFsRMapRowStatus (&IpPrefixName, u4SeqNo, RMAP_ACTIVE)
            == SNMP_FAILURE)
        {
            nmhSetFsRMapMatchRowStatus (&IpPrefixName, u4SeqNo,
                                        i4AddrType,
                                        &InetAddress, u4PrefixLen, 0, 0, 0, 0,
                                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                        RMAP_DESTROY);
            nmhSetFsRMapRowStatus (&IpPrefixName, u4SeqNo, RMAP_DESTROY);
            RMapUnLockWrite ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to add the Ip Prefix entry");
            return;
        }
    }
    else if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {
        if (nmhTestv2FsRMapMatchRowStatus
            (&u4ErrCode, &IpPrefixName, u4SeqNo, i4AddrType, &InetAddress,
             u4PrefixLen, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             DESTROY) == SNMP_FAILURE)
        {
            RMapUnLockWrite ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to Delete the entry");
            return;
        }
        if (nmhSetFsRMapMatchRowStatus (&IpPrefixName, u4SeqNo,
                                        i4AddrType, &InetAddress,
                                        u4PrefixLen, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                        0, 0, 0, 0, 0, DESTROY) == SNMP_FAILURE)
        {
            RMapUnLockWrite ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to Delete the entry");
            return;
        }

        if (nmhTestv2FsRMapRowStatus (&u4ErrCode, &IpPrefixName, u4SeqNo,
                                      DESTROY) == SNMP_FAILURE)
        {
            RMapUnLockWrite ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to Delete the entry");
            return;
        }
        if (nmhSetFsRMapRowStatus (&IpPrefixName, u4SeqNo, DESTROY)
            == SNMP_FAILURE)
        {
            RMapUnLockWrite ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to Delete the entry");
            return;
        }
    }
    RMapUnLockWrite ();
    WebnmUnRegisterLock (pHttp);
    IssProcessRMapIpPrefixPageGet (pHttp);
    return;
}
