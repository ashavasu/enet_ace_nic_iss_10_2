/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: wsscontrollerweb.c,v 1.3 2017/05/23 14:21:47 siva Exp $
 *
 * Description: Routines for WSS CONTROLLER WEB Module
 *******************************************************************/

#ifndef _WSS_CONTROLLER_WEB_C_
#define _WSS_CONTROLLER_WEB_C_

#include "webiss.h"
#include "isshttp.h"
#include "webinc.h"
#include "issmacro.h"
#include "snmputil.h"
#include "utilcli.h"
#include "wsscfgcli.h"
#include "capwapcliinc.h"
#include "capwapclidefg.h"

#include "capwapcliprot.h"
#include "capwapcli.h"
#include "wsscfgwlanproto.h"
#include "wsscfgcli.h"
#include "wsscfgtdfsg.h"
#include "wsscfgtdfs.h"
#include "wsscfglwg.h"

#define    wss_web_trace_enable      1

#define    WSS_WEB_TRC               if(wss_web_trace_enable)  printf
#define    MAX_RADIO_TYPE            6

/* extern INT4 CapwapTestAllFsWtpModelTable PROTO ((UINT4 *, 
 * tCapwapFsWtpModelEntry *, tCapwapIsSetFsWtpModelEntry *, INT4 , INT4));
 extern INT4 CapwapSetAllFsWtpModelTable PROTO ((tCapwapFsWtpModelEntry *, 
 tCapwapIsSetFsWtpModelEntry *, INT4 , INT4 )); */
extern INT4 WsscfgGetAllFsWtpImageUpgradeTable
PROTO ((tWsscfgFsWtpImageUpgradeEntry *));
extern INT4 WsscfgTestAllFsWtpImageUpgradeTable
PROTO ((UINT4 *, tWsscfgFsWtpImageUpgradeEntry *,
        tWsscfgIsSetFsWtpImageUpgradeEntry *, INT4, INT4));
extern INT4         WsscfgSetAllFsWtpImageUpgradeTable
PROTO ((tWsscfgFsWtpImageUpgradeEntry *, tWsscfgIsSetFsWtpImageUpgradeEntry *,
        INT4, INT4));
extern UINT1        gau1ModelNumber[CLI_MODEL_NAME_LEN];
UINT1               gau1CurrentModelNumber[OCTETSTR_SIZE];
/* UINT1 gau1CurrentEditModelNumber[OCTETSTR_SIZE]; */
INT4                gi4CurrentMaxSSID;
UINT4               gu4CurrentNoOfRadios;
BOOL1               bIsEdit;
UINT1               gu1ApImageUpgradestatus = WSS_IMAGE_UPGRADE_NOT_INITIATED;

#ifdef WLC_WANTED
UINT1               WSS_IMG_DOWNLOAD_FAIL[] =
    "<font color=red size=+2>Image Download Failed !";
UINT1               WSS_IMG_DOWNLOAD_SUCCESS[] =
    "<font color=red size=+2>Image Download Successful";
UINT1               WSS_IMG_DOWNLOAD_PROGRESS[] =
    "<font color=red size=+2>Image Download In Progress...";
UINT1               WSS_IMG_DOWNLOAD_INITIATED[] =
    "<font color=red size=+2>Image Download Not Initiated";
#endif
/*********************************************************************
*  Function Name : IssProcessControllerGeneralPage
*  Description   : This function processes the request coming for the
*                  Controller General Page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessControllerGeneralPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessControllerGeneralPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessControllerGeneralPageSet (pHttp);
    }

}

/*********************************************************************
*  Function Name : IssProcessControllerGeneralPageGet
*  Description   : This function processes the set request coming for the
*                  AP Model  page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessControllerGeneralPageGet (tHttp * pHttp)
{

    UINT1               au1ModelNumber[OCTETSTR_SIZE];
    UINT1               au1APSwVersion[OCTETSTR_SIZE];
    UINT1               au1SelectAp[OCTETSTR_SIZE];
    UINT1               au1ImageServerIPVersion[OCTETSTR_SIZE];
    UINT1               au1ImageServerIP[OCTETSTR_SIZE];
    UINT1               au1ImageName[OCTETSTR_SIZE];
    UINT1               au1ApName[OCTETSTR_SIZE];
    INT4                i4OutCome = 0, i4AddressType = 0, i4RetVal = 0;
    UINT4               currentProfileId = 0, nextProfileId = 0;
    UINT1               ProfileId[OCTETSTR_SIZE];
    tSNMP_OCTET_STRING_TYPE *currentModelNumber = NULL, *nextModelNumber = NULL,
        *WtpName = NULL, *ImageName = NULL,
        *ImageServerIPVer = NULL, *SwVersion = NULL, *ServerIp = NULL;
    tSNMP_OCTET_STRING_TYPE ProfileName;
    UINT1               au1ProfileName[256];
    tCapwapFsWtpModelEntry capwapFsWtpModelEntry;
    tSNMP_OCTET_STRING_TYPE *Softwareversion = NULL;

    tWsscfgFsWtpImageUpgradeEntry wsscfgFsWtpImageUpgradeEntry;
    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ProfileName, 0, 256);
    MEMSET (ProfileId, 0, OCTETSTR_SIZE);
    ProfileName.pu1_OctetList = au1ProfileName;

    currentModelNumber =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (currentModelNumber == NULL)
    {
        return;
    }
    nextModelNumber =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (nextModelNumber == NULL)
    {
        free_octetstring (currentModelNumber);
        return;
    }
    WtpName =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (CLI_MODEL_NAME_LEN);
    if (WtpName == NULL)
    {
        free_octetstring (currentModelNumber);
        free_octetstring (nextModelNumber);
        return;
    }
    ImageName =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (CLI_MODEL_NAME_LEN);
    if (ImageName == NULL)
    {
        free_octetstring (currentModelNumber);
        free_octetstring (nextModelNumber);
        free_octetstring (WtpName);
        return;
    }
    ImageServerIPVer =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (ImageServerIPVer == NULL)
    {
        free_octetstring (currentModelNumber);
        free_octetstring (nextModelNumber);
        free_octetstring (WtpName);
        free_octetstring (ImageName);
        return;
    }
    SwVersion =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (SwVersion == NULL)
    {
        free_octetstring (currentModelNumber);
        free_octetstring (nextModelNumber);
        free_octetstring (WtpName);
        free_octetstring (ImageName);
        free_octetstring (ImageServerIPVer);
        return;
    }
    ServerIp = (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);

    if (ServerIp == NULL)
    {
        free_octetstring (currentModelNumber);
        free_octetstring (nextModelNumber);
        free_octetstring (WtpName);
        free_octetstring (ImageName);
        free_octetstring (ImageServerIPVer);
        free_octetstring (SwVersion);
        return;
    }
    Softwareversion =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (Softwareversion == NULL)
    {
        free_octetstring (currentModelNumber);
        free_octetstring (nextModelNumber);
        free_octetstring (WtpName);
        free_octetstring (ImageName);
        free_octetstring (ImageServerIPVer);
        free_octetstring (SwVersion);
        free_octetstring (ServerIp);
        return;
    }

    MEMSET (currentModelNumber->pu1_OctetList, 0, OCTETSTR_SIZE);

    MEMSET (WtpName->pu1_OctetList, 0, CLI_MODEL_NAME_LEN);
    MEMSET (ImageName->pu1_OctetList, 0, CLI_MODEL_NAME_LEN);
    MEMSET (ImageServerIPVer->pu1_OctetList, 0, OCTETSTR_SIZE);
    MEMSET (SwVersion->pu1_OctetList, 0, OCTETSTR_SIZE);
    MEMSET (ServerIp->pu1_OctetList, 0, OCTETSTR_SIZE);
    MEMSET (nextModelNumber->pu1_OctetList, 0, OCTETSTR_SIZE);
    MEMSET (&wsscfgFsWtpImageUpgradeEntry, 0,
            sizeof (tWsscfgFsWtpImageUpgradeEntry));
    MEMSET (Softwareversion->pu1_OctetList, 0, OCTETSTR_SIZE);

    Softwareversion->i4_Length = 0;

    currentModelNumber->i4_Length = 0;
    nextModelNumber->i4_Length = 0;
    WtpName->i4_Length = 0;
    ImageName->i4_Length = 0;
    ImageServerIPVer->i4_Length = 0;
    SwVersion->i4_Length = 0;
    ServerIp->i4_Length = 0;

    MEMSET (&au1ModelNumber, 0, OCTETSTR_SIZE);
    MEMSET (&au1APSwVersion, 0, OCTETSTR_SIZE);
    MEMSET (&au1SelectAp, 0, OCTETSTR_SIZE);
    MEMSET (&au1ImageServerIPVersion, 0, OCTETSTR_SIZE);
    MEMSET (&au1ImageServerIP, 0, OCTETSTR_SIZE);
    MEMSET (&au1ImageName, 0, OCTETSTR_SIZE);
    MEMSET (&au1ApName, 0, OCTETSTR_SIZE);
    MEMSET (&capwapFsWtpModelEntry, 0, sizeof (tCapwapFsWtpModelEntry));

    WebnmRegisterLock (pHttp, CapwapMainTaskLock, CapwapMainTaskUnLock);
    CAPWAP_LOCK;
    MEMSET (currentModelNumber->pu1_OctetList, 0, OCTETSTR_SIZE);
    currentModelNumber->i4_Length = 0;
    MEMCPY (wsscfgFsWtpImageUpgradeEntry.MibObject.
            au1CapwapBaseWtpProfileWtpModelNumber, gau1ModelNumber,
            STRLEN (gau1ModelNumber));
    wsscfgFsWtpImageUpgradeEntry.MibObject.
        i4CapwapBaseWtpProfileWtpModelNumberLen = STRLEN (gau1ModelNumber);

    if (WsscfgGetAllFsWtpImageUpgradeTable (&wsscfgFsWtpImageUpgradeEntry) ==
        OSIX_SUCCESS)
    {
        MEMCPY (WtpName->pu1_OctetList,
                wsscfgFsWtpImageUpgradeEntry.MibObject.au1FsWtpName,
                wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpNameLen);
        WtpName->i4_Length =
            wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpNameLen;
        MEMCPY (ImageName->pu1_OctetList,
                wsscfgFsWtpImageUpgradeEntry.MibObject.au1FsWtpImageName,
                wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpImageNameLen);
        ImageName->i4_Length =
            wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpImageNameLen;
        i4AddressType =
            wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpAddressType;
        MEMCPY (SwVersion->pu1_OctetList,
                wsscfgFsWtpImageUpgradeEntry.MibObject.au1FsWtpImageVersion,
                wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpImageVersionLen);
        SwVersion->i4_Length =
            wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpImageVersionLen;
        MEMCPY (ServerIp->pu1_OctetList,
                wsscfgFsWtpImageUpgradeEntry.MibObject.au1FsWtpServerIP,
                wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpServerIPLen);

        ServerIp->i4_Length =
            wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpServerIPLen;
    }

    MEMCPY (capwapFsWtpModelEntry.MibObject.au1FsCapwapWtpModelNumber,
            gau1ModelNumber, STRLEN (gau1ModelNumber));
    capwapFsWtpModelEntry.MibObject.i4FsCapwapWtpModelNumberLen =
        STRLEN (gau1ModelNumber);
    if (CapwapGetAllFsWtpModelTable (&capwapFsWtpModelEntry) == OSIX_SUCCESS)
    {
        MEMCPY (Softwareversion->pu1_OctetList,
                capwapFsWtpModelEntry.MibObject.au1FsCapwapSwVersion,
                capwapFsWtpModelEntry.MibObject.i4FsCapwapSwVersionLen);
        Softwareversion->i4_Length =
            capwapFsWtpModelEntry.MibObject.i4FsCapwapSwVersionLen;

    }

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "ModelNumberKey");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", gau1ModelNumber);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "SelectApKey");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", WtpName->pu1_OctetList);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    HTTP_STRLEN (pHttp->au1DataString));

    i4OutCome = nmhGetFirstIndexCapwapBaseWtpProfileTable (&nextProfileId);
    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }

    STRCPY (pHttp->au1KeyString, "<! AP_NAME>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        currentProfileId = nextProfileId;

        if (currentProfileId != 0)
        {
            MEMSET (au1ProfileName, 0, 256);
            MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
            ProfileName.pu1_OctetList = au1ProfileName;

            i4RetVal = CapwapUtilGetBaseWtpProfileName (currentProfileId,
                                                        &ProfileName,
                                                        gau1ModelNumber);
            if (SNMP_FAILURE == i4RetVal)
            {
                continue;
            }

            STRCPY (pHttp->au1Name, "AP_Name");
            SPRINTF ((CHR1 *) pHttp->au1DataString,
                     "<option value = \"%s\" selected>%s \n",
                     ProfileName.pu1_OctetList, ProfileName.pu1_OctetList);

        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));

    }
    while (nmhGetNextIndexCapwapBaseWtpProfileTable (currentProfileId,
                                                     &nextProfileId));
#if 0
    STRCPY (pHttp->au1KeyString, "AP_NAME_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", ProfileName.pu1_OctetList);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);
#endif

    STRCPY (pHttp->au1KeyString, "IpVerKey");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4AddressType);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "ServerIpKey");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", ServerIp->pu1_OctetList);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "ImageNameKey");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", ImageName->pu1_OctetList);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "status_key");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", gu1ApImageUpgradestatus);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "<! UPGRADE_STATUS>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    switch (gu1ApImageUpgradestatus)
    {
        case (WSS_IMAGE_UPGRADE_WAITING):
        {
            WebnmSockWrite (pHttp, WSS_IMG_DOWNLOAD_PROGRESS,
                            STRLEN (WSS_IMG_DOWNLOAD_PROGRESS));
            break;
        }
        case (WSS_IMAGE_UPGRADE_FAILURE):
        {
            WebnmSockWrite (pHttp, WSS_IMG_DOWNLOAD_FAIL,
                            STRLEN (WSS_IMG_DOWNLOAD_FAIL));
            break;
        }
        case (WSS_IMAGE_UPGRADE_SUCCESS):
        {
            WebnmSockWrite (pHttp, WSS_IMG_DOWNLOAD_SUCCESS,
                            STRLEN (WSS_IMG_DOWNLOAD_SUCCESS));
            break;
        }
        default:
        {
            WebnmSockWrite (pHttp, WSS_IMG_DOWNLOAD_INITIATED,
                            STRLEN (WSS_IMG_DOWNLOAD_INITIATED));
            break;
        }
    }

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    WebnmUnRegisterLock (pHttp);
    free_octetstring (currentModelNumber);
    free_octetstring (nextModelNumber);
    free_octetstring (WtpName);
    free_octetstring (ImageName);
    free_octetstring (ImageServerIPVer);
    free_octetstring (SwVersion);
    free_octetstring (ServerIp);
    free_octetstring (Softwareversion);
    CAPWAP_UNLOCK;

}

/*********************************************************************
*  Function Name : IssProcessControllerGeneralPageSet
*  Description   : This function processes the set request coming for the
*                  AP Model  page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessControllerGeneralPageSet (tHttp * pHttp)
{

    UINT1               au1ModelNumber[CLI_MAX_PROFILE_NAME];
    UINT1               au1APSwVersion[CLI_MAX_PROFILE_NAME];
    UINT1               au1SelectAp[CLI_MAX_PROFILE_NAME];
    UINT1               au1ImageServerIPVersion[CLI_MAX_PROFILE_NAME];
    tIPvXAddr           IpAddress;
    UINT1               au1ImageServerIP[CLI_MAX_PROFILE_NAME];
    UINT1               au1ImageName[CLI_MAX_PROFILE_NAME];
    UINT1               au1ApName[CLI_MAX_PROFILE_NAME];
    UINT4               u4ErrorCode = 0, u4ApOption = 0;
    UINT4               currentProfileId = 0, nextProfileId = 0;
    INT4                i4APSwVersion = 0, i4AddressType = 0, i4OutCome =
        0, i4RetVal = 0;
    UINT1               TftpStr[300];
    FILE               *fp;

    tSNMP_OCTET_STRING_TYPE ProfileName;
    UINT1               au1ProfileName[256];

    tWsscfgFsWtpImageUpgradeEntry wsscfgFsWtpImageUpgradeEntry;
    tWsscfgIsSetFsWtpImageUpgradeEntry wsscfgIsSetFsWtpImageUpgradeEntry;

    MEMSET (&au1ModelNumber, 0, CLI_MAX_PROFILE_NAME);
    MEMSET (&au1APSwVersion, 0, CLI_MAX_PROFILE_NAME);
    MEMSET (&au1SelectAp, 0, CLI_MAX_PROFILE_NAME);
    MEMSET (&au1ImageServerIPVersion, 0, CLI_MAX_PROFILE_NAME);
    MEMSET (&au1ImageServerIP, 0, CLI_MAX_PROFILE_NAME);
    MEMSET (&IpAddress, 0, sizeof (tIPvXAddr));
    MEMSET (&au1ImageName, 0, CLI_MAX_PROFILE_NAME);
    MEMSET (&au1ApName, 0, CLI_MAX_PROFILE_NAME);
    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ProfileName, 0, 256);
    MEMSET (TftpStr, 0, 300);

    WSS_WEB_TRC (" Entering IssProcessControllerGeneralPageSet !!\n");
    MEMSET (&wsscfgFsWtpImageUpgradeEntry, 0,
            sizeof (tWsscfgFsWtpImageUpgradeEntry));
    MEMSET (&wsscfgIsSetFsWtpImageUpgradeEntry, 0,
            sizeof (tWsscfgIsSetFsWtpImageUpgradeEntry));
    WebnmRegisterLock (pHttp, CapwapMainTaskLock, CapwapMainTaskUnLock);
    CAPWAP_LOCK;

    STRCPY (pHttp->au1Name, "ModelNumber");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ModelNumber, pHttp->au1Value, (sizeof (au1ModelNumber) - 1));

    STRCPY (pHttp->au1Name, "SelectAp");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4ApOption = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "AP_Name");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ApName, pHttp->au1Value, (sizeof (au1ApName) - 1));

    STRCPY (pHttp->au1Name, "IpVer");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4AddressType = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "SERVER_IP");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (IpAddress.au1Addr, pHttp->au1Value, IPVX_MAX_INET_ADDR_LEN);
    IpAddress.au1Addr[IPVX_MAX_INET_ADDR_LEN - 1] = '\0';

    STRCPY (pHttp->au1Name, "IMAGE_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    MEMCPY (au1ImageName, pHttp->au1Value,
            MEM_MAX_BYTES (sizeof (au1ImageName), STRLEN (pHttp->au1Value)));

    au1ImageName[MEM_MAX_BYTES (sizeof (au1ImageName),
                                STRLEN (pHttp->au1Value)) - 1] = '\0';

    WSS_WEB_TRC
        ("values from General Page\n=================================\n");
    WSS_WEB_TRC ("Model Number :%s\n", au1ModelNumber);
    WSS_WEB_TRC ("au1APSwVersion :%d\n", i4APSwVersion);
    WSS_WEB_TRC ("ApName :%s \n", au1ApName);
    WSS_WEB_TRC ("ImageName    :%s \n", au1ImageName);
    WSS_WEB_TRC ("ImageServerIPVer :%s \n", au1ImageServerIPVersion);
    WSS_WEB_TRC ("Server IP   :%s \n", IpAddress.au1Addr);

    STRCPY (wsscfgFsWtpImageUpgradeEntry.MibObject.au1FsWtpImageVersion,
            au1ImageServerIPVersion);
    wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpImageVersionLen =
        STRLEN (au1ImageServerIPVersion);
    STRNCPY (wsscfgFsWtpImageUpgradeEntry.MibObject.au1FsWtpName, au1ApName,
             STRLEN (au1ApName));
    wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpNameLen = STRLEN (au1ApName);
    STRCPY (wsscfgFsWtpImageUpgradeEntry.MibObject.au1FsWtpImageName,
            au1ImageName);
    wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpImageNameLen =
        STRLEN (au1ImageName);
    STRNCPY (wsscfgFsWtpImageUpgradeEntry.MibObject.au1FsWtpServerIP,
             IpAddress.au1Addr, IPVX_MAX_INET_ADDR_LEN);
    wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpServerIPLen =
        IPVX_MAX_INET_ADDR_LEN;

    STRCPY (wsscfgFsWtpImageUpgradeEntry.MibObject.
            au1CapwapBaseWtpProfileWtpModelNumber, au1ModelNumber);
    wsscfgFsWtpImageUpgradeEntry.MibObject.
        i4CapwapBaseWtpProfileWtpModelNumberLen = STRLEN (au1ModelNumber);
    wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpRowStatus = ACTIVE;
    wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpUpgradeDev = u4ApOption;
    wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpAddressType = i4AddressType;

    wsscfgIsSetFsWtpImageUpgradeEntry.bFsWtpUpgradeDev = OSIX_TRUE;
    wsscfgIsSetFsWtpImageUpgradeEntry.bFsWtpAddressType = OSIX_TRUE;
    wsscfgIsSetFsWtpImageUpgradeEntry.bFsWtpImageVersion = OSIX_TRUE;
    wsscfgIsSetFsWtpImageUpgradeEntry.bFsWtpName = OSIX_TRUE;
    wsscfgIsSetFsWtpImageUpgradeEntry.bFsWtpImageName = OSIX_TRUE;
    wsscfgIsSetFsWtpImageUpgradeEntry.bFsWtpServerIP = OSIX_TRUE;
    wsscfgIsSetFsWtpImageUpgradeEntry.bFsWtpRowStatus = OSIX_TRUE;
    wsscfgIsSetFsWtpImageUpgradeEntry.bCapwapBaseWtpProfileWtpModelNumber =
        OSIX_TRUE;

    fp = fopen ((const char *) au1ImageName, "rb");
    if (NULL == fp)
    {
        SPRINTF ((CHR1 *) TftpStr, "tftp://%s/%s", IpAddress.au1Addr,
                 au1ImageName);
        gu1ApImageUpgradestatus = WSS_IMAGE_UPGRADE_WAITING;
        i4RetVal =
            WsscfgUtilImageTransfer ((INT1 *) &TftpStr, (INT1 *) &au1ImageName,
                                     &IpAddress);
        if (SNMP_FAILURE == i4RetVal)
        {
            gu1ApImageUpgradestatus = WSS_IMAGE_UPGRADE_FAILURE;
            printf ("Image transfer Failed FAILED!\n");
            IssSendError (pHttp, (CONST INT1 *) "Image Transfer Failed\n");
            WebnmUnRegisterLock (pHttp);
            CAPWAP_UNLOCK;
            return;

        }
    }
    if (u4ApOption == WSSCFG_VERSION_AP_NAME_NONE)
    {
        if (SNMP_SUCCESS == i4RetVal)
        {
            gu1ApImageUpgradestatus = WSS_IMAGE_UPGRADE_SUCCESS;
            WebnmUnRegisterLock (pHttp);
            CAPWAP_UNLOCK;
            if (NULL != fp)
            {
                fclose (fp);
            }
            return;
        }
    }
    if (u4ApOption == WSSCFG_VERSION_AP_NAME_ALL)
    {
        i4OutCome = nmhGetFirstIndexCapwapBaseWtpProfileTable (&nextProfileId);
        if (i4OutCome == SNMP_FAILURE)
        {
            gu1ApImageUpgradestatus = WSS_IMAGE_UPGRADE_FAILURE;
            if (NULL != fp)
            {
                fclose (fp);
            }
            return;
        }
        do
        {
            currentProfileId = nextProfileId;

            if (currentProfileId != 0)
            {
                MEMSET (au1ProfileName, 0, 256);
                MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
                ProfileName.pu1_OctetList = au1ProfileName;

                i4RetVal = CapwapUtilGetBaseWtpProfileName (currentProfileId,
                                                            &ProfileName,
                                                            gau1ModelNumber);
                if (SNMP_FAILURE == i4RetVal)
                {
                    continue;
                }
                STRCPY (wsscfgFsWtpImageUpgradeEntry.MibObject.au1FsWtpName,
                        ProfileName.pu1_OctetList);
                wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpNameLen =
                    STRLEN (ProfileName.pu1_OctetList);

                if (WsscfgTestAllFsWtpImageUpgradeTable
                    (&u4ErrorCode, &wsscfgFsWtpImageUpgradeEntry,
                     &wsscfgIsSetFsWtpImageUpgradeEntry, OSIX_TRUE,
                     OSIX_TRUE) != OSIX_SUCCESS)
                {
                    gu1ApImageUpgradestatus = WSS_IMAGE_UPGRADE_FAILURE;
                    printf ("WsscfgTestAllFsWtpImageUpgradeTable FAILED!\n");
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Invalid Configuration\n");
                    WebnmUnRegisterLock (pHttp);
                    CAPWAP_UNLOCK;
                    if (NULL != fp)
                    {
                        fclose (fp);
                    }
                    return;
                }
                if (WsscfgSetAllFsWtpImageUpgradeTable
                    (&wsscfgFsWtpImageUpgradeEntry,
                     &wsscfgIsSetFsWtpImageUpgradeEntry, OSIX_FALSE,
                     OSIX_TRUE) != OSIX_SUCCESS)
                {
                    gu1ApImageUpgradestatus = WSS_IMAGE_UPGRADE_FAILURE;
                    printf ("WsscfgSetAllFsWtpImageUpgradeTable FAILED !\n");
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Invalid Configuration\n");
                    WebnmUnRegisterLock (pHttp);
                    CAPWAP_UNLOCK;
                    if (NULL != fp)
                    {
                        fclose (fp);
                    }
                    return;
                }
            }
        }
        while (nmhGetNextIndexCapwapBaseWtpProfileTable (currentProfileId,
                                                         &nextProfileId));
        gu1ApImageUpgradestatus = WSS_IMAGE_UPGRADE_SUCCESS;

    }
    else
    {
        if (WsscfgTestAllFsWtpImageUpgradeTable (&u4ErrorCode,
                                                 &wsscfgFsWtpImageUpgradeEntry,
                                                 &wsscfgIsSetFsWtpImageUpgradeEntry,
                                                 OSIX_TRUE,
                                                 OSIX_TRUE) != OSIX_SUCCESS)
        {
            gu1ApImageUpgradestatus = WSS_IMAGE_UPGRADE_FAILURE;
            printf ("WsscfgTestAllFsWtpImageUpgradeTable FAILED!\n");
            IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
            WebnmUnRegisterLock (pHttp);
            CAPWAP_UNLOCK;
            if (NULL != fp)
            {
                fclose (fp);
            }
            return;
        }
        if (WsscfgSetAllFsWtpImageUpgradeTable (&wsscfgFsWtpImageUpgradeEntry,
                                                &wsscfgIsSetFsWtpImageUpgradeEntry,
                                                OSIX_FALSE,
                                                OSIX_TRUE) != OSIX_SUCCESS)
        {
            gu1ApImageUpgradestatus = WSS_IMAGE_UPGRADE_FAILURE;
            printf ("WsscfgSetAllFsWtpImageUpgradeTable FAILED !\n");
            IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
            WebnmUnRegisterLock (pHttp);
            CAPWAP_UNLOCK;
            if (NULL != fp)
            {
                fclose (fp);
            }
            return;
        }
        gu1ApImageUpgradestatus = WSS_IMAGE_UPGRADE_SUCCESS;
    }
    WebnmUnRegisterLock (pHttp);
    CAPWAP_UNLOCK;
    if (NULL != fp)
    {
        fclose (fp);
    }
    return;
}

/*********************************************************************
*  Function Name : IssProcessAPModelEntryPage 
*  Description   : This function processes the request coming for the 
*                  AP Model Entry Page 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessAPModelEntryPage (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE *Model;
    Model = (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);;
    if (Model == NULL)
    {
        return;
    }
    MEMSET (Model->pu1_OctetList, 0, OCTETSTR_SIZE);
    Model->i4_Length = 0;
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessAPModelEntryPageGet (pHttp, Model);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessAPModelEntryPageSet (pHttp);
    }
    free_octetstring (Model);
}

/*********************************************************************
*  Function Name : IssProcessAPModelEntryPageGet 
*  Description   : This function processes the request coming for the 
*                  AP Model Entry Page 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessAPModelEntryPageGet (tHttp * pHttp,
                               tSNMP_OCTET_STRING_TYPE * ModelNumber)
{
    tSNMP_OCTET_STRING_TYPE *Tunnel = NULL;
    tSNMP_OCTET_STRING_TYPE *Softwareversion = NULL;
    tSNMP_OCTET_STRING_TYPE *Imageversion = NULL;
    tSNMP_OCTET_STRING_TYPE *QosProfile;
    tSNMP_OCTET_STRING_TYPE *pCurrentProfileName = NULL, *pNextProfileName =
        NULL;
    INT4                i4OutCome = 0, i4Mactype = 0;
    UINT4               u4NoOfRadios = 0;
    INT4                i4CurrentIndex = 0, i4NextIndex = 0;
    tCapwapFsWtpModelEntry capwapFsWtpModelEntry;

    if (CapwapCheckModuleStatus () == OSIX_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Capwap module is shutdown. Can't configure!");
        return;
    }

    pCurrentProfileName =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (pCurrentProfileName == NULL)
    {
        return;
    }
    pNextProfileName =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (pNextProfileName == NULL)
    {
        free_octetstring (pCurrentProfileName);
        return;
    }

    pCurrentProfileName->i4_Length = 0;

    pNextProfileName->i4_Length = 0;

    Imageversion =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (Imageversion == NULL)
    {
        free_octetstring (pCurrentProfileName);
        free_octetstring (pNextProfileName);
        return;
    }
    Softwareversion =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);

    if (Softwareversion == NULL)
    {
        free_octetstring (pCurrentProfileName);
        free_octetstring (pNextProfileName);
        free_octetstring (Imageversion);
        return;
    }
    QosProfile =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (QosProfile == NULL)
    {
        free_octetstring (pCurrentProfileName);
        free_octetstring (pNextProfileName);
        free_octetstring (Softwareversion);
        free_octetstring (Imageversion);
        return;
    }
    Tunnel = (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (Tunnel == NULL)
    {
        free_octetstring (pCurrentProfileName);
        free_octetstring (pNextProfileName);
        free_octetstring (Softwareversion);
        free_octetstring (Imageversion);
        free_octetstring (QosProfile);
        return;
    }
    MEMSET (pNextProfileName->pu1_OctetList, 0, OCTETSTR_SIZE);
    MEMSET (pCurrentProfileName->pu1_OctetList, 0, OCTETSTR_SIZE);
    MEMSET (&capwapFsWtpModelEntry, 0, sizeof (tCapwapFsWtpModelEntry));
    MEMSET (QosProfile->pu1_OctetList, 0, OCTETSTR_SIZE);

    QosProfile->i4_Length = 0;

    MEMSET (pCurrentProfileName->pu1_OctetList, 0, OCTETSTR_SIZE);
    pCurrentProfileName->i4_Length = 0;

    MEMSET (Softwareversion->pu1_OctetList, 0, OCTETSTR_SIZE);
    Softwareversion->i4_Length = 0;

    MEMSET (Imageversion->pu1_OctetList, 0, OCTETSTR_SIZE);
    Imageversion->i4_Length = 0;

    MEMSET (Tunnel->pu1_OctetList, 0, OCTETSTR_SIZE);
    Tunnel->i4_Length = 0;

    WebnmRegisterLock (pHttp, CapwapMainTaskLock, CapwapMainTaskUnLock);
    CAPWAP_LOCK;

    MEMCPY (capwapFsWtpModelEntry.MibObject.au1FsCapwapWtpModelNumber,
            ModelNumber->pu1_OctetList, ModelNumber->i4_Length);
    capwapFsWtpModelEntry.MibObject.i4FsCapwapWtpModelNumberLen =
        ModelNumber->i4_Length;
    if (CapwapGetAllFsWtpModelTable (&capwapFsWtpModelEntry) == OSIX_SUCCESS)
    {
        u4NoOfRadios = capwapFsWtpModelEntry.MibObject.u4FsNoOfRadio;
        i4Mactype = capwapFsWtpModelEntry.MibObject.i4FsCapwapWtpMacType;
        MEMCPY (Tunnel->pu1_OctetList,
                capwapFsWtpModelEntry.MibObject.au1FsCapwapWtpTunnelMode,
                capwapFsWtpModelEntry.MibObject.i4FsCapwapWtpTunnelModeLen);
        Tunnel->i4_Length =
            capwapFsWtpModelEntry.MibObject.i4FsCapwapWtpTunnelModeLen;

        MEMCPY (Softwareversion->pu1_OctetList,
                capwapFsWtpModelEntry.MibObject.au1FsCapwapSwVersion,
                capwapFsWtpModelEntry.MibObject.i4FsCapwapSwVersionLen);
        Softwareversion->i4_Length =
            capwapFsWtpModelEntry.MibObject.i4FsCapwapSwVersionLen;
        MEMCPY (Imageversion->pu1_OctetList,
                capwapFsWtpModelEntry.MibObject.au1FsCapwapImageName,
                capwapFsWtpModelEntry.MibObject.i4FsCapwapImageNameLen);
        Imageversion->i4_Length =
            capwapFsWtpModelEntry.MibObject.i4FsCapwapImageNameLen;
        MEMCPY (QosProfile->pu1_OctetList,
                capwapFsWtpModelEntry.MibObject.au1FsCapwapQosProfileName,
                capwapFsWtpModelEntry.MibObject.i4FsCapwapQosProfileNameLen);
        QosProfile->i4_Length =
            capwapFsWtpModelEntry.MibObject.i4FsCapwapQosProfileNameLen;

    }

    STRCPY (pHttp->au1KeyString, "MODEL_NUM_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", ModelNumber->pu1_OctetList);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    HTTP_STRLEN (pHttp->au1DataString));
    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "NO_OF_RADIOS_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    if (u4NoOfRadios)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4NoOfRadios);
    }
    else
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "");
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    HTTP_STRLEN (pHttp->au1DataString));
    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "CAPWAP_MACTYPE_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Mactype);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    HTTP_STRLEN (pHttp->au1DataString));
    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "CAPWAP_TUNNELMODE_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", Tunnel->pu1_OctetList[0]);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    HTTP_STRLEN (pHttp->au1DataString));
    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    /*
       i4OutCome = nmhGetFirstIndexFsDot11QosProfileTable(pNextProfileName); */
    i4OutCome =
        nmhGetFirstIndexFsQAPProfileTable (pNextProfileName, &i4NextIndex);
    if (i4OutCome == SNMP_FAILURE)
    {
        printf ("\n[VIVEK] Line - %d , function - %s\n", __LINE__,
                __FUNCTION__);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }

    STRCPY (pHttp->au1KeyString, "<! QOS_PROFILES>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        if (STRCMP (pCurrentProfileName->pu1_OctetList,
                    pNextProfileName->pu1_OctetList) != 0)
        {
            STRCPY (pHttp->au1Name, "QosProfile");
            SPRINTF ((CHR1 *) pHttp->au1DataString,
                     "<option value = \"%s\" >%s \n",
                     pNextProfileName->pu1_OctetList,
                     pNextProfileName->pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));

            WSS_WEB_TRC ("pNextProfileName : %s\n",
                         pNextProfileName->pu1_OctetList);
        }
        else
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        }
        /* Copy next index to current index */
        STRCPY (pCurrentProfileName->pu1_OctetList,
                pNextProfileName->pu1_OctetList);
        pCurrentProfileName->i4_Length = pNextProfileName->i4_Length;
        MEMSET (pNextProfileName->pu1_OctetList, 0, OCTETSTR_SIZE);
        pNextProfileName->i4_Length = 0;
        i4CurrentIndex = i4NextIndex;
    }
    while (nmhGetNextIndexFsQAPProfileTable (pCurrentProfileName,
                                             pNextProfileName, i4CurrentIndex,
                                             &i4NextIndex) == SNMP_SUCCESS);

    STRCPY (pHttp->au1KeyString, "IMAGE_VER_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", Imageversion->pu1_OctetList);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    HTTP_STRLEN (pHttp->au1DataString));
    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    WebnmUnRegisterLock (pHttp);
    CAPWAP_UNLOCK;
    free_octetstring (pNextProfileName);
    free_octetstring (pCurrentProfileName);
    free_octetstring (Softwareversion);
    free_octetstring (Imageversion);
    free_octetstring (QosProfile);
    free_octetstring (Tunnel);
}

/*********************************************************************
*  Function Name : IssProcessAPModelEntryEditPage 
*  Description   : This function processes the request coming for the 
*                  AP Model Entry Page 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessAPModelEntryEditPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessAPModelEntryEditPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessAPModelEntryEditPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessAPModelEntryEditPageGet 
*  Description   : This function processes the set request coming for the  
*                  AP Model Entry page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessAPModelEntryEditPageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE *ModelNumber = NULL;
    tSNMP_OCTET_STRING_TYPE *Tunnel = NULL;
    tSNMP_OCTET_STRING_TYPE *Softwareversion = NULL;
    tSNMP_OCTET_STRING_TYPE *Imageversion = NULL;
    tSNMP_OCTET_STRING_TYPE *QosProfile = NULL;
    INT4                i4Mactype = 0;
    tCapwapFsWtpModelEntry capwapFsWtpModelEntry;
    INT4                i4CurrentIndex = 0, i4NextIndex = 0;

    tSNMP_OCTET_STRING_TYPE *pCurrentProfileName = NULL,
        *pNextProfileName = NULL;
    INT4                i4OutCome = 0;

    pCurrentProfileName =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (pCurrentProfileName == NULL)
    {
        return;
    }
    pNextProfileName =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (pNextProfileName == NULL)
    {
        free_octetstring (pCurrentProfileName);
        return;
    }
    Softwareversion =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (Softwareversion == NULL)
    {
        free_octetstring (pCurrentProfileName);
        free_octetstring (pNextProfileName);
        return;
    }
    Imageversion =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (Imageversion == NULL)
    {
        free_octetstring (pCurrentProfileName);
        free_octetstring (pNextProfileName);
        free_octetstring (Softwareversion);
        return;
    }
    QosProfile =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (QosProfile == NULL)
    {
        free_octetstring (pCurrentProfileName);
        free_octetstring (pNextProfileName);
        free_octetstring (Softwareversion);
        free_octetstring (Imageversion);
        return;
    }
    MEMSET (QosProfile->pu1_OctetList, 0, OCTETSTR_SIZE);
    QosProfile->i4_Length = 0;

    MEMSET (pCurrentProfileName->pu1_OctetList, 0, OCTETSTR_SIZE);
    pCurrentProfileName->i4_Length = 0;

    MEMSET (Softwareversion->pu1_OctetList, 0, OCTETSTR_SIZE);
    Softwareversion->i4_Length = 0;

    MEMSET (Imageversion->pu1_OctetList, 0, OCTETSTR_SIZE);
    Imageversion->i4_Length = 0;

    MEMSET (&capwapFsWtpModelEntry, 0, sizeof (tCapwapFsWtpModelEntry));
    MEMSET (pNextProfileName->pu1_OctetList, 0, OCTETSTR_SIZE);
    bIsEdit = OSIX_TRUE;
    STRCPY (pHttp->au1KeyString, "AP_MODEL_NAME_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    /* if(gau1CurrentEditModelNumber!=NULL) */
    if (gau1CurrentModelNumber != NULL)
    {
        Tunnel =
            (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
        if (Tunnel == NULL)
        {
            free_octetstring (pCurrentProfileName);
            free_octetstring (pNextProfileName);
            free_octetstring (Softwareversion);
            free_octetstring (Imageversion);
            free_octetstring (QosProfile);
            return;
        }
        MEMSET (Tunnel->pu1_OctetList, 0, OCTETSTR_SIZE);

        ModelNumber =
            (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
        if (ModelNumber == NULL)
        {
            free_octetstring (pCurrentProfileName);
            free_octetstring (pNextProfileName);
            free_octetstring (Softwareversion);
            free_octetstring (Imageversion);
            free_octetstring (QosProfile);
            free_octetstring (Tunnel);
            return;
        }
        MEMSET (ModelNumber->pu1_OctetList, 0, OCTETSTR_SIZE);

        STRCPY (ModelNumber->pu1_OctetList, gau1CurrentModelNumber);
        ModelNumber->i4_Length = STRLEN (gau1CurrentModelNumber);
        /* STRCPY(ModelNumber->pu1_OctetList,gau1CurrentEditModelNumber); */
        /* ModelNumber->i4_Length = STRLEN(gau1CurrentEditModelNumber); */
        /* nmhGetFsMaxSSIDSupported(ModelNumber,u4RadioId,&gi4CurrentMaxSSID); */

        MEMCPY (capwapFsWtpModelEntry.MibObject.au1FsCapwapWtpModelNumber,
                ModelNumber->pu1_OctetList, ModelNumber->i4_Length);
        capwapFsWtpModelEntry.MibObject.i4FsCapwapWtpModelNumberLen =
            ModelNumber->i4_Length;
        if (CapwapGetAllFsWtpModelTable (&capwapFsWtpModelEntry) ==
            OSIX_SUCCESS)
        {
            gu4CurrentNoOfRadios =
                capwapFsWtpModelEntry.MibObject.u4FsNoOfRadio;
            i4Mactype = capwapFsWtpModelEntry.MibObject.i4FsCapwapWtpMacType;
            MEMCPY (Tunnel->pu1_OctetList,
                    capwapFsWtpModelEntry.MibObject.au1FsCapwapWtpTunnelMode,
                    capwapFsWtpModelEntry.MibObject.i4FsCapwapWtpTunnelModeLen);
            Tunnel->i4_Length =
                capwapFsWtpModelEntry.MibObject.i4FsCapwapWtpTunnelModeLen;

            MEMCPY (Softwareversion->pu1_OctetList,
                    capwapFsWtpModelEntry.MibObject.au1FsCapwapSwVersion,
                    capwapFsWtpModelEntry.MibObject.i4FsCapwapSwVersionLen);
            Softwareversion->i4_Length =
                capwapFsWtpModelEntry.MibObject.i4FsCapwapSwVersionLen;

            MEMCPY (Imageversion->pu1_OctetList,
                    capwapFsWtpModelEntry.MibObject.au1FsCapwapImageName,
                    capwapFsWtpModelEntry.MibObject.i4FsCapwapImageNameLen);
            Imageversion->i4_Length =
                capwapFsWtpModelEntry.MibObject.i4FsCapwapImageNameLen;
            MEMCPY (QosProfile->pu1_OctetList,
                    capwapFsWtpModelEntry.MibObject.au1FsCapwapQosProfileName,
                    capwapFsWtpModelEntry.MibObject.
                    i4FsCapwapQosProfileNameLen);
            QosProfile->i4_Length =
                capwapFsWtpModelEntry.MibObject.i4FsCapwapQosProfileNameLen;

        }
    }
    WebnmRegisterLock (pHttp, CapwapMainTaskLock, CapwapMainTaskUnLock);
    CAPWAP_LOCK;

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", gau1CurrentModelNumber);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "NO_OF_RADIOS_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", gu4CurrentNoOfRadios);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "CAPWAP_MAC_TYPE_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Mactype);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "CAPWAP_TUNNEL_MODE_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    WSS_WEB_TRC ("Tunnel Mode[0]: %d\n", Tunnel->pu1_OctetList[0]);

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", Tunnel->pu1_OctetList[0]);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    i4OutCome =
        nmhGetFirstIndexFsQAPProfileTable (pNextProfileName, &i4NextIndex);
    if (i4OutCome == SNMP_SUCCESS)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }

    STRCPY (pHttp->au1KeyString, "<! QOS PROFILES>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        if (STRCMP (pCurrentProfileName->pu1_OctetList,
                    pNextProfileName->pu1_OctetList) != 0)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString,
                     "<option value = \"%s\" >%s \n",
                     pNextProfileName->pu1_OctetList,
                     pNextProfileName->pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
        }
        else
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        }
        WSS_WEB_TRC ("pNextProfileName : %s\n",
                     pNextProfileName->pu1_OctetList);

        /* Copy next index to current index */
        STRCPY (pCurrentProfileName->pu1_OctetList,
                pNextProfileName->pu1_OctetList);
        pCurrentProfileName->i4_Length = pNextProfileName->i4_Length;
        MEMSET (pNextProfileName->pu1_OctetList, 0, OCTETSTR_SIZE);
        pNextProfileName->i4_Length = 0;
        i4CurrentIndex = i4NextIndex;
    }
    while (nmhGetNextIndexFsQAPProfileTable
           (pCurrentProfileName, pNextProfileName, i4CurrentIndex,
            &i4NextIndex) == SNMP_SUCCESS);

    STRCPY (pHttp->au1KeyString, "IMAGE_VERSION_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", Imageversion->pu1_OctetList);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    WebnmUnRegisterLock (pHttp);
    CAPWAP_UNLOCK;
    free_octetstring (pCurrentProfileName);
    free_octetstring (pNextProfileName);
    free_octetstring (Softwareversion);
    free_octetstring (Imageversion);
    free_octetstring (QosProfile);
    free_octetstring (Tunnel);
    free_octetstring (ModelNumber);

}

/*********************************************************************
*  Function Name : IssProcessAPModelEntryEditPageSet 
*  Description   : This function processes the set request coming for the  
*                  AP Model Entry page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessAPModelEntryEditPageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE *ModelNumber = NULL;
    tSNMP_OCTET_STRING_TYPE *Tunnel = NULL;
    INT4                macType = 0;
    UINT1               tunnelMode[VLAN_STATIC_MAX_NAME_LEN + 1];
    UINT1               au1ModelName[CLI_MODEL_NAME_LEN];
    UINT1               qosProfile[CLI_MODEL_NAME_LEN];
    UINT1               swVer[CLI_MODEL_NAME_LEN];
    UINT1               imgVer[CLI_MODEL_NAME_LEN];
    UINT4               noOfRadio;
    UINT4               u4ErrorCode;
    INT4                i4ModelNameLen = 0;
    INT4                i4TunnelModeLen = 1;
    UINT4               tunnelModeBit = 0;
    UINT1               au1CurrentTime[ISS_PAGE_STRING_SIZE];
    tCapwapFsWtpModelEntry capwapSetFsWtpModelEntry;
    tCapwapIsSetFsWtpModelEntry capwapIsSetFsWtpModelEntry;
    time_t              t = time (NULL);
    struct tm          *tm = NULL;

    tm = localtime (&t);

    if (tm == NULL)
    {
        return;
    }

    Tunnel = (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (Tunnel == NULL)
    {
        free_octetstring (Tunnel);
        return;
    }
    MEMSET (Tunnel->pu1_OctetList, 0, OCTETSTR_SIZE);

    ModelNumber =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (ModelNumber == NULL)
    {
        free_octetstring (Tunnel);
        return;
    }
    MEMSET (ModelNumber->pu1_OctetList, 0, OCTETSTR_SIZE);

    MEMSET (&capwapSetFsWtpModelEntry, 0, sizeof (tCapwapFsWtpModelEntry));
    MEMSET (&capwapIsSetFsWtpModelEntry, 0,
            sizeof (tCapwapIsSetFsWtpModelEntry));

    MEMSET (&au1ModelName, 0, CLI_MODEL_NAME_LEN);
    MEMSET (&qosProfile, 0, CLI_MODEL_NAME_LEN);
    MEMSET (&swVer, 0, CLI_MODEL_NAME_LEN);
    MEMSET (&imgVer, 0, CLI_MODEL_NAME_LEN);
    MEMSET (&tunnelMode, 0, VLAN_STATIC_MAX_NAME_LEN + 1);
    MEMSET (&au1CurrentTime, 0, ISS_PAGE_STRING_SIZE);

    SPRINTF ((CHR1 *) au1CurrentTime, "%d-%02d-%02d %02d:%02d:%02d",
             tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday, tm->tm_hour,
             tm->tm_min, tm->tm_sec);

    WebnmRegisterLock (pHttp, CapwapMainTaskLock, CapwapMainTaskUnLock);
    CAPWAP_LOCK;

    STRCPY (pHttp->au1Name, "AP_MODEL_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ModelName, pHttp->au1Value, (sizeof (au1ModelName) - 1));
    STRCPY (gau1CurrentModelNumber, au1ModelName);
    STRCPY (pHttp->au1Name, "NO_OF_RADIOS");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    noOfRadio = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "CAPWAP_MAC_TYPE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    macType = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "CAPWAP_TUNNEL_MODE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    tunnelModeBit = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "QosProfile");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (qosProfile, pHttp->au1Value, (sizeof (qosProfile) - 1));

    STRCPY (pHttp->au1Name, "IMAGE_VERSION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (imgVer, pHttp->au1Value, (sizeof (imgVer) - 1));

    i4ModelNameLen = STRLEN (au1ModelName);
    i4TunnelModeLen = 1;

    STRCPY (ModelNumber->pu1_OctetList, au1ModelName);
    ModelNumber->i4_Length = STRLEN (au1ModelName);

    capwapSetFsWtpModelEntry.MibObject.u4FsNoOfRadio = noOfRadio;
    capwapSetFsWtpModelEntry.MibObject.i4FsCapwapWtpMacType = macType;
    STRNCPY (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapWtpModelNumber,
             au1ModelName,
             (sizeof
              (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapWtpModelNumber) -
              1));
    capwapSetFsWtpModelEntry.MibObject.i4FsCapwapWtpModelNumberLen =
        i4ModelNameLen;

    if (capwapSetFsWtpModelEntry.MibObject.i4FsCapwapWtpMacType ==
        CLI_MAC_TYPE_SPLIT)
    {
        capwapSetFsWtpModelEntry.MibObject.au1FsCapwapWtpTunnelMode[0] =
            CLI_TUNNEL_MODE_NATIVE;
    }
    else
    {
        capwapSetFsWtpModelEntry.MibObject.au1FsCapwapWtpTunnelMode[0] =
            tunnelModeBit;
    }
    capwapSetFsWtpModelEntry.MibObject.i4FsCapwapWtpTunnelModeLen =
        i4TunnelModeLen;
    STRCPY (capwapSetFsWtpModelEntry.au1LastUpdated, au1CurrentTime);
    capwapSetFsWtpModelEntry.i4LastUpdatedLen = STRLEN (au1CurrentTime);
    STRNCPY (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapSwVersion,
             swVer,
             (sizeof (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapSwVersion) -
              1));
    capwapSetFsWtpModelEntry.MibObject.i4FsCapwapSwVersionLen = STRLEN (swVer);
    STRNCPY (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapImageName,
             imgVer,
             (sizeof (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapImageName) -
              1));
    capwapSetFsWtpModelEntry.MibObject.i4FsCapwapImageNameLen = STRLEN (imgVer);
    STRNCPY (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapQosProfileName,
             qosProfile,
             (sizeof
              (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapQosProfileName) -
              1));
    capwapSetFsWtpModelEntry.MibObject.i4FsCapwapQosProfileNameLen =
        STRLEN (qosProfile);

    /* Fill the booleans indicating presence of corresponding field */
    capwapIsSetFsWtpModelEntry.bFsCapwapWtpModelNumber = OSIX_TRUE;
    capwapIsSetFsWtpModelEntry.bFsNoOfRadio = OSIX_TRUE;
    capwapIsSetFsWtpModelEntry.bFsCapwapWtpMacType = OSIX_TRUE;
    capwapIsSetFsWtpModelEntry.bFsCapwapWtpTunnelMode = OSIX_TRUE;
    capwapIsSetFsWtpModelEntry.bFsWtpModelRowStatus = OSIX_TRUE;
    capwapIsSetFsWtpModelEntry.bFsCapwapSwVersion = OSIX_TRUE;
    capwapIsSetFsWtpModelEntry.bFsCapwapQosProfileName = OSIX_TRUE;
    capwapIsSetFsWtpModelEntry.bFsCapwapImageName = OSIX_TRUE;

    capwapSetFsWtpModelEntry.MibObject.i4FsWtpModelRowStatus = NOT_IN_SERVICE;

    if (CapwapTestAllFsWtpModelTable (&u4ErrorCode, &capwapSetFsWtpModelEntry,
                                      &capwapIsSetFsWtpModelEntry, OSIX_TRUE,
                                      OSIX_TRUE) != OSIX_SUCCESS)
    {
        printf ("CapwapTestAllFsWtpModelTable FAILED for  first time!\n");
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        free_octetstring (Tunnel);
        free_octetstring (ModelNumber);
        return;
    }

    if (CapwapSetAllFsWtpModelTable (&capwapSetFsWtpModelEntry,
                                     &capwapIsSetFsWtpModelEntry, OSIX_TRUE,
                                     OSIX_TRUE) != OSIX_SUCCESS)
    {
        printf ("CapwapSetAllFsWtpModelTable FAILED for  first time!\n");
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        free_octetstring (Tunnel);
        free_octetstring (ModelNumber);
        return;
    }

    capwapSetFsWtpModelEntry.MibObject.i4FsWtpModelRowStatus = ACTIVE;

    if (CapwapTestAllFsWtpModelTable (&u4ErrorCode, &capwapSetFsWtpModelEntry,
                                      &capwapIsSetFsWtpModelEntry, OSIX_TRUE,
                                      OSIX_TRUE) != OSIX_SUCCESS)
    {
        printf ("CapwapTestAllFsWtpModelTable FAILED for  first time!\n");
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        free_octetstring (Tunnel);
        free_octetstring (ModelNumber);
        return;
    }

    if (CapwapSetAllFsWtpModelTable (&capwapSetFsWtpModelEntry,
                                     &capwapIsSetFsWtpModelEntry, OSIX_TRUE,
                                     OSIX_TRUE) != OSIX_SUCCESS)
    {
        printf ("CapwapSetAllFsWtpModelTable FAILED for  first time!\n");
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        free_octetstring (Tunnel);
        free_octetstring (ModelNumber);
        return;
    }

    WebnmUnRegisterLock (pHttp);
    free_octetstring (Tunnel);
    free_octetstring (ModelNumber);
    CAPWAP_UNLOCK;

}

/*********************************************************************
*  Function Name : IssProcessAPModelEntryPageSet 
*  Description   : This function processes the set request coming for the  
*                  AP Model Entry page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessAPModelEntryPageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE *pModelNumber = NULL, *pQos = NULL, *pSwVer = NULL;
    UINT1               au1ModelName[CLI_MODEL_NAME_LEN];
    UINT1               au1QosProfile[CLI_MODEL_NAME_LEN];
    UINT1               au1SwVer[CLI_MODEL_NAME_LEN];
    UINT1               au1ImgVer[CLI_MODEL_NAME_LEN];
    INT4                i4MacType = 0;
    UINT1               au1TunnelMode[OCTETSTR_SIZE];
    UINT1               au1CurrentTime[ISS_PAGE_STRING_SIZE];
    UINT4               u4NoOfRadios = 0;
    INT4                i4ModelNameLen = 0, i4TunnelModeLen = 0;
    UINT4               u4ErrorCode = 0, u4TunnelModeBit = 0;
    INT4                i4FsWtpModelRowStatus = NOT_IN_SERVICE;

    time_t              t = time (NULL);
    struct tm          *tm = NULL;

    tm = localtime (&t);

    if (tm == NULL)
    {
        return;
    }

    WSS_WEB_TRC (" Entering IssProcessAPModelEntryPageSet \n");

    if (CapwapCheckModuleStatus () == OSIX_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Capwap module is shutdown. Can't configure!");
        return;
    }

    tCapwapFsWtpModelEntry capwapSetFsWtpModelEntry;
    tCapwapIsSetFsWtpModelEntry capwapIsSetFsWtpModelEntry;

    pModelNumber =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (pModelNumber == NULL)
    {
        return;
    }

    pQos = (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (pQos == NULL)
    {
        free_octetstring (pModelNumber);
        return;
    }

    pSwVer = (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (pSwVer == NULL)
    {
        free_octetstring (pModelNumber);
        free_octetstring (pQos);
        return;
    }

    MEMSET (pModelNumber->pu1_OctetList, 0, OCTETSTR_SIZE);
    MEMSET (pQos->pu1_OctetList, 0, OCTETSTR_SIZE);
    MEMSET (pSwVer->pu1_OctetList, 0, OCTETSTR_SIZE);

    pQos->i4_Length = 0;
    pSwVer->i4_Length = 0;
    MEMSET (&capwapSetFsWtpModelEntry, 0, sizeof (tCapwapFsWtpModelEntry));
    MEMSET (&capwapIsSetFsWtpModelEntry, 0,
            sizeof (tCapwapIsSetFsWtpModelEntry));

    MEMSET (&capwapSetFsWtpModelEntry, 0, sizeof (capwapSetFsWtpModelEntry));
    MEMSET (&capwapIsSetFsWtpModelEntry, 0,
            sizeof (capwapIsSetFsWtpModelEntry));

    MEMSET (&au1ModelName, 0, CLI_MODEL_NAME_LEN);
    MEMSET (&au1QosProfile, 0, CLI_MODEL_NAME_LEN);
    MEMSET (&au1SwVer, 0, CLI_MODEL_NAME_LEN);
    MEMSET (&au1ImgVer, 0, CLI_MODEL_NAME_LEN);
    MEMSET (&au1TunnelMode, 0, OCTETSTR_SIZE);
    MEMSET (&au1CurrentTime, 0, ISS_PAGE_STRING_SIZE);

    SPRINTF ((CHR1 *) au1CurrentTime, "%d-%02d-%02d %02d:%02d:%02d",
             tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday, tm->tm_hour,
             tm->tm_min, tm->tm_sec);

    WebnmRegisterLock (pHttp, CapwapMainTaskLock, CapwapMainTaskUnLock);
    CAPWAP_LOCK;

    STRCPY (pHttp->au1Name, "AP_MODEL_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ModelName, pHttp->au1Value, (sizeof (au1ModelName) - 1));
    /*STRCPY(au1ModelName ,  pHttp->au1Value); */

    /* Store the Model Number . It will be used by Radio Table */
    STRNCPY (gau1CurrentModelNumber, au1ModelName,
             (sizeof (gau1CurrentModelNumber) - 1));
    STRCPY (pHttp->au1Name, "NO_OF_RADIOS");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4NoOfRadios = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "CAPWAP_MAC_TYPE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4MacType = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "CAPWAP_TUNNEL_MODE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4TunnelModeBit = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "QosProfile");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1QosProfile, pHttp->au1Value, (sizeof (au1QosProfile) - 1));

    STRCPY (pHttp->au1Name, "IMAGE_VERSION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ImgVer, pHttp->au1Value, (sizeof (au1ImgVer) - 1));

    i4ModelNameLen = STRLEN (au1ModelName);
    i4TunnelModeLen = 1;

    STRCPY (pModelNumber->pu1_OctetList, au1ModelName);
    pModelNumber->i4_Length = i4ModelNameLen;

    /* Check if the profile is already present */
    if ((nmhGetFsWtpModelRowStatus (pModelNumber, &i4FsWtpModelRowStatus)) ==
        SNMP_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Entry already Exists\n");
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        free_octetstring (pModelNumber);
        free_octetstring (pQos);
        free_octetstring (pSwVer);
        return;
    }
    /* Create Model Table */
    /* Fill the fields */
    capwapSetFsWtpModelEntry.MibObject.u4FsNoOfRadio = u4NoOfRadios;
    capwapSetFsWtpModelEntry.MibObject.i4FsCapwapWtpMacType = i4MacType;
    capwapSetFsWtpModelEntry.MibObject.i4FsWtpModelRowStatus = CREATE_AND_GO;
    STRNCPY (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapWtpModelNumber,
             au1ModelName,
             (sizeof
              (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapWtpModelNumber) -
              1));
    capwapSetFsWtpModelEntry.MibObject.i4FsCapwapWtpModelNumberLen =
        i4ModelNameLen;
    capwapSetFsWtpModelEntry.MibObject.au1FsCapwapWtpTunnelMode[0] =
        u4TunnelModeBit;
    capwapSetFsWtpModelEntry.MibObject.i4FsCapwapWtpTunnelModeLen =
        i4TunnelModeLen;
    STRCPY (capwapSetFsWtpModelEntry.au1LastUpdated, au1CurrentTime);
    capwapSetFsWtpModelEntry.i4LastUpdatedLen = STRLEN (au1CurrentTime);
    STRNCPY (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapSwVersion,
             au1SwVer,
             (sizeof (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapSwVersion) -
              1));
    capwapSetFsWtpModelEntry.MibObject.i4FsCapwapSwVersionLen =
        STRLEN (au1SwVer);
    STRNCPY (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapImageName, au1ImgVer,
             (sizeof (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapImageName) -
              1));
    capwapSetFsWtpModelEntry.MibObject.i4FsCapwapImageNameLen =
        STRLEN (au1ImgVer);
    STRNCPY (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapQosProfileName,
             au1QosProfile,
             (sizeof
              (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapQosProfileName) -
              1));
    capwapSetFsWtpModelEntry.MibObject.i4FsCapwapQosProfileNameLen =
        STRLEN (au1QosProfile);

    /* Fill the booleans indicating presence of corresponding field */
    capwapIsSetFsWtpModelEntry.bFsCapwapWtpModelNumber = OSIX_TRUE;
    capwapIsSetFsWtpModelEntry.bFsNoOfRadio = OSIX_TRUE;

    capwapIsSetFsWtpModelEntry.bFsCapwapSwVersion = OSIX_TRUE;
    capwapIsSetFsWtpModelEntry.bFsCapwapQosProfileName = OSIX_TRUE;
    capwapIsSetFsWtpModelEntry.bFsCapwapImageName = OSIX_TRUE;

    if ((i4MacType == CLI_MAC_TYPE_SPLIT) || (i4MacType == CLI_MAC_TYPE_LOCAL))
    {
        capwapIsSetFsWtpModelEntry.bFsCapwapWtpMacType = OSIX_TRUE;
    }
    if ((capwapSetFsWtpModelEntry.MibObject.au1FsCapwapWtpTunnelMode[0] ==
         CLI_TUNNEL_MODE_BRIDGE) ||
        (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapWtpTunnelMode[0] ==
         CLI_TUNNEL_MODE_DOT3) ||
        (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapWtpTunnelMode[0] ==
         CLI_TUNNEL_MODE_NATIVE))
    {
        capwapIsSetFsWtpModelEntry.bFsCapwapWtpTunnelMode = OSIX_TRUE;
    }
    capwapIsSetFsWtpModelEntry.bFsWtpModelRowStatus = OSIX_TRUE;

    if (CapwapTestAllFsWtpModelTable (&u4ErrorCode, &capwapSetFsWtpModelEntry,
                                      &capwapIsSetFsWtpModelEntry, OSIX_TRUE,
                                      OSIX_TRUE) != OSIX_SUCCESS)
    {
        /* Delete Profile */
        nmhSetFsWtpModelRowStatus (pModelNumber, DESTROY);
        free_octetstring (pModelNumber);
        free_octetstring (pQos);
        free_octetstring (pSwVer);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        return;
    }

    if (CapwapSetAllFsWtpModelTable (&capwapSetFsWtpModelEntry,
                                     &capwapIsSetFsWtpModelEntry, OSIX_TRUE,
                                     OSIX_TRUE) != OSIX_SUCCESS)
    {
        printf ("CapwapSetAllFsWtpModelTable FAILED!\n");
        /* Delete Profile */
        nmhSetFsWtpModelRowStatus (pModelNumber, DESTROY);
        free_octetstring (pModelNumber);
        free_octetstring (pQos);
        free_octetstring (pSwVer);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        return;
    }
    WebnmUnRegisterLock (pHttp);
    CAPWAP_UNLOCK;
    IssProcessAPModelEntryPageGet (pHttp, pModelNumber);
    free_octetstring (pModelNumber);
    free_octetstring (pQos);
    free_octetstring (pSwVer);
    return;
}

/*********************************************************************
*  Function Name : IssProcessAPModelTablePage 
*  Description   : This function processes the request coming for the
*                  AP Model Table page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessAPModelTablePage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessAPModelTablePageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessAPModelTablePageSet (pHttp);
    }
    return;
}

/*********************************************************************
*  Function Name : IssProcessAPModelTablePageGet
*  Description   : This function processes the get request coming for the  
*                  AP Model Table page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessAPModelTablePageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE *currentModelNumber = NULL, *nextModelNumber = NULL;
    tSNMP_MULTI_DATA_TYPE *pu1DataString = NULL;
    INT4                i4OutCome = 0;
    UINT4               u4Temp = 0, u4FsNoOfRadios = 0;
    tSNMP_OCTET_STRING_TYPE *pLastUpdated = NULL;
    tCapwapFsWtpModelEntry capwapFsWtpModelEntry;

    if (CapwapCheckModuleStatus () == OSIX_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Capwap module is shutdown. Can't configure!");
        return;
    }

    if ((pu1DataString = WebnmAllocMultiData ()) == NULL)
    {
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        free_octetstring (pLastUpdated);
        return;
    }

    currentModelNumber =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (currentModelNumber == NULL)
    {
        /*free_octetstring (currentModelNumber); */
        return;
    }
    nextModelNumber =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (nextModelNumber == NULL)
    {
        free_octetstring (currentModelNumber);
        return;
    }
    pLastUpdated =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (pLastUpdated == NULL)
    {
        free_octetstring (currentModelNumber);
        free_octetstring (nextModelNumber);
        return;
    }
    MEMSET (currentModelNumber->pu1_OctetList, 0, OCTETSTR_SIZE);
    MEMSET (nextModelNumber->pu1_OctetList, 0, OCTETSTR_SIZE);
    MEMSET (pLastUpdated->pu1_OctetList, 0, OCTETSTR_SIZE);
    MEMSET (&capwapFsWtpModelEntry, 0, sizeof (tCapwapFsWtpModelEntry));
    currentModelNumber->i4_Length = 0;
    nextModelNumber->i4_Length = 0;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = pHttp->i4Write;
    i4OutCome = nmhGetFirstIndexFsWtpModelTable (nextModelNumber);
    WebnmRegisterLock (pHttp, CapwapMainTaskLock, CapwapMainTaskUnLock);
    CAPWAP_LOCK;

    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html +
                                          pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        free_octetstring (currentModelNumber);
        free_octetstring (nextModelNumber);
        free_octetstring (pLastUpdated);
        return;
    }
    else
    {
        do
        {
            pHttp->i4Write = u4Temp;

            STRCPY (pu1DataString->pOctetStrValue->pu1_OctetList,
                    nextModelNumber->pu1_OctetList);
            pu1DataString->pOctetStrValue->i4_Length =
                nextModelNumber->i4_Length;

            STRCPY (pHttp->au1KeyString, "AP_MODEL_NAME_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            WebnmSendToSocket (pHttp, "AP_MODEL_NAME_KEY", pu1DataString,
                               ENM_DISPLAYSTRING);

            STRCPY (pHttp->au1KeyString, "NO_OF_RADIOS_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            MEMCPY (capwapFsWtpModelEntry.MibObject.au1FsCapwapWtpModelNumber,
                    nextModelNumber->pu1_OctetList, nextModelNumber->i4_Length);

            capwapFsWtpModelEntry.MibObject.i4FsCapwapWtpModelNumberLen =
                nextModelNumber->i4_Length;
            capwapFsWtpModelEntry.MibObject.i4FsWtpModelRowStatus = ACTIVE;
            if (CapwapGetAllFsWtpModelTable (&capwapFsWtpModelEntry) ==
                OSIX_SUCCESS)
            {
                u4FsNoOfRadios = capwapFsWtpModelEntry.MibObject.u4FsNoOfRadio;

            }
            nmhGetLastUpdated (nextModelNumber, pLastUpdated);
            MEMCPY (pLastUpdated->pu1_OctetList,
                    capwapFsWtpModelEntry.au1LastUpdated,
                    capwapFsWtpModelEntry.i4LastUpdatedLen);
            pLastUpdated->i4_Length = capwapFsWtpModelEntry.i4LastUpdatedLen;

            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4FsNoOfRadios);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            HTTP_STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            STRCPY (pHttp->au1KeyString, "LAST_UPDATED_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     pLastUpdated->pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            HTTP_STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

            /* Copy next index to current index */
            STRCPY (currentModelNumber->pu1_OctetList,
                    nextModelNumber->pu1_OctetList);
            currentModelNumber->i4_Length = nextModelNumber->i4_Length;

        }
        while (nmhGetNextIndexFsWtpModelTable
               (currentModelNumber, nextModelNumber) == SNMP_SUCCESS);

        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));

    }
    WebnmUnRegisterLock (pHttp);
    CAPWAP_UNLOCK;
    free_octetstring (currentModelNumber);
    free_octetstring (nextModelNumber);
    free_octetstring (pLastUpdated);
}

/*********************************************************************
*  Function Name : IssProcessAPModelTablePageSet 
*  Description   : This function processes the set request coming for
*                  AP Model Table page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessAPModelTablePageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE *ModelNumber;
    UINT1               au1ModelName[CLI_MODEL_NAME_LEN];
    UINT4               noOfRadio, lastUpdated;
    UINT4               u4ErrorCode = 0;
    INT4                i4ModelNameLen = 0, i4OutCome = 0;
    UINT4               currentProfileId = 0, nextProfileId = 0;
    BOOL1               matched_profile = FALSE;

    if (CapwapCheckModuleStatus () == OSIX_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Capwap module is shutdown. Can't configure!");
        return;
    }

    ModelNumber =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (ModelNumber == NULL)
    {
        return;
    }
    MEMSET (ModelNumber->pu1_OctetList, 0, OCTETSTR_SIZE);

    MEMSET (&au1ModelName, 0, CLI_MODEL_NAME_LEN);

    WebnmRegisterLock (pHttp, CapwapMainTaskLock, CapwapMainTaskUnLock);
    CAPWAP_LOCK;

    STRCPY (pHttp->au1Name, "AP_MODEL_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ModelName, pHttp->au1Value, (sizeof (au1ModelName) - 1));

    STRCPY (pHttp->au1Name, "NO_OF_RADIOS");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    noOfRadio = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "LAST_UPDATED");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    lastUpdated = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    WSS_WEB_TRC ("Model Name:%s\n", au1ModelName);
    WSS_WEB_TRC ("NoOfRadio:%d\n", noOfRadio);
    WSS_WEB_TRC ("LastUpdated:%d\n", lastUpdated);
    WSS_WEB_TRC ("Action:%s\n", pHttp->au1Value);

    if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {
        i4ModelNameLen = STRLEN (au1ModelName);

        i4OutCome = nmhGetFirstIndexCapwapBaseWtpProfileTable (&nextProfileId);
        if (i4OutCome == SNMP_FAILURE)
        {
            matched_profile = FALSE;
        }
        else
        {
            do
            {
                currentProfileId = nextProfileId;

                if (currentProfileId != 0)
                {
                    nmhGetCapwapBaseWtpProfileWtpModelNumber (currentProfileId,
                                                              ModelNumber);
                    if (!STRCMP (ModelNumber->pu1_OctetList, au1ModelName))
                    {
                        matched_profile = TRUE;
                    }
                }
                currentProfileId = nextProfileId;

            }
            while (nmhGetNextIndexCapwapBaseWtpProfileTable (currentProfileId,
                                                             &nextProfileId));
        }
        if (!matched_profile)
        {
            STRCPY (ModelNumber->pu1_OctetList, au1ModelName);
            ModelNumber->i4_Length = i4ModelNameLen;
            if (nmhTestv2FsWtpModelRowStatus (&u4ErrorCode,
                                              ModelNumber,
                                              DESTROY) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
                WebnmUnRegisterLock (pHttp);
                CAPWAP_UNLOCK;
                free_octetstring (ModelNumber);
                return;
            }
            if (nmhSetFsWtpModelRowStatus (ModelNumber, DESTROY) ==
                SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
                WebnmUnRegisterLock (pHttp);
                CAPWAP_UNLOCK;
                free_octetstring (ModelNumber);
                return;
            }

        }
        else
        {
            IssSendError (pHttp, (CONST INT1 *) "Associated Profile Exists\n");
            WebnmUnRegisterLock (pHttp);
            CAPWAP_UNLOCK;
            free_octetstring (ModelNumber);
            return;
        }
    }
    else if (STRCMP (pHttp->au1Value, "Edit") == 0)
    {
        MEMCPY (gau1CurrentModelNumber, au1ModelName,
                MEM_MAX_BYTES (sizeof (gau1CurrentModelNumber),
                               STRLEN (au1ModelName)));
        gu4CurrentNoOfRadios = noOfRadio;
    }
    else
    {
        printf ("Unsupported Action\n");
        IssSendError (pHttp, (CONST INT1 *) "Invalid Operation\n");
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        free_octetstring (ModelNumber);
        return;
    }
    WebnmUnRegisterLock (pHttp);
    CAPWAP_UNLOCK;
    free_octetstring (ModelNumber);
    IssProcessAPModelTablePageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssProcessRadioTablePage 
*  Description   : This function processes the request coming for the
*                 Radio Table page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessRadioTablePage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessRadioTablePageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessRadioTablePageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name :IssProcessRadioTablePageGet 
*  Description   : This function processes the get request coming for the  
*                  Radio Table page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessRadioTablePageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    INT4                i4RadioType = 0, i4RadioStatus = 0;
    tSNMP_OCTET_STRING_TYPE *currentModelNumber = NULL, *nextModelNumber = NULL;
    UINT4               u4CurrentRadioId = 1, u4NextRadioId = 1;
    tSNMP_MULTI_DATA_TYPE *pu1DataString = NULL;
    UINT4               au4RadioType[MAX_RADIO_TYPE];
    tCapwapFsWtpRadioEntry capwapFsWtpRadioEntry;
    au4RadioType[0] = 1;
    au4RadioType[1] = 2;
    au4RadioType[2] = 4;
    au4RadioType[3] = 5;
    au4RadioType[4] = 10;
    au4RadioType[5] = 13;
    MEMSET (&capwapFsWtpRadioEntry, 0, sizeof (tCapwapFsWtpRadioEntry));

    if ((pu1DataString = WebnmAllocMultiData ()) == NULL)
    {
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        return;
    }

    currentModelNumber =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (currentModelNumber == NULL)
    {
        return;
    }
    nextModelNumber =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (nextModelNumber == NULL)
    {
        free_octetstring (currentModelNumber);
        return;
    }

    MEMSET (currentModelNumber->pu1_OctetList, 0, OCTETSTR_SIZE);
    MEMSET (nextModelNumber->pu1_OctetList, 0, OCTETSTR_SIZE);
    currentModelNumber->i4_Length = 0;
    nextModelNumber->i4_Length = 0;

    WSS_WEB_TRC ("CurrentModel Name : %s\n", gau1CurrentModelNumber);
    WSS_WEB_TRC ("NextModel Name Length : %d\n", OCTETSTR_SIZE);

    WebnmRegisterLock (pHttp, CapwapMainTaskLock, CapwapMainTaskUnLock);
    CAPWAP_LOCK;
    /*Copy the stored Model Number which corresponds to selected entry */
    if (bIsEdit == OSIX_TRUE)
    {
        STRCPY (nextModelNumber->pu1_OctetList, gau1CurrentModelNumber);
        nextModelNumber->i4_Length = STRLEN (gau1CurrentModelNumber);
        bIsEdit = OSIX_FALSE;
    }
    else
    {
        STRCPY (nextModelNumber->pu1_OctetList, gau1CurrentModelNumber);
        nextModelNumber->i4_Length = STRLEN (gau1CurrentModelNumber);
    }

    /* Copy next index to current index */
    STRCPY (currentModelNumber->pu1_OctetList, nextModelNumber->pu1_OctetList);
    currentModelNumber->i4_Length = nextModelNumber->i4_Length;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = pHttp->i4Write;

    do
    {
        pHttp->i4Write = u4Temp;

        /* Break out if the Current and Next Model Number differ */
        if (STRCMP
            (currentModelNumber->pu1_OctetList, nextModelNumber->pu1_OctetList))
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html +
                                              pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            break;
        }

        /*Sends the Value for the Index */
        pu1DataString->i4_SLongValue = u4NextRadioId;
        STRCPY (pHttp->au1KeyString, "RADIO_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSendToSocket (pHttp, "RADIO_ID_KEY", pu1DataString, ENM_INTEGER32);

        MEMCPY (capwapFsWtpRadioEntry.MibObject.au1FsCapwapWtpModelNumber,
                nextModelNumber->pu1_OctetList, nextModelNumber->i4_Length);
        capwapFsWtpRadioEntry.MibObject.i4FsCapwapWtpModelNumberLen =
            nextModelNumber->i4_Length;
        capwapFsWtpRadioEntry.MibObject.u4FsNumOfRadio = u4NextRadioId;

        if (CapwapGetAllFsWtpRadioTable (&capwapFsWtpRadioEntry) ==
            OSIX_SUCCESS)
        {

            i4RadioType = capwapFsWtpRadioEntry.MibObject.i4FsWtpRadioType;
            i4RadioStatus =
                capwapFsWtpRadioEntry.MibObject.i4FsRadioAdminStatus;
        }

        STRCPY (pHttp->au1KeyString, "RADIO_TYPE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RadioType);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString, "RADIO_ADMIN_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RadioStatus);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        HTTP_STRLEN (pHttp->au1DataString));

        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        u4CurrentRadioId = u4NextRadioId;

    }
    while (nmhGetNextIndexFsWtpRadioTable (currentModelNumber, nextModelNumber,
                                           u4CurrentRadioId,
                                           &u4NextRadioId) == SNMP_SUCCESS);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    WebnmUnRegisterLock (pHttp);
    CAPWAP_UNLOCK;
    free_octetstring (currentModelNumber);
    free_octetstring (nextModelNumber);
}

/*********************************************************************
*  Function Name :IssProcessRadioTablePageSet  
*  Description   : This function processes the get request coming for the  
*                  Radio Table page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessRadioTablePageSet (tHttp * pHttp)
{
    INT4                radioId = 0, radioType = 0, radioAdminStatus = 0;
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE *currentModelNumber = NULL;
    INT4                gau1CurrentModelNumber_Len =
        STRLEN (gau1CurrentModelNumber);
    tCapwapFsWtpModelEntry capwapSetFsWtpModelEntry;
    tCapwapIsSetFsWtpModelEntry capwapIsSetFsWtpModelEntry;
    UINT1               au1CurrentTime[ISS_PAGE_STRING_SIZE];

    time_t              t = time (NULL);
    struct tm          *tm = NULL;

    tm = localtime (&t);

    if (tm == NULL)
    {
        return;
    }

    MEMSET (&au1CurrentTime, 0, ISS_PAGE_STRING_SIZE);

    SPRINTF ((CHR1 *) au1CurrentTime, "%d-%02d-%02d %02d:%02d:%02d",
             tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday, tm->tm_hour,
             tm->tm_min, tm->tm_sec);

    currentModelNumber =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (currentModelNumber == NULL)
    {
        return;
    }
    MEMSET (currentModelNumber->pu1_OctetList, 0, OCTETSTR_SIZE);
    currentModelNumber->i4_Length = 0;
    STRCPY (currentModelNumber->pu1_OctetList, gau1CurrentModelNumber);
    currentModelNumber->i4_Length = gau1CurrentModelNumber_Len;

    WebnmRegisterLock (pHttp, CapwapMainTaskLock, CapwapMainTaskUnLock);
    CAPWAP_LOCK;
    MEMSET (&capwapSetFsWtpModelEntry, 0, sizeof (tCapwapFsWtpModelEntry));
    MEMSET (&capwapIsSetFsWtpModelEntry, 0,
            sizeof (tCapwapIsSetFsWtpModelEntry));

    STRCPY (pHttp->au1Name, "RADIO_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    radioId = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "RADIO_TYPE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    radioType = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "RADIO_ADMIN_STATUS");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    radioAdminStatus = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    WSS_WEB_TRC ("radioId : %d\n", radioId);
    WSS_WEB_TRC ("radioType : %d\n", radioType);
    WSS_WEB_TRC ("radioAdminStatus : %d\n", radioAdminStatus);

    if (nmhTestv2FsWtpRadioType
        (&u4ErrorCode, currentModelNumber, radioId, radioType) != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        free_octetstring (currentModelNumber);
        return;
    }
    if (nmhSetFsWtpRadioType (currentModelNumber, radioId, radioType) !=
        SNMP_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        free_octetstring (currentModelNumber);
        return;
    }
    if (nmhTestv2FsRadioAdminStatus
        (&u4ErrorCode, currentModelNumber, radioId,
         radioAdminStatus) != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        free_octetstring (currentModelNumber);
        return;
    }
    if (nmhSetFsRadioAdminStatus (currentModelNumber, radioId, radioAdminStatus)
        != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        free_octetstring (currentModelNumber);
        return;
    }

    capwapSetFsWtpModelEntry.MibObject.i4FsWtpModelRowStatus = ACTIVE;
    MEMCPY
        (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapWtpModelNumber,
         currentModelNumber->pu1_OctetList, currentModelNumber->i4_Length);
    capwapSetFsWtpModelEntry.MibObject.i4FsCapwapWtpModelNumberLen =
        gau1CurrentModelNumber_Len;

    capwapIsSetFsWtpModelEntry.bFsWtpModelRowStatus = OSIX_TRUE;
    capwapIsSetFsWtpModelEntry.bFsCapwapWtpModelNumber = OSIX_TRUE;

    STRCPY (capwapSetFsWtpModelEntry.au1LastUpdated, au1CurrentTime);
    capwapSetFsWtpModelEntry.i4LastUpdatedLen = STRLEN (au1CurrentTime);

    if (CapwapTestAllFsWtpModelTable (&u4ErrorCode, &capwapSetFsWtpModelEntry,
                                      &capwapIsSetFsWtpModelEntry, OSIX_TRUE,
                                      OSIX_TRUE) != OSIX_SUCCESS)
    {
        printf ("CapwapTestAllFsWtpModelTable FAILED !\n");
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        free_octetstring (currentModelNumber);
        return;
    }

    if (CapwapSetAllFsWtpModelTable (&capwapSetFsWtpModelEntry,
                                     &capwapIsSetFsWtpModelEntry, OSIX_TRUE,
                                     OSIX_TRUE) != OSIX_SUCCESS)
    {
        printf ("CapwapSetAllFsWtpModelTable FAILED !\n");
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        free_octetstring (currentModelNumber);
        return;
    }

    WebnmUnRegisterLock (pHttp);
    CAPWAP_UNLOCK;
    free_octetstring (currentModelNumber);
}
#endif
