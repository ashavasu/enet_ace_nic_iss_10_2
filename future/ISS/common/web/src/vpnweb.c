
/* $Id: vpnweb.c,v 1.19 2017/06/19 12:25:37 siva Exp $*/

#ifdef VPN_WANTED
#ifndef __VPNWEB_C__
#define __VPNWEB_C__

#include "webiss.h"
#include "isshttp.h"
#include "msr.h"
#include "cli.h"

#include "utilcli.h"
#include "webiss.h"
#include "lr.h"
#include "cfa.h"
#include "snmcdefn.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "fswebnm.h"
#include "vpninc.h"
#include "webiss.h"
#include "vpnclip.h"
#include "fsvpnwr.h"
#include "fsvpnextn.h"
#include "vpnweb.h"

/*********************************************************************
*  Function Name : IssProcessVpnGlobalConfPageGet
*  Description   : This function processes the get request coming for the
*                  Global Configuration web page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessVpnGlobalConfPageGet (tHttp * pHttp)
{
    tVpnIdInfo         *pVpnIdInfoNode = NULL;
    UINT4               u4Temp;

    pHttp->i4Write = 0;

    WebnmRegisterLock (pHttp, VpnDsLock, VpnDsUnLock);
    VpnDsLock ();

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    TMO_SLL_Scan (&gVpnRemoteIdList, pVpnIdInfoNode, tVpnIdInfo *)
    {
        pHttp->i4Write = (INT4) u4Temp;

        STRCPY (pHttp->au1KeyString, "REM_TYPE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        switch (VPN_ID_TYPE (pVpnIdInfoNode))
        {
            case VPN_ID_TYPE_IPV4:
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "IPv4");
                break;
            case VPN_ID_TYPE_FQDN:
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Fqdn");
                break;
            case VPN_ID_TYPE_EMAIL:
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "E-Mail");
                break;
            case VPN_ID_TYPE_KEYID:
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Key-Id");
                break;
            case VPN_ID_TYPE_IPV6:
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "IPv6");
                break;
            default:
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Unknown");
                break;
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "REM_VALUE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                 VPN_ID_VALUE (pVpnIdInfoNode));
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

    }
    if (pVpnIdInfoNode == NULL)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }

    VpnDsUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
*  Function Name : IssProcessVpnGlobalConfPageSet
*  Description   : This function processes the set request coming for the
*                  Global Configuration web page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessVpnGlobalConfPageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE IdValue;
    UINT1               au1IdValue[WEB_VPN_MAX_NAME_LEN + 1];
    UINT1               au1PresharedKey[WEB_PRESHARED_MAX_KEY_SIZE + 1];
    UINT4               u4SnmpErrorStatus = 0;
    INT4                i4IdType = 0;
    tIp6Addr            Ipv6Address;
#ifdef CLI_WANTED
    UINT4               u4AuthType = 0;
#endif

    MEMSET (au1IdValue, '\0', WEB_VPN_MAX_NAME_LEN + 1);
    MEMSET (au1PresharedKey, '\0', WEB_PRESHARED_MAX_KEY_SIZE + 1);
    MEMSET (&Ipv6Address, 0, sizeof (tIp6Addr));
    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Add") == 0)
    {
        STRCPY (pHttp->au1Name, "REM_ID_TYPE");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4IdType = (INT4) (UINT4) ATOI (pHttp->au1Value);

        STRCPY (pHttp->au1Name, "REM_ID_VALUE");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        issDecodeSpecialChar (pHttp->au1Value);

        if (i4IdType == 5)
        {
            if (INET_ATON6 (pHttp->au1Value, &Ipv6Address) == 0)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid IPv6 address");
                return;
            }
        }
        STRNCPY (au1IdValue, pHttp->au1Value, WEB_VPN_MAX_NAME_LEN);

        STRCPY (pHttp->au1Name, "PRE_SHARED");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        issDecodeSpecialChar (pHttp->au1Value);
        STRNCPY (au1PresharedKey, pHttp->au1Value, WEB_PRESHARED_MAX_KEY_SIZE);

        STRCPY (pHttp->au1Name, "AUTH_TYPE");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);

#ifdef CLI_WANTED

        u4AuthType = (UINT4) ATOI (pHttp->au1Value);
        if (VpnAddRemoteIdInfo (WEB_CLI_HANDLE, i4IdType, au1IdValue,
                                u4AuthType, au1PresharedKey) == CLI_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unable To Add Remote Identity Information");
            return;
        }
#endif
        IssProcessVpnGlobalConfPageGet (pHttp);
    }

    if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {
        STRCPY (pHttp->au1Name, "REM_ID_TYPE");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        if (STRCMP (pHttp->au1Value, "IPv4") == 0)
        {
            i4IdType = 1;
        }
        else if (STRCMP (pHttp->au1Value, "Fqdn") == 0)
        {
            i4IdType = 2;
        }
        else if (STRCMP (pHttp->au1Value, "E-Mail") == 0)
        {
            i4IdType = 3;
        }
        else if (STRCMP (pHttp->au1Value, "Key-Id") == 0)
        {
            i4IdType = 11;
        }
        else if (STRCMP (pHttp->au1Value, "IPv6") == 0)
        {
            i4IdType = 5;
        }

        STRCPY (pHttp->au1Name, "REM_ID_VALUE");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        issDecodeSpecialChar (pHttp->au1Value);

        IdValue.i4_Length = (INT4) STRLEN (pHttp->au1Value);
        IdValue.pu1_OctetList = pHttp->au1Value;
        if (nmhTestv2FsVpnRemoteIdStatus (&u4SnmpErrorStatus, i4IdType,
                                          &IdValue, DESTROY) == SNMP_FAILURE)
        {
            switch (u4SnmpErrorStatus)
            {
                case SNMP_ERR_NO_CREATION:
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Remote Identity Does not Exists");
                    break;
                }
                case SNMP_ERR_INCONSISTENT_VALUE:
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Failed to delete the Id since there is an "
                                  " active policy using this Remote Id");
                    break;
                }
                default:
                    break;
            }
            return;
        }
        if (nmhSetFsVpnRemoteIdStatus (i4IdType, &IdValue, DESTROY)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Unable to Delete the Row");
            return;
        }

        IssProcessVpnGlobalConfPageGet (pHttp);
    }
    return;
}

/*********************************************************************
*  Function Name : IssProcessIkeConfPageGet 
*  Description   : This function processes the get request coming for the  
*                  IpSec/Ike web page.   
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessIkeConfPageGet (tHttp * pHttp, UINT1 *pu1PolicyName, UINT1 u1DelFlag)
{
    tSNMP_OCTET_STRING_TYPE *pPolicyName = NULL;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    tSNMP_OCTET_STRING_TYPE *pPortRange = NULL;
    tVpnPolicy         *pVpnPolicy = NULL;
    INT4                i4Len = 0;
    UINT4               u4RetVal = 0;
    INT4                i4RetVal = 0;
    INT4                i4RetVal1 = 0;
    UINT1               au1TempStr[WEB_MAX_NAME_LENGTH];
    UINT1               au1String[20];
    UINT1               au1IpAddr[IP6_ADDR_SIZE];
    UINT4               u4PrefixLen = 0;
    INT4                i4AddrType = 0;
    UINT4               u4BitMap;
    tIp6Addr            Ipv6Address;

    pHttp->i4Write = 0;

    WebnmRegisterLock (pHttp, VpnDsLock, VpnDsUnLock);
    VpnDsLock ();

    MEMSET (au1TempStr, 0, sizeof (au1TempStr));
    MEMSET (au1IpAddr, '\0', IP6_ADDR_SIZE);

    if (u1DelFlag == 1)
    {
        /*This is called for Get Operation when a policy is deleted. *
         * pHttp->au1Value is set to empty string ,since any value 
         * present in it may be taken for calculation which is not 
         * desired */
        VpnPrintPolicies (pHttp, NULL, au1TempStr, WEB_VPN_PRESHARED);
        pHttp->au1Value[0] = '\0';
    }
    else
    {
        STRCPY (pHttp->au1Name, "CURR_POLICY");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_FAILURE)
        {
            if (pu1PolicyName == NULL)
            {
                /*When pu1PolicyName is NULL , it is called for Normal Get 
                 * Operation from issweb.c.*/
                VpnPrintPolicies (pHttp, NULL, au1TempStr, WEB_VPN_PRESHARED);
            }
            else
            {
                /*When pu1PolicyName is not NULL , it is called for Get Operation
                 * when a new policy is created and called from the set routinue.*/
                VpnPrintPolicies (pHttp, pu1PolicyName, NULL,
                                  WEB_VPN_PRESHARED);

                /*Copying the PolicyName into temporary buffer so as to display the
                 * policy configuration.*/
                MEMSET (au1TempStr, '\0', WEB_MAX_NAME_LENGTH);
                STRNCPY (au1TempStr, pu1PolicyName, (WEB_MAX_NAME_LENGTH - 1));
            }
        }
        else
        {
            /*This is called for Get Operation for displaying the policy configuration. */
            issDecodeSpecialChar (pHttp->au1Value);
            VpnPrintPolicies (pHttp, pHttp->au1Value, NULL, WEB_VPN_PRESHARED);
            MEMSET (au1TempStr, '\0', WEB_MAX_NAME_LENGTH);
            STRNCPY (au1TempStr, pHttp->au1Value, (WEB_MAX_NAME_LENGTH - 1));
        }
    }

    VpnDsUnLock ();
    WebnmUnRegisterLock (pHttp);
    u4BitMap = VPN_WEB_ETH_TYPE | VPN_WEB_PPP_TYPE | VPN_WEB_MP_TYPE;
    SecPrintIpInterfaces (pHttp, u4BitMap, VPN_WEB_NETWORK_TYPE_WAN,
                          VPN_WEB_WAN_TYPE_PUBLIC);
    WebnmRegisterLock (pHttp, VpnDsLock, VpnDsUnLock);
    VpnDsLock ();

    /*If a new policy is created or displaying the existing policy , then it displays the 
     * configuration for that policy.*/

    if (au1TempStr[0] != '\0')
    {
        i4Len = (INT4) STRLEN (au1TempStr);

        if ((pPolicyName = allocmem_octetstring (i4Len + 1)) == NULL)
        {
            VpnDsUnLock ();
            WebnmUnRegisterLock (pHttp);
            return;
        }
        MEMSET (pPolicyName->pu1_OctetList, 0, (size_t) (i4Len + 1));
        MEMCPY (pPolicyName->pu1_OctetList, au1TempStr, i4Len);
        pPolicyName->i4_Length = i4Len;
        pVpnPolicy =
            VpnSnmpGetPolicy (pPolicyName->pu1_OctetList,
                              pPolicyName->i4_Length);
        if (pVpnPolicy == NULL)
        {
            free_octetstring (pPolicyName);
            VpnDsUnLock ();
            WebnmUnRegisterLock (pHttp);
            return;
        }

        /*Displaying Interface for which policy is applied  */

        nmhGetFsVpnPolicyIntfIndex (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "IP_INTERFACES_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /* Displaying VPN Status */
        nmhGetFsVpnPolicyRowStatus (pPolicyName, &i4RetVal);
        if (i4RetVal == ACTIVE)
        {
            i4RetVal = VPN_POLICY_ENABLE;
            STRCPY (pHttp->au1KeyString, "VPN_STATUS_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

        }
        else
        {
            i4RetVal = VPN_POLICY_DISABLE;
            STRCPY (pHttp->au1KeyString, "VPN_STATUS_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

        }

        OctetStr.i4_Length = IP6_ADDR_SIZE;
        OctetStr.pu1_OctetList = au1IpAddr;
        nmhGetFsVpnRemoteTunTermAddr (pPolicyName, &OctetStr);

        if (OctetStr.i4_Length == IPVX_IPV4_ADDR_LEN)
        {
            i4AddrType = 1;
            STRCPY (pHttp->au1KeyString, "ADDR_TYPE_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4AddrType);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            /* Displaying the IPSEC Gateway IP Address */
            MEMCPY (&u4RetVal, OctetStr.pu1_OctetList, sizeof (UINT4));
            STRCPY (pHttp->au1KeyString, "IPSEC_GW_IP_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMSET (au1String, 0, sizeof (au1String));
            VPN_INET_NTOA (au1String, u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
        }
        else if (OctetStr.i4_Length == IPVX_IPV6_ADDR_LEN)
        {
            i4AddrType = 2;
            STRCPY (pHttp->au1KeyString, "ADDR_TYPE_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4AddrType);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "IPSEC_GW_IPV6_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMCPY (&Ipv6Address, OctetStr.pu1_OctetList, IP6_ADDR_SIZE);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     Ip6PrintAddr (&Ipv6Address));
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
        }

        /* Displaying IKE Version */

        STRCPY (pHttp->au1KeyString, "IKE_VER_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "0x%x",
                 pVpnPolicy->u1VpnIkeVer);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        nmhGetFsVpnProtectNetworkType (pPolicyName, &i4AddrType);

        if (i4AddrType == WEB_VPN_IKE_KEY_IPV4)
        {
            /* Displaying the Local Protected Network IP Address */

            OctetStr.i4_Length = sizeof (UINT4);
            OctetStr.pu1_OctetList = au1IpAddr;
            nmhGetFsVpnLocalProtectNetwork (pPolicyName, &OctetStr);
            MEMCPY (&u4RetVal, OctetStr.pu1_OctetList, sizeof (UINT4));
            STRCPY (pHttp->au1KeyString, "LCL_IP_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMSET (au1String, 0, sizeof (au1String));
            VPN_INET_NTOA (au1String, u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            /* Displaying Local Protected Network Mask */

            nmhGetFsVpnLocalProtectNetworkPrefixLen (pPolicyName, &u4PrefixLen);
            IPV4_MASKLEN_TO_MASK (u4RetVal, u4PrefixLen);
            STRCPY (pHttp->au1KeyString, "LCL_MASK_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMSET (au1String, 0, sizeof (au1String));
            VPN_INET_NTOA (au1String, u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            /* Displaying Remote Protected Network IP Address */
            OctetStr.i4_Length = sizeof (UINT4);
            OctetStr.pu1_OctetList = au1IpAddr;
            nmhGetFsVpnRemoteProtectNetwork (pPolicyName, &OctetStr);
            MEMCPY (&u4RetVal, OctetStr.pu1_OctetList, sizeof (UINT4));
            STRCPY (pHttp->au1KeyString, "REM_ADD_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMSET (au1String, 0, sizeof (au1String));
            VPN_INET_NTOA (au1String, u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            /* Displaying Remote Protected Network Mask */
            nmhGetFsVpnRemoteProtectNetworkPrefixLen (pPolicyName,
                                                      &u4PrefixLen);
            IPV4_MASKLEN_TO_MASK (u4RetVal, u4PrefixLen);
            STRCPY (pHttp->au1KeyString, "REM_MASK_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMSET (au1String, 0, sizeof (au1String));
            VPN_INET_NTOA (au1String, u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
        }
        else if (i4AddrType == WEB_VPN_IKE_KEY_IPV6)
        {
            OctetStr.i4_Length = IP6_ADDR_SIZE;
            OctetStr.pu1_OctetList = au1IpAddr;
            nmhGetFsVpnLocalProtectNetwork (pPolicyName, &OctetStr);
            STRCPY (pHttp->au1KeyString, "LCLV6_ADD_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMCPY (&Ipv6Address, OctetStr.pu1_OctetList, IP6_ADDR_SIZE);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     Ip6PrintAddr (&Ipv6Address));
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            nmhGetFsVpnLocalProtectNetworkPrefixLen (pPolicyName, &u4PrefixLen);
            STRCPY (pHttp->au1KeyString, "LCL_PREFIX_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4PrefixLen);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            nmhGetFsVpnRemoteProtectNetwork (pPolicyName, &OctetStr);
            STRCPY (pHttp->au1KeyString, "REMV6_ADD_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMCPY (&Ipv6Address, OctetStr.pu1_OctetList, IP6_ADDR_SIZE);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     Ip6PrintAddr (&Ipv6Address));
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            nmhGetFsVpnRemoteProtectNetworkPrefixLen (pPolicyName,
                                                      &u4PrefixLen);
            STRCPY (pHttp->au1KeyString, "REM_PREFIX_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4PrefixLen);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
        }
        if ((pPortRange = allocmem_octetstring (WEB_PORT_RANGE_LENGTH)) == NULL)
        {
            free_octetstring (pPolicyName);
            VpnDsUnLock ();
            WebnmUnRegisterLock (pHttp);
            return;
        }

        /* Displaying Local Port Range */
        MEMSET (pPortRange->pu1_OctetList, 0, WEB_PORT_RANGE_LENGTH);

        nmhGetFsVpnIkeSrcPortRange (pPolicyName, pPortRange);
        STRCPY (pHttp->au1KeyString, "LCL_PORT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        MEMSET (pHttp->au1DataString, 0,
                (size_t) STRLEN (pHttp->au1DataString));
        MEMCPY (pHttp->au1DataString, pPortRange->pu1_OctetList,
                pPortRange->i4_Length);
        pHttp->au1DataString[pPortRange->i4_Length] = '\0';
        WebnmSockWrite (pHttp, pHttp->au1DataString, pPortRange->i4_Length);

        /* Displaying Remote Port Range */
        MEMSET (pPortRange->pu1_OctetList, 0, WEB_PORT_RANGE_LENGTH);

        nmhGetFsVpnIkeDstPortRange (pPolicyName, pPortRange);
        STRCPY (pHttp->au1KeyString, "REM_PORT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        MEMSET (pHttp->au1DataString, 0,
                (size_t) STRLEN (pHttp->au1DataString));
        MEMCPY (pHttp->au1DataString, pPortRange->pu1_OctetList,
                pPortRange->i4_Length);
        pHttp->au1DataString[pPortRange->i4_Length] = '\0';
        WebnmSockWrite (pHttp, pHttp->au1DataString, pPortRange->i4_Length);

        /* Displaying IPSEC Protocol */
        nmhGetFsVpnProtocol (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "IPSEC_PROTO_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying the Phase1 Encryption Algo */

        nmhGetFsVpnIkePhase1EncryptionAlgo (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "P1_IKE_ENCR_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying the Phase1 Authentication Key */

        nmhGetFsVpnIkePhase1HashAlgo (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "P1_IKE_AUTH_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying Phase1 DH Algo */

        nmhGetFsVpnIkePhase1DHGroup (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "P1_DHGRP_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /* Displaying the Exchange Mode */
        nmhGetFsVpnIkePhase1Mode (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "P1_EXCH_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying Phase1 Lifetime Type */

        nmhGetFsVpnIkePhase1LifeTimeType (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "P1_LIFE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying Phase1 Life Time Value */
        nmhGetFsVpnIkePhase1LifeTime (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "P1_TIME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying Peer Identity Type */

        nmhGetFsVpnIkePhase1PeerIdType (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "PEER_TYPE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        MEMSET (au1TempStr, 0, sizeof (au1TempStr));
        OctetStr.pu1_OctetList = au1TempStr;

        /*Displaying Peer Identity Value */

        VpnPrintAvailableRemIdValues (pHttp);

        nmhGetFsVpnIkePhase1PeerIdValue (pPolicyName, &OctetStr);
        STRCPY (pHttp->au1KeyString, "PEER_VALUE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", OctetStr.pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying Local Identity Type */

        nmhGetFsVpnIkePhase1LocalIdType (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "LOC_TYP_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying Local Identity Value */

        MEMSET (au1TempStr, 0, sizeof (au1TempStr));
        OctetStr.pu1_OctetList = au1TempStr;

        nmhGetFsVpnIkePhase1LocalIdValue (pPolicyName, &OctetStr);
        STRCPY (pHttp->au1KeyString, "LOC_VALUE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", OctetStr.pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying the Phase2 Protocol */
        nmhGetFsVpnSecurityProtocol (pPolicyName, &i4RetVal);
        nmhGetFsVpnIkePhase2EspEncryptionAlgo (pPolicyName, &i4RetVal1);
        STRCPY (pHttp->au1KeyString, "P2_PROT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        if ((i4RetVal1 != 0) && (i4RetVal == 0))
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", WEB_ESP_VALUE);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying the Phase2 Encryption Algo */

        nmhGetFsVpnIkePhase2EspEncryptionAlgo (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "P2_IKE_ENCR_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying the Phase2 Authentication Key */

        nmhGetFsVpnIkePhase2AuthAlgo (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "P2_IKE_AUTH_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /* Displaying the Phase 2 IKE Mode */
        nmhGetFsVpnMode (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "P2_IKE_MODE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying Phase2 Preferred Forwared Secrecy */

        nmhGetFsVpnIkePhase2DHGroup (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "P2_PFS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying Phase2 Lifetime Type */

        nmhGetFsVpnIkePhase2LifeTimeType (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "P2_LIFE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying Phase2 Life Time Value */

        nmhGetFsVpnIkePhase2LifeTime (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "P2_TIME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        free_octetstring (pPolicyName);
        free_octetstring (pPortRange);

    }
    else
    {
        /*If no policy is present for displaying , blank space is displayed
         * by default.*/

        u4RetVal = VPN_POLICY_DISABLE;
        STRCPY (pHttp->au1KeyString, "VPN_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        STRCPY (pHttp->au1KeyString, "PRESHARED_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        pHttp->au1DataString[0] = '\0';
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        STRCPY (pHttp->au1KeyString, "P1_TIME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        pHttp->au1DataString[0] = '\0';
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        VpnPrintAvailableRemIdValues (pHttp);

        STRCPY (pHttp->au1KeyString, "LOC_VALUE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        pHttp->au1DataString[0] = '\0';
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        STRCPY (pHttp->au1KeyString, "P2_TIME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        pHttp->au1DataString[0] = '\0';
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

    }

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    VpnDsUnLock ();
    WebnmUnRegisterLock (pHttp);
    return;

}

/*********************************************************************
*  Function Name : IssProcessIpSecConfPageGet 
*  Description   : This function processes the get request coming for the  
*                  IpSec/Ike web page.   
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessIpSecConfPageGet (tHttp * pHttp, UINT1 *pu1PolicyName,
                            UINT1 u1DelFlag)
{
    tSNMP_OCTET_STRING_TYPE *pOctectStr = NULL;
    tSNMP_OCTET_STRING_TYPE *pPolicyName = NULL;
    tVpnPolicy         *pVpnPolicy = NULL;
    INT4                i4Len = 0;
    UINT4               u4RetVal = 0;
    UINT4               u4BitMap;
    INT4                i4RetVal = 0;
    UINT1               au1TempStr[WEB_MAX_NAME_LENGTH];
    UINT1               au1String[20];
    UINT1               au1IpAddr[IP6_ADDR_SIZE];
    UINT4               u4PrefixLen = 0;
    INT4                i4AddrType = 0;
    tSNMP_OCTET_STRING_TYPE IpAddr;
    tIp6Addr            Ipv6Address;

    pHttp->i4Write = 0;

    WebnmRegisterLock (pHttp, VpnDsLock, VpnDsUnLock);
    VpnDsLock ();

    MEMSET (au1TempStr, 0, sizeof (au1TempStr));
    MEMSET (au1IpAddr, '\0', IP6_ADDR_SIZE);
    IpAddr.pu1_OctetList = au1IpAddr;

    if (u1DelFlag == 1)
    {
        /*This is called for Get Operation when a policy is deleted. *
         * pHttp->au1Value is set to empty string ,since any value 
         * present in it may be taken for calculation which is not 
         * desired */
        VpnPrintPolicies (pHttp, NULL, au1TempStr, WEB_VPN_IPSEC_MANUAL);
        pHttp->au1Value[0] = '\0';
    }
    else
    {
        STRCPY (pHttp->au1Name, "IPSEC_POLICY");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_FAILURE)
        {
            if (pu1PolicyName == NULL)
            {
                /*When pu1PolicyName is NULL , it is called for Normal Get 
                 * Operation from issweb.c.*/
                VpnPrintPolicies (pHttp, NULL, au1TempStr,
                                  WEB_VPN_IPSEC_MANUAL);
            }
            else
            {
                /*When pu1PolicyName is not NULL , it is called for Get Operation
                 * when a new policy is created and called from the set routinue.*/
                VpnPrintPolicies (pHttp, pu1PolicyName, NULL,
                                  WEB_VPN_IPSEC_MANUAL);

                /*Copying the PolicyName into temporary buffer so as to display the
                 * policy configuration.*/
                MEMSET (au1TempStr, '\0', WEB_MAX_NAME_LENGTH);
                STRNCPY (au1TempStr, pu1PolicyName, (WEB_MAX_NAME_LENGTH - 1));

            }
        }
        else
        {
            /*This is called for Get Operation for displaying the policy configuration. */
            issDecodeSpecialChar (pHttp->au1Value);
            VpnPrintPolicies (pHttp, pHttp->au1Value, NULL,
                              WEB_VPN_IPSEC_MANUAL);
            MEMSET (au1TempStr, '\0', WEB_MAX_NAME_LENGTH);
            STRNCPY (au1TempStr, pHttp->au1Value, (WEB_MAX_NAME_LENGTH - 1));
        }
    }

    VpnDsUnLock ();
    WebnmUnRegisterLock (pHttp);

    u4BitMap = VPN_WEB_ETH_TYPE | VPN_WEB_PPP_TYPE | VPN_WEB_MP_TYPE;
    SecPrintIpInterfaces (pHttp, u4BitMap, VPN_WEB_NETWORK_TYPE_WAN,
                          VPN_WEB_WAN_TYPE_PUBLIC);

    WebnmSendString (pHttp, pHttp->au1KeyString);
    WebnmRegisterLock (pHttp, VpnDsLock, VpnDsUnLock);
    VpnDsLock ();

    /*If a new policy is created or displaying the existing policy , then it displays the 
     * configuration for that policy.*/

    if (au1TempStr[0] != '\0')
    {
        i4Len = (INT4) STRLEN (au1TempStr);
        if ((pPolicyName = allocmem_octetstring (i4Len + 1)) == NULL)
        {
            VpnDsUnLock ();
            WebnmUnRegisterLock (pHttp);
            return;
        }
        MEMSET (pPolicyName->pu1_OctetList, 0, (size_t) (i4Len + 1));
        MEMCPY (pPolicyName->pu1_OctetList, au1TempStr, i4Len);
        pPolicyName->i4_Length = i4Len;
        pVpnPolicy =
            VpnSnmpGetPolicy (pPolicyName->pu1_OctetList,
                              pPolicyName->i4_Length);
        if (pVpnPolicy == NULL)

        {
            free_octetstring (pPolicyName);
            VpnDsUnLock ();
            WebnmUnRegisterLock (pHttp);
            return;
        }

        /*Displaying Interface for which policy is applied  */

        nmhGetFsVpnPolicyIntfIndex (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "IP_INTERFACES_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /* Displaying VPN Status */
        nmhGetFsVpnPolicyRowStatus (pPolicyName, &i4RetVal);
        if (i4RetVal == ACTIVE)
        {
            i4RetVal = VPN_POLICY_ENABLE;
            STRCPY (pHttp->au1KeyString, "VPN_STATUS_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

        }
        else
        {
            i4RetVal = VPN_POLICY_DISABLE;
            STRCPY (pHttp->au1KeyString, "VPN_STATUS_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

        }

        IpAddr.pu1_OctetList = au1IpAddr;
        IpAddr.i4_Length = IP6_ADDR_SIZE;
        nmhGetFsVpnRemoteTunTermAddr (pPolicyName, &IpAddr);
        if (IpAddr.i4_Length == IPVX_IPV4_ADDR_LEN)
        {
            i4AddrType = 1;
            STRCPY (pHttp->au1KeyString, "ADDR_TYPE_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4AddrType);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            MEMCPY (&u4RetVal, IpAddr.pu1_OctetList, sizeof (UINT4));
            STRCPY (pHttp->au1KeyString, "IPSEC_GW_IP_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMSET (au1String, 0, sizeof (au1String));
            VPN_INET_NTOA (au1String, u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
        }
        else if (IpAddr.i4_Length == IPVX_IPV6_ADDR_LEN)
        {
            i4AddrType = 2;
            STRCPY (pHttp->au1KeyString, "ADDR_TYPE_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4AddrType);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "IPSEC_GW_IPV6_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMCPY (&Ipv6Address, IpAddr.pu1_OctetList, IP6_ADDR_SIZE);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     Ip6PrintAddr (&Ipv6Address));
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
        }

        /* Displaying IPSEC Protocol */
        nmhGetFsVpnProtocol (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "IPSEC_PROTO_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        nmhGetFsVpnProtectNetworkType (pPolicyName, &i4AddrType);

        if (i4AddrType == WEB_VPN_IKE_KEY_IPV4)
        {
            IpAddr.i4_Length = sizeof (UINT4);
            nmhGetFsVpnLocalProtectNetwork (pPolicyName, &IpAddr);
            MEMCPY (&u4RetVal, IpAddr.pu1_OctetList, sizeof (UINT4));
            STRCPY (pHttp->au1KeyString, "LCL_IP_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMSET (au1String, 0, sizeof (au1String));
            VPN_INET_NTOA (au1String, u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            nmhGetFsVpnLocalProtectNetworkPrefixLen (pPolicyName, &u4PrefixLen);
            IPV4_MASKLEN_TO_MASK (u4RetVal, u4PrefixLen);
            STRCPY (pHttp->au1KeyString, "LCL_MASK_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMSET (au1String, 0, sizeof (au1String));
            VPN_INET_NTOA (au1String, u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            IpAddr.i4_Length = sizeof (UINT4);
            nmhGetFsVpnRemoteProtectNetwork (pPolicyName, &IpAddr);
            MEMCPY (&u4RetVal, IpAddr.pu1_OctetList, sizeof (UINT4));
            STRCPY (pHttp->au1KeyString, "REM_ADD_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMSET (au1String, 0, sizeof (au1String));
            VPN_INET_NTOA (au1String, u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            nmhGetFsVpnRemoteProtectNetworkPrefixLen (pPolicyName,
                                                      &u4PrefixLen);
            IPV4_MASKLEN_TO_MASK (u4RetVal, u4PrefixLen);
            STRCPY (pHttp->au1KeyString, "REM_MASK_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMSET (au1String, 0, sizeof (au1String));
            VPN_INET_NTOA (au1String, u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
        }
        else if (i4AddrType == WEB_VPN_IKE_KEY_IPV6)
        {
            IpAddr.i4_Length = IP6_ADDR_SIZE;
            IpAddr.pu1_OctetList = au1IpAddr;
            MEMSET (au1IpAddr, 0, IP6_ADDR_SIZE);
            nmhGetFsVpnLocalProtectNetwork (pPolicyName, &IpAddr);
            STRCPY (pHttp->au1KeyString, "LCL_IPV6_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMCPY (&Ipv6Address, IpAddr.pu1_OctetList, IP6_ADDR_SIZE);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     Ip6PrintAddr (&Ipv6Address));
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            nmhGetFsVpnLocalProtectNetworkPrefixLen (pPolicyName, &u4PrefixLen);
            STRCPY (pHttp->au1KeyString, "LCL_PREFIX_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4PrefixLen);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            IpAddr.i4_Length = IP6_ADDR_SIZE;
            nmhGetFsVpnRemoteProtectNetwork (pPolicyName, &IpAddr);
            STRCPY (pHttp->au1KeyString, "REMV6_ADD_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMCPY (&Ipv6Address, IpAddr.pu1_OctetList, IP6_ADDR_SIZE);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     Ip6PrintAddr (&Ipv6Address));
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            nmhGetFsVpnRemoteProtectNetworkPrefixLen (pPolicyName,
                                                      &u4PrefixLen);
            STRCPY (pHttp->au1KeyString, "REM_PREFIX_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4PrefixLen);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

        }

        /* Displaying IPSEC mode type - Tunnel or Transport */
        nmhGetFsVpnMode (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "IPSEC_MODE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying Phase2 Preferred Forwared Secrecy */
        nmhGetFsVpnSecurityProtocol (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "IPSEC_PROT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying IPSEC Authentication Type */

        nmhGetFsVpnAuthAlgo (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "IPSEC_AUTH_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying Authentication Key */
        if ((pOctectStr = allocmem_octetstring (WEB_VPN_KEY_MAX_LEN)) == NULL)
        {
            free_octetstring (pPolicyName);
            VpnDsUnLock ();
            WebnmUnRegisterLock (pHttp);
            return;
        }
        MEMSET (pOctectStr->pu1_OctetList, 0, WEB_VPN_KEY_MAX_LEN);
        pOctectStr->i4_Length = WEB_VPN_KEY_MAX_LEN - 1;

        nmhGetFsVpnAhKey (pPolicyName, pOctectStr);
        STRCPY (pHttp->au1KeyString, "IPSEC_AUTH_KY_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                 pOctectStr->pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying Encryption Algorithm Type */
        nmhGetFsVpnEncrAlgo (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "IPSEC_ENCRY_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying Encryption Key */
        MEMSET (pOctectStr->pu1_OctetList, 0, WEB_VPN_KEY_MAX_LEN);
        nmhGetFsVpnEspKey (pPolicyName, pOctectStr);
        if ((INT4) STRLEN (pOctectStr->pu1_OctetList) == WEB_VPN_3DES_KEY_LEN)
        {
            STRTOK (pOctectStr->pu1_OctetList, ".");
            STRCPY (pHttp->au1KeyString, "ENCRYPKEY1_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     pOctectStr->pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            pOctectStr->pu1_OctetList = (UINT1 *) STRTOK (NULL, ".");
            STRCPY (pHttp->au1KeyString, "ENCRYPKEY2_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     pOctectStr->pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            pOctectStr->pu1_OctetList = (UINT1 *) STRTOK (NULL, ".");
            STRCPY (pHttp->au1KeyString, "ENCRYPKEY3_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     pOctectStr->pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
        }
        else
        {
            STRCPY (pHttp->au1KeyString, "ENCRYPKEY1_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     pOctectStr->pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "ENCRYPKEY2_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "\0");
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "ENCRYPKEY3_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "\0");
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

        }

        /*Displaying OutBound SPI Value */
        nmhGetFsVpnOutboundSpi (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "OUT_SPI_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying InBound SPI Time Value */
        nmhGetFsVpnInboundSpi (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "IN_SPI_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying Anti Replay Value */
        nmhGetFsVpnAntiReplay (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "VPN_ANTIREPLAY_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        free_octetstring (pPolicyName);
        free_octetstring (pOctectStr);

    }
    else
    {
        /*If no policy is present for displaying , blank space is displayed
         * by default.*/

        u4RetVal = VPN_POLICY_DISABLE;
        STRCPY (pHttp->au1KeyString, "VPN_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        STRCPY (pHttp->au1KeyString, "IPSEC_AUTH_KY_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        pHttp->au1DataString[0] = '\0';
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        STRCPY (pHttp->au1KeyString, "ENCRYPKEY1_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        pHttp->au1DataString[0] = '\0';
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        STRCPY (pHttp->au1KeyString, "ENCRYPKEY2_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        pHttp->au1DataString[0] = '\0';
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        STRCPY (pHttp->au1KeyString, "ENCRYPKEY3_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        pHttp->au1DataString[0] = '\0';
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        STRCPY (pHttp->au1KeyString, "OUT_SPI_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        pHttp->au1DataString[0] = '\0';
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        STRCPY (pHttp->au1KeyString, "IN_SPI_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        pHttp->au1DataString[0] = '\0';
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

    }

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    VpnDsUnLock ();
    WebnmUnRegisterLock (pHttp);
    return;

}

/*********************************************************************
*  Function Name : IssProcessIpSecConfPageSet 
*  Description   : This function processes the Set request coming for the  
*                  IpSec web page.   
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessIpSecConfPageSet (tHttp * pHttp)
{

    tWebIpSecPolicy    *pWebVpnPolicy = NULL;
    tVpnNetwork         LocalNet;
    tVpnNetwork         RemoteNet;
    tIp6Addr            Ipv6Address;

    UINT1               u1PolicyDelFlag = FALSE;
    UINT1               u1VpnReturnStatus = FALSE;
    INT4                i4Len = 0;
    UINT1               au1SecAssocAhKey[WEB_SEC_AUTH_KEY_SIZE];    /* configured ah key */
    UINT4               u4IfIndex = 0;
#ifdef CLI_WANTED
    UINT4               u4PeerAddress = 0;
    UINT4               u4Action = 0;
    UINT4               u4Mode = 0;
#endif
    UINT4               u4Proto = 0;
    UINT4               u4AuthProto = 0;
    UINT1               u1SecAssocAhKeyLength = 0;
    UINT4               u4InboundSpi = 0;
    UINT4               u4OutBoundSpi = 0;
    UINT4               u4AntiReplay = 0;
    UINT4               u4EspProto = 0;
    UINT1               au1SecAssocEspKey[WEB_VPN_KEY_MAX_LEN];
    UINT1               au1TempEspKey[IPSEC_WEB_ARG_LEN + 1];
    UINT1               au1TempEspKey1[IPSEC_WEB_ARG_LEN + 1];
    UINT4               i = 0;
    UINT4               j = 0;
    UINT4               u4SnmpErrorStatus = 0;
    tSNMP_OCTET_STRING_TYPE *pPolicyName = NULL;
    tSNMP_OCTET_STRING_TYPE *pSrcPortRange = NULL;
    tSNMP_OCTET_STRING_TYPE *pDstPortRange = NULL;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *piIfName;
    INT4                i4VpnPolicyRowStatus = 0;
    UINT4               u4VpnStatus = VPN_POLICY_DISABLE;
    INT4                i4PolicyType = 0;
    UINT4               u4AddrType = 0;
    tIp6Addr           *pip6addr = NULL;
    WebnmRegisterLock (pHttp, VpnDsLock, VpnDsUnLock);
    VpnDsLock ();

    MEMSET (au1SecAssocAhKey, '\0', WEB_SEC_AUTH_KEY_SIZE);
    MEMSET (au1SecAssocEspKey, '\0', WEB_VPN_KEY_MAX_LEN);
    MEMSET (au1TempEspKey, '\0', IPSEC_WEB_ARG_LEN + 1);
    MEMSET (au1TempEspKey1, '\0', IPSEC_WEB_ARG_LEN + 1);
    MEMSET (au1IfName, '\0', CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (&LocalNet, '\0', sizeof (tVpnNetwork));
    MEMSET (&RemoteNet, '\0', sizeof (tVpnNetwork));

    if (MemAllocateMemBlock
        (VPN_WEB_IPSEC_POLICY_PID,
         (UINT1 **) (VOID *) &pWebVpnPolicy) != MEM_SUCCESS)
    {
        return;
    }
    MEMSET (pWebVpnPolicy, 0, sizeof (tWebIpSecPolicy));
    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCASECMP (pHttp->au1Value, "DELETE") == 0)
    {
        u1PolicyDelFlag = TRUE;
    }

    do
    {
        /* Setting the polciy name . Reff CLI Command : cryto map <policy name> */
        STRCPY (pHttp->au1Name, "IPSEC_POLICY");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        issDecodeSpecialChar (pHttp->au1Value);
        MEMSET (pWebVpnPolicy->au1IpSecPolicyName, '\0', IPSEC_POLICY_NAME_LEN);
        STRNCPY (pWebVpnPolicy->au1IpSecPolicyName, pHttp->au1Value,
                 (IPSEC_POLICY_NAME_LEN - 1));

        i4Len = (INT4) STRLEN (pWebVpnPolicy->au1IpSecPolicyName);

        if ((pPolicyName = allocmem_octetstring (i4Len + 1)) == NULL)
        {
            IssSendError (pHttp, (CONST INT1 *) "Policy Creation Failed!");
            break;
        }

        MEMSET (pPolicyName->pu1_OctetList, 0, (size_t) (i4Len + 1));
        MEMCPY (pPolicyName->pu1_OctetList, pWebVpnPolicy->au1IpSecPolicyName,
                i4Len);
        pPolicyName->i4_Length = i4Len;

        if (u1PolicyDelFlag == TRUE)
        {
            break;
        }

        /*If Policy is ACITVE , for modifying the parameters we set the Row status to NOT IN SERVICE 
         * and modify the policy parameters */
        nmhGetFsVpnPolicyRowStatus (pPolicyName, &i4VpnPolicyRowStatus);
        if (nmhGetFsVpnPolicyType (pPolicyName, &i4PolicyType) == SNMP_SUCCESS)
        {
            if (((i4PolicyType == WEB_VPN_PRESHARED)
                 || (i4PolicyType == WEB_VPN_XAUTH)
                 || (i4PolicyType == VPN_IKE_RA_PRESHAREDKEY))
                && (i4VpnPolicyRowStatus == ACTIVE))
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "Cannot Modify an Active Policy!");
                break;
            }
        }

        if (i4VpnPolicyRowStatus != ACTIVE)
        {

#ifdef CLI_WANTED
            if (VpnCreatePolicy (WEB_CLI_HANDLE, pPolicyName) == CLI_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Policy Creation Failed!");
                break;
            }

            /* Setting the cryto mode to ipsec manual . Reff CLI Command : cryto mode <ipsec-manual> */
            if (VpnSetPolicyType (WEB_CLI_HANDLE, pPolicyName,
                                  WEB_VPN_IPSEC_MANUAL) == CLI_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Failed to set the crypto mode to ipsec manual!");
                break;
            }
#endif
            /* Setting the ipsec mode Reff CLI COMMAND : crypto ipsec mode {tunnel | transport}  */

            STRCPY (pHttp->au1Name, "IPSEC_MODE");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);
            MEMSET (pWebVpnPolicy->au1IpSecMode, '\0', IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1IpSecMode, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

#ifdef CLI_WANTED
            if (STRCMP (pWebVpnPolicy->au1IpSecMode, WEB_TUNNEL) == 0)    /* Tunnel Mode */
            {
                u4Mode = WEB_VPN_TUNNEL;
            }
            else if (STRCMP (pWebVpnPolicy->au1IpSecMode, WEB_TRANSPORT) == 0)    /* Transport Mode */
            {
                u4Mode = WEB_VPN_TRANSPORT;
            }
            else
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Ipsec Mode!");
                break;
            }
            if (VpnSetPolicyMode (WEB_CLI_HANDLE, pPolicyName, u4Mode) ==
                CLI_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "Failed to set Ipsec Mode!");
                break;
            }
#endif
            STRCPY (pHttp->au1Name, "ADDR_TYPE");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);
            u4AddrType = (UINT4) ATOI (pHttp->au1Value);

            if (u4AddrType == WEB_VPN_IKE_KEY_IPV4)
            {
                /*Setting the IpSec Gateway Address Reff CLI COMMAND : set peer <num_str> */
                STRCPY (pHttp->au1Name, "IPSEC_GW_IP");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                issDecodeSpecialChar (pHttp->au1Value);
                MEMSET (pWebVpnPolicy->au1IpsecGwIpAddr, '\0',
                        IPSEC_IPADDR_LEN);
                STRNCPY (pWebVpnPolicy->au1IpsecGwIpAddr, pHttp->au1Value,
                         (IPSEC_IPADDR_LEN - 1));

#ifdef CLI_WANTED
                u4PeerAddress =
                    OSIX_NTOHL (INET_ADDR (pWebVpnPolicy->au1IpsecGwIpAddr));
                if (VpnSetPeerIp (WEB_CLI_HANDLE, pPolicyName, u4PeerAddress) ==
                    VPN_FAILURE)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Failed to set Ipsec Gateway Address!");
                    break;
                }
#endif
            }
            else
            {
                /*Setting the IpSec IPv6 Gateway Address Reff CLI COMMAND : set ipv6 peer <num_str> */
                STRCPY (pHttp->au1Name, "IPSEC_GW_IPV6");
                MEMSET (&Ipv6Address, '\0', sizeof (tIp6Addr));
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                issDecodeSpecialChar (pHttp->au1Value);

                if (INET_ATON6 (pHttp->au1Value, &Ipv6Address) == 0)
                {
                    IssSendError (pHttp, (CONST INT1 *) "Invalid IPv6 address");
                    break;
                }

#ifdef CLI_WANTED
                if (VpnSetIpv6Peer
                    (WEB_CLI_HANDLE, pPolicyName,
                     Ipv6Address.u1_addr) == VPN_FAILURE)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Failed to set Ipsec V6 Gateway Address!");
                    break;
                }
#endif
            }

          /*************************************************************************************   
           * Setting the following IpSec Parameters 
           * Protocol - ESP/AH
           * IpSec Authentication - hmac-mds/hmac-sha1
           * IpSec Authentication Key
           * IpSec Encryption - DES/3DES/AES128/AES192/AES256
           * Encryption Key1
           * Encryption Key2
           * Encryption Key3
           * Outgoing SPI
           * Incoming SPI
           * 
           Reff CLI  COMMAND : set session-key  authenticator 
                               {hmac-md5|hmac-sha1} <num_str>  [esp- {des cipher <num_str> | 
                               triple-des cipher <num_str> <num_str> <num_str> | {aes-128|
                               aes-192|aes-256} cipher <num_str>}] outbound <num_str> 
                               inbound <num_str> [anti-replay]

          ****************************************************************************************/

            STRCPY (pHttp->au1Name, "IPSEC_PROT");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);
            MEMSET (pWebVpnPolicy->au1IpSecProtocol, '\0', IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1IpSecProtocol, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

            STRCPY (pHttp->au1Name, "IPSEC_AUTH");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);
            MEMSET (pWebVpnPolicy->au1IpSecAuthenticator, '\0',
                    IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1IpSecAuthenticator, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

            STRCPY (pHttp->au1Name, "IPSEC_AUTH_KY");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);
            MEMSET (pWebVpnPolicy->au1IpSecAuthenticationKey, '\0',
                    WEB_SEC_AUTH_KEY_SIZE);
            STRNCPY (pWebVpnPolicy->au1IpSecAuthenticationKey, pHttp->au1Value,
                     (WEB_SEC_AUTH_KEY_SIZE - 1));

            STRCPY (pHttp->au1Name, "IPSEC_ENCRY");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);
            MEMSET (pWebVpnPolicy->au1IpSecEncryption, '\0', IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1IpSecEncryption, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

            STRCPY (pHttp->au1Name, "ENCRYPKEY1");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);
            MEMSET (pWebVpnPolicy->au1IpSecEncryptionKey1, '\0',
                    IPSEC_MAX_KEY_LEN);
            STRNCPY (pWebVpnPolicy->au1IpSecEncryptionKey1, pHttp->au1Value,
                     (IPSEC_MAX_KEY_LEN - 1));

            STRCPY (pHttp->au1Name, "ENCRYPKEY2");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);
            MEMSET (pWebVpnPolicy->au1IpSecEncryptionKey2, '\0',
                    IPSEC_MAX_KEY_LEN);
            STRNCPY (pWebVpnPolicy->au1IpSecEncryptionKey2, pHttp->au1Value,
                     (IPSEC_MAX_KEY_LEN - 1));

            STRCPY (pHttp->au1Name, "ENCRYPKEY3");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);
            MEMSET (pWebVpnPolicy->au1IpSecEncryptionKey3, '\0',
                    IPSEC_MAX_KEY_LEN);
            STRNCPY (pWebVpnPolicy->au1IpSecEncryptionKey3, pHttp->au1Value,
                     (IPSEC_MAX_KEY_LEN - 1));

            STRCPY (pHttp->au1Name, "OUT_SPI");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);
            MEMSET (pWebVpnPolicy->au1IpSecOutgoingSPI, '\0',
                    IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1IpSecOutgoingSPI, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

            STRCPY (pHttp->au1Name, "IN_SPI");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);
            MEMSET (pWebVpnPolicy->au1IpSecIncomingSPI, '\0',
                    IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1IpSecIncomingSPI, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

            STRCPY (pHttp->au1Name, "VPN_ANTIREPLAY");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);
            MEMSET (pWebVpnPolicy->au1IpSecAntiReplay, '\0', IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1IpSecAntiReplay, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

            SSCANF ((CONST CHR1 *)
                    pWebVpnPolicy->au1IpSecProtocol, "%u", &u4AuthProto);
            if (u4AuthProto != 0)
            {
                SSCANF ((CONST CHR1 *)
                        pWebVpnPolicy->au1IpSecAuthenticator, "%u", &u4Proto);
                MEMSET (au1SecAssocAhKey, '\0', WEB_SEC_AUTH_KEY_SIZE);
                STRNCPY (au1SecAssocAhKey,
                         pWebVpnPolicy->au1IpSecAuthenticationKey,
                         (WEB_SEC_AUTH_KEY_SIZE - 1));
                u1SecAssocAhKeyLength = (UINT1)
                    (INT4) STRLEN (pWebVpnPolicy->au1IpSecAuthenticationKey);
            }

            SSCANF ((CONST CHR1 *)
                    pWebVpnPolicy->au1IpSecEncryption, "%u", &u4EspProto);
            if (u4EspProto != 0)
            {
                MEMSET (au1SecAssocEspKey, '\0', WEB_VPN_KEY_MAX_LEN);
                STRNCPY (au1SecAssocEspKey,
                         pWebVpnPolicy->au1IpSecEncryptionKey1,
                         (WEB_VPN_KEY_MAX_LEN - 1));

                if (u4EspProto == WEB_3DES)
                {
                    STRNCPY (au1TempEspKey,
                             pWebVpnPolicy->au1IpSecEncryptionKey2,
                             IPSEC_WEB_ARG_LEN);
                    STRNCPY (au1TempEspKey1,
                             pWebVpnPolicy->au1IpSecEncryptionKey3,
                             IPSEC_WEB_ARG_LEN);

                    au1SecAssocEspKey[ESP_FIRST_KEY_TERMINATION] = '.';

                    for (i = 17; i < 33; i++)
                    {
                        au1SecAssocEspKey[i] = au1TempEspKey[j];
                        j++;

                    }

                    au1SecAssocEspKey[ESP_SECOND_KEY_TERMINATION] = '.';
                    j = 0;

                    for (i = 34; i < 50; i++)
                    {
                        au1SecAssocEspKey[i] = au1TempEspKey1[j];
                        j++;
                    }
                    au1SecAssocEspKey[50] = '\0';
                }
            }

            SSCANF ((CONST CHR1 *)
                    pWebVpnPolicy->au1IpSecOutgoingSPI, "%u", &u4OutBoundSpi);
            SSCANF ((CONST CHR1 *)
                    pWebVpnPolicy->au1IpSecIncomingSPI, "%u", &u4InboundSpi);
            SSCANF ((CONST CHR1 *)
                    pWebVpnPolicy->au1IpSecAntiReplay, "%u", &u4AntiReplay);

#ifdef CLI_WANTED
            if (VpnSetCryptoParams (WEB_CLI_HANDLE, pPolicyName,
                                    u4AuthProto, u4Proto,
                                    au1SecAssocAhKey,
                                    u4InboundSpi, u4OutBoundSpi,
                                    (INT4) u4EspProto, au1SecAssocEspKey,
                                    u4AntiReplay) == CLI_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Failed to set Ipsec Crpto Parameters");
                break;
            }

#endif
            if (u4AddrType == WEB_VPN_IKE_KEY_IPV4)
            {

                /* Configuration of Traffic Selector  Reff CLI Command
                 * access-list {permit|deny|apply} {any|tcp|udp|icmpv4|ahproto|espproto}
                 source <num_str> <num_str> destination <num_str> <num_str> */

                STRCPY (pHttp->au1Name, "LCL_IP");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                issDecodeSpecialChar (pHttp->au1Value);
                MEMSET (pWebVpnPolicy->au1IpsecLocalIpAddr, '\0',
                        IPSEC_IPADDR_LEN);
                STRNCPY (pWebVpnPolicy->au1IpsecLocalIpAddr, pHttp->au1Value,
                         (IPSEC_IPADDR_LEN - 1));

                STRCPY (pHttp->au1Name, "LCL_MASK");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                issDecodeSpecialChar (pHttp->au1Value);
                MEMSET (pWebVpnPolicy->au1IpsecLocalIpMask, '\0',
                        IPSEC_IPADDR_LEN);
                STRNCPY (pWebVpnPolicy->au1IpsecLocalIpMask, pHttp->au1Value,
                         (IPSEC_IPADDR_LEN - 1));

                STRCPY (pHttp->au1Name, "REM_ADD");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                issDecodeSpecialChar (pHttp->au1Value);
                MEMSET (pWebVpnPolicy->au1IpsecRemoteIpAddr, '\0',
                        IPSEC_IPADDR_LEN);
                STRNCPY (pWebVpnPolicy->au1IpsecRemoteIpAddr, pHttp->au1Value,
                         (IPSEC_IPADDR_LEN - 1));

                STRCPY (pHttp->au1Name, "REM_MASK");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                issDecodeSpecialChar (pHttp->au1Value);
                MEMSET (pWebVpnPolicy->au1IpsecRemoteIpMask, '\0',
                        IPSEC_IPADDR_LEN);
                STRNCPY (pWebVpnPolicy->au1IpsecRemoteIpMask, pHttp->au1Value,
                         (IPSEC_IPADDR_LEN - 1));

                STRCPY (pHttp->au1Name, "IPSEC_PROTO");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                issDecodeSpecialChar (pHttp->au1Value);
                MEMSET (pWebVpnPolicy->au1IpSecTransportProtocol, '\0',
                        IPSEC_WEB_ARG_LEN);
                STRNCPY (pWebVpnPolicy->au1IpSecTransportProtocol,
                         pHttp->au1Value, (IPSEC_WEB_ARG_LEN - 1));
                STRCPY (pHttp->au1Name, "vpnStatus");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                issDecodeSpecialChar (pHttp->au1Value);
                u4VpnStatus = (UINT4) ATOI (pHttp->au1Value);
#ifdef CLI_WANTED
                u4Action = WEB_SEC_APPLY;
#endif
                SSCANF ((CONST CHR1 *)
                        pWebVpnPolicy->au1IpSecTransportProtocol, "%u",
                        &u4Proto);
                LocalNet.IpAddr.uIpAddr.Ip4Addr =
                    OSIX_NTOHL (INET_ADDR (pWebVpnPolicy->au1IpsecLocalIpAddr));
                IPV4_MASK_TO_MASKLEN (LocalNet.u4AddrPrefixLen,
                                      OSIX_NTOHL (INET_ADDR
                                                  (pWebVpnPolicy->
                                                   au1IpsecLocalIpMask)));
                RemoteNet.IpAddr.uIpAddr.Ip4Addr =
                    OSIX_NTOHL (INET_ADDR
                                (pWebVpnPolicy->au1IpsecRemoteIpAddr));
                IPV4_MASK_TO_MASKLEN (RemoteNet.u4AddrPrefixLen,
                                      OSIX_NTOHL (INET_ADDR
                                                  (pWebVpnPolicy->
                                                   au1IpsecRemoteIpMask)));

#ifdef CLI_WANTED
                if (VpnSetAccessParams
                    (pPolicyName, &LocalNet, &RemoteNet, u4Action, u4Proto,
                     pSrcPortRange, pDstPortRange) == CLI_FAILURE)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Failed to Set Ipsec Traffic Selector!");
                    break;
                }
#endif
            }
            else
            {
                /* Configuration of Traffic Selector  Reff CLI Command
                   access-list ipv6 {permit|deny|apply} {any|tcp|udp|icmpv6|ahproto|espproto}
                   source <ucast_addr> <prefixlen> destination <ucast_addr> <prefixlen>
                   [srcport <random_str>] [destport <random_str>] */

                STRCPY (pHttp->au1Name, "LCLV6_ADD");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                issDecodeSpecialChar (pHttp->au1Value);
                if ((pip6addr = str_to_ip6addr (pHttp->au1Value)) != NULL)
                {
                    MEMCPY (LocalNet.IpAddr.uIpAddr.Ip6Addr.u1_addr,
                            pip6addr, sizeof (tIp6Addr));
                }
                STRCPY (pHttp->au1Name, "LCL_PREFIX");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                issDecodeSpecialChar (pHttp->au1Value);
                LocalNet.u4AddrPrefixLen = (UINT4) ATOI (pHttp->au1Value);

                STRCPY (pHttp->au1Name, "REMV6_ADD");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                issDecodeSpecialChar (pHttp->au1Value);
                if ((pip6addr = str_to_ip6addr (pHttp->au1Value)) != NULL)
                {
                    MEMCPY (RemoteNet.IpAddr.uIpAddr.Ip6Addr.u1_addr,
                            pip6addr, sizeof (tIp6Addr));
                }
                STRCPY (pHttp->au1Name, "REM_PREFIX");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                issDecodeSpecialChar (pHttp->au1Value);
                RemoteNet.u4AddrPrefixLen = (UINT4) ATOI (pHttp->au1Value);
                STRCPY (pHttp->au1Name, "IPSEC_PROTO");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                issDecodeSpecialChar (pHttp->au1Value);
                MEMSET (pWebVpnPolicy->au1IpSecTransportProtocol, '\0',
                        IPSEC_WEB_ARG_LEN);
                STRNCPY (pWebVpnPolicy->au1IpSecTransportProtocol,
                         pHttp->au1Value, (IPSEC_WEB_ARG_LEN - 1));
                STRCPY (pHttp->au1Name, "vpnStatus");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                issDecodeSpecialChar (pHttp->au1Value);
                u4VpnStatus = (UINT4) ATOI (pHttp->au1Value);

#ifdef CLI_WANTED
                u4Action = WEB_SEC_APPLY;
#endif
                SSCANF ((CONST CHR1 *)
                        pWebVpnPolicy->au1IpSecTransportProtocol, "%u",
                        &u4Proto);

#ifdef CLI_WANTED
                if (VpnSetIpv6AccessParams
                    (pPolicyName, &LocalNet, &RemoteNet,
                     u4Action, u4Proto, pSrcPortRange,
                     pDstPortRange) == CLI_FAILURE)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Failed to Set IPv6 Ipsec Traffic Selector!");
                    break;
                }

#endif
            }
            /* Applying the policy to the interface. Reff CLI COMMAND : crypto map apply <policy name> */
            STRCPY (pHttp->au1Name, "IPSEC_INTF");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);

            MEMSET (au1IfName, 0, sizeof (au1IfName));
            piIfName = (INT1 *) &au1IfName[0];
            /* We get interface as either in numeric or alias format like "1" or "Fa0/1"
             * so check the format first and fetch the interface number 
             */
            if (STRSTR (pHttp->au1Value, "/") != NULL)
            {
                for (u4IfIndex = 1; u4IfIndex <= SYS_MAX_INTERFACES;
                     u4IfIndex++)
                {

                    CfaCliGetIfName (u4IfIndex, piIfName);
                    if (STRCMP (piIfName, pHttp->au1Value) == 0)
                    {
                        break;
                    }

                }
            }
            else
            {
                u4IfIndex = (UINT4) ATOI (pHttp->au1Value);
            }

            if (nmhTestv2FsVpnPolicyIntfIndex
                (&u4SnmpErrorStatus, pPolicyName,
                 (INT4) u4IfIndex) == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "VPN policies can be applied only on "
                              "untrusted/Public L3 interfaces(PPP, Ethernet etc )"
                              "  or Invalid ip address is assigned to the"
                              " Interface");

                break;
            }
            nmhSetFsVpnPolicyIntfIndex (pPolicyName, (INT4) u4IfIndex);
        }
        if (u4VpnStatus == VPN_POLICY_ENABLE)
        {
            if (nmhTestv2FsVpnPolicyRowStatus (&u4SnmpErrorStatus, pPolicyName,
                                               ACTIVE) == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to Set Policy to Interface");
                break;
            }

            if (nmhSetFsVpnPolicyRowStatus (pPolicyName, ACTIVE) ==
                SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to Set Policy to Interface");
                break;
            }

        }
        else
        {
            if (nmhTestv2FsVpnPolicyRowStatus (&u4SnmpErrorStatus, pPolicyName,
                                               NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "Unable to Modify the Policy");
                break;
            }

            if (nmhSetFsVpnPolicyRowStatus (pPolicyName, NOT_IN_SERVICE) ==
                SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "Unable to Modify the Policy");
                break;
            }
        }

        u1VpnReturnStatus = TRUE;
    }
    while (0);

    if (pPolicyName != NULL)
    {
#ifdef CLI_WANTED
        if (u1PolicyDelFlag == TRUE)
        {
            if (VpnDeletePolicy (WEB_CLI_HANDLE, pPolicyName) == VPN_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Policy Deletion Failed!");
            }
            else
            {
                u1VpnReturnStatus = TRUE;
            }
        }
#endif
        free_octetstring (pPolicyName);
    }
    UNUSED_PARAM (u1SecAssocAhKeyLength);
    UNUSED_PARAM (pDstPortRange);
    UNUSED_PARAM (pSrcPortRange);
    MemReleaseMemBlock (VPN_WEB_IPSEC_POLICY_PID, (UINT1 *) pWebVpnPolicy);
    VpnDsUnLock ();
    WebnmUnRegisterLock (pHttp);
    if (u1VpnReturnStatus == TRUE)
    {
        IssProcessIpSecConfPageGet (pHttp, NULL, u1PolicyDelFlag);
    }
    return;
}

/*********************************************************************
*  Function Name : IssProcessIkeConfPageSet 
*  Description   : This function processes the Set request coming for the  
*                  Ike web page.   
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessIkeConfPageSet (tHttp * pHttp)
{

    tWebIkePolicy      *pWebVpnPolicy = NULL;
    tVpnNetwork         LocalNet;
    tVpnNetwork         RemoteNet;

    UINT1               u1PolicyDelFlag = FALSE;
    UINT1               u1VpnReturnStatus = FALSE;
    INT4                i4Len = 0;
    UINT4               u4IfIndex = 0;
#ifdef CLI_WANTED
    UINT4               u4PeerAddress = 0;
    UINT4               u4Action = 0;
#endif
    UINT4               u4Proto = 0;
    UINT4               u4Mode = 0;
    UINT4               u4SnmpErrorStatus = 0;
    UINT4               u4EncrAlgo = 0;
    UINT4               u4HashAlgo = 0;
    UINT4               u4DHGrp = 0;
    UINT4               u4ExchMode = 0;
    UINT4               u4LifeTime = 0;
    UINT4               u4LifeTimeType = 0;
    UINT4               u4Protocol = 0;
    UINT4               u4PeerType;
    UINT4               u4LocalIdType = 0;
    UINT4               u4VpnStatus = VPN_POLICY_DISABLE;
    UINT4               u4IkeVersion = 0;
    tSNMP_OCTET_STRING_TYPE *pPolicyName = NULL;
    tSNMP_OCTET_STRING_TYPE *pSrcPortRange = NULL;
    tSNMP_OCTET_STRING_TYPE *pDstPortRange = NULL;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *piIfName;
    INT4                i4VpnPolicyRowStatus = 0;
    INT4                i4PolicyType = 0;
    tIp6Addr            Ipv6Address;
    tIp6Addr           *pip6addr = NULL;
    UINT4               u4AddrType = 0;
    WebnmRegisterLock (pHttp, VpnDsLock, VpnDsUnLock);
    VpnDsLock ();

    MEMSET (&LocalNet, '\0', sizeof (tVpnNetwork));
    MEMSET (&RemoteNet, '\0', sizeof (tVpnNetwork));

    if (MemAllocateMemBlock
        (VPN_WEB_IKE_POLICY_PID,
         (UINT1 **) (VOID *) &pWebVpnPolicy) != MEM_SUCCESS)
    {
        return;
    }
    MEMSET (pWebVpnPolicy, 0, sizeof (tWebIkePolicy));
    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCASECMP (pHttp->au1Value, "DELETE") == 0)
    {
        u1PolicyDelFlag = TRUE;
    }

    do
    {
        /* Setting the polciy name . Reff CLI Command : cryto map <policy name> */
        STRCPY (pHttp->au1Name, "CURR_POLICY");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        issDecodeSpecialChar (pHttp->au1Value);
        MEMSET (pWebVpnPolicy->au1IpSecPolicyName, '\0', IPSEC_POLICY_NAME_LEN);
        STRNCPY (pWebVpnPolicy->au1IpSecPolicyName, pHttp->au1Value,
                 (IPSEC_POLICY_NAME_LEN - 1));

        i4Len = (INT4) STRLEN (pWebVpnPolicy->au1IpSecPolicyName);

        if ((pPolicyName = allocmem_octetstring (i4Len + 1)) == NULL)
        {
            IssSendError (pHttp, (CONST INT1 *) "Policy Creation Failed!");
            break;
        }

        MEMSET (pPolicyName->pu1_OctetList, 0, (size_t) (i4Len + 1));
        MEMCPY (pPolicyName->pu1_OctetList, pWebVpnPolicy->au1IpSecPolicyName,
                i4Len);
        pPolicyName->i4_Length = i4Len;

        if (u1PolicyDelFlag == TRUE)
        {
            break;
        }

        STRCPY (pHttp->au1Name, "vpnStatus");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        issDecodeSpecialChar (pHttp->au1Value);
        u4VpnStatus = (UINT4) ATOI (pHttp->au1Value);
        /*If Policy is ACITVE , for modifying the parameters we set the Row status to NOT IN SERVICE 
         * and modify the policy parameters */
        nmhGetFsVpnPolicyRowStatus (pPolicyName, &i4VpnPolicyRowStatus);
        if (nmhGetFsVpnPolicyType (pPolicyName, &i4PolicyType) == SNMP_SUCCESS)
        {
            if (((i4PolicyType == WEB_VPN_IPSEC_MANUAL)
                 || (i4PolicyType == WEB_VPN_XAUTH)
                 || (i4PolicyType == VPN_IKE_RA_PRESHAREDKEY))
                && (i4VpnPolicyRowStatus == ACTIVE))
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "Cannot Modify an Active Policy!");
                break;
            }
        }

        if (i4VpnPolicyRowStatus != ACTIVE)
        {
#ifdef CLI_WANTED

            if (VpnCreatePolicy (WEB_CLI_HANDLE, pPolicyName) == CLI_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Policy Creation Failed!");
                break;
            }

            /* Setting the cryto mode to pre-shared . Reff CLI Command : cryto mode <ipsec-manual> */

            if (VpnSetPolicyType (WEB_CLI_HANDLE, pPolicyName,
                                  WEB_VPN_PRESHARED) == CLI_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Failed to set Crypto policy to pre-shared mode!");
                break;
            }
#endif
            STRCPY (pHttp->au1Name, "ADDR_TYPE");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);
            u4AddrType = (UINT4) ATOI (pHttp->au1Value);

            if (u4AddrType == WEB_VPN_IKE_KEY_IPV4)
            {
                /*Setting the IpSec Gateway Address Reff CLI COMMAND : set peer <num_str> */

                STRCPY (pHttp->au1Name, "IPSEC_GW_IP");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                issDecodeSpecialChar (pHttp->au1Value);
                MEMSET (pWebVpnPolicy->au1IpsecGwIpAddr, '\0',
                        IPSEC_IPADDR_LEN);
                STRNCPY (pWebVpnPolicy->au1IpsecGwIpAddr, pHttp->au1Value,
                         (IPSEC_IPADDR_LEN - 1));

#ifdef CLI_WANTED
                u4PeerAddress =
                    OSIX_NTOHL (INET_ADDR (pWebVpnPolicy->au1IpsecGwIpAddr));
                if (VpnSetPeerIp (WEB_CLI_HANDLE, pPolicyName, u4PeerAddress) ==
                    VPN_FAILURE)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Failed to set Ipsec Gateway address!");
                    break;
                }
#endif
            }
            else if (u4AddrType == WEB_VPN_IKE_KEY_IPV6)
            {
                /*Setting the IpSec IPv6 Gateway Address Reff CLI COMMAND : set ipv6 peer <num_str> */

                STRCPY (pHttp->au1Name, "IPSEC_GW_IPV6");
                MEMSET (&Ipv6Address, '\0', sizeof (tIp6Addr));

                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                issDecodeSpecialChar (pHttp->au1Value);
                if (INET_ATON6 (pHttp->au1Value, &Ipv6Address) == 0)
                {
                    IssSendError (pHttp, (CONST INT1 *) "Invalid IPv6 address");
                    break;
                }

#ifdef CLI_WANTED
                if (VpnSetIpv6Peer (WEB_CLI_HANDLE, pPolicyName,
                                    Ipv6Address.u1_addr) == VPN_FAILURE)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Failed to set Ipsec V6 Gateway Address!");
                    break;
                }
#endif
            }

            /* Setting the IKE Version, Ref CLI COMMAND : set ike version {v1 | v2} */
            STRCPY (pHttp->au1Name, "IKE_VERSION");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            SSCANF ((CONST CHR1 *) pHttp->au1Value, "%x", &u4IkeVersion);

#ifdef CLI_WANTED
            if (VpnSetIkeVersion (WEB_CLI_HANDLE, pPolicyName,
                                  u4IkeVersion) == CLI_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "Failed to set IKE version");
                break;
            }
#endif

            /* Setting IKE Phase 1 Peer Type.
             ** Reff CLI COMMAND : isakmp peer identity <peer-type> <peer-value> */

            /* Set Peer Id type and Peer Id Value */

            STRCPY (pHttp->au1Name, "PEER_ID_TYPE");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            SSCANF ((CONST CHR1 *) pHttp->au1Value, "%u", &u4PeerType);

            STRCPY (pHttp->au1Name, "PEER_ID_VALUE");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);

#ifdef CLI_WANTED
            if (VpnSetPeerType (WEB_CLI_HANDLE, pPolicyName, u4PeerType,
                                pHttp->au1Value) == CLI_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Failed to set Ike Phase1 Peertype!");
                break;
            }

#endif
            /* Setting IKE Phase 1 Local Identity Type.
             * Reff CLI COMMAND : set local identity <local-type> <local-value> */

            /* Set Peer Id type and Peer Id Value for xauth */

            STRCPY (pHttp->au1Name, "LOCAL_ID_TYPE");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            SSCANF ((CONST CHR1 *) pHttp->au1Value, "%u", &u4LocalIdType);

            STRCPY (pHttp->au1Name, "LOCAL_ID_VALUE");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);

#ifdef CLI_WANTED
            if (VpnSetLocalIdType (WEB_CLI_HANDLE, pPolicyName, u4LocalIdType,
                                   pHttp->au1Value) == CLI_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Failed to set Ike Phase1 Local Id Type!");
                break;
            }
#endif

          /*************************************************************************************   
           * Setting the Ike Phase 1 Proposal With the following Parameters  
           * IpSec Encryption - DES/3DES/AES128/AES192/AES256
           * IpSec Authentication - hmac-mds/hmac-sha1
           * DH Group - Group 1/ Group 2/ Group 5                     
           * Exchange - main / aggressive 
           * LifeTime - Seconds / Minutes / Hours 
           * LifeTime Value                  
           * 
           Reff CLI  COMMAND : 
           isakmp policy encryption {des | triple-des | aes | aes-192 | aes-256}  hash {sha1|md5} 
                                     dh {group1|group2|group5}  exch {main|aggressive}
                                     lifetime {secs|mins|hrs} <lifetime-value>

          ****************************************************************************************/

            STRCPY (pHttp->au1Name, "P1_IPSEC_ENCR");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            MEMSET (pWebVpnPolicy->au1Phase1Encryption, '\0',
                    IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1Phase1Encryption, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

            STRCPY (pHttp->au1Name, "P1_IPSEC_AUTH");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            MEMSET (pWebVpnPolicy->au1Phase1Authentication, '\0',
                    IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1Phase1Authentication, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

            STRCPY (pHttp->au1Name, "DH_GROUP");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            MEMSET (pWebVpnPolicy->au1Phase1DHGroup, '\0', IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1Phase1DHGroup, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

            STRCPY (pHttp->au1Name, "EXCHANGE_MODE");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            MEMSET (pWebVpnPolicy->au1Phase1ExchangeType, '\0',
                    IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1Phase1ExchangeType, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

            STRCPY (pHttp->au1Name, "P1_LIFE_TIME");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            MEMSET (pWebVpnPolicy->au1Phase1LifeTimeType, '\0',
                    IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1Phase1LifeTimeType, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

            STRCPY (pHttp->au1Name, "P1_TIME_VALUE");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            MEMSET (pWebVpnPolicy->au1Phase1LifeTimeValue, '\0',
                    IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1Phase1LifeTimeValue, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

            SSCANF ((CONST CHR1 *)
                    pWebVpnPolicy->au1Phase1Encryption, "%u", &u4EncrAlgo);
            SSCANF ((CONST CHR1 *)
                    pWebVpnPolicy->au1Phase1Authentication, "%u", &u4HashAlgo);
            SSCANF ((CONST CHR1 *)
                    pWebVpnPolicy->au1Phase1DHGroup, "%u", &u4DHGrp);
            SSCANF ((CONST CHR1 *)
                    pWebVpnPolicy->au1Phase1ExchangeType, "%u", &u4ExchMode);
            SSCANF ((CONST CHR1 *)
                    pWebVpnPolicy->au1Phase1LifeTimeType, "%u",
                    &u4LifeTimeType);
            SSCANF ((CONST CHR1 *)
                    pWebVpnPolicy->au1Phase1LifeTimeValue, "%u", &u4LifeTime);

#ifdef CLI_WANTED
            if (VpnCliSetVpnPolicyIkeProposal (WEB_CLI_HANDLE,
                                               pPolicyName, u4EncrAlgo,
                                               u4HashAlgo, u4DHGrp,
                                               u4ExchMode,
                                               u4LifeTimeType,
                                               u4LifeTime) == CLI_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Failed to set Ike Phase1 Proposal!");
                break;
            }
#endif

          /*************************************************************************************   
           * Setting the Ike Phase 2 Proposal With the following Parameters  
           * IpSec Protocol - AH/ESP                                  
           * IpSec Encryption - None/DES/3DES/AES128/AES192/AES256
           * IpSec Authentication - hmac-mds/hmac-sha1
           * IpSec Mode - Transport/Tunnel 
           * PFS/DH Group - Group 1/ Group 2/ Group 5                     
           * LifeTime - Seconds / Minutes / Hours 
           * LifeTime Value                  
           * 
           Reff CLI  COMMAND : 
                                crypto map ipsec {esp encryption {null | des | triple-des | aes |
                                aes-192 | aes-256 }} {esp|ah} {md5|sha1}  mode {tunnel|transport}
                                [pfs {group1| group2| group5} ] [lifetime {secs|mins|hrs} 
                                <lifetime-value>]

          ****************************************************************************************/

            STRCPY (pHttp->au1Name, "P2_IPSEC_PROT");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            MEMSET (pWebVpnPolicy->au1Phase2Protocol, '\0', IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1Phase2Protocol, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

            STRCPY (pHttp->au1Name, "P2_IKE_ENCR");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            MEMSET (pWebVpnPolicy->au1Phase2Encryption, '\0',
                    IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1Phase2Encryption, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

            if (STRCMP (pHttp->au1Value, "\0") == 0)
            {
                u4EncrAlgo = 0;
            }
            else
            {
                MEMSET (pWebVpnPolicy->au1Phase2Encryption, '\0',
                        IPSEC_WEB_ARG_LEN);
                STRNCPY (pWebVpnPolicy->au1Phase2Encryption, pHttp->au1Value,
                         (IPSEC_WEB_ARG_LEN - 1));
                SSCANF ((CONST CHR1 *)
                        pWebVpnPolicy->au1Phase2Encryption, "%u", &u4EncrAlgo);

            }

            STRCPY (pHttp->au1Name, "P2_IPSEC_AUTH");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            MEMSET (pWebVpnPolicy->au1Phase2Authentication, '\0',
                    IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1Phase2Authentication, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

            STRCPY (pHttp->au1Name, "P2_IPSEC_MODE");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            MEMSET (pWebVpnPolicy->au1Phase2Mode, '\0', IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1Phase2Mode, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

            STRCPY (pHttp->au1Name, "P2_PFS");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            MEMSET (pWebVpnPolicy->au1Phase2DHGroup, '\0', IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1Phase2DHGroup, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

            STRCPY (pHttp->au1Name, "P2_LIFE_TIME");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            MEMSET (pWebVpnPolicy->au1Phase2LifeTimeType, '\0',
                    IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1Phase2LifeTimeType, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

            STRCPY (pHttp->au1Name, "P2_TIME_VALUE");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            MEMSET (pWebVpnPolicy->au1Phase2LifeTimeValue, '\0',
                    IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1Phase2LifeTimeValue, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

            if (STRCMP (pHttp->au1Value, "\0") == 0)
            {
                u4LifeTime = 0;
            }
            else
            {
                SSCANF ((CONST CHR1 *)
                        pWebVpnPolicy->au1Phase2LifeTimeValue, "%u",
                        &u4LifeTime);
            }

            SSCANF ((CONST CHR1 *)
                    pWebVpnPolicy->au1Phase2Protocol, "%u", &u4Protocol);
            SSCANF ((CONST CHR1 *)
                    pWebVpnPolicy->au1Phase2Authentication, "%u", &u4HashAlgo);
            SSCANF ((CONST CHR1 *) pWebVpnPolicy->au1Phase2Mode, "%u", &u4Mode);
            SSCANF ((CONST CHR1 *)
                    pWebVpnPolicy->au1Phase2DHGroup, "%u", &u4DHGrp);
            SSCANF ((CONST CHR1 *)
                    pWebVpnPolicy->au1Phase2LifeTimeType, "%u",
                    &u4LifeTimeType);

            /* Setting the ipsec mode Reff CLI COMMAND : crypto ipsec mode {tunnel | transport}  */

            if (STRCMP (pWebVpnPolicy->au1Phase2Mode, WEB_TUNNEL) == 0)    /* Tunnel Mode */
            {
                u4Mode = WEB_VPN_TUNNEL;
            }
            else if (STRCMP (pWebVpnPolicy->au1Phase2Mode, WEB_TRANSPORT) == 0)    /* Transport Mode */
            {
                u4Mode = WEB_VPN_TRANSPORT;
            }
            else
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Ipsec Mode!");
                break;
            }

#ifdef CLI_WANTED
            if (VpnSetPolicyMode (WEB_CLI_HANDLE, pPolicyName, u4Mode) ==
                CLI_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "Failed to set Ipsec Mode!");
                break;
            }

            if (VpnCliSetVpnPolicyIpsecProposal (WEB_CLI_HANDLE, pPolicyName,
                                                 u4EncrAlgo, u4Protocol,
                                                 u4HashAlgo, u4DHGrp,
                                                 u4LifeTimeType,
                                                 u4LifeTime) == CLI_FAILURE)

            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Failed to set Ike Phase2 Proposal!");
                break;
            }

#endif
            STRCPY (pHttp->au1Name, "ADDR_TYPE");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);
            u4AddrType = (UINT4) ATOI (pHttp->au1Value);

            if (u4AddrType == WEB_VPN_IKE_KEY_IPV4)
            {
                /* Configuration of Traffic Selector  Reff CLI Command
                   access-list {permit|deny|apply} {any|tcp|udp|icmpv4|ahproto|espproto}
                   source <num_str> <num_str> destination <num_str> <num_str> 
                   [srcport <num_str>] [destport <num_str>] */

                STRCPY (pHttp->au1Name, "LCL_IP");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                issDecodeSpecialChar (pHttp->au1Value);
                MEMSET (pWebVpnPolicy->au1IpsecLocalIpAddr, '\0',
                        IPSEC_IPADDR_LEN);
                STRNCPY (pWebVpnPolicy->au1IpsecLocalIpAddr, pHttp->au1Value,
                         (IPSEC_IPADDR_LEN - 1));

                STRCPY (pHttp->au1Name, "LCL_MASK");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                issDecodeSpecialChar (pHttp->au1Value);
                MEMSET (pWebVpnPolicy->au1IpsecLocalIpMask, '\0',
                        IPSEC_IPADDR_LEN);
                STRNCPY (pWebVpnPolicy->au1IpsecLocalIpMask, pHttp->au1Value,
                         (IPSEC_IPADDR_LEN - 1));

                STRCPY (pHttp->au1Name, "REM_ADD");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                issDecodeSpecialChar (pHttp->au1Value);
                MEMSET (pWebVpnPolicy->au1IpsecRemoteIpAddr, '\0',
                        IPSEC_IPADDR_LEN);
                STRNCPY (pWebVpnPolicy->au1IpsecRemoteIpAddr, pHttp->au1Value,
                         (IPSEC_IPADDR_LEN - 1));

                STRCPY (pHttp->au1Name, "REM_MASK");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                issDecodeSpecialChar (pHttp->au1Value);
                MEMSET (pWebVpnPolicy->au1IpsecRemoteIpMask, '\0',
                        IPSEC_IPADDR_LEN);
                STRNCPY (pWebVpnPolicy->au1IpsecRemoteIpMask, pHttp->au1Value,
                         (IPSEC_IPADDR_LEN - 1));

                LocalNet.IpAddr.uIpAddr.Ip4Addr =
                    OSIX_NTOHL (INET_ADDR (pWebVpnPolicy->au1IpsecLocalIpAddr));
                IPV4_MASK_TO_MASKLEN (LocalNet.u4AddrPrefixLen,
                                      OSIX_NTOHL (INET_ADDR
                                                  (pWebVpnPolicy->
                                                   au1IpsecLocalIpMask)));
                RemoteNet.IpAddr.uIpAddr.Ip4Addr =
                    OSIX_NTOHL (INET_ADDR
                                (pWebVpnPolicy->au1IpsecRemoteIpAddr));
                IPV4_MASK_TO_MASKLEN (RemoteNet.u4AddrPrefixLen,
                                      OSIX_NTOHL (INET_ADDR
                                                  (pWebVpnPolicy->
                                                   au1IpsecRemoteIpMask)));
            }
            else
            {
                /* Configuration of Traffic Selector  Reff CLI Command
                 * access-list ipv6 {permit|deny|apply} {any|tcp|udp|icmpv6|ahproto|espproto}
                 *  source <ucast_addr> <prefixlen> destination <ucast_addr> <prefixlen>
                 *   [srcport <random_str>] [destport <random_str>] */

                STRCPY (pHttp->au1Name, "LCLV6_ADD");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                issDecodeSpecialChar (pHttp->au1Value);
                if ((pip6addr = str_to_ip6addr (pHttp->au1Value)) != NULL)
                {
                    MEMCPY (LocalNet.IpAddr.uIpAddr.Ip6Addr.u1_addr,
                            pip6addr, sizeof (tIp6Addr));
                }

                STRCPY (pHttp->au1Name, "LCL_PREFIX");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                issDecodeSpecialChar (pHttp->au1Value);
                LocalNet.u4AddrPrefixLen = (UINT4) ATOI (pHttp->au1Value);

                STRCPY (pHttp->au1Name, "REMV6_ADD");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                issDecodeSpecialChar (pHttp->au1Value);
                if ((pip6addr = str_to_ip6addr (pHttp->au1Value)) != NULL)
                {
                    MEMCPY (RemoteNet.IpAddr.uIpAddr.Ip6Addr.u1_addr,
                            pip6addr, sizeof (tIp6Addr));
                }

                STRCPY (pHttp->au1Name, "REM_PREFIX");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                issDecodeSpecialChar (pHttp->au1Value);
                RemoteNet.u4AddrPrefixLen = (UINT4) ATOI (pHttp->au1Value);
            }

            STRCPY (pHttp->au1Name, "LCL_PORT");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);
            MEMSET (pWebVpnPolicy->au1IpSecLocalPortRange, '\0',
                    WEB_PORT_RANGE_LENGTH);
            STRNCPY (pWebVpnPolicy->au1IpSecLocalPortRange, pHttp->au1Value,
                     (WEB_PORT_RANGE_LENGTH - 1));

            i4Len = (INT4) STRLEN (pWebVpnPolicy->au1IpSecLocalPortRange);

            if ((pSrcPortRange = allocmem_octetstring (i4Len + 1)) == NULL)
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "Failed to Set Traffic Selector!");
                break;
            }

            MEMSET (pSrcPortRange->pu1_OctetList, 0, (size_t) (i4Len + 1));
            MEMCPY (pSrcPortRange->pu1_OctetList,
                    pWebVpnPolicy->au1IpSecLocalPortRange, i4Len);
            pSrcPortRange->i4_Length = i4Len;

            STRCPY (pHttp->au1Name, "REM_PORT");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);
            MEMSET (pWebVpnPolicy->au1IpSecRemotePortRange, '\0',
                    WEB_PORT_RANGE_LENGTH);
            STRNCPY (pWebVpnPolicy->au1IpSecRemotePortRange, pHttp->au1Value,
                     (WEB_PORT_RANGE_LENGTH - 1));

            i4Len = (INT4) STRLEN (pWebVpnPolicy->au1IpSecRemotePortRange);

            if ((pDstPortRange = allocmem_octetstring (i4Len + 1)) == NULL)
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "Failed to Set Traffic Selector!");
                break;
            }

            MEMSET (pDstPortRange->pu1_OctetList, 0, (size_t) (i4Len + 1));
            MEMCPY (pDstPortRange->pu1_OctetList,
                    pWebVpnPolicy->au1IpSecRemotePortRange, i4Len);
            pDstPortRange->i4_Length = i4Len;

            STRCPY (pHttp->au1Name, "IPSEC_PROTO");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);
            MEMSET (pWebVpnPolicy->au1IpSecTransportProtocol, '\0',
                    IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1IpSecTransportProtocol, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

#ifdef CLI_WANTED
            u4Action = WEB_SEC_APPLY;
#endif
            SSCANF ((CONST CHR1 *)
                    pWebVpnPolicy->au1IpSecTransportProtocol, "%u", &u4Proto);

#ifdef CLI_WANTED
            if (u4AddrType == WEB_VPN_IKE_KEY_IPV4)
            {
                if (VpnSetAccessParams
                    (pPolicyName, &LocalNet, &RemoteNet,
                     u4Action, u4Proto, pSrcPortRange,
                     pDstPortRange) == CLI_FAILURE)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Failed to set Traffic Selector!");
                    break;
                }
            }
            else
            {
                if (VpnSetIpv6AccessParams
                    (pPolicyName, &LocalNet, &RemoteNet,
                     u4Action, u4Proto, pSrcPortRange,
                     pDstPortRange) == CLI_FAILURE)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Failed to set IPv6 Traffic Selector!");
                    break;
                }
            }
            /* Applying the policy to the interface. Reff CLI COMMAND : crypto map apply <policy name> */

#endif
            STRCPY (pHttp->au1Name, "IPSEC_INTF");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);

            MEMSET (au1IfName, 0, sizeof (au1IfName));
            piIfName = (INT1 *) &au1IfName[0];
            /* We get interface as either in numeric or alias format like "1" or "Fa0/1"
             * so check the format first and fetch the interface number 
             */
            if (STRSTR (pHttp->au1Value, "/") != NULL)
            {
                for (u4IfIndex = 1; u4IfIndex <= SYS_MAX_INTERFACES;
                     u4IfIndex++)
                {

                    CfaCliGetIfName (u4IfIndex, piIfName);
                    if (STRCMP (piIfName, pHttp->au1Value) == 0)
                    {
                        break;
                    }

                }
            }
            else
            {
                u4IfIndex = (UINT4) ATOI (pHttp->au1Value);
            }
            if (nmhTestv2FsVpnPolicyIntfIndex
                (&u4SnmpErrorStatus, pPolicyName,
                 (INT4) u4IfIndex) == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "VPN policies can be applied only on "
                              "untrusted/Public L3 interfaces(PPP, Ethernet etc)"
                              " or Invalid ip address is assigned to the"
                              " Interface");

                break;
            }
            nmhSetFsVpnPolicyIntfIndex (pPolicyName, (INT4) u4IfIndex);
        }
        if (u4VpnStatus == VPN_POLICY_ENABLE)
        {

            if (nmhTestv2FsVpnPolicyRowStatus (&u4SnmpErrorStatus, pPolicyName,
                                               ACTIVE) == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to Set Policy to Interface");
                break;
            }

            if (nmhSetFsVpnPolicyRowStatus (pPolicyName, ACTIVE) ==
                SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to Set Policy to Interface");
                break;
            }
        }
        else
        {
            if (nmhTestv2FsVpnPolicyRowStatus (&u4SnmpErrorStatus, pPolicyName,
                                               NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "Unable to Modify the Policy");
                break;
            }

            if (nmhSetFsVpnPolicyRowStatus (pPolicyName, NOT_IN_SERVICE) ==
                SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "Unable to Modify the Policy");
                break;
            }
        }

        u1VpnReturnStatus = TRUE;
    }
    while (0);

    if (pPolicyName != NULL)
    {

#ifdef CLI_WANTED
        if (u1PolicyDelFlag == TRUE)
        {
            if (VpnDeletePolicy (WEB_CLI_HANDLE, pPolicyName) == VPN_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Policy Deletion Failed!");
            }

            else
            {
                u1VpnReturnStatus = TRUE;
            }
        }
#endif
        free_octetstring (pPolicyName);
    }

    if (pSrcPortRange != NULL)
    {
        free_octetstring (pSrcPortRange);
    }

    if (pDstPortRange != NULL)
    {
        free_octetstring (pDstPortRange);
    }
    MemReleaseMemBlock (VPN_WEB_IKE_POLICY_PID, (UINT1 *) pWebVpnPolicy);

    VpnDsUnLock ();
    WebnmUnRegisterLock (pHttp);
    if (u1VpnReturnStatus == TRUE)
    {
        IssProcessIkeConfPageGet (pHttp, NULL, u1PolicyDelFlag);
    }
    return;

}

/*********************************************************************
*  Function Name : IssProcessVpnPolicyConfPageGet 
*  Description   : This function processes the get request coming for the  
*                  Vpn Policy web page.   
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessVpnPolicyConfPageGet (tHttp * pHttp, UINT1 *pu1PolicyName)
{
    tSNMP_OCTET_STRING_TYPE *pPolicyName = NULL;
    tSNMP_OCTET_STRING_TYPE *pPortRange = NULL;
    INT4                i4Len = 0;
    INT4                i4Size = 0;
    INT4                i4VpnStatus = 0;
    INT4                i4RaVpnMode = 0;
    INT4                i4IkeVersion = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4PrefixLen = 0;
    INT4                i4PolicyType = 0;
    INT4                i4RetVal = 0;
    INT4                i4SecRetVal = 0;
    UINT1               au1pString[16];
    tVpnPolicy         *pVpnPolicy = NULL;
    UINT1              *pu1DisplayBuffer = NULL;
    UINT1              *pu1TempBuffer = NULL;
    UINT1               au1TempStr[WEB_MAX_NAME_LENGTH];
    UINT1               au1VpnIntfName[CFA_MAX_PORT_NAME_LENGTH];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT1               au1IpAddr[IP6_ADDR_SIZE];
    INT4                i4AddrType = 0;
    tIp6Addr            Ipv6Address;
    UINT1               u1AdminStatus = 0;

    pHttp->i4Write = 0;
    MEMSET (au1IpAddr, '\0', IP6_ADDR_SIZE);

    MEMSET (au1TempStr, '\0', WEB_MAX_NAME_LENGTH);
    WebnmRegisterLock (pHttp, VpnDsLock, VpnDsUnLock);
    VpnDsLock ();

    nmhGetFsVpnGlobalStatus (&i4VpnStatus);
    STRCPY (pHttp->au1KeyString, "VPN_STATUS_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SNPRINTF ((CHR1 *) pHttp->au1DataString, ENM_MAX_NAME_LEN, "%d",
              i4VpnStatus);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    nmhGetFsVpnRaServer (&i4RaVpnMode);
    STRCPY (pHttp->au1KeyString, "RAVPN_MODE_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SNPRINTF ((CHR1 *) pHttp->au1DataString, ENM_MAX_NAME_LEN,
              "%d", i4RaVpnMode);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    VpnPrintPolicies (pHttp, pu1PolicyName, NULL, 0);

    if (pu1PolicyName != NULL)
    {
        i4Len = (INT4) STRLEN (pu1PolicyName);
        pPolicyName = allocmem_octetstring (i4Len + 1);
        if (pPolicyName == NULL)
        {
            VpnDsUnLock ();
            WebnmUnRegisterLock (pHttp);
            return;
        }
        MEMCPY (pPolicyName->pu1_OctetList, pu1PolicyName, i4Len);
        pPolicyName->i4_Length = i4Len;
        pPolicyName->pu1_OctetList[i4Len] = '\0';
        pVpnPolicy =
            VpnSnmpGetPolicy (pPolicyName->pu1_OctetList,
                              pPolicyName->i4_Length);

        if (MemAllocateMemBlock
            (VPN_WEB_POLL_BUFF_PID,
             (UINT1 **) &pu1DisplayBuffer) != MEM_SUCCESS)
        {
            STRCPY (pHttp->au1KeyString, "POL_PARAM_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SNPRINTF ((CHR1 *) pHttp->au1DataString, ENM_MAX_NAME_LEN, "%s",
                      " ");
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            free_octetstring (pPolicyName);
            VpnDsUnLock ();
            WebnmUnRegisterLock (pHttp);
            return;

        }
        if (MemAllocateMemBlock
            (VPN_WEB_POLL_BUFF_PID, (UINT1 **) &pu1TempBuffer) != MEM_SUCCESS)
        {
            STRCPY (pHttp->au1KeyString, "POL_PARAM_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SNPRINTF ((CHR1 *) pHttp->au1DataString, ENM_MAX_NAME_LEN, "%s",
                      " ");
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            free_octetstring (pPolicyName);
            MemReleaseMemBlock (VPN_WEB_POLL_BUFF_PID,
                                (UINT1 *) pu1DisplayBuffer);
            VpnDsUnLock ();
            WebnmUnRegisterLock (pHttp);
            return;

        }

        if ((pVpnPolicy == NULL) ||
            (pu1DisplayBuffer == NULL) || (pu1TempBuffer == NULL))
        {
            STRCPY (pHttp->au1KeyString, "POL_PARAM_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SNPRINTF ((CHR1 *) pHttp->au1DataString, ENM_MAX_NAME_LEN, "%s",
                      " ");
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            free_octetstring (pPolicyName);
            MemReleaseMemBlock (VPN_WEB_POLL_BUFF_PID,
                                (UINT1 *) pu1DisplayBuffer);
            MemReleaseMemBlock (VPN_WEB_POLL_BUFF_PID, (UINT1 *) pu1TempBuffer);
            VpnDsUnLock ();
            WebnmUnRegisterLock (pHttp);
            return;
        }

        MEMSET (pu1DisplayBuffer, 0, WEB_POL_BUFF);
        MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
        MEMSET (au1VpnIntfName, 0, CFA_MAX_PORT_NAME_LENGTH);

        SPRINTF ((CHR1 *) pu1DisplayBuffer,
                 "Policy Name              :  %s \r\n",
                 pPolicyName->pu1_OctetList);

        if (pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE)
        {
            STRCAT (pu1DisplayBuffer,
                    "Policy Status            :  Active \r\n");
        }
        else
        {
            STRCAT (pu1DisplayBuffer,
                    "Policy Status            :  Inactive \r\n");
        }

        if (nmhGetFsVpnPolicyType (pPolicyName, &i4PolicyType) == SNMP_SUCCESS)
        {
            if (i4PolicyType == VPN_IPSEC_MANUAL)
            {
                STRCAT (pu1DisplayBuffer,
                        "Policy Type              :  IPSEC MANUAL \r\n");
            }
            else if (i4PolicyType == VPN_IKE_PRESHAREDKEY)
            {
                STRCAT (pu1DisplayBuffer,
                        "Policy Type              :  IKE PRESHARED   \r\n");
            }
            else if (i4PolicyType == VPN_IKE_RA_PRESHAREDKEY)
            {
                STRCAT (pu1DisplayBuffer,
                        "Policy Type              :  RAVPN PRESHARED-KEY  \r\n");
            }
            else if (i4PolicyType == VPN_XAUTH)
            {
                STRCAT (pu1DisplayBuffer,
                        "Policy Type              :  RAVPN XAUTH    \r\n");
            }
        }
        else
        {
            STRCAT (pu1DisplayBuffer,
                    "Policy Type              :               \r\n");
        }

        /* Skipping version incase of VPN_IPSEC_MANUAL */
        if (i4PolicyType != VPN_IPSEC_MANUAL)
        {
            if (nmhGetFsVpnIkeVersion (pPolicyName, &i4IkeVersion) ==
                SNMP_SUCCESS)
            {
                MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                STRCPY (pu1TempBuffer, pu1DisplayBuffer);
                if (i4IkeVersion == CLI_VPN_IKE_V2)
                {
                    SPRINTF ((CHR1 *) pu1DisplayBuffer,
                             "%sIKE VERSION              :  IKEv2 \r\n",
                             pu1TempBuffer);
                }
                else
                {
                    SPRINTF ((CHR1 *) pu1DisplayBuffer,
                             "%sIKE VERSION              :  IKEv1 \r\n",
                             pu1TempBuffer);
                }
            }
            else
            {
                STRCAT (pu1DisplayBuffer,
                        "IKE VERSION              :           \r\n");
            }
        }
        nmhGetFsVpnProtectNetworkType (pPolicyName, &i4AddrType);
        if (i4AddrType == WEB_VPN_IKE_KEY_IPV4)
        {
            OctetStr.i4_Length = sizeof (UINT4);
            OctetStr.pu1_OctetList = au1IpAddr;
            if (nmhGetFsVpnLocalProtectNetwork (pPolicyName, &OctetStr) ==
                SNMP_SUCCESS)
            {
                MEMCPY (&u4IpAddr, OctetStr.pu1_OctetList, sizeof (UINT4));
                MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                STRNCPY (pu1TempBuffer, pu1DisplayBuffer, (WEB_POL_BUFF - 1));
                VPN_INET_NTOA (au1pString, u4IpAddr);
                SPRINTF ((CHR1 *) pu1DisplayBuffer,
                         "%sLocal Network Address    :  %s \r\n", pu1TempBuffer,
                         au1pString);
            }
            else
            {
                STRCAT (pu1DisplayBuffer,
                        "Local Network Address    :           \r\n");
            }

            if (nmhGetFsVpnLocalProtectNetworkPrefixLen (pPolicyName,
                                                         &u4PrefixLen) ==
                SNMP_SUCCESS)
            {
                IPV4_MASKLEN_TO_MASK (u4IpAddr, u4PrefixLen);
                MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                STRCPY (pu1TempBuffer, pu1DisplayBuffer);
                VPN_INET_NTOA (au1pString, u4IpAddr);
                SPRINTF ((CHR1 *) pu1DisplayBuffer,
                         "%sLocal Network Mask       :  %s \r\n", pu1TempBuffer,
                         au1pString);
            }
            else
            {
                STRCAT (pu1DisplayBuffer,
                        "Local Network Mask       :           \r\n");
            }
        }
        else
        {
            OctetStr.i4_Length = IP6_ADDR_SIZE;
            OctetStr.pu1_OctetList = au1IpAddr;
            if (nmhGetFsVpnLocalProtectNetwork (pPolicyName, &OctetStr) ==
                SNMP_SUCCESS)
            {
                MEMCPY (&Ipv6Address, OctetStr.pu1_OctetList, IP6_ADDR_SIZE);
                MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                STRNCPY (pu1TempBuffer, pu1DisplayBuffer, (WEB_POL_BUFF - 1));
                SPRINTF ((CHR1 *) pu1DisplayBuffer,
                         "%sLocal Network Address    :  %s \r\n", pu1TempBuffer,
                         Ip6PrintAddr (&Ipv6Address));
            }
            else
            {
                STRCAT (pu1DisplayBuffer,
                        "Local Network Address    :           \r\n");
            }

            if (nmhGetFsVpnLocalProtectNetworkPrefixLen (pPolicyName,
                                                         &u4PrefixLen) ==
                SNMP_SUCCESS)
            {
                SPRINTF ((CHR1 *) pu1DisplayBuffer,
                         "%sLocal Network Prefix Length       :  %d \r\n",
                         pu1TempBuffer, u4PrefixLen);
            }
            else
            {
                STRCAT (pu1DisplayBuffer,
                        "Local Network Prefix Length       :           \r\n");
            }
        }

        pPortRange = allocmem_octetstring (WEB_PORT_RANGE_LENGTH);
        if (pPortRange != NULL)
        {
            MEMSET (pPortRange->pu1_OctetList, 0, WEB_PORT_RANGE_LENGTH);

            if (nmhGetFsVpnIkeSrcPortRange (pPolicyName, pPortRange) ==
                SNMP_SUCCESS)
            {
                MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                STRCPY (pu1TempBuffer, pu1DisplayBuffer);
                SPRINTF ((CHR1 *) pu1DisplayBuffer,
                         "%sLocal Port Range         :  %s \r\n", pu1TempBuffer,
                         pPortRange->pu1_OctetList);
            }
            else
            {
                STRCAT (pu1DisplayBuffer,
                        "Local Port Range         :           \r\n");
            }
        }
        else
        {
            STRCAT (pu1DisplayBuffer,
                    "Local Port Range         :           \r\n");
        }

        nmhGetFsVpnProtectNetworkType (pPolicyName, &i4AddrType);
        if (i4AddrType == WEB_VPN_IKE_KEY_IPV4)
        {
            OctetStr.i4_Length = sizeof (UINT4);
            OctetStr.pu1_OctetList = au1IpAddr;
            if (nmhGetFsVpnRemoteProtectNetwork (pPolicyName, &OctetStr) ==
                SNMP_SUCCESS)
            {
                MEMCPY (&u4IpAddr, OctetStr.pu1_OctetList, sizeof (UINT4));
                MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                STRCPY (pu1TempBuffer, pu1DisplayBuffer);
                VPN_INET_NTOA (au1pString, u4IpAddr);
                SPRINTF ((CHR1 *) pu1DisplayBuffer,
                         "%sRemote Network Address   :  %s \r\n", pu1TempBuffer,
                         au1pString);
            }
            else
            {
                STRCAT (pu1DisplayBuffer,
                        "Remote Network Address   :           \r\n");
            }

            if (nmhGetFsVpnRemoteProtectNetworkPrefixLen (pPolicyName,
                                                          &u4PrefixLen) ==
                SNMP_SUCCESS)
            {
                IPV4_MASKLEN_TO_MASK (u4IpAddr, u4PrefixLen);
                MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                STRCPY (pu1TempBuffer, pu1DisplayBuffer);
                VPN_INET_NTOA (au1pString, u4IpAddr);
                SPRINTF ((CHR1 *) pu1DisplayBuffer,
                         "%sRemote Network Mask      :  %s \r\n", pu1TempBuffer,
                         au1pString);
            }
            else
            {
                STRCAT (pu1DisplayBuffer,
                        "Remote Network Mask      :           \r\n");
            }
        }

        else
        {
            OctetStr.i4_Length = IP6_ADDR_SIZE;
            OctetStr.pu1_OctetList = au1IpAddr;
            if (nmhGetFsVpnRemoteProtectNetwork (pPolicyName, &OctetStr) ==
                SNMP_SUCCESS)
            {
                MEMCPY (&Ipv6Address, OctetStr.pu1_OctetList, IP6_ADDR_SIZE);
                MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                STRNCPY (pu1TempBuffer, pu1DisplayBuffer, (WEB_POL_BUFF - 1));
                SPRINTF ((CHR1 *) pu1DisplayBuffer,
                         "%sRemote Network Address    :  %s \r\n",
                         pu1TempBuffer, Ip6PrintAddr (&Ipv6Address));
            }
            else
            {
                STRCAT (pu1DisplayBuffer,
                        "Remote Network Address    :           \r\n");
            }

            if (nmhGetFsVpnRemoteProtectNetworkPrefixLen (pPolicyName,
                                                          &u4PrefixLen) ==
                SNMP_SUCCESS)
            {
                SPRINTF ((CHR1 *) pu1DisplayBuffer,
                         "%sRemote Network Prefix Length       :  %d \r\n",
                         pu1TempBuffer, u4PrefixLen);
            }
            else
            {
                STRCAT (pu1DisplayBuffer,
                        "Remote Network Prefix Length       :           \r\n");
            }
        }

        if (pPortRange != NULL)
        {
            MEMSET (pPortRange->pu1_OctetList, 0, WEB_PORT_RANGE_LENGTH);

            if (nmhGetFsVpnIkeDstPortRange (pPolicyName, pPortRange) ==
                SNMP_SUCCESS)
            {
                MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                STRCPY (pu1TempBuffer, pu1DisplayBuffer);
                SPRINTF ((CHR1 *) pu1DisplayBuffer,
                         "%sRemote Port Range        :  %s \r\n", pu1TempBuffer,
                         pPortRange->pu1_OctetList);
            }
            else
            {
                STRCAT (pu1DisplayBuffer,
                        "Remote Port Range        :           \r\n");
            }
            free_octetstring (pPortRange);
        }
        else
        {
            STRCAT (pu1DisplayBuffer,
                    "Remote Port Range        :           \r\n");
        }

        if (nmhGetFsVpnMode (pPolicyName, &i4RetVal) == SNMP_SUCCESS)
        {
            if (i4RetVal == WEB_VPN_TRANSPORT)
            {
                STRCAT (pu1DisplayBuffer,
                        "Security Mode            :  Transport\r\n");
            }
            else if (i4RetVal == WEB_VPN_TUNNEL)
            {
                STRCAT (pu1DisplayBuffer,
                        "Security Mode            :  Tunnel\r\n");
            }
            else
            {
                STRCAT (pu1DisplayBuffer,
                        "Security Mode            :  Unknown Mode\r\n");
            }
        }
        else
        {
            STRCAT (pu1DisplayBuffer, "Security Mode            :  \r\n");
        }

        nmhGetFsVpnTunTermAddrType (pPolicyName, &i4AddrType);
        if (i4AddrType == WEB_VPN_IKE_KEY_IPV4)
        {
            OctetStr.i4_Length = sizeof (UINT4);
            OctetStr.pu1_OctetList = au1IpAddr;
            if (nmhGetFsVpnLocalTunTermAddr (pPolicyName, &OctetStr) !=
                SNMP_FAILURE)
            {
                MEMCPY (&u4IpAddr, OctetStr.pu1_OctetList, sizeof (UINT4));
                MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                STRCPY (pu1TempBuffer, pu1DisplayBuffer);
                VPN_INET_NTOA (au1pString, u4IpAddr);
                SPRINTF ((CHR1 *) pu1DisplayBuffer,
                         "%sLocal Tunnel Term Addr   :  %s \r\n", pu1TempBuffer,
                         au1pString);
            }
            else
            {
                STRCAT (pu1DisplayBuffer,
                        "Local Tunnel Term Addr    :           \r\n");
            }

            OctetStr.i4_Length = sizeof (UINT4);
            OctetStr.pu1_OctetList = au1IpAddr;
            if (nmhGetFsVpnRemoteTunTermAddr (pPolicyName, &OctetStr) !=
                SNMP_FAILURE)
            {
                MEMCPY (&u4IpAddr, OctetStr.pu1_OctetList, sizeof (UINT4));
                MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                STRCPY (pu1TempBuffer, pu1DisplayBuffer);
                VPN_INET_NTOA (au1pString, u4IpAddr);
                SPRINTF ((CHR1 *) pu1DisplayBuffer,
                         "%sRemote Tunnel Term Addr  :  %s \r\n", pu1TempBuffer,
                         au1pString);
            }
            else
            {
                STRCAT (pu1DisplayBuffer,
                        "Remote Tunnel Term Addr    :           \r\n");
            }
        }
        else
        {
            OctetStr.i4_Length = IP6_ADDR_SIZE;
            OctetStr.pu1_OctetList = au1IpAddr;
            if (nmhGetFsVpnLocalTunTermAddr (pPolicyName, &OctetStr) !=
                SNMP_FAILURE)
            {
                MEMCPY (&Ipv6Address, OctetStr.pu1_OctetList, IP6_ADDR_SIZE);
                MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                STRCPY (pu1TempBuffer, pu1DisplayBuffer);
                SPRINTF ((CHR1 *) pu1DisplayBuffer,
                         "%sLocal Tunnel Term Add   :  %s \r\n", pu1TempBuffer,
                         Ip6PrintAddr (&Ipv6Address));
            }
            else
            {
                STRCAT (pu1DisplayBuffer,
                        "Local Tunnel Term Addr    :           \r\n");
            }

            OctetStr.i4_Length = IP6_ADDR_SIZE;
            OctetStr.pu1_OctetList = au1IpAddr;
            if (nmhGetFsVpnRemoteTunTermAddr (pPolicyName, &OctetStr) !=
                SNMP_FAILURE)
            {
                MEMCPY (&Ipv6Address, OctetStr.pu1_OctetList, IP6_ADDR_SIZE);
                MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                STRCPY (pu1TempBuffer, pu1DisplayBuffer);
                SPRINTF ((CHR1 *) pu1DisplayBuffer,
                         "%sRemote Tunnel Term Addr  :  %s \r\n", pu1TempBuffer,
                         Ip6PrintAddr (&Ipv6Address));
            }
            else
            {
                STRCAT (pu1DisplayBuffer,
                        "Remote Tunnel Term Addr    :           \r\n");
            }
        }

        if (nmhGetFsVpnPolicyIntfIndex (pPolicyName, &i4RetVal) == SNMP_SUCCESS)
        {
            if (i4RetVal == 0)
            {
                STRCAT (pu1DisplayBuffer,
                        "Interface Name           :  Not Configured\r\n");
            }
            else
            {
                MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                STRCPY (pu1TempBuffer, pu1DisplayBuffer);
                CfaGetInterfaceNameFromIndex ((UINT4) i4RetVal, au1VpnIntfName);
                SPRINTF ((CHR1 *) pu1DisplayBuffer,
                         "%sInterface Name           :  %s \r\n", pu1TempBuffer,
                         au1VpnIntfName);

                /* To display the VPN interface UP/DOWN */
                CfaApiGetIfAdminStatus (i4RetVal, &u1AdminStatus);
                if (u1AdminStatus == CFA_IF_UP)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Tunnel Status            :  UP \r\n");
                }
                else if (u1AdminStatus == CFA_IF_DOWN)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Tunnel Status            :  DOWN \r\n");
                }

            }
        }
        else
        {
            STRCAT (pu1DisplayBuffer, "Interface Name           :  \r\n");
        }

        if (i4PolicyType == VPN_IPSEC_MANUAL)
        {

            nmhGetFsVpnTunTermAddrType (pPolicyName, &i4AddrType);
            if (i4AddrType == WEB_VPN_IKE_KEY_IPV4)
            {
                OctetStr.i4_Length = sizeof (UINT4);
                OctetStr.pu1_OctetList = au1IpAddr;
                if (nmhGetFsVpnLocalTunTermAddr (pPolicyName, &OctetStr) ==
                    SNMP_SUCCESS)
                {
                    MEMCPY (&u4IpAddr, OctetStr.pu1_OctetList, sizeof (UINT4));
                    MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                    STRCPY (pu1TempBuffer, pu1DisplayBuffer);
                    VPN_INET_NTOA (au1pString, u4IpAddr);
                    SPRINTF ((CHR1 *) pu1DisplayBuffer,
                             "%sLocal Gateway            :  %s \r\n",
                             pu1TempBuffer, au1pString);
                }
                else
                {
                    STRCAT (pu1DisplayBuffer,
                            "Local Gateway            :           \r\n");
                }

                OctetStr.i4_Length = sizeof (UINT4);
                OctetStr.pu1_OctetList = au1IpAddr;
                if (nmhGetFsVpnRemoteTunTermAddr (pPolicyName, &OctetStr) ==
                    SNMP_SUCCESS)
                {
                    MEMCPY (&u4IpAddr, OctetStr.pu1_OctetList, sizeof (UINT4));
                    MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                    STRCPY (pu1TempBuffer, pu1DisplayBuffer);
                    VPN_INET_NTOA (au1pString, u4IpAddr);
                    SPRINTF ((CHR1 *) pu1DisplayBuffer,
                             "%sRemote Gateway           :  %s \r\n",
                             pu1TempBuffer, au1pString);
                }
                else
                {
                    STRCAT (pu1DisplayBuffer,
                            "Remote Gateway           :           \r\n");
                }

            }
            else
            {
                OctetStr.i4_Length = IP6_ADDR_SIZE;
                OctetStr.pu1_OctetList = au1IpAddr;
                if (nmhGetFsVpnLocalTunTermAddr (pPolicyName, &OctetStr) ==
                    SNMP_SUCCESS)
                {
                    MEMCPY (&Ipv6Address, OctetStr.pu1_OctetList,
                            IP6_ADDR_SIZE);
                    MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                    STRCPY (pu1TempBuffer, pu1DisplayBuffer);
                    SPRINTF ((CHR1 *) pu1DisplayBuffer,
                             "%sLocal Gateway            :  %s \r\n",
                             pu1TempBuffer, Ip6PrintAddr (&Ipv6Address));
                }
                else
                {
                    STRCAT (pu1DisplayBuffer,
                            "Local Gateway            :           \r\n");
                }

                OctetStr.i4_Length = IP6_ADDR_SIZE;
                OctetStr.pu1_OctetList = au1IpAddr;
                if (nmhGetFsVpnRemoteTunTermAddr (pPolicyName, &OctetStr) ==
                    SNMP_SUCCESS)
                {
                    MEMCPY (&Ipv6Address, OctetStr.pu1_OctetList,
                            IP6_ADDR_SIZE);
                    MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                    STRCPY (pu1TempBuffer, pu1DisplayBuffer);
                    SPRINTF ((CHR1 *) pu1DisplayBuffer,
                             "%sRemote Gateway           :  %s \r\n",
                             pu1TempBuffer, Ip6PrintAddr (&Ipv6Address));
                }
                else
                {
                    STRCAT (pu1DisplayBuffer,
                            "Remote Gateway           :           \r\n");
                }
            }

            if (nmhGetFsVpnProtocol (pPolicyName, &i4RetVal) == SNMP_SUCCESS)
            {

                if (i4RetVal == WEB_TCP)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Policy Protocol          :  TCP\r\n");
                }
                else if (i4RetVal == WEB_UDP)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Policy Protocol          :  UDP\r\n");
                }
                else if (i4RetVal == WEB_ICMP)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Policy Protocol          :  ICMP\r\n");
                }
                else if (i4RetVal == WEB_AH)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Policy Protocol          :  AH\r\n");
                }
                else if (i4RetVal == WEB_ESP)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Policy Protocol          :  ESP\r\n");
                }
                else if (i4RetVal == WEB_ANY_PROTO)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Policy Protocol          :  ANY\r\n");
                }
                else
                {
                    STRCAT (pu1DisplayBuffer,
                            "Policy Protocol          :  Unknown Protocol\r\n");
                }
            }
            else
            {
                STRCAT (pu1DisplayBuffer, "Policy Protocol          :  \r\n");
            }

            if (nmhGetFsVpnSecurityProtocol (pPolicyName, &i4RetVal) ==
                SNMP_SUCCESS)
            {
                if (i4RetVal == WEB_AH)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Security Protocol        :  AH\r\n");
                }
                else if (i4RetVal == WEB_ESP)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Security Protocol        :  ESP\r\n");
                }
                else
                {
                    STRCAT (pu1DisplayBuffer,
                            "Security Protocol        :  Unknown Protocol\r\n");
                }
            }
            else
            {
                STRCAT (pu1DisplayBuffer, "Security Protocol        :  \r\n");
            }

            if (nmhGetFsVpnInboundSpi (pPolicyName, &i4RetVal) == SNMP_SUCCESS)
            {
                MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                STRCPY (pu1TempBuffer, pu1DisplayBuffer);
                SPRINTF ((CHR1 *) pu1DisplayBuffer,
                         "%sInBound SPI              :  %d \r\n",
                         pu1TempBuffer, i4RetVal);
            }
            else
            {
                STRCAT (pu1DisplayBuffer, "InBound SPI              :  \r\n");
            }

            if (nmhGetFsVpnOutboundSpi (pPolicyName, &i4RetVal) == SNMP_SUCCESS)
            {
                MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                STRCPY (pu1TempBuffer, pu1DisplayBuffer);
                SPRINTF ((CHR1 *) pu1DisplayBuffer,
                         "%sOutBound SPI             :  %d \r\n",
                         pu1TempBuffer, i4RetVal);
            }
            else
            {
                STRCAT (pu1DisplayBuffer, "OutBound SPI             :  \r\n");
            }

            if (nmhGetFsVpnAuthAlgo (pPolicyName, &i4RetVal) == SNMP_SUCCESS)
            {
                if (i4RetVal == WEB_HMAC_MD5)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Authentication Algorithm :  HMAC MD5\r\n");
                }
                if (i4RetVal == WEB_HMAC_SHA1)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Authentication Algorithm :  HMAC SHA1\r\n");
                }
                if (i4RetVal == SEC_XCBCMAC)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Authentication Algorithm :  XCBC MAC\r\n");
                }
                if (i4RetVal == HMAC_SHA_256)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Authentication Algorithm :  HMAC_SHA_256\r\n");
                }
                if (i4RetVal == HMAC_SHA_384)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Authentication Algorithm :  HMAC_SHA_384\r\n");
                }
                if (i4RetVal == HMAC_SHA_512)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Authentication Algorithm :  HMAC_SHA_512\r\n");
                }
            }
            else
            {
                STRCAT (pu1DisplayBuffer, "Authentication Algorithm :  \r\n");
            }

            if (nmhGetFsVpnSecurityProtocol (pPolicyName, &i4RetVal) ==
                SNMP_SUCCESS)
            {
                if (i4RetVal == WEB_AH)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Encryption Algorithm     :  Not Configured\r\n");
                }
            }

            if (nmhGetFsVpnEncrAlgo (pPolicyName, &i4RetVal) == SNMP_SUCCESS)
            {
                if (i4RetVal == WEB_DES)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Encryption Algorithm     :  DES\r\n");
                }
                else if (i4RetVal == WEB_3DES)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Encryption Algorithm     :  3DES\r\n");
                }
                else if (i4RetVal == WEB_AES_128)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Encryption Algorithm     :  AES-128\r\n");
                }
                else if (i4RetVal == WEB_AES_192)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Encryption Algorithm     :  AES-192\r\n");
                }
                else if (i4RetVal == WEB_AES_256)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Encryption Algorithm     :  AES-256\r\n");
                }
                else if (i4RetVal == SEC_AESCTR)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Encryption Algorithm     :  AES-CTR-128\r\n");
                }
                else if (i4RetVal == SEC_AESCTR192)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Encryption Algorithm     :  AES-CTR-192\r\n");
                }
                else if (i4RetVal == SEC_AESCTR256)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Encryption Algorithm     :  AES-CTR-256\r\n");
                }
                else
                {
                    STRCAT (pu1DisplayBuffer,
                            "Encryption Algorithm     :  Unknown Algorithm\r\n");
                }
            }
            else
            {
                STRCAT (pu1DisplayBuffer, "Encryption Algorithm     :  \r\n");
            }

            if (nmhGetFsVpnAntiReplay (pPolicyName, &i4RetVal) == SNMP_SUCCESS)
            {
                if (i4RetVal == WEB_ANTI_REPLAY_ENABLE)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Anti Replay              :  Enable\r\n");
                }
                else
                {
                    STRCAT (pu1DisplayBuffer,
                            "Anti Replay              :  Disable\r\n");
                }
            }
            else
            {
                STRCAT (pu1DisplayBuffer, "Anti Replay              :  \r\n");
            }

            if (nmhGetFsVpnPolicyFlag (pPolicyName, &i4RetVal) == SNMP_SUCCESS)
            {
                if (i4RetVal == WEB1_SEC_APPLY)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Policy Action            :  Apply\r\n");
                }
                else if (i4RetVal == WEB_SEC_BYPASS)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Policy Action            :  Bypass\r\n");
                }
                else
                {
                    STRCAT (pu1DisplayBuffer,
                            "Policy Action            :  Filter\r\n");
                }
            }
            else
            {
                STRCAT (pu1DisplayBuffer, "Policy Action            :  \r\n");
            }

        }
        else if ((i4PolicyType == VPN_IKE_PRESHAREDKEY) ||
                 (i4PolicyType == VPN_IKE_RA_PRESHAREDKEY)
                 || (i4PolicyType == VPN_XAUTH))
        {
            if (nmhGetFsVpnProtocol (pPolicyName, &i4RetVal) == SNMP_SUCCESS)
            {

                if (i4RetVal == WEB_TCP)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Policy Protocol          :  TCP\r\n");
                }
                else if (i4RetVal == WEB_UDP)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Policy Protocol          :  UDP\r\n");
                }
                else if (i4RetVal == WEB_ICMP)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Policy Protocol          :  ICMP\r\n");
                }
                else if (i4RetVal == WEB_AH)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Policy Protocol          :  AH\r\n");
                }
                else if (i4RetVal == WEB_ESP)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Policy Protocol          :  ESP\r\n");
                }
                else if (i4RetVal == WEB_ANY_PROTO)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Policy Protocol          :  ANY\r\n");
                }
                else
                {
                    STRCAT (pu1DisplayBuffer,
                            "Policy Protocol          :  Unknown Protocol\r\n");
                }
            }
            STRCAT (pu1DisplayBuffer, "----------------------------\r\n");
            STRCAT (pu1DisplayBuffer, "ISAKMP INFO ( PHASE 1 )  :  \r\n");
            STRCAT (pu1DisplayBuffer, "----------------------------\r\n");

            if (nmhGetFsVpnIkePhase1EncryptionAlgo (pPolicyName, &i4RetVal) !=
                SNMP_FAILURE)
            {
                if (i4RetVal == WEB_DES)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Encryption Algorithm     :  DES\r\n");
                }
                else if (i4RetVal == WEB_3DES)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Encryption Algorithm     :  3DES\r\n");
                }
                else if (i4RetVal == WEB_AES_128)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Encryption Algorithm     :  AES-128\r\n");
                }
                else if (i4RetVal == WEB_AES_192)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Encryption Algorithm     :  AES-192\r\n");
                }
                else if (i4RetVal == WEB_AES_256)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Encryption Algorithm     :  AES-256\r\n");
                }
                else
                {
                    STRCAT (pu1DisplayBuffer,
                            "Encryption Algorithm     :  Unknown Algorithm\r\n");
                }
            }
            else
            {
                STRCAT (pu1DisplayBuffer, "Encryption Algorithm     :  \r\n");
            }

            if (nmhGetFsVpnIkePhase1HashAlgo (pPolicyName, &i4RetVal) !=
                SNMP_FAILURE)
            {
                if (i4RetVal == WEB_HMAC_MD5)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Authentication Algorithm :  HMAC MD5\r\n");
                }
                if (i4RetVal == WEB_HMAC_SHA1)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Authentication Algorithm :  HMAC SHA1\r\n");
                }
                if (i4RetVal == SEC_XCBCMAC)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Authentication Algorithm :  XCBC MAC\r\n");
                }
                if (i4RetVal == HMAC_SHA_256)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Authentication Algorithm :  HMAC_SHA_256\r\n");
                }
                if (i4RetVal == HMAC_SHA_384)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Authentication Algorithm :  HMAC_SHA_384\r\n");
                }
                if (i4RetVal == HMAC_SHA_512)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Authentication Algorithm :  HMAC_SHA_512\r\n");
                }

            }
            else
            {
                STRCAT (pu1DisplayBuffer, "Authentication Algorithm :  \r\n");
            }

            if (nmhGetFsVpnIkePhase1DHGroup (pPolicyName, &i4RetVal) !=
                SNMP_FAILURE)
            {
                if (i4RetVal == WEB_VPN_IKE_DH_GROUP_1)
                {
                    STRCAT (pu1DisplayBuffer,
                            "DH Group                 :  DH Group 1\r\n");
                }
                if (i4RetVal == WEB_VPN_IKE_DH_GROUP_2)
                {
                    STRCAT (pu1DisplayBuffer,
                            "DH Group                 :  DH Group 2\r\n");
                }
                if (i4RetVal == WEB_VPN_IKE_DH_GROUP_5)
                {
                    STRCAT (pu1DisplayBuffer,
                            "DH Group                 :  DH Group 5\r\n");
                }
                if (i4RetVal == SEC_DH_GROUP_14)
                {
                    STRCAT (pu1DisplayBuffer,
                            "DH Group                 :  DH Group 14\r\n");
                }

            }
            else
            {
                STRCAT (pu1DisplayBuffer,
                        "DH Group                 :           \r\n");
            }

            if (i4IkeVersion == CLI_VPN_IKE_V1)
            {
                if (nmhGetFsVpnIkePhase1Mode (pPolicyName, &i4RetVal) !=
                    SNMP_FAILURE)
                {
                    if (i4RetVal == WEB_VPN_IKE_MAIN_MODE)
                    {
                        STRCAT (pu1DisplayBuffer,
                                "IKE Mode                 :  Main      \r\n");
                    }
                    if (i4RetVal == WEB_VPN_IKE_AGGRESSIVE_MODE)
                    {
                        STRCAT (pu1DisplayBuffer,
                                "IKE Mode                 :  Aggressive\r\n");
                    }

                }
                else
                {
                    STRCAT (pu1DisplayBuffer,
                            "IKE Mode                 :           \r\n");
                }
            }

            if (nmhGetFsVpnIkePhase1LifeTime (pPolicyName, &i4RetVal) !=
                SNMP_FAILURE)
            {
                MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                STRCPY (pu1TempBuffer, pu1DisplayBuffer);
                SPRINTF ((CHR1 *) pu1DisplayBuffer,
                         "%sLife Time                :  %d ", pu1TempBuffer,
                         i4RetVal);
            }
            else
            {
                STRCAT (pu1DisplayBuffer, "Life Time                :        ");
            }

            if (nmhGetFsVpnIkePhase1LifeTimeType (pPolicyName, &i4RetVal) !=
                SNMP_FAILURE)
            {
                if (i4RetVal == WEB_LIFETIME_SECS)
                {
                    STRCAT (pu1DisplayBuffer, "Secs \r\n");
                }
                if (i4RetVal == WEB_LIFETIME_KB)
                {
                    STRCAT (pu1DisplayBuffer, "KBs  \r\n");
                }
                if (i4RetVal == WEB_LIFETIME_MINS)
                {
                    STRCAT (pu1DisplayBuffer, "Minutes  \r\n");
                }
                if (i4RetVal == WEB_LIFETIME_HRS)
                {
                    STRCAT (pu1DisplayBuffer, "Hours  \r\n");
                }
                if (i4RetVal == WEB_LIFETIME_DAYS)
                {
                    STRCAT (pu1DisplayBuffer, "Days  \r\n");
                }

            }
            else
            {
                STRCAT (pu1DisplayBuffer, "  \r\n");
            }
            if ((i4PolicyType == VPN_XAUTH) ||
                (i4PolicyType == VPN_IKE_RA_PRESHAREDKEY))
            {
                STRCAT (pu1DisplayBuffer, "----------------------------\r\n");
                STRCAT (pu1DisplayBuffer, "RAVPN INFO               : \r\n");
                STRCAT (pu1DisplayBuffer, "----------------------------\r\n");
            }

            if (i4PolicyType == VPN_IKE_PRESHAREDKEY)
            {
                STRCAT (pu1DisplayBuffer, "----------------------------\r\n");
                STRCAT (pu1DisplayBuffer, "IKE_PRESHARED INFO       : \r\n");
                STRCAT (pu1DisplayBuffer, "----------------------------\r\n");
            }
            MEMSET (au1TempStr, 0, sizeof (au1TempStr));
            OctetStr.pu1_OctetList = au1TempStr;

            if (nmhGetFsVpnIkePhase1LocalIdType (pPolicyName, &i4RetVal) !=
                SNMP_FAILURE)
            {

                nmhGetFsVpnIkePhase1LocalIdValue (pPolicyName, &OctetStr);
                switch (i4RetVal)
                {
                    case VPN_ID_TYPE_IPV4:
                        STRCAT (pu1DisplayBuffer,
                                "Local Identity Type      : IPv4 \r\n");
                        MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                        STRCPY (pu1TempBuffer, pu1DisplayBuffer);
                        SPRINTF ((CHR1 *) pu1DisplayBuffer,
                                 "%sLocal Identity value     : %s \r\n",
                                 pu1TempBuffer, OctetStr.pu1_OctetList);
                        break;

                    case VPN_ID_TYPE_EMAIL:
                        STRCAT (pu1DisplayBuffer,
                                "Local Identity Type      : email \r\n");
                        MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                        STRCPY (pu1TempBuffer, pu1DisplayBuffer);
                        SPRINTF ((CHR1 *) pu1DisplayBuffer,
                                 "%sLocal Identity value     : %s \r\n",
                                 pu1TempBuffer, OctetStr.pu1_OctetList);
                        break;

                    case VPN_ID_TYPE_FQDN:
                        STRCAT (pu1DisplayBuffer,
                                "Local Identity Type      : fqdn \r\n");
                        MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                        STRCPY (pu1TempBuffer, pu1DisplayBuffer);
                        SPRINTF ((CHR1 *) pu1DisplayBuffer,
                                 "%sLocal Identity value     : %s \r\n",
                                 pu1TempBuffer, OctetStr.pu1_OctetList);
                        break;

                    case VPN_ID_TYPE_KEYID:
                        STRCAT (pu1DisplayBuffer,
                                "Local Identity Type      : keyid \r\n");
                        MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                        STRCPY (pu1TempBuffer, pu1DisplayBuffer);
                        SPRINTF ((CHR1 *) pu1DisplayBuffer,
                                 "%sLocal Identity value     : %s \r\n",
                                 pu1TempBuffer, OctetStr.pu1_OctetList);

                        break;

                    case VPN_ID_TYPE_IPV6:
                        STRCAT (pu1DisplayBuffer,
                                "Local Identity Type      : IPv6 \r\n");
                        MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                        STRCPY (pu1TempBuffer, pu1DisplayBuffer);
                        SPRINTF ((CHR1 *) pu1DisplayBuffer,
                                 "%sLocal Identity value     : %s \r\n",
                                 pu1TempBuffer, OctetStr.pu1_OctetList);
                        break;

                    default:
                        STRCAT (pu1DisplayBuffer,
                                "Local Identity Type      : default \r\n");
                        MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                        STRCPY (pu1TempBuffer, pu1DisplayBuffer);
                        SPRINTF ((CHR1 *) pu1DisplayBuffer,
                                 "%sLocal Identity value     : %s \r\n",
                                 pu1TempBuffer, OctetStr.pu1_OctetList);
                        break;

                }
            }

            MEMSET (au1TempStr, 0, sizeof (au1TempStr));
            OctetStr.pu1_OctetList = au1TempStr;

            if (nmhGetFsVpnIkePhase1PeerIdType (pPolicyName, &i4RetVal) !=
                SNMP_FAILURE)
            {

                nmhGetFsVpnIkePhase1PeerIdValue (pPolicyName, &OctetStr);
                switch (i4RetVal)
                {
                    case VPN_ID_TYPE_IPV4:
                        STRCAT (pu1DisplayBuffer,
                                "Peer Identity Type       : IPv4 \r\n");
                        MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                        STRCPY (pu1TempBuffer, pu1DisplayBuffer);
                        SPRINTF ((CHR1 *) pu1DisplayBuffer,
                                 "%sPeer Identity value      : %s \r\n",
                                 pu1TempBuffer, OctetStr.pu1_OctetList);
                        break;

                    case VPN_ID_TYPE_EMAIL:
                        STRCAT (pu1DisplayBuffer,
                                "Peer Identity Type       : email \r\n");
                        MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                        STRCPY (pu1TempBuffer, pu1DisplayBuffer);
                        SPRINTF ((CHR1 *) pu1DisplayBuffer,
                                 "%sPeer Identity value      : %s \r\n",
                                 pu1TempBuffer, OctetStr.pu1_OctetList);
                        break;

                    case VPN_ID_TYPE_FQDN:
                        STRCAT (pu1DisplayBuffer,
                                "Peer Identity Type       : fqdn \r\n");
                        MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                        STRCPY (pu1TempBuffer, pu1DisplayBuffer);
                        SPRINTF ((CHR1 *) pu1DisplayBuffer,
                                 "%sPeer Identity value      : %s \r\n",
                                 pu1TempBuffer, OctetStr.pu1_OctetList);
                        break;

                    case VPN_ID_TYPE_KEYID:
                        STRCAT (pu1DisplayBuffer,
                                "Peer Identity Type       : keyid \r\n");
                        MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                        STRCPY (pu1TempBuffer, pu1DisplayBuffer);
                        SPRINTF ((CHR1 *) pu1DisplayBuffer,
                                 "%sPeer Identity value      : %s \r\n",
                                 pu1TempBuffer, OctetStr.pu1_OctetList);
                        break;

                    case VPN_ID_TYPE_IPV6:
                        STRCAT (pu1DisplayBuffer,
                                "Peer Identity Type       : IPv6 \r\n");
                        MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                        STRCPY (pu1TempBuffer, pu1DisplayBuffer);
                        SPRINTF ((CHR1 *) pu1DisplayBuffer,
                                 "%sPeer Identity value      : %s \r\n",
                                 pu1TempBuffer, OctetStr.pu1_OctetList);
                        break;

                    default:
                        STRCAT (pu1DisplayBuffer,
                                "Peer Identity Type      : default \r\n");
                        MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                        STRCPY (pu1TempBuffer, pu1DisplayBuffer);
                        SPRINTF ((CHR1 *) pu1DisplayBuffer,
                                 "%sPeer Identity value      : %s \r\n",
                                 pu1TempBuffer, OctetStr.pu1_OctetList);
                        break;

                }
            }

            STRCAT (pu1DisplayBuffer, "----------------------------\r\n");
            STRCAT (pu1DisplayBuffer, "ISAKMP INFO ( PHASE II ) : \r\n");
            STRCAT (pu1DisplayBuffer, "----------------------------\r\n");

            if (nmhGetFsVpnIkePhase2EspEncryptionAlgo (pPolicyName, &i4RetVal)
                != SNMP_FAILURE)
            {
                if (i4RetVal == WEB_NULL)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Encryption Algorithm     :  NULL\r\n");
                }
                else if (i4RetVal == WEB_DES)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Encryption Algorithm     :  DES\r\n");
                }
                else if (i4RetVal == WEB_3DES)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Encryption Algorithm     :  3DES\r\n");
                }
                else if (i4RetVal == WEB_AES_128)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Encryption Algorithm     :  AES-128\r\n");
                }
                else if (i4RetVal == WEB_AES_192)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Encryption Algorithm     :  AES-192\r\n");
                }
                else if (i4RetVal == WEB_AES_256)
                {
                    STRCAT (pu1DisplayBuffer,
                            "Encryption Algorithm     :  AES-256\r\n");
                }

            }
            else
            {
                STRCAT (pu1DisplayBuffer,
                        "Encryption Algorithm     :         \r\n");
            }
            if (nmhGetFsVpnSecurityProtocol (pPolicyName, &i4SecRetVal) !=
                SNMP_FAILURE)
            {
                if (i4SecRetVal == WEB_ESP)
                {

                    if (nmhGetFsVpnIkePhase2AuthAlgo (pPolicyName, &i4RetVal) !=
                        SNMP_FAILURE)
                    {
                        if (i4RetVal == WEB_HMAC_MD5)
                        {
                            STRCAT (pu1DisplayBuffer,
                                    "Authentication Algorithm :  ESP - HMAC MD5\r\n");
                        }
                        if (i4RetVal == WEB_HMAC_SHA1)
                        {
                            STRCAT (pu1DisplayBuffer,
                                    "Authentication Algorithm :  ESP - HMAC SHA1\r\n");
                        }
                        if (i4RetVal == SEC_XCBCMAC)
                        {
                            STRCAT (pu1DisplayBuffer,
                                    "Authentication Algorithm :  ESP - XCBC MAC\r\n");
                        }
                        if (i4RetVal == HMAC_SHA_256)
                        {
                            STRCAT (pu1DisplayBuffer,
                                    "Authentication Algorithm :  ESP - HMAC_SHA_256\r\n");
                        }
                        if (i4RetVal == HMAC_SHA_384)
                        {
                            STRCAT (pu1DisplayBuffer,
                                    "Authentication Algorithm :  ESP - HMAC_SHA_384\r\n");
                        }
                        if (i4RetVal == HMAC_SHA_512)
                        {
                            STRCAT (pu1DisplayBuffer,
                                    "Authentication Algorithm :  ESP - HMAC_SHA_512\r\n");
                        }

                    }
                    else
                    {
                        STRCAT (pu1DisplayBuffer,
                                "Authentication Algorithm :           \r\n");
                    }
                }
                else
                {
                    if (nmhGetFsVpnIkePhase2AuthAlgo (pPolicyName, &i4RetVal) !=
                        SNMP_FAILURE)
                    {
                        if (i4RetVal == WEB_HMAC_MD5)
                        {
                            STRCAT (pu1DisplayBuffer,
                                    "Authentication Algorithm :  AH - HMAC MD5\r\n");
                        }
                        if (i4RetVal == WEB_HMAC_SHA1)
                        {
                            STRCAT (pu1DisplayBuffer,
                                    "Authentication Algorithm :  AH - HMAC SHA1\r\n");
                        }
                        if (i4RetVal == SEC_XCBCMAC)
                        {
                            STRCAT (pu1DisplayBuffer,
                                    "Authentication Algorithm :  AH - XCBC MAC\r\n");
                        }
                        if (i4RetVal == HMAC_SHA_256)
                        {
                            STRCAT (pu1DisplayBuffer,
                                    "Authentication Algorithm :  AH - HMAC_SHA_256\r\n");
                        }
                        if (i4RetVal == HMAC_SHA_384)
                        {
                            STRCAT (pu1DisplayBuffer,
                                    "Authentication Algorithm :  AH - HMAC_SHA_384\r\n");
                        }
                        if (i4RetVal == HMAC_SHA_512)
                        {
                            STRCAT (pu1DisplayBuffer,
                                    "Authentication Algorithm :  AH - HMAC_SHA_512\r\n");
                        }

                    }
                    else
                    {
                        STRCAT (pu1DisplayBuffer,
                                "Authentication Algorithm :           \r\n");
                    }
                }
            }

            if (nmhGetFsVpnIkePhase2DHGroup (pPolicyName, &i4RetVal) !=
                SNMP_FAILURE)
            {
                if (i4RetVal == WEB_VPN_IKE_DH_GROUP_1)
                {
                    STRCAT (pu1DisplayBuffer,
                            "DH Group                 :  PFS Group 1\r\n");
                }
                else if (i4RetVal == WEB_VPN_IKE_DH_GROUP_2)
                {
                    STRCAT (pu1DisplayBuffer,
                            "DH Group                 :  PFS Group 2\r\n");
                }
                if (i4RetVal == WEB_VPN_IKE_DH_GROUP_5)
                {
                    STRCAT (pu1DisplayBuffer,
                            "DH Group                 :  PFS Group 5\r\n");
                }
                if (i4RetVal == SEC_DH_GROUP_14)
                {
                    STRCAT (pu1DisplayBuffer,
                            "DH Group                 :  PFS Group 14\r\n");
                }
            }
            else
            {
                STRCAT (pu1DisplayBuffer,
                        "DH Group                 :           \r\n");
            }

            if (nmhGetFsVpnIkePhase2LifeTime (pPolicyName, &i4RetVal) !=
                SNMP_FAILURE)
            {
                MEMSET (pu1TempBuffer, 0, WEB_POL_BUFF);
                STRNCPY (pu1TempBuffer, pu1DisplayBuffer, (WEB_POL_BUFF - 1));
                SPRINTF ((CHR1 *) pu1DisplayBuffer,
                         "%sLife Time                :  %d ",
                         pu1TempBuffer, i4RetVal);
            }
            else
            {
                STRCAT (pu1DisplayBuffer, "Life Time                :        ");
            }

            if (nmhGetFsVpnIkePhase2LifeTimeType (pPolicyName, &i4RetVal) !=
                SNMP_FAILURE)
            {
                if (i4RetVal == WEB_LIFETIME_SECS)
                {
                    STRCAT (pu1DisplayBuffer, "Secs \r\n");
                }
                else if (i4RetVal == WEB_LIFETIME_KB)
                {
                    STRCAT (pu1DisplayBuffer, "KBs  \r\n");
                }
                else if (i4RetVal == WEB_LIFETIME_MINS)
                {
                    STRCAT (pu1DisplayBuffer, "Minutes  \r\n");
                }
                else if (i4RetVal == WEB_LIFETIME_HRS)
                {
                    STRCAT (pu1DisplayBuffer, "Hours  \r\n");
                }
                if (i4RetVal == WEB_LIFETIME_DAYS)
                {
                    STRCAT (pu1DisplayBuffer, "Days  \r\n");
                }

            }
            else
            {
                STRCAT (pu1DisplayBuffer, "      \r\n");
            }

        }

        STRCPY (pHttp->au1KeyString, "POL_PARAM_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        while (i4Size < (INT4) STRLEN (pu1DisplayBuffer))
        {
            SNPRINTF ((CHR1 *) pHttp->au1DataString, ENM_MAX_NAME_LEN, "%s",
                      pu1DisplayBuffer + i4Size);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            i4Size += ENM_MAX_NAME_LEN;
        }
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));

        MemReleaseMemBlock (VPN_WEB_POLL_BUFF_PID, (UINT1 *) pu1DisplayBuffer);
        MemReleaseMemBlock (VPN_WEB_POLL_BUFF_PID, (UINT1 *) pu1TempBuffer);

        free_octetstring (pPolicyName);
        VpnDsUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;

    }
    else
    {

        STRCPY (pHttp->au1KeyString, "POL_PARAM_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, ENM_MAX_NAME_LEN, "%s", " ");
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

    }

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    VpnDsUnLock ();
    WebnmUnRegisterLock (pHttp);
    return;

}

/*********************************************************************
*  Function Name : IssProcessVpnPolicyConfPageSet 
*  Description   : This function processes the Set request coming for the  
*                  Vpn Polciy web page.   
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessVpnPolicyConfPageSet (tHttp * pHttp)
{
    UINT1               au1VpnPolicyName[WEB_MAX_NAME_LENGTH];
    INT4                i4Len = 0;
    INT4                i4Status;
    tSNMP_OCTET_STRING_TYPE *pPolicyName = NULL;

    MEMSET (au1VpnPolicyName, '\0', WEB_MAX_NAME_LENGTH);
    WebnmRegisterLock (pHttp, VpnDsLock, VpnDsUnLock);
    VpnDsLock ();

    STRCPY (pHttp->au1Name, "VPN_STATUS");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4Status = (INT4) ATOI (pHttp->au1Value);
        nmhSetFsVpnGlobalStatus (i4Status);
    }

    STRCPY (pHttp->au1Name, "RAVPN_MODE");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4Status = (INT4) ATOI (pHttp->au1Value);
        nmhSetFsVpnRaServer (i4Status);
    }

    STRCPY (pHttp->au1Name, "IPSEC_POLICY");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_FAILURE)
    {
        VpnDsUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssProcessVpnPolicyConfPageGet (pHttp, NULL);
        return;
    }
    issDecodeSpecialChar (pHttp->au1Value);
    if (pHttp->au1Value[0] == '\0')
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Policy Name!");
        VpnDsUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }
    MEMSET (au1VpnPolicyName, '\0', sizeof (au1VpnPolicyName));
    STRNCPY (au1VpnPolicyName, pHttp->au1Value, (WEB_MAX_NAME_LENGTH - 1));

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {

        i4Len = (INT4) STRLEN (au1VpnPolicyName);
        if ((pPolicyName = allocmem_octetstring (i4Len + 1)) == NULL)
        {
            IssSendError (pHttp, (CONST INT1 *) "Policy Deletion Failed!");
            VpnDsUnLock ();
            WebnmUnRegisterLock (pHttp);
            return;
        }

        MEMSET (pPolicyName->pu1_OctetList, 0, (size_t) (i4Len + 1));
        MEMCPY (pPolicyName->pu1_OctetList, au1VpnPolicyName, i4Len);
        pPolicyName->i4_Length = i4Len;

#ifdef CLI_WANTED

        if (VpnDeletePolicy (WEB_CLI_HANDLE, pPolicyName) == VPN_FAILURE)
        {

            if (pPolicyName != NULL)
            {
                free_octetstring (pPolicyName);
            }
            IssSendError (pHttp, (CONST INT1 *) "Policy Deletion Failed!");
            VpnDsUnLock ();
            WebnmUnRegisterLock (pHttp);
            return;
        }
#endif

        if (pPolicyName != NULL)
        {
            free_octetstring (pPolicyName);
        }
        VpnDsUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssProcessVpnPolicyConfPageGet (pHttp, NULL);
        return;
    }
    else if (STRCMP (pHttp->au1Value, "Display") == 0)
    {
        VpnDsUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssProcessVpnPolicyConfPageGet (pHttp, au1VpnPolicyName);
        return;
    }

}

/*********************************************************************
*  Function Name : IssProcessVpnRaUserDBConfPageGet 
*  Description   : This function processes the Get request coming for the  
*                  Vpn Remote Access Database .   
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessVpnRaUserDBConfPageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE *pVpnRaUserName = NULL;
    tSNMP_OCTET_STRING_TYPE *pNextVpnRaUserName = NULL;
    UINT4               u4Temp = 0;
    INT4                i4UsrStatus;
    CHR1               *pc1Status = NULL;

    if ((pVpnRaUserName =
         allocmem_octetstring (RA_USER_NAME_LENGTH + 1)) == NULL)
    {
        return;
    }
    if ((pNextVpnRaUserName =
         allocmem_octetstring (RA_USER_NAME_LENGTH + 1)) == NULL)
    {
        free_octetstring (pVpnRaUserName);
        return;
    }

    MEMSET (pVpnRaUserName->pu1_OctetList, 0, RA_USER_NAME_LENGTH + 1);
    MEMSET (pNextVpnRaUserName->pu1_OctetList, 0, RA_USER_NAME_LENGTH + 1);

    pHttp->i4Write = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    WebnmRegisterLock (pHttp, VpnDsLock, VpnDsUnLock);
    VpnDsLock ();

    /*Getting the First entry of User Database Table. */
    if (nmhGetFirstIndexFsVpnRaUsersTable (pVpnRaUserName) == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        VpnDsUnLock ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (pVpnRaUserName);
        free_octetstring (pNextVpnRaUserName);
        return;
    }

    MEMCPY (pNextVpnRaUserName->pu1_OctetList,
            pVpnRaUserName->pu1_OctetList, pVpnRaUserName->i4_Length);
    pNextVpnRaUserName->pu1_OctetList[STRLEN (pVpnRaUserName->pu1_OctetList)] =
        '\0';

    pNextVpnRaUserName->i4_Length = pVpnRaUserName->i4_Length;

    /*Displaying all the Entries of the User Database. */
    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        STRCPY (pHttp->au1KeyString, "PROV_USERNAME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                 pNextVpnRaUserName->pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsVpnRaUserRowStatus (pNextVpnRaUserName, &i4UsrStatus);
        if (i4UsrStatus == ACTIVE)
        {
            pc1Status = "Enabled";
        }
        else
        {
            pc1Status = "Disabled";
        }
        STRCPY (pHttp->au1KeyString, "PROV_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pc1Status);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pVpnRaUserName->pu1_OctetList,
                pNextVpnRaUserName->pu1_OctetList);
        pVpnRaUserName->i4_Length = pNextVpnRaUserName->i4_Length;
        MEMSET (pNextVpnRaUserName->pu1_OctetList, 0, RA_USER_NAME_LENGTH + 1);

    }
    while (nmhGetNextIndexFsVpnRaUsersTable
           (pVpnRaUserName, pNextVpnRaUserName) != SNMP_FAILURE);

    VpnDsUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    free_octetstring (pVpnRaUserName);
    free_octetstring (pNextVpnRaUserName);
    return;
}

/*********************************************************************
*  Function Name : IssProcessVpnRaUserDBConfPageSet 
*  Description   : This function processes the Set request coming for the  
*                  Vpn Remote Access Database .   
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessVpnRaUserDBConfPageSet (tHttp * pHttp)
{
    INT4                i4Len = 0;
    tSNMP_OCTET_STRING_TYPE *pVpnRaUserName = NULL;
    tSNMP_OCTET_STRING_TYPE *pVpnRaUserSecret = NULL;
    INT4                i4RetVal = VPN_FAILURE;
    UINT4               u4SnmpErrorStatus = 0;

    WebnmRegisterLock (pHttp, VpnDsLock, VpnDsUnLock);
    VpnDsLock ();

    do
    {
        STRCPY (pHttp->au1Name, "VPN_USERNAME");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        issDecodeSpecialChar (pHttp->au1Value);
        i4Len = (INT4) STRLEN (pHttp->au1Value);

        if ((pVpnRaUserName = allocmem_octetstring (i4Len + 1)) == NULL)
        {
            IssSendError (pHttp, (CONST INT1 *) "Unable to Add an User!");
            break;
        }
        MEMSET (pVpnRaUserName->pu1_OctetList, 0, (size_t) (i4Len + 1));
        MEMCPY (pVpnRaUserName->pu1_OctetList, pHttp->au1Value, i4Len);
        pVpnRaUserName->i4_Length = i4Len;

        STRCPY (pHttp->au1Name, "VPN_PASSWORD");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        issDecodeSpecialChar (pHttp->au1Value);
        i4Len = (INT4) STRLEN (pHttp->au1Value);

        if ((pVpnRaUserSecret = allocmem_octetstring (i4Len + 1)) == NULL)
        {
            IssSendError (pHttp, (CONST INT1 *) "Unable to Add an User!");
            break;
        }
        MEMSET (pVpnRaUserSecret->pu1_OctetList, 0, (size_t) (i4Len + 1));
        MEMCPY (pVpnRaUserSecret->pu1_OctetList, pHttp->au1Value, i4Len);
        pVpnRaUserSecret->i4_Length = i4Len;
        STRCPY (pHttp->au1Name, "ACTION");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        if (STRCMP (pHttp->au1Value, "Apply") == 0)
        {

            if (nmhTestv2FsVpnRaUserRowStatus
                (&u4SnmpErrorStatus, pVpnRaUserName, VPN_STATUS_CREATE_AND_WAIT)
                == SNMP_FAILURE)
            {
                switch (u4SnmpErrorStatus)
                {
                    case SNMP_ERR_INCONSISTENT_NAME:
                        IssSendError (pHttp,
                                      (CONST INT1 *)
                                      " Entry already exists with the "
                                      "given name");

                        break;
                    case SNMP_ERR_INCONSISTENT_VALUE:
                        IssSendError (pHttp,
                                      (CONST INT1 *)
                                      " No.of ravpn users reached the"
                                      " maximum.");

                        break;
                    default:
                        break;
                }
                break;
            }
            if (pVpnRaUserSecret->i4_Length < VPN_MIN_RA_USER_SECRET_LEN)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Password should be greater than or equal to 4 "
                              "characters ");

                break;
            }
#ifdef CLI_WANTED
            if (VpnAddRaUserInfo (WEB_CLI_HANDLE, pVpnRaUserName,
                                  pVpnRaUserSecret) == CLI_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Unable to Add the User!");
                break;
            }
#endif

        }
        else if (STRCMP (pHttp->au1Value, "MODIFY") == 0)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Modifying the User is not supported currently!");
            break;

        }

#ifdef CLI_WANTED
        else if (STRCMP (pHttp->au1Value, "DELETE") == 0)
        {
            if (VpnDelRaUserInfo (WEB_CLI_HANDLE, pVpnRaUserName) ==
                CLI_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "Unable to Delete the User!");
                break;
            }
        }

#endif
        i4RetVal = VPN_SUCCESS;

        break;

    }
    while (0);

    if (pVpnRaUserName != NULL)
    {
        free_octetstring (pVpnRaUserName);
    }

    if (pVpnRaUserSecret != NULL)
    {
        free_octetstring (pVpnRaUserSecret);
    }

    VpnDsUnLock ();
    WebnmUnRegisterLock (pHttp);

    /*When configuration is succes, then we display the configured values.
     * If failure we display the error messageonly.*/

    if (i4RetVal == VPN_SUCCESS)
    {
        IssProcessVpnRaUserDBConfPageGet (pHttp);
    }
    return;

}

/*********************************************************************
*  Function Name : IssProcessVpnRaAddressPoolConfPageGet 
*  Description   : This function processes the Get request coming for the  
*                  Vpn Remote Access Address Pool .   
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessVpnRaAddressPoolConfPageGet (tHttp * pHttp)
{

    tSNMP_OCTET_STRING_TYPE *pVpnRaAddressPoolName = NULL;

    pHttp->i4Write = 0;

    WebnmRegisterLock (pHttp, VpnDsLock, VpnDsUnLock);
    VpnDsLock ();

    do
    {

        if ((pVpnRaAddressPoolName =
             allocmem_octetstring (RA_ADDRESS_POOL_NAME_LENGTH + 1)) == NULL)
        {
            break;
        }

        /* Displaying the Currently configured Address Pool. */
        MEMSET (pVpnRaAddressPoolName->pu1_OctetList, 0,
                RA_ADDRESS_POOL_NAME_LENGTH + 1);
        if (nmhGetFirstIndexFsVpnRaAddressPoolTable (pVpnRaAddressPoolName) ==
            SNMP_SUCCESS)
        {
            VpnGetAddressPoolInfo (pHttp, pVpnRaAddressPoolName);
            break;

        }
        VpnGetAddressPoolInfo (pHttp, NULL);

    }
    while (0);

    if (pVpnRaAddressPoolName != NULL)
    {
        free_octetstring (pVpnRaAddressPoolName);
    }

    VpnDsUnLock ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    return;
}

/*********************************************************************
*  Function Name : IssProcessVpnRaAddressPoolConfPageSet 
*  Description   : This function processes the Set request coming for the  
*                  Vpn Remote Access Address Pool .   
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessVpnRaAddressPoolConfPageSet (tHttp * pHttp)
{
    INT4                i4Len = 0;
    tSNMP_OCTET_STRING_TYPE *pTempVpnRaAddressPoolName = NULL;
    UINT4               u4TempVpnRaStartIp = 0;
    UINT4               u4TempVpnRaEndIp = 0;
    tSNMP_OCTET_STRING_TYPE *pVpnRaAddressPoolName = NULL;
#ifdef CLI_WANTED
    UINT4               u4VpnRaStartIp = 0;
    UINT4               u4VpnRaEndIp = 0;
    UINT4               u4PrefixLen = 0;
#endif
    INT4                i4RetVal = VPN_FAILURE;
    UINT1               au1IpAddr[IP6_ADDR_SIZE];
    tIp6Addr            StartIpv6Address;
    tIp6Addr            EndIpv6Address;
    UINT4               u4TempPrefixLen = 0;
    UINT4               u4AddrType = 0;
    tIp6Addr            TempVpnRaStartIpv6Addr;
    tIp6Addr            TempVpnRaEndIpv6Addr;
    tIp6Addr           *pip6addr = NULL;

    tSNMP_OCTET_STRING_TYPE OctetStr;

    WebnmRegisterLock (pHttp, VpnDsLock, VpnDsUnLock);
    VpnDsLock ();

    do
    {
        MEMSET (au1IpAddr, 0, IP6_ADDR_SIZE);

        if ((pTempVpnRaAddressPoolName =
             allocmem_octetstring (RA_ADDRESS_POOL_NAME_LENGTH + 1)) == NULL)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to Add an Address Pool!");
            break;
        }

        if ((pVpnRaAddressPoolName =
             allocmem_octetstring (RA_ADDRESS_POOL_NAME_LENGTH + 1)) == NULL)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to Add an Address Pool!");
            break;
        }

        MEMSET (pTempVpnRaAddressPoolName->pu1_OctetList, 0,
                RA_ADDRESS_POOL_NAME_LENGTH + 1);

        OctetStr.i4_Length = IP6_ADDR_SIZE;
        OctetStr.pu1_OctetList = au1IpAddr;

        /*If any AddressPool is configured it is deleted first and then
         * the new addresspool is added to the database.*/
        if (nmhGetFirstIndexFsVpnRaAddressPoolTable (pTempVpnRaAddressPoolName)
            == SNMP_SUCCESS)
        {

            nmhGetFsVpnRaAddressPoolStart (pTempVpnRaAddressPoolName,
                                           &OctetStr);
            if (OctetStr.i4_Length == IPVX_IPV4_ADDR_LEN)
            {
                MEMCPY (&u4TempVpnRaStartIp, OctetStr.pu1_OctetList,
                        sizeof (UINT4));
            }
            else
            {
                MEMCPY (&TempVpnRaStartIpv6Addr, OctetStr.pu1_OctetList,
                        IP6_ADDR_SIZE);

            }
            nmhGetFsVpnRaAddressPoolEnd (pTempVpnRaAddressPoolName, &OctetStr);
            if (OctetStr.i4_Length == IPVX_IPV4_ADDR_LEN)
            {
                MEMCPY (&u4TempVpnRaEndIp, OctetStr.pu1_OctetList,
                        sizeof (UINT4));

            }
            else
            {
                MEMCPY (&TempVpnRaEndIpv6Addr, OctetStr.pu1_OctetList,
                        IP6_ADDR_SIZE);
                nmhGetFsVpnRaAddressPoolPrefixLen (pTempVpnRaAddressPoolName,
                                                   &u4TempPrefixLen);
            }

#ifdef CLI_WANTED
            if (VpnDelAddressPool (WEB_CLI_HANDLE,
                                   pTempVpnRaAddressPoolName) == CLI_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to Add/Modify the Address Pool. "
                              "There could be active policies using "
                              "this address pool.");
                break;
            }
#endif
        }

        STRCPY (pHttp->au1Name, "VPN_POOL_NAME");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        issDecodeSpecialChar (pHttp->au1Value);
        i4Len = (INT4) STRLEN (pHttp->au1Value);

        MEMSET (pVpnRaAddressPoolName->pu1_OctetList, 0, (size_t) (i4Len + 1));
        MEMCPY (pVpnRaAddressPoolName->pu1_OctetList, pHttp->au1Value, i4Len);
        pVpnRaAddressPoolName->i4_Length = i4Len;

        STRCPY (pHttp->au1Name, "ADDR_TYPE");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        issDecodeSpecialChar (pHttp->au1Value);
        u4AddrType = (UINT4) ATOI (pHttp->au1Value);

        if (u4AddrType == WEB_VPN_IKE_KEY_IPV4)
        {
            STRCPY (pHttp->au1Name, "RA_START_ADDR");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
#ifdef CLI_WANTED
            u4VpnRaStartIp = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
            STRCPY (pHttp->au1Name, "RA_END_ADDR");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            u4VpnRaEndIp = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));

            STRCPY (pHttp->au1Name, "ACTION");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            if (STRCMP (pHttp->au1Value, "Apply") == 0)
            {
                if (VpnAddAddressPool (WEB_CLI_HANDLE, pVpnRaAddressPoolName,
                                       u4VpnRaStartIp,
                                       u4VpnRaEndIp) == CLI_FAILURE)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Unable to Add the Address Pool!");
                    break;
                }
            }
#endif
        }
        else
        {

            STRCPY (pHttp->au1Name, "STARTV6_ADD");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);
            if ((pip6addr = str_to_ip6addr (pHttp->au1Value)) != NULL)
            {
                MEMCPY (StartIpv6Address.u1_addr, pip6addr, sizeof (tIp6Addr));
            }
            if (INET_ATON6 (pHttp->au1Value, &StartIpv6Address) == 0)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid IPv6 address");
                break;
            }

            STRCPY (pHttp->au1Name, "PREFIX");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);
#ifdef CLI_WANTED
            u4PrefixLen = (UINT4) ATOI (pHttp->au1Value);
#endif
            STRCPY (pHttp->au1Name, "ENDV6_ADD");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);
            if ((pip6addr = str_to_ip6addr (pHttp->au1Value)) != NULL)
            {
                MEMCPY (EndIpv6Address.u1_addr, pip6addr, sizeof (tIp6Addr));
            }
            if (INET_ATON6 (pHttp->au1Value, &EndIpv6Address) == 0)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid IPv6 address");
                break;
            }

            STRCPY (pHttp->au1Name, "ACTION");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
#ifdef CLI_WANTED
            if (STRCMP (pHttp->au1Value, "Apply") == 0)
            {

                if (VpnIpv6AddAddressPool
                    (WEB_CLI_HANDLE, pVpnRaAddressPoolName,
                     StartIpv6Address.u1_addr, EndIpv6Address.u1_addr,
                     u4PrefixLen) == CLI_FAILURE)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Unable to Add the Address Pool!");
                    break;
                }
            }
#endif
        }
        i4RetVal = VPN_SUCCESS;
    }
    while (0);

#ifdef CLI_WANTED

    if (i4RetVal != VPN_SUCCESS && pTempVpnRaAddressPoolName != NULL)
    {
        if (u4AddrType == WEB_VPN_IKE_KEY_IPV4)
        {
            VpnAddAddressPool (WEB_CLI_HANDLE, pTempVpnRaAddressPoolName,
                               u4TempVpnRaStartIp, u4TempVpnRaEndIp);
        }
        else
        {
            if (pVpnRaAddressPoolName != NULL)
            {
                VpnIpv6AddAddressPool (WEB_CLI_HANDLE, pVpnRaAddressPoolName,
                                       TempVpnRaStartIpv6Addr.u1_addr,
                                       TempVpnRaEndIpv6Addr.u1_addr,
                                       u4TempPrefixLen);
            }
        }
    }

    if (pVpnRaAddressPoolName != NULL)
    {
        free_octetstring (pVpnRaAddressPoolName);
    }

    if (pTempVpnRaAddressPoolName != NULL)
    {
        free_octetstring (pTempVpnRaAddressPoolName);
    }

#endif
    VpnDsUnLock ();
    WebnmUnRegisterLock (pHttp);

    if (i4RetVal == VPN_SUCCESS)
    {
        IssProcessVpnRaAddressPoolConfPageGet (pHttp);
    }
    return;

}

/*********************************************************************
*  Function Name : IssProcessVpnRaClientTerminationConfPageSet 
*  Description   : This function processes the Set request coming for the  
*                  Vpn Client Termination Page. This page is used to 
*                  configure the policies to give the access for the 
*                  remote users.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessVpnRaClientTerminationConfPageSet (tHttp * pHttp)
{

    tWebIkePolicy      *pWebVpnPolicy = NULL;
    tVpnNetwork         LocalNet;
    tVpnNetwork         RemoteNet;
    tIp6Addr           *pip6addr = NULL;
    tSNMP_OCTET_STRING_TYPE *pPolicyName = NULL;
    tSNMP_OCTET_STRING_TYPE *pSrcPortRange = NULL;
    tSNMP_OCTET_STRING_TYPE *pDstPortRange = NULL;
    INT1               *piIfName;
    INT4                i4RetVal = VPN_FAILURE;
    INT4                i4Len = 0;
    INT4                i4VpnPolicyRowStatus = 0;
    UINT4               u4VpnStatus = 0;
    UINT4               u4IfIndex = 0;
#ifdef CLI_WANTED
    UINT4               u4Action = 0;
    UINT4               u4PolicyType = 0;
#endif
    UINT4               u4Proto = 0;
    UINT4               u4Mode = 0;
    UINT4               u4SnmpErrorStatus = 0;
    UINT4               u4EncrAlgo = 0;
    UINT4               u4HashAlgo = 0;
    UINT4               u4DHGrp = 0;
    UINT4               u4ExchMode = 0;
    UINT4               u4LifeTime = 0;
    UINT4               u4LifeTimeType = 0;
    UINT4               u4AuthProtocol = 0;
    UINT4               u4PeerType = 0;
    UINT4               u4LocalIdType = 0;
    UINT4               u4IkeVersion = 0;
    UINT1               u1PolicyDelFlag = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1TempStr[WEB_MAX_NAME_LENGTH];
    INT4                i4PolicyType = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4AddrType = 0;
    tSNMP_OCTET_STRING_TYPE VpnRaAddressPoolName;
    tSNMP_OCTET_STRING_TYPE TempIpv6Addr;

    MEMSET (au1IfName, '\0', CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (&LocalNet, '\0', sizeof (tVpnNetwork));
    MEMSET (&RemoteNet, '\0', sizeof (tVpnNetwork));

    WebnmRegisterLock (pHttp, VpnDsLock, VpnDsUnLock);
    VpnDsLock ();

    if (MemAllocateMemBlock
        (VPN_WEB_IKE_POLICY_PID,
         (UINT1 **) (VOID *) &pWebVpnPolicy) != MEM_SUCCESS)
    {
        VpnDsUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    MEMSET (pWebVpnPolicy, '\0', sizeof (tWebIkePolicy));
    MEMSET (au1TempStr, 0, sizeof (au1TempStr));
    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCASECMP (pHttp->au1Value, "DELETE") == 0)
    {
        u1PolicyDelFlag = TRUE;
    }
    do
    {

        /* Setting the polciy name . Reff CLI Command : cryto map <policy name> */

        STRCPY (pHttp->au1Name, "IKE_POLICY");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        issDecodeSpecialChar (pHttp->au1Value);
        MEMSET (pWebVpnPolicy->au1IpSecPolicyName, '\0', IPSEC_POLICY_NAME_LEN);
        STRNCPY (pWebVpnPolicy->au1IpSecPolicyName, pHttp->au1Value,
                 (IPSEC_POLICY_NAME_LEN - 1));

        /* This Check is performed so that the policy can 
         * be selected from Existing Policies. */
        if (pWebVpnPolicy->au1IpSecPolicyName[0] == '\0')
        {
            STRCPY (pHttp->au1Name, "CURR_POLICY");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);
            MEMSET (pWebVpnPolicy->au1IpSecPolicyName, '\0',
                    IPSEC_POLICY_NAME_LEN);
            STRNCPY (pWebVpnPolicy->au1IpSecPolicyName, pHttp->au1Value,
                     (IPSEC_POLICY_NAME_LEN - 1));
        }

        /*Copying the Policy Name into Temporary Buffer.
         * This Buffer is passed to the get routine so that the 
         * the policy can be displayed after configuring. */
        STRNCPY (au1TempStr, pWebVpnPolicy->au1IpSecPolicyName,
                 (WEB_MAX_NAME_LENGTH - 1));

        i4Len = (INT4) STRLEN (pWebVpnPolicy->au1IpSecPolicyName);

        if ((pPolicyName = allocmem_octetstring (i4Len + 1)) == NULL)
        {
            break;
        }

        MEMSET (pPolicyName->pu1_OctetList, 0, (size_t) (i4Len + 1));
        MEMCPY (pPolicyName->pu1_OctetList, pWebVpnPolicy->au1IpSecPolicyName,
                i4Len);

        pPolicyName->i4_Length = i4Len;
        if (u1PolicyDelFlag == 1)
        {
            /* Seting the PolicyDelFlag to 1 if Deleting the Policy.  */
            break;
        }
        STRCPY (pHttp->au1Name, "vpnStatus");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        issDecodeSpecialChar (pHttp->au1Value);
        u4VpnStatus = (UINT4) ATOI (pHttp->au1Value);

        /*If Policy is ACITVE , for modifying the parameters we set the Row status to NOT IN SERVICE 
         * and modify the policy parameters */
        nmhGetFsVpnPolicyRowStatus (pPolicyName, &i4VpnPolicyRowStatus);
        if (nmhGetFsVpnPolicyType (pPolicyName, &i4PolicyType) == SNMP_SUCCESS)
        {
            if (((i4PolicyType == WEB_VPN_IPSEC_MANUAL)
                 || (i4PolicyType == WEB_VPN_PRESHARED))
                && (i4VpnPolicyRowStatus == ACTIVE))
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "Cannot Modify an Active Policy!");
                break;
            }
        }

        if (i4VpnPolicyRowStatus != ACTIVE)
        {

#ifdef CLI_WANTED
            if (VpnCreatePolicy (WEB_CLI_HANDLE, pPolicyName) == CLI_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Policy Creation Failed!");
                break;
            }
#endif
            /* Setting the cryto mode to XAUTH . Reff CLI Command : cryto mode <ipsec-manual> */
            STRCPY (pHttp->au1Name, "POLICY_TYPE");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);
#ifdef CLI_WANTED
            u4PolicyType = (UINT4) ATOI (pHttp->au1Value);

            if (VpnSetPolicyType (WEB_CLI_HANDLE, pPolicyName,
                                  u4PolicyType) == CLI_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Failed to set Crypto policy to IKE pre-shared Extended Authentication!");
                break;
            }
#endif
            /* setting the IKE version . Reff CLI Command : set ike version {v1 | v2} */
            STRCPY (pHttp->au1Name, "IKE_VERSION");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            SSCANF ((CONST CHR1 *) pHttp->au1Value, "%x", &u4IkeVersion);

#ifdef CLI_WANTED
            if (VpnSetIkeVersion (WEB_CLI_HANDLE, pPolicyName,
                                  u4IkeVersion) == CLI_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "Failed to set IKE Version!");
                break;
            }
#endif
            /* Setting IKE Phase 1 Peer Type.
             * Reff CLI COMMAND : isakmp peer identity <peer-type> <peer-value> */

            /* Set Peer Id type and Peer Id Value for xauth */

            STRCPY (pHttp->au1Name, "PEER_ID_TYPE");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            SSCANF ((CONST CHR1 *) pHttp->au1Value, "%u", &u4PeerType);

            STRCPY (pHttp->au1Name, "PEER_ID_VALUE");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);

#ifdef CLI_WANTED
            if (VpnSetPeerType (WEB_CLI_HANDLE, pPolicyName, u4PeerType,
                                pHttp->au1Value) == CLI_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Failed to set Ike Phase1 Peertype!");
                break;
            }
#endif
            /* Setting IKE Phase 1 Local Identity Type.
             * Reff CLI COMMAND : set local identity <local-type> <local-value> */

            /* Set Peer Id type and Peer Id Value for xauth */

            STRCPY (pHttp->au1Name, "LOCAL_ID_TYPE");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            SSCANF ((CONST CHR1 *) pHttp->au1Value, "%u", &u4LocalIdType);

            STRCPY (pHttp->au1Name, "LOCAL_ID_VALUE");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);

#ifdef CLI_WANTED
            if (VpnSetLocalIdType (WEB_CLI_HANDLE, pPolicyName, u4LocalIdType,
                                   pHttp->au1Value) == CLI_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Failed to set Ike Phase1 Local Id Type!");
                break;
            }
#endif
          /*************************************************************************************   
           * Setting the Ike Phase 1 Proposal With the following Parameters  
           * IpSec Encryption - DES/3DES/AES128/AES192/AES256
           * IpSec Authentication - hmac-mds/hmac-sha1
           * DH Group - Group 1/ Group 2/ Group 5                     
           * Exchange - main / aggressive 
           * LifeTime - Seconds / Minutes / Hours 
           * LifeTime Value                  
           * 
           Reff CLI  COMMAND : 
           isakmp policy encryption {des | triple-des | aes | aes-192 | aes-256}  hash {sha1|md5} 
                                     dh {group1|group2|group5}  exch {main|aggressive}
                                     lifetime {secs|mins|hrs} <lifetime-value>

          ****************************************************************************************/

            STRCPY (pHttp->au1Name, "P1_IPSEC_ENCR");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            MEMSET (pWebVpnPolicy->au1Phase1Encryption, '\0',
                    IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1Phase1Encryption, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

            STRCPY (pHttp->au1Name, "P1_IPSEC_AUTH");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            MEMSET (pWebVpnPolicy->au1Phase1Authentication, '\0',
                    IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1Phase1Authentication, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

            STRCPY (pHttp->au1Name, "DH_GROUP");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            MEMSET (pWebVpnPolicy->au1Phase1DHGroup, '\0', IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1Phase1DHGroup, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

            STRCPY (pHttp->au1Name, "EXCHANGE_MODE");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            MEMSET (pWebVpnPolicy->au1Phase1ExchangeType, '\0',
                    IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1Phase1ExchangeType, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

            STRCPY (pHttp->au1Name, "P1_LIFE_TIME");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            MEMSET (pWebVpnPolicy->au1Phase1LifeTimeType, '\0',
                    IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1Phase1LifeTimeType, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

            STRCPY (pHttp->au1Name, "P1_TIME_VALUE");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            MEMSET (pWebVpnPolicy->au1Phase1LifeTimeValue, '\0',
                    IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1Phase1LifeTimeValue, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

            SSCANF ((CONST CHR1 *)
                    pWebVpnPolicy->au1Phase1Encryption, "%u", &u4EncrAlgo);
            SSCANF ((CONST CHR1 *)
                    pWebVpnPolicy->au1Phase1Authentication, "%u", &u4HashAlgo);
            SSCANF ((CONST CHR1 *)
                    pWebVpnPolicy->au1Phase1DHGroup, "%u", &u4DHGrp);
            SSCANF ((CONST CHR1 *)
                    pWebVpnPolicy->au1Phase1ExchangeType, "%u", &u4ExchMode);
            SSCANF ((CONST CHR1 *)
                    pWebVpnPolicy->au1Phase1LifeTimeType, "%u",
                    &u4LifeTimeType);
            SSCANF ((CONST CHR1 *)
                    pWebVpnPolicy->au1Phase1LifeTimeValue, "%u", &u4LifeTime);

            /* Set Ike Mode as Aggressive for xauth */

#ifdef CLI_WANTED
            if (VpnCliSetVpnPolicyIkeProposal (WEB_CLI_HANDLE,
                                               pPolicyName, u4EncrAlgo,
                                               u4HashAlgo, u4DHGrp,
                                               u4ExchMode,
                                               u4LifeTimeType,
                                               u4LifeTime) == CLI_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Failed to set Ike Phase1 Proposal!");
                break;
            }

#endif
          /*************************************************************************************   
           * Setting the Ike Phase 2 Proposal With the following Parameters  
           * IpSec Protocol - AH/ESP                                  
           * IpSec Encryption - None/DES/3DES/AES128/AES192/AES256
           * IpSec Authentication - hmac-mds/hmac-sha1
           * IpSec Mode - Transport/Tunnel 
           * PFS/DH Group - Group 1/ Group 2/ Group 5                     
           * LifeTime - Seconds / Minutes / Hours 
           * LifeTime Value                  
           * 
           Reff CLI  COMMAND : 
                                crypto map ipsec {esp encryption {null | des | triple-des | aes |
                                aes-192 | aes-256 }} {esp|ah} {md5|sha1}  mode {tunnel|transport}
                                [pfs {group1| group2| group5} ] [lifetime {secs|mins|hrs} 
                                <lifetime-value>]

          ****************************************************************************************/

            STRCPY (pHttp->au1Name, "P2_IPSEC_PROT");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            MEMSET (pWebVpnPolicy->au1Phase2Protocol, '\0', IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1Phase2Protocol, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

            STRCPY (pHttp->au1Name, "P2_IPSEC_ENCR");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            MEMSET (pWebVpnPolicy->au1Phase2Encryption, '\0',
                    IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1Phase2Encryption, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));
            if (pWebVpnPolicy->au1Phase2Encryption[0] == '\0')
            {
                MEMSET (pWebVpnPolicy->au1Phase2Encryption, '\0',
                        IPSEC_WEB_ARG_LEN);
                STRNCPY (pWebVpnPolicy->au1Phase2Encryption, "0",
                         (IPSEC_WEB_ARG_LEN - 1));
            }

            STRCPY (pHttp->au1Name, "P2_IPSEC_AUTH");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            MEMSET (pWebVpnPolicy->au1Phase2Authentication, '\0',
                    IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1Phase2Authentication, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

            STRCPY (pHttp->au1Name, "P2_IPSEC_MODE");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            MEMSET (pWebVpnPolicy->au1Phase2Mode, '\0', IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1Phase2Mode, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

            STRCPY (pHttp->au1Name, "P2_PFS");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            MEMSET (pWebVpnPolicy->au1Phase2DHGroup, '\0', IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1Phase2DHGroup, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

            STRCPY (pHttp->au1Name, "P2_LIFE_TIME");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            MEMSET (pWebVpnPolicy->au1Phase2LifeTimeType, '\0',
                    IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1Phase2LifeTimeType, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

            STRCPY (pHttp->au1Name, "P2_TIME_VALUE");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            MEMSET (pWebVpnPolicy->au1Phase2LifeTimeValue, '\0',
                    IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1Phase2LifeTimeValue, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));
            if (pWebVpnPolicy->au1Phase2LifeTimeValue[0] == '\0')
            {
                STRCPY (pWebVpnPolicy->au1Phase2LifeTimeValue, "0");
            }

            SSCANF ((CONST CHR1 *)
                    pWebVpnPolicy->au1Phase2Encryption, "%u", &u4EncrAlgo);
            SSCANF ((CONST CHR1 *)
                    pWebVpnPolicy->au1Phase2Protocol, "%u", &u4AuthProtocol);
            SSCANF ((CONST CHR1 *)
                    pWebVpnPolicy->au1Phase2Authentication, "%u", &u4HashAlgo);
            SSCANF ((CONST CHR1 *) pWebVpnPolicy->au1Phase2Mode, "%u", &u4Mode);
            SSCANF ((CONST CHR1 *)
                    pWebVpnPolicy->au1Phase2DHGroup, "%u", &u4DHGrp);
            SSCANF ((CONST CHR1 *)
                    pWebVpnPolicy->au1Phase2LifeTimeType, "%u",
                    &u4LifeTimeType);
            SSCANF ((CONST CHR1 *)
                    pWebVpnPolicy->au1Phase2LifeTimeValue, "%u", &u4LifeTime);

            if ((u4EncrAlgo != 0) && (u4HashAlgo == 0))
            {
                /*If only ESP is selected setting Authentication Protocol to 0 */
                u4AuthProtocol = 0;
            }

            /* Set Ike Phase 2 Mode as Tunnel for xauth */
            u4Mode = VPN_TUNNEL;

#ifdef CLI_WANTED
            /* Setting the ipsec mode Reff CLI COMMAND : crypto ipsec mode {tunnel | transport}  */
            if (VpnSetPolicyMode (WEB_CLI_HANDLE, pPolicyName, u4Mode) ==
                CLI_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "Failed to set Ipsec Mode!");
                break;
            }
            if (VpnCliSetVpnPolicyIpsecProposal (WEB_CLI_HANDLE, pPolicyName,
                                                 u4EncrAlgo, u4AuthProtocol,
                                                 u4HashAlgo, u4DHGrp,
                                                 u4LifeTimeType,
                                                 u4LifeTime) == CLI_FAILURE)

            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Failed to set Ike Phase2 Proposal!");
                break;
            }
#endif
            STRCPY (pHttp->au1Name, "ADDR_TYPE");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);
            u4AddrType = (UINT4) ATOI (pHttp->au1Value);

            if (u4AddrType == WEB_VPN_IKE_KEY_IPV4)
            {
                /* Configuration of Traffic Selector  Reff CLI Command
                 * access-list {permit|deny|apply} {any|tcp|udp|icmpv4|ahproto|espproto}
                 source <num_str> <num_str> destination <num_str> <num_str> */

                /* Set the Access Parameters from Address Pool for xauth 
                 * Support for user-configured access list to be added in web
                 * page. It is availabl ein CLI/SNMP */

                STRCPY (pHttp->au1Name, "LCL_IP");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                issDecodeSpecialChar (pHttp->au1Value);
                MEMSET (pWebVpnPolicy->au1IpsecLocalIpAddr, '\0',
                        IPSEC_IPADDR_LEN);
                STRNCPY (pWebVpnPolicy->au1IpsecLocalIpAddr, pHttp->au1Value,
                         (IPSEC_IPADDR_LEN - 1));

                STRCPY (pHttp->au1Name, "LCL_MASK");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                issDecodeSpecialChar (pHttp->au1Value);
                MEMSET (pWebVpnPolicy->au1IpsecLocalIpMask, '\0',
                        IPSEC_IPADDR_LEN);
                STRNCPY (pWebVpnPolicy->au1IpsecLocalIpMask, pHttp->au1Value,
                         (IPSEC_IPADDR_LEN - 1));

                LocalNet.IpAddr.uIpAddr.Ip4Addr =
                    OSIX_NTOHL (INET_ADDR (pWebVpnPolicy->au1IpsecLocalIpAddr));
                IPV4_MASK_TO_MASKLEN (LocalNet.u4AddrPrefixLen,
                                      OSIX_NTOHL (INET_ADDR
                                                  (pWebVpnPolicy->
                                                   au1IpsecLocalIpMask)));

                if (VpnGetRaPoolInfo (&(RemoteNet.IpAddr.uIpAddr.Ip4Addr),
                                      &u4IpAddr) == VPN_FAILURE)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Address Pool is not configured for remote users!");
                    break;
                }
                IPV4_MASK_TO_MASKLEN (RemoteNet.u4AddrPrefixLen, u4IpAddr);
            }
            else
            {
                /* Configuration of Traffic Selector  Reff CLI Command
                   access-list ipv6 {permit|deny|apply} {any|tcp|udp|icmpv6|ahproto|espproto}
                   source <ucast_addr> <prefixlen> destination <ucast_addr> <prefixlen>
                   [srcport <random_str>] [destport <random_str>] */

                STRCPY (pHttp->au1Name, "LCLV6_ADD");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                issDecodeSpecialChar (pHttp->au1Value);
                if ((pip6addr = str_to_ip6addr (pHttp->au1Value)) != NULL)
                {
                    MEMCPY (LocalNet.IpAddr.uIpAddr.Ip6Addr.u1_addr,
                            pip6addr, sizeof (tIp6Addr));
                }
                STRCPY (pHttp->au1Name, "LCL_PREFIX");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                issDecodeSpecialChar (pHttp->au1Value);
                LocalNet.u4AddrPrefixLen = (UINT4) ATOI (pHttp->au1Value);

                /* Since only 1 address pool is allowed , we are getting the first entry
                 * to find whether address pool is configured or not. if it fails we send 0
                 * to the front end .In front end this value is stored in a variable and
                 * prompts an alert message of the dependency of configuring the address pool.*/
                if (nmhGetFirstIndexFsVpnRaAddressPoolTable
                    (&VpnRaAddressPoolName) == SNMP_SUCCESS)
                {
                    if (nmhGetFirstIndexFsVpnRaAddressPoolTable
                        (&VpnRaAddressPoolName) == SNMP_SUCCESS)
                    {
                        MEMSET (au1TempStr, 0, IP6_ADDR_SIZE);
                        TempIpv6Addr.i4_Length = IP6_ADDR_SIZE;
                        TempIpv6Addr.pu1_OctetList = au1TempStr;
                        nmhGetFsVpnRaAddressPoolStart (&VpnRaAddressPoolName,
                                                       &TempIpv6Addr);
                        MEMCPY (RemoteNet.IpAddr.uIpAddr.Ip6Addr.u1_addr,
                                TempIpv6Addr.pu1_OctetList, IP6_ADDR_SIZE);
                        nmhGetFsVpnRaAddressPoolPrefixLen
                            (&VpnRaAddressPoolName, &RemoteNet.u4AddrPrefixLen);
                    }
                }
                else
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Address Pool is not configured for remote users!");
                    break;

                }
            }
            STRCPY (pHttp->au1Name, "LCL_PORT");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);
            MEMSET (pWebVpnPolicy->au1IpSecLocalPortRange, '\0',
                    WEB_PORT_RANGE_LENGTH);
            STRNCPY (pWebVpnPolicy->au1IpSecLocalPortRange, pHttp->au1Value,
                     (WEB_PORT_RANGE_LENGTH - 1));
            i4Len = (INT4) STRLEN (pWebVpnPolicy->au1IpSecLocalPortRange);

            if ((pSrcPortRange = allocmem_octetstring (i4Len + 1)) == NULL)
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "Failed to Set Traffic Selector!");
                break;
            }

            MEMSET (pSrcPortRange->pu1_OctetList, 0, (size_t) (i4Len + 1));
            MEMCPY (pSrcPortRange->pu1_OctetList,
                    pWebVpnPolicy->au1IpSecLocalPortRange, i4Len);
            pSrcPortRange->i4_Length = i4Len;

            STRCPY (pHttp->au1Name, "REM_PORT");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);
            MEMSET (pWebVpnPolicy->au1IpSecRemotePortRange, '\0',
                    WEB_PORT_RANGE_LENGTH);
            STRNCPY (pWebVpnPolicy->au1IpSecRemotePortRange, pHttp->au1Value,
                     (WEB_PORT_RANGE_LENGTH - 1));

            i4Len = (INT4) STRLEN (pWebVpnPolicy->au1IpSecRemotePortRange);

            if ((pDstPortRange = allocmem_octetstring (i4Len + 1)) == NULL)
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "Failed to Set Traffic Selector!");
                break;
            }

            MEMSET (pDstPortRange->pu1_OctetList, 0, (size_t) (i4Len + 1));
            MEMCPY (pDstPortRange->pu1_OctetList,
                    pWebVpnPolicy->au1IpSecRemotePortRange, i4Len);
            pDstPortRange->i4_Length = i4Len;

            STRCPY (pHttp->au1Name, "IPSEC_PROTO");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);
            MEMSET (pWebVpnPolicy->au1IpSecTransportProtocol, '\0',
                    IPSEC_WEB_ARG_LEN);
            STRNCPY (pWebVpnPolicy->au1IpSecTransportProtocol, pHttp->au1Value,
                     (IPSEC_WEB_ARG_LEN - 1));

#ifdef CLI_WANTED
            u4Action = WEB_SEC_APPLY;
#endif
            SSCANF ((CONST CHR1 *)
                    pWebVpnPolicy->au1IpSecTransportProtocol, "%u", &u4Proto);

#ifdef CLI_WANTED
            if (u4AddrType == WEB_VPN_IKE_KEY_IPV4)
            {
                if (VpnSetAccessParams
                    (pPolicyName, &LocalNet, &RemoteNet,
                     u4Action, u4Proto, pSrcPortRange,
                     pDstPortRange) == CLI_FAILURE)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Failed to set Traffic Selector for allowing remote users!");
                    break;
                }
            }
            else
            {
                if (VpnSetIpv6AccessParams
                    (pPolicyName, &LocalNet, &RemoteNet,
                     u4Action, u4Proto, pSrcPortRange,
                     pDstPortRange) == CLI_FAILURE)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Failed to Set IPv6 Ipsec Traffic Selector!");
                    break;
                }
            }
            /* Applying the policy to the interface. Reff CLI COMMAND : crypto map apply <policy name> */

#endif
            STRCPY (pHttp->au1Name, "IPSEC_INTF");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            issDecodeSpecialChar (pHttp->au1Value);

            MEMSET (au1IfName, 0, sizeof (au1IfName));
            piIfName = (INT1 *) &au1IfName[0];
            /* We get interface as either in numeric or alias format like "1" or "Fa0/1"
             * so check the format first and fetch the interface number 
             */
            if (STRSTR (pHttp->au1Value, "/") != NULL)
            {
                for (u4IfIndex = 1; u4IfIndex <= SYS_MAX_INTERFACES;
                     u4IfIndex++)
                {

                    CfaCliGetIfName (u4IfIndex, piIfName);
                    if (STRCMP (piIfName, pHttp->au1Value) == 0)
                    {
                        break;
                    }

                }
            }
            else
            {
                u4IfIndex = (UINT4) ATOI (pHttp->au1Value);
            }

            if (nmhTestv2FsVpnPolicyIntfIndex
                (&u4SnmpErrorStatus, pPolicyName,
                 (INT4) u4IfIndex) == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "VPN policies can be applied only on "
                              "untrusted/Public L3 interfaces (PPP, Ethernet etc)"
                              " or Invalid ip address is assigned to the"
                              " Interface");
                break;
            }
            nmhSetFsVpnPolicyIntfIndex (pPolicyName, (INT4) u4IfIndex);
        }

        if (u4VpnStatus == VPN_POLICY_ENABLE)
        {

            if (nmhTestv2FsVpnPolicyRowStatus (&u4SnmpErrorStatus, pPolicyName,
                                               ACTIVE) == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to Set Policy to Interface");
                break;
            }

            if (nmhSetFsVpnPolicyRowStatus (pPolicyName, ACTIVE) ==
                SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to Set Policy to Interface");
                break;
            }
        }
        else
        {
            if (nmhTestv2FsVpnPolicyRowStatus (&u4SnmpErrorStatus, pPolicyName,
                                               NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "Unable to Modify the Policy");
                break;
            }

            if (nmhSetFsVpnPolicyRowStatus (pPolicyName, NOT_IN_SERVICE) ==
                SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "Unable to Modify the Policy");
                break;
            }
        }

        i4RetVal = VPN_SUCCESS;

    }
    while (0);

#ifdef CLI_WANTED
    if (pPolicyName != NULL)
    {
        /* Deleting the policy */
        if (u1PolicyDelFlag == 1)
        {
            if (VpnDeletePolicy (WEB_CLI_HANDLE, pPolicyName) == CLI_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "Unable to Delete the Policy");
            }
            else
            {
                /*
                 * If we are deleting a policy ,
                 * we are setting Delete Flag to 1
                 */
                i4RetVal = VPN_SUCCESS;
            }
        }
        free_octetstring (pPolicyName);
    }

#endif
    MemReleaseMemBlock (VPN_WEB_IKE_POLICY_PID, (UINT1 *) pWebVpnPolicy);
    VpnDsUnLock ();
    WebnmUnRegisterLock (pHttp);

    if (i4RetVal == VPN_SUCCESS)
    {
        IssProcessVpnRaClientTerminationConfPageGet (pHttp, au1TempStr,
                                                     u1PolicyDelFlag);
    }
    if (pSrcPortRange != NULL)
    {
        free_octetstring (pSrcPortRange);
    }
    if (pDstPortRange != NULL)
    {
        free_octetstring (pDstPortRange);
    }
    return;
}

/*********************************************************************
*  Function Name : IssProcessVpnRaClientTerminationConfPageGet 
*  Description   : This function processes the Get request coming for the  
*                  Vpn Remote Client Termination Page .   
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID 
     
     
     
     
     
     
     
    IssProcessVpnRaClientTerminationConfPageGet
    (tHttp * pHttp, UINT1 *pu1PolicyName, UINT1 u1DelFlag)
{

    tSNMP_OCTET_STRING_TYPE OctetStr;
    tSNMP_OCTET_STRING_TYPE *pPolicyName = NULL;
    tSNMP_OCTET_STRING_TYPE *pPortRange = NULL;
    tVpnPolicy         *pVpnPolicy = NULL;
    INT4                i4Len = 0;
    UINT4               u4RetVal = 0;
    UINT4               u4BitMap;
    INT4                i4RetVal = 0;
    INT4                i4RetVal1 = 0;
    UINT1               au1TempStr[WEB_MAX_NAME_LENGTH];
    UINT1               au1String[20];
    UINT4               u4PrefixLen = 0;
    INT4                i4AddrType = 0;
    tIp6Addr            Ipv6Address;

    pHttp->i4Write = 0;

    WebnmRegisterLock (pHttp, VpnDsLock, VpnDsUnLock);
    VpnDsLock ();

    MEMSET (au1TempStr, 0, sizeof (au1TempStr));
    MEMSET (au1String, 0, sizeof (au1String));

    if (u1DelFlag == 1)
    {
        /*This is called for Get Operation when a policy is deleted. *
         * pHttp->au1Value is set to empty string ,since any value 
         * present in it may be taken for calculation which is not 
         * desired */
        VpnPrintPolicies (pHttp, NULL, au1TempStr, WEB_VPN_XAUTH);
        pHttp->au1Value[0] = '\0';
    }
    else
    {
        STRCPY (pHttp->au1Name, "CURR_POLICY");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_FAILURE)
        {
            if (pu1PolicyName == NULL)
            {
                /*When pu1PolicyName is NULL , it is called for Normal Get 
                 * Operation from issweb.c.*/
                VpnPrintPolicies (pHttp, NULL, au1TempStr, WEB_VPN_XAUTH);
            }
            else
            {
                /*When pu1PolicyName is not NULL , it is called for Get Operation
                 * when a new policy is created and called from the set routinue.*/
                VpnPrintPolicies (pHttp, pu1PolicyName, NULL, WEB_VPN_XAUTH);

                /*Copying the PolicyName into temporary buffer so as to display the
                 * policy configuration.*/
                if (STRLEN (pu1PolicyName) >= WEB_MAX_NAME_LENGTH)
                {
                    VpnDsUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    return;
                }
                STRCPY (au1TempStr, pu1PolicyName);
            }
        }
        else
        {
            /*This is called for Get Operation for displaying the policy configuration. */
            issDecodeSpecialChar (pHttp->au1Value);
            VpnPrintPolicies (pHttp, pHttp->au1Value, NULL, WEB_VPN_XAUTH);
        }
    }

    VpnDsUnLock ();
    WebnmUnRegisterLock (pHttp);
    u4BitMap = VPN_WEB_ETH_TYPE | VPN_WEB_PPP_TYPE | VPN_WEB_MP_TYPE;
    SecPrintIpInterfaces (pHttp, u4BitMap, VPN_WEB_NETWORK_TYPE_WAN,
                          VPN_WEB_WAN_TYPE_PUBLIC);
    WebnmRegisterLock (pHttp, VpnDsLock, VpnDsUnLock);
    VpnDsLock ();

    /*If a new policy is created or displaying the existing policy , then it displays the 
     * configuration for that policy.*/

    if (au1TempStr[0] != '\0')
    {
        i4Len = (INT4) STRLEN (au1TempStr);

        if ((pPolicyName = allocmem_octetstring (i4Len + 1)) == NULL)
        {
            VpnDsUnLock ();
            WebnmUnRegisterLock (pHttp);
            return;
        }
        MEMSET (pPolicyName->pu1_OctetList, 0, (size_t) (i4Len + 1));
        MEMCPY (pPolicyName->pu1_OctetList, au1TempStr, i4Len);
        pPolicyName->i4_Length = i4Len;
        pPolicyName->pu1_OctetList[i4Len] = '\0';
        pVpnPolicy =
            VpnSnmpGetPolicy (pPolicyName->pu1_OctetList,
                              pPolicyName->i4_Length);
        if (pVpnPolicy == NULL)
        {
            free_octetstring (pPolicyName);
            VpnDsUnLock ();
            WebnmUnRegisterLock (pHttp);
            return;
        }

        /*Displaying Interface for which policy is applied  */

        nmhGetFsVpnPolicyIntfIndex (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "IPINTERFACES_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /* Displaying VPN Status */
        nmhGetFsVpnPolicyRowStatus (pPolicyName, &i4RetVal);
        if (i4RetVal == ACTIVE)
        {
            i4RetVal = VPN_POLICY_ENABLE;
            STRCPY (pHttp->au1KeyString, "VPN_STATUS_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

        }
        else
        {
            i4RetVal = VPN_POLICY_DISABLE;
            STRCPY (pHttp->au1KeyString, "VPN_STATUS_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

        }

        nmhGetFsVpnPolicyType (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "POLICY_TYPE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        nmhGetFsVpnIkeVersion (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "IKE_VERSION_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "0x%x", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying the Phase1 Encryption Algo */

        nmhGetFsVpnIkePhase1EncryptionAlgo (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "P1_IKE_ENCR_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying the Phase1 Authentication Key */

        nmhGetFsVpnIkePhase1HashAlgo (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "P1_IKE_AUTH_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying Phase1 DH Algo */

        nmhGetFsVpnIkePhase1DHGroup (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "P1_DHGRP_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying Phase1 Exchange Type */

        nmhGetFsVpnIkePhase1Mode (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "P1_EXCH_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying Phase1 Lifetime Type */

        nmhGetFsVpnIkePhase1LifeTimeType (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "P1_LIFE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying Phase1 Life Time Value */

        nmhGetFsVpnIkePhase1LifeTime (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "P1_TIME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying Peer Identity Type */

        nmhGetFsVpnIkePhase1PeerIdType (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "PEER_TYPE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying Peer Identity Value */

        MEMSET (au1TempStr, 0, sizeof (au1TempStr));
        OctetStr.pu1_OctetList = au1TempStr;
        VpnPrintAvailableRemIdValues (pHttp);

        nmhGetFsVpnIkePhase1PeerIdValue (pPolicyName, &OctetStr);
        STRCPY (pHttp->au1KeyString, "PEER_VAL_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", OctetStr.pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying Local Identity Type */

        nmhGetFsVpnIkePhase1LocalIdType (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "LOC_TYP_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying Local Identity Value */

        MEMSET (au1TempStr, 0, sizeof (au1TempStr));
        OctetStr.pu1_OctetList = au1TempStr;

        nmhGetFsVpnIkePhase1LocalIdValue (pPolicyName, &OctetStr);
        STRCPY (pHttp->au1KeyString, "LOC_VAL_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", OctetStr.pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        nmhGetFsVpnProtectNetworkType (pPolicyName, &i4AddrType);
        if (i4AddrType == WEB_VPN_IKE_KEY_IPV4)
        {
            /* Displaying the Local Protected Network IP Address */

            MEMSET (au1TempStr, 0, sizeof (UINT4));
            OctetStr.pu1_OctetList = au1TempStr;
            nmhGetFsVpnLocalProtectNetwork (pPolicyName, &OctetStr);
            MEMCPY (&u4RetVal, OctetStr.pu1_OctetList, sizeof (UINT4));
            STRCPY (pHttp->au1KeyString, "LCL_IP_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMSET (au1String, 0, sizeof (au1String));
            VPN_INET_NTOA (au1String, u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            /* Displaying Local Protected Network Mask */
            nmhGetFsVpnLocalProtectNetworkPrefixLen (pPolicyName, &u4PrefixLen);
            IPV4_MASKLEN_TO_MASK (u4RetVal, u4PrefixLen);
            STRCPY (pHttp->au1KeyString, "LCL_MASK_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMSET (au1String, 0, sizeof (au1String));
            VPN_INET_NTOA (au1String, u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            /* Displaying Remote Protected Network IP Address */
            OctetStr.i4_Length = sizeof (UINT4);
            OctetStr.pu1_OctetList = au1TempStr;
            nmhGetFsVpnRemoteProtectNetwork (pPolicyName, &OctetStr);
            MEMCPY (&u4RetVal, OctetStr.pu1_OctetList, sizeof (UINT4));
            STRCPY (pHttp->au1KeyString, "REM_ADD_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMSET (au1String, 0, sizeof (au1String));
            VPN_INET_NTOA (au1String, u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            /* Displaying Remote Protected Network Mask */
            nmhGetFsVpnRemoteProtectNetworkPrefixLen (pPolicyName,
                                                      &u4PrefixLen);
            IPV4_MASKLEN_TO_MASK (u4RetVal, u4PrefixLen);
            STRCPY (pHttp->au1KeyString, "REM_MASK_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMSET (au1String, 0, sizeof (au1String));
            VPN_INET_NTOA (au1String, u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
        }
        else
        {
            MEMSET (au1TempStr, 0, IP6_ADDR_SIZE);
            OctetStr.pu1_OctetList = au1TempStr;
            OctetStr.i4_Length = IP6_ADDR_SIZE;
            nmhGetFsVpnLocalProtectNetwork (pPolicyName, &OctetStr);
            STRCPY (pHttp->au1KeyString, "LCLV6_ADD_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMCPY (&Ipv6Address, OctetStr.pu1_OctetList, IP6_ADDR_SIZE);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     Ip6PrintAddr (&Ipv6Address));
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            nmhGetFsVpnLocalProtectNetworkPrefixLen (pPolicyName, &u4PrefixLen);
            STRCPY (pHttp->au1KeyString, "LCL_PREFIX_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4PrefixLen);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            OctetStr.i4_Length = IP6_ADDR_SIZE;
            nmhGetFsVpnRemoteProtectNetwork (pPolicyName, &OctetStr);
            STRCPY (pHttp->au1KeyString, "REMV6_ADD_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMCPY (&Ipv6Address, OctetStr.pu1_OctetList, IP6_ADDR_SIZE);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     Ip6PrintAddr (&Ipv6Address));
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            nmhGetFsVpnRemoteProtectNetworkPrefixLen (pPolicyName,
                                                      &u4PrefixLen);
            STRCPY (pHttp->au1KeyString, "REM_PREFIX_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4PrefixLen);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

        }
        if ((pPortRange = allocmem_octetstring (WEB_PORT_RANGE_LENGTH)) == NULL)
        {
            free_octetstring (pPolicyName);
            VpnDsUnLock ();
            WebnmUnRegisterLock (pHttp);
            return;
        }

        /* Displaying Local Port Range */
        MEMSET (pPortRange->pu1_OctetList, 0, WEB_PORT_RANGE_LENGTH);

        nmhGetFsVpnIkeSrcPortRange (pPolicyName, pPortRange);
        STRCPY (pHttp->au1KeyString, "LCL_PORT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        MEMSET (pHttp->au1DataString, 0, (size_t) ENM_MAX_NAME_LEN);
        MEMCPY (pHttp->au1DataString, pPortRange->pu1_OctetList,
                pPortRange->i4_Length);
        pHttp->au1DataString[STRLEN (pPortRange->pu1_OctetList)] = '\0';
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /* Displaying Remote Port Range */
        MEMSET (pPortRange->pu1_OctetList, 0, WEB_PORT_RANGE_LENGTH);

        nmhGetFsVpnIkeDstPortRange (pPolicyName, pPortRange);
        STRCPY (pHttp->au1KeyString, "REM_PORT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        MEMSET (pHttp->au1DataString, 0, (size_t) ENM_MAX_NAME_LEN);
        MEMCPY (pHttp->au1DataString, pPortRange->pu1_OctetList,
                pPortRange->i4_Length);
        pHttp->au1DataString[STRLEN (pPortRange->pu1_OctetList)] = '\0';
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /* Displaying IPSEC Protocol */
        nmhGetFsVpnProtocol (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "IPSEC_PROTO_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying the Phase2 Protocol */
        nmhGetFsVpnSecurityProtocol (pPolicyName, &i4RetVal);
        nmhGetFsVpnIkePhase2EspEncryptionAlgo (pPolicyName, &i4RetVal1);
        STRCPY (pHttp->au1KeyString, "P2_PROT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if ((i4RetVal1 != 0) && (i4RetVal == 0))
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", WEB_ESP_VALUE);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying the Phase2 Encryption Algo */

        nmhGetFsVpnIkePhase2EspEncryptionAlgo (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "P2_IKE_ENCR_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying the Phase2 Authentication Key */

        nmhGetFsVpnIkePhase2AuthAlgo (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "P2_IKE_AUTH_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying Phase2 Preferred Forwared Secrecy */

        nmhGetFsVpnIkePhase2DHGroup (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "P2_PFS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying Phase2 Lifetime Type */

        nmhGetFsVpnIkePhase2LifeTimeType (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "P2_LIFE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        /*Displaying Phase2 Life Time Value */

        nmhGetFsVpnIkePhase2LifeTime (pPolicyName, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "P2_TIME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        free_octetstring (pPolicyName);
        free_octetstring (pPortRange);

    }
    else
    {
        /*If no policy is present for displaying , blank space is displayed
         * by default.*/
        i4RetVal = VPN_POLICY_DISABLE;
        STRCPY (pHttp->au1KeyString, "VPN_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        STRCPY (pHttp->au1KeyString, "P1_TIME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        pHttp->au1DataString[0] = '\0';
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        VpnPrintAvailableRemIdValues (pHttp);

        STRCPY (pHttp->au1KeyString, "LOC_VAL_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        pHttp->au1DataString[0] = '\0';
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        STRCPY (pHttp->au1KeyString, "P2_TIME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        pHttp->au1DataString[0] = '\0';
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

    }

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    VpnDsUnLock ();
    WebnmUnRegisterLock (pHttp);
    return;

}

/********************************************************************
*  Function Name : VpnPrintPolicies    
*  Description   : This function prints the Policies configured for 
                   IPSec.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
VpnPrintPolicies (tHttp * pHttp, UINT1 *pu1PolicyName,
                  UINT1 *pu1TempPolicyName, UINT1 u1PolicyPage)
{

    tSNMP_OCTET_STRING_TYPE *pPolicyName = NULL;
    tSNMP_OCTET_STRING_TYPE *pNextPolicyName = NULL;
    UINT1               au1PolicyName[WEB_MAX_NAME_LENGTH];
    INT4                i4PolicyType = 0;
    UINT1               u1Display = FALSE;

    /* u1PolicyPage
     * 
     * This argument is used to know from which page VpnPrintPolicies function
     * is called.The following values are set accordingly as follows.
     * VpnPolicyPage - 0
     * VpnIpsecPage - 1 
     * VpnIkePage - 2 
     * VpnClientTerminationPage - 4 */

    /* This routine displays all the configured policies according to 
     * the type of the policy.
     * 
     * The arguments can be of the following combinations 
     * 
     * 1. pu1PolicyName     - NULL 
     *    pu1TempPolicyName - pointer to a char 
     *    
     * 2. pu1PolicyName     - Pointer to a char 
     *    pu1TempPolicyName - NULL 
     *    
     * 3. pu1PolicyName     - NULL 
     *    pu1TempPolicyName - NULL
     *
     *  Example of Combination 1
     *  
     * **************************************************************
     * When the configuration page get routinue is either called for the 
     * normal get or called from set routinue after deleting a policy ,
     * pu1PolicyName has to be NULL and pu1TempPolicyName has to be a 
     * pointer to a temporary buffer.
     *
     * Once all the policies are displayed in the drop down in that page,
     * the last policy configurations are displayed in the page.This policy
     * is returned in the temporary buffer so that corresponding configruations
     * of the policy can be displayed in the page.
     *
     * ex : when deleting a policy or for normal get operation from client
     * termination page.
     * *****************************************************************
     *
     * Example of Combination 2 
     *
     * ******************************************************************
     * When the policy to be displayed is known as in the case of creating a 
     * new policy or displaying the select policy , combination 2 is selected.
     * Here the policy to be displayed is passed as argument 1 and second argument
     * is NULL.
     *
     * ex1:When a new policy is created or for displaying a policy cofigurations 
     * from client termination page. 
     * ex2: When a policy configuration has to be displayed from policy configuration
     *  page
     * ***********************************************************************
     *
     * Example of Combination 3
     *
     * ***********************************************************************
     * Here only the policies are displayed in the dropdown and the configurations
     * are not displayed in the page. Hence both the arguments are NULL.
     *
     * ex2:For displaying the policies in the policy configuatiion page for normal
     * get operation.
     * **********************************************************************
     * 
     * */

    MEMSET (au1PolicyName, 0, WEB_MAX_NAME_LENGTH);
    pPolicyName = allocmem_octetstring (WEB_MAX_NAME_LENGTH);
    if (pPolicyName == NULL)
    {
        return;
    }

    MEMSET (pPolicyName->pu1_OctetList, 0, WEB_MAX_NAME_LENGTH);

    if (nmhGetFirstIndexFsVpnTable (pPolicyName) == SNMP_FAILURE)
    {
        free_octetstring (pPolicyName);
        return;
    }

    pNextPolicyName = allocmem_octetstring (WEB_MAX_NAME_LENGTH);
    if (pNextPolicyName == NULL)
    {
        free_octetstring (pPolicyName);
        return;
    }
    pNextPolicyName->i4_Length = pPolicyName->i4_Length;
    MEMSET (pNextPolicyName->pu1_OctetList, 0, WEB_MAX_NAME_LENGTH);
    MEMCPY (pNextPolicyName->pu1_OctetList, pPolicyName->pu1_OctetList,
            pPolicyName->i4_Length);

    STRCPY (pHttp->au1KeyString, "<! IPSEC POLICIES>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {

        u1Display = FALSE;
        nmhGetFsVpnPolicyType (pNextPolicyName, &i4PolicyType);
        if ((u1PolicyPage == VPN_IKE_XAUTH_PAGE)
            && ((i4PolicyType == VPN_XAUTH)
                || (i4PolicyType == VPN_IKE_RA_PRESHAREDKEY)))
        {
            /* If this routine is called from Client termination page and the
             * policy type is XAUTH*/
            u1Display = TRUE;
        }
        if ((u1PolicyPage == VPN_IPSEC_PAGE)
            && (i4PolicyType == VPN_IPSEC_MANUAL))
        {
            u1Display = TRUE;
        }
        if ((u1PolicyPage == VPN_IKE_PAGE)
            && (i4PolicyType == VPN_IKE_PRESHAREDKEY))
        {
            u1Display = TRUE;
        }
        else if ((u1PolicyPage == VPN_POLICY_PAGE) &&
                 ((i4PolicyType == VPN_IPSEC_MANUAL)
                  || (i4PolicyType == VPN_IKE_PRESHAREDKEY)
                  || (i4PolicyType == VPN_IKE_RA_PRESHAREDKEY)
                  || (i4PolicyType == VPN_XAUTH)))
        {
            u1Display = TRUE;
        }

        if (u1Display == TRUE)
        {
            MEMSET (au1PolicyName, 0, WEB_MAX_NAME_LENGTH);
            STRNCPY (au1PolicyName, pNextPolicyName->pu1_OctetList,
                     (size_t) pNextPolicyName->i4_Length);

            SNPRINTF ((CHR1 *) pHttp->au1DataString,
                      sizeof (pHttp->au1DataString),
                      "<option value = \"%s\">%s \n", au1PolicyName,
                      au1PolicyName);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            if (pu1TempPolicyName != NULL)
            {
                MEMSET (pu1TempPolicyName, '\0', WEB_MAX_NAME_LENGTH);
                STRNCPY (pu1TempPolicyName, au1PolicyName,
                         (WEB_MAX_NAME_LENGTH - 1));
            }

        }

        MEMSET (pPolicyName->pu1_OctetList, 0, WEB_MAX_NAME_LENGTH);
        MEMCPY (pPolicyName->pu1_OctetList,
                pNextPolicyName->pu1_OctetList, pNextPolicyName->i4_Length);
        pPolicyName->i4_Length = pNextPolicyName->i4_Length;
    }
    while (nmhGetNextIndexFsVpnTable (pPolicyName, pNextPolicyName)
           == SNMP_SUCCESS);

    STRCPY (pHttp->au1KeyString, "POLICIES_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    if (pu1PolicyName != NULL)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1PolicyName);
    }
    else
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1PolicyName);
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    free_octetstring (pPolicyName);
    free_octetstring (pNextPolicyName);
}

/*********************************************************************
*  Function Name : VpnGetAddressPoolInfo    
*  Description   : This function prints the Information for AddressPool 
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
VpnGetAddressPoolInfo (tHttp * pHttp,
                       tSNMP_OCTET_STRING_TYPE * pVpnRaAddressPoolName)
{

    UINT1               au1IpAddr[IP6_ADDR_SIZE];
    CHR1                ac1Temp[16];
    UINT4               u4IpAddr = 0;
    CHR1               *pc1Temp = NULL;
    CHR1               *pc1TempBuff[1];
    CONST CHR1         *VpnStartAddrKey[] = {
        NULL,
        "START_ADDR1_KEY",
        "START_ADDR2_KEY",
        "START_ADDR3_KEY",
        "START_ADDR4_KEY"
    };
    CONST CHR1         *VpnEndAddrKey[] = {
        NULL,
        "END_ADDR1_KEY",
        "END_ADDR2_KEY",
        "END_ADDR3_KEY",
        "END_ADDR4_KEY"
    };
    UINT1               u1Flag = 0;
    UINT4               u4PrefixLen = 0;
    tIp6Addr            Ip6Addr;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT4               u4AddrType = 0;

    if (pVpnRaAddressPoolName != NULL)
    {
        STRCPY (pHttp->au1KeyString, "VPN_POOL_NAME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                 pVpnRaAddressPoolName->pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        MEMSET (au1IpAddr, 0, sizeof (au1IpAddr));
        OctetStr.i4_Length = sizeof (IP6_ADDR_SIZE);
        OctetStr.pu1_OctetList = au1IpAddr;
        nmhGetFsVpnRaAddressPoolStart (pVpnRaAddressPoolName, &OctetStr);

        if (OctetStr.i4_Length == IPVX_IPV4_ADDR_LEN)
        {
            MEMCPY (&u4IpAddr, OctetStr.pu1_OctetList, sizeof (UINT4));
            VPN_INET_NTOA (au1IpAddr, u4IpAddr);

            pc1TempBuff[0] = ac1Temp;
            do
            {
                if (u1Flag == 0)
                {
                    pc1Temp = STRTOK_R (au1IpAddr, ".", &pc1TempBuff);
                    u1Flag++;
                }
                else
                {
                    pc1Temp = STRTOK_R (NULL, ".", &pc1TempBuff);
                }
                if (u1Flag < 5)
                {
                    STRNCPY (pHttp->au1KeyString, VpnStartAddrKey[u1Flag],
                             ENM_MAX_NAME_LEN);
                    pHttp->au1KeyString[ENM_MAX_NAME_LEN - 1] = '\0';
                    WebnmSendString (pHttp, (UINT1 *) pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pc1Temp);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) STRLEN (pHttp->au1DataString));
                }
                u1Flag++;
            }
            while (u1Flag != 5);

            u1Flag = 0;
            MEMSET (au1IpAddr, 0, sizeof (au1IpAddr));
            OctetStr.i4_Length = sizeof (UINT4);
            OctetStr.pu1_OctetList = au1IpAddr;
            nmhGetFsVpnRaAddressPoolEnd (pVpnRaAddressPoolName, &OctetStr);
            MEMCPY (&u4IpAddr, OctetStr.pu1_OctetList, sizeof (UINT4));
            VPN_INET_NTOA (au1IpAddr, u4IpAddr);

            pc1TempBuff[0] = ac1Temp;
            do
            {
                if (u1Flag == 0)
                {
                    pc1Temp = STRTOK_R (au1IpAddr, ".", &pc1TempBuff);
                    u1Flag++;
                }
                else
                {
                    pc1Temp = STRTOK_R (NULL, ".", &pc1TempBuff);
                }

                if (u1Flag < 5)
                {
                    STRNCPY (pHttp->au1KeyString, VpnEndAddrKey[u1Flag],
                             ENM_MAX_NAME_LEN);
                    pHttp->au1KeyString[STRLEN (VpnEndAddrKey[u1Flag])] = '\0';
                    WebnmSendString (pHttp, (UINT1 *) pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pc1Temp);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) STRLEN (pHttp->au1DataString));
                }
                u1Flag++;
            }
            while (u1Flag != 5);
        }
        else
        {
            u4AddrType = 2;
            STRCPY (pHttp->au1KeyString, "ADDR_TYPE_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4AddrType);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            for (u1Flag = 1; u1Flag < 5; u1Flag++)
            {
                STRCPY (pHttp->au1KeyString, VpnStartAddrKey[u1Flag]);
                WebnmSendString (pHttp, pHttp->au1KeyString);
                pHttp->au1DataString[0] = '\0';
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
            }

            for (u1Flag = 1; u1Flag < 5; u1Flag++)
            {
                STRCPY (pHttp->au1KeyString, VpnEndAddrKey[u1Flag]);
                WebnmSendString (pHttp, pHttp->au1KeyString);
                pHttp->au1DataString[0] = '\0';
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
            }
            MEMCPY (Ip6Addr.u1_addr, OctetStr.pu1_OctetList, IP6_ADDR_SIZE);
            STRCPY (pHttp->au1KeyString, "START_IPV6_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     Ip6PrintAddr (&Ip6Addr));
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            MEMSET (au1IpAddr, 0, sizeof (au1IpAddr));
            OctetStr.i4_Length = IP6_ADDR_SIZE;
            OctetStr.pu1_OctetList = au1IpAddr;
            nmhGetFsVpnRaAddressPoolEnd (pVpnRaAddressPoolName, &OctetStr);
            MEMCPY (Ip6Addr.u1_addr, OctetStr.pu1_OctetList, IP6_ADDR_SIZE);
            STRCPY (pHttp->au1KeyString, "END_IPV6_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     Ip6PrintAddr (&Ip6Addr));
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "PREFIX_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetFsVpnRaAddressPoolPrefixLen (pVpnRaAddressPoolName,
                                               &u4PrefixLen);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4PrefixLen);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
        }

    }
    else
    {
        STRCPY (pHttp->au1KeyString, "VPN_POOL_NAME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        pHttp->au1DataString[0] = '\0';
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        for (u1Flag = 1; u1Flag < 5; u1Flag++)
        {
            STRNCPY (pHttp->au1KeyString, VpnStartAddrKey[u1Flag],
                     sizeof (pHttp->au1KeyString));
            WebnmSendString (pHttp, pHttp->au1KeyString);
            pHttp->au1DataString[0] = '\0';
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
        }

        for (u1Flag = 1; u1Flag < 5; u1Flag++)
        {
            STRCPY (pHttp->au1KeyString, VpnEndAddrKey[u1Flag]);
            WebnmSendString (pHttp, pHttp->au1KeyString);
            pHttp->au1DataString[0] = '\0';
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
        }
        STRCPY (pHttp->au1KeyString, "START_IPV6_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        pHttp->au1DataString[0] = '\0';
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "END_IPV6_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        pHttp->au1DataString[0] = '\0';
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "PREFIX_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        pHttp->au1DataString[0] = '\0';
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
    }

}

/*********************************************************************
*  Function Name : VpnGetRaPoolInfo    
*  Description   : This function returns the RA address pool netmask 
*                  and network.   
*  Input(s)      : None.
*  Output(s)     : PoolNetwork and PoolNetmask.
*  Return Values : VPN_SUCCESS/VPN_FAILURE
*********************************************************************/

INT4
VpnGetRaPoolInfo (UINT4 *pu4PoolNetwork, UINT4 *pu4PoolNetMask)
{
    tSNMP_OCTET_STRING_TYPE VpnRaAddressPoolName;
    UINT1               au1TempStr[WEB_MAX_NAME_LENGTH];
    UINT4               u4DestMask = 0;
    UINT4               u4DestIp = 0;
    UINT4               u4IpAddr = 0;
    tSNMP_OCTET_STRING_TYPE IpAddr;

    /* Since only 1 address pool is allowed , we are getting the first entry
     * to find whether address pool is configured or not. if it fails we send 0
     * to the front end .In front end this value is stored in a variable and
     * prompts an alert message of the dependency of configuring the address pool.*/

    MEMSET (au1TempStr, 0, sizeof (au1TempStr));
    VpnRaAddressPoolName.pu1_OctetList = au1TempStr;

    if (nmhGetFirstIndexFsVpnRaAddressPoolTable (&VpnRaAddressPoolName) ==
        SNMP_FAILURE)
    {
        return VPN_FAILURE;
    }
    else
    {
        MEMSET (au1TempStr, 0, sizeof (UINT4));
        IpAddr.i4_Length = sizeof (UINT4);
        IpAddr.pu1_OctetList = au1TempStr;
        nmhGetFsVpnRaAddressPoolStart (&VpnRaAddressPoolName, &IpAddr);
        if (IpAddr.i4_Length == IPVX_IPV4_ADDR_LEN)
        {
            MEMCPY (&u4IpAddr, IpAddr.pu1_OctetList, sizeof (UINT4));
        }
        if (VPN_IS_ADDR_CLASS_A (u4IpAddr))
        {
            /* CLASS A ADDRESS */
            u4DestMask = 0xff000000;
            u4DestIp = (u4IpAddr & u4DestMask);
        }
        else if (VPN_IS_ADDR_CLASS_B (u4IpAddr))
        {
            /* CLASS B ADDRESS */
            u4DestMask = 0xffff0000;
            u4DestIp = (u4IpAddr & u4DestMask);
        }
        else if (VPN_IS_ADDR_CLASS_C (u4IpAddr))
        {
            /* CLASS C ADDRESS */
            u4DestMask = 0xffffff00;
            u4DestIp = (u4IpAddr & u4DestMask);
        }
    }

    *pu4PoolNetMask = u4DestMask;
    *pu4PoolNetwork = u4DestIp;
    return VPN_SUCCESS;

}

/*********************************************************************
*  Function Name : VpnPrintAvailableRemIdValues
*  Description   : This function sends the list of Remote Identity
*                   values configured in the system.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
VpnPrintAvailableRemIdValues (tHttp * pHttp)
{
    INT4                i4RemIdType = 0;
    INT4                i4NextRemIdType = 0;
    INT4                i4OutCome = 0;
    tSNMP_OCTET_STRING_TYPE *pRemIdValue;
    tSNMP_OCTET_STRING_TYPE *pNextRemIdValue;

    pRemIdValue = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (WEB_VPN_MAX_NAME_LEN + 1);
    if (pRemIdValue == NULL)
    {
        IssSendError (pHttp, (CONST INT1 *) "Failed to Allocate Memory");
        return;
    }

    pNextRemIdValue = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (WEB_VPN_MAX_NAME_LEN + 1);

    if (pNextRemIdValue == NULL)
    {
        free_octetstring (pRemIdValue);
        IssSendError (pHttp, (CONST INT1 *) "Failed to Allocate Memory");
        return;
    }
    MEMSET (pNextRemIdValue->pu1_OctetList, 0, WEB_VPN_MAX_NAME_LEN + 1);

    i4OutCome = nmhGetFirstIndexFsVpnRemoteIdTable (&i4NextRemIdType,
                                                    pNextRemIdValue);
    if (i4OutCome == SNMP_FAILURE)
    {
        free_octetstring (pRemIdValue);
        free_octetstring (pNextRemIdValue);
        return;
    }

    STRCPY (pHttp->au1KeyString, "<! REMIDVALUES>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        MEMSET (pRemIdValue->pu1_OctetList, 0, WEB_VPN_MAX_NAME_LEN + 1);
        SPRINTF ((CHR1 *) pHttp->au1DataString,
                 "<option value = \"%s\">%s \n",
                 pNextRemIdValue->pu1_OctetList,
                 pNextRemIdValue->pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        i4RemIdType = i4NextRemIdType;
        pRemIdValue->i4_Length = pNextRemIdValue->i4_Length;
        MEMCPY (pRemIdValue->pu1_OctetList,
                pNextRemIdValue->pu1_OctetList, pNextRemIdValue->i4_Length);
        MEMSET (pNextRemIdValue->pu1_OctetList, 0, WEB_VPN_MAX_NAME_LEN + 1);
    }
    while (nmhGetNextIndexFsVpnRemoteIdTable (i4RemIdType, &i4NextRemIdType,
                                              pRemIdValue,
                                              pNextRemIdValue) == SNMP_SUCCESS);

    free_octetstring (pRemIdValue);
    free_octetstring (pNextRemIdValue);
}

#endif /*  __VPNWEB_C__  */
#endif /* VPN_WANTED */
