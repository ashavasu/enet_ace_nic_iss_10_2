/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlweb.c,v 1.10 2014/03/16 11:41:01 siva Exp $
 *
 * Description: This file contains Firewall routines web interface.
 *
 *******************************************************************/

#ifndef __FWLWEB_C__
#define __FWLWEB_C__

#include "webiss.h"
#include "fwlweb.h"
#include "firewall.h"
#include "snmputil.h"
/*********************************************************************
*  Function Name : IssProcessFwlIntfPage
*  Description   : This function processes the request coming for the 
*                  Firewall  page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessFwlIntfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessFwlIntfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessFwlIntfPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessFwlIntfPageSet
*  Description   : This function processes the request coming for the 
*                  FireWall  page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessFwlIntfPageSet (tHttp * pHttp)
{
    UINT4               u4IfIndex = 0;
    UINT4               u4IfType = 0;
    UINT4               u4IfTrapThreshold = 0;
    INT4                i4RetVal = 0;
    UINT4               u4Error = 0;
    INT4                i4RowStatus = 0;

    STRCPY (pHttp->au1Name, "WLAN_ID_DUMMY");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4IfIndex = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "IF_TYPE");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4IfType = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "TRAP_THRESHOLD");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4IfTrapThreshold = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {
        if (nmhTestv2FwlIfRowStatus
            (&u4Error, (INT4) u4IfIndex, (INT4) DESTROY) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (INT1 *) "Configuration Failed "
                          "Check all Inputs");
            return;
        }
        nmhSetFwlIfRowStatus ((INT4) u4IfIndex, (INT4) DESTROY);
    }
    else
    {
        if ((i4RetVal = nmhGetFwlIfRowStatus ((INT4) u4IfIndex, &i4RowStatus))
            != SNMP_SUCCESS)
        {
            if (nmhTestv2FwlIfRowStatus
                (&u4Error, (INT4) u4IfIndex,
                 (INT4) CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (INT1 *) "Configuration Failed "
                              "Check all Inputs");
                return;
            }
            nmhSetFwlIfRowStatus ((INT4) u4IfIndex, (INT4) CREATE_AND_WAIT);
            nmhSetFwlIfRowStatus ((INT4) u4IfIndex, (INT4) NOT_IN_SERVICE);
        }
        else
        {
            if (nmhTestv2FwlIfRowStatus
                (&u4Error, (INT4) u4IfIndex,
                 (INT4) NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (INT1 *) "Configuration Failed "
                              "Check all Inputs");
                return;
            }
            nmhSetFwlIfRowStatus ((INT4) u4IfIndex, (INT4) NOT_IN_SERVICE);
        }
        if (nmhTestv2FwlIfIfType (&u4Error, (INT4) u4IfIndex, (INT4) u4IfType)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp, (INT1 *) "Configuration Failed "
                          "Check all Inputs");
            nmhSetFwlIfRowStatus ((INT4) u4IfIndex, (INT4) DESTROY);
            return;
        }

        nmhSetFwlIfIfType ((INT4) u4IfIndex, (INT4) u4IfType);

        if (nmhTestv2FwlIfTrapThreshold
            (&u4Error, (INT4) u4IfIndex,
             (INT4) u4IfTrapThreshold) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (INT1 *) "Configuration Failed "
                          "Check all Inputs");
            nmhSetFwlIfRowStatus ((INT4) u4IfIndex, (INT4) DESTROY);
            return;
        }

        nmhSetFwlIfTrapThreshold ((INT4) u4IfIndex, (INT4) u4IfTrapThreshold);

        if (nmhTestv2FwlIfRowStatus
            (&u4Error, (INT4) u4IfIndex, (INT4) ACTIVE) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (INT1 *) "Configuration Failed "
                          "Check all Inputs");
            nmhSetFwlIfRowStatus ((INT4) u4IfIndex, (INT4) DESTROY);
            return;
        }
        nmhSetFwlIfRowStatus ((INT4) u4IfIndex, (INT4) ACTIVE);
    }
    IssProcessFwlIntfPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssProcessFwlIntfPageGet
*  Description   : This function processes the request coming for the 
*                  FireWall  page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessFwlIntfPageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    INT4                i4OutCome = 0;
    INT4                i4FwlIfIndex = 0;
    INT1                ai1IfName[32];
    INT4                i4IfType = 0;
    INT4                i4IfTrapThreshold = 0;

    pHttp->i4Write = 0;

    STRNCPY (pHttp->au1Value, "DEFAULT_VLAN", STRLEN ("DEFAULT_VLAN"));

    SecPrintIpInterfaces (pHttp, ISS_WEB_ALL_TYPE, ISS_WEB_NETWORK_TYPE_WAN,
                          ISS_WEB_WAN_TYPE_ALL);
    STRCPY (pHttp->au1Value, "");

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    i4OutCome = nmhGetFirstIndexFwlDefnIfTable (&i4FwlIfIndex);
    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    do
    {
        if (i4FwlIfIndex == FWL_GLOBAL_IDX)
        {

            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            continue;
        }
        pHttp->i4Write = (INT4) u4Temp;

        STRCPY (pHttp->au1KeyString, "WLAN_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        MEMSET (ai1IfName, 0, sizeof (ai1IfName));
        CfaCliGetIfName ((UINT4) i4FwlIfIndex, ai1IfName);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", (CHR1 *) ai1IfName);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString, "WLAN_ID_DUMMY_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4FwlIfIndex);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString, "IF_TYPE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFwlIfIfType (i4FwlIfIndex, &i4IfType);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4IfType);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString, "TRAP_THRESHOLD_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFwlIfTrapThreshold (i4FwlIfIndex, &i4IfTrapThreshold);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4IfTrapThreshold);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    while ((nmhGetNextIndexFwlDefnIfTable (i4FwlIfIndex, &i4FwlIfIndex))
           == SNMP_SUCCESS);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
*  Function Name : IssProcessFwlAclPage 
*  Description   : This function processes the request coming for the 
*                  ACL  page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessFwlAclPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessFwlAclPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessFwlAclPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessFwlAclPageGet 
*  Description   : This function processes the request coming for the 
*                  ACL  page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessFwlAclPageGet (tHttp * pHttp)
{
    INT4                i4FwlAclAction = -1;
    INT4                i4FwlAclSeqNum = -1;
    INT4                i4FwlAclLogTrigger = -1;
    INT4                i4FwlAclDirection = -1;
    INT4                i4FwlAclIfIndex = -1;
    INT4                i4NextFwlAclIfIndex = -1;
    INT4                i4NextFwlAclDirection = -1;
    INT4                i4rc = -1;
    tSNMP_OCTET_STRING_TYPE FwlAclName;
    tSNMP_OCTET_STRING_TYPE FilterSet;
    tSNMP_OCTET_STRING_TYPE NextFwlAclName;
    UINT1               au1FwlAclName[FWL_MAX_ACL_NAME_LEN];
    UINT1               au1NextFwlAclName[FWL_MAX_ACL_NAME_LEN];
    UINT1               au1FilterSet[FWL_MAX_FILTER_NAME_LEN];
    UINT1               au1Temp[FWL_MAX_ACL_NAME_LEN];
    INT4                i4FwlAclFragAction;
    UINT4               u4Temp = 0;
    UINT2               u2Flag = 0;

    IssPrintAvailableFirewallFilters (pHttp);
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    MEMSET (au1FwlAclName, 0, FWL_MAX_ACL_NAME_LEN);
    MEMSET (au1NextFwlAclName, 0, FWL_MAX_ACL_NAME_LEN);
    MEMSET (au1FilterSet, 0, FWL_MAX_FILTER_NAME_LEN);

    FwlAclName.pu1_OctetList = au1FwlAclName;
    NextFwlAclName.pu1_OctetList = au1NextFwlAclName;
    FilterSet.pu1_OctetList = au1FilterSet;

    i4rc =
        nmhGetFirstIndexFwlDefnAclTable (&i4FwlAclIfIndex, &FwlAclName,
                                         &i4FwlAclDirection);
    if (i4rc == SNMP_SUCCESS)
    {
        MEMCPY (NextFwlAclName.pu1_OctetList, FwlAclName.pu1_OctetList,
                FwlAclName.i4_Length);
        NextFwlAclName.i4_Length = FwlAclName.i4_Length;
        i4NextFwlAclIfIndex = i4FwlAclIfIndex;
        i4NextFwlAclDirection = i4FwlAclDirection;

        while (1)
        {
            pHttp->i4Write = (INT4) u4Temp;
            u2Flag = 1;
            STRCPY (pHttp->au1KeyString, "ACL_NAME_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMCPY (au1Temp, NextFwlAclName.pu1_OctetList,
                    NextFwlAclName.i4_Length);
            au1Temp[NextFwlAclName.i4_Length] = FWL_END_OF_STRING;

            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Temp);

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "FILTER_NAME_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            nmhGetFwlRuleFilterSet (&NextFwlAclName, &FilterSet);

            MEMCPY (au1Temp, FilterSet.pu1_OctetList, FilterSet.i4_Length);
            au1Temp[FilterSet.i4_Length] = FWL_END_OF_STRING;

            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Temp);

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "PACKET_DIR_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                     (UINT2) i4NextFwlAclDirection);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "ACL_ACTION_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            nmhGetFwlAclAction (i4NextFwlAclIfIndex, &NextFwlAclName,
                                i4NextFwlAclDirection, &i4FwlAclAction);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                     (UINT2) i4FwlAclAction);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "PRIORITY_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetFwlAclSequenceNumber (i4NextFwlAclIfIndex, &NextFwlAclName,
                                        i4NextFwlAclDirection, &i4FwlAclSeqNum);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                     (UINT2) i4FwlAclSeqNum);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "LOGS_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetFwlAclLogTrigger (i4NextFwlAclIfIndex, &NextFwlAclName,
                                    i4NextFwlAclDirection, &i4FwlAclLogTrigger);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                     (UINT2) i4FwlAclLogTrigger);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "ACL_FRAG_ACTION_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetFwlAclFragAction (i4NextFwlAclIfIndex, &NextFwlAclName,
                                    i4NextFwlAclDirection, &i4FwlAclFragAction);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                     (UINT2) i4FwlAclFragAction);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

            /* get the next elem */
            MEMCPY (FwlAclName.pu1_OctetList, NextFwlAclName.pu1_OctetList,
                    NextFwlAclName.i4_Length);
            FwlAclName.i4_Length = NextFwlAclName.i4_Length;

            i4FwlAclIfIndex = i4NextFwlAclIfIndex;
            i4FwlAclDirection = i4NextFwlAclDirection;
            i4rc =
                nmhGetNextIndexFwlDefnAclTable (i4FwlAclIfIndex,
                                                &i4NextFwlAclIfIndex,
                                                &FwlAclName, &NextFwlAclName,
                                                i4FwlAclDirection,
                                                &i4NextFwlAclDirection);

            if (i4rc == SNMP_FAILURE)
            {
                break;
            }
        }
    }
    if (u2Flag == 1)
    {
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
    }
    return;
}

/*********************************************************************
*  Function Name : IssProcessFwlAclPageSet 
*  Description   : This function processes the request coming for the 
*                  ACL  page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessFwlAclPageSet (tHttp * pHttp)
{

    UINT4               u4Direction = 0;
    UINT4               u4Permit = 0;
    UINT4               u4Priority = 0;
    UINT2               u1LogTrigger = 0;
    UINT4               u4Interface = 0;
    UINT4               u4Error;
    UINT1               u1Apply = 0;
    INT4                i4RowStatus;
    INT4                i4Status = ISS_CREATE_AND_WAIT;
    INT4                i4RetVal = SNMP_FAILURE;
    INT4                i4FragAction = 2;

    tSNMP_OCTET_STRING_TYPE AclName;
    tSNMP_OCTET_STRING_TYPE FilterSet;
    UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN];
    UINT1               au1FilterSet[FWL_MAX_FILTER_NAME_LEN];

    MEMSET (&AclName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&FilterSet, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    STRCPY (pHttp->au1Name, "ACL_NAME");

    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        AclName.i4_Length = (INT4) STRLEN (pHttp->au1Value);
        if (AclName.i4_Length > FWL_MAX_ACL_NAME_LEN)
        {
            IssSendError (pHttp,
                          (INT1 *)
                          "Access List name cannot exceed more than 36 characters !\r\n");
            return;
        }
        AclName.pu1_OctetList = au1AclName;
        MEMCPY (AclName.pu1_OctetList, pHttp->au1Value, AclName.i4_Length);
    }

    STRCPY (pHttp->au1Name, "FILTER_NAME");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        FilterSet.i4_Length = (INT4) STRLEN (pHttp->au1Value);
        if (FilterSet.i4_Length > FWL_MAX_ACL_NAME_LEN)
        {
            IssSendError (pHttp,
                          (INT1 *)
                          "Filter name cannot exceed more than 36 characters !\r\n");
            return;
        }
        FilterSet.pu1_OctetList = au1FilterSet;
        MEMCPY (FilterSet.pu1_OctetList, pHttp->au1Value, FilterSet.i4_Length);
    }

    if ((FilterSet.pu1_OctetList == NULL) || (AclName.pu1_OctetList == NULL))
    {
        IssSendError (pHttp, (INT1 *) "Access List could not "
                      "be created due to lack of  resources ");
        return;
    }

    STRCPY (pHttp->au1Name, "PACKET_DIR");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4Direction = (UINT2) (ATOI (pHttp->au1Value));
    }

    STRCPY (pHttp->au1Name, "ACTION");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        if (STRCMP (pHttp->au1Value, "Delete") == 0)
        {
            /* Delete the ACL */

            nmhSetFwlAclRowStatus ((INT4) u4Interface, &AclName,
                                   (INT4) u4Direction, ISS_DESTROY);
            nmhSetFwlRuleRowStatus (&AclName, ISS_DESTROY);

            IssProcessFwlAclPageGet (pHttp);
            return;
        }
    }
    if (STRCMP (pHttp->au1Value, "Apply") == 0)
    {
        u1Apply = 1;
        i4Status = ISS_NOT_IN_SERVICE;
    }

    STRCPY (pHttp->au1Name, "ACL_ACT");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4Permit = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "PRIORITY");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4Priority = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "LOGS");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u1LogTrigger = (UINT1) (ATOI (pHttp->au1Value));
    }

    STRCPY (pHttp->au1Name, "ACL_FRAG_ACT");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4FragAction = ATOI (pHttp->au1Value);
    }
    if (u1Apply != 1)
    {
        if ((i4RetVal = nmhGetFwlFilterRowStatus (&AclName, &i4RowStatus))
            == SNMP_SUCCESS)
        {
            IssSendError (pHttp,
                          (INT1 *) "A matching Filter name already exists "
                          " for this ACL name.\r\nACL name should be "
                          "unique!\r\n");
            return;
        }
    }

    if ((i4RetVal = nmhGetFwlRuleRowStatus (&AclName, &i4RowStatus))
        != SNMP_SUCCESS)
    {

        if (nmhSetFwlRuleRowStatus (&AclName, ISS_CREATE_AND_WAIT) !=
            SNMP_SUCCESS)
        {
            IssSendError (pHttp,
                          (INT1 *) "%%Max Access Lists configured. Cannot"
                          " create a new ACL !\r\n");
            return;

        }

        if (nmhTestv2FwlRuleFilterSet (&u4Error, &AclName, &FilterSet) !=
            SNMP_SUCCESS)
        {

            IssSendError (pHttp, (INT1 *) "Check the filter set specified !");
            if (i4RetVal != SNMP_SUCCESS)
            {
                nmhSetFwlRuleRowStatus (&AclName, ISS_DESTROY);
            }
            return;
        }
        if (nmhSetFwlRuleFilterSet (&AclName, &FilterSet) != SNMP_SUCCESS)
        {
            IssSendError (pHttp,
                          (INT1 *) "Check the filter set specified or specify "
                          "filter combination like filter1,filter2 !\r\n");
            nmhSetFwlRuleRowStatus (&AclName, FWL_DESTROY);
            return;
        }
    }

    else
    {
        tSNMP_OCTET_STRING_TYPE RuleFilterSet;
        UINT1               au1RuleFilterSet[MAX_OCTETSTRING_SIZE];

        RuleFilterSet.i4_Length = 256;
        RuleFilterSet.pu1_OctetList = au1RuleFilterSet;

        /* Rule exists. Now get the filter set for the corresponding acl name */
        if (nmhGetFwlRuleFilterSet (&AclName, &RuleFilterSet) == SNMP_SUCCESS)
        {
            RuleFilterSet.pu1_OctetList[RuleFilterSet.i4_Length] = 0;
            FilterSet.pu1_OctetList[FilterSet.i4_Length] = 0;

            /* Check if the filter set is different from the user specified one.
             * If so dont allow the user to modify the filter set */
            if (STRCMP (RuleFilterSet.pu1_OctetList,
                        FilterSet.pu1_OctetList) != 0)
            {

                IssSendError (pHttp,
                              (INT1 *) "filter Set cannot be modified\r\n");
                return;
            }
        }
    }

    nmhSetFwlRuleRowStatus (&AclName, ISS_ACTIVE);
    if (nmhGetFwlAclRowStatus ((INT4) u4Interface, &AclName,
                               (INT4) u4Direction,
                               &i4RowStatus) != SNMP_SUCCESS)
    {
        if (nmhSetFwlAclRowStatus
            ((INT4) u4Interface, &AclName, (INT4) u4Direction,
             ISS_CREATE_AND_WAIT) != SNMP_SUCCESS)
        {

            IssSendError (pHttp, (INT1 *) "Access List table  is full !\r\n");
            return;
        }
    }
    else
    {
        if (i4RowStatus == ISS_ACTIVE)
        {
            nmhSetFwlAclRowStatus ((INT4) u4Interface, &AclName,
                                   (INT4) u4Direction, ISS_NOT_IN_SERVICE);
        }
    }

    nmhSetFwlAclAction ((INT4) u4Interface, &AclName, (INT4) u4Direction,
                        (INT4) u4Permit);

    if (nmhSetFwlAclSequenceNumber
        ((INT4) u4Interface, &AclName, (INT4) u4Direction,
         (INT4) u4Priority) != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (INT1 *) "Cannot assign the given priority "
                      "since an Access list already exists with this priority\r\n");
        return;
    }
    nmhSetFwlAclFragAction ((INT4) u4Interface, &AclName, (INT4) u4Direction,
                            i4FragAction);
    nmhSetFwlAclLogTrigger ((INT4) u4Interface, &AclName, (INT4) u4Direction,
                            u1LogTrigger);
    nmhSetFwlAclRowStatus ((INT4) u4Interface, &AclName, (INT4) u4Direction,
                           ISS_ACTIVE);
    IssProcessFwlAclPageGet (pHttp);

    UNUSED_PARAM (i4Status);
}

/*********************************************************************
 *  Function Name : IssProcessFwlLogPage 
 *  Description   : This function processes the request coming for the  
 *                  firewall log page. 
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessFwlLogPage (tHttp * pHttp)
{
    UINT1              *pu1LogStr = NULL;

    STRCPY (pHttp->au1KeyString, "FWL_LOG_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1Name, "ACTION");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        if (STRCMP (pHttp->au1Value, "Clear") == 0)
        {
#ifndef KERNEL_PROGRAMMING_WANTED
            FwlClearLogBuffer ();
#endif
        }
    }

#ifndef KERNEL_PROGRAMMING_WANTED
    pu1LogStr = (UINT1 *) FwlUtilShowLogBuffer ();
#endif
    if (pu1LogStr == NULL)
    {
        return;
    }

    if (STRLEN (pu1LogStr) != 0)
    {
        WebnmSockWrite (pHttp, pu1LogStr, (INT4) STRLEN (pu1LogStr));
    }
    FwlUtilReleaseWebLog (pu1LogStr);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

}

/*********************************************************************
*  Function Name : IssPrintAvailableFirewallFilters
*  Description   : This function sends the list of Firewall Filters
*                  configured in the system.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssPrintAvailableFirewallFilters (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE FilterName;
    tSNMP_OCTET_STRING_TYPE NextFilterName;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN];
    UINT1               au1NextFilterName[FWL_MAX_FILTER_NAME_LEN];
    INT4                i4OutCome = 0;

    FilterName.pu1_OctetList = au1FilterName;
    FilterName.i4_Length = FWL_MAX_FILTER_NAME_LEN;

    NextFilterName.pu1_OctetList = au1NextFilterName;
    NextFilterName.i4_Length = FWL_MAX_FILTER_NAME_LEN;

    MEMSET (FilterName.pu1_OctetList, 0, FWL_MAX_FILTER_NAME_LEN);
    MEMSET (NextFilterName.pu1_OctetList, 0, FWL_MAX_FILTER_NAME_LEN);
    FilterName.i4_Length = 0;
    NextFilterName.i4_Length = 0;
    i4OutCome = nmhGetFirstIndexFwlDefnFilterTable (&NextFilterName);
    if (i4OutCome == SNMP_FAILURE)
    {
        return;
    }

    STRCPY (pHttp->au1KeyString, "<! FIREWALL FILTERS>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        MEMSET (FilterName.pu1_OctetList, 0, FWL_MAX_FILTER_NAME_LEN);
        FilterName.i4_Length = 0;

        SPRINTF ((CHR1 *) pHttp->au1DataString,
                 "<option value =\"%s\" >%s\n",
                 NextFilterName.pu1_OctetList, NextFilterName.pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        MEMCPY (FilterName.pu1_OctetList, NextFilterName.pu1_OctetList,
                NextFilterName.i4_Length);
        FilterName.i4_Length = NextFilterName.i4_Length;
        MEMSET (NextFilterName.pu1_OctetList, 0, FWL_MAX_FILTER_NAME_LEN);
        NextFilterName.i4_Length = 0;
    }
    while (nmhGetNextIndexFwlDefnFilterTable (&FilterName, &NextFilterName) ==
           SNMP_SUCCESS);

}

/*********************************************************************
 *  Function Name : IssProcessFwlStatsPage 
 *  Description   : This function processes the request coming for the  
 *                  firewall statistics page. 
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessFwlStatsPage (tHttp * pHttp)
{

    UINT4               u4RetVal;

    STRCPY (pHttp->au1KeyString, "TOTAL_PACKETS_INSPECTED_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFwlStatInspectedPacketsCount (&u4RetVal);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RetVal);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "TOTAL_PACKETS_DROPPED_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFwlStatTotalPacketsDenied (&u4RetVal);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RetVal);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "TOTAL_PACKETS_ACCEPTED_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFwlStatTotalPacketsAccepted (&u4RetVal);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RetVal);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "ICMP_PACKETS_DROPPED_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFwlStatTotalIcmpPacketsDenied (&u4RetVal);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RetVal);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "SYN_PACKETS_DROPPED_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFwlStatTotalSynPacketsDenied (&u4RetVal);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RetVal);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "IP_SPOOFED_PACKETS_DROPPED_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFwlStatTotalIpSpoofedPacketsDenied (&u4RetVal);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RetVal);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "SOURCE_ROUTE_PACKETS_DENIED_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFwlStatTotalSrcRoutePacketsDenied (&u4RetVal);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RetVal);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "TINY_FRAGMENT_PACKETS_DROPPED_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFwlStatTotalTinyFragmentPacketsDenied (&u4RetVal);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RetVal);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "LARGE_FRAGMENTS_PACKETS_DROPPED_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFwlStatTotalLargeFragmentPacketsDenied (&u4RetVal);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RetVal);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "IP_OPTION_DROPPED_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFwlStatTotalIpOptionPacketsDenied (&u4RetVal);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RetVal);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "SUSPICIOUS_ATTACKS_DROPPED_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFwlStatTotalAttacksPacketsDenied (&u4RetVal);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RetVal);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
*  Function Name : IssProcessFwlDMZV6Page 
*  Description   : This function processes the request coming for the 
*                  DMZV6 page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessFwlDMZV6Page (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessFwlDMZV6PageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessFwlDMZV6PageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessFwlDMZV6PageSet
*  Description   : This function processes the request coming for the 
*                  DMZv6  page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessFwlDMZV6PageSet (tHttp * pHttp)
{
    UINT4               u4Error = 0;
    UINT4               u4DmzExists = 0;
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    tSNMP_OCTET_STRING_TYPE *pDmzIp6Host;
    tIp6Addr            Ipv6Address;
    pDmzIp6Host = allocmem_octetstring (SNMP_MAX_OCTETSTRING_SIZE);
    if (pDmzIp6Host == NULL)
    {
        return;
    }

    STRCPY (pHttp->au1Name, "HOST_IP");
    MEMSET (&Ipv6Address, '\0', sizeof (tIp6Addr));
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    issDecodeSpecialChar (pHttp->au1Value);
    if (INET_ATON6 (pHttp->au1Value, &Ipv6Address) == 0)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid IPv6 address");
        free_octetstring (pDmzIp6Host);
        return;
    }

    MEMCPY (pDmzIp6Host->pu1_OctetList, &Ipv6Address, IP6_ADDR_SIZE);
    pDmzIp6Host->i4_Length = IP6_ADDR_SIZE;

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {
        /* Delete the DMZ V6 */
        if (nmhTestv2FwlDmzIpv6RowStatus
            (&u4Error, pDmzIp6Host, DESTROY) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (INT1 *) "Configuration Failed "
                          "Check all Inputs");
            free_octetstring (pDmzIp6Host);
            return;
        }
        nmhSetFwlDmzIpv6RowStatus (pDmzIp6Host, 6);
    }

    if (STRCMP (pHttp->au1Value, "Add") == 0)
    {

        i4RetVal = nmhGetFwlDmzIpv6RowStatus (pDmzIp6Host, &i4RowStatus);

        if (i4RetVal != SNMP_SUCCESS)
        {

            if (nmhTestv2FwlDmzIpv6RowStatus (&u4DmzExists, pDmzIp6Host,
                                              4) != SNMP_FAILURE)
            {
                if (nmhSetFwlDmzIpv6RowStatus (pDmzIp6Host, 4) != SNMP_SUCCESS)
                {
                    free_octetstring (pDmzIp6Host);
                    return;
                }
                nmhSetFwlDmzIpv6RowStatus (pDmzIp6Host, 1);
            }
            else
            {
                if (u4DmzExists == SNMP_ERR_NO_CREATION)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) "ERROR: Maximum number of "
                                  "DMZIPv6 hosts reached");
                    free_octetstring (pDmzIp6Host);
                    return;
                }
                if (u4DmzExists == SNMP_ERR_WRONG_VALUE)
                {

                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "ERROR: Cannot Add the DMZIpv6 Entry. "
                                  "Exceeds the Maximum Entry Limit");
                    free_octetstring (pDmzIp6Host);
                    return;
                }
            }

        }
        else
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          " ERROR : DMZ IPv6 host already Exits ");
            free_octetstring (pDmzIp6Host);
            return;
        }
    }
    IssProcessFwlDMZV6PageGet (pHttp);
    free_octetstring (pDmzIp6Host);
}

/*********************************************************************
*  Function Name : IssProcessFwlDMZV6PageGet
*  Description   : This function processes the request coming for the 
*                  DMZV6  page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessFwlDMZV6PageGet (tHttp * pHttp)
{
    INT4                i4Temp = 0;
    tSNMP_OCTET_STRING_TYPE *pFwlDmzIpv6Index;
    tIp6Addr            DmzIp6Host;
    MEMSET (&DmzIp6Host, 0, sizeof (tIp6Addr));
    pHttp->i4Write = 0;

    pFwlDmzIpv6Index = allocmem_octetstring (SNMP_MAX_OCTETSTRING_SIZE);
    if (NULL == pFwlDmzIpv6Index)
    {
        return;
    }
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    i4Temp = pHttp->i4Write;
    if ((nmhGetFirstIndexFwlDefnIPv6DmzTable
         (pFwlDmzIpv6Index)) == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);

        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        free_octetstring (pFwlDmzIpv6Index);
        return;
    }
    do
    {
        pHttp->i4Write = i4Temp;

        STRCPY (pHttp->au1KeyString, "HOST_IP_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        MEMCPY (&DmzIp6Host, pFwlDmzIpv6Index->pu1_OctetList, IP6_ADDR_SIZE);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                 Ip6PrintAddr (&DmzIp6Host));
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

    }
    while (nmhGetNextIndexFwlDefnIPv6DmzTable
           (pFwlDmzIpv6Index, pFwlDmzIpv6Index) == SNMP_SUCCESS);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    free_octetstring (pFwlDmzIpv6Index);
}

/*********************************************************************
*  Function Name : IssProcessFwlIcmpPage 
*  Description   : This function processes the request coming for the 
*                  ICMPv6  page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessFwlIcmpPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessFwlicmpPageGet (pHttp);
    }

    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessFwlicmpPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessFwlicmpPageGet
*  Description   : This function processes the request coming for the 
*                  ICMPv6  page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessFwlicmpPageGet (tHttp * pHttp)
{
    INT4                i4RetValFwlIcmpv6MsgType = 0;
    INT4                i4RetValFwlIcmpv4MsgType = 0;
    INT4                i4Check = 1;
    INT4                i4UnCheck = 0;
    INT4                i4Temp = 0;
    pHttp->i4Write = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    i4Temp = pHttp->i4Write;

    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);

    pHttp->i4Write = i4Temp;

    nmhGetFwlIfICMPv6MsgType (0, &i4RetValFwlIcmpv6MsgType);
    if ((i4RetValFwlIcmpv6MsgType & 0x00000000) == 0)
    {
        STRCPY (pHttp->au1KeyString, "ICMPV6_NO_INSPECT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    }
    else
    {
        STRCPY (pHttp->au1KeyString, "ICMPV6_NO_INSPECT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    }
    if ((i4RetValFwlIcmpv6MsgType & 0x00000001) != 0)
    {
        STRCPY (pHttp->au1KeyString, "ICMPV6_DESTINATION_UNREACHABLE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    }
    else
    {
        STRCPY (pHttp->au1KeyString, "ICMPV6_DESTINATION_UNREACHABLE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    }

    if ((i4RetValFwlIcmpv6MsgType & 0x00000002) != 0)
    {
        STRCPY (pHttp->au1KeyString, "ICMPV6_TIME_EXCEEDED_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    }
    else
    {
        STRCPY (pHttp->au1KeyString, "ICMPV6_TIME_EXCEEDED_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    }

    if ((i4RetValFwlIcmpv6MsgType & 0x00000004) != 0)
    {
        STRCPY (pHttp->au1KeyString, "ICMPV6_PARAMETER_PROBLEM_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    }

    else
    {
        STRCPY (pHttp->au1KeyString, "ICMPV6_PARAMETER_PROBLEM_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    }

    if ((i4RetValFwlIcmpv6MsgType & 0x00000020) != 0)
    {
        STRCPY (pHttp->au1KeyString, "ICMPV6_ECHO_REQUEST_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    }

    else
    {
        STRCPY (pHttp->au1KeyString, "ICMPV6_ECHO_REQUEST_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    }

    if ((i4RetValFwlIcmpv6MsgType & 0x00000040) != 0)
    {
        STRCPY (pHttp->au1KeyString, "ICMPV6_ECHO_REPLY_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    }

    else
    {
        STRCPY (pHttp->au1KeyString, "ICMPV6_ECHO_REPLY_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    }

    if ((i4RetValFwlIcmpv6MsgType & 0x00000080) != 0)
    {
        STRCPY (pHttp->au1KeyString, "ICMPV6_REDIRECT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    }

    else
    {
        STRCPY (pHttp->au1KeyString, "ICMPV6_REDIRECT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    }

    if ((i4RetValFwlIcmpv6MsgType & 0x00000100) != 0)
    {
        STRCPY (pHttp->au1KeyString, "ICMPV6_INFORMATION_REQUEST_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    }

    else
    {
        STRCPY (pHttp->au1KeyString, "ICMPV6_INFORMATION_REQUEST_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    }

    if ((i4RetValFwlIcmpv6MsgType & 0x00000200) != 0)
    {
        STRCPY (pHttp->au1KeyString, "ICMPV6_INFORMATION_REPLY_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    }

    else
    {
        STRCPY (pHttp->au1KeyString, "ICMPV6_INFORMATION_REPLY_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    }
    STRCPY (pHttp->au1KeyString, "icmpv4Option_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFwlIfICMPType (0, &i4RetValFwlIcmpv4MsgType);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetValFwlIcmpv4MsgType);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
*  Function Name : IssProcessFwlicmpPageSet
*  Description   : This function processes the request coming for the 
*                  ICMPv6  page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessFwlicmpPageSet (tHttp * pHttp)
{
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    UINT4               u4Error = 0;
    UINT4               u4Interface = 0;
    UINT4               u4NoIncpect = 0;
    UINT4               u4IncpectAll = 0;
    UINT4               u4IncpectUnreachable = 0;
    UINT4               u4IncpectExcced = 0;
    UINT4               u4IncpectProblem = 0;
    UINT4               u4IncpectReply = 0;
    UINT4               u4IncpectRedirect = 0;
    UINT4               u4IncpectInformRequ = 0;
    UINT4               u4IncpectEchoReply = 0;
    UINT4               u4GlobalValue = 0;
    UINT4               u4IncpectInformRepl = 0;
    UINT4               u4IcmpType = 0;
    UINT4               u4IcmpCode = 255;
    UINT1               u1Flag = 0;

    STRCPY (pHttp->au1Name, "ICMPV6_NO_INSPECT");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4NoIncpect = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "ICMPV6_INSPECT_ALL");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4IncpectAll = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "ICMPV6_DESTINATION_UNREACHABLE");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4IncpectUnreachable = (UINT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "ICMPV6_TIME_EXCEEDED");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4IncpectExcced = (UINT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "ICMPV6_PARAMETER_PROBLEM");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4IncpectProblem = (UINT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "ICMPV6_ECHO_REQUEST");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4IncpectReply = (UINT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "ICMPV6_REDIRECT");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4IncpectRedirect = (UINT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "ICMPV6_ECHO_REPLY");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4IncpectEchoReply = (UINT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "ICMPV6_INFORMATION_REQUEST");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4IncpectInformRequ = (UINT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "ICMPV6_INFORMATION_REPLY");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4IncpectInformRepl = (UINT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "icmpv4Option");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4IcmpType = (UINT4) ATOI (pHttp->au1Value);
    }
    if (u4IncpectAll != 0)
    {
        u4GlobalValue = u4IncpectAll;
        u1Flag = 1;
    }
    if (u4IncpectUnreachable != 0)
    {
        u4GlobalValue = u4GlobalValue + u4IncpectUnreachable;
        u1Flag = 1;
    }
    if (u4IncpectEchoReply != 0)
    {
        u4GlobalValue = u4GlobalValue + u4IncpectEchoReply;
        u1Flag = 1;
    }
    if (u4IncpectExcced != 0)
    {
        u4GlobalValue = u4GlobalValue + u4IncpectExcced;
        u1Flag = 1;
    }
    if (u4IncpectProblem != 0)
    {
        u4GlobalValue = u4GlobalValue + u4IncpectProblem;
        u1Flag = 1;
    }
    if (u4IncpectReply != 0)
    {
        u4GlobalValue = u4GlobalValue + u4IncpectReply;
        u1Flag = 1;
    }
    if (u4IncpectRedirect != 0)
    {
        u4GlobalValue = u4GlobalValue + u4IncpectRedirect;
        u1Flag = 1;
    }
    if (u4IncpectInformRequ != 0)
    {
        u4GlobalValue = u4GlobalValue + u4IncpectInformRequ;
        u1Flag = 1;
    }

    if (u4IncpectInformRepl != 0)
    {
        u4GlobalValue = u4GlobalValue + u4IncpectInformRepl;
        u1Flag = 1;
    }
    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if ((i4RetVal = nmhGetFwlIfRowStatus ((INT4) u4Interface, &i4RowStatus))
        != SNMP_SUCCESS)
    {
        /* DSL_ADD : Change FWL_CREATE_AND_GO to FWL_CREATE_AND_WAIT after
         * testing */
        nmhSetFwlIfRowStatus ((INT4) u4Interface, ISS_CREATE_AND_GO);
        nmhSetFwlIfRowStatus ((INT4) u4Interface, ISS_NOT_IN_SERVICE);
    }
    else
    {
        if (i4RowStatus == ISS_ACTIVE)
        {
            nmhSetFwlIfRowStatus ((INT4) u4Interface, ISS_NOT_IN_SERVICE);
        }
    }
    if (nmhTestv2FwlIfICMPv6MsgType
        (&u4Error, (INT4) u4Interface, (INT4) u4GlobalValue) != SNMP_SUCCESS)
    {
        if (i4RetVal != SNMP_SUCCESS)
        {
            nmhSetFwlIfRowStatus ((INT4) u4Interface, ISS_DESTROY);
            return;
        }
        if (i4RowStatus == ISS_ACTIVE)
        {
            nmhSetFwlIfRowStatus ((INT4) u4Interface, ISS_ACTIVE);
            return;
        }
    }
    nmhSetFwlIfICMPv6MsgType ((INT4) u4Interface, (INT4) u4GlobalValue);
    nmhSetFwlIfRowStatus ((INT4) u4Interface, ISS_ACTIVE);

    if ((i4RetVal = nmhGetFwlIfRowStatus ((INT4) u4Interface, &i4RowStatus))
        != SNMP_SUCCESS)
    {
        /* DSL_ADD : Change FWL_CREATE_AND_GO to FWL_CREATE_AND_WAIT after
         *          * testing */
        nmhSetFwlIfRowStatus ((INT4) u4Interface, ISS_CREATE_AND_GO);
        nmhSetFwlIfRowStatus ((INT4) u4Interface, ISS_NOT_IN_SERVICE);
    }
    else
    {
        if (i4RowStatus == ISS_ACTIVE)
        {
            nmhSetFwlIfRowStatus ((INT4) u4Interface, ISS_NOT_IN_SERVICE);
        }
    }

    if (nmhTestv2FwlIfICMPType (&u4Error, (INT4) u4Interface, (INT4) u4IcmpType)
        != SNMP_SUCCESS)
    {
        /*  CliPrintf (CliHandle, "%%ICMP type should be one of the "
           "following values \r\n0 3 4 5 8 11 12 "
           "13 14 15 16 17 18 !\r\n"); */
        if (i4RetVal != SNMP_SUCCESS)
        {
            nmhSetFwlIfRowStatus ((INT4) u4Interface, ISS_DESTROY);
        }
        if (i4RowStatus == ISS_ACTIVE)
        {
            nmhSetFwlIfRowStatus ((INT4) u4Interface, ISS_ACTIVE);
        }
    }
    if (nmhTestv2FwlIfICMPCode (&u4Error, (INT4) u4Interface, (INT4) u4IcmpCode)
        != SNMP_SUCCESS)
    {
        /*    CliPrintf (CliHandle, "%%ICMP Code should be one of the "
           "following values \r\n1 2 3 4 5 6 7 8 9 " "10 11 12 !\r\n"); */
        if (i4RetVal != SNMP_SUCCESS)
        {
            nmhSetFwlIfRowStatus ((INT4) u4Interface, ISS_DESTROY);
        }
        if (i4RowStatus == ISS_ACTIVE)
        {
            nmhSetFwlIfRowStatus ((INT4) u4Interface, ISS_ACTIVE);
        }
    }
    nmhSetFwlIfICMPType ((INT4) u4Interface, (INT4) u4IcmpType);
    nmhSetFwlIfICMPCode ((INT4) u4Interface, (INT4) u4IcmpCode);
    nmhSetFwlIfRowStatus ((INT4) u4Interface, ISS_ACTIVE);

    IssProcessFwlicmpPageGet (pHttp);
    UNUSED_PARAM (u4NoIncpect);
    UNUSED_PARAM (u1Flag);
    return;
}
#endif /*__FWLWEB_C__*/
