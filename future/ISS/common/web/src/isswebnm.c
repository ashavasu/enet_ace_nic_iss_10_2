/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: isswebnm.c,v 1.56 2014/07/17 12:34:07 siva Exp $
 *
 * Description: Routines for ISS web Module 
 *******************************************************************/
#ifdef WEBNM_WANTED

#include "isswebnm.h"
#include "webnmutl.h"
#include "ipv6.h"
#include "vcm.h"
#include"fsvlan.h"
#include "l2iwf.h"
#include "isshttp.h"
#ifdef NPAPI_WANTED
#include "nputil.h"
#endif

#include "fssyslog.h"
#include "fswebnm.h"

extern INT4         IssAddLinksToPage (tHttp * pHttp);
tsQuery             gQuery[MAX_HTTP_PROCCESSING_TASKS][ISS_MAX_QUERY];

/************************************************************************
 *  Function Name   : IssProcessGet 
 *  Description     : Process the Get Request for both scalar and table
 *  Input           : pHttp - Pointer to Http entry
 *  Output          : None
 *  Returns         : None
 ************************************************************************/

VOID
IssProcessGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0, u4Cnt = 0;
    UINT4               u4Count = 0, u4Error = 0, u4TempCount = 0;
    UINT4               u4I = 0, u4Indices = 0;
    UINT4               u4InContext = 0;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort;
    UINT1               u1FirstScalar = 0;
    UINT1               u1Mi = OSIX_FALSE;
    UINT1               TABLE_STOP_FLAG[] = "<! TABLE STOP>";
    tSNMP_OID_TYPE     *pCurOid = NULL, *pNextOid = NULL, *pInstance = NULL;
    tSNMP_MULTI_DATA_TYPE *pData = NULL;
    tSNMP_MULTI_DATA_TYPE *pTableData[WENM_MAX_MULTI_DATA];
    tSNMP_MULTI_DATA_TYPE *pTempTableData = NULL;
    INT4                i4PageNo = 0, i4EntryCnt = 0;
    INT4                i4OutCome;
    INT4                i4PageEnd = 0, i4PageStart = 0, i = 0, i4PageFound = 0;
    INT1                i1Flag = 0;
    tSnmpIndex         *pCurIndex = NULL, *pNextIndex = NULL;
    UINT4               u4Index = 0;
    INT2                i1count = 0;
    INT1               *piIfName;
    INT1                i1TempFlag = 0;
    UINT1               au1Temp[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4L3ContextId = 0;
    UINT4               u4VcId = 0;
    INT4                i4Index = ENM_FAILURE;
#ifdef STACKING_WANTED
    INT4                i4LocalStackPort = 0;
    INT4                i4RemoteStackPort = 0;
    INT4                i4StackIndex = 0;
    tHwPortInfo         HwPortInfo;
    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);

    i4LocalStackPort = NpUtilGetStackingPortIndex (&HwPortInfo, i4StackIndex);
    i4RemoteStackPort = NpUtilGetRemoteStackingPortIndex (&HwPortInfo);
#endif

    CHR1               *api1ChgOidSnmp[] = { "port_control.html",
        "port_ratectrl.html",
        "vlan_pvidsetting.html",
        "vlan_switchport_filtering.html",
        "bcm_vlan_pvidsetting.html",
        "vlan_gvrpportconf.html",
        "vlan_garptimersconf.html",
        "port_monitoring.html",
        "mstp_portconf.html",
        "rstp_portconf.html",
        "la_portconf.html",
        "pnac_portconfig.html",
        "pnac_porttimer.html",
        "vlan_gmrpportconf.html",
        "interface_stats.html",
        "ether_stats.html",
        "la_portlacpstats.html",
        "la_neighbourstats.html",
        "pnac_sessionstats.html",
        "eoam_portsettings.html",
        "eoam_loopbackconfig.html",
        "rstp_portstats.html",
        "pnac_suppsessionstats.html",
        "dhcpc_statistics.html",
        "dcbx_adminstatus.html",
        NULL
    };

    MEMSET (pTableData, 0, sizeof (pTableData));
    if (HttpChildProcessIndex (pHttp->ProcessingTaskId, &i4Index) ==
        ENM_FAILURE)
    {
        return;
    }
    pCurIndex = WebnmIndexPool1[i4Index];
    pNextIndex = WebnmIndexPool2[i4Index];

    piIfName = (INT1 *) &au1Temp[0];

    MEMSET (au1Temp, 0, CFA_MAX_PORT_NAME_LENGTH);

    if (IssAddLinksToPage (pHttp) == SNMP_SUCCESS)
    {
        STRCPY (pHttp->au1Name, "PAGENO");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4PageNo = ATOI (pHttp->au1Value);
        }
        i4PageFound = 1;
    }
    i4PageStart = ((INT4) INTERFACES_PER_PAGE * i4PageNo) + 1;
    i4PageEnd = (i4PageNo + 1) * (INT4) INTERFACES_PER_PAGE;
    for (u4Count = 0; u4Count < (UINT4) pHttp->i4NoOfSnmpOid; u4Count++)
    {
        if (u1FirstScalar == 1)
            break;
        pCurOid = &pHttp->pOid[u4Count];
        if (pCurOid->pu4_OidList[pCurOid->u4_Length - 1] == 0)
        {
            ISS_ALLOC_MULTIDATA (pData);
            MEMSET (pData->pOctetStrValue->pu1_OctetList, 0, 10);
            WebnmConvertOidToString (pCurOid, pHttp->au1KeyString);
            STRCAT (pHttp->au1KeyString, ISS_KEY_STRING);
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (SNMPGet (*pCurOid, pData, &u4Error, pCurIndex,
                         SNMP_DEFAULT_CONTEXT) == SNMP_SUCCESS)
            {
                IssConvertDataToString (pData, pHttp->au1DataString,
                                        pHttp->pu1Type[u4Count]);
            }
            else
            {
                pHttp->au1DataString[0] = '\0';
                if (u4Error == NO_SUCH_OBJECT)
                    HTTP_STRCPY (pHttp->au1DataString, ISS_NO_SUCH_OBJ_STR);
                else if (u4Error == NO_SUCH_INSTANCE)
                    HTTP_STRCPY (pHttp->au1DataString, ISS_NO_SUCH_INS_STR);
                else
                    HTTP_STRCPY (pHttp->au1DataString, IssSnmpError[u4Error]);
            }
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            if (pHttp->pu1Access[u4Count] != READONLY)
            {
                if (WebnmSendString (pHttp, pHttp->au1KeyString) == ENM_SUCCESS)
                {
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                }
            }
        }
        else
        {
            /* Get Process for Table Elements */
            for (u4TempCount = u4Count; u4TempCount <
                 (UINT4) pHttp->i4NoOfSnmpOid; u4TempCount++)
            {
                ISS_ALLOC_MULTIDATA (pTableData[u4TempCount]);
            }
            ISS_ALLOC_OID (pCurOid);
            ISS_ALLOC_OID (pNextOid);
            ISS_ALLOC_OID (pInstance);
            ISS_ALLOC_MULTIDATA (pTempTableData);
            i4OutCome = WebnmSendString (pHttp, ISS_MI_FLAG);
            /*Handle both MI& SI Page   */
            if (i4OutCome == ENM_SUCCESS)
            {
                u1Mi = OSIX_TRUE;    /* Flag reflecting MI */
            }
            if (i4OutCome == ENM_FAILURE)
            {
                WebnmSendString (pHttp, ISS_TABLE_FLAG);
            }
            u4Temp = (UINT4) pHttp->i4Write;

            u4TempCount = u4Count;
            while (pHttp->pu1Access[u4TempCount] == INDEX)
            {
                u4TempCount++;
                u4Indices++;
            }
            pCurOid->u4_Length = 0;
            WebnmCopyOid (pCurOid, &pHttp->pOid[u4TempCount]);
            SNMPGetNextOID (*pCurOid, pNextOid, pTableData[u4TempCount],
                            &u4Error, pCurIndex, pNextIndex,
                            SNMP_DEFAULT_CONTEXT);

            WebnmGetInstanceOid (&pHttp->pOid[u4TempCount], pNextOid,
                                 pInstance);

            if (pInstance->u4_Length == 0)
            {
                u1FirstScalar = 1;
                IssSkipString (pHttp, TABLE_STOP_FLAG);
                break;
            }

            if (pNextIndex->u4No != u4Indices)
            {
                u1FirstScalar = 1;
                IssSkipString (pHttp, TABLE_STOP_FLAG);
                break;
            }

            if (pNextOid->u4_Length <= pCurOid->u4_Length)
            {
                u1FirstScalar = 1;
                IssSkipString (pHttp, TABLE_STOP_FLAG);
                break;
            }

            u4TempCount = u4Count;
            for (u4Cnt = 0; u4Cnt < pNextIndex->u4No; u4Cnt++)
            {
                pNextIndex->pIndex[u4Cnt].i2_DataType =
                    pHttp->pu1Type[u4TempCount + u4Cnt];
                pTableData[u4TempCount + u4Cnt] = &(pNextIndex->pIndex[u4Cnt]);
            }

            pInstance->u4_Length = pNextOid->u4_Length - pCurOid->u4_Length;

            for (u4I = pCurOid->u4_Length; u4I < pNextOid->u4_Length; u4I++)
            {
                pInstance->pu4_OidList[u4I - pCurOid->u4_Length] =
                    pNextOid->pu4_OidList[u4I];
            }

            while (1)
            {
                if (i4PageNo != -1)
                {
                    for (i = 0; i < (i4PageNo * (INT4) INTERFACES_PER_PAGE);
                         i++)
                    {

                        if ((*(pInstance->pu4_OidList) >= (UINT4) i4PageStart)
                            && (*(pInstance->pu4_OidList) <= (UINT4) i4PageEnd))
                        {
                            break;
                        }
                        pCurOid->u4_Length = 0;
                        WebnmCopyOid (pCurOid,
                                      &pHttp->pOid[pHttp->i4NoOfSnmpOid - 1]);
                        WebnmCopyOid (pCurOid, pInstance);
                        SNMPGetNextOID (*pCurOid, pNextOid, pTempTableData,
                                        &u4Error, pCurIndex, pNextIndex,
                                        SNMP_DEFAULT_CONTEXT);

                        WebnmGetInstanceOid (&pHttp->
                                             pOid[pHttp->i4NoOfSnmpOid - 1],
                                             pNextOid, pInstance);

                        if (pInstance->u4_Length == 0)
                        {
                            u1FirstScalar = 1;
                            IssSkipString (pHttp, TABLE_STOP_FLAG);
                            i1Flag = 1;
                            break;
                        }

                    }
                    i4PageNo = -1;
                }

                if (i1Flag == 1)
                {
                    i1Flag = 0;
                    break;
                }

                pHttp->i4Write = (INT4) u4Temp;
                for (u4TempCount = u4Count; u4TempCount < u4Count + u4Cnt;
                     u4TempCount++)
                {
                    if ((i4PageFound) &&
                        (pTableData[u4TempCount]->i4_SLongValue > i4PageEnd))
                    {
                        u1FirstScalar = 1;
                        i1Flag = 0;
                        IssSkipString (pHttp, TABLE_STOP_FLAG);
                        break;
                    }
                    if (u1Mi == OSIX_TRUE)
                        /*Check for MI to Display only the 
                           Context Specific Ports */
                    {
                        VcmGetContextInfoFromIfIndex
                            (*(pInstance->pu4_OidList),
                             &u4InContext, &u2LocalPort);
                        /*Get the Context for the Session */
                        if (WebnmApiGetContextId (pHttp, &u4ContextId)
                            != ENM_SUCCESS)
                        {
                            continue;
                        }
                    }
                    pCurOid->u4_Length = 0;
                    WebnmCopyOid (pCurOid, &pHttp->pOid[u4TempCount]);
                    WebnmConvertOidToString (pCurOid, pHttp->au1KeyString);

                    if ((u1Mi == OSIX_TRUE) && (u4InContext == u4ContextId))
                        /* For MI Cases Dislay the mapped Ports */
                    {
                        /*L3 Context Support */
                        if ((STRCMP ("ivr6_conf.html", pHttp->ai1HtmlName) == 0)
                            || (STRCMP ("ipv6if_stats.html", pHttp->ai1HtmlName)
                                == 0))
                        {
                            IssConvertDataToString (pTableData[u4TempCount],
                                                    pHttp->au1DataString,
                                                    pHttp->
                                                    pu1Type[u4TempCount]);

                            if (WebnmApiGetL3ContextId (pHttp, &u4L3ContextId)
                                == ENM_FAILURE)
                            {
                                continue;
                            }

                            u4Index = (UINT4) ATOI (pHttp->au1DataString);
                            VcmGetContextInfoFromIfIndex ((UINT4) u4Index,
                                                          &u4VcId,
                                                          &u2LocalPort);
                            if (u4L3ContextId == u4VcId)
                            {
#ifdef IP6_WANTED
                                if (Ip6SelectContext (u4L3ContextId) ==
                                    SNMP_SUCCESS)
                                {
                                    Ip6ReleaseContext ();
                                }
                                else
                                {
                                    Ip6ReleaseContext ();
                                }
#endif
                            }
                            if (u4L3ContextId != u4VcId)
                            {

                                IssSkipString (pHttp, TABLE_STOP_FLAG);
                                /*        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG); */
                                continue;
                            }
                        }
#ifdef STACKING_WANTED
                        if (STRCMP ("interface_stats.html", pHttp->ai1HtmlName)
                            == 0)
                        {
                            IssConvertDataToString (pTableData[u4TempCount],
                                                    pHttp->au1DataString,
                                                    pHttp->
                                                    pu1Type[u4TempCount]);
                            u4Index = (UINT4) ATOI (pHttp->au1DataString);
                            if ((u4Index == (UINT4) i4LocalStackPort) ||
                                (u4Index == (UINT4) i4RemoteStackPort))
                            {
                                IssSkipString (pHttp, TABLE_STOP_FLAG);
                                continue;
                            }
                        }
#endif

                        STRCAT (pHttp->au1KeyString, ISS_KEY_STRING);
                        WebnmSendString (pHttp, pHttp->au1KeyString);

                        IssConvertDataToString (pTableData[u4TempCount],
                                                pHttp->au1DataString,
                                                pHttp->pu1Type[u4TempCount]);

                        while (api1ChgOidSnmp[i1count] != NULL)
                        {
                            if (STRCMP
                                (api1ChgOidSnmp[i1count],
                                 pHttp->ai1HtmlName) == 0)
                            {
                                i1TempFlag = 1;
                                u4Index = (UINT4) ATOI (pHttp->au1DataString);
                                CfaCliGetIfName (u4Index, piIfName);
                                STRNCPY (pHttp->au1DataString, piIfName,
                                         (sizeof (pHttp->au1DataString) - 1));
                                pHttp->
                                    au1DataString[(sizeof (pHttp->au1DataString)
                                                   - 1)] = '\0';
                                break;
                            }
                            i1count++;
                        }
                        if (i1TempFlag == 1)
                        {
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));

                            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4Index);
                            i1TempFlag = 0;
                        }
                        WebnmSockWrite (pHttp, pHttp->au1DataString,
                                        (INT4) HTTP_STRLEN (pHttp->
                                                            au1DataString));
                        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
                    }

                    else if (u1Mi == OSIX_FALSE)
                        /* For SI case Procee as such */
                    {
                        /*L3 Context Support */
                        if ((STRCMP ("ivr6_conf.html", pHttp->ai1HtmlName) == 0)
                            || (STRCMP ("ipv6if_stats.html", pHttp->ai1HtmlName)
                                == 0))
                        {
                            IssConvertDataToString (pTableData[u4TempCount],
                                                    pHttp->au1DataString,
                                                    pHttp->
                                                    pu1Type[u4TempCount]);

                            if (WebnmApiGetL3ContextId (pHttp, &u4L3ContextId)
                                == ENM_FAILURE)
                            {
                                continue;
                            }

                            u4Index = (UINT4) ATOI (pHttp->au1DataString);
                            VcmGetContextInfoFromIfIndex ((UINT4) u4Index,
                                                          &u4VcId,
                                                          &u2LocalPort);
                            if (u4L3ContextId == u4VcId)
                            {
#ifdef IP6_WANTED
                                if (Ip6SelectContext (u4L3ContextId) ==
                                    SNMP_SUCCESS)
                                {
                                    Ip6ReleaseContext ();
                                }
                                else
                                {
                                    Ip6ReleaseContext ();
                                }
#endif
                            }
                            if (u4L3ContextId != u4VcId)
                            {

                                IssSkipString (pHttp, TABLE_STOP_FLAG);
                                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
                                continue;
                            }
                        }
#ifdef STACKING_WANTED
                        if (STRCMP ("interface_stats.html", pHttp->ai1HtmlName)
                            == 0)
                        {
                            IssConvertDataToString (pTableData[u4TempCount],
                                                    pHttp->au1DataString,
                                                    pHttp->
                                                    pu1Type[u4TempCount]);
                            u4Index = (UINT4) ATOI (pHttp->au1DataString);
                            if ((u4Index == (UINT4) i4LocalStackPort) ||
                                (u4Index == (UINT4) i4RemoteStackPort))
                            {
                                IssSkipString (pHttp, TABLE_STOP_FLAG);
                                continue;
                            }
                        }
#endif

                        STRCAT (pHttp->au1KeyString, ISS_KEY_STRING);
                        WebnmSendString (pHttp, pHttp->au1KeyString);
                        IssConvertDataToString (pTableData[u4TempCount],
                                                pHttp->au1DataString,
                                                pHttp->pu1Type[u4TempCount]);

                        while (api1ChgOidSnmp[i1count] != NULL)
                        {
                            if (STRCMP
                                (api1ChgOidSnmp[i1count],
                                 pHttp->ai1HtmlName) == 0)
                            {
                                u4Index = (UINT4) ATOI (pHttp->au1DataString);
                                CfaCliGetIfName (u4Index, piIfName);
                                STRNCPY (pHttp->au1DataString, piIfName,
                                         (sizeof (pHttp->au1DataString) - 1));
                                pHttp->
                                    au1DataString[(sizeof (pHttp->au1DataString)
                                                   - 1)] = '\0';
                                i1TempFlag = 1;
                                break;
                            }
                            i1count++;
                        }
                        if (i1TempFlag == 1)
                        {
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4Index);
                            i1TempFlag = 0;
                        }
                        WebnmSockWrite (pHttp, pHttp->au1DataString,
                                        (INT4) HTTP_STRLEN (pHttp->
                                                            au1DataString));
                        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
                    }

                }
                if (i1Flag == 1)
                {
                    i1Flag = 0;
                    break;
                }

                for (u4TempCount = (u4Count + u4Cnt); u4TempCount <
                     (UINT4) pHttp->i4NoOfSnmpOid; u4TempCount++)
                {

                    if (u1Mi == OSIX_TRUE)
                        /*Check for MI to Display only the 
                           Context Specific Ports */
                    {
                        VcmGetContextInfoFromIfIndex
                            (*(pInstance->pu4_OidList),
                             &u4InContext, &u2LocalPort);
                        /*Get the Context for the Session */
                        if (WebnmApiGetContextId (pHttp, &u4ContextId)
                            != ENM_SUCCESS)
                        {
                            continue;
                        }
                    }
                    pCurOid->u4_Length = 0;
                    WebnmCopyOid (pCurOid, &pHttp->pOid[u4TempCount]);
                    WebnmCopyOid (pCurOid, pInstance);
                    SNMPGet (*pCurOid, pTableData[u4TempCount], &u4Error,
                             pCurIndex, SNMP_DEFAULT_CONTEXT);

                    pCurOid->u4_Length = 0;
                    WebnmCopyOid (pCurOid, &pHttp->pOid[u4TempCount]);
                    if ((u1Mi == OSIX_TRUE) && (u4InContext == u4ContextId))
                        /* For MI Display only the Mapped Ports */
                    {
                        WebnmConvertOidToString (pCurOid, pHttp->au1KeyString);
                        STRCAT (pHttp->au1KeyString, ISS_KEY_STRING);
                        if (WebnmSendString (pHttp, pHttp->au1KeyString) ==
                            ENM_FAILURE)
                            continue;

                        IssConvertDataToString (pTableData[u4TempCount],
                                                pHttp->au1DataString,
                                                pHttp->pu1Type[u4TempCount]);
                        WebnmSockWrite (pHttp, pHttp->au1DataString,
                                        (INT4) HTTP_STRLEN (pHttp->
                                                            au1DataString));
                        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
                    }

                    else if (u1Mi == OSIX_FALSE)
                        /* For SI case Proceed as such */
                    {
                        WebnmConvertOidToString (pCurOid, pHttp->au1KeyString);
                        STRCAT (pHttp->au1KeyString, ISS_KEY_STRING);

                        if (WebnmSendString (pHttp, pHttp->au1KeyString) ==
                            ENM_FAILURE)
                            continue;
                        IssConvertDataToString (pTableData[u4TempCount],
                                                pHttp->au1DataString,
                                                pHttp->pu1Type[u4TempCount]);

                        WebnmSockWrite (pHttp, pHttp->au1DataString,
                                        (INT4) HTTP_STRLEN (pHttp->
                                                            au1DataString));
                        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
                    }

                }

                WebnmCopyOid (pCurOid, pInstance);
                SNMPGetNextOID (*pCurOid, pNextOid, pTempTableData,
                                &u4Error, pCurIndex, pNextIndex,
                                SNMP_DEFAULT_CONTEXT);

                WebnmGetInstanceOid (&pHttp->pOid[u4TempCount - 1], pNextOid,
                                     pInstance);

                if (i4PageFound == 1)
                    i4EntryCnt++;
                if (pInstance->u4_Length == 0
                    || i4EntryCnt > (INT4) ISS_DISP_CNT || u4Error == 0x82)
                {
                    u1FirstScalar = 1;
                    IssSkipString (pHttp, TABLE_STOP_FLAG);
                    break;
                }
            }
        }
    }
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/************************************************************************
 *  Function Name   : IssProcessPost 
 *  Description     : Processes the Post Request. 
 *  Input           : pHttp - Pointer to Http Entry
 *  Output          : None
 *  Returns         : None
 ************************************************************************/

INT4
IssProcessPost (tHttp * pHttp)
{
    tSNMP_OID_TYPE     *pOid = NULL, *pInsOid = NULL;
    tSNMP_OID_TYPE     *pTmpOid = NULL;
    tSNMP_OID_TYPE     *pRowOid = NULL;
    UINT4               u4Count = 0;
    UINT4               u4Cnt = 0;
    UINT4               u4Tmp = 0;
    UINT4               u4TableFound = 0;
    tSNMP_MULTI_DATA_TYPE *pData = NULL;
    tSnmpIndex         *pCurIndex = NULL;
    UINT4               u4Error = 0, u4RetVal = 0;
    UINT1              *pu1Name = NULL;
    UINT1               au1String[WEBNM_MAX_STRING];
    INT4                i4PageNo = 0;
    INT4                i4Index = ENM_FAILURE;
    UINT4               u4RowStatus = OSIX_FALSE;
    UINT4               u4IsRowCreateAndWait = OSIX_FALSE;

    if (HttpChildProcessIndex (pHttp->ProcessingTaskId, &i4Index) ==
        ENM_FAILURE)
    {
        return FAILURE;
    }

    /*setting WebSnmpQuery. 
     * This is used to identify if the SYS_LOG_CMD
     * is generated from web generic page or snmp in SNMPSet*/
    WebnmSetSnmpQueryFromWebStatus (OSIX_TRUE);

    MEMSET (&WebnmIndexPool1[i4Index][0], 0,
            (sizeof (tSnmpIndex) * ISS_MAX_INDICES));
    MEMSET (&WebnmMultiPool1[i4Index][0], 0,
            (sizeof (tSNMP_MULTI_DATA_TYPE) * ISS_MAX_INDICES));
    MEMSET (&WebnmOctetPool1[i4Index][0], 0,
            (sizeof (tSNMP_OCTET_STRING_TYPE) * ISS_MAX_INDICES));
    MEMSET (&WebnmOIDPool1[i4Index][0], 0,
            (sizeof (tSNMP_OID_TYPE) * ISS_MAX_INDICES));
    MEMSET (&au1WebnmData1[i4Index][0][0], 0, 1024);

    for (u4Cnt = 0; u4Cnt < ISS_MAX_INDICES; u4Cnt++)
    {
        WebnmIndexPool1[i4Index][u4Cnt].pIndex
            = &(WebnmMultiPool1[i4Index][u4Cnt]);
        WebnmMultiPool1[i4Index][u4Cnt].pOctetStrValue
            = &(WebnmOctetPool1[i4Index][u4Cnt]);
        WebnmMultiPool1[i4Index][u4Cnt].pOidValue
            = &(WebnmOIDPool1[i4Index][u4Cnt]);
        WebnmMultiPool1[i4Index][u4Cnt].pOctetStrValue->pu1_OctetList
            = (UINT1 *) au1WebnmData1[i4Index][u4Cnt];
        WebnmMultiPool1[i4Index][u4Cnt].pOidValue->pu4_OidList
            = (UINT4 *) (VOID *) au1WebnmData1[i4Index][u4Cnt];
    }
    pCurIndex = WebnmIndexPool1[i4Index];

    ISS_WEBNM_ALLOC_OID (pOid);
    ISS_WEBNM_ALLOC_OID (pTmpOid);
    ISS_WEBNM_ALLOC_OID (pRowOid);
    ISS_WEBNM_ALLOC_OID (pInsOid);
    pInsOid->u4_Length = 0;

    if (HttpNameValueFromPostQuery (gQuery[i4Index], pHttp->au1PostQuery)
        != ENM_SUCCESS)
    {
        IssSendError (pHttp, (INT1 *) "ERROR: Check the query");
        return FAILURE;
    }

    /* [0] - first entry will be GAMBIT
       [1] - second one will be TABLE_INDEX for Table
       [2] - rowstatus/row creation object, with create&wait or
       'not-in-service'
       ...
       [last] - rowstatus with 'active' or 'destroy' value (for table)
     */

    u4Count++;
    if ((pu1Name = (UINT1 *) allocmem_EnmBlk ()) == NULL)
    {
        return ENM_FAILURE;
    }

    /* check whether the query is for TABLE or SCALAR */
    if (STRCMP (gQuery[i4Index][u4Count].u1Name, ISS_TABLE_INDEX) == 0)
    {
        WebnmFormInstance (pHttp, pu1Name);
        WebnmConvertStringToOid (pInsOid, pu1Name, (UINT1) ENM_FAILURE);
        u4TableFound = 1;
        u4Count++;
    }

    for (; (gQuery[i4Index][u4Count].u1Name[0] != '\0'); u4Count++)
    {
        /* do SET for each name-value pair */
        WebnmConvertStringToOid (pOid, gQuery[i4Index][u4Count].u1Name,
                                 (UINT1) ENM_FAILURE);
        WebnmCopyOid (pOid, pInsOid);

        /* get the type from the OID list */
        for (u4Tmp = 0; u4Tmp < (UINT4) pHttp->i4NoOfSnmpOid; u4Tmp++)
        {
            pTmpOid->u4_Length = 0;
            WebnmCopyOid (pTmpOid, &pHttp->pOid[u4Tmp]);
            WebnmConvertOidToString (pTmpOid, pu1Name);
            if (STRCMP (pu1Name, gQuery[i4Index][u4Count].u1Name) == 0)
            {
                if (pHttp->pu1Access[u4Tmp] == INDEX)
                {
                    i4PageNo =
                        (INT4) ((((UINT4)
                                  (ATOI (gQuery[i4Index][u4Count].u1Value)) +
                                  ISS_DISP_CNT) / INTERFACES_PER_PAGE) - 1);
                }
                /* check whether parameter is read-write */
                if (pHttp->pu1Access[u4Tmp] == READWRITE
                    || pHttp->pu1Access[u4Tmp] == READCREATE)
                {
                    if (STRCMP (gQuery[i4Index][u4Count].u1Value, "") != 0)
                    {
                        /* convert the data */
                        if ((pData =
                             IssConvertStringToData (gQuery[i4Index][u4Count].
                                                     u1Value,
                                                     pHttp->pu1Type[u4Tmp])) ==
                            NULL)
                        {
                            IssSendError (pHttp,
                                          (INT1 *)
                                          "ERROR: Unable to extract value");
                            free_EnmBlk (pu1Name);
                            return FAILURE;
                        }
                        SNMPOIDIsRowStatus (*pOid, &u4RowStatus);
                        u4RetVal =
                            (UINT4) SNMPTest (*pOid, pData, &u4Error, pCurIndex,
                                              SNMP_DEFAULT_CONTEXT);
                        if (u4RetVal == SNMP_FAILURE)
                        {
                            if (u4Error == SNMP_ERR_AUTHORIZATION_ERROR)
                            {
                                IssSendError (pHttp,
                                              (INT1 *)
                                              "Operation not Permitted");
                            }
                            else if (u4Error == SNMP_ERR_WRONG_VALUE)
                            {
                                IssSendError (pHttp,
                                              (INT1 *)
                                              "Entered Value Range Is Not Correct");
                            }
                            else if (u4Error == SNMP_ERR_RESOURCE_UNAVAILABLE)
                            {
                                IssSendError (pHttp,
                                              (INT1 *)
                                              "Resources unavailable currently");
                            }
                            else
                            {
                                IssSendCommonError (pHttp,
                                                    (u4Count - u4TableFound));
                            }
                            if (u4IsRowCreateAndWait == OSIX_TRUE)
                            {
                                /* One of the object failed in the post, destroy
                                 * the row, since this is a new row
                                 */
                                pData->i4_SLongValue = DESTROY;
                                SNMPSet (*pRowOid, pData, &u4Error, pCurIndex,
                                         SNMP_DEFAULT_CONTEXT);
                                u4IsRowCreateAndWait = OSIX_FALSE;
                            }
                            free_EnmBlk (pu1Name);
                            return FAILURE;
                        }
                        else
                        {
                            SNMPOIDIsRowStatus (*pOid, &u4RowStatus);
                            if (SNMPSet (*pOid, pData, &u4Error, pCurIndex,
                                         SNMP_DEFAULT_CONTEXT) == SNMP_FAILURE)
                            {
                                if (u4Error == SNMP_ERR_AUTHORIZATION_ERROR)
                                {
                                    IssSendError (pHttp,
                                                  (INT1 *)
                                                  "Operation not Permitted");
                                }
                                else if (u4Error == SNMP_ERR_WRONG_VALUE)
                                {
                                    IssSendError (pHttp,
                                                  (INT1 *)
                                                  "Entered Value Range Is Not Correct");
                                }

                                else
                                {
                                    IssSendCommonError (pHttp,
                                                        (u4Count -
                                                         u4TableFound));
                                }
                                if (u4IsRowCreateAndWait == OSIX_TRUE)
                                {
                                    /* One of the object failed in the post, destroy
                                     * the row, since this is a new row
                                     */
                                    pData->i4_SLongValue = DESTROY;
                                    SNMPSet (*pRowOid, pData, &u4Error,
                                             pCurIndex, SNMP_DEFAULT_CONTEXT);
                                    u4IsRowCreateAndWait = OSIX_FALSE;
                                }
                                free_EnmBlk (pu1Name);
                                return FAILURE;
                            }
                            if ((u4RowStatus == OSIX_TRUE)
                                && (pData->i4_SLongValue == CREATE_AND_WAIT))
                            {
                                pRowOid->u4_Length = 0;
                                WebnmCopyOid (pRowOid, pOid);
                                u4IsRowCreateAndWait = OSIX_TRUE;
                            }
                        }
                    }
                }
                break;
            }
        }
    }
    free_EnmBlk (pu1Name);

    if (pHttp->i4Objreq != ISS_SPECIFIC_REQUEST)
    {
        SPRINTF ((CHR1 *) au1String, "?PAGENO=%d", i4PageNo);
        STRNCAT (pHttp->au1PostQuery, au1String,
                 (ENM_MAX_NAME_LEN - STRLEN (pHttp->au1PostQuery)));
        IssProcessGet (pHttp);
    }

    /* Used to identify if the request is from Web or SNMP 
     * Resetting the variable after Web request is completed*/
    WebnmSetSnmpQueryFromWebStatus (OSIX_FALSE);
    return SUCCESS;
}

/************************************************************************
 *  Function Name   : HttpNameValueFromPostQuery
 *  Description     : Get the Name-Value pair from Post Query
 *  Input           : pu1QueryBuf - Pointer to post query
 *  Output          : pu1NameArray, pu1VaueArray - Pointer to an array, 
 *                    where name-values are stored which are fetched 
 *                    from Query Buffer
 *  Returns         : SUCCESS or FAILURE
 ************************************************************************/

INT4
HttpNameValueFromPostQuery (tsQuery * pQuery, UINT1 *pu1QueryBuf)
{
    UINT1              *pu1Ptr = pu1QueryBuf;
    UINT1              *pu1Name;
    UINT1              *pu1Value;
    UINT4               u4Count;
    UINT4               u4VarCount = 0;
    UINT1               u1IsSearchingForName = FALSE;

    /* search for name-value pair */
    while (*pu1Ptr != '\0')
    {
        pu1Name = (UINT1 *) pQuery[u4VarCount].u1Name;
        pu1Value = (UINT1 *) pQuery[u4VarCount].u1Value;

        u1IsSearchingForName = TRUE;
        u4Count = 0;

        while ((*pu1Ptr != '\0') && (*pu1Ptr != '&'))
        {
            if (*pu1Ptr == '=')
            {
                pu1Name[u4Count] = '\0';

                /* got the name, check if its ACTION */
                if (STRCMP (pu1Name, "ACTION") == 0)
                {
                    pu1Name[0] = '\0';
                    return ENM_SUCCESS;
                }
                u1IsSearchingForName = FALSE;
                u4Count = 0;
                pu1Ptr++;
                continue;
            }
            else
            {
                if (u1IsSearchingForName)
                    pu1Name[u4Count] = *pu1Ptr;
                else
                    pu1Value[u4Count] = *pu1Ptr;
            }
            u4Count++;
            pu1Ptr++;
        }
        pu1Value[u4Count] = '\0';

        /* Check whether i/p value has any special character */
        issDecodeSpecialChar (pu1Value);

        /* got the name-value pair from query */
        pu1Ptr++;
        u4VarCount++;

        if (u4VarCount >= ISS_MAX_QUERY)
            /* No of input queries exceed the max count */
            return ENM_FAILURE;
    }
    return ENM_SUCCESS;
}

/************************************************************************
 *  Function Name   : IssSkipString
 *  Description     : This function will skip until the given string
 *  Input           : pData - String until which it will has to be skipped
 *  Output          : NONE                                              
 *  Returns         : SUCCESS or FAILURE
 ************************************************************************/

INT4
IssSkipString (tHttp * pHttp, UINT1 *pData)
{
    UINT1              *pu1StartPtr = NULL, *pu1EndPtr = NULL;
    pu1StartPtr = (UINT1 *) pHttp->pi1Html + pHttp->i4Write;
    pu1EndPtr = (UINT1 *) STRSTR (pu1StartPtr, pData);
    if (pu1EndPtr != NULL &&
        (pu1EndPtr < (UINT1 *) (pHttp->pi1Html + pHttp->i4HtmlSize)))
    {
        pHttp->i4Write += (pu1EndPtr - pu1StartPtr);
        pHttp->i4Write = pHttp->i4Write + (INT4) HTTP_STRLEN (pData);
        return ENM_SUCCESS;
    }
    return ENM_FAILURE;
}

/************************************************************************
 *  Function Name   : issDecodeSpecialChar
 *  Description     : This function will replace the special characters
 *  Input           : pu1Str- String which has to parsed and modified     
 *  Output          : NONE                                              
 *  Returns         : NONE                 
 ************************************************************************/

VOID
issDecodeSpecialChar (UINT1 *pu1Str)
{
    INT4                i4NewIndex, i4ExistIndex;

    for (i4NewIndex = 0, i4ExistIndex = 0; pu1Str[i4ExistIndex];
         ++i4NewIndex, ++i4ExistIndex)
    {
        if ((pu1Str[i4NewIndex] = pu1Str[i4ExistIndex]) == '%')
        {
            pu1Str[i4NewIndex] =
                (UINT1) UtlHex2Char (pu1Str + i4ExistIndex + 1);
            i4ExistIndex += 2;
        }
        else if (pu1Str[i4NewIndex] == '+')
        {
            pu1Str[i4NewIndex] = ' ';
        }
        else
            pu1Str[i4NewIndex] = pu1Str[i4ExistIndex];
    }
    pu1Str[i4NewIndex] = '\0';
}

/************************************************************************
 *  Function Name   : issDecodeMacAddr
 *  Description     : This function will replace the special characters
 *  Input           : pu1Str- String which has to parsed and modified     
 *  Output          : NONE                                              
 *  Returns         : NONE                 
 ************************************************************************/

VOID
issDecodeMacAddr (UINT1 *pu1Str)
{
    INT4                i4NewIndex, i4ExistIndex;

    for (i4NewIndex = 0, i4ExistIndex = 0; pu1Str[i4ExistIndex];
         ++i4NewIndex, ++i4ExistIndex)
    {
        if ((pu1Str[i4NewIndex] = pu1Str[i4ExistIndex]) == '%')
        {
            pu1Str[i4NewIndex] =
                (UINT1) UtlHex2Char (pu1Str + i4ExistIndex + 1);
            i4ExistIndex += 2;
        }
        else if (pu1Str[i4NewIndex] == '+')
        {
            break;
        }
        else
            pu1Str[i4NewIndex] = pu1Str[i4ExistIndex];
    }
    pu1Str[i4NewIndex] = '\0';
}

/*********************************************************************
 *  Function Name : IssSendCommonError
 *  Description   : Send error page if error occur.
 *  Input(s)      : pi1Error- Error Message.
 *  Output(s)     : None.
 *  Return Values : None.
 *********************************************************************/

VOID
IssSendCommonError (tHttp * pHttp, UINT4 iErrorNum)
{
    tsErrorMsg         *pErrorMsg = NULL;
    UINT1              *pu1ErrStr = NULL;
    UINT1               au1StdErr[] =
        "Unable to process your request, check all inputs";
    UINT1               au1ErrStart[] = "<center>ERROR: ";
    UINT1               au1ErrEnd[] = "<center>";
    INT4                i4Count;
    INT1                i1ErrFound = 0;

    pErrorMsg = asErrorMsg;
    WebnmSockWrite (pHttp, ISS_ERROR_START_STR,
                    (INT4) HTTP_STRLEN (ISS_ERROR_START_STR));

    WebnmSockWrite (pHttp, au1ErrStart, 15);
    for (i4Count = 0; pErrorMsg[i4Count].au1Url[0] != '\0'; i4Count++)
    {
        if (STRCMP (pHttp->ai1HtmlName, pErrorMsg[i4Count].au1Url) == 0)
        {
            /* check whether error message exist */
            if (pErrorMsg[i4Count].sError[iErrorNum].au1errMsg[0] != '\0')
            {
                pu1ErrStr = pErrorMsg[i4Count].sError[iErrorNum].au1errMsg;
                WebnmSockWrite (pHttp, pu1ErrStr,
                                (INT4) HTTP_STRLEN (pu1ErrStr));
                SYS_LOG_CMD ((SYSLOG_DEBUG_LEVEL, SYSLOG_DEF_USER_INDEX,
                              "WEBNM %s %s: %s", pHttp->ai1Username,
                              pHttp->ai1Url, pu1ErrStr));
                i1ErrFound = 1;
            }
            break;
        }
    }
    if (!i1ErrFound)
    {
        WebnmSockWrite (pHttp, au1StdErr, (INT4) HTTP_STRLEN (au1StdErr));
        SYS_LOG_CMD ((SYSLOG_DEBUG_LEVEL, SYSLOG_DEF_USER_INDEX,
                      "WEBNM %s %s: %s", pHttp->ai1Username, pHttp->ai1Url,
                      au1StdErr));
    }
    WebnmSockWrite (pHttp, au1ErrEnd, 8);
    WebnmSockWrite (pHttp, ISS_ERROR_END_STR1,
                    (INT4) HTTP_STRLEN (ISS_ERROR_END_STR1));
    WebnmSockWrite (pHttp, ISS_GAMBIT_STR, (INT4) STRLEN (ISS_GAMBIT_STR));
    WebnmSockWrite (pHttp, ISS_ERROR_BACK_STR,
                    (INT4) STRLEN (ISS_ERROR_BACK_STR));
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", (CHR1 *) pHttp->ai1Url);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    WebnmSockWrite (pHttp, ISS_ERROR_BACK_STR_CONT,
                    (INT4) STRLEN (ISS_ERROR_BACK_STR_CONT));
    WebnmSockWrite (pHttp, ISS_ERROR_END_STR2,
                    (INT4) HTTP_STRLEN (ISS_ERROR_END_STR2));
    return;
}

/************************************************************************
 *  Function Name   : IssConvertDataToString 
 *  Description     : Convert the given multidata to string for webnm to
 *                    write the data to client.
 *  Input           : pData - Pointer to multidata
 *                    u1Type - Type of element in multidata
 *  Output          : pu1String - Pointer to a string where the result will
 *                    written
 *  Returns         : None
 ************************************************************************/

VOID
IssConvertDataToString (tSNMP_MULTI_DATA_TYPE * pData,
                        UINT1 *pu1String, UINT1 u1Type)
{
    UINT4               u4Count = 0, u4Value = 0, u4Temp = 0;
    INT4                i4Count = 0;
    UINT1               au1Temp[200];
    UINT4               u4TempVal = 0;
    UINT1              *pu1OctetBits = NULL;
    tUtlIn6Addr         utlIpv6Tmp;

    pu1String[0] = '\0';
    switch (u1Type)
    {
        case ENM_HEX32:
            SPRINTF ((CHR1 *) pu1String, "0x%x", pData->i4_SLongValue);
            break;
        case ENM_INTEGER32:
            SPRINTF ((CHR1 *) pu1String, "%d", pData->i4_SLongValue);
            break;
        case ENM_UINTEGER32:
        case ENM_TIMETICKS:
        case ENM_GAUGE:
        case ENM_COUNTER32:

            SPRINTF ((CHR1 *) pu1String, "%u", pData->u4_ULongValue);
            break;
        case ENM_COUNTER64:
            SPRINTF ((CHR1 *) pu1String, "%u", pData->u8_Counter64Value.msn);
            SPRINTF ((CHR1 *) au1Temp, "%u", pData->u8_Counter64Value.lsn);
            HTTP_STRNCAT (pu1String, au1Temp, sizeof (au1Temp));
            break;
        case ENM_IPADDRESS:
            u4Value = pData->u4_ULongValue;
            for (u4Count = 0; u4Count < (ENM_IPADDRESS_LEN - 1); u4Count++)
            {
                u4Temp = (u4Value & 0xff000000);
                u4Temp = u4Temp >> 24;
                SPRINTF ((CHR1 *) au1Temp, "%u.", u4Temp);
                HTTP_STRNCAT (pu1String, au1Temp, sizeof (au1Temp));
                u4Value = u4Value << ENM_BYTE_LEN;
            }
            u4Temp = (u4Value & 0xff000000);
            u4Temp = u4Temp >> 24;
            SPRINTF ((CHR1 *) au1Temp, "%u", u4Temp);
            HTTP_STRNCAT (pu1String, au1Temp, sizeof (au1Temp));
            break;
        case ENM_OIDTYPE:
        case ENM_IMP_OIDTYPE:
            for (u4Count = 0; u4Count < pData->pOidValue->u4_Length; u4Count++)
            {
                if (u4Count == pData->pOidValue->u4_Length - 1)
                {
                    SPRINTF ((CHR1 *) au1Temp, "%u",
                             pData->pOidValue->pu4_OidList[u4Count]);
                }
                else
                {
                    SPRINTF ((CHR1 *) au1Temp, "%u.",
                             pData->pOidValue->pu4_OidList[u4Count]);
                }
                HTTP_STRNCAT (pu1String, au1Temp, sizeof (au1Temp));
            }
            break;
        case ENM_OCTETSTRING:
            if (pData->pOctetStrValue->i4_Length != 0)
            {
                for (i4Count = 0; i4Count <
                     (pData->pOctetStrValue->i4_Length - 1); i4Count++)
                {
                    SPRINTF ((CHR1 *) au1Temp, "%.2x:",
                             pData->pOctetStrValue->pu1_OctetList[i4Count]);
                    HTTP_STRNCAT (pu1String, au1Temp, sizeof (au1Temp));
                }
                SPRINTF ((CHR1 *) au1Temp, "%.2x",
                         pData->pOctetStrValue->pu1_OctetList[i4Count]);
                HTTP_STRNCAT (pu1String, au1Temp, sizeof (au1Temp));
            }
            break;
        case ENM_DISPLAYSTRING:
            for (u4Count = 0; u4Count <
                 (UINT4) pData->pOctetStrValue->i4_Length; u4Count++)
            {
                SPRINTF ((CHR1 *) au1Temp, "%c",
                         pData->pOctetStrValue->pu1_OctetList[u4Count]);
                HTTP_STRNCAT (pu1String, au1Temp, sizeof (au1Temp));
            }
            break;
        case ENM_IMP_DISPLAYSTRING:
            for (u4Count = 0; u4Count <
                 (UINT4) pData->pOctetStrValue->i4_Length; u4Count++)
            {
                SPRINTF ((CHR1 *) au1Temp, "%c",
                         pData->pOctetStrValue->pu1_OctetList[u4Count]);
                HTTP_STRNCAT (pu1String, au1Temp, sizeof (au1Temp));
            }
            break;
        case ENM_STR_IPADDRESS:
            MEMCPY (&u4TempVal, pData->pOctetStrValue->pu1_OctetList, 4);
            MEMCPY (pData->pOctetStrValue->pu1_OctetList, &u4TempVal, 4);
            for (u4Count = 0; u4Count <
                 (UINT4) (pData->pOctetStrValue->i4_Length - 1); u4Count++)
            {
                SPRINTF ((char *) au1Temp, "%u.",
                         pData->pOctetStrValue->pu1_OctetList[u4Count]);
                HTTP_STRNCAT (pu1String, au1Temp, sizeof (au1Temp));
            }
            SPRINTF ((char *) au1Temp, "%u",
                     pData->pOctetStrValue->pu1_OctetList[u4Count]);
            HTTP_STRNCAT (pu1String, au1Temp, sizeof (au1Temp));
            break;

        case ENM_BITS:
            u4Value =
                pData->pOctetStrValue->pu1_OctetList[pData->pOctetStrValue->
                                                     i4_Length - 1];
            u4Temp = 0x100;
            for (u4Count = (UINT4) (pData->pOctetStrValue->i4_Length - 1);
                 u4Count > 0; u4Count--)
            {
                u4Value +=
                    (pData->pOctetStrValue->pu1_OctetList[u4Count] * u4Temp);
                u4Temp = u4Temp << 8;
            }
            SPRINTF ((CHR1 *) pu1String, "%u", u4Value);
            break;

        case ENM_OCTETBITS:
            if (pData->pOctetStrValue->i4_Length == IPVX_IPV6_ADDR_LEN)
            {
                /* ipv6 flow */
                MEMCPY (utlIpv6Tmp.u1addr,
                        (pData->pOctetStrValue->pu1_OctetList),
                        sizeof (utlIpv6Tmp.u1addr));
                pu1OctetBits = (UINT1 *) UtlInetNtoa6 (utlIpv6Tmp);
                MEMCPY (pu1String, pu1OctetBits, STRLEN (pu1OctetBits));
            }
            else
            {
                /* ipv4 flow */
                MEMCPY (&u4TempVal, (pData->pOctetStrValue->pu1_OctetList), 4);
                WEB_INET_NTOHL (pData->pOctetStrValue->pu1_OctetList);
                u4TempVal = OSIX_NTOHL (u4TempVal);
                WEB_CONVERT_IPADDR_TO_STR (pu1OctetBits, u4TempVal);
                MEMCPY (pu1String, pu1OctetBits, STRLEN (pu1OctetBits));
            }
            break;
        default:
            break;
    }
    return;
}

/************************************************************************
 *  Function Name   : IssConvertStringToData 
 *  Description     : Convert a data in string format to multidata.
 *  Input           : pu1String - String contain data in string format.
 *                    u1Type - Type of the data 
 *  Output          : None
 *  Returns         : pointer to a multidata or null
 ************************************************************************/

tSNMP_MULTI_DATA_TYPE *
IssConvertStringToData (UINT1 *pu1String, UINT1 u1Type)
{
    tSNMP_MULTI_DATA_TYPE *pData = NULL;
    UINT4               u4TempVal = 0;
    UINT4               u4IPv4Flag = OSIX_FALSE;
    tUtlIn6Addr         utlIpv6Tmp;
    if ((pData = WebnmAllocMultiData ()) == NULL)
    {
        return NULL;
    }
    switch (u1Type)
    {
        case ENM_INTEGER32:
            pData->i4_SLongValue = ATOI (pu1String);
            pData->i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            break;
        case ENM_UINTEGER32:
            pData->i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pData->u4_ULongValue = (UINT4) ATOI (pu1String);
            break;
        case ENM_GAUGE:
            pData->u4_ULongValue = (UINT4) ATOI (pu1String);
            pData->i2_DataType = SNMP_DATA_TYPE_GAUGE;
            break;
        case ENM_COUNTER32:
            pData->u4_ULongValue = (UINT4) ATOI (pu1String);
            pData->i2_DataType = SNMP_DATA_TYPE_COUNTER32;
            break;
        case ENM_IPADDRESS:
            WebnmConvertStringToOctet (pData->pOctetStrValue, pu1String);
            MEMCPY (&pData->u4_ULongValue,
                    pData->pOctetStrValue->pu1_OctetList, 4);
            pData->u4_ULongValue = OSIX_NTOHL (pData->u4_ULongValue);
            pData->i2_DataType = SNMP_DATA_TYPE_IP_ADDR_PRIM;
            break;
        case ENM_OIDTYPE:
        case ENM_IMP_OIDTYPE:
            WebnmConvertStringToOid (pData->pOidValue, pu1String, ENM_DEC_DATA);
            pData->i2_DataType = SNMP_DATA_TYPE_OBJECT_ID;
            break;
        case ENM_OCTETSTRING:
            WebnmConvertColonStringToOctet (pData->pOctetStrValue, pu1String);
            pData->i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            break;
        case ENM_DISPLAYSTRING:
            HTTP_STRCPY (pData->pOctetStrValue->pu1_OctetList, pu1String);
            pData->pOctetStrValue->i4_Length = (INT4) HTTP_STRLEN (pu1String);
            pData->i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            break;
        case ENM_IMP_DISPLAYSTRING:
            HTTP_STRCPY (pData->pOctetStrValue->pu1_OctetList, pu1String);
            pData->pOctetStrValue->i4_Length = (INT4) HTTP_STRLEN (pu1String);
            pData->i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            break;
        case ENM_STR_IPADDRESS:
            WebnmConvertStringToOctet (pData->pOctetStrValue, pu1String);
            MEMCPY (&u4TempVal, pData->pOctetStrValue->pu1_OctetList, 4);
            MEMCPY (pData->pOctetStrValue->pu1_OctetList, &u4TempVal, 4);
            pData->pOctetStrValue->i4_Length = 4;
            pData->i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            break;
        case ENM_BITS:
            WebnmConvertBitsStringToOctet (pData->pOctetStrValue,
                                           ATOI (pu1String));
            pData->i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            break;
        case ENM_TIMETICKS:
            pData->u4_ULongValue = (UINT4) ATOI (pu1String);
            pData->i2_DataType = SNMP_DATA_TYPE_TIME_TICKS;
            break;
        case ENM_OCTETBITS:
            if (STRCHR ((char *) pu1String, ':') == NULL)
            {
                u4IPv4Flag = OSIX_TRUE;
            }

            if (u4IPv4Flag == OSIX_TRUE)
            {
                /* ipv4 flow */
                WebnmConvertStringToOctet (pData->pOctetStrValue, pu1String);
                u4TempVal = INET_ADDR (pu1String);
                MEMCPY (pData->pOctetStrValue->pu1_OctetList, &u4TempVal, 4);
                pData->pOctetStrValue->i4_Length = 4;
                pData->i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            }
            else
            {
                /* ipv6 flow */
                UtlInetAton6 ((char *) pu1String, &utlIpv6Tmp);
                MEMCPY (pData->pOctetStrValue->pu1_OctetList,
                        &utlIpv6Tmp.u1addr, IPVX_IPV6_ADDR_LEN);
                pData->pOctetStrValue->i4_Length = IPVX_IPV6_ADDR_LEN;
            }
            pData->i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            break;
        default:
            return NULL;
    }
    return pData;
}

/************************************************************************
 *  Function Name   : IssConvertToLocalPortList
 *  Description     : Convert the global Port List to Local Port List.
 *  Input           : pHttp - Pointer Http Data Structure.
 *                    tPortList- The Interface PortList.
 *  Output          : LocalPortList - The Local PortList.
 *  Returns         : pointer to a multidata or null
 ************************************************************************/
INT4
IssConvertToLocalPortList (tHttp * pHttp, tPortList IfPortList,
                           tLocalPortList LocalPortList)
{
    UINT4               u4ContextId;
    UINT4               u4Port;
    UINT2               u2LocalPortId;
    UINT2               u2ByteIndex;
    UINT2               u2BitIndex;
    UINT1               u1PortFlag;
    UINT4               u4CId;
    for (u2ByteIndex = 0; u2ByteIndex < BRG_PORT_LIST_SIZE; u2ByteIndex++)
    {
        u1PortFlag = IfPortList[u2ByteIndex];
        for (u2BitIndex = 0; ((u2BitIndex < VLAN_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {

            if ((u1PortFlag & VLAN_BIT8) != 0)
            {
                u4Port = (UINT4) ((u2ByteIndex * VLAN_PORTS_PER_BYTE) +
                                  u2BitIndex + 1);
                if (VcmGetContextInfoFromIfIndex (u4Port,
                                                  &u4ContextId,
                                                  &u2LocalPortId) !=
                    VLAN_SUCCESS)
                {
                    return OSIX_FAILURE;
                }
                if (L2IwfIsPortInPortChannel (u4Port) == L2IWF_SUCCESS)
                {
                    return OSIX_FAILURE;
                }
                if (WebnmApiGetContextId (pHttp, &u4CId) != ENM_SUCCESS)
                {
                    return ENM_FAILURE;
                }
                if (u4ContextId != u4CId)
                {
                    return OSIX_FAILURE;
                }
                OSIX_BITLIST_SET_BIT (LocalPortList, u2LocalPortId,
                                      CONTEXT_PORT_LIST_SIZE);
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }
    return VLAN_SUCCESS;
}

/************************************************************************
 *  Function Name   : IssConvertToIfPortList
 *  Description     : Convert theLocal Port List to global Port List.
 *  Input           : pHttp - Pointer Http Data Structure.
 *                    LocalPortList - The Local PortList.
 *  Output          : IfPortList - Interface PortList 
 *  Returns         : pointer to a multidata or null
 ************************************************************************/
VOID
IssConvertToIfPortList (tHttp * pHttp, tSNMP_OCTET_STRING_TYPE * LocalPortList,
                        tPortList IfPortList)
{
    UINT2               u2Port;
    UINT2               u2BytIndex;
    UINT2               u2BitIndex;
    UINT1               u1PortFlag;
    UINT4               u4CId;
    UINT4               u4IfIndex;

    for (u2BytIndex = 0; u2BytIndex < VLAN_PORT_LIST_SIZE; u2BytIndex++)
    {
        u1PortFlag = LocalPortList->pu1_OctetList[u2BytIndex];

        for (u2BitIndex = 0; ((u2BitIndex < VLAN_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & VLAN_BIT8) != 0)
            {
                u2Port = (UINT2) ((u2BytIndex * VLAN_PORTS_PER_BYTE) +
                                  u2BitIndex + 1);

                if (WebnmApiGetContextId (pHttp, &u4CId) != ENM_SUCCESS)
                {
                    return;
                }

                VcmGetIfIndexFromLocalPort (u4CId, u2Port, &u4IfIndex);
                OSIX_BITLIST_SET_BIT (IfPortList, u4IfIndex,
                                      BRG_PORT_LIST_SIZE);
            }

            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }
}

#endif
