/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: bgpweb.c,v 1.29 2017/02/09 14:07:39 siva Exp $
 *
 * Description: Routines for ISS web for BGP Module
 *******************************************************************/

#ifndef _BGP_WEB_C_
#define _BGP_WEB_C_

#ifdef WEBNM_WANTED

#include "webiss.h"
#include "isshttp.h"
#include "msr.h"
#include "cli.h"
#include "webinc.h"
#include "issmacro.h"
#include "snmputil.h"
#include "issnvram.h"
#include "utilcli.h"
#include "fssyslog.h"
#include "fswebnm.h"
#include "rmap.h"
#include "vcm.h"

/*********************************************************************
*  Function Name : IssPrintAvailableBgpContextId
*  Description   : This function sends the list of BGP Context Id
*                  configured in the system.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssPrintAvailableBgpContextId (tHttp * pHttp)
{
    UINT1               au1BgpCxtName[VCM_ALIAS_MAX_LEN];
    INT4                i4OutCome;
    INT4                i4BgpContextId = 0;
    INT4                i4BgpPrevContextId = 0;

    MEMSET (au1BgpCxtName, 0, VCM_ALIAS_MAX_LEN);
    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    i4OutCome = nmhGetFirstIndexFsMIBgpContextTable (&i4BgpContextId);

    if (i4OutCome == SNMP_FAILURE)
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    STRCPY (pHttp->au1KeyString, "<! CONTEXT ID>");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    do
    {
        VcmGetAliasName ((UINT4) i4BgpContextId, au1BgpCxtName);
        SPRINTF ((CHR1 *) pHttp->au1DataString,
                 "<option value = \"%d\" selected>%s \n",
                 i4BgpContextId, au1BgpCxtName);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        i4BgpPrevContextId = i4BgpContextId;
    }
    while (nmhGetNextIndexFsMIBgpContextTable
           (i4BgpPrevContextId, &i4BgpContextId) == SNMP_SUCCESS);

    BgpUnLock ();

    WebnmUnRegisterLock (pHttp);
}

/*********************************************************************
 *  Function Name : IssProcessBgpFilterConfPage
 *  Description   : This function processes the request coming for the
 *                  BGP Filter settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpFilterConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessBgpFilterConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessBgpFilterConfPageSet (pHttp);
    }
}

/*********************************************************************
 *  Function Name : IssProcessBgpFilterConfPageGet
 *  Description   : This function processes the GET request coming for the
 *                  BGP Filter settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpFilterConfPageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    UINT4               u4IpAddress = 0;
    INT4                i4RetVal = 0;
    INT4                i4OutCome = SNMP_FAILURE;
    INT4                i4FilterIndex = 0;
    INT4                i4IpAfi = 0;
    INT4                i4CxtId = 0;
    UINT1               au1Ip6Addr[ISS_BGP4_IP6_ADDR_STRING_LEN];
    UINT1              *pu1TempString = NULL;
    tSNMP_OCTET_STRING_TYPE IpAddress;
    tSNMP_OCTET_STRING_TYPE *InterAs;
    tSNMP_OCTET_STRING_TYPE CxtName;
    UINT1               au1FilterIpAddress[ISS_BGP4_IP_MAX_LENGTH];
    UINT1               au1BgpCxtName[VCM_ALIAS_MAX_LEN];

    MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
    MEMSET (au1FilterIpAddress, 0, ISS_BGP4_IP_MAX_LENGTH);
    CxtName.pu1_OctetList = au1BgpCxtName;
    CxtName.i4_Length = 0;

    pHttp->i4Write = 0;
    IssPrintAvailableBgpContextId (pHttp);

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    IpAddress.pu1_OctetList = au1FilterIpAddress;

    InterAs = (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring
        (ISS_BGP4_INTER_AS_LENGTH);

    if (InterAs == NULL)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\rInsufficient memory\r\n");
        return;
    }
    MEMSET (InterAs->pu1_OctetList, 0, ISS_BGP4_INTER_AS_LENGTH);
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    /*Process the Filter table */
    i4OutCome = nmhGetFirstIndexFsbgp4MpeUpdateFilterTable (&i4FilterIndex);
    if (i4OutCome == SNMP_FAILURE)
    {
        free_octetstring (InterAs);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }
    do
    {
        pHttp->i4Write = (INT4) u4Temp;
        STRCPY (pHttp->au1KeyString, "fsbgp4UpdateFilterIndex_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4FilterIndex);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeUpdateFilterRemoteAS (i4FilterIndex,
                                             (UINT4 *) &i4RetVal);
        STRCPY (pHttp->au1KeyString, "fsbgp4UpdateFilterRemoteAS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", (UINT4) i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeUpdateFilterIPAddrAfi (i4FilterIndex, &i4IpAfi);
        STRCPY (pHttp->au1KeyString, "fsbgp4UpdateFilterIPAddrAfi_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4IpAfi);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeUpdateFilterIPAddrSafi (i4FilterIndex, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "fsbgp4UpdateFilterIPAddrSafi_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeUpdateFilterIPAddrPrefix (i4FilterIndex, &IpAddress);
        STRCPY (pHttp->au1KeyString, "fsbgp4UpdateFilterIPAddrPrefix_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4IpAfi == BGP4_INET_AFI_IPV4)
        {
            MEMCPY (&u4IpAddress, IpAddress.pu1_OctetList, IpAddress.i4_Length);
            WEB_CONVERT_IPADDR_TO_STR (pu1TempString, OSIX_NTOHL (u4IpAddress));
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1TempString);
        }
#ifdef BGP4_IPV6_WANTED
        else
        {
            Bgp4PrintIp6Addr (IpAddress.pu1_OctetList, (au1Ip6Addr));
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Ip6Addr);
            MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
        }
#endif /* BGP4_IPV6_WANTED */
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeUpdateFilterIPAddrPrefixLen (i4FilterIndex, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "fsbgp4UpdateFilterIPAddrPrefixLen_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeUpdateFilterIntermediateAS (i4FilterIndex, InterAs);
        STRCPY (pHttp->au1KeyString, "fsbgp4UpdateFilterIntermediateAS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", InterAs->pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        MEMSET (InterAs->pu1_OctetList, 0, ISS_BGP4_INTER_AS_LENGTH);

        nmhGetFsbgp4mpeUpdateFilterDirection (i4FilterIndex, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "fsbgp4UpdateFilterDirection_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeUpdateFilterAction (i4FilterIndex, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "fsbgp4UpdateFilterAction_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeUpdateFilterAdminStatus (i4FilterIndex, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "fsbgp4UpdateFilterAdminStatus_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        MEMSET (au1BgpCxtName, 0, VCM_ALIAS_MAX_LEN);
        nmhGetFsMIBgp4mpeUpdateFilterVrfName (i4FilterIndex, &CxtName);
        STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString,
                  (UINT4) (CxtName.i4_Length + 1), "%s", CxtName.pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        VcmIsVrfExist (CxtName.pu1_OctetList, (UINT4 *) &i4CxtId);
        STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_DUMMY_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4CxtId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    }
    while (nmhGetNextIndexFsbgp4MpeUpdateFilterTable (i4FilterIndex, &i4FilterIndex) != SNMP_FAILURE);    /*end of do-while */

    free_octetstring (InterAs);
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
 *  Function Name : IssProcessBgpFilterConfPageSet
 *  Description   : This function processes the SET request coming for the
 *                  BGP Filter settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpFilterConfPageSet (tHttp * pHttp)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4CxtId = 0;
    INT4                i4FilterIndex = 0;
    UINT4               u4RemoteAs = 0;
    INT4                i4IpAfi = 0;
    INT4                i4IpSafi = 0;
    INT4                i4Action = 0;
    INT4                i4Direction = 0;
    INT4                i4PrefixLen = 0;
    INT4                i4Status = 0;
    INT4                i4SupportStatus;
    INT1                i1RetVal = SNMP_FAILURE;
    tNetAddress         NetAddress;
    tUtlInAddr          InAddr;
    tSNMP_OCTET_STRING_TYPE IpAddress;
    tSNMP_OCTET_STRING_TYPE *pCxtName = NULL;
    UINT1               au1FilterIpAddress[ISS_BGP4_IP_MAX_LENGTH];

    MEMSET (&NetAddress, 0, sizeof (tNetAddress));
    MEMSET (au1FilterIpAddress, 0, ISS_BGP4_IP_MAX_LENGTH);

    pCxtName =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (VCM_ALIAS_MAX_LEN);
    if (pCxtName == NULL)
    {
        return;
    }

    MEMSET (pCxtName->pu1_OctetList, 0, VCM_ALIAS_MAX_LEN);

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    STRCPY (pHttp->au1Name, "CONTEXT_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4CxtId = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Apply") == 0)
    {
        STRCPY (pHttp->au1Name, "BGP_CONTEXT_DUMMY");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4CxtId = ATOI (pHttp->au1Value);
    }

    if (BgpSetContext (i4CxtId) == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
        free_octetstring (pCxtName);
        return;
    }

    VcmGetAliasName ((UINT4) i4CxtId, pCxtName->pu1_OctetList);
    pCxtName->i4_Length = (INT4) STRLEN (pCxtName->pu1_OctetList);

    STRCPY (pHttp->au1Name, "fsbgp4UpdateFilterIndex");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4FilterIndex = (INT4) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4UpdateFilterAdminStatus");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Status = ATOI (pHttp->au1Value);

    HttpGetValuebyName (ISS_ACTION, pHttp->au1Value, pHttp->au1PostQuery);
    /* INPUT VALIDATION */
    i1RetVal =
        nmhValidateIndexInstanceFsbgp4MpeUpdateFilterTable (i4FilterIndex);
    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "\r Invalid Filter Table Index\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (pCxtName);
        return;
    }

    i1RetVal = nmhSetFsbgp4mpeUpdateFilterAdminStatus (i4FilterIndex, i4Status);
    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "\r Unable to set the admin status down\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (pCxtName);
        return;
    }

    if (i4Status == BGP4_ADMIN_UP)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssProcessBgpFilterConfPageGet (pHttp);
        free_octetstring (pCxtName);
        return;
    }

    STRCPY (pHttp->au1Name, "fsbgp4UpdateFilterRemoteAS");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4RemoteAs = (UINT4) ATOI (pHttp->au1Value);

    nmhGetFsbgp4FourByteASNSupportStatus (&i4SupportStatus);
    if ((i4SupportStatus == BGP4_DISABLE_4BYTE_ASN_SUPPORT) &&
        (u4RemoteAs > BGP4_MAX_TWO_BYTE_AS))
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "4-byte ASN Capability is disabled. Configure value < 65536");
        IssProcessBgpContextCreationPageGet (pHttp);
        free_octetstring (pCxtName);
        return;
    }

    STRCPY (pHttp->au1Name, "fsbgp4UpdateFilterIPAddrAfi");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4IpAfi = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4UpdateFilterIPAddrSafi");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4IpSafi = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4UpdateFilterIPAddrPrefixLen");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PrefixLen = ATOI (pHttp->au1Value);

    /* Address processing IPv4 & IPV6 */
    STRCPY (pHttp->au1Name, "fsbgp4UpdateFilterIPAddrPrefix");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);

    if (i4IpAfi == BGP4_INET_AFI_IPV4)
    {
        NetAddress.NetAddr.u2Afi = BGP4_INET_AFI_IPV4;
        NetAddress.u2Safi = BGP4_INET_SAFI_UNICAST;
        if (0 == BGP4_INET_ATON (pHttp->au1Value, &InAddr))
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessBgpFilterConfPageGet (pHttp);
            free_octetstring (pCxtName);
            return;
        }
        MEMCPY ((NetAddress.NetAddr.au1Address),
                &InAddr.u4Addr, sizeof (UINT4));
        NetAddress.NetAddr.u2AddressLen = sizeof (UINT4);
        NetAddress.u2PrefixLen = (UINT2) i4PrefixLen;

    }
#ifdef BGP4_IPV6_WANTED
    else if (str_to_ip6addr (pHttp->au1Value) != NULL)
    {
        NetAddress.NetAddr.u2Afi = BGP4_INET_AFI_IPV6;
        NetAddress.u2Safi = BGP4_INET_SAFI_UNICAST;
        MEMCPY (NetAddress.NetAddr.au1Address,
                str_to_ip6addr (pHttp->au1Value), BGP4_IPV6_PREFIX_LEN);

        NetAddress.NetAddr.u2AddressLen = BGP4_IPV6_PREFIX_LEN;

        NetAddress.u2PrefixLen = (UINT2) i4PrefixLen;

    }
#endif /* BGP4_IPV6_WANTED */

    IpAddress.i4_Length = (UINT4) NetAddress.NetAddr.u2AddressLen;
    IpAddress.pu1_OctetList = au1FilterIpAddress;
    MEMCPY ((IpAddress.pu1_OctetList),
            (NetAddress.NetAddr.au1Address), NetAddress.NetAddr.u2AddressLen);

    STRCPY (pHttp->au1Name, "fsbgp4UpdateFilterDirection");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Direction = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4UpdateFilterAction");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Action = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4UpdateFilterIntermediateAS");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);
    i1RetVal = Bgp4Testv2Fsbgp4UpdateFilterIntermediateAS (&u4ErrorCode,
                                                           i4FilterIndex,
                                                           pHttp->au1Value,
                                                           (INT4) STRLEN
                                                           (pHttp->au1Value));
    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the Intermediate AS\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (pCxtName);
        return;
    }

    i1RetVal = nmhTestv2Fsbgp4mpeUpdateFilterAction (&u4ErrorCode,
                                                     i4FilterIndex, i4Action);
    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the Filter Action.\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (pCxtName);
        return;

    }

    i1RetVal = nmhTestv2Fsbgp4mpeUpdateFilterIPAddrAfi (&u4ErrorCode,
                                                        i4FilterIndex, i4IpAfi);
    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the AFI\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (pCxtName);
        return;
    }

    i1RetVal = nmhSetFsbgp4mpeUpdateFilterIPAddrAfi (i4FilterIndex, i4IpAfi);

    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the AFI\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (pCxtName);
        return;
    }

    i1RetVal = nmhTestv2Fsbgp4mpeUpdateFilterIPAddrSafi (&u4ErrorCode,
                                                         i4FilterIndex,
                                                         i4IpSafi);
    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the SAFI\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (pCxtName);
        return;
    }

    i1RetVal = nmhTestv2Fsbgp4mpeUpdateFilterIPAddrPrefix (&u4ErrorCode,
                                                           i4FilterIndex,
                                                           &IpAddress);
    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the Prifix\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (pCxtName);
        return;
    }
    i1RetVal = nmhTestv2Fsbgp4mpeUpdateFilterIPAddrPrefixLen (&u4ErrorCode,
                                                              i4FilterIndex,
                                                              i4PrefixLen);
    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the Prifix Length\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (pCxtName);
        return;

    }

    i1RetVal = nmhTestv2Fsbgp4mpeUpdateFilterRemoteAS (&u4ErrorCode,
                                                       i4FilterIndex,
                                                       u4RemoteAs);
    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the Remote AS\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (pCxtName);
        return;

    }

    i1RetVal = nmhTestv2Fsbgp4mpeUpdateFilterDirection (&u4ErrorCode,
                                                        i4FilterIndex,
                                                        i4Direction);
    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the direction\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (pCxtName);
        return;

    }
    i1RetVal =
        nmhTestv2FsMIBgp4mpeUpdateFilterVrfName (&u4ErrorCode, i4FilterIndex,
                                                 pCxtName);

    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid to Vrf Name.\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (pCxtName);
        return;

    }
    i1RetVal = nmhSetFsbgp4mpeUpdateFilterIPAddrSafi (i4FilterIndex, i4IpSafi);
    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the SAFI\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (pCxtName);
        return;
    }

    /* INPUT PROCESSING */
    i1RetVal = nmhSetFsbgp4mpeUpdateFilterAction (i4FilterIndex, i4Action);
    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the Filter Action\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (pCxtName);
        return;

    }

    i1RetVal =
        nmhSetFsbgp4mpeUpdateFilterIPAddrPrefix (i4FilterIndex, &IpAddress);
    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the Ip Address\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (pCxtName);
        return;

    }

    i1RetVal = nmhSetFsbgp4mpeUpdateFilterIPAddrPrefixLen (i4FilterIndex,
                                                           i4PrefixLen);
    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the Prefix Length\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (pCxtName);
        return;

    }

    i1RetVal = nmhSetFsbgp4mpeUpdateFilterRemoteAS (i4FilterIndex, u4RemoteAs);
    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the Remote As\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (pCxtName);
        return;

    }

    i1RetVal =
        nmhSetFsbgp4mpeUpdateFilterDirection (i4FilterIndex, i4Direction);
    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the Direction\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (pCxtName);
        return;

    }

    i1RetVal = Bgp4SetFsbgp4UpdateFilterIntermediateAS (i4FilterIndex,
                                                        pHttp->au1Value);

    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the Intermediate AS\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (pCxtName);
        return;
    }

    i1RetVal = nmhSetFsMIBgp4mpeUpdateFilterVrfName (i4FilterIndex, pCxtName);
    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the Vrf Name\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (pCxtName);
        return;
    }

    BgpResetContext ();
    free_octetstring (pCxtName);
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessBgpFilterConfPageGet (pHttp);
    return;
}

/*********************************************************************
 *  Function Name : IssProcessBgpMedConfPage
 *  Description   : This function processes the request coming for the
 *                  BGP MED settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpMedConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessBgpMedConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessBgpMedConfPageSet (pHttp);
    }
}

/*********************************************************************
 *  Function Name : IssProcessBgpMedConfPageGet
 *  Description   : This function processes the GET request coming for the
 *                  BGP MED settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpMedConfPageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    UINT4               u4IpAddress = 0;
    INT4                i4RetVal = 0;
    INT4                i4MEDIndex = 0;
    INT4                i4OutCome = SNMP_FAILURE;
    INT4                i4IpAfi = 0;
    INT4                i4CxtId = 0;
    UINT1               au1Ip6Addr[ISS_BGP4_IP6_ADDR_STRING_LEN];
    UINT1              *pu1TempString = NULL;
    tSNMP_OCTET_STRING_TYPE IpAddress;
    tSNMP_OCTET_STRING_TYPE *InterAs;
    tSNMP_OCTET_STRING_TYPE CxtName;
    UINT1               au1MedIpAddress[ISS_BGP4_IP_MAX_LENGTH];
    UINT1               au1BgpCxtName[VCM_ALIAS_MAX_LEN];

    MEMSET (au1MedIpAddress, 0, ISS_BGP4_IP_MAX_LENGTH);
    MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));

    CxtName.pu1_OctetList = au1BgpCxtName;
    CxtName.i4_Length = 0;

    pHttp->i4Write = 0;
    IssPrintAvailableBgpContextId (pHttp);

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    IpAddress.pu1_OctetList = au1MedIpAddress;

    if (IpAddress.pu1_OctetList == NULL)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\rInsufficient memory\r\n");
        return;
    }

    InterAs = (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring
        (ISS_BGP4_INTER_AS_LENGTH);

    if (InterAs == NULL)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\rInsufficient memory\r\n");
        return;
    }
    MEMSET (InterAs->pu1_OctetList, 0, ISS_BGP4_INTER_AS_LENGTH);
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    /*Process the MED table */
    i4OutCome = nmhGetFirstIndexFsbgp4MpeMEDTable (&i4MEDIndex);

    if (i4OutCome == SNMP_FAILURE)
    {
        free_octetstring (InterAs);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }
    do
    {
        pHttp->i4Write = (INT4) u4Temp;
        STRCPY (pHttp->au1KeyString, "fsbgp4MEDIndex_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4MEDIndex);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeMEDRemoteAS (i4MEDIndex, (UINT4 *) &i4RetVal);
        STRCPY (pHttp->au1KeyString, "fsbgp4MEDRemoteAS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", (UINT4) i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeMEDIPAddrAfi (i4MEDIndex, &i4IpAfi);
        STRCPY (pHttp->au1KeyString, "fsbgp4MEDIPAddrAfi_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4IpAfi);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeMEDIPAddrSafi (i4MEDIndex, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "fsbgp4MEDIPAddrSafi_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeMEDIPAddrPrefix (i4MEDIndex, &IpAddress);
        STRCPY (pHttp->au1KeyString, "fsbgp4MEDIPAddrPrefix_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4IpAfi == BGP4_INET_AFI_IPV4)
        {
            MEMCPY (&u4IpAddress, IpAddress.pu1_OctetList, IpAddress.i4_Length);
            WEB_CONVERT_IPADDR_TO_STR (pu1TempString, OSIX_NTOHL (u4IpAddress));
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1TempString);
        }
#ifdef BGP4_IPV6_WANTED
        else
        {
            Bgp4PrintIp6Addr (IpAddress.pu1_OctetList, (au1Ip6Addr));
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Ip6Addr);
            MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
        }
#endif /* BGP4_IPV6_WANTED */
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeMEDIPAddrPrefixLen (i4MEDIndex, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "fsbgp4MEDIPAddrPrefixLen_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeMEDIntermediateAS (i4MEDIndex, InterAs);
        STRCPY (pHttp->au1KeyString, "fsbgp4MEDIntermediateAS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", InterAs->pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        MEMSET (InterAs->pu1_OctetList, 0, ISS_BGP4_INTER_AS_LENGTH);

        nmhGetFsbgp4mpeMEDDirection (i4MEDIndex, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "fsbgp4MEDDirection_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeMEDValue (i4MEDIndex, (UINT4 *) &i4RetVal);
        STRCPY (pHttp->au1KeyString, "fsbgp4MEDValue_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeMEDPreference (i4MEDIndex, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "fsbgp4MEDPreference_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeMEDAdminStatus (i4MEDIndex, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "fsbgp4MEDAdminStatus_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        MEMSET (au1BgpCxtName, 0, VCM_ALIAS_MAX_LEN);
        nmhGetFsMIBgp4mpeMEDVrfName (i4MEDIndex, &CxtName);
        STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString,
                  (UINT4) (CxtName.i4_Length + 1), "%s", CxtName.pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        VcmIsVrfExist (CxtName.pu1_OctetList, (UINT4 *) &i4CxtId);
        STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_DUMMY_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4CxtId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    }
    while (nmhGetNextIndexFsbgp4MpeMEDTable (i4MEDIndex, &i4MEDIndex) != SNMP_FAILURE);    /*end of do-while */

    free_octetstring (InterAs);
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
 *  Function Name : IssProcessBgpMedConfPageSet
 *  Description   : This function processes the SET request coming for the
 *                  BGP MED settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpMedConfPageSet (tHttp * pHttp)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4CxtId = 0;
    INT4                i4MedIndex = 0;
    UINT4               u4RemoteAs = 0;
    INT4                i4IpAfi = 0;
    INT4                i4IpSafi = 0;
    INT4                i4Direction = 0;
    INT4                i4PrefixLen = 0;
    INT4                i4Status = 0;
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4Preference = 0;
    INT4                i4Value = 0;
    INT4                i4SupportStatus;
    tNetAddress         NetAddress;
    tUtlInAddr          InAddr;
    tSNMP_OCTET_STRING_TYPE IpAddress;
    tSNMP_OCTET_STRING_TYPE *pCxtName = NULL;
    UINT1               au1MedIpAddress[ISS_BGP4_IP_MAX_LENGTH];

    MEMSET (au1MedIpAddress, 0, ISS_BGP4_IP_MAX_LENGTH);
    MEMSET (&NetAddress, 0, sizeof (tNetAddress));

    pCxtName =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (VCM_ALIAS_MAX_LEN);
    if (pCxtName == NULL)
    {
        return;
    }

    MEMSET (pCxtName->pu1_OctetList, 0, VCM_ALIAS_MAX_LEN);

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    STRCPY (pHttp->au1Name, "CONTEXT_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4CxtId = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Apply") == 0)
    {
        STRCPY (pHttp->au1Name, "BGP_CONTEXT_DUMMY");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4CxtId = ATOI (pHttp->au1Value);
    }

    if (BgpSetContext (i4CxtId) == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
        return;
    }

    VcmGetAliasName ((UINT4) i4CxtId, pCxtName->pu1_OctetList);
    pCxtName->i4_Length = (INT4) STRLEN (pCxtName->pu1_OctetList);

    STRCPY (pHttp->au1Name, "fsbgp4MEDIndex");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4MedIndex = (INT4) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4MEDRemoteAS");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4RemoteAs = (UINT4) ATOI (pHttp->au1Value);

    nmhGetFsbgp4FourByteASNSupportStatus (&i4SupportStatus);

    if ((i4SupportStatus == BGP4_DISABLE_4BYTE_ASN_SUPPORT) &&
        (u4RemoteAs > BGP4_MAX_TWO_BYTE_AS))
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "4-byte ASN Capability is disabled. Configure value < 65536");
        IssProcessBgpContextCreationPageGet (pHttp);
        free_octetstring (pCxtName);
        return;
    }
    STRCPY (pHttp->au1Name, "fsbgp4MEDAdminStatus");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Status = ATOI (pHttp->au1Value);

    HttpGetValuebyName (ISS_ACTION, pHttp->au1Value, pHttp->au1PostQuery);
    /* INPUT VALIDATION */
    i1RetVal = nmhValidateIndexInstanceFsbgp4MpeMEDTable (i4MedIndex);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *)
                      "\r Invalid Filter Table Index\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    i1RetVal = nmhSetFsbgp4mpeMEDAdminStatus (i4MedIndex, i4Status);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the Admin status.\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    if (i4Status == BGP4_ADMIN_UP)
    {
        free_octetstring (pCxtName);
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }
    STRCPY (pHttp->au1Name, "fsbgp4MEDIPAddrAfi");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4IpAfi = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4MEDIPAddrSafi");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4IpSafi = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4MEDIPAddrPrefixLen");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PrefixLen = ATOI (pHttp->au1Value);

    /* Address processing IPv4 & IPV6 */
    STRCPY (pHttp->au1Name, "fsbgp4MEDIPAddrPrefix");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);

    if (i4IpAfi == BGP4_INET_AFI_IPV4)
    {
        NetAddress.NetAddr.u2Afi = BGP4_INET_AFI_IPV4;
        NetAddress.u2Safi = BGP4_INET_SAFI_UNICAST;
        if (0 == BGP4_INET_ATON (pHttp->au1Value, &InAddr))
        {
            free_octetstring (pCxtName);
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessBgpMedConfPageGet (pHttp);
            return;
        }
        MEMCPY ((NetAddress.NetAddr.au1Address),
                &InAddr.u4Addr, sizeof (UINT4));
        NetAddress.NetAddr.u2AddressLen = sizeof (UINT4);
        NetAddress.u2PrefixLen = (UINT2) i4PrefixLen;

    }
#ifdef BGP4_IPV6_WANTED
    else if (str_to_ip6addr (pHttp->au1Value) != NULL)
    {
        NetAddress.NetAddr.u2Afi = BGP4_INET_AFI_IPV6;
        NetAddress.u2Safi = BGP4_INET_SAFI_UNICAST;
        MEMCPY (NetAddress.NetAddr.au1Address,
                str_to_ip6addr (pHttp->au1Value), BGP4_IPV6_PREFIX_LEN);

        NetAddress.NetAddr.u2AddressLen = BGP4_IPV6_PREFIX_LEN;

        NetAddress.u2PrefixLen = (UINT2) i4PrefixLen;

    }
#endif /* BGP4_IPV6_WANTED */

    IpAddress.i4_Length = (UINT4) NetAddress.NetAddr.u2AddressLen;
    IpAddress.pu1_OctetList = au1MedIpAddress;
    MEMCPY ((IpAddress.pu1_OctetList),
            (NetAddress.NetAddr.au1Address), NetAddress.NetAddr.u2AddressLen);

    STRCPY (pHttp->au1Name, "fsbgp4MEDDirection");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Direction = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4MEDValue");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Value = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4MEDPreference");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Preference = ATOI (pHttp->au1Value);

    i1RetVal = nmhTestv2Fsbgp4mpeMEDIPAddrPrefix (&u4ErrorCode,
                                                  i4MedIndex, &IpAddress);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the Med IpAddress Prefix.\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;

    }

    i1RetVal =
        nmhTestv2Fsbgp4mpeMEDIPAddrAfi (&u4ErrorCode, i4MedIndex, i4IpAfi);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the Med IP Address AFI.\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    i1RetVal = nmhSetFsbgp4mpeMEDIPAddrAfi (i4MedIndex, i4IpAfi);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the Med Entry.\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    i1RetVal = nmhTestv2Fsbgp4mpeMEDIPAddrSafi (&u4ErrorCode,
                                                i4MedIndex, i4IpSafi);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the Med Ip Address SAFI.\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    i1RetVal = nmhTestv2Fsbgp4mpeMEDIPAddrPrefixLen (&u4ErrorCode,
                                                     i4MedIndex, i4PrefixLen);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set Med IpAddress Prefix Length.\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;

    }

    i1RetVal = nmhTestv2Fsbgp4mpeMEDRemoteAS (&u4ErrorCode, i4MedIndex,
                                              u4RemoteAs);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the Med Remote As.\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    i1RetVal =
        nmhTestv2Fsbgp4mpeMEDValue (&u4ErrorCode, i4MedIndex, (UINT4) i4Value);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the Med value.\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;

    }

    i1RetVal = nmhTestv2Fsbgp4mpeMEDDirection (&u4ErrorCode, i4MedIndex,
                                               i4Direction);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the Med Direction.\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;

    }
    i1RetVal =
        nmhTestv2FsMIBgp4mpeMEDVrfName (&u4ErrorCode, i4MedIndex, pCxtName);

    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *) "Unable to Vrf Name.\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;

    }

    STRCPY (pHttp->au1Name, "fsbgp4MEDIntermediateAS");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);

    i1RetVal = Bgp4Testv2Fsbgp4MEDIntermediateAS (&u4ErrorCode,
                                                  i4MedIndex,
                                                  pHttp->au1Value,
                                                  (INT4) STRLEN (pHttp->
                                                                 au1Value));
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *)
                      "Failed in testing Med IntermediateAs.\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    Bgp4SetFsbgp4MEDIntermediateAS (i4MedIndex, pHttp->au1Value);

    i1RetVal = nmhSetFsbgp4mpeMEDIPAddrPrefix (i4MedIndex, &IpAddress);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the Ip Address Prefix\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    i1RetVal = nmhSetFsbgp4mpeMEDIPAddrSafi (i4MedIndex, i4IpSafi);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the Med Entry.\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    i1RetVal = nmhSetFsbgp4mpeMEDIPAddrPrefixLen (i4MedIndex, i4PrefixLen);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the Prefix Length.\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;

    }
    i1RetVal = nmhSetFsbgp4mpeMEDRemoteAS (i4MedIndex, u4RemoteAs);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the remote AS \r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    i1RetVal = nmhSetFsbgp4mpeMEDValue (i4MedIndex, (UINT4) i4Value);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the MED Value\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    i1RetVal = nmhSetFsbgp4mpeMEDDirection (i4MedIndex, i4Direction);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the MED Direction\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    i1RetVal = nmhSetFsbgp4mpeMEDPreference (i4MedIndex, i4Preference);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the MED Preference\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }
    i1RetVal = nmhSetFsMIBgp4mpeMEDVrfName (i4MedIndex, pCxtName);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the Vrf Name\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }
    free_octetstring (pCxtName);
    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessBgpMedConfPageGet (pHttp);
    return;
}

/*********************************************************************
 *  Function Name : IssProcessBgpRouteAggrConfPage
 *  Description   : This function processes the request coming for the
 *                  BGP Route Aggregation settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpRouteAggrConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessBgpRouteAggrConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessBgpRouteAggrConfPageSet (pHttp);
    }
}

/*********************************************************************
 *  Function Name : IssProcessBgpRouteAggrConfPageGet
 *  Description   : This function processes the GET request coming for the
 *                  BGP Route Aggregation settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpRouteAggrConfPageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    UINT4               u4IpAddress = 0;
    INT4                i4RetVal = 0;
    INT4                i4OutCome = SNMP_FAILURE;
    INT4                i4RtAggrIndex = 0;
    INT4                i4IpAfi = 0;
    INT4                i4CxtId = 0;
    UINT1               au1Ip6Addr[ISS_BGP4_IP6_ADDR_STRING_LEN];
    UINT1              *pu1TempString = NULL;
    tSNMP_OCTET_STRING_TYPE IpAddress;
    tSNMP_OCTET_STRING_TYPE SuppressMap;
    tSNMP_OCTET_STRING_TYPE AdvertiseMap;
    tSNMP_OCTET_STRING_TYPE AttributeMap;
    tSNMP_OCTET_STRING_TYPE CxtName;
    UINT1               au1AdvMap[RMAP_MAX_NAME_LEN];
    UINT1               au1SuppMap[RMAP_MAX_NAME_LEN];
    UINT1               au1AttMap[RMAP_MAX_NAME_LEN];
    UINT1               au1AggrIpAddress[ISS_BGP4_IP_MAX_LENGTH];
    UINT1               au1BgpCxtName[VCM_ALIAS_MAX_LEN];
    INT4                i4adminstatus = 1;
    INT4                i4retval = 0;
    INT4                i4Status = 0;

    MEMSET (au1AggrIpAddress, 0, ISS_BGP4_IP_MAX_LENGTH);
    MEMSET (au1BgpCxtName, 0, VCM_ALIAS_MAX_LEN);
    AdvertiseMap.pu1_OctetList = au1AdvMap;
    AdvertiseMap.i4_Length = 0;
    SuppressMap.pu1_OctetList = au1SuppMap;
    SuppressMap.i4_Length = 0;
    AttributeMap.pu1_OctetList = au1AttMap;
    AttributeMap.i4_Length = 0;

    IpAddress.pu1_OctetList = au1AggrIpAddress;

    CxtName.pu1_OctetList = au1BgpCxtName;
    CxtName.i4_Length = 0;

    pHttp->i4Write = 0;
    IssPrintAvailableBgpContextId (pHttp);

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    /*Process the Filter table */
    i4OutCome = nmhGetFirstIndexFsbgp4MpeAggregateTable (&i4RtAggrIndex);

    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        /* setting global admin status as up to avoid refreshing the page */
        STRCPY (pHttp->au1KeyString, "fsbgp4AggregateAdminStatus_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4adminstatus);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }
    do
    {
        MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
        MEMSET (au1AdvMap, 0, RMAP_MAX_NAME_LEN);
        MEMSET (au1SuppMap, 0, RMAP_MAX_NAME_LEN);
        MEMSET (au1AttMap, 0, RMAP_MAX_NAME_LEN);

        pHttp->i4Write = (INT4) u4Temp;
        STRCPY (pHttp->au1KeyString, "fsbgp4AggregateIndex_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RtAggrIndex);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeAggregateIPAddrAfi (i4RtAggrIndex, &i4IpAfi);
        STRCPY (pHttp->au1KeyString, "fsbgp4AggregateIPAddrAfi_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4IpAfi);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeAggregateIPAddrSafi (i4RtAggrIndex, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "fsbgp4AggregateIPAddrSafi_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeAggregateIPAddrPrefix (i4RtAggrIndex, &IpAddress);
        STRCPY (pHttp->au1KeyString, "fsbgp4AggregateIPAddrPrefix_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4IpAfi == BGP4_INET_AFI_IPV4)
        {
            MEMCPY (&u4IpAddress, IpAddress.pu1_OctetList, IpAddress.i4_Length);
            WEB_CONVERT_IPADDR_TO_STR (pu1TempString, OSIX_NTOHL (u4IpAddress));
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1TempString);
        }
#ifdef BGP4_IPV6_WANTED
        else
        {
            Bgp4PrintIp6Addr (IpAddress.pu1_OctetList, (au1Ip6Addr));

            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Ip6Addr);
            MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
        }
#endif /* BGP4_IPV6_WANTED */
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeAggregateIPAddrPrefixLen (i4RtAggrIndex, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "fsbgp4AggregateIPAddrPrefixLen_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeAggregateAdvertise (i4RtAggrIndex, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "fsbgp4AggregateAdvertise_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeAggregateAsSet (i4RtAggrIndex, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "fsbgp4mpeAggregateAsSet_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeAggregateSuppressRouteMapName (i4RtAggrIndex,
                                                      &SuppressMap);
        STRCPY (pHttp->au1KeyString,
                "fsbgp4mpeAggregateSuppressRouteMapName_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString,
                  (UINT4) (SuppressMap.i4_Length + 1), "%s",
                  SuppressMap.pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeAggregateAdvertiseRouteMapName (i4RtAggrIndex,
                                                       &AdvertiseMap);
        STRCPY (pHttp->au1KeyString,
                "fsbgp4mpeAggregateAdvertiseRouteMapName_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString,
                  (UINT4) (AdvertiseMap.i4_Length + 1), "%s",
                  AdvertiseMap.pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeAggregateAttributeRouteMapName (i4RtAggrIndex,
                                                       &AttributeMap);
        STRCPY (pHttp->au1KeyString,
                "fsbgp4mpeAggregateAttributeRouteMapName_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString,
                  (UINT4) (AttributeMap.i4_Length + 1), "%s",
                  AttributeMap.pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        i4retval =
            nmhGetFsbgp4mpeAggregateAdminStatus (i4RtAggrIndex, &i4Status);
        if ((i4retval == SNMP_FAILURE) || (i4Status != 1))
        {
            i4adminstatus = 2;
        }
        MEMSET (au1BgpCxtName, 0, VCM_ALIAS_MAX_LEN);

        nmhGetFsMIBgp4mpeAggregateVrfName (i4RtAggrIndex, &CxtName);
        STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString,
                  (UINT4) (CxtName.i4_Length + 1), "%s", CxtName.pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        VcmIsVrfExist (CxtName.pu1_OctetList, (UINT4 *) &i4CxtId);
        STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_DUMMY_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4CxtId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    }
    while (nmhGetNextIndexFsbgp4MpeAggregateTable (i4RtAggrIndex, &i4RtAggrIndex) != SNMP_FAILURE);    /*end of do-while */

    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    STRCPY (pHttp->au1KeyString, "fsbgp4AggregateAdminStatus_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4adminstatus);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
 *  Function Name : IssProcessBgpRouteAggrConfPageSet
 *  Description   : This function processes the SET request coming for the
 *                  BGP Route Aggregation settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpRouteAggrConfPageSet (tHttp * pHttp)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4CxtId = 0;
    INT4                i4Index = 0;
    INT4                i4IpAfi = 0;
    INT4                i4IpSafi = 0;
    INT4                i4Advertise = 0;
    INT4                i4PrefixLen = 0;
    INT4                i4Status = 0;
    INT1                i1RetVal = 0;
    INT4                i4AsSet = 0;
    tNetAddress         NetAddress;
    tUtlInAddr          InAddr;
    tSNMP_OCTET_STRING_TYPE IpAddress;
    tSNMP_OCTET_STRING_TYPE SuppressMap;
    tSNMP_OCTET_STRING_TYPE AdvertiseMap;
    tSNMP_OCTET_STRING_TYPE AttributeMap;
    tSNMP_OCTET_STRING_TYPE *pCxtName = NULL;
    UINT1               au1AdvMap[RMAP_MAX_NAME_LEN + 4];
    UINT1               au1SuppMap[RMAP_MAX_NAME_LEN + 4];
    UINT1               au1AttMap[RMAP_MAX_NAME_LEN + 4];
    UINT1               au1AggrIpAddress[ISS_BGP4_IP_MAX_LENGTH];

    MEMSET (au1AggrIpAddress, 0, ISS_BGP4_IP_MAX_LENGTH);
    MEMSET (au1AdvMap, '\0', (RMAP_MAX_NAME_LEN + 4));
    MEMSET (au1SuppMap, '\0', (RMAP_MAX_NAME_LEN + 4));
    MEMSET (au1AttMap, '\0', (RMAP_MAX_NAME_LEN + 4));
    MEMSET (&NetAddress, 0, sizeof (tNetAddress));

    pCxtName =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (VCM_ALIAS_MAX_LEN);
    if (pCxtName == NULL)
    {
        return;
    }
    MEMSET (pCxtName->pu1_OctetList, 0, VCM_ALIAS_MAX_LEN);

    SuppressMap.pu1_OctetList = au1SuppMap;
    SuppressMap.i4_Length = 0;
    AdvertiseMap.pu1_OctetList = au1AdvMap;
    AdvertiseMap.i4_Length = 0;
    AttributeMap.pu1_OctetList = au1AttMap;
    AttributeMap.i4_Length = 0;

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    STRCPY (pHttp->au1Name, "CONTEXT_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4CxtId = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {
        STRCPY (pHttp->au1Name, "BGP_CONTEXT_DUMMY");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4CxtId = ATOI (pHttp->au1Value);
    }

    if (BgpSetContext (i4CxtId) == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
        return;
    }

    VcmGetAliasName ((UINT4) i4CxtId, pCxtName->pu1_OctetList);
    pCxtName->i4_Length = (INT4) STRLEN (pCxtName->pu1_OctetList);

    STRCPY (pHttp->au1Name, "fsbgp4AggregateIndex");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Index = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4AggregateAdminStatus");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Status = ATOI (pHttp->au1Value);

    /* INPUT VALIDATION */
    i1RetVal = nmhValidateIndexInstanceFsbgp4MpeAggregateTable (i4Index);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid Aggr Table Index\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    STRCPY (pHttp->au1Name, "ACTION");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)

    {
        if (0 == STRCMP (pHttp->au1Value, "Delete"))
        {
            i1RetVal = nmhSetFsbgp4mpeAggregateAdminStatus (i4Index,
                                                            BGP4_ADMIN_INVALID);
            if (i1RetVal == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to delete entry\r\n");
            }
            free_octetstring (pCxtName);
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessBgpRouteAggrConfPageGet (pHttp);
            return;
        }
    }

    STRCPY (pHttp->au1Name, "fsbgp4AggregateIPAddrAfi");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4IpAfi = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4AggregateIPAddrSafi");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4IpSafi = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4AggregateIPAddrPrefixLen");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PrefixLen = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4mpeAggregateAsSet");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4AsSet = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4mpeAggregateSuppressRouteMapName");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        SuppressMap.i4_Length = (INT4) STRLEN (pHttp->au1Value);
        if (SuppressMap.i4_Length >= (RMAP_MAX_NAME_LEN + 4))
        {
            SuppressMap.i4_Length = RMAP_MAX_NAME_LEN + 3;
        }
        MEMCPY (SuppressMap.pu1_OctetList, pHttp->au1Value,
                SuppressMap.i4_Length);
        SuppressMap.pu1_OctetList[SuppressMap.i4_Length] = 0;
    }

    STRCPY (pHttp->au1Name, "fsbgp4mpeAggregateAdvertiseRouteMapName");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        AdvertiseMap.i4_Length = (INT4) STRLEN (pHttp->au1Value);
        if (AdvertiseMap.i4_Length >= (RMAP_MAX_NAME_LEN + 4))
        {
            AdvertiseMap.i4_Length = RMAP_MAX_NAME_LEN + 3;
        }
        MEMCPY (AdvertiseMap.pu1_OctetList, pHttp->au1Value,
                AdvertiseMap.i4_Length);
        AdvertiseMap.pu1_OctetList[AdvertiseMap.i4_Length] = 0;
    }

    STRCPY (pHttp->au1Name, "fsbgp4mpeAggregateAttributeRouteMapName");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        AttributeMap.i4_Length = (INT4) STRLEN (pHttp->au1Value);
        if (AttributeMap.i4_Length >= (RMAP_MAX_NAME_LEN + 4))
        {
            AttributeMap.i4_Length = RMAP_MAX_NAME_LEN + 3;
        }
        MEMCPY (AttributeMap.pu1_OctetList, pHttp->au1Value,
                AttributeMap.i4_Length);
        AttributeMap.pu1_OctetList[AttributeMap.i4_Length] = 0;
    }

    STRCPY (pHttp->au1Name, "fsbgp4AggregateAdvertise");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Advertise = ATOI (pHttp->au1Value);

    /* Address processing IPv4 & IPV6 */
    STRCPY (pHttp->au1Name, "fsbgp4AggregateIPAddrPrefix");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);
    if (i4IpAfi == BGP4_INET_AFI_IPV4)
    {
        NetAddress.NetAddr.u2Afi = BGP4_INET_AFI_IPV4;
        NetAddress.u2Safi = BGP4_INET_SAFI_UNICAST;
        if (0 == BGP4_INET_ATON (pHttp->au1Value, &InAddr))
        {
            free_octetstring (pCxtName);
            IssSendError (pHttp, (CONST INT1 *) "\rInvalid IPv4 address\r\n");
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            return;
        }
        MEMCPY ((NetAddress.NetAddr.au1Address),
                &InAddr.u4Addr, sizeof (UINT4));
        NetAddress.NetAddr.u2AddressLen = sizeof (UINT4);
        NetAddress.u2PrefixLen = (UINT2) i4PrefixLen;

    }
#ifdef BGP4_IPV6_WANTED
    else if (str_to_ip6addr (pHttp->au1Value) != NULL)
    {
        NetAddress.NetAddr.u2Afi = BGP4_INET_AFI_IPV6;
        NetAddress.u2Safi = BGP4_INET_SAFI_UNICAST;
        MEMCPY (NetAddress.NetAddr.au1Address,
                str_to_ip6addr (pHttp->au1Value), BGP4_IPV6_PREFIX_LEN);

        NetAddress.NetAddr.u2AddressLen = BGP4_IPV6_PREFIX_LEN;

        NetAddress.u2PrefixLen = (UINT2) i4PrefixLen;

    }
#endif /* BGP4_IPV6_WANTED */

    /* Aggregate entry is first made admin down to set the other aggregate parameters */
    i1RetVal = nmhSetFsbgp4mpeAggregateAdminStatus (i4Index, BGP4_ADMIN_DOWN);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *)
                      "\rUnable to set the admin status\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    IpAddress.i4_Length = (UINT4) NetAddress.NetAddr.u2AddressLen;
    IpAddress.pu1_OctetList = au1AggrIpAddress;
    MEMCPY ((IpAddress.pu1_OctetList),
            (NetAddress.NetAddr.au1Address), NetAddress.NetAddr.u2AddressLen);

    i1RetVal = nmhTestv2Fsbgp4mpeAggregateIPAddrPrefix (&u4ErrorCode,
                                                        i4Index, &IpAddress);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) " \rInvalid prefix\r\n");
        return;
    }
    i1RetVal = nmhSetFsbgp4mpeAggregateIPAddrAfi (i4Index, i4IpAfi);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *)
                      " \rUnable to set the address family\r\n");
        return;
    }
    i1RetVal = nmhTestv2Fsbgp4mpeAggregateIPAddrPrefixLen (&u4ErrorCode,
                                                           i4Index,
                                                           i4PrefixLen);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) " \rInvalid prefix length\r\n");
        return;
    }

    i1RetVal = nmhTestv2Fsbgp4mpeAggregateAsSet (&u4ErrorCode,
                                                 i4Index, i4AsSet);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) " \rInvalid AS Set\r\n");
        return;
    }

    i1RetVal = nmhTestv2Fsbgp4mpeAggregateSuppressRouteMapName (&u4ErrorCode,
                                                                i4Index,
                                                                &SuppressMap);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) " \rInvalid Suppress Map\r\n");
        return;
    }

    i1RetVal = nmhTestv2Fsbgp4mpeAggregateAdvertiseRouteMapName (&u4ErrorCode,
                                                                 i4Index,
                                                                 &AdvertiseMap);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) " \rInvalid Advertise Map\r\n");
        return;
    }

    i1RetVal = nmhTestv2Fsbgp4mpeAggregateAttributeRouteMapName (&u4ErrorCode,
                                                                 i4Index,
                                                                 &AttributeMap);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) " \rInvalid Attribute Map\r\n");
        return;
    }

    i1RetVal = nmhTestv2Fsbgp4mpeAggregateAdvertise (&u4ErrorCode,
                                                     i4Index, i4Advertise);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);

        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) " \rInvalid Advertise field\r\n");
        return;
    }
    i1RetVal =
        nmhTestv2FsMIBgp4mpeAggregateVrfName (&u4ErrorCode, i4Index, pCxtName);

    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *) "Unable to Vrf Name.\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;

    }

    i1RetVal = nmhSetFsbgp4mpeAggregateIPAddrSafi (i4Index, i4IpSafi);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);

        IssSendError (pHttp, (CONST INT1 *)
                      " \rUnable to set the sub address family\r\n");
        return;
    }
    i1RetVal = nmhSetFsbgp4mpeAggregateIPAddrPrefix (i4Index, &IpAddress);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *)
                      "\rUnable to set aggregation IP address\r\n");
        return;
    }
    i1RetVal = nmhSetFsbgp4mpeAggregateIPAddrPrefixLen (i4Index, i4PrefixLen);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *)
                      " \rUnable to set the IP address prefix length\r\n");
        return;
    }

    i1RetVal = nmhSetFsbgp4mpeAggregateAsSet (i4Index, i4AsSet);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) " \rUnable to set As-Set field\r\n");
        return;
    }

    i1RetVal =
        nmhSetFsbgp4mpeAggregateSuppressRouteMapName (i4Index, &SuppressMap);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) " \rUnable to set Suppress Map\r\n");
        return;
    }

    i1RetVal =
        nmhSetFsbgp4mpeAggregateAdvertiseRouteMapName (i4Index, &AdvertiseMap);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) " \rUnable to set Advertise Map\r\n");
        return;
    }

    i1RetVal =
        nmhSetFsbgp4mpeAggregateAttributeRouteMapName (i4Index, &AttributeMap);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) " \rUnable to set Attribute Map\r\n");
        return;
    }

    i1RetVal = nmhSetFsbgp4mpeAggregateAdvertise (i4Index, i4Advertise);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) " \rInvalid Advertise field\r\n");
        return;
    }

    i1RetVal = nmhSetFsMIBgp4mpeAggregateVrfName (i4Index, pCxtName);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the Vrf Name\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    i1RetVal = nmhSetFsbgp4mpeAggregateAdminStatus (i4Index, i4Status);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *)
                      "\rUnable to set the admin status\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    free_octetstring (pCxtName);
    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessBgpRouteAggrConfPageGet (pHttp);
    return;
}

/*********************************************************************
 *  Function Name : IssProcessBgpLocalPrefConfPage
 *  Description   : This function processes the request coming for the
 *                  BGP Local Preference settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpLocalPrefConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessBgpLocalPrefConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessBgpLocalPrefConfPageSet (pHttp);
    }
}

/*********************************************************************
 *  Function Name : IssProcessBgpLocalPrefConfPageGet
 *  Description   : This function processes the GET request coming for the
 *                  BGP Local Preference settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpLocalPrefConfPageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    UINT4               u4IpAddress = 0;
    INT4                i4RetVal = 0;
    INT4                i4OutCome = SNMP_FAILURE;
    INT4                i4LocPrefIndex = 0;
    INT4                i4IpAfi = 0;
    INT4                i4CxtId = 0;
    tSNMP_OCTET_STRING_TYPE IpAddress;
    tSNMP_OCTET_STRING_TYPE *InterAs;
    tSNMP_OCTET_STRING_TYPE CxtName;
    UINT1              *pu1TempString = NULL;
    UINT1               au1Ip6Addr[ISS_BGP4_IP6_ADDR_STRING_LEN];
    UINT1               au1LocalIpAddress[ISS_BGP4_IP_MAX_LENGTH];
    UINT1               au1BgpCxtName[VCM_ALIAS_MAX_LEN];

    MEMSET (au1LocalIpAddress, 0, ISS_BGP4_IP_MAX_LENGTH);
    MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
    CxtName.pu1_OctetList = au1BgpCxtName;
    CxtName.i4_Length = 0;

    pHttp->i4Write = 0;
    IssPrintAvailableBgpContextId (pHttp);

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();
    IpAddress.pu1_OctetList = au1LocalIpAddress;

    InterAs = (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring
        (ISS_BGP4_INTER_AS_LENGTH);

    if (InterAs == NULL)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\rInsufficient memory\r\n");
        return;
    }
    MEMSET (InterAs->pu1_OctetList, 0, ISS_BGP4_INTER_AS_LENGTH);
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;
    /*Process the Filter table */
    i4OutCome = nmhGetFirstIndexFsbgp4MpeLocalPrefTable (&i4LocPrefIndex);

    if (i4OutCome == SNMP_FAILURE)
    {
        free_octetstring (InterAs);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }
    do
    {
        pHttp->i4Write = (INT4) u4Temp;
        STRCPY (pHttp->au1KeyString, "fsbgp4LocalPrefIndex_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4LocPrefIndex);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeLocalPrefRemoteAS (i4LocPrefIndex, (UINT4 *) &i4RetVal);
        STRCPY (pHttp->au1KeyString, "fsbgp4LocalPrefRemoteAS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", (UINT4) i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeLocalPrefIPAddrAfi (i4LocPrefIndex, &i4IpAfi);
        STRCPY (pHttp->au1KeyString, "fsbgp4LocalPrefIPAddrAfi_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4IpAfi);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeLocalPrefIPAddrSafi (i4LocPrefIndex, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "fsbgp4LocalPrefIPAddrSafi_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeLocalPrefIPAddrPrefix (i4LocPrefIndex, &IpAddress);
        STRCPY (pHttp->au1KeyString, "fsbgp4LocalPrefIPAddrPrefix_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4IpAfi == BGP4_INET_AFI_IPV4)
        {
            MEMCPY (&u4IpAddress, IpAddress.pu1_OctetList, IpAddress.i4_Length);
            WEB_CONVERT_IPADDR_TO_STR (pu1TempString, OSIX_NTOHL (u4IpAddress));
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1TempString);
        }
#ifdef BGP4_IPV6_WANTED
        else
        {
            Bgp4PrintIp6Addr (IpAddress.pu1_OctetList, (au1Ip6Addr));
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Ip6Addr);
            MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
        }
#endif /* BGP4_IPV6_WANTED */
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeLocalPrefIPAddrPrefixLen (i4LocPrefIndex, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "fsbgp4LocalPrefIPAddrPrefixLen_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeLocalPrefIntermediateAS (i4LocPrefIndex, InterAs);
        STRCPY (pHttp->au1KeyString, "fsbgp4LocalPrefIntermediateAS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", InterAs->pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        MEMSET (InterAs->pu1_OctetList, 0, ISS_BGP4_INTER_AS_LENGTH);

        nmhGetFsbgp4mpeLocalPrefDirection (i4LocPrefIndex, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "fsbgp4LocalPrefDirection_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeLocalPrefValue (i4LocPrefIndex, (UINT4 *) &i4RetVal);
        STRCPY (pHttp->au1KeyString, "fsbgp4LocalPrefValue_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeLocalPrefPreference (i4LocPrefIndex, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "fsbgp4LocalPrefPreference_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4mpeLocalPrefAdminStatus (i4LocPrefIndex, &i4RetVal);
        STRCPY (pHttp->au1KeyString, "fsbgp4LocalPrefAdminStatus_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        MEMSET (au1BgpCxtName, 0, VCM_ALIAS_MAX_LEN);
        nmhGetFsMIBgp4mpeLocalPrefVrfName (i4LocPrefIndex, &CxtName);
        STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString,
                  (UINT4) (CxtName.i4_Length + 1), "%s", CxtName.pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        VcmIsVrfExist (CxtName.pu1_OctetList, (UINT4 *) &i4CxtId);
        STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_DUMMY_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4CxtId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    }
    while (nmhGetNextIndexFsbgp4MpeLocalPrefTable (i4LocPrefIndex, &i4LocPrefIndex) != SNMP_FAILURE);    /*end of do-while */

    free_octetstring (InterAs);
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
 *  Function Name : IssProcessBgpLocalPrefConfPageSet
 *  Description   : This function processes the SET request coming for the
 *                  BGP Local Preference settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpLocalPrefConfPageSet (tHttp * pHttp)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4LocalPrefValue = 0;
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4CxtId = 0;
    UINT4               u4RemoteAs = 0;
    INT4                i4IpAfi = 0;
    INT4                i4IpSafi = 0;
    INT4                i4Direction = 0;
    INT4                i4PrefixLen = 0;
    INT4                i4Status = 0;
    INT4                i4Index = 0;
    INT4                i4Preference = 0;
    INT4                i4SupportStatus;
    tSNMP_OCTET_STRING_TYPE IpAddress;
    tSNMP_OCTET_STRING_TYPE *pCxtName = NULL;
    tNetAddress         NetAddress;
    tUtlInAddr          InAddr;
    UINT1               au1LocalIpAddress[ISS_BGP4_IP_MAX_LENGTH];

    MEMSET (au1LocalIpAddress, 0, ISS_BGP4_IP_MAX_LENGTH);
    MEMSET (&NetAddress, 0, sizeof (tNetAddress));

    pCxtName =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (VCM_ALIAS_MAX_LEN);
    if (pCxtName == NULL)
    {
        return;
    }
    MEMSET (pCxtName->pu1_OctetList, 0, VCM_ALIAS_MAX_LEN);

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();
    STRCPY (pHttp->au1Name, "CONTEXT_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4CxtId = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Apply") == 0)
    {
        STRCPY (pHttp->au1Name, "BGP_CONTEXT_DUMMY");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4CxtId = ATOI (pHttp->au1Value);
    }

    if (BgpSetContext (i4CxtId) == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
        return;
    }
    VcmGetAliasName ((UINT4) i4CxtId, pCxtName->pu1_OctetList);
    pCxtName->i4_Length = (INT4) STRLEN (pCxtName->pu1_OctetList);

    STRCPY (pHttp->au1Name, "fsbgp4LocalPrefIndex");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Index = ATOI (pHttp->au1Value);
    /* INPUT VALIDATION */
    i1RetVal = nmhValidateIndexInstanceFsbgp4MpeLocalPrefTable (i4Index);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *) "\rInvalid Local Pref Index\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }
    STRCPY (pHttp->au1Name, "fsbgp4LocalPrefAdminStatus");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Status = ATOI (pHttp->au1Value);

    i1RetVal = nmhSetFsbgp4mpeLocalPrefAdminStatus (i4Index, i4Status);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the Local Preference Admin Status.\r\n");
        return;

    }
    if (i4Status == BGP4_ADMIN_UP)
    {
        free_octetstring (pCxtName);
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssProcessBgpLocalPrefConfPageGet (pHttp);
        return;
    }

    STRCPY (pHttp->au1Name, "fsbgp4LocalPrefRemoteAS");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    u4RemoteAs = (UINT4) ATOI (pHttp->au1Value);
    nmhGetFsbgp4FourByteASNSupportStatus (&i4SupportStatus);

    if ((i4SupportStatus == BGP4_DISABLE_4BYTE_ASN_SUPPORT) &&
        (u4RemoteAs > BGP4_MAX_TWO_BYTE_AS))
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "4-byte ASN Capability is disabled. Configure value < 65536");
        IssProcessBgpContextCreationPageGet (pHttp);
        free_octetstring (pCxtName);
        return;
    }

    STRCPY (pHttp->au1Name, "fsbgp4LocalPrefIPAddrAfi");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4IpAfi = ATOI (pHttp->au1Value);
    STRCPY (pHttp->au1Name, "fsbgp4LocalPrefIPAddrSafi");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4IpSafi = ATOI (pHttp->au1Value);
    /* Address processing IPv4 & IPV6 */
    STRCPY (pHttp->au1Name, "fsbgp4LocalPrefIPAddrPrefixLen");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PrefixLen = ATOI (pHttp->au1Value);
    STRCPY (pHttp->au1Name, "fsbgp4LocalPrefDirection");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Direction = ATOI (pHttp->au1Value);
    STRCPY (pHttp->au1Name, "fsbgp4LocalPrefValue");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4LocalPrefValue = (UINT4) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4LocalPrefPreference");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Preference = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4LocalPrefIPAddrPrefix");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);
    if (i4IpAfi == BGP4_INET_AFI_IPV4)
    {
        NetAddress.NetAddr.u2Afi = BGP4_INET_AFI_IPV4;
        NetAddress.u2Safi = BGP4_INET_SAFI_UNICAST;
        if (0 == BGP4_INET_ATON (pHttp->au1Value, &InAddr))
        {
            free_octetstring (pCxtName);
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessBgpLocalPrefConfPageGet (pHttp);
            return;
        }
        MEMCPY ((NetAddress.NetAddr.au1Address),
                &InAddr.u4Addr, sizeof (UINT4));
        NetAddress.NetAddr.u2AddressLen = sizeof (UINT4);
        NetAddress.u2PrefixLen = (UINT2) i4PrefixLen;

    }
#ifdef BGP4_IPV6_WANTED
    else if (str_to_ip6addr (pHttp->au1Value) != NULL)
    {
        NetAddress.NetAddr.u2Afi = BGP4_INET_AFI_IPV6;
        NetAddress.u2Safi = BGP4_INET_SAFI_UNICAST;
        MEMCPY (NetAddress.NetAddr.au1Address,
                str_to_ip6addr (pHttp->au1Value), BGP4_IPV6_PREFIX_LEN);
        NetAddress.NetAddr.u2AddressLen = BGP4_IPV6_PREFIX_LEN;
        NetAddress.u2PrefixLen = (UINT2) i4PrefixLen;

    }
#endif /* BGP4_IPV6_WANTED */

    IpAddress.i4_Length = (UINT4) NetAddress.NetAddr.u2AddressLen;
    IpAddress.pu1_OctetList = au1LocalIpAddress;
    MEMCPY ((IpAddress.pu1_OctetList),
            (NetAddress.NetAddr.au1Address), NetAddress.NetAddr.u2AddressLen);

    STRCPY (pHttp->au1Name, "fsbgp4LocalPrefIntermediateAS");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);
    if ((pHttp->au1Value != NULL) && ((INT4) STRLEN (pHttp->au1Value) != 0))
    {
        i1RetVal = Bgp4Testv2Fsbgp4LocalPrefIntermediateAS (&u4ErrorCode,
                                                            i4Index,
                                                            pHttp->au1Value,
                                                            (INT4) STRLEN
                                                            (pHttp->au1Value));
        if (i1RetVal == SNMP_FAILURE)
        {
            free_octetstring (pCxtName);
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          " \rInvalid Intermediate As\r\n");
            return;

        }
    }

    i1RetVal = nmhTestv2Fsbgp4mpeLocalPrefIPAddrAfi (&u4ErrorCode,
                                                     i4Index, i4IpAfi);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the AFI.\r\n");

        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    i1RetVal = nmhSetFsbgp4mpeLocalPrefIPAddrAfi (i4Index, i4IpAfi);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the AFI.\r\n");

        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    i1RetVal = nmhTestv2Fsbgp4mpeLocalPrefIPAddrSafi (&u4ErrorCode,
                                                      i4Index, i4IpSafi);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the SAFI.\r\n");

        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    i1RetVal = nmhTestv2Fsbgp4mpeLocalPrefIPAddrPrefix (&u4ErrorCode,
                                                        i4Index, &IpAddress);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the Local Address Prefix.\r\n");

        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;

    }

    i1RetVal = nmhTestv2Fsbgp4mpeLocalPrefIPAddrPrefixLen (&u4ErrorCode,
                                                           i4Index,
                                                           i4PrefixLen);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the Local Address Prefix Length.\r\n");

        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;

    }
    i1RetVal = nmhTestv2Fsbgp4mpeLocalPrefRemoteAS (&u4ErrorCode,
                                                    i4Index, u4RemoteAs);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the Remote As.\r\n");

        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;

    }

    i1RetVal = nmhTestv2Fsbgp4mpeLocalPrefValue (&u4ErrorCode, i4Index,
                                                 u4LocalPrefValue);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the Local Preference Value.\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);

        return;

    }
    i1RetVal = nmhTestv2Fsbgp4mpeLocalPrefDirection (&u4ErrorCode,
                                                     i4Index, i4Direction);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the Local Preference Direction.\r\n");
        return;

    }

    i1RetVal = nmhSetFsbgp4mpeLocalPrefIPAddrSafi (i4Index, i4IpSafi);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the SAFI.\r\n");

        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    /* INPUT PROCESSING */
    i1RetVal = nmhSetFsbgp4mpeLocalPrefIPAddrPrefix (i4Index, &IpAddress);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the IP address.\r\n");

        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;

    }

    i1RetVal = nmhSetFsbgp4mpeLocalPrefIPAddrPrefixLen (i4Index, i4PrefixLen);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the IP address prefix length.\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);

        return;

    }
    i1RetVal = nmhSetFsbgp4mpeLocalPrefRemoteAS (i4Index, u4RemoteAs);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the remote AS.\r\n");

        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;

    }
    i1RetVal = nmhSetFsbgp4mpeLocalPrefValue (i4Index, u4LocalPrefValue);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the value.\r\n");

        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;

    }

    i1RetVal = nmhSetFsbgp4mpeLocalPrefDirection (i4Index, i4Direction);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the direction.\r\n");

        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;

    }
    i1RetVal = nmhSetFsbgp4mpeLocalPrefPreference (i4Index, i4Preference);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the preference.\r\n");

        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;

    }

    i1RetVal = Bgp4SetFsbgp4LocalPrefIntermediateAS (i4Index, pHttp->au1Value);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the Intermediate AS.\r\n");

        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;

    }

    i1RetVal = nmhTestv2FsMIBgp4mpeLocalPrefVrfName (&u4ErrorCode,
                                                     i4Index, pCxtName);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the Local Preference Direction.\r\n");
        return;

    }

    i1RetVal = nmhSetFsMIBgp4mpeLocalPrefVrfName (i4Index, pCxtName);
    if (i1RetVal == SNMP_FAILURE)
    {
        free_octetstring (pCxtName);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the Vrf Name\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }
    free_octetstring (pCxtName);
    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessBgpLocalPrefConfPageGet (pHttp);
    return;
}

/*********************************************************************
 *  Function Name : IssProcessBgpCommFilterConfPage
 *  Description   : This function processes the request coming for the
 *                  BGP Community Filter settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpCommFilterConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessBgpCommFilterConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessBgpCommFilterConfPageSet (pHttp);
    }
}

/*********************************************************************
 *  Function Name : IssProcessBgpCommFilterConfPageGet
 *  Description   : This function processes the GET request coming for the
 *                  BGP Community Filter settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpCommFilterConfPageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    UINT4               u4RetVal = 0;
    INT4                i4Status = 0;
    INT4                i4BgpContextId = 0;
    INT4                i4BgpPrevContextId = 0;
    UINT1               au1BgpCxtName[VCM_ALIAS_MAX_LEN];

    MEMSET (au1BgpCxtName, 0, VCM_ALIAS_MAX_LEN);

    pHttp->i4Write = 0;
    IssPrintAvailableBgpContextId (pHttp);

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    if (nmhGetFirstIndexFsMIBgpContextTable (&i4BgpContextId) == SNMP_FAILURE)
    {
        pHttp->i4Write = (INT4) u4Temp;
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        if (BgpSetContext (i4BgpContextId) == SNMP_FAILURE)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
            return;
        }

        if (nmhGetFirstIndexFsbgp4CommOutFilterTable (&u4RetVal)
            == SNMP_SUCCESS)
        {
            /*Process the Community out filter table */
            do
            {
                pHttp->i4Write = (INT4) u4Temp;

                STRCPY (pHttp->au1KeyString, "COMM_VALUE");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                nmhGetFsbgp4CommOutgoingFilterStatus (u4RetVal, &i4Status);
                STRCPY (pHttp->au1KeyString, "FILTER_STATUS");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Status);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "FILTER_TYPE");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "2");
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                VcmGetAliasName ((UINT4) i4BgpContextId, au1BgpCxtName);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1BgpCxtName);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            }
            while (nmhGetNextIndexFsbgp4CommOutFilterTable (u4RetVal, &u4RetVal) == SNMP_SUCCESS);    /*end of do-while */
        }

        /*Process the Community In Filter table */

        if (nmhGetFirstIndexFsbgp4CommInFilterTable (&u4RetVal) == SNMP_FAILURE)
        {
            i4BgpPrevContextId = i4BgpContextId;
            if (nmhGetNextIndexFsMIBgpContextTable (i4BgpPrevContextId,
                                                    &i4BgpContextId) !=
                SNMP_SUCCESS)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
                WebnmSockWrite (pHttp,
                                (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                                (pHttp->i4HtmlSize - pHttp->i4Write));

                return;
            }
            continue;
        }

        do
        {
            pHttp->i4Write = (INT4) u4Temp;
            STRCPY (pHttp->au1KeyString, "COMM_VALUE");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4CommIncomingFilterStatus (u4RetVal, &i4Status);
            STRCPY (pHttp->au1KeyString, "FILTER_STATUS");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Status);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "FILTER_TYPE");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "1");
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            VcmGetAliasName ((UINT4) i4BgpContextId, au1BgpCxtName);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1BgpCxtName);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        }
        while (nmhGetNextIndexFsbgp4CommInFilterTable (u4RetVal, &u4RetVal) != SNMP_FAILURE);    /*end of do-while */

        i4BgpPrevContextId = i4BgpContextId;

    }
    while (nmhGetNextIndexFsMIBgpContextTable
           (i4BgpPrevContextId, &i4BgpContextId) == SNMP_SUCCESS);

    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
 *  Function Name : IssProcessBgpCommFilterConfPageSet
 *  Description   : This function processes the SET request coming for the
 *                  BGP Community Filter settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpCommFilterConfPageSet (tHttp * pHttp)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4CommValue = 0;
    INT4                i4Action = 0;
    INT4                i4CxtId = 0;
    INT4                i4FilterTable = ISS_BGP4_COMM_IN_FLT_TBL;
    INT4                i4RowStatus = ISS_NOT_IN_SERVICE;
    UINT1               u1Set = ISS_BGP_COMM_FILTER_DELETE;
    INT1                i1RetVal = SNMP_FAILURE;

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    HttpGetValuebyName (ISS_ACTION, pHttp->au1Value, pHttp->au1PostQuery);
    if ((0 == STRCMP (pHttp->au1Value, "Delete"))
        || (0 == STRCMP (pHttp->au1Value, "Apply")))
    {
        STRCPY (pHttp->au1Name, "CONTEXT_ID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        VcmIsVrfExist (pHttp->au1Value, (UINT4 *) &i4CxtId);
    }
    else
    {
        STRCPY (pHttp->au1Name, "CONTEXT_ID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4CxtId = ATOI (pHttp->au1Value);
    }

    if (BgpSetContext ((INT4) i4CxtId) == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
        return;
    }

    STRCPY (pHttp->au1Name, "CommunityValue");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4CommValue = (UINT4) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "FilterStatus");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Action = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "FilterTable");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4FilterTable = ATOI (pHttp->au1Value);

    HttpGetValuebyName (ISS_ACTION, pHttp->au1Value, pHttp->au1PostQuery);
    if (0 == STRCMP (pHttp->au1Value, "Delete"))
    {
        u1Set = ISS_BGP_COMM_FILTER_DELETE;
    }
    else
    {
        u1Set = ISS_BGP_COMM_FILTER_ADD;
    }

    /*Check for valid indexes passed to the appropriate table */
    if (i4FilterTable == ISS_BGP4_COMM_IN_FLT_TBL)
    {
        i1RetVal =
            nmhValidateIndexInstanceFsbgp4CommInFilterTable (u4CommValue);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Invalid Index to Community In Filter Table\r\n");
            return;
        }
        i1RetVal = nmhGetFsbgp4InFilterRowStatus (u4CommValue, &i4RowStatus);

    }
    else
    {
        i1RetVal =
            nmhValidateIndexInstanceFsbgp4CommOutFilterTable (u4CommValue);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "\rInvalid Index to CommOut Table\r\n");
            return;
        }

        i1RetVal = nmhGetFsbgp4OutFilterRowStatus (u4CommValue, &i4RowStatus);
    }

    /*If trying to delete non-existing entry */
    if ((i1RetVal == SNMP_FAILURE) && (u1Set == ISS_BGP_COMM_FILTER_DELETE))
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "Trying to delete non-existing entry\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    /*Process  the apply or delete request */
    if (u1Set == ISS_BGP_COMM_FILTER_ADD)
    {
        /* i1RetVal derived from return value of
         * nmhGetFsbgp4InFilterRowStatus/nmhGetFsbgp4OutFilterRowStatus */
        if (i1RetVal == SNMP_FAILURE)
        {
            /* Entry not present in Comm IN/OUT filter Table.
             * Need to create a entry, set the input action and
             * activate the entry. */
            if (i4FilterTable == ISS_BGP4_COMM_IN_FLT_TBL)
            {
                i1RetVal =
                    nmhTestv2Fsbgp4InFilterRowStatus (&u4ErrorCode,
                                                      u4CommValue,
                                                      CREATE_AND_WAIT);
                if (i1RetVal == SNMP_FAILURE)
                {
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *)
                                  "Invalid rowstatus\r\n");
                    return;
                }
                i1RetVal = nmhSetFsbgp4InFilterRowStatus (u4CommValue,
                                                          CREATE_AND_WAIT);
            }
            else
            {
                i1RetVal =
                    nmhTestv2Fsbgp4OutFilterRowStatus (&u4ErrorCode,
                                                       u4CommValue,
                                                       CREATE_AND_WAIT);
                if (i1RetVal == SNMP_FAILURE)
                {
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *)
                                  "Invalid rowstatus\r\n");
                    return;
                }
                i1RetVal = nmhSetFsbgp4OutFilterRowStatus (u4CommValue,
                                                           CREATE_AND_WAIT);
            }

            if (i1RetVal == SNMP_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to set the row status\r\n");
                return;

            }
        }
        else
        {
            /* Already Entry is present. First make the entry row status
             * not in service, set the input action and activate the
             * entry */
            if (i4FilterTable == ISS_BGP4_COMM_IN_FLT_TBL)
            {
                i1RetVal =
                    nmhTestv2Fsbgp4InFilterRowStatus (&u4ErrorCode,
                                                      u4CommValue,
                                                      NOT_IN_SERVICE);
                if (i1RetVal == SNMP_FAILURE)
                {
                    nmhSetFsbgp4InFilterRowStatus (u4CommValue, ISS_DESTROY);
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    return;
                }
                i1RetVal =
                    nmhSetFsbgp4InFilterRowStatus (u4CommValue, NOT_IN_SERVICE);

                if (i1RetVal == SNMP_FAILURE)
                {
                    nmhSetFsbgp4InFilterRowStatus (u4CommValue, ISS_DESTROY);
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    return;
                }
            }
            else
            {
                i1RetVal =
                    nmhTestv2Fsbgp4OutFilterRowStatus (&u4ErrorCode,
                                                       u4CommValue,
                                                       NOT_IN_SERVICE);
                if (i1RetVal == SNMP_FAILURE)
                {
                    nmhSetFsbgp4OutFilterRowStatus (u4CommValue, ISS_DESTROY);
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    return;
                }
                i1RetVal =
                    nmhSetFsbgp4OutFilterRowStatus (u4CommValue,
                                                    NOT_IN_SERVICE);
                if (i1RetVal == SNMP_FAILURE)
                {
                    nmhSetFsbgp4OutFilterRowStatus (u4CommValue, ISS_DESTROY);
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    return;
                }
            }
        }

        /* Entry present and not active. Set the input action and
         * make the entry status as active */
        if (i4FilterTable == ISS_BGP4_COMM_IN_FLT_TBL)
        {
            i1RetVal =
                nmhTestv2Fsbgp4CommIncomingFilterStatus (&u4ErrorCode,
                                                         u4CommValue, i4Action);
            if (i1RetVal == SNMP_FAILURE)
            {
                nmhSetFsbgp4InFilterRowStatus (u4CommValue, DESTROY);
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                return;
            }
            i1RetVal =
                nmhSetFsbgp4CommIncomingFilterStatus (u4CommValue, i4Action);
            if (i1RetVal == SNMP_FAILURE)
            {
                nmhSetFsbgp4InFilterRowStatus (u4CommValue, DESTROY);
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                return;
            }
            i1RetVal =
                nmhTestv2Fsbgp4InFilterRowStatus (&u4ErrorCode, u4CommValue,
                                                  ACTIVE);
            if (i1RetVal == SNMP_FAILURE)
            {
                nmhSetFsbgp4InFilterRowStatus (u4CommValue, DESTROY);
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                return;
            }
            i1RetVal = nmhSetFsbgp4InFilterRowStatus (u4CommValue, ACTIVE);
            if (i1RetVal == SNMP_FAILURE)
            {
                nmhSetFsbgp4InFilterRowStatus (u4CommValue, DESTROY);
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                return;
            }

        }
        else
        {
            i1RetVal =
                nmhTestv2Fsbgp4CommOutgoingFilterStatus (&u4ErrorCode,
                                                         u4CommValue, i4Action);
            if (i1RetVal == SNMP_FAILURE)
            {
                nmhSetFsbgp4OutFilterRowStatus (u4CommValue, DESTROY);
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                return;
            }
            i1RetVal =
                nmhSetFsbgp4CommOutgoingFilterStatus (u4CommValue, i4Action);
            if (i1RetVal == SNMP_FAILURE)
            {
                nmhSetFsbgp4OutFilterRowStatus (u4CommValue, DESTROY);
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                return;
            }
            i1RetVal =
                nmhTestv2Fsbgp4OutFilterRowStatus (&u4ErrorCode,
                                                   u4CommValue, ACTIVE);
            if (i1RetVal == SNMP_FAILURE)
            {
                nmhSetFsbgp4OutFilterRowStatus (u4CommValue, DESTROY);
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                return;
            }
            i1RetVal = nmhSetFsbgp4OutFilterRowStatus (u4CommValue, ACTIVE);
            if (i1RetVal == SNMP_FAILURE)
            {
                nmhSetFsbgp4OutFilterRowStatus (u4CommValue, DESTROY);
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                return;
            }

        }
    }
    else if (u1Set == ISS_BGP_COMM_FILTER_DELETE)
    {
        /* Entry present in Comm IN/OUT filter  Table. Need to Delete */
        if (i4FilterTable == ISS_BGP4_COMM_IN_FLT_TBL)
        {
            i1RetVal =
                nmhTestv2Fsbgp4InFilterRowStatus (&u4ErrorCode,
                                                  u4CommValue, DESTROY);
            if (i1RetVal == SNMP_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                return;
            }
            i1RetVal = nmhSetFsbgp4InFilterRowStatus (u4CommValue, DESTROY);
            if (i1RetVal == SNMP_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                return;
            }
        }
        else
        {
            i1RetVal =
                nmhTestv2Fsbgp4OutFilterRowStatus (&u4ErrorCode,
                                                   u4CommValue, DESTROY);
            if (i1RetVal == SNMP_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                return;
            }
            i1RetVal = nmhSetFsbgp4OutFilterRowStatus (u4CommValue, DESTROY);
            if (i1RetVal == SNMP_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                return;
            }
        }
    }
    else
    {
        IssSendError (pHttp, (CONST INT1 *) "\rInvalid action \r\n");
    }

    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessBgpCommFilterConfPageGet (pHttp);
    return;
}

/*********************************************************************
 *  Function Name : IssProcessCommBgpLocPolicyConfPage
 *  Description   : This function processes the request coming for the
 *                  BGP Community Local Policy settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessCommBgpLocPolicyConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessCommBgpLocPolicyConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessCommBgpLocPolicyConfPageSet (pHttp);
    }
}

/*********************************************************************
 *  Function Name : IssProcessCommBgpLocPolicyConfPageGet
 *  Description   : This function processes the GET request coming for the
 *                  BGP Community Local policy settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessCommBgpLocPolicyConfPageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    UINT4               u4IpAddress = 0;
    INT4                i4BgpContextId = 0;
    INT4                i4BgpPrevContextId = 0;
    INT4                i4Fsbgp4CommSetStatusAfi = BGP4_INET_AFI_IPV4;
    INT4                i4Fsbgp4CommSetStatusSafi = BGP4_INET_SAFI_UNICAST;
    INT4                i4Fsbgp4CommSetStatusIpPrefixLen = 0;
    INT4                i4Fsbgp4CommSetStatus = 0;
    INT4                i4OutCome = SNMP_FAILURE;
    UINT1              *pu1TempString = NULL;
    tSNMP_OCTET_STRING_TYPE Fsbgp4CommSetStatusIpNw;
    UINT1               au1CommIpAddress[ISS_BGP4_IP_MAX_LENGTH];
    UINT1               au1Ip6Addr[ISS_BGP4_IP6_ADDR_STRING_LEN];
    UINT1               au1BgpCxtName[VCM_ALIAS_MAX_LEN];

    MEMSET (au1CommIpAddress, 0, ISS_BGP4_IP_MAX_LENGTH);
    MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
    MEMSET (au1BgpCxtName, 0, VCM_ALIAS_MAX_LEN);

    pHttp->i4Write = 0;
    IssPrintAvailableBgpContextId (pHttp);

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    if (nmhGetFirstIndexFsMIBgpContextTable (&i4BgpContextId) == SNMP_FAILURE)
    {
        pHttp->i4Write = (INT4) u4Temp;
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    /*Allocate memory */
    Fsbgp4CommSetStatusIpNw.pu1_OctetList = au1CommIpAddress;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        if (BgpSetContext (i4BgpContextId) == SNMP_FAILURE)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
            return;
        }

        i4OutCome =
            nmhGetFirstIndexFsbgp4MpeCommRouteCommSetStatusTable
            (&i4Fsbgp4CommSetStatusAfi, &i4Fsbgp4CommSetStatusSafi,
             &Fsbgp4CommSetStatusIpNw, &i4Fsbgp4CommSetStatusIpPrefixLen);

        if (i4OutCome == SNMP_FAILURE)
        {
            i4BgpPrevContextId = i4BgpContextId;
            if (nmhGetNextIndexFsMIBgpContextTable (i4BgpPrevContextId,
                                                    &i4BgpContextId) !=
                SNMP_SUCCESS)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
                WebnmSockWrite (pHttp,
                                (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                                (pHttp->i4HtmlSize - pHttp->i4Write));

                return;
            }
            continue;
        }

        /*Process the BGP community set status table */
        do
        {
            pHttp->i4Write = (INT4) u4Temp;

            STRCPY (pHttp->au1KeyString, "IP_ADDRESS");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (i4Fsbgp4CommSetStatusAfi == BGP4_INET_AFI_IPV4)
            {
                MEMCPY (&u4IpAddress,
                        Fsbgp4CommSetStatusIpNw.pu1_OctetList,
                        Fsbgp4CommSetStatusIpNw.i4_Length);
                WEB_CONVERT_IPADDR_TO_STR (pu1TempString,
                                           OSIX_NTOHL (u4IpAddress));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1TempString);
            }
#ifdef BGP4_IPV6_WANTED
            else
            {
                Bgp4PrintIp6Addr (Fsbgp4CommSetStatusIpNw.pu1_OctetList,
                                  (au1Ip6Addr));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Ip6Addr);
                MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
            }
#endif /* BGP4_IPV6_WANTED */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "PREFIX_LENGTH");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                     i4Fsbgp4CommSetStatusIpPrefixLen);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpeCommSetStatus (i4Fsbgp4CommSetStatusAfi,
                                          i4Fsbgp4CommSetStatusSafi,
                                          &Fsbgp4CommSetStatusIpNw,
                                          i4Fsbgp4CommSetStatusIpPrefixLen,
                                          &i4Fsbgp4CommSetStatus);

            STRCPY (pHttp->au1KeyString, "COMM_STATUS");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                     i4Fsbgp4CommSetStatus);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            VcmGetAliasName ((UINT4) i4BgpContextId, au1BgpCxtName);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1BgpCxtName);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        }
        while (nmhGetNextIndexFsbgp4MpeCommRouteCommSetStatusTable
               (i4Fsbgp4CommSetStatusAfi, &i4Fsbgp4CommSetStatusAfi,
                i4Fsbgp4CommSetStatusSafi, &i4Fsbgp4CommSetStatusSafi,
                &Fsbgp4CommSetStatusIpNw, &Fsbgp4CommSetStatusIpNw,
                i4Fsbgp4CommSetStatusIpPrefixLen,
                &i4Fsbgp4CommSetStatusIpPrefixLen) == SNMP_SUCCESS);
        /*end of do-while */
        i4BgpPrevContextId = i4BgpContextId;

    }
    while (nmhGetNextIndexFsMIBgpContextTable
           (i4BgpPrevContextId, &i4BgpContextId) == SNMP_SUCCESS);

    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    return;

}

/*********************************************************************
 *  Function Name : IssProcessCommBgpLocPolicyConfPageSet
 *  Description   : This function processes the request coming for the
 *                  BGP Community Local policy settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessCommBgpLocPolicyConfPageSet (tHttp * pHttp)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4IpMask;
    INT4                i4RowStatus = 0;
    INT4                i4Action = 0;
    INT4                i4Len = 0;
    INT4                i4CxtId = 0;
    UINT1               u1Set = ISS_BGP4_APPLY;
    UINT1               au1Ip6Addr[ISS_BGP4_IP6_ADDR_STRING_LEN];
    INT1                i1RetVal = 0;
    tNetAddress         NetAddress;
    tUtlInAddr          InAddr;
    tSNMP_OCTET_STRING_TYPE IpAddr;
    UINT1               au1LocalIpAddress[ISS_BGP4_IP_MAX_LENGTH];

    MEMSET (au1LocalIpAddress, 0, ISS_BGP4_IP_MAX_LENGTH);
    MEMSET (&au1Ip6Addr, 0, sizeof (au1Ip6Addr));
    MEMSET (&NetAddress, 0, sizeof (tNetAddress));

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    if (0 == STRCMP (pHttp->au1Value, "Delete"))
    {
        STRCPY (pHttp->au1Name, "CONTEXT_ID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        VcmIsVrfExist (pHttp->au1Value, (UINT4 *) &i4CxtId);
    }
    else
    {
        STRCPY (pHttp->au1Name, "CONTEXT_ID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4CxtId = ATOI (pHttp->au1Value);
    }

    if (BgpSetContext ((INT4) i4CxtId) == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
        return;
    }

    STRCPY (pHttp->au1Name, "PrefixLength");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4IpMask = (UINT4) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "CommunityStatus");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Action = ATOI (pHttp->au1Value);

    HttpGetValuebyName (ISS_ACTION, pHttp->au1Value, pHttp->au1PostQuery);

    if (0 == STRCMP (pHttp->au1Value, "Delete"))
    {
        u1Set = ISS_BGP4_DELETE;
    }
    else
    {
        u1Set = ISS_BGP4_APPLY;
    }

    STRCPY (pHttp->au1Name, "IpAddress");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);
    /*Process the IP address with AFI and SAFI parameters */
    i4Len = CliDotStrLength (pHttp->au1Value);
    if (i4Len == sizeof (UINT4))
    {
        NetAddress.NetAddr.u2Afi = BGP4_INET_AFI_IPV4;
        NetAddress.u2Safi = BGP4_INET_SAFI_UNICAST;
        if (0 == BGP4_INET_ATON (pHttp->au1Value, &InAddr))
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            return;
        }
        MEMCPY ((NetAddress.NetAddr.au1Address),
                &InAddr.u4Addr, sizeof (UINT4));
        NetAddress.NetAddr.u2AddressLen = sizeof (UINT4);
        NetAddress.u2PrefixLen = (UINT2) u4IpMask;

    }
#ifdef BGP4_IPV6_WANTED
    else if (str_to_ip6addr (pHttp->au1Value) != NULL)
    {
        NetAddress.NetAddr.u2Afi = BGP4_INET_AFI_IPV6;
        NetAddress.u2Safi = BGP4_INET_SAFI_UNICAST;
        MEMCPY (NetAddress.NetAddr.au1Address,
                str_to_ip6addr (pHttp->au1Value), BGP4_IPV6_PREFIX_LEN);

        NetAddress.NetAddr.u2AddressLen = BGP4_IPV6_PREFIX_LEN;

        NetAddress.u2PrefixLen = (UINT2) u4IpMask;

    }
#endif /* BGP4_IPV6_WANTED */

    IpAddr.i4_Length = (UINT4) NetAddress.NetAddr.u2AddressLen;
    IpAddr.pu1_OctetList = au1LocalIpAddress;

    MEMCPY ((IpAddr.pu1_OctetList),
            (NetAddress.NetAddr.au1Address), NetAddress.NetAddr.u2AddressLen);

    /* Validate the index to the table */
    i1RetVal =
        nmhValidateIndexInstanceFsbgp4MpeCommRouteCommSetStatusTable
        (NetAddress.NetAddr.u2Afi, NetAddress.u2Safi, &IpAddr,
         NetAddress.u2PrefixLen);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *)
                      "\r Invalid Index to the CommSetStatus Table\r\n");
        return;
    }

    if (u1Set == ISS_BGP4_APPLY)
    {
        i1RetVal =
            nmhTestv2Fsbgp4mpeCommSetStatus (&u4ErrorCode,
                                             NetAddress.NetAddr.u2Afi,
                                             NetAddress.u2Safi,
                                             &IpAddr,
                                             NetAddress.u2PrefixLen, i4Action);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);

            IssSendError (pHttp, (CONST INT1 *)
                          "\r Invalid community status\r\n");
            return;
        }
    }
    i1RetVal =
        nmhGetFsbgp4mpeCommSetStatusRowStatus (NetAddress.NetAddr.u2Afi,
                                               NetAddress.u2Safi,
                                               &IpAddr,
                                               NetAddress.u2PrefixLen,
                                               &i4RowStatus);

    if ((i1RetVal == SNMP_FAILURE) && (u1Set == ISS_BGP4_DELETE))
    {
        /* Trying to delete a non-existing Entry */
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);

        IssSendError (pHttp, (CONST INT1 *) "\r Entry doesn't exist\r\n");
        return;
    }

    if (u1Set == ISS_BGP4_APPLY)
    {
        /* i1RetVal derived from return value of
           nmhGetFsbgp4CommSetStatusRowStatus */
        if (i1RetVal == SNMP_FAILURE)
        {
            /* Entry not present in Comm Set Status Table.
             * Need to create */
            i1RetVal =
                nmhTestv2Fsbgp4mpeCommSetStatusRowStatus (&u4ErrorCode,
                                                          NetAddress.
                                                          NetAddr.u2Afi,
                                                          NetAddress.u2Safi,
                                                          &IpAddr,
                                                          NetAddress.
                                                          u2PrefixLen,
                                                          CREATE_AND_WAIT);
            if (i1RetVal == SNMP_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);

                IssSendError (pHttp, (CONST INT1 *)
                              "\r Invalid row status\r\n");
                return;
            }

            i1RetVal =
                nmhSetFsbgp4mpeCommSetStatusRowStatus (NetAddress.NetAddr.
                                                       u2Afi,
                                                       NetAddress.u2Safi,
                                                       &IpAddr,
                                                       NetAddress.
                                                       u2PrefixLen,
                                                       CREATE_AND_WAIT);
            if (i1RetVal == SNMP_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);

                IssSendError (pHttp, (CONST INT1 *)
                              "\r Unable to set the row status\r\n");
                return;

            }
        }
        else
        {
            /* Already Entry is present. Set it to not in service */
            i1RetVal =
                nmhTestv2Fsbgp4mpeCommSetStatusRowStatus (&u4ErrorCode,
                                                          NetAddress.
                                                          NetAddr.u2Afi,
                                                          NetAddress.u2Safi,
                                                          &IpAddr,
                                                          NetAddress.
                                                          u2PrefixLen,
                                                          NOT_IN_SERVICE);
            if (i1RetVal == SNMP_FAILURE)
            {
                nmhSetFsbgp4mpeCommSetStatusRowStatus (NetAddress.NetAddr.
                                                       u2Afi,
                                                       NetAddress.u2Safi,
                                                       &IpAddr,
                                                       NetAddress.
                                                       u2PrefixLen, DESTROY);
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);

                IssSendError (pHttp, (CONST INT1 *)
                              "\r Unable to set the row status\r\n");
                return;

            }

            i1RetVal =
                nmhSetFsbgp4mpeCommSetStatusRowStatus (NetAddress.NetAddr.
                                                       u2Afi,
                                                       NetAddress.u2Safi,
                                                       &IpAddr,
                                                       NetAddress.
                                                       u2PrefixLen,
                                                       NOT_IN_SERVICE);
            if (i1RetVal == SNMP_FAILURE)
            {
                nmhSetFsbgp4mpeCommSetStatusRowStatus (NetAddress.NetAddr.
                                                       u2Afi,
                                                       NetAddress.u2Safi,
                                                       &IpAddr,
                                                       NetAddress.
                                                       u2PrefixLen, DESTROY);
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);

                IssSendError (pHttp, (CONST INT1 *)
                              "\r Unable to delete the entry\r\n");
                return;
            }
        }

        /* Entry Present. But not in Active State. Set the input action
         * and make the entry status as active. */

        i1RetVal =
            nmhSetFsbgp4mpeCommSetStatus (NetAddress.NetAddr.u2Afi,
                                          NetAddress.u2Safi,
                                          &IpAddr,
                                          NetAddress.u2PrefixLen, i4Action);
        if (i1RetVal == SNMP_FAILURE)
        {
            nmhSetFsbgp4mpeCommSetStatusRowStatus (NetAddress.NetAddr.u2Afi,
                                                   NetAddress.u2Safi,
                                                   &IpAddr,
                                                   NetAddress.u2PrefixLen,
                                                   DESTROY);
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);

            IssSendError (pHttp, (CONST INT1 *)
                          "\r Unable to set the status\r\n");
            return;

        }

        i1RetVal =
            nmhTestv2Fsbgp4mpeCommSetStatusRowStatus (&u4ErrorCode,
                                                      NetAddress.NetAddr.
                                                      u2Afi,
                                                      NetAddress.u2Safi,
                                                      &IpAddr,
                                                      NetAddress.
                                                      u2PrefixLen, ACTIVE);
        if (i1RetVal == SNMP_FAILURE)
        {
            nmhSetFsbgp4mpeCommSetStatusRowStatus (NetAddress.NetAddr.u2Afi,
                                                   NetAddress.u2Safi,
                                                   &IpAddr,
                                                   NetAddress.u2PrefixLen,
                                                   DESTROY);
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);

            IssSendError (pHttp, (CONST INT1 *)
                          "\r Unable to delete the entry\r\n");
            return;

        }

        i1RetVal =
            nmhSetFsbgp4mpeCommSetStatusRowStatus (NetAddress.NetAddr.u2Afi,
                                                   NetAddress.u2Safi,
                                                   &IpAddr,
                                                   NetAddress.u2PrefixLen,
                                                   ACTIVE);
        if (i1RetVal == SNMP_FAILURE)
        {
            nmhSetFsbgp4mpeCommSetStatusRowStatus (NetAddress.NetAddr.u2Afi,
                                                   NetAddress.u2Safi,
                                                   &IpAddr,
                                                   NetAddress.u2PrefixLen,
                                                   DESTROY);
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);

            IssSendError (pHttp, (CONST INT1 *)
                          "\r Unable to set the row status\r\n");
            return;

        }
    }

    else if (u1Set == ISS_BGP4_DELETE)
    {
        /* Entry present in Comm Peer Table. Need to Delete */
        i1RetVal =
            nmhTestv2Fsbgp4mpeCommSetStatusRowStatus (&u4ErrorCode,
                                                      NetAddress.NetAddr.
                                                      u2Afi,
                                                      NetAddress.u2Safi,
                                                      &IpAddr,
                                                      NetAddress.
                                                      u2PrefixLen, DESTROY);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);

            IssSendError (pHttp, (CONST INT1 *)
                          "\r Invalid row status to the table\r\n");
            return;

        }

        i1RetVal =
            nmhSetFsbgp4mpeCommSetStatusRowStatus (NetAddress.NetAddr.u2Afi,
                                                   NetAddress.u2Safi,
                                                   &IpAddr,
                                                   NetAddress.u2PrefixLen,
                                                   DESTROY);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);

            IssSendError (pHttp, (CONST INT1 *)
                          "\r Unable to delete the entry\r\n");
            return;

        }

    }
    else
    {
        IssSendError (pHttp, (CONST INT1 *) "\rInvalid action \r\n");
    }
    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);

    IssProcessCommBgpLocPolicyConfPageGet (pHttp);
    return;

}

/*********************************************************************
 *  Function Name : IssProcessBgpCommRouteConfPage
 *  Description   : This function processes the request coming for the
 *                  BGP Community Route settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpCommRouteConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessBgpCommRouteConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessBgpCommRouteConfPageSet (pHttp);
    }
}

/*********************************************************************
 *  Function Name : IssProcessBgpCommRouteConfPageGet
 *  Description   : This function processes the GET request coming for the
 *                  BGP Community Route settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 **********************************************************************/
VOID
IssProcessBgpCommRouteConfPageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    UINT4               u4IpAddress = 0;
    UINT4               u4Fsbgp4CommVal = 0;
    INT4                i4Fsbgp4CommRtAfi = 0;
    INT4                i4Fsbgp4CommRtSafi = 0;
    INT4                i4Fsbgp4CommIpPrefixLen = 0;
    INT4                i4TableValue = ISS_BGP4_COMM_ROUTE_ADD;
    INT4                i4OutCome = SNMP_FAILURE;
    INT4                i4BgpContextId = 0;
    INT4                i4BgpPrevContextId = 0;
    UINT1              *pu1TempString = NULL;
    UINT1               au1Ip6Addr[ISS_BGP4_IP6_ADDR_STRING_LEN];
    tSNMP_OCTET_STRING_TYPE Fsbgp4CommIpNetwork;
    UINT1               au1LocalIpAddress[ISS_BGP4_IP_MAX_LENGTH];
    UINT1               au1BgpCxtName[VCM_ALIAS_MAX_LEN];

    MEMSET (au1LocalIpAddress, 0, ISS_BGP4_IP_MAX_LENGTH);
    MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
    MEMSET (au1BgpCxtName, 0, VCM_ALIAS_MAX_LEN);

    pHttp->i4Write = 0;
    IssPrintAvailableBgpContextId (pHttp);

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    if (nmhGetFirstIndexFsMIBgpContextTable (&i4BgpContextId) == SNMP_FAILURE)
    {
        pHttp->i4Write = (INT4) u4Temp;
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    Fsbgp4CommIpNetwork.pu1_OctetList = au1LocalIpAddress;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        if (BgpSetContext (i4BgpContextId) == SNMP_FAILURE)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
            return;
        }

        /* Process the BGP Community Route Add table */
        i4TableValue = ISS_BGP4_COMM_ROUTE_ADD;
        i4OutCome =
            nmhGetFirstIndexFsbgp4MpeCommRouteAddCommTable (&i4Fsbgp4CommRtAfi,
                                                            &i4Fsbgp4CommRtSafi,
                                                            &Fsbgp4CommIpNetwork,
                                                            &i4Fsbgp4CommIpPrefixLen,
                                                            &u4Fsbgp4CommVal);
        if (i4OutCome == SNMP_SUCCESS)
        {
            do
            {
                pHttp->i4Write = (INT4) u4Temp;

                STRCPY (pHttp->au1KeyString, "IP_ADDRESS");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                if (i4Fsbgp4CommRtAfi == BGP4_INET_AFI_IPV4)
                {
                    MEMCPY (&u4IpAddress,
                            Fsbgp4CommIpNetwork.pu1_OctetList,
                            Fsbgp4CommIpNetwork.i4_Length);
                    WEB_CONVERT_IPADDR_TO_STR (pu1TempString,
                                               OSIX_NTOHL (u4IpAddress));
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                             pu1TempString);
                }
#ifdef BGP4_IPV6_WANTED
                else
                {
                    Bgp4PrintIp6Addr (Fsbgp4CommIpNetwork.pu1_OctetList,
                                      (au1Ip6Addr));
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Ip6Addr);
                    MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
                }
#endif /* BGP4_IPV6_WANTED */
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "PREFIX_LENGTH");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         i4Fsbgp4CommIpPrefixLen);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "COMM_VALUE");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4Fsbgp4CommVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "ROUTE_TABLE_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4TableValue);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                VcmGetAliasName ((UINT4) i4BgpContextId, au1BgpCxtName);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1BgpCxtName);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            }
            while (nmhGetNextIndexFsbgp4MpeCommRouteAddCommTable (i4Fsbgp4CommRtAfi, &i4Fsbgp4CommRtAfi, i4Fsbgp4CommRtSafi, &i4Fsbgp4CommRtSafi, &Fsbgp4CommIpNetwork, &Fsbgp4CommIpNetwork, i4Fsbgp4CommIpPrefixLen, &i4Fsbgp4CommIpPrefixLen, u4Fsbgp4CommVal, &u4Fsbgp4CommVal) != SNMP_FAILURE);    /*end of do-while */
        }

        /* Process the BGP Community Route Delete table */
        i4TableValue = ISS_BGP4_COMM_ROUTE_DELETE;
        i4OutCome =
            nmhGetFirstIndexFsbgp4MpeCommRouteDeleteCommTable
            (&i4Fsbgp4CommRtAfi, &i4Fsbgp4CommRtSafi, &Fsbgp4CommIpNetwork,
             &i4Fsbgp4CommIpPrefixLen, &u4Fsbgp4CommVal);
        if (i4OutCome == SNMP_FAILURE)
        {
            i4BgpPrevContextId = i4BgpContextId;
            if (nmhGetNextIndexFsMIBgpContextTable (i4BgpPrevContextId,
                                                    &i4BgpContextId) !=
                SNMP_SUCCESS)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
                WebnmSockWrite (pHttp,
                                (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                                (pHttp->i4HtmlSize - pHttp->i4Write));

                return;
            }
            continue;

        }

        do
        {
            pHttp->i4Write = (INT4) u4Temp;

            STRCPY (pHttp->au1KeyString, "IP_ADDRESS");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (i4Fsbgp4CommRtAfi == BGP4_INET_AFI_IPV4)
            {
                MEMCPY (&u4IpAddress,
                        Fsbgp4CommIpNetwork.pu1_OctetList,
                        Fsbgp4CommIpNetwork.i4_Length);
                WEB_CONVERT_IPADDR_TO_STR (pu1TempString,
                                           OSIX_NTOHL (u4IpAddress));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1TempString);
            }
#ifdef BGP4_IPV6_WANTED
            else                /* IPV6 formatting */
            {
                Bgp4PrintIp6Addr (Fsbgp4CommIpNetwork.pu1_OctetList,
                                  (au1Ip6Addr));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Ip6Addr);
                MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
            }
#endif /* BGP4_IPV6_WANTED */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "PREFIX_LENGTH");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                     i4Fsbgp4CommIpPrefixLen);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "COMM_VALUE");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4Fsbgp4CommVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "ROUTE_TABLE_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4TableValue);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            VcmGetAliasName ((UINT4) i4BgpContextId, au1BgpCxtName);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1BgpCxtName);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        }
        while (nmhGetNextIndexFsbgp4MpeCommRouteDeleteCommTable (i4Fsbgp4CommRtAfi, &i4Fsbgp4CommRtAfi, i4Fsbgp4CommRtSafi, &i4Fsbgp4CommRtSafi, &Fsbgp4CommIpNetwork, &Fsbgp4CommIpNetwork, i4Fsbgp4CommIpPrefixLen, &i4Fsbgp4CommIpPrefixLen, u4Fsbgp4CommVal, &u4Fsbgp4CommVal) == SNMP_SUCCESS);    /*end of do-while */

        i4BgpPrevContextId = i4BgpContextId;

    }
    while (nmhGetNextIndexFsMIBgpContextTable
           (i4BgpPrevContextId, &i4BgpContextId) == SNMP_SUCCESS);

    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    return;

}

/**********************************************************************
 *  Function Name : IssProcessBgpCommRouteConfPageSet
 *  Description   : This function processes the  SET request coming for the
 *                  BGP Community Route settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpCommRouteConfPageSet (tHttp * pHttp)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4IpMask = 0;
    UINT4               u4CommValue = 0;
    INT4                i4RowStatus = 0;
    INT4                i4Len = 0;
    INT4                i4Action = 0;
    INT4                i4CxtId = 0;
    UINT1               u1Set = 0;
    INT1                i1RetVal = 0;
    tSNMP_OCTET_STRING_TYPE IpAddr;
    tNetAddress         NetAddress;
    tUtlInAddr          InAddr;
    UINT1               au1LocalIpAddress[ISS_BGP4_IP_MAX_LENGTH];

    MEMSET (au1LocalIpAddress, 0, ISS_BGP4_IP_MAX_LENGTH);
    MEMSET (&NetAddress, 0, sizeof (tNetAddress));

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    if (0 == STRCMP (pHttp->au1Value, "Delete"))
    {
        STRCPY (pHttp->au1Name, "CONTEXT_ID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        VcmIsVrfExist (pHttp->au1Value, (UINT4 *) &i4CxtId);
    }
    else
    {
        STRCPY (pHttp->au1Name, "CONTEXT_ID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4CxtId = ATOI (pHttp->au1Value);
    }

    if (BgpSetContext ((INT4) i4CxtId) == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
        return;
    }

    STRCPY (pHttp->au1Name, "PrefixLength");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4IpMask = (UINT4) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "CommunityValue");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4CommValue = (UINT4) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "RouteTable");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Action = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    if (0 == STRCMP (pHttp->au1Value, "Delete"))
    {
        u1Set = ISS_BGP4_DELETE;
    }
    else
    {
        u1Set = ISS_BGP4_APPLY;
    }

    STRCPY (pHttp->au1Name, "IpAddress");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);

    i4Len = CliDotStrLength (pHttp->au1Value);
    if (i4Len == sizeof (UINT4))
    {
        NetAddress.NetAddr.u2Afi = BGP4_INET_AFI_IPV4;
        NetAddress.u2Safi = BGP4_INET_SAFI_UNICAST;
        if (0 == BGP4_INET_ATON (pHttp->au1Value, &InAddr))
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Invalid Address\r\n");
            return;
        }
        MEMCPY ((NetAddress.NetAddr.au1Address),
                &InAddr.u4Addr, sizeof (UINT4));

        NetAddress.NetAddr.u2AddressLen = sizeof (UINT4);
        NetAddress.u2PrefixLen = (UINT2) u4IpMask;

    }
#ifdef BGP4_IPV6_WANTED
    else if (str_to_ip6addr (pHttp->au1Value) != NULL)
    {
        NetAddress.NetAddr.u2Afi = BGP4_INET_AFI_IPV6;
        NetAddress.u2Safi = BGP4_INET_SAFI_UNICAST;
        MEMCPY (NetAddress.NetAddr.au1Address,
                str_to_ip6addr (pHttp->au1Value), BGP4_IPV6_PREFIX_LEN);

        NetAddress.NetAddr.u2AddressLen = BGP4_IPV6_PREFIX_LEN;

        NetAddress.u2PrefixLen = (UINT2) u4IpMask;

    }
#endif /* BGP4_IPV6_WANTED */
    IpAddr.i4_Length = NetAddress.NetAddr.u2AddressLen;
    IpAddr.pu1_OctetList = au1LocalIpAddress;
    MEMCPY ((IpAddr.pu1_OctetList),
            (NetAddress.NetAddr.au1Address), NetAddress.NetAddr.u2AddressLen);

    if (i4Action == BGP4_COMM_ADD_TBL)
    {
        i1RetVal =
            nmhValidateIndexInstanceFsbgp4MpeCommRouteAddCommTable (NetAddress.
                                                                    NetAddr.
                                                                    u2Afi,
                                                                    NetAddress.
                                                                    u2Safi,
                                                                    &IpAddr,
                                                                    NetAddress.
                                                                    u2PrefixLen,
                                                                    u4CommValue);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Invalid indices into CommRouteAddTable\r\n");
            return;
        }
        i1RetVal =
            nmhGetFsbgp4mpeAddCommRowStatus (NetAddress.NetAddr.u2Afi,
                                             NetAddress.u2Safi,
                                             &IpAddr,
                                             NetAddress.u2PrefixLen,
                                             u4CommValue, &i4RowStatus);
    }
    else
    {
        i1RetVal =
            nmhValidateIndexInstanceFsbgp4MpeCommRouteDeleteCommTable
            (NetAddress.NetAddr.u2Afi, NetAddress.u2Safi, &IpAddr,
             NetAddress.u2PrefixLen, u4CommValue);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Invalid indices into CommRouteDeleteTable\r\n");
            return;
        }
        i1RetVal =
            nmhGetFsbgp4mpeDeleteCommRowStatus (NetAddress.NetAddr.u2Afi,
                                                NetAddress.u2Safi,
                                                &IpAddr,
                                                NetAddress.u2PrefixLen,
                                                u4CommValue, &i4RowStatus);

    }
    if ((i1RetVal == SNMP_FAILURE) && (u1Set == ISS_BGP4_DELETE))
    {
        /* Trying to delete a non-existing Entry */
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *)
                      "Deleting a non-existing entry\r\n");
        return;
    }

    if ((i1RetVal != SNMP_FAILURE) && (i4RowStatus == ACTIVE) &&
        (u1Set == ISS_BGP4_APPLY))
    {
        /* Entry already exits and active */
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Entry exists and active\r\n");
        return;
    }

    if (u1Set == ISS_BGP4_APPLY)
    {
        /* i1RetVal derived from return value of
         * nmhGetFsbgp4DeleteCommRowStatus/nmhGetFsbgp4AddCommRowStatus */
        if (i1RetVal == SNMP_FAILURE)
        {
            /* Entry not present in Comm Add/Delete Table. Need to create */
            if (i4Action == BGP4_COMM_ADD_TBL)
            {
                i1RetVal =
                    nmhTestv2Fsbgp4mpeAddCommRowStatus (&u4ErrorCode,
                                                        NetAddress.NetAddr.
                                                        u2Afi,
                                                        NetAddress.u2Safi,
                                                        &IpAddr,
                                                        NetAddress.
                                                        u2PrefixLen,
                                                        u4CommValue,
                                                        CREATE_AND_GO);
                if (i1RetVal == SNMP_FAILURE)
                {
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *)
                                  "Invalid row status\r\n");
                    return;
                }
                i1RetVal =
                    nmhSetFsbgp4mpeAddCommRowStatus (NetAddress.NetAddr.
                                                     u2Afi,
                                                     NetAddress.u2Safi,
                                                     &IpAddr,
                                                     NetAddress.u2PrefixLen,
                                                     u4CommValue,
                                                     CREATE_AND_GO);
                if (i1RetVal == SNMP_FAILURE)
                {
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *)
                                  "Unable to set the row status\r\n");
                    return;
                }
            }
            else
            {
                i1RetVal =
                    nmhTestv2Fsbgp4mpeDeleteCommRowStatus (&u4ErrorCode,
                                                           NetAddress.
                                                           NetAddr.u2Afi,
                                                           NetAddress.
                                                           u2Safi, &IpAddr,
                                                           NetAddress.
                                                           u2PrefixLen,
                                                           u4CommValue,
                                                           CREATE_AND_GO);
                if (i1RetVal == SNMP_FAILURE)
                {
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *)
                                  "Invalid row status\r\n");
                    return;
                }
                i1RetVal =
                    nmhSetFsbgp4mpeDeleteCommRowStatus (NetAddress.NetAddr.
                                                        u2Afi,
                                                        NetAddress.u2Safi,
                                                        &IpAddr,
                                                        NetAddress.
                                                        u2PrefixLen,
                                                        u4CommValue,
                                                        CREATE_AND_GO);
                if (i1RetVal == SNMP_FAILURE)
                {
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *)
                                  "Unable to set the row status\r\n");
                    return;
                }

            }

        }
        else
        {
            /* Already Entry is present. But not Active */
            if (i4Action == BGP4_COMM_ADD_TBL)
            {
                i1RetVal =
                    nmhTestv2Fsbgp4mpeAddCommRowStatus (&u4ErrorCode,
                                                        NetAddress.NetAddr.
                                                        u2Afi,
                                                        NetAddress.u2Safi,
                                                        &IpAddr,
                                                        NetAddress.
                                                        u2PrefixLen,
                                                        u4CommValue, ACTIVE);
                if (i1RetVal == SNMP_FAILURE)
                {
                    nmhSetFsbgp4mpeAddCommRowStatus (NetAddress.NetAddr.
                                                     u2Afi,
                                                     NetAddress.u2Safi,
                                                     &IpAddr,
                                                     NetAddress.u2PrefixLen,
                                                     u4CommValue, DESTROY);
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *)
                                  "Unable to set the row status\r\n");
                    return;
                }
            }
            else
            {
                i1RetVal =
                    nmhTestv2Fsbgp4mpeDeleteCommRowStatus (&u4ErrorCode,
                                                           NetAddress.
                                                           NetAddr.u2Afi,
                                                           NetAddress.
                                                           u2Safi, &IpAddr,
                                                           NetAddress.
                                                           u2PrefixLen,
                                                           u4CommValue, ACTIVE);
                if (i1RetVal == SNMP_FAILURE)
                {
                    nmhSetFsbgp4mpeDeleteCommRowStatus (NetAddress.NetAddr.
                                                        u2Afi,
                                                        NetAddress.u2Safi,
                                                        &IpAddr,
                                                        NetAddress.
                                                        u2PrefixLen,
                                                        u4CommValue, DESTROY);
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *)
                                  "Invalid row status\r\n");
                    return;
                }
            }
        }

        if (i4Action == BGP4_COMM_ADD_TBL)
        {
            i1RetVal =
                nmhSetFsbgp4mpeAddCommRowStatus (NetAddress.NetAddr.u2Afi,
                                                 NetAddress.u2Safi,
                                                 &IpAddr,
                                                 NetAddress.u2PrefixLen,
                                                 u4CommValue, ACTIVE);
            if (i1RetVal == SNMP_FAILURE)
            {
                nmhSetFsbgp4mpeAddCommRowStatus (NetAddress.NetAddr.u2Afi,
                                                 NetAddress.u2Safi,
                                                 &IpAddr,
                                                 NetAddress.u2PrefixLen,
                                                 u4CommValue, DESTROY);
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to set the row status\r\n");
                return;
            }
        }
        else
        {
            i1RetVal =
                nmhSetFsbgp4mpeDeleteCommRowStatus (NetAddress.NetAddr.
                                                    u2Afi,
                                                    NetAddress.u2Safi,
                                                    &IpAddr,
                                                    NetAddress.u2PrefixLen,
                                                    u4CommValue, ACTIVE);
            if (i1RetVal == SNMP_FAILURE)
            {
                nmhSetFsbgp4mpeDeleteCommRowStatus (NetAddress.NetAddr.
                                                    u2Afi,
                                                    NetAddress.u2Safi,
                                                    &IpAddr,
                                                    NetAddress.u2PrefixLen,
                                                    u4CommValue, DESTROY);
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to set the row status\r\n");
                return;
            }
        }
    }

    else if (u1Set == ISS_BGP4_DELETE)
    {
        /* Entry present in Comm Add/Delete  Table. Need to Delete */
        if (i4Action == BGP4_COMM_ADD_TBL)
        {
            i1RetVal =
                nmhTestv2Fsbgp4mpeAddCommRowStatus (&u4ErrorCode,
                                                    NetAddress.NetAddr.
                                                    u2Afi,
                                                    NetAddress.u2Safi,
                                                    &IpAddr,
                                                    NetAddress.u2PrefixLen,
                                                    u4CommValue, DESTROY);
            if (i1RetVal == SNMP_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Invalid row status\r\n");
                return;
            }
            i1RetVal =
                nmhSetFsbgp4mpeAddCommRowStatus (NetAddress.NetAddr.u2Afi,
                                                 NetAddress.u2Safi,
                                                 &IpAddr,
                                                 NetAddress.u2PrefixLen,
                                                 u4CommValue, DESTROY);
            if (i1RetVal == SNMP_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to set the row status\r\n");
                return;

            }
        }
        else
        {
            i1RetVal =
                nmhTestv2Fsbgp4mpeDeleteCommRowStatus (&u4ErrorCode,
                                                       NetAddress.NetAddr.
                                                       u2Afi,
                                                       NetAddress.u2Safi,
                                                       &IpAddr,
                                                       NetAddress.
                                                       u2PrefixLen,
                                                       u4CommValue, DESTROY);
            if (i1RetVal == SNMP_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to set the row status\r\n");
                return;
            }
            i1RetVal =
                nmhSetFsbgp4mpeDeleteCommRowStatus (NetAddress.NetAddr.
                                                    u2Afi,
                                                    NetAddress.u2Safi,
                                                    &IpAddr,
                                                    NetAddress.u2PrefixLen,
                                                    u4CommValue, DESTROY);
            if (i1RetVal == SNMP_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to set the row status\r\n");
                return;
            }
        }
    }
    else
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Type\r\n");
    }
    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessBgpCommRouteConfPageGet (pHttp);
    return;

}

/*********************************************************************
 *  Function Name : IssProcessBgpECommFilterConfPage
 *  Description   : This function processes the request coming for the
 *                  BGP Community settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpECommFilterConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessBgpECommFilterConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessBgpECommFilterConfPageSet (pHttp);
    }
}

/*********************************************************************
 *  Function Name : IssProcessBgpECommFilterConfPageGet
 *  Description   : This function processes the GET request coming for the
 *                  BGP Extended Community Filter settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpECommFilterConfPageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    INT4                i4BgpContextId = 0;
    INT4                i4BgpPrevContextId = 0;
    INT4                i4Status = 0;
    INT4                i4FilterTable = ISS_BGP4_ECOMM_OUT_FLT_TBL;
    INT4                i4OutCome = SNMP_FAILURE;
    UINT1               au1TempString[ISS_BGP4_EXT_COMM_VALUE_LEN];
    tSNMP_OCTET_STRING_TYPE ECommValue;
    UINT1               au1LocalIpAddress[ISS_BGP4_EXT_COMM_VALUE_LEN];
    UINT1               au1BgpCxtName[VCM_ALIAS_MAX_LEN];

    MEMSET (au1LocalIpAddress, 0, ISS_BGP4_EXT_COMM_VALUE_LEN);
    MEMSET (au1BgpCxtName, 0, VCM_ALIAS_MAX_LEN);

    pHttp->i4Write = 0;
    IssPrintAvailableBgpContextId (pHttp);

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    if (nmhGetFirstIndexFsMIBgpContextTable (&i4BgpContextId) == SNMP_FAILURE)
    {
        pHttp->i4Write = (INT4) u4Temp;
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    ECommValue.pu1_OctetList = au1LocalIpAddress;
    MEMSET (au1TempString, 0, sizeof (au1TempString));
    MEMCPY (au1TempString, "NONE", 4);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        if (BgpSetContext (i4BgpContextId) == SNMP_FAILURE)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
            return;
        }

        /* Process the Ext Communities Out Filter Table */
        i4FilterTable = ISS_BGP4_ECOMM_OUT_FLT_TBL;
        i4OutCome = nmhGetFirstIndexFsbgp4ExtCommOutFilterTable (&ECommValue);
        if (i4OutCome == SNMP_SUCCESS)
        {
            do
            {
                pHttp->i4Write = (INT4) u4Temp;

                au1TempString[0] = (UINT4) ECommValue.pu1_OctetList[0];
                au1TempString[1] = (UINT4) ECommValue.pu1_OctetList[1];
                au1TempString[2] = (UINT4) ECommValue.pu1_OctetList[2];
                au1TempString[3] = (UINT4) ECommValue.pu1_OctetList[3];
                au1TempString[4] = (UINT4) ECommValue.pu1_OctetList[4];
                au1TempString[5] = (UINT4) ECommValue.pu1_OctetList[5];
                au1TempString[6] = (UINT4) ECommValue.pu1_OctetList[6];
                au1TempString[7] = (UINT4) ECommValue.pu1_OctetList[7];
                STRCPY (pHttp->au1KeyString, "COMM_VALUE");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString,
                         "%x:%x:%x:%x:%x:%x:%x:%x", au1TempString[0],
                         au1TempString[1], au1TempString[2], au1TempString[3],
                         au1TempString[4], au1TempString[5], au1TempString[6],
                         au1TempString[7]);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                nmhGetFsbgp4ExtCommOutgoingFilterStatus (&ECommValue,
                                                         &i4Status);
                STRCPY (pHttp->au1KeyString, "FILTER_STATUS");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Status);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "FILTER_TYPE");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4FilterTable);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                VcmGetAliasName ((UINT4) i4BgpContextId, au1BgpCxtName);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1BgpCxtName);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            }
            while (nmhGetNextIndexFsbgp4ExtCommOutFilterTable (&ECommValue, &ECommValue) == SNMP_SUCCESS);    /* end of do-while */
        }
        /* Process the Ext Comm In filter table */
        i4FilterTable = ISS_BGP4_ECOMM_IN_FLT_TBL;
        i4OutCome = nmhGetFirstIndexFsbgp4ExtCommInFilterTable (&ECommValue);
        if (i4OutCome == SNMP_FAILURE)
        {
            i4BgpPrevContextId = i4BgpContextId;
            if (nmhGetNextIndexFsMIBgpContextTable (i4BgpPrevContextId,
                                                    &i4BgpContextId) !=
                SNMP_SUCCESS)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
                WebnmSockWrite (pHttp,
                                (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                                (pHttp->i4HtmlSize - pHttp->i4Write));

                return;
            }
            continue;

        }
        do
        {
            pHttp->i4Write = (INT4) u4Temp;

            au1TempString[0] = (UINT4) ECommValue.pu1_OctetList[0];
            au1TempString[1] = (UINT4) ECommValue.pu1_OctetList[1];
            au1TempString[2] = (UINT4) ECommValue.pu1_OctetList[2];
            au1TempString[3] = (UINT4) ECommValue.pu1_OctetList[3];
            au1TempString[4] = (UINT4) ECommValue.pu1_OctetList[4];
            au1TempString[5] = (UINT4) ECommValue.pu1_OctetList[5];
            au1TempString[6] = (UINT4) ECommValue.pu1_OctetList[6];
            au1TempString[7] = (UINT4) ECommValue.pu1_OctetList[7];
            STRCPY (pHttp->au1KeyString, "COMM_VALUE");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%x:%x:%x:%x:%x:%x:%x:%x",
                     au1TempString[0], au1TempString[1], au1TempString[2],
                     au1TempString[3], au1TempString[4], au1TempString[5],
                     au1TempString[6], au1TempString[7]);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4ExtCommIncomingFilterStatus (&ECommValue, &i4Status);
            STRCPY (pHttp->au1KeyString, "FILTER_STATUS");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Status);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "FILTER_TYPE");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4FilterTable);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            VcmGetAliasName ((UINT4) i4BgpContextId, au1BgpCxtName);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1BgpCxtName);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        }
        while (nmhGetNextIndexFsbgp4ExtCommInFilterTable (&ECommValue, &ECommValue) == SNMP_SUCCESS);    /*end of do-while */

        i4BgpPrevContextId = i4BgpContextId;

    }
    while (nmhGetNextIndexFsMIBgpContextTable
           (i4BgpPrevContextId, &i4BgpContextId) == SNMP_SUCCESS);

    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    return;

}

/*********************************************************************
 *  Function Name : IssProcessBgpECommFilterConfPageSet
 *  Description   : This function processes the SET request coming for the
 *                  BGP Extended Community Filter settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpECommFilterConfPageSet (tHttp * pHttp)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4CxtId = 0;
    INT4                i4Action = 0;
    INT4                i4RowStatus = 0;
    INT4                i4Direction = 0;
    UINT1               u1EcommVal[ECOMM_LEN];
    UINT1               u1Set = 0;
    UINT1               au1EcommVal[30];
    INT1                i1RetVal = 0;
    tBgp4Ecomm         *pBgp4EcommTbl;
    tBgp4Ecomm          tempBgp4EcommTbl;
    UINT4               u4StrLength;
    tSNMP_OCTET_STRING_TYPE EcommValue;

    MEMSET (&tempBgp4EcommTbl, 0, sizeof (tBgp4Ecomm));

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if ((0 == STRCMP (pHttp->au1Value, "Delete"))
        || (0 == STRCMP (pHttp->au1Value, "Apply")))
    {
        STRCPY (pHttp->au1Name, "CONTEXT_ID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        VcmIsVrfExist (pHttp->au1Value, (UINT4 *) &i4CxtId);
    }
    else
    {
        STRCPY (pHttp->au1Name, "CONTEXT_ID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4CxtId = ATOI (pHttp->au1Value);
    }

    if (BgpSetContext ((INT4) i4CxtId) == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
        return;
    }

    STRCPY (pHttp->au1Name, "FilterStatus");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Action = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "FilterTable");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Direction = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (0 == STRCMP (pHttp->au1Value, "Delete"))
    {
        u1Set = ISS_BGP_COMM_FILTER_DELETE;
    }
    else
    {
        u1Set = ISS_BGP_COMM_FILTER_ADD;
    }

    STRCPY (pHttp->au1Name, "CommunityValue");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    /*u4CommValue = (UINT4) ATOI (pHttp->au1Value); */

    issDecodeSpecialChar (pHttp->au1Value);
    pBgp4EcommTbl = (tBgp4Ecomm *) (VOID *) &tempBgp4EcommTbl;
    MEMSET (pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal, 0,
            ECOMM_LEN + ECOMM_EXT_LEN);
    u4StrLength = STRLEN (pHttp->au1Value) + 1;
    if (u4StrLength > 30)
    {
        u4StrLength = 30;
    }
    MEMCPY (au1EcommVal, pHttp->au1Value, u4StrLength);
    au1EcommVal[u4StrLength - 1] = 0;

    if ((sscanf
         ((const char *) pHttp->au1Value, "%2x:%2x:%2x:%2x:%2x:%2x:%2x:%2x%x",
          &(pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[0]),
          &(pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[1]),
          &(pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[2]),
          &(pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[3]),
          &(pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[4]),
          &(pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[5]),
          &(pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[6]),
          &(pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[7]),
          &(pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[8])) !=
         ECOMM_LEN)
        || (pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[8] != 0))

    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Input valid ecomm-values\r\n");
        return;
    }

    /* Copy the received Ecomm value. */
    u1EcommVal[0] =
        (UINT1) pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[0];
    u1EcommVal[1] =
        (UINT1) pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[1];
    u1EcommVal[2] =
        (UINT1) pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[2];
    u1EcommVal[3] =
        (UINT1) pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[3];
    u1EcommVal[4] =
        (UINT1) pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[4];
    u1EcommVal[5] =
        (UINT1) pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[5];
    u1EcommVal[6] =
        (UINT1) pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[6];
    u1EcommVal[7] =
        (UINT1) pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[7];

    /* Extract Ecomm Value */
    EcommValue.pu1_OctetList = u1EcommVal;
    EcommValue.i4_Length = ECOMM_LEN;

    if (i4Direction == ISS_BGP4_ECOMM_IN_FLT_TBL)
    {
        i1RetVal =
            nmhValidateIndexInstanceFsbgp4ExtCommInFilterTable (&EcommValue);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "\rInvalid indices \r\n");
            return;
        }
        i1RetVal =
            nmhGetFsbgp4ExtCommInFilterRowStatus (&EcommValue, &i4RowStatus);
    }
    else
    {
        i1RetVal =
            nmhValidateIndexInstanceFsbgp4ExtCommOutFilterTable (&EcommValue);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "\rInvalid indices \r\n");
            return;
        }
        i1RetVal =
            nmhGetFsbgp4ExtCommOutFilterRowStatus (&EcommValue, &i4RowStatus);

    }

    /*Trying to delete a non-existing entry */
    if ((i1RetVal == SNMP_FAILURE) && (u1Set == ISS_BGP_COMM_FILTER_DELETE))
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *)
                      "\rTrying to delete a non-existing entry\r\n");
        return;
    }
    if (u1Set == ISS_BGP_ECOMM_FILTER_ADD)
    {
        /* i1RetVal derived from return value of
         * nmhGetFsbgp4ExtCommInFilterRowStatus or
         * nmhGetFsbgp4ExtCommOutFilterRowStatus */
        if (i1RetVal == SNMP_FAILURE)
        {
            /* Entry not present in Ext Comm IN/OUT filter Table.
             * Need to create a entry, set the input action and
             * activate the entry. */
            if (i4Direction == ISS_BGP4_ECOMM_IN_FLT_TBL)
            {
                i1RetVal =
                    nmhTestv2Fsbgp4ExtCommInFilterRowStatus (&u4ErrorCode,
                                                             &EcommValue,
                                                             CREATE_AND_WAIT);
                if (i1RetVal == SNMP_FAILURE)
                {
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *)
                                  "\rInvalid row status entry \r\n");
                    return;
                }
                i1RetVal =
                    nmhSetFsbgp4ExtCommInFilterRowStatus (&EcommValue,
                                                          CREATE_AND_WAIT);
                if (i1RetVal == SNMP_FAILURE)
                {
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *)
                                  "\rUnable to set row status entry \r\n");
                    return;
                }

            }
            else
            {
                i1RetVal =
                    nmhTestv2Fsbgp4ExtCommOutFilterRowStatus (&u4ErrorCode,
                                                              &EcommValue,
                                                              CREATE_AND_WAIT);
                if (i1RetVal == SNMP_FAILURE)
                {
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *)
                                  "\rInvalid row status entry \r\n");
                    return;
                }
                i1RetVal =
                    nmhSetFsbgp4ExtCommOutFilterRowStatus (&EcommValue,
                                                           CREATE_AND_WAIT);
                if (i1RetVal == SNMP_FAILURE)
                {
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *)
                                  "\rUnable to set row status entry \r\n");
                    return;
                }
            }
        }
        else
        {
            /* Already Entry is present. First make the entry row status
             * not in service, set the input action and activate the
             * entry */
            if (i4Direction == ISS_BGP4_ECOMM_IN_FLT_TBL)
            {
                i1RetVal =
                    nmhTestv2Fsbgp4ExtCommInFilterRowStatus (&u4ErrorCode,
                                                             &EcommValue,
                                                             NOT_IN_SERVICE);
                if (i1RetVal == SNMP_FAILURE)
                {
                    nmhSetFsbgp4ExtCommInFilterRowStatus (&EcommValue, DESTROY);
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *)
                                  "\rInvalid row status entry \r\n");
                    return;
                }
                i1RetVal =
                    nmhSetFsbgp4ExtCommInFilterRowStatus (&EcommValue,
                                                          NOT_IN_SERVICE);
                if (i1RetVal == SNMP_FAILURE)
                {
                    nmhSetFsbgp4ExtCommInFilterRowStatus (&EcommValue, DESTROY);
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *)
                                  "\rUnable to set row status entry \r\n");
                    return;
                }
            }
            else
            {
                i1RetVal =
                    nmhTestv2Fsbgp4ExtCommOutFilterRowStatus (&u4ErrorCode,
                                                              &EcommValue,
                                                              NOT_IN_SERVICE);
                if (i1RetVal == SNMP_FAILURE)
                {
                    nmhSetFsbgp4ExtCommOutFilterRowStatus (&EcommValue,
                                                           DESTROY);
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *)
                                  "\rInvalid row status entry \r\n");
                    return;
                }
                i1RetVal =
                    nmhSetFsbgp4ExtCommOutFilterRowStatus (&EcommValue,
                                                           NOT_IN_SERVICE);
                if (i1RetVal == SNMP_FAILURE)
                {
                    nmhSetFsbgp4ExtCommOutFilterRowStatus (&EcommValue,
                                                           DESTROY);
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *)
                                  "\rUnable to set row status entry \r\n");
                    return;
                }
            }
        }
        /* Entry present and not active. Set the input action and
         * make the entry status as active */
        if (i4Direction == ISS_BGP4_ECOMM_IN_FLT_TBL)
        {
            i1RetVal =
                nmhTestv2Fsbgp4ExtCommIncomingFilterStatus (&u4ErrorCode,
                                                            &EcommValue,
                                                            i4Action);
            if (i1RetVal == SNMP_FAILURE)
            {
                nmhSetFsbgp4ExtCommInFilterRowStatus (&EcommValue, DESTROY);
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rInvalid row status entry \r\n");
                return;
            }
            i1RetVal =
                nmhSetFsbgp4ExtCommIncomingFilterStatus (&EcommValue, i4Action);
            if (i1RetVal == SNMP_FAILURE)
            {
                nmhSetFsbgp4ExtCommInFilterRowStatus (&EcommValue, DESTROY);
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to set row status entry \r\n");
                return;
            }
        }
        else
        {
            i1RetVal =
                nmhTestv2Fsbgp4ExtCommOutgoingFilterStatus (&u4ErrorCode,
                                                            &EcommValue,
                                                            i4Action);
            if (i1RetVal == SNMP_FAILURE)
            {
                nmhSetFsbgp4ExtCommOutFilterRowStatus (&EcommValue, DESTROY);
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rInvalid row status entry \r\n");
                return;
            }
            i1RetVal =
                nmhSetFsbgp4ExtCommOutgoingFilterStatus (&EcommValue, i4Action);
            if (i1RetVal == SNMP_FAILURE)
            {
                nmhSetFsbgp4ExtCommOutFilterRowStatus (&EcommValue, DESTROY);
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to set row status entry \r\n");
                return;
            }
        }
        if (i4Direction == ISS_BGP4_ECOMM_IN_FLT_TBL)
        {
            i1RetVal =
                nmhTestv2Fsbgp4ExtCommInFilterRowStatus (&u4ErrorCode,
                                                         &EcommValue, ACTIVE);
            if (i1RetVal == SNMP_FAILURE)
            {
                nmhSetFsbgp4ExtCommInFilterRowStatus (&EcommValue, DESTROY);
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rInvalid row status entry \r\n");
                return;
            }
            i1RetVal =
                nmhSetFsbgp4ExtCommInFilterRowStatus (&EcommValue, ACTIVE);
            if (i1RetVal == SNMP_FAILURE)
            {
                nmhSetFsbgp4ExtCommInFilterRowStatus (&EcommValue, DESTROY);
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to set row status entry \r\n");
                return;
            }
        }
        else
        {
            i1RetVal =
                nmhTestv2Fsbgp4ExtCommOutFilterRowStatus (&u4ErrorCode,
                                                          &EcommValue, ACTIVE);
            if (i1RetVal == SNMP_FAILURE)
            {
                nmhSetFsbgp4ExtCommOutFilterRowStatus (&EcommValue, DESTROY);
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rInvalid row status entry \r\n");
                return;
            }
            i1RetVal =
                nmhSetFsbgp4ExtCommOutFilterRowStatus (&EcommValue, ACTIVE);
            if (i1RetVal == SNMP_FAILURE)
            {
                nmhSetFsbgp4ExtCommOutFilterRowStatus (&EcommValue, DESTROY);
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to set row status entry \r\n");
                return;
            }
        }
    }

    else if (u1Set == ISS_BGP_ECOMM_FILTER_DELETE)
    {
        /* Entry present in Ext Comm IN/OUT filter Table. Need to Delete */
        if (i4Direction == ISS_BGP4_ECOMM_IN_FLT_TBL)
        {
            i1RetVal =
                nmhTestv2Fsbgp4ExtCommInFilterRowStatus (&u4ErrorCode,
                                                         &EcommValue, DESTROY);
            if (i1RetVal == SNMP_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rInvalid row status entry \r\n");
                return;
            }
            i1RetVal =
                nmhSetFsbgp4ExtCommInFilterRowStatus (&EcommValue, DESTROY);
            if (i1RetVal == SNMP_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to set row status entry \r\n");
                return;
            }
        }
        else
        {
            i1RetVal =
                nmhTestv2Fsbgp4ExtCommOutFilterRowStatus (&u4ErrorCode,
                                                          &EcommValue, DESTROY);
            if (i1RetVal == SNMP_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rInvalid row status entry \r\n");
                return;
            }
            i1RetVal =
                nmhSetFsbgp4ExtCommOutFilterRowStatus (&EcommValue, DESTROY);
            if (i1RetVal == SNMP_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to set row status entry \r\n");
                return;
            }
        }
    }
    else
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "\r Invalid action to be performed \r\n");
    }

    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessBgpECommFilterConfPageGet (pHttp);
    return;

}

/*********************************************************************
 *  Function Name : IssProcessBgpECommLocPolicyConfPage
 *  Description   : This function processes the request coming for the
 *                  BGP Extended communities local policy settings page
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpECommLocPolicyConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessBgpECommLocPolicyConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessBgpECommLocPolicyConfPageSet (pHttp);
    }
}

/*********************************************************************
 *  Function Name : IssProcessBgpECommLocPolicyConfPageGet
 *  Description   : This function processes the GET request coming for the
 *                  BGP Extended communities local policy settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpECommLocPolicyConfPageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    UINT4               u4IpAddress = 0;
    INT4                i4Fsbgp4ECommSetStatusAfi = 0;
    INT4                i4BgpContextId = 0;
    INT4                i4BgpPrevContextId = 0;
    INT4                i4Fsbgp4ECommSetStatusSafi = 0;
    INT4                i4Fsbgp4ECommSetStatusIpPrefixLen = 0;
    INT4                i4Fsbgp4ECommSetStatus = 0;
    INT4                i4RetVal = ENM_FAILURE;
    UINT1              *pu1TempString = NULL;
    UINT1               au1Ip6Addr[ISS_BGP4_IP6_ADDR_STRING_LEN];
    tSNMP_OCTET_STRING_TYPE Fsbgp4ECommSetStatusIpNetwork;
    UINT1               au1LocalIpAddress[ISS_BGP4_IP_MAX_LENGTH];
    UINT1               au1BgpCxtName[VCM_ALIAS_MAX_LEN];

    MEMSET (au1LocalIpAddress, 0, ISS_BGP4_IP_MAX_LENGTH);
    MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
    MEMSET (au1BgpCxtName, 0, VCM_ALIAS_MAX_LEN);

    pHttp->i4Write = 0;
    IssPrintAvailableBgpContextId (pHttp);

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    if (nmhGetFirstIndexFsMIBgpContextTable (&i4BgpContextId) == SNMP_FAILURE)
    {
        pHttp->i4Write = (INT4) u4Temp;
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    Fsbgp4ECommSetStatusIpNetwork.pu1_OctetList = au1LocalIpAddress;

    if (Fsbgp4ECommSetStatusIpNetwork.pu1_OctetList == NULL)
    {
        IssSendError (pHttp, (CONST INT1 *) "\rInsufficient memory\r\n");
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        if (BgpSetContext (i4BgpContextId) == SNMP_FAILURE)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
            return;
        }

        i4RetVal =
            nmhGetFirstIndexFsbgp4MpeExtCommRouteExtCommSetStatusTable
            (&i4Fsbgp4ECommSetStatusAfi, &i4Fsbgp4ECommSetStatusSafi,
             &Fsbgp4ECommSetStatusIpNetwork,
             &i4Fsbgp4ECommSetStatusIpPrefixLen);

        if (i4RetVal == SNMP_FAILURE)
        {
            i4BgpPrevContextId = i4BgpContextId;
            if (nmhGetNextIndexFsMIBgpContextTable (i4BgpPrevContextId,
                                                    &i4BgpContextId) !=
                SNMP_SUCCESS)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
                WebnmSockWrite (pHttp,
                                (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                                (pHttp->i4HtmlSize - pHttp->i4Write));

                return;
            }
            continue;
        }

        do
        {
            pHttp->i4Write = (INT4) u4Temp;

            STRCPY (pHttp->au1KeyString, "IP_ADDRESS");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (i4Fsbgp4ECommSetStatusAfi == BGP4_INET_AFI_IPV4)
            {
                MEMCPY (&u4IpAddress,
                        Fsbgp4ECommSetStatusIpNetwork.pu1_OctetList,
                        Fsbgp4ECommSetStatusIpNetwork.i4_Length);
                WEB_CONVERT_IPADDR_TO_STR (pu1TempString,
                                           OSIX_NTOHL (u4IpAddress));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1TempString);
            }
#ifdef BGP4_IPV6_WANTED
            else
            {
                Bgp4PrintIp6Addr (Fsbgp4ECommSetStatusIpNetwork.pu1_OctetList,
                                  (au1Ip6Addr));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Ip6Addr);
                MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
            }
#endif /* BGP4_IPV6_WANTED */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "PREFIX_LENGTH");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                     i4Fsbgp4ECommSetStatusIpPrefixLen);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpeExtCommSetStatus (i4Fsbgp4ECommSetStatusAfi,
                                             i4Fsbgp4ECommSetStatusSafi,
                                             &Fsbgp4ECommSetStatusIpNetwork,
                                             i4Fsbgp4ECommSetStatusIpPrefixLen,
                                             &i4Fsbgp4ECommSetStatus);

            STRCPY (pHttp->au1KeyString, "COMM_STATUS");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                     i4Fsbgp4ECommSetStatus);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            VcmGetAliasName ((UINT4) i4BgpContextId, au1BgpCxtName);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1BgpCxtName);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        }
        while (nmhGetNextIndexFsbgp4MpeExtCommRouteExtCommSetStatusTable
               (i4Fsbgp4ECommSetStatusAfi, &i4Fsbgp4ECommSetStatusAfi,
                i4Fsbgp4ECommSetStatusSafi, &i4Fsbgp4ECommSetStatusSafi,
                &Fsbgp4ECommSetStatusIpNetwork, &Fsbgp4ECommSetStatusIpNetwork,
                i4Fsbgp4ECommSetStatusIpPrefixLen,
                &i4Fsbgp4ECommSetStatusIpPrefixLen) != SNMP_FAILURE);

        i4BgpPrevContextId = i4BgpContextId;

    }
    while (nmhGetNextIndexFsMIBgpContextTable
           (i4BgpPrevContextId, &i4BgpContextId) == SNMP_SUCCESS);
    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    return;

}

/*********************************************************************
 *  Function Name : IssProcessBgpECommLocPolicyConfPageSet
 *  Description   : This function processes the request coming for the
 *                  BGP Extended Communities Local policy settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpECommLocPolicyConfPageSet (tHttp * pHttp)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4IpMask;
    INT4                i4RowStatus = 0;
    INT4                i4Len;
    INT4                i4Action;
    INT4                i4CxtId = 0;
    INT1                i1RetVal = 0;
    UINT1               u1Set;
    tNetAddress         NetAddress;
    tUtlInAddr          InAddr;
    tSNMP_OCTET_STRING_TYPE IpAddr;
    UINT1               au1LocalIpAddress[ISS_BGP4_IP_MAX_LENGTH];

    MEMSET (&NetAddress, 0, sizeof (tNetAddress));
    MEMSET (au1LocalIpAddress, 0, ISS_BGP4_IP_MAX_LENGTH);
    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    if (0 == STRCMP (pHttp->au1Value, "Delete"))
    {
        STRCPY (pHttp->au1Name, "CONTEXT_ID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        VcmIsVrfExist (pHttp->au1Value, (UINT4 *) &i4CxtId);
    }
    else
    {
        STRCPY (pHttp->au1Name, "CONTEXT_ID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4CxtId = ATOI (pHttp->au1Value);
    }

    if (BgpSetContext ((INT4) i4CxtId) == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
        return;
    }

    STRCPY (pHttp->au1Name, "PrefixLength");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4IpMask = (UINT4) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "CommunityStatus");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Action = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (0 == STRCMP (pHttp->au1Value, "Delete"))
    {
        u1Set = ISS_BGP4_DELETE;
    }
    else
    {
        u1Set = ISS_BGP4_APPLY;
    }

    STRCPY (pHttp->au1Name, "IpAddress");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);

    i4Len = CliDotStrLength (pHttp->au1Value);
    if (i4Len == sizeof (UINT4))
    {
        NetAddress.NetAddr.u2Afi = BGP4_INET_AFI_IPV4;
        NetAddress.u2Safi = BGP4_INET_SAFI_UNICAST;
        if (0 == BGP4_INET_ATON (pHttp->au1Value, &InAddr))
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            return;
        }
        MEMCPY ((NetAddress.NetAddr.au1Address),
                &InAddr.u4Addr, sizeof (UINT4));
        NetAddress.NetAddr.u2AddressLen = sizeof (UINT4);
        NetAddress.u2PrefixLen = (UINT2) u4IpMask;
    }
#ifdef BGP4_IPV6_WANTED
    else if (str_to_ip6addr (pHttp->au1Value) != NULL)
    {
        NetAddress.NetAddr.u2Afi = BGP4_INET_AFI_IPV6;
        NetAddress.u2Safi = BGP4_INET_SAFI_UNICAST;
        MEMCPY (NetAddress.NetAddr.au1Address,
                str_to_ip6addr (pHttp->au1Value), BGP4_IPV6_PREFIX_LEN);
        NetAddress.NetAddr.u2AddressLen = BGP4_IPV6_PREFIX_LEN;
        NetAddress.u2PrefixLen = (UINT2) u4IpMask;
    }
#endif /* BGP4_IPV6_WANTED */
    IpAddr.i4_Length = NetAddress.NetAddr.u2AddressLen;
    IpAddr.pu1_OctetList = au1LocalIpAddress;
    MEMCPY ((IpAddr.pu1_OctetList),
            (NetAddress.NetAddr.au1Address), NetAddress.NetAddr.u2AddressLen);

    /*Validation of indices */
    i1RetVal =
        nmhValidateIndexInstanceFsbgp4MpeExtCommRouteExtCommSetStatusTable
        (NetAddress.NetAddr.u2Afi, NetAddress.u2Safi, &IpAddr,
         NetAddress.u2PrefixLen);

    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *)
                      "\r Invalid Index to RouteExtCommSetStatusTable\r\n");

        return;
    }

    i1RetVal =
        nmhGetFsbgp4mpeExtCommSetStatusRowStatus (NetAddress.NetAddr.u2Afi,
                                                  NetAddress.u2Safi,
                                                  &IpAddr,
                                                  NetAddress.u2PrefixLen,
                                                  &i4RowStatus);

    if ((i1RetVal == SNMP_FAILURE) && (u1Set == ISS_BGP4_DELETE))
    {
        /* Trying to delete a non-existing Entry */
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Entry doesn't exist\r\n");

        return;
    }

    if (u1Set == ISS_BGP4_APPLY)
    {
        /* i1RetVal derived from return value of
           nmhGetFsbgp4CommSetStatusRowStatus */
        if (i1RetVal == SNMP_FAILURE)
        {
            /* Entry not present in Comm Set Status Table.
             * Need to create */
            i1RetVal =
                nmhTestv2Fsbgp4mpeExtCommSetStatusRowStatus (&u4ErrorCode,
                                                             NetAddress.
                                                             NetAddr.u2Afi,
                                                             NetAddress.
                                                             u2Safi,
                                                             &IpAddr,
                                                             NetAddress.
                                                             u2PrefixLen,
                                                             CREATE_AND_WAIT);
            if (i1RetVal == SNMP_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);

                IssSendError (pHttp, (CONST INT1 *)
                              "\r Invalid row status\r\n");
                return;

            }

            i1RetVal =
                nmhSetFsbgp4mpeExtCommSetStatusRowStatus (NetAddress.
                                                          NetAddr.u2Afi,
                                                          NetAddress.u2Safi,
                                                          &IpAddr,
                                                          NetAddress.
                                                          u2PrefixLen,
                                                          CREATE_AND_WAIT);
            if (i1RetVal == SNMP_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\r Unable to set row status as create and wait \r\n");
                return;

            }
        }
        else
        {
            /* Already Entry is present. Set it to not in service */
            i1RetVal =
                nmhTestv2Fsbgp4mpeExtCommSetStatusRowStatus (&u4ErrorCode,
                                                             NetAddress.
                                                             NetAddr.u2Afi,
                                                             NetAddress.
                                                             u2Safi,
                                                             &IpAddr,
                                                             NetAddress.
                                                             u2PrefixLen,
                                                             NOT_IN_SERVICE);
            if (i1RetVal == SNMP_FAILURE)
            {
                nmhSetFsbgp4mpeExtCommSetStatusRowStatus (NetAddress.
                                                          NetAddr.u2Afi,
                                                          NetAddress.u2Safi,
                                                          &IpAddr,
                                                          NetAddress.
                                                          u2PrefixLen, DESTROY);
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);

                IssSendError (pHttp, (CONST INT1 *)
                              "\r Invalid row status\r\n");
                return;

            }

            i1RetVal =
                nmhSetFsbgp4mpeExtCommSetStatusRowStatus (NetAddress.
                                                          NetAddr.u2Afi,
                                                          NetAddress.u2Safi,
                                                          &IpAddr,
                                                          NetAddress.
                                                          u2PrefixLen,
                                                          NOT_IN_SERVICE);
            if (i1RetVal == SNMP_FAILURE)
            {
                nmhSetFsbgp4mpeExtCommSetStatusRowStatus (NetAddress.
                                                          NetAddr.u2Afi,
                                                          NetAddress.u2Safi,
                                                          &IpAddr,
                                                          NetAddress.
                                                          u2PrefixLen, DESTROY);
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);

                IssSendError (pHttp, (CONST INT1 *)
                              "\r Unable to set row status as not in service \r\n");
                return;

            }
        }

        /* Entry Present. But not in Active State. Set the input action
         * and make the entry status as active. */

        i1RetVal =
            nmhTestv2Fsbgp4mpeExtCommSetStatus (&u4ErrorCode,
                                                NetAddress.NetAddr.u2Afi,
                                                NetAddress.u2Safi,
                                                &IpAddr,
                                                NetAddress.u2PrefixLen,
                                                i4Action);
        if (i1RetVal == SNMP_FAILURE)
        {
            nmhSetFsbgp4mpeExtCommSetStatusRowStatus (NetAddress.NetAddr.
                                                      u2Afi,
                                                      NetAddress.u2Safi,
                                                      &IpAddr,
                                                      NetAddress.
                                                      u2PrefixLen, DESTROY);
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);

            IssSendError (pHttp, (CONST INT1 *) "\r Invalid row status\r\n");
            return;

        }

        i1RetVal =
            nmhSetFsbgp4mpeExtCommSetStatus (NetAddress.NetAddr.u2Afi,
                                             NetAddress.u2Safi,
                                             &IpAddr,
                                             NetAddress.u2PrefixLen, i4Action);
        if (i1RetVal == SNMP_FAILURE)
        {
            nmhSetFsbgp4mpeExtCommSetStatusRowStatus (NetAddress.NetAddr.
                                                      u2Afi,
                                                      NetAddress.u2Safi,
                                                      &IpAddr,
                                                      NetAddress.
                                                      u2PrefixLen, DESTROY);
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);

            IssSendError (pHttp, (CONST INT1 *)
                          "\r Unable to set row status as  active\r\n");
            return;
        }

        i1RetVal =
            nmhTestv2Fsbgp4mpeExtCommSetStatusRowStatus (&u4ErrorCode,
                                                         NetAddress.NetAddr.
                                                         u2Afi,
                                                         NetAddress.u2Safi,
                                                         &IpAddr,
                                                         NetAddress.
                                                         u2PrefixLen, ACTIVE);
        if (i1RetVal == SNMP_FAILURE)
        {
            nmhSetFsbgp4mpeExtCommSetStatusRowStatus (NetAddress.NetAddr.
                                                      u2Afi,
                                                      NetAddress.u2Safi,
                                                      &IpAddr,
                                                      NetAddress.
                                                      u2PrefixLen, DESTROY);
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);

            IssSendError (pHttp, (CONST INT1 *) "\r Invalid row status\r\n");
            return;
        }

        i1RetVal =
            nmhSetFsbgp4mpeExtCommSetStatusRowStatus (NetAddress.NetAddr.
                                                      u2Afi,
                                                      NetAddress.u2Safi,
                                                      &IpAddr,
                                                      NetAddress.
                                                      u2PrefixLen, ACTIVE);
        if (i1RetVal == SNMP_FAILURE)
        {
            nmhSetFsbgp4mpeExtCommSetStatusRowStatus (NetAddress.NetAddr.
                                                      u2Afi,
                                                      NetAddress.u2Safi,
                                                      &IpAddr,
                                                      NetAddress.
                                                      u2PrefixLen, DESTROY);
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);

            IssSendError (pHttp, (CONST INT1 *)
                          "\r Unable to set row status as  active\r\n");
            return;
        }

    }
    else if (u1Set == ISS_BGP4_DELETE)
    {
        /* Entry present in Comm Peer Table. Need to Delete */
        i1RetVal =
            nmhTestv2Fsbgp4mpeExtCommSetStatusRowStatus (&u4ErrorCode,
                                                         NetAddress.NetAddr.
                                                         u2Afi,
                                                         NetAddress.u2Safi,
                                                         &IpAddr,
                                                         NetAddress.
                                                         u2PrefixLen, DESTROY);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);

            IssSendError (pHttp, (CONST INT1 *) "\r Invalid row status\r\n");
            return;

        }
        i1RetVal =
            nmhSetFsbgp4mpeExtCommSetStatusRowStatus (NetAddress.NetAddr.
                                                      u2Afi,
                                                      NetAddress.u2Safi,
                                                      &IpAddr,
                                                      NetAddress.
                                                      u2PrefixLen, DESTROY);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);

            return;
        }
    }
    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessBgpECommLocPolicyConfPageGet (pHttp);
    return;

}

/*********************************************************************
 *  Function Name : IssProcessBgpECommRouteConfPage
 *  Description   : This function processes the request coming for the
 *                  BGP Extended Community Route settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpECommRouteConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessBgpECommRouteConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessBgpECommRouteConfPageSet (pHttp);
    }
}

/*********************************************************************
 *  Function Name : IssProcessBgpECommRouteConfPageGet
 *  Description   : This function processes the GET request coming for the
 *                  BGP Extended Community Route settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpECommRouteConfPageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    UINT4               u4IpAddress = 0;
    INT4                i4Fsbgp4CommRtAfi = 0;
    INT4                i4Fsbgp4CommRtSafi = 0;
    INT4                i4Fsbgp4CommIpPrefixLen = 0;
    INT4                i4TableValue = 0;
    INT4                i4OutCome;
    INT4                i4BgpContextId = 0;
    INT4                i4BgpPrevContextId = 0;
    UINT1              *pu1TempString = NULL;
    UINT1               au1TempString[ISS_BGP4_EXT_COMM_VALUE_LEN];
    UINT1               au1Ip6Addr[ISS_BGP4_IP6_ADDR_STRING_LEN];
    tSNMP_OCTET_STRING_TYPE Fsbgp4CommIpNetwork;
    tSNMP_OCTET_STRING_TYPE Fsbgp4CommVal;
    UINT1               au1LocalIpAddress[ISS_BGP4_IP_MAX_LENGTH];
    UINT1               au1CommValIpAddress[ISS_BGP4_EXT_COMM_VALUE_LEN];
    UINT1               au1BgpCxtName[VCM_ALIAS_MAX_LEN];

    MEMSET (au1CommValIpAddress, 0, ISS_BGP4_EXT_COMM_VALUE_LEN);
    MEMSET (au1LocalIpAddress, 0, ISS_BGP4_IP_MAX_LENGTH);
    MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
    MEMSET (au1BgpCxtName, 0, VCM_ALIAS_MAX_LEN);

    Fsbgp4CommIpNetwork.pu1_OctetList = au1LocalIpAddress;
    Fsbgp4CommVal.pu1_OctetList = au1CommValIpAddress;

    pHttp->i4Write = 0;
    IssPrintAvailableBgpContextId (pHttp);

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    if (nmhGetFirstIndexFsMIBgpContextTable (&i4BgpContextId) == SNMP_FAILURE)
    {
        pHttp->i4Write = (INT4) u4Temp;
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        if (BgpSetContext (i4BgpContextId) == SNMP_FAILURE)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
            return;
        }

        /*Process the Ext Comm Route Delete Table */
        i4TableValue = ISS_BGP4_COMM_ROUTE_ADD;
        i4OutCome =
            nmhGetFirstIndexFsbgp4MpeExtCommRouteAddExtCommTable
            (&i4Fsbgp4CommRtAfi, &i4Fsbgp4CommRtSafi, &Fsbgp4CommIpNetwork,
             &i4Fsbgp4CommIpPrefixLen, &Fsbgp4CommVal);

        if (i4OutCome == SNMP_SUCCESS)
        {
            do
            {

                pHttp->i4Write = (INT4) u4Temp;

                STRCPY (pHttp->au1KeyString, "IP_ADDRESS");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                if (i4Fsbgp4CommRtAfi == BGP4_INET_AFI_IPV4)
                {
                    MEMCPY (&u4IpAddress,
                            Fsbgp4CommIpNetwork.pu1_OctetList,
                            Fsbgp4CommIpNetwork.i4_Length);
                    WEB_CONVERT_IPADDR_TO_STR (pu1TempString,
                                               OSIX_NTOHL (u4IpAddress));
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                             pu1TempString);
                }
#ifdef BGP4_IPV6_WANTED
                else
                {
                    Bgp4PrintIp6Addr (Fsbgp4CommIpNetwork.pu1_OctetList,
                                      (au1Ip6Addr));
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Ip6Addr);
                    MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
                }
#endif /* BGP4_IPV6_WANTED */
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "PREFIX_LENGTH");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         i4Fsbgp4CommIpPrefixLen);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                au1TempString[0] = (UINT4) Fsbgp4CommVal.pu1_OctetList[0];
                au1TempString[1] = (UINT4) Fsbgp4CommVal.pu1_OctetList[1];
                au1TempString[2] = (UINT4) Fsbgp4CommVal.pu1_OctetList[2];
                au1TempString[3] = (UINT4) Fsbgp4CommVal.pu1_OctetList[3];
                au1TempString[4] = (UINT4) Fsbgp4CommVal.pu1_OctetList[4];
                au1TempString[5] = (UINT4) Fsbgp4CommVal.pu1_OctetList[5];
                au1TempString[6] = (UINT4) Fsbgp4CommVal.pu1_OctetList[6];
                au1TempString[7] = (UINT4) Fsbgp4CommVal.pu1_OctetList[7];
                STRCPY (pHttp->au1KeyString, "COMM_VALUE");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString,
                         "%2x:%2x:%2x:%2x:%2x:%2x:%2x:%x",
                         au1TempString[0], au1TempString[1], au1TempString[2],
                         au1TempString[3], au1TempString[4], au1TempString[5],
                         au1TempString[6], au1TempString[7]);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "TABLE_VALUE_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4TableValue);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                VcmGetAliasName ((UINT4) i4BgpContextId, au1BgpCxtName);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1BgpCxtName);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            }
            while (nmhGetNextIndexFsbgp4MpeExtCommRouteAddExtCommTable (i4Fsbgp4CommRtAfi, &i4Fsbgp4CommRtAfi, i4Fsbgp4CommRtSafi, &i4Fsbgp4CommRtSafi, &Fsbgp4CommIpNetwork, &Fsbgp4CommIpNetwork, i4Fsbgp4CommIpPrefixLen, &i4Fsbgp4CommIpPrefixLen, &Fsbgp4CommVal, &Fsbgp4CommVal) != SNMP_FAILURE);    /*end of do-while */
        }

        /*Process the Ext Comm Route Delete Table */
        i4TableValue = ISS_BGP4_COMM_ROUTE_DELETE;
        i4OutCome =
            nmhGetFirstIndexFsbgp4MpeExtCommRouteDeleteExtCommTable
            (&i4Fsbgp4CommRtAfi, &i4Fsbgp4CommRtSafi, &Fsbgp4CommIpNetwork,
             &i4Fsbgp4CommIpPrefixLen, &Fsbgp4CommVal);
        if (i4OutCome == SNMP_FAILURE)
        {
            i4BgpPrevContextId = i4BgpContextId;
            if (nmhGetNextIndexFsMIBgpContextTable (i4BgpPrevContextId,
                                                    &i4BgpContextId) !=
                SNMP_SUCCESS)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
                WebnmSockWrite (pHttp,
                                (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                                (pHttp->i4HtmlSize - pHttp->i4Write));

                return;
            }
            continue;
        }
        do
        {
            pHttp->i4Write = (INT4) u4Temp;

            STRCPY (pHttp->au1KeyString, "IP_ADDRESS");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (i4Fsbgp4CommRtAfi == BGP4_INET_AFI_IPV4)
            {
                MEMCPY (&u4IpAddress,
                        Fsbgp4CommIpNetwork.pu1_OctetList,
                        Fsbgp4CommIpNetwork.i4_Length);
                WEB_CONVERT_IPADDR_TO_STR (pu1TempString,
                                           OSIX_NTOHL (u4IpAddress));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1TempString);
            }
#ifdef BGP4_IPV6_WANTED
            else
            {
                Bgp4PrintIp6Addr (Fsbgp4CommIpNetwork.pu1_OctetList,
                                  (au1Ip6Addr));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Ip6Addr);
                MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
            }
#endif /* BGP4_IPV6_WANTED */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "PREFIX_LENGTH");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                     i4Fsbgp4CommIpPrefixLen);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            au1TempString[0] = (UINT4) Fsbgp4CommVal.pu1_OctetList[0];
            au1TempString[1] = (UINT4) Fsbgp4CommVal.pu1_OctetList[1];
            au1TempString[2] = (UINT4) Fsbgp4CommVal.pu1_OctetList[2];
            au1TempString[3] = (UINT4) Fsbgp4CommVal.pu1_OctetList[3];
            au1TempString[4] = (UINT4) Fsbgp4CommVal.pu1_OctetList[4];
            au1TempString[5] = (UINT4) Fsbgp4CommVal.pu1_OctetList[5];
            au1TempString[6] = (UINT4) Fsbgp4CommVal.pu1_OctetList[6];
            au1TempString[7] = (UINT4) Fsbgp4CommVal.pu1_OctetList[7];
            STRCPY (pHttp->au1KeyString, "COMM_VALUE");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString,
                     "%2x:%2x:%2x:%2x:%2x:%2x:%2x:%2x", au1TempString[0],
                     au1TempString[1], au1TempString[2], au1TempString[3],
                     au1TempString[4], au1TempString[5], au1TempString[6],
                     au1TempString[7]);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "TABLE_VALUE_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4TableValue);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            VcmGetAliasName ((UINT4) i4BgpContextId, au1BgpCxtName);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1BgpCxtName);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        }
        while (nmhGetNextIndexFsbgp4MpeExtCommRouteDeleteExtCommTable (i4Fsbgp4CommRtAfi, &i4Fsbgp4CommRtAfi, i4Fsbgp4CommRtSafi, &i4Fsbgp4CommRtSafi, &Fsbgp4CommIpNetwork, &Fsbgp4CommIpNetwork, i4Fsbgp4CommIpPrefixLen, &i4Fsbgp4CommIpPrefixLen, &Fsbgp4CommVal, &Fsbgp4CommVal) != SNMP_FAILURE);    /*do-whlie end */

        i4BgpPrevContextId = i4BgpContextId;

    }
    while (nmhGetNextIndexFsMIBgpContextTable
           (i4BgpPrevContextId, &i4BgpContextId) == SNMP_SUCCESS);

    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    return;

}

/*********************************************************************
 *  Function Name : IssProcessBgpECommRouteConfPageSet
 *  Description   : This function processes the SET request coming for the
 *                  BGP Extended Community Route settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpECommRouteConfPageSet (tHttp * pHttp)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4IpMask = 0;
    INT4                i4RowStatus = 0;
    INT4                i4Len = 0;
    INT4                i4CxtId = 0;
    UINT1               i4Action = 0;
    UINT1               u1EcommVal[ECOMM_LEN];
    UINT1               u1Set = 0;
    INT1                i1RetVal = 0;
    tSNMP_OCTET_STRING_TYPE EcommValue;
    tSNMP_OCTET_STRING_TYPE IpAddr;
    tNetAddress         NetAddress;
    tUtlInAddr          InAddr;
    tBgp4Ecomm         *pBgp4EcommTbl;
    tBgp4Ecomm          tempBgp4EcommTbl;
    UINT1               au1LocalIpAddress[ISS_BGP4_IP_MAX_LENGTH];

    MEMSET (&NetAddress, 0, sizeof (tNetAddress));
    MEMSET (&tempBgp4EcommTbl, 0, sizeof (tBgp4Ecomm));
    MEMSET (au1LocalIpAddress, 0, ISS_BGP4_IP_MAX_LENGTH);
    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    if (0 == STRCMP (pHttp->au1Value, "Delete"))
    {
        STRCPY (pHttp->au1Name, "CONTEXT_ID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        VcmIsVrfExist (pHttp->au1Value, (UINT4 *) &i4CxtId);
    }
    else
    {
        STRCPY (pHttp->au1Name, "CONTEXT_ID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4CxtId = ATOI (pHttp->au1Value);
    }

    if (BgpSetContext ((INT4) i4CxtId) == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
        return;
    }

    STRCPY (pHttp->au1Name, "PrefixLength");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4IpMask = (UINT4) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "RouteTable");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Action = (UINT1) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (0 == STRCMP (pHttp->au1Value, "Delete"))
    {
        u1Set = ISS_BGP4_DELETE;
    }
    else
    {
        u1Set = ISS_BGP4_APPLY;
    }

    STRCPY (pHttp->au1Name, "CommunityValue");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);
    pBgp4EcommTbl = (tBgp4Ecomm *) (VOID *) &tempBgp4EcommTbl;
    MEMSET
        (pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal,
         0, ECOMM_LEN + ECOMM_EXT_LEN);
    if ((sscanf
         ((const char *) pHttp->au1Value,
          "%2x:%2x:%2x:%2x:%2x:%2x:%2x:%2x%x",
          &(pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[0]),
          &(pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[1]),
          &(pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[2]),
          &(pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[3]),
          &(pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[4]),
          &(pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[5]),
          &(pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[6]),
          &(pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[7]),
          &(pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[8])) !=
         ECOMM_LEN)
        || (pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[8] != 0))
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Input valid ecomm-values\r\n");
        return;
    }
    /* Copy the received Ecomm value. */
    u1EcommVal[0] =
        (UINT1) pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[0];
    u1EcommVal[1] =
        (UINT1) pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[1];
    u1EcommVal[2] =
        (UINT1) pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[2];
    u1EcommVal[3] =
        (UINT1) pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[3];
    u1EcommVal[4] =
        (UINT1) pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[4];
    u1EcommVal[5] =
        (UINT1) pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[5];
    u1EcommVal[6] =
        (UINT1) pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[6];
    u1EcommVal[7] =
        (UINT1) pBgp4EcommTbl->Bgp4EcommRouteTblConfig.au4EcommVal[7];

    STRCPY (pHttp->au1Name, "IpAddress");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);

    i4Len = CliDotStrLength (pHttp->au1Value);
    if (i4Len == sizeof (UINT4))
    {
        NetAddress.NetAddr.u2Afi = BGP4_INET_AFI_IPV4;
        NetAddress.u2Safi = BGP4_INET_SAFI_UNICAST;
        if (0 == BGP4_INET_ATON (pHttp->au1Value, &InAddr))
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "\r Invalid address\r\n");
            return;
        }
        MEMCPY ((NetAddress.NetAddr.au1Address),
                &InAddr.u4Addr, sizeof (UINT4));

        NetAddress.NetAddr.u2AddressLen = sizeof (UINT4);
        NetAddress.u2PrefixLen = (UINT2) u4IpMask;

    }
#ifdef BGP4_IPV6_WANTED
    else if (str_to_ip6addr (pHttp->au1Value) != NULL)
    {
        NetAddress.NetAddr.u2Afi = BGP4_INET_AFI_IPV6;
        NetAddress.u2Safi = BGP4_INET_SAFI_UNICAST;
        MEMCPY (NetAddress.NetAddr.au1Address,
                str_to_ip6addr (pHttp->au1Value), BGP4_IPV6_PREFIX_LEN);
        NetAddress.NetAddr.u2AddressLen = BGP4_IPV6_PREFIX_LEN;
        NetAddress.u2PrefixLen = (UINT2) u4IpMask;

    }
#endif /* BGP4_IPV6_WANTED */
    IpAddr.i4_Length = NetAddress.NetAddr.u2AddressLen;
    IpAddr.pu1_OctetList = au1LocalIpAddress;
    MEMCPY ((IpAddr.pu1_OctetList),
            (NetAddress.NetAddr.au1Address), NetAddress.NetAddr.u2AddressLen);

    /* Extract Ecomm Value */
    EcommValue.pu1_OctetList = u1EcommVal;
    EcommValue.i4_Length = ECOMM_LEN;

    if ((u1Set == ISS_BGP4_APPLY) && (i4Action == BGP4_ECOMM_ADD_TBL))
    {
        i1RetVal =
            nmhValidateIndexInstanceFsbgp4MpeExtCommRouteAddExtCommTable
            (NetAddress.NetAddr.u2Afi,
             NetAddress.u2Safi, &IpAddr, NetAddress.u2PrefixLen, &EcommValue);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);

            IssSendError (pHttp,
                          (CONST INT1 *)
                          "\r Invalid eComm-route entry indices\r\n");
            return;
        }
        i1RetVal =
            nmhGetFsbgp4mpeAddExtCommRowStatus (NetAddress.NetAddr.u2Afi,
                                                NetAddress.u2Safi,
                                                &IpAddr,
                                                NetAddress.u2PrefixLen,
                                                &EcommValue, &i4RowStatus);

    }
    else if ((u1Set == ISS_BGP4_APPLY) && (i4Action == BGP4_ECOMM_DEL_TBL))
    {
        i1RetVal =
            nmhValidateIndexInstanceFsbgp4MpeExtCommRouteDeleteExtCommTable
            (NetAddress.NetAddr.u2Afi,
             NetAddress.u2Safi, &IpAddr, NetAddress.u2PrefixLen, &EcommValue);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "\r Invalid eComm-route entry indices\r\n");
            return;
        }
        i1RetVal =
            nmhGetFsbgp4mpeDeleteExtCommRowStatus (NetAddress.NetAddr.u2Afi,
                                                   NetAddress.u2Safi,
                                                   &IpAddr,
                                                   NetAddress.u2PrefixLen,
                                                   &EcommValue, &i4RowStatus);

    }

    if ((i1RetVal != SNMP_FAILURE) && (i4RowStatus == ACTIVE) &&
        (u1Set == ISS_BGP4_APPLY))
    {
        /* Entry already exits and active */
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    if (u1Set == ISS_BGP4_APPLY)
    {
        /* i1RetVal derived from return value of
         * nmhGetFsbgp4DeleteExtCommRowStatus
         * or nmhGetFsbgp4AddExtCommRowStatus */
        if (i1RetVal == SNMP_FAILURE)
        {
            /* Entry not present in Ext Comm Add/Delete  Table.
             * Need to create */
            if (i4Action == BGP4_ECOMM_ADD_TBL)
            {
                i1RetVal =
                    nmhTestv2Fsbgp4mpeAddExtCommRowStatus (&u4ErrorCode,
                                                           NetAddress.
                                                           NetAddr.u2Afi,
                                                           NetAddress.
                                                           u2Safi, &IpAddr,
                                                           NetAddress.
                                                           u2PrefixLen,
                                                           &EcommValue,
                                                           CREATE_AND_GO);
                if (i1RetVal == SNMP_FAILURE)
                {
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *)
                                  "\rInvalid row status\r\n");
                    return;
                }
                i1RetVal =
                    nmhSetFsbgp4mpeAddExtCommRowStatus (NetAddress.NetAddr.
                                                        u2Afi,
                                                        NetAddress.u2Safi,
                                                        &IpAddr,
                                                        NetAddress.
                                                        u2PrefixLen,
                                                        &EcommValue,
                                                        CREATE_AND_GO);
                if (i1RetVal == SNMP_FAILURE)
                {
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *)
                                  "\rUnable to create entry in Route ADD table\r\n");
                    return;
                }

            }
            else
            {
                i1RetVal =
                    nmhTestv2Fsbgp4mpeDeleteExtCommRowStatus (&u4ErrorCode,
                                                              NetAddress.
                                                              NetAddr.u2Afi,
                                                              NetAddress.
                                                              u2Safi,
                                                              &IpAddr,
                                                              NetAddress.
                                                              u2PrefixLen,
                                                              &EcommValue,
                                                              CREATE_AND_GO);
                if (i1RetVal == SNMP_FAILURE)
                {
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *)
                                  "\r Invalid row status\r\n");
                    return;
                }
                i1RetVal =
                    nmhSetFsbgp4mpeDeleteExtCommRowStatus (NetAddress.
                                                           NetAddr.u2Afi,
                                                           NetAddress.
                                                           u2Safi, &IpAddr,
                                                           NetAddress.
                                                           u2PrefixLen,
                                                           &EcommValue,
                                                           CREATE_AND_GO);
                if (i1RetVal == SNMP_FAILURE)
                {
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);

                    IssSendError (pHttp, (CONST INT1 *)
                                  "\rUnable to create entry in Route delete table\r\n");
                    return;
                }

            }
        }

        else
        {
            /* Already Entry is present. But not Active */
            if (i4Action == BGP4_ECOMM_ADD_TBL)
            {
                i1RetVal =
                    nmhTestv2Fsbgp4mpeAddExtCommRowStatus (&u4ErrorCode,
                                                           NetAddress.
                                                           NetAddr.u2Afi,
                                                           NetAddress.
                                                           u2Safi, &IpAddr,
                                                           NetAddress.
                                                           u2PrefixLen,
                                                           &EcommValue,
                                                           NOT_IN_SERVICE);
                if (i1RetVal == SNMP_FAILURE)
                {
                    nmhSetFsbgp4mpeAddExtCommRowStatus (NetAddress.NetAddr.
                                                        u2Afi,
                                                        NetAddress.u2Safi,
                                                        &IpAddr,
                                                        NetAddress.
                                                        u2PrefixLen,
                                                        &EcommValue, DESTROY);
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);

                    IssSendError (pHttp, (CONST INT1 *)
                                  "\r Invalid row status\r\n");
                    return;
                }
                i1RetVal =
                    nmhSetFsbgp4mpeAddExtCommRowStatus (NetAddress.NetAddr.
                                                        u2Afi,
                                                        NetAddress.u2Safi,
                                                        &IpAddr,
                                                        NetAddress.
                                                        u2PrefixLen,
                                                        &EcommValue,
                                                        NOT_IN_SERVICE);
                if (i1RetVal == SNMP_FAILURE)
                {
                    nmhSetFsbgp4mpeAddExtCommRowStatus (NetAddress.NetAddr.
                                                        u2Afi,
                                                        NetAddress.u2Safi,
                                                        &IpAddr,
                                                        NetAddress.
                                                        u2PrefixLen,
                                                        &EcommValue, DESTROY);
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);

                    IssSendError (pHttp, (CONST INT1 *)
                                  "\rUnable to set row status as not in service\r\n");
                    return;
                }

            }
            else
            {
                i1RetVal =
                    nmhTestv2Fsbgp4mpeDeleteExtCommRowStatus (&u4ErrorCode,
                                                              NetAddress.
                                                              NetAddr.u2Afi,
                                                              NetAddress.
                                                              u2Safi,
                                                              &IpAddr,
                                                              NetAddress.
                                                              u2PrefixLen,
                                                              &EcommValue,
                                                              NOT_IN_SERVICE);
                if (i1RetVal == SNMP_FAILURE)
                {
                    nmhSetFsbgp4mpeDeleteExtCommRowStatus (NetAddress.
                                                           NetAddr.u2Afi,
                                                           NetAddress.
                                                           u2Safi, &IpAddr,
                                                           NetAddress.
                                                           u2PrefixLen,
                                                           &EcommValue,
                                                           DESTROY);
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);

                    IssSendError (pHttp, (CONST INT1 *)
                                  "\r Invalid row status\r\n");
                    return;
                }
                i1RetVal =
                    nmhSetFsbgp4mpeDeleteExtCommRowStatus (NetAddress.
                                                           NetAddr.u2Afi,
                                                           NetAddress.
                                                           u2Safi, &IpAddr,
                                                           NetAddress.
                                                           u2PrefixLen,
                                                           &EcommValue,
                                                           NOT_IN_SERVICE);
                if (i1RetVal == SNMP_FAILURE)
                {
                    nmhSetFsbgp4mpeDeleteExtCommRowStatus (NetAddress.
                                                           NetAddr.u2Afi,
                                                           NetAddress.
                                                           u2Safi, &IpAddr,
                                                           NetAddress.
                                                           u2PrefixLen,
                                                           &EcommValue,
                                                           DESTROY);
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);

                    IssSendError (pHttp, (CONST INT1 *)
                                  "\rUnable to set row status as active\r\n");
                    return;
                }

            }
        }

        /* Already Entry is present. But not Active */
        if (i4Action == BGP4_ECOMM_ADD_TBL)
        {
            i1RetVal =
                nmhTestv2Fsbgp4mpeAddExtCommRowStatus (&u4ErrorCode,
                                                       NetAddress.NetAddr.
                                                       u2Afi,
                                                       NetAddress.u2Safi,
                                                       &IpAddr,
                                                       NetAddress.
                                                       u2PrefixLen,
                                                       &EcommValue, ACTIVE);
            if (i1RetVal == SNMP_FAILURE)
            {
                nmhSetFsbgp4mpeAddExtCommRowStatus (NetAddress.NetAddr.
                                                    u2Afi,
                                                    NetAddress.u2Safi,
                                                    &IpAddr,
                                                    NetAddress.u2PrefixLen,
                                                    &EcommValue, DESTROY);
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);

                IssSendError (pHttp, (CONST INT1 *)
                              "\r Invalid row status\r\n");
                return;
            }
            i1RetVal =
                nmhSetFsbgp4mpeAddExtCommRowStatus (NetAddress.NetAddr.
                                                    u2Afi,
                                                    NetAddress.u2Safi,
                                                    &IpAddr,
                                                    NetAddress.u2PrefixLen,
                                                    &EcommValue, ACTIVE);
            if (i1RetVal == SNMP_FAILURE)
            {
                nmhSetFsbgp4mpeAddExtCommRowStatus (NetAddress.NetAddr.
                                                    u2Afi,
                                                    NetAddress.u2Safi,
                                                    &IpAddr,
                                                    NetAddress.u2PrefixLen,
                                                    &EcommValue, DESTROY);
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);

                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to set row status as active\r\n");
                return;
            }

        }
        else
        {
            i1RetVal =
                nmhTestv2Fsbgp4mpeDeleteExtCommRowStatus (&u4ErrorCode,
                                                          NetAddress.
                                                          NetAddr.u2Afi,
                                                          NetAddress.u2Safi,
                                                          &IpAddr,
                                                          NetAddress.
                                                          u2PrefixLen,
                                                          &EcommValue, ACTIVE);
            if (i1RetVal == SNMP_FAILURE)
            {
                nmhSetFsbgp4mpeDeleteExtCommRowStatus (NetAddress.NetAddr.
                                                       u2Afi,
                                                       NetAddress.u2Safi,
                                                       &IpAddr,
                                                       NetAddress.
                                                       u2PrefixLen,
                                                       &EcommValue, DESTROY);
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);

                IssSendError (pHttp, (CONST INT1 *)
                              "\r Invalid row status\r\n");
                return;
            }
            i1RetVal =
                nmhSetFsbgp4mpeDeleteExtCommRowStatus (NetAddress.NetAddr.
                                                       u2Afi,
                                                       NetAddress.u2Safi,
                                                       &IpAddr,
                                                       NetAddress.
                                                       u2PrefixLen,
                                                       &EcommValue, ACTIVE);
            if (i1RetVal == SNMP_FAILURE)
            {
                nmhSetFsbgp4mpeDeleteExtCommRowStatus (NetAddress.NetAddr.
                                                       u2Afi,
                                                       NetAddress.u2Safi,
                                                       &IpAddr,
                                                       NetAddress.
                                                       u2PrefixLen,
                                                       &EcommValue, DESTROY);
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);

                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to set row status as active\r\n");
                return;
            }

        }
    }
    else if (u1Set == ISS_BGP4_DELETE)
    {
        /* Entry present in Ext Comm Add/Delete  Table. Need to Delete */
        if (i4Action == BGP4_ECOMM_ADD_TBL)
        {
            i1RetVal =
                nmhSetFsbgp4mpeAddExtCommRowStatus (NetAddress.NetAddr.
                                                    u2Afi,
                                                    NetAddress.u2Safi,
                                                    &IpAddr,
                                                    NetAddress.u2PrefixLen,
                                                    &EcommValue, DESTROY);
            if (i1RetVal == SNMP_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);

                IssSendError (pHttp, (CONST INT1 *)
                              "\r Invalid row status\r\n");
                return;
            }

        }
        else
        {
            i1RetVal =
                nmhSetFsbgp4mpeDeleteExtCommRowStatus (NetAddress.NetAddr.
                                                       u2Afi,
                                                       NetAddress.u2Safi,
                                                       &IpAddr,
                                                       NetAddress.
                                                       u2PrefixLen,
                                                       &EcommValue, DESTROY);
            if (i1RetVal == SNMP_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);

                IssSendError (pHttp, (CONST INT1 *)
                              "\r Unable to delete the entry\r\n");
                return;
            }

        }
    }
    else
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "\r Invalid action to be performed\r\n");
    }

    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessBgpECommRouteConfPageGet (pHttp);
    return;

}

/*********************************************************************
 *  Function Name : IssProcessBgpConfedConfPage
 *  Description   : This function processes the request coming for the
 *                  BGP Confederation configuration page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpConfedConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessBgpConfedConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessBgpConfedConfPageSet (pHttp);
    }
}

/*********************************************************************
 *  Function Name : IssProcessBgpConfedConfPageGet
 *  Description   : This function processes the SET request coming for the
 *                  BGP Confederation configuration page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpConfedConfPageGet (tHttp * pHttp)
{
    INT4                i4Temp = 0;
    INT4                i4Status = 0;
    UINT4               u4ConfedPeerASNo = BGP4_INV_AS;
    INT4                i4BgpContextId = 0;
    INT4                i4BgpPrevContextId = 0;
    UINT1               au1BgpCxtName[VCM_ALIAS_MAX_LEN];

    MEMSET (au1BgpCxtName, 0, VCM_ALIAS_MAX_LEN);
    pHttp->i4Write = 0;
    IssPrintAvailableBgpContextId (pHttp);

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    if (nmhGetFirstIndexFsMIBgpContextTable (&i4BgpContextId) == SNMP_FAILURE)
    {
        pHttp->i4Write = i4Temp;
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    i4Temp = pHttp->i4Write;

    do
    {
        pHttp->i4Write = i4Temp;

        if (BgpSetContext (i4BgpContextId) == SNMP_FAILURE)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
            return;
        }

        if (nmhGetFirstIndexFsbgpAscConfedPeerTable (&u4ConfedPeerASNo)
            == SNMP_FAILURE)
        {
            i4BgpPrevContextId = i4BgpContextId;
            if (nmhGetNextIndexFsMIBgpContextTable (i4BgpPrevContextId,
                                                    &i4BgpContextId) !=
                SNMP_SUCCESS)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
                WebnmSockWrite (pHttp,
                                (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                                (pHttp->i4HtmlSize - pHttp->i4Write));

                return;
            }
            continue;
        }

        /*Process the BGP confederation peer table */
        do
        {
            pHttp->i4Write = i4Temp;
            STRCPY (pHttp->au1KeyString, "PEER_AS_NUM_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4ConfedPeerASNo);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgpAscConfedPeerStatus (u4ConfedPeerASNo, &i4Status);
            STRCPY (pHttp->au1KeyString, "STATUS_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Status);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            VcmGetAliasName ((UINT4) i4BgpContextId, au1BgpCxtName);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1BgpCxtName);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        }
        while (nmhGetNextIndexFsbgpAscConfedPeerTable
               (u4ConfedPeerASNo, &u4ConfedPeerASNo) != SNMP_FAILURE);
        /*end of do-while */

        i4BgpPrevContextId = i4BgpContextId;

    }
    while (nmhGetNextIndexFsMIBgpContextTable
           (i4BgpPrevContextId, &i4BgpContextId) == SNMP_SUCCESS);

    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;

}

/*********************************************************************
 *  Function Name : IssProcessBgpConfedConfPageSet
 *  Description   : This function processes the request coming for the
 *                  BGP Confederation configuration page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpConfedConfPageSet (tHttp * pHttp)
{
    UINT4               u4ConfedPeerStatus = ISS_BGP4_CONFED_PEER_AS_DISABLE;
    UINT4               u4ErrorCode = 0;
    UINT4               u4ConfedPeer = 0;
    INT4                i4CxtId = 0;
    INT4                i4Status = 0;
    INT4                i4SupportStatus;
    INT1                i1RetVal = SNMP_FAILURE;

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    if (0 == STRCMP (pHttp->au1Value, "Delete"))
    {
        STRCPY (pHttp->au1Name, "CONTEXT_ID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        VcmIsVrfExist (pHttp->au1Value, (UINT4 *) &i4CxtId);
    }
    else
    {
        STRCPY (pHttp->au1Name, "CONTEXT_ID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4CxtId = ATOI (pHttp->au1Value);
    }

    if (BgpSetContext ((INT4) i4CxtId) == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
        return;
    }

    /*Get the AS number */
    STRCPY (pHttp->au1Name, "PeerASNumber");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4ConfedPeer = (UINT4) ATOI (pHttp->au1Value);
        nmhGetFsbgp4FourByteASNSupportStatus (&i4SupportStatus);

        if ((i4SupportStatus == BGP4_DISABLE_4BYTE_ASN_SUPPORT) &&
            (u4ConfedPeer > BGP4_MAX_TWO_BYTE_AS))
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "4-byte ASN Capability is disabled. Configure value < 65536");
            IssProcessBgpContextCreationPageGet (pHttp);
            return;
        }
    }

    i1RetVal = nmhValidateIndexInstanceFsbgpAscConfedPeerTable ((INT4)
                                                                u4ConfedPeer);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *)
                      "\r Invalid indices to the confederation table\r\n");
        return;
    }

    /*Get the peer status */
    STRCPY (pHttp->au1Name, "ConfedPeerStatus");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
        i4Status = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ACTION");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)

        if (0 == STRCMP (pHttp->au1Value, "Delete"))
        {
            /*Delete the confederation peer by disabling it */
            i1RetVal = nmhSetFsbgpAscConfedPeerStatus (u4ConfedPeer,
                                                       ISS_BGP4_CONFED_PEER_AS_DISABLE);

            if (i1RetVal == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "\rCannot be disabled\r\n");
            }
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessBgpConfedConfPageGet (pHttp);
            return;
        }

    i1RetVal = nmhGetFsbgpAscConfedPeerStatus (u4ConfedPeer,
                                               (INT4 *) &u4ConfedPeerStatus);

    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *)
                      "\rUnable to get the peer status\r\n");
        return;
    }
    /* Confederation Peer is already enabled */
    if ((u4ConfedPeerStatus == ISS_BGP4_CONFED_PEER_AS_ENABLE) &&
        (i4Status == ISS_BGP4_ENABLE))
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    if (i4Status == ISS_BGP4_ENABLE)
    {
        i1RetVal = nmhTestv2FsbgpAscConfedPeerStatus (&u4ErrorCode,
                                                      u4ConfedPeer,
                                                      ISS_BGP4_CONFED_PEER_AS_ENABLE);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "\rInvalid peer status\r\n");
            return;
        }
        i1RetVal = nmhSetFsbgpAscConfedPeerStatus (u4ConfedPeer,
                                                   ISS_BGP4_CONFED_PEER_AS_ENABLE);

        /* if operation is not successfull */
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "\rUnable to set  peer status\r\n");
            return;
        }
    }
    if (i4Status == ISS_BGP4_DISABLE)
    {
        i1RetVal = nmhTestv2FsbgpAscConfedPeerStatus (&u4ErrorCode,
                                                      u4ConfedPeer,
                                                      ISS_BGP4_CONFED_PEER_AS_DISABLE);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "\rInvalid peer status\r\n");
            return;
        }
        i1RetVal = nmhSetFsbgpAscConfedPeerStatus (u4ConfedPeer,
                                                   ISS_BGP4_CONFED_PEER_AS_DISABLE);

        /* if operation is not successfull */
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "\rUnable to set  peer status\r\n");
            return;
        }
    }

    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessBgpConfedConfPageGet (pHttp);
    return;

}

/*********************************************************************
 *  Function Name : IssProcessBgpImportConfPage
 *  Description   : This function processes the request coming for the
 *                  BGP Import configuration page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpImportConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessBgpImportConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessBgpImportConfPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessBgpImportConfPageGet
*  Description   : This function processes the GET request coming for 
*                  the BGP Import configuration page
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessBgpImportConfPageGet (tHttp * pHttp)
{

    UINT4               u4Temp = 0;
    UINT4               u4IpAddress = 0;
    INT4                i4RoutePrefixAfi = 0;
    INT4                i4RoutePrefixSafi = 0;
    INT4                i4RoutePrefixLen = 0;
    INT4                i4RouteProtocol = 0;
    INT4                i4RouteIfIndex = 0;
    INT4                i4RouteMetric = 0;
    INT4                i4OutCome = 0;
    UINT1               au1Ip6Addr[ISS_BGP4_IP6_ADDR_STRING_LEN];
    UINT1              *pu1TempString = NULL;
    tSNMP_OCTET_STRING_TYPE RouteVrf;
    tSNMP_OCTET_STRING_TYPE RoutePrefix;
    tSNMP_OCTET_STRING_TYPE RouteNextHop;
    UINT1               au1PrefixAddress[ISS_BGP4_IP_MAX_LENGTH];
    UINT1               au1NextHopIpAddress[ISS_BGP4_IP_MAX_LENGTH];
    UINT1               au1VrfIpAddress[VCM_ALIAS_MAX_LEN];

    MEMSET (au1PrefixAddress, 0, ISS_BGP4_IP_MAX_LENGTH);
    MEMSET (au1NextHopIpAddress, 0, ISS_BGP4_IP_MAX_LENGTH);
    MEMSET (au1VrfIpAddress, 0, VCM_ALIAS_MAX_LEN);
    MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));

    pHttp->i4Write = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    RoutePrefix.pu1_OctetList = au1PrefixAddress;
    RouteNextHop.pu1_OctetList = au1NextHopIpAddress;
    RouteVrf.pu1_OctetList = au1VrfIpAddress;

    /*Process the import configuration table */
    i4OutCome = nmhGetFirstIndexFsbgp4MpeImportRouteTable (&i4RoutePrefixAfi,
                                                           &i4RoutePrefixSafi,
                                                           &RoutePrefix,
                                                           &i4RoutePrefixLen,
                                                           &i4RouteProtocol,
                                                           &RouteNextHop,
                                                           &i4RouteIfIndex,
                                                           &i4RouteMetric,
                                                           &RouteVrf);
    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);

        return;
    }

    /*Process the BGP import configuration table */
    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        STRCPY (pHttp->au1KeyString, "fsbgp4ImportRoutePrefix_Key");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4RoutePrefixAfi == BGP4_INET_AFI_IPV4)
        {
            MEMCPY (&u4IpAddress,
                    RoutePrefix.pu1_OctetList, RoutePrefix.i4_Length);
            WEB_CONVERT_IPADDR_TO_STR (pu1TempString, OSIX_NTOHL (u4IpAddress));
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1TempString);
        }
#ifdef BGP4_IPV6_WANTED
        else
        {
            Bgp4PrintIp6Addr (RoutePrefix.pu1_OctetList, (au1Ip6Addr));
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Ip6Addr);
            MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
        }
#endif /* BGP4_IPV6_WANTED */
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsbgp4ImportRoutePrefixLen_Key");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RoutePrefixLen);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsbgp4ImportRouteNextHop_Key");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4RoutePrefixAfi == BGP4_INET_AFI_IPV4)
        {
            MEMCPY (&u4IpAddress,
                    RouteNextHop.pu1_OctetList, RouteNextHop.i4_Length);
            WEB_CONVERT_IPADDR_TO_STR (pu1TempString, OSIX_NTOHL (u4IpAddress));
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1TempString);
        }
#ifdef BGP4_IPV6_WANTED
        else
        {
            Bgp4PrintIp6Addr (RouteNextHop.pu1_OctetList, (au1Ip6Addr));
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Ip6Addr);
            MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
        }
#endif /* BGP4_IPV6_WANTED */
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsbgp4ImportRouteMetric_Key");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RouteMetric);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsbgp4ImportRouteIfIndex_Key");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RouteIfIndex);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsbgp4ImportRouteProtocol_Key");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RouteProtocol);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    }
    while (nmhGetNextIndexFsbgp4MpeImportRouteTable (i4RoutePrefixAfi, &i4RoutePrefixAfi, i4RoutePrefixSafi, &i4RoutePrefixSafi, &RoutePrefix, &RoutePrefix, i4RoutePrefixLen, &i4RoutePrefixLen, i4RouteProtocol, &i4RouteProtocol, &RouteNextHop, &RouteNextHop, i4RouteIfIndex, &i4RouteIfIndex, i4RouteMetric, &i4RouteMetric, &RouteVrf, &RouteVrf) == SNMP_SUCCESS);    /*end of do-while */
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    return;
}

/*********************************************************************
*  Function Name : IssProcessBgpImportConfPageSet
*  Description   : This function processes the SET request for the 
*                  Import Configuration of BGP.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessBgpImportConfPageSet (tHttp * pHttp)
{
    UINT4               u4Pref0 = 0;
    UINT4               u4Pref1 = 0;
    UINT4               u4Pref2 = 0;
    UINT4               u4Pref3 = 0;
    UINT4               u4Prefix = 0;
    UINT4               u4Mask = 0;
    UINT4               u4Err = 0;
    UINT4               u4RoutePrefixLen = 0;
    INT4                i1RetVal = 0;
    INT4                i4Action = 0;
    INT4                i4Cnt = 0;
    INT4                i4NumRts = 0;
    INT4                i4Metric = 0;
    INT4                i4Index = 0;
    INT4                i4Protocol = 0;
    INT4                i4Len = 0;
    tSNMP_OCTET_STRING_TYPE IpAddr;
    tSNMP_OCTET_STRING_TYPE NextHop;
    tSNMP_OCTET_STRING_TYPE VrfName;
    tNetAddress         NetAddress;
    tAddrPrefix         AddrPrefix;
    tUtlInAddr          InAddr;
    UINT4               u4LocalIpAddress = 0;
    UINT4               u4NextIpAddress = 0;

    MEMSET (&NetAddress, 0, sizeof (tNetAddress));
    MEMSET (&AddrPrefix, 0, sizeof (tAddrPrefix));

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (0 == STRCMP (pHttp->au1Value, "Delete"))
    {
        i4Action = ISS_BGP4_APPLY;
    }
    else
    {
        i4Action = ISS_BGP4_DELETE;
    }
    STRCPY (pHttp->au1Name, "fsbgp4ImportRoutePrefixLen");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4RoutePrefixLen = (UINT4) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4ImportRouteMetric");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Metric = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4ImportRouteIfIndex");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Index = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4ImportRouteProtocol");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Protocol = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4ImportRouteCount");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4NumRts = ATOI (pHttp->au1Value);

    NetAddress.NetAddr.u2Afi = BGP4_INET_AFI_IPV4;
    NetAddress.u2Safi = BGP4_INET_SAFI_UNICAST;

    STRCPY (pHttp->au1Name, "fsbgp4ImportRoutePrefix");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);

    i4Len = CliDotStrLength (pHttp->au1Value);
    if (i4Len == sizeof (UINT4))
    {
        NetAddress.NetAddr.u2Afi = BGP4_INET_AFI_IPV4;
        NetAddress.u2Safi = BGP4_INET_SAFI_UNICAST;

        if (0 == BGP4_INET_ATON (pHttp->au1Value, &InAddr))
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "\rInvalid address\r\n");
            return;
        }
        MEMCPY ((NetAddress.NetAddr.au1Address),
                &(InAddr.u4Addr), sizeof (UINT4));
        NetAddress.NetAddr.u2AddressLen = sizeof (UINT4);
        NetAddress.u2PrefixLen = (UINT2) u4RoutePrefixLen;
    }

    STRCPY (pHttp->au1Name, "fsbgp4ImportRouteNextHop");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);

    i4Len = CliDotStrLength (pHttp->au1Value);
    if (i4Len == sizeof (UINT4))
    {
        AddrPrefix.u2Afi = BGP4_INET_AFI_IPV4;
        if (0 == BGP4_INET_ATON (pHttp->au1Value, &InAddr))
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "\rInvalid Next Hop\r\n");
            return;
        }
        MEMCPY ((AddrPrefix.au1Address), &(InAddr.u4Addr), sizeof (UINT4));
        AddrPrefix.u2AddressLen = sizeof (UINT4);
    }

    IpAddr.pu1_OctetList = (UINT1 *) (VOID *) &u4LocalIpAddress;

    MEMCPY (IpAddr.pu1_OctetList, NetAddress.NetAddr.au1Address,
            NetAddress.NetAddr.u2AddressLen);

    IpAddr.i4_Length = NetAddress.NetAddr.u2AddressLen;
    NextHop.pu1_OctetList = (UINT1 *) (VOID *) &u4NextIpAddress;

    MEMCPY (NextHop.pu1_OctetList, AddrPrefix.au1Address,
            AddrPrefix.u2AddressLen);
    NextHop.i4_Length = AddrPrefix.u2AddressLen;

    VrfName.pu1_OctetList = NULL;
    VrfName.i4_Length = 0;

    PTR_FETCH4 (u4Pref0, NetAddress.NetAddr.au1Address);
    u4Mask = Bgp4GetSubnetmask ((UINT1) NetAddress.u2PrefixLen);
    u4Pref0 = u4Pref0 & u4Mask;
    u4Pref1 = 0x00010000 & u4Mask;
    u4Pref2 = 0x00000100 & u4Mask;

    while (i4Cnt < i4NumRts)
    {
        if (i4Cnt == 0)
        {
            u4Prefix = u4Pref0;
        }
#ifndef RRD_WANTED
        else
        {
            u4Prefix = u4Pref0 + u4Pref1 + u4Pref2 + u4Pref3;
        }
#endif
        u4Prefix = u4Prefix & u4Mask;
        PTR_ASSIGN4 (IpAddr.pu1_OctetList, (u4Prefix));
        IpAddr.i4_Length = sizeof (UINT4);
        i1RetVal = nmhTestv2Fsbgp4mpeImportRouteAction (&u4Err,
                                                        NetAddress.NetAddr.
                                                        u2Afi,
                                                        NetAddress.u2Safi,
                                                        &IpAddr,
                                                        NetAddress.u2PrefixLen,
                                                        i4Protocol, &NextHop,
                                                        i4Index, i4Metric,
                                                        &VrfName, i4Action);

        if (i1RetVal == SNMP_FAILURE)
        {

            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "\rInvalid action\r\n");
            return;
        }
        i1RetVal = nmhSetFsbgp4mpeImportRouteAction (NetAddress.NetAddr.u2Afi,
                                                     NetAddress.u2Safi,
                                                     &IpAddr,
                                                     NetAddress.u2PrefixLen,
                                                     i4Protocol, &NextHop,
                                                     i4Index, i4Metric,
                                                     &VrfName, i4Action);

        if (i1RetVal == SNMP_FAILURE)
        {

            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "\rRoute cannot be added\r\n");
            return;
        }
#ifndef RRD_WANTED
        if (u4Pref2 != 0x00000000)
        {
            u4Pref2 += 0x00000100;
            if (u4Pref2 > 0x0000ff00)
            {
                u4Pref2 = 0x00000100;
                u4Pref1 += 0x00010000;
            }
        }
        if (u4Pref1 != 0x00000000)
        {
            if (u4Pref1 > 0x00ff0000)
            {
                u4Pref1 = 0x00010000;
                u4Pref0 += 0x01000000;
            }
        }
        if ((u4Pref1 == 0x00000000) && (u4Pref0 != 0x00000000))
        {
            if (u4Pref0 < 0xff000000)
            {
                u4Pref0 += 0x01000000;
            }
        }
        if ((u4Pref1 != 0x00000000) && (u4Pref2 == 0x00000000))
        {
            if (u4Pref1 < 0xff000000)
            {
                u4Pref1 += 0x00010000;
            }
        }
        i4Cnt++;
        i4Metric++;
#endif
    }                            /* end of while */
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
#ifdef RRD_WANTED
    UNUSED_PARAM (u4Pref1);
    UNUSED_PARAM (u4Pref2);
    UNUSED_PARAM (u4Pref3);
#endif
    IssProcessBgpImportConfPageGet (pHttp);
    return;
}

/*********************************************************************
 *  Function Name : IssProcessBgpRfdConfPage
 *  Description   : This function processes the request coming for the
 *                  BGP RFD configuration page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpRfdConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessBgpRfdConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessBgpRfdConfPageSet (pHttp);
    }
}

/*********************************************************************
 *  Function Name : IssProcessBgpRfdConfPageGet
 *  Description   : This function processes the GET request coming for the
 *                  BGP RFD configuration page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpRfdConfPageGet (tHttp * pHttp)
{
    INT4                i4RetVal = 0;
    INT4                i4BgpContextId = 0;
    INT4                i4BgpPrevContextId = 0;
    UINT1               au1BgpCxtName[VCM_ALIAS_MAX_LEN];

    MEMSET (au1BgpCxtName, 0, VCM_ALIAS_MAX_LEN);

    pHttp->i4Write = 0;
    /*   IssPrintAvailableBgpContextId (pHttp); */
    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    pHttp->i4Write = 0;

    STRCPY (pHttp->au1Name, "BGP_CONTEXT_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4BgpContextId = ATOI (pHttp->au1Value);

    if (BgpSetContext (i4BgpContextId) == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
        return;
    }

    nmhGetFsbgp4RfdDecayHalfLifeTime (&i4RetVal);
    STRCPY (pHttp->au1KeyString, "halfLifeTime_Key");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    nmhGetFsbgp4RfdReuse (&i4RetVal);
    STRCPY (pHttp->au1KeyString, "reuseValue_Key");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    nmhGetFsbgp4RfdCutOff (&i4RetVal);
    STRCPY (pHttp->au1KeyString, "suppressValue_Key");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    nmhGetFsbgp4RfdMaxHoldDownTime (&i4RetVal);
    STRCPY (pHttp->au1KeyString, "maxSuppressTime_Key");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    nmhGetFsbgp4RfdDecayTimerGranularity (&i4RetVal);
    STRCPY (pHttp->au1KeyString, "decayTimerGranularity_Key");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    nmhGetFsbgp4RfdReuseTimerGranularity (&i4RetVal);
    STRCPY (pHttp->au1KeyString, "reuseTimerGranularity_Key");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    nmhGetFsbgp4RfdReuseIndxArraySize (&i4RetVal);
    STRCPY (pHttp->au1KeyString, "reuseArrayIndex_Key");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    if (nmhGetFirstIndexFsMIBgpContextTable (&i4BgpContextId))
    {
        STRCPY (pHttp->au1KeyString, "<! CONTEXT ID>");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        do
        {
            VcmGetAliasName ((UINT4) i4BgpContextId, au1BgpCxtName);
            SPRINTF ((CHR1 *) pHttp->au1DataString,
                     "<option value = \"%d\" selected>%s \n",
                     i4BgpContextId, au1BgpCxtName);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            i4BgpPrevContextId = i4BgpContextId;
        }
        while (nmhGetNextIndexFsMIBgpContextTable
               (i4BgpPrevContextId, &i4BgpContextId) == SNMP_SUCCESS);
    }

    STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    VcmGetAliasName ((UINT4) i4BgpContextId, au1BgpCxtName);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1BgpCxtName);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;

}

/*********************************************************************
 *  Function Name : IssProcessBgpRfdConfPageSet
 *  Description   : This function processes the SET request coming for the
 *                  BGP Route Flap Dampening configuration page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpRfdConfPageSet (tHttp * pHttp)
{

    INT4                i4CxtId = 0;
    INT4                i4HalfLifeTime = ISS_RFD_DEF_DECAY_HALF_LIFE_TIME;
    INT4                i4ReuseValue = ISS_RFD_DEF_REUSE_THRESHOLD;
    INT4                i4SuppressVal = ISS_RFD_DEF_CUTOFF_THRESHOLD;
    INT4                i4MaxSuppressTime = ISS_RFD_DEF_MAX_HOLD_DOWN_TIME;
    INT4                i4DecayTimerGranularity =
        ISS_RFD_DEF_DECAY_TIMER_GRANULARITY;
    INT4                i4ReuseTimerGranularity =
        ISS_RFD_DEF_REUSE_TIMER_GRANULARITY;
    INT4                i4ReuseArrayIndex = ISS_RFD_DEF_REUSE_INDEX_ARRAY_SIZE;

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    STRCPY (pHttp->au1Name, "CONTEXT_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    VcmIsVrfExist (pHttp->au1Value, (UINT4 *) &i4CxtId);

    if (BgpSetContext ((INT4) i4CxtId) == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
        return;
    }

    /*If reset then set the default parameters */
    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (0 == STRCMP (pHttp->au1Value, "Reset"))
    {
        nmhSetFsbgp4RfdDecayHalfLifeTime (i4HalfLifeTime);
        nmhSetFsbgp4RfdReuse (i4ReuseValue);
        nmhSetFsbgp4RfdCutOff (i4SuppressVal);
        nmhSetFsbgp4RfdMaxHoldDownTime (i4MaxSuppressTime);
        nmhSetFsbgp4RfdReuseTimerGranularity (i4ReuseTimerGranularity);
        nmhSetFsbgp4RfdDecayTimerGranularity (i4DecayTimerGranularity);
        nmhSetFsbgp4RfdReuseIndxArraySize (i4ReuseArrayIndex);
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssProcessBgpRfdConfPageGet (pHttp);
        return;
    }
    STRCPY (pHttp->au1Name, "halfLifeTime");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4HalfLifeTime = ATOI (pHttp->au1Value);
    nmhSetFsbgp4RfdDecayHalfLifeTime (i4HalfLifeTime);

    STRCPY (pHttp->au1Name, "reuseValue");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4ReuseValue = ATOI (pHttp->au1Value);
    nmhSetFsbgp4RfdReuse (i4ReuseValue);

    STRCPY (pHttp->au1Name, "suppressValue");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4SuppressVal = ATOI (pHttp->au1Value);
    nmhSetFsbgp4RfdCutOff (i4SuppressVal);

    STRCPY (pHttp->au1Name, "maxSuppressTime");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4MaxSuppressTime = ATOI (pHttp->au1Value);
    nmhSetFsbgp4RfdMaxHoldDownTime (i4MaxSuppressTime);

    STRCPY (pHttp->au1Name, "decayTimerGranularity");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4DecayTimerGranularity = ATOI (pHttp->au1Value);
    nmhSetFsbgp4RfdDecayTimerGranularity (i4DecayTimerGranularity);

    STRCPY (pHttp->au1Name, "reuseTimerGranularity");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4ReuseTimerGranularity = ATOI (pHttp->au1Value);
    nmhSetFsbgp4RfdReuseTimerGranularity (i4ReuseTimerGranularity);

    STRCPY (pHttp->au1Name, "reuseArrayIndex");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4ReuseArrayIndex = ATOI (pHttp->au1Value);
    nmhSetFsbgp4RfdReuseIndxArraySize (i4ReuseArrayIndex);

    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessBgpRfdConfPageGet (pHttp);
    return;

}

/*********************************************************************
*  Function Name : IssProcessBgpTimerconfPage
*  Description   : This function processes the request coming for the
*                  BGP Timer configuration page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessBgpTimerconfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessBgpTimerconfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessBgpTimerconfPageSet (pHttp);
    }
}

/*********************************************************************
 *  Function Name : IssProcessBgpTimerconfPageGet
 *  Description   : This function processes the GET request coming for the
 *                  BGP Timer configuration page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpTimerconfPageGet (tHttp * pHttp)
{
    UINT4               u4IpAddress = 0;
    INT4                i4Temp = 0;
    INT4                i4AddrType = 0;
    INT4                i4RetVal = 0;
    INT4                i4BgpContextId = 0;
    INT4                i4BgpPrevContextId = 0;
    UINT1              *pu1TempString = NULL;
    UINT1               au1Ip6Addr[ISS_BGP4_IP6_ADDR_STRING_LEN];
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddr;
    UINT1               au1LocalIpAddress[ISS_BGP4_IP_MAX_LENGTH];
    UINT1               au1BgpCxtName[VCM_ALIAS_MAX_LEN];

    MEMSET (au1LocalIpAddress, 0, ISS_BGP4_IP_MAX_LENGTH);
    MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
    MEMSET (au1BgpCxtName, 0, VCM_ALIAS_MAX_LEN);

    PeerRemoteAddr.pu1_OctetList = au1LocalIpAddress;
    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    pHttp->i4Write = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    if (nmhGetFirstIndexFsMIBgpContextTable (&i4BgpContextId) == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }
    i4Temp = pHttp->i4Write;

    do
    {
        pHttp->i4Write = i4Temp;

        if (BgpSetContext (i4BgpContextId) == SNMP_FAILURE)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
            return;
        }

        if (nmhGetFirstIndexFsbgp4MpeBgpPeerTable (&i4AddrType,
                                                   &PeerRemoteAddr) ==
            SNMP_FAILURE)
        {
            i4BgpPrevContextId = i4BgpContextId;

            if (nmhGetNextIndexFsMIBgpContextTable (i4BgpPrevContextId,
                                                    &i4BgpContextId) !=
                SNMP_SUCCESS)
            {

                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
                WebnmSockWrite (pHttp,
                                (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                                (pHttp->i4HtmlSize - pHttp->i4Write));
                return;
            }
            continue;
        }

        /*Process the BGP Peer Table table */
        do
        {

            STRCPY (pHttp->au1KeyString, "fsbgp4mpebgpPeerRemoteAddrType_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4AddrType);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "fsbgp4mpebgpPeerIdentifier_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (i4AddrType == BGP4_INET_AFI_IPV4)
            {
                MEMCPY (&u4IpAddress,
                        PeerRemoteAddr.pu1_OctetList, PeerRemoteAddr.i4_Length);
                WEB_CONVERT_IPADDR_TO_STR (pu1TempString,
                                           OSIX_NTOHL (u4IpAddress));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1TempString);
            }
#ifdef BGP4_IPV6_WANTED
            else                /* IPV6 processing */
            {
                Bgp4PrintIp6Addr (PeerRemoteAddr.pu1_OctetList, (au1Ip6Addr));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Ip6Addr);
                MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
            }
#endif /* BGP4_IPV6_WANTED */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpebgpPeerKeepAliveConfigured (i4AddrType,
                                                       &PeerRemoteAddr,
                                                       &i4RetVal);
            STRCPY (pHttp->au1KeyString,
                    "fsbgp4mpebgpPeerKeepAliveConfigured_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpebgpPeerHoldTimeConfigured (i4AddrType,
                                                      &PeerRemoteAddr,
                                                      &i4RetVal);
            STRCPY (pHttp->au1KeyString,
                    "fsbgp4mpebgpPeerHoldTimeConfigured_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpebgpPeerMinRouteAdvertisementInterval (i4AddrType,
                                                                 &PeerRemoteAddr,
                                                                 &i4RetVal);
            STRCPY (pHttp->au1KeyString,
                    "fsbgp4mpebgpPeerMinRouteAdvertisementInterval_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpebgpPeerMinASOriginationInterval (i4AddrType,
                                                            &PeerRemoteAddr,
                                                            &i4RetVal);
            STRCPY (pHttp->au1KeyString,
                    "fsbgp4mpebgpPeerMinASOriginationInterval_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpebgpPeerConnectRetryInterval (i4AddrType,
                                                        &PeerRemoteAddr,
                                                        &i4RetVal);
            STRCPY (pHttp->au1KeyString,
                    "fsbgp4mpebgpPeerConnectRetryInterval_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpebgpPeerIdleHoldTimeConfigured (i4AddrType,
                                                          &PeerRemoteAddr,
                                                          &i4RetVal);
            STRCPY (pHttp->au1KeyString,
                    "fsbgp4mpebgpPeerIdleHoldTimeConfigured_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpebgpPeerDelayOpenTimeConfigured (i4AddrType,
                                                           &PeerRemoteAddr,
                                                           &i4RetVal);
            STRCPY (pHttp->au1KeyString,
                    "fsbgp4mpebgpPeerDelayOpenTimeConfigured_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            VcmGetAliasName ((UINT4) i4BgpContextId, au1BgpCxtName);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1BgpCxtName);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        }
        while (nmhGetNextIndexFsbgp4MpeBgpPeerTable (i4AddrType, &i4AddrType, &PeerRemoteAddr, &PeerRemoteAddr) != SNMP_FAILURE);    /*end of do-while */
        i4BgpPrevContextId = i4BgpContextId;
    }
    while (nmhGetNextIndexFsMIBgpContextTable
           (i4BgpPrevContextId, &i4BgpContextId) == SNMP_SUCCESS);

    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
*  Function Name : IssProcessBgpglobalConfScalarsPage
*  Description   : This function processes the request coming for the
*                  BGP Global Scalars configuration page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessBgpglobalConfScalarsPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessBgpglobalConfScalarsPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessBgpglobalConfScalarsPageSet (pHttp);
    }
}

/*********************************************************************
 *  Function Name : IssProcessBgpglobalConfScalarsPageGet
 *  Description   : This function processes the request coming for the
 *                  Bgp Global Configuration Settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpglobalConfScalarsPageGet (tHttp * pHttp)
{
    INT4                i4Value = 0;
    INT4                i4Admin = 0;
    INT4                i4BgpContextId = 0;
    INT4                i4BgpPrevContextId = 0;
    INT4                i4Temp = 0;
    INT4                i4CapStat = 0;
    UINT4               u4IpAddress = 0;
    UINT4               u4Metric = 0;
    UINT1              *pu1TempString = NULL;
    UINT1               au1LocalClusterId[ISS_BGP4_IP_MAX_LENGTH];
    UINT1               au1BgpCxtName[VCM_ALIAS_MAX_LEN];
    UINT4               u4TableVersion = 0;
    tSNMP_OCTET_STRING_TYPE ClusterId;

    MEMSET (au1LocalClusterId, 0, ISS_BGP4_IP_MAX_LENGTH);
    MEMSET (au1BgpCxtName, 0, VCM_ALIAS_MAX_LEN);

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    pHttp->i4Write = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    if (nmhGetFirstIndexFsMIBgpContextTable (&i4BgpContextId) == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }
    i4Temp = pHttp->i4Write;

    do
    {
        pHttp->i4Write = i4Temp;
        if (BgpSetContext (i4BgpContextId) == SNMP_FAILURE)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
            return;
        }

        ClusterId.pu1_OctetList = au1LocalClusterId;
        nmhGetFsbgp4RflbgpClusterId (&ClusterId);
        STRCPY (pHttp->au1KeyString, "fsbgp4RflbgpClusterId_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        MEMCPY (&u4IpAddress, ClusterId.pu1_OctetList, ClusterId.i4_Length);
        WEB_CONVERT_IPADDR_TO_STR (pu1TempString, OSIX_NTOHL (u4IpAddress));
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1TempString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4NextHopProcessingInterval (&i4Value);
        STRCPY (pHttp->au1KeyString, "fsbgp4NextHopProcessingInterval_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Value);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4RRDDefaultMetric (&u4Metric);
        STRCPY (pHttp->au1KeyString, "fsbgp4RRDDefaultMetric_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4Metric);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4RfdAdminStatus (&i4Admin);
        STRCPY (pHttp->au1KeyString, "fsbgp4RfdAdminStatus_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Admin);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4CapabilitySupportAvailable (&i4CapStat);
        STRCPY (pHttp->au1KeyString, "fsbgp4CapabilitySupportAvailable_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4CapStat);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        nmhGetFsbgp4IBGPMaxPaths (&i4Value);
        STRCPY (pHttp->au1KeyString, "fsbgp4IBGPMaxPaths_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Value);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4EBGPMaxPaths (&i4Value);
        STRCPY (pHttp->au1KeyString, "fsbgp4EBGPMaxPaths_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Value);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsbgp4EIBGPMaxPaths (&i4Value);
        STRCPY (pHttp->au1KeyString, "fsbgp4EIBGPMaxPaths_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Value);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "BGP_TABLE_VERSION_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4TableVersion = BgpGetTableVersion ((UINT4) i4BgpContextId);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4TableVersion);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4BgpContextId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        i4BgpPrevContextId = i4BgpContextId;
    }
    while (nmhGetNextIndexFsMIBgpContextTable
           (i4BgpPrevContextId, &i4BgpContextId) == SNMP_SUCCESS);
    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
 *  Function Name : IssProcessBgpglobalConfScalarsPageSet
 *  Description   : This function processes the request coming for the
 *                  Bgp Global Configuration Scalar page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpglobalConfScalarsPageSet (tHttp * pHttp)
{
    INT4                i4NextHop = 0;
    INT4                i4Len = 0;
    INT4                i4CxtId = 0;
    INT4                i4Val = 0;
    INT4                i4CapStat = 0;
    tAddrPrefix         IpAddress;
    UINT4               u4ErrorCode = 0;
    tUtlInAddr          InAddr;
    INT4                i4GlobalAdmin = 0;
    INT4                i4DefaultMetric = 0;
    UINT4               u4DefaultMetric = 0;
    UINT4               u4TempClID = 0;
    INT4                i4Admin = 0;
    tSNMP_OCTET_STRING_TYPE ClusterId;
    UINT1               au1ClusterId[ISS_BGP4_IP_MAX_LENGTH];

    MEMSET (&ClusterId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&IpAddress, 0, sizeof (tAddrPrefix));
    MEMSET (au1ClusterId, 0, ISS_BGP4_IP_MAX_LENGTH);

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();
    STRCPY (pHttp->au1Name, "CONTEXT_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4CxtId = ATOI (pHttp->au1Value);

    if (BgpSetContext (i4CxtId) == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
        return;
    }

    nmhGetFsbgp4GlobalAdminStatus (&i4GlobalAdmin);
    if (i4GlobalAdmin != ISS_BGP4_ADMIN_UP)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Enable BGP Admin");
        return;
    }

    STRCPY (pHttp->au1Name, "ACTION");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) != SUCCESS)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Request");
        return;
    }

    if (STRCMP (pHttp->au1Value, ISS_APPLY) == 0)
    {
        STRCPY (pHttp->au1Name, "fsbgp4RflbgpClusterId");
        if (HttpGetValuebyName (pHttp->au1Name,
                                pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            i4Len = CliDotStrLength (pHttp->au1Value);
            if (i4Len == 0)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "\rInvalid Cluster Id\r\n");
                return;
            }
            if (i4Len == sizeof (UINT4))
            {
                IpAddress.u2Afi = BGP4_INET_AFI_IPV4;
                if (0 == UtlInetAton ((const CHR1 *) pHttp->au1Value, &InAddr))
                {
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *) "\rInvalid Cluster Id\r\n");
                    return;
                }
                MEMCPY ((IpAddress.au1Address), &(InAddr.u4Addr),
                        sizeof (UINT4));
                IpAddress.u2AddressLen = sizeof (UINT4);
            }
            if (i4Len == 1)
            {
                u4TempClID = (UINT4) ATOI (pHttp->au1Value);
                if (u4TempClID == 0)
                {
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *) "\rInvalid Cluster Id\r\n");
                    return;
                }
                u4TempClID = OSIX_NTOHL (u4TempClID);
                MEMCPY ((IpAddress.au1Address), &u4TempClID, sizeof (UINT4));
                IpAddress.u2AddressLen = sizeof (UINT4);
            }

            ClusterId.pu1_OctetList = au1ClusterId;
            MEMCPY (ClusterId.pu1_OctetList, IpAddress.au1Address,
                    IpAddress.u2AddressLen);
            ClusterId.i4_Length = IpAddress.u2AddressLen;
        }
        if (nmhTestv2Fsbgp4RflbgpClusterId (&u4ErrorCode, &ClusterId) ==
            SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Invalid Cluster Id.");
            return;
        }
        if (nmhSetFsbgp4RflbgpClusterId (&ClusterId) == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set Cluster Id.");
            return;
        }
        STRCPY (pHttp->au1Name, "fsbgp4NextHopProcessingInterval");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4NextHop = ATOI (pHttp->au1Value);

        if (nmhTestv2Fsbgp4NextHopProcessingInterval (&u4ErrorCode, i4NextHop)
            == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Invalid NextHop .");
            return;
        }

        if (nmhSetFsbgp4NextHopProcessingInterval (i4NextHop) == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set Next Hop.");
            return;
        }

        STRCPY (pHttp->au1Name, "fsbgp4RRDDefaultMetric");
        if (HttpGetValuebyName (pHttp->au1Name,
                                pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4DefaultMetric = ATOI (pHttp->au1Value);
            u4DefaultMetric = (UINT4) i4DefaultMetric;
        }
        if (nmhTestv2Fsbgp4RRDDefaultMetric (&u4ErrorCode, u4DefaultMetric)
            == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Invalid Default Metric Value");
            return;
        }
        if (nmhSetFsbgp4RRDDefaultMetric (u4DefaultMetric) == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set the Default Metric");
            return;
        }

        STRCPY (pHttp->au1Name, "fsbgp4RfdAdminStatus");
        if (HttpGetValuebyName (pHttp->au1Name,
                                pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4Admin = ATOI (pHttp->au1Value);
        }
        if (nmhTestv2Fsbgp4RfdAdminStatus (&u4ErrorCode, i4Admin)
            == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Invalid Admin Status");
            return;
        }
        if (nmhSetFsbgp4RfdAdminStatus (i4Admin) == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set Admin Status");
            return;
        }
        if (nmhSetFsbgp4GlobalAdminStatus (ISS_BGP4_ADMIN_DOWN) == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set the Capability status");
            return;
        }
        STRCPY (pHttp->au1Name, "fsbgp4CapabilitySupportAvailable");
        if (HttpGetValuebyName (pHttp->au1Name,
                                pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4CapStat = ATOI (pHttp->au1Value);
        }
        if (nmhTestv2Fsbgp4CapabilitySupportAvailable (&u4ErrorCode, i4CapStat)
            == SNMP_FAILURE)
        {
            nmhSetFsbgp4GlobalAdminStatus (ISS_BGP4_ADMIN_UP);
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Invalid Capability Support Staus");
            return;
        }
        if (nmhSetFsbgp4CapabilitySupportAvailable (i4CapStat) == SNMP_FAILURE)
        {
            nmhSetFsbgp4GlobalAdminStatus (ISS_BGP4_ADMIN_UP);
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unable to set Capability Support Status");
            return;
        }
        if (nmhSetFsbgp4GlobalAdminStatus (ISS_BGP4_ADMIN_UP) == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set the Capability status");
            return;
        }
        STRCPY (pHttp->au1Name, "fsbgp4IBGPMaxPaths");
        if (HttpGetValuebyName (pHttp->au1Name,
                                pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4Val = ATOI (pHttp->au1Value);
        }
        if (nmhTestv2Fsbgp4IBGPMaxPaths (&u4ErrorCode, i4Val) == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Invalid iBgp Configuration");
            return;
        }
        if (nmhSetFsbgp4IBGPMaxPaths (i4Val) == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set iBgp Multipath Count");
            return;
        }

        STRCPY (pHttp->au1Name, "fsbgp4EBGPMaxPaths");
        if (HttpGetValuebyName (pHttp->au1Name,
                                pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4Val = ATOI (pHttp->au1Value);
        }
        if (nmhTestv2Fsbgp4EBGPMaxPaths (&u4ErrorCode, i4Val) == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Invalid eBgp Configuration");
            return;
        }
        if (nmhSetFsbgp4EBGPMaxPaths (i4Val) == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set eBgp Multipath Count");
            return;
        }

        STRCPY (pHttp->au1Name, "fsbgp4EIBGPMaxPaths");
        if (HttpGetValuebyName (pHttp->au1Name,
                                pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4Val = ATOI (pHttp->au1Value);
        }
        if (nmhTestv2Fsbgp4EIBGPMaxPaths (&u4ErrorCode, i4Val) == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Invalid eiBgp Configuration");
            return;
        }
        if (nmhSetFsbgp4EIBGPMaxPaths (i4Val) == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set eiBgp Multipath Count");
            return;
        }
    }
    else
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Non existant button press regis.");
        return;
    }
    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessBgpglobalConfScalarsPageGet (pHttp);
}

/*********************************************************************
 *  Function Name : IssProcessBgpTimerconfPageSet
 *  Description   : This function processes the request coming for the
 *                  BGP Timer configuration page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpTimerconfPageSet (tHttp * pHttp)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4CxtId = 0;
    INT4                i4MinRouteAdvtInterval = 0;
    INT4                i4HoldTime = 0;
    INT4                i4ConnectRetryInterval = 0;
    INT4                i4IdleHoldInterval = 0;
    INT4                i4DelayOpenInterval = 0;
    INT4                i4AddrType = 0;
    INT4                i4KeepAliveTime = 0;
    INT4                i4MinAsOrigInterval = 0;
    INT4                i4AutoStatus = 0;
    INT4                i4DelayStatus = 0;
    INT4                i4DampStatus = 0;
    INT1                i1RetVal = SNMP_FAILURE;
    tUtlInAddr          InAddr;
    tNetAddress         IpAddr;
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddr;
    UINT1               au1LocalIpAddress[ISS_BGP4_IP_MAX_LENGTH];

    MEMSET (&IpAddr, 0, sizeof (tNetAddress));
    MEMSET (au1LocalIpAddress, 0, ISS_BGP4_IP_MAX_LENGTH);
    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (0 == STRCMP (pHttp->au1Value, "Apply"))
    {
        STRCPY (pHttp->au1Name, "CONTEXT_ID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        VcmIsVrfExist (pHttp->au1Value, (UINT4 *) &i4CxtId);
    }
    else
    {
        STRCPY (pHttp->au1Name, "CONTEXT_ID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4CxtId = ATOI (pHttp->au1Value);
    }

    if (BgpSetContext (i4CxtId) == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
        return;
    }

    STRCPY (pHttp->au1Name, "fsbgp4mpebgpPeerRemoteAddrType");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
        i4AddrType = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4mpebgpPeerKeepAliveConfigured");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
        i4KeepAliveTime = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4mpebgpPeerHoldTimeConfigured");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
        i4HoldTime = ATOI (pHttp->au1Value);

    if (i4KeepAliveTime > i4HoldTime)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *)
                      "\r Please configure Hold time greater than keep alive time\r\n");
        return;
    }

    STRCPY (pHttp->au1Name, "fsbgp4mpebgpPeerMinRouteAdvertisementInterval");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
        i4MinRouteAdvtInterval = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4mpebgpPeerMinASOriginationInterval");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
        i4MinAsOrigInterval = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4mpebgpPeerConnectRetryInterval");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
        i4ConnectRetryInterval = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4mpebgpPeerIdleHoldTimeConfigured");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
        i4IdleHoldInterval = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4mpebgpPeerDelayOpenTimeConfigured");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
        i4DelayOpenInterval = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4mpebgpPeerIdentifier");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);

        if (i4AddrType == BGP4_INET_AFI_IPV4)
        {
            IpAddr.NetAddr.u2Afi = BGP4_INET_AFI_IPV4;
            IpAddr.u2Safi = BGP4_INET_SAFI_UNICAST;
            if (0 == BGP4_INET_ATON (pHttp->au1Value, &InAddr))
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "\r Invalid address\r\n");
                return;
            }
            MEMCPY ((IpAddr.NetAddr.au1Address),
                    &InAddr.u4Addr, sizeof (UINT4));

            IpAddr.NetAddr.u2AddressLen = sizeof (UINT4);

        }
#ifdef BGP4_IPV6_WANTED
        else if (str_to_ip6addr (pHttp->au1Value) != NULL)
        {
            IpAddr.NetAddr.u2Afi = BGP4_INET_AFI_IPV6;
            IpAddr.u2Safi = BGP4_INET_SAFI_UNICAST;
            MEMCPY (IpAddr.NetAddr.au1Address,
                    str_to_ip6addr (pHttp->au1Value), BGP4_IPV6_PREFIX_LEN);

            IpAddr.NetAddr.u2AddressLen = BGP4_IPV6_PREFIX_LEN;
        }
#endif /* BGP4_IPV6_WANTED */
    }
    PeerRemoteAddr.i4_Length = IpAddr.NetAddr.u2AddressLen;
    PeerRemoteAddr.pu1_OctetList = au1LocalIpAddress;
    MEMCPY ((PeerRemoteAddr.pu1_OctetList),
            (IpAddr.NetAddr.au1Address), IpAddr.NetAddr.u2AddressLen);

    /*Validate the indices passed from the web page */
    i1RetVal = nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable (i4AddrType,
                                                              &PeerRemoteAddr);

    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);

        IssSendError (pHttp, (CONST INT1 *)
                      "\r Invalid indices to the peer table\r\n");
        return;
    }

    i1RetVal = nmhTestv2Fsbgp4mpebgpPeerConnectRetryInterval (&u4ErrorCode,
                                                              i4AddrType,
                                                              &PeerRemoteAddr,
                                                              i4ConnectRetryInterval);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);

        IssSendError (pHttp, (CONST INT1 *)
                      "\r Invalid Connect Retry interval value\r\n");
        return;
    }

    i1RetVal = nmhSetFsbgp4mpebgpPeerConnectRetryInterval (i4AddrType,
                                                           &PeerRemoteAddr,
                                                           i4ConnectRetryInterval);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);

        IssSendError (pHttp, (CONST INT1 *)
                      "\r Unable to set the connect retry interval\r\n");
        return;
    }

    i1RetVal = nmhTestv2Fsbgp4mpebgpPeerHoldTimeConfigured (&u4ErrorCode,
                                                            i4AddrType,
                                                            &PeerRemoteAddr,
                                                            i4HoldTime);

    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);

        IssSendError (pHttp, (CONST INT1 *) "\r Invalid Hold time \r\n");
        return;
    }

    i1RetVal = nmhSetFsbgp4mpebgpPeerHoldTimeConfigured (i4AddrType,
                                                         &PeerRemoteAddr,
                                                         i4HoldTime);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);

        IssSendError (pHttp, (CONST INT1 *) "\r Unable to set hold time\r\n");
        return;
    }

    i1RetVal = nmhGetFsbgp4mpePeerAllowAutomaticStart (i4AddrType,
                                                       &PeerRemoteAddr,
                                                       &i4AutoStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);

        IssSendError (pHttp, (CONST INT1 *) "\r Unable to get AutoStart"
                      " Status! Cannot Configure Idle Hold Time \r\n");
        return;
    }

    i1RetVal = nmhGetFsbgp4mpeDampPeerOscillations (i4AddrType,
                                                    &PeerRemoteAddr,
                                                    &i4DampStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);

        IssSendError (pHttp, (CONST INT1 *) "\r Unable to get Damp Peer"
                      " Status! Cannot Configure Idle Hold Time \r\n");
        return;
    }

    i1RetVal = nmhGetFsbgp4mpePeerDelayOpen (i4AddrType,
                                             &PeerRemoteAddr, &i4DelayStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);

        IssSendError (pHttp, (CONST INT1 *) "\r Unable to get DelayOpen"
                      " Status! Cannot Configure DelayOpen Time \r\n");
        return;
    }

    if (((i4AutoStatus == BGP4_PEER_AUTOMATICSTART_ENABLE) ||
         (i4DampStatus == BGP4_DAMP_PEER_OSCILLATIONS_ENABLE)) &&
        (i4IdleHoldInterval != 0))
    {

        i1RetVal =
            nmhTestv2Fsbgp4mpebgpPeerIdleHoldTimeConfigured (&u4ErrorCode,
                                                             i4AddrType,
                                                             &PeerRemoteAddr,
                                                             i4IdleHoldInterval);

        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);

            IssSendError (pHttp,
                          (CONST INT1 *) "\r Invalid Idle Hold time \r\n");
            return;
        }

        i1RetVal = nmhSetFsbgp4mpebgpPeerIdleHoldTimeConfigured (i4AddrType,
                                                                 &PeerRemoteAddr,
                                                                 i4IdleHoldInterval);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);

            IssSendError (pHttp,
                          (CONST INT1 *) "\r Unable to set Idle Hold time\r\n");
            return;
        }

    }

    if (i4DelayStatus == BGP4_PEER_DELAY_OPEN_ENABLE)
    {

        i1RetVal =
            nmhTestv2Fsbgp4mpebgpPeerDelayOpenTimeConfigured (&u4ErrorCode,
                                                              i4AddrType,
                                                              &PeerRemoteAddr,
                                                              i4DelayOpenInterval);

        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);

            IssSendError (pHttp,
                          (CONST INT1 *) "\r Invalid DelayOpen time \r\n");
            return;
        }

        i1RetVal = nmhSetFsbgp4mpebgpPeerDelayOpenTimeConfigured (i4AddrType,
                                                                  &PeerRemoteAddr,
                                                                  i4DelayOpenInterval);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);

            IssSendError (pHttp,
                          (CONST INT1 *) "\r Unable to set DelayOpen time\r\n");
            return;
        }
    }

    i1RetVal = nmhTestv2Fsbgp4mpebgpPeerKeepAliveConfigured (&u4ErrorCode,
                                                             i4AddrType,
                                                             &PeerRemoteAddr,
                                                             i4KeepAliveTime);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);

        IssSendError (pHttp, (CONST INT1 *) "\r Invalid Keep alive\r\n");
        return;
    }

    i1RetVal = nmhSetFsbgp4mpebgpPeerKeepAliveConfigured (i4AddrType,
                                                          &PeerRemoteAddr,
                                                          i4KeepAliveTime);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);

        IssSendError (pHttp, (CONST INT1 *)
                      "\r Unable to set Keepalive time\r\n");
        return;
    }

    i1RetVal = nmhTestv2Fsbgp4mpebgpPeerMinASOriginationInterval (&u4ErrorCode,
                                                                  i4AddrType,
                                                                  &PeerRemoteAddr,
                                                                  i4MinAsOrigInterval);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);

        IssSendError (pHttp, (CONST INT1 *)
                      "\r Invalid Min As origination interval\r\n");
        return;
    }

    i1RetVal = nmhSetFsbgp4mpebgpPeerMinASOriginationInterval (i4AddrType,
                                                               &PeerRemoteAddr,
                                                               i4MinAsOrigInterval);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);

        IssSendError (pHttp, (CONST INT1 *)
                      "\r Unable to set MinAsOrigination  interval\r\n");
        return;
    }

    i1RetVal =
        nmhTestv2Fsbgp4mpebgpPeerMinRouteAdvertisementInterval (&u4ErrorCode,
                                                                i4AddrType,
                                                                &PeerRemoteAddr,
                                                                i4MinRouteAdvtInterval);

    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);

        IssSendError (pHttp, (CONST INT1 *)
                      "\r Invalid Route Advertisement interval\r\n");
        return;
    }
    i1RetVal = nmhSetFsbgp4mpebgpPeerMinRouteAdvertisementInterval (i4AddrType,
                                                                    &PeerRemoteAddr,
                                                                    i4MinRouteAdvtInterval);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);

        IssSendError (pHttp, (CONST INT1 *)
                      "\r Unable to set MinRouteAdvtInterval\r\n");
        return;
    }

    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);

    IssProcessBgpTimerconfPageGet (pHttp);
    return;
}

/*********************************************************************
*  Function Name : IssProcessBgpPeerconfPage
*  Description   : This function processes the request coming for the
*                  BGP Peer configuration page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessBgpPeerConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessBgpPeerConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessBgpPeerConfPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessBgpPeerConfPageGet
*  Description   : This function processes the GET request coming for the
*                  BGP Peer configuration page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessBgpPeerConfPageGet (tHttp * pHttp)
{
    UINT4               u4IpAddress = 0;
    INT4                i4BgpContextId = 0;
    INT4                i4BgpPrevContextId = 0;
    INT4                i4AddrType = 0;
    INT4                i4Temp = 0;
    INT4                i4RetVal = 0;
    INT4                i4MktId;
    tAddrPrefix         AoPeerAddr;
    UINT1              *pu1TempString = NULL;
    UINT1               au1Ip6Addr[ISS_BGP4_IP6_ADDR_STRING_LEN];
    UINT1               au1LocalIpAddress[ISS_BGP4_IP_MAX_LENGTH];
    UINT1               au1TempIpAddress[ISS_BGP4_IP_MAX_LENGTH];
    UINT1               au1Password[ISS_BGP_PASSWORD_MAX_LENGTH + 4];
    tSNMP_OCTET_STRING_TYPE Password;
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddr;
    tSNMP_OCTET_STRING_TYPE TempAddr;
    UINT1               au1BgpCxtName[VCM_ALIAS_MAX_LEN];

    MEMSET (au1Password, 0, sizeof (au1Password));
    MEMSET (au1BgpCxtName, 0, VCM_ALIAS_MAX_LEN);
    Password.pu1_OctetList = au1Password;
    Password.i4_Length = (INT4) STRLEN (au1Password);

    MEMSET (au1LocalIpAddress, 0, ISS_BGP4_IP_MAX_LENGTH);
    MEMSET (au1TempIpAddress, 0, ISS_BGP4_IP_MAX_LENGTH);
    MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));

    PeerRemoteAddr.pu1_OctetList = au1LocalIpAddress;
    TempAddr.pu1_OctetList = au1TempIpAddress;

    pHttp->i4Write = 0;
    IssPrintAvailableBgpContextId (pHttp);

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    if (nmhGetFirstIndexFsMIBgpContextTable (&i4BgpContextId) == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;

    }

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    i4Temp = pHttp->i4Write;

    do
    {
        pHttp->i4Write = i4Temp;

        if (BgpSetContext (i4BgpContextId) == SNMP_FAILURE)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
            return;
        }

        if (nmhGetFirstIndexFsbgp4MpePeerExtTable (&i4AddrType,
                                                   &PeerRemoteAddr) ==
            SNMP_FAILURE)
        {
            i4BgpPrevContextId = i4BgpContextId;

            if (nmhGetNextIndexFsMIBgpContextTable (i4BgpPrevContextId,
                                                    &i4BgpContextId) !=
                SNMP_SUCCESS)
            {
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
                WebnmSockWrite (pHttp,
                                (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                                (pHttp->i4HtmlSize - pHttp->i4Write));
                return;
            }
            continue;
        }

        /*Process the BGP peer table */
        do
        {
            pHttp->i4Write = i4Temp;

            STRCPY (pHttp->au1KeyString, "fsbgp4PeerExtPeerRemoteAddr_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (i4AddrType == BGP4_INET_AFI_IPV4)
            {
                MEMCPY (&u4IpAddress,
                        PeerRemoteAddr.pu1_OctetList, PeerRemoteAddr.i4_Length);
                WEB_CONVERT_IPADDR_TO_STR (pu1TempString,
                                           OSIX_NTOHL (u4IpAddress));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1TempString);
            }
#ifdef BGP4_IPV6_WANTED
            else                /* BGP ipv6 processing */
            {
                Bgp4PrintIp6Addr (PeerRemoteAddr.pu1_OctetList, (au1Ip6Addr));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Ip6Addr);
                MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
            }
#endif /* BGP4_IPV6_WANTED */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpePeerExtPeerRemoteAs (i4AddrType,
                                                &PeerRemoteAddr,
                                                (UINT4 *) &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsbgp4PeerExtPeerRemoteAs_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", (UINT4) i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            /* RFC 4271 Updates */
            nmhGetFsbgp4mpePeerPrefixUpperLimit (i4AddrType,
                                                 &PeerRemoteAddr, &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsbgp4mpePeerPrefixUpperLimit_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpePeerTcpConnectRetryCnt (i4AddrType,
                                                   &PeerRemoteAddr, &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsbgp4mpePeerTcpConnectRetryCnt_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpePeerAllowAutomaticStart (i4AddrType,
                                                    &PeerRemoteAddr, &i4RetVal);
            STRCPY (pHttp->au1KeyString,
                    "fsbgp4mpePeerAllowAutomaticStart_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpePeerAllowAutomaticStop (i4AddrType,
                                                   &PeerRemoteAddr, &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsbgp4mpePeerAllowAutomaticStop_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpeDampPeerOscillations (i4AddrType,
                                                 &PeerRemoteAddr, &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsbgp4mpeDampPeerOscillations_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpePeerDelayOpen (i4AddrType, &PeerRemoteAddr,
                                          &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsbgp4mpePeerDelayOpen_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpePeerExtEBGPMultiHop (i4AddrType,
                                                &PeerRemoteAddr, &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsbgp4PeerExtEBGPMultiHop_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpePeerExtNextHopSelf (i4AddrType, &PeerRemoteAddr,
                                               &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsbgp4PeerExtNextHopSelf_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpePeerExtLclAddress (i4AddrType, &PeerRemoteAddr,
                                              &TempAddr);
            STRCPY (pHttp->au1KeyString, "fsbgp4PeerExtLclAddress_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (i4AddrType == BGP4_INET_AFI_IPV4)
            {
                MEMCPY (&u4IpAddress, TempAddr.pu1_OctetList,
                        TempAddr.i4_Length);
                WEB_CONVERT_IPADDR_TO_STR (pu1TempString,
                                           OSIX_NTOHL (u4IpAddress));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1TempString);
            }
#ifdef BGP4_IPV6_WANTED
            else                /* IPV6 processing */
            {
                Bgp4PrintIp6Addr (TempAddr.pu1_OctetList, (au1Ip6Addr));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Ip6Addr);
                MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
            }
#endif /* BGP4_IPV6_WANTED */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpePeerExtGateway (i4AddrType, &PeerRemoteAddr,
                                           &TempAddr);
            STRCPY (pHttp->au1KeyString, "fsbgp4PeerExtGateway_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (i4AddrType == BGP4_INET_AFI_IPV4)
            {
                MEMCPY (&u4IpAddress, TempAddr.pu1_OctetList,
                        TempAddr.i4_Length);
                WEB_CONVERT_IPADDR_TO_STR (pu1TempString,
                                           OSIX_NTOHL (u4IpAddress));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1TempString);
            }
#ifdef BGP4_IPV6_WANTED
            else                /* IPV6 processing */
            {
                Bgp4PrintIp6Addr (TempAddr.pu1_OctetList, (au1Ip6Addr));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Ip6Addr);
                MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
            }
#endif /* BGP4_IPV6_WANTED */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpePeerExtDefaultOriginate (i4AddrType,
                                                    &PeerRemoteAddr, &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsbgp4PeerExtDefaultOriginate_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpePeerExtCommSendStatus (i4AddrType,
                                                  &PeerRemoteAddr, &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsbgp4PeerExtCommSendStatus_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpePeerExtECommSendStatus (i4AddrType,
                                                   &PeerRemoteAddr, &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsbgp4PeerExtECommSendStatus_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpePeerExtRflClient (i4AddrType, &PeerRemoteAddr,
                                             &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsbgp4PeerExtRflClient_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpePeerExtPassive (i4AddrType, &PeerRemoteAddr,
                                           &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsbgp4PeerExtPassive_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpePeerExtEBGPHopLimit (i4AddrType,
                                                &PeerRemoteAddr, &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsbgp4PeerExtEBGPHopLimit_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpePeerExtTcpSendBufSize (i4AddrType,
                                                  &PeerRemoteAddr, &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsbgp4PeerExtTcpSendBufSize_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpePeerExtTcpRcvBufSize (i4AddrType,
                                                 &PeerRemoteAddr, &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsbgp4PeerExtTcpRcvBufSize_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4TCPMD5AuthPassword (i4AddrType, &PeerRemoteAddr,
                                            &Password);
            if (Password.i4_Length != 0)
            {
                STRCPY (pHttp->au1KeyString, "fsbgp4Pwd_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         Password.pu1_OctetList);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }
            else
            {
                STRCPY (pHttp->au1KeyString, "fsbgp4Pwd_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "");
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }

            /*Find TCP-AO MKT ID associated with peer */
            i4RetVal = Bgp4LowGetIpAddress (i4AddrType,
                                            &PeerRemoteAddr, &AoPeerAddr);
            i4RetVal =
                Bgp4GetFirstTCPAOKeyId ((UINT4) i4BgpContextId, AoPeerAddr,
                                        &i4MktId);
            if (i4RetVal != BGP4_SUCCESS)
            {

                STRCPY (pHttp->au1KeyString, "fsbgp4TcpAO_key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "none");
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            }
            else
            {
                STRCPY (pHttp->au1KeyString, "fsbgp4TcpAO_key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4MktId);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            }
            nmhGetFsbgp4mpebgpPeerAdminStatus (i4AddrType,
                                               &PeerRemoteAddr, &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsbgp4PeerAdminStatus_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpePeerBfdStatus (i4AddrType,
                                          &PeerRemoteAddr, &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsbgp4mpePeerBfdStatus_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            VcmGetAliasName ((UINT4) i4BgpContextId, au1BgpCxtName);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1BgpCxtName);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        }
        while (nmhGetNextIndexFsbgp4MpePeerExtTable (i4AddrType, &i4AddrType, &PeerRemoteAddr, &PeerRemoteAddr) == SNMP_SUCCESS);    /*end of do-while */

        i4BgpPrevContextId = i4BgpContextId;

    }
    while (nmhGetNextIndexFsMIBgpContextTable
           (i4BgpPrevContextId, &i4BgpContextId) == SNMP_SUCCESS);

    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
*  Function Name : IssProcessBgpPeerConfPageSet
*  Description   : This function processes the SET request coming for the
*                  BGP Peer configuration page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessBgpPeerConfPageSet (tHttp * pHttp)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4RemoteAs = BGP4_INV_AS;
    UINT4               u4CurRemoteAs = BGP4_INV_AS;
    UINT4               u4IpAddress = 0;
    INT4                i4CxtId = 0;
    INT4                i4BfdStatus = 0;
    INT4                i4PeerPassive = 0;
    INT4                i4EBGPHopLimit = 0;
    INT4                i4RflClient = ISS_BGP4_RFL_CLIENT;
    INT4                i4ExtCommAction = 0;
    INT4                i4CommAction = 0;
    INT4                i4DefRouteStatus = 0;
    INT4                i4NextHop = 0;
    INT4                i4MultiHopEBGPStatus = 0;
    INT4                i4AutoStartStatus = 0;
    INT4                i4AutoStopStatus = 0;
    INT4                i4DampPeerStatus = 0;
    INT4                i4DelayOpenStatus = 0;
    INT4                i4MaxPrefixLimit = 0;
    INT4                i4ConnectRetryCount = 0;
    INT4                i4AdminStatus = 0;
    INT4                i4TcpSendSize = 65536;
    INT4                i4TcpRcvSize = 65536;
    INT4                i4Len = 0;
    INT4                i4PeerAdminState = 0;
    INT4                i4AddressFamily = 0;
    INT4                i4TcpPwdStatus = 0;
    INT4                i4SupportStatus;
    INT1                i1ChkLocalAddr = 0;
    INT1                i1ChkGatewayAddr = 0;
    INT1                i1Status = 0;
    INT1                i1RetVal = SNMP_FAILURE;
    tUtlInAddr          InAddr;
    tAddrPrefix         IpAddress;
    tAddrPrefix         PeerRemAddr;
    tAddrPrefix         AoPeerAddr;
    INT4                i4MktId = -1;
    INT4                i4MktVal = -1;
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    tSNMP_OCTET_STRING_TYPE LocalAddr;
    tSNMP_OCTET_STRING_TYPE Password;
    tSNMP_OCTET_STRING_TYPE GatewayAddr;
    UINT1               au1PeerIpAddress[ISS_BGP4_IP_MAX_LENGTH];
    UINT1               au1GatewayIpAddress[ISS_BGP4_IP_MAX_LENGTH];
    UINT1               au1LocalIpAddress[ISS_BGP4_IP_MAX_LENGTH];

    MEMSET (au1LocalIpAddress, 0, ISS_BGP4_IP_MAX_LENGTH);
    MEMSET (au1PeerIpAddress, 0, ISS_BGP4_IP_MAX_LENGTH);
    MEMSET (au1GatewayIpAddress, 0, ISS_BGP4_IP_MAX_LENGTH);
    MEMSET (&IpAddress, 0, sizeof (tAddrPrefix));
    MEMSET (&PeerAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&Password, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&LocalAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&GatewayAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if ((0 == STRCMP (pHttp->au1Value, "Delete"))
        || (0 == STRCMP (pHttp->au1Value, "Apply")))
    {
        STRCPY (pHttp->au1Name, "CONTEXT_ID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        VcmIsVrfExist (pHttp->au1Value, (UINT4 *) &i4CxtId);
    }
    else
    {
        STRCPY (pHttp->au1Name, "CONTEXT_ID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4CxtId = ATOI (pHttp->au1Value);
    }

    if (BgpSetContext (i4CxtId) == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
        return;
    }

    STRCPY (pHttp->au1Name, "fsbgp4PeerExtPeerRemoteAddr");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Len = CliDotStrLength (pHttp->au1Value);
        if (i4Len == sizeof (UINT4))
        {
            IpAddress.u2Afi = BGP4_INET_AFI_IPV4;
            i4AddressFamily = IpAddress.u2Afi;
            if (0 == BGP4_INET_ATON (pHttp->au1Value, &InAddr))
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rInvalid peer address\r\n");
                return;
            }
            MEMCPY ((IpAddress.au1Address), &(InAddr.u4Addr), sizeof (UINT4));
            IpAddress.u2AddressLen = sizeof (UINT4);
        }
#ifdef BGP4_IPV6_WANTED
        else if (str_to_ip6addr (pHttp->au1Value) != NULL)
        {
            IpAddress.u2Afi = BGP4_INET_AFI_IPV6;
            i4AddressFamily = IpAddress.u2Afi;
            MEMCPY (IpAddress.au1Address,
                    str_to_ip6addr (pHttp->au1Value), BGP4_IPV6_PREFIX_LEN);
            IpAddress.u2AddressLen = BGP4_IPV6_PREFIX_LEN;
        }
#endif /* BGP4_IPV6_WANTED */

        PeerAddr.pu1_OctetList = au1PeerIpAddress;
        MEMCPY (PeerAddr.pu1_OctetList, IpAddress.au1Address,
                IpAddress.u2AddressLen);
        PeerAddr.i4_Length = IpAddress.u2AddressLen;
    }

    /*Configure the peer parameters */
    switch (i4AddressFamily)
    {
        case BGP4_INET_AFI_IPV4:
            PTR_FETCH4 (u4IpAddress, IpAddress.au1Address);
            Bgp4InitAddrPrefixStruct (&PeerRemAddr, BGP4_INET_AFI_IPV4);
            MEMCPY (PeerRemAddr.au1Address, &u4IpAddress, sizeof (UINT4));
            break;
        case BGP4_INET_AFI_IPV6:
            PeerRemAddr = IpAddress;
            break;
        default:
            break;
    }

    i1RetVal = nmhValidateIndexInstanceFsbgp4MpePeerExtTable (i4AddressFamily,
                                                              &PeerAddr);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid Peer Address\r\n");
        return;
    }

    /*Remote AS */
    STRCPY (pHttp->au1Name, "fsbgp4PeerAdminStatus");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
        i4AdminStatus = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4PeerExtPeerRemoteAs");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
        u4RemoteAs = (UINT4) ATOI (pHttp->au1Value);

    nmhGetFsbgp4FourByteASNSupportStatus (&i4SupportStatus);

    if ((i4SupportStatus == BGP4_DISABLE_4BYTE_ASN_SUPPORT) &&
        (u4RemoteAs > BGP4_MAX_TWO_BYTE_AS))
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "4-byte ASN Capability is disabled. Configure value < 65536");
        IssProcessBgpContextCreationPageGet (pHttp);
        return;
    }

    STRCPY (pHttp->au1Name, "ACTION");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)

    {
        if (0 == STRCMP (pHttp->au1Value, "Delete"))
        {
            i1RetVal = nmhSetFsbgp4mpePeerExtConfigurePeer (i4AddressFamily,
                                                            &PeerAddr,
                                                            ISS_BGP4_DELETE);
            if (i1RetVal == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to delete entry\r\n");
            }
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessBgpPeerConfPageGet (pHttp);
            return;

        }
    }

    /*RFC 4271 Update */
    STRCPY (pHttp->au1Name, "fsbgp4mpePeerPrefixUpperLimit");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
        i4MaxPrefixLimit = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4mpePeerTcpConnectRetryCnt");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
        i4ConnectRetryCount = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4mpePeerAllowAutomaticStart");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery) ==
        ENM_SUCCESS)
        i4AutoStartStatus = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4mpePeerAllowAutomaticStop");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery) ==
        ENM_SUCCESS)
        i4AutoStopStatus = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4mpeDampPeerOscillations");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery) ==
        ENM_SUCCESS)
        i4DampPeerStatus = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4mpePeerDelayOpen");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery) ==
        ENM_SUCCESS)
        i4DelayOpenStatus = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4PeerExtEBGPMultiHop");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery) ==
        ENM_SUCCESS)
        i4MultiHopEBGPStatus = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4PeerExtNextHopSelf");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
        i4NextHop = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4PeerExtLclAddress");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)

    {
        issDecodeSpecialChar (pHttp->au1Value);
        if ((pHttp->au1Value != NULL)
            && (STRCMP (pHttp->au1Value, "0.0.0.0") != 0))
        {
            i4Len = CliDotStrLength (pHttp->au1Value);
            if (i4Len == sizeof (UINT4))
            {
                i1ChkLocalAddr = 1;
                IpAddress.u2Afi = BGP4_INET_AFI_IPV4;
                if (0 == BGP4_INET_ATON (pHttp->au1Value, &InAddr))
                {
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *)
                                  "\rInvalid local address\r\n");
                    return;
                }
                if (InAddr.u4Addr != 0)
                {
                    MEMCPY ((IpAddress.au1Address), &(InAddr.u4Addr),
                            sizeof (UINT4));
                    IpAddress.u2AddressLen = sizeof (UINT4);
                }
                else
                {
                    IpAddress.u2AddressLen = 0;
                }

            }
#ifdef BGP4_IPV6_WANTED
            else if (str_to_ip6addr (pHttp->au1Value) != NULL)
            {
                i1ChkLocalAddr = 1;
                IpAddress.u2Afi = BGP4_INET_AFI_IPV6;
                MEMCPY (IpAddress.au1Address, str_to_ip6addr (pHttp->au1Value),
                        BGP4_IPV6_PREFIX_LEN);
                IpAddress.u2AddressLen = BGP4_IPV6_PREFIX_LEN;
            }
#endif /* BGP4_IPV6_WANTED */
        }
    }
    if (IpAddress.u2AddressLen != 0)
    {
        LocalAddr.pu1_OctetList = au1LocalIpAddress;
        if (i1ChkLocalAddr != 0)
        {
            LocalAddr.i4_Length = IpAddress.u2AddressLen;
            MEMCPY ((LocalAddr.pu1_OctetList),
                    IpAddress.au1Address, IpAddress.u2AddressLen);
        }
    }
    STRCPY (pHttp->au1Name, "fsbgp4PeerExtGateway");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery) ==
        ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        if ((pHttp->au1Value != NULL)
            && (STRCMP (pHttp->au1Value, "0.0.0.0") != 0))
        {
            i4Len = CliDotStrLength (pHttp->au1Value);
            if (i4Len == sizeof (UINT4))
            {
                i1ChkGatewayAddr = 1;
                IpAddress.u2Afi = BGP4_INET_AFI_IPV4;
                if (0 == BGP4_INET_ATON (pHttp->au1Value, &InAddr))
                {
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    return;
                }
                if (InAddr.u4Addr != 0)
                {
                    MEMCPY ((IpAddress.au1Address), &(InAddr.u4Addr),
                            sizeof (UINT4));
                    IpAddress.u2AddressLen = sizeof (UINT4);
                }
                else
                {
                    IpAddress.u2AddressLen = 0;
                }
            }
#ifdef BGP4_IPV6_WANTED
            else if (str_to_ip6addr (pHttp->au1Value) != NULL)
            {
                i1ChkGatewayAddr = 1;
                IpAddress.u2Afi = BGP4_INET_AFI_IPV6;
                MEMCPY (IpAddress.au1Address, str_to_ip6addr (pHttp->au1Value),
                        BGP4_IPV6_PREFIX_LEN);
                IpAddress.u2AddressLen = BGP4_IPV6_PREFIX_LEN;
            }
#endif /* BGP4_IPV6_WANTED */
        }
    }
    if (IpAddress.u2AddressLen != 0)
    {
        GatewayAddr.pu1_OctetList = au1GatewayIpAddress;
        if (i1ChkGatewayAddr != 0)
        {
            GatewayAddr.i4_Length = IpAddress.u2AddressLen;
            MEMCPY ((GatewayAddr.pu1_OctetList),
                    IpAddress.au1Address, IpAddress.u2AddressLen);
        }
    }
    STRCPY (pHttp->au1Name, "fsbgp4PeerExtDefaultOriginate");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery) ==
        ENM_SUCCESS)
        i4DefRouteStatus = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4PeerExtCommSendStatus");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
        i4CommAction = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4PeerExtECommSendStatus");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
        i4ExtCommAction = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4PeerExtRflClient");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
        i4RflClient = ATOI (pHttp->au1Value);

/*   4271 Web Changes */

    STRCPY (pHttp->au1Name, "fsbgp4PeerExtPassive");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
        i4PeerPassive = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4PeerExtEBGPHopLimit");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
        i4EBGPHopLimit = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4PeerExtTcpSendBufSize");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
        i4TcpSendSize = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4PeerExtTcpRcvBufSize");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
        i4TcpRcvSize = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4Pwd");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        Password.pu1_OctetList = pHttp->au1Value;
        Password.i4_Length = (INT4) STRLEN (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "fsbgp4TCPAOAuthKeyId");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
    {

        if (0 != STRCMP (pHttp->au1Value, "none"))
        {
            i4MktId = ATOI (pHttp->au1Value);
        }
    }

    STRCPY (pHttp->au1Name, "fsbgp4mpePeerBfdStatus");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
    {
        i4BfdStatus = ATOI (pHttp->au1Value);
    }

    nmhGetFsbgp4mpePeerExtPeerRemoteAs (i4AddressFamily,
                                        &PeerAddr, &u4CurRemoteAs);
    nmhGetFsbgp4mpebgpPeerAdminStatus (i4AddressFamily,
                                       &PeerAddr, &i4PeerAdminState);

    nmhGetFsbgp4TCPMD5AuthPwdSet (IpAddress.u2Afi, &PeerAddr, &i4TcpPwdStatus);
    if ((u4CurRemoteAs != 0) && (i4PeerAdminState == BGP4_PEER_START))
    {
        if (u4CurRemoteAs != u4RemoteAs)
        {
            /* Disable the peer session and restart the peer session
             *              * after setting the new AS number. */
            i1RetVal = nmhSetFsbgp4mpebgpPeerAdminStatus (i4AddressFamily,
                                                          &PeerAddr,
                                                          BGP4_PEER_STOP);
            if (i1RetVal == SNMP_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to set the peer status\r\n");
                return;
            }

            /* Configure remote AS for the peer */
            nmhSetFsbgp4mpePeerExtPeerRemoteAs (i4AddressFamily,
                                                &PeerAddr, u4RemoteAs);

            i1RetVal = nmhSetFsbgp4mpebgpPeerAdminStatus (i4AddressFamily,
                                                          &PeerAddr,
                                                          BGP4_PEER_START);
            if (i1RetVal == SNMP_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to set the peer status\r\n");
                return;
            }
        }
    }
    if (u4CurRemoteAs == BGP4_INV_AS)
    {

        i1RetVal = nmhTestv2Fsbgp4mpePeerExtPeerRemoteAs (&u4ErrorCode,
                                                          i4AddressFamily,
                                                          &PeerAddr,
                                                          u4RemoteAs);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "\r Invalid Remote AS\r\n");
            return;
        }

        /* Peer Entry may not be existing or existing with a
         *          * INVALID AS no. Try creating a peer entry. */
        i1RetVal = nmhSetFsbgp4mpePeerExtConfigurePeer (i4AddressFamily,
                                                        &PeerAddr,
                                                        BGP4_PEER_CREATE);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          " \rUnable to create peer entry\r\n");
            return;
        }

        i1RetVal = nmhSetFsbgp4mpePeerExtPeerRemoteAs (i4AddressFamily,
                                                       &PeerAddr,
                                                       (INT4) u4RemoteAs);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set the remote AS number\r\n");
            return;
        }
    }

    /* RFC 4271 Updates */

    /* Testing and Setting the BGP Maximum PrefixLimit */
    i1RetVal = nmhTestv2Fsbgp4mpePeerPrefixUpperLimit (&u4ErrorCode,
                                                       i4AddressFamily,
                                                       &PeerAddr,
                                                       i4MaxPrefixLimit);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "\r Invalid Prefix Limit Value!\r\n");
        return;
    }

    i1RetVal = nmhTestv2Fsbgp4mpePeerTcpConnectRetryCnt (&u4ErrorCode,
                                                         i4AddressFamily,
                                                         &PeerAddr,
                                                         i4ConnectRetryCount);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "\r Invalid ConnectRetry count\r\n");
        return;
    }

    i1RetVal = nmhTestv2Fsbgp4mpePeerAllowAutomaticStart (&u4ErrorCode,
                                                          i4AddressFamily,
                                                          &PeerAddr,
                                                          i4AutoStartStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid AutoStart Status\r\n");
        return;
    }

    i1RetVal = nmhTestv2Fsbgp4mpePeerAllowAutomaticStop (&u4ErrorCode,
                                                         i4AddressFamily,
                                                         &PeerAddr,
                                                         i4AutoStopStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid AutoStop Status\r\n");
        return;
    }

    i1RetVal = nmhTestv2Fsbgp4mpeDampPeerOscillations (&u4ErrorCode,
                                                       i4AddressFamily,
                                                       &PeerAddr,
                                                       i4DampPeerStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid DampPeer Status\r\n");
        return;
    }

    i1RetVal = nmhTestv2Fsbgp4mpePeerDelayOpen (&u4ErrorCode,
                                                i4AddressFamily, &PeerAddr,
                                                i4DelayOpenStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid DelayOpen Status\r\n");
        return;
    }

    /* Configuring the Gateway address */
    if (GatewayAddr.i4_Length != 0)
    {
        i1RetVal = nmhTestv2Fsbgp4mpePeerExtGateway (&u4ErrorCode,
                                                     i4AddressFamily,
                                                     &PeerAddr, &GatewayAddr);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "\rInvalid  Gateway address\r\n");
            return;
        }
    }

    /* Configuring the Local address */
    if (LocalAddr.i4_Length != 0)
    {
        i1RetVal = nmhTestv2Fsbgp4mpePeerExtLclAddress (&u4ErrorCode,
                                                        i4AddressFamily,
                                                        &PeerAddr, &LocalAddr);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "\rInvalid update-source address\r\n");
            return;
        }
    }

    i1RetVal = nmhTestv2Fsbgp4mpePeerExtEBGPHopLimit (&u4ErrorCode,
                                                      i4AddressFamily,
                                                      &PeerAddr,
                                                      i4EBGPHopLimit);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid EBGP Hop Limit\r\n");
        return;
    }

    i1RetVal = nmhTestv2Fsbgp4mpePeerExtTcpSendBufSize (&u4ErrorCode,
                                                        i4AddressFamily,
                                                        &PeerAddr,
                                                        i4TcpSendSize);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid TCP Send Buff Size\r\n");
        return;
    }

    i1RetVal = nmhTestv2Fsbgp4mpePeerExtTcpRcvBufSize (&u4ErrorCode,
                                                       i4AddressFamily,
                                                       &PeerAddr, i4TcpRcvSize);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid TCP Recv. Buff Size\r\n");
        return;
    }

    if (Password.i4_Length > 0)
    {
        i1Status = nmhTestv2Fsbgp4TCPMD5AuthPwdSet (&u4ErrorCode,
                                                    IpAddress.u2Afi, &PeerAddr,
                                                    BGP4_TCPMD5_PWD_SET);
        if (i1Status == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Invalid password status");
            return;
        }

        i1Status = nmhTestv2Fsbgp4TCPMD5AuthPassword (&u4ErrorCode,
                                                      IpAddress.u2Afi,
                                                      &PeerAddr, &Password);

        if (i1Status == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Invalid password");
            return;
        }
    }
    else if (i4TcpPwdStatus == BGP4_TCPMD5_PWD_SET)
    {
        i1Status =
            nmhTestv2Fsbgp4TCPMD5AuthPwdSet (&u4ErrorCode, IpAddress.u2Afi,
                                             &PeerAddr, BGP4_TCPMD5_PWD_RESET);
        if (i1Status == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Invalid password status");
            return;
        }
    }

    if (i4MktId != -1)
    {
        i1RetVal =
            nmhTestv2Fsbgp4TCPAOAuthKeyStatus (&u4ErrorCode, i4AddressFamily,
                                               &PeerAddr, i4MktId, 1);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unable to set TCP-AO authentication for peer");
            return;
        }
    }
    else
    {
        i1RetVal = Bgp4LowGetIpAddress (i4AddressFamily,
                                        &PeerAddr, &AoPeerAddr);
        i1RetVal =
            Bgp4GetFirstTCPAOKeyId ((UINT4) i4CxtId, AoPeerAddr, &i4MktVal);
        if (i1RetVal == BGP4_SUCCESS)
        {
            i1RetVal =
                nmhTestv2Fsbgp4TCPAOAuthKeyStatus (&u4ErrorCode,
                                                   i4AddressFamily, &PeerAddr,
                                                   i4MktVal, 2);
            if (i1RetVal == SNMP_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to reset TCP-AO authentication for peer");
                return;
            }
        }
    }

    i1RetVal = nmhTestv2Fsbgp4mpePeerBfdStatus (&u4ErrorCode,
                                                i4AddressFamily,
                                                &PeerAddr, i4BfdStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid BFD status\r\n");
        return;
    }

    i1RetVal = nmhSetFsbgp4mpebgpPeerAdminStatus (i4AddressFamily,
                                                  &PeerAddr, i4AdminStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *)
                      " \rUnable to set the admin status of peer\r\n");
        return;
    }

    /* RFC 4271 Updates */

    i1RetVal = nmhSetFsbgp4mpePeerPrefixUpperLimit (i4AddressFamily,
                                                    &PeerAddr,
                                                    i4MaxPrefixLimit);
    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Unable to set Prefix Limit\r\n");
    }

    /* Testing and Setting the ConnectRetry Count */
    i1RetVal =
        nmhSetFsbgp4mpePeerTcpConnectRetryCnt (i4AddressFamily, &PeerAddr,
                                               i4ConnectRetryCount);
    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set ConnectRetry Count\r\n");
    }

    /* Configuring Automatic Start */
    i1RetVal = nmhSetFsbgp4mpePeerAllowAutomaticStart (i4AddressFamily,
                                                       &PeerAddr,
                                                       i4AutoStartStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set AutoStart Status\r\n");
    }

    /* Configuring Automatic Stop */
    i1RetVal = nmhSetFsbgp4mpePeerAllowAutomaticStop (i4AddressFamily,
                                                      &PeerAddr,
                                                      i4AutoStopStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set AutoStop Status\r\n");
    }

    /* Configuring Damp Peer Oscillations */
    i1RetVal = nmhSetFsbgp4mpeDampPeerOscillations (i4AddressFamily,
                                                    &PeerAddr,
                                                    i4DampPeerStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set DampPeer Status\r\n");
    }

    if ((i4AutoStartStatus == BGP4_PEER_AUTOMATICSTART_DISABLE) &&
        (i4DampPeerStatus == BGP4_DAMP_PEER_OSCILLATIONS_DISABLE))
    {
        i1RetVal =
            nmhSetFsbgp4mpebgpPeerIdleHoldTimeConfigured (i4AddressFamily,
                                                          &PeerAddr,
                                                          BGP4_DEF_IDLEHOLDINTERVAL);
        if (i1RetVal == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set DampPeer Status\r\n");
        }
    }

    /* Configuring Delay OPEN */
    i1RetVal = nmhSetFsbgp4mpePeerDelayOpen (i4AddressFamily,
                                             &PeerAddr, i4DelayOpenStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set DelayOpen Status\r\n");
    }

    /*Configuring EBGP MultiHop */

    i1RetVal = nmhSetFsbgp4mpePeerExtEBGPMultiHop (i4AddressFamily,
                                                   &PeerAddr,
                                                   i4MultiHopEBGPStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set ExtEBGPMultiHop\r\n");
    }

    /*Configuring Next Hop */
    i1RetVal = nmhSetFsbgp4mpePeerExtNextHopSelf (i4AddressFamily,
                                                  &PeerAddr, i4NextHop);
    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) " \rNext Hop was was not set\r\n");
    }

    /*Configuring Default origination status */
    i1RetVal = nmhSetFsbgp4mpePeerExtDefaultOriginate (i4AddressFamily,
                                                       &PeerAddr,
                                                       i4DefRouteStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *)
                      " \rDefault route status was not set\r\n");
    }

    /*Configuring Community send status */
    i1RetVal = nmhSetFsbgp4mpePeerExtCommSendStatus (i4AddressFamily,
                                                     &PeerAddr, i4CommAction);

    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "\rUnable to set the Community send status\r\n");
    }

    /*Configuring Extended community send status */
    i1RetVal = nmhSetFsbgp4mpePeerExtECommSendStatus (i4AddressFamily,
                                                      &PeerAddr,
                                                      i4ExtCommAction);

    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *)
                      " \rUnable to set the extended community send status\r\n");
    }

    /*Configuring Route Reflection status */

    if (i4RflClient == ISS_BGP4_RFL_CLIENT)
    {

        i1RetVal = nmhSetFsbgp4mpePeerExtRflClient (i4AddressFamily,
                                                    &PeerAddr, i4RflClient);
        if (i1RetVal == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *)
                          " \rExternal peer: RFL client status cannot be enabled\r\n");
        }
    }
    /* Configuring the Gateway address */
    if (GatewayAddr.i4_Length != 0)
    {
        i1RetVal = nmhSetFsbgp4mpePeerExtGateway (i4AddressFamily,
                                                  &PeerAddr, &GatewayAddr);

        if (i1RetVal == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *)
                          "Failed in configuring the gateway address\r\n");
        }
    }

    if (LocalAddr.i4_Length != 0)
    {
        /*Testing and configuring local address */
        i1RetVal = nmhSetFsbgp4mpePeerExtLclAddress (i4AddressFamily,
                                                     &PeerAddr, &LocalAddr);
        if (i1RetVal == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *)
                          "Failed in configuring update-source address\r\n");
        }
    }
    /*Since the proper validations are done in the web page itself test routine
     * is not needed before Configuring the PeerExtPassive and Hop Limit
     * objects */

    i1RetVal = nmhSetFsbgp4mpePeerExtPassive (i4AddressFamily,
                                              &PeerAddr, i4PeerPassive);
    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "Failed in configuring PeerExtPassive object\r\n");
    }

    i1RetVal = nmhSetFsbgp4mpePeerExtEBGPHopLimit (i4AddressFamily,
                                                   &PeerAddr, i4EBGPHopLimit);
    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "Failed in configuring Hop Limit\r\n");
    }

    i1RetVal = nmhSetFsbgp4mpePeerExtTcpSendBufSize (i4AddressFamily,
                                                     &PeerAddr, i4TcpSendSize);
    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "Failed in configuring Tcp buffer send size\r\n");
    }

    i1RetVal = nmhSetFsbgp4mpePeerExtTcpRcvBufSize (i4AddressFamily,
                                                    &PeerAddr, i4TcpRcvSize);
    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "Failed in configuring Tcp buffer Receive size\r\n");
    }

    i1RetVal = nmhSetFsbgp4mpePeerBfdStatus (i4AddressFamily,
                                             &PeerAddr, i4BfdStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "Failed in configuring BFD status\r\n");
    }

    if (Password.i4_Length > 0)
    {

        i1Status = nmhSetFsbgp4TCPMD5AuthPwdSet (IpAddress.u2Afi,
                                                 &PeerAddr,
                                                 BGP4_TCPMD5_PWD_SET);
        if (i1Status == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set password status");
            return;
        }
        i1Status = nmhSetFsbgp4TCPMD5AuthPassword (IpAddress.u2Afi,
                                                   &PeerAddr, &Password);
        if (i1Status == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set password");
            return;
        }
    }

    else if (i4TcpPwdStatus == BGP4_TCPMD5_PWD_SET)
    {
        i1Status = nmhSetFsbgp4TCPMD5AuthPwdSet (IpAddress.u2Afi,
                                                 &PeerAddr,
                                                 BGP4_TCPMD5_PWD_RESET);
        if (i1Status == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Invalid password");
            return;
        }
    }
    if (i4MktId != -1)
    {
        /*Set */
        i1Status =
            nmhSetFsbgp4TCPAOAuthKeyStatus (i4AddressFamily, &PeerAddr, i4MktId,
                                            1);
        if (i1Status == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unable to perform TCP-AO operation on peer");
            return;
        }
    }
    else
    {
        /*Clear */
        i1RetVal = Bgp4LowGetIpAddress (i4AddressFamily,
                                        &PeerAddr, &AoPeerAddr);
        i1RetVal =
            Bgp4GetFirstTCPAOKeyId ((UINT4) i4CxtId, AoPeerAddr, &i4MktVal);
        if (i1RetVal == BGP4_SUCCESS)
        {
            i1Status =
                nmhSetFsbgp4TCPAOAuthKeyStatus (i4AddressFamily, &PeerAddr,
                                                i4MktVal, 2);
            if (i1Status == SNMP_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to perform TCP-AO operation on peer");
                return;
            }
        }

    }
    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessBgpPeerConfPageGet (pHttp);
    return;
}

/*********************************************************************
*  Function Name : IssProcessBgpTcpAoconfPage
*  Description   : This function processes the request coming for the
*                  BGP TCP-AO configuration page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessBgpTcpAoConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessBgpTcpAoConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessBgpTcpAoConfPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessBgpTcpAoConfPageGet
*  Description   : This function processes the GET request coming for the
*                  BGP TCP-AO configuration page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessBgpTcpAoConfPageGet (tHttp * pHttp)
{
    INT4                i4BgpContextId;
    INT4                i4BgpPrevContextId;
    INT4                i4Temp;
    INT4                i4MktId;
    INT4                i4PrevMktId;
    INT4                i4RetVal;

    UINT1               au1BgpCxtName[VCM_ALIAS_MAX_LEN];

    MEMSET (au1BgpCxtName, 0, VCM_ALIAS_MAX_LEN);
    pHttp->i4Write = 0;
    IssPrintAvailableBgpContextId (pHttp);
    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    if (nmhGetFirstIndexFsMIBgpContextTable (&i4BgpContextId) == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;

    }

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    i4Temp = pHttp->i4Write;

    do
    {
        pHttp->i4Write = i4Temp;

        if (BgpSetContext (i4BgpContextId) == SNMP_FAILURE)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
            return;
        }

        if (nmhGetFirstIndexFsbgp4TCPMKTAuthTable (&i4MktId) == SNMP_FAILURE)
        {
            i4BgpPrevContextId = i4BgpContextId;

            if (nmhGetNextIndexFsMIBgpContextTable (i4BgpPrevContextId,
                                                    &i4BgpContextId) !=
                SNMP_SUCCESS)
            {
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
                WebnmSockWrite (pHttp,
                                (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                                (pHttp->i4HtmlSize - pHttp->i4Write));
                return;
            }
            continue;
        }
        do
        {
            /*Display only active MKT IDs */
            nmhGetFsbgp4TCPMKTAuthRowStatus (i4MktId, &i4RetVal);
            if (i4RetVal != ACTIVE)
            {
                i4PrevMktId = i4MktId;
                continue;
            }
            pHttp->i4Write = i4Temp;

            STRCPY (pHttp->au1KeyString, "fsbgp4TcpAoKeyId_key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4MktId);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4TCPMKTAuthRecvKeyId (i4MktId, &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsbgp4TcpAoReceiveKeyId_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4TCPMKTAuthAlgo (i4MktId, &i4RetVal);
            STRCPY (pHttp->au1KeyString,
                    "fsbgp4mpeTcpAoAuthenticationAlgo_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4TCPMKTAuthTcpOptExc (i4MktId, &i4RetVal);
            if (i4RetVal == 1)
            {
                i4RetVal = 9999;
            }
            else
            {
                i4RetVal = 9998;
            }
            STRCPY (pHttp->au1KeyString, "fsbgp4TcpAoOptExc_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            VcmGetAliasName ((UINT4) i4BgpContextId, au1BgpCxtName);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1BgpCxtName);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            i4PrevMktId = i4MktId;

        }
        while (nmhGetNextIndexFsbgp4TCPMKTAuthTable (i4PrevMktId, &i4MktId) ==
               SNMP_SUCCESS);

        i4BgpPrevContextId = i4BgpContextId;

    }
    while (nmhGetNextIndexFsMIBgpContextTable
           (i4BgpPrevContextId, &i4BgpContextId) == SNMP_SUCCESS);

    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    return;

}

/*********************************************************************
*  Function Name : IssProcessBgpTcpAoConfPageSet
*  Description   : This function processes the SET request coming for the
*                  BGP TCP-AO configuration page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessBgpTcpAoConfPageSet (tHttp * pHttp)
{
    INT4                i4MktId = -1;
    INT4                i4ReceiveId = -1;
    INT4                i4OptExc = 0;
    INT4                i4CxtId;
    INT4                i4RetVal;
    INT4                i4GetVal;
    UINT1               au1Password[ISS_BGP_PASSWORD_MAX_LENGTH + 4];
    /*INT4                    i4MktStatus = -1; */
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE Password;

    MEMSET (&Password, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    Password.pu1_OctetList = au1Password;
    Password.i4_Length = 0;

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    HttpGetValuebyName (ISS_ACTION, pHttp->au1Array, pHttp->au1PostQuery);

    if ((0 == STRCMP (pHttp->au1Value, "Delete"))
        || (0 == STRCMP (pHttp->au1Value, "Apply")))
    {
        STRCPY (pHttp->au1Name, "CONTEXT_ID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        VcmIsVrfExist (pHttp->au1Value, (UINT4 *) &i4CxtId);
    }
    else
    {
        STRCPY (pHttp->au1Name, "CONTEXT_ID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4CxtId = ATOI (pHttp->au1Value);
    }

    if (BgpSetContext (i4CxtId) == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
        return;
    }

    STRCPY (pHttp->au1Name, "fsbgp4TcpAoKeyId");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
        i4MktId = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4TcpAoReceiveKeyId");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
        i4ReceiveId = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4mpeTcpAoAuthenticationAlgo");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
        /*Currently HMAC-SHA-1 is only being set */

        STRCPY (pHttp->au1Name, "fsbgp4TcpAoAuthKey");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        Password.i4_Length = (INT4) STRLEN (pHttp->au1Value);
        if ((Password.i4_Length != 0)
            && (Password.i4_Length < ISS_BGP_PASSWORD_MAX_LENGTH + 4))
        {
            MEMCPY (Password.pu1_OctetList, pHttp->au1Value,
                    Password.i4_Length);
        }
    }

    STRCPY (pHttp->au1Name, "fsbgp4TcpAoOptExc");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4OptExc = ATOI (pHttp->au1Value);
        if (i4OptExc == 9999)
        {
            i4OptExc = 1;
        }
        else
        {
            i4OptExc = 0;
        }
    }

    /*
       STRCPY (pHttp->au1Name, "fsbgp4TcpAoMktStatus");

       if (HttpGetValuebyName (pHttp->au1Name,
       pHttp->au1Value,
       pHttp->au1PostQuery) == ENM_SUCCESS)
       i4MktStatus = ATOI (pHttp->au1Value);
     */
    STRCPY (pHttp->au1Name, "ACTION");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)

    {
        if (0 == STRCMP (pHttp->au1Value, "Delete"))
        {

            i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthRowStatus (&u4ErrorCode,
                                                           i4MktId, DESTROY);
            if (i4RetVal != SNMP_SUCCESS)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to clear MKT entry\r\n");
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssProcessBgpTcpAoConfPageGet (pHttp);
                return;
            }

            i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4MktId, DESTROY);
            if (i4RetVal != SNMP_SUCCESS)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to clear MKT. Check neighbor association\r\n");
            }

            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessBgpTcpAoConfPageGet (pHttp);
            return;

        }

        if (0 == STRCMP (pHttp->au1Value, "Apply"))
        {
            /*Apply */
            i4RetVal =
                nmhTestv2Fsbgp4TCPMKTAuthRowStatus (&u4ErrorCode, i4MktId,
                                                    NOT_IN_SERVICE);
            if (i4RetVal != SNMP_SUCCESS)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to modify MKT. Check neighbor assocation\r\n");
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssProcessBgpTcpAoConfPageGet (pHttp);
                return;

            }
            i4RetVal =
                nmhSetFsbgp4TCPMKTAuthRowStatus (i4MktId, NOT_IN_SERVICE);
            if (i4RetVal != SNMP_SUCCESS)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to modify MKT. Check neighbor association\r\n");
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssProcessBgpTcpAoConfPageGet (pHttp);
                return;
            }
            i4RetVal =
                nmhTestv2Fsbgp4TCPMKTAuthRecvKeyId (&u4ErrorCode, i4MktId,
                                                    i4ReceiveId);
            if (i4RetVal != SNMP_SUCCESS)
            {
                nmhSetFsbgp4TCPMKTAuthRowStatus (i4MktId, ACTIVE);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to modify Receive key-id\r\n");
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssProcessBgpTcpAoConfPageGet (pHttp);
                return;
            }

            if (Password.i4_Length != 0)
            {
                i4RetVal =
                    nmhTestv2Fsbgp4TCPMKTAuthMasterKey (&u4ErrorCode, i4MktId,
                                                        &Password);
                if (i4RetVal != SNMP_SUCCESS)
                {
                    nmhSetFsbgp4TCPMKTAuthRowStatus (i4MktId, ACTIVE);
                    IssSendError (pHttp, (CONST INT1 *)
                                  "\rUnable to modify password\r\n");
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssProcessBgpTcpAoConfPageGet (pHttp);
                    return;
                }
            }
            i4RetVal = nmhSetFsbgp4TCPMKTAuthRecvKeyId (i4MktId, i4ReceiveId);
            if (i4RetVal != SNMP_SUCCESS)
            {
                nmhSetFsbgp4TCPMKTAuthRowStatus (i4MktId, DESTROY);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to set receive key-id.\r\n");
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssProcessBgpTcpAoConfPageGet (pHttp);
                return;
            }

            if (Password.i4_Length != 0)
            {
                i4RetVal = nmhSetFsbgp4TCPMKTAuthMasterKey (i4MktId, &Password);
                if (i4RetVal != SNMP_SUCCESS)
                {
                    nmhSetFsbgp4TCPMKTAuthRowStatus (i4MktId, DESTROY);
                    IssSendError (pHttp, (CONST INT1 *)
                                  "\rUnable to set tcp-ao key.\r\n");
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssProcessBgpTcpAoConfPageGet (pHttp);
                    return;
                }
            }

            i4RetVal = nmhSetFsbgp4TCPMKTAuthTcpOptExc (i4MktId, i4OptExc);
            if (i4RetVal != SNMP_SUCCESS)
            {
                nmhSetFsbgp4TCPMKTAuthRowStatus (i4MktId, DESTROY);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to set options exclude.\r\n");
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssProcessBgpTcpAoConfPageGet (pHttp);
                return;
            }

            i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4MktId, ACTIVE);
            if (i4RetVal != SNMP_SUCCESS)
            {
                nmhSetFsbgp4TCPMKTAuthRowStatus (i4MktId, DESTROY);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnexpected Error!!!.\r\n");
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssProcessBgpTcpAoConfPageGet (pHttp);
                return;
            }
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessBgpTcpAoConfPageGet (pHttp);
            return;

        }
    }

    i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4MktId, &i4GetVal);

    if ((i4RetVal == SNMP_SUCCESS) && (i4GetVal == ACTIVE))
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "\rCannot recreate an Active MKT. Pls modify using the second form in the page\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssProcessBgpTcpAoConfPageGet (pHttp);
        return;

    }

    i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthRowStatus (&u4ErrorCode,
                                                   i4MktId, CREATE_AND_WAIT);

    if (i4RetVal != SNMP_SUCCESS)
    {
        i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthRowStatus (&u4ErrorCode,
                                                       i4MktId, DESTROY);
        if (i4RetVal != SNMP_SUCCESS)
        {
            IssSendError (pHttp, (CONST INT1 *)
                          "\rUnable to clear MKT. Check neighbor association\r\n");
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessBgpTcpAoConfPageGet (pHttp);
            return;
        }

    }

    i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4MktId, CREATE_AND_WAIT);
    if (i4RetVal != SNMP_SUCCESS)
    {
        nmhSetFsbgp4TCPMKTAuthRowStatus (i4MktId, DESTROY);
        IssSendError (pHttp, (CONST INT1 *) "\rUnable to set MKT.\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssProcessBgpTcpAoConfPageGet (pHttp);
        return;
    }

    i4RetVal =
        nmhTestv2Fsbgp4TCPMKTAuthRecvKeyId (&u4ErrorCode, i4MktId, i4ReceiveId);
    if (i4RetVal != SNMP_SUCCESS)
    {
        nmhSetFsbgp4TCPMKTAuthRowStatus (i4MktId, DESTROY);
        IssSendError (pHttp, (CONST INT1 *)
                      "\rUnable to set Receive key-id\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssProcessBgpTcpAoConfPageGet (pHttp);
        return;
    }

    i4RetVal = nmhSetFsbgp4TCPMKTAuthRecvKeyId (i4MktId, i4ReceiveId);
    if (i4RetVal != SNMP_SUCCESS)
    {
        nmhSetFsbgp4TCPMKTAuthRowStatus (i4MktId, DESTROY);
        IssSendError (pHttp, (CONST INT1 *)
                      "\rUnable to set receive key-id.\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssProcessBgpTcpAoConfPageGet (pHttp);
        return;
    }

    i4RetVal =
        nmhTestv2Fsbgp4TCPMKTAuthMasterKey (&u4ErrorCode, i4MktId, &Password);
    if (i4RetVal != SNMP_SUCCESS)
    {
        nmhSetFsbgp4TCPMKTAuthRowStatus (i4MktId, DESTROY);
        IssSendError (pHttp, (CONST INT1 *) "\rUnable to set password\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssProcessBgpTcpAoConfPageGet (pHttp);
        return;
    }

    i4RetVal = nmhSetFsbgp4TCPMKTAuthMasterKey (i4MktId, &Password);
    if (i4RetVal != SNMP_SUCCESS)
    {
        nmhSetFsbgp4TCPMKTAuthRowStatus (i4MktId, DESTROY);
        IssSendError (pHttp, (CONST INT1 *) "\rUnable to set tcp-ao key.\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssProcessBgpTcpAoConfPageGet (pHttp);
        return;
    }

    i4RetVal = nmhSetFsbgp4TCPMKTAuthTcpOptExc (i4MktId, i4OptExc);
    if (i4RetVal != SNMP_SUCCESS)
    {
        nmhSetFsbgp4TCPMKTAuthRowStatus (i4MktId, DESTROY);
        IssSendError (pHttp, (CONST INT1 *)
                      "\rUnable to set options exclude.\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssProcessBgpTcpAoConfPageGet (pHttp);
        return;
    }

    i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4MktId, ACTIVE);
    if (i4RetVal != SNMP_SUCCESS)
    {
        nmhSetFsbgp4TCPMKTAuthRowStatus (i4MktId, DESTROY);
        IssSendError (pHttp, (CONST INT1 *) "\rUnexpected Error!!!.\r\n");
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssProcessBgpTcpAoConfPageGet (pHttp);
        return;
    }

    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessBgpTcpAoConfPageGet (pHttp);
    return;

}

/*********************************************************************
*  Function Name : IssProcessBgpRouteMapPage
*  Description   : This function processes the request coming for the
*                  BGP Route Map configuration page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessBgpRouteMapPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessBgpRouteMapPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessBgpRouteMapPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessBgpRouteMapPageGet
*  Description   : This function processes the GET request coming for the
*                  BGP Route Map configuration page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessBgpRouteMapPageGet (tHttp * pHttp)
{
    UINT4               u4IpAddress = 0;
    INT4                i4AddrType = 0;
    INT4                i4Direction = 0;
    INT4                i4Temp = 0;
    INT4                i4BgpContextId = 0;
    INT4                i4BgpPrevContextId = 0;
    UINT1              *pu1TempString = NULL;
    UINT1               au1Ip6Addr[ISS_BGP4_IP6_ADDR_STRING_LEN];
    UINT1               au1LocalIpAddress[ISS_BGP4_IP_MAX_LENGTH];
    UINT1               au1RouteMapName[ISS_BGP4_ROUTE_NAME_LEN];
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddr;
    UINT1               au1BgpCxtName[VCM_ALIAS_MAX_LEN];

    MEMSET (au1LocalIpAddress, 0, ISS_BGP4_IP_MAX_LENGTH);
    MEMSET (au1RouteMapName, 0, sizeof (au1RouteMapName));
    MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
    MEMSET (au1BgpCxtName, 0, VCM_ALIAS_MAX_LEN);

    PeerRemoteAddr.pu1_OctetList = au1LocalIpAddress;
    RouteMapName.pu1_OctetList = au1RouteMapName;
    RouteMapName.i4_Length = (INT4) STRLEN (au1RouteMapName);

    pHttp->i4Write = 0;
    IssPrintAvailableBgpContextId (pHttp);

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    if (nmhGetFirstIndexFsMIBgpContextTable (&i4BgpContextId) == SNMP_FAILURE)
    {
        pHttp->i4Write = i4Temp;
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    i4Temp = pHttp->i4Write;

    do
    {
        if (BgpSetContext (i4BgpContextId) == SNMP_FAILURE)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
            return;
        }

        if (nmhGetFirstIndexFsBgp4NeighborRouteMapTable (&i4AddrType,
                                                         &PeerRemoteAddr,
                                                         &i4Direction) ==
            SNMP_FAILURE)
        {
            i4BgpPrevContextId = i4BgpContextId;
            if (nmhGetNextIndexFsMIBgpContextTable (i4BgpPrevContextId,
                                                    &i4BgpContextId) !=
                SNMP_SUCCESS)
            {
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
                WebnmSockWrite (pHttp,
                                (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                                (pHttp->i4HtmlSize - pHttp->i4Write));
                return;

            }
            continue;
        }

        /*Process the BGP peer table */
        do
        {
            pHttp->i4Write = i4Temp;

            STRCPY (pHttp->au1KeyString, "fsBgp4NeighborRouteMapPeer_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (i4AddrType == BGP4_INET_AFI_IPV4)
            {
                MEMCPY (&u4IpAddress,
                        PeerRemoteAddr.pu1_OctetList, PeerRemoteAddr.i4_Length);
                WEB_CONVERT_IPADDR_TO_STR (pu1TempString,
                                           OSIX_NTOHL (u4IpAddress));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1TempString);
            }
#ifdef BGP4_IPV6_WANTED
            else                /* BGP ipv6 processing */
            {
                Bgp4PrintIp6Addr (PeerRemoteAddr.pu1_OctetList, (au1Ip6Addr));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Ip6Addr);
                MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
            }
#endif /* BGP4_IPV6_WANTED */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "fsBgp4NeighborRouteMapDirection_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Direction);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsBgp4NeighborRouteMapName (i4AddrType,
                                              &PeerRemoteAddr, i4Direction,
                                              &RouteMapName);
            STRCPY (pHttp->au1KeyString, "fsBgp4NeighborRouteMapName_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SNPRINTF ((CHR1 *) pHttp->au1DataString,
                      (UINT4) (RouteMapName.i4_Length + 1), "%s",
                      RouteMapName.pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            VcmGetAliasName ((UINT4) i4BgpContextId, au1BgpCxtName);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1BgpCxtName);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        }
        while (nmhGetNextIndexFsBgp4NeighborRouteMapTable (i4AddrType, &i4AddrType, &PeerRemoteAddr, &PeerRemoteAddr, i4Direction, &i4Direction) == SNMP_SUCCESS);    /*end of do-while */
        i4BgpPrevContextId = i4BgpContextId;
    }
    while (nmhGetNextIndexFsMIBgpContextTable
           (i4BgpPrevContextId, &i4BgpContextId) == SNMP_SUCCESS);

    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
*  Function Name : IssProcessBgpRouteMapPageSet
*  Description   : This function processes the SET request coming for the
*                  BGP Route Map Configuration page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessBgpRouteMapPageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    tUtlInAddr          InAddr;
    tAddrPrefix         IpAddress;
    UINT1               au1RouteMapName[ISS_BGP4_ROUTE_NAME_LEN];
    UINT1               au1PeerIpAddress[ISS_BGP4_IP_MAX_LENGTH];
    UINT4               u4ErrorCode = 0;
    INT4                i4CxtId = 0;
    INT4                i4Direction = 0;
    INT4                i4Len = 0;
    INT4                i4AddressFamily = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (au1RouteMapName, '\0', ISS_BGP4_ROUTE_NAME_LEN);
    MEMSET (au1PeerIpAddress, 0, ISS_BGP4_IP_MAX_LENGTH);
    MEMSET (&IpAddress, 0, sizeof (tAddrPrefix));
    MEMSET (&PeerAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    RouteMapName.pu1_OctetList = au1RouteMapName;
    RouteMapName.i4_Length = 0;

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if ((0 == STRCMP (pHttp->au1Value, "Delete"))
        || (0 == STRCMP (pHttp->au1Value, "Apply")))
    {
        STRCPY (pHttp->au1Name, "CONTEXT_ID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        VcmIsVrfExist (pHttp->au1Value, (UINT4 *) &i4CxtId);
    }
    else
    {
        STRCPY (pHttp->au1Name, "CONTEXT_ID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4CxtId = ATOI (pHttp->au1Value);
    }

    if (BgpSetContext (i4CxtId) == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
        return;
    }

    STRCPY (pHttp->au1Name, "fsBgp4NeighborRouteMapPeer");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Len = CliDotStrLength (pHttp->au1Value);
        if (i4Len == sizeof (UINT4))
        {
            IpAddress.u2Afi = BGP4_INET_AFI_IPV4;
            i4AddressFamily = IpAddress.u2Afi;
            if (0 == BGP4_INET_ATON (pHttp->au1Value, &InAddr))
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rInvalid peer address\r\n");
                return;
            }
            MEMCPY ((IpAddress.au1Address), &(InAddr.u4Addr), sizeof (UINT4));
            IpAddress.u2AddressLen = sizeof (UINT4);
        }
#ifdef BGP4_IPV6_WANTED
        else if (str_to_ip6addr (pHttp->au1Value) != NULL)
        {
            IpAddress.u2Afi = BGP4_INET_AFI_IPV6;
            i4AddressFamily = IpAddress.u2Afi;
            MEMCPY (IpAddress.au1Address,
                    str_to_ip6addr (pHttp->au1Value), BGP4_IPV6_PREFIX_LEN);
            IpAddress.u2AddressLen = BGP4_IPV6_PREFIX_LEN;
        }
#endif /* BGP4_IPV6_WANTED */

        PeerAddr.pu1_OctetList = au1PeerIpAddress;
        MEMCPY (PeerAddr.pu1_OctetList, IpAddress.au1Address,
                IpAddress.u2AddressLen);
        PeerAddr.i4_Length = IpAddress.u2AddressLen;
    }

    STRCPY (pHttp->au1Name, "fsBgp4NeighborRouteMapDirection");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
        i4Direction = ATOI (pHttp->au1Value);

    i1RetVal =
        nmhValidateIndexInstanceFsBgp4NeighborRouteMapTable (i4AddressFamily,
                                                             &PeerAddr,
                                                             i4Direction);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid Peer Address\r\n");
        return;
    }

    STRCPY (pHttp->au1Name, "ACTION");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)

    {
        if (0 == STRCMP (pHttp->au1Value, "Delete"))
        {
            i1RetVal = nmhSetFsBgp4NeighborRouteMapRowStatus (i4AddressFamily,
                                                              &PeerAddr,
                                                              i4Direction,
                                                              DESTROY);
            if (i1RetVal == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to delete entry\r\n");
            }
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessBgpRouteMapPageGet (pHttp);
            return;

        }
    }
    if (0 == STRCMP (pHttp->au1Value, "ADD"))
    {
        STRCPY (pHttp->au1Name, "fsBgp4NeighborRouteMapName");
        if (HttpGetValuebyName (pHttp->au1Name,
                                pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            RouteMapName.i4_Length = (INT4) STRLEN (pHttp->au1Value);
            if (RouteMapName.i4_Length >= (ISS_BGP4_ROUTE_NAME_LEN))
            {
                RouteMapName.i4_Length = ISS_BGP4_ROUTE_NAME_LEN - 1;
            }
            MEMCPY (RouteMapName.pu1_OctetList, pHttp->au1Value,
                    RouteMapName.i4_Length);
            RouteMapName.pu1_OctetList[RouteMapName.i4_Length] = 0;
        }

        if (nmhTestv2FsBgp4NeighborRouteMapRowStatus
            (&u4ErrorCode, i4AddressFamily, &PeerAddr, i4Direction,
             CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Invalid Row status");
            return;

        }

        if (nmhSetFsBgp4NeighborRouteMapRowStatus
            (i4AddressFamily, &PeerAddr, i4Direction,
             CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set Row status");
            return;

        }

        if (nmhTestv2FsBgp4NeighborRouteMapName
            (&u4ErrorCode, i4AddressFamily, &PeerAddr, i4Direction,
             &RouteMapName) == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Invalid Route Map name");
            return;

        }

        if (nmhSetFsBgp4NeighborRouteMapName
            (i4AddressFamily, &PeerAddr, i4Direction,
             &RouteMapName) == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Invalid Row status");
            return;

        }
        if (nmhSetFsBgp4NeighborRouteMapRowStatus
            (i4AddressFamily, &PeerAddr, i4Direction, ACTIVE) == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set Row status");
            return;

        }
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssProcessBgpRouteMapPageGet (pHttp);
        return;
    }

    if (0 == STRCMP (pHttp->au1Value, "Apply"))
    {
        STRCPY (pHttp->au1Name, "fsBgp4NeighborRouteMapName");
        if (HttpGetValuebyName (pHttp->au1Name,
                                pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            RouteMapName.i4_Length = (INT4) STRLEN (pHttp->au1Value);
            if (RouteMapName.i4_Length >= ISS_BGP4_ROUTE_NAME_LEN)
            {
                RouteMapName.i4_Length = ISS_BGP4_ROUTE_NAME_LEN - 1;
            }
            MEMCPY (RouteMapName.pu1_OctetList, pHttp->au1Value,
                    RouteMapName.i4_Length);
            RouteMapName.pu1_OctetList[RouteMapName.i4_Length] = 0;
        }

        if (nmhTestv2FsBgp4NeighborRouteMapRowStatus
            (&u4ErrorCode, i4AddressFamily, &PeerAddr, i4Direction,
             NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Invalid Row status");
            return;

        }

        if (nmhSetFsBgp4NeighborRouteMapRowStatus
            (i4AddressFamily, &PeerAddr, i4Direction,
             NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set Row status");
            return;

        }

        if (nmhTestv2FsBgp4NeighborRouteMapName
            (&u4ErrorCode, i4AddressFamily, &PeerAddr, i4Direction,
             &RouteMapName) == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Invalid Route Map name");
            return;

        }

        if (nmhSetFsBgp4NeighborRouteMapName
            (i4AddressFamily, &PeerAddr, i4Direction,
             &RouteMapName) == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Invalid Row status");
            return;

        }
        if (nmhSetFsBgp4NeighborRouteMapRowStatus
            (i4AddressFamily, &PeerAddr, i4Direction, ACTIVE) == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set Row status");
            return;

        }
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssProcessBgpRouteMapPageGet (pHttp);
        return;
    }

}

/*********************************************************************
 *  Function Name : IssProcessBgpPeerStatsPage
 *  Description   : This function processes the request coming for the
                   BGP Peer statistics page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ***********************************************************************/
VOID
IssProcessBgpPeerStatsPage (tHttp * pHttp)
{
    UINT4               u4IpAddress = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    INT4                i4AddrType = 0;
    INT4                i4Temp = 0;
    INT4                i4BgpContextId = 0;
    INT4                i4BgpPrevContextId = 0;
    UINT1              *pu1TempString = NULL;
    UINT1               au1UpTime[BGP_CLI_DISPLAY_TIME_BUF];
    UINT1               au1Ip6Addr[ISS_BGP4_IP6_ADDR_STRING_LEN];
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddr;
    UINT1               au1LocalIpAddress[ISS_BGP4_IP_MAX_LENGTH];
    UINT1               au1BgpCxtName[VCM_ALIAS_MAX_LEN];

    MEMSET (au1LocalIpAddress, 0, ISS_BGP4_IP_MAX_LENGTH);
    MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
    MEMSET (au1UpTime, 0, BGP_CLI_DISPLAY_TIME_BUF);
    MEMSET (au1BgpCxtName, 0, VCM_ALIAS_MAX_LEN);

    PeerRemoteAddr.pu1_OctetList = au1LocalIpAddress;

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();
    pHttp->i4Write = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    if (nmhGetFirstIndexFsMIBgpContextTable (&i4BgpContextId) == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;

    }

    i4Temp = pHttp->i4Write;

    do
    {
        pHttp->i4Write = i4Temp;

        if (BgpSetContext (i4BgpContextId) == SNMP_FAILURE)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Error in setting Bgp Context");
            return;
        }

        if (nmhGetFirstIndexFsbgp4MpeBgpPeerTable (&i4AddrType,
                                                   &PeerRemoteAddr) ==
            SNMP_FAILURE)
        {
            i4BgpPrevContextId = i4BgpContextId;
            if (nmhGetNextIndexFsMIBgpContextTable (i4BgpPrevContextId,
                                                    &i4BgpContextId) !=
                SNMP_SUCCESS)
            {

                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                return;
            }
            continue;
        }
        /*Process the BGP  peer table */
        do
        {
            pHttp->i4Write = i4Temp;

            STRCPY (pHttp->au1KeyString, "Context_Bgp_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            VcmGetAliasName ((UINT4) i4BgpContextId, au1BgpCxtName);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1BgpCxtName);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "fsbgp4mpebgpPeerRemoteAddr_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (i4AddrType == BGP4_INET_AFI_IPV4)
            {
                MEMCPY (&u4IpAddress,
                        PeerRemoteAddr.pu1_OctetList, PeerRemoteAddr.i4_Length);
                WEB_CONVERT_IPADDR_TO_STR (pu1TempString,
                                           OSIX_NTOHL (u4IpAddress));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1TempString);
            }
#ifdef BGP4_IPV6_WANTED
            else                /* IPV6 processing */
            {
                Bgp4PrintIp6Addr (PeerRemoteAddr.pu1_OctetList, (au1Ip6Addr));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Ip6Addr);
                MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
            }
#endif /* BGP4_IPV6_WANTED */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpebgpPeerState (i4AddrType, &PeerRemoteAddr,
                                         &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsbgp4mpebgpPeerState_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpebgpPeerAdminStatus (i4AddrType,
                                               &PeerRemoteAddr, &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsbgp4mpebgpPeerAdminStatus_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpebgpPeerNegotiatedVersion (i4AddrType,
                                                     &PeerRemoteAddr,
                                                     &i4RetVal);
            STRCPY (pHttp->au1KeyString,
                    "fsbgp4mpebgpPeerNegotiatedVersion_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpebgpPeerRemoteAs (i4AddrType, &PeerRemoteAddr,
                                            (UINT4 *) &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsbgp4mpebgpPeerRemoteAs_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", (UINT4) i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpebgpPeerInUpdates (i4AddrType,
                                             &PeerRemoteAddr,
                                             (UINT4 *) &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsbgp4mpebgpPeerInUpdates_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpebgpPeerOutUpdates (i4AddrType,
                                              &PeerRemoteAddr,
                                              (UINT4 *) &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsbgp4mpebgpPeerOutUpdates_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpebgpPeerInTotalMessages (i4AddrType,
                                                   &PeerRemoteAddr,
                                                   (UINT4 *) &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsbgp4mpebgpPeerInTotalMessages_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpebgpPeerOutTotalMessages (i4AddrType,
                                                    &PeerRemoteAddr,
                                                    (UINT4 *) &i4RetVal);
            STRCPY (pHttp->au1KeyString,
                    "fsbgp4mpebgpPeerOutTotalMessages_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsbgp4mpebgpPeerFsmEstablishedTime (i4AddrType,
                                                      &PeerRemoteAddr,
                                                      (UINT4 *) &i4RetVal);
            BGP_CLI_SECS_TO_STRING ((UINT4) i4RetVal, au1UpTime);
            STRCPY (pHttp->au1KeyString,
                    "fsbgp4mpebgpPeerFsmEstablishedTime_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%15s", au1UpTime);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        }
        while (nmhGetNextIndexFsbgp4MpeBgpPeerTable (i4AddrType, &i4AddrType, &PeerRemoteAddr, &PeerRemoteAddr) != SNMP_FAILURE);    /*end of do-while */
        i4BgpPrevContextId = i4BgpContextId;
    }
    while (nmhGetNextIndexFsMIBgpContextTable
           (i4BgpPrevContextId, &i4BgpContextId) == SNMP_SUCCESS);
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
 *  Function Name : IssProcessBgpGrConfPage
 *  Description   : This function processes the request coming for the
 *                  BGP Graceful Restart configuration page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpGrConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessBgpGrConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessBgpGrConfPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessBgpGrConfPageGet
*  Description   : This function processes the get request for the BGP 
*                  Graceful Restart page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessBgpGrConfPageGet (tHttp * pHttp)
{
    INT4                i4GrAdminStatus = 0;
    INT4                i4RestartInterval = 0;
    INT4                i4DeferralInterval = 0;
    INT4                i4StaleInterval = 0;
    INT4                i4RestartSupport = 0;
    INT4                i4RestartReason = 0;

    pHttp->i4Write = 0;
    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();
    if (BgpSetContext (BGP4_DFLT_VRFID) == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    nmhGetFsbgp4GRAdminStatus (&i4GrAdminStatus);
    STRCPY (pHttp->au1KeyString, "fsbgp4GRAdminStatus_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4GrAdminStatus);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    nmhGetFsbgp4GRRestartTimeInterval (&i4RestartInterval);
    STRCPY (pHttp->au1KeyString, "fsbgp4GRRestartTimeInterval_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RestartInterval);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    nmhGetFsbgp4GRSelectionDeferralTimeInterval (&i4DeferralInterval);
    STRCPY (pHttp->au1KeyString, "fsbgp4GRSelectionDeferralTimeInterval_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4DeferralInterval);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    nmhGetFsbgp4GRStaleTimeInterval (&i4StaleInterval);
    STRCPY (pHttp->au1KeyString, "fsbgp4GRStaleTimeInterval_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4StaleInterval);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    nmhGetFsbgp4RestartSupport (&i4RestartSupport);
    STRCPY (pHttp->au1KeyString, "fsbgp4RestartSupport_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RestartSupport);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    nmhGetFsbgp4RestartReason (&i4RestartReason);
    STRCPY (pHttp->au1KeyString, "fsbgp4RestartReason_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RestartReason);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
*  Function Name : IssProcessBgpGrConfPageSet
*  Description   : This function processes the set request for the BGP 
*                  Graceful Restart page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessBgpGrConfPageSet (tHttp * pHttp)
{
    INT4                i4GrAdminStatus = 0;
    INT4                i4RestartInterval = 0;
    INT4                i4DeferralInterval = 0;
    INT4                i4StaleInterval = 0;
    INT4                i4RestartSupport = 0;
    INT4                i4RestartReason = 0;
    UINT4               u4ErrorCode = 0;

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();
    if (BgpSetContext (BGP4_DFLT_VRFID) == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    STRCPY (pHttp->au1Name, "fsbgp4GRAdminStatus");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4GrAdminStatus = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4GRRestartTimeInterval");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4RestartInterval = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4GRSelectionDeferralTimeInterval");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4DeferralInterval = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4GRStaleTimeInterval");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4StaleInterval = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4RestartSupport");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4RestartSupport = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsbgp4RestartReason");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4RestartReason = ATOI (pHttp->au1Value);

    if (i4RestartInterval == 0)
    {
        if (nmhTestv2Fsbgp4GRAdminStatus
            (&u4ErrorCode, i4GrAdminStatus) == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set GR Admin Status");
            return;
        }
        nmhSetFsbgp4GRAdminStatus (i4GrAdminStatus);

        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssProcessBgpGrConfPageGet (pHttp);
        return;
    }
    if (nmhTestv2Fsbgp4GRRestartTimeInterval
        (&u4ErrorCode, i4RestartInterval) == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set Restart Interval");
        return;
    }
    nmhSetFsbgp4GRRestartTimeInterval (i4RestartInterval);

    if (nmhTestv2Fsbgp4GRSelectionDeferralTimeInterval
        (&u4ErrorCode, i4DeferralInterval) == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set Deferral Interval");
        return;
    }
    nmhSetFsbgp4GRSelectionDeferralTimeInterval (i4DeferralInterval);

    if (nmhTestv2Fsbgp4GRStaleTimeInterval
        (&u4ErrorCode, i4StaleInterval) == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set Stale Interval");
        return;
    }
    nmhSetFsbgp4GRStaleTimeInterval (i4StaleInterval);

    if (nmhTestv2Fsbgp4RestartSupport
        (&u4ErrorCode, i4RestartSupport) == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set Restart Support");
        return;
    }

    nmhSetFsbgp4RestartSupport (i4RestartSupport);

    if (nmhTestv2Fsbgp4RestartReason
        (&u4ErrorCode, i4RestartReason) == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set Restart Reason");
        return;
    }

    nmhSetFsbgp4RestartReason (i4RestartReason);

    if (nmhTestv2Fsbgp4GRAdminStatus
        (&u4ErrorCode, i4GrAdminStatus) == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set GR Admin Status");
        return;
    }
    nmhSetFsbgp4GRAdminStatus (i4GrAdminStatus);

    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);

    IssProcessBgpGrConfPageGet (pHttp);
}

/*********************************************************************
 *  Function Name : IssProcessBgpPeerGroupConfPage
 *  Description   : This function processes the request coming for the
 *                  BGP PeerGroup configuration page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpPeerGroupConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessBgpPeerGroupConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessBgpPeerGroupConfPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name :IssProcessBgpPeerGroupConfPageGet 
*  Description   : This function processes the GET request coming for the
*                  BGP PeerGroup configuration page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessBgpPeerGroupConfPageGet (tHttp * pHttp)
{
    INT4                i4Temp = 0;
    INT4                i4RetVal = 0;
    INT4                i4BgpContextId = 0;
    INT4                i4BgpPrevContextId = 0;
    UINT1               au1PeerGroup[BGP_MAX_PEER_GROUP_NAME];
    UINT1               au1PeerGroupNext[BGP_MAX_PEER_GROUP_NAME];
    UINT1               au1BgpCxtName[VCM_ALIAS_MAX_LEN];
    tSNMP_OCTET_STRING_TYPE PeerGroup;
    tSNMP_OCTET_STRING_TYPE PeerGroupNext;

    MEMSET (au1PeerGroup, 0, BGP_MAX_PEER_GROUP_NAME);
    MEMSET (au1PeerGroupNext, 0, BGP_MAX_PEER_GROUP_NAME);
    MEMSET (&PeerGroup, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PeerGroupNext, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1BgpCxtName, 0, VCM_ALIAS_MAX_LEN);

    PeerGroup.pu1_OctetList = au1PeerGroup;
    PeerGroupNext.pu1_OctetList = au1PeerGroupNext;

    pHttp->i4Write = 0;
    IssPrintAvailableBgpContextId (pHttp);

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    if (nmhGetFirstIndexFsMIBgpContextTable (&i4BgpContextId) == SNMP_FAILURE)
    {
        pHttp->i4Write = i4Temp;
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    i4Temp = pHttp->i4Write;

    do
    {
        pHttp->i4Write = i4Temp;

        if (BgpSetContext (i4BgpContextId) == SNMP_FAILURE)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
            return;
        }

        if (nmhGetFirstIndexFsBgp4PeerGroupTable (&PeerGroup) == SNMP_FAILURE)
        {
            i4BgpPrevContextId = i4BgpContextId;
            if (nmhGetNextIndexFsMIBgpContextTable (i4BgpPrevContextId,
                                                    &i4BgpContextId) !=
                SNMP_SUCCESS)
            {
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
                WebnmSockWrite (pHttp,
                                (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                                (pHttp->i4HtmlSize - pHttp->i4Write));
                return;
            }
            continue;
        }

        /*Process the BGP peer table */
        do
        {
            pHttp->i4Write = i4Temp;
            STRCPY (pHttp->au1KeyString, "fsBgp4PeerGroupName_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SNPRINTF ((CHR1 *) pHttp->au1DataString,
                      (STRLEN (PeerGroup.pu1_OctetList) + 1), "%s",
                      PeerGroup.pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsBgp4PeerGroupRemoteAs (&PeerGroup, (UINT4 *) &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsBgp4PeerGroupRemoteAs_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", (UINT4) i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsBgp4PeerGroupHoldTimeConfigured (&PeerGroup, &i4RetVal);
            STRCPY (pHttp->au1KeyString,
                    "fsBgp4PeerGroupHoldTimeConfigured_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsBgp4PeerGroupKeepAliveConfigured (&PeerGroup, &i4RetVal);
            STRCPY (pHttp->au1KeyString,
                    "fsBgp4PeerGroupKeepAliveConfigured_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsBgp4PeerGroupConnectRetryInterval (&PeerGroup, &i4RetVal);
            STRCPY (pHttp->au1KeyString,
                    "fsBgp4PeerGroupConnectRetryInterval_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsBgp4PeerGroupMinASOriginInterval (&PeerGroup, &i4RetVal);
            STRCPY (pHttp->au1KeyString,
                    "fsBgp4PeerGroupMinASOriginInterval_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsBgp4PeerGroupMinRouteAdvInterval (&PeerGroup, &i4RetVal);
            STRCPY (pHttp->au1KeyString,
                    "fsBgp4PeerGroupMinRouteAdvInterval_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsBgp4PeerGroupAllowAutomaticStart (&PeerGroup, &i4RetVal);
            STRCPY (pHttp->au1KeyString,
                    "fsBgp4PeerGroupAllowAutomaticStart_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsBgp4PeerGroupAllowAutomaticStop (&PeerGroup, &i4RetVal);
            STRCPY (pHttp->au1KeyString,
                    "fsBgp4PeerGroupAllowAutomaticStop_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsBgp4PeerGroupIdleHoldTimeConfigured (&PeerGroup, &i4RetVal);
            STRCPY (pHttp->au1KeyString,
                    "fsBgp4PeerGroupIdleHoldTimeConfigured_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsBgp4PeerGroupDampPeerOscillations (&PeerGroup, &i4RetVal);
            STRCPY (pHttp->au1KeyString,
                    "fsBgp4PeerGroupDampPeerOscillations_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsBgp4PeerGroupDelayOpen (&PeerGroup, &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsBgp4PeerGroupDelayOpen_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsBgp4PeerGroupDelayOpenTimeConfigured (&PeerGroup,
                                                          &i4RetVal);
            STRCPY (pHttp->au1KeyString,
                    "fsBgp4PeerGroupDelayOpenTimeConfigured_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsBgp4PeerGroupPrefixUpperLimit (&PeerGroup, &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsBgp4PeerGroupPrefixUpperLimit_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsBgp4PeerGroupTcpConnectRetryCnt (&PeerGroup, &i4RetVal);
            STRCPY (pHttp->au1KeyString,
                    "fsBgp4PeerGroupTcpConnectRetryCnt_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            MEMCPY (&PeerGroupNext, &PeerGroup, sizeof (PeerGroup));

            STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            VcmGetAliasName ((UINT4) i4BgpContextId, au1BgpCxtName);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1BgpCxtName);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        }
        while (nmhGetNextIndexFsBgp4PeerGroupTable (&PeerGroupNext, &PeerGroup) == SNMP_SUCCESS);    /*end of do-while */
        i4BgpPrevContextId = i4BgpContextId;
    }
    while (nmhGetNextIndexFsMIBgpContextTable
           (i4BgpPrevContextId, &i4BgpContextId) == SNMP_SUCCESS);
    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
*  Function Name :IssProcessBgpPeerGroupConfPageSet 
*  Description   : This function processes the SET request coming for the
*                  BGP PeerGroup configuration page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessBgpPeerGroupConfPageSet (tHttp * pHttp)
{
    UINT4               u4ErrorCode = BGP4_DOWN;
    UINT4               u4RemoteAs = BGP4_INV_AS;
    INT4                i4HoldTime = BGP4_DEF_HOLDINTERVAL;
    INT4                i4CxtId = 0;
    INT4                i4KeepAlive = BGP4_DEF_KEEPALIVEINTERVAL;
    INT4                i4ConnectRetryInterval = BGP4_DEF_CONNRETRYINTERVAL;
    INT4                i4MinASOrigInt = BGP4_DEF_MINASORIGINTERVAL;
    INT4                i4MinRtAdvInt = BGP4_DEF_IBGP_MINROUTEADVINTERVAL;
    INT4                i4AutoStart = BGP4_PEER_AUTOMATICSTART_DISABLE;
    INT4                i4AutoStop = BGP4_PEER_AUTOMATICSTOP_DISABLE;
    INT4                i4IdleHoldTime = BGP4_DEF_IDLEHOLDINTERVAL;
    INT4                i4DampPeerOsi = BGP4_DAMP_PEER_OSCILLATIONS_DISABLE;
    INT4                i4Delayopen = BGP4_PEER_DELAY_OPEN_DISABLE;
    INT4                i4DelayOpenTimeInt = BGP4_DEF_DELAYOPENINTERVAL;
    INT4                i4MaxUpperLimt = BGP4_DEF_MAX_PEER_PREFIX_LIMIT;
    INT4                i4ConnectRetryCnt = BGP4_DEF_CONNECT_RETRY_COUNT;
    INT4                i1RetVal = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;
    INT4                i4SupportStatus;
    tSNMP_OCTET_STRING_TYPE PeerGroup;
    UINT1               au1PeerGroup[BGP_MAX_PEER_GROUP_NAME] = { 0 };

    MEMSET (&PeerGroup, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1PeerGroup, '\0', BGP_MAX_PEER_GROUP_NAME);

    PeerGroup.pu1_OctetList = au1PeerGroup;
    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (0 == STRCMP (pHttp->au1Value, "Apply"))
    {
        STRCPY (pHttp->au1Name, "CONTEXT_ID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        VcmIsVrfExist (pHttp->au1Value, (UINT4 *) &i4CxtId);
    }
    else
    {
        STRCPY (pHttp->au1Name, "CONTEXT_ID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4CxtId = ATOI (pHttp->au1Value);
    }

    if (BgpSetContext (i4CxtId) == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
        return;
    }

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupName");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
    {
        PeerGroup.i4_Length = (INT4) STRLEN (pHttp->au1Value);
        if (PeerGroup.i4_Length >= (BGP_MAX_PEER_GROUP_NAME))
        {
            PeerGroup.i4_Length = BGP_MAX_PEER_GROUP_NAME - 1;
        }
        MEMCPY (PeerGroup.pu1_OctetList, pHttp->au1Value, PeerGroup.i4_Length);
        PeerGroup.pu1_OctetList[PeerGroup.i4_Length] = 0;
    }

    STRCPY (pHttp->au1Name, "ACTION");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)

    {
        if (0 == STRCMP (pHttp->au1Value, "Delete"))
        {
            i1RetVal = nmhSetFsBgp4PeerGroupStatus (&PeerGroup, ISS_DESTROY);

            if (i1RetVal == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to delete entry\r\n");
            }
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessBgpPeerGroupConfPageGet (pHttp);
            return;

        }
        else if (0 == STRCMP (pHttp->au1Value, "ADD"))
        {
            if (nmhGetFsBgp4PeerGroupStatus (&PeerGroup, &i4RowStatus) ==
                SNMP_FAILURE)
            {
                i1RetVal =
                    nmhSetFsBgp4PeerGroupStatus (&PeerGroup, ISS_CREATE_AND_GO);
                if (i1RetVal == SNMP_FAILURE)
                {
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "\r Unable to Create a Row!\r\n");
                    return;
                }
            }
        }
    }

    /*Remote AS */
    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupRemoteAs");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        u4RemoteAs = (UINT4) ATOI (pHttp->au1Value);
        nmhGetFsbgp4FourByteASNSupportStatus (&i4SupportStatus);

        if ((i4SupportStatus == BGP4_DISABLE_4BYTE_ASN_SUPPORT) &&
            (u4RemoteAs > BGP4_MAX_TWO_BYTE_AS))
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "4-byte ASN Capability is disabled. Configure value < 65536");
            IssProcessBgpContextCreationPageGet (pHttp);
            return;
        }
    }

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupHoldTimeConfigured");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
        i4HoldTime = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupKeepAliveConfigured");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
        i4KeepAlive = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupConnectRetryInterval");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery) ==
        ENM_SUCCESS)
        i4ConnectRetryInterval = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupMinASOriginInterval");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery) ==
        ENM_SUCCESS)
        i4MinASOrigInt = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupMinRouteAdvInterval");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery) ==
        ENM_SUCCESS)
        i4MinRtAdvInt = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupAllowAutomaticStart");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery) ==
        ENM_SUCCESS)
        i4AutoStart = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupAllowAutomaticStop");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
        i4AutoStop = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupIdleHoldTimeConfigured");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery) ==
        ENM_SUCCESS)
        i4IdleHoldTime = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupDampPeerOscillations");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
        i4DampPeerOsi = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupDelayOpen");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
        i4Delayopen = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupDelayOpenTimeConfigured");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
        i4DelayOpenTimeInt = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupPrefixUpperLimit");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
        i4MaxUpperLimt = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupTcpConnectRetryCnt");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
        i4ConnectRetryCnt = ATOI (pHttp->au1Value);

    i1RetVal = nmhTestv2FsBgp4PeerGroupRemoteAs (&u4ErrorCode,
                                                 &PeerGroup, (INT4) u4RemoteAs);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid Remote-As!\r\n");
        return;
    }
    i1RetVal = nmhTestv2FsBgp4PeerGroupHoldTimeConfigured (&u4ErrorCode,
                                                           &PeerGroup,
                                                           i4HoldTime);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid Hold Time!\r\n");
        return;
    }
    i1RetVal = nmhTestv2FsBgp4PeerGroupKeepAliveConfigured (&u4ErrorCode,
                                                            &PeerGroup,
                                                            i4KeepAlive);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid KeepAlive Time!\r\n");
        return;
    }
    i1RetVal = nmhTestv2FsBgp4PeerGroupConnectRetryInterval (&u4ErrorCode,
                                                             &PeerGroup,
                                                             i4ConnectRetryInterval);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "\r Invalid Connect Retry Interval!\r\n");
        return;
    }
    i1RetVal = nmhTestv2FsBgp4PeerGroupMinASOriginInterval (&u4ErrorCode,
                                                            &PeerGroup,
                                                            i4MinASOrigInt);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "\r Invalid Min ASOrigin Interval!\r\n");
        return;
    }
    i1RetVal = nmhTestv2FsBgp4PeerGroupMinRouteAdvInterval (&u4ErrorCode,
                                                            &PeerGroup,
                                                            i4MinRtAdvInt);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "\r Invalid  RouteAdv Interval!\r\n");
        return;
    }
    i1RetVal = nmhTestv2FsBgp4PeerGroupAllowAutomaticStart (&u4ErrorCode,
                                                            &PeerGroup,
                                                            (UINT1)
                                                            i4AutoStart);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "\r Invalid  AutoStart Status!\r\n");
        return;
    }
    i1RetVal = nmhTestv2FsBgp4PeerGroupAllowAutomaticStop (&u4ErrorCode,
                                                           &PeerGroup,
                                                           (UINT1) i4AutoStop);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid  AutoStop Status!\r\n");
        return;
    }
    i1RetVal = nmhTestv2FsBgp4PeerGroupDampPeerOscillations (&u4ErrorCode,
                                                             &PeerGroup,
                                                             (UINT1)
                                                             i4DampPeerOsi);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "\r Invalid Damppeer Oscillations Status!\r\n");
        return;
    }
    i1RetVal = nmhTestv2FsBgp4PeerGroupDelayOpen (&u4ErrorCode,
                                                  &PeerGroup,
                                                  (UINT1) i4Delayopen);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid DelayOpen Status!\r\n");
        return;
    }
    i1RetVal = nmhTestv2FsBgp4PeerGroupDelayOpenTimeConfigured (&u4ErrorCode,
                                                                &PeerGroup,
                                                                i4DelayOpenTimeInt);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "\r Invalid Delay Open Interval!\r\n");
        return;
    }
    i1RetVal = nmhTestv2FsBgp4PeerGroupPrefixUpperLimit (&u4ErrorCode,
                                                         &PeerGroup,
                                                         i4MaxUpperLimt);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "\r Invalid PrefixUpper Limit!\r\n");
        return;
    }
    i1RetVal = nmhTestv2FsBgp4PeerGroupTcpConnectRetryCnt (&u4ErrorCode,
                                                           &PeerGroup,
                                                           i4ConnectRetryCnt);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid Retry Count!\r\n");
        return;
    }

    i1RetVal = nmhSetFsBgp4PeerGroupRemoteAs (&PeerGroup, (INT4) u4RemoteAs);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid Remote-As!\r\n");
        return;
    }
    i1RetVal = nmhSetFsBgp4PeerGroupHoldTimeConfigured (&PeerGroup, i4HoldTime);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "\r Invalid HoldTime Interval!\r\n");
        return;
    }
    i1RetVal = nmhSetFsBgp4PeerGroupKeepAliveConfigured (&PeerGroup,
                                                         i4KeepAlive);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "\r Invalid KeepAlive Interval!\r\n");
        return;
    }
    i1RetVal = nmhSetFsBgp4PeerGroupConnectRetryInterval (&PeerGroup,
                                                          i4ConnectRetryInterval);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "\r Invalid ConnectRetry Interval!\r\n");
        return;
    }
    i1RetVal = nmhSetFsBgp4PeerGroupMinASOriginInterval (&PeerGroup,
                                                         i4MinASOrigInt);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "\r Invalid Min ASOrigin Interval!\r\n");
        return;
    }
    i1RetVal =
        nmhSetFsBgp4PeerGroupMinRouteAdvInterval (&PeerGroup, i4MinRtAdvInt);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "\r Invalid RouteAdv Interval!\r\n");
        return;
    }
    i1RetVal =
        nmhSetFsBgp4PeerGroupAllowAutomaticStart (&PeerGroup, i4AutoStart);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "\r Invalid AutomaticStart Status!\r\n");
        return;
    }

    if (i4AutoStart != 2)
    {
        i1RetVal = nmhTestv2FsBgp4PeerGroupIdleHoldTimeConfigured (&u4ErrorCode,
                                                                   &PeerGroup,
                                                                   i4IdleHoldTime);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "\r Invalid Idle Hold Time Interval!\r\n");
            return;
        }
    }
    i1RetVal = nmhSetFsBgp4PeerGroupAllowAutomaticStop (&PeerGroup, i4AutoStop);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "\r Invalid AutomaticStop Status!\r\n");
        return;
    }
    i1RetVal =
        nmhSetFsBgp4PeerGroupIdleHoldTimeConfigured (&PeerGroup,
                                                     i4IdleHoldTime);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid IdleHold Time!\r\n");
        return;
    }
    i1RetVal =
        nmhSetFsBgp4PeerGroupDampPeerOscillations (&PeerGroup, i4DampPeerOsi);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "\r Invalid DampPeeroscillation Status!\r\n");
        return;
    }
    i1RetVal = nmhSetFsBgp4PeerGroupDelayOpen (&PeerGroup, i4Delayopen);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid DelayOpen Status!\r\n");
        return;
    }
    i1RetVal =
        nmhSetFsBgp4PeerGroupDelayOpenTimeConfigured (&PeerGroup,
                                                      i4DelayOpenTimeInt);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "\r Invalid DelayOpenTime Interval!\r\n");
        return;
    }
    i1RetVal =
        nmhSetFsBgp4PeerGroupPrefixUpperLimit (&PeerGroup, i4MaxUpperLimt);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid PrefixUpperLimit!\r\n");
        return;
    }

    i1RetVal =
        nmhSetFsBgp4PeerGroupTcpConnectRetryCnt (&PeerGroup, i4ConnectRetryCnt);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "\r Invalid Tcp ConnectRetry Count!\r\n");
        return;
    }
    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessBgpPeerGroupConfPageGet (pHttp);
    return;
}

/*********************************************************************
 *  Function Name :IssProcessBgpGroupConfContPage 
 *  Description   : This function processes the request coming for the
 *                  BGP PeerGroup(contd...) configuration page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpGroupConfContPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessBgpPeerGroupConfPageContGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessBgpPeerGroupConfContPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessBgpPeerGroupConfPageContGet
*  Description   : This function processes the GET request coming for the
*                  BGP PeerGroup(contd..) configuration page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessBgpPeerGroupConfPageContGet (tHttp * pHttp)
{
    INT4                i4Temp = 0;
    INT4                i4RetVal = 0;
    INT4                i4BgpContextId = 0;
    INT4                i4BgpPrevContextId = 0;
    INT4                i4OrfMode = 0;
    UINT4               u4OrfType = 0;
    INT4                i4BfdStatus = 0;
    UINT1              *pu1String = NULL;
    UINT1               au1PeerGroup[BGP_MAX_PEER_GROUP_NAME];
    UINT1               au1PeerGroupNext[BGP_MAX_PEER_GROUP_NAME];
    UINT1               au1RMapNameIn[RMAP_MAX_NAME_LEN + 4] = { 0 };
    UINT1               au1RMapNameOut[RMAP_MAX_NAME_LEN + 4] = { 0 };
    UINT1               au1BgpCxtName[VCM_ALIAS_MAX_LEN];
    tSNMP_OCTET_STRING_TYPE PeerGroup;
    tSNMP_OCTET_STRING_TYPE PeerGroupNext;
    tSNMP_OCTET_STRING_TYPE RouteMapNameIn;
    tSNMP_OCTET_STRING_TYPE RouteMapNameOut;

    MEMSET (au1PeerGroup, '\0', BGP_MAX_PEER_GROUP_NAME);
    MEMSET (au1PeerGroupNext, '\0', BGP_MAX_PEER_GROUP_NAME);
    MEMSET (&PeerGroup, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PeerGroupNext, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&RouteMapNameIn, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&RouteMapNameOut, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1RMapNameIn, '\0', (RMAP_MAX_NAME_LEN + 4));
    MEMSET (au1RMapNameOut, '\0', (RMAP_MAX_NAME_LEN + 4));
    MEMSET (au1BgpCxtName, 0, VCM_ALIAS_MAX_LEN);

    PeerGroup.pu1_OctetList = au1PeerGroup;
    PeerGroupNext.pu1_OctetList = au1PeerGroupNext;
    RouteMapNameIn.pu1_OctetList = au1RMapNameIn;
    RouteMapNameOut.pu1_OctetList = au1RMapNameOut;
    pu1String = au1RMapNameIn;

    pHttp->i4Write = 0;
    IssPrintAvailableBgpContextId (pHttp);

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    if (nmhGetFirstIndexFsMIBgpContextTable (&i4BgpContextId) == SNMP_FAILURE)
    {
        pHttp->i4Write = i4Temp;
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    i4Temp = pHttp->i4Write;

    do
    {
        if (BgpSetContext (i4BgpContextId) == SNMP_FAILURE)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
            return;
        }

        if (nmhGetFirstIndexFsBgp4PeerGroupTable (&PeerGroup) == SNMP_FAILURE)
        {
            i4BgpPrevContextId = i4BgpContextId;
            if (nmhGetNextIndexFsMIBgpContextTable (i4BgpPrevContextId,
                                                    &i4BgpContextId) !=
                SNMP_SUCCESS)
            {
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
                WebnmSockWrite (pHttp,
                                (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                                (pHttp->i4HtmlSize - pHttp->i4Write));
                return;
            }
            continue;
        }

        /*Process the BGP peer table */
        do
        {
            pHttp->i4Write = i4Temp;
            STRCPY (pHttp->au1KeyString, "fsBgp4PeerGroupName_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SNPRINTF ((CHR1 *) pHttp->au1DataString,
                      (STRLEN (PeerGroup.pu1_OctetList) + 1), "%s",
                      PeerGroup.pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsBgp4PeerGroupEBGPMultiHop (&PeerGroup, &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsBgp4PeerGroupEBGPMultiHop_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsBgp4PeerGroupEBGPHopLimit (&PeerGroup, &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsBgp4PeerGroupEBGPHopLimit_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsBgp4PeerGroupNextHopSelf (&PeerGroup, &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsBgp4PeerGroupNextHopSelf_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsBgp4PeerGroupRflClient (&PeerGroup, &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsBgp4PeerGroupRflClient_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsBgp4PeerGroupTcpSendBufSize (&PeerGroup, &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsBgp4PeerGroupTcpSendBufSize_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsBgp4PeerGroupTcpRcvBufSize (&PeerGroup, &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsBgp4PeerGroupTcpRcvBufSize_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsBgp4PeerGroupCommSendStatus (&PeerGroup, &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsBgp4PeerGroupCommSendStatus_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsBgp4PeerGroupECommSendStatus (&PeerGroup, &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsBgp4PeerGroupECommSendStatus_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsBgp4PeerGroupPassive (&PeerGroup, &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsBgp4PeerGroupPassive_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsBgp4PeerGroupDefaultOriginate (&PeerGroup, &i4RetVal);
            STRCPY (pHttp->au1KeyString, "fsBgp4PeerGroupDefaultOriginate_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            /* By default for Activate MPCapability ipv4 is dispalyed */

            i4RetVal = 1;
            STRCPY (pHttp->au1KeyString,
                    "fsBgp4PeerGroupActivateMPCapability_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            /*By default for Deactivate MPCapability ipv4 is dispalyed */

            i4RetVal = 1;
            STRCPY (pHttp->au1KeyString,
                    "fsBgp4PeerGroupDeactivateMPCapability_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsBgp4PeerGroupRouteMapNameIn (&PeerGroup, &RouteMapNameIn);
            STRCPY (pHttp->au1KeyString, "fsBgp4PeerGroupRouteMapNameIn_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SNPRINTF ((CHR1 *) pHttp->au1DataString, (RMAP_MAX_NAME_LEN + 1),
                      "%s", pu1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            pu1String = NULL;

            pu1String = au1RMapNameOut;
            nmhGetFsBgp4PeerGroupRouteMapNameOut (&PeerGroup, &RouteMapNameOut);
            STRCPY (pHttp->au1KeyString, "fsBgp4PeerGroupRouteMapNameOut_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SNPRINTF ((CHR1 *) pHttp->au1DataString, (RMAP_MAX_NAME_LEN + 1),
                      "%s", pu1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            MEMSET (&RouteMapNameIn, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
            MEMSET (au1RMapNameIn, 0, sizeof (au1RMapNameIn));
            RouteMapNameIn.pu1_OctetList = au1RMapNameIn;
            nmhGetFsBgp4PeerGroupIpPrefixNameIn (&PeerGroup, &RouteMapNameIn);
            STRCPY (pHttp->au1KeyString, "fsBgp4PeerGroupIpPrefixNameIn_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SNPRINTF ((CHR1 *) pHttp->au1DataString, (RMAP_MAX_NAME_LEN + 1),
                      "%s", RouteMapNameIn.pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            MEMSET (&RouteMapNameOut, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
            MEMSET (au1RMapNameOut, 0, sizeof (au1RMapNameOut));
            RouteMapNameOut.pu1_OctetList = au1RMapNameOut;
            nmhGetFsBgp4PeerGroupIpPrefixNameOut (&PeerGroup, &RouteMapNameOut);
            STRCPY (pHttp->au1KeyString, "fsBgp4PeerGroupIpPrefixNameOut_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SNPRINTF ((CHR1 *) pHttp->au1DataString, (RMAP_MAX_NAME_LEN + 1),
                      "%s", RouteMapNameOut.pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsBgp4PeerGroupOrfType (&PeerGroup, &u4OrfType);
            STRCPY (pHttp->au1KeyString, "fsBgp4PeerGroupOrfType_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (u4OrfType == BGP_ORF_TYPE_ADDRESS_PREFIX)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Address-Prefix");
            }
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsBgp4PeerGroupOrfCapMode (&PeerGroup, &i4OrfMode);
            STRCPY (pHttp->au1KeyString, "fsBgp4PeerGroupOrfMode_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4OrfMode);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsBgp4PeerGroupBfdStatus (&PeerGroup, &i4BfdStatus);
            STRCPY (pHttp->au1KeyString, "fsBgp4PeerGroupBfdStatus_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4OrfMode);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            MEMCPY (&PeerGroupNext, &PeerGroup, sizeof (PeerGroup));

            STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            VcmGetAliasName ((UINT4) i4BgpContextId, au1BgpCxtName);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1BgpCxtName);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        }
        while (nmhGetNextIndexFsBgp4PeerGroupTable (&PeerGroupNext, &PeerGroup) == SNMP_SUCCESS);    /*end of do-while */

        i4BgpPrevContextId = i4BgpContextId;
    }
    while (nmhGetNextIndexFsMIBgpContextTable
           (i4BgpPrevContextId, &i4BgpContextId) == SNMP_SUCCESS);
    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
*  Function Name :IssProcessBgpPeerGroupConfContPageSet 
*  Description   : This function processes the SET request coming for the
*                  BGP PeerGroup(contd..) configuration page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessBgpPeerGroupConfContPageSet (tHttp * pHttp)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4OrfType = 0;
    INT4                i4BfdStatus = 0;
    INT4                i4OrfMode = 0;
    INT4                i4CxtId = 0;
    INT4                i4EBgpStatus = BGP4_EBGP_MULTI_HOP_DISABLE;
    INT4                i4EbgpHopLimit = BGP4_DEFAULT_HOPLIMIT;
    INT4                i4NextHopStatus = BGP4_DISABLE_NEXTHOP_SELF;
    INT4                i4RflAction = 0;
    INT4                i4TcpSendSize = BGP4_DEFAULT_SENDBUF;
    INT4                i4TcpRcvSize = BGP4_DEFAULT_RECVBUF;
    INT4                i4CommSendStatus = 0;
    INT4                i4EcommSendStatus = 0;
    INT4                i4PeerPassive = 0;
    INT4                i4DefRouteStatus = 0;
    INT4                i4ActiveCapValue = 0;
    INT4                i4DeactiveCapValue = 0;
    INT4                i1RetVal = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;
    tSNMP_OCTET_STRING_TYPE PeerGroup;
    tSNMP_OCTET_STRING_TYPE RouteMapNameIn;
    tSNMP_OCTET_STRING_TYPE RouteMapNameOut;
    tSNMP_OCTET_STRING_TYPE IpPrefixNameIn;
    tSNMP_OCTET_STRING_TYPE IpPrefixNameOut;
    UINT1               au1PeerGroup[BGP_MAX_PEER_GROUP_NAME] = { 0 };
    UINT1               au1RMapNameIn[RMAP_MAX_NAME_LEN + 4] = { 0 };
    UINT1               au1RMapNameOut[RMAP_MAX_NAME_LEN + 4] = { 0 };
    UINT1               au1IpPrefixNameIn[RMAP_MAX_NAME_LEN + 4] = { 0 };
    UINT1               au1IpPrefixNameOut[RMAP_MAX_NAME_LEN + 4] = { 0 };
    UINT1               au1OrfType[RMAP_MAX_NAME_LEN + 4] = { 0 };

    MEMSET (&PeerGroup, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&RouteMapNameIn, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&RouteMapNameOut, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&IpPrefixNameIn, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&IpPrefixNameOut, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    PeerGroup.pu1_OctetList = au1PeerGroup;
    RouteMapNameIn.pu1_OctetList = au1RMapNameIn;
    RouteMapNameOut.pu1_OctetList = au1RMapNameOut;
    IpPrefixNameIn.pu1_OctetList = au1IpPrefixNameIn;
    IpPrefixNameOut.pu1_OctetList = au1IpPrefixNameOut;
    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if ((0 == STRCMP (pHttp->au1Value, "Delete"))
        || (0 == STRCMP (pHttp->au1Value, "Apply")))
    {
        STRCPY (pHttp->au1Name, "CONTEXT_ID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        VcmIsVrfExist (pHttp->au1Value, (UINT4 *) &i4CxtId);
    }
    else
    {
        STRCPY (pHttp->au1Name, "CONTEXT_ID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4CxtId = ATOI (pHttp->au1Value);
    }

    if (BgpSetContext (i4CxtId) == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
        return;
    }

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupName");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
    {
        PeerGroup.i4_Length = (INT4) STRLEN (pHttp->au1Value);
        if (PeerGroup.i4_Length >= (BGP_MAX_PEER_GROUP_NAME))
        {
            PeerGroup.i4_Length = BGP_MAX_PEER_GROUP_NAME - 1;
        }
        MEMCPY (PeerGroup.pu1_OctetList, pHttp->au1Value, PeerGroup.i4_Length);
        PeerGroup.pu1_OctetList[PeerGroup.i4_Length] = 0;
    }

    STRCPY (pHttp->au1Name, "ACTION");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)

    {
        if (0 == STRCMP (pHttp->au1Value, "Delete"))
        {
            i1RetVal = nmhSetFsBgp4PeerGroupStatus (&PeerGroup, ISS_DESTROY);

            if (i1RetVal == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to delete entry\r\n");
            }
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessBgpPeerGroupConfPageContGet (pHttp);
            return;
        }
        else if (0 == STRCMP (pHttp->au1Value, "ADD"))
        {
            if (nmhGetFsBgp4PeerGroupStatus (&PeerGroup, &i4RowStatus) ==
                SNMP_FAILURE)
            {
                i1RetVal =
                    nmhSetFsBgp4PeerGroupStatus (&PeerGroup, ISS_CREATE_AND_GO);
                if (i1RetVal == SNMP_FAILURE)
                {
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "\r Unable to Create a Row!\r\n");
                    return;
                }
            }
        }
    }

    /*Remote AS */
    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupEBGPMultiHop");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
        i4EBgpStatus = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupEBGPHopLimit");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
        i4EbgpHopLimit = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupNextHopSelf");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
        i4NextHopStatus = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupRflClient");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery) ==
        ENM_SUCCESS)
        i4RflAction = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupTcpSendBufSize");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery) ==
        ENM_SUCCESS)
        i4TcpSendSize = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupTcpRcvBufSize");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery) ==
        ENM_SUCCESS)
        i4TcpRcvSize = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupCommSendStatus");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery) ==
        ENM_SUCCESS)
        i4CommSendStatus = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupECommSendStatus");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
        i4EcommSendStatus = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupPassive");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery) ==
        ENM_SUCCESS)
        i4PeerPassive = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupDefaultOriginate");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
        i4DefRouteStatus = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupActivateMPCapability");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
        i4ActiveCapValue = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupDeactivateMPCapability");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
        i4DeactiveCapValue = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupRouteMapNameIn");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
    {
        RouteMapNameIn.i4_Length = (INT4) STRLEN (pHttp->au1Value);
        if (RouteMapNameIn.i4_Length >= (RMAP_MAX_NAME_LEN + 4))
        {
            RouteMapNameIn.i4_Length = RMAP_MAX_NAME_LEN + 3;
        }
        MEMCPY (RouteMapNameIn.pu1_OctetList, pHttp->au1Value,
                RouteMapNameIn.i4_Length);
        RouteMapNameIn.pu1_OctetList[RouteMapNameIn.i4_Length] = 0;

    }

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupRouteMapNameOut");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
    {
        RouteMapNameOut.i4_Length = (INT4) STRLEN (pHttp->au1Value);
        if (RouteMapNameOut.i4_Length >= (RMAP_MAX_NAME_LEN + 4))
        {
            RouteMapNameOut.i4_Length = RMAP_MAX_NAME_LEN + 3;
        }
        MEMCPY (RouteMapNameOut.pu1_OctetList, pHttp->au1Value,
                RouteMapNameOut.i4_Length);
        RouteMapNameOut.pu1_OctetList[RouteMapNameOut.i4_Length] = 0;
    }

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupIpPrefixNameIn");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
    {
        IpPrefixNameIn.i4_Length = (INT4) STRLEN (pHttp->au1Value);
        if (IpPrefixNameIn.i4_Length >= (RMAP_MAX_NAME_LEN + 4))
        {
            IpPrefixNameIn.i4_Length = RMAP_MAX_NAME_LEN + 3;
        }
        MEMCPY (IpPrefixNameIn.pu1_OctetList, pHttp->au1Value,
                IpPrefixNameIn.i4_Length);
        IpPrefixNameIn.pu1_OctetList[IpPrefixNameIn.i4_Length] = 0;
    }

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupIpPrefixNameOut");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
    {
        IpPrefixNameOut.i4_Length = (INT4) STRLEN (pHttp->au1Value);
        if (IpPrefixNameOut.i4_Length >= (RMAP_MAX_NAME_LEN + 4))
        {
            IpPrefixNameOut.i4_Length = RMAP_MAX_NAME_LEN + 3;
        }
        MEMCPY (IpPrefixNameOut.pu1_OctetList, pHttp->au1Value,
                IpPrefixNameOut.i4_Length);
        IpPrefixNameOut.pu1_OctetList[IpPrefixNameOut.i4_Length] = 0;
    }

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupOrfType");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
    {
        MEMCPY (au1OrfType, pHttp->au1Value, RMAP_MAX_NAME_LEN);
    }

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupOrfMode");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
        i4OrfMode = (INT4) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupBfdStatus");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
        i4BfdStatus = (INT4) ATOI (pHttp->au1Value);

    i1RetVal = nmhTestv2FsBgp4PeerGroupEBGPMultiHop (&u4ErrorCode,
                                                     &PeerGroup, i4EBgpStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid EBGP!\r\n");
        return;
    }
    i1RetVal = nmhTestv2FsBgp4PeerGroupEBGPHopLimit (&u4ErrorCode,
                                                     &PeerGroup,
                                                     i4EbgpHopLimit);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid EBGP HopLimit!\r\n");
        return;
    }
    i1RetVal = nmhTestv2FsBgp4PeerGroupNextHopSelf (&u4ErrorCode,
                                                    &PeerGroup,
                                                    i4NextHopStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "\r Invalid Self NextHop Status!\r\n");
        return;
    }
    i1RetVal = nmhTestv2FsBgp4PeerGroupRflClient (&u4ErrorCode,
                                                  &PeerGroup, i4RflAction);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid Rfl Status!\r\n");
        return;
    }
    i1RetVal = nmhTestv2FsBgp4PeerGroupTcpSendBufSize (&u4ErrorCode,
                                                       &PeerGroup,
                                                       i4TcpSendSize);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "\r Invalid TCP Send Buff Size!\r\n");
        return;
    }
    i1RetVal = nmhTestv2FsBgp4PeerGroupTcpRcvBufSize (&u4ErrorCode,
                                                      &PeerGroup, i4TcpRcvSize);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "\r Invalid TCP Rcv Buff size!\r\n");
        return;
    }
    i1RetVal = nmhTestv2FsBgp4PeerGroupCommSendStatus (&u4ErrorCode,
                                                       &PeerGroup,
                                                       i4CommSendStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid Comm Send Status!\r\n");
        return;
    }
    i1RetVal = nmhTestv2FsBgp4PeerGroupECommSendStatus (&u4ErrorCode,
                                                        &PeerGroup,
                                                        i4EcommSendStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "\r Invalid  Ecomm Send Stauts!\r\n");
        return;
    }
    i1RetVal = nmhTestv2FsBgp4PeerGroupPassive (&u4ErrorCode,
                                                &PeerGroup, i4PeerPassive);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid Passive Status!\r\n");
        return;
    }
    i1RetVal = nmhTestv2FsBgp4PeerGroupDefaultOriginate (&u4ErrorCode,
                                                         &PeerGroup,
                                                         i4DefRouteStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "\r Invalid Default Orginator!\r\n");
        return;
    }
    i1RetVal = nmhTestv2FsBgp4PeerGroupActivateMPCapability (&u4ErrorCode,
                                                             &PeerGroup,
                                                             i4ActiveCapValue);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "\r Invalid ActivateMPCapability!\r\n");
        return;
    }
    i1RetVal = nmhTestv2FsBgp4PeerGroupDeactivateMPCapability (&u4ErrorCode,
                                                               &PeerGroup,
                                                               i4DeactiveCapValue);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "\r Invalid DeactivateMPCapability!\r\n");
        return;
    }
    if (RouteMapNameIn.i4_Length != 0)
    {
        i1RetVal = nmhTestv2FsBgp4PeerGroupRouteMapNameIn (&u4ErrorCode,
                                                           &PeerGroup,
                                                           &RouteMapNameIn);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "\r Invalid InRouteMap!\r\n");
            return;
        }
    }
    if (RouteMapNameOut.i4_Length != 0)
    {
        i1RetVal = nmhTestv2FsBgp4PeerGroupRouteMapNameOut (&u4ErrorCode,
                                                            &PeerGroup,
                                                            &RouteMapNameOut);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "\r Invalid Out Route-Map!\r\n");
            return;
        }
    }

    if (IpPrefixNameIn.i4_Length != 0)
    {
        i1RetVal = nmhTestv2FsBgp4PeerGroupIpPrefixNameIn (&u4ErrorCode,
                                                           &PeerGroup,
                                                           &IpPrefixNameIn);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "\r Invalid In Prefix list!\r\n");
            return;
        }
    }

    if (IpPrefixNameOut.i4_Length != 0)
    {
        i1RetVal = nmhTestv2FsBgp4PeerGroupIpPrefixNameOut (&u4ErrorCode,
                                                            &PeerGroup,
                                                            &IpPrefixNameOut);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "\r Invalid Out Prefix list!\r\n");
            return;
        }
    }

    if (MEMCMP (au1OrfType, "Address-Prefix", STRLEN ("Address-Prefix")) == 0)
    {
        u4OrfType = BGP_ORF_TYPE_ADDRESS_PREFIX;
    }
    if (u4OrfType != 0)
    {
        i1RetVal = nmhTestv2FsBgp4PeerGroupOrfType (&u4ErrorCode,
                                                    &PeerGroup, u4OrfType);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "\r Invalid Orf Type!\r\n");
            return;
        }

        i1RetVal = nmhTestv2FsBgp4PeerGroupOrfCapMode (&u4ErrorCode,
                                                       &PeerGroup, i4OrfMode);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "\r Invalid Orf Mode!\r\n");
            return;
        }
    }

    i1RetVal = nmhTestv2FsBgp4PeerGroupBfdStatus (&u4ErrorCode,
                                                  &PeerGroup, i4BfdStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid BFD status!\r\n");
        return;
    }

    i1RetVal = nmhSetFsBgp4PeerGroupEBGPMultiHop (&PeerGroup, i4EBgpStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid EBgp!\r\n");
        return;
    }
    i1RetVal = nmhSetFsBgp4PeerGroupEBGPHopLimit (&PeerGroup, i4EbgpHopLimit);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid EBgp HopLimit!\r\n");
        return;
    }
    i1RetVal = nmhSetFsBgp4PeerGroupNextHopSelf (&PeerGroup, i4NextHopStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid nextHop status!\r\n");
        return;
    }
    i1RetVal = nmhSetFsBgp4PeerGroupRflClient (&PeerGroup, i4RflAction);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid RFL Status!\r\n");
        return;
    }
    i1RetVal = nmhSetFsBgp4PeerGroupTcpSendBufSize (&PeerGroup, i4TcpSendSize);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "\r Invalid TCP Send Buff Size!\r\n");
        return;
    }
    i1RetVal = nmhSetFsBgp4PeerGroupTcpRcvBufSize (&PeerGroup, i4TcpRcvSize);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "\r Invalid TCP Rcv Buff Size!\r\n");
        return;
    }
    i1RetVal =
        nmhSetFsBgp4PeerGroupCommSendStatus (&PeerGroup, i4CommSendStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid Comm Send Status!\r\n");
        return;
    }
    i1RetVal =
        nmhSetFsBgp4PeerGroupECommSendStatus (&PeerGroup, i4EcommSendStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "\r Invalid Ecomm Send Status!\r\n");
        return;
    }
    i1RetVal = nmhSetFsBgp4PeerGroupPassive (&PeerGroup, i4PeerPassive);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "\r Invalid Peer Passive Status!\r\n");
        return;
    }
    i1RetVal =
        nmhSetFsBgp4PeerGroupDefaultOriginate (&PeerGroup, i4DefRouteStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "\r Invalid Default Orginator!\r\n");
        return;
    }
    i1RetVal =
        nmhSetFsBgp4PeerGroupActivateMPCapability (&PeerGroup,
                                                   i4ActiveCapValue);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid MPCapability!\r\n");
        return;
    }
    i1RetVal =
        nmhSetFsBgp4PeerGroupDeactivateMPCapability (&PeerGroup,
                                                     i4DeactiveCapValue);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid MPCapability!\r\n");
        return;
    }
    if (RouteMapNameIn.i4_Length != 0)
    {
        i1RetVal =
            nmhSetFsBgp4PeerGroupRouteMapNameIn (&PeerGroup, &RouteMapNameIn);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "\r Invalid IN RouteMap Name!\r\n");
            return;
        }
    }

    if (RouteMapNameOut.i4_Length != 0)
    {
        i1RetVal =
            nmhSetFsBgp4PeerGroupRouteMapNameOut (&PeerGroup, &RouteMapNameOut);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "\r Invalid OutRouteMap Name!\r\n");
            return;
        }
    }

    if (IpPrefixNameIn.i4_Length != 0)
    {
        i1RetVal =
            nmhSetFsBgp4PeerGroupIpPrefixNameIn (&PeerGroup, &IpPrefixNameIn);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "\r Invalid IN Prefix list Name!\r\n");
            return;
        }
    }

    if (IpPrefixNameOut.i4_Length != 0)
    {
        i1RetVal =
            nmhSetFsBgp4PeerGroupIpPrefixNameOut (&PeerGroup, &IpPrefixNameOut);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "\r Invalid Out Prefix list Name!\r\n");
            return;
        }
    }
    if (u4OrfType != 0)
    {
        i1RetVal = nmhSetFsBgp4PeerGroupOrfType (&PeerGroup, u4OrfType);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "\r Invalid Orf Type!\r\n");
            return;
        }

        i1RetVal = nmhSetFsBgp4PeerGroupOrfCapMode (&PeerGroup, i4OrfMode);
        if (i1RetVal == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            return;
        }
    }

    i1RetVal = nmhSetFsBgp4PeerGroupBfdStatus (&PeerGroup, i4BfdStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid BFD Status!\r\n");
        return;
    }

    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessBgpPeerGroupConfPageContGet (pHttp);
    return;
}

/*********************************************************************
 *  Function Name : IssProcessBgpPeerGroupAddPage
 *  Description   : This function processes the request coming for the
 *                  BGP PeerGroup Addition page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpPeerGroupAddPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessBgpPeerGroupAddPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessBgpPeerGroupAddPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessBgpPeerGroupAddPageGet 
*  Description   : This function processes the GET request coming for the
*                  BGP PeerGroup Addition page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessBgpPeerGroupAddPageGet (tHttp * pHttp)
{
    INT4                i4Temp = 0;
    UINT4               u4IpAddress = 0;
    INT4                i4AddrFamily = 0;
    INT4                i4BgpContextId = 0;
    INT4                i4BgpPrevContextId = 0;
    INT4                i4NextAddrFamily = 0;
    UINT1              *pu1TempString = NULL;
#ifdef BGP4_IPV6_WANTED
    UINT1               au1Ip6Addr[ISS_BGP4_IP6_ADDR_STRING_LEN];
#endif
    UINT1               au1PeerGroup[BGP_MAX_PEER_GROUP_NAME];
    UINT1               au1PeerGroupNext[BGP_MAX_PEER_GROUP_NAME];
    UINT1               au1IpNetwork[BGP4_MAX_INET_ADDRESS_LEN];
    UINT1               au1IpNextNetwork[BGP4_MAX_INET_ADDRESS_LEN];
    UINT1               au1BgpCxtName[VCM_ALIAS_MAX_LEN];
    tSNMP_OCTET_STRING_TYPE PeerGroup;
    tSNMP_OCTET_STRING_TYPE PeerGroupNext;
    tSNMP_OCTET_STRING_TYPE IpNetwork;
    tSNMP_OCTET_STRING_TYPE IpNextNetwork;

    MEMSET (au1PeerGroup, 0, BGP_MAX_PEER_GROUP_NAME);
    MEMSET (au1PeerGroupNext, 0, BGP_MAX_PEER_GROUP_NAME);
    MEMSET (au1IpNetwork, 0, BGP4_MAX_INET_ADDRESS_LEN);
    MEMSET (au1IpNextNetwork, 0, BGP4_MAX_INET_ADDRESS_LEN);
    MEMSET (&PeerGroup, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PeerGroupNext, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&IpNetwork, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&IpNextNetwork, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1BgpCxtName, 0, VCM_ALIAS_MAX_LEN);

    PeerGroup.pu1_OctetList = au1PeerGroup;
    PeerGroupNext.pu1_OctetList = au1PeerGroupNext;
    IpNetwork.pu1_OctetList = au1IpNetwork;
    IpNextNetwork.pu1_OctetList = au1IpNextNetwork;

    pHttp->i4Write = 0;
    IssPrintAvailableBgpContextId (pHttp);

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();
    if (nmhGetFirstIndexFsMIBgpContextTable (&i4BgpContextId) == SNMP_FAILURE)
    {
        pHttp->i4Write = i4Temp;
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;

    }
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    i4Temp = pHttp->i4Write;

    do
    {
        pHttp->i4Write = i4Temp;

        if (BgpSetContext (i4BgpContextId) == SNMP_FAILURE)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
            return;
        }

        if (nmhGetFirstIndexFsBgp4PeerGroupListTable
            (&PeerGroup, &i4AddrFamily, &IpNetwork) == SNMP_FAILURE)
        {
            i4BgpPrevContextId = i4BgpContextId;
            if (nmhGetNextIndexFsMIBgpContextTable (i4BgpPrevContextId,
                                                    &i4BgpContextId) !=
                SNMP_SUCCESS)
            {
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
                WebnmSockWrite (pHttp,
                                (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                                (pHttp->i4HtmlSize - pHttp->i4Write));
                return;

            }
            continue;
        }

        /*Process the BGP peer table */
        do
        {
            pHttp->i4Write = i4Temp;
            STRCPY (pHttp->au1KeyString, "fsBgp4PeerGroupName_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SNPRINTF ((CHR1 *) pHttp->au1DataString,
                      (STRLEN (PeerGroup.pu1_OctetList) + 1), "%s",
                      PeerGroup.pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "fsBgp4PeerAddrType_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4AddrFamily);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "fsBgp4PeerAddress_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (i4AddrFamily == BGP4_INET_AFI_IPV4)
            {
                MEMCPY (&u4IpAddress, IpNetwork.pu1_OctetList,
                        IpNetwork.i4_Length);
                WEB_CONVERT_IPADDR_TO_STR (pu1TempString,
                                           OSIX_NTOHL (u4IpAddress));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1TempString);
            }
#ifdef BGP4_IPV6_WANTED
            else                /* BGP ipv6 processing */
            {
                Bgp4PrintIp6Addr (IpNetwork.pu1_OctetList, (au1Ip6Addr));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Ip6Addr);
                MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
            }
#endif /* BGP4_IPV6_WANTED */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            MEMCPY (&PeerGroupNext, &PeerGroup, sizeof (PeerGroup));
            MEMCPY (&IpNextNetwork, &IpNetwork, sizeof (IpNetwork));
            i4NextAddrFamily = i4AddrFamily;

            STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            VcmGetAliasName ((UINT4) i4BgpContextId, au1BgpCxtName);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1BgpCxtName);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        }
        while (nmhGetNextIndexFsBgp4PeerGroupListTable (&PeerGroupNext, &PeerGroup, i4NextAddrFamily, &i4AddrFamily, &IpNextNetwork, &IpNetwork) == SNMP_SUCCESS);    /*end of do-while */
        i4BgpPrevContextId = i4BgpContextId;
    }
    while (nmhGetNextIndexFsMIBgpContextTable
           (i4BgpPrevContextId, &i4BgpContextId) == SNMP_SUCCESS);

    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
*  Function Name : IssProcessBgpPeerGroupAddPageSet
*  Description   : This function processes the SET request coming for the
*                  BGP Peer configuration page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessBgpPeerGroupAddPageSet (tHttp * pHttp)
{
    INT4                i4RowStatus = SNMP_FAILURE;
    INT4                i4AddressFamily = 0;
    INT4                i4Len = 0;
    INT4                i4CxtId = 0;
    UINT4               u4ErrCode;
    UINT1               au1PeerGroup[BGP_MAX_PEER_GROUP_NAME] = { 0 };
    UINT1               au1PeerIpAddress[ISS_BGP4_IP_MAX_LENGTH] = { 0 };
    tUtlInAddr          InAddr;
    tSNMP_OCTET_STRING_TYPE PeerGroup;
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddr;
    tAddrPrefix         IpAddress;
    tSNMP_OCTET_STRING_TYPE PeerAddr;

    MEMSET (&PeerGroup, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PeerRemoteAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&IpAddress, 0, sizeof (tAddrPrefix));
    MEMSET (&PeerAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    PeerGroup.pu1_OctetList = au1PeerGroup;

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();
    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (0 == STRCMP (pHttp->au1Value, "Delete"))
    {
        STRCPY (pHttp->au1Name, "CONTEXT_ID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        VcmIsVrfExist (pHttp->au1Value, (UINT4 *) &i4CxtId);
    }
    else
    {
        STRCPY (pHttp->au1Name, "CONTEXT_ID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4CxtId = ATOI (pHttp->au1Value);
    }

    if (BgpSetContext (i4CxtId) == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
        return;
    }

    STRCPY (pHttp->au1Name, "fsBgp4PeerGroupName");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
    {
        PeerGroup.i4_Length = (INT4) STRLEN (pHttp->au1Value);
        if (PeerGroup.i4_Length >= (BGP_MAX_PEER_GROUP_NAME))
        {
            PeerGroup.i4_Length = BGP_MAX_PEER_GROUP_NAME - 1;
        }
        MEMCPY (PeerGroup.pu1_OctetList, pHttp->au1Value, PeerGroup.i4_Length);
        PeerGroup.pu1_OctetList[PeerGroup.i4_Length] = 0;
    }

    STRCPY (pHttp->au1Name, "fsBgp4PeerAddrType");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
        ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsBgp4PeerAddress");

    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Len = CliDotStrLength (pHttp->au1Value);
        if (i4Len == sizeof (UINT4))
        {
            IpAddress.u2Afi = BGP4_INET_AFI_IPV4;
            i4AddressFamily = IpAddress.u2Afi;
            if (0 == BGP4_INET_ATON (pHttp->au1Value, &InAddr))
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rInvalid peer address\r\n");
                return;
            }
            MEMCPY ((IpAddress.au1Address), &(InAddr.u4Addr), sizeof (UINT4));
            IpAddress.u2AddressLen = sizeof (UINT4);
        }
#ifdef BGP4_IPV6_WANTED
        else if (str_to_ip6addr (pHttp->au1Value) != NULL)
        {
            IpAddress.u2Afi = BGP4_INET_AFI_IPV6;
            i4AddressFamily = IpAddress.u2Afi;
            MEMCPY (IpAddress.au1Address,
                    str_to_ip6addr (pHttp->au1Value), BGP4_IPV6_PREFIX_LEN);
            IpAddress.u2AddressLen = BGP4_IPV6_PREFIX_LEN;
        }
#endif /* BGP4_IPV6_WANTED */

        PeerAddr.pu1_OctetList = au1PeerIpAddress;
        MEMCPY (PeerAddr.pu1_OctetList, IpAddress.au1Address,
                IpAddress.u2AddressLen);
        PeerAddr.i4_Length = IpAddress.u2AddressLen;
    }

    STRCPY (pHttp->au1Name, "ACTION");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)

    {
        if (0 == STRCMP (pHttp->au1Value, "Delete"))
        {
            i4RowStatus = 2;
            if (nmhTestv2FsBgp4PeerAddStatus
                (&u4ErrCode, &PeerGroup, i4AddressFamily, &PeerAddr,
                 i4RowStatus) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to delete entry\r\n");
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssProcessBgpPeerGroupAddPageGet (pHttp);
                return;
            }
            if (nmhSetFsBgp4PeerAddStatus
                (&PeerGroup, i4AddressFamily, &PeerAddr,
                 i4RowStatus) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to delete entry\r\n");
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssProcessBgpPeerGroupAddPageGet (pHttp);
                return;
            }
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessBgpPeerGroupAddPageGet (pHttp);
            return;
        }
        else if (0 == STRCMP (pHttp->au1Value, "ADD"))
        {
            i4RowStatus = 1;
            if (nmhTestv2FsBgp4PeerAddStatus
                (&u4ErrCode, &PeerGroup, i4AddressFamily, &PeerAddr,
                 i4RowStatus) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to Add an entry\r\n");
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssProcessBgpPeerGroupAddPageGet (pHttp);
                return;
            }
            if (nmhSetFsBgp4PeerAddStatus
                (&PeerGroup, i4AddressFamily, &PeerAddr,
                 i4RowStatus) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to Add an entry\r\n");
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssProcessBgpPeerGroupAddPageGet (pHttp);
                return;
            }
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessBgpPeerGroupAddPageGet (pHttp);
            return;
        }
    }

    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessBgpPeerGroupAddPageGet (pHttp);
    return;
}

/*********************************************************************
 *  Function Name : IssProcessBgpClearConfPage 
 *  Description   : This function processes the request coming for the
 *                  BGP Graceful Restart configuration page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpClearConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessBgpClearConfPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessBgpClearConfPageSet 
*  Description   : This function processes the SET request coming for the
*                  BGP Peer configuration page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessBgpClearConfPageSet (tHttp * pHttp)
{
    INT4                i4Len = 0;
    INT4                i4RetVal = BGP4_FAILURE;
    INT4                i4RetVal1 = BGP4_FAILURE;
    INT4                i4BgpContextId = 0;
    INT4                i4BgpPrevContextId = 0;
    INT4                i4ContextFound = 0;
    INT4                i4SupportStatus;
    UINT4               u4options = 0;
    UINT4               u4Set = 0;
    UINT4               u4Soft = ISS_BGP_HARD;
    UINT4               u4PrefixLen = 0;
    UINT4               u4AsNum = 0;
    UINT4               u4IpAddress = 0;
    UINT1               u1Dir = 0;
    UINT1               u1OrfFlag = BGP4_FALSE;
    UINT1               au1PeerGroup[BGP_MAX_PEER_GROUP_NAME] = { 0 };
    UINT1               au1BgpCxtName[VCM_ALIAS_MAX_LEN];
    tUtlInAddr          InAddr;
    tNetAddress         IpAddress;
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddr;
    tSNMP_OCTET_STRING_TYPE PeerGroup;

    MEMSET (au1BgpCxtName, 0, VCM_ALIAS_MAX_LEN);
    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    STRCPY (pHttp->au1Name, "BGP_CONTEXT");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    if ((*pHttp->au1Value != 0)
        && nmhGetFirstIndexFsMIBgpContextTable (&i4BgpContextId))
    {
        do
        {
            VcmGetAliasName ((UINT4) i4BgpContextId, au1BgpCxtName);
            if (STRCMP (au1BgpCxtName, pHttp->au1Value) == 0)
            {
                i4ContextFound++;
                if (BgpSetContext (i4BgpContextId) == SNMP_FAILURE)
                {
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Error in setting L3 Context");
                    return;
                }
                break;
            }

            i4BgpPrevContextId = i4BgpContextId;
        }
        while (nmhGetNextIndexFsMIBgpContextTable
               (i4BgpPrevContextId, &i4BgpContextId) == SNMP_SUCCESS);

        if (0 == i4ContextFound)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Bgp is not enabled in that context");
            return;
        }

    }
    else
    {
        if (BgpSetContext (i4BgpContextId) == SNMP_FAILURE)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
            return;
        }
    }

    MEMSET (&PeerGroup, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PeerRemoteAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&IpAddress, 0, sizeof (tNetAddress));
    MEMSET (&InAddr, 0, sizeof (tUtlInAddr));

    PeerGroup.pu1_OctetList = au1PeerGroup;

    STRCPY (pHttp->au1Name, "Clear_Global");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
    {
        u4options = (UINT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "fsbgp4Soft");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
    {
        u4Soft = (UINT4) ATOI (pHttp->au1Value);
        if (u4Soft == ISS_BGP_SOFT_IN)
        {
            u1Dir = ISS_BGP_OP_SOFT_IN;
        }
        else if (u4Soft == ISS_BGP_SOFT_OUT)
        {
            u1Dir = ISS_BGP_OP_SOFT_OUT;
        }
        else if (u4Soft == ISS_BGP_SOFT_BOTH)
        {
            u1Dir = ISS_BGP_OP_SOFT_BOTH;
        }
        else if (u4Soft == ISS_BGP_SOFT_IN_PREFIX_FILTER)
        {
            u1Dir = ISS_BGP_OP_SOFT_IN;
            u1OrfFlag = BGP4_TRUE;
        }
    }
    if (u4options == ISS_BGP_PEER_ADDR)
    {
        STRCPY (pHttp->au1Name, "PeerRemoteAddress");
        if (HttpGetValuebyName (pHttp->au1Name,
                                pHttp->au1Value, pHttp->au1PostQuery)
            == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            i4Len = CliDotStrLength (pHttp->au1Value);
            if (i4Len == sizeof (UINT4))
            {
                IpAddress.NetAddr.u2Afi = BGP4_INET_AFI_IPV4;
                IpAddress.u2Safi = BGP4_INET_SAFI_UNICAST;
                if (0 == BGP4_INET_ATON (pHttp->au1Value, &InAddr))
                {
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *)
                                  "\rInvalid peer address\r\n");
                    return;
                }
                MEMCPY ((IpAddress.NetAddr.au1Address), &(InAddr.u4Addr),
                        sizeof (UINT4));
                IpAddress.NetAddr.u2AddressLen = sizeof (UINT4);
            }
#ifdef BGP4_IPV6_WANTED
            else if (str_to_ip6addr (pHttp->au1Value) != NULL)
            {
                IpAddress.NetAddr.u2Afi = BGP4_INET_AFI_IPV6;
                IpAddress.u2Safi = BGP4_INET_SAFI_UNICAST;
                MEMCPY (IpAddress.NetAddr.au1Address,
                        str_to_ip6addr (pHttp->au1Value), BGP4_IPV6_PREFIX_LEN);
                IpAddress.NetAddr.u2AddressLen = BGP4_IPV6_PREFIX_LEN;
            }
#endif /* BGP4_IPV6_WANTED */

        }
    }
    else if (u4options == ISS_BGP_PEER_GR)
    {
        STRCPY (pHttp->au1Name, "PeerGroup");
        if (HttpGetValuebyName (pHttp->au1Name,
                                pHttp->au1Value, pHttp->au1PostQuery)
            == ENM_SUCCESS)
        {
            PeerGroup.i4_Length = (INT4) STRLEN (pHttp->au1Value);
            if (PeerGroup.i4_Length >= (BGP_MAX_PEER_GROUP_NAME))
            {
                PeerGroup.i4_Length = BGP_MAX_PEER_GROUP_NAME - 1;
            }
            MEMCPY (PeerGroup.pu1_OctetList, pHttp->au1Value,
                    PeerGroup.i4_Length);
            PeerGroup.pu1_OctetList[PeerGroup.i4_Length] = '\0';
        }
    }
    else if (u4options == ISS_BGP_AS_NUM)
    {
        STRCPY (pHttp->au1Name, "AsNum");
        if (HttpGetValuebyName (pHttp->au1Name,
                                pHttp->au1Value, pHttp->au1PostQuery)
            == ENM_SUCCESS)
        {
            u4AsNum = (UINT4) ATOI (pHttp->au1Value);
            nmhGetFsbgp4FourByteASNSupportStatus (&i4SupportStatus);
            if ((i4SupportStatus == BGP4_DISABLE_4BYTE_ASN_SUPPORT) &&
                (u4AsNum > BGP4_MAX_TWO_BYTE_AS))
            {
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "4-byte ASN Capability is disabled. Configure value < 65536");
                IssProcessBgpContextCreationPageGet (pHttp);
                return;
            }

        }
    }
    else if (u4options == ISS_BGP_FLAP_STAT)
    {
        STRCPY (pHttp->au1Name, "flapNetwork");
        if (HttpGetValuebyName (pHttp->au1Name,
                                pHttp->au1Value, pHttp->au1PostQuery)
            == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            i4Len = CliDotStrLength (pHttp->au1Value);
            if (i4Len == sizeof (UINT4))
            {
                IpAddress.NetAddr.u2Afi = BGP4_INET_AFI_IPV4;
                IpAddress.u2Safi = BGP4_INET_SAFI_UNICAST;
                if (0 == BGP4_INET_ATON (pHttp->au1Value, &InAddr))
                {
                    IssSendError (pHttp, (CONST INT1 *)
                                  "\rInvalid Network address\r\n");
                    return;
                }
                MEMCPY ((IpAddress.NetAddr.au1Address), &(InAddr.u4Addr),
                        sizeof (UINT4));
                IpAddress.NetAddr.u2AddressLen = sizeof (UINT4);
            }
#ifdef BGP4_IPV6_WANTED
            else if (str_to_ip6addr (pHttp->au1Value) != NULL)
            {
                IpAddress.NetAddr.u2Afi = BGP4_INET_AFI_IPV6;
                IpAddress.u2Safi = BGP4_INET_SAFI_UNICAST;
                MEMCPY (IpAddress.NetAddr.au1Address,
                        str_to_ip6addr (pHttp->au1Value), BGP4_IPV6_PREFIX_LEN);
                IpAddress.NetAddr.u2AddressLen = BGP4_IPV6_PREFIX_LEN;
            }
#endif /* BGP4_IPV6_WANTED */

        }
        else
        {
            u4Set = 1;
        }
        STRCPY (pHttp->au1Name, "flapPrefixLength");
        if (HttpGetValuebyName (pHttp->au1Name,
                                pHttp->au1Value, pHttp->au1PostQuery)
            == ENM_SUCCESS)
        {
            u4PrefixLen = (UINT4) ATOI (pHttp->au1Value);
            IpAddress.u2PrefixLen = (UINT2) u4PrefixLen;
        }
    }
    else if (u4options == ISS_BGP_DAMP)
    {
        STRCPY (pHttp->au1Name, "dampNetwork");
        if (HttpGetValuebyName (pHttp->au1Name,
                                pHttp->au1Value, pHttp->au1PostQuery)
            == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            i4Len = CliDotStrLength (pHttp->au1Value);
            if (i4Len == sizeof (UINT4))
            {
                IpAddress.NetAddr.u2Afi = BGP4_INET_AFI_IPV4;
                IpAddress.u2Safi = BGP4_INET_SAFI_UNICAST;
                if (0 == BGP4_INET_ATON (pHttp->au1Value, &InAddr))
                {
                    IssSendError (pHttp, (CONST INT1 *)
                                  "\rInvalid Network address\r\n");
                    return;
                }
                MEMCPY ((IpAddress.NetAddr.au1Address), &(InAddr.u4Addr),
                        sizeof (UINT4));
                IpAddress.NetAddr.u2AddressLen = sizeof (UINT4);
            }
#ifdef BGP4_IPV6_WANTED
            else if (str_to_ip6addr (pHttp->au1Value) != NULL)
            {
                IpAddress.NetAddr.u2Afi = BGP4_INET_AFI_IPV6;
                IpAddress.u2Safi = BGP4_INET_SAFI_UNICAST;
                MEMCPY (IpAddress.NetAddr.au1Address,
                        str_to_ip6addr (pHttp->au1Value), BGP4_IPV6_PREFIX_LEN);
                IpAddress.NetAddr.u2AddressLen = BGP4_IPV6_PREFIX_LEN;
            }
#endif /* BGP4_IPV6_WANTED */

        }
        else
        {
            u4Set = 1;
        }
        STRCPY (pHttp->au1Name, "dampPrefixLength");
        if (HttpGetValuebyName (pHttp->au1Name,
                                pHttp->au1Value, pHttp->au1PostQuery)
            == ENM_SUCCESS)
        {
            u4PrefixLen = (UINT4) ATOI (pHttp->au1Value);
            IpAddress.u2PrefixLen = (UINT2) u4PrefixLen;
        }
    }

    if (u4Soft == ISS_BGP_HARD)
    {
        if (u4options == ISS_BGP_IPV4)
        {
            i4RetVal =
                Bgp4ConfigClearIpBgp (BGP4_AS_NO_CHECK, 0, BGP4_INET_AFI_IPV4,
                                      BGP4_CLEAR_GROUP1);
            if (i4RetVal == BGP4_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rNo Peer is configured\r\n");
                return;
            }
        }
        else if (u4options == ISS_BGP_IPV6)
        {
            i4RetVal =
                Bgp4ConfigClearIpBgp (BGP4_AS_NO_CHECK, 0, BGP4_INET_AFI_IPV6,
                                      BGP4_CLEAR_GROUP1);
            if (i4RetVal == BGP4_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rNo Peer is configured\r\n");
                return;
            }
        }
        else if (u4options == ISS_BGP_ALL)
        {
            i4RetVal =
                Bgp4ConfigClearIpBgp (BGP4_AS_NO_CHECK, 0, BGP4_INET_AFI_IPV4,
                                      BGP4_CLEAR_GROUP1);
            i4RetVal1 =
                Bgp4ConfigClearIpBgp (BGP4_AS_NO_CHECK, 0, BGP4_INET_AFI_IPV6,
                                      BGP4_CLEAR_GROUP1);
            if ((i4RetVal == BGP4_FAILURE) && (i4RetVal1 == BGP4_FAILURE))
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rNo Peer is configured\r\n");
                return;
            }
        }
        else if (u4options == ISS_BGP_EXTERNAL)
        {
            i4RetVal =
                Bgp4ConfigClearIpBgp (BGP4_AS_CHECK, 0, BGP4_INET_AFI_IPV4,
                                      BGP4_CLEAR_GROUP1);
            i4RetVal1 =
                Bgp4ConfigClearIpBgp (BGP4_AS_CHECK, 0, BGP4_INET_AFI_IPV6,
                                      BGP4_CLEAR_GROUP1);
            if ((i4RetVal == BGP4_FAILURE) && (i4RetVal1 == BGP4_FAILURE))
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rNo Peer is configured\r\n");
                return;
            }
        }
        else if (u4options == ISS_BGP_AS_NUM)
        {
            i4RetVal =
                Bgp4ConfigClearIpBgp (BGP4_AS_CHECK, u4AsNum,
                                      BGP4_INET_AFI_IPV4, BGP4_CLEAR_GROUP3);
            i4RetVal1 =
                Bgp4ConfigClearIpBgp (BGP4_AS_CHECK, u4AsNum,
                                      BGP4_INET_AFI_IPV6, BGP4_CLEAR_GROUP3);

            if ((i4RetVal == BGP4_FAILURE) && (i4RetVal1 == BGP4_FAILURE))
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rNo Peer is configured\r\n");
                return;
            }
        }
        else if (u4options == ISS_BGP_PEER_ADDR)
        {
            if (IpAddress.NetAddr.u2Afi == BGP4_INET_AFI_IPV4)
            {
                u4IpAddress = OSIX_NTOHL (InAddr.u4Addr);
                MEMCPY (IpAddress.NetAddr.au1Address, &u4IpAddress,
                        sizeof (UINT4));
            }
            i4RetVal = Bgp4UtilNeighborClearIpBgp (&IpAddress);
            if (i4RetVal == BGP4_CLEAR_PEER_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "\rPeer Doesn't exist\r\n");
                return;
            }
            else if ((i4RetVal == BGP4_CLEAR_FAILURE) ||
                     (i4RetVal == BGP4_FAILURE))
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to trigger Clear ip bgp\r\n");
                return;
            }
        }
        else if (u4options == ISS_BGP_DAMP)
        {
            if (u4Set != 1)
            {
                if (IpAddress.NetAddr.u2Afi == BGP4_INET_AFI_IPV4)
                {
                    u4IpAddress = OSIX_NTOHL (InAddr.u4Addr);
                    MEMCPY (IpAddress.NetAddr.au1Address, &u4IpAddress,
                            sizeof (UINT4));
                }
                i4RetVal = Bgp4UtilClearRouteDampening (IpAddress, 1);
            }
            else
            {
                i4RetVal = Bgp4UtilClearRouteDampening (IpAddress, 0);
            }
            if (i4RetVal == BGP4_CLEAR_RIB_LOCK_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "\rRib lock failed\r\n");
                return;
            }
            else if (i4RetVal == BGP4_CLEAR_RIB_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rRoute not present in RIB\r\n");
                return;
            }
            else if (i4RetVal == BGP4_CLEAR_RFD_DIS_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rRFD is not enabled for the Router\r\n");
                return;
            }
            else if (i4RetVal == BGP4_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to trigger Clear ip bgp\r\n");
                return;
            }
        }
        else if (u4options == ISS_BGP_FLAP_STAT)
        {
            if (u4Set != 1)
            {
                if (IpAddress.NetAddr.u2Afi == BGP4_INET_AFI_IPV4)
                {
                    u4IpAddress = OSIX_NTOHL (InAddr.u4Addr);
                    MEMCPY (IpAddress.NetAddr.au1Address, &u4IpAddress,
                            sizeof (UINT4));
                }
                i4RetVal = Bgp4UtilClearRouteFlapStats (IpAddress, 1);
            }
            else
            {
                i4RetVal = Bgp4UtilClearRouteFlapStats (IpAddress, 0);
            }
            if (i4RetVal == BGP4_CLEAR_RIB_LOCK_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "\rRib lock failed\r\n");
                return;
            }
            else if (i4RetVal == BGP4_CLEAR_RIB_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rRoute not present in RIB\r\n");
                return;
            }
            else if (i4RetVal == BGP4_CLEAR_RFD_DIS_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rRFD is not enabled for the Router\r\n");
                return;
            }
            else if (i4RetVal == BGP4_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to trigger Clear ip bgp\r\n");
                return;
            }
        }
        else if (u4options == ISS_BGP_PEER_GR)
        {
            if (Bgp4ValidatePeerGroupName (au1PeerGroup) == BGP4_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "\rInvalid Peer\r\n");
                return;
            }
            else
            {
                i4RetVal = Bgp4UtilClearPeerGroup (au1PeerGroup, u1Dir, 0, 0);
            }
            if (i4RetVal == BGP4_CLEAR_PEER_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rInvalid peer group name\r\n");
                return;
            }
            else if ((i4RetVal == BGP4_FAILURE) ||
                     (i4RetVal == BGP4_CLEAR_SET_FAILURE))
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to trigger Clear ip bgp\r\n");
                return;
            }
        }
    }
    else
    {
        if (u4options == ISS_BGP_IPV4)
        {
            i4RetVal =
                Bgp4UtilNeighborClearIpBgpSoftAll (BGP4_AS_NO_CHECK, 0,
                                                   BGP4_INET_AFI_IPV4, u1Dir,
                                                   u1OrfFlag);
            if (i4RetVal == BGP4_CLEAR_PEER_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "\rPeer Doesn't exist\r\n");
                return;
            }
            else if ((i4RetVal == BGP4_CLEAR_SET_FAILURE) ||
                     (i4RetVal == BGP4_FAILURE))
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to trigger Clear ip bgp\r\n");
                return;
            }
        }
        else if (u4options == ISS_BGP_IPV6)
        {
            i4RetVal =
                Bgp4UtilNeighborClearIpBgpSoftAll (BGP4_AS_NO_CHECK, 0,
                                                   BGP4_INET_AFI_IPV6, u1Dir,
                                                   u1OrfFlag);
            if (i4RetVal == BGP4_CLEAR_PEER_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "\rPeer Doesn't exist\r\n");
                return;
            }
            else if ((i4RetVal == BGP4_CLEAR_SET_FAILURE) ||
                     (i4RetVal == BGP4_FAILURE))
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to trigger Clear ip bgp\r\n");
                return;
            }
        }
        else if (u4options == ISS_BGP_ALL)
        {
            i4RetVal =
                Bgp4UtilNeighborClearIpBgpSoftAll (BGP4_AS_NO_CHECK, 0,
                                                   BGP4_INET_AFI_IPV4, u1Dir,
                                                   u1OrfFlag);
            i4RetVal1 =
                Bgp4UtilNeighborClearIpBgpSoftAll (BGP4_AS_NO_CHECK, 0,
                                                   BGP4_INET_AFI_IPV6, u1Dir,
                                                   u1OrfFlag);

            if ((i4RetVal == BGP4_CLEAR_PEER_FAILURE) &&
                (i4RetVal1 == BGP4_CLEAR_PEER_FAILURE))
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "\rPeer Doesn't exist\r\n");
                return;
            }
            else if ((i4RetVal == BGP4_CLEAR_SET_FAILURE) ||
                     (i4RetVal1 == BGP4_CLEAR_SET_FAILURE) ||
                     ((i4RetVal == BGP4_FAILURE) &&
                      (i4RetVal1 == BGP4_FAILURE)))
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to trigger Clear ip bgp\r\n");
                return;
            }
        }
        else if (u4options == ISS_BGP_EXTERNAL)
        {
            i4RetVal =
                Bgp4UtilNeighborClearIpBgpSoftAll (BGP4_AS_CHECK, 0,
                                                   BGP4_INET_AFI_IPV4, u1Dir,
                                                   u1OrfFlag);
            i4RetVal1 =
                Bgp4UtilNeighborClearIpBgpSoftAll (BGP4_AS_CHECK, 0,
                                                   BGP4_INET_AFI_IPV6, u1Dir,
                                                   u1OrfFlag);
            if ((i4RetVal == BGP4_CLEAR_PEER_FAILURE)
                && (i4RetVal1 == BGP4_CLEAR_PEER_FAILURE))
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "\rPeer Doesn't exist\r\n");
                return;
            }
            else if ((i4RetVal == BGP4_CLEAR_SET_FAILURE) ||
                     (i4RetVal1 == BGP4_CLEAR_SET_FAILURE) ||
                     ((i4RetVal == BGP4_FAILURE) &&
                      (i4RetVal1 == BGP4_FAILURE)))
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to trigger Clear ip bgp\r\n");
                return;
            }
        }
        else if (u4options == ISS_BGP_AS_NUM)
        {
            i4RetVal =
                Bgp4UtilNeighborClearIpBgpSoftAll (BGP4_AS_CHECK, u4AsNum,
                                                   BGP4_INET_AFI_IPV4, u1Dir,
                                                   u1OrfFlag);
            i4RetVal1 =
                Bgp4UtilNeighborClearIpBgpSoftAll (BGP4_AS_CHECK, u4AsNum,
                                                   BGP4_INET_AFI_IPV6, u1Dir,
                                                   u1OrfFlag);
            if ((i4RetVal == BGP4_CLEAR_PEER_FAILURE)
                && (i4RetVal1 == BGP4_CLEAR_PEER_FAILURE))
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "\rPeer Doesn't exist\r\n");
                return;
            }
            else if ((i4RetVal == BGP4_CLEAR_SET_FAILURE) ||
                     (i4RetVal1 == BGP4_CLEAR_SET_FAILURE) ||
                     ((i4RetVal == BGP4_FAILURE) &&
                      (i4RetVal1 == BGP4_FAILURE)))
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to trigger Clear ip bgp\r\n");
                return;
            }
        }
        else if (u4options == ISS_BGP_PEER_ADDR)
        {
            i4RetVal =
                Bgp4UtilNeighborSoftReconfigClearIpBgp (u1Dir,
                                                        &IpAddress,
                                                        IpAddress.NetAddr.
                                                        u2Afi, u1OrfFlag);
            if (i4RetVal == BGP4_CLEAR_PEER_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "\rPeer Doesn't exist\r\n");
                return;
            }
            else if ((i4RetVal == BGP4_CLEAR_SET_FAILURE) ||
                     (i4RetVal == BGP4_FAILURE))
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to trigger Clear ip bgp\r\n");
                return;
            }
        }
        else if (u4options == ISS_BGP_PEER_GR)
        {
            if (Bgp4ValidatePeerGroupName (au1PeerGroup) == BGP4_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "\rInValid peer\r\n");
                return;
            }
            else
            {
                i4RetVal =
                    Bgp4UtilClearPeerGroup (au1PeerGroup, BGP4_CLEAR_SOFT,
                                            u1Dir, u1OrfFlag);
            }

            if (i4RetVal == BGP4_CLEAR_PEER_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rInvalid peer group name\r\n");
                return;
            }
            else if ((i4RetVal == BGP4_FAILURE) ||
                     (i4RetVal == BGP4_CLEAR_SET_FAILURE))
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to trigger Clear ip bgp\r\n");
                return;
            }
        }
    }
    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessGet (pHttp);
    return;

}

/*********************************************************************
 *  Function Name : IssProcessBgpOrfConfPage 
 *  Description   : This function processes the request coming for the
 *                  BGP Orf configuration page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpOrfConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessBgpOrfConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessBgpOrfConfPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessBgpOrfConfPageSet 
*  Description   : This function processes the SET request coming for the
*                  BGP Orf configuration page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessBgpOrfConfPageSet (tHttp * pHttp)
{
    INT4                i4Len = 0;
    UINT4               u4Context = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4TmpAddr = 0;
    tUtlInAddr          InAddr;
    tAddrPrefix         IpAddress;
    UINT1               au1IpPrefixNameIn[RMAP_MAX_NAME_LEN + 4];
    UINT1               au1IpPrefixNameOut[RMAP_MAX_NAME_LEN + 4];
    UINT1               au1PeerIpAddress[ISS_BGP4_IP_MAX_LENGTH];
    tSNMP_OCTET_STRING_TYPE IpPrefixNameIn;
    tSNMP_OCTET_STRING_TYPE IpPrefixNameOut;
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    UINT1               u1SendModeStatus = 0;
    UINT1               u1RcvModeStatus = 0;
    UINT1               u1OrfType = 0;
    INT1                i1RetVal = 0;

    MEMSET (&IpAddress, 0, sizeof (tAddrPrefix));
    MEMSET (&InAddr, 0, sizeof (tUtlInAddr));
    MEMSET (&PeerAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&IpPrefixNameIn, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&IpPrefixNameOut, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1PeerIpAddress, 0, sizeof (au1PeerIpAddress));
    MEMSET (au1IpPrefixNameIn, 0, sizeof (au1IpPrefixNameIn));
    MEMSET (au1IpPrefixNameOut, 0, sizeof (au1IpPrefixNameOut));

    IpPrefixNameIn.pu1_OctetList = au1IpPrefixNameIn;
    IpPrefixNameOut.pu1_OctetList = au1IpPrefixNameOut;

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    STRCPY (pHttp->au1Name, "BgpVrfName");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    VcmIsVrfExist (pHttp->au1Value, (UINT4 *) &u4Context);

    STRCPY (pHttp->au1Name, "BgpPeerAddr");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Len = CliDotStrLength (pHttp->au1Value);
        if (i4Len == sizeof (UINT4))
        {
            IpAddress.u2Afi = BGP4_INET_AFI_IPV4;
            if (0 == BGP4_INET_ATON (pHttp->au1Value, &InAddr))
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rInvalid peer address\r\n");
                return;
            }
            MEMCPY ((IpAddress.au1Address), &(InAddr.u4Addr), sizeof (UINT4));
            IpAddress.u2AddressLen = sizeof (UINT4);
        }
#ifdef BGP4_IPV6_WANTED
        else if (str_to_ip6addr (pHttp->au1Value) != NULL)
        {
            IpAddress.u2Afi = BGP4_INET_AFI_IPV6;
            MEMCPY (IpAddress.au1Address,
                    str_to_ip6addr (pHttp->au1Value), BGP4_IPV6_PREFIX_LEN);
            IpAddress.u2AddressLen = BGP4_IPV6_PREFIX_LEN;
        }
#endif /* BGP4_IPV6_WANTED */
        PeerAddr.pu1_OctetList = au1PeerIpAddress;
        MEMCPY (PeerAddr.pu1_OctetList, IpAddress.au1Address,
                IpAddress.u2AddressLen);
        PeerAddr.i4_Length = IpAddress.u2AddressLen;
    }

    if (BgpSetContext ((INT4) u4Context) == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
        return;
    }

    STRCPY (pHttp->au1Name, "BgpOrfType");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
    {
        if (STRCMP (pHttp->au1Value, "Address-Prefix") == 0)
        {
            u1OrfType = BGP_ORF_TYPE_ADDRESS_PREFIX;
        }
    }

    STRCPY (pHttp->au1Name, "BgpOrfSendModeStatus");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery) ==
        ENM_SUCCESS)
    {
        u1SendModeStatus = (UINT1) ATOI (pHttp->au1Value);
    }

    if (IpAddress.u2Afi == BGP4_INET_AFI_IPV4)
    {
        MEMCPY (&u4TmpAddr, IpAddress.au1Address, BGP4_IPV4_PREFIX_LEN);
        u4TmpAddr = OSIX_HTONL (u4TmpAddr);
        MEMCPY (&IpAddress.au1Address, &u4TmpAddr, BGP4_IPV4_PREFIX_LEN);
    }
    if (u1SendModeStatus == BGP4_TRUE)
    {
        if (Bgp4SnmphOrfCapabilityEnable (u4Context, IpAddress, IpAddress.u2Afi,
                                          BGP4_INET_SAFI_UNICAST, u1OrfType,
                                          BGP4_CLI_ORF_MODE_SEND)
            == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessBgpOrfConfPageGet (pHttp);
            return;
        }
    }
    else
    {
        if (Bgp4SnmphOrfCapabilityDisable
            (u4Context, IpAddress, IpAddress.u2Afi, BGP4_INET_SAFI_UNICAST,
             u1OrfType, BGP4_CLI_ORF_MODE_SEND) == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessBgpOrfConfPageGet (pHttp);
            return;
        }
    }

    STRCPY (pHttp->au1Name, "BgpOrfReceiveModeStatus");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery) ==
        ENM_SUCCESS)
    {
        u1RcvModeStatus = (UINT1) ATOI (pHttp->au1Value);
    }
    if (u1RcvModeStatus == BGP4_TRUE)
    {
        if (Bgp4SnmphOrfCapabilityEnable (u4Context, IpAddress, IpAddress.u2Afi,
                                          BGP4_INET_SAFI_UNICAST, u1OrfType,
                                          BGP4_CLI_ORF_MODE_RECEIVE)
            == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessBgpOrfConfPageGet (pHttp);
            return;
        }
    }
    else
    {
        if (Bgp4SnmphOrfCapabilityDisable
            (u4Context, IpAddress, IpAddress.u2Afi, BGP4_INET_SAFI_UNICAST,
             u1OrfType, BGP4_CLI_ORF_MODE_RECEIVE) == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessBgpOrfConfPageGet (pHttp);
            return;
        }
    }

    STRCPY (pHttp->au1Name, "BgpInPrefixList");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
    {
        IpPrefixNameIn.i4_Length = (INT4) STRLEN (pHttp->au1Value);
        if (IpPrefixNameIn.i4_Length >= (RMAP_MAX_NAME_LEN + 4))
        {
            IpPrefixNameIn.i4_Length = RMAP_MAX_NAME_LEN + 3;
        }
        MEMCPY (IpPrefixNameIn.pu1_OctetList, pHttp->au1Value,
                IpPrefixNameIn.i4_Length);
        IpPrefixNameIn.pu1_OctetList[IpPrefixNameIn.i4_Length] = 0;
    }

    i1RetVal = nmhTestv2Fsbgp4mpePeerIpPrefixNameIn (&u4ErrorCode,
                                                     (INT4) IpAddress.u2Afi,
                                                     &PeerAddr,
                                                     &IpPrefixNameIn);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid In Prefix list!\r\n");
        return;
    }

    i1RetVal =
        nmhSetFsbgp4mpePeerIpPrefixNameIn ((INT4) IpAddress.u2Afi, &PeerAddr,
                                           &IpPrefixNameIn);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid In Prefix list!\r\n");
        return;
    }

    STRCPY (pHttp->au1Name, "BgpOutPrefixList");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
    {
        IpPrefixNameOut.i4_Length = (INT4) STRLEN (pHttp->au1Value);
        if (IpPrefixNameOut.i4_Length >= (RMAP_MAX_NAME_LEN + 4))
        {
            IpPrefixNameOut.i4_Length = RMAP_MAX_NAME_LEN + 3;
        }
        MEMCPY (IpPrefixNameOut.pu1_OctetList, pHttp->au1Value,
                IpPrefixNameOut.i4_Length);
        IpPrefixNameOut.pu1_OctetList[IpPrefixNameOut.i4_Length] = 0;
    }

    i1RetVal = nmhTestv2Fsbgp4mpePeerIpPrefixNameOut (&u4ErrorCode,
                                                      (INT4) IpAddress.u2Afi,
                                                      &PeerAddr,
                                                      &IpPrefixNameOut);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid Out Prefix list!\r\n");
        return;
    }

    i1RetVal =
        nmhSetFsbgp4mpePeerIpPrefixNameOut ((INT4) IpAddress.u2Afi, &PeerAddr,
                                            &IpPrefixNameOut);
    if (i1RetVal == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "\r Invalid Out Prefix list!\r\n");
        return;
    }

    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessBgpOrfConfPageGet (pHttp);
    return;
}

/*********************************************************************
*  Function Name : IssProcessBgpOrfConfPageGet 
*  Description   : This function processes the GET request coming for the
*                  BGP Orf configuration page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessBgpOrfConfPageGet (tHttp * pHttp)
{
    UINT4               u4IpAddress = 0;
    INT4                i4BgpContextId = 0;
    INT4                i4BgpPrevContextId = 0;
    INT4                i4AddrType = 0;
    INT4                i4Temp = 0;
    INT4                i4Status = 0;
    UINT1              *pu1TempString = NULL;
#ifdef BGP4_IPV6_WANTED
    UINT1               au1Ip6Addr[ISS_BGP4_IP6_ADDR_STRING_LEN];
#endif
    UINT1               au1LocalIpAddress[ISS_BGP4_IP_MAX_LENGTH];
    UINT1               au1OrfCapValue[12];
    UINT1               au1IpPrefixName[RMAP_MAX_NAME_LEN + 4];
    tSNMP_OCTET_STRING_TYPE PeerRemoteAddr;
    tSNMP_OCTET_STRING_TYPE OrfCapValue;
    tSNMP_OCTET_STRING_TYPE IpPrefixName;
    UINT1               au1BgpCxtName[VCM_ALIAS_MAX_LEN];

    MEMSET (au1BgpCxtName, 0, VCM_ALIAS_MAX_LEN);

    MEMSET (au1IpPrefixName, 0, sizeof (au1IpPrefixName));
    MEMSET (au1OrfCapValue, 0, sizeof (au1OrfCapValue));
    MEMSET (au1LocalIpAddress, 0, ISS_BGP4_IP_MAX_LENGTH);
#ifdef BGP4_IPV6_WANTED
    MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
#endif

    MEMSET (&OrfCapValue, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&IpPrefixName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    OrfCapValue.pu1_OctetList = au1OrfCapValue;
    IpPrefixName.pu1_OctetList = au1IpPrefixName;

    PeerRemoteAddr.pu1_OctetList = au1LocalIpAddress;

    pHttp->i4Write = 0;

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    if (nmhGetFirstIndexFsMIBgpContextTable (&i4BgpContextId) == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    i4Temp = pHttp->i4Write;

    do
    {
        pHttp->i4Write = i4Temp;

        if (BgpSetContext (i4BgpContextId) == SNMP_FAILURE)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
            return;
        }

        if (nmhGetFirstIndexFsbgp4MpePeerExtTable (&i4AddrType,
                                                   &PeerRemoteAddr) ==
            SNMP_FAILURE)
        {
            i4BgpPrevContextId = i4BgpContextId;

            if (nmhGetNextIndexFsMIBgpContextTable (i4BgpPrevContextId,
                                                    &i4BgpContextId) !=
                SNMP_SUCCESS)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
                WebnmSockWrite (pHttp,
                                (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                                (pHttp->i4HtmlSize - pHttp->i4Write));
                return;
            }
            continue;
        }

        /*Process the BGP peer table */
        do
        {
            pHttp->i4Write = i4Temp;

            STRCPY (pHttp->au1KeyString, "BgpPeerAddr_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (i4AddrType == BGP4_INET_AFI_IPV4)
            {
                MEMCPY (&u4IpAddress,
                        PeerRemoteAddr.pu1_OctetList, PeerRemoteAddr.i4_Length);
                WEB_CONVERT_IPADDR_TO_STR (pu1TempString,
                                           OSIX_NTOHL (u4IpAddress));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1TempString);
            }
#ifdef BGP4_IPV6_WANTED
            else                /* BGP ipv6 processing */
            {
                Bgp4PrintIp6Addr (PeerRemoteAddr.pu1_OctetList, (au1Ip6Addr));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Ip6Addr);
                MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
            }
#endif /* BGP4_IPV6_WANTED */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "BgpPeerOrfType_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Address-Prefix");
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            /* ORF Send  mode Announced status */
            OrfCapValue.pu1_OctetList[1] = (UINT1) i4AddrType;
            OrfCapValue.pu1_OctetList[3] = BGP4_INET_SAFI_UNICAST;    /* Safi */
            OrfCapValue.pu1_OctetList[4] = 1;    /* Number of ORFs */
            OrfCapValue.pu1_OctetList[5] = BGP_ORF_TYPE_ADDRESS_PREFIX;    /* Orf type */
            OrfCapValue.pu1_OctetList[6] = BGP4_CLI_ORF_MODE_SEND;    /* Orf Mode */

            OrfCapValue.i4_Length = BGP_ORF_CAPS_LEN;
            nmhGetFsbgp4mpeCapAnnouncedStatus (i4AddrType, &PeerRemoteAddr,
                                               CAP_CODE_ORF, BGP_ORF_CAPS_LEN,
                                               &OrfCapValue, &i4Status);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Status);
            STRCPY (pHttp->au1KeyString, "BgpOrfSendModeStatus_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            /* ORF Receive mode Announced status */
            i4Status = 0;
            OrfCapValue.pu1_OctetList[6] = BGP4_CLI_ORF_MODE_RECEIVE;    /* Orf Mode */
            nmhGetFsbgp4mpeCapAnnouncedStatus (i4AddrType, &PeerRemoteAddr,
                                               CAP_CODE_ORF, BGP_ORF_CAPS_LEN,
                                               &OrfCapValue, &i4Status);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Status);
            STRCPY (pHttp->au1KeyString, "BgpOrfReceiveModeStatus_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            /* ORF Send mode Received status */
            i4Status = 0;
            OrfCapValue.pu1_OctetList[6] = BGP4_CLI_ORF_MODE_SEND;    /* Orf Mode */
            nmhGetFsbgp4mpeCapReceivedStatus (i4AddrType, &PeerRemoteAddr,
                                              CAP_CODE_ORF, BGP_ORF_CAPS_LEN,
                                              &OrfCapValue, &i4Status);
            if (i4Status == BGP4_TRUE)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Received");
            }
            else
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Not Received");
            }
            STRCPY (pHttp->au1KeyString, "BgpOrfSendModeRxStatus_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            /* ORF Receive mode Received status */
            i4Status = 0;
            OrfCapValue.pu1_OctetList[6] = BGP4_CLI_ORF_MODE_RECEIVE;    /* Orf Mode */
            nmhGetFsbgp4mpeCapReceivedStatus (i4AddrType, &PeerRemoteAddr,
                                              CAP_CODE_ORF, BGP_ORF_CAPS_LEN,
                                              &OrfCapValue, &i4Status);
            if (i4Status == BGP4_TRUE)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Received");
            }
            else if (i4Status == BGP4_FALSE)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Not Received");
            }
            STRCPY (pHttp->au1KeyString, "BgpOrfReceiveModeRxStatus_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            /* In Prefix Name */
            nmhGetFsbgp4mpePeerIpPrefixNameIn (i4AddrType, &PeerRemoteAddr,
                                               &IpPrefixName);

            STRCPY (pHttp->au1KeyString, "BgpInPrefixList_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     IpPrefixName.pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            /* Out Prefix Name */
            MEMSET (au1IpPrefixName, 0, sizeof (au1IpPrefixName));
            IpPrefixName.i4_Length = 0;
            nmhGetFsbgp4mpePeerIpPrefixNameOut (i4AddrType, &PeerRemoteAddr,
                                                &IpPrefixName);

            STRCPY (pHttp->au1KeyString, "BgpOutPrefixList_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     IpPrefixName.pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "BgpVrfName_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            VcmGetAliasName ((UINT4) i4BgpContextId, au1BgpCxtName);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1BgpCxtName);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        }
        while (nmhGetNextIndexFsbgp4MpePeerExtTable (i4AddrType, &i4AddrType, &PeerRemoteAddr, &PeerRemoteAddr) == SNMP_SUCCESS);    /*end of do-while */

        i4BgpPrevContextId = i4BgpContextId;

    }
    while (nmhGetNextIndexFsMIBgpContextTable
           (i4BgpPrevContextId, &i4BgpContextId) == SNMP_SUCCESS);

    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
 *  Function Name : IssProcessBgpOrfFiltersPage 
 *  Description   : This function processes the request coming for the
 *                  BGP Orf filters page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessBgpOrfFiltersPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessBgpOrfFiltersPageGet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessBgpOrfFiltersPageGet 
*  Description   : This function processes the GET request coming for the
*                  BGP Orf filter page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessBgpOrfFiltersPageGet (tHttp * pHttp)
{
    UINT1              *pu1TempString = NULL;
    INT4                i4AddrType = 0;
    INT4                i4Afi = 0;
    INT4                i4Safi = 0;
    INT4                i4Action = 0;
    INT4                i4BgpContextId = 0;
    INT4                i4BgpPrevContextId = 0;
    INT4                i4Temp = 0;
    UINT4               u4AddrPrefixLen = 0;
    UINT4               u4MinLen = 0;
    UINT4               u4MaxLen = 0;
    UINT4               u4OrfType = 0;
    UINT4               u4SeqNo = 0;
    UINT4               u4IpAddress = 0;
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    tSNMP_OCTET_STRING_TYPE AddrPrefix;
    UINT1               au1PeerAddress[ISS_BGP4_IP_MAX_LENGTH];
    UINT1               au1AddrPrefix[ISS_BGP4_IP_MAX_LENGTH];
    UINT1               au1BgpCxtName[VCM_ALIAS_MAX_LEN];
#ifdef BGP4_IPV6_WANTED
    UINT1               au1Ip6Addr[ISS_BGP4_IP6_ADDR_STRING_LEN];
#endif

    MEMSET (&PeerAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&AddrPrefix, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1PeerAddress, 0, sizeof (au1PeerAddress));
    MEMSET (au1AddrPrefix, 0, sizeof (au1AddrPrefix));
    MEMSET (au1BgpCxtName, 0, VCM_ALIAS_MAX_LEN);
#ifdef BGP4_IPV6_WANTED
    MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
#endif

    PeerAddr.pu1_OctetList = au1PeerAddress;
    AddrPrefix.pu1_OctetList = au1AddrPrefix;

    pHttp->i4Write = 0;

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    if (nmhGetFirstIndexFsMIBgpContextTable (&i4BgpContextId) == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    i4Temp = pHttp->i4Write;

    do
    {
        pHttp->i4Write = i4Temp;

        if (BgpSetContext (i4BgpContextId) == SNMP_FAILURE)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
            return;
        }

        if (nmhGetFirstIndexFsBgp4ORFListTable
            (&i4AddrType, &PeerAddr, &i4Afi, &i4Safi, &u4OrfType, &u4SeqNo,
             &AddrPrefix, &u4AddrPrefixLen, &u4MaxLen, &u4MinLen,
             &i4Action) == SNMP_FAILURE)
        {
            i4BgpPrevContextId = i4BgpContextId;

            if (nmhGetNextIndexFsMIBgpContextTable (i4BgpPrevContextId,
                                                    &i4BgpContextId) !=
                SNMP_SUCCESS)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
                WebnmSockWrite (pHttp,
                                (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                                (pHttp->i4HtmlSize - pHttp->i4Write));
                return;
            }
            continue;
        }
        do
        {
            pHttp->i4Write = i4Temp;

            STRCPY (pHttp->au1KeyString, "BgpOrfPeerAddr_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (i4AddrType == BGP4_INET_AFI_IPV4)
            {
                MEMCPY (&u4IpAddress,
                        PeerAddr.pu1_OctetList, PeerAddr.i4_Length);
                WEB_CONVERT_IPADDR_TO_STR (pu1TempString, u4IpAddress);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1TempString);
            }
#ifdef BGP4_IPV6_WANTED
            else                /* BGP ipv6 processing */
            {
                Bgp4PrintIp6Addr (PeerAddr.pu1_OctetList, (au1Ip6Addr));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Ip6Addr);
                MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
            }
#endif /* BGP4_IPV6_WANTED */

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "BgpOrfSequenceNumber_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4SeqNo);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "BgpOrfAction_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (i4Action == BGP4_ORF_PERMIT)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Permit");
            }
            else if (i4Action == BGP4_ORF_DENY)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Deny");
            }
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "BgpOrfAddrPrefix_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (i4Afi == BGP4_INET_AFI_IPV4)
            {
                MEMCPY (&u4IpAddress,
                        AddrPrefix.pu1_OctetList, AddrPrefix.i4_Length);
                WEB_CONVERT_IPADDR_TO_STR (pu1TempString, u4IpAddress);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1TempString);
            }
#ifdef BGP4_IPV6_WANTED
            else                /* BGP ipv6 processing */
            {
                Bgp4PrintIp6Addr (AddrPrefix.pu1_OctetList, (au1Ip6Addr));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Ip6Addr);
                MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
            }
#endif /* BGP4_IPV6_WANTED */

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "BgpOrfPrefixLength_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4AddrPrefixLen);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "BgpOrfMinPrefixLen_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4MinLen);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "BgpOrfMaxPrefixLen_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4MaxLen);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "BgpOrfVrfName_Key");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            VcmGetAliasName ((UINT4) i4BgpContextId, au1BgpCxtName);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1BgpCxtName);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        }
        while (nmhGetNextIndexFsBgp4ORFListTable (i4AddrType, &i4AddrType, &PeerAddr, &PeerAddr, i4Afi, &i4Afi, i4Safi, &i4Safi, u4OrfType, &u4OrfType, u4SeqNo, &u4SeqNo, &AddrPrefix, &AddrPrefix, u4AddrPrefixLen, &u4AddrPrefixLen, u4MinLen, &u4MinLen, u4MaxLen, &u4MaxLen, i4Action, &i4Action) == SNMP_SUCCESS);    /*end of do-while */

        i4BgpPrevContextId = i4BgpContextId;
    }
    while (nmhGetNextIndexFsMIBgpContextTable
           (i4BgpPrevContextId, &i4BgpContextId) == SNMP_SUCCESS);

    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    return;
}

/*********************************************************************
*  Function Name : IssProcessRRDBgpConfPage
*  Description   : This function processes the request coming for the
*                  RRD BGP configuration page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessRRDBgpConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessRRDBgpConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessRRDBgpConfPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessRRDBgpGet
*  Description   : This function processes the GET request coming for the
*                  RRD Protocol enable. 
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessRRDBgpGet (tHttp * pHttp, UINT4 u4MaskValue, INT4 i4ProtoId)
{
    INT4                i4MetricValue = 0;
    INT4                i4MatchType = 0;
    INT4                i4AdminStatus = 0;
    UINT1               au1PeerName[RMAP_MAX_NAME_LEN + 4];
    tSNMP_OCTET_STRING_TYPE PeerName;
    UINT1              *pu1Ptr = NULL;

    MEMSET (au1PeerName, 0, sizeof (au1PeerName));
    PeerName.pu1_OctetList = au1PeerName;
    PeerName.i4_Length = (INT4) STRLEN (au1PeerName);

    nmhGetFsbgp4RRDAdminStatus (&i4AdminStatus);
    STRCPY (pHttp->au1KeyString, "fsbgp4RRDAdminStatus_Key");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4AdminStatus);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "fsbgp4RRDProtoMaskForEnable_Key");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4MaskValue);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    nmhGetFsbgp4RRDRouteMapName (&PeerName);
    if (PeerName.i4_Length != 0)
    {
        STRCPY (pHttp->au1KeyString, "fsbgp4RRDRouteMapName_Key");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", PeerName.pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    }
    else
    {
        STRCPY (pHttp->au1KeyString, "fsbgp4RRDRouteMapName_Key");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "");
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    }

    nmhGetFsBgp4RRDMetricValue (i4ProtoId, &i4MetricValue);
    STRCPY (pHttp->au1KeyString, "fsBgp4RRDMetricValue_Key");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4MetricValue);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    if (i4ProtoId == BGP4_OSPF_METRIC)
    {
        nmhGetFsbgp4RRDMatchTypeEnable (&i4MatchType);
        STRCPY (pHttp->au1KeyString, "fsbgp4RRDMatchTypeEnable_Key");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        pu1Ptr = pHttp->au1DataString;
        if (i4MatchType == 0)
        {
            SPRINTF ((CHR1 *) pu1Ptr, "%s", "\0");
        }
        else
        {
            if ((i4MatchType & RTM_MATCH_EXT_TYPE) == RTM_MATCH_EXT_TYPE)
            {
                SPRINTF ((CHR1 *) pu1Ptr, "%s", "External ");
                pu1Ptr += STRLEN ("External ");
            }
            if ((i4MatchType & RTM_MATCH_INT_TYPE) == RTM_MATCH_INT_TYPE)
            {
                SPRINTF ((CHR1 *) pu1Ptr, "%s", "Internal ");
                pu1Ptr += STRLEN ("Internal ");
            }
            if ((i4MatchType & RTM_MATCH_NSSA_TYPE) == RTM_MATCH_NSSA_TYPE)
            {
                SPRINTF ((CHR1 *) pu1Ptr, "%s", "NSSA External");
            }
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    }
    else
    {
        STRCPY (pHttp->au1KeyString, "fsbgp4RRDMatchTypeEnable_Key");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "\0");
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    }

}

/*********************************************************************
*  Function Name : IssProcessRRDBgpConfPageGet
*  Description   : This function processes the GET request coming for the
*                  RRD BGP configuration page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessRRDBgpConfPageGet (tHttp * pHttp)
{
    UINT4               u4RRDSrcProtoMaskEnable = 0;
    INT4                i4Temp = 0;
    INT4                i4MaskEnable = 0;
    INT4                i4BgpContextId = 0;
    INT4                i4BgpPrevContextId = 0;
    INT4                i4ProtoId = 0;
    UINT1               au1BgpCxtName[VCM_ALIAS_MAX_LEN];

    MEMSET (au1BgpCxtName, 0, VCM_ALIAS_MAX_LEN);
    pHttp->i4Write = 0;
    IssPrintAvailableBgpContextId (pHttp);

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    if (nmhGetFirstIndexFsMIBgpContextTable (&i4BgpContextId) == SNMP_FAILURE)
    {
        pHttp->i4Write = i4Temp;
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    i4Temp = pHttp->i4Write;

    do
    {
/*        pHttp->i4Write = i4Temp;*/

        if (BgpSetContext (i4BgpContextId) == SNMP_FAILURE)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
            return;
        }

        nmhGetFsbgp4RRDProtoMaskForEnable (&i4MaskEnable);
        u4RRDSrcProtoMaskEnable = (UINT4) i4MaskEnable;

        if ((u4RRDSrcProtoMaskEnable & BGP4_IMPORT_STATIC) ==
            BGP4_IMPORT_STATIC)
        {
            pHttp->i4Write = i4Temp;
            i4ProtoId = BGP4_STATIC_METRIC;
            IssProcessRRDBgpGet (pHttp, BGP4_IMPORT_STATIC, i4ProtoId);
            STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            VcmGetAliasName ((UINT4) i4BgpContextId, au1BgpCxtName);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1BgpCxtName);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        }
        if ((u4RRDSrcProtoMaskEnable & BGP4_IMPORT_DIRECT) ==
            BGP4_IMPORT_DIRECT)
        {
            pHttp->i4Write = i4Temp;
            i4ProtoId = BGP4_DIRECT_METRIC;
            IssProcessRRDBgpGet (pHttp, BGP4_IMPORT_DIRECT, i4ProtoId);
            STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            VcmGetAliasName ((UINT4) i4BgpContextId, au1BgpCxtName);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1BgpCxtName);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        }
        if ((u4RRDSrcProtoMaskEnable & BGP4_IMPORT_RIP) == BGP4_IMPORT_RIP)
        {
            pHttp->i4Write = i4Temp;
            i4ProtoId = BGP4_RIP_METRIC;
            IssProcessRRDBgpGet (pHttp, BGP4_IMPORT_RIP, i4ProtoId);
            STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            VcmGetAliasName ((UINT4) i4BgpContextId, au1BgpCxtName);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1BgpCxtName);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        }
        if ((u4RRDSrcProtoMaskEnable & BGP4_IMPORT_OSPF) == BGP4_IMPORT_OSPF)
        {
            pHttp->i4Write = i4Temp;
            i4ProtoId = BGP4_OSPF_METRIC;
            IssProcessRRDBgpGet (pHttp, BGP4_IMPORT_OSPF, i4ProtoId);
            STRCPY (pHttp->au1KeyString, "BGP_CONTEXT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            VcmGetAliasName ((UINT4) i4BgpContextId, au1BgpCxtName);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1BgpCxtName);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        }
        i4BgpPrevContextId = i4BgpContextId;

    }
    while (nmhGetNextIndexFsMIBgpContextTable
           (i4BgpPrevContextId, &i4BgpContextId) == SNMP_SUCCESS);

    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;

}

/*********************************************************************
*  Function Name : IssProcessRRDBgpConfPageSet
*  Description   : This function processes the set request for the RRD
*                  BGP Configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessRRDBgpConfPageSet (tHttp * pHttp)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4AdminStatus = 0;
    INT4                i4CxtId = 0;
    INT4                i4MaskEnable = 0;
    INT4                i4MetricValue = 0;
    INT4                i4ProtoId = 0;
    INT4                i4MatchType = 0;

    tSNMP_OCTET_STRING_TYPE PeerName;
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4] = { 0 };

    STRCPY (pHttp->au1Name, "CONTEXT_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4CxtId = ATOI (pHttp->au1Value);

    if (BgpSetContext ((INT4) i4CxtId) == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
        return;
    }

    PeerName.pu1_OctetList = au1RMapName;
    PeerName.i4_Length = 0;

    STRCPY (pHttp->au1Name, "fsbgp4RRDAdminStatus");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4AdminStatus = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "fsBgp4RRDMetricValue");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4MetricValue = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "fsbgp4RRDMatchTypeEnable");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4MatchType = ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "fsbgp4RRDProtoMaskForEnable");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4MaskEnable = ATOI (pHttp->au1Value);
    }
    switch (i4MaskEnable)
    {
        case BGP4_IMPORT_STATIC:
            i4ProtoId = BGP4_STATIC_METRIC;
            break;
        case BGP4_IMPORT_DIRECT:
            i4ProtoId = BGP4_DIRECT_METRIC;
            break;
        case BGP4_IMPORT_RIP:
            i4ProtoId = BGP4_RIP_METRIC;
            break;
        case BGP4_IMPORT_OSPF:
            i4ProtoId = BGP4_OSPF_METRIC;
            break;
        default:
            break;
    }
    STRCPY (pHttp->au1Name, "ACTION");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)

    {
        if (0 == STRCMP (pHttp->au1Value, "Delete"))
        {
            STRCPY (pHttp->au1Name, "fsbgp4RRDRouteMapName");
            if (HttpGetValuebyName (pHttp->au1Name,
                                    pHttp->au1Value,
                                    pHttp->au1PostQuery) == ENM_SUCCESS)
            {
                PeerName.pu1_OctetList = pHttp->au1Value;
                PeerName.i4_Length = (INT4) STRLEN (pHttp->au1Value);
            }

            if (PeerName.i4_Length != 0)
            {
                PeerName.pu1_OctetList = au1RMapName;
                PeerName.i4_Length = 0;

                if (nmhSetFsbgp4RRDRouteMapName (&PeerName) == SNMP_FAILURE)
                {
                    BgpResetContext ();
                    BgpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *)
                                  "\rUnable to delete RouteMap Name\r\n");
                    return;
                }
            }

            if (nmhSetFsbgp4RRDSrcProtoMaskForDisable (i4MaskEnable) ==
                SNMP_FAILURE)
            {
                BgpResetContext ();
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "\rUnable to delete Redistribution protocol\r\n");
                return;
            }
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessRRDBgpConfPageGet (pHttp);
            return;

        }
    }

    STRCPY (pHttp->au1Name, "fsbgp4RRDRouteMapName");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        PeerName.pu1_OctetList = pHttp->au1Value;
        PeerName.i4_Length = (INT4) STRLEN (pHttp->au1Value);
    }

    if (nmhTestv2Fsbgp4RRDAdminStatus (&u4ErrorCode, i4AdminStatus)
        == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid BGP Status");
        return;

    }

    if (nmhTestv2FsBgp4RRDMetricValue (&u4ErrorCode, i4ProtoId, i4MetricValue)
        == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Metric Value");
        return;

    }

    if (i4MatchType != 0)
    {
        if (nmhTestv2Fsbgp4RRDMatchTypeEnable (&u4ErrorCode, i4MatchType)
            == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Invalid Match Type");
            return;

        }
    }

    if (nmhTestv2Fsbgp4RRDProtoMaskForEnable (&u4ErrorCode, i4MaskEnable)
        == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Invalid Redistribution Protocol Mask");
        return;

    }

    if (nmhTestv2Fsbgp4RRDRouteMapName (&u4ErrorCode, &PeerName)
        == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *)
                      "Route-map association failed (probably another route map is used)");
        return;

    }

    if (nmhSetFsbgp4RRDAdminStatus (i4AdminStatus) == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set BGP Status / Enable BGP Module ");
        return;
    }

    if (nmhSetFsBgp4RRDMetricValue (i4ProtoId, i4MetricValue) == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the Metric Value");
        return;
    }

    if (i4MatchType != 0)
    {
        if (nmhSetFsbgp4RRDMatchTypeEnable (i4MatchType) == SNMP_FAILURE)
        {
            BgpResetContext ();
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set the Match Type");
            return;
        }
    }
    if (nmhSetFsbgp4RRDProtoMaskForEnable (i4MaskEnable) == SNMP_FAILURE)
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to set Redistribution Protocol");
        return;
    }
    if ((PeerName.i4_Length != 0)
        && (nmhSetFsbgp4RRDRouteMapName (&PeerName) == SNMP_FAILURE))
    {
        BgpResetContext ();
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the RouteMap Name");
        return;
    }
    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessRRDBgpConfPageGet (pHttp);
    return;
}

/*********************************************************************
*  Function Name : IssProcessBgpContextCreationPage
*  Description   : This function processes the request coming for the
*                   configuration page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessBgpContextCreationPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessBgpContextCreationPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessBgpContextCreationPageSet (pHttp);
    }
    return;

}

/*********************************************************************
 * *  Function Name : IssProcessBgpContextCreationPageGet
 * *  Description   : This function processes the get request coming for 
 * *                  the configuration page. 
 * *                     
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssProcessBgpContextCreationPageGet (tHttp * pHttp)
{

    UINT1               au1BgpCxtName[VCM_ALIAS_MAX_LEN];
    UINT4               u4Temp = 0;
    INT4                i4RowFound = 0;
    INT4                i4BgpContextId;
    INT4                i4BgpPrevContextId = 0;
    INT4                i4RetValBgpCxtStat;
    INT4                i4OutCome;

    MEMSET (au1BgpCxtName, 0, VCM_ALIAS_MAX_LEN);
    pHttp->i4Write = 0;

    IssPrintVcmAvailableContextId (pHttp);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    u4Temp = (UINT4) pHttp->i4Write;

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);

    BgpLock ();

    i4OutCome = nmhGetFirstIndexFsMIBgpContextTable (&i4BgpContextId);

    if (i4OutCome == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        STRCPY (pHttp->au1KeyString, "CONTEXT_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        VcmGetAliasName ((UINT4) i4BgpContextId, au1BgpCxtName);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1BgpCxtName);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

        STRCPY (pHttp->au1KeyString, "BGP_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetFsMIBgp4ContextStatus (i4BgpContextId,
                                         &i4RetValBgpCxtStat) == SNMP_FAILURE)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to Get the Context Status");
            return;

        }
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetValBgpCxtStat);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        i4RowFound = 1;
        i4BgpPrevContextId = i4BgpContextId;
    }
    while (nmhGetNextIndexFsMIBgpContextTable
           (i4BgpPrevContextId, &i4BgpContextId) == SNMP_SUCCESS);

    BgpUnLock ();

    WebnmUnRegisterLock (pHttp);

    if (i4RowFound == 1)
    {
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
    }

}

/*********************************************************************
 * *  Function Name : IssProcessBgpContextCreationPageSet
 * *  Description   : This function processes the get request coming for 
 * *                  the  configuration page. 
 * *                     
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssProcessBgpContextCreationPageSet (tHttp * pHttp)
{
    UINT4               u4ErrCode;
    UINT4               u4LocalAS = BGP4_INV_AS;
    UINT4               u4AsNo = 0;
    INT4                i4GlobalAdmin = 0;
    INT4                i4MaxPeer = 0;
    INT4                i4MaxRoute = 0;
    INT4                i4BgpCxtId;
    INT4                i4AdminStatus = BGP4_ADMIN_DOWN;
    INT4                i4RetValue = BGP4_ADMIN_DOWN;
    INT4                i4Status = BGP4_FALSE;
    INT4                i4SupportStatus;

    if (HttpGetValuebyName (ISS_ACTION, pHttp->au1Array,
                            pHttp->au1PostQuery) != ENM_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Request");
        return;
    }

    if (STRCMP (pHttp->au1Array, ISS_ADD) == 0)
    {
        STRCPY (pHttp->au1Name, "CONTEXT_ID");

        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) != SUCCESS)
        {
            IssProcessBgpContextCreationPageGet (pHttp);
            return;
        }

        BgpLock ();

        i4BgpCxtId = ATOI (pHttp->au1Value);

        STRCPY (pHttp->au1Name, "CONTEXT_STATUS");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4AdminStatus = ATOI (pHttp->au1Value);

        STRCPY (pHttp->au1Name, "fsMIBgp4LocalAs");
        if (HttpGetValuebyName (pHttp->au1Name,
                                pHttp->au1Value,
                                pHttp->au1PostQuery) != ENM_SUCCESS)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to get AS Number");
            IssProcessBgpContextCreationPageGet (pHttp);
            return;
        }
        u4AsNo = (UINT4) ATOI (pHttp->au1Value);

        if (BgpSetContext (i4BgpCxtId) == SNMP_FAILURE)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Error in setting L3 Context");
            IssProcessBgpContextCreationPageGet (pHttp);
            return;
        }
        nmhGetFsbgp4FourByteASNSupportStatus (&i4SupportStatus);

        if ((i4SupportStatus == BGP4_DISABLE_4BYTE_ASN_SUPPORT) &&
            (u4AsNo > BGP4_MAX_TWO_BYTE_AS))
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "4-byte ASN Capability is disabled. Configure value < 65536");
            IssProcessBgpContextCreationPageGet (pHttp);
            return;
        }

        STRCPY (pHttp->au1Name, "fsMIBgp4MaxPeerEntry");
        if (HttpGetValuebyName (pHttp->au1Name,
                                pHttp->au1Value,
                                pHttp->au1PostQuery) != ENM_SUCCESS)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to get MaxPeer Entry");
            IssProcessBgpContextCreationPageGet (pHttp);
            return;
        }
        i4MaxPeer = ATOI (pHttp->au1Value);

        STRCPY (pHttp->au1Name, "fsMIBgp4MaxNoofRoutes");
        if (HttpGetValuebyName (pHttp->au1Name,
                                pHttp->au1Value,
                                pHttp->au1PostQuery) != ENM_SUCCESS)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to get Max No.of Routes");
            IssProcessBgpContextCreationPageGet (pHttp);
            return;
        }
        i4MaxRoute = ATOI (pHttp->au1Value);

        nmhGetFsMIBgp4LocalAsNo (i4BgpCxtId, &u4LocalAS);
        nmhGetFsMIBgp4GlobalAdminStatus (i4BgpCxtId, &i4GlobalAdmin);

        if ((u4LocalAS != BGP4_INV_AS) && (u4AsNo != BGP4_INV_AS) &&
            (u4AsNo != u4LocalAS) && (i4GlobalAdmin == BGP4_ADMIN_UP))
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Mismatch between local AS and input AS");
            return;
        }
        i4RetValue =
            nmhTestv2FsMIBgp4LocalAsNo (&u4ErrCode, i4BgpCxtId, u4AsNo);
        if (i4RetValue == SNMP_FAILURE)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set Local As");
            return;
        }

        i4RetValue = nmhSetFsMIBgp4LocalAsNo (i4BgpCxtId, u4AsNo);
        if (i4RetValue == SNMP_FAILURE)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set Local As");
            return;
        }

        i4RetValue = nmhTestv2FsMIBgp4MaxPeerEntry (&u4ErrCode, i4MaxPeer);
        if (i4RetValue == SNMP_FAILURE)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to Set Max Peer");
            return;
        }

        i4RetValue = nmhSetFsMIBgp4MaxPeerEntry (i4MaxPeer);
        if (i4RetValue == SNMP_FAILURE)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set Max Peer");
            return;
        }

        i4RetValue = nmhTestv2FsMIBgp4MaxNoofRoutes (&u4ErrCode, i4MaxRoute);
        if (i4RetValue == SNMP_FAILURE)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to Set Max Route");
            return;
        }

        i4RetValue = nmhSetFsMIBgp4MaxNoofRoutes (i4MaxRoute);
        if (i4RetValue == SNMP_FAILURE)
        {
            BgpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set Max Route");
            return;
        }

        if (i4AdminStatus == BGP4_TRUE)
        {
            if (i4BgpCxtId != BGP4_DFLT_VRFID)
            {
                nmhGetFsMIBgp4GlobalAdminStatus (i4BgpCxtId, &i4AdminStatus);
            }
            if (i4AdminStatus == BGP4_ADMIN_DOWN)
            {
                i4RetValue =
                    nmhTestv2FsMIBgp4ContextStatus (&u4ErrCode, i4BgpCxtId,
                                                    BGP4_TRUE);
            }
            if (i4RetValue == SNMP_FAILURE)
            {
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "Cannot Disable In Add Option!Click On Delete");
                return;
            }
            i4Status = BGP4_TRUE;
            i4RetValue = nmhSetFsMIBgp4ContextStatus (i4BgpCxtId, i4Status);
            if (i4RetValue == SNMP_FAILURE)
            {
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "Cannot Disable In Add Option!Click On Delete");
                return;
            }
        }
        else
        {
            nmhGetFsMIBgp4GlobalAdminStatus (i4BgpCxtId, &i4AdminStatus);
            if (i4AdminStatus == BGP4_ADMIN_UP)
            {
                nmhSetFsMIBgp4GlobalAdminStatus (i4BgpCxtId, BGP4_ADMIN_DOWN);
            }
            i4RetValue = nmhTestv2FsMIBgp4ContextStatus (&u4ErrCode,
                                                         i4BgpCxtId,
                                                         BGP4_FALSE);
            if (i4RetValue == SNMP_FAILURE)
            {
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "Cannot disable the Context");
                return;
            }
            i4RetValue = nmhSetFsMIBgp4ContextStatus (i4BgpCxtId, BGP4_FALSE);
            if (i4RetValue == SNMP_FAILURE)
            {
                BgpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "Cannot disable the Context");
                return;
            }

        }
    }
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessBgpContextCreationPageGet (pHttp);
}
#endif
#endif
