/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mplsweb.c,v 1.36 2014/11/08 11:59:50 siva Exp $
 *
 * Description: This file contains routines for MPLS web Module 
 ********************************************************************/

#ifndef _MPLS_WEB_C
#define _MPLS_WEB_C

#include "webiss.h"
#include "isshttp.h"
#include "cfa.h"
#include "rtm.h"
#include "vcm.h"
#include "mpls.h"
#include "mplsweb.h"
#include "webinc.h"
#ifdef MPLS_L3VPN_WANTED
#include "l3vpncli.h"
#include "l3vpntdfsg.h"
#include "l3vpntdfs.h"
#include "l3vpndefg.h"
#include "l3vpntmr.h"
#include "l3vpnprot.h"
#endif

/*******************************************************************************
*  Function Name : IssProcessMplsIfPage 
*  Description   : This function processes the request coming for the Mpls If 
*                  Page.                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssProcessMplsIfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessMplsIfGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessMplsIfSet (pHttp);
    }
}

/*******************************************************************************
*  Function Name : IssProcessMplsIfGet
*  Description   : This function gets and displays all the MPLS Interfaces 
*                  enabled and the associated vlan interface in the web page
*                  and enables the user to enable MPLS on a vlan interface.                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/

VOID
IssProcessMplsIfGet (tHttp * pHttp)
{
    tCfaIfInfo          CfaIfInfo;
    INT4                i4Index = 0;
    UINT1               u1BridgedIfStatus = 0;
    UINT4               u4Temp = 0;
    UINT4               u4MplsIfIndex = 0;

    INT1               *pi1MplsIfName = NULL;

    INT1                ai1MplsIfName[CFA_MAX_PORT_NAME_LENGTH] = { 0 };
    UINT1               au1String[20] = { 0 };

    UINT1              *pu1String = NULL;

    pi1MplsIfName = &ai1MplsIfName[0];
    MEMSET (au1String, 0, sizeof (au1String));
    pu1String = &au1String[0];

    pHttp->i4Write = 0;

    /* Display the web page till the scroll bar that will contain all L3 
     * Interfaces */
    IssPrintL3Interfaces (pHttp, 0);
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    /* Register The lock with Cfa to make Lock and unlock */
    WebnmRegisterLock (pHttp, CfaLock, CfaUnlock);
    CFA_LOCK ();

    if (nmhGetFirstIndexIfIpTable (&i4Index) == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    /* Display the top form, till the table object in the bottom form */
    /* Have a copy of this pointer location */
    u4Temp = (UINT4) pHttp->i4Write;
    do
    {
#ifdef CFA_WANTED
        /* Check whether the interface is only L3 interface */
        if ((CfaGetIfBridgedIfaceStatus (i4Index, &u1BridgedIfStatus)
             == CFA_FAILURE))
        {
            continue;
        }
        CfaGetIfInfo ((UINT4) i4Index, &CfaIfInfo);

        if ((CfaIfInfo.u1IfType != CFA_L3IPVLAN) &&
            ((CfaIfInfo.u1IfType == CFA_ENET) &&
             (CfaIfInfo.u1BridgedIface != CFA_DISABLED)))
        {
            continue;
        }
#endif
        /* Check whether MPLS is enabled on the interface and 
         * display the page only if it is MPLS ENABLED*/
#ifdef CFA_WANTED
        if (CfaUtilGetMplsIfFromIfIndex ((UINT4) i4Index,
                                         &u4MplsIfIndex, FALSE) == CFA_FAILURE)
        {
            continue;
        }
#endif
        pHttp->i4Write = (INT4) u4Temp;

        /* Display all the details */
#ifdef CFA_WANTED
        CfaCliGetIfName (u4MplsIfIndex, pi1MplsIfName);
#endif
        STRNCPY (pu1String, pi1MplsIfName, (sizeof (au1String) - 1));
        STRCPY (pHttp->au1KeyString, "MPLSINTERFACES_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "IPINTERFACES_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", CfaIfInfo.au1IfName);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    while (nmhGetNextIndexIfIpTable (i4Index, &i4Index) == SNMP_SUCCESS);

    /* All the Mpls Interfaces displayed in the table format */
    /* Now, Move the pointer till the table stop flag */
    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    CFA_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    /* Display all the information in the html page after the Table stop flag */
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/******************************************************************************
*  Function Name : IssProcessMplsIfSet
*  Description   : This function gets the user input from the web page and 
*                  enables or disables MPLS on a VLAN Interface.                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
******************************************************************************/

VOID
IssProcessMplsIfSet (tHttp * pHttp)
{
    tCfaIfInfo          CfaIfInfo;
    UINT4               u4IfaceIndex = 0;
    UINT4               u4MplsIfIndex = 0;

    /* Get the interface name to be set from the form and get
     * the interface index from it */
    STRCPY (pHttp->au1Name, "INTERFACE_ID");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
#ifdef CFA_WANTED
        CFA_LOCK ();
        if (CfaGetInterfaceIndexFromName (pHttp->au1Value, &u4IfaceIndex)
            == OSIX_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Wrong Interface");
            CFA_UNLOCK ();
            return;
        }
        CFA_UNLOCK ();
#endif
    }

    /* Get the Processing to be done (Add or delete) from the form */
    STRCPY (pHttp->au1Name, "DELETE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {
        /* MPLS is to be disabled on the interface */
#ifdef CFA_WANTED
        if (CfaUtilGetMplsIfFromIfIndex (u4IfaceIndex, &u4MplsIfIndex, TRUE) ==
            CFA_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Mpls is not enabled already");
            return;
        }
#endif

        if (MplsDeleteMplsIfOrMplsTnlIf (u4IfaceIndex, u4MplsIfIndex,
                                         CFA_MPLS, MPLS_TRUE) == MPLS_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Cannot disable Mpls");
            return;
        }
        IssProcessMplsIfGet (pHttp);
        return;
    }
    else
    {
        /* MPLS is to be enabled on the interface */
        CFA_LOCK ();
#ifdef CFA_WANTED
        if (CfaValidateCfaIfIndex ((UINT2) u4IfaceIndex) == CFA_FAILURE)

        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid L3 Interface Index");
            CFA_UNLOCK ();
            return;
        }

        if (CfaUtilGetMplsIfFromIfIndex (u4IfaceIndex, &u4MplsIfIndex, FALSE) ==
            CFA_SUCCESS)
        {
            IssSendError (pHttp, (CONST INT1 *) "Mpls Already Enabled");
            CFA_UNLOCK ();
            return;
        }

        CfaGetIfInfo (u4IfaceIndex, &CfaIfInfo);
#endif
        if (!((CfaIfInfo.u1IfType == CFA_L3IPVLAN) ||
              ((CfaIfInfo.u1IfType == CFA_ENET) &&
               (CfaIfInfo.u1BridgedIface == CFA_DISABLED))))
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "MPLS Should be enabled only on L3 "
                          "Interface only");
            CFA_UNLOCK ();
            return;
        }

        CFA_UNLOCK ();

        if (CfaGetFreeInterfaceIndex (&u4MplsIfIndex, CFA_MPLS) == OSIX_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Cannot enable Mpls");
            return;
        }

        if (MplsCreateMplsIfOrMplsTnlIf (u4IfaceIndex, u4MplsIfIndex, CFA_MPLS)
            == MPLS_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Cannot enable Mpls");
            return;
        }

        IssProcessMplsIfGet (pHttp);
        return;
    }
}

/*******************************************************************************
*  Function Name : IssProcessMplsFtnPage 
*  Description   : This function processes request from the MPLS FTN Page.                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/

VOID
IssProcessMplsFtnPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessMplsFtnGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessMplsFtnSet (pHttp);
    }
}

/*******************************************************************************
*  Function Name : IssProcessMplsFtnGet
*  Description   : This function displays all the ftn entries available in the 
*                  web page and enables user to configure (add or delete) FTN
*                  Entry.                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/

VOID
IssProcessMplsFtnGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE DestAddrMin;
    tSNMP_OCTET_STRING_TYPE DestAddrMax;
    tSNMP_OCTET_STRING_TYPE NextHopAddr;
    tSNMP_OID_TYPE      ActionPointer;

    UINT4               u4FtnIndex = 0;
    UINT4               u4PrevIndex = 0;
    UINT4               u4MinAddr = 0;
    UINT4               u4MaxAddr = 0;
    UINT4               u4Prefix = 0;
    UINT4               u4Mask = 0;
    UINT4               u4XCIndex = 0;
    UINT4               u4InIndex = 0;
    UINT4               u4OutIndex = 0;
    UINT4               u4InLabel = 0;
    UINT4               u4OutLabel = 0;
    UINT4               u4NextHopAddr = 0;
    UINT4               u4Direction = 0;
    UINT4               u4Temp = 0;

    INT4                i4InStatus = 0;
    INT4                i4OutStatus = 0;
    INT4                i4OutCome = 0;
    INT4                i4XCOwner = 0;
    INT4                i4InInterface = 0;

    static UINT4        au4ActionPointer[MPLS_TE_XC_TABLE_OFFSET];
    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1NextHopAddr[MPLS_INDEX_LENGTH];
    static UINT1        au1DestAddrMin[MPLS_INDEX_LENGTH];
    static UINT1        au1DestAddrMax[MPLS_INDEX_LENGTH];

    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = MPLS_INDEX_LENGTH;
    NextHopAddr.pu1_OctetList = au1NextHopAddr;
    DestAddrMax.pu1_OctetList = au1DestAddrMax;
    DestAddrMin.pu1_OctetList = au1DestAddrMin;
    ActionPointer.pu4_OidList = au4ActionPointer;

    pHttp->i4Write = (INT4) u4Temp;
    /* Display the web page till the L3 Interfaces scroll bar */
    IssPrintL3Interfaces (pHttp, 0);

    /* Display the web page till the table in the bottom form */
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    /* Now, try to fetch each FTN entry and display corresponding entry 
     * in the webpage */
    i4OutCome = nmhGetFirstIndexMplsFTNTable (&u4FtnIndex);

    /* No Entry Available, So, just displaying web page 
     * till the Table stop flag */
    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;
    do
    {
        u4Prefix = 0;
        u4Mask = 0;
        u4NextHopAddr = 0;
        u4XCIndex = 0;
        u4InIndex = 0;
        u4OutIndex = 0;
        u4InLabel = 0;
        u4OutLabel = 0;
        i4InStatus = 0;
        i4OutStatus = 0;
        i4InInterface = 0;
        u4Direction = 0;
        u4PrevIndex = u4FtnIndex;
        /* get min and max dest address....get prefix and mask from that */
        if ((nmhGetMplsFTNDestAddrMin (u4FtnIndex, &DestAddrMin))
            == SNMP_FAILURE)
        {
            continue;
        }
        WEB_OCTETSTRING_TO_INTEGER ((&DestAddrMin), u4MinAddr);
        if ((nmhGetMplsFTNDestAddrMax (u4FtnIndex, &DestAddrMax))
            == SNMP_FAILURE)
        {
            continue;
        }
        WEB_OCTETSTRING_TO_INTEGER ((&DestAddrMax), u4MaxAddr);

        u4Prefix = u4MinAddr;
        u4Mask = u4MaxAddr;

        /* Fetch the Action Pointer, From action pointer, get the 
         * XCIndex, InSegmentIndex and OutSegmentIndex */
        if ((nmhGetMplsFTNActionPointer (u4FtnIndex, &ActionPointer))
            == SNMP_FAILURE)
        {
            continue;
        }
        /* If No XCIndex exists, try and fetch the next entry */
        if (ActionPointer.u4_Length == 2)
        {
            pHttp->i4Write = (INT4) u4Temp;
            IssMplsDisplayFtn (pHttp, u4Prefix, u4Mask, u4Direction, 0, 0, 0,
                               0);
            continue;
        }

        MPLS_OID_TO_INTEGER ((&ActionPointer), u4XCIndex,
                             MPLS_XC_INDEX_START_OFFSET);
        WEB_INTEGER_TO_OCTETSTRING (u4XCIndex, (&XCIndex));
        MPLS_OID_TO_INTEGER ((&ActionPointer), u4InIndex,
                             MPLS_IN_INDEX_START_OFFSET);
        WEB_INTEGER_TO_OCTETSTRING (u4InIndex, (&InSegmentIndex));
        MPLS_OID_TO_INTEGER ((&ActionPointer), u4OutIndex,
                             MPLS_OUT_INDEX_START_OFFSET);
        WEB_INTEGER_TO_OCTETSTRING (u4OutIndex, (&OutSegmentIndex));

        /* Show the entries only if the XCOwner is Not SNMP */
        if ((nmhGetMplsXCOwner (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                                &i4XCOwner)) == SNMP_FAILURE)
        {
            continue;
        }
        /* MPLS_OWNER_SNMP = 3 */
        if (i4XCOwner != 3)
        {
            continue;
        }

        /* If InIndex is present the ftn is of type Incoming,
         * so, fetch the InInterface and InLabel and display it*/
        if (u4InIndex != 0)
        {
            if (nmhGetMplsInSegmentRowStatus (&InSegmentIndex, &i4InStatus)
                == SNMP_FAILURE)
            {
                continue;
            }
            if (i4InStatus == ISS_ACTIVE)
            {
                if (nmhGetMplsInSegmentInterface (&InSegmentIndex,
                                                  &i4InInterface)
                    == SNMP_FAILURE)
                {
                    continue;
                }

                if (nmhGetMplsInSegmentLabel (&InSegmentIndex, &u4InLabel)
                    == SNMP_FAILURE)
                {
                    continue;
                }
            }
        }

        /* If InIndex is present the ftn is of type Outgoing,
         * so, fetch the NextHopAddr and OutLabel and display it*/
        if (u4OutIndex != 0)
        {
            if (nmhGetMplsOutSegmentRowStatus (&OutSegmentIndex, &i4OutStatus)
                == SNMP_FAILURE)
            {
                continue;
            }
            if (i4OutStatus == ISS_ACTIVE)
            {
                /* Get the Next Hop */
                if (nmhGetMplsOutSegmentNextHopAddr (&OutSegmentIndex,
                                                     &NextHopAddr) ==
                    SNMP_FAILURE)
                {
                    continue;
                }
                WEB_OCTETSTRING_TO_INTEGER ((&NextHopAddr), u4NextHopAddr);

                if (nmhGetMplsOutSegmentTopLabel (&OutSegmentIndex,
                                                  &u4OutLabel) == SNMP_FAILURE)
                {
                    continue;
                }
            }
        }

        if (u4InLabel != 0 && u4OutLabel != 0)
        {
            pHttp->i4Write = (INT4) u4Temp;
            IssMplsDisplayFtn (pHttp, u4Prefix, u4Mask, 3, u4InLabel,
                               (UINT4) i4InInterface, u4OutLabel,
                               u4NextHopAddr);
        }
        else if (u4InLabel != 0)
        {
            pHttp->i4Write = (INT4) u4Temp;
            IssMplsDisplayFtn (pHttp, u4Prefix, u4Mask, 1, u4InLabel,
                               (UINT4) i4InInterface, u4OutLabel,
                               u4NextHopAddr);
        }
        else
        {
            pHttp->i4Write = (INT4) u4Temp;
            IssMplsDisplayFtn (pHttp, u4Prefix, u4Mask, 2, u4InLabel,
                               (UINT4) i4InInterface, u4OutLabel,
                               u4NextHopAddr);
        }
    }
    while ((nmhGetNextIndexMplsFTNTable (u4PrevIndex, &u4FtnIndex)) !=
           SNMP_FAILURE);

    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    /* Display all the information in the html page after the Table stop flag */
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*******************************************************************************
*  Function Name : IssMplsDisplayFtn
*  Description   : This function Displays the FTN Details collected in the 
*                  web page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*                  u4Prefix - Destination Prefix
*                  u4Mask - Mask
*                  u4Direction - Incoming or Outgoing FTN
*                  u4Label - InLabel or OutLabel
*                  u4NextHopOrInInterface - NextHop or InInterface
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssMplsDisplayFtn (tHttp * pHttp, UINT4 u4Prefix, UINT4 u4Mask,
                   UINT4 u4Direction, UINT4 u4InLabel, UINT4 u4InInterface,
                   UINT4 u4OutLabel, UINT4 u4NextHop)
{
    UINT4               u4L3Intf = 0;
    tCfaIfInfo          CfaIfInfo;

    UINT1              *pu1Temp = NULL;
    INT4                i4RetVal = 0;

    /* Display the Destination Ip */
    WEB_CONVERT_IPADDR_TO_STR (pu1Temp, u4Prefix);
    STRCPY (pHttp->au1KeyString, "DESTINATION_PREFIX");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1Temp);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, pHttp->au1KeyString);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    WEB_CONVERT_IPADDR_TO_STR (pu1Temp, u4Mask);
    STRCPY (pHttp->au1KeyString, "DESTINATION_MASK");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1Temp);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, pHttp->au1KeyString);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "DIRECTION");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    if (u4Direction == 1)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "IN");
    }
    else if (u4Direction == 2)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "OUT");
    }
    else if (u4Direction == 3)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "INOUT");
    }
    else
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "NONE");
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, pHttp->au1KeyString);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "INLABEL");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    if (u4InLabel != 0)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4InLabel);
    }
    else
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, pHttp->au1KeyString);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "INTERFACE_ID");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    if (u4InInterface != 0)
    {
        i4RetVal =
            CfaUtilGetIfIndexFromMplsTnlIf (u4InInterface, &u4L3Intf, TRUE);
        UNUSED_PARAM (i4RetVal);
        CfaGetIfInfo (u4L3Intf, &CfaIfInfo);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", CfaIfInfo.au1IfName);
    }
    else
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, pHttp->au1KeyString);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "OUTLABEL");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    if (u4OutLabel != 0)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4OutLabel);
    }
    else
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, pHttp->au1KeyString);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "NEXT_HOP");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    if (u4NextHop != 0)
    {
        WEB_CONVERT_IPADDR_TO_STR (pu1Temp, u4NextHop);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1Temp);
    }
    else
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, pHttp->au1KeyString);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
}

/*******************************************************************************
*  Function Name : IssProcessMplsFtnSet
*  Description   : This function gets the user input from the web page and 
*                  configures (add or delete) FTN Entry.                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/

VOID
IssProcessMplsFtnSet (tHttp * pHttp)
{
    tCfaIfInfo          CfaIfInfo;

    UINT4               u4Prefix = 0;
    UINT4               u4Mask = 0;
    UINT4               u4Direction = 0;
    UINT4               u4InLabel = 0;
    UINT4               u4OutLabel = 0;
    UINT4               u4NextHop = 0;
    UINT4               u4InIndex = 0;
    UINT4               u4MplsIfIndex = 0;
    INT4                i4RetVal = WEB_SUCCESS;

    STRCPY (pHttp->au1Name, "DESTINATION_PREFIX");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4Prefix = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));

    STRCPY (pHttp->au1Name, "DESTINATION_MASK");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4Mask = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));

    STRCPY (pHttp->au1Name, "DIRECTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "IN") == 0)
    {
        u4Direction = 1;
    }
    else if (STRCMP (pHttp->au1Value, "OUT") == 0)
    {
        u4Direction = 2;
    }
    else
    {
        u4Direction = 0;
    }

    STRCPY (pHttp->au1Name, "INLABEL");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (u4Direction == 1)
    {
        u4InLabel = (UINT4) ATOI (pHttp->au1Value);
    }
    else if (u4Direction == 2)
    {
        u4OutLabel = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "INTERFACE_ID");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        if (STRCMP (pHttp->au1Value, "-") != 0)
        {
            issDecodeSpecialChar (pHttp->au1Value);
#ifdef CFA_WANTED
            if (CfaGetInterfaceIndexFromName (pHttp->au1Value, &u4InIndex)
                == OSIX_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Wrong Index");
                return;
            }
            if (CfaUtilGetMplsIfFromIfIndex (u4InIndex, &u4MplsIfIndex, TRUE) ==
                CFA_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "Mpls is not enabled on this interface");
                return;
            }
            CfaGetIfInfo (u4InIndex, &CfaIfInfo);
#endif
        }
        else
        {
            u4InIndex = 0;
        }
    }

    STRCPY (pHttp->au1Name, "NEXT_HOP");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if ((STRCMP (pHttp->au1Value, "...") == 0) ||
        (STRCMP (pHttp->au1Value, "-") == 0))
    {
        u4NextHop = 0;
    }
    else
    {
        u4NextHop = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
    }

    STRCPY (pHttp->au1Name, "DELETE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {
        i4RetVal = IssMplsDeleteFtn (pHttp, u4Prefix, u4Mask);
    }
    else
    {
        i4RetVal = IssMplsFtnLabelBinding (pHttp, u4Prefix, u4Mask, u4NextHop,
                                           CfaIfInfo.au1IfName, u4InLabel,
                                           u4OutLabel);
    }

    if (i4RetVal == WEB_SUCCESS)
    {
        IssProcessMplsFtnGet (pHttp);
    }
}

/*******************************************************************************
*  Function Name : IssMplsDeleteFtn
*  Description   : This function deletes the FTN Entries corresponding to passed
*                  Prefix and mask.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*                  u4Prefix - Destination Prefix to be deleted
*                  u4Mask - Destination Mask.
*  Output(s)     : None.
*  Return Values : WEB_SUCCESS or WEB_FAILURE
*******************************************************************************/

INT4
IssMplsDeleteFtn (tHttp * pHttp, UINT4 u4Prefix, UINT4 u4Mask)
{
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE DestMin;
    tSNMP_OCTET_STRING_TYPE DestMax;
    tSNMP_OID_TYPE      ActionPointer;
    INT4                i4XCOwner = 0;
    INT4                i4IfIndex = 0;
    UINT4               u4FtnIndex = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4XCIndex = 0;
    UINT4               u4InIndex = 0;
    UINT4               u4OutIndex = 0;
    UINT4               u4L3Intf = 0;
    INT4                i4ActionType = 0;
    INT4                i4InStatus = 0;
    INT4                i4OutStatus = 0;
    UINT4               u4GetPrefix = 0;
    UINT4               u4GetMask = 0;
    UINT1               u1EntryDeleted = FALSE;

    static UINT4        au4ActionPointer[MPLS_TE_XC_TABLE_OFFSET];
    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1DestMin[MPLS_INDEX_LENGTH];
    static UINT1        au1DestMax[MPLS_INDEX_LENGTH];

    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = MPLS_INDEX_LENGTH;
    ActionPointer.pu4_OidList = au4ActionPointer;
    DestMin.pu1_OctetList = au1DestMin;
    DestMax.pu1_OctetList = au1DestMax;

    /* get the first index of ftn table */
    if ((nmhGetFirstIndexMplsFTNTable (&u4FtnIndex)) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "No FTN Entry Exists");
        return WEB_FAILURE;
    }
    do
    {
        u1EntryDeleted = FALSE;
        if (nmhGetMplsFTNDestAddrMin (u4FtnIndex, &DestMin) == SNMP_FAILURE)
        {
            continue;
        }

        if (nmhGetMplsFTNDestAddrMax (u4FtnIndex, &DestMax) == SNMP_FAILURE)
        {
            continue;
        }

        WEB_OCTETSTRING_TO_INTEGER ((&DestMin), u4GetPrefix);
        WEB_OCTETSTRING_TO_INTEGER ((&DestMax), u4GetMask);

        /* scan the ftn table */
        if ((nmhGetMplsFTNActionPointer (u4FtnIndex, &ActionPointer))
            == SNMP_FAILURE)
        {
            continue;
        }
        if ((nmhGetMplsFTNActionType (u4FtnIndex, &i4ActionType))
            == SNMP_FAILURE)
        {
            continue;
        }
        if (i4ActionType != 1)
        {
            continue;
        }

        if (!((u4Prefix == u4GetPrefix) && (u4Mask == u4GetMask)))
        {
            continue;
        }

        /* from the action pointer get the InSegment , OutSegment and XC table 
         * index */
        MPLS_OID_TO_INTEGER ((&ActionPointer), u4XCIndex,
                             MPLS_XC_INDEX_START_OFFSET);
        WEB_INTEGER_TO_OCTETSTRING (u4XCIndex, (&XCIndex));
        MPLS_OID_TO_INTEGER ((&ActionPointer), u4InIndex,
                             MPLS_IN_INDEX_START_OFFSET);
        WEB_INTEGER_TO_OCTETSTRING (u4InIndex, (&InSegmentIndex));
        MPLS_OID_TO_INTEGER ((&ActionPointer), u4OutIndex,
                             MPLS_OUT_INDEX_START_OFFSET);
        WEB_INTEGER_TO_OCTETSTRING (u4OutIndex, (&OutSegmentIndex));

        /* get the XCOwner */
        if ((nmhGetMplsXCOwner (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                                &i4XCOwner)) == SNMP_FAILURE)
        {
            continue;
        }
        if (i4XCOwner != 3)
        {
            continue;
        }
        /* if owner is MPLS_OWNER_SNMP delete ftn entry and xc entry */
        if ((nmhTestv2MplsFTNRowStatus
             (&u4ErrCode, u4FtnIndex, ISS_DESTROY)) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "FTN is not destroyed");
            return WEB_FAILURE;
        }
        if ((nmhSetMplsFTNRowStatus (u4FtnIndex, ISS_DESTROY)) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "FTN is not destroyed");
            return WEB_FAILURE;
        }
        if ((nmhTestv2MplsXCRowStatus (&u4ErrCode, &XCIndex,
                                       &InSegmentIndex, &OutSegmentIndex,
                                       ISS_DESTROY)) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "XC is not destroyed");
            return WEB_FAILURE;
        }
        if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                    &OutSegmentIndex,
                                    ISS_DESTROY)) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "XC is not destroyed");
            return WEB_FAILURE;
        }

        /* 4. delete in and out segment entries if link is present */
        if (u4InIndex != 0)
        {
            /* get the interface index b4 deleting the In seg entry */
            if ((nmhGetMplsInSegmentRowStatus (&InSegmentIndex,
                                               &i4InStatus)) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Wrong In Segment Entry");
                return WEB_FAILURE;
            }
            /* delete the out interface also */
            if ((nmhGetMplsInSegmentInterface (&InSegmentIndex,
                                               &i4IfIndex)) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Wrong In Interface");
                return WEB_FAILURE;
            }
            if ((i4InStatus == ISS_NOT_READY) && (i4IfIndex != 0))
            {
#ifdef CFA_WANTED
                if ((CfaUtilGetIfIndexFromMplsTnlIf
                     ((UINT4) i4IfIndex, &u4L3Intf, TRUE)) == CFA_FAILURE)
                {
                    IssSendError (pHttp, (CONST INT1 *) "Wrong In Interface");
                    return WEB_FAILURE;
                }
#endif
                if (MplsDeleteMplsIfOrMplsTnlIf (u4L3Intf, (UINT4) i4IfIndex,
                                                 CFA_MPLS_TUNNEL, MPLS_TRUE)
                    == MPLS_FAILURE)
                {
                    IssSendError (pHttp, (CONST INT1 *) "Wrong In Interface");
                    return WEB_FAILURE;
                }
            }
            if ((nmhTestv2MplsInSegmentRowStatus (&u4ErrCode,
                                                  &InSegmentIndex,
                                                  ISS_DESTROY)) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "In Segment Not destroyed");
                return WEB_FAILURE;
            }
            if ((nmhSetMplsInSegmentRowStatus
                 (&InSegmentIndex, ISS_DESTROY)) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "In Segment Not destroyed");
                return WEB_FAILURE;
            }
            u1EntryDeleted = TRUE;
        }
        if (u4OutIndex != 0)
        {
            /* get the interface index b4 deleting the out seg entry */
            if ((nmhGetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                                &i4OutStatus)) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Wrong Out Segment Entry");
                return WEB_FAILURE;
            }
            /* delete the out interface also */
            if ((nmhGetMplsOutSegmentInterface (&OutSegmentIndex,
                                                &i4IfIndex)) == SNMP_FAILURE)
            {
            }
            if ((i4OutStatus == ISS_NOT_READY) && (i4IfIndex != 0))
            {
#ifdef CFA_WANTED
                if ((CfaUtilGetIfIndexFromMplsTnlIf
                     ((UINT4) i4IfIndex, &u4L3Intf, TRUE)) == CFA_FAILURE)
                {
                    IssSendError (pHttp, (CONST INT1 *) "Wrong Out Interface");
                    return WEB_FAILURE;
                }
#endif
                if (MplsDeleteMplsIfOrMplsTnlIf (u4L3Intf, (UINT4) i4IfIndex,
                                                 CFA_MPLS_TUNNEL, MPLS_TRUE)
                    == MPLS_FAILURE)
                {
                    IssSendError (pHttp, (CONST INT1 *) "Wrong Out Interface");
                    return WEB_FAILURE;
                }
            }
            if ((nmhTestv2MplsOutSegmentRowStatus (&u4ErrCode,
                                                   &OutSegmentIndex,
                                                   ISS_DESTROY)) ==
                SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "OutSegment is not destroyed");
                return WEB_FAILURE;
            }
            if ((nmhSetMplsOutSegmentRowStatus
                 (&OutSegmentIndex, ISS_DESTROY)) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "OutSegment is not destroyed");
                return WEB_FAILURE;
            }
            u1EntryDeleted = TRUE;
        }
    }
    while ((u1EntryDeleted == FALSE) &&
           ((nmhGetNextIndexMplsFTNTable (u4FtnIndex, &u4FtnIndex)) !=
            SNMP_FAILURE));

    return WEB_SUCCESS;
}

/******************************************************************************
* Function Name : IssMplsFtnLabelBinding 
* Description   : This routine is used for static binding of labels to IPv4 
*                 prefixes
* Input(s)      : pHttp       - Http Pointer
*                 u4Prefix    - ip address prefix
*                 u4Mask      - mask
*                 u4NextHop   - NextHop
*                 pu1InIfName - Incoming Interface name
*                 u4InLabel   - Incoming Label
*                 u4OutLabel  - Outgoing Label
* Output(s)     : None
* Return(s)     : WEB_SUCCESS or WEB_FAILURE
******************************************************************************/
INT4
IssMplsFtnLabelBinding (tHttp * pHttp, UINT4 u4Prefix, UINT4 u4Mask,
                        UINT4 u4NextHop, UINT1 *pu1InIfName, UINT4 u4InLabel,
                        UINT4 u4OutLabel)
{
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OID_TYPE      ActionPointer;
    UINT4               u4FtnIndex = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4XCIndex = 0;
    UINT4               u4InIndex = 0;
    UINT4               u4OutIndex = 0;
    INT4                i4InStatus = 0;
    INT4                i4OutStatus = 0;
    UINT4               u4InSegLabel = 0;
    UINT4               u4OutSegLabel = 0;
    BOOL1               bFtnEntryExists = FALSE;

    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4ActionType = 0;
    INT4                i4FtnActType = 1;
    static UINT4        au4ActionPointer[MPLS_TE_XC_TABLE_OFFSET];
    static UINT4        au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 2, 1, 10, 1, 4, 4, 0, 0, 0, 0, 4, 0, 0, 0,
        0, 4, 0, 0, 0, 0
    };
    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
    tRtInfoQueryMsg     RtQuery;
    tNetIpv4RtInfo      NetIpRtInfo;
    UINT4               u4RouteMask = 0xffffffff;
    UINT4               u4InIfIndex = 0;

    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = MPLS_INDEX_LENGTH;
    ActionPointer.pu4_OidList = au4ActionPointer;

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));
    MEMSET (au1InSegIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1XCIndex, 0, MPLS_INDEX_LENGTH);

    RtQuery.u4DestinationSubnetMask = u4RouteMask;
    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

    if ((u4NextHop != 0) &&
        (NetIpv4IfIsOurAddress (u4NextHop) == NETIPV4_SUCCESS))
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "Next Hop is same as self-ip address");
        return WEB_FAILURE;
    }

    if (u4InLabel != 0)
    {
#ifdef CFA_WANTED
        if ((CfaGetInterfaceIndexFromName (pu1InIfName,
                                           &u4InIfIndex)) == OSIX_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "InValid Interface Index");
            return WEB_FAILURE;
        }
#endif
    }
    /* Check whether an FTN for the same prefix and Mask */
    if ((IssMplsGetOrCreateFTNEntry (u4Prefix, u4Mask, &u4FtnIndex,
                                     &bFtnEntryExists, i4FtnActType))
        == WEB_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Failed in Configuring FTN");
        return WEB_FAILURE;
    }
    if (bFtnEntryExists)
    {
        /* set the InSeg Entry corresponding to the ftn entry as not in 
         * service. Index of the InSeg entry is obtained from the action 
         * pointer .Also corresponding XC entry's index is also obtained.*/

        /* Because an FTN entry already exists, it would be in 
         * 'not in service' state before entering here */

        if ((nmhGetMplsFTNActionType (u4FtnIndex, &i4ActionType))
            == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Failed in setting LSP Type");
            return WEB_FAILURE;
        }
        if (i4ActionType != 1)
        {
            IssSendError (pHttp, (CONST INT1 *) "Failed in setting LSP Type");
            return WEB_FAILURE;
        }
        if ((nmhGetMplsFTNActionPointer (u4FtnIndex, &ActionPointer))
            == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "ERROR in FTN Action Pointer");
            return WEB_FAILURE;
        }

        MPLS_OID_TO_INTEGER ((&ActionPointer), u4XCIndex,
                             MPLS_XC_INDEX_START_OFFSET);
        WEB_INTEGER_TO_OCTETSTRING (u4XCIndex, (&XCIndex));
        MPLS_OID_TO_INTEGER ((&ActionPointer), u4InIndex,
                             MPLS_IN_INDEX_START_OFFSET);
        WEB_INTEGER_TO_OCTETSTRING (u4InIndex, (&InSegmentIndex));
        MPLS_OID_TO_INTEGER ((&ActionPointer), u4OutIndex,
                             MPLS_OUT_INDEX_START_OFFSET);
        WEB_INTEGER_TO_OCTETSTRING (u4OutIndex, (&OutSegmentIndex));

        /* deleting XC entry with the InSeg index and OutSeg index obtained
         * from the action pointer */
        if ((nmhTestv2MplsXCRowStatus (&u4ErrCode, &XCIndex, &InSegmentIndex,
                                       &OutSegmentIndex,
                                       ISS_NOT_IN_SERVICE)) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Error in XC");
            return WEB_FAILURE;
        }
        if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                    &OutSegmentIndex,
                                    ISS_NOT_IN_SERVICE)) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Error in XC");
            return WEB_FAILURE;
        }
        if ((nmhGetMplsInSegmentRowStatus (&InSegmentIndex,
                                           &i4InStatus)) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Error in InSegment");
            return WEB_FAILURE;
        }

        if (u4InLabel != 0 && i4InStatus == ISS_ACTIVE)
        {
            nmhGetMplsInSegmentLabel (&InSegmentIndex, &u4InSegLabel);
            if (u4InLabel == u4InSegLabel)
            {
                /* Already an FTN Entry exists with the same inLabel,
                 * So, Making the entry that was made 'not in service' 
                 * ===> active*/
                IssMplsInOutXcFtnActive (&InSegmentIndex, &OutSegmentIndex,
                                         &XCIndex, u4FtnIndex, 1);
                IssSendError (pHttp, (CONST INT1 *)
                              "Entry with same in label already exists");
                return WEB_FAILURE;
            }
            if ((nmhTestv2MplsInSegmentRowStatus (&u4ErrCode, &InSegmentIndex,
                                                  ISS_NOT_IN_SERVICE)) ==
                SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "Wrong In Segment Row Status");
                return WEB_FAILURE;
            }
            if ((nmhSetMplsInSegmentRowStatus (&InSegmentIndex,
                                               ISS_NOT_IN_SERVICE)) ==
                SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "Wrong In Segment Row Status");
                return WEB_FAILURE;
            }
        }
        if ((nmhGetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                            &i4OutStatus)) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Wrong In Segment RowStatus");
            return WEB_FAILURE;
        }
        if (u4OutLabel != 0 && i4OutStatus == ISS_ACTIVE)
        {
            nmhGetMplsOutSegmentTopLabel (&OutSegmentIndex, &u4OutSegLabel);
            if (u4OutLabel == u4OutSegLabel)
            {
                /* Already an FTN Entry exists with the same outlabel,
                 * So, Making the entry that was made 'not in service' 
                 * ===> active*/
                IssMplsInOutXcFtnActive (&InSegmentIndex, &OutSegmentIndex,
                                         &XCIndex, u4FtnIndex, 2);
                IssSendError (pHttp, (CONST INT1 *)
                              "Entry with same in label already exists");
                return WEB_FAILURE;
            }

            if ((nmhTestv2MplsOutSegmentRowStatus (&u4ErrCode, &OutSegmentIndex,
                                                   ISS_NOT_IN_SERVICE))
                == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "Wrong In Segment RowStatus");
                return WEB_FAILURE;
            }
            if ((nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                                ISS_NOT_IN_SERVICE)) ==
                SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "Wrong In Segment RowStatus");
                return WEB_FAILURE;
            }
        }
    }
    if (IssMplsInFTNEntry (&InSegmentIndex,
                           u4InLabel, u4InIfIndex, bFtnEntryExists)
        == WEB_FAILURE)
    {
        if (!bFtnEntryExists)
        {
            nmhSetMplsFTNRowStatus (u4FtnIndex, ISS_DESTROY);
        }
        IssSendError (pHttp, (CONST INT1 *)
                      "Failed in Creating InSegment Entry");
        return WEB_FAILURE;
    }
    if (IssMplsOutFTNEntry (&OutSegmentIndex,
                            u4OutLabel, u4NextHop,
                            bFtnEntryExists) == WEB_FAILURE)
    {
        if (!bFtnEntryExists)
        {
            nmhSetMplsInSegmentRowStatus (&InSegmentIndex, ISS_DESTROY);
            nmhSetMplsFTNRowStatus (u4FtnIndex, ISS_DESTROY);
        }
        IssSendError (pHttp, (CONST INT1 *)
                      "Failed in Creating OutSegment Entry");
        return WEB_FAILURE;
    }
    do
    {
        if (!bFtnEntryExists)
        {
            /* create XC entry */
            if ((nmhGetMplsXCIndexNext (&XCIndex)) == SNMP_FAILURE)
            {
                i1RetVal = SNMP_FAILURE;
                break;
            }
            /* In segment index */
            if ((nmhTestv2MplsXCRowStatus (&u4ErrCode, &XCIndex,
                                           &InSegmentIndex, &OutSegmentIndex,
                                           ISS_CREATE_AND_WAIT)) ==
                SNMP_FAILURE)
            {
                i1RetVal = SNMP_FAILURE;
                break;
            }
            if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                        &OutSegmentIndex,
                                        ISS_CREATE_AND_WAIT)) == SNMP_FAILURE)
            {
                i1RetVal = SNMP_FAILURE;
                break;
            }
        }
        /* set the action pointer */
        WEB_OCTETSTRING_TO_INTEGER ((&XCIndex), u4XCIndex);
        WEB_OCTETSTRING_TO_INTEGER ((&OutSegmentIndex), u4OutIndex);
        WEB_OCTETSTRING_TO_INTEGER ((&InSegmentIndex), u4InIndex);

        MPLS_OCTETSTRING_TO_OID (au4XCTableOid,
                                 (&XCIndex), MPLS_XC_INDEX_START_OFFSET);
        MPLS_OCTETSTRING_TO_OID (au4XCTableOid,
                                 (&InSegmentIndex), MPLS_IN_INDEX_START_OFFSET);
        MPLS_OCTETSTRING_TO_OID (au4XCTableOid,
                                 (&OutSegmentIndex),
                                 MPLS_OUT_INDEX_START_OFFSET);

        ActionPointer.pu4_OidList = au4XCTableOid;
        ActionPointer.u4_Length = MPLS_TE_XC_TABLE_OFFSET;
        if ((nmhTestv2MplsFTNActionPointer (&u4ErrCode, u4FtnIndex,
                                            &ActionPointer)) == SNMP_FAILURE)
        {
            i1RetVal = SNMP_FAILURE;
            break;
        }
        if ((nmhSetMplsFTNActionPointer (u4FtnIndex, &ActionPointer))
            == SNMP_FAILURE)
        {
            i1RetVal = SNMP_FAILURE;
            break;
        }
        /*   all set operations were successful.break from the while loop */
        i1RetVal = SNMP_SUCCESS;
    }
    while (0);
    if (i1RetVal != SNMP_SUCCESS)
    {
        IssMplsDelTables (&InSegmentIndex, &OutSegmentIndex,
                          &XCIndex, u4InIfIndex, u4FtnIndex);
        IssSendError (pHttp, (CONST INT1 *) "Cannot Create FTN Entry");
        return WEB_FAILURE;
    }

    do
    {
        if (u4OutLabel != 0)
        {
            if ((nmhTestv2MplsOutSegmentRowStatus (&u4ErrCode, &OutSegmentIndex,
                                                   ISS_ACTIVE)) == SNMP_FAILURE)
            {
                break;
            }
            if ((nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                                ISS_ACTIVE)) == SNMP_FAILURE)
            {
                break;
            }
        }
        else                    /*(u4Inlabel != 0) */
        {
            if ((nmhTestv2MplsInSegmentRowStatus (&u4ErrCode, &InSegmentIndex,
                                                  ISS_ACTIVE)) == SNMP_FAILURE)
            {
                break;
            }
            if ((nmhSetMplsInSegmentRowStatus (&InSegmentIndex,
                                               ISS_ACTIVE)) == SNMP_FAILURE)
            {
                break;
            }
        }

        if ((nmhTestv2MplsXCRowStatus (&u4ErrCode, &XCIndex, &InSegmentIndex,
                                       &OutSegmentIndex,
                                       ISS_ACTIVE)) == SNMP_FAILURE)
        {
            break;
        }
        if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                    &OutSegmentIndex,
                                    ISS_ACTIVE)) == SNMP_FAILURE)
        {
            break;
        }
        /* Making all tables (FTN, XC) Active */
        /* set row status to active */
        if ((nmhTestv2MplsFTNRowStatus (&u4ErrCode, u4FtnIndex,
                                        ISS_ACTIVE)) == SNMP_FAILURE)
        {
            break;
        }
        if ((nmhSetMplsFTNRowStatus (u4FtnIndex, ISS_ACTIVE)) == SNMP_FAILURE)
        {
            break;
        }
        /* every set operation have been sucessful */
        return WEB_SUCCESS;
    }
    while (0);

    /* delete the created ftn and OutSegment and XC entry */
    IssMplsDelTables (&InSegmentIndex, &OutSegmentIndex, &XCIndex,
                      u4InIfIndex, u4FtnIndex);
    IssSendError (pHttp, (CONST INT1 *) "Failed in Configuring FTN");
    return WEB_FAILURE;
}

/******************************************************************************
* Function Name : IssMplsGetOrCreateFTNEntry 
* Description   : This routine is used to check whether any FTN with the passed
*                 prefix or mask exist. If any FTN exists, it returns with 
*                 bFtnEntryExists set as true, other wise tries to create one 
*                 FTN and returns bFtnEntryExists set as false.
* Input(s)      : u4Prefix   - ip address prefix
*                 u4Mask     - mask
*                 pu4FtnIndex  - FTN Index
*                 i4FtnActType  - FTNAction Type
* Output(s)     : bFtnEntryExists    - FTN Entry check
* Return(s)     : WEB_SUCCESS/WEB_FAILURE
******************************************************************************/

INT1
IssMplsGetOrCreateFTNEntry (UINT4 u4Prefix, UINT4 u4Mask,
                            UINT4 *pu4FtnIndex, BOOL1 * bFtnEntryExists,
                            INT4 i4FtnActType)
{
    tSNMP_OCTET_STRING_TYPE DestAddrMin;
    tSNMP_OCTET_STRING_TYPE DestAddrMax;
    tSNMP_OCTET_STRING_TYPE MaskBit;
    static UINT1        au1DestAddrMin[MPLS_INDEX_LENGTH];
    static UINT1        au1DestAddrMax[MPLS_INDEX_LENGTH];
    static UINT1        au1Mask[MPLS_INDEX_LENGTH];

    UINT4               u4MinAddr = u4Prefix;
    UINT4               u4MaxAddr = u4Mask;
    UINT4               u4ErrCode;
    UINT4               u4FtnIndex = 0;
    UINT1               u1EntryFound = FALSE;
    UINT1               u1Loop = FALSE;
    UINT4               u4GetPrefix = 0;
    UINT4               u4GetMask = 0;

    INT4                i4ActionType = 0;

    MaskBit.pu1_OctetList = au1Mask;
    MaskBit.i4_Length = MPLS_INDEX_LENGTH;
    DestAddrMin.pu1_OctetList = au1DestAddrMin;
    DestAddrMax.pu1_OctetList = au1DestAddrMax;

    /*TODO The following is to be modified with a better trie search
     * method, once tFtnEntry is visible to this file */
    do
    {
        if (u1Loop == FALSE)
        {
            /* get the first index of ftn table */
            if ((nmhGetFirstIndexMplsFTNTable (&u4FtnIndex)) == SNMP_FAILURE)
            {
                break;
            }
        }
        u1Loop = TRUE;
        u1EntryFound = FALSE;
        if (nmhGetMplsFTNDestAddrMin (u4FtnIndex, &DestAddrMin) == SNMP_FAILURE)
        {
            continue;
        }

        if (nmhGetMplsFTNDestAddrMax (u4FtnIndex, &DestAddrMax) == SNMP_FAILURE)
        {
            continue;
        }

        WEB_OCTETSTRING_TO_INTEGER ((&DestAddrMin), u4GetPrefix);
        WEB_OCTETSTRING_TO_INTEGER ((&DestAddrMax), u4GetMask);

        if ((nmhGetMplsFTNActionType (u4FtnIndex, &i4ActionType))
            == SNMP_FAILURE)
        {
            continue;
        }
        if (i4ActionType != 1)
        {
            continue;
        }

        if (!((u4Prefix == u4GetPrefix) && (u4Mask == u4GetMask)))
        {
            continue;
        }
        u1EntryFound = TRUE;
    }
    while ((u1EntryFound == FALSE) &&
           ((nmhGetNextIndexMplsFTNTable (u4FtnIndex, &u4FtnIndex)
             != SNMP_FAILURE)));

    /* The FTN Entry already exists. So, Make the row status 
     * from active to NOT_IN_SERVICE and return */
    if (u1EntryFound == TRUE)
    {
        *pu4FtnIndex = u4FtnIndex;
        *bFtnEntryExists = TRUE;
        if ((nmhTestv2MplsFTNRowStatus (&u4ErrCode, u4FtnIndex,
                                        ISS_NOT_IN_SERVICE)) == SNMP_FAILURE)
        {
            return WEB_FAILURE;
        }
        if ((nmhSetMplsFTNRowStatus
             (u4FtnIndex, ISS_NOT_IN_SERVICE)) == SNMP_FAILURE)
        {
            return WEB_FAILURE;
        }
        return WEB_SUCCESS;
    }

    /*  create an ftn entry */
    if ((nmhGetMplsFTNIndexNext (&u4FtnIndex)) == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }
    if ((nmhTestv2MplsFTNRowStatus (&u4ErrCode, u4FtnIndex,
                                    ISS_CREATE_AND_WAIT)) == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }
    if ((nmhSetMplsFTNRowStatus (u4FtnIndex, ISS_CREATE_AND_WAIT))
        == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }
    /* setting the action type */
    if (nmhTestv2MplsFTNActionType (&u4ErrCode, u4FtnIndex, i4FtnActType)
        == SNMP_FAILURE)
    {
        nmhSetMplsFTNRowStatus (u4FtnIndex, ISS_DESTROY);
        return WEB_FAILURE;
    }
    if (nmhSetMplsFTNActionType (u4FtnIndex, i4FtnActType) == SNMP_FAILURE)
    {
        nmhSetMplsFTNRowStatus (u4FtnIndex, ISS_DESTROY);
        return WEB_FAILURE;
    }

    /* set the address type */
    if (nmhTestv2MplsFTNAddrType (&u4ErrCode, u4FtnIndex, MPLS_IPV4_ADDR_TYPE)
        == SNMP_FAILURE)
    {
        nmhSetMplsFTNRowStatus (u4FtnIndex, ISS_DESTROY);
        return WEB_FAILURE;
    }
    if (nmhSetMplsFTNAddrType (u4FtnIndex, MPLS_IPV4_ADDR_TYPE) == SNMP_FAILURE)
    {
        nmhSetMplsFTNRowStatus (u4FtnIndex, ISS_DESTROY);
        return WEB_FAILURE;
    }

    /* set the min and max address generated from input prefix and mask 
     * to ftn dest min and max address*/

    WEB_INTEGER_TO_OCTETSTRING (u4MinAddr, (&DestAddrMin));
    WEB_INTEGER_TO_OCTETSTRING (u4MaxAddr, (&DestAddrMax));
    if ((nmhTestv2MplsFTNDestAddrMin (&u4ErrCode, u4FtnIndex,
                                      &DestAddrMin)) == SNMP_FAILURE)
    {
        nmhSetMplsFTNRowStatus (u4FtnIndex, ISS_DESTROY);
        return WEB_FAILURE;
    }
    if ((nmhSetMplsFTNDestAddrMin (u4FtnIndex, &DestAddrMin)) == SNMP_FAILURE)
    {
        nmhSetMplsFTNRowStatus (u4FtnIndex, ISS_DESTROY);
        return WEB_FAILURE;
    }
    if ((nmhTestv2MplsFTNDestAddrMax (&u4ErrCode, u4FtnIndex,
                                      &DestAddrMax)) == SNMP_FAILURE)
    {
        nmhSetMplsFTNRowStatus (u4FtnIndex, ISS_DESTROY);
        return WEB_FAILURE;
    }
    if ((nmhSetMplsFTNDestAddrMax (u4FtnIndex, &DestAddrMax)) == SNMP_FAILURE)
    {
        nmhSetMplsFTNRowStatus (u4FtnIndex, ISS_DESTROY);
        return WEB_FAILURE;
    }
    /* setting the mask bit */
    *(MaskBit.pu1_OctetList) = FTN_DEST_BITS_SET;
    if (nmhTestv2MplsFTNMask (&u4ErrCode, u4FtnIndex, &MaskBit) == SNMP_FAILURE)
    {
        nmhSetMplsFTNRowStatus (u4FtnIndex, ISS_DESTROY);
        return WEB_FAILURE;
    }
    if (nmhSetMplsFTNMask (u4FtnIndex, &MaskBit) == SNMP_FAILURE)
    {
        nmhSetMplsFTNRowStatus (u4FtnIndex, ISS_DESTROY);
        return WEB_FAILURE;
    }

    *pu4FtnIndex = u4FtnIndex;
    *bFtnEntryExists = FALSE;
    return WEB_SUCCESS;
}

/*******************************************************************************
* Function Name : IssMplsInFTNEntry  
* Description   : This routine creates or Modifies InSegment Entries for a 
*                 prefix.
* Input(s)      : pInSegmentIndex  - Insegment Index
*                 u4InLabel       - the In label
*                 bFtnEntryExists - FTN Entry check
* Output(s)     : None
* Return(s)     : WEB_SUCCESS/WEB_FAILURE
*******************************************************************************/
INT1
IssMplsInFTNEntry (tSNMP_OCTET_STRING_TYPE * pInSegmentIndex, UINT4 u4InLabel,
                   UINT4 u4InIfIndex, BOOL1 bFtnEntryExists)
{
    UINT4               u4ErrCode = 0;
    UINT4               u4MplsTnlIfIndex = 0;

    if (!bFtnEntryExists)
    {
        /* the other case that is no entry is found with matching address */
        /* create out segmnt entry */
        if ((nmhGetMplsInSegmentIndexNext (pInSegmentIndex)) == SNMP_FAILURE)
        {
            return WEB_FAILURE;
        }
        if ((nmhTestv2MplsInSegmentRowStatus (&u4ErrCode, pInSegmentIndex,
                                              ISS_CREATE_AND_WAIT))
            == SNMP_FAILURE)
        {
            return WEB_FAILURE;
        }
        if ((nmhSetMplsInSegmentRowStatus
             (pInSegmentIndex, ISS_CREATE_AND_WAIT)) == SNMP_FAILURE)
        {
            return WEB_FAILURE;
        }

        /* setting the InSegment ip addr family to ipv4 LSR_ADDR_IPV4 ==> 1 */
        if (nmhTestv2MplsInSegmentAddrFamily (&u4ErrCode, pInSegmentIndex,
                                              1) == SNMP_FAILURE)
        {
            nmhSetMplsInSegmentRowStatus (pInSegmentIndex, ISS_DESTROY);
            return WEB_FAILURE;
        }
        if (nmhSetMplsInSegmentAddrFamily (pInSegmentIndex, 1) == SNMP_FAILURE)
        {
            nmhSetMplsInSegmentRowStatus (pInSegmentIndex, ISS_DESTROY);
            return WEB_FAILURE;
        }
    }
    if (u4InLabel != 0)
    {
        /* Set label value in InSegment table */
        if ((nmhTestv2MplsInSegmentLabel (&u4ErrCode, pInSegmentIndex,
                                          u4InLabel)) == SNMP_FAILURE)
        {
            nmhSetMplsInSegmentRowStatus (pInSegmentIndex, ISS_DESTROY);
            return WEB_FAILURE;
        }
        if ((nmhSetMplsInSegmentLabel (pInSegmentIndex, u4InLabel))
            == SNMP_FAILURE)
        {
            nmhSetMplsInSegmentRowStatus (pInSegmentIndex, ISS_DESTROY);
            return WEB_FAILURE;
        }
        /* Set InSegment NPop to one */
        if ((nmhTestv2MplsInSegmentNPop (&u4ErrCode, pInSegmentIndex,
                                         1)) == SNMP_FAILURE)
        {
            nmhSetMplsInSegmentRowStatus (pInSegmentIndex, ISS_DESTROY);
            return WEB_FAILURE;
        }
        if ((nmhSetMplsInSegmentNPop (pInSegmentIndex, 1)) == SNMP_FAILURE)
        {
            nmhSetMplsInSegmentRowStatus (pInSegmentIndex, ISS_DESTROY);
            return WEB_FAILURE;
        }
        if (CfaGetFreeInterfaceIndex (&u4MplsTnlIfIndex, CFA_MPLS_TUNNEL) ==
            OSIX_FAILURE)
        {
            nmhSetMplsInSegmentRowStatus (pInSegmentIndex, ISS_DESTROY);
            return WEB_FAILURE;
        }
        if ((nmhTestv2MplsInSegmentInterface (&u4ErrCode,
                                              pInSegmentIndex,
                                              (INT4) u4MplsTnlIfIndex))
            == SNMP_FAILURE)
        {
            nmhSetMplsInSegmentRowStatus (pInSegmentIndex, ISS_DESTROY);
            return WEB_FAILURE;
        }
        if ((nmhSetMplsInSegmentInterface (pInSegmentIndex,
                                           (INT4) u4MplsTnlIfIndex))
            == SNMP_FAILURE)
        {
            nmhSetMplsInSegmentRowStatus (pInSegmentIndex, ISS_DESTROY);
            return WEB_FAILURE;
        }
        if (MplsCreateMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlIfIndex,
                                         CFA_MPLS_TUNNEL) == MPLS_FAILURE)
        {
            nmhSetMplsInSegmentRowStatus (pInSegmentIndex, ISS_DESTROY);
            return WEB_FAILURE;
        }
    }
    return WEB_SUCCESS;
}

/******************************************************************************
* Function Name : IssMplsOutFTNEntry 
* Description   : This routine creates or modifies out segment entries for a
*                 prefix.
* Input(s)      : pOutSegmentIndex - OutSegment Index
*                 u4OutLabel       - u4OutLabel
*                 u4NextHop        - NextHop
*                 bFtnEntryExists - A Boolean flag that shows whether FTN 
*                                   already exists or not
* Output(s)     : None
* Return(s)     : WEB_SUCCESS/WEB_FAILURE
******************************************************************************/

INT1
IssMplsOutFTNEntry (tSNMP_OCTET_STRING_TYPE * pOutSegmentIndex,
                    UINT4 u4OutLabel, UINT4 u4NextHop, BOOL1 bFtnEntryExists)
{
    tSNMP_OCTET_STRING_TYPE NextHopAddr;
    static UINT1        au1NextHopAddr[MPLS_INDEX_LENGTH];
    tRtInfoQueryMsg     RtQuery;
    tNetIpv4RtInfo      NetIpRtInfo;
    UINT4               u4RouteMask = 0xffffffff;
    UINT4               u4IfaceIndex = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4MplsTnlIfIndex = 0;
    UINT4               u4L3Intf = 0;

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

    NextHopAddr.pu1_OctetList = au1NextHopAddr;

    RtQuery.u4DestinationSubnetMask = u4RouteMask;
    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

    RtQuery.u4DestinationIpAddress = u4NextHop;
    if (u4NextHop != 0)
    {
        if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_SUCCESS)
        {
            if (NetIpv4GetCfaIfIndexFromPort (NetIpRtInfo.u4RtIfIndx,
                                              &u4IfaceIndex) == NETIPV4_FAILURE)
            {
                return WEB_FAILURE;
            }
        }
    }
    if (!bFtnEntryExists)
    {
        /* create out segmnt entry */
        if ((nmhGetMplsOutSegmentIndexNext (pOutSegmentIndex)) == SNMP_FAILURE)
        {
            return WEB_FAILURE;
        }
        if ((nmhTestv2MplsOutSegmentRowStatus (&u4ErrCode, pOutSegmentIndex,
                                               ISS_CREATE_AND_WAIT))
            == SNMP_FAILURE)
        {
            return WEB_FAILURE;
        }
        if ((nmhSetMplsOutSegmentRowStatus
             (pOutSegmentIndex, ISS_CREATE_AND_WAIT)) == SNMP_FAILURE)
        {
            return WEB_FAILURE;
        }
    }
    if (u4OutLabel != 0)
    {
        if ((nmhGetMplsOutSegmentInterface (pOutSegmentIndex,
                                            &i4MplsTnlIfIndex)) == SNMP_FAILURE)
        {
            nmhSetMplsOutSegmentRowStatus (pOutSegmentIndex, ISS_DESTROY);
            return WEB_FAILURE;
        }
        /* create an mpls interface and set this interface index in the 
         *  OutSegment table */
        if (i4MplsTnlIfIndex != 0)
        {
#ifdef CFA_WANTED
            if (CfaUtilGetIfIndexFromMplsTnlIf ((UINT4) i4MplsTnlIfIndex,
                                                &u4L3Intf, TRUE) == CFA_FAILURE)
            {
                nmhSetMplsOutSegmentRowStatus (pOutSegmentIndex, ISS_DESTROY);
                return WEB_FAILURE;
            }
#endif
            if (u4L3Intf != u4IfaceIndex)
            {
                if (MplsDeleteMplsIfOrMplsTnlIf (u4IfaceIndex,
                                                 (UINT4) i4MplsTnlIfIndex,
                                                 CFA_MPLS_TUNNEL, MPLS_TRUE)
                    == MPLS_FAILURE)
                {
                    nmhSetMplsOutSegmentRowStatus (pOutSegmentIndex,
                                                   ISS_DESTROY);
                    return WEB_FAILURE;
                }
            }
        }

        if ((nmhTestv2MplsOutSegmentTopLabel (&u4ErrCode, pOutSegmentIndex,
                                              u4OutLabel)) == SNMP_FAILURE)
        {
            nmhSetMplsOutSegmentRowStatus (pOutSegmentIndex, ISS_DESTROY);
            return WEB_FAILURE;
        }
        if ((nmhSetMplsOutSegmentTopLabel (pOutSegmentIndex, u4OutLabel))
            == SNMP_FAILURE)
        {
            nmhSetMplsOutSegmentRowStatus (pOutSegmentIndex, ISS_DESTROY);
            return WEB_FAILURE;
        }

        /* set the PushTopLabel in OutSegment table to true */
        if ((nmhTestv2MplsOutSegmentPushTopLabel
             (&u4ErrCode, pOutSegmentIndex, MPLS_TRUE)) == SNMP_FAILURE)
        {
            nmhSetMplsOutSegmentRowStatus (pOutSegmentIndex, ISS_DESTROY);
            return WEB_FAILURE;
        }
        if ((nmhSetMplsOutSegmentPushTopLabel (pOutSegmentIndex,
                                               MPLS_TRUE)) == SNMP_FAILURE)
        {
            nmhSetMplsOutSegmentRowStatus (pOutSegmentIndex, ISS_DESTROY);
            return WEB_FAILURE;
        }

        /* set the next hop addr type to ipv4 */
        if (nmhTestv2MplsOutSegmentNextHopAddrType (&u4ErrCode,
                                                    pOutSegmentIndex,
                                                    MPLS_IPV4_ADDR_TYPE) ==
            SNMP_FAILURE)
        {
            nmhSetMplsOutSegmentRowStatus (pOutSegmentIndex, ISS_DESTROY);
            return WEB_FAILURE;
        }
        if (nmhSetMplsOutSegmentNextHopAddrType (pOutSegmentIndex,
                                                 MPLS_IPV4_ADDR_TYPE)
            == SNMP_FAILURE)
        {
            nmhSetMplsOutSegmentRowStatus (pOutSegmentIndex, ISS_DESTROY);
            return WEB_FAILURE;
        }

        /* set the NextHop address */
        WEB_INTEGER_TO_OCTETSTRING (u4NextHop, (&NextHopAddr));
        if ((nmhTestv2MplsOutSegmentNextHopAddr (&u4ErrCode,
                                                 pOutSegmentIndex,
                                                 &NextHopAddr)) == SNMP_FAILURE)
        {
            nmhSetMplsOutSegmentRowStatus (pOutSegmentIndex, ISS_DESTROY);
            return WEB_FAILURE;
        }
        if ((nmhSetMplsOutSegmentNextHopAddr (pOutSegmentIndex,
                                              &NextHopAddr)) == SNMP_FAILURE)
        {
            nmhSetMplsOutSegmentRowStatus (pOutSegmentIndex, ISS_DESTROY);
            return WEB_FAILURE;
        }
        if (u4L3Intf != u4IfaceIndex)
        {
            if (CfaGetFreeInterfaceIndex ((UINT4 *) &i4MplsTnlIfIndex,
                                          CFA_MPLS_TUNNEL) == OSIX_FAILURE)
            {
                nmhSetMplsOutSegmentRowStatus (pOutSegmentIndex, ISS_DESTROY);
                return WEB_FAILURE;
            }
            if ((nmhTestv2MplsOutSegmentInterface (&u4ErrCode,
                                                   pOutSegmentIndex,
                                                   i4MplsTnlIfIndex))
                == SNMP_FAILURE)
            {
                nmhSetMplsOutSegmentRowStatus (pOutSegmentIndex, ISS_DESTROY);
                return WEB_FAILURE;
            }
            if ((nmhSetMplsOutSegmentInterface (pOutSegmentIndex,
                                                i4MplsTnlIfIndex))
                == SNMP_FAILURE)
            {
                nmhSetMplsOutSegmentRowStatus (pOutSegmentIndex, ISS_DESTROY);
                return WEB_FAILURE;
            }
            /* create an mpls interface and set this interface index in the 
             *  OutSegment table */
            if (MplsCreateMplsIfOrMplsTnlIf (u4IfaceIndex,
                                             (UINT4) i4MplsTnlIfIndex,
                                             CFA_MPLS_TUNNEL) == MPLS_FAILURE)
            {
                nmhSetMplsOutSegmentRowStatus (pOutSegmentIndex, ISS_DESTROY);
                return WEB_FAILURE;
            }
        }
    }
    return WEB_SUCCESS;
}

/******************************************************************************
* Function Name : IssMplsDelTables 
* Description   : This routine is used for deleting the MPLS tables
* Input(s)      : pInSegmentIndex  - InSegment Index
*                 pOutSegmentIndex - OutSegment Index
*                 pXCIndex         - Cross connect Index
*                 u4InIfIndex     - Incoming Interface Index
*                 u4FtnIndex      - FEC to NHLFE Index
* Output(s)     : None
* Return(s)     : NONE
******************************************************************************/

VOID
IssMplsDelTables (tSNMP_OCTET_STRING_TYPE * pInSegmentIndex,
                  tSNMP_OCTET_STRING_TYPE * pOutSegmentIndex,
                  tSNMP_OCTET_STRING_TYPE * pXCIndex, UINT4 u4InIfIndex,
                  UINT4 u4FtnIndex)
{
    INT4                i4MplsTnlIfIndex = 0;
    UINT4               u4OutIfIndex = 0;
    INT4                i4RetVal = 0;
    INT1                i1RetVal = 0;

    nmhSetMplsXCRowStatus (pXCIndex, pInSegmentIndex, pOutSegmentIndex,
                           ISS_DESTROY);
    i1RetVal =
        nmhGetMplsInSegmentInterface (pInSegmentIndex, &i4MplsTnlIfIndex);
    if (u4InIfIndex == 0)
    {
#ifdef CFA_WANTED
        i4RetVal = CfaUtilGetIfIndexFromMplsTnlIf ((UINT4) i4MplsTnlIfIndex,
                                                   &u4InIfIndex, TRUE);
#endif
    }
    MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, (UINT4) i4MplsTnlIfIndex,
                                 CFA_MPLS_TUNNEL, MPLS_TRUE);
    i1RetVal =
        nmhGetMplsOutSegmentInterface (pOutSegmentIndex, &i4MplsTnlIfIndex);
#ifdef CFA_WANTED
    i4RetVal =
        CfaUtilGetIfIndexFromMplsTnlIf ((UINT4) i4MplsTnlIfIndex, &u4OutIfIndex,
                                        TRUE);
#endif
    MplsDeleteMplsIfOrMplsTnlIf (u4OutIfIndex, (UINT4) i4MplsTnlIfIndex,
                                 CFA_MPLS_TUNNEL, MPLS_TRUE);
    nmhSetMplsOutSegmentRowStatus (pOutSegmentIndex, ISS_DESTROY);
    nmhSetMplsInSegmentRowStatus (pInSegmentIndex, ISS_DESTROY);
    nmhSetMplsFTNRowStatus (u4FtnIndex, ISS_DESTROY);
    UNUSED_PARAM (i1RetVal);
    UNUSED_PARAM (i4RetVal);

}

/*******************************************************************************
* Function Name : IssMplsInOutXcFtnActive 
* Description   : This routine is used for setting the XC, InSegment,
*                 OutSegment and FTN active.
* Input(s)      : pXCIndex  - Cross Connect Index
*                 pInSegmentIndex - In Segment Entry Index
*                 pOutSegmentIndex - Out Segment Entry Index
*                 u4FtnIndex - FTN Entry Index
*                 u4Flag - MPLS_CLI_FTN_EGRS_LBL_BINDING or 
*                 MPLS_CLI_FTN_INGRS_LBL_BINDING
* Output(s)     : None
* Return(s)     : WEB_SUCCESS/WEB_FAILURE
*******************************************************************************/
INT4
IssMplsInOutXcFtnActive (tSNMP_OCTET_STRING_TYPE * pInSegmentIndex,
                         tSNMP_OCTET_STRING_TYPE * pOutSegmentIndex,
                         tSNMP_OCTET_STRING_TYPE * pXCIndex,
                         UINT4 u4FtnIndex, UINT4 u4Flag)
{
    INT1                i1RetVal = CLI_SUCCESS;
    UINT4               u4ErrCode = 0;
    INT4                i4MplsTnlIfIndex = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetVal = 0;
    INT1                i1RetValue = 0;
    do
    {
        if (nmhTestv2MplsXCRowStatus (&u4ErrCode, pXCIndex, pInSegmentIndex,
                                      pOutSegmentIndex, ISS_ACTIVE)
            == SNMP_FAILURE)
        {
            i1RetVal = WEB_FAILURE;
            break;
        }
        if (nmhSetMplsXCRowStatus (pXCIndex, pInSegmentIndex, pOutSegmentIndex,
                                   ISS_ACTIVE) == SNMP_FAILURE)
        {
            i1RetVal = WEB_FAILURE;
            break;
        }

        if (nmhTestv2MplsFTNRowStatus (&u4ErrCode, u4FtnIndex,
                                       ISS_ACTIVE) == SNMP_FAILURE)
        {
            i1RetVal = WEB_FAILURE;
            break;
        }

        if (nmhSetMplsFTNRowStatus (u4FtnIndex, ISS_ACTIVE) == SNMP_FAILURE)
        {
            i1RetVal = WEB_FAILURE;
            break;
        }
        return WEB_SUCCESS;
    }
    while (0);

    if (u4Flag == 1)
    {
        i1RetValue =
            nmhGetMplsInSegmentInterface (pInSegmentIndex, &i4MplsTnlIfIndex);
#ifdef CFA_WANTED
        i4RetVal =
            CfaUtilGetIfIndexFromMplsTnlIf ((UINT4) i4MplsTnlIfIndex,
                                            &u4IfIndex, TRUE);
#endif
        MplsDeleteMplsIfOrMplsTnlIf (u4IfIndex, (UINT4) i4MplsTnlIfIndex,
                                     CFA_MPLS_TUNNEL, MPLS_TRUE);
        nmhSetMplsInSegmentRowStatus (pInSegmentIndex, ISS_DESTROY);
    }
    else
    {
        i1RetValue =
            nmhGetMplsOutSegmentInterface (pOutSegmentIndex, &i4MplsTnlIfIndex);
#ifdef CFA_WANTED
        i4RetVal =
            CfaUtilGetIfIndexFromMplsTnlIf ((UINT4) i4MplsTnlIfIndex,
                                            &u4IfIndex, TRUE);
#endif
        MplsDeleteMplsIfOrMplsTnlIf (u4IfIndex, (UINT4) i4MplsTnlIfIndex,
                                     CFA_MPLS_TUNNEL, MPLS_TRUE);
        nmhSetMplsOutSegmentRowStatus (pOutSegmentIndex, ISS_DESTROY);
    }
    nmhSetMplsXCRowStatus (pXCIndex, pInSegmentIndex, pOutSegmentIndex,
                           ISS_DESTROY);
    nmhSetMplsFTNRowStatus (u4FtnIndex, ISS_DESTROY);
    return WEB_FAILURE;
    UNUSED_PARAM (i1RetVal);
    UNUSED_PARAM (i4RetVal);
    UNUSED_PARAM (i1RetValue);
}

/*******************************************************************************
*  Function Name : IssProcessMplsXcPage 
*  Description   : This function processes the request coming for the Mpls XC 
*                  Page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/

VOID
IssProcessMplsXcPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessMplsXcGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessMplsXcSet (pHttp);
    }
}

/*******************************************************************************
*  Function Name : IssProcessMplsXcGet
*  Description   : This function shows the XC Entries in the web page and 
*                  enables user to enter entries to configure XC
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/

VOID
IssProcessMplsXcGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE InSegmentIndexPrev;
    tSNMP_OCTET_STRING_TYPE XCIndexPrev;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndexPrev;
    tSNMP_OCTET_STRING_TYPE NextHopAddr;
    tCfaIfInfo          CfaIfInfo;
    UINT1              *pu1Temp = NULL;
    UINT4               u4InSegLabel;
    UINT4               u4OutSegTopLabel;
    UINT4               u4XCIndex = 0;
    UINT4               u4InIndex = 0;
    UINT4               u4OutIndex = 0;
    UINT4               u4NextHop = 0;
    UINT4               u4Temp;
    INT4                i4InIfIndex = 0;
    INT4                i4OutIfIndex = 0;
    INT4                i4XCOwner;
    UINT4               u4L3VlanIf = 0;

    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { 0 };
    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1NextHopAddr[MPLS_INDEX_LENGTH];
    static UINT1        au1InSegIndexPrev[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndexPrev[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndexPrev[MPLS_INDEX_LENGTH];

    pu1Temp = &au1IfName[0];

    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = MPLS_INDEX_LENGTH;
    NextHopAddr.pu1_OctetList = au1NextHopAddr;
    InSegmentIndexPrev.pu1_OctetList = au1InSegIndexPrev;
    OutSegmentIndexPrev.pu1_OctetList = au1OutSegIndexPrev;
    XCIndexPrev.pu1_OctetList = au1XCIndexPrev;

    pHttp->i4Write = 0;

    /* Display the webpage till the in interface scroll bar */
    IssPrintL3Interfaces (pHttp, 0);

    /* Display the webpage from in interface scroll bar to out 
     * interface scroll bar */
    IssPrintL3Interfaces (pHttp, 0);

    /* Now, Display the webpage till TABLE of second form */
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    u4Temp = (UINT4) pHttp->i4Write;

    /* Scan the XC entry, Get the first index of XC table 
     * ,in seg and out seg table */
    while (1)
    {
        /* Not executed First time */
        if ((u4XCIndex != 0) &&
            ((nmhGetNextIndexMplsXCTable
              (&XCIndexPrev, &XCIndex, &InSegmentIndexPrev, &InSegmentIndex,
               &OutSegmentIndexPrev, &OutSegmentIndex)) == SNMP_FAILURE))
        {
            break;
        }

        /* Only executed first time */
        if ((u4XCIndex == 0) &&
            ((nmhGetFirstIndexMplsXCTable (&XCIndex, &InSegmentIndex,
                                           &OutSegmentIndex)) == SNMP_FAILURE))
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            return;
        }

        WEB_OCTETSTRING_TO_INTEGER ((&InSegmentIndex), u4InIndex);
        WEB_OCTETSTRING_TO_INTEGER ((&OutSegmentIndex), u4OutIndex);
        WEB_OCTETSTRING_TO_INTEGER ((&XCIndex), u4XCIndex);

        /* Move the current index of InSegment, OutSegment, and XC Table
         * to a temporary value to be used as input GetNextRoutine */
        MEMCPY (XCIndexPrev.pu1_OctetList, XCIndex.pu1_OctetList,
                XCIndex.i4_Length);
        XCIndexPrev.i4_Length = XCIndex.i4_Length;

        MEMCPY (InSegmentIndexPrev.pu1_OctetList, InSegmentIndex.pu1_OctetList,
                InSegmentIndex.i4_Length);
        InSegmentIndexPrev.i4_Length = InSegmentIndex.i4_Length;

        MEMCPY (OutSegmentIndexPrev.pu1_OctetList,
                OutSegmentIndex.pu1_OctetList, OutSegmentIndex.i4_Length);
        OutSegmentIndexPrev.i4_Length = OutSegmentIndex.i4_Length;

        /* Fetch the XCOwner, InSegmentRowStatus, OutSegmentRowStatus,
         * if fetching any of it fails or if they have value that is not
         * required, try and fetch next index */
        if (nmhGetMplsXCOwner (&XCIndex, &InSegmentIndex,
                               &OutSegmentIndex, &i4XCOwner) == SNMP_FAILURE)
        {
            continue;
        }
        if (u4InIndex == 0 || u4OutIndex == 0 || i4XCOwner != 3)
        {
            continue;
        }

        /* Now, we have required Index, get the InInterface and OutInterface. 
         * Check whether they are MPLS enabled, if yes Proceed else fetch 
         * the next index. */
        if (nmhGetMplsInSegmentInterface (&InSegmentIndex, &i4InIfIndex)
            == SNMP_FAILURE)
        {
            continue;
        }
#ifdef CFA_WANTED
        if (CfaUtilGetIfIndexFromMplsTnlIf
            ((UINT4) i4InIfIndex, &u4L3VlanIf, TRUE) == CFA_FAILURE)
        {
            continue;
        }
#endif
        if (nmhGetMplsOutSegmentInterface (&OutSegmentIndex, &i4OutIfIndex)
            == SNMP_FAILURE)
        {
            continue;
        }
#ifdef CFA_WANTED
        if (CfaUtilGetIfIndexFromMplsTnlIf
            ((UINT4) i4OutIfIndex, &u4L3VlanIf, TRUE) == CFA_FAILURE)
        {
            continue;
        }
#endif
        /* Both In and Out Interface are MPLS enabled, so, fetch in label,
         * out label, Next hop and start Display */
        if (nmhGetMplsInSegmentLabel (&InSegmentIndex, &u4InSegLabel)
            == SNMP_FAILURE)
        {
            continue;
        }
        if (nmhGetMplsOutSegmentTopLabel (&OutSegmentIndex,
                                          &u4OutSegTopLabel) == SNMP_FAILURE)
        {
            continue;
        }
        if (nmhGetMplsOutSegmentNextHopAddr (&OutSegmentIndex, &NextHopAddr)
            == SNMP_FAILURE)
        {
            continue;
        }
        WEB_OCTETSTRING_TO_INTEGER ((&NextHopAddr), u4NextHop);

        pHttp->i4Write = (INT4) u4Temp;

        /* Every detail needed has been fetched, so display in the web page */

        /* Display In Label */
        STRCPY (pHttp->au1KeyString, "INLABEL");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (u4InSegLabel != 0)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4InSegLabel);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* Display In Interface */
        STRCPY (pHttp->au1KeyString, "IN_INTERFACE_ID");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4InIfIndex != 0)
        {
#ifdef CFA_WANTED
            CfaUtilGetIfIndexFromMplsTnlIf ((UINT4) i4InIfIndex, &u4L3VlanIf,
                                            TRUE);
            CfaGetIfInfo (u4L3VlanIf, &CfaIfInfo);
#endif
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", CfaIfInfo.au1IfName);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* Display Out Label */
        STRCPY (pHttp->au1KeyString, "OUTLABEL");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (u4OutSegTopLabel == 0)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "explicit-null");
        }
        else if (u4OutSegTopLabel == 3)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "implicit-null");
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4OutSegTopLabel);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* Display Out Interface */
        STRCPY (pHttp->au1KeyString, "OUT_INTERFACE_ID");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4OutIfIndex != 0)
        {
#ifdef CFA_WANTED
            CfaUtilGetIfIndexFromMplsTnlIf ((UINT4) i4OutIfIndex, &u4L3VlanIf,
                                            TRUE);
            CfaGetIfInfo (u4L3VlanIf, &CfaIfInfo);
#endif
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", CfaIfInfo.au1IfName);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* Display Next Hop */
        STRCPY (pHttp->au1KeyString, "NEXT_HOP");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (u4NextHop != 0)
        {
            WEB_CONVERT_IPADDR_TO_STR (pu1Temp, u4NextHop);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1Temp);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

        /* Fetch the Next Entry */
    }

    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    /* Display all the information in the html page after the Table stop flag */
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*******************************************************************************
*  Function Name : IssProcessMplsXcSet 
*  Description   : This function processes the request coming for
*                  Mpls XC Page and tries to set the XC Table entries 
*                  with the given use input from the web page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/

VOID
IssProcessMplsXcSet (tHttp * pHttp)
{
    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;
    UINT4               u4Mask = 0xffffffff;
    UINT4               u4InLabel = 0;
    UINT4               u4OutLabel = 0;
    UINT4               u4NextHop = 0;
    UINT4               u4InIndex = 0;
    UINT4               u4OutIndex = 0;
    UINT4               u4MplsIfIndex = 0;
    INT4                i4RetVal = WEB_SUCCESS;

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

    STRCPY (pHttp->au1Name, "INLABEL");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4InLabel = (UINT4) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "IN_INTERFACE_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "-") != 0)
    {
        issDecodeSpecialChar (pHttp->au1Value);
#ifdef CFA_WANTED
        if (CfaGetInterfaceIndexFromName (pHttp->au1Value, &u4InIndex)
            == OSIX_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Wrong Index");
            return;
        }
        if (CfaUtilGetMplsIfFromIfIndex (u4InIndex, &u4MplsIfIndex, TRUE) ==
            CFA_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *)
                          "Mpls is not enabled on Incoming interface");
            return;
        }
#endif
    }
    else
    {
        u4InIndex = 0;
    }

    STRCPY (pHttp->au1Name, "OUTLABEL");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4OutLabel = (UINT4) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "OUT_INTERFACE_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "-") != 0)
    {
        issDecodeSpecialChar (pHttp->au1Value);
#ifdef CFA_WANTED
        if (CfaGetInterfaceIndexFromName (pHttp->au1Value, &u4OutIndex)
            == OSIX_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Wrong Index");
            return;
        }
        if (CfaUtilGetMplsIfFromIfIndex (u4OutIndex, &u4MplsIfIndex, TRUE) ==
            CFA_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *)
                          "Mpls is not enabled on Outgoing interface");
            return;
        }
#endif
    }
    else
    {
        u4OutIndex = 0;
    }

    STRCPY (pHttp->au1Name, "NEXT_HOP");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if ((STRCMP (pHttp->au1Value, "...") == 0) ||
        (STRCMP (pHttp->au1Value, "-") == 0))
    {
        u4NextHop = 0;
    }
    else
    {
        u4NextHop = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
    }

    STRCPY (pHttp->au1Name, "DELETE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Delete") != 0)
    {
        RtQuery.u4DestinationIpAddress = u4NextHop;
        RtQuery.u4DestinationSubnetMask = u4Mask;
        RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

        if (NetIpv4IfIsOurAddress (u4NextHop) == NETIPV4_SUCCESS)
        {
            IssSendError (pHttp, (CONST INT1 *)
                          "Next Hop is same as Self-Ip Address");
            return;
        }
        if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) != NETIPV4_SUCCESS)
        {
            IssSendError (pHttp, (CONST INT1 *)
                          "Next Hop Not Directly Connected");
            return;
        }
        if (u4InIndex == u4OutIndex)
        {
            IssSendError (pHttp, (CONST INT1 *) "In Index is same as OutIndex");
            return;
        }
        i4RetVal = IssMplsCreateXc (pHttp, u4InLabel, u4InIndex, u4OutLabel,
                                    u4OutIndex, u4NextHop);
    }
    else
    {
        i4RetVal = IssMplsDeleteXc (pHttp, u4InLabel, u4OutLabel, u4NextHop);
    }

    if (i4RetVal == WEB_SUCCESS)
    {
        IssProcessMplsXcGet (pHttp);
    }
    return;
}

/*****************************************************************************
*  Function Name : IssMplsCreateXc
*  Description   : This Routine Creates an XC Entry
*                  in the web page.
*  Input(s)      : pHttp        - Pointer to the global HTTP data structure.
*                  u4InLabel    - In Label
*                  u4InIfIndex  - In Interface Index
*                  u4OutLabel   - Out Label
*                  u4OutIfIndex - Out Interface Index.
*                  u4NextHop    - Next Hop
*  Output(s)     : None.
*  Return Values : WEB_SUCCESS or WEB_FAILURE
*****************************************************************************/

INT4
IssMplsCreateXc (tHttp * pHttp, UINT4 u4InLabel, UINT4 u4InIfIndex,
                 UINT4 u4OutLabel, UINT4 u4OutIfIndex, UINT4 u4NextHop)
{
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE NextHopAddr;
    INT4                i4InSegmntNPop;
    UINT4               u4ErrCode = 0;
    UINT4               u4MplsTnlInIfIndex = 0;
    UINT4               u4MplsTnlOutIfIndex = 0;
    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH] = { 0 };
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH] = { 0 };
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH] = { 0 };
    static UINT1        au1NextHopAddr[MPLS_INDEX_LENGTH] = { 0 };

    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = MPLS_INDEX_LENGTH;
    NextHopAddr.pu1_OctetList = au1NextHopAddr;

    /* create a new in seg , out seg and XC table entry */
    if ((nmhGetMplsInSegmentIndexNext (&InSegmentIndex)) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "No In Segment Index Available");
        return WEB_FAILURE;
    }
    if ((nmhTestv2MplsInSegmentRowStatus (&u4ErrCode, &InSegmentIndex,
                                          ISS_CREATE_AND_WAIT)) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Wrong In Segment");
        return WEB_FAILURE;
    }
    if ((nmhSetMplsInSegmentRowStatus
         (&InSegmentIndex, ISS_CREATE_AND_WAIT)) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Wrong In Segment");
        return WEB_FAILURE;
    }

    /* create an OutSeg entry */
    if ((nmhGetMplsOutSegmentIndexNext (&OutSegmentIndex)) == SNMP_FAILURE)
    {
        /* delete the created InSeg entry */
        nmhSetMplsInSegmentRowStatus (&InSegmentIndex, ISS_DESTROY);
        IssSendError (pHttp, (CONST INT1 *) "No Out Segment Index Available");
        return WEB_FAILURE;
    }
    if ((nmhTestv2MplsOutSegmentRowStatus (&u4ErrCode, &OutSegmentIndex,
                                           ISS_CREATE_AND_WAIT)) ==
        SNMP_FAILURE)
    {
        /* delete the created InSeg entry */
        nmhSetMplsInSegmentRowStatus (&InSegmentIndex, ISS_DESTROY);
        IssSendError (pHttp, (CONST INT1 *) "Wrong Out Segment");
        return WEB_FAILURE;
    }
    if ((nmhSetMplsOutSegmentRowStatus
         (&OutSegmentIndex, ISS_CREATE_AND_WAIT)) == SNMP_FAILURE)
    {
        /* delete the created InSeg entry */
        nmhSetMplsInSegmentRowStatus (&InSegmentIndex, ISS_DESTROY);
        IssSendError (pHttp, (CONST INT1 *) "Wrong Out Segment");
        return WEB_FAILURE;
    }

    /* create an XC entry */
    if ((nmhGetMplsXCIndexNext (&XCIndex)) == SNMP_FAILURE)
    {
        /* delete the created InSeg and OutSeg entry */
        nmhSetMplsInSegmentRowStatus (&InSegmentIndex, ISS_DESTROY);
        nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex, ISS_DESTROY);
        IssSendError (pHttp, (CONST INT1 *) "No XC Segment Index Available");
        return WEB_FAILURE;
    }
    if ((nmhTestv2MplsXCRowStatus (&u4ErrCode, &XCIndex, &InSegmentIndex,
                                   &OutSegmentIndex,
                                   ISS_CREATE_AND_WAIT)) == SNMP_FAILURE)
    {
        /* delete the created InSeg and OutSeg entry */
        nmhSetMplsInSegmentRowStatus (&InSegmentIndex, ISS_DESTROY);
        nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex, ISS_DESTROY);
        IssSendError (pHttp, (CONST INT1 *) "Wrong XC");
        return WEB_FAILURE;
    }
    if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                &OutSegmentIndex,
                                ISS_CREATE_AND_WAIT)) == SNMP_FAILURE)
    {
        /* delete the created InSeg and OutSeg entry */
        nmhSetMplsInSegmentRowStatus (&InSegmentIndex, ISS_DESTROY);
        nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex, ISS_DESTROY);
        IssSendError (pHttp, (CONST INT1 *) "Wrong XC");
        return WEB_FAILURE;
    }

    do
    {
        /* OUTSEGMENT TABLE CONFIGURATION */
        /* 2. set inlabel value in seg table */
        if ((nmhTestv2MplsInSegmentLabel (&u4ErrCode, &InSegmentIndex,
                                          u4InLabel)) == SNMP_FAILURE)
        {
            break;
        }
        if ((nmhSetMplsInSegmentLabel (&InSegmentIndex, u4InLabel))
            == SNMP_FAILURE)
        {
            break;
        }
        /* 3.set InSegment NPop */
        i4InSegmntNPop = 1;
        if ((nmhTestv2MplsInSegmentNPop (&u4ErrCode, &InSegmentIndex,
                                         i4InSegmntNPop)) == SNMP_FAILURE)
        {
            break;
        }
        if ((nmhSetMplsInSegmentNPop (&InSegmentIndex, i4InSegmntNPop))
            == SNMP_FAILURE)
        {
            break;
        }
        /* setting the InSegment ip addr family to ipv4 */
        if (nmhTestv2MplsInSegmentAddrFamily (&u4ErrCode, &InSegmentIndex,
                                              MPLS_IPV4_ADDR_TYPE)
            == SNMP_FAILURE)
        {
            break;
        }
        if (nmhSetMplsInSegmentAddrFamily (&InSegmentIndex, MPLS_IPV4_ADDR_TYPE)
            == SNMP_FAILURE)
        {
            break;
        }
        /* OUTSEGMENT TABLE CONFIGURATION */
        /* 5. set out segmnt top label */
        if (nmhTestv2MplsOutSegmentTopLabel (&u4ErrCode, &OutSegmentIndex,
                                             u4OutLabel) == SNMP_FAILURE)
        {
            break;
        }
        if (nmhSetMplsOutSegmentTopLabel (&OutSegmentIndex, u4OutLabel)
            == SNMP_FAILURE)
        {
            break;
        }
        /* 6. set out segmnt push top label to true */
        if (nmhTestv2MplsOutSegmentPushTopLabel (&u4ErrCode, &OutSegmentIndex,
                                                 MPLS_TRUE) == SNMP_FAILURE)
        {
            break;
        }
        if (nmhSetMplsOutSegmentPushTopLabel (&OutSegmentIndex,
                                              MPLS_TRUE) == SNMP_FAILURE)
        {
            break;
        }
        /* set the next hop addr type to ipv4 */
        if (nmhTestv2MplsOutSegmentNextHopAddrType (&u4ErrCode,
                                                    &OutSegmentIndex,
                                                    MPLS_IPV4_ADDR_TYPE)
            == SNMP_FAILURE)
        {
            break;
        }
        if (nmhSetMplsOutSegmentNextHopAddrType (&OutSegmentIndex,
                                                 MPLS_IPV4_ADDR_TYPE) ==
            SNMP_FAILURE)
        {
            break;
        }
        /* 7. set nexthop address */
        WEB_INTEGER_TO_OCTETSTRING (u4NextHop, (&NextHopAddr));
        if (nmhTestv2MplsOutSegmentNextHopAddr (&u4ErrCode, &OutSegmentIndex,
                                                &NextHopAddr) == SNMP_FAILURE)
        {
            break;
        }
        if (nmhSetMplsOutSegmentNextHopAddr (&OutSegmentIndex,
                                             &NextHopAddr) == SNMP_FAILURE)
        {
            break;
        }
        if (CfaGetFreeInterfaceIndex (&u4MplsTnlInIfIndex,
                                      CFA_MPLS_TUNNEL) == OSIX_FAILURE)
        {
            break;
        }
        if ((nmhTestv2MplsInSegmentInterface (&u4ErrCode,
                                              &InSegmentIndex,
                                              (INT4) u4MplsTnlInIfIndex))
            == SNMP_FAILURE)
        {
            break;
        }
        if ((nmhSetMplsInSegmentInterface (&InSegmentIndex,
                                           (INT4) u4MplsTnlInIfIndex))
            == SNMP_FAILURE)
        {
            break;
        }
        if (MplsCreateMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL) == MPLS_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *)
                          "Enable MPLS on the Incoming interface !!!");
            break;
        }
        /* 9.set the row status of the InSegment tables as active */
        if ((nmhTestv2MplsInSegmentRowStatus (&u4ErrCode, &InSegmentIndex,
                                              ISS_ACTIVE)) == SNMP_FAILURE)
        {
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            break;
        }
        if ((nmhSetMplsInSegmentRowStatus (&InSegmentIndex, ISS_ACTIVE))
            == SNMP_FAILURE)
        {
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            break;
        }
        if (CfaGetFreeInterfaceIndex (&u4MplsTnlOutIfIndex,
                                      CFA_MPLS_TUNNEL) == OSIX_FAILURE)
        {
            break;
        }
        if ((nmhTestv2MplsOutSegmentInterface (&u4ErrCode,
                                               &OutSegmentIndex,
                                               (INT4) u4MplsTnlOutIfIndex))
            == SNMP_FAILURE)
        {
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            break;
        }
        if ((nmhSetMplsOutSegmentInterface (&OutSegmentIndex,
                                            (INT4) u4MplsTnlOutIfIndex))
            == SNMP_FAILURE)
        {
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            break;
        }
        /*4. Create a new MPLS Tunnel Interface and stack over MPLS If */
        if (MplsCreateMplsIfOrMplsTnlIf (u4OutIfIndex, u4MplsTnlOutIfIndex,
                                         CFA_MPLS_TUNNEL) == MPLS_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *)
                          "Enable MPLS on the Outgoing interface!!!");
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            break;
        }
        if ((nmhTestv2MplsOutSegmentRowStatus (&u4ErrCode, &OutSegmentIndex,
                                               ISS_ACTIVE)) == SNMP_FAILURE)
        {
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            MplsDeleteMplsIfOrMplsTnlIf (u4OutIfIndex, u4MplsTnlOutIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            break;
        }
        if ((nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                            ISS_ACTIVE)) == SNMP_FAILURE)
        {
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            MplsDeleteMplsIfOrMplsTnlIf (u4OutIfIndex, u4MplsTnlOutIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            break;
        }
        if ((nmhTestv2MplsXCRowStatus (&u4ErrCode, &XCIndex, &InSegmentIndex,
                                       &OutSegmentIndex,
                                       ISS_ACTIVE)) == SNMP_FAILURE)
        {
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            MplsDeleteMplsIfOrMplsTnlIf (u4OutIfIndex, u4MplsTnlOutIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            break;
        }
        if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                    &OutSegmentIndex,
                                    ISS_ACTIVE)) == SNMP_FAILURE)
        {
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            MplsDeleteMplsIfOrMplsTnlIf (u4OutIfIndex, u4MplsTnlOutIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            break;
        }
        /* all set operations have been successful */
        return WEB_SUCCESS;
    }
    while (0);
    /* delete the created InSeg and OutSeg and XC entry */
    nmhSetMplsInSegmentRowStatus (&InSegmentIndex, ISS_DESTROY);
    nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex, ISS_DESTROY);
    nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                           ISS_DESTROY);
    IssSendError (pHttp, (CONST INT1 *) "XC Not Created");
    return WEB_FAILURE;
}

/*****************************************************************************
*  Function Name : IssMplsDeleteXc
*  Description   : This function deletes an XC Entry
*                  in the web page.
*  Input(s)      : pHttp       - Pointer to the global HTTP data structure.
*                  u4InLabel   - In Label
*                  u4OutLabel  - Out Label
*                  u4NextHop   - Next Hop
*  Output(s)     : None.
*  Return Values : WEB_SUCCESS or WEB_FAILURE
*****************************************************************************/
INT4
IssMplsDeleteXc (tHttp * pHttp, UINT4 u4InLabel, UINT4 u4OutLabel,
                 UINT4 u4NextHop)
{
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE InSegmentIndexPrev;
    tSNMP_OCTET_STRING_TYPE XCIndexPrev;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndexPrev;
    tSNMP_OCTET_STRING_TYPE NextHopAddr;
    INT4                i4IfIndex = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4InSegLabel = 0;
    UINT4               u4NextHopAddr = 0;
    UINT4               u4OutSegLabel = 0;
    UINT4               u4L3Intf = 0;
    BOOL1               bEntryPresent = FALSE;
    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1NextHopAddr[MPLS_INDEX_LENGTH];
    static UINT1        au1InSegIndexPrev[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndexPrev[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndexPrev[MPLS_INDEX_LENGTH];

    MEMSET (&InSegmentIndex, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&XCIndex, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&OutSegmentIndex, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&InSegmentIndexPrev, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&XCIndexPrev, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&OutSegmentIndexPrev, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextHopAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = MPLS_INDEX_LENGTH;
    NextHopAddr.pu1_OctetList = au1NextHopAddr;
    InSegmentIndexPrev.pu1_OctetList = au1InSegIndexPrev;
    OutSegmentIndexPrev.pu1_OctetList = au1OutSegIndexPrev;
    XCIndexPrev.pu1_OctetList = au1XCIndexPrev;

    /* 1. scan the XC entry */
    /* get the first index of XC table , in seg and out seg table */
    if (nmhGetFirstIndexMplsXCTable (&XCIndex, &InSegmentIndex,
                                     &OutSegmentIndex) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "No XC Entry Exists");
        return WEB_FAILURE;
    }
    do
    {
        bEntryPresent = FALSE;
        if (nmhGetMplsInSegmentLabel (&InSegmentIndex, &u4InSegLabel)
            == SNMP_FAILURE)
        {
            continue;
        }
        if (nmhGetMplsOutSegmentTopLabel (&OutSegmentIndex,
                                          &u4OutSegLabel) == SNMP_FAILURE)
        {
            continue;
        }
        if (nmhGetMplsOutSegmentNextHopAddr (&OutSegmentIndex, &NextHopAddr)
            == SNMP_FAILURE)
        {
            continue;
        }
        WEB_OCTETSTRING_TO_INTEGER ((&NextHopAddr), u4NextHopAddr);

        if (u4InLabel == u4InSegLabel && u4OutLabel == u4OutSegLabel &&
            u4NextHop == u4NextHopAddr)
        {
            bEntryPresent = TRUE;
            if ((nmhTestv2MplsXCRowStatus (&u4ErrCode, &XCIndex,
                                           &InSegmentIndex, &OutSegmentIndex,
                                           ISS_DESTROY)) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "XC Deletion Failed");
                return WEB_FAILURE;
            }
            if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                        &OutSegmentIndex,
                                        ISS_DESTROY)) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "XC Deletion Failed");
                return WEB_FAILURE;
            }
            if (nmhGetMplsInSegmentInterface (&InSegmentIndex, &i4IfIndex)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "Wrong In MPLS Tunnel If Index");
                return WEB_FAILURE;
            }
#ifdef CFA_WANTED
            if ((CfaUtilGetIfIndexFromMplsTnlIf
                 ((UINT4) i4IfIndex, &u4L3Intf, TRUE)) == CFA_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Wrong In L3 If Index");
                return WEB_FAILURE;
            }
#endif
            if (MplsDeleteMplsIfOrMplsTnlIf (u4L3Intf, (UINT4) i4IfIndex,
                                             CFA_MPLS_TUNNEL, MPLS_TRUE)
                == MPLS_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "In Tunnel Interface Deletion Failed");
                return WEB_FAILURE;
            }
            if ((nmhTestv2MplsInSegmentRowStatus (&u4ErrCode,
                                                  &InSegmentIndex,
                                                  ISS_DESTROY)) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "In Segment Deletion Failed");
                return WEB_FAILURE;
            }
            if ((nmhSetMplsInSegmentRowStatus
                 (&InSegmentIndex, ISS_DESTROY)) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "In Segment Deletion Failed");
                return WEB_FAILURE;
            }
            if (nmhGetMplsOutSegmentInterface (&OutSegmentIndex, &i4IfIndex)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "Wrong Out MPLS Tunnel If Index");
                return WEB_FAILURE;
            }
#ifdef CFA_WANTED
            if ((CfaUtilGetIfIndexFromMplsTnlIf
                 ((UINT4) i4IfIndex, &u4L3Intf, TRUE)) == CFA_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Wrong In L3 If Index");
                return WEB_FAILURE;
            }
#endif
            if (MplsDeleteMplsIfOrMplsTnlIf (u4L3Intf, (UINT4) i4IfIndex,
                                             CFA_MPLS_TUNNEL, MPLS_TRUE)
                == MPLS_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "Out Tunnel Interface Deletion Failed");
                return WEB_FAILURE;
            }

            if ((nmhTestv2MplsOutSegmentRowStatus (&u4ErrCode,
                                                   &OutSegmentIndex,
                                                   ISS_DESTROY)) ==
                SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "Out Segment Deletion Failed");
                return WEB_FAILURE;
            }
            if ((nmhSetMplsOutSegmentRowStatus
                 (&OutSegmentIndex, ISS_DESTROY)) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "Out Segment Deletion Failed");
                return WEB_FAILURE;
            }
        }
        MEMCPY (XCIndexPrev.pu1_OctetList, XCIndex.pu1_OctetList,
                XCIndex.i4_Length);
        XCIndexPrev.i4_Length = XCIndex.i4_Length;

        MEMCPY (InSegmentIndexPrev.pu1_OctetList, InSegmentIndex.pu1_OctetList,
                InSegmentIndex.i4_Length);
        InSegmentIndexPrev.i4_Length = InSegmentIndex.i4_Length;

        MEMCPY (OutSegmentIndexPrev.pu1_OctetList,
                OutSegmentIndex.pu1_OctetList, OutSegmentIndex.i4_Length);
        OutSegmentIndexPrev.i4_Length = OutSegmentIndex.i4_Length;
    }
    while ((bEntryPresent != TRUE) &&
           (nmhGetNextIndexMplsXCTable (&XCIndexPrev, &XCIndex,
                                        &InSegmentIndexPrev, &InSegmentIndex,
                                        &OutSegmentIndexPrev, &OutSegmentIndex)
            != SNMP_FAILURE));

    if (bEntryPresent == FALSE)
    {
        IssSendError (pHttp, (CONST INT1 *) "XC Entry Not Found");
        return WEB_FAILURE;
    }

    return WEB_SUCCESS;
}

/*******************************************************************************
*  Function Name : IssProcessMplsTunnelPage 
*  Description   : This function processes the request coming for the Mpls 
*                  Tunnel Page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssProcessMplsTunnelPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessMplsTunnelGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessMplsTunnelSet (pHttp);
    }
}

/*******************************************************************************
*  Function Name : IssProcessMplsTunnelGet
*  Description   : This function shows the Mpls Tunnel Entries in the web page 
*                  and enables user to enter entries to configure Mpls Tunnel.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/

VOID
IssProcessMplsTunnelGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE InIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutIndex;
    tSNMP_OCTET_STRING_TYPE NextHop;
    tSNMP_OID_TYPE      XCSegIndex;
    tSNMP_OID_TYPE      ResourcePointer;
    tCfaIfInfo          CfaIfInfo;

    UINT4               u4TunnelId = 0;
    UINT4               u4TunnelInstance = 0;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;
    UINT4               u4TunnelIdPrev = 0;
    UINT4               u4TunnelInstancePrev = 0;
    UINT4               u4IngressIdPrev = 0;
    UINT4               u4EgressIdPrev = 0;
    UINT4               u4Loop = FALSE;
    UINT4               u4XCIndex = 0;
    UINT4               u4InIndex = 0;
    UINT4               u4OutIndex = 0;
    UINT4               u4InLabel = 0;
    UINT4               u4OutLabel = 0;
    UINT4               u4NextHop = 0;
    UINT4               u4ResourceIndex = 0;
    UINT4               u4BandWidth = 0;
    UINT4               u4Temp = 0;

    INT4                i4MplsTnlIfIndex = 0;
    INT4                i4TunnelRole = 0;
    INT4                i4TunnelAdminStatus = 0;
    INT4                i4TunnelOperStatus = 0;

    UINT4               u4InL3Intf = 0;
    UINT4               u4OutL3Intf = 0;

    static UINT4        au4XCSegIndex[MPLS_TE_XC_TABLE_OFFSET] = { 0 };
    static UINT4        au4ResourcePtr[MPLS_TE_RSRC_TABLE_MAX_RATE_OFFSET];
    static UINT1        au1XcIndex[MPLS_INDEX_LENGTH] = { 0 };
    static UINT1        au1InIndex[MPLS_INDEX_LENGTH] = { 0 };
    static UINT1        au1OutIndex[MPLS_INDEX_LENGTH] = { 0 };
    static UINT1        au1NextHop[MPLS_INDEX_LENGTH] = { 0 };

    UINT1              *pu1Temp = NULL;
    UINT1               au1Temp[CFA_MAX_PORT_NAME_LENGTH];

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
    pu1Temp = &au1Temp[0];
    XCSegIndex.pu4_OidList = au4XCSegIndex;
    ResourcePointer.pu4_OidList = au4ResourcePtr;
    XCIndex.pu1_OctetList = au1XcIndex;
    XCIndex.i4_Length = MPLS_INDEX_LENGTH;
    InIndex.pu1_OctetList = au1InIndex;
    InIndex.i4_Length = MPLS_INDEX_LENGTH;
    OutIndex.pu1_OctetList = au1OutIndex;
    OutIndex.i4_Length = MPLS_INDEX_LENGTH;
    NextHop.pu1_OctetList = au1NextHop;

    pHttp->i4Write = 0;

    /* First the display the tunnel page till the in interface scroll bar in 
     * the top form */
    IssPrintL3Interfaces (pHttp, 0);

    /* Now, Display till the table header in the bottom form */
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    /* Fetch every entry in the Tunnel Table */
    while (1)
    {
        u4NextHop = 0;
        u4InLabel = 0;
        u4OutLabel = 0;
        u4InIndex = 0;
        u4OutIndex = 0;
        u4InL3Intf = 0;
        u4OutL3Intf = 0;

        if ((u4Loop == TRUE) &&
            (nmhGetNextIndexMplsTunnelTable (u4TunnelIdPrev, &u4TunnelId,
                                             u4TunnelInstancePrev,
                                             &u4TunnelInstance,
                                             u4IngressIdPrev, &u4IngressId,
                                             u4EgressIdPrev, &u4EgressId)
             == SNMP_FAILURE))
        {
            break;
        }

        if ((u4Loop != TRUE) &&
            (nmhGetFirstIndexMplsTunnelTable (&u4TunnelId, &u4TunnelInstance,
                                              &u4IngressId, &u4EgressId)
             == SNMP_FAILURE))
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            return;
        }

        u4TunnelIdPrev = u4TunnelId;
        u4TunnelInstancePrev = u4TunnelInstance;
        u4IngressIdPrev = u4IngressId;
        u4EgressIdPrev = u4EgressId;
        u4Loop = TRUE;

        if (nmhGetMplsTunnelRole (u4TunnelId, u4TunnelInstance, u4IngressId,
                                  u4EgressId, &i4TunnelRole) == SNMP_FAILURE)
        {
            continue;
        }

        if (nmhGetMplsTunnelXCPointer (u4TunnelId, u4TunnelInstance,
                                       u4IngressId, u4EgressId, &XCSegIndex)
            == SNMP_FAILURE)
        {
            continue;
        }

        MplsGetSegFromTunnelXC (&XCSegIndex, &XCIndex, &InIndex,
                                &OutIndex, &u4XCIndex, &u4InIndex, &u4OutIndex);
        if (u4XCIndex == 0)
        {
            continue;
        }

        /* Fetch the resource pointer to get the bandwidth allotted */
        if (nmhGetMplsTunnelResourcePointer (u4TunnelId, u4TunnelInstance,
                                             u4IngressId, u4EgressId,
                                             &ResourcePointer) == SNMP_FAILURE)
        {
            continue;
        }
        u4ResourceIndex =
            ResourcePointer.pu4_OidList[MPLS_TE_RSRC_TABLE_MAX_RATE_OFFSET - 1];
        if (u4ResourceIndex == 0)
        {
            continue;
        }
        if (nmhGetMplsTunnelResourceMaxRate (u4ResourceIndex,
                                             &u4BandWidth) == SNMP_FAILURE)
        {
            continue;
        }

        /* From InIndex and OutIndex, Fetch the InLabel, 
         * InInterface (MplsTnlIf) from InSegmentTable and OutLabel, NextHop
         * from OutSegment Table */
        if (u4InIndex != 0)
        {
            if (nmhGetMplsInSegmentLabel (&InIndex, &u4InLabel) == SNMP_FAILURE)
            {
                continue;
            }
            if (nmhGetMplsInSegmentInterface (&InIndex, &i4MplsTnlIfIndex)
                == SNMP_FAILURE)
            {
                continue;
            }
#ifdef CFA_WANTED
            if (CfaUtilGetIfIndexFromMplsTnlIf ((UINT4) i4MplsTnlIfIndex,
                                                &u4InL3Intf,
                                                TRUE) == CFA_FAILURE)
            {
                continue;
            }
#endif
        }

        if (u4OutIndex != 0)
        {
            if (nmhGetMplsOutSegmentTopLabel (&OutIndex, &u4OutLabel) ==
                SNMP_FAILURE)
            {
                continue;
            }

            if (nmhGetMplsOutSegmentInterface (&OutIndex, &i4MplsTnlIfIndex)
                == SNMP_FAILURE)
            {
                continue;
            }
#ifdef CFA_WANTED
            if (CfaUtilGetIfIndexFromMplsTnlIf ((UINT4) i4MplsTnlIfIndex,
                                                &u4OutL3Intf,
                                                TRUE) == CFA_FAILURE)
            {
                continue;
            }
#endif

            if (nmhGetMplsOutSegmentNextHopAddr (&OutIndex, &NextHop)
                == SNMP_FAILURE)
            {
                continue;
            }
            WEB_OCTETSTRING_TO_INTEGER ((&NextHop), u4NextHop);
        }

        /* Fetch the oper status and admin status of the tunnel */
        if (nmhGetMplsTunnelOperStatus (u4TunnelId, u4TunnelInstance,
                                        u4IngressId, u4EgressId,
                                        &i4TunnelOperStatus) == SNMP_FAILURE)
        {
            continue;
        }
        if (nmhGetMplsTunnelAdminStatus (u4TunnelId, u4TunnelInstance,
                                         u4IngressId, u4EgressId,
                                         &i4TunnelAdminStatus) == SNMP_FAILURE)
        {
            continue;
        }

        pHttp->i4Write = (INT4) u4Temp;
        /* All details have been fetched from the tunnel table,
         * Now, start displaying it in the web page */

        /* Display Tunnel Id */
        STRCPY (pHttp->au1KeyString, "TUNNEL_ID");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4TunnelId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* Display Tunnel Destination */
        STRCPY (pHttp->au1KeyString, "TUNNEL_DEST");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WEB_CONVERT_IPADDR_TO_STR (pu1Temp, u4EgressId);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1Temp);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* Display Tunnel Source */
        STRCPY (pHttp->au1KeyString, "TUNNEL_SOURCE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WEB_CONVERT_IPADDR_TO_STR (pu1Temp, u4IngressId);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1Temp);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* Display Tunnel Role */
        STRCPY (pHttp->au1KeyString, "TUNNEL_ROLE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4TunnelRole == 1)

        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "HEAD");
        }
        else if (i4TunnelRole == 2)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "TRANSIT");
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "TAIL");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* Display Tunnel Bandwidth */
        STRCPY (pHttp->au1KeyString, "TUNNEL_BW");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (u4BandWidth != 0)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4BandWidth);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* Display Tunnel In Label */
        STRCPY (pHttp->au1KeyString, "INLABEL");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (u4InLabel != 0)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4InLabel);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* Display Tunnel In Interface */
        STRCPY (pHttp->au1KeyString, "IN_INTERFACE_ID");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (u4InL3Intf != 0)
        {
#ifdef CFA_WANTED
            CfaGetIfInfo (u4InL3Intf, &CfaIfInfo);
#endif
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", CfaIfInfo.au1IfName);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* Display Tunnel Out Label */
        STRCPY (pHttp->au1KeyString, "OUTLABEL");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (u4OutLabel != 0)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4OutLabel);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* Display Tunnel In Interface */
        STRCPY (pHttp->au1KeyString, "OUT_INTERFACE_ID");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (u4OutL3Intf != 0)
        {
            CfaCliGetIfName (u4OutL3Intf, (INT1 *) pu1Temp);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1Temp);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* Display Tunnel Next Hop */
        STRCPY (pHttp->au1KeyString, "NEXT_HOP");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WEB_CONVERT_IPADDR_TO_STR (pu1Temp, u4NextHop);
        if (u4NextHop != 0)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1Temp);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* Display Tunnel Oper Status */
        STRCPY (pHttp->au1KeyString, "TUNNEL_OPER");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4TunnelOperStatus == 1)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "UP");
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "DOWN");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* Display Tunnel Admin Status */
        STRCPY (pHttp->au1KeyString, "TUNNEL_ADMIN");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4TunnelAdminStatus == 1)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "UP");
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "DOWN");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    /* Display all the information in the html page after the Table stop flag */
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*******************************************************************************
*  Function Name : IssProcessMplsTunnelSet 
*  Description   : This function processes the request coming for
*                  Mpls Tunnel Page and tries to set the Tunnel Table entries 
*                  with the given use input from the web page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/

VOID
IssProcessMplsTunnelSet (tHttp * pHttp)
{
    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;
    tTunnelArgs         TunnelArgs;
    UINT4               u4Mask = 0xffffffff;
    UINT4               u4TunnelId = 0;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;
    UINT4               u4TunnelInstance = 1;
    UINT4               u4InLabel = 0;
    UINT4               u4OutLabel = 0;
    UINT4               u4BandWidth = 0;
    UINT4               u4NextHop = 0;
    UINT4               u4InIndex = 0;
    UINT4               u4OutIfIndex = 0;
    UINT4               u4MplsIfIndex = 0;
    INT4                i4TunnelRole = 0;
    INT4                i4TunnelAdminStatus = 0;
    INT4                i4RetVal = WEB_SUCCESS;

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

    STRCPY (pHttp->au1Name, "TUNNEL_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4TunnelId = (UINT4) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "TUNNEL_DEST");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4EgressId = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));

    STRCPY (pHttp->au1Name, "TUNNEL_SOURCE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "...") != 0)
    {
        u4IngressId = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
    }

    STRCPY (pHttp->au1Name, "TUNNEL_ROLE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "HEAD") == 0)
    {
        i4TunnelRole = 1;
    }
    else if (STRCMP (pHttp->au1Value, "TRANSIT") == 0)
    {
        i4TunnelRole = 2;
    }
    else
    {
        i4TunnelRole = 3;
    }

    STRCPY (pHttp->au1Name, "TUNNEL_BW");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if ((STRCMP (pHttp->au1Value, "") != 0) &&
        (STRCMP (pHttp->au1Value, "-") != 0))
    {
        u4BandWidth = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "INLABEL");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if ((STRCMP (pHttp->au1Value, "") != 0) &&
        (STRCMP (pHttp->au1Value, "-") != 0))
    {
        u4InLabel = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "IN_INTERFACE_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if ((STRCMP (pHttp->au1Value, "-") != 0) &&
        (STRCMP (pHttp->au1Value, "") != 0))
    {
        issDecodeSpecialChar (pHttp->au1Value);
#ifdef CFA_WANTED
        if (CfaGetInterfaceIndexFromName (pHttp->au1Value, &u4InIndex)
            == OSIX_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Wrong Index");
            return;
        }
        if (CfaUtilGetMplsIfFromIfIndex (u4InIndex, &u4MplsIfIndex, TRUE) ==
            CFA_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *)
                          "Mpls is not enabled on this interface");
            return;
        }
#endif
    }

    STRCPY (pHttp->au1Name, "OUTLABEL");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if ((STRCMP (pHttp->au1Value, "") != 0) &&
        (STRCMP (pHttp->au1Value, "-") != 0))
    {
        u4OutLabel = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "NEXT_HOP");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if ((STRCMP (pHttp->au1Value, "...") != 0) &&
        (STRCMP (pHttp->au1Value, "-") != 0))
    {
        u4NextHop = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
    }

    STRCPY (pHttp->au1Name, "TUNNEL_ADMIN");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "ENABLE") == 0)
    {
        i4TunnelAdminStatus = 1;
    }
    else
    {
        i4TunnelAdminStatus = 2;
    }

    STRCPY (pHttp->au1Name, "DELETE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Delete") != 0)
    {
        RtQuery.u4DestinationIpAddress = u4NextHop;
        RtQuery.u4DestinationSubnetMask = u4Mask;
        RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

        if (u4IngressId == 0)
        {
            /* IngressId is not given. So, trying to get the 
             * source address based on the destination address given */
            if (NetIpv4GetSrcAddrToUseForDest (u4EgressId, &u4IngressId)
                == NETIPV4_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "Failed to get source address");
                return;
            }
        }

        if (i4TunnelRole != 3)
        {
            if (NetIpv4IfIsOurAddress (u4NextHop) == NETIPV4_SUCCESS)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "Next Hop is same as Self-Ip Address");
                return;
            }
            if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) != NETIPV4_SUCCESS)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "Next Hop Not Directly Connected");
                return;
            }
            if (NetIpv4GetCfaIfIndexFromPort (NetIpRtInfo.u4RtIfIndx,
                                              &u4OutIfIndex) == NETIPV4_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "Failed in getting OutIfIndex");
                return;
            }
        }

        TunnelArgs.u4TunnelIndex = u4TunnelId;
        TunnelArgs.u4TunnelInstance = u4TunnelInstance;
        TunnelArgs.u4IngressId = u4IngressId;
        TunnelArgs.u4EgressId = u4EgressId;
        TunnelArgs.i4TunnelRole = i4TunnelRole;
        TunnelArgs.u4Bandwidth = u4BandWidth;
        TunnelArgs.u4InLabel = u4InLabel;
        TunnelArgs.u4InIndex = u4InIndex;
        TunnelArgs.u4OutLabel = u4OutLabel;
        TunnelArgs.u4NextHop = u4NextHop;
        TunnelArgs.u4OutIfIndex = u4OutIfIndex;
        TunnelArgs.i4AdminStatus = i4TunnelAdminStatus;

        i4RetVal = IssMplsCreateTunnel (pHttp, &TunnelArgs);
    }
    else
    {
        i4RetVal = IssMplsDeleteTunnel (pHttp, u4TunnelId, u4TunnelInstance,
                                        u4IngressId, u4EgressId);
    }

    if (i4RetVal == WEB_SUCCESS)
    {
        IssProcessMplsTunnelGet (pHttp);
    }
    return;
}

/******************************************************************************
*  Function Name : IssMplsCreateTunnel
*  Description   : This Routine Creates the tunnel indexed by the arguments 
*                  passed to this function and its related XC Entries.
*  Input(s)      : pHttp            - Pointer to the global HTTP data structure.
*                  pTunnelArgs      - Pointer to a dummy structure that carries
*                                     values to be set during tunnel creation.
*  Output(s)     : None.
*  Return Values : WEB_SUCCESS or WEB_FAILURE
******************************************************************************/

INT4
IssMplsCreateTunnel (tHttp * pHttp, tTunnelArgs * pTunnelArgs)
{
    CHR1                ac1TunnelName[MPLS_MAX_STR_LEN];
    tSNMP_OCTET_STRING_TYPE MplsTunnelNames;
    UINT4               u4ErrorCode = 0;
    UINT4               u4TunnelIndex = 0;
    UINT4               u4TunnelInstance = 0;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;

    UINT4               u4GetTunnelId = 0;
    UINT4               u4GetTunnelIdPrev = 0;
    UINT4               u4GetTunnelInst = 0;
    UINT4               u4GetTunnelInstPrev = 0;
    UINT4               u4GetIngressId = 0;
    UINT4               u4GetIngressIdPrev = 0;
    UINT4               u4GetEgressId = 0;
    UINT4               u4GetEgressIdPrev = 0;

    BOOL1               bEntryExists = FALSE;
    BOOL1               bLoop = FALSE;

    MEMSET (ac1TunnelName, 0, sizeof (ac1TunnelName));
    u4TunnelIndex = pTunnelArgs->u4TunnelIndex;
    u4TunnelInstance = pTunnelArgs->u4TunnelInstance;
    u4IngressId = pTunnelArgs->u4IngressId;
    u4EgressId = pTunnelArgs->u4EgressId;

    if (u4EgressId == u4IngressId)
    {
        IssSendError (pHttp, (CONST INT1 *) "Both Egress and Ingress are same");
        return WEB_FAILURE;
    }

    SNPRINTF (ac1TunnelName, MPLS_MAX_STR_LEN, "%s%d",
              "mplsTunnel", u4TunnelIndex);
    MplsTunnelNames.pu1_OctetList = (UINT1 *) ac1TunnelName;
    MplsTunnelNames.i4_Length = (INT4) STRLEN (ac1TunnelName);

    /* Walk the MplsTunnelTable to check whether tunnel already exists,
     * if so, return else proceed with creating a new tunnel */
    while (1)
    {
        if ((bLoop == TRUE) &&
            (nmhGetNextIndexMplsTunnelTable (u4GetTunnelIdPrev, &u4GetTunnelId,
                                             u4GetTunnelInstPrev,
                                             &u4GetTunnelInst,
                                             u4GetIngressIdPrev,
                                             &u4GetIngressId, u4GetEgressIdPrev,
                                             &u4GetEgressId) == SNMP_FAILURE))
        {
            break;
        }

        if ((bLoop != TRUE) &&
            (nmhGetFirstIndexMplsTunnelTable (&u4GetTunnelId, &u4GetTunnelInst,
                                              &u4GetIngressId, &u4GetEgressId)
             == SNMP_FAILURE))
        {
            break;
        }

        u4GetTunnelIdPrev = u4GetTunnelId;
        u4GetTunnelInstPrev = u4GetTunnelInst;
        u4GetIngressIdPrev = u4GetIngressId;
        u4GetEgressIdPrev = u4GetEgressId;
        bLoop = TRUE;

        if (u4TunnelIndex != u4GetTunnelId ||
            u4TunnelInstance != u4GetTunnelInst ||
            u4IngressId != u4GetIngressId || u4EgressId != u4GetEgressId)
        {
            continue;
        }

        bEntryExists = TRUE;
        break;
    }
    if (bEntryExists == TRUE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Tunnel Already Exists");
        return WEB_FAILURE;
    }

    if (pTunnelArgs->i4TunnelRole == 1)
    {
        if (NetIpv4IfIsOurAddress (u4IngressId) != NETIPV4_SUCCESS)
        {
            IssSendError (pHttp, (CONST INT1 *) "Wrong Tunnel Role");
            return WEB_FAILURE;
        }
    }
    else if (pTunnelArgs->i4TunnelRole == 3)
    {
        if (NetIpv4IfIsOurAddress (u4EgressId) != NETIPV4_SUCCESS)
        {
            IssSendError (pHttp, (CONST INT1 *) "Wrong Tunnel Role");
            return WEB_FAILURE;
        }
    }
    else
    {
        if ((NetIpv4IfIsOurAddress (u4IngressId) == NETIPV4_SUCCESS)
            || (NetIpv4IfIsOurAddress (u4EgressId) == NETIPV4_SUCCESS))
        {
            IssSendError (pHttp, (CONST INT1 *) "Wrong Tunnel Role");
            return WEB_FAILURE;
        }
    }

    /* Tunnel does not exist. So, creating one */
    if (nmhTestv2MplsTunnelRowStatus (&u4ErrorCode, u4TunnelIndex,
                                      u4TunnelInstance, u4IngressId,
                                      u4EgressId, ISS_CREATE_AND_WAIT) ==
        SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Tunnel Not Created");
        return WEB_FAILURE;
    }

    if (nmhSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                   u4IngressId, u4EgressId, ISS_CREATE_AND_WAIT)
        == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Tunnel Not Created");
        return WEB_FAILURE;
    }

    /* Set Tunnel IsIf to true */
    if (nmhTestv2MplsTunnelIsIf (&u4ErrorCode, u4TunnelIndex, u4TunnelInstance,
                                 u4IngressId, u4EgressId, MPLS_TRUE)
        == SNMP_FAILURE)
    {
        nmhSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                   u4EgressId, ISS_DESTROY);
        IssSendError (pHttp, (CONST INT1 *) "TunnelIsIf Not set");
        return WEB_FAILURE;
    }
    if (nmhSetMplsTunnelIsIf (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                              u4EgressId, MPLS_TRUE) == SNMP_FAILURE)
    {
        nmhSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                   u4EgressId, ISS_DESTROY);
        IssSendError (pHttp, (CONST INT1 *) "TunnelIsIf Not set");
        return WEB_FAILURE;
    }

    /* Set Tunnel Name */
    if (nmhTestv2MplsTunnelName (&u4ErrorCode, u4TunnelIndex, u4TunnelInstance,
                                 u4IngressId, u4EgressId, &MplsTunnelNames)
        == SNMP_FAILURE)
    {
        nmhSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                   u4EgressId, ISS_DESTROY);
        IssSendError (pHttp, (CONST INT1 *) "Tunnel Name Not Set");
        return WEB_FAILURE;
    }

    if (nmhSetMplsTunnelName (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                              u4EgressId, &MplsTunnelNames) == SNMP_FAILURE)
    {
        nmhSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                   u4EgressId, ISS_DESTROY);
        IssSendError (pHttp, (CONST INT1 *) "Tunnel Name Not Set");
        return WEB_FAILURE;
    }

    /* Set the Tunnel Role */
    if (nmhTestv2MplsTunnelRole (&u4ErrorCode, u4TunnelIndex, u4TunnelInstance,
                                 u4IngressId, u4EgressId,
                                 pTunnelArgs->i4TunnelRole) == SNMP_FAILURE)
    {
        nmhSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                   u4EgressId, ISS_DESTROY);
        IssSendError (pHttp, (CONST INT1 *) "TunnelRole Not set");
        return WEB_FAILURE;
    }
    if (nmhSetMplsTunnelRole (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                              u4EgressId, pTunnelArgs->i4TunnelRole)
        == SNMP_FAILURE)
    {
        nmhSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                   u4EgressId, ISS_DESTROY);
        IssSendError (pHttp, (CONST INT1 *) "TunnelRole Not set");
        return WEB_FAILURE;
    }

    if (IssMplsDestroyTunnelXc (pTunnelArgs) == WEB_FAILURE)
    {
        nmhSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                   u4EgressId, ISS_DESTROY);
        IssSendError (pHttp, (CONST INT1 *)
                      "Failed in Deleting Existing XC, InSeg & OutSeg");
        return WEB_FAILURE;
    }

    if (IssMplsCreateTunnelXc (pTunnelArgs) == WEB_FAILURE)
    {
        nmhSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                   u4EgressId, ISS_DESTROY);
        IssSendError (pHttp, (CONST INT1 *) "Failed in Creating Tunnel");
        IssSendError (pHttp, (CONST INT1 *)
                      "Check Whether Interface is MPLS Enabled");
        return WEB_FAILURE;
    }

    if (IssMplsCreateOrModifyResource (pTunnelArgs) == WEB_FAILURE)
    {
        nmhSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                   u4EgressId, ISS_DESTROY);
        IssSendError (pHttp, (CONST INT1 *)
                      "Failed in Allocating Resource to Tunnel");
        return WEB_FAILURE;
    }

    if (IssMplsTunnelActive (pTunnelArgs) == WEB_FAILURE)
    {
        nmhSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                   u4EgressId, ISS_DESTROY);
        IssSendError (pHttp, (CONST INT1 *) "Failed To Make Tunnel Active");
        return WEB_FAILURE;
    }

    return WEB_SUCCESS;
}

/******************************************************************************
*  Function Name : IssMplsDestroyTunnelXc
*  Description   : This Routine destroys the existing XC entries corresponding 
*                  to the tunnel. It also destroys InSegment Entries or 
*                  OutSegment Entries whichever is present.
*  Input(s)      : pTunnelArgs      - Pointer to the TunnelArgs Dummy structure.
*  Output(s)     : None.
*  Return Values : WEB_FAILURE or WEB_SUCCESS.
******************************************************************************/

INT1
IssMplsDestroyTunnelXc (tTunnelArgs * pTunnelArgs)
{
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCSegmentIndex;

    tSNMP_OID_TYPE      XCSegIndex;
    static UINT4        au4XCSegIndex[MPLS_TE_XC_TABLE_OFFSET];
    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCSegmentIndex[MPLS_INDEX_LENGTH];

    UINT4               u4TunnelIndex = 0;
    UINT4               u4TunnelInstance = 0;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;
    UINT4               u4XCSegInd = 0;
    UINT4               u4InSegInd = 0;
    UINT4               u4OutSegInd = 0;
    UINT4               u4ErrorCode = 0;

    XCSegIndex.pu4_OidList = au4XCSegIndex;
    XCSegIndex.u4_Length = 0;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    XCSegmentIndex.pu1_OctetList = au1XCSegmentIndex;

    u4TunnelIndex = pTunnelArgs->u4TunnelIndex;
    u4TunnelInstance = pTunnelArgs->u4TunnelInstance;
    u4IngressId = pTunnelArgs->u4IngressId;
    u4EgressId = pTunnelArgs->u4EgressId;

    /* Get the tunnel XC pointer from passed Tunnel Indices. From the XCPointer 
     * value obtain the XCIndex,InSegment and OutSegment Index. */
    if (nmhGetMplsTunnelXCPointer (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                   u4EgressId, &XCSegIndex) == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }

    MplsGetSegFromTunnelXC (&XCSegIndex, &XCSegmentIndex, &InSegmentIndex,
                            &OutSegmentIndex, &u4XCSegInd, &u4InSegInd,
                            &u4OutSegInd);
    /* If In Segment, Out Segment and XC Indices are already present for the corresponding 
     * Tunnel, then destroy the corresponding XC ,InSeg and OutSeg entries , and update with 
     * the new inputs.*/
    if (u4XCSegInd != 0)
    {
        /* Destroy the XC entry */
        if (nmhTestv2MplsXCRowStatus (&u4ErrorCode, &XCSegmentIndex,
                                      &InSegmentIndex, &OutSegmentIndex,
                                      ISS_DESTROY) == SNMP_FAILURE)
        {
            return WEB_FAILURE;
        }

        if (nmhSetMplsXCRowStatus
            (&XCSegmentIndex, &InSegmentIndex, &OutSegmentIndex,
             ISS_DESTROY) == SNMP_FAILURE)
        {
            return WEB_FAILURE;
        }
    }

    /* Destroy the InSegment entry */
    if (u4InSegInd != 0)
    {
        if (nmhTestv2MplsInSegmentRowStatus (&u4ErrorCode, &InSegmentIndex,
                                             ISS_DESTROY) == SNMP_FAILURE)
        {
            return WEB_FAILURE;
        }

        if (nmhSetMplsInSegmentRowStatus (&InSegmentIndex, ISS_DESTROY)
            == SNMP_FAILURE)
        {
            return WEB_FAILURE;
        }
    }

    /* Destroy the OutSegment entry */
    if (u4OutSegInd != 0)
    {
        if (nmhTestv2MplsOutSegmentRowStatus (&u4ErrorCode, &OutSegmentIndex,
                                              ISS_DESTROY) == SNMP_FAILURE)
        {
            return WEB_FAILURE;
        }

        if (nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex, ISS_DESTROY)
            == SNMP_FAILURE)
        {
            return WEB_FAILURE;
        }
    }
    return WEB_SUCCESS;
}

/******************************************************************************
*  Function Name : IssMplsCreateTunnelXc
*  Description   : This Routine Creates XC entries, InSegment Entries and 
*                  OutSegment Entries and associates them with the tunnel 
*                  through TunneXCPointer.
*  Input(s)      : pTunnelArgs      - Pointer to the TunnelArgs Dummy structure.
*  Output(s)     : None.
*  Return Values : WEB_SUCCESS or WEB_FAILURE
******************************************************************************/

INT1
IssMplsCreateTunnelXc (tTunnelArgs * pTunnelArgs)
{
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OID_TYPE      TunnelXCPointer;

    static UINT4        au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 2, 1, 10, 1, 4, 4,
        0, 0, 0, 0, 4, 0, 0, 0, 0, 4, 0, 0, 0, 0
    };

    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH] = { 0 };
    static UINT1        au1InIndex[MPLS_INDEX_LENGTH] = { 0 };
    static UINT1        au1OutIndex[MPLS_INDEX_LENGTH] = { 0 };

    UINT4               u4XCTabIndex = 0;
    UINT4               u4InIndex = 0;
    UINT4               u4OutIndex = 0;

    UINT4               u4TunnelIndex = 0;
    UINT4               u4TunnelInstance = 0;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;
    UINT4               u4InLabel = 0;
    UINT4               u4OutLabel = 0;
    UINT4               u4InIfIndex = 0;
    UINT4               u4OutIfIndex = 0;
    UINT4               u4NextHop = 0;
    UINT4               u4MplsTnlInIfIndex = 0;
    UINT4               u4MplsTnlOutIfIndex = 0;
    UINT4               u4ErrorCode = 0;

    INT4                i4TunnelRole = 0;

    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = MPLS_INDEX_LENGTH;
    InSegmentIndex.pu1_OctetList = au1InIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    OutSegmentIndex.pu1_OctetList = au1OutIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    TunnelXCPointer.pu4_OidList = au4XCTableOid;
    TunnelXCPointer.u4_Length = 0;

    u4TunnelIndex = pTunnelArgs->u4TunnelIndex;
    u4TunnelInstance = pTunnelArgs->u4TunnelInstance;
    u4IngressId = pTunnelArgs->u4IngressId;
    u4EgressId = pTunnelArgs->u4EgressId;
    u4InLabel = pTunnelArgs->u4InLabel;
    u4OutLabel = pTunnelArgs->u4OutLabel;
    u4InIfIndex = pTunnelArgs->u4InIndex;
    u4OutIfIndex = pTunnelArgs->u4OutIfIndex;
    u4NextHop = pTunnelArgs->u4NextHop;
    i4TunnelRole = pTunnelArgs->i4TunnelRole;

    /* Create an Inseg entry only if Tunnel Role is TAIL or TRANSIT */
    if (i4TunnelRole != 1)
    {
        if ((nmhGetMplsInSegmentIndexNext (&InSegmentIndex)) == SNMP_FAILURE)
        {
            return WEB_FAILURE;
        }
        if ((nmhTestv2MplsInSegmentRowStatus (&u4ErrorCode, &InSegmentIndex,
                                              ISS_CREATE_AND_WAIT)) ==
            SNMP_FAILURE)
        {
            return WEB_FAILURE;
        }
        if ((nmhSetMplsInSegmentRowStatus
             (&InSegmentIndex, ISS_CREATE_AND_WAIT)) == SNMP_FAILURE)
        {
            return WEB_FAILURE;
        }
    }

    /* Create an OutSeg entry only if Tunnel Role is HEAD or TRANSIT, 
     * Delete the created InSeg entry in case of any failure. */
    if (i4TunnelRole != 3)
    {
        if ((nmhGetMplsOutSegmentIndexNext (&OutSegmentIndex)) == SNMP_FAILURE)
        {
            if (i4TunnelRole == 2)
            {
                nmhSetMplsInSegmentRowStatus (&InSegmentIndex, ISS_DESTROY);
            }
            return WEB_FAILURE;
        }
        if ((nmhTestv2MplsOutSegmentRowStatus (&u4ErrorCode, &OutSegmentIndex,
                                               ISS_CREATE_AND_WAIT)) ==
            SNMP_FAILURE)
        {
            if (i4TunnelRole == 2)
            {
                nmhSetMplsInSegmentRowStatus (&InSegmentIndex, ISS_DESTROY);
            }
            return WEB_FAILURE;
        }
        if ((nmhSetMplsOutSegmentRowStatus
             (&OutSegmentIndex, ISS_CREATE_AND_WAIT)) == SNMP_FAILURE)
        {
            if (i4TunnelRole == 2)
            {
                nmhSetMplsInSegmentRowStatus (&InSegmentIndex, ISS_DESTROY);
            }
            return WEB_FAILURE;
        }
    }

    /* Create an XC entry. Delete the created InSeg and OutSeg entry 
     * in case of any failure based on the TunnelRole */
    if ((nmhGetMplsXCIndexNext (&XCIndex)) == SNMP_FAILURE)
    {
        IssMplsCleanUpInSegOutSeg (&InSegmentIndex, &OutSegmentIndex,
                                   i4TunnelRole);
        return WEB_FAILURE;
    }
    if ((nmhTestv2MplsXCRowStatus (&u4ErrorCode, &XCIndex, &InSegmentIndex,
                                   &OutSegmentIndex, ISS_CREATE_AND_WAIT))
        == SNMP_FAILURE)
    {
        IssMplsCleanUpInSegOutSeg (&InSegmentIndex, &OutSegmentIndex,
                                   i4TunnelRole);
        return WEB_FAILURE;
    }
    if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                                ISS_CREATE_AND_WAIT)) == SNMP_FAILURE)
    {
        IssMplsCleanUpInSegOutSeg (&InSegmentIndex, &OutSegmentIndex,
                                   i4TunnelRole);
        return WEB_FAILURE;
    }

    /* Populate the InSeg, OutSeg and XC Tables and make Tables active */
    do
    {
        if (i4TunnelRole == 1)
        {
            /* Here, Tunnel Role is HEAD. So, Populate OutSegment Table. */
            if (IssMplsCreateOutSegment (&OutSegmentIndex, u4OutLabel,
                                         u4OutIfIndex, u4NextHop,
                                         &u4MplsTnlOutIfIndex) == WEB_FAILURE)
            {
                break;
            }
        }
        else if (i4TunnelRole == 2)
        {
            /* Here, Tunnel Role is TRANSIT, So, Populate OutSegment Table 
             * and InSegmentTable */
            if (IssMplsCreateInSegment (&InSegmentIndex, u4InLabel,
                                        u4InIfIndex, &u4MplsTnlInIfIndex)
                == WEB_FAILURE)
            {
                break;
            }
            if (IssMplsCreateOutSegment (&OutSegmentIndex, u4OutLabel,
                                         u4OutIfIndex, u4NextHop,
                                         &u4MplsTnlOutIfIndex) == WEB_FAILURE)
            {
                break;
            }
        }
        else if (i4TunnelRole == 3)
        {
            /* Here, Tunnel Role is TAIL, So, Populate InSegmentTable */
            if (IssMplsCreateInSegment (&InSegmentIndex, u4InLabel,
                                        u4InIfIndex, &u4MplsTnlInIfIndex)
                == WEB_FAILURE)
            {
                break;
            }
        }
        else
        {
            /* Here, Role of the Tunnel is Undefined. So, break */
            break;
        }

        /* InSegmentTable or OutSegmentTable or Both have been populated
         * Now, Make XCTable Active */
        if ((nmhTestv2MplsXCRowStatus (&u4ErrorCode, &XCIndex,
                                       &InSegmentIndex, &OutSegmentIndex,
                                       ISS_ACTIVE)) == SNMP_FAILURE)
        {
            break;
        }
        if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                    &OutSegmentIndex,
                                    ISS_ACTIVE)) == SNMP_FAILURE)
        {
            break;
        }

        /*  Set the value of mplsTunnelXCPointer. */
        WEB_OCTETSTRING_TO_INTEGER ((&XCIndex), u4XCTabIndex);
        if (i4TunnelRole == 1)
        {
            WEB_OCTETSTRING_TO_INTEGER ((&OutSegmentIndex), u4OutIndex);
            u4InIndex = 0;
        }
        else if (i4TunnelRole == 2)
        {
            WEB_OCTETSTRING_TO_INTEGER ((&OutSegmentIndex), u4OutIndex);
            WEB_OCTETSTRING_TO_INTEGER ((&InSegmentIndex), u4InIndex);
        }
        else
        {
            WEB_OCTETSTRING_TO_INTEGER ((&InSegmentIndex), u4InIndex);
            u4OutIndex = 0;
        }
        au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET - 11] = u4XCTabIndex;
        au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET - 6] = u4InIndex;
        au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET - 1] = u4OutIndex;
        TunnelXCPointer.pu4_OidList = au4XCTableOid;
        TunnelXCPointer.u4_Length = MPLS_TE_XC_TABLE_OFFSET;

        if ((nmhTestv2MplsTunnelXCPointer (&u4ErrorCode, u4TunnelIndex,
                                           u4TunnelInstance, u4IngressId,
                                           u4EgressId, &TunnelXCPointer))
            == SNMP_FAILURE)
        {
            break;
        }
        if ((nmhSetMplsTunnelXCPointer (u4TunnelIndex, u4TunnelInstance,
                                        u4IngressId, u4EgressId,
                                        &TunnelXCPointer)) == SNMP_FAILURE)
        {
            break;
        }

        /* All set operations have been successful, return success. */
        return WEB_SUCCESS;
    }
    while (0);

    /*If all or any of the set operations is NOT successful, do
     * 1. Destroy the Mpls Tunnel Interfaces created for both InSegment and 
     *    OutSegment Interfaces.
     * 2. Delete the created InSegment, OutSegment and XC entry. 
     * 3. Return failure.                                                    */
    if (u4MplsTnlInIfIndex != 0)
    {
        MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                     CFA_MPLS_TUNNEL, MPLS_TRUE);
    }
    if (u4MplsTnlOutIfIndex != 0)
    {
        MplsDeleteMplsIfOrMplsTnlIf (u4OutIfIndex, u4MplsTnlOutIfIndex,
                                     CFA_MPLS_TUNNEL, MPLS_TRUE);
    }
    nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                           ISS_DESTROY);
    IssMplsCleanUpInSegOutSeg (&InSegmentIndex, &OutSegmentIndex, i4TunnelRole);
    return WEB_FAILURE;
}

/******************************************************************************
*  Function Name : IssMplsCreateOrModifyResource
*  Description   : This Routine Creates or Modifies the bandwidth allocated.
*  Input(s)      : pTunnelArgs      - Pointer to the TunnelArgs Dummy structure.
*  Output(s)     : None.
*  Return Values : WEB_SUCCESS or WEB_FAILURE
******************************************************************************/

INT1
IssMplsCreateOrModifyResource (tTunnelArgs * pTunnelArgs)
{
    UINT4               u4TunnelResourceIndex = 0;
    UINT4               u4ErrorCode = 0;

    UINT4               u4TunnelIndex = 0;
    UINT4               u4TunnelInstance = 0;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;
    UINT4               u4BandWidth = 0;

    tSNMP_OID_TYPE      ResourcePointer;
    tSNMP_OID_TYPE      GetResourcePointer;
    INT4                i4RetValTunnelRowStatus;

    static UINT4        au4GetRsrcPointer[MPLS_TE_RSRC_TABLE_MAX_RATE_OFFSET];
    static UINT4        au4ResourceTableOid[MPLS_TE_RSRC_TABLE_MAX_RATE_OFFSET]
        = { 1, 3, 6, 1, 2, 1, 10, 166, 3, 2, 6, 1, 2 };

    GetResourcePointer.pu4_OidList = au4GetRsrcPointer;
    MEMSET (GetResourcePointer.pu4_OidList, 0, sizeof (UINT4) * 2);
    GetResourcePointer.u4_Length = 0;

    u4TunnelIndex = pTunnelArgs->u4TunnelIndex;
    u4TunnelInstance = pTunnelArgs->u4TunnelInstance;
    u4IngressId = pTunnelArgs->u4IngressId;
    u4EgressId = pTunnelArgs->u4EgressId;
    u4BandWidth = pTunnelArgs->u4Bandwidth;

    if (nmhGetMplsTunnelResourceIndexNext (&u4TunnelResourceIndex) !=
        SNMP_SUCCESS)
    {
        return WEB_FAILURE;
    }

    au4ResourceTableOid[MPLS_TE_RSRC_TABLE_MAX_RATE_OFFSET - 1] =
        u4TunnelResourceIndex;
    ResourcePointer.pu4_OidList = au4ResourceTableOid;
    ResourcePointer.u4_Length = MPLS_TE_RSRC_TABLE_MAX_RATE_OFFSET;

    if ((nmhGetMplsTunnelResourcePointer
         (u4TunnelIndex, u4TunnelInstance, u4IngressId, u4EgressId,
          &GetResourcePointer)) == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }

    if ((nmhGetMplsTunnelRowStatus
         (u4TunnelIndex, u4TunnelInstance, u4IngressId,
          u4EgressId, &i4RetValTunnelRowStatus)) == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }

    /* If Tunnel Row status is active make it to Not In Service. */
    if (i4RetValTunnelRowStatus == ISS_ACTIVE)
    {
        if ((nmhTestv2MplsTunnelRowStatus (&u4ErrorCode,
                                           u4TunnelIndex,
                                           u4TunnelInstance,
                                           u4IngressId, u4EgressId,
                                           ISS_NOT_IN_SERVICE)) == SNMP_FAILURE)
        {
            return WEB_FAILURE;
        }
        if ((nmhSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                        u4IngressId, u4EgressId,
                                        ISS_NOT_IN_SERVICE)) == SNMP_FAILURE)
        {
            return WEB_FAILURE;
        }
    }

    /* Resource Already exists. So Destroy it, Create the entry again 
     * and assign the new bandwidth to it */
    if (GetResourcePointer.u4_Length != 2)
    {

        u4TunnelResourceIndex =
            GetResourcePointer.pu4_OidList[MPLS_TE_RSRC_TABLE_MAX_RATE_OFFSET -
                                           1];
        ResourcePointer.pu4_OidList[MPLS_TE_RSRC_TABLE_MAX_RATE_OFFSET - 1] =
            u4TunnelResourceIndex;
        if ((nmhTestv2MplsTunnelResourceRowStatus
             (&u4ErrorCode, u4TunnelResourceIndex,
              ISS_DESTROY)) == SNMP_FAILURE)
        {
            return WEB_FAILURE;
        }

        if ((nmhSetMplsTunnelResourceRowStatus (u4TunnelResourceIndex,
                                                ISS_DESTROY)) == SNMP_FAILURE)
        {
            return WEB_FAILURE;
        }

    }

    if ((nmhTestv2MplsTunnelResourceRowStatus (&u4ErrorCode,
                                               u4TunnelResourceIndex,
                                               ISS_CREATE_AND_WAIT))
        == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }

    if ((nmhSetMplsTunnelResourceRowStatus (u4TunnelResourceIndex,
                                            ISS_CREATE_AND_WAIT))
        == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }

    /* Set Tunnel Resource Max Rate */
    if ((nmhTestv2MplsTunnelResourceMaxRate (&u4ErrorCode,
                                             u4TunnelResourceIndex,
                                             u4BandWidth)) == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }
    if ((nmhSetMplsTunnelResourceMaxRate (u4TunnelResourceIndex,
                                          u4BandWidth)) == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }

    /* Set Resource Row Status to active */
    if ((nmhTestv2MplsTunnelResourceRowStatus (&u4ErrorCode,
                                               u4TunnelResourceIndex,
                                               ISS_ACTIVE)) == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }

    if ((nmhSetMplsTunnelResourceRowStatus (u4TunnelResourceIndex,
                                            ISS_ACTIVE)) == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }

    /* Set Tunnel Resource Pointer */
    if ((nmhTestv2MplsTunnelResourcePointer (&u4ErrorCode, u4TunnelIndex,
                                             u4TunnelInstance, u4IngressId,
                                             u4EgressId, &ResourcePointer))
        == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }

    if ((nmhSetMplsTunnelResourcePointer (u4TunnelIndex, u4TunnelInstance,
                                          u4IngressId, u4EgressId,
                                          &ResourcePointer)) == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }
    return (WEB_SUCCESS);
}

/******************************************************************************
*  Function Name : IssMplsCleanUpInSegOutSeg
*  Description   : This Routine destroys Insegment or OutSegment based on the
*                  TunnelRole.
*  Input(s)      : pTunnelArgs      - Pointer to the TunnelArgs Dummy structure.
*  Output(s)     : None.
*  Return Values : WEB_SUCCESS or WEB_FAILURE
******************************************************************************/
VOID
IssMplsCleanUpInSegOutSeg (tSNMP_OCTET_STRING_TYPE * pInSegmentIndex,
                           tSNMP_OCTET_STRING_TYPE * pOutSegmentIndex,
                           INT4 i4TunnelRole)
{
    if (i4TunnelRole == 1)
    {
        nmhSetMplsOutSegmentRowStatus (pOutSegmentIndex, ISS_DESTROY);
    }
    else if (i4TunnelRole == 2)
    {
        nmhSetMplsInSegmentRowStatus (pInSegmentIndex, ISS_DESTROY);
        nmhSetMplsOutSegmentRowStatus (pOutSegmentIndex, ISS_DESTROY);
    }
    else
    {
        nmhSetMplsInSegmentRowStatus (pInSegmentIndex, ISS_DESTROY);
    }
    return;
}

/******************************************************************************
*  Function Name : IssMplsTunnelActive
*  Description   : This Routine Makes the tunnel active.
*  Input(s)      : pTunnelArgs      - Pointer to the TunnelArgs Dummy structure.
*  Output(s)     : None.
*  Return Values : WEB_SUCCESS or WEB_FAILURE
******************************************************************************/

INT1
IssMplsTunnelActive (tTunnelArgs * pTunnelArgs)
{
    UINT4               u4TunnelIndex = pTunnelArgs->u4TunnelIndex;
    UINT4               u4TunnelInstance = pTunnelArgs->u4TunnelInstance;
    UINT4               u4IngressId = pTunnelArgs->u4IngressId;
    UINT4               u4EgressId = pTunnelArgs->u4EgressId;

    UINT4               u4ErrorCode = 0;
    INT4                i4AdminStatus = pTunnelArgs->i4AdminStatus;

    /* Set Tunnel Row Status to active */
    if (nmhTestv2MplsTunnelRowStatus (&u4ErrorCode,
                                      u4TunnelIndex, u4TunnelInstance,
                                      u4IngressId, u4EgressId, ISS_ACTIVE)
        == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }

    if (nmhSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                   u4EgressId, ISS_ACTIVE) == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }

    /* Set Admin Status to up */

    if (nmhTestv2MplsTunnelAdminStatus (&u4ErrorCode,
                                        u4TunnelIndex, u4TunnelInstance,
                                        u4IngressId, u4EgressId,
                                        i4AdminStatus) == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }

    if (nmhSetMplsTunnelAdminStatus (u4TunnelIndex, u4TunnelInstance,
                                     u4IngressId, u4EgressId, i4AdminStatus)
        == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }
    return WEB_SUCCESS;
}

/******************************************************************************
*  Function Name : IssMplsCreateInSegment
*  Description   : This Routine Populates the InSegmentTable and makes it 
*                  active.
*  Input(s)      : pInSegmentIndex     - Pointer to InSegmentIndex
*                  u4InLabel           - In Label
*                  u4InIfIndex         - Incoming Interface 
*  Output(s)     : pu4MplsTnlInIfIndex - Pointer to Mpls Tunnel Interfcace 
*                                        Index.
*  Return Values : WEB_FAILURE or WEB_SUCCESS
*******************************************************************************/

INT1
IssMplsCreateInSegment (tSNMP_OCTET_STRING_TYPE * pInSegmentIndex,
                        UINT4 u4InLabel, UINT4 u4InIfIndex,
                        UINT4 *pu4MplsTnlInIfIndex)
{
    INT4                i4InSegmntNPop = 1;
    UINT4               u4ErrorCode = 0;
    UINT4               u4MplsTnlIfIndex = 0;

    /* Set inlabel value in the already created InSegmentTable */
    if ((nmhTestv2MplsInSegmentLabel (&u4ErrorCode, pInSegmentIndex,
                                      u4InLabel)) == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }
    if ((nmhSetMplsInSegmentLabel (pInSegmentIndex, u4InLabel)) == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }

    /* Set InSegment NPop */
    if ((nmhTestv2MplsInSegmentNPop (&u4ErrorCode, pInSegmentIndex,
                                     i4InSegmntNPop)) == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }
    if ((nmhSetMplsInSegmentNPop (pInSegmentIndex, i4InSegmntNPop))
        == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }

    /* Setting the InSegment ip addr family to ipv4 */
    if (nmhTestv2MplsInSegmentAddrFamily (&u4ErrorCode, pInSegmentIndex,
                                          MPLS_IPV4_ADDR_TYPE) == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }
    if (nmhSetMplsInSegmentAddrFamily (pInSegmentIndex, MPLS_IPV4_ADDR_TYPE)
        == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }
    if (CfaGetFreeInterfaceIndex (&u4MplsTnlIfIndex, CFA_MPLS_TUNNEL)
        == OSIX_FAILURE)
    {
        return WEB_FAILURE;
    }

    /* Set the Created MPLS Tunnel Interface in InSegmentTable */
    if ((nmhTestv2MplsInSegmentInterface (&u4ErrorCode, pInSegmentIndex,
                                          (INT4) u4MplsTnlIfIndex))
        == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }
    if ((nmhSetMplsInSegmentInterface (pInSegmentIndex,
                                       (INT4) u4MplsTnlIfIndex))
        == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }

    /* Create MPLS Tunnel Interface for the Incoming Interface and 
     * stack it over MPLS Interface */
    if (MplsCreateMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlIfIndex,
                                     CFA_MPLS_TUNNEL) == MPLS_FAILURE)
    {
        return WEB_FAILURE;
    }
    *pu4MplsTnlInIfIndex = u4MplsTnlIfIndex;

    /* Set the row status of the InSegment active */
    if ((nmhTestv2MplsInSegmentRowStatus (&u4ErrorCode, pInSegmentIndex,
                                          ISS_ACTIVE)) == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }
    if ((nmhSetMplsInSegmentRowStatus (pInSegmentIndex, ISS_ACTIVE))
        == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }
    return WEB_SUCCESS;
}

/******************************************************************************
*  Function Name : IssMplsCreateOutSegment
*  Description   : This Routine Populates the InSegmentTable and makes it 
*                  active.
*  Input(s)      : pInSegmentIndex      - Pointer to InSegmentIndex
*                  u4OutLabel           - Out Label
*                  u4OutIfIndex         - Outgoing Interface 
*                  u4NextHop            - Next Hop in the tunnel
*  Output(s)     : pu4MplsTnlOutIfIndex - Pointer to Mpls Tunnel Interfcace 
*                                         Index.
*  Return Values : WEB_FAILURE or WEB_SUCCESS
******************************************************************************/

INT1
IssMplsCreateOutSegment (tSNMP_OCTET_STRING_TYPE * pOutSegmentIndex,
                         UINT4 u4OutLabel, UINT4 u4OutIfIndex,
                         UINT4 u4NextHop, UINT4 *pu4MplsTnlOutIfIndex)
{
    tSNMP_OCTET_STRING_TYPE NextHopAddr;
    static UINT1        au1NextHopAddr[MPLS_INDEX_LENGTH] = { 0 };

    UINT4               u4MplsTnlIfIndex = 0;
    UINT4               u4ErrorCode = 0;

    NextHopAddr.pu1_OctetList = au1NextHopAddr;

    /* Set outsegment top label in the already created OutSegmentTable */
    if (nmhTestv2MplsOutSegmentTopLabel (&u4ErrorCode, pOutSegmentIndex,
                                         u4OutLabel) == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }
    if (nmhSetMplsOutSegmentTopLabel (pOutSegmentIndex, u4OutLabel)
        == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }

    /* set out segment push top label to true */
    if (nmhTestv2MplsOutSegmentPushTopLabel (&u4ErrorCode, pOutSegmentIndex,
                                             MPLS_TRUE) == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }
    if (nmhSetMplsOutSegmentPushTopLabel (pOutSegmentIndex, MPLS_TRUE)
        == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }

    /* set the next hop addr type to ipv4 */
    if (nmhTestv2MplsOutSegmentNextHopAddrType (&u4ErrorCode, pOutSegmentIndex,
                                                MPLS_IPV4_ADDR_TYPE)
        == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }
    if (nmhSetMplsOutSegmentNextHopAddrType (pOutSegmentIndex,
                                             MPLS_IPV4_ADDR_TYPE)
        == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }

    /* set nexthop address */
    WEB_INTEGER_TO_OCTETSTRING (u4NextHop, (&NextHopAddr));
    if (nmhTestv2MplsOutSegmentNextHopAddr (&u4ErrorCode, pOutSegmentIndex,
                                            &NextHopAddr) == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }
    if (nmhSetMplsOutSegmentNextHopAddr (pOutSegmentIndex, &NextHopAddr)
        == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }
    if (CfaGetFreeInterfaceIndex (&u4MplsTnlIfIndex, CFA_MPLS_TUNNEL)
        == OSIX_FAILURE)
    {
        return WEB_FAILURE;
    }
    if ((nmhTestv2MplsOutSegmentInterface (&u4ErrorCode, pOutSegmentIndex,
                                           (INT4) u4MplsTnlIfIndex))
        == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }
    if ((nmhSetMplsOutSegmentInterface (pOutSegmentIndex,
                                        (INT4) u4MplsTnlIfIndex))
        == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }
    /* Create a new MPLS Tunnel Interface and stack over MPLS If */
    if (MplsCreateMplsIfOrMplsTnlIf (u4OutIfIndex, u4MplsTnlIfIndex,
                                     CFA_MPLS_TUNNEL) == MPLS_FAILURE)
    {
        return WEB_FAILURE;
    }
    *pu4MplsTnlOutIfIndex = u4MplsTnlIfIndex;

    if ((nmhTestv2MplsOutSegmentRowStatus (&u4ErrorCode, pOutSegmentIndex,
                                           ISS_ACTIVE)) == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }
    if ((nmhSetMplsOutSegmentRowStatus (pOutSegmentIndex, ISS_ACTIVE))
        == SNMP_FAILURE)
    {
        return WEB_FAILURE;
    }
    return WEB_SUCCESS;
}

/******************************************************************************
*  Function Name : IssMplsDeleteTunnel
*  Description   : This Routine deletes the tunnel indexed by the arguments 
*                  passed to this function and its related XC Entries.
*  Input(s)      : pHttp            - Pointer to the global HTTP data structure.
*                  u4TunnelId       - Tunnel Identifier
*                  u4TunnelInstance - Tunnel Instance
*                  u4IngressId      - u4IngressId
*                  u4EgressId       - u4EgressId
*  Output(s)     : None.
*  Return Values : WEB_SUCCESS or WEB_FAILURE
******************************************************************************/

INT4
IssMplsDeleteTunnel (tHttp * pHttp, UINT4 u4TunnelIndex,
                     UINT4 u4TunnelInstance, UINT4 u4IngressId,
                     UINT4 u4EgressId)
{
    tSNMP_OID_TYPE      GetResourcePointer;
    tSNMP_OID_TYPE      XCIndex;

    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCSegmentIndex;
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;

    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH] = { 0 };
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH] = { 0 };
    static UINT4        au4XCIndex[MPLS_TE_XC_TABLE_OFFSET] = { 0 };
    static UINT1        au1XCSegIndex[MPLS_INDEX_LENGTH] = { 0 };
    static UINT4        au4GetRsrcPointer[MPLS_TE_RSRC_TABLE_MAX_RATE_OFFSET];

    UINT4               u4TunnelResourceIndex = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4InIndex = 0;
    UINT4               u4OutIndex = 0;
    UINT4               u4XCInd = 0;
    UINT4               u4L3Intf = 0;
    INT4                i4IfIndex = 0;

    GetResourcePointer.pu4_OidList = au4GetRsrcPointer;
    MEMSET (GetResourcePointer.pu4_OidList, 0, sizeof (UINT4) * 2);
    GetResourcePointer.u4_Length = 0;
    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    XCSegmentIndex.pu1_OctetList = au1XCSegIndex;
    XCSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    XCIndex.pu4_OidList = au4XCIndex;
    XCIndex.u4_Length = 0;

    /* Get The XC Pointer from the Tunnel Indices Passed */
    if (nmhGetMplsTunnelXCPointer
        (u4TunnelIndex, u4TunnelInstance, u4IngressId, u4EgressId,
         &XCIndex) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "No XC is Associated");
        return WEB_FAILURE;
    }
    MplsGetSegFromTunnelXC (&XCIndex, &XCSegmentIndex, &InSegmentIndex,
                            &OutSegmentIndex, &u4XCInd, &u4InIndex,
                            &u4OutIndex);
    /* Destroy the XCTable entry */
    if (u4XCInd != 0)
    {
        if (nmhTestv2MplsXCRowStatus (&u4ErrorCode, &XCSegmentIndex,
                                      &InSegmentIndex,
                                      &OutSegmentIndex,
                                      ISS_DESTROY) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "XC Deletion Failed");
            return WEB_FAILURE;
        }
        if (nmhSetMplsXCRowStatus
            (&XCSegmentIndex, &InSegmentIndex, &OutSegmentIndex,
             ISS_DESTROY) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "XC Deletion Failed");
            return WEB_FAILURE;
        }
    }

    /* Destroy the InSegment entry */
    if (u4InIndex != 0)
    {
        /* Delete the MPLS Tnl Interface associated with the InSegment 
         * Interface*/
        if ((nmhGetMplsInSegmentInterface (&InSegmentIndex,
                                           &i4IfIndex)) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *)
                          "InSegmentInterface is not available");
            return WEB_FAILURE;
        }
        if (i4IfIndex != 0)
        {
#ifdef CFA_WANTED
            if ((CfaUtilGetIfIndexFromMplsTnlIf ((UINT4) i4IfIndex,
                                                 &u4L3Intf,
                                                 TRUE)) == CFA_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "InSegment Interface is not MPLS Enabled");
                return WEB_FAILURE;
            }
#endif
            if (MplsDeleteMplsIfOrMplsTnlIf (u4L3Intf, (UINT4) i4IfIndex,
                                             CFA_MPLS_TUNNEL, MPLS_TRUE)
                == MPLS_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "MPLS Tunnel Interface deletion failed");
                return WEB_FAILURE;
            }
        }
        if (nmhTestv2MplsInSegmentRowStatus (&u4ErrorCode,
                                             &InSegmentIndex,
                                             ISS_DESTROY) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "InSegment Deletion Failed");
            return WEB_FAILURE;
        }
        if (nmhSetMplsInSegmentRowStatus
            (&InSegmentIndex, ISS_DESTROY) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "InSegment Deletion Failed");
            return WEB_FAILURE;
        }
    }

    /* Destroy the OutSegment entry */
    if (u4OutIndex != 0)
    {
        /* Delete the MPLS Tnl Interface associated with the OutSegment 
         * Interface*/
        if ((nmhGetMplsOutSegmentInterface (&OutSegmentIndex,
                                            &i4IfIndex)) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *)
                          "OutSegment Interface is not available");
            return WEB_FAILURE;
        }
        if (i4IfIndex != 0)
        {
#ifdef CFA_WANTED
            if ((CfaUtilGetIfIndexFromMplsTnlIf ((UINT4) i4IfIndex,
                                                 &u4L3Intf,
                                                 TRUE)) == CFA_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "OutSegment Interface is not MPLS Enabled");
                return WEB_FAILURE;
            }
#endif
            if (MplsDeleteMplsIfOrMplsTnlIf (u4L3Intf, (UINT4) i4IfIndex,
                                             CFA_MPLS_TUNNEL, MPLS_TRUE)
                == MPLS_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "MPLS Tunnel Interface deletion failed");
                return WEB_FAILURE;
            }
        }
        if (nmhTestv2MplsOutSegmentRowStatus (&u4ErrorCode,
                                              &OutSegmentIndex,
                                              ISS_DESTROY) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "OutSegment Deletion Failed");
            return WEB_FAILURE;
        }

        if (nmhSetMplsOutSegmentRowStatus
            (&OutSegmentIndex, ISS_DESTROY) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "OutSegment Deletion Failed");
            return WEB_FAILURE;
        }
    }

    /* Destroy the Tunnel Resource  entry */
    if ((nmhGetMplsTunnelResourcePointer
         (u4TunnelIndex, u4TunnelInstance, u4IngressId, u4EgressId,
          &GetResourcePointer)) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "TunnelResourcePointer is not available");
        return WEB_FAILURE;
    }
    if (nmhTestv2MplsTunnelRowStatus (&u4ErrorCode,
                                      u4TunnelIndex, u4TunnelInstance,
                                      u4IngressId, u4EgressId,
                                      ISS_DESTROY) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "TunnelTable Deletion Failed");
        return WEB_FAILURE;
    }

    if (nmhSetMplsTunnelRowStatus
        (u4TunnelIndex, u4TunnelInstance, u4IngressId, u4EgressId,
         ISS_DESTROY) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "TunnelTable Deletion Failed");
        return WEB_FAILURE;
    }

    u4TunnelResourceIndex =
        GetResourcePointer.pu4_OidList[MPLS_TE_RSRC_TABLE_MAX_RATE_OFFSET - 1];
    /* Default Resource Index is one, so don't allow to delete it. */
    if (u4TunnelResourceIndex != 1)
    {
        if (nmhTestv2MplsTunnelResourceRowStatus (&u4ErrorCode,
                                                  u4TunnelResourceIndex,
                                                  ISS_DESTROY) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *)
                          "TunnelResourceTable Deletion Failed");
            return WEB_FAILURE;
        }

        if (nmhSetMplsTunnelResourceRowStatus (u4TunnelResourceIndex,
                                               ISS_DESTROY) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *)
                          "TunnelResourceTable Deletion Failed");
            return WEB_FAILURE;
        }
    }
    return WEB_SUCCESS;
}

#ifdef MI_WANTED
/*******************************************************************************
*  Function Name : IssProcessMplsVfiPage 
*  Description   : This function processes the request coming for the Mpls 
*                  Vfi Page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssProcessMplsVfiPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessMplsVfiGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessMplsVfiSet (pHttp);
    }
}

/*******************************************************************************
*  Function Name : IssProcessMplsVfiGet
*  Description   : This function shows the Mpls VFI Entries in the web page 
*                  and enables user to enter entries to configure Mpls l2 VFI.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/

VOID
IssProcessMplsVfiGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE VplsName;
    tSNMP_OCTET_STRING_TYPE VpnId;
    INT4                i4ContextId = 0;
    INT4                i4PwMode = 0;
    INT4                i4RetValFsMplsVplsL2MapFdbId = 0;

    UINT4               u4VplsIndex = 0;
    UINT4               u4VplsIndexPrev = 0;
    UINT4               u4Temp = 0;
    UINT4               u4VpnId = 0;

    static UINT1        au1VplsName[L2VPN_MAX_VPLS_NAME_LEN + 1] = { 0 };
    static UINT1        au1VpnId[L2VPN_MAX_VPLS_VPNID_LEN] = { 0 };

    BOOL1               bLoop = FALSE;

    pHttp->i4Write = 0;
    VplsName.pu1_OctetList = au1VplsName;
    VplsName.i4_Length = L2VPN_MAX_VPLS_NAME_LEN;
    VpnId.pu1_OctetList = au1VpnId;
    VpnId.i4_Length = L2VPN_MAX_VPLS_VPNID_LEN;

    /* Display the VFI page till the scroll bar Context ID */
    IssPrintAvailableContexts (pHttp);

    /* Display till the Table header of the bottom form in VFI Page */
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    WebnmRegisterLock (pHttp, L2vpnLock, L2vpnUnLock);

    u4Temp = (UINT4) pHttp->i4Write;
    while (1)
    {
        MPLS_L2VPN_LOCK ();
        i4PwMode = 0;
        if ((bLoop == TRUE) &&
            (nmhGetNextIndexFsMplsVplsConfigTable (u4VplsIndexPrev,
                                                   &u4VplsIndex)
             == SNMP_FAILURE))
        {
            break;
        }

        if ((bLoop == FALSE) &&
            (nmhGetFirstIndexFsMplsVplsConfigTable (&u4VplsIndex)
             == SNMP_FAILURE))
        {
            MPLS_L2VPN_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            return;
        }

        bLoop = TRUE;
        u4VplsIndexPrev = u4VplsIndex;

        MEMSET (VplsName.pu1_OctetList, 0, L2VPN_MAX_VPLS_NAME_LEN);
        if (nmhGetFsMplsVplsName (u4VplsIndex, &VplsName) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }

        if (nmhGetFsMplsVplsVpnId (u4VplsIndex, &VpnId) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        if (nmhGetFsMplsVplsL2MapFdbId (u4VplsIndex,
                                        &i4RetValFsMplsVplsL2MapFdbId) ==
            SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        if (nmhGetFsMplsVplsVsi (u4VplsIndex, &i4ContextId) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }

        MPLS_L2VPN_UNLOCK ();
        IssMplsGetPwModeFromVplsIndex (u4VplsIndex, &i4PwMode);

        if (i4PwMode != 1)
        {
            MEMCPY (&u4VpnId, &au1VpnId[3], sizeof (UINT4));
            u4VpnId = OSIX_NTOHL (u4VpnId);
            pHttp->i4Write = (INT4) u4Temp;

            STRCPY (pHttp->au1KeyString, "VFI_NAME");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1VplsName);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, pHttp->au1KeyString);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "VPN_ID");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4VpnId);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, pHttp->au1KeyString);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            STRCPY (pHttp->au1KeyString, "FDB_NAME");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                     i4RetValFsMplsVplsL2MapFdbId);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "CONTEXT_ID");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (i4ContextId != 0)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4ContextId);
            }
            else
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "default");
            }
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, pHttp->au1KeyString);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        }
    }
    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    /* Display all the information in the html page after the Table stop flag */
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    MPLS_L2VPN_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
}

/*******************************************************************************
*  Function Name : IssProcessMplsVfiSet 
*  Description   : This function processes the request coming for
*                  Mpls VFI Page and tries to set the L2 VPLS VFI entries 
*                  with the given use input from the web page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/

VOID
IssProcessMplsVfiSet (tHttp * pHttp)
{

    UINT4               u4VpnId = 0;
    UINT4               u4FdbId = 0;
    UINT1              *pu1VfiName = NULL;
    UINT1              *pu1VcAlias = NULL;

#ifdef VPLSADS_WANTED
    char               *pu1String = NULL;
    char               *pu2String = NULL;
    UINT1              *pu1Rd = NULL;
    UINT1              *pu1Rt = NULL;
#endif

    UINT4               u4ContextId = 0;
    INT4                i4RetVal = WEB_SUCCESS;

    UINT1               au1VfiName[L2VPN_MAX_VPLS_NAME_LEN] = { 0 };
    UINT1               au1VcAlias[VCM_ALIAS_MAX_LEN] = { 0 };

#ifdef VPLSADS_WANTED
    UINT1               au1Mode[L2VPN_MAX_VPLS_MODE_LEN] = { 0 };
    UINT1               au1Mtu[L2VPN_MAX_VPLS_MTU_LEN] = { 0 };
    UINT1               au1ControlWord[L2VPN_MAX_VPLS_CONTROL_WORD_LEN] = { 0 };
    UINT1               au1Rd[L2VPN_MAX_RD_RT_STRING_LEN] = { 0 };
    UINT1               au1Rt[L2VPN_MAX_RD_RT_STRING_LEN] = { 0 };
    UINT1               au1RdTemp[L2VPN_MAX_VPLS_RD_LEN] = { 0 };
    UINT1               au1RtTemp[L2VPN_MAX_VPLS_RT_LEN] = { 0 };
    UINT4               u4LoopIndex1 = 0;
    UINT1
         
         
         
         
         
         
         
        au4String1[L2VPN_MAX_TOKEN_IN_RD_RT_STRING][L2VPN_MAX_RD_RT_STRING_LEN];
    UINT1
         
         
         
         
         
         
         
        au4String2[L2VPN_MAX_TOKEN_IN_RD_RT_STRING][L2VPN_MAX_RD_RT_STRING_LEN];
    UINT4               u4LoopIndex2 = 0;
    UINT1               au1VeName[L2VPN_MAX_VPLS_NAME_LEN] = { 0 };
    UINT1               au1VeId[L2VPN_MAX_VPLS_VPNID_LEN] = { 0 };
    UINT1               au1RtPolicy[L2VPN_MAX_VPLS_RT_LEN] = { 0 };
    UINT4               u4Flag1 = 0;
    UINT4               u4Flag2 = 0;
    UINT4               u4LoopIndex3 = 0;
    UINT4               u4LoopIndex4 = 0;
#endif

    MEMSET (pHttp->au1Name, 0, sizeof (pHttp->au1Name));
    MEMSET (au1VcAlias, 0, VCM_ALIAS_MAX_LEN);
    MEMSET (au1VfiName, 0, L2VPN_MAX_VPLS_NAME_LEN);

#ifdef VPLSADS_WANTED
    MEMSET (au1Mode, 0, L2VPN_MAX_VPLS_NAME_LEN);
    MEMSET (au1Mtu, 0, L2VPN_MAX_VPLS_MTU_LEN);
    MEMSET (au1ControlWord, 0, L2VPN_MAX_VPLS_CONTROL_WORD_LEN);
    MEMSET (au1Rd, 0, L2VPN_MAX_RD_RT_STRING_LEN);
    MEMSET (au1Rt, 0, L2VPN_MAX_RD_RT_STRING_LEN);
    MEMSET (au1RdTemp, 0, L2VPN_MAX_VPLS_RD_LEN);
    MEMSET (au1RtTemp, 0, L2VPN_MAX_VPLS_RT_LEN);
    MEMSET (au1VeName, 0, L2VPN_MAX_VPLS_NAME_LEN);
    MEMSET (au1VeId, 0, L2VPN_MAX_VPLS_VPNID_LEN);
    MEMSET (au1RtPolicy, 0, L2VPN_MAX_VPLS_RT_LEN);
#endif

    pu1VfiName = &au1VfiName[0];
    pu1VcAlias = &au1VcAlias[0];

    STRNCPY (pHttp->au1Name, "VFI_NAME", (sizeof (pHttp->au1Name) - 1));
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1VfiName, pHttp->au1Value, (sizeof (au1VfiName) - 1));

    STRCPY (pHttp->au1Name, "VPN_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4VpnId = (UINT4) ATOI (pHttp->au1Value);
    STRCPY (pHttp->au1Name, "FDB_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4FdbId = (UINT4) ATOI (pHttp->au1Value);
    STRCPY (pHttp->au1Name, "CONTEXT_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY ((CHR1 *) pu1VcAlias, pHttp->au1Value, (sizeof (au1VcAlias) - 1));

#ifdef VPLSADS_WANTED
    STRNCPY (pHttp->au1Name, "mode", (sizeof (pHttp->au1Name) - 1));
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1Mode, pHttp->au1Value, (sizeof (au1Mode) - 1));

    if (STRCMP (au1Mode, "Autodiscover") == 0)
    {

        STRNCPY (pHttp->au1Name, "MTU", (sizeof (pHttp->au1Name) - 1));
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        STRNCPY (au1Mtu, pHttp->au1Value, (sizeof (au1Mtu) - 1));

        STRNCPY (pHttp->au1Name, "Control_word", (sizeof (pHttp->au1Name) - 1));
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        STRNCPY (au1ControlWord, pHttp->au1Value,
                 (sizeof (au1ControlWord) - 1));

        STRNCPY (pHttp->au1Name, "RD", (sizeof (pHttp->au1Name) - 1));
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);

        if (STRLEN (pHttp->au1Value) != 0)
        {
            u4LoopIndex1 = 0;    /* always intialize it to zero */
            u4LoopIndex2 = 0;    /* always intialize it to zero */
            pu1String = strtok ((char *) (pHttp->au1Value), "%3A");
            while (NULL != pu1String)
            {
                STRNCPY (au4String1[u4LoopIndex1], pu1String,
                         L2VPN_MAX_RD_RT_STRING_LEN);
                au4String1[u4LoopIndex1][L2VPN_MAX_RD_RT_STRING_LEN - 1] = '\0';
                STRCPY (&au1Rd[u4LoopIndex2], au4String1[u4LoopIndex1]);
                u4LoopIndex1++;
                u4LoopIndex2 =
                    u4LoopIndex2 +
                    strlen ((const char *) (&au1Rd[u4LoopIndex2]));
                if (0 == u4Flag1)
                {
                    au1Rd[u4LoopIndex2] = ':';
                    u4LoopIndex1++;
                    u4LoopIndex2++;
                    u4Flag1++;
                }
                pu1String = strtok (NULL, "%3A");
            }

            if (WEB_SUCCESS != L2VpnUtilParseAndGenerateRdRt (au1Rd, au1RdTemp))
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Failed in Parsing and Generating the RD");
                return;
            }
            au1RdTemp[1] = L2VPN_VPLS_RD_SUBTYPE;
        }

        MEMSET (pHttp->au1Value, 0, ENM_MAX_NAME_LEN);

        STRNCPY (pHttp->au1Name, "RT", (sizeof (pHttp->au1Name) - 1));
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);

        if (STRLEN (pHttp->au1Value) != 0)
        {

            u4LoopIndex1 = 0;    /* always intialize it to zero */
            u4LoopIndex2 = 0;    /* always intialize it to zero */
            pu2String = strtok ((char *) (pHttp->au1Value), "%3A");
            while (NULL != pu2String)
            {
                STRNCPY (au4String2[u4LoopIndex1], pu2String,
                         L2VPN_MAX_RD_RT_STRING_LEN);
                au4String2[u4LoopIndex1][L2VPN_MAX_RD_RT_STRING_LEN - 1] = '\0';
                STRCPY (&au1Rt[u4LoopIndex2], au4String2[u4LoopIndex1]);
                u4LoopIndex1++;
                u4LoopIndex2 =
                    u4LoopIndex2 +
                    strlen ((const char *) (&au1Rt[u4LoopIndex2]));
                if (0 == u4Flag2)
                {
                    au1Rt[u4LoopIndex2] = ':';
                    u4LoopIndex1++;
                    u4LoopIndex2++;
                    u4Flag2++;
                }
                pu2String = strtok (NULL, "%3A");
            }

            if (WEB_SUCCESS != L2VpnUtilParseAndGenerateRdRt (au1Rt, au1RtTemp))
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Failed in Parsing and Generating the RT");
                return;
            }
            au1RtTemp[1] = L2VPN_VPLS_RT_SUBTYPE;
        }

        MEMSET (pHttp->au1Value, 0, ENM_MAX_NAME_LEN);

        STRNCPY (pHttp->au1Name, "VE_NAME", (sizeof (pHttp->au1Name) - 1));
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        STRNCPY (au1VeName, pHttp->au1Value, (sizeof (au1VeName) - 1));

        MEMSET (pHttp->au1Value, 0, ENM_MAX_NAME_LEN);

        STRNCPY (pHttp->au1Name, "VE_ID", (sizeof (pHttp->au1Name) - 1));
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        STRNCPY (au1VeId, pHttp->au1Value, (sizeof (au1VeId) - 1));

        STRNCPY (pHttp->au1Name, "Rt_Policy", (sizeof (pHttp->au1Name) - 1));
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        STRNCPY (au1RtPolicy, pHttp->au1Value, (sizeof (au1RtPolicy) - 1));

        for (u4LoopIndex3 = 0; u4LoopIndex3 < STRLEN (au1Mtu); ++u4LoopIndex3)
        {
            if (!isdigit (au1Mtu[u4LoopIndex3]))
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "MTU value should be integer");
                return;
            }
        }

        for (u4LoopIndex4 = 0; u4LoopIndex4 < STRLEN (au1VeId); ++u4LoopIndex4)
        {
            if (!isdigit (au1VeId[u4LoopIndex4]))
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "VE Id value should be integer");
                return;
            }
        }

    }

    pu1Rd = au1RdTemp;
    pu1Rt = au1RtTemp;
#endif
    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Add") == 0)
    {
        if ((VcmIsSwitchExist (au1VcAlias, &u4ContextId) == VCM_FALSE))
        {
            IssSendError (pHttp, (CONST INT1 *) "Failed in Getting Context Id");
            return;
        }
#ifdef VPLSADS_WANTED
        i4RetVal = IssMplsCreateVfi (pHttp, pu1VfiName, u4VpnId, u4FdbId,
                                     (INT4) u4ContextId, &au1Mode[0],
                                     &au1Mtu[0], &au1ControlWord[0], &pu1Rd,
                                     &pu1Rt, &au1VeName[0], &au1VeId[0],
                                     (UINT1 *) &au1RtPolicy[0]);
#else
        i4RetVal = IssMplsCreateVfi (pHttp, pu1VfiName, u4VpnId, u4FdbId,
                                     (INT4) u4ContextId);
#endif
    }
    else if (STRCMP (pHttp->au1Value, "Apply") == 0)
    {
#ifdef VPLSADS_WANTED
        i4RetVal = IssMplsCreateVfi (pHttp, pu1VfiName, u4VpnId, u4FdbId,
                                     (INT4) u4ContextId, &au1Mode[0],
                                     &au1Mtu[0], &au1ControlWord[0], &pu1Rd,
                                     &pu1Rt, &au1VeName[0], &au1VeId[0],
                                     (UINT1 *) &au1RtPolicy[0]);
#else
        i4RetVal = IssMplsCreateVfi (pHttp, pu1VfiName, u4VpnId, u4FdbId,
                                     (INT4) u4ContextId);
#endif
    }
    else
    {
        i4RetVal = IssMplsDeleteVfi (pHttp, pu1VfiName);
    }

    if (i4RetVal == WEB_SUCCESS)
    {
        IssProcessMplsVfiGet (pHttp);
    }

}

/*******************************************************************************
*  Function Name : IssMplsDeleteVfi
*  Description   : This routine deletes VFI indexed by VFI Name
*  Input(s)      : pHttp       - Pointer to the global HTTP data structure.
*                  pu1VfiName  - Pointer to the VFI Name
*  Output(s)     : None.
*  Return Values : WEB_SUCCESS or WEB_FAILURE
*******************************************************************************/

INT4
IssMplsDeleteVfi (tHttp * pHttp, UINT1 *pu1VfiName)
{
    tSNMP_OCTET_STRING_TYPE VfiName;

    UINT4               u4VplsIndex = 0;
    UINT4               u4VplsIndexPrev = 0;
    UINT4               u4GetVplsIndex = 0;
    UINT4               u4PwIndex = 0;
    UINT4               u4PwIndexPrev = 0;
    UINT4               u4ErrorCode = 0;

    static UINT1        au1VfiName[L2VPN_MAX_VPLS_NAME_LEN + 1] = { 0 };

    BOOL1               bLoop = FALSE;
    BOOL1               bEntryFound = FALSE;

    VfiName.pu1_OctetList = au1VfiName;
    VfiName.i4_Length = L2VPN_MAX_VPLS_NAME_LEN;

    MPLS_L2VPN_LOCK ();
    /* Get The VPLS Index for the VFI Name given */
    if (nmhGetFirstIndexFsMplsVplsConfigTable (&u4VplsIndex) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "No VPLS Entry Found");
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }
    MPLS_L2VPN_UNLOCK ();

    while (1)
    {
        MPLS_L2VPN_LOCK ();
        u4VplsIndexPrev = u4VplsIndex;
        MEMSET (VfiName.pu1_OctetList, 0, L2VPN_MAX_VPLS_NAME_LEN);
        if (nmhGetFsMplsVplsName (u4VplsIndex, &VfiName) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }

        if (STRNCMP (VfiName.pu1_OctetList, pu1VfiName,
                     L2VPN_MAX_VPLS_NAME_LEN) == 0)
        {
            bEntryFound = TRUE;
            break;
        }

        if ((bEntryFound != TRUE) &&
            (nmhGetNextIndexFsMplsVplsConfigTable (u4VplsIndexPrev,
                                                   &u4VplsIndex)
             == SNMP_FAILURE))
        {
            break;
        }
        MPLS_L2VPN_UNLOCK ();
    }
    MPLS_L2VPN_UNLOCK ();

    if (bEntryFound != TRUE)
    {
        IssSendError (pHttp, (CONST INT1 *) "No Such VFI Exists");
        return WEB_FAILURE;
    }

    /* From the VPLS Index obtained, Get all the Pseudo wires associated 
     * with this VPLS Index and destroy it */
    while (1)
    {
        MPLS_L2VPN_LOCK ();
        if ((bLoop == FALSE) &&
            (nmhGetFirstIndexPwTable (&u4PwIndex) == SNMP_FAILURE))
        {
            break;
        }

        if ((bLoop == TRUE) &&
            (nmhGetNextIndexPwTable (u4PwIndexPrev, &u4PwIndex)
             == SNMP_FAILURE))

        {
            break;
        }

        bLoop = TRUE;
        u4PwIndexPrev = u4PwIndex;

        if (nmhGetFsMplsL2VpnVplsIndex (u4PwIndex, &u4GetVplsIndex)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Cannot Delete VFI");
            MPLS_L2VPN_UNLOCK ();
            return WEB_FAILURE;
        }

        if (u4GetVplsIndex == u4VplsIndex)
        {
            if (nmhTestv2PwRowStatus (&u4ErrorCode, u4PwIndex, ISS_DESTROY)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Cannot Delete VFI");
                MPLS_L2VPN_UNLOCK ();
                return WEB_FAILURE;
            }

            if (nmhSetPwRowStatus (u4PwIndex, ISS_DESTROY) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Cannot Delete VFI");
                MPLS_L2VPN_UNLOCK ();
                return WEB_FAILURE;
            }
        }
        MPLS_L2VPN_UNLOCK ();
    }
    MPLS_L2VPN_UNLOCK ();

    MPLS_L2VPN_LOCK ();
    /* Now, Delete the VPLS Entry */
    if (nmhTestv2FsMplsVplsRowStatus (&u4ErrorCode, u4VplsIndex, ISS_DESTROY)
        == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Cannot Delete VFI");
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }
    if (nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_DESTROY) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Cannot Delete VFI");
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }
    MPLS_L2VPN_UNLOCK ();
    return WEB_SUCCESS;
}

/*******************************************************************************
*  Function Name : IssMplsCreateVfi
*  Description   : This routine Creates VFI.
*  Input(s)      : pHttp       - Pointer to the global HTTP data structure.
*                  pu1VfiName  - Pointer to the VFI Name
*                  u4VpnId     - VPN Id
*                  i4ContextId - ContextId
*  Output(s)     : None.
*  Return Values : WEB_SUCCESS or WEB_FAILURE
*******************************************************************************/
#ifdef VPLSADS_WANTED
INT4
IssMplsCreateVfi (tHttp * pHttp, UINT1 *pu1VfiName, UINT4 u4VpnId,
                  UINT4 u4FdbId, INT4 i4ContextId,
                  UINT1 *pau1Mode, UINT1 *pau1Mtu,
                  UINT1 *pau1ControlWord, UINT1 **ppau1Rd,
                  UINT1 **ppau1Rt, UINT1 *pu1VeName,
                  UINT1 *pu1VeId, UINT1 *pu1RtPolicy)
#else
INT4
IssMplsCreateVfi (tHttp * pHttp, UINT1 *pu1VfiName, UINT4 u4VpnId,
                  UINT4 u4FdbId, INT4 i4ContextId)
#endif
{
    UINT4               u4VplsIndex = 0;
    UINT4               u4VplsIndexPrev = 0;
    UINT4               u4GetVpnId = 0;
    UINT4               u4ErrorCode = 0;

    INT4                i4GetContextId = 0;
    INT4                i4RowStatus = 0;

#ifdef VPLSADS_WANTED
    INT4                i4FsmplsVplsControlWord = 0;
#endif

    BOOL1               bLoop = FALSE;
    BOOL1               bEntryFound = FALSE;
    BOOL1               u1AddFlag = FALSE;

#ifdef VPLSADS_WANTED
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4VplsRtIndex = 0;
#endif

    tSNMP_OCTET_STRING_TYPE VfiName;
    tSNMP_OCTET_STRING_TYPE VpnId;

#ifdef VPLSADS_WANTED
    tSNMP_OCTET_STRING_TYPE Rd;
    tSNMP_OCTET_STRING_TYPE Rt;

    tSNMP_OCTET_STRING_TYPE VplsVeName;
#endif

    static UINT1        au1VfiName[L2VPN_MAX_VPLS_NAME_LEN + 1] = { 0 };
    static UINT1        au1VpnId[L2VPN_MAX_VPLS_VPNID_LEN] = { 0 };

#ifdef VPLSADS_WANTED
    static UINT1        au1Mode[L2VPN_MAX_VPLS_MODE_LEN] = { 0 };
    static UINT1        au1Mtu[L2VPN_MAX_VPLS_MTU_LEN] = { 0 };
    static UINT1        au1ControlWord[L2VPN_MAX_VPLS_CONTROL_WORD_LEN] = { 0 };
    static UINT1        au1Rd[L2VPN_MAX_VPLS_RD_LEN] = { 0 };
    static UINT1        au1Rt[L2VPN_MAX_VPLS_RT_LEN] = { 0 };
    static UINT1        au1VeName[L2VPN_MAX_VPLS_NAME_LEN] = { 0 };
    static UINT1        au1VeId[L2VPN_MAX_VPLS_VPNID_LEN] = { 0 };

    MEMSET (au1Mode, 0, L2VPN_MAX_VPLS_NAME_LEN);
    MEMSET (au1Mtu, 0, L2VPN_MAX_VPLS_MTU_LEN);
    MEMSET (au1ControlWord, 0, L2VPN_MAX_VPLS_CONTROL_WORD_LEN);
    MEMSET (au1Rd, 0, L2VPN_MAX_VPLS_RD_LEN);
    MEMSET (au1Rt, 0, L2VPN_MAX_VPLS_RT_LEN);
    MEMSET (au1VeName, 0, L2VPN_MAX_VPLS_NAME_LEN);
    MEMSET (au1VeId, 0, L2VPN_MAX_VPLS_VPNID_LEN);
#endif

    VfiName.pu1_OctetList = au1VfiName;
    VpnId.pu1_OctetList = au1VpnId;
    VfiName.i4_Length = L2VPN_MAX_VPLS_NAME_LEN;
    VpnId.i4_Length = L2VPN_MAX_VPLS_VPNID_LEN;

#ifdef VPLSADS_WANTED
    Rd.pu1_OctetList = au1Rd;
    Rt.pu1_OctetList = au1Rt;
    Rd.i4_Length = L2VPN_MAX_VPLS_RD_LEN;
    Rt.i4_Length = L2VPN_MAX_VPLS_RT_LEN;

    VplsVeName.pu1_OctetList = pu1VeName;
    VplsVeName.i4_Length = (INT4) STRLEN (pu1VeName);
#endif

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Add") == 0)
    {
        u1AddFlag = TRUE;
    }
    /* Check whether VFI Name already exists */
    while (1)
    {
        MPLS_L2VPN_LOCK ();
        if ((bLoop == FALSE) &&
            (nmhGetFirstIndexFsMplsVplsConfigTable (&u4VplsIndex)
             == SNMP_FAILURE))
        {
            break;
        }

        u4VplsIndexPrev = u4VplsIndex;
        bLoop = TRUE;

        MEMSET (VfiName.pu1_OctetList, 0, L2VPN_MAX_VPLS_NAME_LEN);
        if (nmhGetFsMplsVplsName (u4VplsIndex, &VfiName) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        if (nmhGetFsMplsVplsVsi (u4VplsIndex, &i4GetContextId) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        if (nmhGetFsMplsVplsVpnId (u4VplsIndex, &VpnId) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        MEMCPY (&u4GetVpnId, &au1VpnId[3], sizeof (UINT4));
        u4GetVpnId = OSIX_NTOHL (u4GetVpnId);

        if (STRNCMP (VfiName.pu1_OctetList, pu1VfiName,
                     L2VPN_MAX_VPLS_NAME_LEN) == 0)
        {
            bEntryFound = TRUE;
            break;
        }

#ifdef VPLSADS_WANTED
        if (STRCMP (pau1Mode, "Autodiscover") == 0)
        {
#if 0
            if (pau1Mtu != NULL)
            {
                if (nmhGetFsmplsVplsMtu (u4VplsIndex, (UINT4 *) (&au1Mtu)) ==
                    SNMP_FAILURE)
                {
                    MPLS_L2VPN_UNLOCK ();
                    continue;
                }
                if (STRNCMP (au1Mtu, pau1Mtu, L2VPN_MAX_VPLS_MTU_LEN) == 0)
                {
                    bEntryFound = TRUE;
                    break;
                }
            }

            if (pau1ControlWord != NULL)
            {
                if (nmhGetFsmplsVplsControlWord
                    (u4VplsIndex, (INT4 *) (&au1ControlWord)) == SNMP_FAILURE)
                {
                    MPLS_L2VPN_UNLOCK ();
                    continue;
                }
                if (STRNCMP
                    (au1ControlWord, pau1ControlWord,
                     L2VPN_MAX_VPLS_CONTROL_WORD_LEN) == 0)
                {
                    bEntryFound = TRUE;
                    break;
                }
            }
#endif

            if (**ppau1Rd != 0)
            {
                if (nmhGetVplsBgpADConfigRouteDistinguisher (u4VplsIndex, &Rd)
                    == SNMP_FAILURE)
                {
                    MPLS_L2VPN_UNLOCK ();
                    continue;
                }
                if (STRNCMP (Rd.pu1_OctetList, *ppau1Rd, L2VPN_MAX_VPLS_RD_LEN)
                    == 0)
                {
                    bEntryFound = TRUE;
                    break;
                }
            }

            /* Get the free RT Index */
            if (SNMP_FAILURE ==
                L2VpnGetFreeRtIndexForVpls (u4VplsIndex, &u4VplsRtIndex))
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "Free Rt Index not available");
                MPLS_L2VPN_UNLOCK ();
                return WEB_FAILURE;
            }

            if (**ppau1Rt != 0)
            {
                if (nmhGetVplsBgpRteTargetRT (u4VplsIndex, u4VplsRtIndex, &Rt)
                    == SNMP_FAILURE)
                {
                    MPLS_L2VPN_UNLOCK ();
                    continue;
                }
                if (STRNCMP (Rt.pu1_OctetList, *ppau1Rt, L2VPN_MAX_VPLS_RT_LEN)
                    == 0)
                {
                    bEntryFound = TRUE;
                    break;
                }
            }

        }
#endif
        if ((bLoop == TRUE) &&
            (nmhGetNextIndexFsMplsVplsConfigTable (u4VplsIndexPrev,
                                                   &u4VplsIndex)
             == SNMP_FAILURE))
        {
            break;
        }
        MPLS_L2VPN_UNLOCK ();
    }
    MPLS_L2VPN_UNLOCK ();

    /* If already VPLS Entry Exists, Check whether it exists in different 
     * context, and some or same vpn id passed exists already */
    if ((bEntryFound == TRUE) && (u1AddFlag != FALSE))
    {
        if (i4GetContextId != i4ContextId)
        {
            IssSendError (pHttp, (CONST INT1 *)
                          "Same VPLS name is associated in another VSI");
            return WEB_FAILURE;
        }

        if (u4GetVpnId != 0)
        {
            IssSendError (pHttp, (CONST INT1 *)
                          "Already a VPN is associated with this VPLS");
            return WEB_FAILURE;
        }

        if (u4GetVpnId == u4VpnId)
        {
            IssSendError (pHttp, (CONST INT1 *)
                          "Same VPN is associated with this VPLS");
            return WEB_FAILURE;
        }
    }
    if (u1AddFlag != FALSE)
    {
        /* Get The VPLS Index to be used to create VPLS Entry */
        for (u4VplsIndex = 1; u4VplsIndex < L2VPN_MAX_VPLS_ENTRIES;
             u4VplsIndex++)
        {
            MPLS_L2VPN_LOCK ();
            if (nmhGetFsMplsVplsRowStatus (u4VplsIndex, &i4RowStatus)
                == SNMP_FAILURE)
            {
                MPLS_L2VPN_UNLOCK ();
                break;
            }
            MPLS_L2VPN_UNLOCK ();
        }

        VfiName.pu1_OctetList = pu1VfiName;
        VfiName.i4_Length = (INT4) STRLEN (pu1VfiName);
        STRNCPY (au1VpnId, MPLS_OUI_VPN_ID, STRLEN (MPLS_OUI_VPN_ID));
        au1VpnId[(STRLEN (MPLS_OUI_VPN_ID))] = '\0';
        u4VpnId = OSIX_NTOHL (u4VpnId);
        MEMCPY (&au1VpnId[3], &u4VpnId, sizeof (UINT4));

#ifdef VPLSADS_WANTED
        if (**ppau1Rd != 0)
        {
            Rd.pu1_OctetList = *ppau1Rd;
            Rd.i4_Length = L2VPN_MAX_VPLS_RD_LEN;
        }

        if (**ppau1Rt != 0)
        {
            Rt.pu1_OctetList = *ppau1Rt;
            Rt.i4_Length = L2VPN_MAX_VPLS_RT_LEN;
        }

        VplsVeName.pu1_OctetList = pu1VeName;
        VplsVeName.i4_Length = (INT4) STRLEN (pu1VeName);
#endif

        MPLS_L2VPN_LOCK ();
        /* Create the VPLS Entry */
        if (nmhTestv2FsMplsVplsRowStatus
            (&u4ErrorCode, u4VplsIndex, ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Cannot Create VPLS Entry");
            MPLS_L2VPN_UNLOCK ();
            return WEB_FAILURE;
        }
        if (nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Cannot Create VPLS Entry");
            MPLS_L2VPN_UNLOCK ();
            return WEB_FAILURE;
        }
        /* Set Context ID */
        if (nmhTestv2FsMplsVplsVsi (&u4ErrorCode, u4VplsIndex, i4ContextId)
            == SNMP_FAILURE)
        {
            nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_DESTROY);
            IssSendError (pHttp, (CONST INT1 *) "Failed to set Context Id");
            MPLS_L2VPN_UNLOCK ();
            return WEB_FAILURE;
        }
        if (nmhSetFsMplsVplsVsi (u4VplsIndex, i4ContextId) == SNMP_FAILURE)
        {
            nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_DESTROY);
            IssSendError (pHttp, (CONST INT1 *) "Failed to set Context Id");
            MPLS_L2VPN_UNLOCK ();
            return WEB_FAILURE;
        }

        /* Set VPLS Name */
        if (nmhTestv2FsMplsVplsName (&u4ErrorCode, u4VplsIndex, &VfiName)
            == SNMP_FAILURE)
        {
            nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_DESTROY);
            IssSendError (pHttp, (CONST INT1 *) "Failed to set the VPLS Name");
            MPLS_L2VPN_UNLOCK ();
            return WEB_FAILURE;
        }
        if (nmhSetFsMplsVplsName (u4VplsIndex, &VfiName) == SNMP_FAILURE)
        {
            nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_DESTROY);
            IssSendError (pHttp, (CONST INT1 *) "Failed to set the VPLS Name");
            MPLS_L2VPN_UNLOCK ();
            return WEB_FAILURE;
        }

#ifdef VPLSADS_WANTED
        if (STRCMP (pau1Mode, "Autodiscover") == 0)
        {

            /* Set the signaling type as BGP */
            if (nmhTestv2FsmplsVplsSignalingType
                (&u4ErrorCode, u4VplsIndex, L2VPN_VPLS_SIG_BGP) == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Failed to test the VPLS Signaling type as BGP");
                MPLS_L2VPN_UNLOCK ();
                return WEB_FAILURE;
            }

            if (nmhSetFsmplsVplsSignalingType (u4VplsIndex, L2VPN_VPLS_SIG_BGP)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Failed to set the VPLS Signaling type as BGP");
                MPLS_L2VPN_UNLOCK ();
                return WEB_FAILURE;
            }

            if (pau1Mtu != NULL)
            {
                /* Set the VPN MTU */
                if (nmhTestv2FsmplsVplsMtu
                    (&u4ErrorCode, u4VplsIndex,
                     (UINT4) (atoi ((const char *) pau1Mtu))) == SNMP_FAILURE)
                {
                    nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_DESTROY);
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Failed to set the VPLS MTU");
                    MPLS_L2VPN_UNLOCK ();
                    return WEB_FAILURE;
                }
                if (nmhSetFsmplsVplsMtu
                    (u4VplsIndex,
                     (UINT4) (atoi ((const char *) pau1Mtu))) == SNMP_FAILURE)
                {
                    nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_DESTROY);
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Failed to set the VPLS MTU");
                    MPLS_L2VPN_UNLOCK ();
                    return WEB_FAILURE;
                }
            }

            if (pau1ControlWord != NULL)
            {
                /* Set the VPN Control Word */
                if (STRCMP (pau1ControlWord, "Enable") == 0)
                {
                    i4FsmplsVplsControlWord = 1;
                }
                else
                {
                    i4FsmplsVplsControlWord = 2;
                }
                if (nmhTestv2FsmplsVplsControlWord
                    (&u4ErrorCode, u4VplsIndex,
                     i4FsmplsVplsControlWord) == SNMP_FAILURE)
                {
                    nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_DESTROY);
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Failed to set the VPLS Control Word");
                    MPLS_L2VPN_UNLOCK ();
                    return WEB_FAILURE;
                }
                if (nmhSetFsmplsVplsControlWord
                    (u4VplsIndex, i4FsmplsVplsControlWord) == SNMP_FAILURE)
                {
                    nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_DESTROY);
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Failed to set the VPLS Control Word");
                    MPLS_L2VPN_UNLOCK ();
                    return WEB_FAILURE;
                }
            }

            if (**ppau1Rd != 0)
            {
                /* Set the VPN RD */
                i4RetStatus = nmhTestv2VplsBgpADConfigRowStatus (&u4ErrorCode,
                                                                 u4VplsIndex,
                                                                 CREATE_AND_WAIT);
                if (SNMP_FAILURE == i4RetStatus)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "nmhTestv2VplsBgpADConfigRowStatus(c&w) Failed");
                    MPLS_L2VPN_UNLOCK ();
                    return WEB_FAILURE;
                }

                i4RetStatus = nmhSetVplsBgpADConfigRowStatus (u4VplsIndex,
                                                              CREATE_AND_WAIT);
                if (SNMP_FAILURE == i4RetStatus)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "nmhSetVplsBgpADConfigRowStatus(c&w) Failed");
                    MPLS_L2VPN_UNLOCK ();
                    return WEB_FAILURE;
                }

                if (nmhTestv2VplsBgpADConfigRouteDistinguisher
                    (&u4ErrorCode, u4VplsIndex, &Rd) == SNMP_FAILURE)
                {
                    nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_DESTROY);
                    nmhSetVplsBgpADConfigRowStatus (u4VplsIndex, ISS_DESTROY);
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Failed to set the VPLS RD");
                    MPLS_L2VPN_UNLOCK ();
                    return WEB_FAILURE;
                }
                if (nmhSetVplsBgpADConfigRouteDistinguisher (u4VplsIndex, &Rd)
                    == SNMP_FAILURE)
                {
                    nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_DESTROY);
                    nmhSetVplsBgpADConfigRowStatus (u4VplsIndex, ISS_DESTROY);
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Failed to set the VPLS RD");
                    MPLS_L2VPN_UNLOCK ();
                    return WEB_FAILURE;
                }
            }

            if (**ppau1Rt != 0)
            {
                /* Set the VPN RT */
                if (SNMP_FAILURE ==
                    L2VpnGetFreeRtIndexForVpls (u4VplsIndex, &u4VplsRtIndex))
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Free Rt Index not available");
                    MPLS_L2VPN_UNLOCK ();
                    return WEB_FAILURE;
                }

                i4RetStatus =
                    nmhTestv2VplsBgpRteTargetRTRowStatus (&u4ErrorCode,
                                                          u4VplsIndex,
                                                          u4VplsRtIndex,
                                                          CREATE_AND_WAIT);
                if (SNMP_FAILURE == i4RetStatus)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "nmhTestv2VplsBgpRteTargetRTRowStatus(c&w) Failed");
                    MPLS_L2VPN_UNLOCK ();
                    return WEB_FAILURE;
                }

                i4RetStatus = nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex,
                                                                 u4VplsRtIndex,
                                                                 CREATE_AND_WAIT);
                if (SNMP_FAILURE == i4RetStatus)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "nmhSetVplsBgpRteTargetRTRowStatus(c&w) Failed");
                    MPLS_L2VPN_UNLOCK ();
                    return WEB_FAILURE;
                }

                if (nmhTestv2VplsBgpRteTargetRT
                    (&u4ErrorCode, u4VplsIndex, u4VplsRtIndex,
                     &Rt) == SNMP_FAILURE)
                {
                    nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_DESTROY);
                    nmhSetVplsBgpADConfigRowStatus (u4VplsIndex, ISS_DESTROY);
                    nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex,
                                                       u4VplsRtIndex,
                                                       ISS_DESTROY);
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Failed to set the VPLS RT");
                    MPLS_L2VPN_UNLOCK ();
                    return WEB_FAILURE;
                }
                if (nmhSetVplsBgpRteTargetRT (u4VplsIndex, u4VplsRtIndex, &Rt)
                    == SNMP_FAILURE)
                {
                    nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_DESTROY);
                    nmhSetVplsBgpADConfigRowStatus (u4VplsIndex, ISS_DESTROY);
                    nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex,
                                                       u4VplsRtIndex,
                                                       ISS_DESTROY);
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Failed to set the VPLS RT");
                    MPLS_L2VPN_UNLOCK ();
                    return WEB_FAILURE;
                }

                /* Set the RT Policy */
                if (STRCMP (pu1RtPolicy, "Import") == 0)
                {
                    nmhSetVplsBgpRteTargetRTType (u4VplsIndex, u4VplsRtIndex,
                                                  L2VPN_VPLS_RT_IMPORT);
                }
                else if (STRCMP (pu1RtPolicy, "Export") == 0)
                {
                    nmhSetVplsBgpRteTargetRTType (u4VplsIndex, u4VplsRtIndex,
                                                  L2VPN_VPLS_RT_EXPORT);
                }
                else
                {
                    nmhSetVplsBgpRteTargetRTType (u4VplsIndex, u4VplsRtIndex,
                                                  L2VPN_VPLS_RT_BOTH);
                }
            }

            /* Set the VE NAME and VE ID */
            i4RetStatus = nmhTestv2VplsBgpVERowStatus (&u4ErrorCode,
                                                       u4VplsIndex,
                                                       (UINT4) (atoi
                                                                ((const char *)
                                                                 pu1VeId)),
                                                       CREATE_AND_WAIT);
            if (SNMP_FAILURE == i4RetStatus)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "nmhTestv2VplsBgpVERowStatus(c&w) Failed");
                MPLS_L2VPN_UNLOCK ();
                return WEB_FAILURE;
            }

            i4RetStatus = nmhSetVplsBgpVERowStatus (u4VplsIndex,
                                                    (UINT4) (atoi
                                                             ((const char *)
                                                              pu1VeId)),
                                                    CREATE_AND_WAIT);
            if (SNMP_FAILURE == i4RetStatus)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "nmhSetVplsBgpVERowStatus(c&w) Failed");
                MPLS_L2VPN_UNLOCK ();
                return WEB_FAILURE;
            }

            i4RetStatus = nmhTestv2VplsBgpVEName (&u4ErrorCode,
                                                  u4VplsIndex,
                                                  (UINT4) (atoi
                                                           ((const char *)
                                                            pu1VeId)),
                                                  &VplsVeName);
            if (SNMP_FAILURE == i4RetStatus)
            {
                nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_DESTROY);
                nmhSetVplsBgpADConfigRowStatus (u4VplsIndex, ISS_DESTROY);
                nmhSetVplsBgpVERowStatus (u4VplsIndex,
                                          (UINT4) (atoi
                                                   ((const char *) pu1VeId)),
                                          ISS_DESTROY);
                nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex, u4VplsRtIndex,
                                                   ISS_DESTROY);
                IssSendError (pHttp,
                              (CONST INT1 *) "nmhTestv2VplsBgpVEName Failed");
                MPLS_L2VPN_UNLOCK ();
                return WEB_FAILURE;
            }

            i4RetStatus = nmhSetVplsBgpVEName (u4VplsIndex,
                                               (UINT4) (atoi
                                                        ((const char *)
                                                         pu1VeId)),
                                               &VplsVeName);
            if (SNMP_FAILURE == i4RetStatus)
            {
                nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_DESTROY);
                nmhSetVplsBgpADConfigRowStatus (u4VplsIndex, ISS_DESTROY);
                nmhSetVplsBgpVERowStatus (u4VplsIndex,
                                          (UINT4) (atoi
                                                   ((const char *) pu1VeId)),
                                          ISS_DESTROY);
                nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex, u4VplsRtIndex,
                                                   ISS_DESTROY);
                IssSendError (pHttp,
                              (CONST INT1 *) "nmhSetVplsBgpVEName Failed");
                MPLS_L2VPN_UNLOCK ();
                return WEB_FAILURE;
            }

        }

#endif
        /* Set the VPN ID */
        if (nmhTestv2FsMplsVplsVpnId (&u4ErrorCode, u4VplsIndex,
                                      &VpnId) == SNMP_FAILURE)
        {
            nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_DESTROY);
#ifdef VPLSADS_WANTED
            nmhSetVplsBgpADConfigRowStatus (u4VplsIndex, ISS_DESTROY);
            nmhSetVplsBgpVERowStatus (u4VplsIndex,
                                      (UINT4) (atoi ((const char *) pu1VeId)),
                                      ISS_DESTROY);
            nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex, u4VplsRtIndex,
                                               ISS_DESTROY);
#endif
            IssSendError (pHttp, (CONST INT1 *) "Failed to set VPN ID");
            MPLS_L2VPN_UNLOCK ();
            return WEB_FAILURE;
        }
        if (nmhSetFsMplsVplsVpnId (u4VplsIndex, &VpnId) == SNMP_FAILURE)
        {
            nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_DESTROY);
#ifdef VPLSADS_WANTED
            nmhSetVplsBgpADConfigRowStatus (u4VplsIndex, ISS_DESTROY);
            nmhSetVplsBgpVERowStatus (u4VplsIndex,
                                      (UINT4) (atoi ((const char *) pu1VeId)),
                                      ISS_DESTROY);
            nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex, u4VplsRtIndex,
                                               ISS_DESTROY);
#endif
            IssSendError (pHttp, (CONST INT1 *) "Failed to set VPN ID");
            MPLS_L2VPN_UNLOCK ();
            return WEB_FAILURE;
        }

        if (u4FdbId != 0)
        {
            /* mapping VplsInstance to a L2 FDB Id */
            if (nmhTestv2FsMplsVplsL2MapFdbId (&u4ErrorCode, u4VplsIndex,
                                               (INT4) u4FdbId) == SNMP_FAILURE)
            {
                nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_DESTROY);
#ifdef VPLSADS_WANTED
                nmhSetVplsBgpADConfigRowStatus (u4VplsIndex, ISS_DESTROY);
                nmhSetVplsBgpVERowStatus (u4VplsIndex,
                                          (UINT4) (atoi
                                                   ((const char *) pu1VeId)),
                                          ISS_DESTROY);
                nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex, u4VplsRtIndex,
                                                   ISS_DESTROY);
#endif
                IssSendError (pHttp, (CONST INT1 *) "Failed to set FDB ID");
                MPLS_L2VPN_UNLOCK ();
                return WEB_FAILURE;
            }

            if (nmhSetFsMplsVplsL2MapFdbId (u4VplsIndex, u4FdbId) ==
                SNMP_FAILURE)
            {
                nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_DESTROY);
#ifdef VPLSADS_WANTED
                nmhSetVplsBgpADConfigRowStatus (u4VplsIndex, ISS_DESTROY);
                nmhSetVplsBgpVERowStatus (u4VplsIndex,
                                          (UINT4) (atoi
                                                   ((const char *) pu1VeId)),
                                          ISS_DESTROY);
                nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex, u4VplsRtIndex,
                                                   ISS_DESTROY);
#endif
                IssSendError (pHttp, (CONST INT1 *) "Failed to set FDB ID");
                MPLS_L2VPN_UNLOCK ();
                return WEB_FAILURE;
            }
        }
    }
    else
    {
        if (nmhTestv2FsMplsVplsRowStatus
            (&u4ErrorCode, u4VplsIndex, ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Cannot Create VPLS Entry");
            MPLS_L2VPN_UNLOCK ();
            return WEB_FAILURE;
        }
        if (nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Cannot Create VPLS Entry");
            MPLS_L2VPN_UNLOCK ();
            return WEB_FAILURE;
        }
        if (u4FdbId == 0)
        {
            u4FdbId = L2VPN_VPLS_FDB_DEF_VAL;
        }
        /* mapping VplsInstance to a L2 FDB Id */
        if (nmhTestv2FsMplsVplsL2MapFdbId (&u4ErrorCode, u4VplsIndex,
                                           (INT4) u4FdbId) == SNMP_FAILURE)
        {
            nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_ACTIVE);
            IssSendError (pHttp, (CONST INT1 *) "Failed to set FDB ID");
            MPLS_L2VPN_UNLOCK ();
            return WEB_FAILURE;
        }

        if (nmhSetFsMplsVplsL2MapFdbId (u4VplsIndex, u4FdbId) == SNMP_FAILURE)
        {
            nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_ACTIVE);
            IssSendError (pHttp, (CONST INT1 *) "Failed to set FDB ID");
            MPLS_L2VPN_UNLOCK ();
            return WEB_FAILURE;
        }

    }
    /* Make the VPLS Entry Active */
#ifdef VPLSADS_WANTED
    if (STRCMP (pau1Mode, "Autodiscover") == 0)
    {

        i4RetStatus = nmhTestv2VplsBgpVERowStatus (&u4ErrorCode,
                                                   u4VplsIndex,
                                                   (UINT4) (atoi
                                                            ((const char *)
                                                             pu1VeId)),
                                                   ISS_ACTIVE);
        if (SNMP_FAILURE == i4RetStatus)
        {
            nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_DESTROY);
            nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex, u4VplsRtIndex,
                                               ISS_DESTROY);
            nmhSetVplsBgpVERowStatus (u4VplsIndex,
                                      (UINT4) (atoi ((const char *) pu1VeId)),
                                      ISS_DESTROY);
            nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex, u4VplsRtIndex,
                                               ISS_DESTROY);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "nmhTestv2VplsBgpVERowStatus(ACTIVE) Failed");
            MPLS_L2VPN_UNLOCK ();
            return WEB_FAILURE;
        }

        i4RetStatus = nmhSetVplsBgpVERowStatus (u4VplsIndex,
                                                (UINT4) (atoi
                                                         ((const char *)
                                                          pu1VeId)),
                                                ISS_ACTIVE);
        if (SNMP_FAILURE == i4RetStatus)
        {
            nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_DESTROY);
            nmhSetVplsBgpADConfigRowStatus (u4VplsIndex, ISS_DESTROY);
            nmhSetVplsBgpVERowStatus (u4VplsIndex,
                                      (UINT4) (atoi ((const char *) pu1VeId)),
                                      ISS_DESTROY);
            nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex, u4VplsRtIndex,
                                               ISS_DESTROY);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "nmhSetVplsBgpVERowStatus(ACTIVE) Failed");
            MPLS_L2VPN_UNLOCK ();
            return WEB_FAILURE;
        }

        if (**ppau1Rd != 0)
        {
            i4RetStatus = nmhTestv2VplsBgpADConfigRowStatus (&u4ErrorCode,
                                                             u4VplsIndex,
                                                             ISS_ACTIVE);
            if (SNMP_FAILURE == i4RetStatus)
            {
                nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_DESTROY);
                nmhSetVplsBgpADConfigRowStatus (u4VplsIndex, ISS_DESTROY);
                nmhSetVplsBgpVERowStatus (u4VplsIndex,
                                          (UINT4) (atoi
                                                   ((const char *) pu1VeId)),
                                          ISS_DESTROY);
                nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex, u4VplsRtIndex,
                                                   ISS_DESTROY);
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "nmhTestv2VplsBgpADConfigRowStatus(ACTIVE) Failed");
                MPLS_L2VPN_UNLOCK ();
                return WEB_FAILURE;
            }

            i4RetStatus = nmhSetVplsBgpADConfigRowStatus (u4VplsIndex,
                                                          ISS_ACTIVE);
            if (SNMP_FAILURE == i4RetStatus)
            {
                nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_DESTROY);
                nmhSetVplsBgpADConfigRowStatus (u4VplsIndex, ISS_DESTROY);
                nmhSetVplsBgpVERowStatus (u4VplsIndex,
                                          (UINT4) (atoi
                                                   ((const char *) pu1VeId)),
                                          ISS_DESTROY);
                nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex, u4VplsRtIndex,
                                                   ISS_DESTROY);
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "nmhSetVplsBgpADConfigRowStatus(ACTIVE) Failed");
                MPLS_L2VPN_UNLOCK ();
                return WEB_FAILURE;
            }
        }

        if (**ppau1Rt != 0)
        {
            i4RetStatus = nmhTestv2VplsBgpRteTargetRTRowStatus (&u4ErrorCode,
                                                                u4VplsIndex,
                                                                u4VplsRtIndex,
                                                                ISS_ACTIVE);
            if (SNMP_FAILURE == i4RetStatus)
            {
                nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_DESTROY);
                nmhSetVplsBgpADConfigRowStatus (u4VplsIndex, ISS_DESTROY);
                nmhSetVplsBgpVERowStatus (u4VplsIndex,
                                          (UINT4) (atoi
                                                   ((const char *) pu1VeId)),
                                          ISS_DESTROY);
                nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex, u4VplsRtIndex,
                                                   ISS_DESTROY);
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "nmhTestv2VplsBgpRteTargetRTRowStatus(ACTIVE) Failed");
                MPLS_L2VPN_UNLOCK ();
                return WEB_FAILURE;
            }

            i4RetStatus = nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex,
                                                             u4VplsRtIndex,
                                                             ISS_ACTIVE);

            if (SNMP_FAILURE == i4RetStatus)
            {
                nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_DESTROY);
                nmhSetVplsBgpADConfigRowStatus (u4VplsIndex, ISS_DESTROY);
                nmhSetVplsBgpVERowStatus (u4VplsIndex,
                                          (UINT4) (atoi
                                                   ((const char *) pu1VeId)),
                                          ISS_DESTROY);
                nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex, u4VplsRtIndex,
                                                   ISS_DESTROY);
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "nmhSetVplsBgpRteTargetRTRowStatus(ACTIVE) Failed");
                MPLS_L2VPN_UNLOCK ();
                return WEB_FAILURE;
            }
        }
    }
#endif
    if (nmhTestv2FsMplsVplsRowStatus (&u4ErrorCode, u4VplsIndex, ISS_ACTIVE)
        == SNMP_FAILURE)
    {
        nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_DESTROY);
#ifdef VPLSADS_WANTED
        nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex, u4VplsRtIndex,
                                           ISS_DESTROY);
        nmhSetVplsBgpVERowStatus (u4VplsIndex,
                                  (UINT4) (atoi ((const char *) pu1VeId)),
                                  ISS_DESTROY);
        nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex, u4VplsRtIndex,
                                           ISS_DESTROY);
#endif
        IssSendError (pHttp, (CONST INT1 *) "Cannot VPLS Entry Active");
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }
    if (nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsMplsVplsRowStatus (u4VplsIndex, ISS_DESTROY);
#ifdef VPLSADS_WANTED
        nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex, u4VplsRtIndex,
                                           ISS_DESTROY);
        nmhSetVplsBgpVERowStatus (u4VplsIndex,
                                  (UINT4) (atoi ((const char *) pu1VeId)),
                                  ISS_DESTROY);
        nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex, u4VplsRtIndex,
                                           ISS_DESTROY);
#endif
        IssSendError (pHttp, (CONST INT1 *) "Cannot Make VPLS Entry Active");
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }

    MPLS_L2VPN_UNLOCK ();
    return WEB_SUCCESS;
}
#endif

/*******************************************************************************
*  Function Name : IssProcessMplsPwPage 
*  Description   : This function processes the request coming for the Mpls Pw
*                  Page.                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssProcessMplsPwPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessMplsPwGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessMplsPwSet (pHttp);
    }
}

/*******************************************************************************
*  Function Name : IssProcessMplsPwGet
*  Description   : This routines gets and displays all the pseudo wires created
*                  and enables the user to create pseudo wires.                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssProcessMplsPwGet (tHttp * pHttp)
{
    UINT4               u4PwIndex = 0;
    UINT4               u4PwIndexPrev = 0;
    UINT4               u4VplsIndex = 0;
    UINT4               u4LocalLabel = 0;
    UINT4               u4RemoteLabel = 0;
    UINT4               u4PeerAddr = 0;
    UINT4               u4Temp = 0;

    tSNMP_OCTET_STRING_TYPE PeerAddr;
    tSNMP_OCTET_STRING_TYPE VplsName;

    static UINT1        au1PeerAddr[IPVX_IPV6_ADDR_LEN] = { 0 };
    static UINT1        au1VplsName[L2VPN_MAX_VPLS_NAME_LEN + 1] = { 0 };
    UINT1              *pu1Temp = NULL;

    INT4                i4PwMode = 0;
    BOOL1               bLoop = FALSE;

    PeerAddr.pu1_OctetList = au1PeerAddr;
    VplsName.pu1_OctetList = au1VplsName;
    PeerAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
    VplsName.i4_Length = L2VPN_MAX_VPLS_NAME_LEN;

    pHttp->i4Write = 0;

#ifdef MI_WANTED
    IssPrintAvailableVfi (pHttp);
#endif

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    u4Temp = (UINT4) pHttp->i4Write;

    WebnmRegisterLock (pHttp, L2vpnLock, L2vpnUnLock);
    while (1)
    {
        MPLS_L2VPN_LOCK ();
        if ((bLoop == FALSE) &&
            (nmhGetFirstIndexPwTable (&u4PwIndex) == SNMP_FAILURE))
        {
            MPLS_L2VPN_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            return;
        }

        if ((bLoop == TRUE) &&
            (nmhGetNextIndexPwTable (u4PwIndexPrev, &u4PwIndex)
             == SNMP_FAILURE))
        {
            break;
        }

        u4PwIndexPrev = u4PwIndex;
        bLoop = TRUE;

        if (nmhGetPwPeerAddr (u4PwIndex, &PeerAddr) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }

        if (nmhGetPwInboundLabel (u4PwIndex, &u4LocalLabel) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }

        if (nmhGetPwOutboundLabel (u4PwIndex, &u4RemoteLabel) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }

        if (nmhGetFsMplsL2VpnPwMode (u4PwIndex, &i4PwMode) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }

        if (i4PwMode == 2)
        {
            if (nmhGetFsMplsL2VpnVplsIndex (u4PwIndex, &u4VplsIndex)
                == SNMP_FAILURE)
            {
                MPLS_L2VPN_UNLOCK ();
                continue;
            }

            MEMSET (VplsName.pu1_OctetList, 0, L2VPN_MAX_VPLS_NAME_LEN);
            if (nmhGetFsMplsVplsName (u4VplsIndex, &VplsName) == SNMP_FAILURE)
            {
                MPLS_L2VPN_UNLOCK ();
                continue;
            }
        }
        MPLS_L2VPN_UNLOCK ();

        pHttp->i4Write = (INT4) u4Temp;

        STRCPY (pHttp->au1KeyString, "PW_INDEX");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4PwIndex);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "PW_PEER");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WEB_OCTETSTRING_TO_INTEGER ((&PeerAddr), u4PeerAddr);
        WEB_CONVERT_IPADDR_TO_STR (pu1Temp, u4PeerAddr);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1Temp);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "LOCALLABEL");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4LocalLabel);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "REMOTELABEL");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RemoteLabel);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "PW_MODE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4PwMode == 1)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "VPWS");
        }
        else if (i4PwMode == 2)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "VPLS");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "VFI_NAME");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4PwMode == 1)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        else if (i4PwMode == 2)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1VplsName);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    /* Display all the information in the html page after the Table stop flag */
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    MPLS_L2VPN_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
}

/******************************************************************************
*  Function Name : IssProcessMplsPwSet
*  Description   : This function gets the user input from the web page and 
*                  creates the pseudo wires.                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
******************************************************************************/
VOID
IssProcessMplsPwSet (tHttp * pHttp)
{
    tPwArgs             PwArgs;

    UINT4               u4PeerAddr = 0;
    UINT4               u4LocalLabel = 0;
    UINT4               u4RemoteLabel = 0;
    UINT4               u4MplsType = 0;
    UINT4               u4TunnelId = 0;
    UINT4               u4PwIndex = 0;
    UINT4               u4ErrorCode = 0;
    UINT1              *pu1VfiName = NULL;

    INT4                i4PwMode = 0;
    INT4                i4RetVal = WEB_SUCCESS;
    UINT1               au1VfiName[L2VPN_MAX_VPLS_NAME_LEN] = { 0 };

    MEMSET (au1VfiName, 0, sizeof (au1VfiName));
    pu1VfiName = &au1VfiName[0];

    STRCPY (pHttp->au1Name, "PW_PEER");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4PeerAddr = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));

    STRCPY (pHttp->au1Name, "LOCALLABEL");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4LocalLabel = (UINT4) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "REMOTELABEL");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4RemoteLabel = (UINT4) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "PW_MODE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "VPWS") == 0)
    {
        i4PwMode = 1;
    }
    else if (STRCMP (pHttp->au1Value, "VPLS") == 0)
    {
        i4PwMode = 2;
    }

    if (i4PwMode == 2)
    {
        STRCPY (pHttp->au1Name, "VFI_NAME");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        STRNCPY ((CHR1 *) pu1VfiName, pHttp->au1Value,
                 (sizeof (au1VfiName) - 1));
    }
    else
    {
        STRNCPY ((CHR1 *) pu1VfiName, "-", (sizeof (au1VfiName) - 1));
    }

    STRCPY (pHttp->au1Name, "PW_PSN");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "TE") == 0)
    {
        u4MplsType = L2VPN_MPLS_TYPE_TE;
    }
    else if (STRCMP (pHttp->au1Value, "VCONLY") == 0)
    {
        u4MplsType = L2VPN_MPLS_TYPE_VCONLY;
    }
    else if (STRCMP (pHttp->au1Value, "NONTE") == 0)
    {
        u4MplsType = L2VPN_MPLS_TYPE_NONTE;
    }

    if (u4MplsType == L2VPN_MPLS_TYPE_TE)
    {
        STRCPY (pHttp->au1Name, "TUNNEL_ID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4TunnelId = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "DELETE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Delete") != 0)
    {
        PwArgs.u4PeerAddr = u4PeerAddr;
        PwArgs.i4PwMode = i4PwMode;
        PwArgs.u4MplsType = u4MplsType;
        PwArgs.u4TunnelId = u4TunnelId;
        PwArgs.u4LocalLabel = u4LocalLabel;
        PwArgs.u4RemoteLabel = u4RemoteLabel;

        i4RetVal = IssMplsCreatePw (pHttp, &PwArgs, pu1VfiName);
    }
    else
    {
        STRCPY (pHttp->au1Name, "PW_INDEX");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4PwIndex = (UINT4) ATOI (pHttp->au1Value);

        MPLS_L2VPN_LOCK ();
        if (nmhTestv2PwRowStatus (&u4ErrorCode, u4PwIndex, ISS_DESTROY)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *)
                          "Failed in Destroying Pseudowire");
            MPLS_L2VPN_UNLOCK ();
            return;
        }

        if (nmhSetPwRowStatus (u4PwIndex, ISS_DESTROY) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *)
                          "Failed in Destroying Pseudowire");
            MPLS_L2VPN_UNLOCK ();
            return;
        }
        MPLS_L2VPN_UNLOCK ();
    }

    if (i4RetVal == WEB_SUCCESS)
    {
        IssProcessMplsPwGet (pHttp);
    }
}

/*******************************************************************************
*  Function Name : IssMplsCreatePw
*  Description   : This routine creates the pseudo wire with the arguments 
*                  passed.                     
*  Input(s)      : pHttp         - Pointer to the global HTTP data structure.
*                  pPwArgs       - Pointer the PwArgs structure which contains
*                                  the arguments required to create the 
*                                  pseudo wire.
*                  pu1VfiName    - Pointer to VFI Name
*  Output(s)     : None.
*  Return Values : WEB_SUCCESS or WEB_FAILURE
*******************************************************************************/

INT4
IssMplsCreatePw (tHttp * pHttp, tPwArgs * pPwArgs, UINT1 *pu1VfiName)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4PwIndex = 0;
    UINT4               u4VplsIndex = 0;

    INT4                i4AdminStatus = 1;

    tSNMP_OCTET_STRING_TYPE MplsType;
    static UINT1        au1MplsType[1];

    if (pPwArgs->i4PwMode == 2)
    {
        IssGetVplsIndexFromVplsName (pu1VfiName, &u4VplsIndex);

        if (u4VplsIndex == 0)
        {
            IssSendError (pHttp, (CONST INT1 *) "No VPLS Entry Found");
            return WEB_FAILURE;
        }
    }

    if (IssMplsPwEntryExist (pPwArgs, pu1VfiName, &u4VplsIndex) == WEB_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Entry Already Exists");
        return WEB_FAILURE;
    }

    MPLS_L2VPN_LOCK ();
    nmhGetPwIndexNext (&u4PwIndex);
    if (u4PwIndex == 0)
    {
        IssSendError (pHttp, (CONST INT1 *) "No free Index Available");
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }
    MPLS_L2VPN_UNLOCK ();

    if (IssMplsPwTablePopulate (pPwArgs, u4VplsIndex, u4PwIndex) == WEB_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "Failed in Populating Pseudo Wire Table");
        return WEB_FAILURE;
    }

    au1MplsType[0] = (UINT1) (pPwArgs->u4MplsType);
    MplsType.pu1_OctetList = au1MplsType;
    MplsType.i4_Length = (INT4) STRLEN (au1MplsType);
    MPLS_L2VPN_LOCK ();
    if (nmhTestv2PwMplsMplsType (&u4ErrorCode, u4PwIndex, &MplsType)
        == SNMP_FAILURE)
    {
        nmhSetPwRowStatus (u4PwIndex, ISS_DESTROY);
        MPLS_L2VPN_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *) "Failed in Setting Mpls type");
        return WEB_FAILURE;
    }

    if (nmhSetPwMplsMplsType (u4PwIndex, &MplsType) == SNMP_FAILURE)
    {
        nmhSetPwRowStatus (u4PwIndex, ISS_DESTROY);
        MPLS_L2VPN_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *) "Failed in Setting Mpls type");
        return WEB_FAILURE;
    }
    MPLS_L2VPN_UNLOCK ();

    if (IssMplsOutBoundTablePopulate (u4PwIndex, pPwArgs->u4TunnelId,
                                      pPwArgs->u4PeerAddr, pPwArgs->u4MplsType)
        == WEB_FAILURE)
    {
        MPLS_L2VPN_LOCK ();
        nmhSetPwRowStatus (u4PwIndex, ISS_DESTROY);
        MPLS_L2VPN_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *)
                      "Failed in Populating OutBound Table");
        return WEB_FAILURE;
    }

    MPLS_L2VPN_LOCK ();
    if (nmhTestv2PwAdminStatus (&u4ErrorCode, u4PwIndex, i4AdminStatus)
        == SNMP_FAILURE)
    {
        nmhSetPwRowStatus (u4PwIndex, ISS_DESTROY);
        MPLS_L2VPN_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *) "Failed in Making Admin Status up");
        return WEB_FAILURE;
    }

    if (nmhSetPwAdminStatus (u4PwIndex, i4AdminStatus) == SNMP_FAILURE)
    {
        nmhSetPwRowStatus (u4PwIndex, ISS_DESTROY);
        MPLS_L2VPN_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *) "Failed in Making Admin Status up");
        return WEB_FAILURE;
    }
    MPLS_L2VPN_UNLOCK ();
    return WEB_SUCCESS;
}

/*******************************************************************************
*  Function Name : IssMplsPwEntryExist
*  Description   : This routine whether the pseudo wire with the arguments 
*                  already exists.                     
*  Input(s)      : pPwArgs       - Pointer the PwArgs structure which contains
*                                  the arguments required to check the existence 
*                                  of pseudo wire.
*                  pu1VfiName    - Pointer to VFI Name
*  Output(s)     : pu4VplsIndex  - Pointer to VPLS Index
*  Return Values : WEB_FAILURE or WEB_SUCCESS
*******************************************************************************/
INT1
IssMplsPwEntryExist (tPwArgs * pPwArgs, UINT1 *pu1VfiName, UINT4 *pu4VplsIndex)
{
    UINT4               u4PwIndex = 0;
    UINT4               u4GetVplsIndex = 0;
    UINT4               u4PeerAddr = 0;
    INT4                i4GetPwMode = 0;
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    tSNMP_OCTET_STRING_TYPE VplsName;

    static UINT1        au1PeerAddr[IPVX_IPV6_ADDR_LEN] = { 0 };
    static UINT1        au1VplsName[L2VPN_MAX_VPLS_NAME_LEN + 1] = { 0 };

    PeerAddr.pu1_OctetList = au1PeerAddr;
    PeerAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
    VplsName.pu1_OctetList = au1VplsName;
    VplsName.i4_Length = L2VPN_MAX_VPLS_NAME_LEN;

    MPLS_L2VPN_LOCK ();
    /* Scan the Pw Table to check existence of PW */
    if (nmhGetFirstIndexPwTable (&u4PwIndex) == SNMP_FAILURE)
    {
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }
    MPLS_L2VPN_UNLOCK ();
    do
    {
        u4GetVplsIndex = 0;
        u4PeerAddr = 0;
        /* For VPLS case, Check if a pwEntry already exists for the same 
         * label range */
        MPLS_L2VPN_LOCK ();
        if (pPwArgs->i4PwMode == 2)
        {
            if (nmhGetFsMplsL2VpnVplsIndex (u4PwIndex, &u4GetVplsIndex)
                == SNMP_FAILURE)
            {
                MPLS_L2VPN_UNLOCK ();
                continue;
            }

            if (u4GetVplsIndex == 0)
            {
                MPLS_L2VPN_UNLOCK ();
                return WEB_FAILURE;
            }
            else
            {
                /* Now, Some VPLS Entry with a VFI Name exists,
                 * Get VPLS Name for the VPLS Index.
                 * If Given VPLS Name and Obtained VPLS Name matches,
                 * Get the PeerAddr to see the match, 
                 * If match found return FAILURE */
                MEMSET (VplsName.pu1_OctetList, 0, L2VPN_MAX_VPLS_NAME_LEN);
                if (nmhGetFsMplsVplsName (u4GetVplsIndex, &VplsName)
                    == SNMP_FAILURE)
                {
                    MPLS_L2VPN_UNLOCK ();
                    continue;
                }

                if (STRCMP (pu1VfiName, VplsName.pu1_OctetList) != 0)
                {
                    MPLS_L2VPN_UNLOCK ();
                    continue;
                }

                if (nmhGetPwPeerAddr (u4PwIndex, &PeerAddr) == SNMP_FAILURE)
                {
                    MPLS_L2VPN_UNLOCK ();
                    continue;
                }
                WEB_OCTETSTRING_TO_INTEGER ((&PeerAddr), u4PeerAddr);

                if (nmhGetFsMplsL2VpnPwMode (u4PwIndex, &i4GetPwMode)
                    == SNMP_FAILURE)
                {
                    MPLS_L2VPN_UNLOCK ();
                    continue;
                }

                if ((u4PeerAddr == pPwArgs->u4PeerAddr) &&
                    (pPwArgs->i4PwMode == i4GetPwMode))
                {
                    *pu4VplsIndex = u4GetVplsIndex;
                    MPLS_L2VPN_UNLOCK ();
                    return WEB_SUCCESS;
                }
            }
        }
        else if (pPwArgs->i4PwMode == 1)
        {
            /* VPWS Pw Entry Exist Not Handled */
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        MPLS_L2VPN_UNLOCK ();
    }
    while (nmhGetNextIndexPwTable (u4PwIndex, &u4PwIndex) == SNMP_SUCCESS);

    MPLS_L2VPN_UNLOCK ();
    return WEB_FAILURE;
}

/*******************************************************************************
*  Function Name : IssMplsPwTablePopulate
*  Description   : This routine populates the pseudo wire with the arguments 
*                  passed.                     
*  Input(s)      : pPwArgs       - Pointer the PwArgs structure which contains
*                                  the arguments required to populate the 
*                                  pseudo wire.
*                  u4VplsIndex   - VPLS Index to be used for VPLS case.
*                  u4PwIndex     - Pw Index
*  Output(s)     : None.
*  Return Values : WEB_FAILURE or WEB_SUCCESS
*******************************************************************************/
INT1
IssMplsPwTablePopulate (tPwArgs * pPwArgs, UINT4 u4VplsIndex, UINT4 u4PwIndex)
{
    tSNMP_OCTET_STRING_TYPE PwPeerAddress;
    UINT1               au1PeerAddr[MPLS_INDEX_LENGTH];
    UINT4               u4ErrorCode = 0;

    INT4                i4PwOwner = 1;    /* Manual Case */
    INT4                i4PsnType = 1;    /* Mpls Type */

    PwPeerAddress.pu1_OctetList = au1PeerAddr;
    PwPeerAddress.i4_Length = MPLS_INDEX_LENGTH;

    WEB_INTEGER_TO_OCTETSTRING (pPwArgs->u4PeerAddr, (&PwPeerAddress));

    MPLS_L2VPN_LOCK ();
    /* Set Pw Row Status */
    if (nmhTestv2PwRowStatus (&u4ErrorCode, u4PwIndex, ISS_CREATE_AND_WAIT)
        == SNMP_FAILURE)
    {
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }

    if (nmhSetPwRowStatus (u4PwIndex, ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }
    MPLS_L2VPN_UNLOCK ();

    MPLS_L2VPN_LOCK ();
    /* Set Pw Owner */
    if (nmhTestv2PwOwner (&u4ErrorCode, u4PwIndex, i4PwOwner) == SNMP_FAILURE)
    {
        nmhSetPwRowStatus (u4PwIndex, ISS_DESTROY);
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }

    if (nmhSetPwOwner (u4PwIndex, i4PwOwner) == SNMP_FAILURE)
    {
        nmhSetPwRowStatus (u4PwIndex, ISS_DESTROY);
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }
    MPLS_L2VPN_UNLOCK ();

    MPLS_L2VPN_LOCK ();
    /* Set Pw Psn Type */
    if (nmhTestv2PwPsnType (&u4ErrorCode, u4PwIndex, i4PsnType) == SNMP_FAILURE)
    {
        nmhSetPwRowStatus (u4PwIndex, ISS_DESTROY);
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }

    if (nmhSetPwPsnType (u4PwIndex, i4PsnType) == SNMP_FAILURE)
    {
        nmhSetPwRowStatus (u4PwIndex, ISS_DESTROY);
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }
    MPLS_L2VPN_UNLOCK ();

    MPLS_L2VPN_LOCK ();
    /* Set Pw Peer Addr */
    if (nmhTestv2PwPeerAddr (&u4ErrorCode, u4PwIndex, &PwPeerAddress)
        == SNMP_FAILURE)
    {
        nmhSetPwRowStatus (u4PwIndex, ISS_DESTROY);
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }

    if (nmhSetPwPeerAddr (u4PwIndex, &PwPeerAddress) == SNMP_FAILURE)
    {
        nmhSetPwRowStatus (u4PwIndex, ISS_DESTROY);
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }
    MPLS_L2VPN_UNLOCK ();

    /* For PwOwner ====> MANUAL set the Inbound and OutBound Label */
    MPLS_L2VPN_LOCK ();
    /* Set Pw In Bound Label */
    if (nmhTestv2PwInboundLabel (&u4ErrorCode, u4PwIndex,
                                 pPwArgs->u4LocalLabel) == SNMP_FAILURE)
    {
        nmhSetPwRowStatus (u4PwIndex, ISS_DESTROY);
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }
    if (nmhSetPwInboundLabel (u4PwIndex, pPwArgs->u4LocalLabel) == SNMP_FAILURE)
    {
        nmhSetPwRowStatus (u4PwIndex, ISS_DESTROY);
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }

    /* Set Pw Out Bound Label */
    if (nmhTestv2PwOutboundLabel (&u4ErrorCode, u4PwIndex,
                                  pPwArgs->u4RemoteLabel) == SNMP_FAILURE)
    {
        nmhSetPwRowStatus (u4PwIndex, ISS_DESTROY);
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }

    if (nmhSetPwOutboundLabel (u4PwIndex, pPwArgs->u4RemoteLabel)
        == SNMP_FAILURE)
    {
        nmhSetPwRowStatus (u4PwIndex, ISS_DESTROY);
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }
    MPLS_L2VPN_UNLOCK ();

    MPLS_L2VPN_LOCK ();
    /* Set PwMode */
    if (nmhTestv2FsMplsL2VpnPwMode (&u4ErrorCode, u4PwIndex, pPwArgs->i4PwMode)
        == SNMP_FAILURE)
    {
        nmhSetPwRowStatus (u4PwIndex, ISS_DESTROY);
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }
    if (nmhSetFsMplsL2VpnPwMode (u4PwIndex, pPwArgs->i4PwMode) == SNMP_FAILURE)
    {
        nmhSetPwRowStatus (u4PwIndex, ISS_DESTROY);
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }
    MPLS_L2VPN_UNLOCK ();

    if (pPwArgs->i4PwMode == 2)
    {
        MPLS_L2VPN_LOCK ();
        if (nmhTestv2FsMplsL2VpnVplsIndex (&u4ErrorCode, u4PwIndex,
                                           (INT4) u4VplsIndex) == SNMP_FAILURE)
        {
            nmhSetPwRowStatus (u4PwIndex, ISS_DESTROY);
            MPLS_L2VPN_UNLOCK ();
            return WEB_FAILURE;
        }

        if (nmhSetFsMplsL2VpnVplsIndex (u4PwIndex, u4VplsIndex) == SNMP_FAILURE)
        {
            nmhSetPwRowStatus (u4PwIndex, ISS_DESTROY);
            MPLS_L2VPN_UNLOCK ();
            return WEB_FAILURE;
        }
        MPLS_L2VPN_UNLOCK ();
    }

    MPLS_L2VPN_LOCK ();
    /* Set Pw Row Status  to ACTIVE */
    if (nmhTestv2PwRowStatus (&u4ErrorCode, u4PwIndex, ISS_ACTIVE)
        == SNMP_FAILURE)
    {
        nmhSetPwRowStatus (u4PwIndex, ISS_DESTROY);
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }

    if (nmhSetPwRowStatus (u4PwIndex, ISS_ACTIVE) == SNMP_FAILURE)
    {
        nmhSetPwRowStatus (u4PwIndex, ISS_DESTROY);
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }
    MPLS_L2VPN_UNLOCK ();
    return WEB_SUCCESS;
}

/*******************************************************************************
*  Function Name : IssMplsOutBoundTablePopulate
*  Description   : This routine populates the PSN OutBoundTable with the 
*                  arguments passed.                     
*  Input(s)      : u4PwIndex         - Pw Index
*                  u4TunnelId        - Tunnel Index
*                  u4PeerAddr        - Pseudo wire Peer
*                  u4MplsType        - Mpls Type (te , non-te or vconly)
*  Output(s)     : None.
*  Return Values : WEB_FAILURE or WEB_SUCCESS
*******************************************************************************/
INT1
IssMplsOutBoundTablePopulate (UINT4 u4PwIndex, UINT4 u4TunnelId,
                              UINT4 u4PeerAddr, UINT4 u4MplsType)
{
    tSNMP_OCTET_STRING_TYPE PwMplsOutboundTunnelLclLSRId;
    tSNMP_OCTET_STRING_TYPE PwMplsOutboundTunnelPeerLSRId;
    tSNMP_OCTET_STRING_TYPE OutBoundLsrXcIndex;
    tSNMP_OCTET_STRING_TYPE IngressId;
    tSNMP_OCTET_STRING_TYPE EgressId;

    static UINT1        au1MplsOutboundTunnelLclLSR[MPLS_INDEX_LENGTH] = { 0 };
    static UINT1        au1MplsOutboundTunnelPeerLSR[MPLS_INDEX_LENGTH] = { 0 };
    static UINT1        au1OutBoundLsrXcIndex[24];
    static UINT1        au1IngressId[MPLS_INDEX_LENGTH] = { 0 };
    static UINT1        au1EgressId[MPLS_INDEX_LENGTH] = { 0 };

    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;

    UINT4               u4PwMplsOutboundTunnelIndex = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               i4IfIndex = 0;
    UINT4               u4XcIndex = 0;
    UINT4               u4SrcAddr = 0;
    UINT4               u4TunnelInstance = 1;
    UINT4               u4Mask = 0xffffffff;

    INT4                i4RowStatus = 0;

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

    PwMplsOutboundTunnelLclLSRId.pu1_OctetList = au1MplsOutboundTunnelLclLSR;
    PwMplsOutboundTunnelLclLSRId.i4_Length = 0;

    PwMplsOutboundTunnelPeerLSRId.pu1_OctetList = au1MplsOutboundTunnelPeerLSR;
    PwMplsOutboundTunnelPeerLSRId.i4_Length = 0;

    OutBoundLsrXcIndex.pu1_OctetList = au1OutBoundLsrXcIndex;
    OutBoundLsrXcIndex.i4_Length = 0;

    IngressId.pu1_OctetList = au1IngressId;
    IngressId.i4_Length = MPLS_INDEX_LENGTH;
    EgressId.pu1_OctetList = au1EgressId;
    IngressId.i4_Length = MPLS_INDEX_LENGTH;

    /* If PwMpls Type = TE , set OutboundTunnelIndex, LocalLSR and PeerLSR */
    if (u4MplsType == 1)
    {
        if (NetIpv4GetSrcAddrToUseForDest (u4PeerAddr, &u4SrcAddr)
            == NETIPV4_FAILURE)
        {
            return WEB_FAILURE;
        }

        if (nmhGetMplsTunnelRowStatus (u4TunnelId, u4TunnelInstance,
                                       u4SrcAddr, u4PeerAddr, &i4RowStatus)
            == SNMP_FAILURE)
        {
            return WEB_FAILURE;
        }

        WEB_INTEGER_TO_OCTETSTRING (u4SrcAddr, (&IngressId));
        WEB_INTEGER_TO_OCTETSTRING (u4PeerAddr, (&EgressId));
        u4PwMplsOutboundTunnelIndex = u4TunnelId;
        PwMplsOutboundTunnelLclLSRId.pu1_OctetList = au1IngressId;
        PwMplsOutboundTunnelLclLSRId.i4_Length = MPLS_INDEX_LENGTH;
        PwMplsOutboundTunnelPeerLSRId.pu1_OctetList = au1EgressId;
        PwMplsOutboundTunnelPeerLSRId.i4_Length = MPLS_INDEX_LENGTH;

        /* Set Pw Mpls Out Bound Tunnel Index */
        MPLS_L2VPN_LOCK ();
        if (nmhTestv2PwMplsOutboundTunnelIndex
            (&u4ErrorCode, u4PwIndex,
             (INT4) u4PwMplsOutboundTunnelIndex) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            return WEB_FAILURE;
        }
        if (nmhSetPwMplsOutboundTunnelIndex
            (u4PwIndex, u4PwMplsOutboundTunnelIndex) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            return WEB_FAILURE;
        }

        /* Set Pw Mpls Out Bound Tunnel Lcl LSR */
        if (nmhTestv2PwMplsOutboundTunnelLclLSR
            (&u4ErrorCode, u4PwIndex,
             &PwMplsOutboundTunnelLclLSRId) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            return WEB_FAILURE;
        }
        if (nmhSetPwMplsOutboundTunnelLclLSR
            (u4PwIndex, &PwMplsOutboundTunnelLclLSRId) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            return WEB_FAILURE;
        }

        /* Set Pw Mpls Out Bound Tunnel Peer LSR */
        if (nmhTestv2PwMplsOutboundTunnelPeerLSR
            (&u4ErrorCode, u4PwIndex,
             &PwMplsOutboundTunnelPeerLSRId) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            return WEB_FAILURE;
        }
        if (nmhSetPwMplsOutboundTunnelPeerLSR
            (u4PwIndex, &PwMplsOutboundTunnelPeerLSRId) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            return WEB_FAILURE;
        }
        MPLS_L2VPN_UNLOCK ();
    }
    else if (u4MplsType == 2)
    {
        /* If pwMplsType is of type Non-Te, check whether any FTN is associated 
         * with this, if not return failure*/
        if ((u4XcIndex = MplsGetXCIndexFromPrefix (u4PeerAddr)) != 0)
        {
            WEB_INTEGER_TO_OCTETSTRING (u4XcIndex, (&OutBoundLsrXcIndex));
            OutBoundLsrXcIndex.i4_Length = 24;

            MPLS_L2VPN_LOCK ();
            if (nmhTestv2PwMplsOutboundLsrXcIndex (&u4ErrorCode, u4PwIndex,
                                                   &OutBoundLsrXcIndex)
                == SNMP_FAILURE)
            {
                MPLS_L2VPN_UNLOCK ();
                return WEB_FAILURE;
            }

            if (nmhSetPwMplsOutboundLsrXcIndex (u4PwIndex,
                                                &OutBoundLsrXcIndex)
                == SNMP_FAILURE)
            {
                MPLS_L2VPN_UNLOCK ();
                return WEB_FAILURE;
            }
            MPLS_L2VPN_UNLOCK ();
        }
        else
        {
            return WEB_FAILURE;
        }
    }
    else if (u4MplsType == 4)
    {
        RtQuery.u4DestinationIpAddress = u4PeerAddr;
        RtQuery.u4DestinationSubnetMask = u4Mask;
        RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

        /* If pwMplstype is of type VConly , set Outbound If Index */
        if (u4PeerAddr == 0)
        {
            return WEB_FAILURE;
        }

        if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) != NETIPV4_SUCCESS)
        {
            return WEB_FAILURE;
        }
        if (NetIpRtInfo.u2RtProto != CIDR_LOCAL_ID)
        {
            return WEB_FAILURE;
        }
        if (NetIpv4GetCfaIfIndexFromPort (NetIpRtInfo.u4RtIfIndx,
                                          &i4IfIndex) == NETIPV4_FAILURE)
        {
            return WEB_FAILURE;
        }
        MPLS_L2VPN_LOCK ();
        if (nmhTestv2PwMplsOutboundIfIndex
            (&u4ErrorCode, u4PwIndex, i4IfIndex) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            return WEB_FAILURE;
        }
        if (nmhSetPwMplsOutboundIfIndex (u4PwIndex, i4IfIndex) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            return WEB_FAILURE;
        }
        MPLS_L2VPN_UNLOCK ();
    }
    return WEB_SUCCESS;
}

/*******************************************************************************
*  Function Name : IssGetVplsIndexFromVplsName
*  Description   : This function gets the VPLS Index from VPLS Name Passed
*                  Enet Page.                     
*  Input(s)      : pu1VfiName    - Pointer to VFI Name
*  Output(s)     : pu4VplsIndex  - Pointer to VPLS Index
*  Return Values : None
*******************************************************************************/
VOID
IssGetVplsIndexFromVplsName (UINT1 *pu1VfiName, UINT4 *pu4VplsIndex)
{
    UINT4               u4VplsIndexPrev = 0;
    UINT4               u4VplsIndex = 0;

    tSNMP_OCTET_STRING_TYPE VfiName;
    static UINT1        au1VfiName[L2VPN_MAX_VPLS_NAME_LEN + 1] = { 0 };

    BOOL1               bEntryFound = FALSE;

    VfiName.pu1_OctetList = au1VfiName;
    VfiName.i4_Length = L2VPN_MAX_VPLS_NAME_LEN;

    *pu4VplsIndex = 0;

    MPLS_L2VPN_LOCK ();
    if (nmhGetFirstIndexFsMplsVplsConfigTable (&u4VplsIndex) == SNMP_FAILURE)
    {
        MPLS_L2VPN_UNLOCK ();
        return;
    }
    MPLS_L2VPN_UNLOCK ();
    while (1)
    {
        MPLS_L2VPN_LOCK ();
        u4VplsIndexPrev = u4VplsIndex;
        MEMSET (VfiName.pu1_OctetList, 0, L2VPN_MAX_VPLS_NAME_LEN);
        if (nmhGetFsMplsVplsName (u4VplsIndex, &VfiName) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }

        if (STRNCMP (VfiName.pu1_OctetList, pu1VfiName,
                     L2VPN_MAX_VPLS_NAME_LEN) == 0)
        {
            bEntryFound = TRUE;
            break;
        }

        if ((bEntryFound != TRUE) &&
            (nmhGetNextIndexFsMplsVplsConfigTable (u4VplsIndexPrev,
                                                   &u4VplsIndex)
             == SNMP_FAILURE))
        {
            break;
        }
        MPLS_L2VPN_UNLOCK ();
    }
    MPLS_L2VPN_UNLOCK ();

    if (bEntryFound == TRUE)
    {
        *pu4VplsIndex = u4VplsIndex;
        return;
    }
    else
    {
        *pu4VplsIndex = 0;
        return;
    }
}

/*******************************************************************************
*  Function Name : IssProcessMplsPwEnetPage 
*  Description   : This function processes the request coming for the Mpls Pw
*                  Enet Page.                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssProcessMplsPwEnetPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessMplsPwEnetGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessMplsPwEnetSet (pHttp);
    }
}

/*******************************************************************************
*  Function Name : IssProcessMplsPwEnetGet
*  Description   : This routines gets and displays all the pseudo wire enet
*                  entries created and enables the user to create pseudo wire
*                  enet.                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssProcessMplsPwEnetGet (tHttp * pHttp)
{
    UINT4               u4PwIndex = 0;
    UINT4               u4PwIndexPrev = 0;
    UINT4               u4PwInstance = 0;
    UINT4               u4PwInstancePrev = 0;
    UINT4               u4PortVlan = 0;
    UINT4               u4VplsIndex = 0;
    UINT4               u4Temp = 0;
    INT4                i4PortIfIndex = 0;
    INT4                i4PwType = 0;
    INT4                i4PwMode = 0;

    tSNMP_OCTET_STRING_TYPE VplsName;

    static UINT1        au1VplsName[L2VPN_MAX_VPLS_NAME_LEN + 1] = { 0 };

    BOOL1               bLoop = FALSE;

    VplsName.pu1_OctetList = au1VplsName;
    VplsName.i4_Length = L2VPN_MAX_VPLS_NAME_LEN;

    pHttp->i4Write = 0;

    IssPrintAvailableVlans (pHttp);

    IssPrintAvailableL2Ports (pHttp);

#ifdef MI_WANTED
    IssPrintAvailableVfi (pHttp);
#endif

    IssPrintAvailablePw (pHttp);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    u4Temp = (UINT4) pHttp->i4Write;

    WebnmRegisterLock (pHttp, L2vpnLock, L2vpnUnLock);
    while (1)
    {
        MPLS_L2VPN_LOCK ();
        if ((bLoop == FALSE) &&
            (nmhGetFirstIndexPwEnetTable (&u4PwIndex, &u4PwInstance)
             == SNMP_FAILURE))
        {
            MPLS_L2VPN_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            return;
        }

        if ((bLoop == TRUE) &&
            (nmhGetNextIndexPwEnetTable (u4PwIndexPrev, &u4PwIndex,
                                         u4PwInstancePrev, &u4PwInstance)
             == SNMP_FAILURE))
        {
            break;
        }

        bLoop = TRUE;
        u4PwIndexPrev = u4PwIndex;
        u4PwInstancePrev = u4PwInstance;

        if (nmhGetPwType (u4PwIndex, &i4PwType) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }

        if (nmhGetFsMplsL2VpnPwMode (u4PwIndex, &i4PwMode) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }

        if (nmhGetFsMplsL2VpnVplsIndex (u4PwIndex, &u4VplsIndex)
            == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }

        if (u4VplsIndex != 0 && i4PwMode == 2)
        {
            MEMSET (VplsName.pu1_OctetList, 0, L2VPN_MAX_VPLS_NAME_LEN);
            if (nmhGetFsMplsVplsName (u4VplsIndex, &VplsName) == SNMP_FAILURE)
            {
                MPLS_L2VPN_UNLOCK ();
                continue;
            }
        }

        if (i4PwType == 4)
        {
            if (nmhGetPwEnetPortVlan (u4PwIndex, u4PwInstance, &u4PortVlan)
                == SNMP_FAILURE)
            {
                MPLS_L2VPN_UNLOCK ();
                continue;
            }
        }
        else
        {
            if (nmhGetPwEnetPortIfIndex (u4PwIndex, u4PwInstance,
                                         &i4PortIfIndex) == SNMP_FAILURE)
            {
                MPLS_L2VPN_UNLOCK ();
                continue;
            }
        }
        pHttp->i4Write = (INT4) u4Temp;
        MPLS_L2VPN_UNLOCK ();

        STRCPY (pHttp->au1KeyString, "AC_TYPE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4PwType == 4)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "VLAN");
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "PORT");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "VLAN_ID");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4PwType == 4)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4PortVlan);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "PORT_NO");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4PwType == 4)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4PortIfIndex);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "PW_MODE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4PwMode == 1)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "VPWS");
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "VPLS");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "VFI_NAME");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4PwMode == 1)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1VplsName);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    /* Display all the information in the html page after the Table stop flag */
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    MPLS_L2VPN_UNLOCK ();
    WebnmUnRegisterLock (pHttp);

}

/******************************************************************************
*  Function Name : IssProcessMplsPwEnetSet
*  Description   : This function gets the user input from the web page and 
*                  creates the pseudo wire enet.                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
******************************************************************************/
VOID
IssProcessMplsPwEnetSet (tHttp * pHttp)
{
    INT4                i4PwType = 0;
    INT4                i4PwMode = 0;
    INT4                i4PortIfIndex = 0;
    INT4                i4RetVal = WEB_SUCCESS;

    UINT4               u4PortVlan = 0;
    UINT4               u4PwIndex = 0;

    UINT1              *pu1VfiName = NULL;
    UINT1               au1VfiName[L2VPN_MAX_VPLS_NAME_LEN] = { 0 };

    tPwEnetArgs         PwEnetArgs;
    MEMSET (au1VfiName, 0, sizeof (au1VfiName));
    MEMSET (pHttp->au1Name, 0, sizeof (pHttp->au1Name));

    pu1VfiName = &au1VfiName[0];

    STRNCPY (pHttp->au1Name, "AC_TYPE", (sizeof (pHttp->au1Name) - 1));
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "VLAN") == 0)
    {
        i4PwType = 4;
    }
    else if (STRCMP (pHttp->au1Value, "PORT") == 0)
    {
        i4PwType = 5;
    }

    MEMSET (pHttp->au1Name, 0, sizeof (pHttp->au1Name));
    if (i4PwType == 4)
    {
        STRNCPY (pHttp->au1Name, "VLAN_ID", (sizeof (pHttp->au1Name) - 1));
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4PortVlan = (UINT4) ATOI (pHttp->au1Value);
    }
    else
    {
        STRNCPY (pHttp->au1Name, "PORT_NO", (sizeof (pHttp->au1Name) - 1));
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4PortIfIndex = ATOI (pHttp->au1Value);
        if (i4PortIfIndex == 0)
        {
            IssSendError (pHttp, (CONST INT1 *) "Port no. can't be zero");
            return;
        }
    }
    MEMSET (pHttp->au1Name, 0, sizeof (pHttp->au1Name));
    STRNCPY (pHttp->au1Name, "PW_MODE", (sizeof (pHttp->au1Name) - 1));
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Value, "VPWS") == 0)
    {
        i4PwMode = 1;
    }
    else if (STRCMP (pHttp->au1Value, "VPLS") == 0)
    {
        i4PwMode = 2;
    }
    MEMSET (pHttp->au1Name, 0, sizeof (pHttp->au1Name));
    MEMSET (au1VfiName, 0, sizeof (au1VfiName));
    if (i4PwMode == 2)
    {
        STRNCPY (pHttp->au1Name, "VFI_NAME", (sizeof (pHttp->au1Name) - 1));
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        STRNCPY ((CHR1 *) pu1VfiName, pHttp->au1Value,
                 (sizeof (au1VfiName) - 1));
    }
    else
    {
        STRNCPY ((CHR1 *) pu1VfiName, "-", (sizeof (au1VfiName) - 1));
        STRNCPY (pHttp->au1Name, "PW_INDEX", (sizeof (pHttp->au1Name) - 1));
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4PwIndex = (UINT4) ATOI (pHttp->au1Value);
    }
    MEMSET (pHttp->au1Name, 0, sizeof (pHttp->au1Name));
    STRNCPY (pHttp->au1Name, "DELETE", ENM_MAX_NAME_LEN);
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Value, "Delete") != 0)
    {
        if (u4PortVlan == 0)
        {
            PwEnetArgs.u4PortVlan = L2VPN_PWVC_ENET_DEF_PORT_VLAN;
        }
        else
        {
            PwEnetArgs.u4PortVlan = u4PortVlan;
        }
        PwEnetArgs.i4PortIfIndex = i4PortIfIndex;
        PwEnetArgs.i4PwMode = i4PwMode;
        PwEnetArgs.i4PwType = i4PwType;

        i4RetVal = IssMplsCreateEnetEntry (pHttp, &PwEnetArgs, u4PwIndex,
                                           pu1VfiName);
    }
    else
    {
        i4RetVal = IssMplsDeleteEnetEntry (pHttp, i4PwType, i4PwMode,
                                           u4PortVlan, i4PortIfIndex);
    }

    if (i4RetVal == WEB_SUCCESS)
    {
        IssProcessMplsPwEnetGet (pHttp);
    }
}

/******************************************************************************
*  Function Name : IssMplsCreateEnetEntry
*  Description   : This function Creates the ENET Entry corresponding to the 
*                  arguments passed.                     
*  Input(s)      : pHttp          - Pointer to the global HTTP data structure.
*                  pPwEnetArgs    - Pointer to the structure that carries the
*                                   enet information to be populated.
*                  u4PwIndex      - PwIndex
*                  pu1VfiName     - Pointer to VFI Name
*  Output(s)     : None.
*  Return Values : WEB_SUCCESS or WEB_FAILURE
******************************************************************************/

INT4
IssMplsCreateEnetEntry (tHttp * pHttp, tPwEnetArgs * pPwEnetArgs,
                        UINT4 u4PwIndex, UINT1 *pu1VfiName)
{

    tSNMP_OCTET_STRING_TYPE MplsType;

    UINT4               u4VplsIndex = 0;
    UINT4               u4PwIndexPrev = 0;
    UINT4               u4PwEnetInst = 0;
    UINT4               u4PwEnetInstPrev = 0;
    UINT4               u4GetVplsIndex = 0;
    UINT4               u4StartInstance = 0;
    UINT4               u4EnetInstance = 1;
    UINT4               u4ErrorCode = 0;
    static UINT1        au1MplsType[1] = { 0 };

    BOOL1               bLoop = FALSE;
    BOOL1               bFlag = FALSE;
    BOOL1               bFlag1 = FALSE;

    MplsType.pu1_OctetList = au1MplsType;
    MplsType.i4_Length = 1;

    if (pPwEnetArgs->i4PwMode == 1)
    {
        if (IssMplsEnetTablePopulate (pPwEnetArgs, u4PwIndex, u4EnetInstance)
            == WEB_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *)
                          "Failed in Creating Enet Table");
            return WEB_FAILURE;
        }
    }
    else
    {
        /* Get Vpls Index from the VPLS VFI Name */
        IssGetVplsIndexFromVplsName (pu1VfiName, &u4VplsIndex);
        if (u4VplsIndex == 0)
        {
            IssSendError (pHttp, (CONST INT1 *)
                          "No VPLS Entry Found or Invalid VPLS Name");
            return WEB_FAILURE;
        }

        /* For every Pseudo wire, get the VPLS Index associated with it. 
         * If Obtained VPLS Index and Passed VPLS Index matches,
         * check whether PSN Type exists, if Yes create the enet entry */
        while (1)
        {
            MPLS_L2VPN_LOCK ();
            if ((bLoop == FALSE) &&
                (nmhGetFirstIndexPwTable (&u4PwIndex) == SNMP_FAILURE))
            {
                MPLS_L2VPN_UNLOCK ();
                IssSendError (pHttp, (CONST INT1 *) "No Pw Entry Exists");
                return WEB_FAILURE;
            }

            if ((bLoop == TRUE) &&
                (nmhGetNextIndexPwTable (u4PwIndexPrev, &u4PwIndex)
                 == SNMP_FAILURE))
            {
                MPLS_L2VPN_UNLOCK ();
                break;
            }

            bLoop = TRUE;
            u4PwIndexPrev = u4PwIndex;

            if (nmhGetFsMplsL2VpnVplsIndex (u4PwIndex, &u4GetVplsIndex)
                == SNMP_FAILURE)
            {
                MPLS_L2VPN_UNLOCK ();
                break;
            }

            if (u4GetVplsIndex != u4VplsIndex)
            {
                MPLS_L2VPN_UNLOCK ();
                continue;
            }
            if (nmhGetPwMplsMplsType (u4PwIndex, &MplsType) == SNMP_FAILURE)
            {
                MPLS_L2VPN_UNLOCK ();
                break;
            }

            if (bFlag1 == FALSE)
            {
                bFlag1 = TRUE;
                MPLS_L2VPN_UNLOCK ();
                u4EnetInstance = IssGetLastEnetInstance (u4PwIndex);
                MPLS_L2VPN_LOCK ();
                u4StartInstance = u4EnetInstance = u4EnetInstance + 1;
            }

            MPLS_L2VPN_UNLOCK ();
            if (IssMplsEnetTablePopulate (pPwEnetArgs, u4PwIndex,
                                          u4EnetInstance) == WEB_FAILURE)
            {
                bFlag = TRUE;
                break;
            }
            u4EnetInstance++;
        }

        /* Proceed to delete the ENET Entry if any problem occurred during
         * creation of ENET Entry.
         *  Do this only after the successfull creation of FIRST ENET Entry*/
        if (bFlag == TRUE)
        {
            u4EnetInstance = u4StartInstance;
            bLoop = FALSE;

            while (1)
            {
                MPLS_L2VPN_LOCK ();
                if ((bLoop == FALSE) &&
                    (nmhGetFirstIndexPwEnetTable (&u4PwIndex, &u4PwEnetInst)
                     == SNMP_FAILURE))
                {
                    MPLS_L2VPN_UNLOCK ();
                    break;
                }

                if ((bLoop == TRUE) &&
                    (nmhGetNextIndexPwEnetTable (u4PwIndexPrev, &u4PwIndex,
                                                 u4PwEnetInstPrev,
                                                 &u4PwEnetInst) ==
                     SNMP_FAILURE))
                {
                    MPLS_L2VPN_UNLOCK ();
                    break;
                }

                bLoop = TRUE;
                u4PwIndexPrev = u4PwIndex;
                u4PwEnetInstPrev = u4PwEnetInst;
                if (nmhGetFsMplsL2VpnVplsIndex (u4PwIndex, &u4GetVplsIndex)
                    == SNMP_FAILURE)
                {
                    MPLS_L2VPN_UNLOCK ();
                    continue;
                }

                if (u4GetVplsIndex != u4VplsIndex)
                {
                    MPLS_L2VPN_UNLOCK ();
                    continue;
                }

                if (u4EnetInstance == u4PwEnetInst)
                {
                    if (nmhTestv2PwEnetRowStatus (&u4ErrorCode,
                                                  u4PwIndex, u4PwEnetInst,
                                                  ISS_DESTROY) == SNMP_SUCCESS)
                    {
                        nmhSetPwEnetRowStatus (u4PwIndex, u4PwEnetInst,
                                               ISS_DESTROY);
                    }

                    u4EnetInstance++;
                }
                MPLS_L2VPN_UNLOCK ();
            }
            IssSendError (pHttp, (CONST INT1 *)
                          "Failed in Creating Pw Enet Table");
            return WEB_FAILURE;
        }
    }
    return WEB_SUCCESS;
}

/******************************************************************************
*  Function Name : IssMplsEnetTablePopulate
*  Description   : This function Populates the ENET Table.                     
*  Input(s)      : pPwEnetArgs    - Pointer to the structure that carries the
*                                   enet information to be populated.
*                  u4PwIndex      - Pw Inde u4EnetInstance - Enet Instance
*  Output(s)     : None.
*  Return Values : None
******************************************************************************/
INT1
IssMplsEnetTablePopulate (tPwEnetArgs * pPwEnetArgs, UINT4 u4PwIndex,
                          UINT4 u4PwEnetPwInstance)
{
    UINT4               u4ErrorCode = 0;

    MPLS_L2VPN_LOCK ();

    /* Set Pw Enet Pw Row Status to create and wait */
    if (nmhTestv2PwEnetRowStatus (&u4ErrorCode, u4PwIndex, u4PwEnetPwInstance,
                                  ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }

    /* Setting the Pseudo wire to NOT_IN_SERVICE State */
    if (nmhTestv2PwRowStatus (&u4ErrorCode, u4PwIndex, ISS_NOT_IN_SERVICE)
        == SNMP_FAILURE)
    {
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }
    if (nmhSetPwRowStatus (u4PwIndex, ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }

    /* Set Pw Type */
    if (nmhTestv2PwType (&u4ErrorCode, u4PwIndex, pPwEnetArgs->i4PwType)
        == SNMP_FAILURE)
    {
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }

    if (nmhSetPwType (u4PwIndex, pPwEnetArgs->i4PwType) == SNMP_FAILURE)
    {
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }

    /* Setting the Pseudo wire to ACTIVE State */
    if (nmhTestv2PwRowStatus (&u4ErrorCode, u4PwIndex, ISS_ACTIVE)
        == SNMP_FAILURE)
    {
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }
    if (nmhSetPwRowStatus (u4PwIndex, ISS_ACTIVE) == SNMP_FAILURE)
    {
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }

    if (nmhSetPwEnetRowStatus (u4PwIndex, u4PwEnetPwInstance,
                               ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }
    /* Set Pw Enet Port Vlan   */
    if (nmhTestv2PwEnetPortVlan (&u4ErrorCode, u4PwIndex,
                                 u4PwEnetPwInstance,
                                 pPwEnetArgs->u4PortVlan) == SNMP_FAILURE)
    {
        nmhSetPwEnetRowStatus (u4PwIndex, u4PwEnetPwInstance, ISS_DESTROY);
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }

    if (nmhSetPwEnetPortVlan (u4PwIndex, u4PwEnetPwInstance,
                              pPwEnetArgs->u4PortVlan) == SNMP_FAILURE)
    {
        nmhSetPwEnetRowStatus (u4PwIndex, u4PwEnetPwInstance, ISS_DESTROY);
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }

    /* Set Pw Enet Port If Index   */
    if (pPwEnetArgs->i4PortIfIndex != 0)
    {
        if (nmhTestv2PwEnetPortIfIndex (&u4ErrorCode, u4PwIndex,
                                        u4PwEnetPwInstance,
                                        pPwEnetArgs->i4PortIfIndex)
            == SNMP_FAILURE)
        {
            nmhSetPwEnetRowStatus (u4PwIndex, u4PwEnetPwInstance, ISS_DESTROY);
            MPLS_L2VPN_UNLOCK ();
            return WEB_FAILURE;
        }

        if (nmhSetPwEnetPortIfIndex (u4PwIndex, u4PwEnetPwInstance,
                                     pPwEnetArgs->i4PortIfIndex)
            == SNMP_FAILURE)
        {
            nmhSetPwEnetRowStatus (u4PwIndex, u4PwEnetPwInstance, ISS_DESTROY);
            MPLS_L2VPN_UNLOCK ();
            return WEB_FAILURE;
        }
    }

    if (nmhTestv2PwEnetPwVlan (&u4ErrorCode, u4PwIndex, u4PwEnetPwInstance,
                               pPwEnetArgs->u4PortVlan) == SNMP_FAILURE)
    {
        nmhSetPwEnetRowStatus (u4PwIndex, u4PwEnetPwInstance, ISS_DESTROY);
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }

    if (nmhSetPwEnetPwVlan (u4PwIndex, u4PwEnetPwInstance,
                            pPwEnetArgs->u4PortVlan) == SNMP_FAILURE)
    {
        nmhSetPwEnetRowStatus (u4PwIndex, u4PwEnetPwInstance, ISS_DESTROY);
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }

    /* Set Pw Enet Pw Row Status to active */
    if (nmhTestv2PwEnetRowStatus (&u4ErrorCode, u4PwIndex, u4PwEnetPwInstance,
                                  ISS_ACTIVE) == SNMP_FAILURE)
    {
        nmhSetPwEnetRowStatus (u4PwIndex, u4PwEnetPwInstance, ISS_DESTROY);
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }

    if (nmhSetPwEnetRowStatus (u4PwIndex, u4PwEnetPwInstance, ISS_ACTIVE)
        == SNMP_FAILURE)
    {
        nmhSetPwEnetRowStatus (u4PwIndex, u4PwEnetPwInstance, ISS_DESTROY);
        MPLS_L2VPN_UNLOCK ();
        return WEB_FAILURE;
    }

    MPLS_L2VPN_UNLOCK ();
    return WEB_SUCCESS;
}

/******************************************************************************
*  Function Name : IssGetLastEnetInstance
*  Description   : This function Counts the No. of ENET Entries Available and
*                  returns Last Enet Instance Created for the Passed PwIndex.                     
*  Input(s)      : u4PwVcIndex - Pseudo wire Index
*  Output(s)     : None.
*  Return Values : u4EnetInstance
******************************************************************************/

UINT4
IssGetLastEnetInstance (UINT4 u4PwVcIndex)
{
    BOOL1               bLoop = FALSE;
    UINT4               u4PwIndex = 0;
    UINT4               u4PwEnetInst = 0;
    UINT4               u4PwIndexPrev = 0;
    UINT4               u4PwEnetInstPrev = 0;
    UINT4               u4EnetInstance = 0;

    while (1)
    {
        MPLS_L2VPN_LOCK ();
        if ((bLoop == FALSE) &&
            (nmhGetFirstIndexPwEnetTable (&u4PwIndex, &u4PwEnetInst)
             == SNMP_FAILURE))
        {
            break;
        }

        if ((bLoop == TRUE) &&
            (nmhGetNextIndexPwEnetTable (u4PwIndexPrev, &u4PwIndex,
                                         u4PwEnetInstPrev, &u4PwEnetInst)
             == SNMP_FAILURE))
        {
            break;
        }

        bLoop = TRUE;
        u4PwIndexPrev = u4PwIndex;
        u4PwEnetInstPrev = u4PwEnetInst;
        if (u4PwVcIndex == u4PwIndex)
        {
            u4EnetInstance++;
        }
        MPLS_L2VPN_UNLOCK ();
    }
    MPLS_L2VPN_UNLOCK ();
    return u4EnetInstance;
}

/******************************************************************************
*  Function Name : IssMplsDeleteEnetEntry
*  Description   : This function deletes the ENET Entry corresponding to the 
*                  arguments passed.                     
*  Input(s)      : pHttp          - Pointer to the global HTTP data structure.
*                  i4PwType       - Pseudo wire Type
*                  i4PwMode       - Pseudo wire Mode
*                  u4PortVlan     - Vlan Id
*                  i4PortIfIndex  - Port No.
*  Output(s)     : None.
*  Return Values : WEB_SUCCESS or WEB_FAILURE
******************************************************************************/

INT4
IssMplsDeleteEnetEntry (tHttp * pHttp, INT4 i4PwType, INT4 i4PwMode,
                        UINT4 u4PortVlan, INT4 i4PortIfIndex)
{
    INT4                i4GetPwMode = 0;
    INT4                i4GetPwType = 0;
    INT4                i4GetPortIfIndex = 0;

    UINT4               u4GetPortVlan = 0;
    UINT4               u4PwIndex = 0;
    UINT4               u4PwIndexPrev = 0;
    UINT4               u4PwInstance = 0;
    UINT4               u4PwInstancePrev = 0;
    UINT4               u4ErrorCode = 0;

    BOOL1               bLoop = FALSE;

    /* Scan through the PwEnetTable to get the PwType, PwMode,
     * PortVlan and PortIfIndex of each PwEnetEntry */
    while (1)
    {
        MPLS_L2VPN_LOCK ();
        if ((bLoop == FALSE) &&
            (nmhGetFirstIndexPwEnetTable (&u4PwIndex, &u4PwInstance)
             == SNMP_FAILURE))
        {
            break;
        }

        if ((bLoop == TRUE) &&
            (nmhGetNextIndexPwEnetTable (u4PwIndexPrev, &u4PwIndex,
                                         u4PwInstancePrev, &u4PwInstance)
             == SNMP_FAILURE))
        {
            break;
        }

        bLoop = TRUE;
        u4PwInstancePrev = u4PwInstance;
        u4PwIndexPrev = u4PwIndex;
        if (nmhGetFsMplsL2VpnPwMode (u4PwIndex, &i4GetPwMode) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        if (nmhGetPwType (u4PwIndex, &i4GetPwType) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        if (i4GetPwType == 4)
        {
            if (nmhGetPwEnetPortVlan (u4PwIndex, u4PwInstance, &u4GetPortVlan)
                == SNMP_FAILURE)
            {
                MPLS_L2VPN_UNLOCK ();
                continue;
            }
        }
        else
        {
            if (nmhGetPwEnetPortIfIndex (u4PwIndex, u4PwInstance,
                                         &i4GetPortIfIndex) == SNMP_FAILURE)
            {
                MPLS_L2VPN_UNLOCK ();
                continue;
            }
        }

        /* If Match found between the argument passed to this function,
         * (PwType, PwMode, PortVlan and PortIfIndex) and corresponding
         * values obtained from the PwEnetTable, delete the entry */
        if ((i4PwType == i4GetPwType) && (i4PwMode == i4GetPwMode) &&
            (u4PortVlan == u4GetPortVlan) &&
            (i4PortIfIndex == i4GetPortIfIndex))
        {
            if (nmhTestv2PwEnetRowStatus (&u4ErrorCode, u4PwIndex, u4PwInstance,
                                          ISS_DESTROY) == SNMP_FAILURE)
            {
                MPLS_L2VPN_UNLOCK ();
                IssSendError (pHttp, (CONST INT1 *) "Failed in Deleting Enet");
                return WEB_FAILURE;
            }

            if (nmhSetPwEnetRowStatus (u4PwIndex, u4PwInstance, ISS_DESTROY)
                == SNMP_FAILURE)
            {
                MPLS_L2VPN_UNLOCK ();
                IssSendError (pHttp, (CONST INT1 *) "Failed in Deleting Enet");
                return WEB_FAILURE;
            }
            break;
        }
        MPLS_L2VPN_UNLOCK ();
    }
    MPLS_L2VPN_UNLOCK ();
    return WEB_SUCCESS;
}

#ifdef MI_WANTED
/******************************************************************************
*  Function Name : IssPrintAvailableVfi
*  Description   : This function displays the web page with all the available 
*                  VFI Names till the corresponding scrollbar.                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
******************************************************************************/
VOID
IssPrintAvailableVfi (tHttp * pHttp)
{
    UINT4               u4VplsIndex = 0;
    UINT4               u4VplsIndexPrev = 0;
    INT4                i4PwMode = 0;

    tSNMP_OCTET_STRING_TYPE VplsName;

    static UINT1        au1VplsName[L2VPN_MAX_VPLS_NAME_LEN + 1] = { 0 };
    BOOL1               bLoop = FALSE;

    VplsName.pu1_OctetList = au1VplsName;
    VplsName.i4_Length = L2VPN_MAX_VPLS_NAME_LEN;

    STRCPY (pHttp->au1KeyString, "<! VFI NAME>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    while (1)
    {
        MPLS_L2VPN_LOCK ();
        if ((bLoop == TRUE) &&
            (nmhGetNextIndexFsMplsVplsConfigTable (u4VplsIndexPrev,
                                                   &u4VplsIndex)
             == SNMP_FAILURE))
        {
            break;
        }

        if ((bLoop == FALSE) &&
            (nmhGetFirstIndexFsMplsVplsConfigTable (&u4VplsIndex)
             == SNMP_FAILURE))
        {
            MPLS_L2VPN_UNLOCK ();
            return;
        }

        bLoop = TRUE;
        u4VplsIndexPrev = u4VplsIndex;

        MPLS_L2VPN_UNLOCK ();
        IssMplsGetPwModeFromVplsIndex (u4VplsIndex, &i4PwMode);
        MPLS_L2VPN_LOCK ();

        MEMSET (VplsName.pu1_OctetList, 0, L2VPN_MAX_VPLS_NAME_LEN);
        if (nmhGetFsMplsVplsName (u4VplsIndex, &VplsName) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }

        if (i4PwMode != 1)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString,
                     "<option value = \"%s\" selected>%s \n", au1VplsName,
                     au1VplsName);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
        }
        MPLS_L2VPN_UNLOCK ();
    }
    MPLS_L2VPN_UNLOCK ();
}
#endif

/******************************************************************************
*  Function Name : IssMplsGetPwModeFromVplsIndex
*  Description   : This function gets the Pw Mode for the VPLS Index passed.                     
*  Input(s)      : u4VplsIndex - Vpls Index
*  Output(s)     : pi4PwMode   - Pw Mode
*  Return Values : None
******************************************************************************/
VOID
IssMplsGetPwModeFromVplsIndex (UINT4 u4VplsIndex, INT4 *pi4PwMode)
{
    UINT4               u4PwIndex = 0;
    UINT4               u4PwIndexPrev = 0;
    UINT4               u4GetVplsIndex = 0;

    INT4                i4GetPwMode = 0;

    BOOL1               bLoop = FALSE;

    while (1)
    {
        MPLS_L2VPN_LOCK ();
        if ((bLoop == FALSE) &&
            (nmhGetFirstIndexPwTable (&u4PwIndex) == SNMP_FAILURE))
        {
            break;
        }

        if ((bLoop == TRUE) &&
            (nmhGetNextIndexPwTable (u4PwIndexPrev, &u4PwIndex)
             == SNMP_FAILURE))
        {
            break;
        }

        bLoop = TRUE;
        u4PwIndexPrev = u4PwIndex;

        if (nmhGetFsMplsL2VpnVplsIndex (u4PwIndex, &u4GetVplsIndex)
            == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }

        if (u4GetVplsIndex == u4VplsIndex)
        {
            if (nmhGetFsMplsL2VpnPwMode (u4PwIndex, &i4GetPwMode)
                == SNMP_FAILURE)
            {
                MPLS_L2VPN_UNLOCK ();
                continue;
            }
            break;
        }
        MPLS_L2VPN_UNLOCK ();
    }

    *pi4PwMode = i4GetPwMode;
    MPLS_L2VPN_UNLOCK ();
}

/******************************************************************************
*  Function Name : IssPrintAvailablePw
*  Description   : This function displays the web page with all the available 
*                  PW till the corresponding scrollbar.                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
******************************************************************************/
VOID
IssPrintAvailablePw (tHttp * pHttp)
{
    BOOL1               bLoop = FALSE;

    UINT4               u4PwIndex = 0;
    UINT4               u4PwIndexPrev = 0;

    INT4                i4PwMode = 0;

    STRCPY (pHttp->au1KeyString, "<! PW INDEX>");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    while (1)
    {
        MPLS_L2VPN_LOCK ();
        if ((bLoop == FALSE) &&
            (nmhGetFirstIndexPwTable (&u4PwIndex) == SNMP_FAILURE))
        {
            break;
        }

        if ((bLoop == TRUE) &&
            (nmhGetNextIndexPwTable (u4PwIndexPrev, &u4PwIndex)
             == SNMP_FAILURE))
        {
            break;
        }

        u4PwIndexPrev = u4PwIndex;
        bLoop = TRUE;

        if (nmhGetFsMplsL2VpnPwMode (u4PwIndex, &i4PwMode) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }

        if (i4PwMode == 1)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString,
                     "<option value = \"%d\" selected>%d \n", u4PwIndex,
                     u4PwIndex);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
        }
        MPLS_L2VPN_UNLOCK ();
    }
    MPLS_L2VPN_UNLOCK ();
}

/*******************************************************************************
*  Function Name : IssProcessMplsMapPage
*  Description   : This function processes the request coming for the Mpls
*                  Map Page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssProcessMplsMapPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessMplsMapGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessMplsMapSet (pHttp);
    }

}

/*******************************************************************************
*  Function Name : IssProcessMplsMapGet
*  Description   : This function shows theMapping of CFA and  Mpls PW Entries in the web page 
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/

VOID
IssProcessMplsMapGet (tHttp * pHttp)
{
    UINT4               u4PwId = 0;
    UINT4               u4VcId = 0;
    UINT4               u4VcIdPrev = 0;
    INT4                i4PwIfIndex = 0;
    UINT4               u4Temp = 0;
    INT4                i4RetVal = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { 0 };

    /* Display till the Table header of the bottom form in VFI Page */
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    WebnmRegisterLock (pHttp, L2vpnLock, L2vpnUnLock);

    u4Temp = (UINT4) pHttp->i4Write;
    MPLS_L2VPN_LOCK ();
    if (nmhGetFirstIndexPwTable (&u4VcId) == SNMP_FAILURE)
    {
        MPLS_L2VPN_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    do
    {
        i4PwIfIndex = 0;
        u4VcIdPrev = u4VcId;

        nmhGetPwID (u4VcId, &u4PwId);
        i4RetVal = nmhGetPwIfIndex (u4VcId, &i4PwIfIndex);
        if (i4RetVal == SNMP_FAILURE)
        {
            continue;
        }
        if (i4PwIfIndex == 0)
        {
            continue;
        }
        pHttp->i4Write = (INT4) u4Temp;

        STRCPY (pHttp->au1KeyString, "CFA_NAME");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        CfaCliGetIfName ((UINT4) i4PwIfIndex, (INT1 *) au1IfName);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1IfName);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "MPLS_NAME");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4PwId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    while (nmhGetNextIndexPwTable (u4VcIdPrev, &u4VcId) != SNMP_FAILURE);
    MPLS_L2VPN_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    /* Display all the information in the html page after the Table stop flag */
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*******************************************************************************
*  Function Name : IssProcessMplsMapSet 
*  Description   : This function processes the request coming for
*                  Mpls Map Page and tries to map the cfa and mpls pseudowire 
*                  with the given use input from the web page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/

VOID
IssProcessMplsMapSet (tHttp * pHttp)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { 0 };
    UINT4               u4PwId = 0;
    UINT4               u4RetValPwID = 0;
    UINT4               u4PwIndex = 0;
    UINT4               u4PwPrevIndex = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4PwIfIndex = 0;

    STRNCPY (pHttp->au1Name, "CFA_NAME", (sizeof (pHttp->au1Name) - 1));
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1IfName, pHttp->au1Value, (sizeof (au1IfName) - 1));

    if ((CfaGetInterfaceIndexFromName (au1IfName,
                                       &u4PwIfIndex)) == OSIX_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "InValid Interface Index");
        return;
    }
    STRNCPY (pHttp->au1Name, "MPLS_NAME", (sizeof (pHttp->au1Name) - 1));
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    u4PwId = (UINT4) ATOI (pHttp->au1Value);

    if (nmhGetFirstIndexPwTable (&u4PwIndex) == SNMP_FAILURE)
    {
        MPLS_L2VPN_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *) "Invalid PWID");
        return;
    }
    do
    {
        u4PwPrevIndex = u4PwIndex;
        nmhGetPwID (u4PwIndex, &u4RetValPwID);
        if (u4RetValPwID == u4PwId)
        {
            break;
        }
    }
    while (nmhGetNextIndexPwTable (u4PwPrevIndex, &u4PwIndex) == SNMP_SUCCESS);

    if (u4RetValPwID != u4PwId)
    {
        MPLS_L2VPN_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *) "Invalid PWID");
        return;
    }
    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Value, "Delete") != 0)
    {
        if (nmhTestv2PwRowStatus (&u4ErrorCode, u4PwIndex,
                                  ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Failed To Map the Given Entry");
            return;
        }

        if (nmhSetPwRowStatus (u4PwIndex, ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Failed To Map the Given Entry");
            return;
        }
        if (nmhTestv2PwIfIndex (&u4ErrorCode, u4PwIndex, (INT4) u4PwIfIndex)
            == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Failed To Map the Given Entry");
            return;
        }
        if (nmhSetPwIfIndex (u4PwIndex, u4PwIfIndex) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Failed To Map the Given Entry");
            return;
        }

        if (nmhTestv2PwRowStatus (&u4ErrorCode, u4PwIndex,
                                  ISS_ACTIVE) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Failed To Map the Given Entry");
            return;
        }

        if (nmhSetPwRowStatus (u4PwIndex, ISS_ACTIVE) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Failed To Map the Given Entry");
            return;
        }

    }
    else
    {
        u4PwIfIndex = 0;
        if (nmhTestv2PwRowStatus (&u4ErrorCode, u4PwIndex,
                                  ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();

            IssSendError (pHttp,
                          (CONST INT1 *) "Failed To Unmap the Given Entry");
            return;
        }

        if (nmhSetPwRowStatus (u4PwIndex, ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Failed To Unmap the Given Entry");
            return;
        }

        if (nmhTestv2PwIfIndex (&u4ErrorCode, u4PwIndex, (INT4) u4PwIfIndex)
            == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Failed To Unmap the Given Entry");
            return;
        }
        if (nmhSetPwIfIndex (u4PwIndex, u4PwIfIndex) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Failed To Unmap the Given Entry");
            return;
        }

        if (nmhTestv2PwRowStatus (&u4ErrorCode, u4PwIndex,
                                  ISS_ACTIVE) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Failed To Unmap the Given Entry");
            return;
        }

        if (nmhSetPwRowStatus (u4PwIndex, ISS_ACTIVE) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Failed To Unmap the Given Entry");
            return;
        }
    }
    MPLS_L2VPN_UNLOCK ();
    IssProcessMplsMapGet (pHttp);
}

/*******************************************************************************
*  Function Name : IssProcessMplsConfL3vpnPage
*  Description   : This function processes the request coming for the Mpls L3vpn
*                  Page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
#ifdef MPLS_L3VPN_WANTED
VOID
IssProcessMplsConfL3vpnPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessMplsL3vpnGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessMplsL3vpnSet (pHttp);
    }
}
#endif
/*******************************************************************************
*  Function Name : IssProcessMplsL3vpnGet
*  Description   : This function shows the Mpls L3vpn Entries in the web page
*                  and enables user to enter entries to configure Mpls L3vpn 
*                  entries.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
#ifdef MPLS_L3VPN_WANTED
VOID
IssProcessMplsL3vpnGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
    tSNMP_OCTET_STRING_TYPE MplsL3VpnPrevVrfName;
    tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRD;
    tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRT;

    UINT4               u4MplsL3VpnPrevVrfRTIndex = 0;
    UINT4               u4Temp = 0;
    UINT4               u4MplsL3VpnVrfRTIndex = 0;

    INT4                i4MplsL3VpnPrevVrfRTType = 0;
    INT4                i4MplsL3VpnVrfRTType = 0;

    static UINT1        au1MplsL3VpnVrfName[L3VPN_MAX_VRF_NAME_LEN + 1] = { 0 };
    static UINT1        au1l3vpnRd[L3VPN_MAX_RD_LEN] = { 0 };
    static UINT1        au1l3vpnRt[L3VPN_MAX_RT_LEN] = { 0 };

    BOOL1               bLoop = FALSE;
    BOOL1               bRtLoop = FALSE;

    UINT1               au1RDString[100];
    UINT2               u2ASN = 0;
    UINT4               u4AsignedNumber = 0;
    UINT2               u2AsignedNumber = 0;
    UINT4               u4ASN = 0;

    UINT1               au1PolicyString[MAX_RT_POLICY_LEN];
    UINT1               au1RTString[100];

    MEMSET (au1RDString, 0, 100);
    MEMSET (au1PolicyString, 0, MAX_RT_POLICY_LEN);
    pHttp->i4Write = 0;
    MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;
    MplsL3VpnVrfName.i4_Length = L3VPN_MAX_VRF_NAME_LEN;
    MplsL3VpnVrfRD.pu1_OctetList = au1l3vpnRd;
    MplsL3VpnVrfRD.i4_Length = L3VPN_MAX_RD_LEN;
    MplsL3VpnVrfRT.pu1_OctetList = au1l3vpnRt;
    MplsL3VpnVrfRT.i4_Length = L3VPN_MAX_RT_LEN;

    /* Display till the Table header of the bottom form in L3vpn Page */
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    WebnmRegisterLock (pHttp, L3vpnMainTaskLock, L3vpnMainTaskUnLock);
    u4Temp = (UINT4) pHttp->i4Write;
    while (1)
    {
        L3VPN_LOCK;
        if ((bLoop == TRUE) &&
            (nmhGetNextIndexMplsL3VpnVrfTable (&MplsL3VpnPrevVrfName,
                                               &MplsL3VpnVrfName) ==
             SNMP_FAILURE))
        {
            L3VPN_UNLOCK;
            break;
        }

        if ((bLoop == FALSE) &&
            (nmhGetFirstIndexMplsL3VpnVrfTable (&MplsL3VpnVrfName) ==
             SNMP_FAILURE))
        {
            L3VPN_UNLOCK;
            break;
        }
        bLoop = TRUE;

        MEMCPY (&MplsL3VpnPrevVrfName,
                &MplsL3VpnVrfName, sizeof (MplsL3VpnVrfName));

        if (nmhGetMplsL3VpnVrfRD (&MplsL3VpnVrfName, &MplsL3VpnVrfRD)
            == SNMP_FAILURE)
        {
            L3VPN_UNLOCK;
            break;
        }

        if (MPLS_WEB_L3VPN_RD_TYPE_0 == (MplsL3VpnVrfRD.pu1_OctetList)[0])
        {
            MEMCPY (&u2ASN, &(MplsL3VpnVrfRD.pu1_OctetList)[2], sizeof (UINT2));
            MEMCPY (&u4AsignedNumber,
                    &(MplsL3VpnVrfRD.pu1_OctetList)[4], sizeof (UINT4));
            u2ASN = OSIX_NTOHS (u2ASN);
            u4AsignedNumber = OSIX_NTOHL (u4AsignedNumber);
            SPRINTF ((CHR1 *) au1RDString, "%d:%d", u2ASN, u4AsignedNumber);
        }
        else if (MPLS_WEB_L3VPN_RD_TYPE_1 == (MplsL3VpnVrfRD.pu1_OctetList)[0])
        {
            MEMCPY (&u2AsignedNumber,
                    &(MplsL3VpnVrfRD.pu1_OctetList)[6], sizeof (UINT2));
            u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);
            SPRINTF ((CHR1 *) au1RDString, "%d.%d.%d.%d:%d",
                     (MplsL3VpnVrfRD.pu1_OctetList)[2],
                     (MplsL3VpnVrfRD.pu1_OctetList)[3],
                     (MplsL3VpnVrfRD.pu1_OctetList)[4],
                     (MplsL3VpnVrfRD.pu1_OctetList)[5], u2AsignedNumber);
        }
        else
        {
            MEMCPY (&u4ASN, &(MplsL3VpnVrfRD.pu1_OctetList)[2], sizeof (UINT4));
            MEMCPY (&u2AsignedNumber,
                    &(MplsL3VpnVrfRD.pu1_OctetList)[6], sizeof (UINT2));
            u4ASN = OSIX_NTOHL (u4ASN);
            u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);
            SPRINTF ((CHR1 *) au1RDString, "%d.%d:%hu",
                     (UINT4) ((u4ASN & 0xffff0000) >> 16),
                     (UINT4) (u4ASN & 0x0000ffff), u2AsignedNumber);
        }

        pHttp->i4Write = (INT4) u4Temp;
        STRCPY (pHttp->au1KeyString, "VRF_NAME");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1MplsL3VpnVrfName);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "RD");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1RDString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        L3VPN_UNLOCK;
    }
    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    WebnmSendString (pHttp, ISS_RT_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;
    while (1)
    {
        L3VPN_LOCK;
        if ((bRtLoop == FALSE) &&
            (nmhGetFirstIndexMplsL3VpnVrfRTTable (&MplsL3VpnVrfName,
                                                  &u4MplsL3VpnVrfRTIndex,
                                                  &i4MplsL3VpnVrfRTType) ==
             SNMP_FAILURE))
        {
            L3VPN_UNLOCK;
            break;
        }

        if ((bRtLoop == TRUE) &&
            (nmhGetNextIndexMplsL3VpnVrfRTTable
             (&MplsL3VpnPrevVrfName, &MplsL3VpnVrfName,
              u4MplsL3VpnPrevVrfRTIndex, &u4MplsL3VpnVrfRTIndex,
              i4MplsL3VpnPrevVrfRTType, &i4MplsL3VpnVrfRTType) == SNMP_FAILURE))
        {
            L3VPN_UNLOCK;
            break;
        }

        if (nmhGetMplsL3VpnVrfRT
            (&MplsL3VpnVrfName, u4MplsL3VpnVrfRTIndex, i4MplsL3VpnVrfRTType,
             &MplsL3VpnVrfRT) == SNMP_FAILURE)
        {
            L3VPN_UNLOCK;
            break;
        }
        bRtLoop = TRUE;

        MEMCPY (&MplsL3VpnPrevVrfName,
                &MplsL3VpnVrfName, sizeof (MplsL3VpnVrfName));
        u4MplsL3VpnPrevVrfRTIndex = u4MplsL3VpnVrfRTIndex;
        i4MplsL3VpnPrevVrfRTType = i4MplsL3VpnVrfRTType;

        if (MPLS_WEB_L3VPN_RT_TYPE_0 == (MplsL3VpnVrfRT.pu1_OctetList)[0])
        {
            MEMCPY (&u2ASN, &(MplsL3VpnVrfRT.pu1_OctetList)[2], sizeof (UINT2));
            MEMCPY (&u4AsignedNumber,
                    &(MplsL3VpnVrfRT.pu1_OctetList)[4], sizeof (UINT4));
            u2ASN = OSIX_NTOHS (u2ASN);
            u4AsignedNumber = OSIX_NTOHL (u4AsignedNumber);
            SPRINTF ((CHR1 *) au1RTString, "%d:%d", u2ASN, u4AsignedNumber);
        }
        else if (MPLS_WEB_L3VPN_RT_TYPE_1 == (MplsL3VpnVrfRT.pu1_OctetList)[0])
        {
            MEMCPY (&u2AsignedNumber,
                    &(MplsL3VpnVrfRT.pu1_OctetList)[6], sizeof (UINT2));
            u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);
            SPRINTF ((CHR1 *) au1RTString, "%d.%d.%d.%d:%d",
                     (MplsL3VpnVrfRT.pu1_OctetList)[2],
                     (MplsL3VpnVrfRT.pu1_OctetList)[3],
                     (MplsL3VpnVrfRT.pu1_OctetList)[4],
                     (MplsL3VpnVrfRT.pu1_OctetList)[5], u2AsignedNumber);
        }
        else
        {
            MEMCPY (&u4ASN, &(MplsL3VpnVrfRT.pu1_OctetList)[2], sizeof (UINT4));
            MEMCPY (&u2AsignedNumber,
                    &(MplsL3VpnVrfRT.pu1_OctetList)[6], sizeof (UINT2));
            u4ASN = OSIX_NTOHL (u4ASN);
            u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);
            SPRINTF ((CHR1 *) au1RTString, "%d.%d:%hu",
                     (UINT4) ((u4ASN & 0xffff0000) >> 16),
                     (UINT4) (u4ASN & 0x0000ffff), u2AsignedNumber);
        }

        pHttp->i4Write = (INT4) u4Temp;
        STRCPY (pHttp->au1KeyString, "RT_VRF_NAME");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1MplsL3VpnVrfName);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_RT_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "RT_RT");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1RTString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        WebnmSendString (pHttp, ISS_RT_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "Policy");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4MplsL3VpnVrfRTType == MPLS_L3VPN_RT_IMPORT)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Import");
        }
        else if (i4MplsL3VpnVrfRTType == MPLS_L3VPN_RT_EXPORT)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Export");
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Both");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_RT_PARAM_STOP_FLAG);
        L3VPN_UNLOCK;

    }

    WebnmUnRegisterLock (pHttp);
    IssSkipString (pHttp, ISS_RT_TABLE_STOP_FLAG);

    /* Display all the information in the html page after the Table stop flag */
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

#endif

/*******************************************************************************
*  Function Name : IssProcessMplsL3vpnSet
*  Description   : This function processes the request coming for
*                  Mpls L3VPN Page and tries to set the L3VPN entries
*                  with the given use input from the web page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
#ifdef MPLS_L3VPN_WANTED
UINT1               au1RtTemp[128][L3VPN_MAX_RT_LEN];
UINT1               au1RtPolicy[128][L3VPN_MAX_RT_POLICY_LEN];
UINT1              *pu1RtL3vpn[128];

VOID
IssProcessMplsL3vpnSet (tHttp * pHttp)
{

    INT4                i4RetVal = WEB_SUCCESS;
    INT4                i4AdditionalRt = 0;
    INT4                i4arrayindex = 0;

    UINT1              *pu1Rd = NULL;
    UINT1               au1VrfName[L3VPN_MAX_VRF_NAME_LEN] =
        { MPLS_L3VPN_ZERO };
    UINT1               au1RTVrfName[L3VPN_MAX_VRF_NAME_LEN] =
        { MPLS_L3VPN_ZERO };
    UINT1               au1Rd[L3VPN_MAX_RD_RT_STRING_LEN] = { MPLS_L3VPN_ZERO };
    UINT1               au1Rt[L3VPN_MAX_RD_RT_STRING_LEN] = { MPLS_L3VPN_ZERO };
    UINT1               au1DeleteRT[L3VPN_MAX_RD_RT_STRING_LEN] =
        { MPLS_L3VPN_ZERO };
    UINT1               au1RdTemp[L3VPN_MAX_RD_LEN] = { MPLS_L3VPN_ZERO };
    UINT1               au1DeleteRTTemp[L3VPN_MAX_RT_LEN] = { MPLS_L3VPN_ZERO };
    UINT1               au1NumberOfRoutes[L3VPN_MAX_NUMBER_OF_ROUTES_LEN] =
        { MPLS_L3VPN_ZERO };
    UINT1
         
         
         
         
         
         
         
        au1HighRouteThreshold[L3VPN_MAX_HIGH_ROUTE_THRESHOLD_LEN] =
        { MPLS_L3VPN_ZERO };
    UINT1               au1MidRouteThreshold[L3VPN_MAX_MID_ROUTE_THRESHOLD_LEN]
        = { MPLS_L3VPN_ZERO };
    UINT1               au1VrfDescription[L3VPN_MAX_VRF_DESCRIPTION_LEN] =
        { MPLS_L3VPN_ZERO };
    UINT1               au1String[100] = { '\0' };
    UINT1               au1RtString[10] = { '\0' };
    UINT1               au1RtPolicyString[20] = { '\0' };

    CHR1               *pu1String = NULL;
    CHR1               *pu1next;

    MEMSET (au1VrfName, MPLS_L3VPN_ZERO, L3VPN_MAX_VRF_NAME_LEN);
    MEMSET (au1RTVrfName, MPLS_L3VPN_ZERO, L3VPN_MAX_VRF_NAME_LEN);
    MEMSET (au1Rd, MPLS_L3VPN_ZERO, L3VPN_MAX_RD_RT_STRING_LEN);
    MEMSET (au1Rt, MPLS_L3VPN_ZERO, L3VPN_MAX_RD_RT_STRING_LEN);
    MEMSET (au1RdTemp, MPLS_L3VPN_ZERO, L3VPN_MAX_RD_LEN);
    MEMSET (au1RtTemp, MPLS_L3VPN_ZERO, (128 * L3VPN_MAX_RT_LEN));
    MEMSET (au1RtPolicy, MPLS_L3VPN_ZERO, (128 * L3VPN_MAX_RT_POLICY_LEN));
    MEMSET (au1NumberOfRoutes, MPLS_L3VPN_ZERO, L3VPN_MAX_NUMBER_OF_ROUTES_LEN);
    MEMSET (au1HighRouteThreshold, MPLS_L3VPN_ZERO,
            L3VPN_MAX_HIGH_ROUTE_THRESHOLD_LEN);
    MEMSET (au1MidRouteThreshold, MPLS_L3VPN_ZERO,
            L3VPN_MAX_MID_ROUTE_THRESHOLD_LEN);
    MEMSET (au1VrfDescription, MPLS_L3VPN_ZERO, L3VPN_MAX_VRF_DESCRIPTION_LEN);

    MEMSET (pHttp->au1Name, MPLS_L3VPN_ZERO, sizeof (pHttp->au1Name));
    MEMSET (pHttp->au1Value, MPLS_L3VPN_ZERO, ENM_MAX_NAME_LEN);

    MEMSET (pu1RtL3vpn, MPLS_L3VPN_ZERO, sizeof (pu1RtL3vpn));

    STRNCPY (pHttp->au1Name, "VRF_NAME",
             (sizeof (pHttp->au1Name) - MPLS_L3VPN_ONE));
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1VrfName, pHttp->au1Value,
             (sizeof (au1VrfName) - MPLS_L3VPN_ONE));

    MEMSET (pHttp->au1Name, MPLS_L3VPN_ZERO, sizeof (pHttp->au1Name));
    MEMSET (pHttp->au1Value, MPLS_L3VPN_ZERO, ENM_MAX_NAME_LEN);

    STRNCPY (pHttp->au1Name, "RT_VRF_NAME",
             (sizeof (pHttp->au1Name) - MPLS_L3VPN_ONE));
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1RTVrfName, pHttp->au1Value,
             (sizeof (au1RTVrfName) - MPLS_L3VPN_ONE));

    MEMSET (pHttp->au1Name, MPLS_L3VPN_ZERO, sizeof (pHttp->au1Name));
    MEMSET (pHttp->au1Value, MPLS_L3VPN_ZERO, ENM_MAX_NAME_LEN);
    STRNCPY (pHttp->au1Name, "RD", (sizeof (pHttp->au1Name) - MPLS_L3VPN_ONE));

    pu1String = (CHR1 *) au1String;

    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery) != FAILURE)
    {
        STRNCPY (pu1String, pHttp->au1Value,
                 (sizeof (au1String) - MPLS_L3VPN_ONE));

        pu1next = pu1String;
        i4arrayindex = 0;

        if (STRLEN (pHttp->au1Value) != MPLS_L3VPN_ZERO)
        {
            while ((pu1next = strstr (pu1String, "%3A")) != NULL)
            {
                while (pu1next != pu1String)
                {
                    au1Rd[i4arrayindex] = (UINT1) *pu1String;
                    pu1String++;
                    i4arrayindex++;
                }
                pu1String += L3VPN_MAX_RD_RT_STRING_TOKEN;
                au1Rd[i4arrayindex++] = ':';
            }

            while (*pu1String != '\0')
            {
                au1Rd[i4arrayindex] = (UINT1) *pu1String;
                pu1String++;
                i4arrayindex++;
            }
            au1Rd[i4arrayindex] = '\0';

            if (CLI_FAILURE == L3VpnCliParseAndGenerateRdRt (au1Rd, au1RdTemp))
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Failed in Parsing and Generating the RD");
                return;
            }
            au1RdTemp[1] = L3VPN_RD_SUBTYPE;
        }
    }

    /*Loop for parsing and generating RTs */
    while (1)
    {
        MEMSET (au1String, MPLS_L3VPN_ZERO, sizeof (au1String));
        pu1String = (CHR1 *) au1String;

        SNPRINTF ((CHR1 *) au1RtString, sizeof (au1RtString), "%s%d", "RT",
                  i4AdditionalRt);

        MEMSET (pHttp->au1Name, MPLS_L3VPN_ZERO, sizeof (pHttp->au1Name));
        MEMSET (pHttp->au1Value, MPLS_L3VPN_ZERO, ENM_MAX_NAME_LEN);
        STRNCPY (pHttp->au1Name, au1RtString,
                 (sizeof (pHttp->au1Name) - MPLS_L3VPN_ONE));

        if (FAILURE ==
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery))
        {
            break;
        }

        STRNCPY (pu1String, pHttp->au1Value,
                 (sizeof (au1String) - MPLS_L3VPN_ONE));

        pu1next = pu1String;
        i4arrayindex = 0;

        if (STRLEN (pHttp->au1Value) != MPLS_L3VPN_ZERO)
        {
            while ((pu1next = strstr (pu1String, "%3A")) != NULL)
            {
                while (pu1next != pu1String)
                {
                    au1Rt[i4arrayindex] = (UINT1) *pu1String;
                    pu1String++;
                    i4arrayindex++;
                }
                pu1String += L3VPN_MAX_RD_RT_STRING_TOKEN;
                au1Rt[i4arrayindex++] = ':';
            }

            while (*pu1String != '\0')
            {
                au1Rt[i4arrayindex] = (UINT1) *pu1String;
                pu1String++;
                i4arrayindex++;
            }
            au1Rt[i4arrayindex] = '\0';
        }

        if (CLI_FAILURE == L3VpnCliParseAndGenerateRdRt (au1Rt,
                                                         au1RtTemp
                                                         [i4AdditionalRt]))
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Failed in Parsing and Generating the RT");
            return;
        }
        au1RtTemp[i4AdditionalRt][1] = L3VPN_RT_SUBTYPE;

        SPRINTF ((CHR1 *) au1RtPolicyString, "%s%d", "Rt_Policy",
                 i4AdditionalRt);

        MEMSET (pHttp->au1Name, MPLS_L3VPN_ZERO, sizeof (pHttp->au1Name));
        MEMSET (pHttp->au1Value, MPLS_L3VPN_ZERO, ENM_MAX_NAME_LEN);
        STRNCPY (pHttp->au1Name, au1RtPolicyString,
                 (sizeof (pHttp->au1Name) - MPLS_L3VPN_ONE));

        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);

        STRNCPY (au1RtPolicy[i4AdditionalRt], pHttp->au1Value,
                 (sizeof (au1RtPolicy[i4AdditionalRt]) - MPLS_L3VPN_ONE));

        pu1RtL3vpn[i4AdditionalRt] = au1RtTemp[i4AdditionalRt];
        i4AdditionalRt++;

    }
    pu1RtL3vpn[i4AdditionalRt] = NULL;

    /*Code for Retrieving the RT value for deleting the particular RT */
    SPRINTF ((CHR1 *) au1RtString, "%s", "RT_RT");

    MEMSET (pHttp->au1Name, MPLS_L3VPN_ZERO, sizeof (pHttp->au1Name));
    MEMSET (pHttp->au1Value, MPLS_L3VPN_ZERO, ENM_MAX_NAME_LEN);
    STRNCPY (pHttp->au1Name, au1RtString,
             (sizeof (pHttp->au1Name) - MPLS_L3VPN_ONE));

    MEMSET (au1String, MPLS_L3VPN_ZERO, sizeof (au1String));
    pu1String = (CHR1 *) au1String;

    if (FAILURE !=
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery))
    {
        STRNCPY (pu1String, pHttp->au1Value,
                 (sizeof (au1String) - MPLS_L3VPN_ONE));

        pu1next = pu1String;
        i4arrayindex = 0;

        if (STRLEN (pHttp->au1Value) != MPLS_L3VPN_ZERO)
        {
            while ((pu1next = strstr (pu1String, "%3A")) != NULL)
            {
                while (pu1next != pu1String)
                {
                    au1DeleteRT[i4arrayindex] = (UINT1) *pu1String;
                    pu1String++;
                    i4arrayindex++;
                }
                pu1String += L3VPN_MAX_RD_RT_STRING_TOKEN;
                au1DeleteRT[i4arrayindex++] = ':';
            }

            while (*pu1String != '\0')
            {
                au1DeleteRT[i4arrayindex] = (UINT1) *pu1String;
                pu1String++;
                i4arrayindex++;
            }
            au1DeleteRT[i4arrayindex] = '\0';
        }

        if (CLI_FAILURE ==
            L3VpnCliParseAndGenerateRdRt (au1DeleteRT, au1DeleteRTTemp))
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Failed in Parsing and Generating the RT");
            return;
        }
        au1DeleteRTTemp[1] = L3VPN_RT_SUBTYPE;
    }

    MEMSET (pHttp->au1Name, MPLS_L3VPN_ZERO, sizeof (pHttp->au1Name));
    MEMSET (pHttp->au1Value, MPLS_L3VPN_ZERO, ENM_MAX_NAME_LEN);
    STRNCPY (pHttp->au1Name, "Number_Of_Routes",
             (sizeof (pHttp->au1Name) - MPLS_L3VPN_ONE));

    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1NumberOfRoutes, pHttp->au1Value,
             (sizeof (au1NumberOfRoutes) - MPLS_L3VPN_ONE));

    MEMSET (pHttp->au1Name, MPLS_L3VPN_ZERO, sizeof (pHttp->au1Name));
    MEMSET (pHttp->au1Value, MPLS_L3VPN_ZERO, ENM_MAX_NAME_LEN);
    STRNCPY (pHttp->au1Name, "High_Route_Threshold",
             (sizeof (pHttp->au1Name) - MPLS_L3VPN_ONE));

    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1HighRouteThreshold, pHttp->au1Value,
             (sizeof (au1HighRouteThreshold) - MPLS_L3VPN_ONE));

    MEMSET (pHttp->au1Name, MPLS_L3VPN_ZERO, sizeof (pHttp->au1Name));
    MEMSET (pHttp->au1Value, MPLS_L3VPN_ZERO, ENM_MAX_NAME_LEN);
    STRNCPY (pHttp->au1Name, "Mid_Route_Threshold",
             (sizeof (pHttp->au1Name) - MPLS_L3VPN_ONE));

    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1MidRouteThreshold, pHttp->au1Value,
             (sizeof (au1MidRouteThreshold) - MPLS_L3VPN_ONE));

    MEMSET (pHttp->au1Name, MPLS_L3VPN_ZERO, sizeof (pHttp->au1Name));
    MEMSET (pHttp->au1Value, MPLS_L3VPN_ZERO, ENM_MAX_NAME_LEN);
    STRNCPY (pHttp->au1Name, "Vrf_Description",
             (sizeof (pHttp->au1Name) - MPLS_L3VPN_ONE));

    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1VrfDescription, pHttp->au1Value,
             (sizeof (au1VrfDescription) - MPLS_L3VPN_ONE));

    MEMSET (pHttp->au1Name, MPLS_L3VPN_ZERO, sizeof (pHttp->au1Name));
    MEMSET (pHttp->au1Value, MPLS_L3VPN_ZERO, ENM_MAX_NAME_LEN);
    STRCPY (pHttp->au1Name, "ACTION");

    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    pu1Rd = au1RdTemp;

    if (STRCMP (pHttp->au1Value, "Add") == 0)
    {
        i4RetVal = IssMplsCreateL3vpn (pHttp,
                                       &au1VrfName[MPLS_L3VPN_ZERO],
                                       &pu1Rd[MPLS_L3VPN_ZERO],
                                       pu1RtL3vpn,
                                       au1RtPolicy,
                                       au1NumberOfRoutes,
                                       au1HighRouteThreshold,
                                       au1MidRouteThreshold, au1VrfDescription);
    }
    else if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {
        i4RetVal = IssMplsDeleteL3vpn (pHttp, &au1VrfName[MPLS_L3VPN_ZERO]);
    }

    else if (STRCMP (pHttp->au1Value, "DeleteRT") == 0)
    {
        i4RetVal =
            IssMplsDeleteRtL3vpn (pHttp, &au1RTVrfName[MPLS_L3VPN_ZERO],
                                  au1DeleteRTTemp);
    }

    else if (STRCMP (pHttp->au1Value, "Update") == 0)
    {
        i4RetVal = IssMplsUpdateL3vpn (pHttp,
                                       &au1VrfName[MPLS_L3VPN_ZERO],
                                       pu1RtL3vpn, au1RtPolicy);

    }
    if (i4RetVal == WEB_SUCCESS)
    {
        IssProcessMplsL3vpnGet (pHttp);
    }

}

/*******************************************************************************
*  Function Name : IssMplsDeleteRtL3vpn
*  Description   : This routine delete the RT in L3VPN.
*  Input(s)      : pHttp                 - Pointer to the global HTTP data structure.
*                  pu1RtVrfName          - Pointer to the Vrf name
                   au1DeleteRTTemp           - RT to be deleted
*  Output(s)     : None.
*  Return Values : WEB_SUCCESS or WEB_FAILURE
*******************************************************************************/
INT4
IssMplsDeleteRtL3vpn (tHttp * pHttp, UINT1 *au1RTVrfName,
                      UINT1 au1DeleteRTTemp[L3VPN_MAX_RT_LEN])
{
    tSNMP_OCTET_STRING_TYPE VrfName;
    tL3vpnMplsL3VpnVrfRTEntry *pRTEntry = NULL;
    UINT4               u4ErrorCode = MPLS_L3VPN_ZERO;

    MEMSET (&VrfName, 0, sizeof (VrfName));

    VrfName.pu1_OctetList = au1RTVrfName;
    VrfName.i4_Length = (INT4) STRLEN (au1RTVrfName);
    L3VPN_LOCK;

    pRTEntry = L3VpnGetRTEntryFromValue (au1RTVrfName, au1DeleteRTTemp);
    if (NULL == pRTEntry)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "nmhTestv2MplsL3VpnVrfRTRowStatus fails for deleting RT Row, because get row failed ");
        L3VPN_UNLOCK;
        return WEB_FAILURE;
    }

    if (SNMP_FAILURE ==
        nmhTestv2MplsL3VpnVrfRTRowStatus (&u4ErrorCode,
                                          &VrfName,
                                          pRTEntry->MibObject.
                                          u4MplsL3VpnVrfRTIndex,
                                          L3VPN_P_RT_TYPE (pRTEntry),
                                          ISS_DESTROY))
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "nmhTestv2MplsL3VpnVrfRTRowStatus fails for deleting RT Row ");
        L3VPN_UNLOCK;
        return WEB_FAILURE;
    }

    if (SNMP_FAILURE ==
        nmhSetMplsL3VpnVrfRTRowStatus (&VrfName,
                                       pRTEntry->MibObject.
                                       u4MplsL3VpnVrfRTIndex,
                                       L3VPN_P_RT_TYPE (pRTEntry), ISS_DESTROY))
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "nmhSetMplsL3VpnVrfRTRowStatus fails for deleting RT Row ");
        L3VPN_UNLOCK;
        return WEB_FAILURE;
    }
    L3VPN_UNLOCK;
    return WEB_SUCCESS;
}
#endif

/*******************************************************************************
*  Function Name : IssMplsCreateL3vpn
*  Description   : This routine Creates V3VPN.
*  Input(s)      : pHttp                 - Pointer to the global HTTP data structure.
*                  pu1VrfName            - Pointer to the Vrf name
*                  pu1Rd                 - Pointer to the Rd
*                  pu1Rt                 - Pointer to the Rt
*                  pu1RtPolicy           - Pointer to the Rt Policy
*                  pu1NumberOfRoutes     - Pointer to the number of Routes
*                  pu1HighRouteThreshold - Pointer to the High Route Threshold
*                  pu1MidRouteThreshold  - Pointer to the Mid Route Threshold
*                  pu1VrfDescription     - Pointer to the Vrf Description
*  Output(s)     : None.
*  Return Values : WEB_SUCCESS or WEB_FAILURE
*******************************************************************************/
#ifdef MPLS_L3VPN_WANTED
tSNMP_OCTET_STRING_TYPE Rt[128];
INT4
IssMplsCreateL3vpn (tHttp * pHttp, UINT1 *pu1VrfName, UINT1 *pu1Rd,
                    UINT1 **pu1Rt, UINT1 pu1RtPolicy[][L3VPN_MAX_VPLS_RT_LEN],
                    UINT1 *pu1NumberOfRoutes, UINT1 *pu1HighRouteThreshold,
                    UINT1 *pu1MidRouteThreshold, UINT1 *pu1VrfDescription)
{
    INT1                i1RetValue = SNMP_FAILURE;
    UINT4               u4ErrorCode = MPLS_L3VPN_ZERO;
    UINT4               u4MplsL3VpnVrfRTIndex = MPLS_L3VPN_ZERO;
    UINT4               u4ContextId = 0;
    INT4                i4MplsL3VpnVrfRTType;
    UINT4               u4RtCount = 0;

    tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
    tSNMP_OCTET_STRING_TYPE Rd;
    tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfDescription;

    INT4                i4RetValMplsL3VpnVrfConfRowStatus = MPLS_L3VPN_ZERO;
    L3VPN_LOCK;
    if (NULL == pu1VrfName)
    {
        IssSendError (pHttp,
                      (CONST INT1 *) "Vrf name can not be NULL or ZERO. ");
        L3VPN_UNLOCK;
        return WEB_FAILURE;
    }

    MplsL3VpnVrfName.pu1_OctetList = pu1VrfName;
    MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (pu1VrfName);

    MplsL3VpnVrfDescription.pu1_OctetList = pu1VrfDescription;
    MplsL3VpnVrfDescription.i4_Length = (INT4) STRLEN (pu1VrfDescription);

    Rd.pu1_OctetList = pu1Rd;
    Rd.i4_Length = L3VPN_MAX_RD_LEN;

    if (VcmIsVrfExist (pu1VrfName, &u4ContextId) == VCM_FALSE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "VcmIsVrfExist fails for determining the Context Id. ");
        L3VPN_UNLOCK;
        return WEB_FAILURE;
    }

    i1RetValue =
        nmhGetMplsL3VpnVrfConfRowStatus (&MplsL3VpnVrfName,
                                         &i4RetValMplsL3VpnVrfConfRowStatus);
    if (SNMP_SUCCESS == i1RetValue)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "nmhGetMplsL3VpnVrfConfRowStatus fails for VRF Configuration Table ");
        L3VPN_UNLOCK;
        return WEB_FAILURE;
    }

    i1RetValue =
        nmhTestv2MplsL3VpnVrfConfRowStatus (&u4ErrorCode,
                                            &MplsL3VpnVrfName,
                                            ISS_CREATE_AND_WAIT);
    if (SNMP_FAILURE == i1RetValue)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "nmhTestv2MplsL3VpnVrfConfRowStatus fails for VRF Configuration Table ");
        L3VPN_UNLOCK;
        return WEB_FAILURE;
    }

    i1RetValue =
        nmhSetMplsL3VpnVrfConfRowStatus (&MplsL3VpnVrfName,
                                         ISS_CREATE_AND_WAIT);
    if (SNMP_FAILURE == i1RetValue)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Create & wait fails for VRF Configuration Table ");
        L3VPN_UNLOCK;
        return WEB_FAILURE;
    }

    i1RetValue = nmhTestv2MplsL3VpnVrfRD (&u4ErrorCode, &MplsL3VpnVrfName, &Rd);
    if (SNMP_FAILURE == i1RetValue)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "nmhTestv2MplsL3VpnVrfRD functions fails. ");
        L3VPN_UNLOCK;
        return WEB_FAILURE;
    }

    i1RetValue = nmhSetMplsL3VpnVrfRD (&MplsL3VpnVrfName, &Rd);
    if (SNMP_FAILURE == i1RetValue)
    {
        IssSendError (pHttp,
                      (CONST INT1 *) "nmhSetMplsL3VpnVrfRD functions fails. ");
        L3VPN_UNLOCK;
        return WEB_FAILURE;
    }

    i1RetValue =
        nmhSetMplsL3VpnVrfConfRowStatus (&MplsL3VpnVrfName, ISS_ACTIVE);
    if (SNMP_FAILURE == i1RetValue)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "nmhSetMplsL3VpnVrfConfRowStatus function fails for ISS_ACTIVE.");
        L3VPN_UNLOCK;
        return WEB_FAILURE;
    }

    i1RetValue =
        nmhTestv2MplsL3VpnVrfConfMaxRoutes (&u4ErrorCode,
                                            &MplsL3VpnVrfName,
                                            (UINT4) (ATOI
                                                     ((const CHR1 *)
                                                      pu1NumberOfRoutes)));
    if (SNMP_FAILURE == i1RetValue)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "nmhTestv2MplsL3VpnVrfConfMaxRoutes functions fails. ");
        L3VPN_UNLOCK;
        return WEB_FAILURE;
    }

    i1RetValue =
        nmhSetMplsL3VpnVrfConfMaxRoutes (&MplsL3VpnVrfName,
                                         (UINT4) (ATOI
                                                  ((const CHR1 *)
                                                   pu1NumberOfRoutes)));
    if (SNMP_FAILURE == i1RetValue)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "nmhSetMplsL3VpnVrfConfMaxRoutes functions fails. ");
        L3VPN_UNLOCK;
        return WEB_FAILURE;
    }

    i1RetValue =
        nmhTestv2MplsL3VpnVrfConfHighRteThresh (&u4ErrorCode,
                                                &MplsL3VpnVrfName,
                                                (UINT4) (ATOI
                                                         ((const CHR1 *)
                                                          pu1HighRouteThreshold)));
    if (SNMP_FAILURE == i1RetValue)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "nmhTestv2MplsL3VpnVrfConfHighRteThresh functions fails. ");
        L3VPN_UNLOCK;
        return WEB_FAILURE;
    }

    i1RetValue =
        nmhSetMplsL3VpnVrfConfHighRteThresh (&MplsL3VpnVrfName,
                                             (UINT4) (ATOI
                                                      ((const CHR1 *)
                                                       pu1HighRouteThreshold)));
    if (SNMP_FAILURE == i1RetValue)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "nmhSetMplsL3VpnVrfConfHighRteThresh functions fails. ");
        L3VPN_UNLOCK;
        return WEB_FAILURE;
    }

    i1RetValue =
        nmhTestv2MplsL3VpnVrfConfMidRteThresh (&u4ErrorCode,
                                               &MplsL3VpnVrfName,
                                               (UINT4) (ATOI
                                                        ((const CHR1 *)
                                                         pu1MidRouteThreshold)));
    if (SNMP_FAILURE == i1RetValue)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "nmhTestv2MplsL3VpnVrfConfMidRteThresh functions fails. ");
        L3VPN_UNLOCK;
        return WEB_FAILURE;
    }

    i1RetValue =
        nmhSetMplsL3VpnVrfConfMidRteThresh (&MplsL3VpnVrfName,
                                            (UINT4) (ATOI
                                                     ((const CHR1 *)
                                                      pu1MidRouteThreshold)));
    if (SNMP_FAILURE == i1RetValue)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "nmhSetMplsL3VpnVrfConfMidRteThresh functions fails. ");
        L3VPN_UNLOCK;
        return WEB_FAILURE;
    }

    i1RetValue =
        nmhTestv2MplsL3VpnVrfDescription (&u4ErrorCode,
                                          &MplsL3VpnVrfName,
                                          &MplsL3VpnVrfDescription);
    if (SNMP_FAILURE == i1RetValue)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "nmhTestv2MplsL3VpnVrfDescription functions fails. ");
        L3VPN_UNLOCK;
        return WEB_FAILURE;
    }

    i1RetValue =
        nmhSetMplsL3VpnVrfDescription (&MplsL3VpnVrfName,
                                       &MplsL3VpnVrfDescription);
    if (SNMP_FAILURE == i1RetValue)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "nmhSetMplsL3VpnVrfDescription functions fails. ");
        L3VPN_UNLOCK;
        return WEB_FAILURE;
    }

    while ((u4RtCount < 128) && (pu1Rt[u4RtCount] != NULL))
    {
        if (L3VpnUtilFindNextFreeRtIndex (u4ContextId,
                                          &u4MplsL3VpnVrfRTIndex)
            == OSIX_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Error in getting Free RT Index. ");
            L3VPN_UNLOCK;
            return WEB_FAILURE;
        }
        if (STRCMP (pu1RtPolicy[u4RtCount], "Import") == MPLS_L3VPN_ZERO)
        {
            i4MplsL3VpnVrfRTType = MPLS_L3VPN_RT_IMPORT;
        }
        else if (STRCMP (pu1RtPolicy[u4RtCount], "Export") == MPLS_L3VPN_ZERO)
        {
            i4MplsL3VpnVrfRTType = MPLS_L3VPN_RT_EXPORT;
        }
        else
        {
            i4MplsL3VpnVrfRTType = MPLS_L3VPN_RT_BOTH;
        }

        i1RetValue =
            nmhTestv2MplsL3VpnVrfRTRowStatus (&u4ErrorCode,
                                              &MplsL3VpnVrfName,
                                              u4MplsL3VpnVrfRTIndex,
                                              i4MplsL3VpnVrfRTType,
                                              ISS_CREATE_AND_WAIT);
        if (SNMP_FAILURE == i1RetValue)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "nmhTestv2MplsL3VpnVrfRTRowStatus fails for create & wait. ");
            L3VPN_UNLOCK;
            return WEB_FAILURE;
        }

        i1RetValue =
            nmhSetMplsL3VpnVrfRTRowStatus (&MplsL3VpnVrfName,
                                           u4MplsL3VpnVrfRTIndex,
                                           i4MplsL3VpnVrfRTType,
                                           ISS_CREATE_AND_WAIT);

        if (i1RetValue == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "nmhSetMplsL3VpnVrfRTRowStatus fails for create & wait. ");
            L3VPN_UNLOCK;
            return WEB_FAILURE;
        }

        Rt[u4RtCount].pu1_OctetList = pu1Rt[u4RtCount];
        Rt[u4RtCount].i4_Length = L3VPN_MAX_RT_LEN;

        i1RetValue =
            nmhTestv2MplsL3VpnVrfRT (&u4ErrorCode,
                                     &MplsL3VpnVrfName,
                                     u4MplsL3VpnVrfRTIndex,
                                     i4MplsL3VpnVrfRTType, &Rt[u4RtCount]);
        if (SNMP_FAILURE == i1RetValue)
        {
            nmhSetMplsL3VpnVrfRTRowStatus (&MplsL3VpnVrfName,
                                           u4MplsL3VpnVrfRTIndex,
                                           i4MplsL3VpnVrfRTType, ISS_DESTROY);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "nmhTestv2MplsL3VpnVrfRT functions fails. ");
            L3VPN_UNLOCK;
            return WEB_FAILURE;
        }

        i1RetValue =
            nmhSetMplsL3VpnVrfRT (&MplsL3VpnVrfName,
                                  u4MplsL3VpnVrfRTIndex,
                                  i4MplsL3VpnVrfRTType, &Rt[u4RtCount]);
        if (SNMP_FAILURE == i1RetValue)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "nmhSetMplsL3VpnVrfRT functions fails. ");
            L3VPN_UNLOCK;
            return WEB_FAILURE;
        }

        i1RetValue =
            nmhSetMplsL3VpnVrfRTRowStatus (&MplsL3VpnVrfName,
                                           u4MplsL3VpnVrfRTIndex,
                                           i4MplsL3VpnVrfRTType, ISS_ACTIVE);
        if (SNMP_FAILURE == i1RetValue)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "nmhSetMplsL3VpnVrfRTRowStatus fails for active. ");
            L3VPN_UNLOCK;
            return WEB_FAILURE;
        }

        u4RtCount++;
    }
    L3VPN_UNLOCK;
    return WEB_SUCCESS;
}

/*******************************************************************************
*  Function Name : IssMplsUpdateL3vpn
*  Description   : This routine Updates LV3VPN.
*  Input(s)      : pHttp                 - Pointer to the global HTTP data structure.
*                  pu1VrfName            - Pointer to the Vrf name
*                  pu1Rt                 - Pointer to the Rt
*                  pu1RtPolicy           - Pointer to the Rt Policy
*  Output(s)     : None.
*  Return Values : WEB_SUCCESS or WEB_FAILURE
*******************************************************************************/

INT4
IssMplsUpdateL3vpn (tHttp * pHttp,
                    UINT1 *pu1VrfName,
                    UINT1 **pu1Rt, UINT1 pu1RtPolicy[][L3VPN_MAX_VPLS_RT_LEN])
{
    INT1                i1RetValue = SNMP_FAILURE;
    UINT4               u4ErrorCode = MPLS_L3VPN_ZERO;
    UINT4               u4MplsL3VpnVrfRTIndex = MPLS_L3VPN_ZERO;
    UINT4               u4ContextId = 0;
    INT4                i4MplsL3VpnVrfRTType = 0;
    UINT4               u4RtCount = 0;

    tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
    tSNMP_OCTET_STRING_TYPE tempRt;

    MEMSET (&MplsL3VpnVrfName, 0, sizeof (MplsL3VpnVrfName));
    MEMSET (&tempRt, 0, sizeof (tempRt));

    MplsL3VpnVrfName.pu1_OctetList = pu1VrfName;
    MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (pu1VrfName);
    L3VPN_LOCK;
    if (VcmIsVrfExist (pu1VrfName, &u4ContextId) == VCM_FALSE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "VcmIsVrfExist fails for determining the Context Id. ");
        L3VPN_UNLOCK;
        return WEB_FAILURE;
    }
    /*This loop is used to create new RTs */
    while (pu1Rt[u4RtCount] != NULL)
    {
        if (L3VpnUtilFindNextFreeRtIndex (u4ContextId,
                                          &u4MplsL3VpnVrfRTIndex)
            == OSIX_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Error in getting Free RT Index. ");
            L3VPN_UNLOCK;
            return WEB_FAILURE;
        }
        if (STRCMP (pu1RtPolicy[u4RtCount], "Import") == MPLS_L3VPN_ZERO)
        {
            i4MplsL3VpnVrfRTType = MPLS_L3VPN_RT_IMPORT;
        }
        else if (STRCMP (pu1RtPolicy[u4RtCount], "Export") == MPLS_L3VPN_ZERO)
        {
            i4MplsL3VpnVrfRTType = MPLS_L3VPN_RT_EXPORT;
        }
        else
        {
            i4MplsL3VpnVrfRTType = MPLS_L3VPN_RT_BOTH;
        }

        i1RetValue =
            nmhTestv2MplsL3VpnVrfRTRowStatus (&u4ErrorCode,
                                              &MplsL3VpnVrfName,
                                              u4MplsL3VpnVrfRTIndex,
                                              i4MplsL3VpnVrfRTType,
                                              ISS_CREATE_AND_WAIT);
        if (SNMP_FAILURE == i1RetValue)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "nmhTestv2MplsL3VpnVrfRTRowStatus fails for create & wait. ");
            L3VPN_UNLOCK;
            return WEB_FAILURE;
        }

        i1RetValue =
            nmhSetMplsL3VpnVrfRTRowStatus (&MplsL3VpnVrfName,
                                           u4MplsL3VpnVrfRTIndex,
                                           i4MplsL3VpnVrfRTType,
                                           ISS_CREATE_AND_WAIT);
        if (i1RetValue == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "nmhSetMplsL3VpnVrfRTRowStatus fails for create & wait. ");
            L3VPN_UNLOCK;
            return WEB_FAILURE;
        }

        tempRt.pu1_OctetList = pu1Rt[u4RtCount];
        tempRt.i4_Length = L3VPN_MAX_RT_LEN;

        i1RetValue =
            nmhTestv2MplsL3VpnVrfRT (&u4ErrorCode,
                                     &MplsL3VpnVrfName,
                                     u4MplsL3VpnVrfRTIndex,
                                     i4MplsL3VpnVrfRTType, &tempRt);
        if (SNMP_FAILURE == i1RetValue)
        {
            nmhSetMplsL3VpnVrfRTRowStatus (&MplsL3VpnVrfName,
                                           u4MplsL3VpnVrfRTIndex,
                                           i4MplsL3VpnVrfRTType, ISS_DESTROY);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "nmhTestv2MplsL3VpnVrfRT functions fails. ");
            L3VPN_UNLOCK;
            return WEB_FAILURE;
        }

        i1RetValue = nmhSetMplsL3VpnVrfRT (&MplsL3VpnVrfName,
                                           u4MplsL3VpnVrfRTIndex,
                                           i4MplsL3VpnVrfRTType, &tempRt);
        if (SNMP_FAILURE == i1RetValue)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "nmhSetMplsL3VpnVrfRT functions fails. ");
            L3VPN_UNLOCK;
            return WEB_FAILURE;
        }

        i1RetValue =
            nmhSetMplsL3VpnVrfRTRowStatus (&MplsL3VpnVrfName,
                                           u4MplsL3VpnVrfRTIndex,
                                           i4MplsL3VpnVrfRTType, ISS_ACTIVE);
        if (SNMP_FAILURE == i1RetValue)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "nmhSetMplsL3VpnVrfRTRowStatus fails for active. ");
            L3VPN_UNLOCK;
            return WEB_FAILURE;
        }

        u4RtCount++;
    }
    L3VPN_UNLOCK;
    return WEB_SUCCESS;

}

/*******************************************************************************
*  Function Name : IssMplsDeleteL3vpn
*  Description   : This routine Deletes V3VPN.
*  Input(s)      : pHttp                 - Pointer to the global HTTP data structure.
*                  pu1VrfName            - Pointer to the Vrf name
*  Output(s)     : None.
*  Return Values : WEB_SUCCESS or WEB_FAILURE
*******************************************************************************/
INT4
IssMplsDeleteL3vpn (tHttp * pHttp, UINT1 *pu1VrfName)
{
    INT1                i1RetValue = SNMP_FAILURE;
    UINT4               u4ErrorCode = MPLS_L3VPN_ZERO;
    tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;

    MplsL3VpnVrfName.pu1_OctetList = pu1VrfName;
    MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (pu1VrfName);
    L3VPN_LOCK;
    i1RetValue =
        nmhTestv2MplsL3VpnVrfConfRowStatus (&u4ErrorCode,
                                            &MplsL3VpnVrfName, ISS_DESTROY);
    if (SNMP_FAILURE == i1RetValue)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "nmhTestv2MplsL3VpnVrfConfRowStatus fails for VRF Configuration Table ");
        L3VPN_UNLOCK;
        return WEB_FAILURE;
    }

    i1RetValue =
        nmhSetMplsL3VpnVrfConfRowStatus (&MplsL3VpnVrfName, ISS_DESTROY);
    if (SNMP_FAILURE == i1RetValue)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Destroy fails for VRF Configuration Table ");
        L3VPN_UNLOCK;
        return WEB_FAILURE;
    }
    L3VPN_UNLOCK;
    return WEB_SUCCESS;
}
#endif
#endif
