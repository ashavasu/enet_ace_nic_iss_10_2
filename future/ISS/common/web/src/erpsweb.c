/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: erpsweb.c,v 1.17 2016/01/11 13:15:12 siva Exp $
 *
 * Description: This file contains the routines required for ERPS
 *              web module.
 *******************************************************************/

#ifndef _ERPS_WEB_C_
#define _ERPS_WEB_C_

#ifdef WEBNM_WANTED

#include "webiss.h"
#include "isshttp.h"
#include "vcm.h"
#include "ecfm.h"
#include "erps.h"
#include "erpsweb.h"

/*********************************************************************
 * *  Function Name : IssProcessErpsRingTablePage
 * *  Description   : This function processes the request coming for the
 * *                  Ring Status page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/
VOID
IssProcessErpsRingTablePage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessErpsRingTablePageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessErpsRingTablePageSet (pHttp);
    }
}

/*********************************************************************
 * *  Function Name : IssProcessErpsRingStatusPage
 * *  Description   : This function processes the request coming for the
 * *                  Ring Status page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/
VOID
IssProcessErpsRingStatusPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessErpsRingStatusPageGet (pHttp);
    }
}

/*********************************************************************
 * *  Function Name : IssProcessErpsRingTcPropPage
 * *  Description   : This function processes the request coming for the
 * *                  Ring TC Propagation Table.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/
VOID
IssProcessErpsRingTcPropPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessErpsRingTcPropPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessErpsRingTcPropPageSet (pHttp);
    }
}

/*********************************************************************
 * *  Function Name : IssProcessErpsRingVlanPage
 * *  Description   : This function processes the request coming for the
 * *                  Ring Vlan Group Table.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/
VOID
IssProcessErpsRingVlanPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessErpsRingVlanPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessErpsRingVlanPageSet (pHttp);
    }

}

/*********************************************************************
 * *  Function Name : IssProcessErpsRingTablePageGet
 * *  Description   : This function processes the get request coming for the
 * *                  ERPS Ring Configuration.
 * *:
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/
VOID
IssProcessErpsRingTablePageGet (tHttp * pHttp)
{
    INT4                i4OutCome = 0;
    INT4                i4DispCnt = 0;
    INT4                i4RingPort1 = -1;
    INT4                i4RingPort2 = -1;
    INT4                i4RingVlanId = -1;
    INT4                i4RplPort = -1;
    INT4                i4RplNeighbourPort = -1;
    INT4                i4RplNextNeighbourPort = -1;
    INT4                i4OperatingMode = -1;
    INT4                i4RecoveryMethod = -1;
    INT4                i4SwitchCommand = -1;
    INT4                i4SwitchPort = -1;
    INT4                i4RingStatus = -1;
    INT4                i4VCRecovery = -1;
    INT4                i4CompatibleVersion = -1;
    INT4                i4ProtectionType = -1;
    INT4                i4WithoutVC = 1;
    INT4                i4ProtectedVlanId = 0;
    INT4                i4RingMacId = -1;
    INT4                i4RingClear = -1;
    UINT4               u4Temp = 0;
    UINT4               u4FsErpsRingId = 0;
    UINT4               u4FsErpsRingIdNext = 0;
    UINT4               u4FsErpsContextId = 0;
    UINT4               u4FsErpsContextIdNext = 0;
    UINT1               au1RingName[ERPS_MAX_NAME_LENGTH];
    tSNMP_OCTET_STRING_TYPE RingName;
    INT1                ai1IfNamePort[CFA_MAX_PORT_NAME_LENGTH];
    MEMSET (ai1IfNamePort, 0, sizeof (ai1IfNamePort));

    MEMSET (au1RingName, 0, sizeof (au1RingName));
    RingName.pu1_OctetList = au1RingName;
    RingName.i4_Length = (INT4) STRLEN (au1RingName);

    pHttp->i4Write = 0;

    WebnmRegisterLock (pHttp, ErpsApiLock, ErpsApiUnLock);
    ERPS_LOCK ();

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    i4OutCome =
        (INT4) (nmhGetFirstIndexFsErpsRingTable
                (&u4FsErpsContextId, &u4FsErpsRingId));

    if (i4OutCome == SNMP_FAILURE)
    {
        ERPS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;

    u4FsErpsContextIdNext = u4FsErpsContextId;
    u4FsErpsRingIdNext = u4FsErpsRingId;

    do
    {
        u4FsErpsContextId = u4FsErpsContextIdNext;
        u4FsErpsRingId = u4FsErpsRingIdNext;

        pHttp->i4Write = (INT4) u4Temp;

        /* To Display the Context ID */
        STRCPY (pHttp->au1KeyString, "fsErpsContextId_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4FsErpsContextId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* To Display the Ring ID */
        STRCPY (pHttp->au1KeyString, "fsErpsRingId_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4FsErpsRingId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* To Display the Ring Name */
        STRCPY (pHttp->au1KeyString, "fsErpsRingName_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsErpsRingName (u4FsErpsContextId, u4FsErpsRingId, &RingName);

        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", RingName.pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* To Display the Ring Port1 */
        STRCPY (pHttp->au1KeyString, "fsErpsRingPort1_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsErpsRingPort1 (u4FsErpsContextId, u4FsErpsRingId, &i4RingPort1);
        if (i4RingPort1 != 0)
        {
            MEMSET (ai1IfNamePort, 0, sizeof (ai1IfNamePort));
            CfaCliGetIfName ((UINT4) i4RingPort1, ai1IfNamePort);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     (CHR1 *) ai1IfNamePort);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "None");
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* To Display the Ring Port2 */
        STRCPY (pHttp->au1KeyString, "fsErpsRingPort2_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsErpsRingPort2 (u4FsErpsContextId, u4FsErpsRingId, &i4RingPort2);
        if (i4RingPort2 != 0)
        {
            MEMSET (ai1IfNamePort, 0, sizeof (ai1IfNamePort));
            CfaCliGetIfName ((UINT4) i4RingPort2, ai1IfNamePort);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     (CHR1 *) ai1IfNamePort);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "None");
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* To Display the Ring VLAN */
        STRCPY (pHttp->au1KeyString, "fsErpsRingVlanId_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsErpsRingVlanId (u4FsErpsContextId, u4FsErpsRingId,
                                &i4RingVlanId);
        if (i4RingVlanId != 0)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RingVlanId);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "None");
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /*To Display the Compatible version */
        STRCPY (pHttp->au1KeyString, "fsErpsRingRAPSCompatibleVersion_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsErpsRingRAPSCompatibleVersion (u4FsErpsContextId,
                                               u4FsErpsRingId,
                                               &i4CompatibleVersion);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4CompatibleVersion);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* To Display the RPL Port */
        STRCPY (pHttp->au1KeyString, "fsErpsRingRplPort_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsErpsRingRplPort (u4FsErpsContextId, u4FsErpsRingId, &i4RplPort);
        if (i4RplPort != 0)
        {
            MEMSET (ai1IfNamePort, 0, sizeof (ai1IfNamePort));
            CfaCliGetIfName ((UINT4) i4RplPort, ai1IfNamePort);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     (CHR1 *) ai1IfNamePort);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "None");
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* To Display the RPLNeighbour Port */
        STRCPY (pHttp->au1KeyString, "fsErpsRingRplNeighbourPort_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4CompatibleVersion == ERPS_RING_COMPATIBLE_VERSION2)
        {
            nmhGetFsErpsRingRplNeighbourPort (u4FsErpsContextId, u4FsErpsRingId,
                                              &i4RplNeighbourPort);
            if (i4RplNeighbourPort != 0)
            {
                MEMSET (ai1IfNamePort, 0, sizeof (ai1IfNamePort));
                CfaCliGetIfName ((UINT4) i4RplNeighbourPort, ai1IfNamePort);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         (CHR1 *) ai1IfNamePort);
            }
            else
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "None");
            }
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "None");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "fsErpsRingRplNextNeighbourPort_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4CompatibleVersion == ERPS_RING_COMPATIBLE_VERSION2)
        {
            nmhGetFsErpsRingRplNextNeighbourPort (u4FsErpsContextId,
                                                  u4FsErpsRingId,
                                                  &i4RplNextNeighbourPort);
            if (i4RplNextNeighbourPort != 0)
            {
                MEMSET (ai1IfNamePort, 0, sizeof (ai1IfNamePort));
                CfaCliGetIfName ((UINT4) i4RplNextNeighbourPort, ai1IfNamePort);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         (CHR1 *) ai1IfNamePort);
            }
            else
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "None");
            }
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "None");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* To Display the Operating Mode */
        STRCPY (pHttp->au1KeyString, "fsErpsRingOperatingMode_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsErpsRingOperatingMode (u4FsErpsContextId, u4FsErpsRingId,
                                       &i4OperatingMode);

        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4OperatingMode);

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* To Display the Recovery Method */
        STRCPY (pHttp->au1KeyString, "fsErpsRingConfigRecoveryMethod_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsErpsRingConfigRecoveryMethod (u4FsErpsContextId, u4FsErpsRingId,
                                              &i4RecoveryMethod);

        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RecoveryMethod);

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* To Display the Switch Command */
        STRCPY (pHttp->au1KeyString, "fsErpsRingConfigSwitchCmd_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsErpsRingConfigSwitchCmd (u4FsErpsContextId, u4FsErpsRingId,
                                         &i4SwitchCommand);

        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4SwitchCommand);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* To Display the Switch Port */
        STRCPY (pHttp->au1KeyString, "fsErpsRingConfigSwitchPort_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsErpsRingConfigSwitchPort (u4FsErpsContextId, u4FsErpsRingId,
                                          &i4SwitchPort);
        if (i4SwitchPort != 0)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4SwitchPort);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "None");
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* To Display the VC Recovery */
        STRCPY (pHttp->au1KeyString, "fsErpsRingPortBlockingOnVcRecovery_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsErpsRingPortBlockingOnVcRecovery (u4FsErpsContextId,
                                                  u4FsErpsRingId,
                                                  &i4VCRecovery);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4VCRecovery);

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* To Display the Ptrotected Ring VLAN */
        STRCPY (pHttp->au1KeyString, "fsErpsRingProtectedVlanGroupId_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsErpsRingProtectedVlanGroupId (u4FsErpsContextId, u4FsErpsRingId,
                                              &i4ProtectedVlanId);
        if (i4ProtectedVlanId != 0)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4ProtectedVlanId);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "None");
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /*To Display the protection Typs of the ring */
        STRCPY (pHttp->au1KeyString, "fsErpsRingProtectionType_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsErpsRingProtectionType (u4FsErpsContextId, u4FsErpsRingId,
                                        &i4ProtectionType);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4ProtectionType);

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /*To Display the ring with/without virtual channel */
        STRCPY (pHttp->au1KeyString, "fsErpsRingRAPSSubRingWithoutVC_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsErpsRingRAPSSubRingWithoutVC (u4FsErpsContextId, u4FsErpsRingId,
                                              &i4WithoutVC);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4WithoutVC);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /*To Display the clerar command */
        STRCPY (pHttp->au1KeyString, "fsErpsRingConfigClear_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4CompatibleVersion != ERPS_RING_COMPATIBLE_VERSION2)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Not-Supported");
        }
        nmhGetFsErpsRingConfigClear (u4FsErpsContextId, u4FsErpsRingId,
                                     &i4RingClear);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RingClear);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* To Display the Ring mac id */
        STRCPY (pHttp->au1KeyString, "fsErpsRingmacId_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsErpsRingMacId (u4FsErpsContextId, u4FsErpsRingId, &i4RingMacId);
        if (i4RingMacId != 0)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RingMacId);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "None");
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* To Display the Ring Status */
        STRCPY (pHttp->au1KeyString, "fsErpsRingRowStatus_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsErpsRingRowStatus (u4FsErpsContextId, u4FsErpsRingId,
                                   &i4RingStatus);
        /* To display the row status as In-Active to the user */
        if (i4RingStatus == NOT_READY)
        {
            i4RingStatus = NOT_IN_SERVICE;
        }
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RingStatus);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        i4DispCnt++;

    }
    while (nmhGetNextIndexFsErpsRingTable
           (u4FsErpsContextId, &u4FsErpsContextIdNext, u4FsErpsRingId,
            &u4FsErpsRingIdNext) == SNMP_SUCCESS);

    if (i4DispCnt == 0)
    {
        ERPS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    ERPS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
 * *  Function Name : IssProcessErpsRingTablePageSet
 * *  Description   : This function processes the set request coming for the
 * *                  ERPS Ring Configuration.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/
VOID
IssProcessErpsRingTablePageSet (tHttp * pHttp)
{

    INT4                i4RingPort1 = -1;
    INT4                i4RingPort2 = -1;
    INT4                i4RingVlanId = -1;
    INT4                i4RplPort = 0;
    INT4                i4OperatingMode = ERPS_RING_REVERTIVE_MODE;
    INT4                i4RecoveryMethod = ERPS_RING_AUTO_RECOVERY;
    INT4                i4SwitchPort = 0;
    INT4                i4SwitchCmd = ERPS_SWITCH_COMMAND_NONE;
    INT4                i4RingStatus = -1;
    INT4                i4RowStatus = -1;
    INT4                i4VCRecovery = -1;
    INT4                i4RplNeighbourPort = -1;
    INT4                i4RplNextNeighbourPort = -1;
    INT4                i4ProtectionType = -1;
    INT4                i4ProtectedVlanId = 0;
    INT4                i4CompatibleVersion = -1;
    INT4                i4WithoutVC = -1;
    INT4                i4RingMacid = 0;
    INT4                i4RingClear = -1;
    INT4                i4RowStatusValue = 0;
    INT4                i4ErpsMonitoringMech = 0;
    UINT4               u4ErpsRingPwVcId1 = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4FsErpsRingId = 0;
    UINT4               u4FsErpsContextId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4IfIndex;

    STRCPY (pHttp->au1Name, "fsErpsContextId");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4FsErpsContextId = (UINT4) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsErpsRingId");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4FsErpsRingId = (UINT4) ATOI (pHttp->au1Value);

    ErpsApiLock ();

    HttpGetValuebyName (ISS_ACTION, pHttp->au1Value, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Value, "delete") == 0)
    {
        if (nmhGetFsErpsRingRowStatus
            (u4FsErpsContextId, u4FsErpsRingId, &i4RowStatus) != SNMP_SUCCESS)
        {
            IssSendError (pHttp, (CONST INT1 *) "Ring Entry is not present");
            return;
        }

        /* Make the ring in-active */
        if (i4RowStatus == ACTIVE)
        {
            if (nmhTestv2FsErpsRingRowStatus (&u4ErrCode, u4FsErpsContextId,
                                              u4FsErpsRingId,
                                              NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                ErpsApiUnLock ();
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to delete the Ring;Delete CFM entries and TC list before deleting the ring");
                return;
            }

            if (nmhSetFsErpsRingRowStatus
                (u4FsErpsContextId, u4FsErpsRingId,
                 NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                ErpsApiUnLock ();
                IssSendError (pHttp,
                              (CONST INT1 *) "Unable to delete the Ring");
                return;
            }

        }

        /* Delete the CFM entries */
        if (nmhTestv2FsErpsRingCfmRowStatus
            (&u4ErrCode, u4FsErpsContextId, u4FsErpsRingId,
             DESTROY) != SNMP_SUCCESS)
        {
            ErpsApiUnLock ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to delete the CFM Entries");
            return;
        }

        if (nmhSetFsErpsRingCfmRowStatus
            (u4FsErpsContextId, u4FsErpsRingId, DESTROY) != SNMP_SUCCESS)
        {
            ErpsApiUnLock ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to delete the CFM Entries");
            return;
        }

        /*Delete the MPLS PW ENTRY*/
        nmhGetFsErpsRingMonitorMechanism(u4FsErpsContextId, u4FsErpsRingId, &i4ErpsMonitoringMech);
        nmhGetFsErpsRingPwVcId1(u4FsErpsContextId, u4FsErpsRingId, &u4ErpsRingPwVcId1);
        if ((i4ErpsMonitoringMech != ERPS_MONITOR_MECH_CFM) && (u4ErpsRingPwVcId1 != 0))
        {
            if (nmhTestv2FsErpsRingMplsRowStatus
                    (&u4ErrorCode, u4FsErpsContextId, u4FsErpsRingId, DESTROY) != SNMP_SUCCESS)
            {
                return ;
            }
            if (nmhSetFsErpsRingMplsRowStatus (u4FsErpsContextId, u4FsErpsRingId, DESTROY)
                    != SNMP_SUCCESS)
            {
                return ;
            }
        } 

        /* To delete a ring entry */
        if (nmhTestv2FsErpsRingRowStatus (&u4ErrCode, u4FsErpsContextId,
                                          u4FsErpsRingId,
                                          DESTROY) == SNMP_FAILURE)
        {
            ErpsApiUnLock ();
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unable to delete the Ring;Delete CFM entries and TC list before deleting the ring");
            return;
        }

        if (nmhSetFsErpsRingRowStatus
            (u4FsErpsContextId, u4FsErpsRingId, DESTROY) == SNMP_FAILURE)
        {
            ErpsApiUnLock ();
            IssSendError (pHttp, (CONST INT1 *) "Unable to delete the Ring");
            return;
        }
        ErpsApiUnLock ();
        IssProcessErpsRingTablePageGet (pHttp);
        return;
    }

    /* Row must be created, only when a ring is added */
    if (STRCMP (pHttp->au1Value, "Add") == 0)
    {
        /* To create a ring entry */
        if (nmhTestv2FsErpsRingRowStatus (&u4ErrCode, u4FsErpsContextId,
                                          u4FsErpsRingId,
                                          CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            ErpsApiUnLock ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set the ERPS Ring Status");
            return;
        }

        if (nmhSetFsErpsRingRowStatus
            (u4FsErpsContextId, u4FsErpsRingId,
             CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            ErpsApiUnLock ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set the ERPS Ring Status");
            return;
        }
    }

    nmhGetFsErpsRingRowStatus (u4FsErpsContextId, u4FsErpsRingId, &i4RowStatus);
    if (STRCMP (pHttp->au1Value, "Apply") == 0)
    {
        i4RowStatusValue = i4RowStatus;
        if (i4RowStatus == ACTIVE)
        {

            if (nmhTestv2FsErpsRingRowStatus (&u4ErrCode, u4FsErpsContextId,
                                              u4FsErpsRingId,
                                              NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                ErpsApiUnLock ();
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to set the ERPS Ring Status");
                return;
            }
            if (nmhSetFsErpsRingRowStatus
                (u4FsErpsContextId, u4FsErpsRingId,
                 NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                ErpsApiUnLock ();
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to set the ERPS Ring Status");
                return;
            }

        }

    }
    /* To set Ring Port1 */

    STRCPY (pHttp->au1Name, "fsErpsRingPort1");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        if (STRCMP (pHttp->au1Value, "None") != 0)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            if (IssGetIfIndex (pHttp->au1Value, &u4IfIndex) == ENM_FAILURE)
            {

                nmhSetFsErpsRingRowStatus
                    (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
                ErpsApiUnLock ();
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Invalid Port;Enter valid ports gi0/1,Ex0/1,Fa0/1");
                return;
            }
            i4RingPort1 = (INT4) u4IfIndex;
        }
        else
        {
            i4RingPort1 = 0;
        }

        if (nmhTestv2FsErpsRingPort1 (&u4ErrCode, u4FsErpsContextId,
                                      u4FsErpsRingId,
                                      i4RingPort1) == SNMP_FAILURE)
        {
            nmhSetFsErpsRingRowStatus
                (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
            ErpsApiUnLock ();
            IssSendError (pHttp, (CONST INT1 *) "Unable to set the Ring Port1");
            return;
        }

        if (nmhSetFsErpsRingPort1
            (u4FsErpsContextId, u4FsErpsRingId, i4RingPort1) == SNMP_FAILURE)
        {
            nmhSetFsErpsRingRowStatus
                (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
            ErpsApiUnLock ();
            IssSendError (pHttp, (CONST INT1 *) "Unable to set the Ring Port1");
            return;
        }
    }
    /* To set Ring Port2 */

    STRCPY (pHttp->au1Name, "fsErpsRingPort2");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        if (STRCMP (pHttp->au1Value, "None") != 0)
        {

            issDecodeSpecialChar (pHttp->au1Value);
            if (IssGetIfIndex (pHttp->au1Value, &u4IfIndex) == ENM_FAILURE)
            {

                nmhSetFsErpsRingRowStatus
                    (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
                ErpsApiUnLock ();
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Invalid Port;Enter valid ports gi0/1,Ex0/1i,Fa0/1");
                return;
            }
            i4RingPort2 = (INT4) u4IfIndex;
        }
        else
        {
            i4RingPort2 = 0;
        }

        if (nmhTestv2FsErpsRingPort2 (&u4ErrCode, u4FsErpsContextId,
                                      u4FsErpsRingId,
                                      i4RingPort2) == SNMP_FAILURE)
        {
            nmhSetFsErpsRingRowStatus
                (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
            ErpsApiUnLock ();
            IssSendError (pHttp, (CONST INT1 *) "Unable to set the Ring Port2");
            return;
        }

        if (nmhSetFsErpsRingPort2
            (u4FsErpsContextId, u4FsErpsRingId, i4RingPort2) == SNMP_FAILURE)
        {
            nmhSetFsErpsRingRowStatus
                (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
            ErpsApiUnLock ();
            IssSendError (pHttp, (CONST INT1 *) "Unable to set the Ring Port2");
            return;
        }
    }

    /* To set Ring VLAN */

    STRCPY (pHttp->au1Name, "fsErpsRingVlanId");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4RingVlanId = ATOI (pHttp->au1Value);

        if (nmhTestv2FsErpsRingVlanId (&u4ErrCode, u4FsErpsContextId,
                                       u4FsErpsRingId,
                                       i4RingVlanId) == SNMP_FAILURE)
        {
            nmhSetFsErpsRingRowStatus
                (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
            ErpsApiUnLock ();
            IssSendError (pHttp, (CONST INT1 *) "Unable to set the Ring VLAN");
            return;
        }

        if (nmhSetFsErpsRingVlanId
            (u4FsErpsContextId, u4FsErpsRingId, i4RingVlanId) == SNMP_FAILURE)
        {
            nmhSetFsErpsRingRowStatus
                (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
            ErpsApiUnLock ();
            IssSendError (pHttp, (CONST INT1 *) "Unable to set the Ring VLAN");
            return;
        }

    }

    /* To set compatible version in ring node */

    STRCPY (pHttp->au1Name, "fsErpsRingRAPSCompatibleVersion");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4CompatibleVersion = ATOI (pHttp->au1Value);
        if (nmhTestv2FsErpsRingRAPSCompatibleVersion
            (&u4ErrCode, u4FsErpsContextId, u4FsErpsRingId,
             i4CompatibleVersion) == SNMP_FAILURE)
        {
            nmhSetFsErpsRingRowStatus
                (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
            ErpsApiUnLock ();
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unable to set the compatible version");
            return;
        }

        if (nmhSetFsErpsRingRAPSCompatibleVersion (u4FsErpsContextId,
                                                   u4FsErpsRingId,
                                                   i4CompatibleVersion) ==
            SNMP_FAILURE)
        {
            nmhSetFsErpsRingRowStatus
                (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
            ErpsApiUnLock ();
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unable to set the compatible version");
            return;
        }
    }

    /* To set Ring RPL Port */

    STRCPY (pHttp->au1Name, "fsErpsRingRplPort");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        if (STRCMP (pHttp->au1Value, "None") != 0)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            if (IssGetIfIndex (pHttp->au1Value, &u4IfIndex) == ENM_FAILURE)
            {

                nmhSetFsErpsRingRowStatus
                    (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
                ErpsApiUnLock ();
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Invalid Port;Enter valid ports gi0/1,Ex0/1,Fa0/1");
                return;
            }
            i4RplPort = (INT4) u4IfIndex;
        }
        else
        {
            i4RplPort = 0;
        }

        if (nmhTestv2FsErpsRingRplPort (&u4ErrCode, u4FsErpsContextId,
                                        u4FsErpsRingId,
                                        i4RplPort) == SNMP_FAILURE)
        {
            nmhSetFsErpsRingRowStatus
                (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
            ErpsApiUnLock ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set the Ring RPL Port");
            return;
        }

        if (nmhSetFsErpsRingRplPort
            (u4FsErpsContextId, u4FsErpsRingId, i4RplPort) == SNMP_FAILURE)
        {
            nmhSetFsErpsRingRowStatus
                (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
            ErpsApiUnLock ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set the Ring Rpl Port");
            return;
        }
    }

    /* To set Ring RPLNeighbour Port */

    STRCPY (pHttp->au1Name, "fsErpsRingRplNeighbourPort");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        if (STRCMP (pHttp->au1Value, "None") != 0)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            if (IssGetIfIndex (pHttp->au1Value, &u4IfIndex) == ENM_FAILURE)
            {

                nmhSetFsErpsRingRowStatus
                    (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
                ErpsApiUnLock ();
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Invalid Port;Enter valid ports gi0/1,Ex0/1,Fa0/1");
                return;
            }
            i4RplNeighbourPort = (INT4) u4IfIndex;
        }
        else
        {
            i4RplNeighbourPort = 0;
        }
        if (i4CompatibleVersion == 2)
        {

            if (nmhTestv2FsErpsRingRplNeighbourPort
                (&u4ErrCode, u4FsErpsContextId, u4FsErpsRingId,
                 i4RplNeighbourPort) == SNMP_FAILURE)
            {
                nmhSetFsErpsRingRowStatus
                    (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
                ErpsApiUnLock ();
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to set the Ring RPLNeighbour Port");
                return;
            }

            if (nmhSetFsErpsRingRplNeighbourPort
                (u4FsErpsContextId, u4FsErpsRingId,
                 i4RplNeighbourPort) == SNMP_FAILURE)
            {
                nmhSetFsErpsRingRowStatus
                    (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
                ErpsApiUnLock ();
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to set the Ring RPLNeighbour Port");
                return;

            }
        }
    }

    /* To set Ring RPLNext Neighbour Port */

    STRCPY (pHttp->au1Name, "fsErpsRingRplNextNeighbourPort");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        if (STRCMP (pHttp->au1Value, "None") != 0)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            if (IssGetIfIndex (pHttp->au1Value, &u4IfIndex) == ENM_FAILURE)
            {

                nmhSetFsErpsRingRowStatus
                    (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
                ErpsApiUnLock ();
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Invalid Port;Enter valid ports gi0/1,Ex0/1,Fa0/1");
                return;
            }
            i4RplNextNeighbourPort = (INT4) u4IfIndex;
        }
        else
        {
            i4RplNextNeighbourPort = 0;
        }
        if (i4CompatibleVersion == 2)
        {

            if (nmhTestv2FsErpsRingRplNextNeighbourPort
                (&u4ErrCode, u4FsErpsContextId, u4FsErpsRingId,
                 i4RplNextNeighbourPort) == SNMP_FAILURE)
            {
                nmhSetFsErpsRingRowStatus
                    (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
                ErpsApiUnLock ();
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to set the Ring RPLNextNeighbour Port");
                return;
            }

            if (nmhSetFsErpsRingRplNextNeighbourPort
                (u4FsErpsContextId, u4FsErpsRingId,
                 i4RplNextNeighbourPort) == SNMP_FAILURE)
            {
                nmhSetFsErpsRingRowStatus
                    (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
                ErpsApiUnLock ();
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to set the Ring RPLNextNeighbour Port");
                return;

            }
        }
    }

    /* To set Ring Operating Mode */

    STRCPY (pHttp->au1Name, "fsErpsRingOperatingMode");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4OperatingMode = ATOI (pHttp->au1Value);

    if (nmhTestv2FsErpsRingOperatingMode (&u4ErrCode, u4FsErpsContextId,
                                          u4FsErpsRingId,
                                          i4OperatingMode) == SNMP_FAILURE)
    {
        nmhSetFsErpsRingRowStatus
            (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
        ErpsApiUnLock ();
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the operating mode");
        return;
    }

    if (nmhSetFsErpsRingOperatingMode
        (u4FsErpsContextId, u4FsErpsRingId, i4OperatingMode) == SNMP_FAILURE)
    {
        nmhSetFsErpsRingRowStatus
            (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
        ErpsApiUnLock ();
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the operating mode");
        return;
    }

    /* To set Ring Recovery Method */

    STRCPY (pHttp->au1Name, "fsErpsRingConfigRecoveryMethod");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4RecoveryMethod = ATOI (pHttp->au1Value);

    if (nmhTestv2FsErpsRingConfigRecoveryMethod (&u4ErrCode, u4FsErpsContextId,
                                                 u4FsErpsRingId,
                                                 i4RecoveryMethod) ==
        SNMP_FAILURE)
    {
        nmhSetFsErpsRingRowStatus
            (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
        ErpsApiUnLock ();
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to set the Recovery Method");
        return;
    }

    if (nmhSetFsErpsRingConfigRecoveryMethod (u4FsErpsContextId,
                                              u4FsErpsRingId,
                                              i4RecoveryMethod) == SNMP_FAILURE)
    {
        nmhSetFsErpsRingRowStatus
            (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
        ErpsApiUnLock ();
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to set the Recovery Method");
        return;
    }

    /* To set Ring Switch Port */

    STRCPY (pHttp->au1Name, "fsErpsRingConfigSwitchPort");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        if (STRCMP (pHttp->au1Value, "None") != 0)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            if (IssGetIfIndex (pHttp->au1Value, &u4IfIndex) == ENM_FAILURE)
            {

                nmhSetFsErpsRingRowStatus
                    (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
                ErpsApiUnLock ();
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Invalid Port;Enter valid ports gi0/1,Ex0/1,Fa0/1");
                return;
            }
            i4SwitchPort = (INT4) u4IfIndex;
        }
        else
        {
            i4SwitchPort = 0;
        }

        if (nmhTestv2FsErpsRingConfigSwitchPort (&u4ErrCode, u4FsErpsContextId,
                                                 u4FsErpsRingId,
                                                 i4SwitchPort) == SNMP_FAILURE)
        {
            nmhSetFsErpsRingRowStatus
                (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
            ErpsApiUnLock ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set the switch port");
            return;
        }

        if (nmhSetFsErpsRingConfigSwitchPort (u4FsErpsContextId,
                                              u4FsErpsRingId,
                                              i4SwitchPort) == SNMP_FAILURE)
        {
            nmhSetFsErpsRingRowStatus
                (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
            ErpsApiUnLock ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set the switch port");
            return;
        }
    }

    /* To set Ring VC Recovery */

    STRCPY (pHttp->au1Name, "fsErpsRingPortBlockingOnVcRecovery");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4VCRecovery = ATOI (pHttp->au1Value);

        if (i4RingPort2 == 0)
        {

            if (nmhTestv2FsErpsRingPortBlockingOnVcRecovery
                (&u4ErrCode, u4FsErpsContextId, u4FsErpsRingId,
                 i4VCRecovery) == SNMP_FAILURE)
            {
                nmhSetFsErpsRingRowStatus
                    (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
                ErpsApiUnLock ();
                IssSendError (pHttp,
                              (CONST INT1 *) "Unable to set the VC Recovery");
                return;
            }

            if (nmhSetFsErpsRingPortBlockingOnVcRecovery (u4FsErpsContextId,
                                                          u4FsErpsRingId,
                                                          i4VCRecovery) ==
                SNMP_FAILURE)
            {
                nmhSetFsErpsRingRowStatus
                    (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
                ErpsApiUnLock ();
                IssSendError (pHttp,
                              (CONST INT1 *) "Unable to set the VC Recovery");
                return;
            }
        }
    }

    /* To set Protected vlan group id */

    STRCPY (pHttp->au1Name, "fsErpsRingProtectedVlanGroupId");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4ProtectedVlanId = ATOI (pHttp->au1Value);
        if (nmhTestv2FsErpsRingProtectedVlanGroupId
            (&u4ErrCode, u4FsErpsContextId, u4FsErpsRingId,
             i4ProtectedVlanId) == SNMP_FAILURE)
        {
            nmhSetFsErpsRingRowStatus
                (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
            ErpsApiUnLock ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set the protected vlan id");
            return;
        }

        if (nmhSetFsErpsRingProtectedVlanGroupId (u4FsErpsContextId,
                                                  u4FsErpsRingId,
                                                  i4ProtectedVlanId) ==
            SNMP_FAILURE)
        {
            nmhSetFsErpsRingRowStatus
                (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
            ErpsApiUnLock ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set the protected vlan id");
            return;
        }
    }

    /* To set Ring Protection Type */

    STRCPY (pHttp->au1Name, "fsErpsRingProtectionType");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4ProtectionType = ATOI (pHttp->au1Value);
        if (nmhTestv2FsErpsRingProtectionType
            (&u4ErrCode, u4FsErpsContextId, u4FsErpsRingId,
             i4ProtectionType) == SNMP_FAILURE)
        {
            nmhSetFsErpsRingRowStatus
                (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
            ErpsApiUnLock ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set the protection type");
            return;
        }

        if (nmhSetFsErpsRingProtectionType (u4FsErpsContextId,
                                            u4FsErpsRingId,
                                            i4ProtectionType) == SNMP_FAILURE)
        {
            nmhSetFsErpsRingRowStatus
                (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
            ErpsApiUnLock ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set the protection type");
            return;
        }
    }

    /* To set ring with/without virtual channel */

    STRCPY (pHttp->au1Name, "fsErpsRingRAPSSubRingWithoutVC");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4WithoutVC = ATOI (pHttp->au1Value);
        if (i4CompatibleVersion == 2)
        {
            if (nmhTestv2FsErpsRingRAPSSubRingWithoutVC
                (&u4ErrCode, u4FsErpsContextId, u4FsErpsRingId,
                 i4WithoutVC) == SNMP_FAILURE)
            {
                nmhSetFsErpsRingRowStatus
                    (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
                ErpsApiUnLock ();
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to set the without virtual channel in ring node");
                return;
            }

            if (nmhSetFsErpsRingRAPSSubRingWithoutVC (u4FsErpsContextId,
                                                      u4FsErpsRingId,
                                                      i4WithoutVC) ==
                SNMP_FAILURE)
            {
                nmhSetFsErpsRingRowStatus
                    (u4FsErpsContextId, u4FsErpsRingId, i4RowStatusValue);
                ErpsApiUnLock ();
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to set the without virtual channel in ring node");
                return;
            }
        }
    }

    /* To set Ring Row Status */
    HttpGetValuebyName (ISS_ACTION, pHttp->au1Value, pHttp->au1PostQuery);

    /*This check is done to ensure , that user doesnot attempt to apply 
     * In-Active (NOT_READY) to the ring */
    if ((STRCMP (pHttp->au1Value, "Apply") == 0) && (i4RowStatus != NOT_READY))
    {

        STRCPY (pHttp->au1Name, "fsErpsRingRowStatus");
        if (HttpGetValuebyName
            (pHttp->au1Name, pHttp->au1Value,
             pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4RingStatus = ATOI (pHttp->au1Value);
            if (nmhTestv2FsErpsRingRowStatus (&u4ErrCode, u4FsErpsContextId,
                                              u4FsErpsRingId,
                                              i4RingStatus) == SNMP_FAILURE)
            {
                ErpsApiUnLock ();
                IssSendError (pHttp,
                              (CONST INT1 *) "Unable to set the ring status");
                return;
            }

            if (nmhSetFsErpsRingRowStatus (u4FsErpsContextId,
                                           u4FsErpsRingId,
                                           i4RingStatus) == SNMP_FAILURE)
            {
                ErpsApiUnLock ();
                IssSendError (pHttp,
                              (CONST INT1 *) "Unable to set the ring status ");
                return;
            }
        }
    }
    /* to clear the FS/MS */

    STRCPY (pHttp->au1Name, "fsErpsRingConfigClear");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4RingClear = ATOI (pHttp->au1Value);
        if (i4CompatibleVersion == 2)
        {
            if (i4RingClear == 2)
            {
                if (nmhTestv2FsErpsRingConfigClear
                    (&u4ErrCode, u4FsErpsContextId, u4FsErpsRingId,
                     i4RingClear) == SNMP_FAILURE)
                {
                    ErpsApiUnLock ();
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Unable to clear the FS/MS");
                    return;
                }
                if (nmhSetFsErpsRingConfigClear (u4FsErpsContextId,
                                                 u4FsErpsRingId,
                                                 i4RingClear) == SNMP_FAILURE)
                {
                    ErpsApiUnLock ();
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Unable to clear the MS/FS");
                    return;
                }
            }
        }
    }

    /* To set Ring Switch Command */

    STRCPY (pHttp->au1Name, "fsErpsRingConfigSwitchCmd");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4SwitchCmd = ATOI (pHttp->au1Value);

    if ((i4SwitchCmd != 1))
    {
        if (nmhTestv2FsErpsRingConfigSwitchCmd (&u4ErrCode, u4FsErpsContextId,
                                                u4FsErpsRingId,
                                                i4SwitchCmd) == SNMP_FAILURE)
        {
            ErpsApiUnLock ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set the switch command");
            return;
        }

        if (nmhSetFsErpsRingConfigSwitchCmd (u4FsErpsContextId,
                                             u4FsErpsRingId,
                                             i4SwitchCmd) == SNMP_FAILURE)
        {
            ErpsApiUnLock ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set the switch command");
            return;
        }
    }

    /* To set mac id */

    STRCPY (pHttp->au1Name, "fsErpsRingMacId");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4RingMacid = ATOI (pHttp->au1Value);
        if (nmhTestv2FsErpsRingMacId
            (&u4ErrCode, u4FsErpsContextId, u4FsErpsRingId,
             i4RingMacid) == SNMP_FAILURE)
        {
            ErpsApiUnLock ();
            IssSendError (pHttp, (CONST INT1 *) "Unable to set the macid");
            return;
        }

        if (nmhSetFsErpsRingMacId (u4FsErpsContextId,
                                   u4FsErpsRingId, i4RingMacid) == SNMP_FAILURE)
        {
            ErpsApiUnLock ();
            IssSendError (pHttp, (CONST INT1 *) "Unable to set the macid");
            return;
        }
    }

    ErpsApiUnLock ();
    IssProcessErpsRingTablePageGet (pHttp);
}

/*********************************************************************
 * *  Function Name : IssProcessErpsRingStatusPageGet
 * *  Description   : This function processes the get request coming for the
 * *                  ERPS Ring Status.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/
VOID
IssProcessErpsRingStatusPageGet (tHttp * pHttp)
{
    INT4                i4OutCome = 0;
    INT4                i4DispCnt = 0;
    INT4                i4RingPort1Status = -1;
    INT4                i4RingPort2Status = -1;
    INT4                i4RingNodeStatus = -1;
    INT4                i4RingNodeType = -1;
    INT4                i4RingSemState = -1;
    INT4                i4CompatibleVersion = -1;
    INT4                i4RplNeighbourPort = -1;
    UINT4               u4Temp = 0;
    UINT4               u4FsErpsRingId = 0;
    UINT4               u4FsErpsRingIdNext = 0;
    UINT4               u4FsErpsContextId = 0;
    UINT4               u4FsErpsContextIdNext = 0;

    pHttp->i4Write = 0;

    WebnmRegisterLock (pHttp, ErpsApiLock, ErpsApiUnLock);
    ERPS_LOCK ();

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    i4OutCome =
        (INT4) (nmhGetFirstIndexFsErpsRingTable
                (&u4FsErpsContextId, &u4FsErpsRingId));

    if (i4OutCome == SNMP_FAILURE)
    {
        ERPS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;

    u4FsErpsContextIdNext = u4FsErpsContextId;
    u4FsErpsRingIdNext = u4FsErpsRingId;

    do
    {
        u4FsErpsContextId = u4FsErpsContextIdNext;
        u4FsErpsRingId = u4FsErpsRingIdNext;

        pHttp->i4Write = (INT4) u4Temp;

        /* To Display the Context ID */
        STRCPY (pHttp->au1KeyString, "CONTEXT_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4FsErpsContextId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* To Display the Ring ID */
        STRCPY (pHttp->au1KeyString, "RING_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4FsErpsRingId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* To Display the Port1 Status */
        STRCPY (pHttp->au1KeyString, "PORT1_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsErpsRingPort1Status (u4FsErpsContextId, u4FsErpsRingId,
                                     &i4RingPort1Status);

        if (i4RingPort1Status == 1)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Blocked");
        }
        if (i4RingPort1Status == 2)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Un-Blocked");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* To Display the Port2 Status */
        STRCPY (pHttp->au1KeyString, "PORT2_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsErpsRingPort2Status (u4FsErpsContextId, u4FsErpsRingId,
                                     &i4RingPort2Status);

        if (i4RingPort2Status == 1)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Blocked");
        }
        if (i4RingPort2Status == 2)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Un-Blocked");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* To Display the Port1 Fail Status */
        STRCPY (pHttp->au1KeyString, "PORT1_FAIL_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsErpsRingNodeStatus (u4FsErpsContextId, u4FsErpsRingId,
                                    &i4RingNodeStatus);

        if (i4RingNodeStatus & ERPS_NODE_STATUS_PORT1_LOCAL_SF)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Local-Failure");
        }
        else if (i4RingNodeStatus & ERPS_NODE_STATUS_PORT1_RAPS_SF)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Remote-Failure");
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "None");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* To Display the Port2 Fail Status */
        STRCPY (pHttp->au1KeyString, "PORT2_FAIL_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        if (i4RingNodeStatus & ERPS_NODE_STATUS_PORT2_LOCAL_SF)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Local-Failure");
        }
        else if (i4RingNodeStatus & ERPS_NODE_STATUS_PORT2_RAPS_SF)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Remote-Failure");
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "None");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* To Display the Node Type */
        STRCPY (pHttp->au1KeyString, "NODE_TYPE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        nmhGetFsErpsRingNodeType (u4FsErpsContextId, u4FsErpsRingId,
                                  &i4RingNodeType);
        if (i4RingNodeType == ERPS_RPL_OWNER)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "RPL-Owner");
        }
        if (i4RingNodeType == ERPS_NON_RPL_OWNER)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Non-RPL-Owner");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /*To Display the Compatible version */
        STRCPY (pHttp->au1KeyString, "COMPATIBLE_VERSION_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsErpsRingRAPSCompatibleVersion (u4FsErpsContextId,
                                               u4FsErpsRingId,
                                               &i4CompatibleVersion);
        if (i4CompatibleVersion == ERPS_RING_COMPATIBLE_VERSION2)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "V2");
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "V1");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* To Display the RPL neighbor port  */
        STRCPY (pHttp->au1KeyString, "RPL_NEIGHBOUR_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4CompatibleVersion == ERPS_RING_COMPATIBLE_VERSION2)
        {
            nmhGetFsErpsRingRAPSCompatibleVersion (u4FsErpsContextId,
                                                   u4FsErpsRingId,
                                                   &i4CompatibleVersion);
            nmhGetFsErpsRingRplNeighbourPort (u4FsErpsContextId, u4FsErpsRingId,
                                              &i4RplNeighbourPort);
            if (i4RplNeighbourPort != 0)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         "RPL-Neighbor-Port");
            }
            else
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         "Non-RPL-Neighbor-Port");
            }

        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     "Non-RPL-Neighbor-Port");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* To Display the WTR Timer Status */
        STRCPY (pHttp->au1KeyString, "WTR_TIMER_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        if (i4RingNodeStatus & ERPS_WTR_TIMER_RUNNING)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Running");
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Not-Running");
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* To Display the WTB Timer Status */
        STRCPY (pHttp->au1KeyString, "WTB_TIMER_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4CompatibleVersion == ERPS_RING_COMPATIBLE_VERSION2)
        {
            if (i4RingNodeStatus & ERPS_WTB_TIMER_RUNNING)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Running");
            }
            else
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Not-Running");
            }
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Not-Running");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* To Display the Guard Timer Status */
        STRCPY (pHttp->au1KeyString, "GUARD_TIMER_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        if (i4RingNodeStatus & ERPS_GUARD_TIMER_RUNNING)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Running");
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Not-Running");
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* To Display the Hold-off Timer Status */
        STRCPY (pHttp->au1KeyString, "HOLD_TIMER_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        if (i4RingNodeStatus & ERPS_HOLDOFF_TIMER_RUNNING)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Running");
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Not-Running");
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* To Display the Hold-off Timer Status */
        STRCPY (pHttp->au1KeyString, "PERIODIC_TIMER_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        if (i4RingNodeStatus & ERPS_PERIODIC_TIMER_RUNNING)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Running");
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Not-Running");
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* To Display the Sem State */
        STRCPY (pHttp->au1KeyString, "SEM_STATE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        nmhGetFsErpsRingSemState (u4FsErpsContextId, u4FsErpsRingId,
                                  &i4RingSemState);
        switch (i4RingSemState)
        {
            case ERPS_RING_DISABLED_STATE:
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Disabled");
                break;
            case ERPS_RING_IDLE_STATE:
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Idle");
                break;
            case ERPS_RING_PROTECTION_STATE:
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Protection");
                break;
            case ERPS_RING_PENDING_STATE:
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Pending");
                break;
            case ERPS_RING_MANUAL_SWITCH_STATE:
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Manual Switch");
                break;
            case ERPS_RING_FORCED_SWITCH_STATE:
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Force Switch");
                break;
            default:
                break;
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        i4DispCnt++;

    }
    while (nmhGetNextIndexFsErpsRingTable
           (u4FsErpsContextId, &u4FsErpsContextIdNext, u4FsErpsRingId,
            &u4FsErpsRingIdNext) == SNMP_SUCCESS);

    if (i4DispCnt == 0)
    {
        ERPS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    ERPS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
 * *  Function Name : IssProcessErpsRingTcPropPageGet
 * *  Description   : This function processes the get request coming for the
 * *                  ERPS Ring Configuration.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/
VOID
IssProcessErpsRingTcPropPageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    UINT4               u4FsErpsRingId = 0;
    UINT4               u4FsErpsRingIdNext = 0;
    UINT4               u4FsErpsContextId = 0;
    UINT4               u4FsErpsContextIdNext = 0;
    INT4                i4OutCome = 0;
    INT4                i4DispCnt = 0;
    INT4                i4FsErpsRingConfigPropagateTC = -1;

    pHttp->i4Write = 0;

    WebnmRegisterLock (pHttp, ErpsApiLock, ErpsApiUnLock);
    ERPS_LOCK ();

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    i4OutCome =
        (INT4) (nmhGetFirstIndexFsErpsRingTable
                (&u4FsErpsContextId, &u4FsErpsRingId));

    if (i4OutCome == SNMP_FAILURE)
    {
        ERPS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;

    u4FsErpsContextIdNext = u4FsErpsContextId;
    u4FsErpsRingIdNext = u4FsErpsRingId;

    do
    {
        u4FsErpsContextId = u4FsErpsContextIdNext;
        u4FsErpsRingId = u4FsErpsRingIdNext;

        pHttp->i4Write = (INT4) u4Temp;

        /* To Display the Context ID */
        STRCPY (pHttp->au1KeyString, "fsErpsContextId_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4FsErpsContextId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* To Display the Ring ID */
        STRCPY (pHttp->au1KeyString, "fsErpsRingId_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4FsErpsRingId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* To Display the TC Propagation Status */
        STRCPY (pHttp->au1KeyString, "fsErpsRingConfigPropagateTC_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsErpsRingConfigPropagateTC (u4FsErpsContextId, u4FsErpsRingId,
                                           &i4FsErpsRingConfigPropagateTC);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                 i4FsErpsRingConfigPropagateTC);

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* To Display the TC List */
        STRCPY (pHttp->au1KeyString, "fsErpsRingTcPropRingId_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        IssProcessErpsRingTcPropTableGet (pHttp, u4FsErpsContextId,
                                          u4FsErpsRingId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        i4DispCnt++;

    }
    while (nmhGetNextIndexFsErpsRingTable
           (u4FsErpsContextId, &u4FsErpsContextIdNext, u4FsErpsRingId,
            &u4FsErpsRingIdNext) == SNMP_SUCCESS);

    if (i4DispCnt == 0)
    {
        ERPS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    ERPS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
 * *  Function Name : IssProcessErpsRingTcPropPageSet
 * *  Description   : This function processes the set request coming for the
 * *                  ERPS Ring TC Table.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/
VOID
IssProcessErpsRingTcPropPageSet (tHttp * pHttp)
{

    UINT4               au4TCList[ERPS_TC_LIST_SIZE];
    UINT4               u4FsErpsRingId = 0;
    UINT4               u4FsErpsContextId = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RingPropagateTC = -1;

    STRCPY (pHttp->au1Name, "fsErpsContextId");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4FsErpsContextId = (UINT4) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "fsErpsRingId");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4FsErpsRingId = (UINT4) ATOI (pHttp->au1Value);

    ErpsApiLock ();

    /* To Set the TC Prop Status */

    STRCPY (pHttp->au1Name, "fsErpsRingConfigPropagateTC");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4RingPropagateTC = ATOI (pHttp->au1Value);

    if (nmhTestv2FsErpsRingConfigPropagateTC (&u4ErrCode, u4FsErpsContextId,
                                              u4FsErpsRingId,
                                              i4RingPropagateTC) ==
        SNMP_FAILURE)
    {
        ErpsApiUnLock ();
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to set the TC Propagation Status");
        return;
    }

    if (nmhSetFsErpsRingConfigPropagateTC
        (u4FsErpsContextId, u4FsErpsRingId, i4RingPropagateTC) == SNMP_FAILURE)
    {
        ErpsApiUnLock ();
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to set the TC Propagation Status");
        return;
    }

    HttpGetValuebyName (ISS_ACTION, pHttp->au1Value, pHttp->au1PostQuery);

    if ((STRCMP (pHttp->au1Value, "Add") == 0) ||
        (STRCMP (pHttp->au1Value, "Apply") == 0))
    {

        /* To set Ring TC List */

        STRCPY (pHttp->au1Name, "fsErpsRingTcPropRingId");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);

        /* If ring ids are given convert them to array format 
         * and store them in au4TCList
         * au4TCList[0] contains first ring id given .. 
         * au4TCList[1] contains 2nd ring id... etc., 
         * */
        if ((pHttp->au1Value != NULL)
            && (STRCMP (pHttp->au1Value, "None") != 0))
        {
            MEMSET (au4TCList, 0, (4 * ERPS_TC_LIST_SIZE));
            issDecodeSpecialChar (pHttp->au1Value);
            if (ConvertStrToPortArray (pHttp->au1Value, au4TCList,
                                       sizeof (au4TCList),
                                       ERPS_MAX_RING_ID) != OSIX_SUCCESS)
            {
                ErpsApiUnLock ();
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Invalid TC List;TC List must be entered in the format 1,2,3");
                return;
            }
            IssProcessErpsRingTcPropTableSet (u4FsErpsContextId, u4FsErpsRingId,
                                              au4TCList);
        }
        /* All Values in the RING TC propagation list are deleted 
         * if the user enters none*/
        if (STRCMP (pHttp->au1Value, "None") == 0)
        {
            if (ErpsDelAllTopologyList (u4FsErpsContextId, u4FsErpsRingId) ==
                ISS_FAILURE)
            {
                ErpsApiUnLock ();
                IssSendError (pHttp,
                              (CONST INT1 *) "Unable to delete the TC entry");
                return;
            }
        }

    }
    /*To Delete the entire TC List */

    if (STRCMP (pHttp->au1Value, "delete") == 0)
    {
        /* Values in the RING TC propagation list are deleted */
        if (ErpsDelAllTopologyList (u4FsErpsContextId, u4FsErpsRingId) ==
            ISS_FAILURE)
        {
            ErpsApiUnLock ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to delete the TC entry");
            return;
        }

    }

    ErpsApiUnLock ();
    IssProcessErpsRingTcPropPageGet (pHttp);
}

/*********************************************************************
 * *  Function Name : IssProcessErpsRingTcPropTableGet
 * *  Description   : This function processes the get request coming for the
 * *                  ERPS Ring TC Table.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/
VOID
IssProcessErpsRingTcPropTableGet (tHttp * pHttp, UINT4 u4ContextId,
                                  UINT4 u4RingId)
{
    UINT4               u4NextContextId = 0;
    UINT4               u4NextRingId = 0;
    UINT4               u4TcId = 0;
    UINT4               u4NextTcProgId = 0;
    UINT1               au1TmpArray[ERPS_TC_LIST_SIZE];
    UINT2               u2Offset = 0;

    MEMSET (au1TmpArray, 0, (ERPS_TC_LIST_SIZE));
    MEMSET (pHttp->au1DataString, 0, ENM_MAX_NAME_LEN);

    if (nmhGetNextIndexFsErpsRingTcPropTable (u4ContextId, &u4NextContextId,
                                              u4RingId, &u4NextRingId,
                                              0, &u4NextTcProgId)
        == SNMP_FAILURE)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "None");
        return;
    }

    do
    {
        if ((u4ContextId != (UINT4) u4NextContextId) ||
            (u4RingId != u4NextRingId))
        {
            break;
        }

        u4TcId = u4NextTcProgId;
        SPRINTF ((CHR1 *) au1TmpArray, "%d,", u4TcId);
        SPRINTF ((((CHR1 *) pHttp->au1DataString) + u2Offset), "%s",
                 au1TmpArray);
        u2Offset = (UINT2) (u2Offset + STRLEN (au1TmpArray));
    }
    while (nmhGetNextIndexFsErpsRingTcPropTable (u4ContextId, &u4NextContextId,
                                                 u4RingId, &u4NextRingId,
                                                 u4TcId, &u4NextTcProgId)
           == SNMP_SUCCESS);

    printf ("TC String is %s \n", pHttp->au1DataString);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : IssProcessErpsRingTcPropTableSet 
 *
 * DESCRIPTION      : This Routine populates the TC propagation list
 *                    for the ring.
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           : 
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
IssProcessErpsRingTcPropTableSet (UINT4 u4ContextId,
                                  UINT4 u4RingId, UINT4 *pu4TcProgList)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4TcProgId = 0;
    UINT4               u4TCListIndex = 0;
    INT4                i4TCRowStatus = -1;

    /* Configure the TC entries only if they are given as input
     * by user.
     * If no TC entries are given , just configure the TC 
     * propagation status */
    if (pu4TcProgList[0] != 0)
    {
        /* Test each and every TC list value.Only if test is success, 
         * delete the old values and configure the new values */

        for (u4TCListIndex = 0; u4TCListIndex < ERPS_TC_LIST_SIZE;
             u4TCListIndex++)
        {
            if (pu4TcProgList[u4TCListIndex] != 0)
            {
                u4TcProgId = pu4TcProgList[u4TCListIndex];

                /* This check is to ensure that invalid ring ids are not populated. 
                 * The existing TC list will be deleted only if 
                 * the new TC list is correct*/
                if (nmhTestv2FsErpsRingTcPropRowStatus
                    (&u4ErrorCode, u4ContextId, u4RingId, u4TcProgId,
                     CREATE_AND_GO) == SNMP_FAILURE)
                {
                    /* In case, the new TC list contains a TCId that is already 
                     *given by user , test routine returns error.
                     *To avoid that the following check is done.
                     */

                    if (nmhGetFsErpsRingTcPropRowStatus
                        (u4ContextId, u4RingId, u4TcProgId,
                         &i4TCRowStatus) != SNMP_SUCCESS)
                    {
                        return CLI_FAILURE;
                    }
                }
            }
        }
        /* Existing values in the RING TC propagation list are deleted */
        if (ErpsDelAllTopologyList (u4ContextId, u4RingId) == ISS_FAILURE)
        {
            return ISS_FAILURE;
        }

        /* Now set the new TC propagation list values */

        for (u4TCListIndex = 0; u4TCListIndex < ERPS_TC_LIST_SIZE;
             u4TCListIndex++)
        {
            if (pu4TcProgList[u4TCListIndex] != 0)
            {
                u4TcProgId = pu4TcProgList[u4TCListIndex];

                if (nmhSetFsErpsRingTcPropRowStatus (u4ContextId, u4RingId,
                                                     u4TcProgId, CREATE_AND_GO)
                    == SNMP_FAILURE)
                {
                    return ISS_FAILURE;
                }
            }
        }
    }

    return ISS_SUCCESS;
}

/*********************************************************************
 * *  Function Name : IssProcessErpsRingVlanPageGet
 * *  Description   : This function processes the get request coming for the
 * *                  ERPS Vlan table Configuration.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/
VOID
IssProcessErpsRingVlanPageGet (tHttp * pHttp)
{

    UINT4               u4ContextId = 0;
    UINT4               u4FirstContextId = 0;
    UINT4               u4NextContextId = 0;
    UINT4               u4Temp = 0;
    INT4                i4NextVlanGroupId = 0;
    INT4                i4VlanGroupId = 0;
    INT4                i4GroupId = 0;
    INT4                i4CurrVlanGroupId = 0;
    INT4                i4VlanId = 0;
    INT4                i4NextVlanId = 0;
    BOOL1               b1Flag = OSIX_FALSE;
    BOOL1               b1Check = OSIX_FALSE;

    pHttp->i4Write = 0;

    WebnmRegisterLock (pHttp, ErpsApiLock, ErpsApiUnLock);
    ERPS_LOCK ();

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    nmhGetFirstIndexFsErpsContextTable (&u4FirstContextId);

    do
    {
        u4ContextId = u4FirstContextId;
        for (i4GroupId = 0; i4GroupId <= ERPS_MAX_VLAN_GROUP_ID; i4GroupId++)
        {
            MEMSET (&gau1ErpsVlanList[0], 0, VLAN_LIST_SIZE);
            u4NextContextId = 0;
            i4NextVlanGroupId = 0;
            i4VlanId = 0;
            i4VlanGroupId = i4GroupId;
            i4CurrVlanGroupId = i4VlanGroupId;

            b1Flag = OSIX_FALSE;
            while (nmhGetNextIndexFsErpsVlanGroupTable
                   (u4ContextId, &u4NextContextId, i4VlanId, &i4NextVlanId,
                    i4VlanGroupId, &i4NextVlanGroupId) == SNMP_SUCCESS)

            {
                if ((u4ContextId != u4NextContextId))
                {
                    break;
                }

                i4VlanId = i4NextVlanId;
                u4ContextId = u4NextContextId;
                i4VlanGroupId = i4NextVlanGroupId;

                if (i4CurrVlanGroupId != i4NextVlanGroupId)
                {
                    continue;
                }

                /* Set the bitmap for appropriate vlan id  */
                OSIX_BITLIST_SET_BIT (gau1ErpsVlanList, i4NextVlanId,
                                      VLAN_LIST_SIZE);
                /*The Flag is set to determine whether the bitlist has been set
                 *          * or not */
                b1Flag = OSIX_TRUE;
            }
            if (b1Flag == OSIX_TRUE)
            {

                pHttp->i4Write = (INT4) u4Temp;

                /* To Display the Context ID */
                STRCPY (pHttp->au1KeyString, "fsErpsContextId_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4ContextId);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                /* To Display the Vlan group ID */
                STRCPY (pHttp->au1KeyString, "fsErpsVlanGroupId_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         i4CurrVlanGroupId);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                /* To Display the Vlan List */
                STRCPY (pHttp->au1KeyString, "fsErpsVlanId_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                IssProcessErpsRingVlanTableGet (pHttp, &gau1ErpsVlanList[0]);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                b1Check = OSIX_TRUE;

            }
        }
    }
    while ((nmhGetNextIndexFsErpsContextTable (u4ContextId, &u4FirstContextId)
            == SNMP_SUCCESS));
    MEMSET (&gau1ErpsVlanList[0], 0, VLAN_LIST_SIZE);
    if (b1Check == OSIX_FALSE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    ERPS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
 * *  Function Name : IssProcessErpsRingVlanPageSet
 * *  Description   : This function processes the set request coming for the
 * *                  ERPS Ring Vlan Table.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/
VOID
IssProcessErpsRingVlanPageSet (tHttp * pHttp)
{

    INT4                i4FsErpsVlanId = 0;
    UINT4               u4FsErpsContextId = 0;
    UINT1              *pu1VlanList = NULL;

    pu1VlanList = UtlShMemAllocVlanList ();

    if (pu1VlanList == NULL)
    {
        return;
    }

    STRCPY (pHttp->au1Name, "fsErpsContextId");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4FsErpsContextId = (UINT4) ATOI (pHttp->au1Value);

    if (nmhValidateIndexInstanceFsErpsContextTable
        (u4FsErpsContextId) == SNMP_FAILURE)
    {
        UtlShMemFreeVlanList (pu1VlanList);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Context ID");

        return;
    }

    STRCPY (pHttp->au1Name, "fsErpsVlanGroupId");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4FsErpsVlanId = ATOI (pHttp->au1Value);

    ErpsApiLock ();

    HttpGetValuebyName (ISS_ACTION, pHttp->au1Value, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Value, "Add") == 0)
    {

        /* To set Vlan  List */

        STRCPY (pHttp->au1Name, "fsErpsVlanId");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);

        /* If vlan ids are given convert them to array format 
         * and store them in au4Vlan
         * pu1VlanList[0] contains first vlan id given .. 
         * pu1VlanList[1] contains 2nd vlan id... etc., 
         * */
        if ((pHttp->au1Value != NULL)
            && (STRCMP (pHttp->au1Value, "None") != 0))
        {
            MEMSET (pu1VlanList, 0, VLAN_LIST_SIZE);
            issDecodeSpecialChar (pHttp->au1Value);
            if (ConvertStrToPortList (pHttp->au1Value, pu1VlanList,
                                      VLAN_LIST_SIZE,
                                      VLAN_LIST_SIZE) != OSIX_SUCCESS)
            {
                UtlShMemFreeVlanList (pu1VlanList);
                ErpsApiUnLock ();
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Invalid Vlan List;Vlan List must be entered in the format 1,2,3");
                return;
            }
            IssProcessErpsRingVlanTableSet (u4FsErpsContextId, i4FsErpsVlanId,
                                            pu1VlanList);
        }

    }

    if (STRCMP (pHttp->au1Value, "Apply") == 0)
    {

        /* To set Vlan  List */

        STRCPY (pHttp->au1Name, "fsErpsVlanId");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);

        /* If vlan ids are given convert them to array format 
         * and store them in au4Vlan
         * pu1VlanList[0] contains first vlan id given .. 
         * pu1VlanList[1] contains 2nd vlan id... etc., 
         * */
        if ((pHttp->au1Value != NULL)
            && (STRCMP (pHttp->au1Value, "None") != 0))
        {
            MEMSET (pu1VlanList, 0, VLAN_LIST_SIZE);
            issDecodeSpecialChar (pHttp->au1Value);
            if (ConvertStrToPortList (pHttp->au1Value, pu1VlanList,
                                      VLAN_LIST_SIZE,
                                      VLAN_LIST_SIZE) != OSIX_SUCCESS)
            {
                UtlShMemFreeVlanList (pu1VlanList);
                ErpsApiUnLock ();
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Invalid Vlan List;Vlan List must be entered in the format 1,2,3");
                return;
            }
            IssProcessErpsRingVlanTableModify (u4FsErpsContextId,
                                               i4FsErpsVlanId, pu1VlanList);
        }

    }
    if (STRCMP (pHttp->au1Value, "delete") == 0)
    {

        /* To delete Vlan  List */

        STRCPY (pHttp->au1Name, "fsErpsVlanId");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);

        /* If vlan ids are given convert them to array format
         * and store them in au4Vlan
         * pu1VlanList[0] contains first vlan id given ..
         * pu1VlanList[1] contains 2nd vlan id... etc.,
         * */
        if ((pHttp->au1Value != NULL)
            && (STRCMP (pHttp->au1Value, "None") != 0))
        {
            MEMSET (pu1VlanList, 0, VLAN_LIST_SIZE);
            issDecodeSpecialChar (pHttp->au1Value);
            if (ConvertStrToPortList (pHttp->au1Value, pu1VlanList,
                                      VLAN_LIST_SIZE,
                                      VLAN_LIST_SIZE) != OSIX_SUCCESS)
            {
                UtlShMemFreeVlanList (pu1VlanList);
                ErpsApiUnLock ();
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Invalid Vlan List;Vlan List must be entered in the format 1,2,3");
                return;
            }
            IssProcessErpsRingVlanTableDelete (u4FsErpsContextId,
                                               i4FsErpsVlanId, pu1VlanList);
        }

    }

    UtlShMemFreeVlanList (pu1VlanList);
    ErpsApiUnLock ();
    IssProcessErpsRingVlanPageGet (pHttp);
}

/*********************************************************************
 * *  Function Name : IssProcessErpsRingVlanTableGet
 * *  Description   : This function processes the get request coming for the
 * *                  ERPS Ring Vlan table
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssProcessErpsRingVlanTableGet (tHttp * pHttp, UINT1 *pu1VlanList)
{

    UINT4               u4StartVlanId = 0;
    UINT4               u4CurrVlanId = 0;
    UINT4               u4PrevVlanId = 0;
    UINT4               u4PrevVlan = 0;
    INT4                i4Result = OSIX_FALSE;
    UINT2               u2Offset = 0;
    UINT1              *pu1TmpArray = NULL;
    BOOL1               b1Flag = OSIX_FALSE;

    pu1TmpArray = UtlShMemAllocVlanList ();
    if (pu1TmpArray == NULL)
    {
        return;
    }

    MEMSET (pu1TmpArray, 0, VLAN_LIST_SIZE);
    MEMSET (pHttp->au1DataString, 0, ENM_MAX_NAME_LEN);

    if (pu1VlanList != NULL)
    {
        for (u4CurrVlanId = 1; u4CurrVlanId <= VLAN_DEV_MAX_VLAN_ID;
             u4CurrVlanId++)
        {
            OSIX_BITLIST_IS_BIT_SET (pu1VlanList, u4CurrVlanId,
                                     VLAN_LIST_SIZE, i4Result);

            if (i4Result == OSIX_FALSE)
            {
                if (u4PrevVlanId != 0)
                {
                    if (u4StartVlanId != u4PrevVlanId)
                    {
                        SPRINTF ((CHR1 *) pu1TmpArray, "-%d", u4PrevVlanId);
                        SPRINTF ((((CHR1 *) pHttp->au1DataString) + u2Offset),
                                 "%s", pu1TmpArray);
                        u2Offset = (UINT2) (u2Offset + STRLEN (pu1TmpArray));
                    }
                    else
                    {
                        if (u4PrevVlan != 0)
                        {
                            SPRINTF ((CHR1 *) pu1TmpArray, ",");
                            SPRINTF ((((CHR1 *) pHttp->au1DataString) +
                                      u2Offset), "%s", pu1TmpArray);
                            u2Offset =
                                (UINT2) (u2Offset + STRLEN (pu1TmpArray));
                        }
                        SPRINTF ((CHR1 *) pu1TmpArray, "%d", u4PrevVlanId);
                        SPRINTF ((((CHR1 *) pHttp->au1DataString) + u2Offset),
                                 "%s", pu1TmpArray);
                        u2Offset = (UINT2) (u2Offset + STRLEN (pu1TmpArray));
                        u4PrevVlan = u4PrevVlanId;
                    }
                    u4StartVlanId = 0;
                    u4PrevVlanId = 0;
                }
            }
            else
            {
                if ((u4StartVlanId != 0) && (b1Flag == OSIX_TRUE))
                {
                    if ((u4PrevVlanId != u4PrevVlan) && (u4StartVlanId != 1))
                    {
                        SPRINTF ((CHR1 *) pu1TmpArray, ",");
                        SPRINTF ((((CHR1 *) pHttp->au1DataString) + u2Offset),
                                 "%s", pu1TmpArray);
                        u2Offset = (UINT2) (u2Offset + STRLEN (pu1TmpArray));
                    }
                    SPRINTF ((CHR1 *) pu1TmpArray, "%d", u4StartVlanId);
                    SPRINTF ((((CHR1 *) pHttp->au1DataString) + u2Offset), "%s",
                             pu1TmpArray);
                    u2Offset = (UINT2) (u2Offset + STRLEN (pu1TmpArray));
                    b1Flag = OSIX_FALSE;
                }

                if (u4StartVlanId == 0)
                {
                    u4StartVlanId = u4CurrVlanId;
                    b1Flag = OSIX_TRUE;
                }

                u4PrevVlanId = u4CurrVlanId;

                /* Print the last Vlan Id */
                if (u4CurrVlanId == VLAN_DEV_MAX_VLAN_ID)
                {
                    if (u4StartVlanId != u4PrevVlanId)
                    {
                        SPRINTF ((CHR1 *) pu1TmpArray, "-%d", u4CurrVlanId);
                        SPRINTF ((((CHR1 *) pHttp->au1DataString) + u2Offset),
                                 "%s", pu1TmpArray);
                        u2Offset = (UINT2) (u2Offset + STRLEN (pu1TmpArray));
                    }
                    else
                    {
                        SPRINTF ((CHR1 *) pu1TmpArray, "%d", u4CurrVlanId);
                        SPRINTF ((((CHR1 *) pHttp->au1DataString) + u2Offset),
                                 "%s", pu1TmpArray);
                        u2Offset = (UINT2) (u2Offset + STRLEN (pu1TmpArray));
                    }
                }

            }
        }
    }
    else
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "None");
        UtlShMemFreeVlanList (pu1TmpArray);
        return;
    }
    UtlShMemFreeVlanList (pu1TmpArray);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : IssProcessErpsRingVlanTableSet
 *
 * DESCRIPTION      : This Routine populates the ring vlan table
 *                    for the ring.
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           :
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
INT4
IssProcessErpsRingVlanTableSet (UINT4 u4ContextId,
                                INT4 i4VlanGroupId, UINT1 *pu4VlanList)
{

    UINT4               u4ErrorCode = 0;
    INT4                i4VlanGroupRowStat = 0;
    INT4                i4VlanId = 0;
    UINT2               u2ByteIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2VlanFlag = 0;

    /* Configure the VLAN list entries only if they are given as input
     * by user.
     * If no VLAN list is given return success */
    if (pu4VlanList != NULL)
    {

        /* Test each and every VLAN list value.Only if test is success,
         * configure the new values */

        for (u2ByteIndex = 0; u2ByteIndex < VLAN_LIST_SIZE; u2ByteIndex++)
        {
            if (pu4VlanList[u2ByteIndex] != 0)
            {
                u2VlanFlag = pu4VlanList[u2ByteIndex];

                for (u2BitIndex = 0;
                     (u2BitIndex < BITS_PER_BYTE && u2VlanFlag != 0);
                     u2BitIndex++)
                {
                    if ((u2VlanFlag & VLAN_BIT8) != 0)
                    {
                        i4VlanId =
                            (UINT2) ((u2ByteIndex * BITS_PER_BYTE) +
                                     u2BitIndex + 1);

                        if (VLAN_IS_VLAN_ID_VALID (i4VlanId) == OSIX_FALSE)
                        {
                            u2VlanFlag = (UINT2) (u2VlanFlag << 1);
                            continue;
                        }
                        /* This check is to ensure that invalid vlan ids are not
                         * populated.*/
                        if (nmhTestv2FsErpsVlanGroupRowStatus
                            (&u4ErrorCode, u4ContextId, i4VlanId, i4VlanGroupId,
                             CREATE_AND_GO) == SNMP_FAILURE)
                        {
                            if (u4ErrorCode == SNMP_ERR_WRONG_VALUE)
                            {
                                /*  The vlan list contains a vlan id
                                 * (i4VlanGroupId) which exceeds the MAX or MIN
                                 * vlan group configured for the system. In this
                                 * case failure must be returned. To ensure that
                                 * the test routine fails because of this
                                 * condition, the error code (SNMP_ERR_WRONG_VALUE)
                                 * is also validated.
                                 */

                                return ISS_FAILURE;
                            }
                            /* In case, the new Vlan list contains a VlanId
                             * that is already given by user , test routine
                             * returns error.
                             *To avoid that the following check is done.
                             */

                            if (nmhGetFsErpsVlanGroupRowStatus
                                (u4ContextId, i4VlanId, i4VlanGroupId,
                                 &i4VlanGroupRowStat) != SNMP_SUCCESS)
                            {
                                return ISS_FAILURE;
                            }
                        }
                    }
                    u2VlanFlag = (UINT2) (u2VlanFlag << 1);
                }
            }
        }
        i4VlanGroupRowStat = 0;
        i4VlanId = 0;
        u2VlanFlag = 0;

        /* Set the values */
        for (u2ByteIndex = 0; u2ByteIndex < VLAN_LIST_SIZE; u2ByteIndex++)
        {
            if (pu4VlanList[u2ByteIndex] != 0)
            {
                u2VlanFlag = pu4VlanList[u2ByteIndex];

                for (u2BitIndex = 0;
                     (u2BitIndex < BITS_PER_BYTE && u2VlanFlag != 0);
                     u2BitIndex++)
                {
                    if ((u2VlanFlag & VLAN_BIT8) != 0)
                    {
                        i4VlanId =
                            (UINT2) ((u2ByteIndex * BITS_PER_BYTE) +
                                     u2BitIndex + 1);

                        if (VLAN_IS_VLAN_ID_VALID (i4VlanId) == OSIX_FALSE)
                        {
                            u2VlanFlag = (UINT2) (u2VlanFlag << 1);
                            continue;
                        }

                        if (nmhSetFsErpsVlanGroupRowStatus
                            (u4ContextId, i4VlanId, i4VlanGroupId,
                             CREATE_AND_GO) == SNMP_FAILURE)
                        {
                            return ISS_FAILURE;
                        }
                    }
                    u2VlanFlag = (UINT2) (u2VlanFlag << 1);
                }
            }
        }
    }
    return ISS_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME    : IssProcessErpsRingVlanTableModify
 *
 * DESCRIPTION      : This Routine modifies the ring vlan table
 *
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           :
 *
 * RETURNS          : ISS_SUCCESS/ISS_FAILURE
 *
 **************************************************************************/
INT4
IssProcessErpsRingVlanTableModify (UINT4 u4ContextId,
                                   INT4 i4VlanGroupId, UINT1 *pu1VlanList)
{

    UINT4               u4NextContextId = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4NextVlanGroupId = 0;
    INT4                i4CurrVlanGroupId = i4VlanGroupId;
    INT4                i4VlanId = 0;
    INT4                i4NextVlanId = 0;
    INT4                i4VlanCount = 0;
    BOOL1               bResult = OSIX_FALSE;

    MEMSET (&gau1ErpsVlanDelList[0], 0, VLAN_LIST_SIZE);
    /* Scan the ERPS Vlan Group Table , Get the VLAN Id, and check if the bit
     * corresponding to the vlan id is set in pu1VlanList.
     * If bit is set in VlanList then reset the bit as this vlan is already
     * present in the VlanGroup Table.
     * If bit is not set in VlanList, then set a bit in au1VlanDeleteList
     * and test with DESTROY option for the vlan id */

    if (pu1VlanList != NULL)
    {
        while (nmhGetNextIndexFsErpsVlanGroupTable
               (u4ContextId, &u4NextContextId, i4VlanId, &i4NextVlanId,
                i4VlanGroupId, &i4NextVlanGroupId) == SNMP_SUCCESS)

        {
            /* Break from Loop if context Id does not match, as context Id is the
             * primary Index */

            if (u4ContextId != (UINT4) u4NextContextId)
            {
                break;
            }

            /* Continue to Loop if Group Id does not match, as many more
             * Vlans can be mapped to the same group Id (Vlan Id is the
             * secondary index and Group Id is the 3rd Index to vlan
             * group table) */

            if (i4CurrVlanGroupId != i4NextVlanGroupId)
            {
                i4VlanId = i4NextVlanId;
                i4VlanGroupId = i4NextVlanGroupId;
                continue;
            }

            OSIX_BITLIST_IS_BIT_SET (pu1VlanList,
                                     i4NextVlanId, VLAN_LIST_SIZE, bResult);

            if (bResult == OSIX_TRUE)
            {
                OSIX_BITLIST_RESET_BIT (pu1VlanList, i4NextVlanId,
                                        VLAN_LIST_SIZE);
            }
            else
            {
                /* Calling the Test routine to validate the vlan group
                 * to be deleted */
                if (nmhTestv2FsErpsVlanGroupRowStatus
                    (&u4ErrorCode, u4ContextId, i4NextVlanId, i4NextVlanGroupId,
                     DESTROY) == SNMP_FAILURE)
                {
                    return ISS_FAILURE;
                }
                OSIX_BITLIST_SET_BIT (gau1ErpsVlanDelList, i4NextVlanId,
                                      VLAN_LIST_SIZE);
            }

            i4VlanId = i4NextVlanId;
            i4VlanGroupId = i4NextVlanGroupId;
        }

        bResult = OSIX_FALSE;

        /* Test the list of all VLANS to be added in pu1VlanList
         */
        for (i4VlanCount = 0; i4VlanCount <= VLAN_MAX_VLAN_ID; i4VlanCount++)
        {
            /* This check is to ensure that invalid vlan ids are not
             * accessed for creation.*/
            OSIX_BITLIST_IS_BIT_SET (pu1VlanList,
                                     i4VlanCount, VLAN_LIST_SIZE, bResult);
            if (bResult == OSIX_TRUE)
            {
                if (nmhTestv2FsErpsVlanGroupRowStatus
                    (&u4ErrorCode, u4ContextId, i4VlanCount,
                     i4CurrVlanGroupId, CREATE_AND_GO) == SNMP_FAILURE)
                {
                    return ISS_FAILURE;
                }
            }
        }

        bResult = OSIX_FALSE;

        /* Set the list of all VLANS to be added in pu1VlanList.
         * Delete the list of all VLANS in gau1ErpsVlanDelList
         */
        for (i4VlanCount = 0; i4VlanCount <= VLAN_MAX_VLAN_ID; i4VlanCount++)
        {
            OSIX_BITLIST_IS_BIT_SET (pu1VlanList,
                                     i4VlanCount, VLAN_LIST_SIZE, bResult);
            if (bResult == OSIX_TRUE)
            {
                if (nmhSetFsErpsVlanGroupRowStatus (u4ContextId,
                                                    i4VlanCount,
                                                    i4CurrVlanGroupId,
                                                    CREATE_AND_GO) ==
                    SNMP_FAILURE)
                {
                    return ISS_FAILURE;
                }
            }
        }
        for (i4VlanCount = 0; i4VlanCount <= VLAN_MAX_VLAN_ID; i4VlanCount++)
        {
            bResult = OSIX_FALSE;

            OSIX_BITLIST_IS_BIT_SET (gau1ErpsVlanDelList,
                                     i4VlanCount, VLAN_LIST_SIZE, bResult);
            if (bResult == OSIX_TRUE)
            {
                if (nmhSetFsErpsVlanGroupRowStatus (u4ContextId,
                                                    i4VlanCount,
                                                    i4CurrVlanGroupId,
                                                    DESTROY) == SNMP_FAILURE)
                {
                    return ISS_FAILURE;
                }
            }

        }
    }

    return ISS_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : IssProcessErpsRingVlanTableDelete
 *
 * DESCRIPTION      : This Routine deletes the vlan list
 *                    
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           :
 *
 * RETURNS          : ISS_SUCCESS/ISS_FAILURE
 *
 **************************************************************************/
INT4
IssProcessErpsRingVlanTableDelete (UINT4 u4ContextId,
                                   INT4 i4VlanGroupId, UINT1 *pu1VlanList)
{

    UINT4               u4ErrorCode = 0;
    INT4                i4VlanId = 0;
    UINT2               u2ByteIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2VlanFlag = 0;

    /* Configure the VLAN list entries only if they are given as input
     * by user.
     * If no VLAN list is given return success */
    if (pu1VlanList != NULL)
    {
        /* Test each and every VLAN list value. */

        for (u2ByteIndex = 0; u2ByteIndex < VLAN_LIST_SIZE; u2ByteIndex++)
        {
            if (pu1VlanList[u2ByteIndex] != 0)
            {
                u2VlanFlag = pu1VlanList[u2ByteIndex];

                for (u2BitIndex = 0;
                     (u2BitIndex < BITS_PER_BYTE && u2VlanFlag != 0);
                     u2BitIndex++)
                {
                    if ((u2VlanFlag & VLAN_BIT8) != 0)
                    {
                        i4VlanId =
                            (UINT2) ((u2ByteIndex * BITS_PER_BYTE) +
                                     u2BitIndex + 1);

                        if (VLAN_IS_VLAN_ID_VALID (i4VlanId) == OSIX_FALSE)
                        {
                            u2VlanFlag = (UINT2) (u2VlanFlag << 1);
                            continue;
                        }
                        /* This check is to ensure that invalid vlan ids are not
                         * accessed for deletion.*/
                        if (nmhTestv2FsErpsVlanGroupRowStatus
                            (&u4ErrorCode, u4ContextId, i4VlanId,
                             i4VlanGroupId, DESTROY) == SNMP_FAILURE)
                        {
                            return ISS_FAILURE;
                        }
                    }
                    u2VlanFlag = (UINT2) (u2VlanFlag << 1);
                }
            }
        }
        /*Only if test is success,remove the vlans */
        for (u2ByteIndex = 0; u2ByteIndex < VLAN_LIST_SIZE; u2ByteIndex++)
        {
            if (pu1VlanList[u2ByteIndex] != 0)
            {
                u2VlanFlag = pu1VlanList[u2ByteIndex];

                for (u2BitIndex = 0;
                     (u2BitIndex < BITS_PER_BYTE && u2VlanFlag != 0);
                     u2BitIndex++)
                {
                    if ((u2VlanFlag & VLAN_BIT8) != 0)
                    {
                        i4VlanId =
                            (UINT2) ((u2ByteIndex * BITS_PER_BYTE) +
                                     u2BitIndex + 1);

                        if (VLAN_IS_VLAN_ID_VALID (i4VlanId) == OSIX_FALSE)
                        {
                            u2VlanFlag = (UINT2) (u2VlanFlag << 1);
                            continue;
                        }

                        if (nmhSetFsErpsVlanGroupRowStatus (u4ContextId,
                                                            i4VlanId,
                                                            i4VlanGroupId,
                                                            DESTROY) ==
                            SNMP_FAILURE)
                        {
                            return ISS_FAILURE;
                        }
                    }
                    u2VlanFlag = (UINT2) (u2VlanFlag << 1);
                }
            }
        }
    }
    return ISS_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ErpsDelAllTopologyList 
 *
 * DESCRIPTION      : This Routine deletes the entire TC propagation list
 *                    for the ring.
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           : 
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ErpsDelAllTopologyList (UINT4 u4ContextId, UINT4 u4RingId)
{
    UINT4               u4NextContextId = u4ContextId;
    UINT4               u4NextRingId = 0;
    UINT4               u4TcProgId = 0;
    UINT4               u4NextTcProgId = 0;
    UINT4               u4ErrorCode = 0;

    /* Existing values in the RING TC propagation list are deleted */
    while (nmhGetNextIndexFsErpsRingTcPropTable (u4ContextId, &u4NextContextId,
                                                 u4RingId, &u4NextRingId,
                                                 u4TcProgId, &u4NextTcProgId)
           == SNMP_SUCCESS)

    {
        if ((u4ContextId != (UINT4) u4NextContextId) ||
            (u4RingId != u4NextRingId))
        {
            break;
        }

        /* u4NextTcProgId is deleted now from the TC propagation list */

        if (nmhTestv2FsErpsRingTcPropRowStatus (&u4ErrorCode, u4ContextId,
                                                u4NextRingId, u4NextTcProgId,
                                                DESTROY) == SNMP_FAILURE)
        {
            return ISS_FAILURE;
        }
        if (nmhSetFsErpsRingTcPropRowStatus (u4ContextId, u4NextRingId,
                                             u4NextTcProgId, DESTROY)
            == SNMP_FAILURE)
        {
            return ISS_FAILURE;
        }

        u4TcProgId = u4NextTcProgId;
    }

    return ISS_SUCCESS;
}
#endif
#endif
