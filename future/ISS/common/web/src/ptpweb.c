/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpweb.c,v 1.11 2014/05/28 12:14:14 siva Exp $  
 *
 * Description: This file contains the routines required for PTP 
 *              web module. 
 *******************************************************************/

#ifndef _PTP_WEB_C_
#define _PTP_WEB_C_

#ifdef WEBNM_WANTED

#include "webiss.h"
#include "isshttp.h"
#include "vcm.h"
#include "ptp.h"
#include "ptpweb.h"
#include "../../ptp/inc/fsptplw.h"
#include "../../ptp/inc/ptpconst.h"
#define PTP_MAIN_PAGE "/iss/specific/ptp_globalconf.html"

/*******************************************************************************
*  Function Name : IssProcessPtpGlobalConfPage
*  Description   : This function processes the HTTP request coming for the
*                  Ptp Global configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None.
*******************************************************************************/
VOID
IssProcessPtpGlobalConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessPtpGlobalConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessPtpGlobalConfPageSet (pHttp);
    }
    return;
}

/*******************************************************************************
 *  Function Name : IssProcessPtpGlobalConfPageGet
 *  Description   : This function processes the get request for the Ptp
 *                  Global configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessPtpGlobalConfPageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE PtpNotification;
    INT4                i4RetSysStatus = 0;
    INT4                i4PrimaryCxtId = 0;
    UINT1               au1Trap[PTP_TRAP_MAX_LEN];
    UINT1               u1Val = 0;

    PtpNotification.pu1_OctetList = &(au1Trap[0]);
    PtpNotification.i4_Length = sizeof (au1Trap);

    MEMSET (&(au1Trap[0]), 0, sizeof (au1Trap));
    pHttp->i4Write = 0;

    STRCPY (pHttp->au1KeyString, "GLOBAL_STATUS_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    i4RetSysStatus = 0;
    nmhGetFsPtpGlobalSysCtrl (&i4RetSysStatus);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetSysStatus);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "PRIMARY_CONTEXT_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsPtpPrimaryContext (&i4PrimaryCxtId);
    PtpPortVcmGetAliasName ((UINT4) i4PrimaryCxtId, pHttp->au1DataString);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    nmhGetFsPtpNotification (&PtpNotification);

    u1Val = PtpNotification.pu1_OctetList[0];

    if ((u1Val & PTP_TRAP_MASK_GLOB_ERR) == PTP_TRAP_MASK_GLOB_ERR)
    {
        STRCPY (pHttp->au1KeyString, "TRAP_GLB_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", PTP_ENABLED);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
    }

    if ((u1Val & PTP_TRAP_MASK_SYS_CTRL_CHNG) == PTP_TRAP_MASK_SYS_CTRL_CHNG)
    {
        STRCPY (pHttp->au1KeyString, "TRAP_SYS_CTRL_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", PTP_ENABLED);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
    }

    if ((u1Val & PTP_TRAP_MASK_SYS_ADMIN_CHNG) == PTP_TRAP_MASK_SYS_ADMIN_CHNG)
    {

        STRCPY (pHttp->au1KeyString, "TRAP_SYS_ADMIN_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", PTP_ENABLED);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
    }

    if ((u1Val & PTP_TRAP_MASK_PORT_STATE_CHNG) ==
        PTP_TRAP_MASK_PORT_STATE_CHNG)
    {
        STRCPY (pHttp->au1KeyString, "TRAP_PORT_STATE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", PTP_ENABLED);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
    }

    if ((u1Val & PTP_TRAP_MASK_PORT_ADMIN_CHNG) ==
        PTP_TRAP_MASK_PORT_ADMIN_CHNG)
    {
        STRCPY (pHttp->au1KeyString, "TRAP_PORT ADMIN_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", PTP_ENABLED);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
    }

    if ((u1Val & PTP_TRAP_MASK_SYNC_FAULT) == PTP_TRAP_MASK_SYNC_FAULT)
    {
        STRCPY (pHttp->au1KeyString, "TRAP_SYNC_FAULT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", PTP_ENABLED);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
    }

    if ((u1Val & PTP_TRAP_MASK_GM_FAULT) == PTP_TRAP_MASK_GM_FAULT)
    {
        STRCPY (pHttp->au1KeyString, "TRAP_GM_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", PTP_ENABLED);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
    }

    if ((u1Val & PTP_TRAP_MASK_ACC_MASTER_FAULT) ==
        PTP_TRAP_MASK_ACC_MASTER_FAULT)
    {
        STRCPY (pHttp->au1KeyString, "TRAP_ACC_MASTER_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", PTP_ENABLED);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
    }

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

}

/*******************************************************************************
 *  Function Name : IssProcessPtpGlobalConfPageSet
 *  Description   : This function processes the get request for the Ptp
 *                  Global configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessPtpGlobalConfPageSet (tHttp * pHttp)
{
    INT4                i4Status = 0;
    INT4                i4PrimaryContext = 0;;

    WebnmRegisterLock (pHttp, PtpApiLock, PtpApiUnLock);
    PtpApiLock ();

    STRCPY (pHttp->au1Name, "GLOBAL_STATUS");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Status = ATOI (pHttp->au1Value);
    }
    /* Set the Global Sys Control */
    PtpWebSetGlobalSysControl (i4Status);

    STRCPY (pHttp->au1Name, "PRIMARY_CONTEXT");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);

        if (VcmIsSwitchExist (pHttp->au1Value,
                              (UINT4 *) &i4PrimaryContext) == VCM_FALSE)
        {
            PtpApiUnLock ();;
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Please enter a valid context !");
            return;
        }

    }
    /* Set the Primary Context */
    PtpWebSetPrimaryContext (i4PrimaryContext);

    STRCPY (pHttp->au1Name, "ENABLED_TRAPS");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Status = ATOI (pHttp->au1Value);
    }

    /* Set Enabled Traps */
    PtpWebSetNotifyStatus ((UINT4) i4Status, PTP_ENABLED);

    STRCPY (pHttp->au1Name, "DISABLED_TRAPS");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Status = ATOI (pHttp->au1Value);
    }
    /* Set Disabled Traps */
    PtpWebSetNotifyStatus ((UINT4) i4Status, PTP_DISABLED);

    PtpApiUnLock ();;
    WebnmUnRegisterLock (pHttp);
    IssProcessPtpGlobalConfPageGet (pHttp);

}

/*******************************************************************************
*  Function Name : IssProcessPtpClockPage 
*  Description   : This function processes the request coming for the Ptp If 
*                  Page.                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssProcessPtpClockPage (tHttp * pHttp)
{
    INT4                i4PtpSysCtrl = 0;

    /* If PTP Module is shutdown do not display the page */

    nmhGetFsPtpGlobalSysCtrl (&i4PtpSysCtrl);

    if (i4PtpSysCtrl == PTP_SHUTDOWN)
    {
        IssSendSpecificError (pHttp,
                              (CONST INT1 *) "PTP is shutdown, Please Start !",
                              (INT1 *) PTP_MAIN_PAGE);
        return;
    }

    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessPtpClockSet (pHttp);
    }
}

/******************************************************************************
*  Function Name : IssProcessPtpIfSet
*  Description   : This function gets the user input from the web page and 
*                  enables or disables PTP on a VLAN Interface.                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
******************************************************************************/

VOID
IssProcessPtpClockSet (tHttp * pHttp)
{
    INT4                i4ContextId = 0;
    INT4                i4DomainId = 0;
    INT4                i4NumPorts = 0;
    INT4                i4ClockMode = 0;
    INT4                i4Priority = 0;
    INT4                i4Flag = 0;

    /* Get the interface name to be set from the form and get
     * the interface index from it */

    WebnmRegisterLock (pHttp, PtpApiLock, PtpApiUnLock);
    PtpApiLock ();

    STRCPY (pHttp->au1Name, "DOMAIN_NO");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4DomainId = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "CONTEXT_NAME");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4ContextId = ATOI (pHttp->au1Value);
    }

    /* Get the Processing to be done (Add or delete) from the form */

    STRCPY (pHttp->au1Name, "ACTION");

    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Value, "DELETE") == 0)
    {
        nmhSetFsPtpDomainRowStatus (i4ContextId, i4DomainId, DESTROY);
        PtpApiUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssProcessGet (pHttp);
        return;
    }

    /* if it is Not Delete option then convert 
     * context name to context id */

    STRCPY (pHttp->au1Name, "CONTEXT_NAME");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);

        if (VcmIsSwitchExist (pHttp->au1Value,
                              (UINT4 *) &i4ContextId) == VCM_FALSE)
        {
            PtpApiUnLock ();;
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Please enter a valid context !");
            return;
        }

    }

    /* If Clock does not exists configure clock */
    if (nmhValidateIndexInstanceFsPtpDomainDataSetTable
        (i4ContextId, i4DomainId) == SNMP_FAILURE)
    {
        PtpWebConfigureContext (i4ContextId, i4DomainId);
    }
    /* After Configuring the Context Set the Clock Mode 
     * and other parameters */

    STRCPY (pHttp->au1Name, "CLOCK_MODE");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4ClockMode = ATOI (pHttp->au1Value);
    }

    PtpWebSetClkMode (i4ContextId, i4DomainId, i4ClockMode);

    STRCPY (pHttp->au1Name, "NUMPORTS");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4NumPorts = ATOI (pHttp->au1Value);
    }
    if (i4ClockMode == PTP_OC_CLOCK_MODE)
    {
        PtpWebSetClockNumberPorts (i4ContextId, i4DomainId, 1);
    }
    else
    {
        PtpWebSetClockNumberPorts (i4ContextId, i4DomainId, i4NumPorts);
    }

    STRCPY (pHttp->au1Name, "PTP_ADMIN");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Flag = ATOI (pHttp->au1Value);
    }

    PtpWebSetSystemControl (i4ContextId, i4Flag);

    STRCPY (pHttp->au1Name, "PRIMARY_DOMAIN_FLAG");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Flag = ATOI (pHttp->au1Value);
    }
    if (i4Flag == PTP_ENABLED)
    {
        PtpWebSetPrimaryDomain (i4ContextId, i4DomainId);
    }

    if ((i4ClockMode == PTP_P2P_TC_CLOCK_MODE) ||
        (i4ClockMode == PTP_E2E_TC_CLOCK_MODE) ||
        (i4ClockMode == PTP_FORWARD_MODE))
    {
        /* if mode is forward or tranparent do not configure 
           other parameters */

        PtpApiUnLock ();;
        WebnmUnRegisterLock (pHttp);
        IssProcessGet (pHttp);
        return;
    }

    /* Configure other parameters if and only if 
     * clock is Boundary or Ordinary*/

    if ((i4ClockMode == PTP_BOUNDARY_CLOCK_MODE)
        || (i4ClockMode == PTP_OC_CLOCK_MODE))
    {

        STRCPY (pHttp->au1Name, "PRIORITY1");

        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            i4Priority = ATOI (pHttp->au1Value);
        }
        PtpWebSetClkPriority1 (i4ContextId, i4DomainId, i4Priority);

        STRCPY (pHttp->au1Name, "PRIORITY2");

        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            i4Priority = ATOI (pHttp->au1Value);
        }
        PtpWebSetClkPriority2 (i4ContextId, i4DomainId, i4Priority);

        STRCPY (pHttp->au1Name, "TWOSTEP_FLAG");

        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            i4Flag = ATOI (pHttp->au1Value);
        }
        if (PtpWebSetTwoStepFlag (i4ContextId, i4DomainId, i4Flag)
            == OSIX_FAILURE)
        {
            PtpApiUnLock ();;
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Two Step Clock configuraion failed !");
            return;
        }

        STRCPY (pHttp->au1Name, "SLAVE_MODE");

        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            i4Flag = ATOI (pHttp->au1Value);
        }
        PtpWebSetClkSlaveOnly (i4ContextId, i4DomainId, i4Flag);

        STRCPY (pHttp->au1Name, "PATH_TRACE");

        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            i4Flag = ATOI (pHttp->au1Value);
        }
        PtpWebSetClkPathTrace (i4ContextId, i4DomainId, i4Flag);

    }

    PtpApiUnLock ();;
    WebnmUnRegisterLock (pHttp);
    IssProcessGet (pHttp);

}

/*******************************************************************************
*  Function Name : IssProcessPtpIfPage 
*  Description   : This function processes the request coming for the Ptp If 
*                  Page.                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssProcessPtpIfPage (tHttp * pHttp)
{
    INT4                i4PtpSysCtrl = 0;

    /* If PTP Module is shutdown do not display the page */

    nmhGetFsPtpGlobalSysCtrl (&i4PtpSysCtrl);

    if (i4PtpSysCtrl == PTP_SHUTDOWN)
    {
        IssSendSpecificError (pHttp,
                              (CONST INT1 *) "PTP is shutdown, Please Start !",
                              (INT1 *) PTP_MAIN_PAGE);
        return;
    }

    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessPtpIfGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessPtpIfSet (pHttp);
    }
}

/*******************************************************************************
*  Function Name : IssProcessPtpIfGet
*  Description   : This function gets and displays all the PTP Interfaces 
*                  enabled and the associated vlan interface in the web page
*                  and enables the user to enable PTP on a vlan interface.                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/

VOID
IssProcessPtpIfGet (tHttp * pHttp)
{
    IssPrintL3Interfaces (pHttp, 0);
    IssPrintAvailableL2Ports (pHttp);
    IssPrintAvailableVlans (pHttp);

    /* Display all the information in the html page 
     * after  printing revelant info*/

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/******************************************************************************
*  Function Name : IssProcessPtpIfSet
*  Description   : This function gets the user input from the web page and 
*                  enables or disables PTP on a VLAN Interface.                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
******************************************************************************/

VOID
IssProcessPtpIfSet (tHttp * pHttp)
{
    UINT4               u4IfaceIndex = 0;
    INT4                i4ContextId = 0;
    INT4                i4DomainId = 0;
    INT4                i4PtpPortNo = 0;
    INT4                i4RetStatus = 0;
    INT4                i4ClockMode = 0;
    INT4                i4InterfaceType = 0;
    INT4                i4Interval = 0;
    INT4                i4MaxAltMasters = 0;
    INT4                i4Version = 0;
    INT4                i4Flag = 0;

    /* Get the interface name to be set from the form and get
     * the interface index from it */

    WebnmRegisterLock (pHttp, PtpApiLock, PtpApiUnLock);
    PtpApiLock ();

    STRCPY (pHttp->au1Name, "DOMAIN_NO");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4DomainId = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "PORT_ID");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4PtpPortNo = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Value, "DELETE") == 0)
    {

        STRCPY (pHttp->au1Name, "CONTEXT_NAME");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            i4ContextId = ATOI (pHttp->au1Value);

        }
        nmhSetFsPtpPortRowStatus (i4ContextId, i4DomainId, i4PtpPortNo,
                                  DESTROY);

        PtpApiUnLock ();;
        WebnmUnRegisterLock (pHttp);
        IssSendResponse (pHttp,
                         (INT1 *) "<font color=black> Port Deleted ! "
                         "goto Port info page for more details</font><br> <br><input type"
                         "=button name=show value="
                         "\"Show All PTP Ports\" onClick=\'parent.frames[\"DataFrame\"].location="
                         "\"/iss/ptp_portinfo.html?Gambit=GAMBIT\"\'>");
        return;
    }

    else if (STRCMP (pHttp->au1Value, "TRANS_DELETE") == 0)
    {

        STRCPY (pHttp->au1Name, "CONTEXT_NAME");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            i4ContextId = ATOI (pHttp->au1Value);

        }
        /*delete ptp  interface */

        nmhSetFsPtpTransparentPortRowStatus (i4ContextId, i4DomainId,
                                             i4PtpPortNo, DESTROY);

        PtpApiUnLock ();;
        WebnmUnRegisterLock (pHttp);
        IssSendResponse (pHttp,
                         (INT1 *) "<font color=black> Port Deleted ! "
                         "goto Port info page for more details</font><br> <br><input type"
                         "=button name=show value="
                         "\"Show All PTP Ports\" onClick=\'parent.frames[\"DataFrame\"].location="
                         "\"/iss/ptp_portinfo.html?Gambit=GAMBIT\"\'>");
        return;
    }

    else
    {

        STRCPY (pHttp->au1Name, "CONTEXT_NAME");

        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);

            if (VcmIsSwitchExist (pHttp->au1Value,
                                  (UINT4 *) &i4ContextId) == VCM_FALSE)
            {
                PtpApiUnLock ();;
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *) "Please enter a valid context !");
                return;
            }

        }
        /* Port has to be Mapped to that interface */

        STRCPY (pHttp->au1Name, "INTERFACE_TYPE");

        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            i4InterfaceType = ATOI (pHttp->au1Value);
        }

        if ((i4InterfaceType == PTP_IFACE_UDP_IPV4) ||
            (i4InterfaceType == PTP_IFACE_UDP_IPV6))
        {
            STRCPY (pHttp->au1Name, "L3INTERFACE");

            if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery) == ENM_SUCCESS)
            {
                issDecodeSpecialChar (pHttp->au1Value);

                if (CfaGetInterfaceIndexFromName
                    (pHttp->au1Value, &u4IfaceIndex) == OSIX_FAILURE)
                {
                    PtpApiUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Wrong Interface or Interface is invalid");
                    return;
                }
            }
        }
        else if (i4InterfaceType == PTP_IFACE_IEEE_802_3)
        {
            STRCPY (pHttp->au1Name, "L2INTERFACE");

            if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery) == ENM_SUCCESS)
            {
                issDecodeSpecialChar (pHttp->au1Value);
                u4IfaceIndex = (UINT4) ATOI (pHttp->au1Value);
            }
        }
        else if (i4InterfaceType == PTP_IFACE_VLAN)
        {

            STRCPY (pHttp->au1Name, "L2VLAN");
            if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery) == ENM_SUCCESS)
            {
                issDecodeSpecialChar (pHttp->au1Value);
                u4IfaceIndex = (UINT4) ATOI (pHttp->au1Value);
            }

        }
        else
        {
            PtpApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Wrong Interface or Interface is invalid");
            return;
        }

        /* Get the Clock Mode and then Delete or Add  the respective Port */

        i4RetStatus = nmhGetFsPtpDomainClockMode (i4ContextId, i4DomainId,
                                                  &i4ClockMode);
        if (i4RetStatus != SNMP_SUCCESS)
        {
            PtpApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Invalid Domain / Context");
            return;
        }

        /* If Port Already exists dont create port 
         * just change what ever values configured */

        if (PtpIfGetPortNumber (i4ContextId, (UINT1) i4DomainId,
                                i4InterfaceType,
                                u4IfaceIndex, &i4PtpPortNo) != OSIX_SUCCESS)
        {
            /* Interface is not mapped to PTP */

            PtpIfGetNextAvailableIndex ((UINT4) i4ContextId, (UINT1) i4DomainId,
                                        (UINT4 *) &i4PtpPortNo);

            if (i4ClockMode == PTP_TRANSPARENT_CLOCK_MODE)
            {
                i4RetStatus = PtpWebMapPortToTransClk (i4ContextId,
                                                       i4DomainId,
                                                       i4InterfaceType,
                                                       (INT4) u4IfaceIndex,
                                                       i4PtpPortNo);
            }
            else
            {
                i4RetStatus = PtpWebMapPortToClk (i4ContextId, i4DomainId,
                                                  i4InterfaceType,
                                                  (INT4) u4IfaceIndex,
                                                  i4PtpPortNo);
            }

            if (i4RetStatus == OSIX_FAILURE)
            {
                PtpApiUnLock ();;
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Mapping port to PTP clock failed ,"
                              " Please check number of ports");
                return;
            }
        }

        /* After Configuring Port (Adding) Configure Intervals */

        STRCPY (pHttp->au1Name, "VERSION");

        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            i4Version = ATOI (pHttp->au1Value);
        }

        if ((i4Version == PTP_VERSION_ONE)
            && (i4ClockMode != PTP_BOUNDARY_CLOCK_MODE))
        {
            PtpApiUnLock ();;
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Version 1 can only be set for Boundary clock");
            return;
        }

        PtpWebSetVersion (i4ContextId, i4DomainId, i4PtpPortNo, i4Version);

        STRCPY (pHttp->au1Name, "SYNC_INTVL");

        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            i4Interval = ATOI (pHttp->au1Value);
        }
        i4RetStatus = PtpWebSetSyncInterval (i4ContextId,
                                             i4DomainId, i4PtpPortNo,
                                             i4Interval);

        STRCPY (pHttp->au1Name, "ANNOUNCE_INTVL");

        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            i4Interval = ATOI (pHttp->au1Value);
        }

        PtpWebSetAnnounceInterval (i4ContextId, i4DomainId, i4PtpPortNo,
                                   i4Interval);

        STRCPY (pHttp->au1Name, "ANNOUNCE_TIMEOUT");

        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            i4Interval = ATOI (pHttp->au1Value);
        }
        PtpWebSetAnnounceTimeOut (i4ContextId, i4DomainId, i4PtpPortNo,
                                  i4Interval);

        STRCPY (pHttp->au1Name, "DELAY_MECH");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            i4Flag = ATOI (pHttp->au1Value);
        }
        PtpWebSetDelayType (i4ContextId, i4DomainId, i4PtpPortNo, i4Flag);

        STRCPY (pHttp->au1Name, "PDELAY_INTVL");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            i4Interval = ATOI (pHttp->au1Value);
        }
        if (i4ClockMode == PTP_TRANSPARENT_CLOCK_MODE)
        {
            PtpWebSetDelayReqInterval (i4ContextId, i4DomainId, i4PtpPortNo,
                                       i4Interval, OSIX_TRUE);
        }
        else
        {
            PtpWebSetDelayReqInterval (i4ContextId, i4DomainId, i4PtpPortNo,
                                       i4Interval, OSIX_FALSE);
        }
        STRCPY (pHttp->au1Name, "SYNC_LIMIT");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            i4Flag = ATOI (pHttp->au1Value);
        }
        /* Set Value for Sync Limit */

        STRCPY (pHttp->au1Name, "PTP_STATUS");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            i4Flag = ATOI (pHttp->au1Value);
        }
        PtpWebSetPortAdminStaus (i4ContextId, i4DomainId, (UINT4) i4PtpPortNo,
                                 i4Flag);

        STRCPY (pHttp->au1Name, "ALT_MASTER_STATUS");

        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            i4Flag = ATOI (pHttp->au1Value);
        }
        PtpWebSetAltMasterFlag (i4ContextId, i4DomainId, i4PtpPortNo, i4Flag);

        STRCPY (pHttp->au1Name, "ALT_MULTISYNC_INTERVAL");

        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            i4Interval = ATOI (pHttp->au1Value);
        }
        PtpWebSetAltMcastSyncInterval (i4ContextId, i4DomainId, i4PtpPortNo,
                                       i4Interval);

        STRCPY (pHttp->au1Name, "MAX_ALT_MASTERS");

        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            i4MaxAltMasters = ATOI (pHttp->au1Value);
        }

        PtpWebSetPortNumOfAlternateMasters (i4ContextId, i4DomainId,
                                            i4PtpPortNo, i4MaxAltMasters);

        STRCPY (pHttp->au1Name, "ACCEPT_MASTER_STATUS");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            i4Flag = ATOI (pHttp->au1Value);
        }
        PtpWebSetAccMasterFlag (i4ContextId, i4DomainId, i4PtpPortNo, i4Flag);
    }
    PtpApiUnLock ();
    WebnmUnRegisterLock (pHttp);

    IssSendResponse (pHttp,
                     (INT1 *) "<font color=black> Port Successfully added , "
                     "goto Port info page for more details</font><br> <br><input type"
                     "=button name=show value="
                     "\"Show All PTP Ports\" onClick=\'parent.frames[\"DataFrame\"].location="
                     "\"/iss/ptp_portinfo.html?Gambit=GAMBIT\"\'>");

}

/*******************************************************************************
*  Function Name : IssProcessPtpAccMasterPage 
*  Description   : This function processes the request coming for the Ptp Acc
*                  Master Page.                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssProcessPtpAccMasterPage (tHttp * pHttp)
{
    INT4                i4PtpSysCtrl = 0;

    /* If PTP Module is shutdown do not display the page */
    nmhGetFsPtpGlobalSysCtrl (&i4PtpSysCtrl);

    if (i4PtpSysCtrl == PTP_SHUTDOWN)
    {
        IssSendSpecificError (pHttp,
                              (CONST INT1 *) "PTP is shutdown, Please Start !",
                              (INT1 *) PTP_MAIN_PAGE);
        return;
    }

    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessPtpAccMasterGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessPtpAccMasterSet (pHttp);
    }
}

/*******************************************************************************
*  Function Name : IssProcessPtpAltTimePage 
*  Description   : This function processes the request coming for the Ptp Alt
*                  Time Page.                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssProcessPtpAltTimePage (tHttp * pHttp)
{
    INT4                i4PtpSysCtrl = 0;

    /* If PTP Module is shutdown do not display the page */

    nmhGetFsPtpGlobalSysCtrl (&i4PtpSysCtrl);

    if (i4PtpSysCtrl == PTP_SHUTDOWN)
    {
        IssSendSpecificError (pHttp,
                              (CONST INT1 *) "PTP is shutdown, Please Start !",
                              (INT1 *) PTP_MAIN_PAGE);
        return;
    }

    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessPtpAltTimeSet (pHttp);
    }
}

/******************************************************************************
*  Function Name : IssProcessPtpAccMasterSet
*  Description   : This function gets the user input from the web and configures
*                  Acc Master Data Set                    
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
******************************************************************************/

VOID
IssProcessPtpAccMasterSet (tHttp * pHttp)
{
    tIp6Addr            AccAddr;
    INT4                i4ContextId = 0;
    INT4                i4DomainId = 0;
    INT4                i4Proto = 0;
    INT4                i4Priority = 0;
    INT4                i4RetStatus = 0;
    UINT1               au1IpAddr[PTP_MASTER_MAX_ADDR_LEN];
    UINT1              *Pu1Addr = NULL;
    UINT1               au1AccMasterAddr[PTP_MASTER_MAX_ADDR_LEN];

    MEMSET (&AccAddr, 0, sizeof (tIp6Addr));
    MEMSET (&(au1AccMasterAddr[0]), 0, PTP_MASTER_MAX_ADDR_LEN);

    MEMSET (au1IpAddr, 0, sizeof (au1IpAddr));
    Pu1Addr = au1IpAddr;

    WebnmRegisterLock (pHttp, PtpApiLock, PtpApiUnLock);
    PtpApiLock ();

    STRNCPY (pHttp->au1Name, "DOMAIN_NO", (sizeof (pHttp->au1Name) - 1));

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4DomainId = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "NW_PROTOCOL");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Proto = ATOI (pHttp->au1Value);
    }

    /* Get the Processing to be done (Add or delete) from the form */
    /* If Delete Context Number will be passed 
     * instead of Context Name */

    STRCPY (pHttp->au1Name, "CONTEXT_NAME");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4ContextId = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "ACTION");

    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Value, "DELETE") == 0)
    {
        /* If it is deletion only one address in ACM_ADDR 
         * so get it */

        STRCPY (pHttp->au1Name, "ACM_ADDR");

        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            STRNCPY (au1IpAddr, pHttp->au1Value, (sizeof (au1IpAddr) - 1));
        }
        PtpWebDelAccMaster (i4ContextId, i4DomainId, i4Proto,
                            (INT4) STRLEN (au1IpAddr), au1IpAddr);
        PtpApiUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssProcessPtpAccMasterGet (pHttp);
        return;
    }

    /* If Addition - Configure the Acceptable Master Entry */

    STRCPY (pHttp->au1Name, "CONTEXT_NAME");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);

        if (VcmIsSwitchExist (pHttp->au1Value,
                              (UINT4 *) &i4ContextId) == VCM_FALSE)
        {
            PtpApiUnLock ();;
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Please enter a valid context !");
            return;
        }

    }

    if (i4Proto == PTP_IFACE_UDP_IPV4)
    {
        STRCPY (pHttp->au1Name, "ACM_IP_ADDR");

        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            STRNCPY (au1IpAddr, pHttp->au1Value, (sizeof (au1IpAddr) - 1));
        }
    }
    else if (i4Proto == PTP_IFACE_UDP_IPV6)
    {
        STRCPY (pHttp->au1Name, "ACM_IP6_ADDR");

        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            STRNCPY (au1IpAddr, pHttp->au1Value, (sizeof (au1IpAddr) - 1));
        }

        if (INET_ATON6 (pHttp->au1Value, &AccAddr) == 0)
        {
            PtpApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Invalid IPv6 Acceptable Master address");
            return;
        }
    }
    else if (i4Proto == PTP_IFACE_IEEE_802_3)
    {
        STRCPY (pHttp->au1Name, "ACM_MAC_ADDR");

        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            STRNCPY (au1IpAddr, pHttp->au1Value, (sizeof (au1IpAddr) - 1));
        }
    }

    i4RetStatus =
        PtpWebConfigAccMaster (i4ContextId,
                               i4DomainId,
                               i4Proto, (INT4) STRLEN (Pu1Addr),
                               (CHR1 *) Pu1Addr);

    if (i4RetStatus == OSIX_FAILURE)
    {
        PtpApiUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Acceptable master creation failed");
        return;
    }

    /* If Accept Master Priority has to be configured */

    STRCPY (pHttp->au1Name, "ALT_PRIORITY");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Priority = ATOI (pHttp->au1Value);
    }

    i4RetStatus =
        PtpWebSetAccMasterAltPriority (i4ContextId,
                                       i4DomainId,
                                       i4Proto,
                                       (INT4) STRLEN (Pu1Addr),
                                       (UINT1 *) Pu1Addr, i4Priority);

    PtpApiUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessPtpAccMasterGet (pHttp);
    return;
}

/******************************************************************************
*  Function Name : IssProcessPtpAltTimeSet
*  Description   : This function gets the user input from the web and configures
*                  Alternate time Data Set                    
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
******************************************************************************/

VOID
IssProcessPtpAltTimeSet (tHttp * pHttp)
{
    INT4                i4ContextId = 0;
    INT4                i4DomainId = 0;
    INT4                i4KeyId = 0;
    INT4                i4ATSOffset = 0;
    INT4                i4ATSJmpSecs = 0;

    WebnmRegisterLock (pHttp, PtpApiLock, PtpApiUnLock);
    PtpApiLock ();

    STRCPY (pHttp->au1Name, "DOMAIN_NO");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4DomainId = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "CONTEXT_NAME");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4ContextId = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "KEY_INDEX");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4KeyId = ATOI (pHttp->au1Value);
    }
    PtpWebConfigAltTimeScale (i4ContextId, i4DomainId, i4KeyId);
    STRCPY (pHttp->au1Name, "ACTION");

    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Value, "DELETE") == 0)
    {
        nmhSetFsPtpAltTimeScaleRowStatus (i4ContextId, i4DomainId, i4KeyId,
                                          DESTROY);
        PtpApiUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssProcessGet (pHttp);
        return;
    }

    /* If the Action is Delete Context Name will be
     * given as input hence convert it to context id */

    STRCPY (pHttp->au1Name, "CONTEXT_NAME");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);

        if (VcmIsSwitchExist (pHttp->au1Value,
                              (UINT4 *) &i4ContextId) == VCM_FALSE)
        {
            PtpApiUnLock ();;
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Please enter a valid context !");
            return;
        }

    }

    /* If Display Name has to be Configured */
    STRCPY (pHttp->au1Name, "DISP_NAME");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        PtpWebSetAltTimeDispName (i4ContextId,
                                  i4DomainId, i4KeyId, pHttp->au1Value);
    }

    /* If Optional Parameters has to be configured */

    STRCPY (pHttp->au1Name, "OFFSET");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4ATSOffset = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "JUMP_SECS");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4ATSJmpSecs = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "NEXT_JUMP");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
    }

    if (PtpWebUpdateATSEntryOptParams (i4ContextId, i4DomainId, i4KeyId,
                                       i4ATSOffset, i4ATSJmpSecs,
                                       pHttp->au1Value) == OSIX_FAILURE)
    {
        PtpApiUnLock ();;
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Alternate time scale configuration failed");
        return;
    }

    PtpApiUnLock ();;
    WebnmUnRegisterLock (pHttp);
    IssProcessGet (pHttp);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : PtpWebMapPortToTransClk
 *
 * DESCRIPTION      :  This function maps a Port to a PTP Domain for TC
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    
 *
 * OUTPUT           : 
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
PtpWebMapPortToTransClk (INT4 i4ContextId,
                         INT4 i4DomainId, INT4 i4IfType, INT4 i4IfIndex,
                         INT4 i4PtpPortNo)
{

    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpTransparentPortRowStatus (&u4ErrorCode, i4ContextId,
                                                i4DomainId,
                                                i4PtpPortNo,
                                                CREATE_AND_WAIT) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpTransparentPortRowStatus (i4ContextId, i4DomainId,
                                             i4PtpPortNo,
                                             CREATE_AND_WAIT) != SNMP_SUCCESS)
    {

        return OSIX_FAILURE;
    }

    /* Configure Interface Type for PTP Port */

    if (nmhTestv2FsPtpTransparentPortInterfaceType
        (&u4ErrorCode, i4ContextId, i4DomainId, i4PtpPortNo,
         i4IfType) != SNMP_SUCCESS)
    {
        /* Destroy Whenever Failure Occurs */
        nmhSetFsPtpTransparentPortRowStatus (i4ContextId, i4DomainId,
                                             i4PtpPortNo, DESTROY);
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpTransparentPortInterfaceType (i4ContextId, i4DomainId,
                                                 i4PtpPortNo,
                                                 i4IfType) != SNMP_SUCCESS)
    {
        nmhSetFsPtpTransparentPortRowStatus (i4ContextId, i4DomainId,
                                             i4PtpPortNo, DESTROY);

        return OSIX_FAILURE;
    }

    /* Configure Interface Number for PTP Port */

    if (nmhTestv2FsPtpTransparentPortIfaceNumber
        (&u4ErrorCode, i4ContextId, i4DomainId, i4PtpPortNo,
         i4IfIndex) != SNMP_SUCCESS)
    {
        nmhSetFsPtpTransparentPortRowStatus (i4ContextId, i4DomainId,
                                             i4PtpPortNo, DESTROY);
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpTransparentPortIfaceNumber (i4ContextId, i4DomainId,
                                               i4PtpPortNo,
                                               i4IfIndex) != SNMP_SUCCESS)
    {
        nmhSetFsPtpTransparentPortRowStatus (i4ContextId, i4DomainId,
                                             i4PtpPortNo, DESTROY);

        return OSIX_FAILURE;
    }

    /* Make the Port Row Status Active */

    if (nmhSetFsPtpTransparentPortRowStatus (i4ContextId, i4DomainId,
                                             i4PtpPortNo,
                                             ACTIVE) != SNMP_SUCCESS)
    {
        /* If Activation Fails Destroy */

        nmhSetFsPtpTransparentPortRowStatus (i4ContextId, i4DomainId,
                                             i4PtpPortNo, DESTROY);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpWebMapPortToClk
 *
 * DESCRIPTION      :  This function maps a Port to a PTP Domain for OC,BC
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    
 *
 * OUTPUT           : 
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/

PUBLIC INT4
PtpWebMapPortToClk (INT4 i4ContextId, INT4 i4DomainId,
                    INT4 i4IfType, INT4 i4IfIndex, INT4 i4PtpPortNo)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpPortRowStatus (&u4ErrorCode, i4ContextId, i4DomainId,
                                     i4PtpPortNo,
                                     CREATE_AND_WAIT) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpPortRowStatus (i4ContextId, i4DomainId,
                                  i4PtpPortNo, CREATE_AND_WAIT) != SNMP_SUCCESS)
    {

        return OSIX_FAILURE;
    }

    /* Configure Interface Type for PTP Port */

    if (nmhTestv2FsPtpPortInterfaceType (&u4ErrorCode, i4ContextId, i4DomainId,
                                         i4PtpPortNo, i4IfType) != SNMP_SUCCESS)
    {
        /* Destroy Whenever Failure Occurs */
        nmhSetFsPtpPortRowStatus (i4ContextId, i4DomainId,
                                  i4PtpPortNo, DESTROY);
        return OSIX_FAILURE;

    }

    if (nmhSetFsPtpPortInterfaceType (i4ContextId, i4DomainId,
                                      i4PtpPortNo, i4IfType) != SNMP_SUCCESS)
    {
        nmhSetFsPtpPortRowStatus (i4ContextId, i4DomainId,
                                  i4PtpPortNo, DESTROY);

        return OSIX_FAILURE;
    }

    /* Configure Interface Number for PTP Port */

    if (nmhTestv2FsPtpPortIfaceNumber (&u4ErrorCode, i4ContextId, i4DomainId,
                                       i4PtpPortNo, i4IfIndex) != SNMP_SUCCESS)
    {
        nmhSetFsPtpPortRowStatus (i4ContextId, i4DomainId,
                                  i4PtpPortNo, DESTROY);
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpPortIfaceNumber (i4ContextId, i4DomainId,
                                    i4PtpPortNo, i4IfIndex) != SNMP_SUCCESS)
    {
        nmhSetFsPtpPortRowStatus (i4ContextId, i4DomainId,
                                  i4PtpPortNo, DESTROY);

        return OSIX_FAILURE;
    }

    /* Make the Port Row Status Active */
    if (nmhTestv2FsPtpPortRowStatus (&u4ErrorCode, i4ContextId, i4DomainId,
                                     i4PtpPortNo, ACTIVE) != SNMP_SUCCESS)
    {
        nmhSetFsPtpPortRowStatus (i4ContextId, i4DomainId,
                                  i4PtpPortNo, DESTROY);
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpPortRowStatus (i4ContextId, i4DomainId,
                                  i4PtpPortNo, ACTIVE) != SNMP_SUCCESS)
    {
        /* If Activation Fails Destroy */

        nmhSetFsPtpPortRowStatus (i4ContextId, i4DomainId,
                                  i4PtpPortNo, DESTROY);

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME             : PtpWebConfigureContext
 *
 * DESCRIPTION               : This function sets the PTP module status.
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4Status - PTP Module status
 *                             PTP Module Disable / Enable
 * OUTPUT                    : None
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   :
 *
 **************************************************************************/
INT4
PtpWebConfigureContext (INT4 i4ContextId, INT4 i4DomainId)
{

    UINT4               u4ErrorCode = 0;
    INT4                i4ContextRowStatus;
    INT4                i4DomainRowStatus;
    INT4                i4RetVal = SNMP_FAILURE;

    i4RetVal = nmhGetFsPtpContextRowStatus (i4ContextId, &i4ContextRowStatus);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4RetVal = nmhGetFsPtpDomainRowStatus (i4ContextId, i4DomainId,
                                               &i4DomainRowStatus);
        if (i4RetVal != SNMP_FAILURE)
        {
            return OSIX_SUCCESS;
        }
        if (nmhTestv2FsPtpDomainRowStatus (&u4ErrorCode, i4ContextId,
                                           i4DomainId, CREATE_AND_GO)
            == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }

        if (nmhSetFsPtpDomainRowStatus (i4ContextId, i4DomainId, CREATE_AND_GO)
            == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }
        return OSIX_SUCCESS;
    }

    if (nmhTestv2FsPtpContextRowStatus
        (&u4ErrorCode, i4ContextId, CREATE_AND_GO) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpContextRowStatus (i4ContextId, CREATE_AND_GO)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhTestv2FsPtpAdminStatus (&u4ErrorCode, i4ContextId, PTP_ENABLED) ==
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpAdminStatus (i4ContextId, PTP_ENABLED) == SNMP_FAILURE)

    {
        return OSIX_FAILURE;
    }

    /*Configure Domain Row Status */

    if (nmhTestv2FsPtpDomainRowStatus (&u4ErrorCode, i4ContextId,
                                       i4DomainId, CREATE_AND_GO)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpDomainRowStatus (i4ContextId, i4DomainId, CREATE_AND_GO)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : PtpWebSetClkMode
 *
 * DESCRIPTION               : This function sets the PTP module status.
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4Status - PTP Module status
 *                             PTP Module Disable / Enable
 * OUTPUT                    : None
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   :
 *
 **************************************************************************/
INT4
PtpWebSetClkMode (INT4 i4ContextId, INT4 i4DomainId, INT4 i4ClockMode)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4DelayMechanism = 0;
    INT4                i4ClkMode = i4ClockMode;

    /* As values 5 and 6 will configured in WEB it has to be 
     * configured as PTP_TRANSPARENT_CLOCK_MODE */

    if ((i4ClockMode == PTP_P2P_TC_CLOCK_MODE) ||
        (i4ClockMode == PTP_E2E_TC_CLOCK_MODE))
    {
        i4ClockMode = PTP_TRANSPARENT_CLOCK_MODE;
    }

    if (nmhTestv2FsPtpDomainClockMode (&u4ErrorCode, i4ContextId,
                                       i4DomainId, i4ClockMode) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpDomainClockMode (i4ContextId,
                                    i4DomainId, i4ClockMode) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* 19 Feb 2010 - PTP WEB FIX- */
    if ((i4ClkMode == PTP_P2P_TC_CLOCK_MODE) ||
        (i4ClkMode == PTP_E2E_TC_CLOCK_MODE))
    {
        if (i4ClkMode == PTP_P2P_TC_CLOCK_MODE)
        {
            i4DelayMechanism = PTP_PORT_DELAY_MECH_PEER_TO_PEER;
        }
        else
        {
            i4DelayMechanism = PTP_PORT_DELAY_MECH_END_TO_END;
        }

        if (nmhTestv2FsPtpTransparentClockDelaymechanism
            (&u4ErrorCode, i4ContextId, i4DomainId,
             i4DelayMechanism) == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }

        if (nmhSetFsPtpTransparentClockDelaymechanism
            (i4ContextId, i4DomainId, i4DelayMechanism) == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME             : PtpWebSetClkPriority1
 *
 * DESCRIPTION               : This function sets the PTP Priority 1
 *
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId  - Domain Identifier
 *                             i4ClkPriority1 - PTP Priority 1
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/

INT4
PtpWebSetClkPriority1 (INT4 i4ContextId, INT4 i4DomainId, INT4 i4ClkPriority1)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpClockPriority1 (&u4ErrorCode, i4ContextId,
                                      i4DomainId,
                                      i4ClkPriority1) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpClockPriority1 (i4ContextId,
                                   i4DomainId, i4ClkPriority1) == SNMP_FAILURE)
    {

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : PtpWebSetClkPriority2
 *
 * DESCRIPTION               : This function sets the PTP Priority 2
 *
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *                             i4ClkPriority2 -  PTP Priority 2
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified :None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/

INT4
PtpWebSetClkPriority2 (INT4 i4ContextId, INT4 i4DomainId, INT4 i4ClkPriority2)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpClockPriority2 (&u4ErrorCode, i4ContextId,
                                      i4DomainId,
                                      i4ClkPriority2) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpClockPriority2 (i4ContextId,
                                   i4DomainId, i4ClkPriority2) == SNMP_FAILURE)
    {

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : PtpWebSetClkSlaveOnly
 *
 * DESCRIPTION               : This function sets the PTP Clock Slave only
 *                             flag
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId  - PTP Domain Identifier
 *                             i4SlaveOnly - PTP Slave Only Flag
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/

INT4
PtpWebSetClkSlaveOnly (INT4 i4ContextId, INT4 i4DomainId, INT4 i4SlaveOnly)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpClockSlaveOnly (&u4ErrorCode, i4ContextId,
                                      i4DomainId, i4SlaveOnly) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpClockSlaveOnly (i4ContextId,
                                   i4DomainId, i4SlaveOnly) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : PtpWebSetAnnounceInterval
 *
 * DESCRIPTION               : This function sets the PTP Announce Interval
 *
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *                             i4AnnounceInterval - Announce Interval 
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/

INT4
PtpWebSetAnnounceInterval (INT4 i4ContextId,
                           INT4 i4DomainId,
                           INT4 i4PortId, INT4 i4AnnounceInterval)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpPortAnnounceInterval (&u4ErrorCode, i4ContextId,
                                            i4DomainId, i4PortId,
                                            i4AnnounceInterval) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpPortAnnounceInterval (i4ContextId,
                                         i4DomainId, i4PortId,
                                         i4AnnounceInterval) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : PtpWebSetAnnounceTimeOut
 *
 * DESCRIPTION               : This function sets the PTP Announce Timeout
 *
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *                             i4AnnounceTimeOut - Announce Timeout
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/

INT4
PtpWebSetAnnounceTimeOut (INT4 i4ContextId,
                          INT4 i4DomainId,
                          INT4 i4PortId, INT4 i4AnnounceTimeOut)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpPortAnnounceReceiptTimeout (&u4ErrorCode,
                                                  i4ContextId,
                                                  i4DomainId,
                                                  i4PortId,
                                                  i4AnnounceTimeOut)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpPortAnnounceReceiptTimeout (i4ContextId,
                                               i4DomainId,
                                               i4PortId,
                                               i4AnnounceTimeOut)
        == SNMP_FAILURE)
    {

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : PtpWebSetDelayReqInterval
 *
 * DESCRIPTION               : This function sets the PTP delay req interval
 *
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *                             i4DelayReqInterval - Delay Request Interval
 *                             u1IsTransPort  -  OSIX_TRU / OSIX_FALSE
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/

INT4
PtpWebSetDelayReqInterval (INT4 i4ContextId,
                           INT4 i4DomainId,
                           INT4 i4PortId, INT4 i4DelayReqInterval,
                           UINT1 u1IsTransPort)
{
    UINT4               u4ErrorCode = 0;

    if (u1IsTransPort == OSIX_TRUE)
    {
        if (nmhTestv2FsPtpTransparentPortMinPdelayReqInterval
            (&u4ErrorCode, i4ContextId, i4DomainId, i4PortId,
             i4DelayReqInterval) == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }

        if (nmhSetFsPtpTransparentPortMinPdelayReqInterval (i4ContextId,
                                                            i4DomainId,
                                                            i4PortId,
                                                            i4DelayReqInterval)
            == SNMP_FAILURE)
        {

            return OSIX_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsPtpPortMinDelayReqInterval (&u4ErrorCode, i4ContextId,
                                                   i4DomainId, i4PortId,
                                                   i4DelayReqInterval)
            == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }

        if (nmhSetFsPtpPortMinDelayReqInterval (i4ContextId,
                                                i4DomainId, i4PortId,
                                                i4DelayReqInterval) ==
            SNMP_FAILURE)
        {

            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : PtpWebSetDelayRequest
 *
 * DESCRIPTION               : This function sets the PTP Delay Request 
 *
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *                             i4DelayReq - Delay Request 
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/

INT4
PtpWebSetDelayRequest (INT4 i4ContextId,
                       INT4 i4DomainId, INT4 i4PortId, INT4 i4DelayReq)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpPortMinDelayReqInterval
        (&u4ErrorCode, i4ContextId, i4DomainId,
         i4PortId, i4DelayReq) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpPortMinDelayReqInterval
        (i4ContextId, i4DomainId, i4PortId, i4DelayReq) == SNMP_FAILURE)
    {

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : PtpWebSetSyncInterval
 *
 * DESCRIPTION               : This function sets the PTP Sync Interval
 *
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier 
 *                             i4SyncInterval - Sync Interval
 *                             
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/

INT4
PtpWebSetSyncInterval (INT4 i4ContextId,
                       INT4 i4DomainId, INT4 i4PortId, INT4 i4SyncInterval)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpPortSyncInterval (&u4ErrorCode, i4ContextId,
                                        i4DomainId,
                                        i4PortId,
                                        i4SyncInterval) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpPortSyncInterval (i4ContextId,
                                     i4DomainId,
                                     i4PortId, i4SyncInterval) == SNMP_FAILURE)
    {

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : PtpWebSetDelayType
 *
 * DESCRIPTION               : This function sets the PTP Delay Type
 *
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *                             i4DelayType - Delay Type 
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/

INT4
PtpWebSetDelayType (INT4 i4ContextId,
                    INT4 i4DomainId, INT4 i4PortId, INT4 i4DelayType)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpPortDelayMechanism (&u4ErrorCode, i4ContextId,
                                          i4DomainId, i4PortId,
                                          i4DelayType) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpPortDelayMechanism (i4ContextId,
                                       i4DomainId,
                                       i4PortId, i4DelayType) == SNMP_FAILURE)
    {

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : PtpWebSetVersion
 *
 * DESCRIPTION               : This function sets the PTP Protocol Version
 *
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *                             i4Verion - PTP Verison
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/

INT4
PtpWebSetVersion (INT4 i4ContextId,
                  INT4 i4DomainId, INT4 i4PortId, INT4 i4Version)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpPortVersionNumber (&u4ErrorCode, i4ContextId,
                                         i4DomainId, i4PortId,
                                         i4Version) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpPortVersionNumber (i4ContextId,
                                      i4DomainId, i4PortId,
                                      i4Version) == SNMP_FAILURE)
    {

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : PtpWebSetTwoStepFlag
 *
 * DESCRIPTION               : This function sets the PTP Clock 
 *                             Two Step Flag
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *                             i4TwoStepFlag - Two Step Flag
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/

INT4
PtpWebSetTwoStepFlag (INT4 i4ContextId, INT4 i4DomainId, INT4 i4TwoStepFlag)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpDomainRowStatus (&u4ErrorCode, i4ContextId,
                                       i4DomainId, NOT_IN_SERVICE)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpDomainRowStatus (i4ContextId, i4DomainId, NOT_IN_SERVICE)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsPtpClockTwoStepFlag (&u4ErrorCode, i4ContextId,
                                        i4DomainId,
                                        i4TwoStepFlag) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpClockTwoStepFlag (i4ContextId,
                                     i4DomainId, i4TwoStepFlag) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhTestv2FsPtpDomainRowStatus (&u4ErrorCode, i4ContextId,
                                       i4DomainId, ACTIVE) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpDomainRowStatus (i4ContextId, i4DomainId, ACTIVE)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : PtpWebSetClkPathTrace
 *
 * DESCRIPTION               : This function sets the PTP Clock 
 *                             Two Step Flag
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *                             i4TwoStepFlag - Two Step Flag
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
PtpWebSetClkPathTrace (INT4 i4ContextId, INT4 i4DomainId, INT4 i4Status)
{

    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpClockPathTraceOption (&u4ErrorCode, i4ContextId,
                                            i4DomainId,
                                            i4Status) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpClockPathTraceOption (i4ContextId,
                                         i4DomainId, i4Status) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME    :PtpWebUpdateClockNumberPorts 
 *
 * DESCRIPTION      :This function Configures the Number of Ports in a Clock
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    i4DomainId - Domain Identifier
 *                    
 *
 * OUTPUT           : 
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/

INT4
PtpWebSetClockNumberPorts (INT4 i4ContextId,
                           INT4 i4DomainId, INT4 i4NumberPorts)
{
    INT4                i4ClockMode = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4RetStatus = OSIX_FAILURE;

    /* Get the Clock Mode and then Map Port */

    i4RetStatus = nmhGetFsPtpDomainClockMode (i4ContextId, i4DomainId,
                                              &i4ClockMode);
    if (i4RetStatus != SNMP_SUCCESS)
    {
        return i4RetStatus;
    }

    /* Set the Domain Row Status as NOT_IN_SERVICE */

    if (nmhTestv2FsPtpDomainRowStatus (&u4ErrorCode, i4ContextId, i4DomainId,
                                       (INT4) NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpDomainRowStatus (i4ContextId, i4DomainId,
                                    (INT4) NOT_IN_SERVICE) == SNMP_FAILURE)
    {

        return OSIX_FAILURE;
    }

    if ((i4ClockMode == PTP_TRANSPARENT_CLOCK_MODE))
    {
        i4RetStatus = PtpWebUpdateTransClkNumberPorts (i4ContextId,
                                                       i4DomainId,
                                                       i4NumberPorts);
    }
    else
    {
        i4RetStatus = PtpWebUpdateClockNumberPorts (i4ContextId,
                                                    i4DomainId, i4NumberPorts);
    }

    if (i4RetStatus == OSIX_FAILURE)
    {
        return i4RetStatus;
    }

    /* Set the Domain Row Status as ACTIVE */

    if (nmhTestv2FsPtpDomainRowStatus (&u4ErrorCode, i4ContextId, i4DomainId,
                                       (INT4) ACTIVE) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsPtpDomainRowStatus (i4ContextId, i4DomainId,
                                    (INT4) ACTIVE) == SNMP_FAILURE)
    {

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    :PtpWebUpdateClockNumberPorts 
 *
 * DESCRIPTION      :This function Configures the Number of Ports in a Clock
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    i4DomainId - Domain Identifier
 *                    
 *
 * OUTPUT           : 
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
PtpWebUpdateClockNumberPorts (INT4 i4ContextId,
                              INT4 i4DomainId, INT4 i4NumberPorts)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsPtpClockNumberPorts (&u4ErrCode, i4ContextId, i4DomainId,
                                        i4NumberPorts) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsPtpClockNumberPorts (i4ContextId, i4DomainId,
                                     i4NumberPorts) == SNMP_FAILURE)
    {

        return OSIX_FAILURE;

    }

    return OSIX_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME    :PtpWebUpdateTransClkNumberPorts 
 *
 * DESCRIPTION      :This function Configures the Number of Ports in a TC
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    i4DomainId - Domain Identifier
 *                    
 *
 * OUTPUT           : 
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
PtpWebUpdateTransClkNumberPorts (INT4 i4ContextId,
                                 INT4 i4DomainId, INT4 i4NumberPorts)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsPtpTransparentClockNumberPorts
        (&u4ErrCode, i4ContextId, i4DomainId, i4NumberPorts) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpTransparentClockNumberPorts (i4ContextId, i4DomainId,
                                                i4NumberPorts) == SNMP_FAILURE)
    {

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME    : PtpWebSetPortAdminStaus
 *
 * DESCRIPTION      : This function Configures the Number of Ports in a TC
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    i4DomainId - Domain Identifier
 *                    
 *
 * OUTPUT           : 
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
PtpWebSetPortAdminStaus (INT4 i4ContextId,
                         INT4 i4DomainId, UINT4 u4PtpPortNo, INT4 i4Status)
{

    INT4                i4RetStatus = 0;
    INT4                i4ClockMode = 0;
    /* Get the Clock Mode and then configure Port */
    i4RetStatus = nmhGetFsPtpDomainClockMode (i4ContextId, i4DomainId,
                                              &i4ClockMode);
    if (i4RetStatus != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (i4ClockMode == PTP_TRANSPARENT_CLOCK_MODE)
    {
        /*Configure Transpaarent Port Admin Status */
        i4RetStatus = PtpWebSetTransPortAdminStatus (i4ContextId,
                                                     i4DomainId,
                                                     u4PtpPortNo, i4Status);
    }
    else
    {

        i4RetStatus = PtpWebSetPortAdminStatus (i4ContextId,
                                                i4DomainId,
                                                u4PtpPortNo, i4Status);
    }

    return i4RetStatus;
}

/***************************************************************************
 * FUNCTION NAME    : PtpWebSetPortAdminStatus
 *
 * DESCRIPTION      : This function sets the admin status of Ptp
 *                    port as configured from CLI.
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    i4Status - System control status
 *                    (PTP_START/PTP_SHUTDOWN)
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
PtpWebSetPortAdminStatus (INT4 i4ContextId,
                          INT4 i4DomainId, UINT4 u4PtpPortNo, INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;

    if ((nmhTestv2FsPtpPortPtpStatus (&u4ErrorCode, i4ContextId, i4DomainId,
                                      (INT4) u4PtpPortNo,
                                      i4Status) == SNMP_FAILURE))

    {
        return OSIX_FAILURE;
    }
    if ((nmhSetFsPtpPortPtpStatus
         (i4ContextId, i4DomainId, (INT4) u4PtpPortNo,
          i4Status) == SNMP_FAILURE))

    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpWebSetTransPortAdminStatus
 *
 * DESCRIPTION      : This function sets the admin status of Ptp
 *                    port as configured from CLI.
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    i4Status - System control status
 *                    (PTP_START/PTP_SHUTDOWN)
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
PtpWebSetTransPortAdminStatus (INT4 i4ContextId,
                               INT4 i4DomainId, UINT4 u4PtpPortNo,
                               INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;

    if ((nmhTestv2FsPtpTransparentPortPtpStatus
         (&u4ErrorCode, i4ContextId, i4DomainId, (INT4) u4PtpPortNo,
          i4Status) == SNMP_FAILURE))

    {
        return OSIX_FAILURE;
    }

    if ((nmhSetFsPtpTransparentPortPtpStatus
         (i4ContextId, i4DomainId, (INT4) u4PtpPortNo,
          i4Status) == SNMP_FAILURE))

    {

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : PtpWebConfigAccMaster
 *
 * DESCRIPTION               : This function sets the PTP Alternate Masters
 *
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *                             i4Status - Alternate Master Status
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/

INT4
PtpWebConfigAccMaster (INT4 i4ContextId,
                       INT4 i4DomainId, INT4 i4NwProto, INT4 i4AccLen,
                       CHR1 * u1AccMasterAddr)
{
    tSNMP_OCTET_STRING_TYPE AccMasterAddr;
    UINT1               au1AccMasterAddr[PTP_MASTER_MAX_ADDR_LEN];
    UINT4               u4ErrorCode = 0;

    MEMSET (&(au1AccMasterAddr[0]), 0, PTP_MASTER_MAX_ADDR_LEN);
    AccMasterAddr.pu1_OctetList = &au1AccMasterAddr[0];

    PtpWebConverPortAddToOct ((UINT1 *) u1AccMasterAddr, i4NwProto,
                              &AccMasterAddr);
    i4AccLen = AccMasterAddr.i4_Length;

    if (PtpWebUpdateAccMasterEntry (i4ContextId, i4DomainId,
                                    i4NwProto, i4AccLen, AccMasterAddr,
                                    CREATE_AND_GO) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (nmhTestv2FsPtpAccMasterRowStatus
        (&u4ErrorCode, i4ContextId, i4DomainId, i4NwProto,
         i4AccLen, &AccMasterAddr, ACTIVE) == SNMP_FAILURE)
    {
        PtpWebUpdateAccMasterEntry (i4ContextId, i4DomainId,
                                    i4NwProto, i4AccLen,
                                    AccMasterAddr, DESTROY);
        return OSIX_FAILURE;
    }

    if (PtpWebUpdateAccMasterEntry (i4ContextId, i4DomainId,
                                    i4NwProto, i4AccLen,
                                    AccMasterAddr, ACTIVE) != OSIX_SUCCESS)
    {
        nmhSetFsPtpAccMasterRowStatus (i4ContextId, i4DomainId,
                                       i4NwProto, i4AccLen,
                                       &AccMasterAddr, DESTROY);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpWebSetAccMasterAltPriority
 *
 * DESCRIPTION      : This Function Sets the Acceptable Master's Alternate
 *                    Priority Attribute
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
PtpWebSetAccMasterAltPriority (INT4 i4ContextId,
                               INT4 i4DomainId, INT4 i4NwProto, INT4 i4AccLen,
                               UINT1 *u1AccMasterAddr, INT4 i4AlternatePriority)
{
    tSNMP_OCTET_STRING_TYPE AccMasterAddr;
    UINT1               au1AccMasterAddr[PTP_MASTER_MAX_ADDR_LEN];
    UINT4               u4ErrCode = 0;

    MEMSET (&(au1AccMasterAddr[0]), 0, PTP_MASTER_MAX_ADDR_LEN);
    AccMasterAddr.pu1_OctetList = &au1AccMasterAddr[0];

    PtpWebConverPortAddToOct (u1AccMasterAddr, i4NwProto, &AccMasterAddr);
    i4AccLen = AccMasterAddr.i4_Length;

    if (nmhTestv2FsPtpAccMasterAlternatePriority (&u4ErrCode, i4ContextId,
                                                  i4DomainId, i4NwProto,
                                                  i4AccLen, &AccMasterAddr,
                                                  i4AlternatePriority)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpAccMasterAlternatePriority (i4ContextId,
                                               i4DomainId, i4NwProto, i4AccLen,
                                               &AccMasterAddr,
                                               i4AlternatePriority)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpWebUpdateAccMasterEntry
 *
 * DESCRIPTION      :  This function creates or updates a Acc master entry
 *
 * INPUT            : i4ContextId - Context Identifier
 *
 *
 * OUTPUT           :
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *

 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/

INT4
PtpWebUpdateAccMasterEntry (INT4 i4ContextId,
                            INT4 i4DomainId, INT4 i4NwProto,
                            INT4 i4AddrLen,
                            tSNMP_OCTET_STRING_TYPE pAccMasterAddr,
                            INT4 i4RowStatus)
{
    UINT4               u4ErrCode = 0;
    INT4                i4Status = 0;
    INT4                i4Result = 0;

    /* Check if the Domain config entry already exists for the
     * given indices in the database.
     * If the Get function returns Success, for CREATE and WAIT then a
     * user already exists. Otherwise we need to create an entry.
     */
    nmhGetFsPtpAccMasterRowStatus (i4ContextId, i4DomainId, i4NwProto,
                                   i4AddrLen, &pAccMasterAddr, &i4Status);

    switch (i4RowStatus)
    {
        case CREATE_AND_GO:
            i4Status = CREATE_AND_GO;
            break;
        case NOT_IN_SERVICE:

            i4Status = NOT_IN_SERVICE;
            break;

        case DESTROY:
            i4Status = DESTROY;
            break;

        case ACTIVE:
            i4Status = ACTIVE;
            break;

        default:
            return OSIX_FAILURE;
    }

    i4Result = nmhTestv2FsPtpAccMasterRowStatus (&u4ErrCode, i4ContextId,
                                                 i4DomainId, i4NwProto,
                                                 i4AddrLen, &pAccMasterAddr,
                                                 i4Status);
    if (i4Result == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    UNUSED_PARAM (i4Result);
    UNUSED_PARAM (u4ErrCode);

    if (nmhSetFsPtpAccMasterRowStatus (i4ContextId, i4DomainId, i4NwProto,
                                       i4AddrLen, &pAccMasterAddr,
                                       i4Status) == SNMP_FAILURE)
    {

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

 /***************************************************************************
 * FUNCTION NAME    : PtpWebDeletePortFromDomain
 *
 * DESCRIPTION      :  This function deletes a Port from a PTP Domain
 *
 * INPUT            : i4ContextId - Context Identifier
 *
 *
 * OUTPUT           :
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
PtpWebDeletePortFromDomain (INT4 i4ContextId, INT4 i4DomainId, INT4 i4PtpPortNo)
{
    INT4                i4ClockMode = 0;
    INT4                i4RetStatus = OSIX_FAILURE;

    /* Get the Clock Mode and then Delete the respective Port */

    i4RetStatus = nmhGetFsPtpDomainClockMode (i4ContextId, i4DomainId,
                                              &i4ClockMode);
    if (i4RetStatus != SNMP_SUCCESS)
    {
        return i4RetStatus;
    }

    if (i4ClockMode == PTP_TRANSPARENT_CLOCK_MODE)
    {
        nmhSetFsPtpTransparentPortRowStatus (i4ContextId, i4DomainId,
                                             i4PtpPortNo, DESTROY);
    }
    else
    {
        nmhSetFsPtpPortRowStatus (i4ContextId, i4DomainId, i4PtpPortNo,
                                  DESTROY);
    }

    return OSIX_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME    : PtpWebConfigAltTimeScale
 *
 * DESCRIPTION      : This function Configures the Alternate Time Scale
 *                    Parameters
 *
 * INPUT            : i4DomainId - Doamin Identifier
 *                    i4ContextId - Context Identifier
 *
 *
 * OUTPUT           : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
PtpWebConfigAltTimeScale (INT4 i4ContextId,
                          INT4 i4DomainId, INT4 i4AltTimeScaleKeyId)
{
    UINT4               u4ErrorCode = 0;

    if (PtpWebUpdateAltTimeScaleEntry (i4ContextId, i4DomainId,
                                       i4AltTimeScaleKeyId,
                                       CREATE_AND_GO) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (nmhTestv2FsPtpAltTimeScaleRowStatus (&u4ErrorCode, i4ContextId,
                                             i4DomainId,
                                             i4AltTimeScaleKeyId,
                                             ACTIVE) == SNMP_FAILURE)
    {
        PtpWebUpdateAltTimeScaleEntry (i4ContextId, i4DomainId,
                                       i4AltTimeScaleKeyId, DESTROY);
        return OSIX_FAILURE;
    }

    if (PtpWebUpdateAltTimeScaleEntry (i4ContextId, i4DomainId,
                                       i4AltTimeScaleKeyId,
                                       ACTIVE) != OSIX_SUCCESS)
    {
        nmhSetFsPtpAltTimeScaleRowStatus (i4ContextId, i4DomainId,
                                          i4AltTimeScaleKeyId, DESTROY);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpWebDelAltTimeScale
 *
 * DESCRIPTION      : This function delete the Alternate Time Scale Entry
 *
 * INPUT            : CliHandle  - CliHandle
 *                    i4DomainId - Doamin Identifier
 *                    i4ContextId - Context Identifier
 *                    i4AltTimeScaleKeyId - Alternate timescale KeyId
 *
 *
 * OUTPUT           : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
PtpWebDelAltTimeScale (INT4 i4ContextId,
                       INT4 i4DomainId, INT4 i4AltTimeScaleKeyId)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpAltTimeScaleRowStatus (&u4ErrorCode, i4ContextId,
                                             i4DomainId, i4AltTimeScaleKeyId,
                                             DESTROY) == SNMP_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if ((nmhSetFsPtpAltTimeScaleRowStatus (i4ContextId, i4DomainId,
                                           i4AltTimeScaleKeyId, DESTROY)) ==
        SNMP_FAILURE)
    {

        return (OSIX_FAILURE);
    }

    return (OSIX_SUCCESS);
}

/***************************************************************************
 * FUNCTION NAME    : PtpWebUpdateAltTimeScaleEntry
 *
 * DESCRIPTION      :  This function creates or updates a Alt Time scal
 *                     entry
 *
 * INPUT            : i4ContextId - Context Identifier
 *
 *
 * OUTPUT           :
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/

INT4
PtpWebUpdateAltTimeScaleEntry (INT4 i4ContextId,
                               INT4 i4DomainId, INT4 i4AltTimeScaleKeyId,
                               INT4 i4RowStatus)
{
    UINT4               u4ErrCode = 0;
    INT4                i4Status = 0;
    INT4                i4Result = 0;

    /* Check if the Alternate Time scale entry already exists for the
     * given indices in the database.
     * If the Get function returns Success, for CREATE and WAIT then a
     * user already exists. Otherwise we need to create an entry.
     */
    nmhGetFsPtpAltTimeScaleRowStatus (i4ContextId, i4DomainId,
                                      i4AltTimeScaleKeyId, &i4Status);
    switch (i4RowStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            if (i4Status == 0)
            {
                i4Status = CREATE_AND_GO;
            }
            else
            {
                i4Status = NOT_IN_SERVICE;
            }
            break;
        case NOT_IN_SERVICE:

            i4Status = NOT_IN_SERVICE;
            break;

        case DESTROY:
            i4Status = DESTROY;
            break;

        case ACTIVE:
            i4Status = ACTIVE;
            break;

        default:
            return OSIX_FAILURE;
    }

    i4Result = nmhTestv2FsPtpAltTimeScaleRowStatus (&u4ErrCode, i4ContextId,
                                                    i4DomainId,
                                                    i4AltTimeScaleKeyId,
                                                    i4Status);
    if (i4Result == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpAltTimeScaleRowStatus (i4ContextId,
                                          i4DomainId,
                                          i4AltTimeScaleKeyId,
                                          i4Status) == SNMP_FAILURE)
    {

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpWebSetAccMasterFlag
 *
 * DESCRIPTION      :  This function Disables /Enables  Accept Master
 *                     option flag on a port , this enables the Slave port
 *                     to transmit the Sync Message with ALT MASTER FLAG
 *                     set
 *
 * INPUT            : i4ContextId - Context Identifier
 *
 *
 * OUTPUT           :
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
PtpWebSetAccMasterFlag (INT4 i4ContextId,
                        INT4 i4DomainId, INT4 i4PortId, INT4 i4Status)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsPtpPortAccMasterEnabled (&u4ErrCode, i4ContextId, i4DomainId,
                                            i4PortId, i4Status) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpPortAccMasterEnabled (i4ContextId, i4DomainId,
                                         i4PortId, i4Status) == SNMP_FAILURE)
    {

        return OSIX_FAILURE;
    }

    return OSIX_FAILURE;

}

/***************************************************************************
 * FUNCTION NAME    : PtpWebSetAltMasterFlag
 *
 * DESCRIPTION      :  This function Disables /Enables  Alternate Master
 *                     option flag on a port , this enables the Slave port
 *                     to transmit the Sync Message with ALT MASTER FLAG
 *                     set
 *
 * INPUT            : i4ContextId - Context Identifier
 *
 *
 * OUTPUT           :
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
PtpWebSetAltMasterFlag (INT4 i4ContextId,
                        INT4 i4DomainId, INT4 i4PortId, INT4 i4Status)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsPtpPortAltMulcastSync (&u4ErrCode, i4ContextId, i4DomainId,
                                          i4PortId, i4Status) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpPortAltMulcastSync (i4ContextId, i4DomainId,
                                       i4PortId, i4Status) == SNMP_FAILURE)
    {

        return OSIX_FAILURE;
    }

    return OSIX_FAILURE;

}

/***************************************************************************
 * FUNCTION NAME    : PtpWebSetAltMcastSyncInterval
 *
 * DESCRIPTION      :  This function Disables /Enables  Alt Time
 *                     in a Port
 *
 * INPUT            : i4ContextId - Context Identifier
 *
 *
 * OUTPUT           :
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
PtpWebSetAltMcastSyncInterval (INT4 i4ContextId,
                               INT4 i4DomainId,
                               INT4 i4PortId, INT4 i4SyncInterval)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsPtpPortAltMulcastSyncInterval
        (&u4ErrCode, i4ContextId, i4DomainId, i4PortId,
         i4SyncInterval) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpPortAltMulcastSyncInterval (i4ContextId, i4DomainId,
                                               i4PortId,
                                               i4SyncInterval) == SNMP_FAILURE)
    {

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpWebUpdateATSEntryOptParams
 *
 * DESCRIPTION      : This function displays the PTP Clock
 *                    Alternate Time Scale
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    i4DomainId - Domain Identifier
 *
 * OUTPUT           : Prints the Alternate Time Scale
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : VOID
 *
 **************************************************************************/
INT4
PtpWebUpdateATSEntryOptParams (INT4 i4ContextId,
                               INT4 i4DomainId, INT4 i4KeyIndex,
                               INT4 i4ATSOffset, INT4 i4ATSJmpSecs,
                               UINT1 *pu1AltTimeScaleJump)
{
    tSNMP_OCTET_STRING_TYPE AltTimeScaletimeOfNextJump;
    UINT4               u4ErrCode = 0;

    MEMSET (&AltTimeScaletimeOfNextJump, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (nmhTestv2FsPtpAltTimeScaleRowStatus (&u4ErrCode, i4ContextId,
                                             i4DomainId,
                                             i4KeyIndex,
                                             NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpAltTimeScaleRowStatus (i4ContextId, i4DomainId,
                                          i4KeyIndex,
                                          NOT_IN_SERVICE) == SNMP_FAILURE)
    {

        return OSIX_FAILURE;
    }

    if (nmhTestv2FsPtpAltTimeScalecurrentOffset (&u4ErrCode, i4ContextId,
                                                 i4DomainId,
                                                 i4KeyIndex,
                                                 i4ATSOffset) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpAltTimeScalecurrentOffset (i4ContextId, i4DomainId,
                                              i4KeyIndex,
                                              i4ATSOffset) == SNMP_FAILURE)
    {

        return OSIX_FAILURE;
    }

    if (nmhTestv2FsPtpAltTimeScalejumpSeconds (&u4ErrCode,
                                               i4ContextId, i4DomainId,
                                               i4KeyIndex,
                                               i4ATSJmpSecs) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpAltTimeScalejumpSeconds (i4ContextId, i4DomainId,
                                            i4KeyIndex,
                                            i4ATSJmpSecs) == SNMP_FAILURE)
    {

        return OSIX_FAILURE;

    }

    if (pu1AltTimeScaleJump != NULL)
    {
        AltTimeScaletimeOfNextJump.pu1_OctetList = pu1AltTimeScaleJump;
        AltTimeScaletimeOfNextJump.i4_Length =
            (INT4) STRLEN (pu1AltTimeScaleJump);

        if (nmhTestv2FsPtpAltTimeScaletimeOfNextJump (&u4ErrCode,
                                                      i4ContextId,
                                                      i4DomainId,
                                                      i4KeyIndex,
                                                      &AltTimeScaletimeOfNextJump)
            == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }

        if (nmhSetFsPtpAltTimeScaletimeOfNextJump (i4ContextId,
                                                   i4DomainId,
                                                   i4KeyIndex,
                                                   &AltTimeScaletimeOfNextJump)
            == SNMP_FAILURE)
        {

            return OSIX_FAILURE;
        }
    }

    if (nmhTestv2FsPtpAltTimeScaleRowStatus (&u4ErrCode, i4ContextId,
                                             i4DomainId,
                                             i4KeyIndex,
                                             ACTIVE) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpAltTimeScaleRowStatus (i4ContextId, i4DomainId,
                                          i4KeyIndex, ACTIVE) == SNMP_FAILURE)
    {

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpWebSetAltTimeDispName
 *
 * DESCRIPTION      : This function displays the PTP Clock
 *                    Alternate Time Scale
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    i4DomainId - Domain Identifier
 *
 * OUTPUT           :
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : VOID
 *
 **************************************************************************/
INT4
PtpWebSetAltTimeDispName (INT4 i4ContextId,
                          INT4 i4DomainId, INT4 i4KeyIndex,
                          UINT1 *pu1AltTimeDispName)
{
    tSNMP_OCTET_STRING_TYPE AltTimeDispName;
    UINT4               u4ErrCode;

    MEMSET (&AltTimeDispName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (nmhTestv2FsPtpAltTimeScaleRowStatus (&u4ErrCode, i4ContextId,
                                             i4DomainId,
                                             i4KeyIndex,
                                             NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpAltTimeScaleRowStatus (i4ContextId, i4DomainId,
                                          i4KeyIndex,
                                          NOT_IN_SERVICE) == SNMP_FAILURE)
    {

        return OSIX_FAILURE;
    }
    if (pu1AltTimeDispName != NULL)
    {
        AltTimeDispName.pu1_OctetList = pu1AltTimeDispName;
        AltTimeDispName.i4_Length = (INT4) STRLEN (pu1AltTimeDispName);

        if ((nmhTestv2FsPtpAltTimeScaledisplayName (&u4ErrCode, i4ContextId,
                                                    i4DomainId, i4KeyIndex,
                                                    &AltTimeDispName)) ==
            SNMP_FAILURE)
        {
            return (OSIX_FAILURE);
        }

        if ((nmhSetFsPtpAltTimeScaledisplayName (i4ContextId, i4DomainId,
                                                 i4KeyIndex,
                                                 &AltTimeDispName)) ==
            SNMP_FAILURE)
        {

            return (OSIX_FAILURE);
        }
    }

    if (nmhTestv2FsPtpAltTimeScaleRowStatus (&u4ErrCode, i4ContextId,
                                             i4DomainId,
                                             i4KeyIndex,
                                             ACTIVE) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpAltTimeScaleRowStatus (i4ContextId, i4DomainId,
                                          i4KeyIndex, ACTIVE) == SNMP_FAILURE)
    {

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME             : PtpWebConverPortAddToOct
 *
 * DESCRIPTION               : This function used to convert the Prot Address
 *                             format form string to Octet
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *                             i4Status - Alternate Master Status
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
VOID
PtpWebConverPortAddToOct (UINT1 *u1AccMasterAddr, INT4 i4NwProto,
                          tSNMP_OCTET_STRING_TYPE * pAccMasterAddr)
{
    tUtlInAddr          IpAddr;
    UINT4               u4Addr = 0;
    INT4                i4RetVal = 0;

    MEMSET (&IpAddr, 0, sizeof (tUtlInAddr));

    if (i4NwProto == PTP_IFACE_UDP_IPV4)
    {
        i4RetVal = CLI_INET_ATON (u1AccMasterAddr, &IpAddr);
        u4Addr = OSIX_NTOHL (IpAddr.u4Addr);
        PTP_IPADDR_TO_OCT (u4Addr, pAccMasterAddr->pu1_OctetList);
        pAccMasterAddr->i4_Length = IPVX_IPV4_ADDR_LEN;
    }
    else if (i4NwProto == PTP_IFACE_UDP_IPV6)
    {
        INET_ATON6 (u1AccMasterAddr, pAccMasterAddr->pu1_OctetList);
        pAccMasterAddr->i4_Length = IPVX_IPV6_ADDR_LEN;
    }
    else
    {
        CliDotStrToMac ((UINT1 *) u1AccMasterAddr,
                        pAccMasterAddr->pu1_OctetList);
        pAccMasterAddr->i4_Length = CFA_ENET_ADDR_LEN;
    }
    UNUSED_PARAM (i4RetVal);
}

/***************************************************************************
 * FUNCTION NAME             : PtpWebSetPrimaryContext
 *
 * DESCRIPTION               : This function sets the PTP Clock
 *                             Two Step Flag
 *
 * INPUT                     : i4ContextId - Context Identifier
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/

INT4
PtpWebSetPrimaryContext (INT4 i4PrimaryContext)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpPrimaryContext (&u4ErrorCode, i4PrimaryContext)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsPtpPrimaryContext (i4PrimaryContext) == SNMP_FAILURE)
    {

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME             : PtpWebSetPrimaryDomain
 *
 * DESCRIPTION               : This function sets the PTP Clock
 *                             Two Step Flag
 *
 * INPUT                     : i4ContextId - Context Identifier
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/

INT4
PtpWebSetPrimaryDomain (INT4 i4ContextId, INT4 i4PrimaryDomain)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpPrimaryDomain (&u4ErrorCode, i4ContextId, i4PrimaryDomain)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsPtpPrimaryDomain (i4ContextId, i4PrimaryDomain) == SNMP_FAILURE)
    {

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpWebSetGlobalSysControl
 *
 * DESCRIPTION      : This function sets the system control status of Ptp
 *                    module as configured from CLI.
 *
 * INPUT            : i4Status - System control status
 *                    (PTP_START/PTP_SHUTDOWN)
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/

INT4
PtpWebSetGlobalSysControl (INT4 i4Status)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsPtpGlobalSysCtrl (&u4ErrorCode, i4Status) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpGlobalSysCtrl (i4Status) != SNMP_SUCCESS)
    {

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpWebSetSystemControl
 *
 * DESCRIPTION      : This function sets the system control status of Ptp
 *                    module as configured from CLI.
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    i4Status - System control status
 *                    (PTP_START/PTP_SHUTDOWN)
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/

INT4
PtpWebSetSystemControl (INT4 i4ContextId, INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpAdminStatus (&u4ErrorCode,
                                   i4ContextId, i4Status) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpAdminStatus (i4ContextId, i4Status) == SNMP_FAILURE)

    {

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME    : PtpWebSetNotifyStatus
 *
 * DESCRIPTION      : This function enables or disables the trap notification
 *                    for PTP module.
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
PtpWebSetNotifyStatus (UINT4 u4TrapValue, UINT1 u1TrapFlag)
{
    tSNMP_OCTET_STRING_TYPE TrapOption;
    UINT4               u4ErrorCode = 0;
    UINT2               u2TrapOption = 0;

    MEMSET (&TrapOption, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    TrapOption.pu1_OctetList = (UINT1 *) &u2TrapOption;
    TrapOption.i4_Length = 2;

    /* Set the corresponding Trap value which is to be set */

    nmhGetFsPtpNotification (&TrapOption);

    u2TrapOption = TrapOption.pu1_OctetList[0];

    if (u1TrapFlag == CLI_ENABLE)
    {
        u2TrapOption = (UINT2) (u2TrapOption | u4TrapValue);
    }
    else
    {
        u2TrapOption = (UINT2) (u2TrapOption & (~u4TrapValue));

        if (u2TrapOption == 0)
        {
            /* None of the trace option is set */
            TrapOption.i4_Length = 0;
        }
    }

    if (nmhTestv2FsPtpNotification (&u4ErrorCode, &TrapOption) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpNotification (&TrapOption) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME             : PtpWebSetMaxAlternateMasters
 *
 * DESCRIPTION               : This function sets the PTP Priority 2
 *
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4Status - PTP Module status
 *                             PTP Priority 2
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/

INT4
PtpWebSetPortNumOfAlternateMasters (INT4 i4ContextId,
                                    INT4 i4DomainId, INT4 i4PtpPortId,
                                    INT4 i4MaxAltMasters)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpPortNumOfAltMaster
        (&u4ErrorCode, i4ContextId, i4DomainId, i4PtpPortId, i4MaxAltMasters)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsPtpPortNumOfAltMaster (i4ContextId, i4DomainId, i4PtpPortId,
                                       i4MaxAltMasters) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;

}

/*******************************************************************************
*  Function Name : IssProcessPtpTraceConfPage
*  Description   : This function processes the HTTP request coming for the
*                  Ptp Trace configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None.
*******************************************************************************/
VOID
IssProcessPtpTraceConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessPtpTraceConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessPtpTraceConfPageSet (pHttp);
    }
    return;
}

/*******************************************************************************
 *  Function Name : IssProcessPtpTraceConfPageGet
 *  Description   : This function processes the get request for the Ptp
 *                  Global configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessPtpTraceConfPageGet (tHttp * pHttp)
{

    UINT4               u4DbgLevel = 0;
    UINT4               u4ContextId = 0;

    PtpUtilGetDebugLevel (u4ContextId, &u4DbgLevel);

    if ((u4DbgLevel & 1) == 1)
    {
        STRCPY (pHttp->au1KeyString, "GLOBAL_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", PTP_ENABLED);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
    }

    STRCPY (pHttp->au1KeyString, "CONTEXT_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    STRCPY (pHttp->au1DataString, "default");
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    if ((u4DbgLevel & INIT_SHUT_TRC) != 0)
    {
        STRCPY (pHttp->au1KeyString, "TRACE_INIT_SHUT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", PTP_ENABLED);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
    }

    if ((u4DbgLevel & MGMT_TRC) != 0)
    {
        STRCPY (pHttp->au1KeyString, "TRACE_MGMT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", PTP_ENABLED);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
    }

    if ((u4DbgLevel & DATA_PATH_TRC) != 0)
    {

        STRCPY (pHttp->au1KeyString, "TRACE_DATAPATH_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", PTP_ENABLED);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
    }

    if ((u4DbgLevel & CONTROL_PLANE_TRC) != 0)
    {
        STRCPY (pHttp->au1KeyString, "TRACE_CONTROL_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", PTP_ENABLED);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
    }

    if ((u4DbgLevel & DUMP_TRC) != 0)

    {
        STRCPY (pHttp->au1KeyString, "TRACE_PKT_DUMP_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", PTP_ENABLED);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
    }

    if ((u4DbgLevel & OS_RESOURCE_TRC) != 0)
    {
        STRCPY (pHttp->au1KeyString, "TRACE_RESOURCE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", PTP_ENABLED);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
    }
    if ((u4DbgLevel & ALL_FAILURE_TRC) != 0)
    {
        STRCPY (pHttp->au1KeyString, "TRACE_ALL_FAIL_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", PTP_ENABLED);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
    }

    if ((u4DbgLevel & BUFFER_TRC) != 0)
    {
        STRCPY (pHttp->au1KeyString, "TRACE_BUFFER_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", PTP_ENABLED);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
    }

    if ((u4DbgLevel & PTP_CRITICAL_TRC) != 0)
    {
        STRCPY (pHttp->au1KeyString, "TRACE_CRITICAL_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", PTP_ENABLED);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
    }

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

}

/*******************************************************************************
 *  Function Name : IssProcessPtpTraceConfPageSet
 *  Description   : This function processes the get request for the Ptp
 *                  Global configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessPtpTraceConfPageSet (tHttp * pHttp)
{
    INT4                i4GlbStatus = 0;
    INT4                i4Context = 0;;

    WebnmRegisterLock (pHttp, PtpApiLock, PtpApiUnLock);
    PtpApiLock ();

    STRCPY (pHttp->au1Name, "GLOBAL_STATUS");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4GlbStatus = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "CONTEXT");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);

        if (VcmIsSwitchExist (pHttp->au1Value,
                              (UINT4 *) &i4Context) == VCM_FALSE)
        {
            PtpApiUnLock ();;
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Please enter a valid context !");
            return;
        }

    }

    STRCPY (pHttp->au1Name, "ENABLED_TRACES");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
    }

    /* Set Enabled Traces */
    if (i4GlbStatus == PTP_ENABLED)
    {
        PtpWebSetDebugs (pHttp->au1Value);
    }
    else if (i4GlbStatus == PTP_DISABLED)
    {
        PtpWebSetCxtDebugs (i4Context, pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "DISABLED_TRACES");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
    }

    /* Set disabled Traces */
    if (i4GlbStatus == PTP_ENABLED)
    {
        PtpWebSetDebugs (pHttp->au1Value);
    }
    else if (i4GlbStatus == PTP_DISABLED)
    {
        PtpWebSetCxtDebugs (i4Context, pHttp->au1Value);
    }

    PtpApiUnLock ();;
    WebnmUnRegisterLock (pHttp);
    IssProcessPtpTraceConfPageGet (pHttp);
}

/***************************************************************************
 * FUNCTION NAME    : PtpWebSetDebugs
 *
 * DESCRIPTION      : This function sets and resets the Trace input for a
 *                    context
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    pu1TraceInput - Input Trace string
 *
 * OUTPUT           : pi1ModeName - Mode String
 *                    pi1DispStr - Display string
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/

INT4
PtpWebSetDebugs (UINT1 *pu1TraceInput)
{

    tSNMP_OCTET_STRING_TYPE TraceInput;
    UINT4               u4ErrorCode = 0;

    MEMSET (&TraceInput, 0, sizeof (TraceInput));
    TraceInput.pu1_OctetList = pu1TraceInput;
    TraceInput.i4_Length = (INT4) STRLEN (pu1TraceInput);

    if (nmhTestv2FsPtpGblTraceOption (&u4ErrorCode, &TraceInput) ==
        SNMP_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (nmhSetFsPtpGblTraceOption (&TraceInput) == SNMP_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpWebSetCxtDebugs
 *
 * DESCRIPTION      : This function sets and resets the Trace input for a
 *                    context
 *
 * INPUT            :
 *                    u4ContextId - Context Identifier
 *                    pu1TraceInput - Input Trace string
 *
 * OUTPUT           : None.
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
PtpWebSetCxtDebugs (INT4 i4ContextId, UINT1 *pu1TraceInput)
{
    tSNMP_OCTET_STRING_TYPE TraceInput;
    UINT4               u4ErrorCode = 0;

    MEMSET (&TraceInput, 0, sizeof (TraceInput));
    TraceInput.pu1_OctetList = pu1TraceInput;
    TraceInput.i4_Length = (INT4) STRLEN (pu1TraceInput);

    if (nmhTestv2FsPtpTraceOption (&u4ErrorCode, i4ContextId,
                                   &TraceInput) == SNMP_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (nmhSetFsPtpTraceOption (i4ContextId, &TraceInput) == SNMP_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    return OSIX_SUCCESS;
}

/******************************************************************************
*  Function Name : IssProcessPtpAccMasterGet
*  Description   : This function gets the user input from the web and configures
*                  Acc Master Data Set                    
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
******************************************************************************/

VOID
IssProcessPtpAccMasterGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE accMasterAddr;
    tSNMP_OCTET_STRING_TYPE NextaccMasterAddr;
    UINT1              *pu1Temp = NULL;
    INT4                i4NextDomainId = 0;
    INT4                i4NextContextId = 0;
    INT4                i4PrintFlag = PTP_DISABLED;
    INT4                i4AddrLen = 0;
    INT4                i4NextAddrLen = 0;
    INT4                i4AccMasterProto = 0;
    INT4                i4NextAccMasterProto = 0;
    UINT4               u4Temp = 0;
    INT4                i4ContextId = 0;
    INT4                i4DomainId = 0;
    INT4                i4AlternatePriority = 0;
    UINT1               au1Accmaster[PTP_MAX_ADDR_LEN];
    UINT1               au1NextAccMaster[PTP_MAX_ADDR_LEN];
    UINT1               au1TmpAccAddr[PTP_MAX_ADDR_LEN];
    UINT1               u1Byte = 0;

    MEMSET (&accMasterAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextaccMasterAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1Accmaster, 0, PTP_MAX_ADDR_LEN);
    MEMSET (&au1NextAccMaster, 0, PTP_MAX_ADDR_LEN);
    MEMSET (&au1TmpAccAddr, 0, PTP_MAX_ADDR_LEN);

    accMasterAddr.pu1_OctetList = au1Accmaster;
    accMasterAddr.i4_Length = (INT4) STRLEN (au1Accmaster);
    NextaccMasterAddr.pu1_OctetList = au1NextAccMaster;
    NextaccMasterAddr.i4_Length = (INT4) STRLEN (au1NextAccMaster);

    WebnmRegisterLock (pHttp, PtpApiLock, PtpApiUnLock);
    PtpApiLock ();

    pHttp->i4Write = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    u4Temp = (UINT4) pHttp->i4Write;

    while (nmhGetNextIndexFsPtpAccMasterDataSetTable
           (i4ContextId, &i4NextContextId, i4DomainId,
            &i4NextDomainId, i4AccMasterProto, &i4NextAccMasterProto,
            i4AddrLen, &i4NextAddrLen, &accMasterAddr,
            &NextaccMasterAddr) != SNMP_FAILURE)
    {
        i4PrintFlag = PTP_ENABLED;
        pHttp->i4Write = (INT4) u4Temp;

        nmhGetFsPtpAccMasterAlternatePriority (i4NextContextId,
                                               i4NextDomainId,
                                               i4NextAccMasterProto,
                                               i4NextAddrLen,
                                               &NextaccMasterAddr,
                                               &i4AlternatePriority);

        MEMSET (&au1TmpAccAddr[0], 0, PTP_MAX_ADDR_LEN);
        pu1Temp = &au1TmpAccAddr[0];

        STRCPY (pHttp->au1KeyString, "CONTEXTID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextContextId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "DOMAINID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextDomainId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "PROTOCOL_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextAccMasterProto);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        if (i4NextAccMasterProto == PTP_IFACE_UDP_IPV4)
        {
            for (u1Byte = 0; u1Byte < NextaccMasterAddr.i4_Length; u1Byte++)
            {
                pu1Temp += SPRINTF ((CHR1 *) pu1Temp, "%d.",
                                    *(NextaccMasterAddr.pu1_OctetList +
                                      u1Byte));
            }
            *(CHR1 *) (pu1Temp - 1) = '\0';

            STRCPY (pHttp->au1KeyString, "ADDR_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1TmpAccAddr);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        }
        else if (i4NextAccMasterProto == PTP_IFACE_UDP_IPV6)
        {

            STRCPY (pHttp->au1KeyString, "ADDR_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     Ip6PrintNtop ((tIp6Addr *) (VOID *) (NextaccMasterAddr.
                                                          pu1_OctetList)));
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        }
        else if (i4NextAccMasterProto == PTP_IFACE_IEEE_802_3)
        {
            CliMacToStr (NextaccMasterAddr.pu1_OctetList, au1TmpAccAddr);
            STRCPY (pHttp->au1KeyString, "ADDR_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1TmpAccAddr);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        }

        STRCPY (pHttp->au1KeyString, "ALTPRIORITY_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4AlternatePriority);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        i4AddrLen = i4NextAddrLen;
        i4AccMasterProto = i4NextAccMasterProto;
        accMasterAddr.pu1_OctetList = NextaccMasterAddr.pu1_OctetList;
        accMasterAddr.i4_Length = NextaccMasterAddr.i4_Length;
    }

    if (i4PrintFlag == PTP_DISABLED)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        PtpApiUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    PtpApiUnLock ();
    WebnmUnRegisterLock (pHttp);
    return;

}

/***************************************************************************
 * FUNCTION NAME    : PtpWebDelAccMaster            
 *
 * DESCRIPTION      : This Function Del the Acceptable Master's or
 *                     Reset Alternate Priority Attribute
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
PtpWebDelAccMaster (INT4 i4ContextId,
                    INT4 i4DomainId, INT4 i4NwProto, INT4 i4AccLen,
                    UINT1 *u1AccMasterAddr)
{
    tSNMP_OCTET_STRING_TYPE AccMasterAddr;
    UINT1               au1AccMasterAddr[PTP_MASTER_MAX_ADDR_LEN];

    MEMSET (&(au1AccMasterAddr[0]), 0, PTP_MASTER_MAX_ADDR_LEN);
    AccMasterAddr.pu1_OctetList = &au1AccMasterAddr[0];

    PtpWebConverPortAddToOct (u1AccMasterAddr, i4NwProto, &AccMasterAddr);
    i4AccLen = AccMasterAddr.i4_Length;

    return (PtpWebUpdateAccMasterEntry (i4ContextId, i4DomainId,
                                        i4NwProto, i4AccLen,
                                        AccMasterAddr, DESTROY));

}

/******************************* PTP ENDS *******************************/

#endif
#endif
