/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: fltrweb.c,v 1.8 2013/07/03 12:19:58 siva Exp $
 *
 * Description: Routines for ISS web Module
 *******************************************************************/
#include "webiss.h"
#include "webinc.h"
#include "isshttp.h"
#include "fswebnm.h"
#include "fltrweb.h"
#include "rmap.h"
#include "vcm.h"

#ifdef OSPF3_WANTED
#include "ospf3.h"
#endif /* OSPF3_WANTED */

#ifdef OSPF_WANTED
#include "ospf.h"
#endif /* OSPF_WANTED */

#ifdef RIP6_WANTED
#include "ripv6.h"
#endif /* RIP6_WANTED */

#ifdef RIP_WANTED
#include "rip.h"
#endif /* RIP_WANTED */

#ifdef BGP_WANTED
#include "bgp.h"
#endif /* BGP_WANTED */

#ifdef ISIS_WANTED
#ifdef ROUTEMAP_WANTED
#include "isis.h"
#endif /* ROUTEMAP_WANTED */
#endif /* ISIS_WANTED */

#ifdef OSPF3_WANTED
/******************************************************************************
*  Function Name : IssSetProcessFilterOspfv3ConfPage
*  Description   : This function gets the user input from the web page and
*                  creates Ospfv3 route-map filters.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
******************************************************************************/
VOID
IssSetProcessFilterOspfv3ConfPage (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    UINT1               au1RouteMapName[RMAP_MAX_NAME_LEN + 4] = { 0 };
    INT4                i4FilterType = 0;
    INT4                i4DistanceValue = 0;
    INT4                i4RowStatusValue = 0;
    UINT4               u4ErrorCode = SNMP_ERR_NO_CREATION;
    BOOL1               bNeedWebUpdate = FALSE;
    INT4                i4OspfCxtId;
    /*Getting data from post html */

    /* Register the Ospfv3 Lock with Webnm Module and Take the Lock */
    WebnmRegisterLock (pHttp, V3OspfLock, V3OspfUnLock);
    V3OspfLock ();

    STRCPY (pHttp->au1Name, "CONTEXT_ID");

    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    i4OspfCxtId = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "futOspf3PreferenceValue");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4DistanceValue = (INT4) ATOI (pHttp->au1Value);

        if (nmhTestv2FsMIOspfv3PreferenceValue (&u4ErrorCode, i4OspfCxtId,
                                                i4DistanceValue) ==
            SNMP_SUCCESS)
        {
            if (nmhSetFsMIOspfv3PreferenceValue (i4OspfCxtId, i4DistanceValue)
                == SNMP_SUCCESS)
            {
                bNeedWebUpdate = TRUE;
            }
        }
        else
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Error: Incorrect distance value");
        }
    }
    else
    {
        RouteMapName.pu1_OctetList = au1RouteMapName;
        RouteMapName.i4_Length = 0;

        STRCPY (pHttp->au1Name, "futOspfv3DistInOutRouteMapName");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            RouteMapName.i4_Length = (INT4) STRLEN (pHttp->au1Value) + 1;
            if (RouteMapName.i4_Length > (RMAP_MAX_NAME_LEN + 4))
            {
                RouteMapName.i4_Length = RMAP_MAX_NAME_LEN + 4;
            }
            MEMCPY (RouteMapName.pu1_OctetList, pHttp->au1Value,
                    RouteMapName.i4_Length);
            RouteMapName.pu1_OctetList[RouteMapName.i4_Length - 1] = '\0';
        }

        STRCPY (pHttp->au1Name, "futOspfv3DistInOutRouteMapType");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4FilterType = (INT4) ATOI (pHttp->au1Value);
        }

        STRCPY (pHttp->au1Name, "futOspfv3DistInOutRouteMapValue");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4DistanceValue = (INT4) ATOI (pHttp->au1Value);
        }

        STRCPY (pHttp->au1Name, "futOspfv3DistInOutRouteMapRowStatus");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4RowStatusValue = (INT4) ATOI (pHttp->au1Value);
        }

        switch (i4FilterType)
        {
             /*FILTERING*/ case FILTERING_TYPE_DISTANCE:
                /*ON DELETE */
                if (6 == i4RowStatusValue)
                {
                    if (nmhGetFsMIOspfv3DistInOutRouteMapRowStatus
                        (i4OspfCxtId, &RouteMapName, FILTERING_TYPE_DISTANCE,
                         &i4RowStatusValue) == SNMP_SUCCESS)
                    {
                        if (nmhTestv2FsMIOspfv3DistInOutRouteMapRowStatus
                            (&u4ErrorCode, i4OspfCxtId, &RouteMapName,
                             FILTERING_TYPE_DISTANCE, DESTROY) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsMIOspfv3DistInOutRouteMapRowStatus
                                (i4OspfCxtId, &RouteMapName,
                                 FILTERING_TYPE_DISTANCE,
                                 DESTROY) == SNMP_SUCCESS)
                            {
                                bNeedWebUpdate = TRUE;
                                break;
                            }
                        }
                    }

                    IssSendError (pHttp, (CONST INT1 *) "Error: ERROR OCCUR");
                    break;
                }
                else
                    /*ON MODIFY */
                {
                    if (nmhGetFsMIOspfv3DistInOutRouteMapRowStatus
                        (i4OspfCxtId, &RouteMapName, FILTERING_TYPE_DISTANCE,
                         &i4RowStatusValue) != SNMP_SUCCESS)
                    {
                        if (nmhTestv2FsMIOspfv3DistInOutRouteMapRowStatus
                            (&u4ErrorCode, i4OspfCxtId, &RouteMapName,
                             FILTERING_TYPE_DISTANCE,
                             CREATE_AND_WAIT) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsMIOspfv3DistInOutRouteMapRowStatus
                                (i4OspfCxtId, &RouteMapName,
                                 FILTERING_TYPE_DISTANCE,
                                 CREATE_AND_WAIT) == SNMP_SUCCESS)
                            {
                                /*filter can be applied, route map has the same name */
                                u4ErrorCode = SNMP_ERR_NO_ERROR;
                            }
                        }
                        else
                        {
                            /*filter with cannot be applied, route map has another name */
                            IssSendError (pHttp,
                                          (CONST INT1 *)
                                          "Error: Distance filter already applied");
                            break;
                        }
                    }
                    else
                    {
                        /*new distance filter */
                        u4ErrorCode = SNMP_ERR_NO_ERROR;
                    }

                    if (SNMP_ERR_NO_ERROR == u4ErrorCode)
                    {
                        if (nmhTestv2FsMIOspfv3DistInOutRouteMapValue
                            (&u4ErrorCode, i4OspfCxtId, &RouteMapName,
                             FILTERING_TYPE_DISTANCE,
                             i4DistanceValue) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsMIOspfv3DistInOutRouteMapValue
                                (i4OspfCxtId, &RouteMapName,
                                 FILTERING_TYPE_DISTANCE,
                                 i4DistanceValue) == SNMP_SUCCESS)
                            {
                                if (nmhTestv2FsMIOspfv3DistInOutRouteMapRowStatus (&u4ErrorCode, i4OspfCxtId, &RouteMapName, FILTERING_TYPE_DISTANCE, ACTIVE) == SNMP_SUCCESS)
                                {
                                    if (nmhSetFsMIOspfv3DistInOutRouteMapRowStatus (i4OspfCxtId, &RouteMapName, FILTERING_TYPE_DISTANCE, ACTIVE) == SNMP_SUCCESS)
                                    {
                                        bNeedWebUpdate = TRUE;
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    IssSendError (pHttp, (CONST INT1 *) "ERROR OCCURS");
                    break;
                }
                break;            /*FILTERING_TYPE_DISTANCE */
            case FILTERING_TYPE_DISTRIB_IN:
                /*ON DELETE */
                if (6 == i4RowStatusValue)
                {
                    if (nmhGetFsMIOspfv3DistInOutRouteMapRowStatus
                        (i4OspfCxtId, &RouteMapName, FILTERING_TYPE_DISTRIB_IN,
                         &i4RowStatusValue) == SNMP_SUCCESS)
                    {
                        if (nmhTestv2FsMIOspfv3DistInOutRouteMapRowStatus
                            (&u4ErrorCode, i4OspfCxtId, &RouteMapName,
                             FILTERING_TYPE_DISTRIB_IN,
                             DESTROY) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsMIOspfv3DistInOutRouteMapRowStatus
                                (i4OspfCxtId, &RouteMapName,
                                 FILTERING_TYPE_DISTRIB_IN,
                                 DESTROY) == SNMP_SUCCESS)
                            {
                                bNeedWebUpdate = TRUE;
                                break;
                            }
                        }
                    }

                    IssSendError (pHttp, (CONST INT1 *) "ERROR OCCURS");
                    break;
                }
                else
                    /*ON MODIFY */
                {
                    if (nmhGetFsMIOspfv3DistInOutRouteMapRowStatus
                        (i4OspfCxtId, &RouteMapName, FILTERING_TYPE_DISTRIB_IN,
                         &i4RowStatusValue) != SNMP_SUCCESS)
                    {
                        if (nmhTestv2FsMIOspfv3DistInOutRouteMapRowStatus
                            (&u4ErrorCode, i4OspfCxtId, &RouteMapName,
                             FILTERING_TYPE_DISTRIB_IN,
                             CREATE_AND_WAIT) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsMIOspfv3DistInOutRouteMapRowStatus
                                (i4OspfCxtId, &RouteMapName,
                                 FILTERING_TYPE_DISTRIB_IN,
                                 CREATE_AND_WAIT) == SNMP_SUCCESS)
                            {
                                /*filter can be applied, route map has the same name */
                                u4ErrorCode = SNMP_ERR_NO_ERROR;
                            }
                        }
                    }
                    else
                    {
                        /*new distribute-in filter */
                        u4ErrorCode = SNMP_ERR_NO_ERROR;
                    }

                    if (SNMP_ERR_NO_ERROR == u4ErrorCode)
                    {
                        if (nmhTestv2FsMIOspfv3DistInOutRouteMapRowStatus
                            (&u4ErrorCode, i4OspfCxtId, &RouteMapName,
                             FILTERING_TYPE_DISTRIB_IN, ACTIVE) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsMIOspfv3DistInOutRouteMapRowStatus
                                (i4OspfCxtId, &RouteMapName,
                                 FILTERING_TYPE_DISTRIB_IN,
                                 ACTIVE) == SNMP_SUCCESS)
                            {
                                bNeedWebUpdate = TRUE;
                                break;
                            }
                        }
                    }

                    IssSendError (pHttp, (CONST INT1 *) "ERROR OCCURS");
                    break;
                }
                break;            /*FILTERING_TYPE_DISTRIB_IN */

            case FILTERING_TYPE_DISTRIB_OUT:
                /*Distibute-list out filter is not supported */
                break;            /*FILTERING_TYPE_DISTRIB_OUT */
            default:
                IssSendError (pHttp, (CONST INT1 *) "ERROR OCCURS");
        }
    }

    V3OspfUnLock ();
    WebnmUnRegisterLock (pHttp);

    if (TRUE == bNeedWebUpdate)
    {
        IssGetProcessFilterOspfv3ConfPage (pHttp);
    }

    return;
}

/*******************************************************************************
*  Function Name : IssGetProcessFilterOspfv3ConfPage
*  Description   : This function gets and displays all the Ospfv3 route-map
*                  filters created and enables the user to delete or modify
*                  filetrs.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssGetProcessFilterOspfv3ConfPage (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    UINT1               au1RouteMapName[RMAP_MAX_NAME_LEN + 4] = { 0 };
    UINT1              *pu1String;
    UINT4               u4Temp = 0;
    INT4                i4FilterType;
    INT4                i4DistanceValue;
    INT4                i4RowStatusValue;
    INT4                i4PrevCxtId = 0, i4CxtId = 0;
    pu1String = au1RouteMapName;
    RouteMapName.pu1_OctetList = pu1String;
    RouteMapName.i4_Length = 0;

    pHttp->i4Write = 0;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    IssPrintAvailableOspfv3ContextId (pHttp);

    WebnmRegisterLock (pHttp, V3OspfLock, V3OspfUnLock);
    V3OspfLock ();

    STRCPY (pHttp->au1Name, "CONTEXT_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4CxtId = ATOI (pHttp->au1Value);

    if (SNMP_SUCCESS ==
        nmhGetFsMIOspfv3PreferenceValue (i4CxtId, &i4DistanceValue))
    {
        if (0 == i4DistanceValue)
            i4DistanceValue = 110;
        STRCPY (pHttp->au1KeyString, "futOspf3PreferenceValue_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);    /*Sending till _KEY has found */
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 4, "%d", i4DistanceValue);    /*fill out pHttp->au1DataString by rmap name */
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    }

    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);

    V3OspfUnLock ();
    WebnmUnRegisterLock (pHttp);

    IssPrintAvailableOspfv3ContextId (pHttp);

    WebnmRegisterLock (pHttp, V3OspfLock, V3OspfUnLock);
    V3OspfLock ();

    /* Display the web page till the Table Flag */
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    if (SNMP_FAILURE ==
        nmhGetFirstIndexFsMIOspfv3DistInOutRouteMapTable (&i4CxtId,
                                                          &RouteMapName,
                                                          &i4FilterType))
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    else
    {
        u4Temp = (UINT4) pHttp->i4Write;
        do
        {

            pHttp->i4Write = (INT4) u4Temp;
            au1RouteMapName[RouteMapName.i4_Length] = '\0';    /*Null terminate routemap name */

            if ((SNMP_FAILURE ==
                 nmhGetFsMIOspfv3DistInOutRouteMapValue (i4CxtId, &RouteMapName,
                                                         i4FilterType,
                                                         &i4DistanceValue))
                || (SNMP_FAILURE ==
                    nmhGetFsMIOspfv3DistInOutRouteMapRowStatus (i4CxtId,
                                                                &RouteMapName,
                                                                i4FilterType,
                                                                &i4RowStatusValue)))
            {
                continue;
            }
            STRCPY (pHttp->au1KeyString, "CONTEXT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4CxtId);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "futOspfv3DistInOutRouteMapName_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);    /*Sending till _KEY has found */
            SNPRINTF ((CHR1 *) pHttp->au1DataString, RMAP_MAX_NAME_LEN + 1, "%s", pu1String);    /*fill out pHttp->au1DataString by rmap name */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "futOspfv3DistInOutRouteMapType_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);    /*Sending till _KEY has found */
            SNPRINTF ((CHR1 *) pHttp->au1DataString, 2, "%d", i4FilterType);    /*fill out pHttp->au1DataString by filter type */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "futOspfv3DistInOutRouteMapValue_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);    /*Sending till _KEY has found */
            SNPRINTF ((CHR1 *) pHttp->au1DataString, 4, "%d", i4DistanceValue);    /*fill out pHttp->au1DataString by rmap name */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString,
                    "futOspfv3DistInOutRouteMapRowStatus_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);    /*Sending till _KEY has found */
            SNPRINTF ((CHR1 *) pHttp->au1DataString, 2, "%d", i4RowStatusValue);    /*fill out pHttp->au1DataString by rmap name */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            i4PrevCxtId = i4CxtId;
        }
        while (SNMP_FAILURE !=
               nmhGetNextIndexFsMIOspfv3DistInOutRouteMapTable (i4PrevCxtId,
                                                                &i4CxtId,
                                                                &RouteMapName,
                                                                &RouteMapName,
                                                                i4FilterType,
                                                                &i4FilterType));

        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);

        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
    }

    V3OspfUnLock ();
    WebnmUnRegisterLock (pHttp);

    return;
}

/*******************************************************************************
*  Function Name : IssProcessFilterOspfv3ConfPage
*  Description   : This function processes the request coming for the Ospf3 rmap
*                  Filtering page
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssProcessFilterOspfv3ConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssGetProcessFilterOspfv3ConfPage (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssSetProcessFilterOspfv3ConfPage (pHttp);
    }
}
#endif /* OSPF3_WANTED */

#ifdef RIP6_WANTED
/******************************************************************************
*  Function Name : IssSetProcessFilterRip6ConfPage
*  Description   : This function gets the user input from the web page and
*                  creates Rip6 route-map filters.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
******************************************************************************/
VOID
IssSetProcessFilterRip6ConfPage (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    UINT1               au1RouteMapName[RMAP_MAX_NAME_LEN + 4] = { 0 };
    INT4                i4FilterType = 0;
    INT4                i4DistanceValue = 0;
    INT4                i4RowStatusValue = 0;
    UINT4               u4ErrorCode = SNMP_ERR_NO_CREATION;
    BOOL1               bNeedWebUpdate = FALSE;

    WebnmRegisterLock (pHttp, Rip6Lock, Rip6UnLock);
    Rip6Lock ();

    /*Getting data from post html */
    STRCPY (pHttp->au1Name, "fsRip6PreferenceValue");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4DistanceValue = (INT4) ATOI (pHttp->au1Value);

        if (nmhTestv2FsRip6PreferenceValue (&u4ErrorCode,
                                            i4DistanceValue) == SNMP_SUCCESS)
        {
            if (nmhSetFsRip6PreferenceValue (i4DistanceValue) == SNMP_SUCCESS)
            {
                bNeedWebUpdate = TRUE;
            }
        }
        else
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Error: Incorrect distance value");
        }
    }
    else
    {
        RouteMapName.pu1_OctetList = au1RouteMapName;
        RouteMapName.i4_Length = 0;

        STRCPY (pHttp->au1Name, "fsRip6DistInOutRouteMapName");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            RouteMapName.i4_Length = (INT4) STRLEN (pHttp->au1Value) + 1;
            if (RouteMapName.i4_Length > (RMAP_MAX_NAME_LEN + 4))
            {
                RouteMapName.i4_Length = RMAP_MAX_NAME_LEN + 4;
            }
            MEMCPY (RouteMapName.pu1_OctetList, pHttp->au1Value,
                    RouteMapName.i4_Length);
            RouteMapName.pu1_OctetList[RouteMapName.i4_Length - 1] = '\0';
        }

        STRCPY (pHttp->au1Name, "fsRip6DistInOutRouteMapType");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4FilterType = (INT4) ATOI (pHttp->au1Value);
        }

        STRCPY (pHttp->au1Name, "fsRip6DistInOutRouteMapValue");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4DistanceValue = (INT4) ATOI (pHttp->au1Value);
        }

        STRCPY (pHttp->au1Name, "fsRip6DistInOutRouteMapRowStatus");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4RowStatusValue = (INT4) ATOI (pHttp->au1Value);
        }

        switch (i4FilterType)
        {
             /*FILTERING*/ case FILTERING_TYPE_DISTANCE:
                /*ON DELETE */
                if (6 == i4RowStatusValue)
                {
                    if (nmhGetFsRip6DistInOutRouteMapRowStatus (&RouteMapName,
                                                                FILTERING_TYPE_DISTANCE,
                                                                &i4RowStatusValue)
                        == SNMP_SUCCESS)
                    {
                        if (nmhTestv2FsRip6DistInOutRouteMapRowStatus
                            (&u4ErrorCode, &RouteMapName,
                             FILTERING_TYPE_DISTANCE, DESTROY) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsRip6DistInOutRouteMapRowStatus
                                (&RouteMapName, FILTERING_TYPE_DISTANCE,
                                 DESTROY) == SNMP_SUCCESS)
                            {
                                bNeedWebUpdate = TRUE;
                                break;
                            }
                        }
                    }

                    IssSendError (pHttp, (CONST INT1 *) "Error: ERROR OCCUR");
                    break;
                }
                else
                    /*ON MODIFY */
                {
                    if (nmhGetFsRip6DistInOutRouteMapRowStatus (&RouteMapName,
                                                                FILTERING_TYPE_DISTANCE,
                                                                &i4RowStatusValue)
                        != SNMP_SUCCESS)
                    {
                        if (nmhTestv2FsRip6DistInOutRouteMapRowStatus
                            (&u4ErrorCode, &RouteMapName,
                             FILTERING_TYPE_DISTANCE,
                             CREATE_AND_WAIT) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsRip6DistInOutRouteMapRowStatus
                                (&RouteMapName, FILTERING_TYPE_DISTANCE,
                                 CREATE_AND_WAIT) == SNMP_SUCCESS)
                            {
                                /*filter can be applied, route map has the same name */
                                u4ErrorCode = SNMP_ERR_NO_ERROR;
                            }
                        }
                        else
                        {
                            /*filter with cannot be applied, route map has another name */
                            IssSendError (pHttp,
                                          (CONST INT1 *)
                                          "Error: Distance filter already applied");
                            break;
                        }
                    }
                    else
                    {
                        /*new distance filter */
                        u4ErrorCode = SNMP_ERR_NO_ERROR;
                    }

                    if (SNMP_ERR_NO_ERROR == u4ErrorCode)
                    {
                        if (nmhTestv2FsRip6DistInOutRouteMapValue (&u4ErrorCode,
                                                                   &RouteMapName,
                                                                   FILTERING_TYPE_DISTANCE,
                                                                   i4DistanceValue)
                            == SNMP_SUCCESS)
                        {
                            if (nmhSetFsRip6DistInOutRouteMapValue
                                (&RouteMapName, FILTERING_TYPE_DISTANCE,
                                 i4DistanceValue) == SNMP_SUCCESS)
                            {
                                if (nmhTestv2FsRip6DistInOutRouteMapRowStatus
                                    (&u4ErrorCode, &RouteMapName,
                                     FILTERING_TYPE_DISTANCE,
                                     ACTIVE) == SNMP_SUCCESS)
                                {
                                    if (nmhSetFsRip6DistInOutRouteMapRowStatus
                                        (&RouteMapName, FILTERING_TYPE_DISTANCE,
                                         ACTIVE) == SNMP_SUCCESS)
                                    {
                                        bNeedWebUpdate = TRUE;
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    IssSendError (pHttp, (CONST INT1 *) "ERROR OCCURS");
                    break;
                }
                break;            /*FILTERING_TYPE_DISTANCE */
            case FILTERING_TYPE_DISTRIB_IN:
            case FILTERING_TYPE_DISTRIB_OUT:
                /*ON DELETE */
                if (6 == i4RowStatusValue)
                {
                    if (nmhGetFsRip6DistInOutRouteMapRowStatus (&RouteMapName,
                                                                i4FilterType,
                                                                &i4RowStatusValue)
                        == SNMP_SUCCESS)
                    {
                        if (nmhTestv2FsRip6DistInOutRouteMapRowStatus
                            (&u4ErrorCode, &RouteMapName, i4FilterType,
                             DESTROY) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsRip6DistInOutRouteMapRowStatus
                                (&RouteMapName, i4FilterType,
                                 DESTROY) == SNMP_SUCCESS)
                            {
                                bNeedWebUpdate = TRUE;
                                break;
                            }
                        }
                    }

                    IssSendError (pHttp, (CONST INT1 *) "ERROR OCCURS");
                    break;
                }
                else
                    /*ON MODIFY */
                {

                    if (nmhGetFsRip6DistInOutRouteMapRowStatus (&RouteMapName,
                                                                i4FilterType,
                                                                &i4RowStatusValue)
                        != SNMP_SUCCESS)
                    {
                        if (nmhTestv2FsRip6DistInOutRouteMapRowStatus
                            (&u4ErrorCode, &RouteMapName, i4FilterType,
                             CREATE_AND_WAIT) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsRip6DistInOutRouteMapRowStatus
                                (&RouteMapName, i4FilterType,
                                 CREATE_AND_WAIT) == SNMP_SUCCESS)
                            {
                                /*filter can be applied, route map has the same name */
                                u4ErrorCode = SNMP_ERR_NO_ERROR;
                            }
                        }
                    }
                    else
                    {
                        /*new distribute-list filter */
                        u4ErrorCode = SNMP_ERR_NO_ERROR;
                    }

                    if (SNMP_ERR_NO_ERROR == u4ErrorCode)
                    {
                        if (nmhTestv2FsRip6DistInOutRouteMapRowStatus
                            (&u4ErrorCode, &RouteMapName, i4FilterType,
                             ACTIVE) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsRip6DistInOutRouteMapRowStatus
                                (&RouteMapName, i4FilterType,
                                 ACTIVE) == SNMP_SUCCESS)
                            {
                                bNeedWebUpdate = TRUE;
                                break;
                            }
                        }
                    }

                    IssSendError (pHttp, (CONST INT1 *) "ERROR OCCURS");
                    break;
                }
                break;            /*FILTERING_TYPE_DISTRIB_IN */
            default:
                IssSendError (pHttp, (CONST INT1 *) "ERROR OCCURS");
        }
    }

    Rip6UnLock ();
    WebnmUnRegisterLock (pHttp);

    if (TRUE == bNeedWebUpdate)
    {
        IssGetProcessFilterRip6ConfPage (pHttp);
    }

    return;
}

/*******************************************************************************
*  Function Name : IssGetProcessFilterRip6ConfPage
*  Description   : This function gets and displays all the Rip6 route-map
*                  filters created and enables the user to delete or modify
*                  filetrs.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssGetProcessFilterRip6ConfPage (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    UINT1               au1RouteMapName[RMAP_MAX_NAME_LEN + 4] = { 0 };
    UINT1              *pu1String;
    UINT4               u4Temp = 0;
    INT4                i4FilterType;
    INT4                i4DistanceValue;
    INT4                i4RowStatusValue;

    pu1String = au1RouteMapName;
    RouteMapName.pu1_OctetList = pu1String;
    RouteMapName.i4_Length = 0;

    pHttp->i4Write = 0;

    WebnmRegisterLock (pHttp, Rip6Lock, Rip6UnLock);
    Rip6Lock ();

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    if (SNMP_SUCCESS == nmhGetFsRip6PreferenceValue (&i4DistanceValue))
    {
        if (0 == i4DistanceValue)
            i4DistanceValue = 120;
        STRCPY (pHttp->au1KeyString, "fsRip6PreferenceValue_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);    /*Sending till _KEY has found */
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 4, "%d", i4DistanceValue);    /*fill out pHttp->au1DataString by rmap name */
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    }

    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);

    /* Display the web page till the Table Flag */
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    if (SNMP_FAILURE ==
        nmhGetFirstIndexFsRip6DistInOutRouteMapTable (&RouteMapName,
                                                      &i4FilterType))
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    else
    {
        u4Temp = (UINT4) pHttp->i4Write;
        do
        {

            pHttp->i4Write = (INT4) u4Temp;
            au1RouteMapName[RouteMapName.i4_Length] = '\0';    /*Null terminate routemap name */

            if ((SNMP_FAILURE ==
                 nmhGetFsRip6DistInOutRouteMapValue (&RouteMapName,
                                                     i4FilterType,
                                                     &i4DistanceValue))
                || (SNMP_FAILURE ==
                    nmhGetFsRip6DistInOutRouteMapRowStatus (&RouteMapName,
                                                            i4FilterType,
                                                            &i4RowStatusValue)))
            {
                continue;
            }

            STRCPY (pHttp->au1KeyString, "fsRip6DistInOutRouteMapName_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);    /*Sending till _KEY has found */
            SNPRINTF ((CHR1 *) pHttp->au1DataString, RMAP_MAX_NAME_LEN + 1, "%s", pu1String);    /*fill out pHttp->au1DataString by rmap name */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "fsRip6DistInOutRouteMapType_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);    /*Sending till _KEY has found */
            SNPRINTF ((CHR1 *) pHttp->au1DataString, 2, "%d", i4FilterType);    /*fill out pHttp->au1DataString by filter type */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "fsRip6DistInOutRouteMapValue_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);    /*Sending till _KEY has found */
            SNPRINTF ((CHR1 *) pHttp->au1DataString, 4, "%d", i4DistanceValue);    /*fill out pHttp->au1DataString by rmap name */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString,
                    "fsRip6DistInOutRouteMapRowStatus_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);    /*Sending till _KEY has found */
            SNPRINTF ((CHR1 *) pHttp->au1DataString, 2, "%d", i4RowStatusValue);    /*fill out pHttp->au1DataString by rmap name */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

        }
        while (SNMP_FAILURE !=
               nmhGetNextIndexFsRip6DistInOutRouteMapTable (&RouteMapName,
                                                            &RouteMapName,
                                                            i4FilterType,
                                                            &i4FilterType));

        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);

        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
    }

    Rip6UnLock ();
    WebnmUnRegisterLock (pHttp);

    return;
}

/*******************************************************************************
*  Function Name : IssProcessFilterRip6ConfPage
*  Description   : This function processes the request coming for the Rip6 rmap
*                  Filtering page
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssProcessFilterRip6ConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssGetProcessFilterRip6ConfPage (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssSetProcessFilterRip6ConfPage (pHttp);
    }
}
#endif /* RIP6_WANTED */

#ifdef OSPF_WANTED

/*******************************************************************************
*  Function Name : IssGetProcessFilterOspfConfPage
*  Description   : This function gets and displays all the Ospf route-map
*                  filters created and enables the user to delete or modify
*                  filetrs.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssGetProcessFilterOspfConfPage (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    UINT1               au1RouteMapName[RMAP_MAX_NAME_LEN + 4] = { 0 };
    UINT1              *pu1String;
    UINT4               u4Temp = 0;
    INT4                i4FilterType;
    INT4                i4DistanceValue;
    INT4                i4RowStatusValue;
    INT4                i4PrevCxtId = 0, i4CxtId = 0;
    pu1String = au1RouteMapName;
    RouteMapName.pu1_OctetList = pu1String;
    RouteMapName.i4_Length = 0;

    pHttp->i4Write = 0;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    IssPrintAvailableOspfContextId (pHttp);

    WebnmRegisterLock (pHttp, OspfLock, OspfUnLock);
    OspfLock ();

    STRCPY (pHttp->au1Name, "CONTEXT_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4CxtId = ATOI (pHttp->au1Value);

    if (SNMP_SUCCESS ==
        nmhGetFsMIOspfPreferenceValue (i4CxtId, &i4DistanceValue))
    {
        if (0 == i4DistanceValue)
            i4DistanceValue = 119;
        STRCPY (pHttp->au1KeyString, "futOspfPreferenceValue_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);    /*Sending till _KEY has found */
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 4, "%d", i4DistanceValue);    /*fill out pHttp->au1DataString by rmap name */
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    }

    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    OspfUnLock ();
    WebnmUnRegisterLock (pHttp);

    IssPrintAvailableOspfContextId (pHttp);

    WebnmRegisterLock (pHttp, OspfLock, OspfUnLock);
    OspfLock ();
    /* Display the web page till the Table Flag */
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    if (SNMP_FAILURE ==
        nmhGetFirstIndexFsMIOspfDistInOutRouteMapTable (&i4CxtId, &RouteMapName,
                                                        &i4FilterType))
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    else
    {
        u4Temp = (UINT4) pHttp->i4Write;
        do
        {

            pHttp->i4Write = (INT4) u4Temp;
            au1RouteMapName[RouteMapName.i4_Length] = '\0';    /*Null terminate routemap name */

            if ((SNMP_FAILURE ==
                 nmhGetFsMIOspfDistInOutRouteMapValue (i4CxtId, &RouteMapName,
                                                       i4FilterType,
                                                       &i4DistanceValue))
                || (SNMP_FAILURE ==
                    nmhGetFsMIOspfDistInOutRouteMapRowStatus (i4CxtId,
                                                              &RouteMapName,
                                                              i4FilterType,
                                                              &i4RowStatusValue)))
            {
                continue;
            }
            STRCPY (pHttp->au1KeyString, "CONTEXT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4CxtId);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "futOspfDistInOutRouteMapName_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);    /*Sending till _KEY has found */
            SNPRINTF ((CHR1 *) pHttp->au1DataString, RMAP_MAX_NAME_LEN + 1, "%s", pu1String);    /*fill out pHttp->au1DataString by rmap name */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "futOspfDistInOutRouteMapType_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);    /*Sending till _KEY has found */
            SNPRINTF ((CHR1 *) pHttp->au1DataString, 2, "%d", i4FilterType);    /*fill out pHttp->au1DataString by filter type */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "futOspfDistInOutRouteMapValue_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);    /*Sending till _KEY has found */
            SNPRINTF ((CHR1 *) pHttp->au1DataString, 4, "%d", i4DistanceValue);    /*fill out pHttp->au1DataString by rmap name */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString,
                    "futOspfDistInOutRouteMapRowStatus_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);    /*Sending till _KEY has found */
            SNPRINTF ((CHR1 *) pHttp->au1DataString, 2, "%d", i4RowStatusValue);    /*fill out pHttp->au1DataString by rmap name */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            i4PrevCxtId = i4CxtId;
        }
        while (SNMP_FAILURE !=
               nmhGetNextIndexFsMIOspfDistInOutRouteMapTable (i4PrevCxtId,
                                                              &i4CxtId,
                                                              &RouteMapName,
                                                              &RouteMapName,
                                                              i4FilterType,
                                                              &i4FilterType));

        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);

        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
    }

    OspfUnLock ();
    WebnmUnRegisterLock (pHttp);

    return;
}

/******************************************************************************
*  Function Name : IssSetProcessFilterOspfConfPage
*  Description   : This function gets the user input from the web page and
*                  creates Ospf route-map filters.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
******************************************************************************/
VOID
IssSetProcessFilterOspfConfPage (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    UINT1               au1RouteMapName[RMAP_MAX_NAME_LEN + 4] = { 0 };
    INT4                i4FilterType = 0;
    INT4                i4DistanceValue = 0;
    INT4                i4RowStatusValue = 0;
    UINT4               u4ErrorCode = SNMP_ERR_NO_CREATION;
    BOOL1               bNeedWebUpdate = FALSE;
    INT4                i4OspfCxtId = (INT4) OSPF_INVALID_CXT_ID;

    /*Getting data from post html */
    WebnmRegisterLock (pHttp, OspfLock, OspfUnLock);
    OspfLock ();

    STRCPY (pHttp->au1Name, "CONTEXT_ID");

    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    i4OspfCxtId = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "futOspfPreferenceValue");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4DistanceValue = (INT4) ATOI (pHttp->au1Value);

        if (nmhTestv2FsMIOspfPreferenceValue (&u4ErrorCode,
                                              i4OspfCxtId,
                                              i4DistanceValue) == SNMP_SUCCESS)
        {
            if (nmhSetFsMIOspfPreferenceValue (i4OspfCxtId, i4DistanceValue)
                == SNMP_SUCCESS)
            {
                bNeedWebUpdate = TRUE;
            }
        }
        else
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Error: Incorrect distance value");
        }
    }
    else
    {
        RouteMapName.pu1_OctetList = au1RouteMapName;
        RouteMapName.i4_Length = 0;

        STRCPY (pHttp->au1Name, "futOspfDistInOutRouteMapName");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            RouteMapName.i4_Length = (INT4) (STRLEN (pHttp->au1Value) + 1);
            if (RouteMapName.i4_Length > (RMAP_MAX_NAME_LEN + 4))
            {
                RouteMapName.i4_Length = RMAP_MAX_NAME_LEN + 4;
            }
            MEMCPY (RouteMapName.pu1_OctetList, pHttp->au1Value,
                    RouteMapName.i4_Length);
            RouteMapName.pu1_OctetList[RouteMapName.i4_Length - 1] = '\0';
        }

        STRCPY (pHttp->au1Name, "futOspfDistInOutRouteMapType");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4FilterType = (INT4) ATOI (pHttp->au1Value);
        }

        STRCPY (pHttp->au1Name, "futOspfDistInOutRouteMapValue");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4DistanceValue = (INT4) ATOI (pHttp->au1Value);
        }

        STRCPY (pHttp->au1Name, "futOspfDistInOutRouteMapRowStatus");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4RowStatusValue = (INT4) ATOI (pHttp->au1Value);
        }

        switch (i4FilterType)
        {
             /*FILTERING*/ case FILTERING_TYPE_DISTANCE:
                /*ON DELETE */
                if (6 == i4RowStatusValue)
                {
                    if (nmhGetFsMIOspfDistInOutRouteMapRowStatus (i4OspfCxtId,
                                                                  &RouteMapName,
                                                                  FILTERING_TYPE_DISTANCE,
                                                                  &i4RowStatusValue)
                        == SNMP_SUCCESS)
                    {
                        if (nmhTestv2FsMIOspfDistInOutRouteMapRowStatus
                            (&u4ErrorCode, i4OspfCxtId, &RouteMapName,
                             FILTERING_TYPE_DISTANCE, DESTROY) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsMIOspfDistInOutRouteMapRowStatus
                                (i4OspfCxtId, &RouteMapName,
                                 FILTERING_TYPE_DISTANCE,
                                 DESTROY) == SNMP_SUCCESS)
                            {
                                bNeedWebUpdate = TRUE;
                                break;
                            }
                        }
                    }

                    IssSendError (pHttp, (CONST INT1 *) "Error: ERROR OCCUR");
                    break;
                }
                else
                    /*ON MODIFY */
                {
                    if (nmhGetFsMIOspfDistInOutRouteMapRowStatus (i4OspfCxtId,
                                                                  &RouteMapName,
                                                                  FILTERING_TYPE_DISTANCE,
                                                                  &i4RowStatusValue)
                        != SNMP_SUCCESS)
                    {
                        if (nmhTestv2FsMIOspfDistInOutRouteMapRowStatus
                            (&u4ErrorCode, i4OspfCxtId, &RouteMapName,
                             FILTERING_TYPE_DISTANCE,
                             CREATE_AND_WAIT) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsMIOspfDistInOutRouteMapRowStatus
                                (i4OspfCxtId, &RouteMapName,
                                 FILTERING_TYPE_DISTANCE,
                                 CREATE_AND_WAIT) == SNMP_SUCCESS)
                            {
                                /*filter can be applied, route map has the same name */
                                u4ErrorCode = SNMP_ERR_NO_ERROR;
                            }
                        }
                        else
                        {
                            /*filter with cannot be applied, route map has another name */
                            IssSendError (pHttp,
                                          (CONST INT1 *)
                                          "Error: Distance filter already applied");
                            break;
                        }
                    }
                    else
                    {
                        /*new distance filter */
                        u4ErrorCode = SNMP_ERR_NO_ERROR;
                    }

                    if (SNMP_ERR_NO_ERROR == u4ErrorCode)
                    {
                        if (nmhTestv2FsMIOspfDistInOutRouteMapValue
                            (&u4ErrorCode, i4OspfCxtId, &RouteMapName,
                             FILTERING_TYPE_DISTANCE,
                             i4DistanceValue) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsMIOspfDistInOutRouteMapValue
                                (i4OspfCxtId, &RouteMapName,
                                 FILTERING_TYPE_DISTANCE,
                                 i4DistanceValue) == SNMP_SUCCESS)
                            {
                                if (nmhTestv2FsMIOspfDistInOutRouteMapRowStatus
                                    (&u4ErrorCode, i4OspfCxtId, &RouteMapName,
                                     FILTERING_TYPE_DISTANCE,
                                     ACTIVE) == SNMP_SUCCESS)
                                {
                                    if (nmhSetFsMIOspfDistInOutRouteMapRowStatus
                                        (i4OspfCxtId, &RouteMapName,
                                         FILTERING_TYPE_DISTANCE,
                                         ACTIVE) == SNMP_SUCCESS)
                                    {
                                        bNeedWebUpdate = TRUE;
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    IssSendError (pHttp, (CONST INT1 *) "ERROR OCCURS");
                    break;
                }
                break;            /*FILTERING_TYPE_DISTANCE */
            case FILTERING_TYPE_DISTRIB_IN:
                /*ON DELETE */
                if (6 == i4RowStatusValue)
                {
                    if (nmhGetFsMIOspfDistInOutRouteMapRowStatus (i4OspfCxtId,
                                                                  &RouteMapName,
                                                                  FILTERING_TYPE_DISTRIB_IN,
                                                                  &i4RowStatusValue)
                        == SNMP_SUCCESS)
                    {
                        if (nmhTestv2FsMIOspfDistInOutRouteMapRowStatus
                            (&u4ErrorCode, i4OspfCxtId, &RouteMapName,
                             FILTERING_TYPE_DISTRIB_IN,
                             DESTROY) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsMIOspfDistInOutRouteMapRowStatus
                                (i4OspfCxtId, &RouteMapName,
                                 FILTERING_TYPE_DISTRIB_IN,
                                 DESTROY) == SNMP_SUCCESS)
                            {
                                bNeedWebUpdate = TRUE;
                                break;
                            }
                        }
                    }

                    IssSendError (pHttp, (CONST INT1 *) "ERROR OCCURS");
                    break;
                }
                else
                    /*ON MODIFY */
                {
                    if (nmhGetFsMIOspfDistInOutRouteMapRowStatus
                        (i4OspfCxtId, &RouteMapName, FILTERING_TYPE_DISTRIB_IN,
                         &i4RowStatusValue) != SNMP_SUCCESS)
                    {
                        if (nmhTestv2FsMIOspfDistInOutRouteMapRowStatus
                            (&u4ErrorCode, i4OspfCxtId, &RouteMapName,
                             FILTERING_TYPE_DISTRIB_IN,
                             CREATE_AND_WAIT) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsMIOspfDistInOutRouteMapRowStatus
                                (i4OspfCxtId, &RouteMapName,
                                 FILTERING_TYPE_DISTRIB_IN,
                                 CREATE_AND_WAIT) == SNMP_SUCCESS)
                            {
                                /*filter can be applied, route map has the same name */
                                u4ErrorCode = SNMP_ERR_NO_ERROR;
                            }
                        }
                    }
                    else
                    {
                        /*new distribute-in filter */
                        u4ErrorCode = SNMP_ERR_NO_ERROR;
                    }

                    if (SNMP_ERR_NO_ERROR == u4ErrorCode)
                    {
                        if (nmhTestv2FsMIOspfDistInOutRouteMapRowStatus
                            (&u4ErrorCode, i4OspfCxtId, &RouteMapName,
                             FILTERING_TYPE_DISTRIB_IN, ACTIVE) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsMIOspfDistInOutRouteMapRowStatus
                                (i4OspfCxtId, &RouteMapName,
                                 FILTERING_TYPE_DISTRIB_IN,
                                 ACTIVE) == SNMP_SUCCESS)
                            {
                                bNeedWebUpdate = TRUE;
                                break;
                            }
                        }
                    }

                    IssSendError (pHttp, (CONST INT1 *) "ERROR OCCURS");
                    break;
                }
                break;            /*FILTERING_TYPE_DISTRIB_IN */

            case FILTERING_TYPE_DISTRIB_OUT:
                /*Distibute-list out filter is not supported */
                break;            /*FILTERING_TYPE_DISTRIB_OUT */
            default:
                IssSendError (pHttp, (CONST INT1 *) "ERROR OCCURS");
        }
    }

    OspfUnLock ();
    WebnmUnRegisterLock (pHttp);

    if (TRUE == bNeedWebUpdate)
    {
        IssGetProcessFilterOspfConfPage (pHttp);
    }

    return;
}

/*******************************************************************************
*  Function Name : IssProcessFilterOspfConfPage
*  Description   : This function processes the request coming for the Ospf rmap
*                  Filtering page
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssProcessFilterOspfConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssGetProcessFilterOspfConfPage (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssSetProcessFilterOspfConfPage (pHttp);
    }
}

#endif /* OSPF_WANTED */

#ifdef RIP_WANTED

/*******************************************************************************
*  Function Name : IssGetProcessFilterRipConfPage
*  Description   : This function gets and displays all the Rip route-map
*                  filters created and enables the user to delete or modify
*                  filetrs.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssGetProcessFilterRipConfPage (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    UINT1               au1RouteMapName[RMAP_MAX_NAME_LEN + 4] = { 0 };
    UINT1              *pu1String;
    UINT4               u4Temp = 0;
    INT4                i4FilterType;
    INT4                i4DistanceValue;
    INT4                i4RowStatusValue;
    INT4                i4PrevCxtId = 0, i4CxtId = 0;

    pu1String = au1RouteMapName;
    RouteMapName.pu1_OctetList = pu1String;
    RouteMapName.i4_Length = 0;

    pHttp->i4Write = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    IssPrintAvailableRipContextId (pHttp);

    WebnmRegisterLock (pHttp, RipLock, RipUnLock);
    RipLock ();

    STRCPY (pHttp->au1Name, "CONTEXT_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4CxtId = ATOI (pHttp->au1Value);

    if (SNMP_SUCCESS ==
        nmhGetFsMIRipPreferenceValue (i4CxtId, &i4DistanceValue))
    {
        if (0 == i4DistanceValue)
            i4DistanceValue = 121;
        STRCPY (pHttp->au1KeyString, "fsRipPreferenceValue_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);    /*Sending till _KEY has found */
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 4, "%d", i4DistanceValue);    /*fill out pHttp->au1DataString by rmap name */
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    }

    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);

    RipUnLock ();
    WebnmUnRegisterLock (pHttp);

    IssPrintAvailableRipContextId (pHttp);

    WebnmRegisterLock (pHttp, RipLock, RipUnLock);
    RipLock ();
    /* Display the web page till the Table Flag */
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    if (SNMP_FAILURE ==
        nmhGetFirstIndexFsMIRipDistInOutRouteMapTable (&i4CxtId, &RouteMapName,
                                                       &i4FilterType))
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    else
    {
        u4Temp = (UINT4) pHttp->i4Write;
        do
        {

            pHttp->i4Write = (INT4) u4Temp;
            au1RouteMapName[RouteMapName.i4_Length] = '\0';    /*Null terminate routemap name */

            if ((SNMP_FAILURE ==
                 nmhGetFsMIRipDistInOutRouteMapValue (i4CxtId, &RouteMapName,
                                                      i4FilterType,
                                                      &i4DistanceValue))
                || (SNMP_FAILURE ==
                    nmhGetFsMIRipDistInOutRouteMapRowStatus (i4CxtId,
                                                             &RouteMapName,
                                                             i4FilterType,
                                                             &i4RowStatusValue)))
            {
                continue;
            }
            STRCPY (pHttp->au1KeyString, "CONTEXT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4CxtId);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "fsRipDistInOutRouteMapName_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);    /*Sending till _KEY has found */
            SNPRINTF ((CHR1 *) pHttp->au1DataString, RMAP_MAX_NAME_LEN + 1, "%s", pu1String);    /*fill out pHttp->au1DataString by rmap name */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "fsRipDistInOutRouteMapType_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);    /*Sending till _KEY has found */
            SNPRINTF ((CHR1 *) pHttp->au1DataString, 2, "%d", i4FilterType);    /*fill out pHttp->au1DataString by filter type */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "fsRipDistInOutRouteMapValue_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);    /*Sending till _KEY has found */
            SNPRINTF ((CHR1 *) pHttp->au1DataString, 4, "%d", i4DistanceValue);    /*fill out pHttp->au1DataString by rmap name */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "fsRipDistInOutRouteMapRowStatus_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);    /*Sending till _KEY has found */
            SNPRINTF ((CHR1 *) pHttp->au1DataString, 2, "%d", i4RowStatusValue);    /*fill out pHttp->au1DataString by rmap name */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            i4PrevCxtId = i4CxtId;
        }
        while (SNMP_FAILURE !=
               nmhGetNextIndexFsMIRipDistInOutRouteMapTable (i4PrevCxtId,
                                                             &i4CxtId,
                                                             &RouteMapName,
                                                             &RouteMapName,
                                                             i4FilterType,
                                                             &i4FilterType));

        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);

        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
    }

    RipUnLock ();
    WebnmUnRegisterLock (pHttp);

    return;
}

/******************************************************************************
*  Function Name : IssSetProcessFilterRipConfPage
*  Description   : This function gets the user input from the web page and
*                  creates Rip route-map filters.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
******************************************************************************/
VOID
IssSetProcessFilterRipConfPage (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    UINT1               au1RouteMapName[RMAP_MAX_NAME_LEN + 4] = { 0 };
    INT4                i4FilterType = 0;
    INT4                i4DistanceValue = 0;
    INT4                i4RowStatusValue = 0;
    UINT4               u4ErrorCode = SNMP_ERR_NO_CREATION;
    BOOL1               bNeedWebUpdate = FALSE;
    INT4                i4OspfCxtId;
    WebnmRegisterLock (pHttp, RipLock, RipUnLock);
    RipLock ();

    STRCPY (pHttp->au1Name, "CONTEXT_ID");

    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    i4OspfCxtId = ATOI (pHttp->au1Value);

    /*Getting data from post html */
    STRCPY (pHttp->au1Name, "fsRipPreferenceValue");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4DistanceValue = (INT4) ATOI (pHttp->au1Value);

        if (nmhTestv2FsMIRipPreferenceValue (&u4ErrorCode, i4OspfCxtId,
                                             i4DistanceValue) == SNMP_SUCCESS)
        {
            if (nmhSetFsMIRipPreferenceValue (i4OspfCxtId, i4DistanceValue) ==
                SNMP_SUCCESS)
            {
                bNeedWebUpdate = TRUE;
            }
        }
        else
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Error: Incorrect distance value");
        }
    }
    else
    {
        RouteMapName.pu1_OctetList = au1RouteMapName;
        RouteMapName.i4_Length = 0;

        STRCPY (pHttp->au1Name, "fsRipDistInOutRouteMapName");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            RouteMapName.i4_Length = (INT4) (STRLEN (pHttp->au1Value) + 1);
            if (RouteMapName.i4_Length > (RMAP_MAX_NAME_LEN + 4))
            {
                RouteMapName.i4_Length = RMAP_MAX_NAME_LEN + 4;
            }
            MEMCPY (RouteMapName.pu1_OctetList, pHttp->au1Value,
                    RouteMapName.i4_Length);
            RouteMapName.pu1_OctetList[RouteMapName.i4_Length - 1] = '\0';
        }

        STRCPY (pHttp->au1Name, "fsRipDistInOutRouteMapType");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4FilterType = (INT4) ATOI (pHttp->au1Value);
        }

        STRCPY (pHttp->au1Name, "fsRipDistInOutRouteMapValue");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4DistanceValue = (INT4) ATOI (pHttp->au1Value);
        }

        STRCPY (pHttp->au1Name, "fsRipDistInOutRouteMapRowStatus");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4RowStatusValue = (INT4) ATOI (pHttp->au1Value);
        }

        switch (i4FilterType)
        {
             /*FILTERING*/ case FILTERING_TYPE_DISTANCE:
                /*ON DELETE */
                if (6 == i4RowStatusValue)
                {
                    if (nmhGetFsMIRipDistInOutRouteMapRowStatus
                        (i4OspfCxtId, &RouteMapName, FILTERING_TYPE_DISTANCE,
                         &i4RowStatusValue) == SNMP_SUCCESS)
                    {
                        if (nmhTestv2FsMIRipDistInOutRouteMapRowStatus
                            (&u4ErrorCode, i4OspfCxtId, &RouteMapName,
                             FILTERING_TYPE_DISTANCE, DESTROY) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsMIRipDistInOutRouteMapRowStatus
                                (i4OspfCxtId, &RouteMapName,
                                 FILTERING_TYPE_DISTANCE,
                                 DESTROY) == SNMP_SUCCESS)
                            {
                                bNeedWebUpdate = TRUE;
                                break;
                            }
                        }
                    }

                    IssSendError (pHttp, (CONST INT1 *) "Error: ERROR OCCUR");
                    break;
                }
                else
                    /*ON MODIFY */
                {
                    if (nmhGetFsMIRipDistInOutRouteMapRowStatus
                        (i4OspfCxtId, &RouteMapName, FILTERING_TYPE_DISTANCE,
                         &i4RowStatusValue) != SNMP_SUCCESS)
                    {
                        if (nmhTestv2FsMIRipDistInOutRouteMapRowStatus
                            (&u4ErrorCode, i4OspfCxtId, &RouteMapName,
                             FILTERING_TYPE_DISTANCE,
                             CREATE_AND_WAIT) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsMIRipDistInOutRouteMapRowStatus
                                (i4OspfCxtId, &RouteMapName,
                                 FILTERING_TYPE_DISTANCE,
                                 CREATE_AND_WAIT) == SNMP_SUCCESS)
                            {
                                /*filter can be applied, route map has the same name */
                                u4ErrorCode = SNMP_ERR_NO_ERROR;
                            }
                        }
                        else
                        {
                            /*filter with cannot be applied, route map has another name */
                            IssSendError (pHttp,
                                          (CONST INT1 *)
                                          "Error: Distance filter already applied");
                            break;
                        }
                    }
                    else
                    {
                        /*new distance filter */
                        u4ErrorCode = SNMP_ERR_NO_ERROR;
                    }

                    if (SNMP_ERR_NO_ERROR == u4ErrorCode)
                    {
                        if (nmhTestv2FsMIRipDistInOutRouteMapValue
                            (&u4ErrorCode, i4OspfCxtId, &RouteMapName,
                             FILTERING_TYPE_DISTANCE,
                             i4DistanceValue) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsMIRipDistInOutRouteMapValue
                                (i4OspfCxtId, &RouteMapName,
                                 FILTERING_TYPE_DISTANCE,
                                 i4DistanceValue) == SNMP_SUCCESS)
                            {
                                if (nmhTestv2FsMIRipDistInOutRouteMapRowStatus
                                    (&u4ErrorCode, i4OspfCxtId, &RouteMapName,
                                     FILTERING_TYPE_DISTANCE,
                                     ACTIVE) == SNMP_SUCCESS)
                                {
                                    if (nmhSetFsMIRipDistInOutRouteMapRowStatus
                                        (i4OspfCxtId, &RouteMapName,
                                         FILTERING_TYPE_DISTANCE,
                                         ACTIVE) == SNMP_SUCCESS)
                                    {
                                        bNeedWebUpdate = TRUE;
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    IssSendError (pHttp, (CONST INT1 *) "ERROR OCCURS");
                    break;
                }
                break;            /*FILTERING_TYPE_DISTANCE */
            case FILTERING_TYPE_DISTRIB_IN:
            case FILTERING_TYPE_DISTRIB_OUT:
                /*ON DELETE */
                if (6 == i4RowStatusValue)
                {
                    if (nmhGetFsMIRipDistInOutRouteMapRowStatus
                        (i4OspfCxtId, &RouteMapName, i4FilterType,
                         &i4RowStatusValue) == SNMP_SUCCESS)
                    {
                        if (nmhTestv2FsMIRipDistInOutRouteMapRowStatus
                            (&u4ErrorCode, i4OspfCxtId, &RouteMapName,
                             i4FilterType, DESTROY) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsMIRipDistInOutRouteMapRowStatus
                                (i4OspfCxtId, &RouteMapName, i4FilterType,
                                 DESTROY) == SNMP_SUCCESS)
                            {
                                bNeedWebUpdate = TRUE;
                                break;
                            }
                        }
                    }

                    IssSendError (pHttp, (CONST INT1 *) "ERROR OCCURS");
                    break;
                }
                else
                    /*ON MODIFY */
                {

                    if (nmhGetFsMIRipDistInOutRouteMapRowStatus
                        (i4OspfCxtId, &RouteMapName, i4FilterType,
                         &i4RowStatusValue) != SNMP_SUCCESS)
                    {
                        if (nmhTestv2FsMIRipDistInOutRouteMapRowStatus
                            (&u4ErrorCode, i4OspfCxtId, &RouteMapName,
                             i4FilterType, CREATE_AND_WAIT) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsMIRipDistInOutRouteMapRowStatus
                                (i4OspfCxtId, &RouteMapName, i4FilterType,
                                 CREATE_AND_WAIT) == SNMP_SUCCESS)
                            {
                                /*filter can be applied, route map has the same name */
                                u4ErrorCode = SNMP_ERR_NO_ERROR;
                            }
                        }
                    }
                    else
                    {
                        /*new distribute-list filter */
                        u4ErrorCode = SNMP_ERR_NO_ERROR;
                    }

                    if (SNMP_ERR_NO_ERROR == u4ErrorCode)
                    {
                        if (nmhTestv2FsMIRipDistInOutRouteMapRowStatus
                            (&u4ErrorCode, i4OspfCxtId, &RouteMapName,
                             i4FilterType, ACTIVE) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsMIRipDistInOutRouteMapRowStatus
                                (i4OspfCxtId, &RouteMapName, i4FilterType,
                                 ACTIVE) == SNMP_SUCCESS)
                            {
                                bNeedWebUpdate = TRUE;
                                break;
                            }
                        }
                    }

                    IssSendError (pHttp, (CONST INT1 *) "ERROR OCCURS");
                    break;
                }
                break;            /*FILTERING_TYPE_DISTRIB_IN */
            default:
                IssSendError (pHttp, (CONST INT1 *) "ERROR OCCURS");
        }
    }

    RipUnLock ();
    WebnmUnRegisterLock (pHttp);

    if (TRUE == bNeedWebUpdate)
    {
        IssGetProcessFilterRipConfPage (pHttp);
    }

    return;
}

/*******************************************************************************
*  Function Name : IssProcessFilterRipConfPage
*  Description   : This function processes the request coming for the Rip rmap
*                  Filtering page
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssProcessFilterRipConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssGetProcessFilterRipConfPage (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssSetProcessFilterRipConfPage (pHttp);
    }
}

#endif /* RIP_WANTED */

#ifdef ISIS_WANTED
#ifdef ROUTEMAP_WANTED

/*******************************************************************************
 *  Function Name : IssGetProcessFilterConfPage
 *  Description   : This function gets and displays all the Isis route-map
 *                  filters created and enables the user to delete or modify
 *                  filetrs.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *******************************************************************************/
VOID
IssGetProcessFilterIsisConfPage (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    UINT1               au1RouteMapName[RMAP_MAX_NAME_LEN + 4] = { 0 };
    UINT1              *pu1String;
    UINT4               u4Temp = 0;
    INT4                i4FilterType;
    INT4                i4DistanceValue;
    INT4                i4RowStatusValue;
    INT4                i4PrevCxtId = 0, i4CxtId = 0;

    pu1String = au1RouteMapName;
    RouteMapName.pu1_OctetList = pu1String;
    RouteMapName.i4_Length = 0;

    pHttp->i4Write = 0;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    IssPrintAvailableIsisContextId (pHttp);

    WebnmRegisterLock (pHttp, IsisLock, IsisUnlock);
    IsisLock ();

    STRCPY (pHttp->au1Name, "CONTEXT_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4CxtId = ATOI (pHttp->au1Value);

    if (SNMP_SUCCESS == nmhGetFsIsisPreferenceValue (i4CxtId, &i4DistanceValue))
    {
        if (0 == i4DistanceValue)
            i4DistanceValue = 120;    /* update */
        STRCPY (pHttp->au1KeyString, "fsIsisPreferenceValue_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);    /*Sending till _KEY has found */
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 4, "%d", i4DistanceValue);    /*fill out pHttp->au1DataString by rmap name */
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    }

    IsisUnlock ();
    WebnmUnRegisterLock (pHttp);

    IssPrintAvailableIsisContextId (pHttp);

    WebnmRegisterLock (pHttp, IsisLock, IsisUnlock);
    IsisLock ();

    /* Display the web page till the Table Flag */
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    if (SNMP_FAILURE ==
        nmhGetFirstIndexFsIsisDistInOutRouteMapTable (&i4CxtId, &RouteMapName,
                                                      &i4FilterType))
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    else
    {
        u4Temp = (UINT4) pHttp->i4Write;
        do
        {

            pHttp->i4Write = (INT4) u4Temp;
            au1RouteMapName[RouteMapName.i4_Length] = '\0';    /*Null terminate routemap name */

            if ((SNMP_FAILURE ==
                 nmhGetFsIsisDistInOutRouteMapValue (i4CxtId, &RouteMapName,
                                                     i4FilterType,
                                                     &i4DistanceValue))
                || (SNMP_FAILURE ==
                    nmhGetFsIsisDistInOutRouteMapRowStatus (i4CxtId,
                                                            &RouteMapName,
                                                            i4FilterType,
                                                            &i4RowStatusValue)))
            {
                continue;
            }
            STRCPY (pHttp->au1KeyString, "CONTEXT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4CxtId);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "fsIsisDistInOutRouteMapName_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);    /*Sending till _KEY has found */
            SNPRINTF ((CHR1 *) pHttp->au1DataString, RMAP_MAX_NAME_LEN + 1, "%s", pu1String);    /*fill out pHttp->au1DataString by rmap name */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "fsIsisDistInOutRouteMapType_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);    /*Sending till _KEY has found */
            SNPRINTF ((CHR1 *) pHttp->au1DataString, 2, "%d", i4FilterType);    /*fill out pHttp->au1DataString by filter type */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "fsIsisDistInOutRouteMapValue_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);    /*Sending till _KEY has found */
            SNPRINTF ((CHR1 *) pHttp->au1DataString, 4, "%d", i4DistanceValue);    /*fill out pHttp->au1DataString by rmap name */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString,
                    "fsIsisDistInOutRouteMapRowStatus_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);    /*Sending till _KEY has found */
            SNPRINTF ((CHR1 *) pHttp->au1DataString, 2, "%d", i4RowStatusValue);    /*fill out pHttp->au1DataString by rmap name */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            i4PrevCxtId = i4CxtId;
        }
        while (SNMP_FAILURE !=
               nmhGetNextIndexFsIsisDistInOutRouteMapTable (i4PrevCxtId,
                                                            &i4CxtId,
                                                            &RouteMapName,
                                                            &RouteMapName,
                                                            i4FilterType,
                                                            &i4FilterType));

        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);

        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
    }

    IsisUnlock ();
    WebnmUnRegisterLock (pHttp);

    return;
}

/******************************************************************************
 *  Function Name : IssSetProcessFilterIsisConfPage
 *  Description   : This function gets the user input from the web page and
 *                  creates Isis route-map filters.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssSetProcessFilterIsisConfPage (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    UINT1               au1RouteMapName[RMAP_MAX_NAME_LEN + 4] = { 0 };
    INT4                i4FilterType = 0;
    INT4                i4DistanceValue = 0;
    INT4                i4RowStatusValue = 0;
    UINT4               u4ErrorCode = SNMP_ERR_NO_CREATION;
    BOOL1               bNeedWebUpdate = FALSE;
    INT4                i4IsisInstId;
    WebnmRegisterLock (pHttp, IsisLock, IsisUnlock);
    IsisLock ();

    STRCPY (pHttp->au1Name, "CONTEXT_ID");

    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    i4IsisInstId = ATOI (pHttp->au1Value);

    /*Getting data from post html */
    STRCPY (pHttp->au1Name, "fsIsisPreferenceValue");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4DistanceValue = (INT4) ATOI (pHttp->au1Value);

        if (nmhTestv2FsIsisPreferenceValue (&u4ErrorCode, i4IsisInstId,
                                            i4DistanceValue) == SNMP_SUCCESS)
        {
            if (nmhSetFsIsisPreferenceValue (i4IsisInstId, i4DistanceValue) ==
                SNMP_SUCCESS)
            {
                bNeedWebUpdate = TRUE;
            }
        }
        else
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Error: Incorrect distance value");
        }
    }
    else
    {
        RouteMapName.pu1_OctetList = au1RouteMapName;
        RouteMapName.i4_Length = 0;

        STRCPY (pHttp->au1Name, "fsIsisDistInOutRouteMapName");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            RouteMapName.i4_Length = (INT4) (STRLEN (pHttp->au1Value) + 1);
            if (RouteMapName.i4_Length > (RMAP_MAX_NAME_LEN + 4))
            {
                RouteMapName.i4_Length = RMAP_MAX_NAME_LEN + 4;
            }
            MEMCPY (RouteMapName.pu1_OctetList, pHttp->au1Value,
                    RouteMapName.i4_Length);
            RouteMapName.pu1_OctetList[RouteMapName.i4_Length - 1] = '\0';
        }

        STRCPY (pHttp->au1Name, "fsIsisDistInOutRouteMapType");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4FilterType = (INT4) ATOI (pHttp->au1Value);
        }

        STRCPY (pHttp->au1Name, "fsIsisDistInOutRouteMapValue");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4DistanceValue = (INT4) ATOI (pHttp->au1Value);
        }

        STRCPY (pHttp->au1Name, "fsIsisDistInOutRouteMapRowStatus");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4RowStatusValue = (INT4) ATOI (pHttp->au1Value);
        }

        switch (i4FilterType)
        {
             /*FILTERING*/ case FILTERING_TYPE_DISTANCE:
                /*ON DELETE */
                if (6 == i4RowStatusValue)
                {
                    if (nmhGetFsIsisDistInOutRouteMapRowStatus
                        (i4IsisInstId, &RouteMapName, FILTERING_TYPE_DISTANCE,
                         &i4RowStatusValue) == SNMP_SUCCESS)
                    {
                        if (nmhTestv2FsIsisDistInOutRouteMapRowStatus
                            (&u4ErrorCode, i4IsisInstId, &RouteMapName,
                             FILTERING_TYPE_DISTANCE, DESTROY) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsIsisDistInOutRouteMapRowStatus
                                (i4IsisInstId, &RouteMapName,
                                 FILTERING_TYPE_DISTANCE,
                                 DESTROY) == SNMP_SUCCESS)
                            {
                                bNeedWebUpdate = TRUE;
                                break;
                            }
                        }
                    }

                    IssSendError (pHttp, (CONST INT1 *) "Error: ERROR OCCUR");
                    break;
                }
                else
                    /*ON MODIFY */
                {
                    if (nmhGetFsIsisDistInOutRouteMapRowStatus
                        (i4IsisInstId, &RouteMapName, FILTERING_TYPE_DISTANCE,
                         &i4RowStatusValue) != SNMP_SUCCESS)
                    {
                        if (nmhTestv2FsIsisDistInOutRouteMapRowStatus
                            (&u4ErrorCode, i4IsisInstId, &RouteMapName,
                             FILTERING_TYPE_DISTANCE,
                             CREATE_AND_WAIT) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsIsisDistInOutRouteMapRowStatus
                                (i4IsisInstId, &RouteMapName,
                                 FILTERING_TYPE_DISTANCE,
                                 CREATE_AND_WAIT) == SNMP_SUCCESS)
                            {
                                /*filter can be applied, route map has the same name */
                                u4ErrorCode = SNMP_ERR_NO_ERROR;
                            }
                        }
                        else
                        {
                            /*filter with cannot be applied, route map has another name */
                            IssSendError (pHttp,
                                          (CONST INT1 *)
                                          "Error: Distance filter already applied");
                            break;
                        }
                    }
                    else
                    {
                        /*new distance filter */
                        u4ErrorCode = SNMP_ERR_NO_ERROR;
                    }

                    if (SNMP_ERR_NO_ERROR == u4ErrorCode)
                    {
                        if (nmhTestv2FsIsisDistInOutRouteMapValue
                            (&u4ErrorCode, i4IsisInstId, &RouteMapName,
                             FILTERING_TYPE_DISTANCE,
                             i4DistanceValue) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsIsisDistInOutRouteMapValue
                                (i4IsisInstId, &RouteMapName,
                                 FILTERING_TYPE_DISTANCE,
                                 i4DistanceValue) == SNMP_SUCCESS)
                            {
                                if (nmhTestv2FsIsisDistInOutRouteMapRowStatus
                                    (&u4ErrorCode, i4IsisInstId, &RouteMapName,
                                     FILTERING_TYPE_DISTANCE,
                                     ACTIVE) == SNMP_SUCCESS)
                                {
                                    if (nmhSetFsIsisDistInOutRouteMapRowStatus
                                        (i4IsisInstId, &RouteMapName,
                                         FILTERING_TYPE_DISTANCE,
                                         ACTIVE) == SNMP_SUCCESS)
                                    {
                                        bNeedWebUpdate = TRUE;
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    IssSendError (pHttp, (CONST INT1 *) "ERROR OCCURS");
                    break;
                }
                break;            /*FILTERING_TYPE_DISTANCE */
            case FILTERING_TYPE_DISTRIB_IN:
            case FILTERING_TYPE_DISTRIB_OUT:
                /*ON DELETE */
                if (6 == i4RowStatusValue)
                {
                    if (nmhGetFsIsisDistInOutRouteMapRowStatus
                        (i4IsisInstId, &RouteMapName, i4FilterType,
                         &i4RowStatusValue) == SNMP_SUCCESS)
                    {
                        if (nmhTestv2FsIsisDistInOutRouteMapRowStatus
                            (&u4ErrorCode, i4IsisInstId, &RouteMapName,
                             i4FilterType, DESTROY) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsIsisDistInOutRouteMapRowStatus
                                (i4IsisInstId, &RouteMapName, i4FilterType,
                                 DESTROY) == SNMP_SUCCESS)
                            {
                                bNeedWebUpdate = TRUE;
                                break;
                            }
                        }
                    }

                    IssSendError (pHttp, (CONST INT1 *) "ERROR OCCURS");
                    break;
                }
                else
                    /*ON MODIFY */
                {

                    if (nmhGetFsIsisDistInOutRouteMapRowStatus
                        (i4IsisInstId, &RouteMapName, i4FilterType,
                         &i4RowStatusValue) != SNMP_SUCCESS)
                    {
                        if (nmhTestv2FsIsisDistInOutRouteMapRowStatus
                            (&u4ErrorCode, i4IsisInstId, &RouteMapName,
                             i4FilterType, CREATE_AND_WAIT) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsIsisDistInOutRouteMapRowStatus
                                (i4IsisInstId, &RouteMapName, i4FilterType,
                                 CREATE_AND_WAIT) == SNMP_SUCCESS)
                            {
                                /*filter can be applied, route map has the same name */
                                u4ErrorCode = SNMP_ERR_NO_ERROR;
                            }
                        }
                    }
                    else
                    {
                        /*new distribute-list filter */
                        u4ErrorCode = SNMP_ERR_NO_ERROR;
                    }

                    if (SNMP_ERR_NO_ERROR == u4ErrorCode)
                    {
                        if (nmhTestv2FsIsisDistInOutRouteMapRowStatus
                            (&u4ErrorCode, i4IsisInstId, &RouteMapName,
                             i4FilterType, ACTIVE) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsIsisDistInOutRouteMapRowStatus
                                (i4IsisInstId, &RouteMapName, i4FilterType,
                                 ACTIVE) == SNMP_SUCCESS)
                            {
                                bNeedWebUpdate = TRUE;
                                break;
                            }
                        }
                    }

                    IssSendError (pHttp, (CONST INT1 *) "ERROR OCCURS");
                    break;
                }
                break;            /*FILTERING_TYPE_DISTRIB_IN */
            default:
                IssSendError (pHttp, (CONST INT1 *) "ERROR OCCURS");
        }
    }

    IsisUnlock ();
    WebnmUnRegisterLock (pHttp);

    if (TRUE == bNeedWebUpdate)
    {
        IssGetProcessFilterIsisConfPage (pHttp);
    }

    return;
}

/*******************************************************************************
 *  Function Name : IssProcessFilterIsisConfPage
 *  Description   : This function processes the request coming for the Isis rmap
 *                  Filtering page
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ********************************************************************************/
VOID
IssProcessFilterIsisConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssGetProcessFilterIsisConfPage (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssSetProcessFilterIsisConfPage (pHttp);
    }
}

#endif /* ROUTEMAP_WANTED */
#endif /* ISIS_WANTED */

/*===========================================================================*/

#ifdef BGP_WANTED

/******************************************************************************
*  Function Name : IssSetProcessFilterBgpConfPage
*  Description   : This function gets the user input from the web page and
*                  creates BGP route-map filters.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
******************************************************************************/
VOID
IssSetProcessFilterBgpConfPage (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    UINT1               au1RouteMapName[RMAP_MAX_NAME_LEN + 4] = { 0 };
    INT4                i4FilterType = 0;
    INT4                i4DistanceValue = 0;
    INT4                i4RowStatusValue = 0;
    UINT4               u4ErrorCode = SNMP_ERR_NO_CREATION;
    BOOL1               bNeedWebUpdate = FALSE;

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    if (BgpSetContext (BGP4_DFLT_VRFID) == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    /*Getting data from post html */
    STRCPY (pHttp->au1Name, "fsBgp4PreferenceValue");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        i4DistanceValue = (INT4) ATOI (pHttp->au1Value);

        if (nmhTestv2FsBgp4PreferenceValue (&u4ErrorCode,
                                            i4DistanceValue) == SNMP_SUCCESS)
        {
            if (nmhSetFsBgp4PreferenceValue (i4DistanceValue) == SNMP_SUCCESS)
            {
                bNeedWebUpdate = TRUE;
            }
        }
        else
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Error: Incorrect distance value");
        }
    }
    else
    {
        RouteMapName.pu1_OctetList = au1RouteMapName;
        RouteMapName.i4_Length = 0;

        STRCPY (pHttp->au1Name, "fsBgp4DistInOutRouteMapName");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            RouteMapName.i4_Length = (INT4) STRLEN (pHttp->au1Value);
            if (RouteMapName.i4_Length > (RMAP_MAX_NAME_LEN + 3))
            {
                RouteMapName.i4_Length = RMAP_MAX_NAME_LEN + 3;
            }
            MEMCPY (RouteMapName.pu1_OctetList, pHttp->au1Value,
                    RouteMapName.i4_Length);
            RouteMapName.pu1_OctetList[RouteMapName.i4_Length] = '\0';
        }

        STRCPY (pHttp->au1Name, "fsBgp4DistInOutRouteMapType");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4FilterType = (INT4) ATOI (pHttp->au1Value);
        }

        STRCPY (pHttp->au1Name, "fsBgp4DistInOutRouteMapValue");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4DistanceValue = (INT4) ATOI (pHttp->au1Value);
        }

        STRCPY (pHttp->au1Name, "fsBgp4DistInOutRouteMapRowStatus");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4RowStatusValue = (INT4) ATOI (pHttp->au1Value);
        }

        switch (i4FilterType)
        {
             /*FILTERING*/ case FILTERING_TYPE_DISTANCE:
                /*ON DELETE */
                if (6 == i4RowStatusValue)
                {

                    if (nmhGetFsBgp4DistInOutRouteMapRowStatus (&RouteMapName,
                                                                FILTERING_TYPE_DISTANCE,
                                                                &i4RowStatusValue)
                        == SNMP_SUCCESS)
                    {
                        if (nmhTestv2FsBgp4DistInOutRouteMapRowStatus
                            (&u4ErrorCode, &RouteMapName,
                             FILTERING_TYPE_DISTANCE, DESTROY) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsBgp4DistInOutRouteMapRowStatus
                                (&RouteMapName, FILTERING_TYPE_DISTANCE,
                                 DESTROY) == SNMP_SUCCESS)
                            {
                                bNeedWebUpdate = TRUE;
                                break;
                            }
                        }
                    }

                    IssSendError (pHttp, (CONST INT1 *) "Error: ERROR OCCUR");
                    break;
                }
                else
                    /*ON MODIFY */
                {
                    if (nmhGetFsBgp4DistInOutRouteMapRowStatus (&RouteMapName,
                                                                FILTERING_TYPE_DISTANCE,
                                                                &i4RowStatusValue)
                        != SNMP_SUCCESS)
                    {
                        if (nmhTestv2FsBgp4DistInOutRouteMapRowStatus
                            (&u4ErrorCode, &RouteMapName,
                             FILTERING_TYPE_DISTANCE,
                             CREATE_AND_WAIT) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsBgp4DistInOutRouteMapRowStatus
                                (&RouteMapName, FILTERING_TYPE_DISTANCE,
                                 CREATE_AND_WAIT) == SNMP_SUCCESS)
                            {
                                /*filter can be applied, route map has the same name */
                                u4ErrorCode = SNMP_ERR_NO_ERROR;
                            }
                        }
                        else
                        {
                            /*filter with cannot be applied, route map has another name */
                            IssSendError (pHttp,
                                          (CONST INT1 *)
                                          "Error: Distance filter already applied");
                            break;
                        }
                    }
                    else
                    {
                        /*new distance filter */
                        u4ErrorCode = SNMP_ERR_NO_ERROR;
                    }

                    if (SNMP_ERR_NO_ERROR == u4ErrorCode)
                    {
                        if (nmhTestv2FsBgp4DistInOutRouteMapValue (&u4ErrorCode,
                                                                   &RouteMapName,
                                                                   FILTERING_TYPE_DISTANCE,
                                                                   i4DistanceValue)
                            == SNMP_SUCCESS)
                        {
                            if (nmhSetFsBgp4DistInOutRouteMapValue
                                (&RouteMapName, FILTERING_TYPE_DISTANCE,
                                 i4DistanceValue) == SNMP_SUCCESS)
                            {
                                if (nmhTestv2FsBgp4DistInOutRouteMapRowStatus
                                    (&u4ErrorCode, &RouteMapName,
                                     FILTERING_TYPE_DISTANCE,
                                     ACTIVE) == SNMP_SUCCESS)
                                {
                                    if (nmhSetFsBgp4DistInOutRouteMapRowStatus
                                        (&RouteMapName, FILTERING_TYPE_DISTANCE,
                                         ACTIVE) == SNMP_SUCCESS)
                                    {
                                        bNeedWebUpdate = TRUE;
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    IssSendError (pHttp, (CONST INT1 *) "ERROR OCCURS");
                    break;
                }
                break;            /*FILTERING_TYPE_DISTANCE */
            case FILTERING_TYPE_DISTRIB_IN:
            case FILTERING_TYPE_DISTRIB_OUT:
                /*ON DELETE */
                if (6 == i4RowStatusValue)
                {
                    if (nmhGetFsBgp4DistInOutRouteMapRowStatus (&RouteMapName,
                                                                i4FilterType,
                                                                &i4RowStatusValue)
                        == SNMP_SUCCESS)
                    {
                        if (nmhTestv2FsBgp4DistInOutRouteMapRowStatus
                            (&u4ErrorCode, &RouteMapName, i4FilterType,
                             DESTROY) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsBgp4DistInOutRouteMapRowStatus
                                (&RouteMapName, i4FilterType,
                                 DESTROY) == SNMP_SUCCESS)
                            {
                                bNeedWebUpdate = TRUE;
                                break;
                            }
                        }
                    }

                    IssSendError (pHttp, (CONST INT1 *) "ERROR OCCURS");
                    break;
                }
                else
                    /*ON MODIFY */
                {

                    if (nmhGetFsBgp4DistInOutRouteMapRowStatus (&RouteMapName,
                                                                i4FilterType,
                                                                &i4RowStatusValue)
                        != SNMP_SUCCESS)
                    {
                        if (nmhTestv2FsBgp4DistInOutRouteMapRowStatus
                            (&u4ErrorCode, &RouteMapName, i4FilterType,
                             CREATE_AND_WAIT) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsBgp4DistInOutRouteMapRowStatus
                                (&RouteMapName, i4FilterType,
                                 CREATE_AND_WAIT) == SNMP_SUCCESS)
                            {
                                /*filter can be applied, route map has the same name */
                                u4ErrorCode = SNMP_ERR_NO_ERROR;
                            }
                        }
                    }
                    else
                    {
                        /*new distribute-list filter */
                        u4ErrorCode = SNMP_ERR_NO_ERROR;
                    }

                    if (SNMP_ERR_NO_ERROR == u4ErrorCode)
                    {
                        if (nmhTestv2FsBgp4DistInOutRouteMapRowStatus
                            (&u4ErrorCode, &RouteMapName, i4FilterType,
                             ACTIVE) == SNMP_SUCCESS)
                        {
                            if (nmhSetFsBgp4DistInOutRouteMapRowStatus
                                (&RouteMapName, i4FilterType,
                                 ACTIVE) == SNMP_SUCCESS)
                            {
                                bNeedWebUpdate = TRUE;
                                break;
                            }
                        }
                    }

                    IssSendError (pHttp, (CONST INT1 *) "ERROR OCCURS");
                    break;
                }
                break;            /*FILTERING_TYPE_DISTRIB_IN */
            default:
                IssSendError (pHttp, (CONST INT1 *) "ERROR OCCURS");
        }
    }

    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);

    if (TRUE == bNeedWebUpdate)
    {
        IssGetProcessFilterBgpConfPage (pHttp);
    }

    return;
}

/*******************************************************************************
*  Function Name : IssGetProcessFilterBGPConfPage
*  Description   : This function gets and displays all the BGP route-map
*                  filters created and enables the user to delete or modify
*                  filetrs.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssGetProcessFilterBgpConfPage (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    UINT1               au1RouteMapName[RMAP_MAX_NAME_LEN + 4] = { 0 };
    UINT1              *pu1String = NULL;
    UINT4               u4Temp = 0;
    INT4                i4FilterType = 0;
    INT4                i4DistanceValue = 0;
    INT4                i4RowStatusValue = 0;

    pu1String = au1RouteMapName;
    RouteMapName.pu1_OctetList = pu1String;
    RouteMapName.i4_Length = 0;

    pHttp->i4Write = 0;

    WebnmRegisterLock (pHttp, BgpLock, BgpUnLock);
    BgpLock ();

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    if (BgpSetContext (BGP4_DFLT_VRFID) == SNMP_FAILURE)
    {
        BgpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    if (SNMP_SUCCESS == nmhGetFsBgp4PreferenceValue (&i4DistanceValue))
    {
        if (0 == i4DistanceValue)
            i4DistanceValue = 122;
        STRCPY (pHttp->au1KeyString, "fsBgp4PreferenceValue_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);    /*Sending till _KEY has found */
        SNPRINTF ((CHR1 *) pHttp->au1DataString, 4, "%d", i4DistanceValue);    /*fill out pHttp->au1DataString by rmap name */
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    }

    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);

    /* Display the web page till the Table Flag */
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    if (SNMP_FAILURE ==
        nmhGetFirstIndexFsBgp4DistInOutRouteMapTable (&RouteMapName,
                                                      &i4FilterType))
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    else
    {
        u4Temp = (UINT4) pHttp->i4Write;
        do
        {

            pHttp->i4Write = (INT4) u4Temp;
            au1RouteMapName[RouteMapName.i4_Length] = '\0';    /*Null terminate routemap name */

            if ((SNMP_FAILURE ==
                 nmhGetFsBgp4DistInOutRouteMapValue (&RouteMapName,
                                                     i4FilterType,
                                                     &i4DistanceValue))
                || (SNMP_FAILURE ==
                    nmhGetFsBgp4DistInOutRouteMapRowStatus (&RouteMapName,
                                                            i4FilterType,
                                                            &i4RowStatusValue)))
            {
                continue;
            }

            STRCPY (pHttp->au1KeyString, "fsBgp4DistInOutRouteMapName_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);    /*Sending till _KEY has found */
            SNPRINTF ((CHR1 *) pHttp->au1DataString, RMAP_MAX_NAME_LEN + 1, "%s", pu1String);    /*fill out pHttp->au1DataString by rmap name */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "fsBgp4DistInOutRouteMapType_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);    /*Sending till _KEY has found */
            SNPRINTF ((CHR1 *) pHttp->au1DataString, 2, "%d", i4FilterType);    /*fill out pHttp->au1DataString by filter type */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "fsBgp4DistInOutRouteMapValue_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);    /*Sending till _KEY has found */
            SNPRINTF ((CHR1 *) pHttp->au1DataString, 4, "%d", i4DistanceValue);    /*fill out pHttp->au1DataString by rmap name */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString,
                    "fsBgp4DistInOutRouteMapRowStatus_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);    /*Sending till _KEY has found */
            SNPRINTF ((CHR1 *) pHttp->au1DataString, 2, "%d", i4RowStatusValue);    /*fill out pHttp->au1DataString by rmap name */
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

        }
        while (SNMP_FAILURE !=
               nmhGetNextIndexFsBgp4DistInOutRouteMapTable (&RouteMapName,
                                                            &RouteMapName,
                                                            i4FilterType,
                                                            &i4FilterType));

        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);

        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
    }

    BgpResetContext ();
    BgpUnLock ();
    WebnmUnRegisterLock (pHttp);

    return;
}

/*******************************************************************************
*  Function Name : IssProcessFilterBGPConfPage
*  Description   : This function processes the request coming for the BGP rmap
*                  Filtering page
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************************/
VOID
IssProcessFilterBgpConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssGetProcessFilterBgpConfPage (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssSetProcessFilterBgpConfPage (pHttp);
    }
}

#endif /* BGP_WANTED */
