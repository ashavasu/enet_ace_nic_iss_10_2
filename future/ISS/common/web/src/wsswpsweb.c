/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: wsswpsweb.c,v 1.1 2015/12/14 11:03:59 siva Exp $
 *
 * Description: Routines for WSS WPS WEB Module
 *******************************************************************/
#ifdef WPS_WANTED
#ifndef _WSS_WPS_WEB_C_
#define _WSS_WPS_WEB_C_
#include "webiss.h"
#include "isshttp.h"
#include "webinc.h"
#include "issmacro.h"
#include "snmputil.h"
#include "utilcli.h"

#include "capwapcliinc.h"
#include "capwapcli.h"
#include "wsscfgcli.h"
#include "wsscfgwlanproto.h"
#include "wsscfgtdfs.h"
#include "wsscfgprotg.h"

#include "capwapclidefg.h"
#include "wsscfglwg.h"
#include "wssifcapdb.h"
#include "wlchdlr.h"


#include "wps.h"

INT4                gi4WlanWpsProfileId;
INT4                gi4WlanApProfileId;

#define wss_web_trace_enable 1
#define WSS_WEB_TRC   if(wss_web_trace_enable)  printf

/*********************************************************************
 * *  Function Name : IssProcessWpsConfigPage
 * *  Description   : This function processes the request coming for the
 * *                  Wps Config Table Page
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssProcessWpsConfigPage (tHttp * pHttp)
{
#ifdef WPS_WANTED
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessWpsConfigPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessWpsConfigPageSet (pHttp);
    }
#else
    UNUSED_PARAM (pHttp);
#endif
}

#ifdef WPS_WANTED
/*********************************************************************
 * *  Function Name : IssProcessWpsConfigPageGet
 * *  Description   : This function processes the get request coming for the
 * *                  Wps Config Table  page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssProcessWpsConfigPageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    UINT4               u4currentWlanId = 0;
    UINT4               u4nextWlanId = 0;
    UINT4               u4ProfileId = 0;
    INT4                i4OutCome = 0;
    INT4                i4WpsEnabled = 0;
    INT4                i4WpsAdditionalIE = 0;
    INT4                i4WpsPbcStatus = 0;
    UINT4		u4WtpProfileId = 0;
    UINT4		currentProfileId = 0;
    tSNMP_OCTET_STRING_TYPE WtpProfileName;
    UINT1               au1WtpName[256];  
    

    u4currentWlanId = 0;
    u4nextWlanId = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;
    WebnmRegisterLock (pHttp, PnacLock, PnacUnLock);
    PNAC_LOCK ();
    i4OutCome = nmhGetFirstIndexCapwapDot11WlanTable (&u4nextWlanId);

    if (i4OutCome == SNMP_FAILURE)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }
    do
    {
        pHttp->i4Write = (INT4) u4Temp;
        u4currentWlanId = u4nextWlanId;
        if (u4currentWlanId != 0)
        {
            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4currentWlanId, (INT4 *) &u4ProfileId) == SNMP_SUCCESS)
            {
                if (u4ProfileId != 0)
                {
                    i4OutCome = nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpProfileId);
                    if (i4OutCome == SNMP_FAILURE)
                    {
                             PNAC_UNLOCK ();
                             WebnmUnRegisterLock (pHttp);
                             IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
                             WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                                             (pHttp->i4HtmlSize - pHttp->i4Write));
                             return;
                     }
                    do
                    {
			pHttp->i4Write = (INT4) u4Temp;
                        currentProfileId = u4WtpProfileId;
   			MEMSET (au1WtpName, 0, 256);
                        MEMSET (&WtpProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
                        WtpProfileName.pu1_OctetList = au1WtpName;
                        if (nmhGetCapwapBaseWtpProfileName
                            (u4WtpProfileId, &WtpProfileName) != SNMP_SUCCESS)
		        {
                             return ;
                        }

                    nmhGetFsWPSSetStatus((INT4) u4ProfileId,u4WtpProfileId, &i4WpsEnabled);

                    nmhGetFsWPSAdditionalIE((INT4) u4ProfileId,u4WtpProfileId,
                                                             &i4WpsAdditionalIE);
                    nmhGetFsWPSPushButtonStatus((INT4) u4ProfileId,u4WtpProfileId, &i4WpsPbcStatus);

                    STRCPY (pHttp->au1KeyString, "wlan_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                             u4currentWlanId);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "ap_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                             WtpProfileName.pu1_OctetList);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);


                    STRCPY (pHttp->au1KeyString, "Wpsstatus_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    if (i4WpsEnabled == WPS_ENABLED)
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Enabled");
                    }
                    else
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Disabled");
                    }
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "WpsadditionalIE_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    if (i4WpsAdditionalIE == WPS_PBC)
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "PBC");
                    }
                    else
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "PIN");
                    }

                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "Wpspbcstatus_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    if (i4WpsPbcStatus == WPS_PBC_SUCCESS)
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Success");
                    }
                    else  if (i4WpsPbcStatus == WPS_PBC_OVERLAP)
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Overlap");
                    }
                    else if (i4WpsPbcStatus == WPS_PBC_INPROGRESS)
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "In-progress");
                    }
                    else if (i4WpsPbcStatus == WPS_PBC_INACTIVE)
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Inactive");
                    }

                    else if(i4WpsPbcStatus == WPS_PBC_ERROR)
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Error");
                    }
                     WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                }
                while (nmhGetNextIndexCapwapBaseWtpProfileTable
                        (currentProfileId, &u4WtpProfileId) == SNMP_SUCCESS);
            }
         }
        }
    }while (nmhGetNextIndexCapwapDot11WlanTable
           (u4currentWlanId, &u4nextWlanId));
    PNAC_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}



/*********************************************************************
 * *  Function Name : IssProcessWpsConfigPageSet
 * *  Description   : This function processes the set request coming for the
 * *                  Wps Config Table page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/
VOID
IssProcessWpsConfigPageSet (tHttp * pHttp)
{
    INT4                i4WlanProfileId = 0;
    UINT4                 u4WlanApProfileId = 0;

    STRCPY (pHttp->au1Name, "WLAN_PROFILE_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4WlanProfileId = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "WLAN_AP_PROFILE_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (CapwapGetWtpProfileIdFromProfileName
         ( pHttp->au1Value, &u4WlanApProfileId) != OSIX_SUCCESS)
               return ;

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    WSS_WEB_TRC ("WLAN Profile Id:%d\n", i4WlanProfileId);

    if (STRCMP (pHttp->au1Value, "Edit") == 0)
    {
        WSS_WEB_TRC ("Edit.....just store the Profile Id:%d\n",
                     i4WlanProfileId);
        gi4WlanWpsProfileId = (INT4) i4WlanProfileId;
        gi4WlanApProfileId = (INT4)u4WlanApProfileId;
    }
    else
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Operation\n");
    }
    
    return;
}
#endif
/*********************************************************************
*  Function Name : IssProcessWpsConfigEditPage
*  Description   : This function processes the request coming for the
*                  Wps Config Table Edit Page
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessWpsConfigEditPage (tHttp * pHttp)
{
#ifdef WPS_WANTED
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessWpsConfigEditPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessWpsConfigEditPageSet (pHttp);
    }
#else
    UNUSED_PARAM (pHttp);
#endif
}
#ifdef WPS_WANTED
/*********************************************************************
*  Function Name : IssProcessWpsConfigEditPageGet
*  Description   : This function processes the get request coming for the
*                  Wps Config Table Edit page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessWpsConfigEditPageGet (tHttp * pHttp)
{
    UINT4               u4ProfileId = 0;
    INT4                i4WpsEnabled = 0;
    INT4                i4WpsAdditionalIE = 0;
    INT4                i4WpsPbcStatus = 0;
    UINT4	        u4WtpProfileId =0;
    tSNMP_OCTET_STRING_TYPE WtpProfileName;
    UINT1               au1WtpName[256];
    INT4                i4OutCome = 0;
    UINT4               currentProfileId = 0;

    WebnmRegisterLock (pHttp, PnacLock, PnacUnLock);
    PNAC_LOCK ();
    printf("i4ApProfileId =%d\n",gi4WlanApProfileId);
       if (nmhGetCapwapDot11WlanProfileIfIndex
        ((UINT4) gi4WlanWpsProfileId, (INT4 *) &u4ProfileId) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));

    }
    else
    {
        if (u4ProfileId != 0)
        {

            nmhGetFsWPSSetStatus((INT4) u4ProfileId,u4WtpProfileId, &i4WpsEnabled);
            nmhGetFsWPSAdditionalIE((INT4) u4ProfileId,u4WtpProfileId, &i4WpsAdditionalIE);
            nmhGetFsWPSPushButtonStatus((INT4) u4ProfileId, u4WtpProfileId, &i4WpsPbcStatus);

            STRCPY (pHttp->au1KeyString, "wlan_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", gi4WlanWpsProfileId);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            i4OutCome = nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpProfileId);
             if (i4OutCome == SNMP_FAILURE)
             {             
                 PNAC_UNLOCK ();
                 WebnmUnRegisterLock (pHttp);
                 IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
                 WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                                (pHttp->i4HtmlSize - pHttp->i4Write));
                 return;
              }
            do 
            {
               currentProfileId = u4WtpProfileId;
               if(u4WtpProfileId == (UINT4)gi4WlanApProfileId)
               {    
  	           MEMSET (au1WtpName, 0, 256);
	           MEMSET (&WtpProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
            	   WtpProfileName.pu1_OctetList = au1WtpName;
                   if (nmhGetCapwapBaseWtpProfileName
                      (u4WtpProfileId, &WtpProfileName) != SNMP_SUCCESS)
                   {
                          return ;
                   }
                   STRCPY (pHttp->au1KeyString, "ap_KEY");
                   WebnmSendString (pHttp, pHttp->au1KeyString);
                   SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", WtpProfileName.pu1_OctetList);
                   WebnmSockWrite (pHttp, pHttp->au1DataString,
                           (INT4) HTTP_STRLEN (pHttp->au1DataString));
               }
             else
             {
                 nmhGetNextIndexCapwapBaseWtpProfileTable(currentProfileId, &u4WtpProfileId);
             }
           }while(currentProfileId != (UINT4)gi4WlanApProfileId);

            STRCPY (pHttp->au1KeyString, "Wpsstatus_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4WpsEnabled);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "WpsadditionalIE_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                     i4WpsAdditionalIE);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "Wpspbcstatus_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            if (i4WpsPbcStatus == WPS_PBC_SUCCESS)
            {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Success");
            }
            else  if (i4WpsPbcStatus == WPS_PBC_OVERLAP)
            {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Overlap");
            }
            else if (i4WpsPbcStatus == WPS_PBC_INPROGRESS)
            {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "In-progress");
            }
            else if (i4WpsPbcStatus == WPS_PBC_INACTIVE)
            {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Inactive");
            }

            else if(i4WpsPbcStatus == WPS_PBC_ERROR)
            {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Error");
            }
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            PNAC_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
           WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));

        }
    }
}
/*********************************************************************
*  Function Name : IssProcessWpsConfigEditPageSet
*  Description   : This function processes the set request coming for the
*                  Wps Config Table Edit page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessWpsConfigEditPageSet (tHttp * pHttp)
{
   UINT4               u4WlanId = 0;
   INT1                i1WpsStatus = 0;
   INT1                i1WpsAdditionalIE =0;

   UINT4               u4ErrorCode = 0;
   UINT4               u4ProfileId = 0;
   UINT4	       u4WtpProfileId =0;

   STRCPY (pHttp->au1Name, "SSID");
   HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
   u4WlanId = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

   STRCPY (pHttp->au1Name, "wps");
   HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
   i1WpsStatus = (INT1) ATOI ((INT1 *) pHttp->au1Value);

   STRCPY (pHttp->au1Name, "wpsAdditionalIE");
   HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
   i1WpsAdditionalIE = (INT1) ATOI ((INT1 *) pHttp->au1Value);

   STRCPY (pHttp->au1Name, "wpsPbcStatus");
   HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

  WebnmRegisterLock (pHttp, PnacLock, PnacUnLock);
    PNAC_LOCK ();

    if (nmhGetCapwapDot11WlanProfileIfIndex
        (u4WlanId, (INT4 *) &u4ProfileId) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in getting WlanIfIndex\n");
        return;
    }

    if (nmhTestv2FsWPSSetStatus(&u4ErrorCode, (INT4) u4ProfileId,u4WtpProfileId, 
                                   i1WpsStatus) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration - WPS\n");
        return;
    }
    if (nmhSetFsWPSSetStatus((INT4) u4ProfileId,u4WtpProfileId,
                                i1WpsStatus) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting  WPS\n");
        return;
    }

   if (nmhTestv2FsWPSAdditionalIE(&u4ErrorCode, (INT4) u4ProfileId,u4WtpProfileId,
                                   i1WpsAdditionalIE) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration - WPS Additional IEs\n");
        return;
    }
    if (nmhSetFsWPSAdditionalIE((INT4) u4ProfileId,u4WtpProfileId,
                                i1WpsAdditionalIE) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting  WPS\n");
        return;
    }

    PNAC_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    IssProcessWpsConfigEditPageGet (pHttp);

}
#endif
/*********************************************************************
 *  Function Name : IssProcessWpsAuthStaPage
 *  Description   : This function processes the request coming for the
 *                  Wps Auth STA Table Page
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 * *********************************************************************/

VOID
IssProcessWpsAuthStaPage(tHttp * pHttp)
{
#ifdef WPS_WANTED
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessWpsAuthStaPageGet(pHttp);
    }
#else
    UNUSED_PARAM (pHttp);
#endif
}
#ifdef WPS_WANTED
/*********************************************************************
 * *  Function Name : IssProcessWpsAuthStaPageGet
 * *  Description   : This function processes the get request coming for the
 * *                  Wps Auth STA page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID IssProcessWpsAuthStaPageGet(tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    UINT4               u4currentWlanId = 0;
    INT4               i4ProfileId =0;
    UINT4               u4nextWlanId =0;
    INT4                i4OutCome = 0;
    INT4               i4NextWlanIndex = 0;
    UINT4               u4CurrentWPSStatsIndex = 0;
    UINT4               u4NextWPSStatsIndex = 0;

   tWpsStat *pTmpWpsStat= NULL;
    tMacAddr            WPSStatsSTAAddress = "";
   UINT1               au1MacAddr[WPS_CLI_MAC_ADDR_LEN] = "";
   INT4               wps_status;
   INT4               wps_pbc_status;

    u4currentWlanId = 0;
    u4nextWlanId = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;
    WebnmRegisterLock (pHttp, PnacLock, PnacUnLock);
    PNAC_LOCK ();
    i4OutCome = nmhGetFirstIndexCapwapDot11WlanTable (&u4nextWlanId);

    if (i4OutCome == SNMP_FAILURE)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    do
    {
        pHttp->i4Write = (INT4) u4Temp;
        u4currentWlanId = u4nextWlanId;
        if (u4currentWlanId != 0)
        {
            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4currentWlanId, (INT4 *) &i4ProfileId) == SNMP_SUCCESS)
            {
                if (i4ProfileId != 0)
                {
                    if(WpsGetNextIndexFsWPSAuthSTAInfoTable(i4ProfileId,
                    &i4NextWlanIndex,u4CurrentWPSStatsIndex,&u4NextWPSStatsIndex) == SNMP_SUCCESS)
                    {

                      WpsUtilGetSuppStatsInfo((UINT2) i4NextWlanIndex,u4CurrentWPSStatsIndex,
                             &pTmpWpsStat);
                       if(pTmpWpsStat == NULL)
                                return ;

                        MEMSET (au1MacAddr, '\0', WPS_CLI_MAC_ADDR_LEN);
                        MEMCPY (WPSStatsSTAAddress,pTmpWpsStat->au1SuppMacAddr,WPS_ETH_ALEN);
                        CliMacToStr (WPSStatsSTAAddress, au1MacAddr);

                        wps_status = pTmpWpsStat->e1Status;
                        wps_pbc_status = pTmpWpsStat->e1PbcStatus;

                        STRCPY (pHttp->au1KeyString, "wlan_KEY");
                        WebnmSendString (pHttp, pHttp->au1KeyString);
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",u4currentWlanId);
                        WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                        WebnmSendString (pHttp, pHttp->au1KeyString);
                        STRCPY (pHttp->au1KeyString, "stat_KEY");
                        WebnmSendString (pHttp, pHttp->au1KeyString);
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",u4CurrentWPSStatsIndex);
                        WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                        WebnmSendString (pHttp, pHttp->au1KeyString);

                        STRCPY (pHttp->au1KeyString, "wpsmac_KEY");
                        WebnmSendString (pHttp, pHttp->au1KeyString);
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                               au1MacAddr);
                        WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                       WebnmSendString (pHttp, pHttp->au1KeyString);

                        STRCPY (pHttp->au1KeyString, "wpsstatus_KEY");
                        WebnmSendString (pHttp, pHttp->au1KeyString);
                        if(wps_status == 1)
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s","Success");
                        else if(wps_status == 2)
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s","Failure");

                        WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                        WebnmSendString (pHttp, pHttp->au1KeyString);


                         STRCPY (pHttp->au1KeyString, "wpspbcstatus_KEY");
                         WebnmSendString (pHttp, pHttp->au1KeyString);
                       if (wps_pbc_status == WPS_PBC_SUCCESS)
                       {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Success");
                       }
                      else  if (wps_pbc_status == WPS_PBC_OVERLAP)
                      {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Overlap");
                      }
                      else if (wps_pbc_status == WPS_PBC_INPROGRESS)
                      {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "In-progress");
                      }
                      else if (wps_pbc_status == WPS_PBC_INACTIVE)
                      {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Inactive");
                      }

                      else if(wps_pbc_status == WPS_PBC_ERROR)
                      {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Error");
                      }
                     WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

                    }
                   else
                   {

                        STRCPY (pHttp->au1KeyString, "wlan_KEY");
                        WebnmSendString (pHttp, pHttp->au1KeyString);
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s","-");
                        WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                        WebnmSendString (pHttp, pHttp->au1KeyString);


                        STRCPY (pHttp->au1KeyString, "stat_KEY");
                        WebnmSendString (pHttp, pHttp->au1KeyString);
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s","-");
                        WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                        WebnmSendString (pHttp, pHttp->au1KeyString);

                        STRCPY (pHttp->au1KeyString, "wpsmac_KEY");
                        WebnmSendString (pHttp, pHttp->au1KeyString);
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s","-");
                        WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                        WebnmSendString (pHttp, pHttp->au1KeyString);


                        STRCPY (pHttp->au1KeyString, "wpsstatus_KEY");
                        WebnmSendString (pHttp, pHttp->au1KeyString);
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s","-");
                        WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                        WebnmSendString (pHttp, pHttp->au1KeyString);

                        STRCPY (pHttp->au1KeyString, "wpspbcstatus_KEY");
                        WebnmSendString (pHttp, pHttp->au1KeyString);
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s","-");
                        WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                        WebnmSendString (pHttp, pHttp->au1KeyString);
                        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                   }


                }
            }
        }


    }while (nmhGetNextIndexCapwapDot11WlanTable
           (u4currentWlanId, &u4nextWlanId));
       PNAC_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;

}
#endif

#endif
#endif


