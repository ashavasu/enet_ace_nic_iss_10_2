/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lldpweb.c,v 1.18 2015/05/14 12:56:43 siva Exp $
 *
 * Description: This file contains the routines required for LLDP
 *              web module.
 *******************************************************************/

#ifndef _LLDP_WEB_C_
#define _LLDP_WEB_C_

#ifdef WEBNM_WANTED

#include "webiss.h"
#include "isshttp.h"
#include "vcm.h"
#include "iss.h"
#include "utilcli.h"
#include "lldp.h"
#include "lldpweb.h"

PRIVATE INT4        LldpWebIsVlanNameEnabled (INT4 i4IfIndex);
PRIVATE INT4
       LldpWebIsProtoVlanEnabled (INT4 i4IfIndex, UINT4 *pu4VlanId);
PRIVATE UINT1
 
 
 
LldpWebIsManAddrTlvEnabled (INT4 i4IfIndex, INT4 *pi4Type,
                            tSNMP_OCTET_STRING_TYPE * pManAddr);

extern UINT1        gu1TxAll;
/*********************************************************************
 *  Function Name : IssProcessLldpGlobalConfPage
 *  Description   : This function processes the request coming for the
 *                  LLDP Global conf page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None.
 *********************************************************************/

VOID
IssProcessLldpGlobalConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessLldpGlobalConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessLldpGlobalConfPageSet (pHttp);
    }
    return;
}

/*******************************************************************************
 *  Function Name : IssProcessLldpGlobalConfPageGet
 *  Description   : This function processes the get request for the Lldp
 *                  Global configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessLldpGlobalConfPageGet (tHttp * pHttp)
{
    INT4                i4RetSysStatus = 0;

    WebnmRegisterLock (pHttp, LldpLock, LldpUnLock);
    LldpLock ();

    pHttp->i4Write = 0;

    STRCPY (pHttp->au1KeyString, "GLOBAL_STATUS_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    i4RetSysStatus = 0;
    nmhGetFsLldpSystemControl (&i4RetSysStatus);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetSysStatus);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "MODULE_STATUS_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    i4RetSysStatus = 0;
    nmhGetFsLldpModuleStatus (&i4RetSysStatus);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetSysStatus);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "VERSION_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    i4RetSysStatus = 0;
    nmhGetFslldpv2Version (&i4RetSysStatus);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetSysStatus);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    LldpUnLock ();
    WebnmUnRegisterLock (pHttp);
}

/*******************************************************************************
 *  Function Name : IssProcessLldpGlobalConfPageSet
 *  Description   : This function processes the get request for the Lldp
 *                  Global configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessLldpGlobalConfPageSet (tHttp * pHttp)
{
    INT4                i4Status = 0;

    /* Locks */
    WebnmRegisterLock (pHttp, LldpLock, LldpUnLock);
    LldpLock ();

    STRCPY (pHttp->au1Name, "GLOBAL_STATUS");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Status = ATOI (pHttp->au1Value);
    }
    /* Set the Global Sys Control */
    LldpWebSetSystemCtrl (i4Status);

    STRCPY (pHttp->au1Name, "MODULE_STATUS");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Status = ATOI (pHttp->au1Value);
    }

    /* Set the Module Status */
    LldpWebSetModuleStatus (i4Status);

    STRCPY (pHttp->au1Name, "VERSION");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Status = ATOI (pHttp->au1Value);
    }

    /* Set the Module Version */
    LldpWebSetVersion (i4Status);

    LldpUnLock ();
    WebnmUnRegisterLock (pHttp);
    /* Page Get */
    IssProcessLldpGlobalConfPageGet (pHttp);

}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpWebSetSystemCtrl
 *
 *     DESCRIPTION      : This function will start/shut Lldp module
 *
 *     INPUT            : 
 *                        i4Status   - Lldp System Control Status
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpWebSetSystemCtrl (INT4 i4Status)
{

    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsLldpSystemControl (&u4ErrorCode, i4Status) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsLldpSystemControl (i4Status) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpWebSetModuleStatus
 *
 *     DESCRIPTION      : This function will enable/disable Lldp module
 *
 *     INPUT            : 
 *                        i4Status   - Lldp Module status
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpWebSetModuleStatus (INT4 i4Status)
{

    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsLldpModuleStatus (&u4ErrorCode, i4Status) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    nmhSetFsLldpModuleStatus (i4Status);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpWebSetVersion
 *
 *     DESCRIPTION      : This function will sets module version 
 *
 *     INPUT            : i4Status   - Lldp Module status
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
LldpWebSetVersion (INT4 i4Status)
{

    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Fslldpv2Version (&u4ErrorCode, i4Status) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    nmhSetFslldpv2Version (i4Status);

    return OSIX_SUCCESS;
}

/*********************************************************************
 *  Function Name : IssProcessLldpGlobalConfPage
 *  Description   : This function processes the request coming for the
 *                  LLDP Global conf page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None.
 *********************************************************************/

VOID
IssProcessLldpTrafficPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessLldpTrafficPageGet (pHttp);
    }
    if (pHttp->i4Method == ISS_SET)
    {
        IssProcessLldpTrafficPageSet (pHttp);
    }
    return;
}

 /**************************************************************************
 *
 *     FUNCTION NAME    : IssProcessLldpTrafficPageGet
 *
 *     DESCRIPTION      : Displays LLDP counters, including the no. of frames
 *                        sent, received, discarded, etc
 *
 *     INPUT            : 
 *                        i4IfIndex - Interface Index
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : OSIX_SUCCESS
 *
 ***************************************************************************/

VOID
IssProcessLldpTrafficPageGet (tHttp * pHttp)
{
    INT1               *pi1IfName = NULL;
    INT1                ai1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4FramesOut = 0;
    UINT4               u4PortFramesOut = 0;
    UINT4               u4FramesRecvInErr = 0;
    UINT4               u4PortFramesErr = 0;
    UINT4               u4EntriesAged = 0;
    UINT4               u4Temp = 0;
    UINT4               u4PortEntAged = 0;
    UINT4               u4PortFramesTot = 0;
    UINT4               u4FramesIn = 0;
    UINT4               u4PortTlvsDiscardTot = 0;
    UINT4               u4TlvsDiscardTot = 0;
    UINT4               u4PortTlvsUnrecogTot = 0;
    UINT4               u4TlvsUnrecogTot = 0;
    UINT4               u4PortFramesDiscardTot = 0;
    UINT4               u4FramesDiscardTot = 0;
    UINT4               u4PduLengthError = 0;
    UINT4               u4PduLengthErrorTot = 0;
    UINT4               u4DestMacIndex = 0;
    UINT4               u4PrevDestMacIndex = 0;
    INT4                i4IfIndex = 0;
    INT4                i4PrevIfIndex = 0;
    INT4                i4PortNum = LLDP_MIN_PORTS;
    INT4                i4MaxPorts = LLDP_MAX_SCAL_PORTS;

    MEMSET (&ai1IfName[0], 0, CFA_MAX_PORT_NAME_LENGTH);
    pi1IfName = ai1IfName;

    WebnmRegisterLock (pHttp, LldpLock, LldpUnLock);
    LldpLock ();

    nmhGetFirstIndexLldpV2PortConfigTable (&i4IfIndex, &u4DestMacIndex);

    do
    {
        u4PortFramesOut = 0;
        /* LLDP Transmit(Tx) Traffic */
        if (nmhGetLldpV2StatsTxPortFramesTotal
            (i4IfIndex, u4DestMacIndex, &u4PortFramesOut) != SNMP_FAILURE)
        {
            u4FramesOut = u4FramesOut + u4PortFramesOut;
        }

        u4PortFramesErr = 0;
        /* LLDP Receive(Rx) Traffic */
        if (nmhGetLldpV2StatsRxPortFramesErrors
            (i4IfIndex, u4DestMacIndex, &u4PortFramesErr) != SNMP_FAILURE)
        {
            u4FramesRecvInErr = u4FramesRecvInErr + u4PortFramesErr;
        }

        u4PortEntAged = 0;
        if (nmhGetLldpV2StatsRxPortAgeoutsTotal
            (i4IfIndex, u4DestMacIndex, &u4PortEntAged) != SNMP_FAILURE)
        {
            u4EntriesAged = u4EntriesAged + u4PortEntAged;
        }

        u4PortFramesTot = 0;
        if (nmhGetLldpV2StatsRxPortFramesTotal
            (i4IfIndex, u4DestMacIndex, &u4PortFramesTot) != SNMP_FAILURE)
        {
            u4FramesIn = u4FramesIn + u4PortFramesTot;
        }

        u4PortTlvsDiscardTot = 0;
        if (nmhGetLldpV2StatsRxPortTLVsDiscardedTotal
            (i4IfIndex, u4DestMacIndex, &u4PortTlvsDiscardTot) != SNMP_FAILURE)
        {
            u4TlvsDiscardTot = u4TlvsDiscardTot + u4PortTlvsDiscardTot;
        }

        u4PortTlvsUnrecogTot = 0;
        if (nmhGetLldpV2StatsRxPortTLVsUnrecognizedTotal
            (i4IfIndex, u4DestMacIndex, &u4PortTlvsUnrecogTot) != SNMP_FAILURE)
        {
            u4TlvsUnrecogTot = u4TlvsUnrecogTot + u4PortTlvsUnrecogTot;
        }

        u4PortFramesDiscardTot = 0;
        if (nmhGetLldpV2StatsRxPortFramesDiscardedTotal
            (i4IfIndex, u4DestMacIndex,
             &u4PortFramesDiscardTot) != SNMP_FAILURE)
        {
            u4FramesDiscardTot = u4FramesDiscardTot + u4PortFramesDiscardTot;
        }

        u4PduLengthError = 0;
        if (nmhGetLldpV2StatsTxLLDPDULengthErrors (i4IfIndex, u4DestMacIndex,
                                                   &u4PduLengthError) !=
            SNMP_FAILURE)
        {
            u4PduLengthErrorTot = u4PduLengthErrorTot + u4PduLengthError;
        }

        i4PrevIfIndex = i4IfIndex;
        u4PrevDestMacIndex = u4DestMacIndex;

    }
    while (nmhGetNextIndexLldpV2PortConfigTable
           (i4PrevIfIndex, &i4IfIndex, u4PrevDestMacIndex,
            &u4DestMacIndex) == SNMP_SUCCESS);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    u4Temp = (UINT4) pHttp->i4Write;

    for (i4PortNum = LLDP_MIN_PORTS; i4PortNum <= i4MaxPorts; i4PortNum++)
    {
        if (LLDP_GET_LOC_PORT_INFO ((UINT4) i4PortNum) == NULL)
        {
            continue;
        }
        pHttp->i4Write = (INT4) u4Temp;

        STRCPY (pHttp->au1KeyString, "PORT_NO_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        CfaCliGetIfName ((UINT4) i4PortNum, pi1IfName);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", (CHR1 *) pi1IfName);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "FRAMES_OUT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4FramesOut);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "ENTRIES_AGED_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4EntriesAged);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "FRAMES_IN_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4FramesIn);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "FRAMES_RX_IN_ERROR_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4FramesRecvInErr);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "FRAMES_DISCARDED_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4TlvsDiscardTot);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "UNRECOGNIZED_TLV_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4TlvsUnrecogTot);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "TOTAL_TLVS__DISCARDED_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4FramesDiscardTot);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "TOTAL_LENGTH_ERROR_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4PduLengthErrorTot);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

    }
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    LldpUnLock ();
    WebnmUnRegisterLock (pHttp);
}

/*********************************************************************
 *  Function Name : IssProcessLldpErrorPage
 *  Description   : This function processes the request coming for the
 *                  LLDP Global conf page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None.
 *********************************************************************/

VOID
IssProcessLldpErrorPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessLldpErrorPageGet (pHttp);
    }
    return;
}

/**************************************************************************
 *
 *     FUNCTION NAME    : IssProcessLldpErrorPageGet
 *
 *     DESCRIPTION      : Displays the information about the errors like
 *                        memory allocation failures, queue overflows, table
 *                        overflows, etc
 *
 *     INPUT            : None
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ***************************************************************************/

VOID
IssProcessLldpErrorPageGet (tHttp * pHttp)
{
    INT4                i4MemAllocFailure = 0;
    INT4                i4InputQOverFlows = 0;
    UINT4               u4RemTableOverFlows = 0;

    WebnmRegisterLock (pHttp, LldpLock, LldpUnLock);
    LldpLock ();

    nmhGetFsLldpMemAllocFailure (&i4MemAllocFailure);
    nmhGetFsLldpInputQOverFlows (&i4InputQOverFlows);
    nmhGetLldpV2StatsRemTablesDrops (&u4RemTableOverFlows);

    pHttp->i4Write = 0;

    STRCPY (pHttp->au1KeyString, "MEM_ALLOC_FAILS_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4MemAllocFailure);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "INQ_OVERFLOWS_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4InputQOverFlows);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "TABLE_OVERFLOWS_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RemTableOverFlows);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    LldpUnLock ();
    WebnmUnRegisterLock (pHttp);

}

/*********************************************************************
 *  Function Name : IssProcessLldpBasicSettingsPage
 *  Description   : This function processes the request coming for the
 *                  LLDP Global conf page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None.
 *********************************************************************/

VOID
IssProcessLldpBasicSettingsPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessLldpBasicSettingsPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessLldpBasicSettingsPageSet (pHttp);
    }
    return;
}

 /**************************************************************************
 *
 *     FUNCTION NAME    : IssProcessLldpBasicSettingsPageGet
 *
 *     DESCRIPTION      : This function will display the LLDP global
 *                        information
 *
 *     INPUT            : pHttp - Pointer to the global HTTP data structure
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 *
 ***************************************************************************/

VOID
IssProcessLldpBasicSettingsPageGet (tHttp * pHttp)
{
    UINT1               au1ChassisId[LLDP_MAX_LEN_CHASSISID + 1];
    UINT1               au1DispChassisId[LLDP_MAX_LEN_CHASSISID + 1];
    CHR1               *pc1DispChassIdNwAddr;
    UINT4               u4ChassIdNwAddr = 0;
    INT4                i4TxIntrvl = 0;
    INT4                i4HoldMulpr = 0;
    INT4                i4ReinitDelay = 0;
    INT4                i4TxDelay = 0;
    INT4                i4NotifyIntrvl = 0;
    INT4                i4ChassisIdSubType = 0;
    UINT4               u4TxCreditMax = 0;
    UINT4               u4MsgFastTx = 0;
    UINT4               u4TxFastInit = 0;
    tSNMP_OCTET_STRING_TYPE ChassisId;

    MEMSET (&ChassisId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ChassisId, 0, LLDP_MAX_LEN_CHASSISID + 1);

    ChassisId.pu1_OctetList = (UINT1 *) &au1ChassisId[0];

    WebnmRegisterLock (pHttp, LldpLock, LldpUnLock);
    LldpLock ();

    nmhGetLldpV2MessageTxInterval ((UINT4 *) &i4TxIntrvl);
    nmhGetLldpV2MessageTxHoldMultiplier ((UINT4 *) &i4HoldMulpr);
    nmhGetLldpV2ReinitDelay ((UINT4 *) &i4ReinitDelay);
    nmhGetLldpTxDelay (&i4TxDelay);
    nmhGetLldpV2NotificationInterval ((UINT4 *) &i4NotifyIntrvl);
    nmhGetFsLldpLocChassisIdSubtype (&i4ChassisIdSubType);
    nmhGetLldpV2TxCreditMax (&u4TxCreditMax);
    nmhGetLldpV2MessageFastTx (&u4MsgFastTx);
    nmhGetLldpV2TxFastInit (&u4TxFastInit);

    pHttp->i4Write = 0;

    STRCPY (pHttp->au1KeyString, "TRANS_INTERVAL_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4TxIntrvl);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "HOLD_TIME_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4HoldMulpr);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "REINIT_DELAY_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4ReinitDelay);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "TX_DELAY_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4TxDelay);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "NOTIFY_INTERVAL_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NotifyIntrvl);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    /*Chassis Id SubType */

    STRCPY (pHttp->au1KeyString, "CHASSIS_ID_SUBTYPE_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4ChassisIdSubType);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    nmhGetLldpV2LocChassisId (&ChassisId);
    if (i4ChassisIdSubType == LLDP_CHASS_ID_SUB_MAC_ADDR)
    {
        MEMSET (&au1DispChassisId, 0, sizeof (au1DispChassisId));

        /* Convert octet values into strings of octets seperated by colon */
        CLI_CONVERT_MAC_TO_DOT_STR (ChassisId.pu1_OctetList,
                                    &au1DispChassisId[0]);

        /* Chassis Id               : au1DispChassisId */
        STRCPY (pHttp->au1KeyString, "CHASSIS_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1DispChassisId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
    }
    else if (i4ChassisIdSubType == LLDP_CHASS_ID_SUB_NW_ADDR)
    {
        MEMSET (au1DispChassisId, 0, sizeof (au1DispChassisId));
        /* To skip the IANA Address Family Numbers */
        ChassisId.pu1_OctetList++;
        PTR_FETCH4 (u4ChassIdNwAddr, ChassisId.pu1_OctetList);
        pc1DispChassIdNwAddr = (CHR1 *) & au1DispChassisId[0];
        /* Convert octet values into strings of octets seperated by dot */
        CLI_CONVERT_IPADDR_TO_STR (pc1DispChassIdNwAddr, u4ChassIdNwAddr);
        /* Chassis Id               : pc1DispChassIdNwAddr */
        STRCPY (pHttp->au1KeyString, "CHASSIS_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pc1DispChassIdNwAddr);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
    }
    else if (i4ChassisIdSubType == LLDP_CHASS_ID_SUB_IF_ALIAS
             || i4ChassisIdSubType == LLDP_CHASS_ID_SUB_IF_NAME)
    {
        /* Chassis Id  : ChassisId.pu1_OctetList */
        STRCPY (pHttp->au1KeyString, "CHASSIS_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", ChassisId.pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
    }
    else
    {
        STRCPY (pHttp->au1Name, "CHASSIS_ID");

        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
        }

        if (STRCMP (pHttp->au1Value, ChassisId.pu1_OctetList) == 0)
        {
            /* Chassis Id  : ChassisId.pu1_OctetList */
            STRCPY (pHttp->au1KeyString, "CHASSIS_ID_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     ChassisId.pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        else
        {
            MEMSET (au1DispChassisId, 0, sizeof (au1DispChassisId));
            pc1DispChassIdNwAddr = (CHR1 *) & au1DispChassisId[0];
            STRCPY (pHttp->au1KeyString, "CHASSIS_ID_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pc1DispChassIdNwAddr);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
    }

    STRCPY (pHttp->au1KeyString, "TX_CREDIT_MAX_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4TxCreditMax);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "MESSAGE_FAST_TX_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4MsgFastTx);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "TX_FAST_INIT_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4TxFastInit);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    LldpUnLock ();
    WebnmUnRegisterLock (pHttp);

    return;
}

 /**************************************************************************
 *
 *     FUNCTION NAME    : IssProcessLldpBasicSettingsPageSet
 *
 *     DESCRIPTION      : This function will display the LLDP global
 *                        information
 *
 *     INPUT            : pHttp - Pointer to the global HTTP data structure
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 *
 ***************************************************************************/

VOID
IssProcessLldpBasicSettingsPageSet (tHttp * pHttp)
{

    INT4                i4Interval = 0;
    INT4                i4Type = 0;
    INT4                i4RetVal = 0;
    tMacAddr            SwitchMacAddr;
    INT4                i4DefaultChassisIdSubtype = LLDP_CHASS_ID_SUB_MAC_ADDR;
    UINT1               au1StoreMacAddr[MAC_ADDR_LEN];

    MEMSET (SwitchMacAddr, 0, sizeof (tMacAddr));
    MEMSET (au1StoreMacAddr, 0, MAC_ADDR_LEN);

    WebnmRegisterLock (pHttp, LldpLock, LldpUnLock);
    LldpLock ();

    STRCPY (pHttp->au1Name, "TRANS_INTERVAL");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Interval = ATOI (pHttp->au1Value);
    }
    /* Set the Trans Interval */
    LldpWebSetTransmitInterval (i4Interval);

    STRCPY (pHttp->au1Name, "HOLD_TIME");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Interval = ATOI (pHttp->au1Value);

    }
    /* Set the Hold Time */
    LldpWebSetHoldtimeMuliplier (i4Interval);

    STRCPY (pHttp->au1Name, "REINIT_DELAY");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Interval = ATOI (pHttp->au1Value);
    }
    /* Set Reinit Delay */
    LldpWebSetReinitDelay (i4Interval);

    STRCPY (pHttp->au1Name, "TX_DELAY");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Interval = ATOI (pHttp->au1Value);
    }
    /* Set TxDelay */
    LldpWebSetTransmitDelay (i4Interval);

    STRCPY (pHttp->au1Name, "NOTIFY_INTERVAL");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Interval = ATOI (pHttp->au1Value);
    }
    LldpWebSetNotifyInterval (i4Interval);

    STRCPY (pHttp->au1Name, "CHASSIS_ID_SUBTYPE");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Type = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "CHASSIS_ID");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
    }

    i4RetVal = LldpWebSetChassisSubtype (i4Type, pHttp->au1Value);

    if (i4RetVal == OSIX_FAILURE)
    {

        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Error in setting Chassis ID,restoring to default values");
        /* if setChassisSubtype fails ,restoring to default value (i.e)mac address) */
        LldpPortGetSysMacAddr (SwitchMacAddr);
        MEMCPY (au1StoreMacAddr, SwitchMacAddr, MAC_ADDR_LEN);

        LldpWebSetChassisSubtype (i4DefaultChassisIdSubtype, au1StoreMacAddr);
    }

    STRCPY (pHttp->au1Name, "TX_CREDIT_MAX");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Interval = ATOI (pHttp->au1Value);
    }
    /* Set TxCreditMax */
    LldpWebSetTxCreditMax (i4Interval);

    STRCPY (pHttp->au1Name, "MESSAGE_FAST_TX");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Interval = ATOI (pHttp->au1Value);
    }
    /* Set MessageFastTx */
    LldpWebSetMessageFastTx (i4Interval);

    STRCPY (pHttp->au1Name, "TX_FAST_INIT");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Interval = ATOI (pHttp->au1Value);
    }
    /* Set TxFastInit */
    LldpWebSetTxFastInit (i4Interval);

    LldpUnLock ();
    WebnmUnRegisterLock (pHttp);

    IssProcessLldpBasicSettingsPageGet (pHttp);
}

/*********************************************************************
 *  Function Name : IssProcessLldpStatsPage
 *  Description   : This function processes the request coming for the
 *                  LLDP Stats page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None.
 *********************************************************************/

VOID
IssProcessLldpStatsPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessLldpStatsPageGet (pHttp);
    }
    return;
}

/**************************************************************************
 *
 *     FUNCTION NAME    : IssProcessLldpTrafficPageSet
 *
 *     DESCRIPTION      : This function will display the LLDP global
 *                        information
 *
 *     INPUT            : pHttp - Pointer to the global HTTP data structure
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 *
 ***************************************************************************/

VOID
IssProcessLldpTrafficPageSet (tHttp * pHttp)
{

    WebnmRegisterLock (pHttp, LldpLock, LldpUnLock);
    LldpLock ();

    LldpWebClearCounters ();

    LldpUnLock ();
    WebnmUnRegisterLock (pHttp);

    IssProcessLldpTrafficPageGet (pHttp);
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpWebSetTransmitInterval
 *
 *     DESCRIPTION      : This module sets the interval (in seconds) at
 *                        which LLDP frames are transmitted.
 *
 *     INPUT            : 
 *                        i4Interval  - Lldp Transmission Interval
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpWebSetTransmitInterval (INT4 i4Interval)
{

    UINT4               u4ErrorCode = 0;

    if (nmhTestv2LldpV2MessageTxInterval (&u4ErrorCode, i4Interval)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    nmhSetLldpV2MessageTxInterval (i4Interval);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpWebSetHoldtimeMuliplier
 *
 *     DESCRIPTION      : This module changes the multiplier (An LLDP switch
 *                        uses the multiplier to calculate the Time-to-Live
 *                        for the LLDP advertisements) on LLDP.
 *
 *     INPUT            : 
 *                        i4Val      - Lldp Holdtime Multiplier
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpWebSetHoldtimeMuliplier (INT4 i4Val)
{

    UINT4               u4ErrorCode = 0;

    if (nmhTestv2LldpV2MessageTxHoldMultiplier (&u4ErrorCode, i4Val) ==
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    nmhSetLldpV2MessageTxHoldMultiplier (i4Val);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpWebSetReinitDelay
 *
 *     DESCRIPTION      : Sets the delay time (in seconds) for LLDP 
 *                        to re-initialize on any interface
 *
 *     INPUT            : 
 *                        i4ReinitDelay - Lldp Reinit Delay
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpWebSetReinitDelay (INT4 i4ReinitDelay)
{

    UINT4               u4ErrorCode = 0;

    if (nmhTestv2LldpV2ReinitDelay (&u4ErrorCode, i4ReinitDelay) ==
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    nmhSetLldpV2ReinitDelay (i4ReinitDelay);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpWebSetTransmitDelay
 *
 *     DESCRIPTION      : Sets the delay (in seconds) between successive 
 *                        LLDP frame transmission
 *
 *     INPUT            : 
 *                        i4TxDelay  - Lldp Transmit Delay
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpWebSetTransmitDelay (INT4 i4TxDelay)
{

    UINT4               u4ErrorCode = 0;

    if (nmhTestv2LldpTxDelay (&u4ErrorCode, i4TxDelay) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    nmhSetLldpTxDelay (i4TxDelay);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpWebSetNotifyInterval
 *
 *     DESCRIPTION      : Sets the interval (in seconds) between successive 
 *                        traps generated by the switch                        
 *
 *     INPUT            : CliHandle        - CliContext ID
 *                        i4NotifyInterval - LLDP Notification Interval
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpWebSetNotifyInterval (INT4 i4NotifyInterval)
{

    UINT4               u4ErrorCode = 0;

    if (nmhTestv2LldpV2NotificationInterval (&u4ErrorCode, i4NotifyInterval)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    nmhSetLldpV2NotificationInterval (i4NotifyInterval);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpWebSetChassisSubtype
 *
 *     DESCRIPTION      : Configures lldp chassis id subtype and chassis 
 *                        id value 
 *
 *     INPUT            : 
 *                        i4ChassisSubtype - Chassis id subtype
 *                        pu1ChassisId     - Chassis id
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpWebSetChassisSubtype (INT4 i4ChassisIdSubtype, UINT1 *pu1ChassisId)
{
    tSNMP_OCTET_STRING_TYPE ChassisId;
    UINT4               u4ErrorCode = 0;
    UINT2               u2ChassisIdLen = 0;

    MEMSET (&ChassisId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    if (nmhTestv2FsLldpLocChassisIdSubtype (&u4ErrorCode, i4ChassisIdSubtype)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsLldpLocChassisIdSubtype (i4ChassisIdSubtype) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if ((i4ChassisIdSubtype == LLDP_CHASS_ID_SUB_CHASSIS_COMP) ||
        (i4ChassisIdSubtype == LLDP_CHASS_ID_SUB_PORT_COMP) ||
        (i4ChassisIdSubtype == LLDP_CHASS_ID_SUB_LOCAL))

    {
        LldpUtilGetChassisIdLen (i4ChassisIdSubtype, pu1ChassisId,
                                 &u2ChassisIdLen);
        ChassisId.pu1_OctetList = pu1ChassisId;
        ChassisId.i4_Length = (INT4) u2ChassisIdLen;

        if (nmhTestv2FsLldpLocChassisId (&u4ErrorCode, &ChassisId) ==
            SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }

        nmhSetFsLldpLocChassisId (&ChassisId);
    }
    if ((i4ChassisIdSubtype == LLDP_CHASS_ID_SUB_MAC_ADDR
         && STRLEN (pu1ChassisId) != 0))
    {
        if (nmhSetFsLldpLocChassisIdSubtype (i4ChassisIdSubtype) ==
            SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }
        ChassisId.pu1_OctetList = pu1ChassisId;
        ChassisId.i4_Length = (INT4) MAC_ADDR_LEN;

        nmhSetFsLldpLocChassisId (&ChassisId);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpWebSetTxCreditMax
 *
 *     DESCRIPTION      : Configures lldp TxCreditMax
 *
 *     INPUT            : i4TxCreditMax 
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
LldpWebSetTxCreditMax (INT4 i4TxCreditMax)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2LldpV2TxCreditMax (&u4ErrorCode, (UINT4) i4TxCreditMax) ==
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    nmhSetLldpV2TxCreditMax ((UINT4) i4TxCreditMax);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpWebSetMessageFastTx
 *
 *     DESCRIPTION      : Configures lldp MessageFastTx
 *
 *     INPUT            : i4MessageFastTx 
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
LldpWebSetMessageFastTx (INT4 i4MessageFastTx)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2LldpV2MessageFastTx (&u4ErrorCode, (UINT4) i4MessageFastTx) ==
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    nmhSetLldpV2MessageFastTx ((UINT4) i4MessageFastTx);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpWebSetTxFastInit
 *
 *     DESCRIPTION      : Configures lldp TxFastInit
 *
 *     INPUT            : i4TxFastInit
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
LldpWebSetTxFastInit (INT4 i4TxFastInit)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2LldpV2TxFastInit (&u4ErrorCode, (UINT4) i4TxFastInit) ==
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    nmhSetLldpV2TxFastInit ((UINT4) i4TxFastInit);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpWebClearCounters
 *
 *     DESCRIPTION      : Clears LLDP transmit and receive statistics
 *
 *     INPUT            : 
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpWebClearCounters ()
{
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;

    UINT4               u4PortNum = 0;

    /* Clears LLDP statistics info for every ports */
    for (u4PortNum = LLDP_MIN_PORTS; u4PortNum <= LLDP_MAX_SCAL_PORTS;
         u4PortNum++)
    {
        if (LldpTxUtlGetLocPortEntry (u4PortNum, &pLldpLocPortInfo)
            == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
        /* Clears LLDP statistics info */
        LldpUtilClearStats (pLldpLocPortInfo);
    }
    return OSIX_SUCCESS;
}

 /****************************************************************************
 *
 *     FUNCTION NAME    : LldpWebClearTable
 *
 *     DESCRIPTION      : Clears LLDP neighbors information
 *
 *     INPUT            : 
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpWebClearTable ()
{

    /* Clears LLDP neighbors info table */
    LldpRxUtlDrainRemSysTables ();
    return OSIX_SUCCESS;
}

 /**************************************************************************
 *
 *     FUNCTION NAME    : IssProcessLldpStatsPageGet
 *
 *     DESCRIPTION      : Displays the LLDP Statistics information
 *
 *     INPUT            : CliHandle - CliContext ID
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 *
 ***************************************************************************/

VOID
IssProcessLldpStatsPageGet (tHttp * pHttp)
{
    UINT4               u4RemTabsLastChgTime = 0;
    UINT4               u4RemTabsInserts = 0;
    UINT4               u4RemTablesDeletes = 0;
    UINT4               u4RemTablesDrops = 0;
    UINT4               u4RemTablesAgeouts = 0;
    UINT4               u4RemTablesUpdates = 0;

    WebnmRegisterLock (pHttp, LldpLock, LldpUnLock);
    LldpLock ();

    nmhGetLldpV2StatsRemTablesLastChangeTime (&u4RemTabsLastChgTime);
    nmhGetLldpV2StatsRemTablesInserts (&u4RemTabsInserts);
    nmhGetLldpV2StatsRemTablesDeletes (&u4RemTablesDeletes);
    nmhGetLldpV2StatsRemTablesDrops (&u4RemTablesDrops);
    nmhGetLldpV2StatsRemTablesAgeouts (&u4RemTablesAgeouts);
    nmhGetFsLldpStatsRemTablesUpdates (&u4RemTablesUpdates);

    pHttp->i4Write = 0;

    STRCPY (pHttp->au1KeyString, "LAST_CHANGE_TIME_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RemTabsLastChgTime);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "TABLE_INSERTS_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RemTabsInserts);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "TABLE_DELETES_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RemTablesDeletes);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "TABLE_DROPS_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RemTablesDrops);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "TABLE_AGEOUTS_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RemTablesAgeouts);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "TABLE_UPDATES_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RemTablesUpdates);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    LldpUnLock ();
    WebnmUnRegisterLock (pHttp);

}

/*********************************************************************
 *  Function Name : IssProcessInterfacePage
 *  Description   : This function processes the request coming for the
 *                  LLDP Global conf page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None.
 *********************************************************************/

VOID
IssProcessLldpInterfacePage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessLldpInterfacePageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessLldpInterfacePageSet (pHttp);
    }
    return;
}

/**************************************************************************
 *
 *     FUNCTION NAME    : IssProcessLldpInterfacePageGet
 *
 *     DESCRIPTION      : Displays LLDP configuration details on a particular
 *                        interface or all interfaces
 *
 *     INPUT            : 
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 *
 ***************************************************************************/

VOID
IssProcessLldpInterfacePageGet (tHttp * pHttp)
{
    INT1               *pi1IfName = NULL;
    INT1                ai1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT4                i4Status = 0;
    INT4                i4PrevIfIndex = 0;
    INT4                i4TxState = 0;
    INT4                i4RxState = 0;
    INT4                i4NotifyStatus = 0;
    INT4                i4NotifyType = 0;
    INT4                i4IfIndex = 0;
    INT4                i4RetVal = 0;
    UINT4               u4Temp = 0;
    UINT4               u4DestMacIndex = 0;
    UINT4               u4PrevDestMacIndex = 0;
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;
    tMacAddr            DstMac;
    UINT1               au1DstMac[LLDP_MAX_MAC_STRING_SIZE] = { 0 };

    pi1IfName = ai1IfName;

    MEMSET (&DstMac, 0, MAC_ADDR_LEN);
    /*MEMSET (au1DstMac, 0, MAC_ADDR_LEN); */

    WebnmRegisterLock (pHttp, LldpLock, LldpUnLock);
    LldpLock ();

    pHttp->i4Write =0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    if (nmhGetFirstIndexLldpV2PortConfigTable (&i4IfIndex, &u4DestMacIndex) ==
        SNMP_FAILURE)
    {
        LldpUnLock ();
        WebnmUnRegisterLock (pHttp);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        return;
    }

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;
    do
    {

        pHttp->i4Write = (INT4) u4Temp;

        STRCPY (pHttp->au1KeyString, "LLDP_PORT_NO_KEY_NAME");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        CfaCliGetIfName ((UINT4) i4IfIndex, pi1IfName);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", (CHR1 *) pi1IfName);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "LLDP_PORT_NO_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4IfIndex);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetLldpV2PortConfigAdminStatus (i4IfIndex, u4DestMacIndex,
                                           &i4Status);
        i4RetVal =
            LldpTxUtlGetLocPortEntry ((UINT4) i4IfIndex, &pLldpLocPortInfo);
        UNUSED_PARAM (i4RetVal);

        /* LLDP Admin Status */
        switch (i4Status)
        {
            case LLDP_ADM_STAT_TX_AND_RX:

                /* Tx State            : Enabled
                   Rx State            : Enabled */
                STRCPY (pHttp->au1KeyString, "TX_STATE_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", LLDP_TRANSMIT);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "RX_STATE_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", LLDP_RECEIVE);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
                break;

            case LLDP_ADM_STAT_TX_ONLY:
                /* Tx State            : Enabled
                   Rx State            : Disabled */
                STRCPY (pHttp->au1KeyString, "TX_STATE_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", LLDP_TRANSMIT);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "RX_STATE_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", LLDP_NO_RECEIVE);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
                break;

            case LLDP_ADM_STAT_RX_ONLY:
                /* Tx State            : Disabled
                   Rx State            : Enabled */
                STRCPY (pHttp->au1KeyString, "TX_STATE_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", LLDP_NO_TRANSMIT);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "RX_STATE_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", LLDP_RECEIVE);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
                break;
            case LLDP_ADM_STAT_DISABLED:
                /* Tx State            : Disabled
                   Rx State            : Disabled */
                STRCPY (pHttp->au1KeyString, "TX_STATE_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", LLDP_NO_TRANSMIT);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "RX_STATE_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", LLDP_NO_RECEIVE);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
                break;
            default:
                break;
        }

        if (pLldpLocPortInfo == NULL)
        {
            LldpUnLock ();
            WebnmUnRegisterLock (pHttp);
            return;
        }

        i4TxState = pLldpLocPortInfo->i4TxSemCurrentState;

        /* Transmit SEM state */
        STRCPY (pHttp->au1KeyString, "TX_SEM_STATE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4TxState);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        i4RxState = pLldpLocPortInfo->i4RxSemCurrentState;

        /* Receive SEM state */

        STRCPY (pHttp->au1KeyString, "RX_SEM_STATE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RxState);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetLldpV2PortConfigNotificationEnable (i4IfIndex, u4DestMacIndex,
                                                  &i4NotifyStatus);

        /* LLDP Notification Status */
        STRCPY (pHttp->au1KeyString, "NOTIFICATION_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NotifyStatus);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsLldpPortConfigNotificationType (i4IfIndex, &i4NotifyType);

        /* LLDP Notification Type  */
        STRCPY (pHttp->au1KeyString, "NOTIFICATION_TYPE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NotifyType);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* LLDP Destination MAC address  */
        nmhGetLldpV2DestMacAddress (u4DestMacIndex, &DstMac);
        PrintMacAddress (DstMac, au1DstMac);
        STRCPY (pHttp->au1KeyString, "DST_MAC_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1DstMac);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        i4PrevIfIndex = i4IfIndex;
        u4PrevDestMacIndex = u4DestMacIndex;
    }
    while (nmhGetNextIndexLldpV2PortConfigTable
           (i4PrevIfIndex, &i4IfIndex, u4PrevDestMacIndex,
            &u4DestMacIndex) == SNMP_SUCCESS);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    LldpUnLock ();
    WebnmUnRegisterLock (pHttp);

    return;
}

/**************************************************************************
 *
 *     FUNCTION NAME    : IssProcessLldpInterfacePageSet
 *
 *     DESCRIPTION      : Displays LLDP configuration details on a particular
 *                        interface or all interfaces
 *
 *     INPUT            : 
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 *
 ***************************************************************************/

VOID
IssProcessLldpInterfacePageSet (tHttp * pHttp)
{

    INT4                i4Status = 0;
    INT4                i4NotificationType = 0;
    INT4                i4NotificationStatus = 0;
    INT4                i4IfIndex = 0;
    UINT4               u4DestMacIndex = 0;
    UINT1               au1DstMac[MAC_ADDR_LEN];

    /* Locks */
    WebnmRegisterLock (pHttp, LldpLock, LldpUnLock);
    LldpLock ();

    MEMSET (au1DstMac, 0, MAC_ADDR_LEN);
    STRCPY (pHttp->au1Name, "LLDP_PORT_NO");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4IfIndex = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "DST_MAC");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeMacAddr (pHttp->au1Value);
        StrToMac ((UINT1 *) pHttp->au1Value, au1DstMac);
    }

    LldpUtlGetDestMacTblIndex (au1DstMac, &u4DestMacIndex);

    STRCPY (pHttp->au1Name, "TX_STATE");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Status = ATOI (pHttp->au1Value);
    }

    /* Set the TX State Control */
    LldpWebSetAdminStatus (i4IfIndex, u4DestMacIndex, i4Status);

    STRCPY (pHttp->au1Name, "RX_STATE");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Status = ATOI (pHttp->au1Value);
    }

    /* Set the RX State Control */
    LldpWebSetAdminStatus (i4IfIndex, u4DestMacIndex, i4Status);

    STRCPY (pHttp->au1Name, "NOTIFICATION_STATUS");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4NotificationStatus = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "NOTIFICATION_TYPE");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4NotificationType = ATOI (pHttp->au1Value);
    }

    /* Set the Notification Status */
    LldpWebSetNotification (i4IfIndex, i4NotificationStatus,
                            i4NotificationType, u4DestMacIndex);

    LldpUnLock ();
    WebnmUnRegisterLock (pHttp);

    /* Page Get */
    IssProcessLldpInterfacePageGet (pHttp);

}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpWebSetAdminStatus
 *
 *     DESCRIPTION      : Enables/Disable transmission/reception or both 
 *                        of LLDP frames on an interface
 *                  
 *     INPUT            : 
 *                        i4IfIndex      -   Interface Index
 *                        i4AdminStatus  -   LLDP Admin Status
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpWebSetAdminStatus (INT4 i4IfIndex, UINT4 u4DestMacIndex, INT4 i4AdminStatus)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4CurrentAdminStatus = 0;

    if (nmhGetLldpV2PortConfigAdminStatus (i4IfIndex, u4DestMacIndex,
                                           &i4CurrentAdminStatus) ==
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Depends upon current admin status, sets admin status */
    switch (i4AdminStatus)
    {
        case LLDP_TRANSMIT:

            if (i4CurrentAdminStatus == LLDP_ADM_STAT_RX_ONLY)
            {
                if (nmhTestv2LldpV2PortConfigAdminStatus
                    (&u4ErrorCode, i4IfIndex, u4DestMacIndex,
                     LLDP_ADM_STAT_TX_AND_RX) == SNMP_FAILURE)
                {
                    return OSIX_FAILURE;
                }
                if (nmhSetLldpV2PortConfigAdminStatus
                    (i4IfIndex, u4DestMacIndex,
                     LLDP_ADM_STAT_TX_AND_RX) == SNMP_FAILURE)
                {

                    return OSIX_FAILURE;
                }
            }
            else if (i4CurrentAdminStatus == LLDP_ADM_STAT_DISABLED)
            {
                if (nmhTestv2LldpV2PortConfigAdminStatus
                    (&u4ErrorCode, i4IfIndex, u4DestMacIndex,
                     LLDP_ADM_STAT_TX_ONLY) == SNMP_FAILURE)
                {
                    return OSIX_FAILURE;
                }
                if (nmhSetLldpV2PortConfigAdminStatus
                    (i4IfIndex, u4DestMacIndex,
                     LLDP_ADM_STAT_TX_ONLY) == SNMP_FAILURE)
                {

                    return OSIX_FAILURE;
                }
            }
            break;

        case LLDP_RECEIVE:

            if (i4CurrentAdminStatus == LLDP_ADM_STAT_TX_ONLY)
            {
                if (nmhTestv2LldpV2PortConfigAdminStatus
                    (&u4ErrorCode, i4IfIndex, u4DestMacIndex,
                     LLDP_ADM_STAT_TX_AND_RX) == SNMP_FAILURE)
                {
                    return OSIX_FAILURE;
                }
                if (nmhSetLldpV2PortConfigAdminStatus
                    (i4IfIndex, u4DestMacIndex,
                     LLDP_ADM_STAT_TX_AND_RX) == SNMP_FAILURE)
                {

                    return OSIX_FAILURE;
                }
            }
            else if (i4CurrentAdminStatus == LLDP_ADM_STAT_DISABLED)
            {
                if (nmhTestv2LldpV2PortConfigAdminStatus
                    (&u4ErrorCode, (INT4) u4DestMacIndex, (UINT4) i4IfIndex,
                     LLDP_ADM_STAT_RX_ONLY) == SNMP_FAILURE)
                {
                    return OSIX_FAILURE;
                }
                if (nmhSetLldpV2PortConfigAdminStatus
                    (i4IfIndex, u4DestMacIndex,
                     LLDP_ADM_STAT_RX_ONLY) == SNMP_FAILURE)
                {

                    return OSIX_FAILURE;
                }
            }
            break;

        case LLDP_NO_TRANSMIT:

            if (i4CurrentAdminStatus == LLDP_ADM_STAT_TX_ONLY)
            {
                if (nmhTestv2LldpV2PortConfigAdminStatus
                    (&u4ErrorCode, i4IfIndex, u4DestMacIndex,
                     LLDP_ADM_STAT_DISABLED) == SNMP_FAILURE)
                {
                    return OSIX_FAILURE;
                }

                if (nmhSetLldpV2PortConfigAdminStatus
                    (i4IfIndex, u4DestMacIndex,
                     LLDP_ADM_STAT_DISABLED) == SNMP_FAILURE)
                {

                    return OSIX_FAILURE;
                }
            }
            else if (i4CurrentAdminStatus == LLDP_ADM_STAT_TX_AND_RX)
            {
                if (nmhTestv2LldpV2PortConfigAdminStatus
                    (&u4ErrorCode, i4IfIndex, u4DestMacIndex,
                     LLDP_ADM_STAT_RX_ONLY) == SNMP_FAILURE)
                {
                    return OSIX_FAILURE;
                }

                if (nmhSetLldpV2PortConfigAdminStatus
                    (i4IfIndex, u4DestMacIndex,
                     LLDP_ADM_STAT_RX_ONLY) == SNMP_FAILURE)
                {

                    return OSIX_FAILURE;
                }
            }
            break;

        case LLDP_NO_RECEIVE:

            if (i4CurrentAdminStatus == LLDP_ADM_STAT_RX_ONLY)
            {
                if (nmhTestv2LldpV2PortConfigAdminStatus
                    (&u4ErrorCode, i4IfIndex, u4DestMacIndex,
                     LLDP_ADM_STAT_DISABLED) == SNMP_FAILURE)
                {
                    return OSIX_FAILURE;
                }

                if (nmhSetLldpV2PortConfigAdminStatus
                    (i4IfIndex, u4DestMacIndex,
                     LLDP_ADM_STAT_DISABLED) == SNMP_FAILURE)
                {

                    return OSIX_FAILURE;
                }
            }
            else if (i4CurrentAdminStatus == LLDP_ADM_STAT_TX_AND_RX)
            {
                if (nmhTestv2LldpV2PortConfigAdminStatus
                    (&u4ErrorCode, i4IfIndex, u4DestMacIndex,
                     LLDP_ADM_STAT_TX_ONLY) == SNMP_FAILURE)
                {
                    return OSIX_FAILURE;
                }

                if (nmhSetLldpV2PortConfigAdminStatus
                    (i4IfIndex, u4DestMacIndex,
                     LLDP_ADM_STAT_TX_ONLY) == SNMP_FAILURE)
                {

                    return OSIX_FAILURE;
                }
            }
            break;
        default:
            LLDP_TRC (ALL_FAILURE_TRC, "LldpWebSetAdminStatus: "
                      "Received invalid option \r\n");
            break;
    }
    return OSIX_SUCCESS;
}

 /****************************************************************************
 *
 *     FUNCTION NAME    : LldpWebSetNotification
 *
 *     DESCRIPTION      : Enables/Disable LLDP trap notification on an 
 *                        interface                        
 *
 *     INPUT            : 
 *                        i4IfIndex    -   Interface Index
 *                        i4Status     -   Port Admin Status
 *                        i4NotifyType -   Notification Type 
 *                                         (Remote tabel change,
 *                                         Misconfiguration)
 *     OUTPUT           : None
 *
 *     RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/

PUBLIC INT4
LldpWebSetNotification (INT4 i4IfIndex, INT4 i4Status, INT4 i4NotifyType,
                        UINT4 u4DestMacIndex)
{
    UINT4               u4ErrorCode = 0;

    /*  Notification Enable/Disable Status */
    if (nmhTestv2LldpV2PortConfigNotificationEnable
        (&u4ErrorCode, i4IfIndex, u4DestMacIndex, i4Status) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetLldpV2PortConfigNotificationEnable
        (i4IfIndex, u4DestMacIndex, i4Status) == SNMP_FAILURE)
    {

        return OSIX_FAILURE;
    }

    /* Notification Type */
    if ((i4NotifyType == LLDP_REMOTE_CHG_MIS_CFG_NOTIFICATION) ||
        (i4NotifyType == LLDP_REMOTE_CHG_NOTIFICATION) ||
        (i4NotifyType == LLDP_MIS_CFG_NOTIFICATION))
    {
        if (nmhTestv2FsLldpPortConfigNotificationType
            (&u4ErrorCode, i4IfIndex, i4NotifyType) == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }
        if (nmhSetFsLldpPortConfigNotificationType (i4IfIndex, i4NotifyType) ==
            SNMP_FAILURE)
        {

            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : LldpWebUpdTraceInput
 *
 *     DESCRIPTION      : This function updates(sets/resets) the trace input
 *
 *     INPUT            : 
 *                        pu1TraceInput   - Pointer to trace input string
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ***************************************************************************/

PUBLIC INT4
LldpWebUpdTraceInput (UINT1 *pu1TraceInput)
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE TraceInput;

    MEMSET (&TraceInput, 0, sizeof (TraceInput));
    TraceInput.pu1_OctetList = pu1TraceInput;
    TraceInput.i4_Length = (INT4) STRLEN (pu1TraceInput);

    if (nmhTestv2FsLldpTraceInput (&u4ErrorCode, &TraceInput) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsLldpTraceInput (&TraceInput) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*********************************************************************
 *  Function Name : IssProcessInterfacePage
 *  Description   : This function processes the request coming for the
 *                  LLDP Global conf page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None.
 *********************************************************************/

VOID
IssProcessLldpNeighborPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessLldpNeighborPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessLldpNeighborPageSet (pHttp);
    }
    return;
}

/*********************************************************************
 *  Function Name : IssProcessInterfacePageSet
 *  Description   : This function processes the request coming for the
 *                  LLDP Global conf page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None.
 *********************************************************************/

VOID
IssProcessLldpNeighborPageSet (tHttp * pHttp)
{
    LldpWebClearTable ();
    IssProcessLldpNeighborPageGet (pHttp);
}

/*********************************************************************
 *  Function Name : IssProcessInterfacePageGet
 *  Description   : This function processes the request coming for the
 *                  LLDP Global conf page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None.
 *********************************************************************/

VOID
IssProcessLldpNeighborPageGet (tHttp * pHttp)
{

    UINT4               u4RemTimeMark = 0;
    INT4                i4RemLocalPortNum = 0;
    INT4                i4RemIndex = 0;
    UINT4               u4PrevRemTimeMark = 0;
    INT4                i4PrevRemLocalPortNum = 0;
    INT4                i4PrevRemIndex = 0;
    INT4                i4TotNeighCount = 0;
    UINT4               u4Temp = 0;
    UINT4               u4RemDestMacInd = 0;
    UINT4               u4PrevRemDestMacInd = 0;
    tLldpWebNeighborInfo WebNeighInfo;

    MEMSET (&WebNeighInfo, 0, sizeof (tLldpWebNeighborInfo));

    WebnmRegisterLock (pHttp, LldpLock, LldpUnLock);
    LldpLock ();

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    if (nmhGetFirstIndexLldpV2RemTable (&u4RemTimeMark,
                                        &i4RemLocalPortNum,
                                        &u4RemDestMacInd,
                                        (UINT4 *) &i4RemIndex) != SNMP_SUCCESS)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        LldpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }
    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        i4TotNeighCount++;

        /* Shows Brief Info */
        LldpWebShowNeighborBriefInfo (pHttp, u4RemTimeMark,
                                      i4RemLocalPortNum, i4RemIndex,
                                      u4RemDestMacInd);

        u4PrevRemTimeMark = u4RemTimeMark;
        i4PrevRemLocalPortNum = i4RemLocalPortNum;
        i4PrevRemIndex = i4RemIndex;
        u4PrevRemDestMacInd = u4RemDestMacInd;

    }
    while (nmhGetNextIndexLldpV2RemTable (u4PrevRemTimeMark,
                                          &u4RemTimeMark,
                                          i4PrevRemLocalPortNum,
                                          &i4RemLocalPortNum,
                                          u4PrevRemDestMacInd,
                                          &u4RemDestMacInd,
                                          (UINT4) i4PrevRemIndex,
                                          (UINT4 *) &i4RemIndex) ==
           SNMP_SUCCESS);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    LldpUnLock ();
    WebnmUnRegisterLock (pHttp);

}

 /**************************************************************************
 *
 *     FUNCTION NAME    : LldpWebShowNeighborBriefInfo
 *
 *     DESCRIPTION      : Displays Brief information about neighbors 
 *                        learnt on an interface or all interfaces
 *
 *     INPUT            :       
 *                        u4RemTimeMark     - Remote Node Time Mark
 *                        i4RemLocalPortNum - Local port where remote
 *                                            information is received
 *                        i4RemIndex        - Remote Node Index 
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ***************************************************************************/

PUBLIC INT4
LldpWebShowNeighborBriefInfo (tHttp * pHttp, UINT4 u4RemTimeMark,
                              INT4 i4RemLocalPortNum, INT4 i4RemIndex,
                              UINT4 u4RemDestMacInd)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1Id[LLDP_MAX_LEN_CHASSISID + 1];
    UINT1               au1DispId[LLDP_MAX_LEN_ID];
    UINT1              *pc1DispChassIdNwAddr;
    INT1               *pi1IfName = NULL;
    UINT2               u2SysCapSup;
    INT4                i4ChassisIdSubtype = 0;
    INT4                i4PortIdSubtype = 0;
    UINT4               u4RxTTL = 0;
    UINT4               u4ChassIdNwAddr = 0;
    /*Making tLldpRemoteNode as static as it is used only here */
    static tLldpRemoteNode RemNode;
    tLldpRemoteNode    *pRemNode = NULL;
    tSNMP_OCTET_STRING_TYPE Id;
    tSNMP_OCTET_STRING_TYPE SysCapSupported;

    MEMSET (&Id, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&SysCapSupported, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    MEMSET (au1Id, 0, LLDP_MAX_LEN_CHASSISID + 1);

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

    pi1IfName = (INT1 *) au1IfName;
    Id.pu1_OctetList = &au1Id[0];
    SysCapSupported.pu1_OctetList = (UINT1 *) &u2SysCapSup;
    SysCapSupported.i4_Length = sizeof (u2SysCapSup);

    nmhGetLldpV2RemChassisIdSubtype (u4RemTimeMark,
                                     i4RemLocalPortNum, u4RemDestMacInd,
                                     i4RemIndex, &i4ChassisIdSubtype);
    if (nmhGetLldpV2RemChassisId (u4RemTimeMark,
                                  i4RemLocalPortNum, u4RemDestMacInd,
                                  i4RemIndex, &Id) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (i4ChassisIdSubtype == LLDP_CHASS_ID_SUB_MAC_ADDR)
    {
        MEMSET (&au1DispId, 0, sizeof (au1DispId));

        /* Convert octet values into strings of octets seperated by colon */
        CLI_CONVERT_MAC_TO_DOT_STR (Id.pu1_OctetList, &au1DispId[0]);
        /* au1DispId */
        STRCPY (pHttp->au1KeyString, "CHASSIS_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1DispId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    }
    else if (i4ChassisIdSubtype == LLDP_CHASS_ID_SUB_NW_ADDR)
    {
        MEMSET (&au1DispId, 0, sizeof (au1DispId));
        /* To skip the IANA Address Family Numbers */
        Id.pu1_OctetList++;
        PTR_FETCH4 (u4ChassIdNwAddr, Id.pu1_OctetList);
        pc1DispChassIdNwAddr = &au1DispId[0];
        /* Convert octet values into strings of octets seperated by dot */
        WEB_CONVERT_IPADDR_TO_STR (pc1DispChassIdNwAddr, u4ChassIdNwAddr);
        /* pc1DispChassIdNwAddr */
        STRCPY (pHttp->au1KeyString, "CHASSIS_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pc1DispChassIdNwAddr);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    }
    else
    {
        if (STRLEN (Id.pu1_OctetList) >= LLDP_CLI_CHASSIS_DISP_LEN)
        {
            MEMSET (&au1DispId, 0, sizeof (au1DispId));
            STRNCPY (au1DispId, Id.pu1_OctetList, LLDP_CLI_CHASSIS_DISP_LEN);
            STRCAT (au1DispId, "...");
            /* au1DispId */
            STRCPY (pHttp->au1KeyString, "CHASSIS_ID_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1DispId);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        }
        else
        {
            /*  Id.pu1_OctetList */
            STRCPY (pHttp->au1KeyString, "CHASSIS_ID_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", Id.pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        }
    }

    /* Get the pointer to the Remote Node with the given indices */
    RemNode.u4RemLastUpdateTime = u4RemTimeMark;
    RemNode.i4RemLocalPortNum = i4RemLocalPortNum;
    RemNode.i4RemIndex = i4RemIndex;
    RemNode.u4DestAddrTblIndex = u4RemDestMacInd;

    /* No MIB object is present for LLDP Holdtime (u4RxTTL).
     * Hence RBTreeGet routine is used here. */
    pRemNode =
        (tLldpRemoteNode *) RBTreeGet (gLldpGlobalInfo.RemSysDataRBTree,
                                       (tRBElem *) & RemNode);

    LLDP_CHK_NULL_PTR_RET (pRemNode, OSIX_FAILURE);
    u4RxTTL = pRemNode->u4RxTTL;

    CfaCliGetIfName ((UINT4) i4RemLocalPortNum, pi1IfName);

    /*  pi1IfName : LOCAL INTERFACE */

    STRCPY (pHttp->au1KeyString, "LOCAL_INTERFACE_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", (CHR1 *) pi1IfName);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    /* u4RxTTL : HOLD TIME */

    STRCPY (pHttp->au1KeyString, "HOLD_TIME_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RxTTL);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    nmhGetLldpV2RemSysCapSupported (u4RemTimeMark,
                                    i4RemLocalPortNum, u4RemDestMacInd,
                                    i4RemIndex, &SysCapSupported);

    if (SysCapSupported.pu1_OctetList[0] == 0)
    {
        /* Space alignment for system capabilities TLV */

        STRCPY (pHttp->au1KeyString, "CAPABILITY_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "---");
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    }
    else
    {
        /* Shows Supported System Capabilities */
        LldpWebShowSysSupportCapab (pHttp, SysCapSupported);
    }

    MEMSET (&Id, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1Id, 0, LLDP_MAX_LEN_PORTID + 1);
    Id.pu1_OctetList = &au1Id[0];
    nmhGetLldpV2RemPortIdSubtype (u4RemTimeMark,
                                  i4RemLocalPortNum, u4RemDestMacInd,
                                  i4RemIndex, &i4PortIdSubtype);
    if (nmhGetLldpV2RemPortId (u4RemTimeMark,
                               i4RemLocalPortNum, u4RemDestMacInd,
                               i4RemIndex, &Id) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (i4PortIdSubtype == LLDP_PORT_ID_SUB_MAC_ADDR)
    {
        MEMSET (&au1DispId, 0, sizeof (au1DispId));

        /* Convert octet values into strings of octets
         * seperated by colon */
        CLI_CONVERT_MAC_TO_DOT_STR (Id.pu1_OctetList, &au1DispId[0]);

        /* au1DispId : PORT_ID_SUB_MAC */

        STRCPY (pHttp->au1KeyString, "PORT_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1DispId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    }
    else if (STRLEN (Id.pu1_OctetList) >= LLDP_CLI_PORT_DISP_LEN)
    {
        MEMSET (&au1DispId, 0, sizeof (au1DispId));
        STRNCPY (au1DispId, Id.pu1_OctetList, LLDP_CLI_PORT_DISP_LEN);

        /* au1DispId */
        STRCPY (pHttp->au1KeyString, "PORT_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1DispId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    }
    else
    {
        /*  Id.pu1_OctetList */
        STRCPY (pHttp->au1KeyString, "PORT_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", Id.pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    }

    return OSIX_SUCCESS;
}

 /**************************************************************************
 *
 *     FUNCTION NAME    : LldpWebShowSysSupportCapab
 *
 *     DESCRIPTION      : This function will display the Supported
 *                        LLDP System Capabilities
 *
 *     INPUT            : 
 *                        SysCapSupported  - System Capabilities Supported
 *                                           Octet String
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 *
 ***************************************************************************/

VOID
LldpWebShowSysSupportCapab (tHttp * pHttp,
                            tSNMP_OCTET_STRING_TYPE SysCapSupported)
{

    UINT1               au1SysCapabSupported[ISS_SYS_CAPABILITIES_LEN];
    UINT1               au1DispSysCapabSupported[LLDP_CLI_SYS_CAPAB_MAX_BITS *
                                                 2];
    UINT1               u1Count = 0;
    UINT1               u1TempCount = 0;
    CHR1                ac1Comma[] = { ',' };
    CHR1                ac1Space[] = { ' ' };
    BOOL1               bResult = OSIX_FALSE;
    const CHR1          ac1WebSysCap[] =
        { 'O', 'P', 'B', 'W', 'R', 'T', 'C', 'S' };

    MEMSET (au1SysCapabSupported, 0, sizeof (au1SysCapabSupported));
    MEMSET (au1DispSysCapabSupported, 0, sizeof (au1DispSysCapabSupported));

    MEMCPY (&au1SysCapabSupported, SysCapSupported.pu1_OctetList,
            SysCapSupported.i4_Length);

    for (u1Count = 1; u1Count <= LLDP_CLI_SYS_CAPAB_MAX_BITS; u1Count++)
    {
        /* Reset flag */
        bResult = OSIX_FALSE;
        OSIX_BITLIST_IS_BIT_SET (au1SysCapabSupported, u1Count,
                                 ISS_SYS_CAPABILITIES_LEN, bResult);
        if (bResult == OSIX_TRUE)
        {
            au1DispSysCapabSupported[u1TempCount] =
                (UINT1) ac1WebSysCap[u1Count - 1];
            /* Putting comma(,) after every system capability display */
            au1DispSysCapabSupported[u1TempCount + 1] = (UINT1) ac1Comma[0];
            u1TempCount = (UINT1) (u1TempCount + 2);
        }
    }
    if (u1TempCount != 0)
    {
        /* Replacing the last comma(,) with space */
        au1DispSysCapabSupported[u1TempCount - 1] = (UINT1) ac1Space[0];
    }
    STRCPY (pHttp->au1KeyString, "CAPABILITY_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1DispSysCapabSupported);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    return;
}

/*********************************************************************
 *  Function Name : ISSProcessLldpAgentPage
 *  Description   : This function processes the request coming for the
 *                  LLDP Global conf page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None.
 *********************************************************************/

VOID
ISSProcessLldpAgentPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessGet (pHttp);
    }
    if (pHttp->i4Method == ISS_SET)
    {
        ISSProcessLldpAgentPageSet (pHttp);
    }
    return;
}

 /**************************************************************************
 *
 *     FUNCTION NAME    : ISSProcessLldpAgentPageSet
 *
 *     DESCRIPTION      : This function will set the LLDP global
 *                        information
 *
 *     INPUT            : pHttp - Pointer to the global HTTP data structure
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 *
 **************************************************************************/
VOID
ISSProcessLldpAgentPageSet (tHttp * pHttp)
{
    UINT4               u4InterfaceIndex = 0;
    tMacAddr            DestMacAddr;

    WebnmRegisterLock (pHttp, LldpLock, LldpUnLock);
    LldpLock ();

    STRCPY (pHttp->au1Name, "INTERFACE_ID");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
    {
        u4InterfaceIndex = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "MAC_ADDRESS");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        StrToMac (pHttp->au1Value, DestMacAddr);
    }

    if (LldpWebSetAgent ((INT4) u4InterfaceIndex, DestMacAddr, CREATE_AND_GO)
        == OSIX_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "\rUnable to Create the Agent\r\n");
    }

    LldpUnLock ();
    WebnmUnRegisterLock (pHttp);

    IssProcessGet (pHttp);
}

 /**************************************************************************
 *
 *     FUNCTION NAME    : LldpWebSetAgent
 *
 *     DESCRIPTION      : This function will create the Agent Information
 *
 *     INPUT            : pHttp - Pointer to the global HTTP data structure
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 *
 **************************************************************************/
PUBLIC INT4
LldpWebSetAgent (INT4 i4IfIndex, UINT1 *pu1MacAddr, UINT1 u1RowStatus)
{

    UINT4               u4ErrorCode = 0;
    UINT4               u4DestIndex = 0;
    tLldpv2DestAddrTbl  DestAddrTbl;
    tLldpv2DestAddrTbl *pDestAddrTbl = NULL;
    tLldpv2AgentToLocPort *pLldpAgentPortInfo = NULL;

    MEMSET (&DestAddrTbl, 0, sizeof (tLldpv2DestAddrTbl));

    if (gLldpGlobalInfo.i4Version == LLDP_VERSION_05)
    {
        if (u1RowStatus == DESTROY)
        {
            MEMCPY (pu1MacAddr, gau1LldpMcastAddr, sizeof (tMacAddr));
        }
        if (nmhTestv2FsLldpLocPortDstMac (&u4ErrorCode, i4IfIndex, pu1MacAddr)
            == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }

        if (nmhSetFsLldpLocPortDstMac (i4IfIndex, pu1MacAddr) == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
    else
    {

        pDestAddrTbl = (tLldpv2DestAddrTbl *)
            RBTreeGetFirst (gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree);

        while (pDestAddrTbl != NULL)
        {
            if (MEMCMP (pDestAddrTbl->Lldpv2DestMacAddress,
                        pu1MacAddr, MAC_ADDR_LEN) == 0)
            {
                u4DestIndex = pDestAddrTbl->u4LlldpV2DestAddrTblIndex;
                break;
            }
            DestAddrTbl.u4LlldpV2DestAddrTblIndex =
                pDestAddrTbl->u4LlldpV2DestAddrTblIndex;
            pDestAddrTbl = (tLldpv2DestAddrTbl *)
                RBTreeGetNext (gLldpGlobalInfo.Lldpv2DestMacAddrTblRBTree,
                               &DestAddrTbl, LldpDstMacAddrUtlRBCmpInfo);
        }

        pLldpAgentPortInfo =
            LldpGetPortMapTableNode ((UINT4) i4IfIndex,
                                     (tMacAddr *) pu1MacAddr);

        if ((pLldpAgentPortInfo != NULL) && (u4DestIndex != 0))
        {
            if (u1RowStatus == CREATE_AND_GO)
            {
                /*Entry is already present just need to Make active. */
                u1RowStatus = ACTIVE;
            }

            if (nmhTestv2Fslldpv2ConfigPortRowStatus (&u4ErrorCode, i4IfIndex,
                                                      pu1MacAddr,
                                                      u1RowStatus) ==
                SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }

            if (nmhSetFslldpv2ConfigPortRowStatus (i4IfIndex, pu1MacAddr,
                                                   u1RowStatus) == SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }
            if (nmhTestv2Fslldpv2DestRowStatus (&u4ErrorCode, u4DestIndex,
                                                u1RowStatus) == SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }
            if (nmhSetFslldpv2DestRowStatus (u4DestIndex, u1RowStatus) ==
                SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }
            return OSIX_SUCCESS;
        }

        if ((u4DestIndex == 0) && (u1RowStatus == CREATE_AND_GO))
        {
            u4DestIndex = gu4LldpDestMacAddrTblIndex;
            /*New Entry Need to Create and then make as active. */
            u1RowStatus = CREATE_AND_WAIT;
        }
        /*For LLDP_DESTROY with u4DestIndex equal to zero will fail below. */
        if (nmhTestv2Fslldpv2DestRowStatus (&u4ErrorCode, u4DestIndex,
                                            (INT4) u1RowStatus) == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }
        if (nmhSetFslldpv2DestRowStatus (u4DestIndex, u1RowStatus) ==
            SNMP_FAILURE)
        {
            return OSIX_FAILURE;

        }
        if (nmhTestv2FslldpV2DestMacAddress (&u4ErrorCode, u4DestIndex,
                                             pu1MacAddr) == SNMP_FAILURE)
        {
            nmhSetFslldpv2DestRowStatus (u4DestIndex, DESTROY);
            gu4LldpDestMacAddrTblIndex--;
            return OSIX_FAILURE;
        }

        if (nmhSetFslldpV2DestMacAddress (u4DestIndex, pu1MacAddr) ==
            SNMP_FAILURE)
        {
            nmhSetFslldpv2DestRowStatus (u4DestIndex, DESTROY);
            gu4LldpDestMacAddrTblIndex--;
            return OSIX_FAILURE;
        }
        if (nmhTestv2Fslldpv2ConfigPortRowStatus (&u4ErrorCode, i4IfIndex,
                                                  pu1MacAddr,
                                                  (INT4) u1RowStatus) ==
            SNMP_FAILURE)
        {
            nmhSetFslldpv2DestRowStatus (u4DestIndex, DESTROY);
            gu4LldpDestMacAddrTblIndex--;
            return OSIX_FAILURE;
        }

        if (nmhSetFslldpv2ConfigPortRowStatus (i4IfIndex, pu1MacAddr,
                                               (INT4) u1RowStatus) ==
            SNMP_FAILURE)
        {
            nmhSetFslldpv2DestRowStatus (u4DestIndex, DESTROY);
            gu4LldpDestMacAddrTblIndex--;
            return OSIX_FAILURE;
        }
        if (nmhTestv2FsLldpLocPortDstMac (&u4ErrorCode, i4IfIndex, pu1MacAddr)
            == SNMP_FAILURE)
        {
            nmhSetFslldpv2DestRowStatus (u4DestIndex, DESTROY);
            nmhSetFslldpv2ConfigPortRowStatus (i4IfIndex, pu1MacAddr, DESTROY);
            gu4LldpDestMacAddrTblIndex--;
            return OSIX_FAILURE;
        }

        if (nmhSetFsLldpLocPortDstMac (i4IfIndex, pu1MacAddr) == SNMP_FAILURE)
        {
            nmhSetFslldpv2DestRowStatus (u4DestIndex, DESTROY);
            nmhSetFslldpv2ConfigPortRowStatus (i4IfIndex, pu1MacAddr, DESTROY);
            gu4LldpDestMacAddrTblIndex--;
            return OSIX_FAILURE;
        }

        if (nmhTestv2Fslldpv2DestRowStatus (&u4ErrorCode, u4DestIndex,
                                            ACTIVE) == SNMP_FAILURE)
        {
            nmhSetFslldpv2DestRowStatus (u4DestIndex, DESTROY);
            nmhSetFslldpv2ConfigPortRowStatus (i4IfIndex, pu1MacAddr, DESTROY);
            gu4LldpDestMacAddrTblIndex--;

            return OSIX_FAILURE;
        }
        if (nmhSetFslldpv2DestRowStatus (u4DestIndex, ACTIVE) == SNMP_FAILURE)
        {
            nmhSetFslldpv2DestRowStatus (u4DestIndex, DESTROY);
            nmhSetFslldpv2ConfigPortRowStatus (i4IfIndex, pu1MacAddr, DESTROY);
            gu4LldpDestMacAddrTblIndex--;
            return OSIX_FAILURE;

        }
        if (nmhTestv2Fslldpv2ConfigPortRowStatus (&u4ErrorCode, i4IfIndex,
                                                  pu1MacAddr,
                                                  ACTIVE) == SNMP_FAILURE)
        {
            nmhSetFslldpv2DestRowStatus (u4DestIndex, DESTROY);
            nmhSetFslldpv2ConfigPortRowStatus (i4IfIndex, pu1MacAddr, DESTROY);
            gu4LldpDestMacAddrTblIndex--;
            return OSIX_FAILURE;
        }

        if (nmhSetFslldpv2ConfigPortRowStatus (i4IfIndex, pu1MacAddr,
                                               ACTIVE) == SNMP_FAILURE)
        {
            nmhSetFslldpv2DestRowStatus (u4DestIndex, DESTROY);
            nmhSetFslldpv2ConfigPortRowStatus (i4IfIndex, pu1MacAddr, DESTROY);
            gu4LldpDestMacAddrTblIndex--;
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;

}

/*********************************************************************
 *  Function Name : ISSProcessLldpAgentDetailsPage
 *  Description   : This function processes the request coming for the
 *                  LLDP Agent Details page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None.
 *********************************************************************/

VOID
ISSProcessLldpAgentDetailsPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessLldpAgentDetailsPageGet (pHttp);
    }
    if (pHttp->i4Method == ISS_SET)
    {
        IssProcessLldpAgentDetailsPageSet (pHttp);
    }
    return;
}

 /**************************************************************************
 *
 *     FUNCTION NAME    : ISSProcessLldpAgentDetailsPageSet
 *
 *     DESCRIPTION      : This function will set the LLDP agent
 *                        information
 *
 *     INPUT            : pHttp - Pointer to the global HTTP data structure
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 *
 **************************************************************************/
VOID
IssProcessLldpAgentDetailsPageSet (tHttp * pHttp)
{
    tMacAddr            DestMacAddr;
    tSNMP_OCTET_STRING_TYPE LldpTlvEnabled;
    tSNMP_OCTET_STRING_TYPE MgmtAddr;
    tSNMP_OCTET_STRING_TYPE LldpDot3TlvEnabled;
    tSNMP_OCTET_STRING_TYPE LldpDot3GetTlvEnabled;
    tSNMP_OCTET_STRING_TYPE PortListEnable;
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;
    tIp6Addr           *pTempIp6Addr = NULL;
    INT4                i4PrevLocVlanId = 0;
    INT4                i4TxStatusVal = 0;
    INT4                i4LocVlanId = 0;
    INT4                i4Val = 0;
    INT4                i4Status = 0;
    INT4                i4VlanStatus = 0;
    INT4                i4MgmtAddrSubType = 3;
    UINT4               u4ErrorCode = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4InterfaceIndex = 0;
    UINT4               u4DestMacAddrIndex = 0;
    UINT4               u4LocPortNum = 0;
    INT4                i4LocPortNum = 0;
    INT4                i4MgmtAddrStatus = 0;
    INT4                i4PrevLocPortNum = 0;
    INT4                i4LocProtoVlanId = 0;
    INT4                i4PrevLocProtoVlanId = 0;
    INT4                i4RetVal = 0;
    UINT1               au1MgmtAddr[IPVX_IPV6_ADDR_LEN] = { 0 };
    UINT1               u1Tlv = 0;
    UINT1               u1Dot3Tlv = 0;
    UINT1               u1ProtoVlanAll = 0;
    UINT1               u1Dot3TlvEnabled = 0;
    UINT1               u1DelFlag = FALSE;
    UINT1               au1ManAddr[LLDP_MAX_LEN_MAN_ADDR];
    UINT1               au1VlanName[VLAN_STATIC_MAX_NAME_LEN] = { 0 };
    UINT1               au1PortList[LLDP_MAN_ADDR_TX_EN_MAX_BYTES] = { 0 };

    tSNMP_OCTET_STRING_TYPE ManAddr;

    MEMSET (au1ManAddr, 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMSET (&ManAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    ManAddr.pu1_OctetList = au1ManAddr;

    MEMSET (DestMacAddr, 0, MAC_ADDR_LEN);
    MEMSET (&MgmtAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&LldpTlvEnabled, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&LldpDot3TlvEnabled, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&LldpDot3GetTlvEnabled, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PortListEnable, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    MgmtAddr.pu1_OctetList = au1MgmtAddr;
    PortListEnable.pu1_OctetList = au1PortList;
    PortListEnable.i4_Length = LLDP_MAN_ADDR_TX_EN_MAX_BYTES;

    WebnmRegisterLock (pHttp, LldpLock, LldpUnLock);
    LldpLock ();
    STRCPY (pHttp->au1Name, "ACTION");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
    {
        if (STRCMP (pHttp->au1Value, "Delete") == 0)
        {
            u1DelFlag = TRUE;
        }
    }

    if (u1DelFlag == TRUE)
    {
        STRCPY (pHttp->au1Name, "LLDP_IF_INDEX");
        if (HttpGetValuebyName (pHttp->au1Name,
                                pHttp->au1Value, pHttp->au1PostQuery)
            == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            u4InterfaceIndex = ATOI (pHttp->au1Value);
        }

        STRCPY (pHttp->au1Name, "DEST_MAC_ADDRESS");
        if (HttpGetValuebyName (pHttp->au1Name,
                                pHttp->au1Value, pHttp->au1PostQuery)
            == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            StrToMac (pHttp->au1Value, DestMacAddr);
        }

        if (LldpWebSetAgent ((INT4) u4InterfaceIndex, DestMacAddr, DESTROY)
            == OSIX_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "\rUnable to DELETE the Agent\r\n");
            LldpUnLock ();
            WebnmUnRegisterLock (pHttp);
            /*Display the details below */
            IssProcessLldpAgentDetailsPageGet (pHttp);
            return;
        }
        else
        {
            LldpUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessLldpAgentDetailsPageGet (pHttp);
            return;
        }
    }

    STRCPY (pHttp->au1Name, "INTERFACE_ID");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
    {
        u4InterfaceIndex = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "MAC_ADDRESS");
    if (HttpGetValuebyName (pHttp->au1Name,
                            pHttp->au1Value, pHttp->au1PostQuery)
        == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        StrToMac (pHttp->au1Value, DestMacAddr);
    }

    if (nmhValidateIndexInstanceFslldpv2ConfigPortMapTable
        ((INT4) u4InterfaceIndex, DestMacAddr) == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "\rSpecified Agent is not created in the system\r\n");
    }

    LldpGetDestAddrTblIndexFromMacAddr (&DestMacAddr, &u4DestMacAddrIndex);

    /* set the port description tlv status */

    STRCPY (pHttp->au1Name, "PORT_DESCR_TLV");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Status = ATOI (pHttp->au1Value);
    }
    if (i4Status == LLDP_TRUE)
    {
        u1Tlv |= (UINT1) LLDP_PORT_DESC_TLV_ENABLED;
    }
    STRCPY (pHttp->au1Name, "SYS_NAME_TLV");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Status = ATOI (pHttp->au1Value);
    }
    if (i4Status == LLDP_TRUE)
    {
        u1Tlv |= (UINT1) LLDP_SYS_NAME_TLV_ENABLED;
    }
    STRCPY (pHttp->au1Name, "SYS_DESCR_TLV");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Status = ATOI (pHttp->au1Value);
    }
    if (i4Status == LLDP_TRUE)
    {
        u1Tlv |= (UINT1) LLDP_SYS_DESC_TLV_ENABLED;
    }
    STRCPY (pHttp->au1Name, "SYS_CAPAB_TLV");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Status = ATOI (pHttp->au1Value);
    }
    if (i4Status == LLDP_TRUE)
    {
        u1Tlv |= (UINT1) LLDP_SYS_CAPAB_TLV_ENABLED;
    }
    STRCPY (pHttp->au1Name, "MGMT_ADDR_TLV");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Status = ATOI (pHttp->au1Value);
        i4Val = i4Status;
        i4MgmtAddrStatus = i4Status;
    }
    if (i4Status == LLDP_TRUE)
    {
        u1Tlv |= (UINT1) LLDP_MAN_ADDR_TLV_ENABLED;
    }
    STRCPY (pHttp->au1Name, "MGMT_ADDR_TLV_TYPE");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Status = ATOI (pHttp->au1Value);
    }
    i4MgmtAddrSubType = i4Status;
    if (i4MgmtAddrStatus == LLDP_TRUE)
    {
        STRCPY (pHttp->au1Name, "MGMT_ADDR_IP");

        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            if (i4MgmtAddrSubType == IPVX_ADDR_FMLY_IPV4)
            {
                u4IpAddr = INET_ADDR ((CHR1 *) pHttp->au1Value);

            }
            else
            {
                MEMCPY (au1MgmtAddr, pHttp->au1Value, IPVX_IPV6_ADDR_LEN);
            }

        }

        if (i4MgmtAddrSubType != 3)
        {
            if (i4MgmtAddrSubType == IPVX_ADDR_FMLY_IPV4)
            {
                MEMCPY (MgmtAddr.pu1_OctetList, &u4IpAddr, IPVX_IPV4_ADDR_LEN);
                MgmtAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
            }
            if (i4MgmtAddrSubType == IPVX_ADDR_FMLY_IPV6)
            {
                MgmtAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
                pTempIp6Addr = str_to_ip6addr (au1MgmtAddr);
                if (pTempIp6Addr != NULL)
                {
                    MEMCPY (MgmtAddr.pu1_OctetList, (UINT1 *) pTempIp6Addr,
                            MgmtAddr.i4_Length);
                }
                else
                {
                    IssSendError (pHttp, (CONST INT1 *)
                                  "%%pTempIp6Addr is NULL\r\n");
                    LldpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    return;
                }
            }
            if (nmhGetLldpConfigManAddrPortsTxEnable (i4MgmtAddrSubType,
                                                      &MgmtAddr,
                                                      &PortListEnable) !=
                SNMP_SUCCESS)
            {
                LldpUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "%%nmhGetLldpConfigManAddrPortsTxEnable returns failure\r\n");
                return;
            }
            if (i4Val == LLDP_TRUE)
            {
                OSIX_BITLIST_SET_BIT (PortListEnable.pu1_OctetList,
                                      (INT4) u4InterfaceIndex,
                                      MEM_MAX_BYTES (PortListEnable.i4_Length,
                                                     LLDP_MAN_ADDR_TX_EN_MAX_BYTES));
            }
            else
            {

                OSIX_BITLIST_RESET_BIT (PortListEnable.pu1_OctetList,
                                        (INT4) u4InterfaceIndex,
                                        MEM_MAX_BYTES (PortListEnable.i4_Length,
                                                       LLDP_MAN_ADDR_TX_EN_MAX_BYTES));
            }

            if (nmhTestv2LldpConfigManAddrPortsTxEnable (&u4ErrorCode,
                                                         i4MgmtAddrSubType,
                                                         &MgmtAddr,
                                                         &PortListEnable) !=
                SNMP_SUCCESS)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "%%Unable to set LldpConfigManAddrPortsTxEnable\r\n");
                LldpUnLock ();
                WebnmUnRegisterLock (pHttp);
                return;
            }
            if (nmhSetLldpConfigManAddrPortsTxEnable (i4MgmtAddrSubType,
                                                      &MgmtAddr,
                                                      &PortListEnable) ==
                SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "%%Unable to set LldpConfigManAddrPortsTxEnable\r\n");
                LldpUnLock ();
                WebnmUnRegisterLock (pHttp);
                return;
            }

        }
    }

    LldpTlvEnabled.pu1_OctetList = &u1Tlv;
    LldpTlvEnabled.i4_Length = sizeof (UINT1);

    if (nmhSetLldpV2PortConfigTLVsTxEnable
        ((INT4) u4InterfaceIndex, u4DestMacAddrIndex,
         &LldpTlvEnabled) == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "\rnmhSetLldpV2PortConfigTLVsTxEnable returned Failure\r\n");
        LldpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    /* Set Dot1 TLVs */

    u1Tlv = 0;
    i4Status = 0;

    /*Port VLAN ID TLV */
    STRCPY (pHttp->au1Name, "PORT_VLAN_TLV");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Status = ATOI (pHttp->au1Value);
    }

    if (i4Status == LLDP_TRUE)
    {
        i4Val = LLDP_TRUE;
    }
    else
    {
        i4Val = LLDP_FALSE;
    }

    if (nmhTestv2LldpV2Xdot1ConfigPortVlanTxEnable
        (&u4ErrorCode, (INT4) u4InterfaceIndex, u4DestMacAddrIndex,
         i4Val) == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "\r\n%%Unable to set PortVlanTLVTxEnable\r\n");
        LldpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }
    if (nmhSetLldpV2Xdot1ConfigPortVlanTxEnable
        ((INT4) u4InterfaceIndex, u4DestMacAddrIndex, i4Val) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "\r\n%%Unable to set PortVlanTLVTxEnable\r\n");
        LldpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    /*Proto VLAN ID TLV */
    STRCPY (pHttp->au1Name, "PROTO_VLAN_TLV");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Status = ATOI (pHttp->au1Value);
    }

    if (i4Status == LLDP_TRUE)
    {
        i4Val = LLDP_TRUE;
        i4VlanStatus = LLDP_TRUE;
    }
    else
    {
        i4Val = LLDP_FALSE;
    }

    STRCPY (pHttp->au1Name, "PROTO_VLAN_ID_ALL");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Status = ATOI (pHttp->au1Value);
    }
    if (i4Status == LLDP_TRUE)
    {
        u1ProtoVlanAll = LLDP_TRUE;
    }
    else
    {
        u1ProtoVlanAll = LLDP_FALSE;
    }

    if (u1ProtoVlanAll == LLDP_TRUE)
    {
        gu1TxAll = OSIX_TRUE;

        if (nmhGetFirstIndexLldpXdot1ConfigProtoVlanTable (&i4LocPortNum,
                                                           &i4LocProtoVlanId)
            != SNMP_SUCCESS)
        {
            LldpUnLock ();
            WebnmUnRegisterLock (pHttp);
            return;
        }
        do
        {
            if ((INT4) u4InterfaceIndex == i4LocPortNum)
            {
                if (nmhTestv2LldpXdot1ConfigProtoVlanTxEnable (&u4ErrorCode,
                                                               i4LocPortNum,
                                                               i4LocProtoVlanId,
                                                               i4Val)
                    != SNMP_SUCCESS)
                {
                    LldpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    return;
                }
                if (nmhSetLldpXdot1ConfigProtoVlanTxEnable
                    ((INT4) u4InterfaceIndex, i4LocProtoVlanId,
                     i4Val) != SNMP_SUCCESS)
                {
                    LldpUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    return;
                }
            }
            i4PrevLocPortNum = i4LocPortNum;
            i4PrevLocProtoVlanId = i4LocProtoVlanId;
        }
        while (nmhGetNextIndexLldpXdot1ConfigProtoVlanTable (i4PrevLocPortNum,
                                                             &i4LocPortNum,
                                                             i4PrevLocProtoVlanId,
                                                             &i4LocProtoVlanId)
               == SNMP_SUCCESS);
        i4RetVal = LldpUtlGetLocalPort ((INT4) u4InterfaceIndex, DestMacAddr,
                                        &u4LocPortNum);
        UNUSED_PARAM (i4RetVal);
        pLldpLocPortInfo = LLDP_GET_LOC_PORT_INFO (u4LocPortNum);
        if (pLldpLocPortInfo != NULL)
        {
            if (LldpTxUtlHandleLocPortInfoChg (pLldpLocPortInfo)
                != OSIX_SUCCESS)
            {
                LLDP_TRC (ALL_FAILURE_TRC,
                          "LldpTxUtlHandleLocPortInfoChg: "
                          "returns FAILURE \r\n");
            }
        }
        gu1TxAll = OSIX_FALSE;

    }
    else
    {
        STRCPY (pHttp->au1Name, "PROTO_VLAN_ID");

        if (i4VlanStatus == LLDP_TRUE)
        {
            if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery) == ENM_SUCCESS)
            {
                issDecodeSpecialChar (pHttp->au1Value);
                i4Status = ATOI (pHttp->au1Value);
            }
            if (nmhTestv2LldpV2Xdot1ConfigProtoVlanTxEnable
                (&u4ErrorCode, (INT4) u4InterfaceIndex, i4Status,
                 i4Val) == SNMP_FAILURE)

            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "\r\n%%Unable to set ProtoVlanTLVTxEnable\r\n");
                LldpUnLock ();
                WebnmUnRegisterLock (pHttp);
                return;
            }
            if (nmhSetLldpV2Xdot1ConfigProtoVlanTxEnable
                ((INT4) u4InterfaceIndex, i4Status, i4Val) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "\r\n%%Unable to set PortVlanTLVTxEnable\r\n");
                LldpUnLock ();
                WebnmUnRegisterLock (pHttp);
                return;
            }
        }
    }

    /*VLAN NAME TLV */
    STRCPY (pHttp->au1Name, "VLAN_NAME_TLV");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Status = ATOI (pHttp->au1Value);
    }

    if (i4Status == LLDP_TRUE)
    {
        i4Val = LLDP_TRUE;
    }
    else
    {
        i4Val = LLDP_FALSE;
    }
    if (i4Val == LLDP_TRUE)
    {
        STRCPY (pHttp->au1Name, "VLAN_NAME_ALL");

        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            issDecodeSpecialChar (pHttp->au1Value);
            i4Status = ATOI (pHttp->au1Value);
        }
        if (i4Status == LLDP_TRUE)
        {
            u1ProtoVlanAll = LLDP_TRUE;
        }
        else
        {
            u1ProtoVlanAll = LLDP_FALSE;
        }

        if (u1ProtoVlanAll == LLDP_TRUE)
        {
            gu1TxAll = OSIX_TRUE;

            /* nmhSet routine needs the VlanId value as Index, so
             * Written utilty function to get vlanId and it calls
             * nhmSet routine to set the value */
            if (nmhGetFirstIndexLldpXdot1ConfigVlanNameTable (&i4LocPortNum,
                                                              &i4LocVlanId)
                != SNMP_SUCCESS)
            {
                LldpUnLock ();
                WebnmUnRegisterLock (pHttp);
                return;
            }
            do
            {
                if ((INT4) u4InterfaceIndex == i4LocPortNum)
                {
                    MEMSET (&au1VlanName[0], 0, VLAN_STATIC_MAX_NAME_LEN);
                    if (LldpVlndbGetVlanName ((UINT4) i4LocPortNum,
                                              (UINT2) i4LocVlanId,
                                              &au1VlanName[0]) == OSIX_FAILURE)
                    {
                        LldpUnLock ();
                        WebnmUnRegisterLock (pHttp);
                        return;
                    }
                    if (STRLEN (au1VlanName) != 0)
                    {
                        if (nmhTestv2LldpXdot1ConfigVlanNameTxEnable
                            (&u4ErrorCode, (INT4) u4InterfaceIndex, i4LocVlanId,
                             i4TxStatusVal) != SNMP_SUCCESS)
                        {
                            LldpUnLock ();
                            WebnmUnRegisterLock (pHttp);
                            return;
                        }
                        if (nmhSetLldpXdot1ConfigVlanNameTxEnable
                            ((INT4) u4InterfaceIndex, i4LocVlanId,
                             i4TxStatusVal) != SNMP_SUCCESS)
                        {
                            LldpUnLock ();
                            WebnmUnRegisterLock (pHttp);
                            return;
                        }
                    }
                }
                i4PrevLocPortNum = i4LocPortNum;
                i4PrevLocVlanId = i4LocVlanId;
            }
            while (nmhGetNextIndexLldpXdot1ConfigVlanNameTable
                   (i4PrevLocPortNum, &i4LocPortNum, i4PrevLocVlanId,
                    &i4LocVlanId) == SNMP_SUCCESS);

            i4RetVal =
                LldpUtlGetLocalPort ((INT4) u4InterfaceIndex, DestMacAddr,
                                     &u4LocPortNum);
            UNUSED_PARAM (i4RetVal);
            pLldpLocPortInfo = LLDP_GET_LOC_PORT_INFO (u4LocPortNum);
            if (pLldpLocPortInfo != NULL)
            {
                if (LldpTxUtlHandleLocPortInfoChg (pLldpLocPortInfo)
                    != OSIX_SUCCESS)
                {
                    LLDP_TRC (ALL_FAILURE_TRC,
                              "LldpTxUtlHandleLocPortInfoChg: "
                              "returns FAILURE \r\n");
                }
            }
            gu1TxAll = OSIX_FALSE;

        }
        else
        {
            STRCPY (pHttp->au1Name, "PROTO_VLAN_NAME");

            if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery) == ENM_SUCCESS)
            {
                issDecodeSpecialChar (pHttp->au1Value);
                i4Status = ATOI (pHttp->au1Value);
            }
            if (nmhTestv2LldpV2Xdot1ConfigVlanNameTxEnable
                (&u4ErrorCode, (INT4) u4InterfaceIndex, i4Status,
                 i4Val) == SNMP_FAILURE)

            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "\r\n%%Unable to set VlanNameTLVTxEnable\r\n");
                LldpUnLock ();
                WebnmUnRegisterLock (pHttp);
                return;
            }
            if (nmhSetLldpV2Xdot1ConfigVlanNameTxEnable
                ((INT4) u4InterfaceIndex, i4Status, i4Val) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "\r\n%%Unable to set VlanNameTLVTxEnable\r\n");
                LldpUnLock ();
                WebnmUnRegisterLock (pHttp);
                return;
            }
        }
    }
    /* VID usage Digest TLV */
    STRCPY (pHttp->au1Name, "VID_DIGEST_TLV");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Status = ATOI (pHttp->au1Value);
    }

    if (i4Status == LLDP_TRUE)
    {
        i4Val = LLDP_TRUE;
    }
    else
    {
        i4Val = LLDP_FALSE;
    }
    if (nmhTestv2LldpV2Xdot1ConfigVidUsageDigestTxEnable
        (&u4ErrorCode, (INT4) u4InterfaceIndex, i4Val) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "\r\n%%Unable to set UsageDigestTxEnable\r\n");
        LldpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }
    if (nmhSetLldpV2Xdot1ConfigVidUsageDigestTxEnable
        ((INT4) u4InterfaceIndex, i4Val) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "\r\n%%Unable to set UsageDigestTxEnable\r\n");
        LldpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    /* VID usage Digest TLV */
    STRCPY (pHttp->au1Name, "MGMT_VID_TLV");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Status = ATOI (pHttp->au1Value);
    }

    if (i4Status == LLDP_TRUE)
    {
        i4Val = LLDP_TRUE;
    }
    else
    {
        i4Val = LLDP_FALSE;
    }
    if (nmhTestv2LldpV2Xdot1ConfigManVidTxEnable
        (&u4ErrorCode, (INT4) u4InterfaceIndex, i4Val) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "\r\n%%Unable to set MgmtVidTxEnable\r\n");
        LldpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }
    if (nmhSetLldpV2Xdot1ConfigManVidTxEnable ((INT4) u4InterfaceIndex, i4Val)
        == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "\r\n%%Unable to set MgmtVidTxEnable\r\n");
        LldpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    /* Set Dot3 TLVs */
    u1Tlv = 0;
    i4Status = 0;
    STRCPY (pHttp->au1Name, "LINK_AGG_TLV");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Status = ATOI (pHttp->au1Value);
    }

    if (i4Status == LLDP_TRUE)
    {
        i4Val = LLDP_TRUE;
    }
    else
    {
        i4Val = LLDP_FALSE;
    }

    u1Dot3Tlv = LLDP_LINK_AGG_TLV_ENABLED;
    LldpDot3TlvEnabled.pu1_OctetList = &u1Dot3Tlv;
    LldpDot3TlvEnabled.i4_Length = sizeof (UINT1);
    LldpDot3GetTlvEnabled.pu1_OctetList = &u1Dot3TlvEnabled;

    if (nmhTestv2LldpXdot3PortConfigTLVsTxEnable (&u4ErrorCode,
                                                  (INT4) u4InterfaceIndex,
                                                  &LldpDot3TlvEnabled)
        == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "\r\n%%Unable to set PortConfigDot3TLVsTxEnable\r\n");
        LldpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }
    if (nmhGetLldpXdot3PortConfigTLVsTxEnable ((INT4) u4InterfaceIndex,
                                               &LldpDot3GetTlvEnabled) ==
        SNMP_FAILURE)
    {
        LldpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }
    /* To Set the Value */
    if (i4Val == OSIX_TRUE)
    {
        u1Dot3Tlv |= *(LldpDot3GetTlvEnabled.pu1_OctetList);
    }
    else
    {
        /* To Reset the value */
        u1Dot3Tlv = (UINT1) ~u1Dot3Tlv;
        u1Dot3Tlv &= *(LldpDot3GetTlvEnabled.pu1_OctetList);
    }
    STRCPY (pHttp->au1Name, "MAC_PHY_TLV");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Status = ATOI (pHttp->au1Value);
    }
    u1Tlv = LLDP_MAC_PHY_TLV_ENABLED;
    u1Dot3Tlv |= u1Tlv;
    if (i4Status == LLDP_TRUE)
    {
        i4Val = LLDP_TRUE;
        u1Dot3Tlv |= *(LldpDot3GetTlvEnabled.pu1_OctetList);
    }
    else
    {
        i4Val = LLDP_FALSE;
        /* To Reset the value */
        u1Dot3Tlv = (UINT1) ~u1Dot3Tlv;
        u1Dot3Tlv &= *(LldpDot3GetTlvEnabled.pu1_OctetList);
    }
    STRCPY (pHttp->au1Name, "MAX_FRAMESIZE_TLV");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4Status = ATOI (pHttp->au1Value);
    }

    u1Tlv = LLDP_MAX_FRAME_SIZE_TLV_ENABLED;
    u1Dot3Tlv |= u1Tlv;
    if (i4Status == LLDP_TRUE)
    {
        i4Val = LLDP_TRUE;
        u1Dot3Tlv |= *(LldpDot3GetTlvEnabled.pu1_OctetList);
    }
    else
    {
        i4Val = LLDP_FALSE;
        /* To Reset the value */
        u1Dot3Tlv = (UINT1) ~u1Dot3Tlv;
        u1Dot3Tlv &= *(LldpDot3GetTlvEnabled.pu1_OctetList);
    }
    if (nmhSetLldpXdot3PortConfigTLVsTxEnable
        ((INT4) u4InterfaceIndex, &LldpDot3TlvEnabled) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *)
                      "\r\n%%Unable to set PortConfigDot3TLVsTxEnable\r\n");
        LldpUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    LldpUnLock ();
    WebnmUnRegisterLock (pHttp);
    /*Display the details below */
    IssProcessLldpAgentDetailsPageGet (pHttp);
    return;
}

VOID
IssProcessLldpAgentDetailsPageGet (tHttp * pHttp)
{
    INT1               *pi1IfName = NULL;
    INT1                ai1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT4                i4Status = 0;
    INT4                i4PrevIfIndex = 0;
    INT4                i4IfIndex = 0;
    INT4                i4SubType = 0;
    INT4                i4LocPortNum = 0;
    INT4                i4LocVlanId = 0;
    INT4                i4PrevLocPortNum = 0;
    INT4                i4PrevLocVlanId = 0;
    INT4                i4LocVlanNameEnabled = 0;
    UINT4               u4Temp = 0;
    UINT4               u4ManAddr = 0;
    UINT4               u4DestMacIndex = 0;
    UINT4               u4PrevDestMacIndex = 0;
    UINT4               u4VlanId = 0;
    tSNMP_OCTET_STRING_TYPE LocVlanName;
    tMacAddr            DstMac;
    UINT1               au1DstMac[LLDP_MAX_MAC_STRING_SIZE] = { 0 };
    UINT1               au1VlanName[VLAN_STATIC_MAX_NAME_LEN + 1];
    CONST UINT1         au1Enabled[10] = "Enabled";
    CONST UINT1         au1Disabled[10] = "Disabled";
    UINT1               u1BasicTlvTxEnable = 0;
    UINT1               u1Dot3TlvTxEnable = 0;
    UINT1               u1Dot3String = 0;
    UINT1               au1ManAddr[LLDP_MAX_LEN_MAN_ADDR];
    CHR1                ac1DispManAddr[LLDP_MAX_LEN_MAN_ADDR + 1] = { 0 };
    CHR1               *pc1DispManAddr = NULL;
    tSNMP_OCTET_STRING_TYPE ManAddr;
    tSNMP_OCTET_STRING_TYPE BasicTlvsTxEnable;
    tSNMP_OCTET_STRING_TYPE Dot3TlvTxEnable;
    MEMSET (&ac1DispManAddr, 0, (LLDP_MAX_LEN_MAN_ADDR + 1));
    pc1DispManAddr = &ac1DispManAddr[0];

    MEMSET (au1ManAddr, 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMSET (&ManAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    MEMSET (&LocVlanName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1VlanName, 0, VLAN_STATIC_MAX_NAME_LEN + 1);

    LocVlanName.pu1_OctetList = au1VlanName;
    LocVlanName.i4_Length = VLAN_STATIC_MAX_NAME_LEN + 1;

    ManAddr.pu1_OctetList = au1ManAddr;
    pi1IfName = ai1IfName;

    u4Temp = (UINT4) pHttp->i4Write;

    MEMSET (&BasicTlvsTxEnable, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&Dot3TlvTxEnable, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&BasicTlvsTxEnable, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&Dot3TlvTxEnable, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    BasicTlvsTxEnable.pu1_OctetList = &u1BasicTlvTxEnable;
    BasicTlvsTxEnable.i4_Length = sizeof (u1BasicTlvTxEnable);
    Dot3TlvTxEnable.pu1_OctetList = &u1Dot3String;
    Dot3TlvTxEnable.i4_Length = sizeof (u1Dot3String);
    MEMSET (&DstMac, 0, MAC_ADDR_LEN);
    MEMSET (au1DstMac, 0, MAC_ADDR_LEN);

    WebnmRegisterLock (pHttp, LldpLock, LldpUnLock);
    LldpLock ();
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    if (nmhGetFirstIndexLldpV2PortConfigTable (&i4IfIndex, &u4DestMacIndex) ==
        SNMP_FAILURE)
    {
        LldpUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;
    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        STRCPY (pHttp->au1KeyString, "LLDP_IF_INDEX_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4IfIndex);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "LLDP_PORT_NO_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        CfaCliGetIfName ((UINT4) i4IfIndex, pi1IfName);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", (CHR1 *) pi1IfName);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        MEMSET (DstMac, 0, MAC_ADDR_LEN);
        nmhGetLldpV2DestMacAddress (u4DestMacIndex, &DstMac);
        PrintMacAddress (DstMac, au1DstMac);

        STRCPY (pHttp->au1KeyString, "DEST_MAC_ADDRESS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1DstMac);
        PrintMacAddress (DstMac, pHttp->au1DataString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetLldpV2PortConfigTLVsTxEnable (i4IfIndex, u4DestMacIndex,
                                            &BasicTlvsTxEnable);
        nmhGetLldpV2Xdot3PortConfigTLVsTxEnable (i4IfIndex, u4DestMacIndex,
                                                 &Dot3TlvTxEnable);

        STRCPY (pHttp->au1KeyString, "GET_PORT_DESCR_TLV_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /*if (u1BasicTlvTxEnable != 0)
           { */
        if (u1BasicTlvTxEnable & LLDP_PORT_DESC_TLV_ENABLED)
        {
            i4Status = 1;
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Enabled);
        }
        else
        {
            i4Status = 3;
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Disabled);
        }

        /*     STRCPY (pHttp->au1KeyString, "GET_PORT_DESCR_TLV_KEY"); */
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "GET_SYS_NAME_TLV_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        if (u1BasicTlvTxEnable & LLDP_SYS_NAME_TLV_ENABLED)
        {
            i4Status = 1;
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Enabled);
        }
        else
        {
            i4Status = 3;
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Disabled);
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "GET_SYS_DESCR_TLV_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        if (u1BasicTlvTxEnable & LLDP_SYS_DESC_TLV_ENABLED)
        {
            i4Status = 1;
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Enabled);
        }
        else
        {
            i4Status = 3;
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Disabled);
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "GET_SYS_CAPAB_TLV_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        if (u1BasicTlvTxEnable & LLDP_SYS_CAPAB_TLV_ENABLED)
        {
            i4Status = 1;
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Enabled);
        }
        else
        {
            i4Status = 3;
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Disabled);
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "GET_MGMT_ADDR_TLV_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        if (LldpWebIsManAddrTlvEnabled (i4IfIndex, &i4SubType, &ManAddr) ==
            OSIX_TRUE)
        {
            i4Status = 1;
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Enabled);
        }
        else
        {
            i4Status = 3;
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Disabled);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "GET_MGMT_ADDR_TLV_TYPE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "IPv4");
        if (i4Status == LLDP_TRUE)
        {
            if (i4SubType != 1)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "IPv6");
                STRCPY (pc1DispManAddr,
                        Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                      ManAddr.pu1_OctetList));
            }
            else
            {
                PTR_FETCH4 (u4ManAddr, ManAddr.pu1_OctetList);
                CLI_CONVERT_IPADDR_TO_STR (pc1DispManAddr, u4ManAddr);
            }
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "MGMT_ADDR_IP_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4Status != LLDP_TRUE)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pc1DispManAddr);
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "GET_PORT_VLAN_TLV_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        nmhGetLldpV2Xdot1ConfigPortVlanTxEnable (i4IfIndex, u4DestMacIndex,
                                                 &i4Status);
        if (i4Status != LLDP_TRUE)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Disabled);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Enabled);
        }
        /* LLDP Dot1 TLVs */
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "GET_PROTO_VLAN_TLV_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        i4Status = LldpWebIsProtoVlanEnabled (i4IfIndex, &u4VlanId);
        if (i4Status != LLDP_TRUE)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Disabled);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Enabled);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "GET_PROTO_VLAN_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        if (i4Status != LLDP_TRUE)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4VlanId);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "GET_VLAN_NAME_TLV_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        i4Status = LldpWebIsVlanNameEnabled (i4IfIndex);

        if (i4Status != LLDP_TRUE)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Disabled);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Enabled);
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "GET_VLAN_NAME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        if (i4Status != LLDP_TRUE)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "--");
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        }
        else
        {
            if (nmhGetFirstIndexLldpV2Xdot1ConfigVlanNameTable (&i4LocPortNum,
                                                                &i4LocVlanId)
                == SNMP_SUCCESS)
            {
                do
                {
                    nmhGetLldpV2Xdot1LocVlanName (i4LocPortNum, i4LocVlanId,
                                                  &LocVlanName);
                    nmhGetLldpV2Xdot1ConfigVlanNameTxEnable (i4LocPortNum,
                                                             i4LocVlanId,
                                                             &i4LocVlanNameEnabled);

                    if (i4IfIndex == i4LocPortNum)
                    {
                        /* Displays VlanId, VlanName & VlanTxEnable Status */
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 LocVlanName.pu1_OctetList);
                    }
                    i4PrevLocPortNum = i4LocPortNum;
                    i4PrevLocVlanId = i4LocVlanId;

                }
                while ((nmhGetNextIndexLldpV2Xdot1ConfigVlanNameTable
                        (i4PrevLocPortNum, &i4LocPortNum, i4PrevLocVlanId,
                         &i4LocVlanId)) == SNMP_SUCCESS);

            }
        }

        STRCPY (pHttp->au1KeyString, "GET_VID_DIGEST_TLV_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        nmhGetLldpV2Xdot1ConfigVidUsageDigestTxEnable (i4IfIndex, &i4Status);
        if (i4Status != LLDP_TRUE)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Disabled);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Enabled);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "GET_MGMT_VID_TLV_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        nmhGetLldpV2Xdot1ConfigManVidTxEnable (i4IfIndex, &i4Status);
        if (i4Status != LLDP_TRUE)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Disabled);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Enabled);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "GET_LINK_AGG_TLV_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        nmhGetLldpV2Xdot3PortConfigTLVsTxEnable (i4IfIndex, u4DestMacIndex,
                                                 &Dot3TlvTxEnable);
        MEMCPY (&u1Dot3TlvTxEnable, Dot3TlvTxEnable.pu1_OctetList,
                Dot3TlvTxEnable.i4_Length);
        /* Enabled Local Dot3 Tlvs */
        /*if (u1Dot3TlvTxEnable != 0)
           { */
        if (u1Dot3TlvTxEnable & LLDP_LINK_AGG_TLV_ENABLED)
        {
            i4Status = LLDP_TRUE;
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Enabled);
        }
        else
        {
            i4Status = 3;
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Disabled);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "GET_MAC_PHY_TLV_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        if (u1Dot3TlvTxEnable & LLDP_MAC_PHY_TLV_ENABLED)
        {
            i4Status = LLDP_TRUE;
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Enabled);
        }
        else
        {
            i4Status = 3;
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Disabled);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "GET_MAX_FRAMESIZE_TLV_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        if (u1Dot3TlvTxEnable & LLDP_MAX_FRAME_SIZE_TLV_ENABLED)
        {
            i4Status = LLDP_TRUE;
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Enabled);
        }
        else
        {
            i4Status = 3;
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Disabled);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        /*  } */
        /* } */
        i4PrevIfIndex = i4IfIndex;
        u4PrevDestMacIndex = u4DestMacIndex;
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    while (nmhGetNextIndexLldpV2PortConfigTable
           (i4PrevIfIndex, &i4IfIndex, u4PrevDestMacIndex,
            &u4DestMacIndex) == SNMP_SUCCESS);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    LldpUnLock ();
    WebnmUnRegisterLock (pHttp);
    return;
}

PRIVATE UINT1
LldpWebIsManAddrTlvEnabled (INT4 i4IfIndex, INT4 *pi4Type,
                            tSNMP_OCTET_STRING_TYPE * pManAddr)
{
    INT4                i4ManAddrSubtype = 0;
    INT4                i4PrevManAddrSubtype = 0;
    INT4                i4Result = 0;
    UINT1               au1ManAddr[LLDP_MAX_LEN_MAN_ADDR];
    UINT1               au1PrevManAddr[LLDP_MAX_LEN_MAN_ADDR];
    UINT1               au1ManAddrTlvTxEnable[LLDP_MAN_ADDR_TX_EN_MAX_BYTES];
    tSNMP_OCTET_STRING_TYPE ManAddr;
    tSNMP_OCTET_STRING_TYPE PrevManAddr;
    tSNMP_OCTET_STRING_TYPE ManAddrTlvTxEnable;

    MEMSET (au1ManAddr, 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMSET (au1PrevManAddr, 0, LLDP_MAX_LEN_MAN_ADDR);
    MEMSET (au1ManAddrTlvTxEnable, 0, LLDP_MAN_ADDR_TX_EN_MAX_BYTES);
    MEMSET (&ManAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PrevManAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&ManAddrTlvTxEnable, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    ManAddr.pu1_OctetList = au1ManAddr;
    PrevManAddr.pu1_OctetList = au1PrevManAddr;
    ManAddrTlvTxEnable.pu1_OctetList = au1ManAddrTlvTxEnable;

    if (nmhGetFirstIndexLldpV2LocManAddrTable (&i4ManAddrSubtype,
                                               &ManAddr) == SNMP_FAILURE)
    {
        return LLDP_FALSE;
    }

    do
    {
        MEMSET (&ManAddrTlvTxEnable, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        ManAddrTlvTxEnable.pu1_OctetList = au1ManAddrTlvTxEnable;

        nmhGetLldpV2ManAddrConfigTxEnable (i4IfIndex,
                                           LldpV1DestMacAddrIndex (i4IfIndex),
                                           i4ManAddrSubtype,
                                           &ManAddr, &i4Result);

        if (i4Result == OSIX_TRUE)
        {
            *pi4Type = i4ManAddrSubtype;
            pManAddr->i4_Length = ManAddr.i4_Length;
            MEMCPY (pManAddr->pu1_OctetList, ManAddr.pu1_OctetList,
                    ManAddr.i4_Length);
            return LLDP_TRUE;
        }
        i4PrevManAddrSubtype = i4ManAddrSubtype;
        PrevManAddr.i4_Length = ManAddr.i4_Length;
        MEMCPY (PrevManAddr.pu1_OctetList, ManAddr.pu1_OctetList,
                ManAddr.i4_Length);
    }
    while (nmhGetNextIndexLldpV2LocManAddrTable (i4PrevManAddrSubtype,
                                                 &i4ManAddrSubtype,
                                                 &PrevManAddr,
                                                 &ManAddr) == SNMP_SUCCESS);
    return LLDP_FALSE;
}
PRIVATE INT4
LldpWebIsProtoVlanEnabled (INT4 i4IfIndex, UINT4 *pu4VlanId)
{
    INT4                i4LocPortNum = 0;
    INT4                i4LocProtoVlanId = 0;
    INT4                i4PrevLocProtoVlanId = 0;
    INT4                i4PrevLocPortNum = 0;
    INT4                i4LocProtoVlanTxEnabled = 0;

    if (nmhGetFirstIndexLldpV2Xdot1LocProtoVlanTable (&i4LocPortNum,
                                                      (UINT4 *)
                                                      &i4LocProtoVlanId) !=
        SNMP_SUCCESS)
    {
        return LLDP_FALSE;
    }

    do
    {

        if (i4IfIndex == i4LocPortNum)
        {
            nmhGetLldpV2Xdot1ConfigProtoVlanTxEnable (i4LocPortNum,
                                                      i4LocProtoVlanId,
                                                      &i4LocProtoVlanTxEnabled);
            *pu4VlanId = (UINT4) i4LocProtoVlanId;
            return i4LocProtoVlanTxEnabled;
        }
        i4PrevLocPortNum = i4LocPortNum;
        i4PrevLocProtoVlanId = i4LocProtoVlanId;

    }
    while (nmhGetNextIndexLldpV2Xdot1LocProtoVlanTable (i4PrevLocPortNum,
                                                        &i4LocPortNum,
                                                        (UINT4)
                                                        i4PrevLocProtoVlanId,
                                                        (UINT4 *)
                                                        &i4LocProtoVlanId) ==
           SNMP_SUCCESS);

    return LLDP_FALSE;
}

PRIVATE INT4
LldpWebIsVlanNameEnabled (INT4 i4IfIndex)
{
    UINT1               au1VlanName[VLAN_STATIC_MAX_NAME_LEN + 1];
    INT4                i4LocPortNum = 0;
    INT4                i4LocVlanId = 0;
    INT4                i4PrevLocPortNum = 0;
    INT4                i4PrevLocVlanId = 0;
    INT4                i4LocVlanNameEnabled = 0;
    tSNMP_OCTET_STRING_TYPE LocVlanName;

    MEMSET (&LocVlanName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1VlanName, 0, VLAN_STATIC_MAX_NAME_LEN + 1);

    LocVlanName.pu1_OctetList = au1VlanName;
    LocVlanName.i4_Length = VLAN_STATIC_MAX_NAME_LEN + 1;

    if (nmhGetFirstIndexLldpV2Xdot1ConfigVlanNameTable (&i4LocPortNum,
                                                        &i4LocVlanId)
        != SNMP_SUCCESS)
    {
        return LLDP_FALSE;
    }

    do
    {

        if (i4IfIndex == i4LocPortNum)
        {
            nmhGetLldpV2Xdot1LocVlanName (i4LocPortNum, i4LocVlanId,
                                          &LocVlanName);
            nmhGetLldpV2Xdot1ConfigVlanNameTxEnable (i4LocPortNum, i4LocVlanId,
                                                     &i4LocVlanNameEnabled);
            return LLDP_TRUE;
        }
        i4PrevLocPortNum = i4LocPortNum;
        i4PrevLocVlanId = i4LocVlanId;

    }
    while ((nmhGetNextIndexLldpV2Xdot1ConfigVlanNameTable (i4PrevLocPortNum,
                                                           &i4LocPortNum,
                                                           i4PrevLocVlanId,
                                                           &i4LocVlanId))
           == SNMP_SUCCESS);

    return LLDP_FALSE;
}

#endif
#endif
