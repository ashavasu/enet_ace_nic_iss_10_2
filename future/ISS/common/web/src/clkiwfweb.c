/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: clkiwfweb.c,v 1.5 2014/07/18 12:23:38 siva Exp $
 *
 * Description: Routines for CLKIWF web Module
 ********************************************************************/

#ifndef _CLKIWF_WEB_C_
#define _CLKIWF_WEB_C_

#ifdef WEBNM_WANTED

#include "webiss.h"
#include "isshttp.h"
#include "clkiwfweb.h"
#include "clkiwcli.h"
/* CLKIWF RELATED FUNCTIONS*/

/*****************************************************************************
 *  Function Name : IssProcessClkiwfConfPage
 *  Description   : This function processes the HTTP request coming for the
 *                  ClkIwf Global configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None.
 ****************************************************************************/
VOID
IssProcessClkiwfConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessClkiwfConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessClkiwfConfPageSet (pHttp);
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ClkIwfWebSetClockVariance
 *
 * DESCRIPTION      : This function sets the Clock Variance of CLKIWF
 *                    module as configured from CLI.
 *
 * INPUT            : i4ClockVariance - Clk Variance
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/

INT4
ClkIwfWebSetClockVariance (INT4 i4ClockVariance)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsClkIwfClockVariance (&u4ErrCode, i4ClockVariance)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsClkIwfClockVariance (i4ClockVariance) == SNMP_FAILURE)
    {

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ClkIwfWebSetClockClass
 *
 * DESCRIPTION      : This function sets the Clock Class of CLKIWF
 *                    module as configured from CLI.
 *
 * INPUT            : i4ClockClass - Clock Class
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/

INT4
ClkIwfWebSetClockClass (INT4 i4ClockClass)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsClkIwfClockClass (&u4ErrCode, i4ClockClass) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsClkIwfClockClass (i4ClockClass) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ClkIwfWebSetClockAccuracy
 *
 * DESCRIPTION      : This function sets the ClockAccuracy  of CLKIWF
 *                    module as configured from CLI.
 *
 * INPUT            : i4ClockAccuracy - Value of Clock Accuracy
 *                    
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/

INT4
ClkIwfWebSetClockAccuracy (INT4 i4ClockAccuracy)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsClkIwfClockAccuracy (&u4ErrCode, i4ClockAccuracy)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsClkIwfClockAccuracy (i4ClockAccuracy) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ClkIwWebPrintAccuracy
 *
 * DESCRIPTION      : This function sets the ClkAccuracyStr value based on 
 *                    the ClockAccuracy  of CLKIWF module 
 *
 * INPUT            : i4ClockAccuracy - Value of Clock Accuracy
 *
 *
 * OUTPUT           : au1ClkAccuracyStr - value set based on the i4ClockAccuracy 
 *
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
ClkIwWebPrintAccuracy (INT4 i4ClockAccuracy, CHR1 au1ClkAccuracyStr[])
{
    switch (i4ClockAccuracy)
    {
        case CLI_CLK_25NS_ACCURACY:

            SPRINTF (au1ClkAccuracyStr, "within 25 ns");
            break;

        case CLI_CLK_100NS_ACCURACY:
            SPRINTF (au1ClkAccuracyStr, "within 100 ns");
            break;

        case CLI_CLK_250NS_ACCURACY:

            SPRINTF (au1ClkAccuracyStr, "within 250 ns");
            break;

        case CLI_CLK_1MS_ACCURACY:

            SPRINTF (au1ClkAccuracyStr, "within 1 micro second");
            break;

        case CLI_CLK_2PT5MS_ACCURACY:

            SPRINTF (au1ClkAccuracyStr, "within 2.5 micro seconds");
            break;

        case CLI_CLK_10MS_ACCURACY:

            SPRINTF (au1ClkAccuracyStr, "within 10 micro seconds");
            break;
        case CLI_CLK_25MS_ACCURACY:

            SPRINTF (au1ClkAccuracyStr, "within 25 micro seconds");
            break;

        case CLI_CLK_100MS_ACCURACY:

            SPRINTF (au1ClkAccuracyStr, "within 100 micro seconds");
            break;

        case CLI_CLK_250MS_ACCURACY:

            SPRINTF (au1ClkAccuracyStr, "within 250 micro seconds");
            break;

        case CLI_CLK_1MIS_ACCURACY:

            SPRINTF (au1ClkAccuracyStr, "within 1 milli second");
            break;

        case CLI_CLK_2PT5MIS_ACCURACY:

            SPRINTF (au1ClkAccuracyStr, "within 2.5 milli seconds");
            break;

        case CLI_CLK_10MIS_ACCURACY:

            SPRINTF (au1ClkAccuracyStr, "within 10 milli seconds");
            break;

        case CLI_CLK_25MIS_ACCURACY:

            SPRINTF (au1ClkAccuracyStr, "within 25 milli seconds");
            break;
        case CLI_CLK_250MIS_ACCURACY:

            SPRINTF (au1ClkAccuracyStr, "within 250 milli seconds");
            break;

        case CLI_CLK_100MIS_ACCURACY:

            SPRINTF (au1ClkAccuracyStr, "within 100 milli seconds");
            break;

        case CLI_CLK_1S_ACCURACY:

            SPRINTF (au1ClkAccuracyStr, "within 1 second");
            break;

        case CLI_CLK_10S_ACCURACY:

            SPRINTF (au1ClkAccuracyStr, "within 10 seconds");
            break;

        case CLI_CLK_GREATER_10S_ACCURACY:

            SPRINTF (au1ClkAccuracyStr, "greater than 10 seconds");
            break;

        default:

            SPRINTF (au1ClkAccuracyStr, "%d", i4ClockAccuracy);
            break;
    }

    return CLI_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME    : ClkIwfWebSetClockTimeSource
 *
 * DESCRIPTION      : This function sets the TimeSource of CLKIWF
 *                    module as configured from CLI.
 *
 * INPUT            : i4ClockTimeSource - Time Source NTP, PTP etc.
 *                    
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/

INT4
ClkIwfWebSetClockTimeSource (INT4 i4ClockTimeSource)
{

    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsClkIwfClockTimeSource (&u4ErrCode, i4ClockTimeSource)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsClkIwfClockTimeSource (i4ClockTimeSource) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME    : ClkIwfWebSetClockUtcOffset
 *
 * DESCRIPTION      : This function sets the UtcOffset of CLKIWF
 *                    module as configured from CLI.
 *
 * INPUT            : au1ClockUtcOffset - Clock offset pointer
 *                    
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/

INT4
ClkIwfWebSetClockUtcOffset (UINT1 *au1ClockUtcOffset)
{
    tSNMP_OCTET_STRING_TYPE ClockUtcOffset;
    UINT4               u4ErrCode = 0;

    MEMSET (&ClockUtcOffset, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    ClockUtcOffset.pu1_OctetList = au1ClockUtcOffset;
    ClockUtcOffset.i4_Length = (INT4) sizeof (au1ClockUtcOffset);

    if (nmhTestv2FsClkIwfUtcOffset (&u4ErrCode, &ClockUtcOffset)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsClkIwfUtcOffset (&ClockUtcOffset) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ClkIwfWebSetClockHoldOver
 *
 * DESCRIPTION      : This function sets the HoldOver of CLKIWF
 *                    module as configured from CLI.
 *
 * INPUT            : i4Status - Status value
 *                    
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/

INT4
ClkIwfWebSetClockHoldOver (INT4 i4Status)
{

    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsClkIwfHoldoverSpecification (&u4ErrCode, i4Status)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsClkIwfHoldoverSpecification (i4Status) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*******************************************************************************
 *  Function Name : IssProcessClkiwfConfPageGet 
 *  Description   : This function processes the get request for the Clock IWF
 *                  Global configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessClkiwfConfPageGet (tHttp * pHttp)
{

    tSNMP_OCTET_STRING_TYPE ClockUtcOffset;
    INT4                i4ClockVariance = 0;
    INT4                i4ClockAccuracy = 0;
    INT4                i4ClockClass = 0;
    INT4                i4HoldOverStatus = 0;
    INT4                i4TimeSource = 0;
    UINT1               au1ClockUtcOffset[CLK_U8_STR_LEN];
    CHR1                au1ClkAccuracyStr[OCTETSTR_SIZE];

    MEMSET (&ClockUtcOffset, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ClockUtcOffset, 0, sizeof (au1ClockUtcOffset));
    MEMSET (au1ClkAccuracyStr, 0, CLK_U8_STR_LEN);

    ClockUtcOffset.pu1_OctetList = au1ClockUtcOffset;
    ClockUtcOffset.i4_Length = (INT4) STRLEN (au1ClockUtcOffset);

    WebnmRegisterLock (pHttp, ClkMainLock, ClkMainUnLock);
    CLK_LOCK ();

    pHttp->i4Write = 0;

    nmhGetFsClkIwfClockVariance (&i4ClockVariance);
    nmhGetFsClkIwfClockAccuracy (&i4ClockAccuracy);
    nmhGetFsClkIwfClockClass (&i4ClockClass);
    nmhGetFsClkIwfHoldoverSpecification (&i4HoldOverStatus);
    nmhGetFsClkIwfClockTimeSource (&i4TimeSource);
    nmhGetFsClkIwfUtcOffset (&ClockUtcOffset);
    if (ClockUtcOffset.pu1_OctetList[0] == 0)
    {
        STRCPY (ClockUtcOffset.pu1_OctetList, "0");
    }
    ClkIwWebPrintAccuracy (i4ClockAccuracy, au1ClkAccuracyStr);

    STRCPY (pHttp->au1KeyString, "ClkIwfClockVariance_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4ClockVariance);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "ClkIwfClockClass_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4ClockClass);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "ClkIwfClockAccuracy_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1ClkAccuracyStr);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "ClkIwfClockTimeSource_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4TimeSource);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "ClkIwfCurrentUtcOffset_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", ClockUtcOffset.pu1_OctetList);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "ClkIwfHoldoverSpecification_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4HoldOverStatus);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    CLK_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    return;
}

/*******************************************************************************
 *  Function Name : IssProcessClkiwfConfPageSet
 *  Description   : This function processes the set request for the Clock IWF
 *                  Global configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessClkiwfConfPageSet (tHttp * pHttp)
{
    INT4                i4ClockVariance = 0;
    INT4                i4ClockAccuracy = 0;
    INT4                i4ClockClass = 0;
    INT4                i4HoldOverStatus = 0;
    INT4                i4TimeSource = 0;

    WebnmRegisterLock (pHttp, ClkMainLock, ClkMainUnLock);
    CLK_LOCK ();

    STRCPY (pHttp->au1Name, "ClkIwfClockVariance");

    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4ClockVariance = ATOI (pHttp->au1Value);
    }
    ClkIwfWebSetClockVariance (i4ClockVariance);

    STRCPY (pHttp->au1Name, "ClkIwfClockAccuracy");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4ClockAccuracy = ATOI (pHttp->au1Value);
    }
    ClkIwfWebSetClockAccuracy (i4ClockAccuracy);

    STRCPY (pHttp->au1Name, "ClkIwfClockClass");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4ClockClass = ATOI (pHttp->au1Value);
    }
    ClkIwfWebSetClockClass (i4ClockClass);

    STRCPY (pHttp->au1Name, "ClkIwfClockTimeSource");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4TimeSource = ATOI (pHttp->au1Value);
    }
    ClkIwfWebSetClockTimeSource (i4TimeSource);

    STRCPY (pHttp->au1Name, "ClkIwfCurrentUtcOffset");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
    }
    ClkIwfWebSetClockUtcOffset (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ClkIwfHoldoverSpecification");
    if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        i4HoldOverStatus = ATOI (pHttp->au1Value);
    }
    ClkIwfWebSetClockHoldOver (i4HoldOverStatus);

    CLK_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    IssProcessClkiwfConfPageGet (pHttp);
}

#endif
#endif
