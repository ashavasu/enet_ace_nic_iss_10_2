/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved]
 *
 * $Id: dhcp6web.c,v 1.14 2014/06/30 11:03:38 siva Exp $
 *
 * Description: This file contains routines for ELPS web Module
 *
 ********************************************************************/

#ifndef _DHCP6_WEB_C_
#define _DHCP6_WEB_C_
#include "snmputil.h"

#ifdef WEBNM_WANTED
#include "webiss.h"
#include "webinc.h"
#include "isshttp.h"
#include "dhcp6.h"
#include "dhcp6web.h"

#ifdef DHCP6_CLNT_WANTED
#include "fsdh6ccli.h"
#endif
#ifdef DHCP6_RLY_WANTED
#include "fsdh6rcli.h"
#endif
#ifdef DHCP6_SRV_WANTED
#include "fsdh6scli.h"
#endif

PRIVATE VOID D6SrWebPrintStringIpAdd PROTO ((tHttp *, UINT1 *));
PRIVATE VOID D6SrWebPrintDuid PROTO ((tHttp *, UINT1 *, INT4));

#ifdef DHCP6_CLNT_WANTED
/*********************************************************************
*  Function Name : IssProcessDhcp6ClientGlobalPage
*  Description   : This function processes the request coming for the 
*                  PG information page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessDhcp6ClientGlobalPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessDhcp6ClientGlobalPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessDhcp6ClientGlobalPageSet (pHttp);
    }
    return;
}

/*******************************************************************************
 *  Function Name : IssProcessDhcp6ClientGlobalPageGet
 *  Description   : This function processes the get request for the Elps
 *                  Protection group configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessDhcp6ClientGlobalPageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE TrapOption;
    tSNMP_OCTET_STRING_TYPE DebugTrace;
    UINT1               au1DebugTrace[255];
    UINT1               u1TrapOption = 0;
    UINT1               u1Trapvalue = 0;
    UINT4               u4TraceMask = 0;
    CHR1               *pc1StrTok = NULL;
    INT4                i4RetVal = 0;
    INT4                i4Check = 1;
    INT4                i4UnCheck = 0;

    MEMSET (&TrapOption, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    TrapOption.pu1_OctetList = &u1TrapOption;
    TrapOption.i4_Length = 1;

    MEMSET (au1DebugTrace, 0, sizeof (au1DebugTrace));
    DebugTrace.pu1_OctetList = au1DebugTrace;
    DebugTrace.i4_Length = 0;

    WebnmRegisterLock (pHttp, D6ClApiLock, D6ClApiUnLock);
    D6ClApiLock ();

    pHttp->i4Write = 0;

    /* Trap Admin Control */
    STRCPY (pHttp->au1KeyString, "TRAP_ADMIN_CONTROL_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    if (nmhGetFsDhcp6ClntTrapAdminControl (&TrapOption) == SNMP_SUCCESS)
    {
        if (TrapOption.i4_Length != 0)
        {
            if (TrapOption.pu1_OctetList[0] & 0x01)
            {
                u1Trapvalue = 1;
            }
            else if (TrapOption.pu1_OctetList[0] & 0x02)
            {
                u1Trapvalue = 2;
            }
            else
            {
                u1Trapvalue = 0;
            }
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u1Trapvalue);
        }
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    /* Syslog Admin Status */
    STRCPY (pHttp->au1KeyString, "SYSLOG_ADMIN_STATUS_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    i4RetVal = 0;
    if (nmhGetFsDhcp6ClntSysLogAdminStatus (&i4RetVal) == SNMP_SUCCESS)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    /* Listen Port */
    STRCPY (pHttp->au1KeyString, "LISTEN_PORT_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    i4RetVal = 0;
    if (nmhGetFsDhcp6ClntListenPort (&i4RetVal) == SNMP_SUCCESS)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    /* Transmit Port */
    STRCPY (pHttp->au1KeyString, "TRANSMIT_PORT_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    i4RetVal = 0;
    if (nmhGetFsDhcp6ClntTransmitPort (&i4RetVal) == SNMP_SUCCESS)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    /* Debug Trace */
    nmhGetFsDhcp6ClntDebugTrace (&DebugTrace);

    if (DebugTrace.i4_Length != 0)
    {
        pc1StrTok = STRTOK (DebugTrace.pu1_OctetList, " ");

        while (pc1StrTok != NULL)
        {
            if (STRCMP (pc1StrTok, "init-shut") == 0)
            {
                u4TraceMask = u4TraceMask | INIT_SHUT_TRC;
            }
            if (STRCMP (pc1StrTok, "mgmt") == 0)
            {
                u4TraceMask = u4TraceMask | MGMT_TRC;
            }
            if (STRCMP (pc1StrTok, "ctrl") == 0)
            {
                u4TraceMask = u4TraceMask | CONTROL_PLANE_TRC;
            }
            if (STRCMP (pc1StrTok, "pkt-dump") == 0)
            {
                u4TraceMask = u4TraceMask | DUMP_TRC;
            }
            if (STRCMP (pc1StrTok, "resource") == 0)
            {
                u4TraceMask = u4TraceMask | OS_RESOURCE_TRC;
            }
            if (STRCMP (pc1StrTok, "all-fail") == 0)
            {
                u4TraceMask = u4TraceMask | ALL_FAILURE_TRC;
            }
            if (STRCMP (pc1StrTok, "buffer") == 0)
            {
                u4TraceMask = u4TraceMask | BUFFER_TRC;
            }
            if (STRCMP (pc1StrTok, "critical") == 0)
            {
                u4TraceMask = u4TraceMask | DHCP6_CRITICAL_TRC;
            }
            if (STRCMP (pc1StrTok, "all") == 0)
            {
                u4TraceMask = u4TraceMask | DHCP6_ALL_TRC;
            }
            pc1StrTok = STRTOK (NULL, " ");
        }
        if (u4TraceMask & INIT_SHUT_TRC)
        {
            STRCPY (pHttp->au1KeyString, "TRACE_INIT_SHUT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        else
        {
            STRCPY (pHttp->au1KeyString, "TRACE_INIT_SHUT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        if (u4TraceMask & MGMT_TRC)
        {
            STRCPY (pHttp->au1KeyString, "TRACE_MGMT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        else
        {
            STRCPY (pHttp->au1KeyString, "TRACE_MGMT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        if (u4TraceMask & CONTROL_PLANE_TRC)
        {
            STRCPY (pHttp->au1KeyString, "TRACE_CTRL_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        else
        {
            STRCPY (pHttp->au1KeyString, "TRACE_CTRL_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        if (u4TraceMask & DUMP_TRC)
        {
            STRCPY (pHttp->au1KeyString, "TRACE_PKTDUMP_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        else
        {
            STRCPY (pHttp->au1KeyString, "TRACE_PKTDUMP_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        if (u4TraceMask & OS_RESOURCE_TRC)
        {
            STRCPY (pHttp->au1KeyString, "TRACE_RESOURCE_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        else
        {
            STRCPY (pHttp->au1KeyString, "TRACE_RESOURCE_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        if (u4TraceMask & ALL_FAILURE_TRC)
        {
            STRCPY (pHttp->au1KeyString, "TRACE_ALLFAIL_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        else
        {
            STRCPY (pHttp->au1KeyString, "TRACE_ALLFAIL_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        if (u4TraceMask & BUFFER_TRC)
        {
            STRCPY (pHttp->au1KeyString, "TRACE_BUFFER_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        else
        {
            STRCPY (pHttp->au1KeyString, "TRACE_BUFFER_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        if (u4TraceMask & DHCP6_CRITICAL_TRC)
        {
            STRCPY (pHttp->au1KeyString, "TRACE_CRITICAL_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        else
        {
            STRCPY (pHttp->au1KeyString, "TRACE_CRITICAL_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        if ((u4TraceMask & DHCP6_ALL_TRC) == DHCP6_ALL_TRC)
        {
            STRCPY (pHttp->au1KeyString, "TRACE_ALL_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        else
        {
            STRCPY (pHttp->au1KeyString, "TRACE_ALL_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
    }

    D6ClApiUnLock ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*******************************************************************************
*  Function Name : IssProcessDhcp6ClientGlobalPageSet
*  Description   : This function processes the HTTP set request coming for the
*                  Elps Protection Group configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None.
*******************************************************************************/
VOID
IssProcessDhcp6ClientGlobalPageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE TrapOption;
    tSNMP_OCTET_STRING_TYPE TraceInput;
    tSNMP_OCTET_STRING_TYPE TraceOldValue;
    UINT1               au1TraceInput[255];
    UINT1               au1TraceOldInput[255];
    UINT4               u4ErrValue = 0;
    INT4                i4SysLogAdminStatus = 0;
    INT4                i4ListenPort = 0;
    INT4                i4TransmitPort = 0;
    INT4                i4DebugValue = 0;
    INT4                i4RetVal = 0;
    UINT1               u1TrapOption = 0;

    MEMSET (&TrapOption, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&TraceOldValue, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    TrapOption.pu1_OctetList = &u1TrapOption;
    TrapOption.i4_Length = 1;

    MEMSET (au1TraceInput, 0, sizeof (au1TraceInput));
    TraceInput.pu1_OctetList = au1TraceInput;
    MEMSET (au1TraceOldInput, 0, sizeof (au1TraceOldInput));
    TraceOldValue.pu1_OctetList = au1TraceOldInput;

    WebnmRegisterLock (pHttp, D6ClApiLock, D6ClApiUnLock);
    D6ClApiLock ();

    STRCPY (pHttp->au1Name, "TRAP_ADMIN_CONTROL");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        u1TrapOption = (UINT1) ATOI (pHttp->au1Value);
        TrapOption.i4_Length = (INT4) STRLEN (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "SYSLOG_ADMIN_STATUS");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4SysLogAdminStatus = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "LISTEN_PORT");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4ListenPort = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "TRANSMIT_PORT");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4TransmitPort = ATOI (pHttp->au1Value);
    }

    i4DebugValue = 0;
    STRCPY (pHttp->au1Name, "DEBUG_TRACE");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DebugValue = ATOI (pHttp->au1Value);
        if (i4DebugValue == 1)
        {
            STRNCPY (au1TraceInput, "enable", STRLEN ("enable"));
        }
        else
        {
            STRNCPY (au1TraceInput, "disable", STRLEN ("disable"));
        }
    }

    i4DebugValue = 0;
    STRCPY (pHttp->au1Name, "TRACE_INIT_SHUT");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DebugValue = ATOI (pHttp->au1Value);
        if (i4DebugValue == 1)
            STRNCAT (au1TraceInput, " init-shut", STRLEN (" init-shut"));
    }

    i4DebugValue = 0;
    STRCPY (pHttp->au1Name, "TRACE_MGMT");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DebugValue = ATOI (pHttp->au1Value);
        if (i4DebugValue == 1)
            STRNCAT (au1TraceInput, " mgmt", STRLEN (" mgmt"));
    }

    i4DebugValue = 0;
    STRCPY (pHttp->au1Name, "TRACE_CTRL");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DebugValue = ATOI (pHttp->au1Value);
        if (i4DebugValue == 1)
            STRNCAT (au1TraceInput, " ctrl", STRLEN (" ctrl"));
    }

    i4DebugValue = 0;
    STRCPY (pHttp->au1Name, "TRACE_PKTDUMP");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DebugValue = ATOI (pHttp->au1Value);
        if (i4DebugValue == 1)
            STRNCAT (au1TraceInput, " pkt-dump", STRLEN (" pkt-dump"));
    }

    i4DebugValue = 0;
    STRCPY (pHttp->au1Name, "TRACE_RESOURCE");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DebugValue = ATOI (pHttp->au1Value);
        if (i4DebugValue == 1)
            STRNCAT (au1TraceInput, " resource", STRLEN (" resource"));
    }

    i4DebugValue = 0;
    STRCPY (pHttp->au1Name, "TRACE_ALLFAIL");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DebugValue = ATOI (pHttp->au1Value);
        if (i4DebugValue == 1)
            STRNCAT (au1TraceInput, " all-fail", STRLEN (" all-fail"));
    }

    i4DebugValue = 0;
    STRCPY (pHttp->au1Name, "TRACE_BUFFER");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DebugValue = ATOI (pHttp->au1Value);
        if (i4DebugValue == 1)
            STRNCAT (au1TraceInput, " buffer", STRLEN (" buffer"));
    }

    i4DebugValue = 0;
    STRCPY (pHttp->au1Name, "TRACE_CRITICAL");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DebugValue = ATOI (pHttp->au1Value);
        if (i4DebugValue == 1)
            STRNCAT (au1TraceInput, " critical", STRLEN (" critical"));
    }

    i4DebugValue = 0;
    STRCPY (pHttp->au1Name, "TRACE_ALL");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DebugValue = ATOI (pHttp->au1Value);
        if (i4DebugValue == 1)
            STRNCAT (au1TraceInput, " all", STRLEN (" all"));
    }

    TraceInput.i4_Length = (INT4) STRLEN (au1TraceInput);

    if (TraceInput.i4_Length == 6)
    {

        STRNCPY (au1TraceInput, "disable all", STRLEN ("disable all"));
        TraceInput.i4_Length = (INT4) STRLEN (au1TraceInput);
    }
    if (nmhTestv2FsDhcp6ClntTrapAdminControl (&u4ErrValue,
                                              &TrapOption) == SNMP_FAILURE)
    {
        D6ClApiUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to configure trap admin control");
        return;
    }

    if (nmhSetFsDhcp6ClntTrapAdminControl (&TrapOption) == SNMP_FAILURE)
    {
        D6ClApiUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set trap admin control");
        return;
    }

    if (nmhTestv2FsDhcp6ClntDebugTrace (&u4ErrValue,
                                        &TraceInput) == SNMP_FAILURE)
    {
        D6ClApiUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to configure debug trace");
        return;
    }

    STRNCPY (au1TraceOldInput, "disable all", STRLEN ("disable all"));
    TraceOldValue.i4_Length = (INT4) STRLEN (au1TraceOldInput);

    if (nmhSetFsDhcp6ClntDebugTrace (&TraceOldValue) == SNMP_FAILURE)
    {
        D6ClApiUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set debug trace");
        return;
    }

    if (nmhSetFsDhcp6ClntDebugTrace (&TraceInput) == SNMP_FAILURE)
    {
        D6ClApiUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set debug trace");
        return;
    }

    i4RetVal = 0;
    nmhGetFsDhcp6ClntSysLogAdminStatus (&i4RetVal);
    if (i4RetVal != i4SysLogAdminStatus)
    {
        if (nmhTestv2FsDhcp6ClntSysLogAdminStatus (&u4ErrValue,
                                                   i4SysLogAdminStatus)
            == SNMP_FAILURE)
        {
            D6ClApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure syslog admin status");
            return;
        }

        if (nmhSetFsDhcp6ClntSysLogAdminStatus (i4SysLogAdminStatus)
            == SNMP_FAILURE)
        {
            D6ClApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set syslog admin status");
            return;
        }
    }

    i4RetVal = 0;
    nmhGetFsDhcp6ClntListenPort (&i4RetVal);
    if (i4RetVal != i4ListenPort)
    {
        if (nmhTestv2FsDhcp6ClntListenPort (&u4ErrValue,
                                            i4ListenPort) == SNMP_FAILURE)
        {
            D6ClApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure source port");
            return;
        }

        if (nmhSetFsDhcp6ClntListenPort (i4ListenPort) == SNMP_FAILURE)
        {
            D6ClApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set source port");
            return;
        }
    }

    i4RetVal = 0;
    nmhGetFsDhcp6ClntTransmitPort (&i4RetVal);
    if (i4RetVal != i4TransmitPort)
    {
        if (nmhTestv2FsDhcp6ClntTransmitPort (&u4ErrValue,
                                              i4TransmitPort) == SNMP_FAILURE)
        {
            D6ClApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure destination port");
            return;
        }

        if (nmhSetFsDhcp6ClntTransmitPort (i4TransmitPort) == SNMP_FAILURE)
        {
            D6ClApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set destination port");
            return;
        }
    }

    D6ClApiUnLock ();
    WebnmUnRegisterLock (pHttp);

    IssProcessDhcp6ClientGlobalPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssProcessDhcp6ClientIfPage
*  Description   : This function processes the request coming for the 
*                  PG information page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessDhcp6ClientIfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessDhcp6ClientIfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessDhcp6ClientIfPageSet (pHttp);
    }
    return;
}

/*******************************************************************************
 *  Function Name : IssProcessDhcp6ClientIfPageGet
 *  Description   : This function processes the get request for the Elps
 *                  Protection group configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessDhcp6ClientIfPageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE SrvAddress;
    tSNMP_OCTET_STRING_TYPE ClientDuid;
    tSNMP_OCTET_STRING_TYPE RealmName;
    tSNMP_OCTET_STRING_TYPE KeyName;
    UINT1               au1SrvAdd[DHCP6_IP6_ADDRESS_SIZE_MAX + 1];
    UINT1               au1DuidName[DHCP6_DUID_SIZE_MAX + 1];
    UINT1               au1RealmName[DHCP6_REALM_SIZE_MAX + 1];
    UINT1               au1KeyName[DHCP6_KEY_SIZE_MAX + 1];
    UINT1               au1String[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4RetVal = 0;
    UINT4               u4Temp = 0;
    INT1               *pi1IfAlias;
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4RetVal = 0;

    MEMSET (&SrvAddress, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&ClientDuid, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&RealmName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&KeyName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    WebnmRegisterLock (pHttp, D6ClApiLock, D6ClApiUnLock);
    D6ClApiLock ();

    pHttp->i4Write = 0;
    STRCPY (pHttp->au1KeyString, "<! IP INTERFACES>");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    IssPrintIpInterfaces (pHttp, 0, 0);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    if (nmhGetFirstIndexFsDhcp6ClntIfTable (&i4IfIndex) == SNMP_FAILURE)
    {
        D6ClApiUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;

    i4IfIndex = 0;

    while (nmhGetNextIndexFsDhcp6ClntIfTable (i4IfIndex,
                                              &i4NextIfIndex) == SNMP_SUCCESS)
    {
        pHttp->i4Write = (INT4) u4Temp;

        MEMSET (au1SrvAdd, 0, DHCP6_IP6_ADDRESS_SIZE_MAX + 1);
        MEMSET (au1DuidName, 0, DHCP6_DUID_SIZE_MAX + 1);
        MEMSET (au1RealmName, 0, DHCP6_REALM_SIZE_MAX + 1);
        MEMSET (au1KeyName, 0, DHCP6_KEY_SIZE_MAX);

        SrvAddress.pu1_OctetList = au1SrvAdd;
        SrvAddress.i4_Length = 0;

        ClientDuid.pu1_OctetList = au1DuidName;
        ClientDuid.i4_Length = 0;

        RealmName.pu1_OctetList = au1RealmName;
        RealmName.i4_Length = 0;

        KeyName.pu1_OctetList = au1KeyName;
        KeyName.i4_Length = 0;

        MEMSET (au1String, 0, CFA_MAX_PORT_NAME_LENGTH);
        pi1IfAlias = (INT1 *) &au1String[0];

        STRCPY (pHttp->au1KeyString, "ALIAS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        CfaCliGetIfName ((UINT4) i4NextIfIndex, pi1IfAlias);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", (CHR1 *) pi1IfAlias);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Server Address */
        STRCPY (pHttp->au1KeyString, "INTERFACE_SRV_ADD_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetFsDhcp6ClntIfSrvAddress (i4NextIfIndex,
                                           &SrvAddress) == SNMP_SUCCESS)
        {
            if (SrvAddress.i4_Length != 0)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         SrvAddress.pu1_OctetList);
            }
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Interface DUID Type */
        STRCPY (pHttp->au1KeyString, "IF_DUIDTYPE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6ClntIfDuidType (i4NextIfIndex,
                                         &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Interface DUID */
        STRCPY (pHttp->au1KeyString, "IF_DUID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetFsDhcp6ClntIfDuid (i4NextIfIndex,
                                     &ClientDuid) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     ClientDuid.pu1_OctetList);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* DUID Interface Index */
        STRCPY (pHttp->au1KeyString, "DUID_IFINDEX_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6ClntIfDuidIfIndex (i4NextIfIndex,
                                            &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Max Retrans Count */
        STRCPY (pHttp->au1KeyString, "MAX_RETRANS_COUNT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6ClntIfMaxRetCount (i4NextIfIndex,
                                            &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Max Retrans Delay */
        STRCPY (pHttp->au1KeyString, "MAX_RETRANS_DELAY_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6ClntIfMaxRetDelay (i4NextIfIndex,
                                            &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Max Retrans Time */
        STRCPY (pHttp->au1KeyString, "MAX_RETRANS_TIME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6ClntIfMaxRetTime (i4NextIfIndex,
                                           &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Initial Retrans Time */
        STRCPY (pHttp->au1KeyString, "INITIAL_RETRANS_TIME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6ClntIfInitRetTime (i4NextIfIndex,
                                            &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Interface Current Time */
        STRCPY (pHttp->au1KeyString, "IF_CURR_TIME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6ClntIfCurrRetTime (i4NextIfIndex,
                                            &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Min Refresh Time */
        STRCPY (pHttp->au1KeyString, "MIN_REFRESH_TIME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4RetVal = 0;
        if (nmhGetFsDhcp6ClntIfMinRefreshTime (i4NextIfIndex,
                                               &u4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));

        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Realm Name */
        STRCPY (pHttp->au1KeyString, "REALM_NAME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetFsDhcp6ClntIfRealmName (i4NextIfIndex,
                                          &RealmName) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     RealmName.pu1_OctetList);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Interface Key */
        STRCPY (pHttp->au1KeyString, "INTERFACE_KEY_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetFsDhcp6ClntIfKey (i4NextIfIndex, &KeyName) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     KeyName.pu1_OctetList);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Interface Key Id */
        STRCPY (pHttp->au1KeyString, "INTERFACE_KEYID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4RetVal = 0;
        if (nmhGetFsDhcp6ClntIfKeyId (i4NextIfIndex, &u4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Counter Reset */
        STRCPY (pHttp->au1KeyString, "IF_COUNTER_RESET_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6ClntIfCounterRest (i4NextIfIndex,
                                            &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Rowstatus */
        STRCPY (pHttp->au1KeyString, "ROW_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6ClntIfRowStatus (i4NextIfIndex,
                                          &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        i4IfIndex = i4NextIfIndex;
    }

    D6ClApiUnLock ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*******************************************************************************
*  Function Name : IssProcessDhcp6ClientIfPageSet
*  Description   : This function processes the HTTP set request coming for the
*                  Elps Protection Group configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None.
*******************************************************************************/
VOID
IssProcessDhcp6ClientIfPageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE RealmName;
    tSNMP_OCTET_STRING_TYPE KeyName;
    UINT1               au1RealmName[DHCP6_REALM_SIZE_MAX + 1];
    UINT1               au1KeyName[DHCP6_KEY_SIZE_MAX + 1];
    UINT4               u4MinRefreshTime = 0;
    UINT4               u4IfKeyId = 1;
    UINT4               u4RetVal = 0;
    UINT4               u4ErrValue = 0;
    UINT1               i4IfIndex = 0;
    UINT1               i4DuidType = 0;
    UINT1               i4DuidIfIndex = 0;
    UINT1               i4MaxRetCount = 0;
    UINT1               i4RetDelay = 0;
    UINT1               i4RetTime = 120;
    UINT1               i4InitRetTime = 1;
    UINT1               i4CounterRest = 0;
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = 0;

    MEMSET (au1RealmName, 0, sizeof (au1RealmName));
    MEMSET (&RealmName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1KeyName, 0, sizeof (au1KeyName));
    MEMSET (&KeyName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    RealmName.pu1_OctetList = au1RealmName;
    RealmName.i4_Length = 0;

    KeyName.pu1_OctetList = au1KeyName;
    KeyName.i4_Length = 0;

    WebnmRegisterLock (pHttp, D6ClApiLock, D6ClApiUnLock);
    D6ClApiLock ();

    STRCPY (pHttp->au1Name, "VLAN_ID");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4IfIndex = (UINT1) ATOI (pHttp->au1Value);
        i4DuidIfIndex = i4IfIndex;
    }
    STRCPY (pHttp->au1Name, "IF_DUIDTYPE");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DuidType = (UINT1) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "DUID_IFINDEX");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DuidIfIndex = (UINT1) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "MAX_RETRANS_COUNT");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4MaxRetCount = (UINT1) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "MAX_RETRANS_DELAY");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4RetDelay = (UINT1) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "MAX_RETRANS_TIME");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4RetTime = (UINT1) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "INITIAL_RETRANS_TIME");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4InitRetTime = (UINT1) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "MIN_REFRESH_TIME");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        u4MinRefreshTime = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "REALM_NAME");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        RealmName.i4_Length = (INT4) STRLEN (pHttp->au1Value);
        MEMCPY (RealmName.pu1_OctetList, pHttp->au1Value,
                MEM_MAX_BYTES (RealmName.i4_Length, DHCP6_REALM_SIZE_MAX));
    }

    STRCPY (pHttp->au1Name, "INTERFACE_KEY");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        KeyName.i4_Length = (INT4) STRLEN (pHttp->au1Value);
        MEMCPY (KeyName.pu1_OctetList, pHttp->au1Value,
                MEM_MAX_BYTES (KeyName.i4_Length, DHCP6_KEY_SIZE_MAX));
    }

    STRCPY (pHttp->au1Name, "INTERFACE_KEYID");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        u4IfKeyId = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "IF_COUNTER_RESET");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4CounterRest = (UINT1) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "ACTION");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        /* Three types of ACTION will be performed.(ADD and APPLY)
         * Actions other than this will result in a failure and will throw 
         * error */
        if (STRCMP (pHttp->au1Value, "Add") == 0)
        {
            /* Set Interface table rowstatus as Create And Wait */
            i4RetVal = nmhGetFsDhcp6ClntIfRowStatus (i4IfIndex, &i4RowStatus);
            if (i4RetVal != SNMP_SUCCESS)
            {
                if (nmhTestv2FsDhcp6ClntIfRowStatus (&u4ErrValue, i4IfIndex,
                                                     CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    D6ClApiUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                                  "needed to create interface table");
                    return;
                }
                if (nmhSetFsDhcp6ClntIfRowStatus (i4IfIndex,
                                                  CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    D6ClApiUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Entry creation failed");
                    return;
                }
            }
            else
            {
                D6ClApiUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Entry Already Exists");
                return;
            }
        }

        if (STRCMP (pHttp->au1Value, "Apply") == 0)
        {
            if (nmhTestv2FsDhcp6ClntIfRowStatus (&u4ErrValue, i4IfIndex,
                                                 NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                D6ClApiUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                              "needed to create interface table");
                return;
            }
            if (nmhSetFsDhcp6ClntIfRowStatus (i4IfIndex, NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                D6ClApiUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Entry creation failed");
                return;
            }
        }
        if (STRCMP (pHttp->au1Value, "Delete") == 0)
        {
            /* Delete the Rowstatus */
            if (nmhTestv2FsDhcp6ClntIfRowStatus (&u4ErrValue, i4IfIndex,
                                                 DESTROY) == SNMP_FAILURE)
            {
                D6ClApiUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                              " needed to delete");
                return;
            }
            if (nmhSetFsDhcp6ClntIfRowStatus (i4IfIndex,
                                              DESTROY) == SNMP_FAILURE)
            {
                D6ClApiUnLock ();
                WebnmUnRegisterLock (pHttp);
                return;
            }

            D6ClApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessDhcp6ClientIfPageGet (pHttp);
            return;
        }
    }
    else
    {
        D6ClApiUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Input");
        return;
    }

    i4RetVal = 0;
    nmhGetFsDhcp6ClntIfDuidType (i4IfIndex, &i4RetVal);
    if (i4RetVal != i4DuidType)
    {
        if (nmhTestv2FsDhcp6ClntIfDuidType (&u4ErrValue, i4IfIndex,
                                            i4DuidType) == SNMP_FAILURE)
        {
            D6ClApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure duid type");
            return;
        }

        if (nmhSetFsDhcp6ClntIfDuidType (i4IfIndex, i4DuidType) == SNMP_FAILURE)
        {
            D6ClApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set duid type");
            return;
        }
    }

    i4RetVal = 0;
    nmhGetFsDhcp6ClntIfDuidType (i4IfIndex, &i4RetVal);
    if (i4RetVal != i4DuidType)
    {
        if (nmhTestv2FsDhcp6ClntIfDuidType (&u4ErrValue, i4IfIndex,
                                            i4DuidType) == SNMP_FAILURE)
        {
            D6ClApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure duid type");
            return;
        }

        if (nmhSetFsDhcp6ClntIfDuidType (i4IfIndex, i4DuidType) == SNMP_FAILURE)
        {
            D6ClApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set duid type");
            return;
        }
    }

    i4RetVal = 0;
    nmhGetFsDhcp6ClntIfDuidIfIndex (i4IfIndex, &i4RetVal);
    if (i4RetVal != i4DuidIfIndex)
    {
        if (nmhTestv2FsDhcp6ClntIfDuidIfIndex (&u4ErrValue, i4IfIndex,
                                               i4DuidIfIndex) == SNMP_FAILURE)
        {
            D6ClApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure duid if index");
            return;
        }

        if (nmhSetFsDhcp6ClntIfDuidType (i4IfIndex,
                                         i4DuidIfIndex) == SNMP_FAILURE)
        {
            D6ClApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set duid if index");
            return;
        }
    }

    i4RetVal = 0;
    nmhGetFsDhcp6ClntIfMaxRetCount (i4IfIndex, &i4RetVal);
    if (i4RetVal != i4MaxRetCount)
    {
        if (nmhTestv2FsDhcp6ClntIfMaxRetCount (&u4ErrValue, i4IfIndex,
                                               i4MaxRetCount) == SNMP_FAILURE)
        {
            D6ClApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure MaxRet Count");
            return;
        }

        if (nmhSetFsDhcp6ClntIfMaxRetCount (i4IfIndex,
                                            i4MaxRetCount) == SNMP_FAILURE)
        {
            D6ClApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set MaxRet Count");
            return;
        }
    }

    i4RetVal = 0;
    nmhGetFsDhcp6ClntIfMaxRetDelay (i4IfIndex, &i4RetVal);
    if (i4RetVal != i4RetDelay)
    {
        if (nmhTestv2FsDhcp6ClntIfMaxRetDelay (&u4ErrValue, i4IfIndex,
                                               i4RetDelay) == SNMP_FAILURE)
        {
            D6ClApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure max RetDelay");
            return;
        }

        if (nmhSetFsDhcp6ClntIfMaxRetDelay (i4IfIndex,
                                            i4RetDelay) == SNMP_FAILURE)
        {
            D6ClApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set max RetDelay");
            return;
        }
    }

    if (nmhTestv2FsDhcp6ClntIfMaxRetTime (&u4ErrValue, i4IfIndex,
                                          i4RetTime) == SNMP_FAILURE)
    {
        D6ClApiUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to configure MaxRetTime");
        return;
    }

    if (nmhSetFsDhcp6ClntIfMaxRetTime (i4IfIndex, i4RetTime) == SNMP_FAILURE)
    {
        D6ClApiUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set MaxRetTime");
        return;
    }

    i4RetVal = 0;
    nmhGetFsDhcp6ClntIfInitRetTime (i4IfIndex, &i4RetVal);
    if (i4RetVal != i4InitRetTime)
    {
        if (nmhTestv2FsDhcp6ClntIfInitRetTime (&u4ErrValue, i4IfIndex,
                                               i4InitRetTime) == SNMP_FAILURE)
        {
            D6ClApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure InitRetTime");
            return;
        }

        if (nmhSetFsDhcp6ClntIfInitRetTime (i4IfIndex,
                                            i4InitRetTime) == SNMP_FAILURE)
        {
            D6ClApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set InitRetTime");
            return;
        }
    }

    u4RetVal = 0;
    nmhGetFsDhcp6ClntIfMinRefreshTime (i4IfIndex, &u4RetVal);
    if (u4RetVal != u4MinRefreshTime)
    {
        if (u4MinRefreshTime == 0)
            u4MinRefreshTime = 86400;
        if (nmhTestv2FsDhcp6ClntIfMinRefreshTime (&u4ErrValue, i4IfIndex,
                                                  u4MinRefreshTime)
            == SNMP_FAILURE)
        {
            D6ClApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set min refresh time");
            return;
        }

        if (nmhSetFsDhcp6ClntIfMinRefreshTime (i4IfIndex,
                                               u4MinRefreshTime)
            == SNMP_FAILURE)
        {
            D6ClApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set min refresh time");
            return;
        }
    }

    if (RealmName.i4_Length != 0)
    {
        if (nmhTestv2FsDhcp6ClntIfRealmName (&u4ErrValue, i4IfIndex,
                                             &RealmName) == SNMP_FAILURE)
        {
            D6ClApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure realm name");
            return;
        }

        if (nmhSetFsDhcp6ClntIfRealmName (i4IfIndex,
                                          &RealmName) == SNMP_FAILURE)
        {
            D6ClApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set config realm name");
            return;
        }
    }
    if (KeyName.i4_Length != 0)
    {
        if (nmhTestv2FsDhcp6ClntIfKey (&u4ErrValue, i4IfIndex,
                                       &KeyName) == SNMP_FAILURE)
        {
            D6ClApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to configure Key");
            return;
        }

        if (nmhSetFsDhcp6ClntIfKey (i4IfIndex, &KeyName) == SNMP_FAILURE)
        {
            D6ClApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set key");
            return;
        }
    }
    u4RetVal = 0;
    nmhGetFsDhcp6ClntIfKeyId (i4IfIndex, &u4RetVal);
    if (u4RetVal != u4IfKeyId)
    {
        if (nmhTestv2FsDhcp6ClntIfKeyId (&u4ErrValue, i4IfIndex,
                                         u4IfKeyId) == SNMP_FAILURE)
        {
            D6ClApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to configure key Id");
            return;
        }

        if (nmhSetFsDhcp6ClntIfKeyId (i4IfIndex, u4IfKeyId) == SNMP_FAILURE)
        {
            D6ClApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set key id");
            return;
        }
    }

    i4RetVal = 0;
    nmhGetFsDhcp6ClntIfCounterRest (i4IfIndex, &i4RetVal);
    if (i4RetVal != i4CounterRest)
    {
        if (nmhTestv2FsDhcp6ClntIfCounterRest (&u4ErrValue, i4IfIndex,
                                               i4CounterRest) == SNMP_FAILURE)
        {
            D6ClApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure counter reset");
            return;
        }

        if (nmhSetFsDhcp6ClntIfCounterRest (i4IfIndex,
                                            i4CounterRest) == SNMP_FAILURE)
        {
            D6ClApiUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set counter reset");
            return;
        }
    }

    if (nmhTestv2FsDhcp6ClntIfRowStatus (&u4ErrValue, i4IfIndex,
                                         ACTIVE) == SNMP_FAILURE)
    {
        D6ClApiUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                      " needed to activate");
        return;
    }
    if (nmhSetFsDhcp6ClntIfRowStatus (i4IfIndex, ACTIVE) == SNMP_FAILURE)
    {
        D6ClApiUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Entry creation failed");
        return;
    }

    D6ClApiUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessDhcp6ClientIfPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssProcessDhcp6ClientOptionPage
*  Description   : This function processes the request coming for the 
*                  PG information page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessDhcp6ClientOptionPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessDhcp6ClientOptionPageGet (pHttp);
    }
    return;
}

/*******************************************************************************
 *  Function Name : IssProcessDhcp6ClientOptionPageGet
 *  Description   : This function processes the get request for the Elps
 *                  Protection group configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessDhcp6ClientOptionPageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE OptionType;
    UINT1               au1OptionVal[DHCP6_CLNT_OPTION_SIZE_MAX + 1];
    UINT4               u4Temp = 0;
    INT4                i4IfIndex = 0;
    INT4                i4OptionType = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4NextOptionType = 0;

    MEMSET (&OptionType, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    WebnmRegisterLock (pHttp, D6ClApiLock, D6ClApiUnLock);
    D6ClApiLock ();

    pHttp->i4Write = 0;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    if (nmhGetFirstIndexFsDhcp6ClntOptionTable (&i4IfIndex,
                                                &i4OptionType) == SNMP_FAILURE)
    {
        D6ClApiUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;

    while (nmhGetNextIndexFsDhcp6ClntOptionTable (i4IfIndex, &i4NextIfIndex,
                                                  i4OptionType,
                                                  &i4NextOptionType)
           == SNMP_SUCCESS)
    {
        pHttp->i4Write = (INT4) u4Temp;

        MEMSET (au1OptionVal, 0, DHCP6_CLNT_OPTION_SIZE_MAX + 1);

        OptionType.pu1_OctetList = au1OptionVal;
        OptionType.i4_Length = 0;

        if (i4NextOptionType == DHCP6_STATUS_CODE_OPTION_TYPE)
        {
            i4IfIndex = i4NextIfIndex;
            i4OptionType = i4NextOptionType;
            continue;
        }
        if (nmhGetFsDhcp6ClntOptionValue (i4NextIfIndex, i4NextOptionType,
                                          &OptionType) == SNMP_SUCCESS)
        {
            if (OptionType.i4_Length == 0)
            {
                i4IfIndex = i4NextIfIndex;
                i4OptionType = i4NextOptionType;
                continue;
            }
        }

        /* Interface Index */
        STRCPY (pHttp->au1KeyString, "INTERFACE_INDEX_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextIfIndex);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Option Type */
        STRCPY (pHttp->au1KeyString, "OPTION_TYPE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextOptionType);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Option Type Value */
        STRCPY (pHttp->au1KeyString, "OPTION_TYPE_VALUE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetFsDhcp6ClntOptionValue (i4NextIfIndex, i4NextOptionType,
                                          &OptionType) == SNMP_SUCCESS)
        {
            if (OptionType.i4_Length != 0)
            {
                if (i4NextOptionType == DHCP6_ORO_DNS_SERVERS ||
                    i4NextOptionType == DHCP6_ORO_SIP_SERVER_A)
                {
                    D6SrWebPrintStringIpAdd (pHttp, OptionType.pu1_OctetList);
                }
                else
                {
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                             OptionType.pu1_OctetList);
                }
            }
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        i4IfIndex = i4NextIfIndex;
        i4OptionType = i4NextOptionType;
    }

    D6ClApiUnLock ();
    WebnmUnRegisterLock (pHttp);

/*    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));*/
}
#endif /* DHCP6_CLNT_WANTED */

#ifdef DHCP6_RLY_WANTED
/*********************************************************************
*  Function Name : IssProcessDhcp6RelayGlobalPage
*  Description   : This function processes the request coming for the 
*                  PG information page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessDhcp6RelayGlobalPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessDhcp6RelayGlobalPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessDhcp6RelayGlobalPageSet (pHttp);
    }
    return;
}

/*******************************************************************************
 *  Function Name : IssProcessDhcp6RelayGlobalPageGet
 *  Description   : This function processes the get request for the Elps
 *                  Protection group configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessDhcp6RelayGlobalPageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE TrapOption;
    tSNMP_OCTET_STRING_TYPE DebugTrace;
    UINT1               au1DebugTrace[255];
    UINT1               u1TrapOption = 0;
    UINT1               u1Trapvalue = 0;
    UINT4               u4TraceMask = 0;
    CHR1               *pc1StrTok = NULL;
    INT4                i4RetVal = 0;
    INT4                i4Check = 1;
    INT4                i4UnCheck = 0;

    MEMSET (&TrapOption, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    TrapOption.pu1_OctetList = &u1TrapOption;
    TrapOption.i4_Length = 1;

    MEMSET (au1DebugTrace, 0, sizeof (au1DebugTrace));
    DebugTrace.pu1_OctetList = au1DebugTrace;
    DebugTrace.i4_Length = 0;

    WebnmRegisterLock (pHttp, D6RlMainLock, D6RlMainUnLock);
    D6RlMainLock ();

    pHttp->i4Write = 0;

    /* Trap Admin Control */
    STRCPY (pHttp->au1KeyString, "TRAP_ADMIN_CONTROL_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    if (nmhGetFsDhcp6RlyTrapAdminControl (&TrapOption) == SNMP_SUCCESS)
    {
        if (TrapOption.i4_Length != 0)
        {
            if (TrapOption.pu1_OctetList[0] & 0x01)
            {
                u1Trapvalue = 1;
            }
            else if (TrapOption.pu1_OctetList[0] & 0x02)
            {
                u1Trapvalue = 2;
            }
            else
            {
                u1Trapvalue = 0;
            }
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u1Trapvalue);
        }
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    /* Syslog Admin Status */
    STRCPY (pHttp->au1KeyString, "SYSLOG_ADMIN_STATUS_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    i4RetVal = 0;
    if (nmhGetFsDhcp6RlySysLogAdminStatus (&i4RetVal) == SNMP_SUCCESS)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    /* Listen Port */
    STRCPY (pHttp->au1KeyString, "LISTEN_PORT_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    i4RetVal = 0;
    if (nmhGetFsDhcp6RlyListenPort (&i4RetVal) == SNMP_SUCCESS)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    /* Transmit Port */
    STRCPY (pHttp->au1KeyString, "TRANSMIT_PORT_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    i4RetVal = 0;
    if (nmhGetFsDhcp6RlyClientTransmitPort (&i4RetVal) == SNMP_SUCCESS)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    /* Server Destination Port */
    STRCPY (pHttp->au1KeyString, "SERVER_PORT_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    i4RetVal = 0;
    if (nmhGetFsDhcp6RlyServerTransmitPort (&i4RetVal) == SNMP_SUCCESS)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    /* Debug Trace */
    nmhGetFsDhcp6RlyDebugTrace (&DebugTrace);

    if (DebugTrace.i4_Length != 0)
    {
        pc1StrTok = STRTOK (DebugTrace.pu1_OctetList, " ");

        while (pc1StrTok != NULL)
        {
            if (STRCMP (pc1StrTok, "init-shut") == 0)
            {
                u4TraceMask = u4TraceMask | INIT_SHUT_TRC;
            }
            if (STRCMP (pc1StrTok, "mgmt") == 0)
            {
                u4TraceMask = u4TraceMask | MGMT_TRC;
            }
            if (STRCMP (pc1StrTok, "ctrl") == 0)
            {
                u4TraceMask = u4TraceMask | CONTROL_PLANE_TRC;
            }
            if (STRCMP (pc1StrTok, "pkt-dump") == 0)
            {
                u4TraceMask = u4TraceMask | DUMP_TRC;
            }
            if (STRCMP (pc1StrTok, "resource") == 0)
            {
                u4TraceMask = u4TraceMask | OS_RESOURCE_TRC;
            }
            if (STRCMP (pc1StrTok, "all-fail") == 0)
            {
                u4TraceMask = u4TraceMask | ALL_FAILURE_TRC;
            }
            if (STRCMP (pc1StrTok, "buffer") == 0)
            {
                u4TraceMask = u4TraceMask | BUFFER_TRC;
            }
            if (STRCMP (pc1StrTok, "critical") == 0)
            {
                u4TraceMask = u4TraceMask | DHCP6_CRITICAL_TRC;
            }
            if (STRCMP (pc1StrTok, "all") == 0)
            {
                u4TraceMask = u4TraceMask | DHCP6_ALL_TRC;
            }
            pc1StrTok = STRTOK (NULL, " ");
        }
        if (u4TraceMask & INIT_SHUT_TRC)
        {
            STRCPY (pHttp->au1KeyString, "TRACE_INIT_SHUT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        else
        {
            STRCPY (pHttp->au1KeyString, "TRACE_INIT_SHUT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        if (u4TraceMask & MGMT_TRC)
        {
            STRCPY (pHttp->au1KeyString, "TRACE_MGMT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        else
        {
            STRCPY (pHttp->au1KeyString, "TRACE_MGMT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        if (u4TraceMask & CONTROL_PLANE_TRC)
        {
            STRCPY (pHttp->au1KeyString, "TRACE_CTRL_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        else
        {
            STRCPY (pHttp->au1KeyString, "TRACE_CTRL_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        if (u4TraceMask & DUMP_TRC)
        {
            STRCPY (pHttp->au1KeyString, "TRACE_PKTDUMP_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        else
        {
            STRCPY (pHttp->au1KeyString, "TRACE_PKTDUMP_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        if (u4TraceMask & OS_RESOURCE_TRC)
        {
            STRCPY (pHttp->au1KeyString, "TRACE_RESOURCE_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        else
        {
            STRCPY (pHttp->au1KeyString, "TRACE_RESOURCE_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        if (u4TraceMask & ALL_FAILURE_TRC)
        {
            STRCPY (pHttp->au1KeyString, "TRACE_ALLFAIL_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        else
        {
            STRCPY (pHttp->au1KeyString, "TRACE_ALLFAIL_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        if (u4TraceMask & BUFFER_TRC)
        {
            STRCPY (pHttp->au1KeyString, "TRACE_BUFFER_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        else
        {
            STRCPY (pHttp->au1KeyString, "TRACE_BUFFER_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        if (u4TraceMask & DHCP6_CRITICAL_TRC)
        {
            STRCPY (pHttp->au1KeyString, "TRACE_CRITICAL_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        else
        {
            STRCPY (pHttp->au1KeyString, "TRACE_CRITICAL_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        if ((u4TraceMask & DHCP6_ALL_TRC) == DHCP6_ALL_TRC)
        {
            STRCPY (pHttp->au1KeyString, "TRACE_ALL_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        else
        {
            STRCPY (pHttp->au1KeyString, "TRACE_ALL_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
    }

    /* Remote-Id Option-37 Control */
    STRCPY (pHttp->au1KeyString, "OPTION_37_CONTROL_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    i4RetVal = 0;
    if (nmhGetFsDhcp6RlyOption37Control (&i4RetVal) == SNMP_SUCCESS)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "PD_ROUTE_CONTROL_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    i4RetVal = 0;
    if (nmhGetFsDhcp6RlyPDRouteControl (&i4RetVal) == SNMP_SUCCESS)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    D6RlMainUnLock ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*******************************************************************************
*  Function Name : IssProcessDhcp6RelayGlobalPageSet
*  Description   : This function processes the HTTP set request coming for the
*                  Elps Protection Group configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None.
*******************************************************************************/
VOID
IssProcessDhcp6RelayGlobalPageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE TrapOption;
    tSNMP_OCTET_STRING_TYPE TraceInput;
    tSNMP_OCTET_STRING_TYPE TraceOldValue;
    UINT1               au1TraceInput[255];
    UINT1               au1TraceOldInput[255];
    UINT4               u4ErrValue = 0;
    INT4                i4SysLogAdminStatus = 0;
    INT4                i4ListenPort = 0;
    INT4                i4TransmitPort = 0;
    INT4                i4ServerPort = 0;
    INT4                i4DebugValue = 0;
    INT4                i4RetVal = 0;
    INT4                i4RemoteIdStatus = 0;
    INT4                i4PDRouteStatus = 0;
    UINT1               u1TrapOption = 0;

    MEMSET (&TrapOption, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&TraceOldValue, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    TrapOption.pu1_OctetList = &u1TrapOption;
    TrapOption.i4_Length = 1;

    MEMSET (au1TraceInput, 0, sizeof (au1TraceInput));
    TraceInput.pu1_OctetList = au1TraceInput;

    MEMSET (au1TraceOldInput, 0, sizeof (au1TraceOldInput));
    TraceOldValue.pu1_OctetList = au1TraceOldInput;

    WebnmRegisterLock (pHttp, D6RlMainLock, D6RlMainUnLock);
    D6RlMainLock ();

    STRCPY (pHttp->au1Name, "TRAP_ADMIN_CONTROL");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        u1TrapOption = (UINT1) ATOI (pHttp->au1Value);
        TrapOption.i4_Length = (INT4) STRLEN (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "SYSLOG_ADMIN_STATUS");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4SysLogAdminStatus = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "LISTEN_PORT");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4ListenPort = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "TRANSMIT_PORT");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4TransmitPort = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "SERVER_PORT");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4ServerPort = ATOI (pHttp->au1Value);
    }

    i4DebugValue = 0;
    STRCPY (pHttp->au1Name, "DEBUG_TRACE");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DebugValue = ATOI (pHttp->au1Value);
        if (i4DebugValue == 1)
        {
            STRNCPY (au1TraceInput, "enable", STRLEN ("enable"));
        }
        else
        {
            STRNCPY (au1TraceInput, "disable", STRLEN ("disable"));
        }
    }

    i4DebugValue = 0;
    STRCPY (pHttp->au1Name, "TRACE_INIT_SHUT");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DebugValue = ATOI (pHttp->au1Value);
        if (i4DebugValue == 1)
            STRNCAT (au1TraceInput, " init-shut", STRLEN (" init-shut"));
    }

    i4DebugValue = 0;
    STRCPY (pHttp->au1Name, "TRACE_MGMT");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DebugValue = ATOI (pHttp->au1Value);
        if (i4DebugValue == 1)
            STRNCAT (au1TraceInput, " mgmt", STRLEN (" mgmt"));
    }

    i4DebugValue = 0;
    STRCPY (pHttp->au1Name, "TRACE_CTRL");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DebugValue = ATOI (pHttp->au1Value);
        if (i4DebugValue == 1)
            STRNCAT (au1TraceInput, " ctrl", STRLEN (" ctrl"));
    }

    i4DebugValue = 0;
    STRCPY (pHttp->au1Name, "TRACE_PKTDUMP");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DebugValue = ATOI (pHttp->au1Value);
        if (i4DebugValue == 1)
            STRNCAT (au1TraceInput, " pkt-dump", STRLEN (" pkt-dump"));
    }

    i4DebugValue = 0;
    STRCPY (pHttp->au1Name, "TRACE_RESOURCE");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DebugValue = ATOI (pHttp->au1Value);
        if (i4DebugValue == 1)
            STRNCAT (au1TraceInput, " resource", STRLEN (" resource"));
    }

    i4DebugValue = 0;
    STRCPY (pHttp->au1Name, "TRACE_ALLFAIL");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DebugValue = ATOI (pHttp->au1Value);
        if (i4DebugValue == 1)
            STRNCAT (au1TraceInput, " all-fail", STRLEN (" all-fail"));
    }

    i4DebugValue = 0;
    STRCPY (pHttp->au1Name, "TRACE_BUFFER");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DebugValue = ATOI (pHttp->au1Value);
        if (i4DebugValue == 1)
            STRNCAT (au1TraceInput, " buffer", STRLEN (" buffer"));
    }

    i4DebugValue = 0;
    STRCPY (pHttp->au1Name, "TRACE_CRITICAL");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DebugValue = ATOI (pHttp->au1Value);
        if (i4DebugValue == 1)
            STRNCAT (au1TraceInput, " critical", STRLEN (" critical"));
    }

    i4DebugValue = 0;
    STRCPY (pHttp->au1Name, "TRACE_ALL");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DebugValue = ATOI (pHttp->au1Value);
        if (i4DebugValue == 1)
            STRNCAT (au1TraceInput, " all", STRLEN (" all"));
    }

    TraceInput.i4_Length = (INT4) STRLEN (au1TraceInput);

    if (TraceInput.i4_Length == 6)
    {

        STRNCPY (au1TraceInput, "disable all", STRLEN ("disable all"));
        TraceInput.i4_Length = (INT4) STRLEN (au1TraceInput);
    }

    /* Option 37 Control */
    STRCPY (pHttp->au1Name, "OPTION_37_CONTROL");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4RemoteIdStatus = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "PD_ROUTE_CONTROL");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4PDRouteStatus = ATOI (pHttp->au1Value);
    }

    if (nmhTestv2FsDhcp6RlyTrapAdminControl (&u4ErrValue,
                                             &TrapOption) == SNMP_FAILURE)
    {
        D6RlMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to configure trap admin control");
        return;
    }

    if (nmhSetFsDhcp6RlyTrapAdminControl (&TrapOption) == SNMP_FAILURE)
    {
        D6RlMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set trap admin control");
        return;
    }

    if (nmhTestv2FsDhcp6RlyDebugTrace (&u4ErrValue,
                                       &TraceInput) == SNMP_FAILURE)
    {
        D6RlMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to configure debug trace");
        return;
    }

    STRNCPY (au1TraceOldInput, "disable all", STRLEN ("disable all"));
    TraceOldValue.i4_Length = (INT4) STRLEN (au1TraceOldInput);

    if (nmhSetFsDhcp6RlyDebugTrace (&TraceOldValue) == SNMP_FAILURE)
    {
        D6RlMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set debug trace");
        return;
    }

    if (nmhSetFsDhcp6RlyDebugTrace (&TraceInput) == SNMP_FAILURE)
    {
        D6RlMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set debug trace");
        return;
    }

    i4RetVal = 0;
    nmhGetFsDhcp6RlySysLogAdminStatus (&i4RetVal);
    if (i4RetVal != i4SysLogAdminStatus)
    {
        if (nmhTestv2FsDhcp6RlySysLogAdminStatus (&u4ErrValue,
                                                  i4SysLogAdminStatus)
            == SNMP_FAILURE)
        {
            D6RlMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure syslog admin status");
            return;
        }

        if (nmhSetFsDhcp6RlySysLogAdminStatus (i4SysLogAdminStatus)
            == SNMP_FAILURE)
        {
            D6RlMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set syslog admin status");
            return;
        }
    }

    i4RetVal = 0;
    nmhGetFsDhcp6RlyListenPort (&i4RetVal);
    if (i4RetVal != i4ListenPort)
    {
        if (nmhTestv2FsDhcp6RlyListenPort (&u4ErrValue,
                                           i4ListenPort) == SNMP_FAILURE)
        {
            D6RlMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure source port");
            return;
        }

        if (nmhSetFsDhcp6RlyListenPort (i4ListenPort) == SNMP_FAILURE)
        {
            D6RlMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set source port");
            return;
        }
    }

    i4RetVal = 0;
    nmhGetFsDhcp6RlyClientTransmitPort (&i4RetVal);
    if (i4RetVal != i4TransmitPort)
    {
        if (nmhTestv2FsDhcp6RlyClientTransmitPort (&u4ErrValue,
                                                   i4TransmitPort)
            == SNMP_FAILURE)
        {
            D6RlMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure client destination port");
            return;
        }

        if (nmhSetFsDhcp6RlyClientTransmitPort (i4TransmitPort) == SNMP_FAILURE)
        {
            D6RlMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set client destination port");
            return;
        }
    }

    i4RetVal = 0;
    nmhGetFsDhcp6RlyServerTransmitPort (&i4RetVal);
    if (i4RetVal != i4ServerPort)
    {
        if (nmhTestv2FsDhcp6RlyServerTransmitPort (&u4ErrValue,
                                                   i4ServerPort)
            == SNMP_FAILURE)
        {
            D6RlMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure server destination port");
            return;
        }

        if (nmhSetFsDhcp6RlyServerTransmitPort (i4ServerPort) == SNMP_FAILURE)
        {
            D6RlMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set server destination port");
            return;
        }
    }

    i4RetVal = 0;
    nmhGetFsDhcp6RlyOption37Control (&i4RetVal);
    if (i4RetVal != i4RemoteIdStatus)
    {
        if (nmhTestv2FsDhcp6RlyOption37Control (&u4ErrValue,
                                                i4RemoteIdStatus)
            == SNMP_FAILURE)
        {
            D6RlMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure Remote-Id Option 37 Control");
            return;
        }

        if (nmhSetFsDhcp6RlyOption37Control (i4RemoteIdStatus) == SNMP_FAILURE)
        {
            D6RlMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set Remote-Id Option 37 Control");
            return;
        }
    }

    i4RetVal = 0;
    nmhGetFsDhcp6RlyPDRouteControl (&i4RetVal);
    if (i4RetVal != i4PDRouteStatus)
    {
        if (nmhTestv2FsDhcp6RlyPDRouteControl (&u4ErrValue,
                                               i4PDRouteStatus) == SNMP_FAILURE)
        {
            D6RlMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure PD Route Control");
            return;
        }

        if (nmhSetFsDhcp6RlyPDRouteControl (i4PDRouteStatus) == SNMP_FAILURE)
        {
            D6RlMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set PD Route Control");
            return;
        }
    }

    D6RlMainUnLock ();
    WebnmUnRegisterLock (pHttp);

    IssProcessDhcp6RelayGlobalPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssProcessDhcp6RelayIfPage
*  Description   : This function processes the request coming for the 
*                  PG information page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessDhcp6RelayIfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessDhcp6RelayIfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessDhcp6RelayIfPageSet (pHttp);
    }
    return;
}

/*******************************************************************************
 *  Function Name : IssProcessDhcp6RelayIfPageGet
 *  Description   : This function processes the get request for the Elps
 *                  Protection group configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessDhcp6RelayIfPageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE RemoteIdValue;
    tSNMP_OCTET_STRING_TYPE RemIdOptVal;
    UINT1               au1RemoteIdValue[DHCP6_DUID_SIZE_MAX];
    UINT1               au1RemIdOptVal[DHCP6_DUID_SIZE_MAX];
    UINT1               au1String[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4Temp = 0;
    INT1               *pi1IfAlias;
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4RetVal = 0;

    MEMSET (&RemoteIdValue, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1RemoteIdValue, 0, DHCP6_DUID_SIZE_MAX);
    MEMSET (&RemIdOptVal, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1RemIdOptVal, 0, DHCP6_DUID_SIZE_MAX);

    RemoteIdValue.pu1_OctetList = au1RemoteIdValue;
    RemoteIdValue.i4_Length = 0;

    RemIdOptVal.pu1_OctetList = au1RemIdOptVal;
    RemIdOptVal.i4_Length = 0;

    WebnmRegisterLock (pHttp, D6RlMainLock, D6RlMainUnLock);
    D6RlMainLock ();

    pHttp->i4Write = 0;
    STRCPY (pHttp->au1KeyString, "<! IP INTERFACES>");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    IssPrintIpInterfaces (pHttp, 0, 0);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    if (nmhGetFirstIndexFsDhcp6RlyIfTable (&i4IfIndex) == SNMP_FAILURE)
    {
        D6RlMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;

    i4IfIndex = 0;

    while (nmhGetNextIndexFsDhcp6RlyIfTable (i4IfIndex,
                                             &i4NextIfIndex) == SNMP_SUCCESS)
    {
        pHttp->i4Write = (INT4) u4Temp;

        MEMSET (au1String, 0, CFA_MAX_PORT_NAME_LENGTH);
        pi1IfAlias = (INT1 *) &au1String[0];

        STRCPY (pHttp->au1KeyString, "ALIAS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        CfaCliGetIfName ((UINT4) i4NextIfIndex, pi1IfAlias);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", (CHR1 *) pi1IfAlias);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Hope Threshold */
        STRCPY (pHttp->au1KeyString, "HOPE_THRESHOLD_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6RlyIfHopThreshold (i4NextIfIndex,
                                            &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Conter Reset */
        STRCPY (pHttp->au1KeyString, "COUNTER_RESET_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6RlyIfCounterRest (i4NextIfIndex,
                                           &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Remote-Id Type */
        STRCPY (pHttp->au1KeyString, "REM_ID_TYPE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6RlyIfRemoteIdOption (i4NextIfIndex,
                                              &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Relay DUID */
        STRCPY (pHttp->au1KeyString, "RELAY_DUID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetFsDhcp6RlyIfRemoteIdDUID (i4NextIfIndex,
                                            &RemoteIdValue) == SNMP_SUCCESS)
        {
            MEMSET (pHttp->au1DataString, 0, ENM_MAX_NAME_LEN);
            MEMSET (au1RemIdOptVal, 0, DHCP6_DUID_SIZE_MAX);
            CliOctetToStr (RemoteIdValue.pu1_OctetList,
                           (UINT4) RemoteIdValue.i4_Length, au1RemIdOptVal,
                           DHCP6_DUID_SIZE_MAX);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     (CHR1 *) au1RemIdOptVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* User Defined */
        MEMSET (RemoteIdValue.pu1_OctetList, 0, DHCP6_DUID_SIZE_MAX);
        RemoteIdValue.i4_Length = 0;
        STRCPY (pHttp->au1KeyString, "USER_DEFINED_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetFsDhcp6RlyIfRemoteIdUserDefined (i4NextIfIndex,
                                                   &RemoteIdValue) ==
            SNMP_SUCCESS)
        {
            MEMSET (pHttp->au1DataString, 0, ENM_MAX_NAME_LEN);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     (CHR1 *) RemoteIdValue.pu1_OctetList);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Remote-Id Option Value */
        STRCPY (pHttp->au1KeyString, "REM_ID_OPTION_VAL_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        MEMSET (RemIdOptVal.pu1_OctetList, 0, DHCP6_DUID_SIZE_MAX);
        RemIdOptVal.i4_Length = 0;
        if (nmhGetFsDhcp6RlyIfRemoteIdOptionValue (i4NextIfIndex,
                                                   &RemIdOptVal)
            == SNMP_SUCCESS)
        {
            MEMSET (pHttp->au1DataString, 0, ENM_MAX_NAME_LEN);
            if ((i4RetVal == DHCP6_RLY_REMOTEID_USERDEFINED)
                || (i4RetVal == DHCP6_RLY_REMOTEID_SW_NAME)
                || (i4RetVal == DHCP6_RLY_REMOTEID_MGMT_IP))
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         (CHR1 *) RemIdOptVal.pu1_OctetList);
            }
            else
            {
                MEMSET (au1RemoteIdValue, 0, DHCP6_DUID_SIZE_MAX);
                CliOctetToStr (RemIdOptVal.pu1_OctetList,
                               (UINT4) RemIdOptVal.i4_Length, au1RemoteIdValue,
                               DHCP6_DUID_SIZE_MAX);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         (CHR1 *) au1RemoteIdValue);
            }
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Rowstatus */
        STRCPY (pHttp->au1KeyString, "ROW_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6RlyIfRowStatus (i4NextIfIndex,
                                         &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        i4IfIndex = i4NextIfIndex;
    }

    D6RlMainUnLock ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*******************************************************************************
*  Function Name : IssProcessDhcp6RelayIfPageSet
*  Description   : This function processes the HTTP set request coming for the
*                  Elps Protection Group configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None.
*******************************************************************************/
VOID
IssProcessDhcp6RelayIfPageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE *pRemoteIdValue = NULL;
    UINT4               u4ErrValue = 0;
    UINT1               au1RemoteIdVal[DHCP6_DUID_SIZE_MAX];
    UINT1               i4HopeThreshold = 4;
    UINT1               i4IfCounterRest = 0;
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = 0;
    INT4                i4RemIdType = 0;
    INT4                i4IfIndex = 0;

    pRemoteIdValue =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (DHCP6_DUID_SIZE_MAX);
    if (pRemoteIdValue == NULL)
    {
        IssSendError (pHttp, (CONST INT1 *) "Memory Allocation Failed !!");
        return;
    }

    MEMSET (pRemoteIdValue->pu1_OctetList, 0, DHCP6_DUID_SIZE_MAX);
    pRemoteIdValue->i4_Length = 0;

    WebnmRegisterLock (pHttp, D6RlMainLock, D6RlMainUnLock);
    D6RlMainLock ();

    STRCPY (pHttp->au1Name, "VLAN_ID");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4IfIndex = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "HOPE_THRESHOLD");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4HopeThreshold = (UINT1) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "COUNTER_RESET");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4IfCounterRest = (UINT1) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "REM_ID_TYPE");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4RemIdType = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "ACTION");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        /* Three types of ACTION will be performed.(ADD and APPLY)
         * Actions other than this will result in a failure and will throw 
         * error */
        if (STRCMP (pHttp->au1Value, "Add") == 0)
        {
            /* Set Interface table rowstatus as Create And Wait */
            i4RetVal = nmhGetFsDhcp6RlyIfRowStatus (i4IfIndex, &i4RowStatus);
            if (i4RetVal != SNMP_SUCCESS)
            {
                if (nmhTestv2FsDhcp6RlyIfRowStatus (&u4ErrValue, i4IfIndex,
                                                    CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    D6RlMainUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                                  "needed to create interface table");
                    free_octetstring (pRemoteIdValue);
                    return;
                }
                if (nmhSetFsDhcp6RlyIfRowStatus (i4IfIndex,
                                                 CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    D6RlMainUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Entry creation failed");
                    free_octetstring (pRemoteIdValue);
                    return;
                }

                if (nmhTestv2FsDhcp6RlyIfHopThreshold (&u4ErrValue, i4IfIndex,
                                                       i4HopeThreshold) ==
                    SNMP_FAILURE)
                {
                    nmhSetFsDhcp6RlyIfRowStatus (i4IfIndex, DESTROY);
                    D6RlMainUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *)
                                  "Unable to configure hope threshold");
                    free_octetstring (pRemoteIdValue);
                    return;
                }

                if (nmhSetFsDhcp6RlyIfHopThreshold (i4IfIndex,
                                                    i4HopeThreshold) ==
                    SNMP_FAILURE)
                {
                    nmhSetFsDhcp6RlyIfRowStatus (i4IfIndex, DESTROY);
                    D6RlMainUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Unable to set hope threshold");
                    free_octetstring (pRemoteIdValue);
                    return;
                }

                i4RetVal = 0;
                nmhGetFsDhcp6RlyIfCounterRest (i4IfIndex, &i4RetVal);
                if (i4RetVal != i4IfCounterRest)
                {
                    if (nmhTestv2FsDhcp6RlyIfCounterRest
                        (&u4ErrValue, i4IfIndex,
                         i4IfCounterRest) == SNMP_FAILURE)
                    {
                        nmhSetFsDhcp6RlyIfRowStatus (i4IfIndex, DESTROY);
                        D6RlMainUnLock ();
                        WebnmUnRegisterLock (pHttp);
                        IssSendError (pHttp, (CONST INT1 *)
                                      "Unable to configure counter reset");
                        free_octetstring (pRemoteIdValue);
                        return;
                    }

                    if (nmhSetFsDhcp6RlyIfCounterRest (i4IfIndex,
                                                       i4IfCounterRest) ==
                        SNMP_FAILURE)
                    {
                        nmhSetFsDhcp6RlyIfRowStatus (i4IfIndex, DESTROY);
                        D6RlMainUnLock ();
                        WebnmUnRegisterLock (pHttp);
                        IssSendError (pHttp,
                                      (CONST INT1 *)
                                      "Unable to set counter reset");
                        free_octetstring (pRemoteIdValue);
                        return;
                    }
                }

                i4RetVal = 0;
                nmhGetFsDhcp6RlyIfRemoteIdOption (i4IfIndex, &i4RetVal);
                if (i4RetVal != i4RemIdType)
                {
                    if (nmhTestv2FsDhcp6RlyIfRemoteIdOption
                        (&u4ErrValue, i4IfIndex, i4RemIdType) == SNMP_FAILURE)
                    {
                        nmhSetFsDhcp6RlyIfRowStatus (i4IfIndex, DESTROY);
                        D6RlMainUnLock ();
                        WebnmUnRegisterLock (pHttp);
                        IssSendError (pHttp, (CONST INT1 *)
                                      "Unable to configure Remote-Id Type");
                        free_octetstring (pRemoteIdValue);
                        return;
                    }

                    if (nmhSetFsDhcp6RlyIfRemoteIdOption (i4IfIndex,
                                                          i4RemIdType) ==
                        SNMP_FAILURE)
                    {
                        nmhSetFsDhcp6RlyIfRowStatus (i4IfIndex, DESTROY);
                        D6RlMainUnLock ();
                        WebnmUnRegisterLock (pHttp);
                        IssSendError (pHttp,
                                      (CONST INT1 *)
                                      "Unable to set Remote-Id Type");
                        free_octetstring (pRemoteIdValue);
                        return;
                    }
                }
                /* Relay DUID */

                STRCPY (pHttp->au1Name, "RELAY_DUID");
                if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                         pHttp->au1PostQuery)) == ENM_SUCCESS)
                {
                    issDecodeSpecialChar (pHttp->au1Value);
                    MEMSET (au1RemoteIdVal, 0, DHCP6_DUID_SIZE_MAX);
                    STRNCPY (au1RemoteIdVal, pHttp->au1Value,
                             DHCP6_DUID_SIZE_MAX);
                    MEMSET (pRemoteIdValue->pu1_OctetList, 0,
                            DHCP6_DUID_SIZE_MAX);
                    pRemoteIdValue->i4_Length = 0;
                    CLI_CONVERT_DOT_STR_TO_ARRAY (au1RemoteIdVal,
                                                  pRemoteIdValue->
                                                  pu1_OctetList);
                    pRemoteIdValue->i4_Length =
                        (UINT1) OctetLen (pHttp->au1Value);
                }

                if (pRemoteIdValue->i4_Length != 0)
                {
                    if (nmhTestv2FsDhcp6RlyIfRemoteIdDUID
                        (&u4ErrValue, i4IfIndex,
                         pRemoteIdValue) == SNMP_FAILURE)
                    {
                        nmhSetFsDhcp6RlyIfRowStatus (i4IfIndex, DESTROY);
                        D6RlMainUnLock ();
                        WebnmUnRegisterLock (pHttp);
                        IssSendError (pHttp,
                                      (CONST INT1 *)
                                      "Unable to configure Relay DUID");
                        free_octetstring (pRemoteIdValue);
                        return;
                    }

                    if (nmhSetFsDhcp6RlyIfRemoteIdDUID
                        (i4IfIndex, pRemoteIdValue) == SNMP_FAILURE)
                    {
                        nmhSetFsDhcp6RlyIfRowStatus (i4IfIndex, DESTROY);
                        D6RlMainUnLock ();
                        WebnmUnRegisterLock (pHttp);
                        IssSendError (pHttp,
                                      (CONST INT1 *)
                                      "Unable to set Relay DUID");
                        free_octetstring (pRemoteIdValue);
                        return;
                    }
                }

                /* Relay User-Defined */

                MEMSET (pRemoteIdValue->pu1_OctetList, 0, DHCP6_DUID_SIZE_MAX);
                pRemoteIdValue->i4_Length = 0;
                STRCPY (pHttp->au1Name, "USER_DEFINED");
                if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                         pHttp->au1PostQuery)) == ENM_SUCCESS)
                {
                    issDecodeSpecialChar (pHttp->au1Value);
                    pRemoteIdValue->i4_Length = (INT4) STRLEN (pHttp->au1Value);
                    MEMCPY (pRemoteIdValue->pu1_OctetList,
                            pHttp->au1Value, pRemoteIdValue->i4_Length);
                }

                if (pRemoteIdValue->i4_Length != 0)
                {
                    if (nmhTestv2FsDhcp6RlyIfRemoteIdUserDefined
                        (&u4ErrValue, i4IfIndex,
                         pRemoteIdValue) == SNMP_FAILURE)
                    {
                        nmhSetFsDhcp6RlyIfRowStatus (i4IfIndex, DESTROY);
                        D6RlMainUnLock ();
                        WebnmUnRegisterLock (pHttp);
                        IssSendError (pHttp,
                                      (CONST INT1 *)
                                      "Unable to configure Relay User-Defined");
                        free_octetstring (pRemoteIdValue);
                        return;
                    }

                    if (nmhSetFsDhcp6RlyIfRemoteIdUserDefined
                        (i4IfIndex, pRemoteIdValue) == SNMP_FAILURE)
                    {
                        nmhSetFsDhcp6RlyIfRowStatus (i4IfIndex, DESTROY);
                        D6RlMainUnLock ();
                        WebnmUnRegisterLock (pHttp);
                        IssSendError (pHttp,
                                      (CONST INT1 *)
                                      "Unable to Set Relay User Defined");
                        free_octetstring (pRemoteIdValue);
                        return;
                    }
                }

                if (nmhTestv2FsDhcp6RlyIfRowStatus (&u4ErrValue, i4IfIndex,
                                                    ACTIVE) == SNMP_FAILURE)
                {
                    nmhSetFsDhcp6RlyIfRowStatus (i4IfIndex, DESTROY);
                    D6RlMainUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                                  " needed to activate");
                    free_octetstring (pRemoteIdValue);
                    return;
                }
                if (nmhSetFsDhcp6RlyIfRowStatus (i4IfIndex, ACTIVE) ==
                    SNMP_FAILURE)
                {
                    nmhSetFsDhcp6RlyIfRowStatus (i4IfIndex, DESTROY);
                    D6RlMainUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Entry creation failed");
                    free_octetstring (pRemoteIdValue);
                    return;
                }
            }
            else
            {
                D6RlMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Entry Already Exists");
                free_octetstring (pRemoteIdValue);
                return;
            }
        }

        if (STRCMP (pHttp->au1Value, "Apply") == 0)
        {
            STRCPY (pHttp->au1Name, "ALIAS");
            if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                     pHttp->au1PostQuery)) == ENM_SUCCESS)
            {
                issDecodeSpecialChar (pHttp->au1Value);
                if (pHttp->au1Value[INITIAL_INDEX] == 'v')
                {
                    if (CfaGetInterfaceIndexFromName
                        (pHttp->au1Value, (UINT4 *) &i4IfIndex) != SUCCESS)
                    {
                        IssSendError (pHttp, (INT1 *) "Invalid Interface.");
                        free_octetstring (pRemoteIdValue);
                        return;
                    }
                }

                else if (IssGetIfIndex (pHttp->au1Value, (UINT4 *)
                                        &i4IfIndex) == ENM_FAILURE)
                {
                    IssSendError (pHttp, (INT1 *) "Invalid Interface.");
                    free_octetstring (pRemoteIdValue);
                    return;
                }

            }

            if (nmhTestv2FsDhcp6RlyIfRowStatus (&u4ErrValue, i4IfIndex,
                                                NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                D6RlMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                              "needed to create interface table");
                free_octetstring (pRemoteIdValue);
                return;
            }
            if (nmhSetFsDhcp6RlyIfRowStatus (i4IfIndex, NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                D6RlMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Entry creation failed");
                free_octetstring (pRemoteIdValue);
                return;
            }

            if (nmhTestv2FsDhcp6RlyIfHopThreshold (&u4ErrValue, i4IfIndex,
                                                   i4HopeThreshold) ==
                SNMP_FAILURE)
            {
                nmhSetFsDhcp6RlyIfRowStatus (i4IfIndex, ACTIVE);
                D6RlMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to configure hope threshold");
                free_octetstring (pRemoteIdValue);
                return;
            }

            if (nmhSetFsDhcp6RlyIfHopThreshold (i4IfIndex,
                                                i4HopeThreshold) ==
                SNMP_FAILURE)
            {
                nmhSetFsDhcp6RlyIfRowStatus (i4IfIndex, ACTIVE);
                D6RlMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *) "Unable to set hope threshold");
                free_octetstring (pRemoteIdValue);
                return;
            }

            i4RetVal = 0;
            nmhGetFsDhcp6RlyIfCounterRest (i4IfIndex, &i4RetVal);
            if (i4RetVal != i4IfCounterRest)
            {
                if (nmhTestv2FsDhcp6RlyIfCounterRest (&u4ErrValue, i4IfIndex,
                                                      i4IfCounterRest) ==
                    SNMP_FAILURE)
                {
                    nmhSetFsDhcp6RlyIfRowStatus (i4IfIndex, ACTIVE);

                    D6RlMainUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *)
                                  "Unable to configure counter reset");
                    free_octetstring (pRemoteIdValue);
                    return;
                }

                if (nmhSetFsDhcp6RlyIfCounterRest (i4IfIndex,
                                                   i4IfCounterRest) ==
                    SNMP_FAILURE)
                {
                    nmhSetFsDhcp6RlyIfRowStatus (i4IfIndex, ACTIVE);
                    D6RlMainUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Unable to set counter reset");
                    free_octetstring (pRemoteIdValue);
                    return;
                }
            }

            i4RetVal = 0;
            nmhGetFsDhcp6RlyIfRemoteIdOption (i4IfIndex, &i4RetVal);
            if (i4RetVal != i4RemIdType)
            {
                if (nmhTestv2FsDhcp6RlyIfRemoteIdOption (&u4ErrValue, i4IfIndex,
                                                         i4RemIdType) ==
                    SNMP_FAILURE)
                {
                    nmhSetFsDhcp6RlyIfRowStatus (i4IfIndex, ACTIVE);
                    D6RlMainUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *)
                                  "Unable to configure Remote-Id Type");
                    free_octetstring (pRemoteIdValue);
                    return;
                }

                if (nmhSetFsDhcp6RlyIfRemoteIdOption (i4IfIndex,
                                                      i4RemIdType) ==
                    SNMP_FAILURE)
                {
                    nmhSetFsDhcp6RlyIfRowStatus (i4IfIndex, ACTIVE);
                    D6RlMainUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Unable to set Remote-Id Type");
                    free_octetstring (pRemoteIdValue);
                    return;
                }
            }

            /* Relay DUID */
            STRCPY (pHttp->au1Name, "RELAY_DUID");
            MEMSET (pHttp->au1Value, 0, ENM_MAX_NAME_LEN);
            if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                     pHttp->au1PostQuery)) == ENM_SUCCESS)
            {
                issDecodeSpecialChar (pHttp->au1Value);
                if (nmhGetFsDhcp6RlyIfRemoteIdDUID (i4IfIndex, pRemoteIdValue)
                    == SNMP_SUCCESS)
                {
                    MEMSET (au1RemoteIdVal, 0, DHCP6_DUID_SIZE_MAX);
                    CliOctetToStr (pRemoteIdValue->pu1_OctetList,
                                   (UINT4) pRemoteIdValue->i4_Length,
                                   au1RemoteIdVal, DHCP6_DUID_SIZE_MAX);
                    if (STRNCMP
                        (au1RemoteIdVal, pHttp->au1Value,
                         STRLEN (pHttp->au1Value)) != 0)
                    {
                        MEMSET (au1RemoteIdVal, 0, DHCP6_DUID_SIZE_MAX);
                        STRNCPY (au1RemoteIdVal, pHttp->au1Value,
                                 DHCP6_DUID_SIZE_MAX);
                        MEMSET (pRemoteIdValue->pu1_OctetList, 0,
                                DHCP6_DUID_SIZE_MAX);
                        pRemoteIdValue->i4_Length = 0;
                        CLI_CONVERT_DOT_STR_TO_ARRAY (au1RemoteIdVal,
                                                      pRemoteIdValue->
                                                      pu1_OctetList);
                        pRemoteIdValue->i4_Length =
                            (UINT1) OctetLen (pHttp->au1Value);

                        if (pRemoteIdValue->i4_Length != 0)
                        {
                            if (nmhTestv2FsDhcp6RlyIfRemoteIdDUID
                                (&u4ErrValue, i4IfIndex,
                                 pRemoteIdValue) == SNMP_FAILURE)
                            {
                                nmhSetFsDhcp6RlyIfRowStatus (i4IfIndex, ACTIVE);
                                D6RlMainUnLock ();
                                WebnmUnRegisterLock (pHttp);
                                IssSendError (pHttp,
                                              (CONST INT1 *)
                                              "Unable to configure Relay DUID");
                                free_octetstring (pRemoteIdValue);
                                return;
                            }

                            if (nmhSetFsDhcp6RlyIfRemoteIdDUID
                                (i4IfIndex, pRemoteIdValue) == SNMP_FAILURE)
                            {
                                nmhSetFsDhcp6RlyIfRowStatus (i4IfIndex, ACTIVE);
                                D6RlMainUnLock ();
                                WebnmUnRegisterLock (pHttp);
                                IssSendError (pHttp,
                                              (CONST INT1 *)
                                              "Unable to set Relay DUID");
                                free_octetstring (pRemoteIdValue);
                                return;
                            }
                        }
                    }
                }
            }

            /* Relay User-Defined */
            STRCPY (pHttp->au1Name, "USER_DEFINED");
            MEMSET (pHttp->au1Value, 0, ENM_MAX_NAME_LEN);
            if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                     pHttp->au1PostQuery)) == ENM_SUCCESS)
            {
                issDecodeSpecialChar (pHttp->au1Value);
                nmhGetFsDhcp6RlyIfRemoteIdUserDefined (i4IfIndex,
                                                       pRemoteIdValue);

                if (STRNCMP (pRemoteIdValue->pu1_OctetList, pHttp->au1Value,
                             STRLEN (pHttp->au1Value)) != 0)
                {
                    MEMSET (pRemoteIdValue->pu1_OctetList, 0,
                            DHCP6_DUID_SIZE_MAX);
                    pRemoteIdValue->i4_Length = 0;
                    pRemoteIdValue->i4_Length = (INT4) STRLEN (pHttp->au1Value);
                    MEMCPY (pRemoteIdValue->pu1_OctetList, pHttp->au1Value,
                            pRemoteIdValue->i4_Length);

                    if (pRemoteIdValue->i4_Length != 0)
                    {
                        if (nmhTestv2FsDhcp6RlyIfRemoteIdUserDefined
                            (&u4ErrValue, i4IfIndex, pRemoteIdValue) ==
                            SNMP_FAILURE)
                        {
                            nmhSetFsDhcp6RlyIfRowStatus (i4IfIndex, ACTIVE);
                            D6RlMainUnLock ();
                            WebnmUnRegisterLock (pHttp);
                            IssSendError (pHttp,
                                          (CONST INT1 *)
                                          "Unable to configure Relay User-Defined");
                            free_octetstring (pRemoteIdValue);
                            return;
                        }

                        if (nmhSetFsDhcp6RlyIfRemoteIdUserDefined
                            (i4IfIndex, pRemoteIdValue) == SNMP_FAILURE)
                        {
                            nmhSetFsDhcp6RlyIfRowStatus (i4IfIndex, ACTIVE);
                            D6RlMainUnLock ();
                            WebnmUnRegisterLock (pHttp);
                            IssSendError (pHttp,
                                          (CONST INT1 *)
                                          "Unable to Set Relay User Defined");
                            free_octetstring (pRemoteIdValue);
                            return;
                        }
                    }
                }
            }
            if (nmhTestv2FsDhcp6RlyIfRowStatus (&u4ErrValue, i4IfIndex,
                                                ACTIVE) == SNMP_FAILURE)
            {
                nmhSetFsDhcp6RlyIfRowStatus (i4IfIndex, ACTIVE);
                D6RlMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                              " needed to activate");
                free_octetstring (pRemoteIdValue);
                return;
            }
            if (nmhSetFsDhcp6RlyIfRowStatus (i4IfIndex, ACTIVE) == SNMP_FAILURE)
            {
                nmhSetFsDhcp6RlyIfRowStatus (i4IfIndex, ACTIVE);
                D6RlMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Entry creation failed");
                free_octetstring (pRemoteIdValue);
                return;
            }
        }
        if (STRCMP (pHttp->au1Value, "Delete") == 0)
        {
            STRCPY (pHttp->au1Name, "ALIAS");
            if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                     pHttp->au1PostQuery)) == ENM_SUCCESS)
            {
                issDecodeSpecialChar (pHttp->au1Value);
                if (pHttp->au1Value[INITIAL_INDEX] == 'v')
                {
                    if (CfaGetInterfaceIndexFromName
                        (pHttp->au1Value, (UINT4 *) &i4IfIndex) != SUCCESS)
                    {
                        IssSendError (pHttp, (INT1 *) "Invalid Interface.");
                        free_octetstring (pRemoteIdValue);
                        return;
                    }
                }

                else if (IssGetIfIndex (pHttp->au1Value, (UINT4 *)
                                        &i4IfIndex) == ENM_FAILURE)
                {
                    IssSendError (pHttp, (INT1 *) "Invalid Interface.");
                    free_octetstring (pRemoteIdValue);
                    return;
                }

            }
            /* Delete the Rowstatus */
            if (nmhTestv2FsDhcp6RlyIfRowStatus (&u4ErrValue, i4IfIndex,
                                                DESTROY) == SNMP_FAILURE)
            {
                D6RlMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                              " needed to delete");
                free_octetstring (pRemoteIdValue);
                return;
            }
            if (nmhSetFsDhcp6RlyIfRowStatus (i4IfIndex,
                                             DESTROY) == SNMP_FAILURE)
            {
                D6RlMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                free_octetstring (pRemoteIdValue);
                return;
            }

            D6RlMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessDhcp6RelayIfPageGet (pHttp);
            free_octetstring (pRemoteIdValue);
            return;
        }
    }
    else
    {
        D6RlMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Input");
        free_octetstring (pRemoteIdValue);
        return;
    }

    D6RlMainUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessDhcp6RelayIfPageGet (pHttp);
    free_octetstring (pRemoteIdValue);
}

/*********************************************************************
*  Function Name : IssProcessDhcp6RelaySrvAddrPage
*  Description   : This function processes the request coming for the 
*                  PG information page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessDhcp6RelaySrvAddrPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessDhcp6RelaySrvAddrPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessDhcp6RelaySrvAddrPageSet (pHttp);
    }
    return;
}

/*******************************************************************************
 *  Function Name : IssProcessDhcp6RelaySrvAddrPageGet
 *  Description   : This function processes the get request for the Elps
 *                  Protection group configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessDhcp6RelaySrvAddrPageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE Dhcp6SrvAddr;
    tSNMP_OCTET_STRING_TYPE Dhcp6SrvNextAddr;
    tIp6Addr            Ip6Addr;
    tIp6Addr            Ip6NextAddr;
    UINT1               au1String[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4Temp = 0;
    INT1               *pi1IfAlias;
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4RetVal = 0;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&Ip6NextAddr, 0, sizeof (tIp6Addr));

    Dhcp6SrvAddr.i4_Length = DHCP6_IP6_ADDRESS_SIZE_MAX;
    Dhcp6SrvNextAddr.i4_Length = DHCP6_IP6_ADDRESS_SIZE_MAX;

    Dhcp6SrvAddr.pu1_OctetList = (UINT1 *) &Ip6Addr;
    Dhcp6SrvNextAddr.pu1_OctetList = (UINT1 *) &Ip6NextAddr;

    WebnmRegisterLock (pHttp, D6RlMainLock, D6RlMainUnLock);
    D6RlMainLock ();

    pHttp->i4Write = 0;
    STRCPY (pHttp->au1KeyString, "<! IP INTERFACES>");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    IssPrintIpInterfaces (pHttp, 0, 0);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    if (nmhGetFirstIndexFsDhcp6RlySrvAddressTable (&i4IfIndex,
                                                   &Dhcp6SrvAddr)
        == SNMP_FAILURE)
    {
        D6RlMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;

    MEMSET (&Ip6Addr, 0, DHCP6_IP6_ADDRESS_SIZE_MAX);
    Dhcp6SrvAddr.i4_Length = DHCP6_IP6_ADDRESS_SIZE_MAX;
    Dhcp6SrvAddr.pu1_OctetList = (UINT1 *) &Ip6Addr;

    while (nmhGetNextIndexFsDhcp6RlySrvAddressTable (i4IfIndex,
                                                     &i4NextIfIndex,
                                                     &Dhcp6SrvAddr,
                                                     &Dhcp6SrvNextAddr)
           == SNMP_SUCCESS)
    {
        pHttp->i4Write = (INT4) u4Temp;

        /* Interface Index */
        MEMSET (au1String, 0, CFA_MAX_PORT_NAME_LENGTH);
        pi1IfAlias = (INT1 *) &au1String[0];

        STRCPY (pHttp->au1KeyString, "ALIAS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        CfaCliGetIfName ((UINT4) i4NextIfIndex, pi1IfAlias);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", (CHR1 *) pi1IfAlias);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Server Address */
        STRCPY (pHttp->au1KeyString, "SERVER_ADDRESS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                 Ip6PrintAddr (&Ip6NextAddr));
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Rowstatus */
        STRCPY (pHttp->au1KeyString, "ROW_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6RlySrvAddressRowStatus (i4NextIfIndex,
                                                 &Dhcp6SrvNextAddr,
                                                 &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        i4IfIndex = i4NextIfIndex;

        MEMCPY (Dhcp6SrvAddr.pu1_OctetList, Dhcp6SrvNextAddr.pu1_OctetList,
                DHCP6_IP6_ADDRESS_SIZE_MAX);
    }

    D6RlMainUnLock ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*******************************************************************************
*  Function Name : IssProcessDhcp6RelaySrvAddrPageSet
*  Description   : This function processes the HTTP set request coming for the
*                  Elps Protection Group configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None.
*******************************************************************************/
VOID
IssProcessDhcp6RelaySrvAddrPageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE Dhcp6SrvAddr;
    tIp6Addr            Ip6Addr;
    UINT4               u4ErrValue = 0;
    UINT1               i4IfIndex = 0;
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = 0;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    Dhcp6SrvAddr.i4_Length = DHCP6_IP6_ADDRESS_SIZE_MAX;
    Dhcp6SrvAddr.pu1_OctetList = (UINT1 *) &Ip6Addr;

    WebnmRegisterLock (pHttp, D6RlMainLock, D6RlMainUnLock);
    D6RlMainLock ();

    STRCPY (pHttp->au1Name, "VLAN_ID");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4IfIndex = (UINT1) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "SERVER_ADDRESS");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        INET_ATON6 (pHttp->au1Value, &Ip6Addr);
        Dhcp6SrvAddr.i4_Length = DHCP6_IP6_ADDRESS_SIZE_MAX;
    }

    STRCPY (pHttp->au1Name, "ACTION");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        /* Three types of ACTION will be performed.(ADD and APPLY)
         * Actions other than this will result in a failure and will throw 
         * error */
        if (STRCMP (pHttp->au1Value, "Add") == 0)
        {
            /* Set Interface table rowstatus as Create And Wait */
            i4RetVal = nmhGetFsDhcp6RlySrvAddressRowStatus (i4IfIndex,
                                                            &Dhcp6SrvAddr,
                                                            &i4RowStatus);
            if (i4RetVal != SNMP_SUCCESS)
            {
                if (nmhTestv2FsDhcp6RlySrvAddressRowStatus (&u4ErrValue,
                                                            i4IfIndex,
                                                            &Dhcp6SrvAddr,
                                                            CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    D6RlMainUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                                  " needed to create server address table");
                    return;
                }
                if (nmhSetFsDhcp6RlySrvAddressRowStatus (i4IfIndex,
                                                         &Dhcp6SrvAddr,
                                                         CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    D6RlMainUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Entry creation failed");
                    return;
                }
            }
            else
            {
                D6RlMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Invalid Input");
                return;
            }
        }
        if (STRCMP (pHttp->au1Value, "Delete") == 0)
        {
            /* Delete the Rowstatus */
            if (nmhTestv2FsDhcp6RlySrvAddressRowStatus (&u4ErrValue,
                                                        i4IfIndex,
                                                        &Dhcp6SrvAddr,
                                                        DESTROY)
                == SNMP_FAILURE)
            {
                D6RlMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                              " needed to delete server address table");
                return;
            }
            if (nmhSetFsDhcp6RlySrvAddressRowStatus (i4IfIndex,
                                                     &Dhcp6SrvAddr,
                                                     DESTROY) == SNMP_FAILURE)
            {
                D6RlMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Delete entry failed");
                return;
            }
            D6RlMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessDhcp6RelaySrvAddrPageGet (pHttp);
            return;
        }
    }
    else
    {
        D6RlMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Input");
        return;
    }

    if (nmhTestv2FsDhcp6RlySrvAddressRowStatus (&u4ErrValue,
                                                i4IfIndex,
                                                &Dhcp6SrvAddr,
                                                ACTIVE) == SNMP_FAILURE)
    {
        D6RlMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                      " needed to create server address table");
        return;
    }
    if (nmhSetFsDhcp6RlySrvAddressRowStatus (i4IfIndex,
                                             &Dhcp6SrvAddr,
                                             ACTIVE) == SNMP_FAILURE)
    {
        D6RlMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Entry creation failed");
        return;
    }

    D6RlMainUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessDhcp6RelaySrvAddrPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssProcessDhcp6RelayOutIfPage
*  Description   : This function processes the request coming for the 
*                  PG information page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessDhcp6RelayOutIfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessDhcp6RelayOutIfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessDhcp6RelayOutIfPageSet (pHttp);
    }
    return;
}

/*******************************************************************************
 *  Function Name : IssProcessDhcp6RelayOutIfPageGet
 *  Description   : This function processes the get request for the Elps
 *                  Protection group configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessDhcp6RelayOutIfPageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE Dhcp6SrvAddr;
    tSNMP_OCTET_STRING_TYPE Dhcp6SrvNextAddr;
    tIp6Addr            Ip6Addr;
    tIp6Addr            Ip6NextAddr;
    UINT1               au1String[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4Temp = 0;
    INT1               *pi1IfAlias;
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4OutIfIndex = 0;
    INT4                i4NextOutIfIndex = 0;
    INT4                i4RetVal = 0;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&Ip6NextAddr, 0, sizeof (tIp6Addr));

    Dhcp6SrvAddr.i4_Length = DHCP6_IP6_ADDRESS_SIZE_MAX;
    Dhcp6SrvNextAddr.i4_Length = DHCP6_IP6_ADDRESS_SIZE_MAX;

    Dhcp6SrvAddr.pu1_OctetList = (UINT1 *) &Ip6Addr;
    Dhcp6SrvNextAddr.pu1_OctetList = (UINT1 *) &Ip6NextAddr;

    WebnmRegisterLock (pHttp, D6RlMainLock, D6RlMainUnLock);
    D6RlMainLock ();

    pHttp->i4Write = 0;
    STRCPY (pHttp->au1KeyString, "<! IP INTERFACES>");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    IssPrintIpInterfaces (pHttp, 0, 0);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    if (nmhGetFirstIndexFsDhcp6RlyOutIfTable (&i4IfIndex,
                                              &Dhcp6SrvAddr,
                                              &i4OutIfIndex) == SNMP_FAILURE)
    {
        D6RlMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;

    i4OutIfIndex = 0;

    while (nmhGetNextIndexFsDhcp6RlyOutIfTable (i4IfIndex, &i4NextIfIndex,
                                                &Dhcp6SrvAddr,
                                                &Dhcp6SrvNextAddr,
                                                i4OutIfIndex,
                                                &i4NextOutIfIndex)
           == SNMP_SUCCESS)
    {
        pHttp->i4Write = (INT4) u4Temp;

        /* Interface Index */
        MEMSET (au1String, 0, CFA_MAX_PORT_NAME_LENGTH);
        pi1IfAlias = (INT1 *) &au1String[0];

        STRCPY (pHttp->au1KeyString, "ALIAS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        CfaCliGetIfName ((UINT4) i4NextIfIndex, pi1IfAlias);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", (CHR1 *) pi1IfAlias);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Server Address */
        STRCPY (pHttp->au1KeyString, "SERVER_ADDRESS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                 Ip6PrintAddr (&Ip6NextAddr));
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Out Interface Index */
        STRCPY (pHttp->au1KeyString, "OUT_INTERFACE_INDEX_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextOutIfIndex);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Rowstatus */
        STRCPY (pHttp->au1KeyString, "ROW_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6RlyOutIfRowStatus (i4NextIfIndex,
                                            &Dhcp6SrvNextAddr,
                                            i4NextOutIfIndex,
                                            &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        i4IfIndex = i4NextIfIndex;

        MEMCPY (Dhcp6SrvAddr.pu1_OctetList, Dhcp6SrvNextAddr.pu1_OctetList,
                DHCP6_IP6_ADDRESS_SIZE_MAX);

        i4OutIfIndex = i4NextOutIfIndex;
    }

    D6RlMainUnLock ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*******************************************************************************
*  Function Name : IssProcessDhcp6RelayOutIfPageSet
*  Description   : This function processes the HTTP set request coming for the
*                  Elps Protection Group configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None.
*******************************************************************************/
VOID
IssProcessDhcp6RelayOutIfPageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE Dhcp6SrvAddr;
    tIp6Addr            Ip6Addr;
    UINT4               u4ErrValue = 0;
    UINT1               i4IfIndex = 0;
    UINT1               i4OutIfIndex = 0;
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = 0;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    Dhcp6SrvAddr.i4_Length = DHCP6_IP6_ADDRESS_SIZE_MAX;
    Dhcp6SrvAddr.pu1_OctetList = (UINT1 *) &Ip6Addr;

    WebnmRegisterLock (pHttp, D6RlMainLock, D6RlMainUnLock);
    D6RlMainLock ();

    STRCPY (pHttp->au1Name, "VLAN_ID");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4IfIndex = (UINT1) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "SERVER_ADDRESS");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        INET_ATON6 (pHttp->au1Value, &Ip6Addr);
        Dhcp6SrvAddr.i4_Length = DHCP6_IP6_ADDRESS_SIZE_MAX;
    }

    STRCPY (pHttp->au1Name, "OUT_INTERFACE_INDEX");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4OutIfIndex = (UINT1) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "ACTION");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        /* Three types of ACTION will be performed.(ADD and APPLY)
         * Actions other than this will result in a failure and will throw 
         * error */
        if (STRCMP (pHttp->au1Value, "Add") == 0)
        {
            /* Set Interface table rowstatus as Create And Wait */
            i4RetVal = nmhGetFsDhcp6RlyOutIfRowStatus (i4IfIndex,
                                                       &Dhcp6SrvAddr,
                                                       i4OutIfIndex,
                                                       &i4RowStatus);
            if (i4RetVal != SNMP_SUCCESS)
            {
                if (nmhTestv2FsDhcp6RlyOutIfRowStatus (&u4ErrValue,
                                                       i4IfIndex,
                                                       &Dhcp6SrvAddr,
                                                       i4OutIfIndex,
                                                       CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    D6RlMainUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *) "Invalid Input");
                    return;
                }
                if (nmhSetFsDhcp6RlyOutIfRowStatus (i4IfIndex, &Dhcp6SrvAddr,
                                                    i4OutIfIndex,
                                                    CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    D6RlMainUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Entry creation failed");
                    return;
                }
            }
            else
            {
                D6RlMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Invalid Input");
                return;
            }
        }
        if (STRCMP (pHttp->au1Value, "Delete") == 0)
        {
            /* Delete the Rowstatus */
            if (nmhTestv2FsDhcp6RlyOutIfRowStatus (&u4ErrValue,
                                                   i4IfIndex,
                                                   &Dhcp6SrvAddr,
                                                   i4OutIfIndex,
                                                   DESTROY) == SNMP_FAILURE)
            {
                D6RlMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                              " needed to delete server address table");
                return;
            }
            if (nmhSetFsDhcp6RlyOutIfRowStatus (i4IfIndex, &Dhcp6SrvAddr,
                                                i4OutIfIndex,
                                                DESTROY) == SNMP_FAILURE)
            {
                D6RlMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Delete entry failed");
                return;
            }
            D6RlMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessDhcp6RelayOutIfPageGet (pHttp);
            return;
        }
    }
    else
    {
        D6RlMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Input");
        return;
    }

    if (nmhTestv2FsDhcp6RlyOutIfRowStatus (&u4ErrValue,
                                           i4IfIndex,
                                           &Dhcp6SrvAddr,
                                           i4OutIfIndex,
                                           ACTIVE) == SNMP_FAILURE)
    {
        D6RlMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                      " needed to create out Interface table");
        return;
    }
    if (nmhSetFsDhcp6RlyOutIfRowStatus (i4IfIndex, &Dhcp6SrvAddr,
                                        i4OutIfIndex, ACTIVE) == SNMP_FAILURE)
    {
        D6RlMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Entry creation failed");
        return;
    }

    D6RlMainUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessDhcp6RelayOutIfPageGet (pHttp);
}
#endif /* DHCP6_RLY_WANTED */

#ifdef DHCP6_SRV_WANTED
/*********************************************************************
 *    Function Name : IssPrintAvailablePoolID
 *    Description   : This function prints available PoolID
 * 
 *    Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *    Output(s)     : None.
 *    Return Values : None.
 ** *********************************************************************/
VOID
IssPrintAvailablePoolID (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE PoolName;
    UINT1               au1PoolName[DHCP6_SRV_POOL_NAME_MAX];
    UINT4               u4NextPoolId = 0;
    UINT4               u4CurrPoolId = 0;
    INT4                i4RetVal = 0;

    MEMSET (&PoolName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1PoolName, 0, DHCP6_SRV_POOL_NAME_MAX);

    PoolName.pu1_OctetList = au1PoolName;
    PoolName.i4_Length = 0;

    i4RetVal = nmhGetFirstIndexFsDhcp6SrvPoolTable (&u4NextPoolId);
    /* No need to Take the Lock as it will be taken in the api called 
     * within the function */

    if (i4RetVal == SNMP_FAILURE)
    {
        return;
    }

    STRCPY (pHttp->au1KeyString, "<! POOL ID>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        u4CurrPoolId = u4NextPoolId;
        nmhGetFsDhcp6SrvPoolName (u4NextPoolId, &PoolName);
        if (PoolName.i4_Length == 0)
        {
            continue;
        }
        SPRINTF ((CHR1 *) pHttp->au1DataString,
                 "<option value = \"%d\" selected>%s \n",
                 u4NextPoolId, PoolName.pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
    }
    while (nmhGetNextIndexFsDhcp6SrvPoolTable (u4CurrPoolId,
                                               &u4NextPoolId) == SNMP_SUCCESS);
}

/*********************************************************************
 *    Function Name : IssPrintAvailableOptionID
 *    Description   : This function prints available Option ID
 * 
 *    Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *    Output(s)     : None.
 *    Return Values : None.
 ** *********************************************************************/
VOID
IssPrintAvailableOptionID (tHttp * pHttp, UINT4 u4PoolIndex,
                           UINT4 u4OptionIndex)
{
    UINT4               u4NextPoolId = 0;
    UINT4               u4CurrPoolId = 0;
    UINT4               u4NextOptionId = 0;
    UINT4               u4CurrOptionId = 0;
    INT4                i4OptionType = 0;
    INT4                i4RetVal = 0;

    i4RetVal = nmhGetFirstIndexFsDhcp6SrvOptionTable (&u4NextPoolId,
                                                      &u4NextOptionId);
    /* No need to Take the Lock as it will be taken in the api called 
     * within the function */

    if (i4RetVal == SNMP_FAILURE)
    {
        return;
    }

    STRCPY (pHttp->au1KeyString, "<! OPTION ID>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        u4CurrPoolId = u4NextPoolId;
        u4CurrOptionId = u4NextOptionId;

        nmhGetFsDhcp6SrvOptionType (u4NextPoolId, u4NextOptionId,
                                    &i4OptionType);

        if (u4PoolIndex != u4NextPoolId || i4OptionType != 17)
        {
            continue;
        }
        if (u4NextOptionId == u4OptionIndex)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString,
                     "<option value = \"%d\" selected>%d \n",
                     u4NextOptionId, u4NextOptionId);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString,
                     "<option value = \"%d\">%d \n",
                     u4NextOptionId, u4NextOptionId);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
    }
    while (nmhGetNextIndexFsDhcp6SrvOptionTable (u4CurrPoolId,
                                                 &u4NextPoolId,
                                                 u4CurrOptionId,
                                                 &u4NextOptionId)
           == SNMP_SUCCESS);
}

/*********************************************************************
 * *    Function Name : IssPrintAvailableRealmIndex
 * *    Description   : This function prints available ContextsIDs
 * * 
 * *    Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *    Output(s)     : None.
 * *    Return Values : None.
 ** *********************************************************************/
VOID
IssPrintAvailableRealmIndex (tHttp * pHttp)
{
    UINT4               u4NextRealm = 0;
    UINT4               u4CurrRealm = 0;
    INT4                i4RetVal = 0;

    i4RetVal = nmhGetFirstIndexFsDhcp6SrvRealmTable (&u4NextRealm);
    /* No need to Take the Lock as it will be taken in the api called 
     * within the function */

    if (i4RetVal == SNMP_FAILURE)
    {
        return;
    }

    STRCPY (pHttp->au1KeyString, "<! REALM INDEX>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString,
                 "<option value = \"%d\" selected>%d \n", u4NextRealm,
                 u4NextRealm);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        u4CurrRealm = u4NextRealm;

    }
    while (nmhGetNextIndexFsDhcp6SrvRealmTable (u4CurrRealm,
                                                &u4NextRealm) == SNMP_SUCCESS);
    return;
}

/*********************************************************************
*  Function Name : IssProcessDhcp6ServerGlobalPage
*  Description   : This function processes the request coming for the 
*                  PG information page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessDhcp6ServerGlobalPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessDhcp6ServerGlobalPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessDhcp6ServerGlobalPageSet (pHttp);
    }
    return;
}

/*******************************************************************************
 *  Function Name : IssProcessDhcp6ServerGlobalPageGet
 *  Description   : This function processes the get request for the Elps
 *                  Protection group configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessDhcp6ServerGlobalPageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE TrapOption;
    tSNMP_OCTET_STRING_TYPE DebugTrace;
    UINT1               au1DebugTrace[255];
    UINT1               u1TrapOption = 0;
    UINT1               u1Trapvalue = 0;
    UINT4               u4TraceMask = 0;
    CHR1               *pc1StrTok = NULL;
    INT4                i4RetVal = 0;
    INT4                i4Check = 1;
    INT4                i4UnCheck = 0;

    MEMSET (&TrapOption, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    TrapOption.pu1_OctetList = &u1TrapOption;
    TrapOption.i4_Length = 1;

    MEMSET (au1DebugTrace, 0, sizeof (au1DebugTrace));
    DebugTrace.pu1_OctetList = au1DebugTrace;
    DebugTrace.i4_Length = 0;

    WebnmRegisterLock (pHttp, D6SrMainLock, D6SrMainUnLock);
    D6SrMainLock ();

    pHttp->i4Write = 0;

    /* Trap Admin Control */
    STRCPY (pHttp->au1KeyString, "TRAP_ADMIN_CONTROL_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    if (nmhGetFsDhcp6SrvTrapAdminControl (&TrapOption) == SNMP_SUCCESS)
    {
        if (TrapOption.i4_Length != 0)
        {
            if (TrapOption.pu1_OctetList[0] & 0x01)
            {
                u1Trapvalue = 1;
            }
            else if (TrapOption.pu1_OctetList[0] & 0x02)
            {
                u1Trapvalue = 2;
            }
            else if (TrapOption.pu1_OctetList[0] & 0x04)
            {
                u1Trapvalue = 4;
            }
            else
            {
                u1Trapvalue = 0;
            }
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u1Trapvalue);
        }
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    /* Syslog Admin Status */
    STRCPY (pHttp->au1KeyString, "SYSLOG_ADMIN_STATUS_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    i4RetVal = 0;
    if (nmhGetFsDhcp6SrvSysLogAdminStatus (&i4RetVal) == SNMP_SUCCESS)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    /* Listen Port */
    STRCPY (pHttp->au1KeyString, "LISTEN_PORT_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    i4RetVal = 0;
    if (nmhGetFsDhcp6SrvListenPort (&i4RetVal) == SNMP_SUCCESS)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    /* Transmit Port */
    STRCPY (pHttp->au1KeyString, "TRANSMIT_PORT_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    i4RetVal = 0;
    if (nmhGetFsDhcp6SrvClientTransmitPort (&i4RetVal) == SNMP_SUCCESS)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    /* Server Destination Port */
    STRCPY (pHttp->au1KeyString, "SERVER_PORT_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    i4RetVal = 0;
    if (nmhGetFsDhcp6SrvRelayTransmitPort (&i4RetVal) == SNMP_SUCCESS)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    /* Debug Trace */
    nmhGetFsDhcp6SrvDebugTrace (&DebugTrace);

    if (DebugTrace.i4_Length != 0)
    {
        pc1StrTok = STRTOK (DebugTrace.pu1_OctetList, " ");

        while (pc1StrTok != NULL)
        {
            if (STRCMP (pc1StrTok, "init-shut") == 0)
            {
                u4TraceMask = u4TraceMask | INIT_SHUT_TRC;
            }
            if (STRCMP (pc1StrTok, "mgmt") == 0)
            {
                u4TraceMask = u4TraceMask | MGMT_TRC;
            }
            if (STRCMP (pc1StrTok, "ctrl") == 0)
            {
                u4TraceMask = u4TraceMask | CONTROL_PLANE_TRC;
            }
            if (STRCMP (pc1StrTok, "pkt-dump") == 0)
            {
                u4TraceMask = u4TraceMask | DUMP_TRC;
            }
            if (STRCMP (pc1StrTok, "resource") == 0)
            {
                u4TraceMask = u4TraceMask | OS_RESOURCE_TRC;
            }
            if (STRCMP (pc1StrTok, "all-fail") == 0)
            {
                u4TraceMask = u4TraceMask | ALL_FAILURE_TRC;
            }
            if (STRCMP (pc1StrTok, "buffer") == 0)
            {
                u4TraceMask = u4TraceMask | BUFFER_TRC;
            }
            if (STRCMP (pc1StrTok, "critical") == 0)
            {
                u4TraceMask = u4TraceMask | DHCP6_CRITICAL_TRC;
            }
            if (STRCMP (pc1StrTok, "all") == 0)
            {
                u4TraceMask = u4TraceMask | DHCP6_ALL_TRC;
            }
            pc1StrTok = STRTOK (NULL, " ");
        }
        if (u4TraceMask & INIT_SHUT_TRC)
        {
            STRCPY (pHttp->au1KeyString, "TRACE_INIT_SHUT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        else
        {
            STRCPY (pHttp->au1KeyString, "TRACE_INIT_SHUT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        if (u4TraceMask & MGMT_TRC)
        {
            STRCPY (pHttp->au1KeyString, "TRACE_MGMT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        else
        {
            STRCPY (pHttp->au1KeyString, "TRACE_MGMT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        if (u4TraceMask & CONTROL_PLANE_TRC)
        {
            STRCPY (pHttp->au1KeyString, "TRACE_CTRL_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        else
        {
            STRCPY (pHttp->au1KeyString, "TRACE_CTRL_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        if (u4TraceMask & DUMP_TRC)
        {
            STRCPY (pHttp->au1KeyString, "TRACE_PKTDUMP_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        else
        {
            STRCPY (pHttp->au1KeyString, "TRACE_PKTDUMP_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        if (u4TraceMask & OS_RESOURCE_TRC)
        {
            STRCPY (pHttp->au1KeyString, "TRACE_RESOURCE_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        else
        {
            STRCPY (pHttp->au1KeyString, "TRACE_RESOURCE_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        if (u4TraceMask & ALL_FAILURE_TRC)
        {
            STRCPY (pHttp->au1KeyString, "TRACE_ALLFAIL_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        else
        {
            STRCPY (pHttp->au1KeyString, "TRACE_ALLFAIL_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        if (u4TraceMask & BUFFER_TRC)
        {
            STRCPY (pHttp->au1KeyString, "TRACE_BUFFER_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        else
        {
            STRCPY (pHttp->au1KeyString, "TRACE_BUFFER_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        if (u4TraceMask & DHCP6_CRITICAL_TRC)
        {
            STRCPY (pHttp->au1KeyString, "TRACE_CRITICAL_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        else
        {
            STRCPY (pHttp->au1KeyString, "TRACE_CRITICAL_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        if ((u4TraceMask & DHCP6_ALL_TRC) == DHCP6_ALL_TRC)
        {
            STRCPY (pHttp->au1KeyString, "TRACE_ALL_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Check);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
        else
        {
            STRCPY (pHttp->au1KeyString, "TRACE_ALL_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UnCheck);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
        }
    }

    D6SrMainUnLock ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*******************************************************************************
*  Function Name : IssProcessDhcp6ServerGlobalPageSet
*  Description   : This function processes the HTTP set request coming for the
*                  Elps Protection Group configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None.
*******************************************************************************/
VOID
IssProcessDhcp6ServerGlobalPageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE TrapOption;
    tSNMP_OCTET_STRING_TYPE TraceInput;
    tSNMP_OCTET_STRING_TYPE TraceOldValue;
    UINT1               au1TraceInput[255];
    UINT1               au1TraceOldInput[255];
    UINT4               u4ErrValue = 0;
    INT4                i4SysLogAdminStatus = 0;
    INT4                i4ListenPort = 0;
    INT4                i4TransmitPort = 0;
    INT4                i4ServerPort = 0;
    INT4                i4DebugValue = 0;
    INT4                i4RetVal = 0;
    UINT1               u1TrapOption = 0;

    MEMSET (&TrapOption, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&TraceOldValue, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    TrapOption.pu1_OctetList = &u1TrapOption;
    TrapOption.i4_Length = 1;

    MEMSET (au1TraceOldInput, 0, sizeof (au1TraceOldInput));
    TraceOldValue.pu1_OctetList = au1TraceOldInput;

    MEMSET (au1TraceInput, 0, sizeof (au1TraceInput));
    TraceInput.pu1_OctetList = au1TraceInput;

    WebnmRegisterLock (pHttp, D6SrMainLock, D6SrMainUnLock);
    D6SrMainLock ();

    STRCPY (pHttp->au1Name, "TRAP_ADMIN_CONTROL");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        u1TrapOption = (UINT1) ATOI (pHttp->au1Value);
        TrapOption.i4_Length = (INT4) STRLEN (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "SYSLOG_ADMIN_STATUS");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4SysLogAdminStatus = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "LISTEN_PORT");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4ListenPort = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "TRANSMIT_PORT");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4TransmitPort = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "SERVER_PORT");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4ServerPort = ATOI (pHttp->au1Value);
    }

    i4DebugValue = 0;
    STRCPY (pHttp->au1Name, "DEBUG_TRACE");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DebugValue = ATOI (pHttp->au1Value);
        if (i4DebugValue == 1)
        {
            STRNCPY (au1TraceInput, "enable", STRLEN ("enable"));
        }
        else
        {
            STRNCPY (au1TraceInput, "disable", STRLEN ("disable"));
        }
    }

    i4DebugValue = 0;
    STRCPY (pHttp->au1Name, "TRACE_INIT_SHUT");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DebugValue = ATOI (pHttp->au1Value);
        if (i4DebugValue == 1)
            STRNCAT (au1TraceInput, " init-shut", STRLEN (" init-shut"));
    }

    i4DebugValue = 0;
    STRCPY (pHttp->au1Name, "TRACE_MGMT");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DebugValue = ATOI (pHttp->au1Value);
        if (i4DebugValue == 1)
            STRNCAT (au1TraceInput, " mgmt", STRLEN (" mgmt"));
    }

    i4DebugValue = 0;
    STRCPY (pHttp->au1Name, "TRACE_CTRL");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DebugValue = ATOI (pHttp->au1Value);
        if (i4DebugValue == 1)
            STRNCAT (au1TraceInput, " ctrl", STRLEN (" ctrl"));
    }

    i4DebugValue = 0;
    STRCPY (pHttp->au1Name, "TRACE_PKTDUMP");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DebugValue = ATOI (pHttp->au1Value);
        if (i4DebugValue == 1)
            STRNCAT (au1TraceInput, " pkt-dump", STRLEN (" pkt-dump"));
    }

    i4DebugValue = 0;
    STRCPY (pHttp->au1Name, "TRACE_RESOURCE");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DebugValue = ATOI (pHttp->au1Value);
        if (i4DebugValue == 1)
            STRNCAT (au1TraceInput, " resource", STRLEN (" resource"));
    }

    i4DebugValue = 0;
    STRCPY (pHttp->au1Name, "TRACE_ALLFAIL");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DebugValue = ATOI (pHttp->au1Value);
        if (i4DebugValue == 1)
            STRNCAT (au1TraceInput, " all-fail", STRLEN (" all-fail"));
    }

    i4DebugValue = 0;
    STRCPY (pHttp->au1Name, "TRACE_BUFFER");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DebugValue = ATOI (pHttp->au1Value);
        if (i4DebugValue == 1)
            STRNCAT (au1TraceInput, " buffer", STRLEN (" buffer"));
    }

    i4DebugValue = 0;
    STRCPY (pHttp->au1Name, "TRACE_CRITICAL");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DebugValue = ATOI (pHttp->au1Value);
        if (i4DebugValue == 1)
            STRNCAT (au1TraceInput, " critical", STRLEN (" critical"));
    }

    i4DebugValue = 0;
    STRCPY (pHttp->au1Name, "TRACE_ALL");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DebugValue = ATOI (pHttp->au1Value);
        if (i4DebugValue == 1)
            STRNCAT (au1TraceInput, " all", STRLEN (" all"));
    }

    TraceInput.i4_Length = (INT4) STRLEN (au1TraceInput);

    if (TraceInput.i4_Length == 6)
    {

        STRNCPY (au1TraceInput, "disable all", STRLEN ("disable all"));
        TraceInput.i4_Length = (INT4) STRLEN (au1TraceInput);
    }

    if (nmhTestv2FsDhcp6SrvTrapAdminControl (&u4ErrValue,
                                             &TrapOption) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to configure trap admin control");
        return;
    }

    if (nmhSetFsDhcp6SrvTrapAdminControl (&TrapOption) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set trap admin control");
        return;
    }

    if (nmhTestv2FsDhcp6SrvDebugTrace (&u4ErrValue,
                                       &TraceInput) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to configure debug trace");
        return;
    }

    STRNCPY (au1TraceOldInput, "disable all", STRLEN ("disable all"));
    TraceOldValue.i4_Length = (INT4) STRLEN (au1TraceOldInput);

    if (nmhSetFsDhcp6SrvDebugTrace (&TraceOldValue) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set debug trace");
        return;
    }

    if (nmhSetFsDhcp6SrvDebugTrace (&TraceInput) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set debug trace");
        return;
    }

    i4RetVal = 0;
    nmhGetFsDhcp6SrvSysLogAdminStatus (&i4RetVal);
    if (i4RetVal != i4SysLogAdminStatus)
    {
        if (nmhTestv2FsDhcp6SrvSysLogAdminStatus (&u4ErrValue,
                                                  i4SysLogAdminStatus)
            == SNMP_FAILURE)
        {
            D6SrMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure syslog admin status");
            return;
        }

        if (nmhSetFsDhcp6SrvSysLogAdminStatus (i4SysLogAdminStatus)
            == SNMP_FAILURE)
        {
            D6SrMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set syslog admin status");
            return;
        }
    }

    i4RetVal = 0;
    nmhGetFsDhcp6SrvListenPort (&i4RetVal);
    if (i4RetVal != i4ListenPort)
    {
        if (nmhTestv2FsDhcp6SrvListenPort (&u4ErrValue,
                                           i4ListenPort) == SNMP_FAILURE)
        {
            D6SrMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure source port");
            return;
        }

        if (nmhSetFsDhcp6SrvListenPort (i4ListenPort) == SNMP_FAILURE)
        {
            D6SrMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set source port");
            return;
        }
    }

    i4RetVal = 0;
    nmhGetFsDhcp6SrvClientTransmitPort (&i4RetVal);
    if (i4RetVal != i4TransmitPort)
    {
        if (nmhTestv2FsDhcp6SrvClientTransmitPort (&u4ErrValue,
                                                   i4TransmitPort)
            == SNMP_FAILURE)
        {
            D6SrMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure client destination port");
            return;
        }

        if (nmhSetFsDhcp6SrvClientTransmitPort (i4TransmitPort) == SNMP_FAILURE)
        {
            D6SrMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set client destination port");
            return;
        }
    }

    i4RetVal = 0;
    nmhGetFsDhcp6SrvRelayTransmitPort (&i4RetVal);
    if (i4RetVal != i4ServerPort)
    {
        if (nmhTestv2FsDhcp6SrvRelayTransmitPort (&u4ErrValue,
                                                  i4ServerPort) == SNMP_FAILURE)
        {
            D6SrMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure relay destination port");
            return;
        }

        if (nmhSetFsDhcp6SrvRelayTransmitPort (i4ServerPort) == SNMP_FAILURE)
        {
            D6SrMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set realy destination port");
            return;
        }
    }

    D6SrMainUnLock ();
    WebnmUnRegisterLock (pHttp);

    IssProcessDhcp6ServerGlobalPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssProcessDhcp6ServerPoolPage
*  Description   : This function processes the request coming for the 
*                  PG information page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessDhcp6ServerPoolPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessDhcp6ServerPoolPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessDhcp6ServerPoolPageSet (pHttp);
    }
    return;
}

/*******************************************************************************
 *  Function Name : IssProcessDhcp6ServerPoolPageGet
 *  Description   : This function processes the get request for the Elps
 *                  Protection group configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessDhcp6ServerPoolPageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE PoolName;
    tSNMP_OCTET_STRING_TYPE PoolDuid;
    UINT1               au1PoolName[DHCP6_SRV_POOL_NAME_MAX];
    UINT1               au1PoolDuid[DHCP6_SRV_DUID_SIZE_MAX];
    UINT4               u4FreePoolIndex = 0;
    UINT4               u4PoolIndex = 0;
    UINT4               u4NextPoolIndex = 0;
    UINT4               u4Temp = 0;
    INT4                i4RetVal = 0;

    MEMSET (&PoolName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1PoolName, 0, DHCP6_SRV_POOL_NAME_MAX);

    MEMSET (&PoolDuid, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1PoolDuid, 0, DHCP6_SRV_DUID_SIZE_MAX);

    PoolName.pu1_OctetList = au1PoolName;
    PoolName.i4_Length = 0;

    PoolDuid.pu1_OctetList = au1PoolDuid;
    PoolDuid.i4_Length = 0;

    WebnmRegisterLock (pHttp, D6SrMainLock, D6SrMainUnLock);
    D6SrMainLock ();

    pHttp->i4Write = 0;

    /* Pool Free Index */
    STRCPY (pHttp->au1KeyString, "POOL_FREE_INDEX_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    if (nmhGetFsDhcp6SrvPoolTableNextIndex (&u4FreePoolIndex) == SNMP_SUCCESS)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4FreePoolIndex);
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    if (nmhGetFirstIndexFsDhcp6SrvPoolTable (&u4NextPoolIndex) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;

    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        /* Pool Index */
        STRCPY (pHttp->au1KeyString, "POOL_INDEX_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4NextPoolIndex);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Pool Name */
        STRCPY (pHttp->au1KeyString, "POOL_NAME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetFsDhcp6SrvPoolName (u4NextPoolIndex,
                                      &PoolName) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     PoolName.pu1_OctetList);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Pool Preference */
        STRCPY (pHttp->au1KeyString, "POOL_PREFERENCE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6SrvPoolPreference (u4NextPoolIndex,
                                            &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Pool Duid Type */
        STRCPY (pHttp->au1KeyString, "POOL_DUIDTYPE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6SrvPoolDuidType (u4NextPoolIndex,
                                          &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Pool DUID */
        STRCPY (pHttp->au1KeyString, "POOL_DUID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetFsDhcp6SrvPoolDuid (u4NextPoolIndex,
                                      &PoolDuid) == SNMP_SUCCESS)
        {
            D6SrWebPrintDuid (pHttp, PoolDuid.pu1_OctetList,
                              PoolDuid.i4_Length);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Pool If Index */
        STRCPY (pHttp->au1KeyString, "POOL_IFINDEX_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6SrvPoolDuidIfIndex (u4NextPoolIndex,
                                             &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Rowstatus */
        STRCPY (pHttp->au1KeyString, "ROW_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6SrvPoolRowStatus (u4NextPoolIndex,
                                           &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        u4PoolIndex = u4NextPoolIndex;

    }
    while (nmhGetNextIndexFsDhcp6SrvPoolTable (u4PoolIndex,
                                               &u4NextPoolIndex)
           == SNMP_SUCCESS);

    D6SrMainUnLock ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*******************************************************************************
*  Function Name : IssProcessDhcp6ServerPoolPageSet
*  Description   : This function processes the HTTP set request coming for the
*                  Elps Protection Group configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None.
*******************************************************************************/
VOID
IssProcessDhcp6ServerPoolPageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE PoolName;
    UINT1               au1PoolName[DHCP6_SRV_POOL_NAME_MAX];
    UINT4               u4ErrValue = 0;
    UINT4               u4PoolIndex = 0;
    INT4                i4Preference = 0;
    INT4                i4DuidType = 0;
    INT4                i4DuidIfIndex = 1;
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = 0;

    MEMSET (&PoolName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1PoolName, 0, DHCP6_SRV_POOL_NAME_MAX);

    PoolName.pu1_OctetList = au1PoolName;
    PoolName.i4_Length = 0;

    WebnmRegisterLock (pHttp, D6SrMainLock, D6SrMainUnLock);
    D6SrMainLock ();

    STRCPY (pHttp->au1Name, "POOL_INDEX");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        u4PoolIndex = (UINT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "POOL_NAME");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        PoolName.i4_Length = (INT4) STRLEN (pHttp->au1Value);
        MEMCPY (PoolName.pu1_OctetList, pHttp->au1Value,
                MEM_MAX_BYTES (PoolName.i4_Length, DHCP6_SRV_POOL_NAME_MAX));
    }

    STRCPY (pHttp->au1Name, "POOL_PREFERENCE");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4Preference = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "POOL_DUID_TYPE");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DuidType = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "POOL_DUID_IFINDEX");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4DuidIfIndex = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "ACTION");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        /* Three types of ACTION will be performed.(ADD and APPLY)
         * Actions other than this will result in a failure and will throw 
         * error */
        if (STRCMP (pHttp->au1Value, "Add") == 0)
        {
            /* Set Interface table rowstatus as Create And Wait */
            i4RetVal = nmhGetFsDhcp6SrvPoolRowStatus (u4PoolIndex,
                                                      &i4RowStatus);
            if (i4RetVal != SNMP_SUCCESS)
            {
                if (nmhTestv2FsDhcp6SrvPoolRowStatus (&u4ErrValue, u4PoolIndex,
                                                      CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    D6SrMainUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                                  "needed to create Pool table");
                    return;
                }
                if (nmhSetFsDhcp6SrvPoolRowStatus (u4PoolIndex,
                                                   CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    D6SrMainUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Entry creation failed");
                    return;
                }
            }
            else
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Entry Already Exists");
                return;
            }
        }
        if (STRCMP (pHttp->au1Value, "Apply") == 0)
        {
            if (nmhTestv2FsDhcp6SrvPoolRowStatus (&u4ErrValue, u4PoolIndex,
                                                  NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                              "needed to create interface table");
                return;
            }
            if (nmhSetFsDhcp6SrvPoolRowStatus (u4PoolIndex, NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Entry creation failed");
                return;
            }
        }
        if (STRCMP (pHttp->au1Value, "Delete") == 0)
        {
            /* Delete the Rowstatus */
            if (nmhTestv2FsDhcp6SrvPoolRowStatus (&u4ErrValue, u4PoolIndex,
                                                  DESTROY) == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                              " needed to delete");
                return;
            }
            if (nmhSetFsDhcp6SrvPoolRowStatus (u4PoolIndex,
                                               DESTROY) == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                return;
            }

            D6SrMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessDhcp6ServerPoolPageGet (pHttp);
            return;
        }
    }
    else
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Input");
        return;
    }

    if (nmhTestv2FsDhcp6SrvPoolName (&u4ErrValue, u4PoolIndex,
                                     &PoolName) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to configure Pool Name");
        return;
    }

    if (nmhSetFsDhcp6SrvPoolName (u4PoolIndex, &PoolName) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set Pool Name");
        return;
    }

    i4RetVal = 0;
    nmhGetFsDhcp6SrvPoolPreference (u4PoolIndex, &i4RetVal);
    if (i4RetVal != i4Preference)
    {
        if (nmhTestv2FsDhcp6SrvPoolPreference (&u4ErrValue, u4PoolIndex,
                                               i4Preference) == SNMP_FAILURE)
        {
            D6SrMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure Pool Preference");
            return;
        }

        if (nmhSetFsDhcp6SrvPoolPreference (u4PoolIndex,
                                            i4Preference) == SNMP_FAILURE)
        {
            D6SrMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set Pool Preference");
            return;
        }
    }

    i4RetVal = 0;
    nmhGetFsDhcp6SrvPoolDuidType (u4PoolIndex, &i4RetVal);
    if (nmhTestv2FsDhcp6SrvPoolDuidType (&u4ErrValue, u4PoolIndex,
                                         i4DuidType) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to configure Pool Duid Type");
        return;
    }

    if (nmhSetFsDhcp6SrvPoolDuidType (u4PoolIndex, i4DuidType) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set Pool Duid Type");
        return;
    }

    i4RetVal = 0;
    nmhGetFsDhcp6SrvPoolDuidIfIndex (u4PoolIndex, &i4RetVal);
    if (i4RetVal != i4DuidIfIndex)
    {
        if (nmhTestv2FsDhcp6SrvPoolDuidIfIndex (&u4ErrValue, u4PoolIndex,
                                                i4DuidIfIndex) == SNMP_FAILURE)
        {
            D6SrMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure Pool IfIndex");
            return;
        }

        if (nmhSetFsDhcp6SrvPoolDuidIfIndex (u4PoolIndex,
                                             i4DuidIfIndex) == SNMP_FAILURE)
        {
            D6SrMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set Pool IfIndex");
            return;
        }
    }

    if (nmhTestv2FsDhcp6SrvPoolRowStatus (&u4ErrValue, u4PoolIndex,
                                          ACTIVE) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                      " needed to activate");
        return;
    }
    if (nmhSetFsDhcp6SrvPoolRowStatus (u4PoolIndex, ACTIVE) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Entry creation failed");
        return;
    }

    D6SrMainUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessDhcp6ServerPoolPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssProcessDhcp6ServerIfPage
*  Description   : This function processes the request coming for the 
*                  PG information page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessDhcp6ServerIfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessDhcp6ServerIfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessDhcp6ServerIfPageSet (pHttp);
    }
    return;
}

/*******************************************************************************
 *  Function Name : IssProcessDhcp6ServerIfPageGet
 *  Description   : This function processes the get request for the Elps
 *                  Protection group configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessDhcp6ServerIfPageGet (tHttp * pHttp)
{
    UINT1               au1String[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4Temp = 0;
    INT1               *pi1IfAlias;
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4RetVal = 0;

    WebnmRegisterLock (pHttp, D6SrMainLock, D6SrMainUnLock);
    D6SrMainLock ();

    pHttp->i4Write = 0;
    STRCPY (pHttp->au1KeyString, "<! IP INTERFACES>");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    IssPrintIpInterfaces (pHttp, 0, 0);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    if (nmhGetFirstIndexFsDhcp6SrvIfTable (&i4NextIfIndex) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;

    i4IfIndex = 0;

    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        /* Interface Index */
        MEMSET (au1String, 0, CFA_MAX_PORT_NAME_LENGTH);
        pi1IfAlias = (INT1 *) &au1String[0];

        STRCPY (pHttp->au1KeyString, "ALIAS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        CfaCliGetIfName ((UINT4) i4NextIfIndex, pi1IfAlias);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", (CHR1 *) pi1IfAlias);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Pool Index */
        STRCPY (pHttp->au1KeyString, "POOL_INDEX_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6SrvIfPool (i4NextIfIndex, &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Conter Reset */
        STRCPY (pHttp->au1KeyString, "COUNTER_RESET_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6SrvIfCounterReset (i4NextIfIndex,
                                            &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Rowstatus */
        STRCPY (pHttp->au1KeyString, "ROW_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6SrvIfRowStatus (i4NextIfIndex,
                                         &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        i4IfIndex = i4NextIfIndex;
    }
    while (nmhGetNextIndexFsDhcp6SrvIfTable (i4IfIndex,
                                             &i4NextIfIndex) == SNMP_SUCCESS);

    D6SrMainUnLock ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*******************************************************************************
*  Function Name : IssProcessDhcp6ServerIfPageSet
*  Description   : This function processes the HTTP set request coming for the
*                  Elps Protection Group configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None.
*******************************************************************************/
VOID
IssProcessDhcp6ServerIfPageSet (tHttp * pHttp)
{
    UINT4               u4ErrValue = 0;
    INT4                i4IfIndex = 0;
    UINT1               i4PoolIndex = 0;
    UINT1               i4IfCounterRest = 0;
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = 0;

    WebnmRegisterLock (pHttp, D6SrMainLock, D6SrMainUnLock);
    D6SrMainLock ();

    STRCPY (pHttp->au1Name, "VLAN_ID");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4IfIndex = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "POOL_INDEX");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4PoolIndex = (UINT1) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "COUNTER_RESET");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4IfCounterRest = (UINT1) ATOI (pHttp->au1Value);
    }

    if ((i4IfIndex < 1) || (i4IfIndex > CFA_MAX_INTERFACES_IN_SYS))
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Interface Index out of range");
        return;
    }
    STRCPY (pHttp->au1Name, "ACTION");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        /* Three types of ACTION will be performed.(ADD and APPLY)
         * Actions other than this will result in a failure and will throw 
         * error */
        if (STRCMP (pHttp->au1Value, "Add") == 0)
        {
            /* Set Interface table rowstatus as Create And Wait */
            i4RetVal = nmhGetFsDhcp6SrvIfRowStatus (i4IfIndex, &i4RowStatus);
            if (i4RetVal != SNMP_SUCCESS)
            {
                if (nmhTestv2FsDhcp6SrvIfRowStatus (&u4ErrValue, i4IfIndex,
                                                    CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    D6SrMainUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                                  "needed to create interface table");
                    return;
                }
                if (nmhSetFsDhcp6SrvIfRowStatus (i4IfIndex,
                                                 CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    D6SrMainUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Entry creation failed");
                    return;
                }
            }
            else
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Entry Already Exists");
                return;
            }
        }

        if (STRCMP (pHttp->au1Value, "Apply") == 0)
        {
            if (nmhTestv2FsDhcp6SrvIfRowStatus (&u4ErrValue, i4IfIndex,
                                                NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                              "needed to create interface table");
                return;
            }
            if (nmhSetFsDhcp6SrvIfRowStatus (i4IfIndex, NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Entry creation failed");
                return;
            }
        }
        if (STRCMP (pHttp->au1Value, "Delete") == 0)
        {
            /* Delete the Rowstatus */
            if (nmhTestv2FsDhcp6SrvIfRowStatus (&u4ErrValue, i4IfIndex,
                                                DESTROY) == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                              " needed to delete");
                return;
            }
            if (nmhSetFsDhcp6SrvIfRowStatus (i4IfIndex,
                                             DESTROY) == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                return;
            }

            D6SrMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessDhcp6ServerIfPageGet (pHttp);
            return;
        }
    }
    else
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Input");
        return;
    }

    i4RetVal = 0;
    nmhGetFsDhcp6SrvIfPool (i4IfIndex, &i4RetVal);
    if ((i4RetVal != i4PoolIndex) && (i4PoolIndex != 0))
    {
        if (nmhTestv2FsDhcp6SrvIfPool (&u4ErrValue, i4IfIndex,
                                       i4PoolIndex) == SNMP_FAILURE)
        {
            D6SrMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure pool Index");
            return;
        }

        if (nmhSetFsDhcp6SrvIfPool (i4IfIndex, i4PoolIndex) == SNMP_FAILURE)
        {
            D6SrMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set pool Index");
            return;
        }
    }

    i4RetVal = 0;
    nmhGetFsDhcp6SrvIfCounterReset (i4IfIndex, &i4RetVal);
    if (i4RetVal != i4IfCounterRest)
    {
        if (nmhTestv2FsDhcp6SrvIfCounterReset (&u4ErrValue, i4IfIndex,
                                               i4IfCounterRest) == SNMP_FAILURE)
        {
            D6SrMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure counter reset");
            return;
        }

        if (nmhSetFsDhcp6SrvIfCounterReset (i4IfIndex,
                                            i4IfCounterRest) == SNMP_FAILURE)
        {
            D6SrMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set counter reset");
            return;
        }
    }

    if (nmhTestv2FsDhcp6SrvIfRowStatus (&u4ErrValue, i4IfIndex,
                                        ACTIVE) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                      " needed to activate");
        return;
    }
    if (nmhSetFsDhcp6SrvIfRowStatus (i4IfIndex, ACTIVE) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Entry creation failed");
        return;
    }

    D6SrMainUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessDhcp6ServerIfPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssProcessDhcp6ServerClientPage
*  Description   : This function processes the request coming for the 
*                  PG information page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessDhcp6ServerClientPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessDhcp6ServerClientPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessDhcp6ServerClientPageSet (pHttp);
    }
    return;
}

/*******************************************************************************
 *  Function Name : IssProcessDhcp6ServerClientPageGet
 *  Description   : This function processes the get request for the Elps
 *                  Protection group configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessDhcp6ServerClientPageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE ClientID;
    UINT1               au1ClientID[DHCP6_SRV_DUID_SIZE_MAX];
    UINT4               u4FreeClientIndex = 0;
    UINT4               u4ClientIndex = 0;
    UINT4               u4NextClientIndex = 0;
    UINT4               u4Temp = 0;
    UINT4               u4RetVal = 0;
    INT4                i4RetVal = 0;

    MEMSET (&ClientID, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    WebnmRegisterLock (pHttp, D6SrMainLock, D6SrMainUnLock);
    D6SrMainLock ();

    pHttp->i4Write = 0;

    /* Client Free Index */
    STRCPY (pHttp->au1KeyString, "CLIENT_FREE_INDEX_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    if (nmhGetFsDhcp6SrvClientTableNextIndex (&u4FreeClientIndex)
        == SNMP_SUCCESS)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4FreeClientIndex);
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    if (nmhGetFirstIndexFsDhcp6SrvClientTable (&u4NextClientIndex)
        == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;

    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        MEMSET (&au1ClientID, 0, DHCP6_SRV_DUID_SIZE_MAX);

        ClientID.pu1_OctetList = au1ClientID;

        /* Client Index */
        STRCPY (pHttp->au1KeyString, "CLIENT_INDEX_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4NextClientIndex);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Client ID */
        STRCPY (pHttp->au1KeyString, "CLIENT_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetFsDhcp6SrvClientId (u4NextClientIndex,
                                      &ClientID) == SNMP_SUCCESS)
        {
            if (ClientID.i4_Length != 0)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         ClientID.pu1_OctetList);
            }
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Client ID Type */
        STRCPY (pHttp->au1KeyString, "CLIENT_ID_TYPE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6SrvClientIdType (u4NextClientIndex,
                                          &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Client Realm */
        STRCPY (pHttp->au1KeyString, "CLIENT_REALM_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4RetVal = 0;
        if (nmhGetFsDhcp6SrvClientRealm (u4NextClientIndex,
                                         &u4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Rowstatus */
        STRCPY (pHttp->au1KeyString, "ROW_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6SrvClientRowStatus (u4NextClientIndex,
                                             &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        u4ClientIndex = u4NextClientIndex;

    }
    while (nmhGetNextIndexFsDhcp6SrvClientTable (u4ClientIndex,
                                                 &u4NextClientIndex)
           == SNMP_SUCCESS);

    D6SrMainUnLock ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*******************************************************************************
*  Function Name : IssProcessDhcp6ServerClientPageSet
*  Description   : This function processes the HTTP set request coming for the
*                  Elps Protection Group configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None.
*******************************************************************************/
VOID
IssProcessDhcp6ServerClientPageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE ClientID;
    UINT1               au1ClientID[DHCP6_SRV_DUID_SIZE_MAX];
    UINT4               u4ErrValue = 0;
    UINT4               u4ClientIndex = 0;
    UINT4               u4ClientRealm = 0;
    UINT4               u4RetVal = 0;
    UINT1               i4ClientIdType = 0;
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = 0;

    MEMSET (&ClientID, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1ClientID, 0, DHCP6_SRV_DUID_SIZE_MAX);

    ClientID.pu1_OctetList = au1ClientID;
    ClientID.i4_Length = 0;

    WebnmRegisterLock (pHttp, D6SrMainLock, D6SrMainUnLock);
    D6SrMainLock ();

    STRCPY (pHttp->au1Name, "CLIENT_INDEX");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        u4ClientIndex = (UINT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "CLIENT_ID");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        ClientID.i4_Length = (INT4) STRLEN (pHttp->au1Value);
        MEMCPY (ClientID.pu1_OctetList, pHttp->au1Value,
                MEM_MAX_BYTES (ClientID.i4_Length, DHCP6_SRV_DUID_SIZE_MAX));
    }

    STRCPY (pHttp->au1Name, "CLIENT_ID_TYPE");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4ClientIdType = (UINT1) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "CLIENT_REALM");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        u4ClientRealm = (UINT4) ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "ACTION");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        /* Three types of ACTION will be performed.(ADD and APPLY)
         * Actions other than this will result in a failure and will throw 
         * error */
        if (STRCMP (pHttp->au1Value, "Add") == 0)
        {
            /* Set Interface table rowstatus as Create And Wait */
            i4RetVal = nmhGetFsDhcp6SrvClientRowStatus (u4ClientIndex,
                                                        &i4RowStatus);
            if (i4RetVal != SNMP_SUCCESS)
            {
                if (nmhTestv2FsDhcp6SrvClientRowStatus (&u4ErrValue,
                                                        u4ClientIndex,
                                                        CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    D6SrMainUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                                  "needed to create Client table");
                    return;
                }
                if (nmhSetFsDhcp6SrvClientRowStatus (u4ClientIndex,
                                                     CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    D6SrMainUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Entry creation failed");
                    return;
                }
            }
            else
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Entry Already Exists");
                return;
            }
        }

        if (STRCMP (pHttp->au1Value, "Apply") == 0)
        {
            if (nmhTestv2FsDhcp6SrvClientRowStatus (&u4ErrValue, u4ClientIndex,
                                                    NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                              "needed to create Client table");
                return;
            }
            if (nmhSetFsDhcp6SrvClientRowStatus (u4ClientIndex, NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Entry creation failed");
                return;
            }
        }
        if (STRCMP (pHttp->au1Value, "Delete") == 0)
        {
            /* Delete the Rowstatus */
            if (nmhTestv2FsDhcp6SrvClientRowStatus (&u4ErrValue, u4ClientIndex,
                                                    DESTROY) == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                              " needed to delete");
                return;
            }
            if (nmhSetFsDhcp6SrvClientRowStatus (u4ClientIndex,
                                                 DESTROY) == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                return;
            }

            D6SrMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessDhcp6ServerClientPageGet (pHttp);
            return;
        }
    }
    else
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Input");
        return;
    }

    if (nmhTestv2FsDhcp6SrvClientId (&u4ErrValue, u4ClientIndex,
                                     &ClientID) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to configure Client ID");
        return;
    }

    if (nmhSetFsDhcp6SrvClientId (u4ClientIndex, &ClientID) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set Client ID");
        return;
    }

    i4RetVal = 0;
    nmhGetFsDhcp6SrvClientIdType (u4ClientIndex, &i4RetVal);
    if (i4RetVal != i4ClientIdType)
    {
        if (nmhTestv2FsDhcp6SrvClientIdType (&u4ErrValue, u4ClientIndex,
                                             i4ClientIdType) == SNMP_FAILURE)
        {
            D6SrMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure Client ID Type");
            return;
        }

        if (nmhSetFsDhcp6SrvClientIdType (u4ClientIndex,
                                          i4ClientIdType) == SNMP_FAILURE)
        {
            D6SrMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set Client ID");
            return;
        }
    }

    u4RetVal = 0;
    nmhGetFsDhcp6SrvClientRealm (u4ClientIndex, &u4RetVal);
    if (u4RetVal != u4ClientRealm)
    {
        if (nmhTestv2FsDhcp6SrvClientRealm (&u4ErrValue, u4ClientIndex,
                                            u4ClientRealm) == SNMP_FAILURE)
        {
            D6SrMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure Client ID Realm");
            return;
        }

        if (nmhSetFsDhcp6SrvClientRealm (u4ClientIndex,
                                         u4ClientRealm) == SNMP_FAILURE)
        {
            D6SrMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set Client Realm ID");
            return;
        }
    }

    if (nmhTestv2FsDhcp6SrvClientRowStatus (&u4ErrValue, u4ClientIndex,
                                            ACTIVE) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                      " needed to activate");
        return;
    }
    if (nmhSetFsDhcp6SrvClientRowStatus (u4ClientIndex, ACTIVE) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Entry creation failed");
        return;
    }

    D6SrMainUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessDhcp6ServerClientPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssProcessDhcp6ServerRealmPage
*  Description   : This function processes the request coming for the 
*                  PG information page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessDhcp6ServerRealmPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessDhcp6ServerRealmPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessDhcp6ServerRealmPageSet (pHttp);
    }
    return;
}

/*******************************************************************************
 *  Function Name : IssProcessDhcp6ServerRealmPageGet
 *  Description   : This function processes the get request for the Elps
 *                  Protection group configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessDhcp6ServerRealmPageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE RealmName;
    UINT1               au1RealmName[DHCP6_SRV_DUID_SIZE_MAX];
    UINT4               u4FreeRealmIndex = 0;
    UINT4               u4RealmIndex = 0;
    UINT4               u4NextRealmIndex = 0;
    UINT4               u4Temp = 0;
    INT4                i4RetVal = 0;

    MEMSET (&RealmName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1RealmName, 0, DHCP6_SRV_DUID_SIZE_MAX);

    RealmName.pu1_OctetList = au1RealmName;
    RealmName.i4_Length = 0;

    WebnmRegisterLock (pHttp, D6SrMainLock, D6SrMainUnLock);
    D6SrMainLock ();

    pHttp->i4Write = 0;

    /* Realm Free Index */
    STRCPY (pHttp->au1KeyString, "REALM_FREE_INDEX_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    if (nmhGetFsDhcp6SrvRealmTableNextIndex (&u4FreeRealmIndex) == SNMP_SUCCESS)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4FreeRealmIndex);
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    if (nmhGetFirstIndexFsDhcp6SrvRealmTable (&u4NextRealmIndex)
        == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;

    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        /* Realm Index */
        STRCPY (pHttp->au1KeyString, "REALM_INDEX_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4NextRealmIndex);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Realm Name */
        STRCPY (pHttp->au1KeyString, "REALM_NAME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetFsDhcp6SrvRealmName (u4NextRealmIndex,
                                       &RealmName) == SNMP_SUCCESS)
        {
            if (RealmName.i4_Length != 0)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         RealmName.pu1_OctetList);
            }
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Rowstatus */
        STRCPY (pHttp->au1KeyString, "ROW_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6SrvRealmRowStatus (u4NextRealmIndex,
                                            &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        u4RealmIndex = u4NextRealmIndex;

    }
    while (nmhGetNextIndexFsDhcp6SrvRealmTable (u4RealmIndex,
                                                &u4NextRealmIndex)
           == SNMP_SUCCESS);

    D6SrMainUnLock ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*******************************************************************************
*  Function Name : IssProcessDhcp6ServerRealmPageSet
*  Description   : This function processes the HTTP set request coming for the
*                  Elps Protection Group configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None.
*******************************************************************************/
VOID
IssProcessDhcp6ServerRealmPageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE RealmName;
    UINT1               au1RealmName[DHCP6_SRV_DUID_SIZE_MAX];
    UINT4               u4ErrValue = 0;
    UINT4               u4RealmIndex = 0;
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = 0;

    MEMSET (&RealmName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1RealmName, 0, DHCP6_SRV_DUID_SIZE_MAX);

    RealmName.pu1_OctetList = au1RealmName;
    RealmName.i4_Length = 0;

    WebnmRegisterLock (pHttp, D6SrMainLock, D6SrMainUnLock);
    D6SrMainLock ();

    STRCPY (pHttp->au1Name, "REALM_INDEX");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        u4RealmIndex = (UINT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "REALM_NAME");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        RealmName.i4_Length = (INT4) STRLEN (pHttp->au1Value);
        MEMCPY (RealmName.pu1_OctetList, pHttp->au1Value,
                MEM_MAX_BYTES (RealmName.i4_Length, DHCP6_SRV_DUID_SIZE_MAX));
    }

    STRCPY (pHttp->au1Name, "ACTION");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        /* Three types of ACTION will be performed.(ADD and APPLY)
         * Actions other than this will result in a failure and will throw 
         * error */
        if (STRCMP (pHttp->au1Value, "Add") == 0)
        {
            /* Set Interface table rowstatus as Create And Wait */
            i4RetVal = nmhGetFsDhcp6SrvRealmRowStatus (u4RealmIndex,
                                                       &i4RowStatus);
            if (i4RetVal != SNMP_SUCCESS)
            {
                if (nmhTestv2FsDhcp6SrvRealmRowStatus (&u4ErrValue,
                                                       u4RealmIndex,
                                                       CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    D6SrMainUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                                  "needed to create Realm table");
                    return;
                }
                if (nmhSetFsDhcp6SrvRealmRowStatus (u4RealmIndex,
                                                    CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    D6SrMainUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Entry creation failed");
                    return;
                }
            }
            else
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Entry Already Exists");
                return;
            }
        }

        if (STRCMP (pHttp->au1Value, "Apply") == 0)
        {
            if (nmhTestv2FsDhcp6SrvRealmRowStatus (&u4ErrValue, u4RealmIndex,
                                                   NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                              "needed to create Realm table");
                return;
            }
            if (nmhSetFsDhcp6SrvRealmRowStatus (u4RealmIndex, NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Entry creation failed");
                return;
            }
        }
        if (STRCMP (pHttp->au1Value, "Delete") == 0)
        {
            /* Delete the Rowstatus */
            if (nmhTestv2FsDhcp6SrvRealmRowStatus (&u4ErrValue, u4RealmIndex,
                                                   DESTROY) == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                              " needed to delete");
                return;
            }
            if (nmhSetFsDhcp6SrvRealmRowStatus (u4RealmIndex,
                                                DESTROY) == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                return;
            }

            D6SrMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessDhcp6ServerRealmPageGet (pHttp);
            return;
        }
    }
    else
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Input");
        return;
    }

    if (nmhTestv2FsDhcp6SrvRealmName (&u4ErrValue, u4RealmIndex,
                                      &RealmName) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to configure Realm Name");
        return;
    }

    if (nmhSetFsDhcp6SrvRealmName (u4RealmIndex, &RealmName) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set Realm Name");
        return;
    }

    if (nmhTestv2FsDhcp6SrvRealmRowStatus (&u4ErrValue, u4RealmIndex,
                                           ACTIVE) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                      " needed to activate");
        return;
    }
    if (nmhSetFsDhcp6SrvRealmRowStatus (u4RealmIndex, ACTIVE) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Entry creation failed");
        return;
    }

    D6SrMainUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessDhcp6ServerRealmPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssProcessDhcp6ServerKeyPage
*  Description   : This function processes the request coming for the 
*                  PG information page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessDhcp6ServerKeyPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessDhcp6ServerKeyPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessDhcp6ServerKeyPageSet (pHttp);
    }
    return;
}

/*******************************************************************************
 *  Function Name : IssProcessDhcp6ServerKeyPageGet
 *  Description   : This function processes the get request for the Elps
 *                  Protection group configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessDhcp6ServerKeyPageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE KeyName;
    UINT1               au1KeyName[DHCP6_SRV_KEY_SIZE_MAX];
    UINT4               u4FreeKeyIndex = 0;
    UINT4               u4RealmIndex = 0;
    UINT4               u4NextRealmIndex = 0;
    UINT4               u4KeyIndex = 0;
    UINT4               u4NextKeyIndex = 0;
    UINT4               u4Temp = 0;
    INT4                i4RetVal = 0;

    MEMSET (&KeyName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1KeyName, 0, DHCP6_SRV_KEY_SIZE_MAX);

    KeyName.pu1_OctetList = au1KeyName;
    KeyName.i4_Length = 0;

    WebnmRegisterLock (pHttp, D6SrMainLock, D6SrMainUnLock);
    D6SrMainLock ();

    pHttp->i4Write = 0;

    STRCPY (pHttp->au1Name, "SELECT_REALM_INDEX");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4RealmIndex = (UINT4) ATOI (pHttp->au1Value);

    /* Realm Index */
    IssPrintAvailableRealmIndex (pHttp);

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    /* Key Free Index */
    STRCPY (pHttp->au1KeyString, "KEY_FREE_INDEX_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsDhcp6SrvRealmKeyTableNextIndex (u4RealmIndex, &u4FreeKeyIndex);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4FreeKeyIndex);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    u4RealmIndex = 0;

    if (nmhGetFirstIndexFsDhcp6SrvKeyTable (&u4NextRealmIndex,
                                            &u4NextKeyIndex) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;

    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        /* Realm Index */
        STRCPY (pHttp->au1KeyString, "REALM_INDEX_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4NextRealmIndex);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Key Index */
        STRCPY (pHttp->au1KeyString, "KEY_INDEX_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4NextKeyIndex);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Key Name */
        STRCPY (pHttp->au1KeyString, "KEY_NAME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetFsDhcp6SrvKey (u4NextRealmIndex, u4NextKeyIndex,
                                 &KeyName) == SNMP_SUCCESS)
        {
            if (KeyName.i4_Length != 0)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         KeyName.pu1_OctetList);
            }
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Rowstatus */
        STRCPY (pHttp->au1KeyString, "ROW_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6SrvKeyRowStatus (u4NextRealmIndex, u4NextKeyIndex,
                                          &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        u4RealmIndex = u4NextRealmIndex;
        u4KeyIndex = u4NextKeyIndex;

    }
    while (nmhGetNextIndexFsDhcp6SrvKeyTable (u4RealmIndex,
                                              &u4NextRealmIndex,
                                              u4KeyIndex,
                                              &u4NextKeyIndex) == SNMP_SUCCESS);

    D6SrMainUnLock ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*******************************************************************************
*  Function Name : IssProcessDhcp6ServerKeyPageSet
*  Description   : This function processes the HTTP set request coming for the
*                  Elps Protection Group configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None.
*******************************************************************************/
VOID
IssProcessDhcp6ServerKeyPageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE KeyName;
    UINT1               au1KeyName[DHCP6_SRV_KEY_SIZE_MAX];
    UINT4               u4ErrValue = 0;
    UINT4               u4RealmIndex = 0;
    UINT4               u4KeyIndex = 0;
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = 0;

    MEMSET (&KeyName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1KeyName, 0, DHCP6_SRV_KEY_SIZE_MAX);

    KeyName.pu1_OctetList = au1KeyName;
    KeyName.i4_Length = 0;

    WebnmRegisterLock (pHttp, D6SrMainLock, D6SrMainUnLock);
    D6SrMainLock ();

    STRCPY (pHttp->au1Name, "REALM_INDEX");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        u4RealmIndex = (UINT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "KEY_INDEX");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        u4KeyIndex = (UINT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "KEY_NAME");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        KeyName.i4_Length = (INT4) STRLEN (pHttp->au1Value);
        MEMCPY (KeyName.pu1_OctetList, pHttp->au1Value,
                MEM_MAX_BYTES (KeyName.i4_Length, DHCP6_SRV_KEY_SIZE_MAX));
    }

    STRCPY (pHttp->au1Name, "ACTION");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        /* Three types of ACTION will be performed.(ADD and APPLY)
         * Actions other than this will result in a failure and will throw 
         * error */
        if (STRCMP (pHttp->au1Value, "Add") == 0)
        {
            /* Set Interface table rowstatus as Create And Wait */
            i4RetVal = nmhGetFsDhcp6SrvKeyRowStatus (u4RealmIndex,
                                                     u4KeyIndex, &i4RowStatus);
            if (i4RetVal != SNMP_SUCCESS)
            {
                if (nmhTestv2FsDhcp6SrvKeyRowStatus (&u4ErrValue,
                                                     u4RealmIndex,
                                                     u4KeyIndex,
                                                     CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    D6SrMainUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                                  "needed to create Key table");
                    return;
                }
                if (nmhSetFsDhcp6SrvKeyRowStatus (u4RealmIndex,
                                                  u4KeyIndex,
                                                  CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    D6SrMainUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Entry creation failed");
                    return;
                }
            }
            else
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Entry Already Exists");
                return;
            }
        }

        if (STRCMP (pHttp->au1Value, "Apply") == 0)
        {
            if (nmhTestv2FsDhcp6SrvKeyRowStatus (&u4ErrValue, u4RealmIndex,
                                                 u4KeyIndex,
                                                 NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                              "needed to create Key table");
                return;
            }
            if (nmhSetFsDhcp6SrvKeyRowStatus (u4RealmIndex,
                                              u4KeyIndex,
                                              NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Entry creation failed");
                return;
            }
        }
        if (STRCMP (pHttp->au1Value, "Delete") == 0)
        {
            /* Delete the Rowstatus */
            if (nmhTestv2FsDhcp6SrvKeyRowStatus (&u4ErrValue, u4RealmIndex,
                                                 u4KeyIndex,
                                                 DESTROY) == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                              " needed to delete");
                return;
            }
            if (nmhSetFsDhcp6SrvKeyRowStatus (u4RealmIndex, u4KeyIndex,
                                              DESTROY) == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                return;
            }

            D6SrMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessDhcp6ServerKeyPageGet (pHttp);
            return;
        }
    }
    else
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Input");
        return;
    }

    if (nmhTestv2FsDhcp6SrvKey (&u4ErrValue, u4RealmIndex,
                                u4KeyIndex, &KeyName) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to configure Key Name");
        return;
    }

    if (nmhSetFsDhcp6SrvKey (u4RealmIndex, u4KeyIndex,
                             &KeyName) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set Key Name");
        return;
    }

    if (nmhTestv2FsDhcp6SrvKeyRowStatus (&u4ErrValue, u4RealmIndex,
                                         u4KeyIndex, ACTIVE) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                      " needed to activate");
        return;
    }
    if (nmhSetFsDhcp6SrvKeyRowStatus (u4RealmIndex, u4KeyIndex,
                                      ACTIVE) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Entry creation failed");
        return;
    }

    D6SrMainUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessDhcp6ServerKeyPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssProcessDhcp6ServerPrefixPage
*  Description   : This function processes the request coming for the 
*                  PG information page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessDhcp6ServerPrefixPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessDhcp6ServerPrefixPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessDhcp6ServerPrefixPageSet (pHttp);
    }
    return;
}

/*******************************************************************************
 *  Function Name : IssProcessDhcp6ServerPrefixPageGet
 *  Description   : This function processes the get request for the Elps
 *                  Protection group configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessDhcp6ServerPrefixPageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE PrefixValue;
    tIp6Addr            Ip6Addr;
    UINT4               u4FreeIndex = 0;
    UINT4               u4Temp = 0;
    INT4                i4PrefixIndex = 0;
    INT4                i4NextIndex = 0;
    INT4                i4ContextId = 0;
    INT4                i4NextId = 0;
    INT4                i4RetVal = 0;

    MEMSET (&PrefixValue, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    PrefixValue.i4_Length = DHCP6_IP6_ADDRESS_SIZE_MAX;
    PrefixValue.pu1_OctetList = (UINT1 *) &Ip6Addr;

    WebnmRegisterLock (pHttp, D6SrMainLock, D6SrMainUnLock);
    D6SrMainLock ();

    pHttp->i4Write = 0;

    /* Prefix Free Index */
    STRCPY (pHttp->au1KeyString, "PREFIX_FREE_INDEX_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    if (nmhGetFsDhcp6SrvIncludePrefixTableNextIndex (&u4FreeIndex)
        == SNMP_SUCCESS)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4FreeIndex);
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    if (nmhGetFirstIndexFsDhcp6SrvIncludePrefixTable (&i4NextId,
                                                      &i4NextIndex)
        == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;

    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        /* Context ID */
        STRCPY (pHttp->au1KeyString, "CONTEXT_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Prefix Index */
        STRCPY (pHttp->au1KeyString, "PREFIX_INDEX_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextIndex);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Prefix Value */
        STRCPY (pHttp->au1KeyString, "PREFIX_VALUE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetFsDhcp6SrvIncludePrefix (i4NextId, i4NextIndex,
                                           &PrefixValue) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     Ip6PrintAddr (&Ip6Addr));
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Prefix Pool */
        STRCPY (pHttp->au1KeyString, "POOL_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6SrvIncludePrefixPool (i4NextId, i4NextIndex,
                                               &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Rowstatus */
        STRCPY (pHttp->au1KeyString, "ROW_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6SrvIncludePrefixRowStatus (i4NextId, i4NextIndex,
                                                    &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        i4ContextId = i4NextId;
        i4PrefixIndex = i4NextIndex;

    }
    while (nmhGetNextIndexFsDhcp6SrvIncludePrefixTable (i4ContextId,
                                                        &i4NextId,
                                                        i4PrefixIndex,
                                                        &i4NextIndex)
           == SNMP_SUCCESS);

    D6SrMainUnLock ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*******************************************************************************
*  Function Name : IssProcessDhcp6ServerPrefixPageSet
*  Description   : This function processes the HTTP set request coming for the
*                  Elps Protection Group configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None.
*******************************************************************************/
VOID
IssProcessDhcp6ServerPrefixPageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE PrefixValue;
    tIp6Addr            Ip6Addr;
    UINT4               u4ErrValue = 0;
    INT4                i4ContextId = 0;
    INT4                i4PrefixIndex = 0;
    INT4                i4PoolId = 0;
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = 0;

    MEMSET (&PrefixValue, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    PrefixValue.i4_Length = DHCP6_IP6_ADDRESS_SIZE_MAX;
    PrefixValue.pu1_OctetList = (UINT1 *) &Ip6Addr;

    WebnmRegisterLock (pHttp, D6SrMainLock, D6SrMainUnLock);
    D6SrMainLock ();

    STRCPY (pHttp->au1Name, "CONTEXT_ID");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4ContextId = ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "PREFIX_INDEX");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4PrefixIndex = ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "PREFIX_VALUE");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        INET_ATON6 (pHttp->au1Value, &Ip6Addr);
        PrefixValue.i4_Length = DHCP6_IP6_ADDRESS_SIZE_MAX;
    }

    STRCPY (pHttp->au1Name, "POOL_ID");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4PoolId = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "ACTION");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        /* Three types of ACTION will be performed.(ADD and APPLY)
         * Actions other than this will result in a failure and will throw 
         * error */
        if (STRCMP (pHttp->au1Value, "Add") == 0)
        {
            /* Set Interface table rowstatus as Create And Wait */
            i4RetVal = nmhGetFsDhcp6SrvIncludePrefixRowStatus (i4ContextId,
                                                               i4PrefixIndex,
                                                               &i4RowStatus);
            if (i4RetVal != SNMP_SUCCESS)
            {
                if (nmhTestv2FsDhcp6SrvIncludePrefixRowStatus (&u4ErrValue,
                                                               i4ContextId,
                                                               i4PrefixIndex,
                                                               CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    D6SrMainUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                                  "needed to create Prefix table");
                    return;
                }
                if (nmhSetFsDhcp6SrvIncludePrefixRowStatus (i4ContextId,
                                                            i4PrefixIndex,
                                                            CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    D6SrMainUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Entry creation failed");
                    return;
                }
            }
            else
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Entry Already Exists");
                return;
            }
        }

        if (STRCMP (pHttp->au1Value, "Apply") == 0)
        {
            if (nmhTestv2FsDhcp6SrvIncludePrefixRowStatus (&u4ErrValue,
                                                           i4ContextId,
                                                           i4PrefixIndex,
                                                           NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                              "needed to create interface table");
                return;
            }
            if (nmhSetFsDhcp6SrvIncludePrefixRowStatus (i4ContextId,
                                                        i4PrefixIndex,
                                                        NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Entry creation failed");
                return;
            }
        }
        if (STRCMP (pHttp->au1Value, "Delete") == 0)
        {
            /* Delete the Rowstatus */
            if (nmhTestv2FsDhcp6SrvIncludePrefixRowStatus (&u4ErrValue,
                                                           i4ContextId,
                                                           i4PrefixIndex,
                                                           DESTROY)
                == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                              " needed to delete");
                return;
            }
            if (nmhSetFsDhcp6SrvIncludePrefixRowStatus (i4ContextId,
                                                        i4PrefixIndex,
                                                        DESTROY)
                == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                return;
            }

            D6SrMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessDhcp6ServerPrefixPageGet (pHttp);
            return;
        }
    }
    else
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Input");
        return;
    }

    if (nmhTestv2FsDhcp6SrvIncludePrefix (&u4ErrValue, i4ContextId,
                                          i4PrefixIndex,
                                          &PrefixValue) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to configure Prefix Value");
        return;
    }

    if (nmhSetFsDhcp6SrvIncludePrefix (i4ContextId, i4PrefixIndex,
                                       &PrefixValue) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set Prefix Value");
        return;
    }

    i4RetVal = 0;
    nmhGetFsDhcp6SrvIncludePrefixPool (i4ContextId, i4PrefixIndex, &i4RetVal);
    if (i4RetVal != i4PoolId)
    {
        if (nmhTestv2FsDhcp6SrvIncludePrefixPool (&u4ErrValue, i4ContextId,
                                                  i4PrefixIndex,
                                                  i4PoolId) == SNMP_FAILURE)
        {
            D6SrMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure Prefix Pool");
            return;
        }

        if (nmhSetFsDhcp6SrvIncludePrefixPool (i4ContextId,
                                               i4PrefixIndex,
                                               i4PoolId) == SNMP_FAILURE)
        {
            D6SrMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set Prefix Pool");
            return;
        }
    }

    if (nmhTestv2FsDhcp6SrvIncludePrefixRowStatus (&u4ErrValue, i4ContextId,
                                                   i4PrefixIndex,
                                                   ACTIVE) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                      " needed to activate");
        return;
    }
    if (nmhSetFsDhcp6SrvIncludePrefixRowStatus (i4ContextId, i4PrefixIndex,
                                                ACTIVE) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Entry creation failed");
        return;
    }

    D6SrMainUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessDhcp6ServerPrefixPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssProcessDhcp6ServerOptionPage
*  Description   : This function processes the request coming for the 
*                  PG information page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessDhcp6ServerOptionPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessDhcp6ServerOptionPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessDhcp6ServerOptionPageSet (pHttp);
    }
    return;
}

/*******************************************************************************
 *  Function Name : IssProcessDhcp6ServerOptionPageGet
 *  Description   : This function processes the get request for the Elps
 *                  Protection group configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessDhcp6ServerOptionPageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE OptionValue;
    UINT1               au1OptionValue[DHCP6_SRV_OPTION_LENGTH_MAX];
    UINT4               u4FreeOptionIndex = 0;
    UINT4               u4OptionIndex = 0;
    UINT4               u4NextPoolIndex = 0;
    UINT4               u4PoolIndex = 0;
    UINT4               u4NextOptionIndex = 0;
    UINT4               u4Temp = 0;
    INT4                i4OptionType = 0;
    INT4                i4RetVal = 0;

    WebnmRegisterLock (pHttp, D6SrMainLock, D6SrMainUnLock);
    D6SrMainLock ();

    pHttp->i4Write = 0;

    STRCPY (pHttp->au1Name, "SELECT_POOL_INDEX");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4PoolIndex = (UINT4) ATOI (pHttp->au1Value);

    /* Pool Index */
    IssPrintAvailablePoolID (pHttp);

    STRCPY (pHttp->au1KeyString, "POOL_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4PoolIndex);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    /* Option Free Index */
    STRCPY (pHttp->au1KeyString, "FREE_OPTION_INDEX_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsDhcp6SrvPoolOptionTableNextIndex (u4PoolIndex, &u4FreeOptionIndex);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4FreeOptionIndex);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    if (nmhGetFirstIndexFsDhcp6SrvOptionTable (&u4NextPoolIndex,
                                               &u4NextOptionIndex)
        == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;

    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        MEMSET (&OptionValue, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (&au1OptionValue, 0, DHCP6_SRV_OPTION_LENGTH_MAX);

        OptionValue.pu1_OctetList = au1OptionValue;

        /* Pool Index */
        STRCPY (pHttp->au1KeyString, "POOL_INDEX_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4NextPoolIndex);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Option Index */
        STRCPY (pHttp->au1KeyString, "OPTION_INDEX_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4NextOptionIndex);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Option Type */
        STRCPY (pHttp->au1KeyString, "OPTION_TYPE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6SrvOptionType (u4NextPoolIndex, u4NextOptionIndex,
                                        &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            i4OptionType = i4RetVal;
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Option Value */
        STRCPY (pHttp->au1KeyString, "OPTION_VALUE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetFsDhcp6SrvOptionValue (u4NextPoolIndex, u4NextOptionIndex,
                                         &OptionValue) == SNMP_SUCCESS)

        {

            if (i4OptionType == DHCP6_ORO_DNS_SERVERS ||
                i4OptionType == DHCP6_ORO_SIP_SERVER_A)
            {
                D6SrWebPrintStringIpAdd (pHttp, OptionValue.pu1_OctetList);
            }
            else
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         OptionValue.pu1_OctetList);
            }
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Option Prefrence */
        STRCPY (pHttp->au1KeyString, "OPTION_PREFERENCE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6SrvOptionPreference (u4NextPoolIndex,
                                              u4NextOptionIndex,
                                              &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Rowstatus */
        STRCPY (pHttp->au1KeyString, "ROW_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6SrvOptionRowStatus (u4NextPoolIndex, u4NextOptionIndex,
                                             &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        u4PoolIndex = u4NextPoolIndex;
        u4OptionIndex = u4NextOptionIndex;

    }
    while (nmhGetNextIndexFsDhcp6SrvOptionTable (u4PoolIndex,
                                                 &u4NextPoolIndex,
                                                 u4OptionIndex,
                                                 &u4NextOptionIndex)
           == SNMP_SUCCESS);

    D6SrMainUnLock ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*******************************************************************************
*  Function Name : IssProcessDhcp6ServerOptionPageSet
*  Description   : This function processes the HTTP set request coming for the
*                  Elps Protection Group configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None.
*******************************************************************************/
VOID
IssProcessDhcp6ServerOptionPageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE OptionValue;
    UINT1               au1OptionValue[DHCP6_SRV_OPTION_LENGTH_MAX];
    UINT4               u4ErrValue = 0;
    UINT4               u4PoolIndex = 0;
    UINT4               u4OptionIndex = 0;
    INT4                i4OptionType = 0;
    INT4                i4OptionPreference = 0;
    INT4                i4OptionLength = 0;
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = 0;
    tIp6Addr            Ipv6Addr;
    MEMSET (&OptionValue, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1OptionValue, 0, DHCP6_SRV_OPTION_LENGTH_MAX);

    OptionValue.pu1_OctetList = au1OptionValue;
    OptionValue.i4_Length = 0;

    WebnmRegisterLock (pHttp, D6SrMainLock, D6SrMainUnLock);
    D6SrMainLock ();

    STRCPY (pHttp->au1Name, "POOL_INDEX");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        u4PoolIndex = (UINT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "OPTION_INDEX");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        u4OptionIndex = (UINT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "OPTION_TYPE");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4OptionType = ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "OPTION_VALUE");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        OptionValue.i4_Length = DHCP6_IP6_ADDRESS_SIZE_MAX;
        if (i4OptionType == DHCP6_ORO_DNS_SERVERS ||
            i4OptionType == DHCP6_ORO_SIP_SERVER_A)
        {

            INET_ATON6 (pHttp->au1Value, &Ipv6Addr.u1_addr);
            MEMCPY (OptionValue.pu1_OctetList, Ipv6Addr.u1_addr,
                    MEM_MAX_BYTES (OptionValue.i4_Length,
                                   DHCP6_SRV_OPTION_LENGTH_MAX));
        }
        else
        {
            MEMCPY (OptionValue.pu1_OctetList, pHttp->au1Value,
                    MEM_MAX_BYTES (OptionValue.i4_Length,
                                   DHCP6_SRV_OPTION_LENGTH_MAX));
        }
        i4OptionLength = OptionValue.i4_Length;
    }
    STRCPY (pHttp->au1Name, "OPTION_PREFERENCE");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4OptionPreference = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "ACTION");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        /* Three types of ACTION will be performed.(ADD and APPLY)
         * Actions other than this will result in a failure and will throw 
         * error */
        if (STRCMP (pHttp->au1Value, "Add") == 0)
        {
            /* Set Interface table rowstatus as Create And Wait */
            i4RetVal = nmhGetFsDhcp6SrvOptionRowStatus (u4PoolIndex,
                                                        u4OptionIndex,
                                                        &i4RowStatus);
            if (i4RetVal != SNMP_SUCCESS)
            {
                if (nmhTestv2FsDhcp6SrvOptionRowStatus (&u4ErrValue,
                                                        u4PoolIndex,
                                                        u4OptionIndex,
                                                        CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    D6SrMainUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                                  "needed to create option table");
                    return;
                }
                if (nmhSetFsDhcp6SrvOptionRowStatus (u4PoolIndex,
                                                     u4OptionIndex,
                                                     CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    D6SrMainUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Entry creation failed");
                    return;
                }
            }
            else
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Entry Already Exists");
                return;
            }
        }

        if (STRCMP (pHttp->au1Value, "Apply") == 0)
        {
            if (nmhTestv2FsDhcp6SrvOptionRowStatus (&u4ErrValue, u4PoolIndex,
                                                    u4OptionIndex,
                                                    NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                              "needed to create option table");
                return;
            }
            if (nmhSetFsDhcp6SrvOptionRowStatus (u4PoolIndex,
                                                 u4OptionIndex,
                                                 NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Entry creation failed");
                return;
            }
        }
        if (STRCMP (pHttp->au1Value, "Delete") == 0)
        {
            /* Delete the Rowstatus */
            if (nmhTestv2FsDhcp6SrvOptionRowStatus (&u4ErrValue, u4PoolIndex,
                                                    u4OptionIndex,
                                                    DESTROY) == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                              " needed to delete");
                return;
            }
            if (nmhSetFsDhcp6SrvOptionRowStatus (u4PoolIndex, u4OptionIndex,
                                                 DESTROY) == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                return;
            }

            D6SrMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessDhcp6ServerOptionPageGet (pHttp);
            return;
        }
    }
    else
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Input");
        return;
    }

    /* Option Type */
    if (nmhTestv2FsDhcp6SrvOptionType (&u4ErrValue, u4PoolIndex,
                                       u4OptionIndex,
                                       i4OptionType) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to configure Option Type");
        return;
    }

    if (nmhSetFsDhcp6SrvOptionType (u4PoolIndex, u4OptionIndex,
                                    i4OptionType) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set option type");
        return;
    }
    /* Option Length */
    if (nmhTestv2FsDhcp6SrvOptionLength (&u4ErrValue, u4PoolIndex,
                                         u4OptionIndex,
                                         i4OptionLength) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to configure Option Length");
        return;
    }

    if (nmhSetFsDhcp6SrvOptionLength (u4PoolIndex, u4OptionIndex,
                                      i4OptionLength) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set option length");
        return;
    }
    /* Option Value */
    if (nmhTestv2FsDhcp6SrvOptionValue (&u4ErrValue, u4PoolIndex,
                                        u4OptionIndex,
                                        &OptionValue) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to configure Option Value");
        return;
    }

    if (nmhSetFsDhcp6SrvOptionValue (u4PoolIndex, u4OptionIndex,
                                     &OptionValue) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set option Value");
        return;
    }

    /* Option Preference */
    if (nmhTestv2FsDhcp6SrvOptionPreference (&u4ErrValue, u4PoolIndex,
                                             u4OptionIndex,
                                             i4OptionPreference)
        == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to configure Option Preference");
        return;
    }

    if (nmhSetFsDhcp6SrvOptionPreference (u4PoolIndex, u4OptionIndex,
                                          i4OptionPreference) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set option Preference");
        return;
    }
    if (nmhTestv2FsDhcp6SrvOptionRowStatus (&u4ErrValue, u4PoolIndex,
                                            u4OptionIndex,
                                            ACTIVE) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                      " needed to activate");
        return;
    }
    if (nmhSetFsDhcp6SrvOptionRowStatus (u4PoolIndex, u4OptionIndex,
                                         ACTIVE) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Entry creation failed");
        return;
    }

    D6SrMainUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessDhcp6ServerOptionPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssProcessDhcp6ServerSubOptionPage
*  Description   : This function processes the request coming for the 
*                  PG information page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessDhcp6ServerSubOptionPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessDhcp6ServerSubOptionPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessDhcp6ServerSubOptionPageSet (pHttp);
    }
    return;
}

/*******************************************************************************
 *  Function Name : IssProcessDhcp6ServerSubOptionPageGet
 *  Description   : This function processes the get request for the Elps
 *                  Protection group configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessDhcp6ServerSubOptionPageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE SubOptionValue;
    UINT1               au1SubOptionValue[DHCP6_SRV_OPTION_LENGTH_MAX];
    UINT4               u4OptionIndex = 0;
    UINT4               u4NextPoolIndex = 0;
    UINT4               u4PoolIndex = 0;
    UINT4               u4NextOptionIndex = 0;
    UINT4               u4SubOptionType = 0;
    UINT4               u4NextSubOptionType = 0;
    UINT4               u4Temp = 0;
    INT4                i4RetVal = 0;

    MEMSET (&SubOptionValue, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1SubOptionValue, 0, DHCP6_SRV_OPTION_LENGTH_MAX);

    WebnmRegisterLock (pHttp, D6SrMainLock, D6SrMainUnLock);
    D6SrMainLock ();

    pHttp->i4Write = 0;

    STRCPY (pHttp->au1Name, "SELECT_POOL_INDEX");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4PoolIndex = (UINT4) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "SELECT_OPTION_INDEX");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4OptionIndex = (UINT4) ATOI (pHttp->au1Value);

    /* Pool Index */
    IssPrintAvailablePoolID (pHttp);

    STRCPY (pHttp->au1KeyString, "POOL_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4PoolIndex);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    /* Option Index */
    IssPrintAvailableOptionID (pHttp, u4PoolIndex, u4OptionIndex);

    if (u4OptionIndex != 0)
    {
        STRCPY (pHttp->au1KeyString, "OPTION_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4OptionIndex);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

        WebnmSendString (pHttp, pHttp->au1KeyString);
    }
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    if (nmhGetFirstIndexFsDhcp6SrvSubOptionTable (&u4NextPoolIndex,
                                                  &u4NextOptionIndex,
                                                  &u4NextSubOptionType)
        == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;

    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        SubOptionValue.pu1_OctetList = au1SubOptionValue;
        SubOptionValue.i4_Length = 0;

        /* Pool Index */
        STRCPY (pHttp->au1KeyString, "POOL_INDEX_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4NextPoolIndex);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));

        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Option Index */
        STRCPY (pHttp->au1KeyString, "OPTION_INDEX_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4NextOptionIndex);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Sub Option Type */
        STRCPY (pHttp->au1KeyString, "SUBOPTION_TYPE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4NextSubOptionType);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Sub Option Value */
        STRCPY (pHttp->au1KeyString, "SUBOPTION_VALUE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetFsDhcp6SrvSubOptionValue (u4NextPoolIndex, u4NextOptionIndex,
                                            u4NextSubOptionType,
                                            &SubOptionValue) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     SubOptionValue.pu1_OctetList);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Rowstatus */
        STRCPY (pHttp->au1KeyString, "ROW_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsDhcp6SrvSubOptionRowStatus (u4NextPoolIndex,
                                                u4NextOptionIndex,
                                                u4NextSubOptionType,
                                                &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        u4PoolIndex = u4NextPoolIndex;
        u4OptionIndex = u4NextOptionIndex;
        u4SubOptionType = u4NextSubOptionType;

    }
    while (nmhGetNextIndexFsDhcp6SrvSubOptionTable (u4PoolIndex,
                                                    &u4NextPoolIndex,
                                                    u4OptionIndex,
                                                    &u4NextOptionIndex,
                                                    u4SubOptionType,
                                                    &u4NextSubOptionType)
           == SNMP_SUCCESS);

    D6SrMainUnLock ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*******************************************************************************
*  Function Name : IssProcessDhcp6ServerSubOptionPageSet
*  Description   : This function processes the HTTP set request coming for the
*                  Elps Protection Group configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None.
*******************************************************************************/
VOID
IssProcessDhcp6ServerSubOptionPageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE SubOptionValue;
    UINT1               au1SubOptionValue[DHCP6_SRV_OPTION_LENGTH_MAX];
    UINT4               u4ErrValue = 0;
    UINT4               u4PoolIndex = 0;
    UINT4               u4OptionIndex = 0;
    UINT4               u4SubOptionType = 0;
    INT4                i4SubOptionLength = 0;
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = 0;

    MEMSET (&SubOptionValue, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1SubOptionValue, 0, DHCP6_SRV_OPTION_LENGTH_MAX);

    SubOptionValue.pu1_OctetList = au1SubOptionValue;
    SubOptionValue.i4_Length = 0;

    WebnmRegisterLock (pHttp, D6SrMainLock, D6SrMainUnLock);
    D6SrMainLock ();

    STRCPY (pHttp->au1Name, "POOL_INDEX");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        u4PoolIndex = (UINT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "OPTION_INDEX");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        u4OptionIndex = (UINT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "SUBOPTION_TYPE");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        u4SubOptionType = (UINT4) ATOI (pHttp->au1Value);
    }
    STRCPY (pHttp->au1Name, "SUBOPTION_VALUE");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        SubOptionValue.i4_Length = (INT4) STRLEN (pHttp->au1Value);
        MEMCPY (SubOptionValue.pu1_OctetList, pHttp->au1Value,
                MEM_MAX_BYTES (SubOptionValue.i4_Length,
                               DHCP6_SRV_OPTION_LENGTH_MAX));
        i4SubOptionLength = SubOptionValue.i4_Length;
    }

    STRCPY (pHttp->au1Name, "ACTION");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        /* Three types of ACTION will be performed.(ADD and APPLY)
         * Actions other than this will result in a failure and will throw 
         * error */
        if (STRCMP (pHttp->au1Value, "Add") == 0)
        {
            /* Set Interface table rowstatus as Create And Wait */
            i4RetVal = nmhGetFsDhcp6SrvSubOptionRowStatus (u4PoolIndex,
                                                           u4OptionIndex,
                                                           u4SubOptionType,
                                                           &i4RowStatus);
            if (i4RetVal != SNMP_SUCCESS)
            {
                if (nmhTestv2FsDhcp6SrvSubOptionRowStatus (&u4ErrValue,
                                                           u4PoolIndex,
                                                           u4OptionIndex,
                                                           u4SubOptionType,
                                                           CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    D6SrMainUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                                  "needed to create sub option table");
                    return;
                }
                if (nmhSetFsDhcp6SrvSubOptionRowStatus (u4PoolIndex,
                                                        u4OptionIndex,
                                                        u4SubOptionType,
                                                        CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    D6SrMainUnLock ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Entry creation failed");
                    return;
                }
            }
            else
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Entry Already Exists");
                return;
            }
        }

        if (STRCMP (pHttp->au1Value, "Apply") == 0)
        {
            if (nmhTestv2FsDhcp6SrvSubOptionRowStatus (&u4ErrValue,
                                                       u4PoolIndex,
                                                       u4OptionIndex,
                                                       u4SubOptionType,
                                                       NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                              "needed to create option table");
                return;
            }
            if (nmhSetFsDhcp6SrvSubOptionRowStatus (u4PoolIndex,
                                                    u4OptionIndex,
                                                    u4SubOptionType,
                                                    NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Entry creation failed");
                return;
            }
        }
        if (STRCMP (pHttp->au1Value, "Delete") == 0)
        {
            /* Delete the Rowstatus */
            if (nmhTestv2FsDhcp6SrvSubOptionRowStatus (&u4ErrValue,
                                                       u4PoolIndex,
                                                       u4OptionIndex,
                                                       u4SubOptionType,
                                                       DESTROY) == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                              " needed to delete");
                return;
            }
            if (nmhSetFsDhcp6SrvSubOptionRowStatus (u4PoolIndex, u4OptionIndex,
                                                    u4SubOptionType,
                                                    DESTROY) == SNMP_FAILURE)
            {
                D6SrMainUnLock ();
                WebnmUnRegisterLock (pHttp);
                return;
            }

            D6SrMainUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssProcessDhcp6ServerSubOptionPageGet (pHttp);
            return;
        }
    }
    else
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Input");
        return;
    }

    /* Option Length */
    if (nmhTestv2FsDhcp6SrvSubOptionLength (&u4ErrValue, u4PoolIndex,
                                            u4OptionIndex,
                                            u4SubOptionType,
                                            i4SubOptionLength) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to configure Sub Option Length");
        return;
    }

    if (nmhSetFsDhcp6SrvSubOptionLength (u4PoolIndex, u4OptionIndex,
                                         u4SubOptionType,
                                         i4SubOptionLength) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set Sub option length");
        return;
    }
    /* Option Value */
    if (nmhTestv2FsDhcp6SrvSubOptionValue (&u4ErrValue, u4PoolIndex,
                                           u4OptionIndex,
                                           u4SubOptionType,
                                           &SubOptionValue) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to configure Option Value");
        return;
    }

    if (nmhSetFsDhcp6SrvSubOptionValue (u4PoolIndex, u4OptionIndex,
                                        u4SubOptionType,
                                        &SubOptionValue) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set option Value");
        return;
    }

    if (nmhTestv2FsDhcp6SrvSubOptionRowStatus (&u4ErrValue, u4PoolIndex,
                                               u4OptionIndex,
                                               u4SubOptionType,
                                               ACTIVE) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                      " needed to activate");
        return;
    }
    if (nmhSetFsDhcp6SrvSubOptionRowStatus (u4PoolIndex, u4OptionIndex,
                                            u4SubOptionType,
                                            ACTIVE) == SNMP_FAILURE)
    {
        D6SrMainUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Entry creation failed");
        return;
    }

    D6SrMainUnLock ();
    WebnmUnRegisterLock (pHttp);
    IssProcessDhcp6ServerSubOptionPageGet (pHttp);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrWebPrintStringIpAdd                                    */
/*                                                                           */
/* Description  : This function will Print the Option Value                  */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                *pu1Buf       - Pointer to Option                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
D6SrWebPrintStringIpAdd (tHttp * pHttp, UINT1 *pu1Buf)
{
    UINT1               au1IpAdd[DHCP6_IP6_ADDRESS_SIZE_MAX];
    INT4                i4Counter = 0;

    MEMSET (&au1IpAdd, 0x00, sizeof (tIp6Addr));

    for (i4Counter = 0; i4Counter < DHCP6_IP6_ADDRESS_SIZE_MAX; i4Counter++)
    {
        au1IpAdd[i4Counter] = *(pu1Buf);
        pu1Buf = pu1Buf + 1;
    }
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
             Ip6PrintAddr ((tIp6Addr *) (VOID *) au1IpAdd));
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrWebPrintDuid                                           */
/*                                                                           */
/* Description  : This function will Print the DUID                          */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                *pu1Buf       - Pointer to DUID                            */
/*                i4Length      - Length of DUID                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
D6SrWebPrintDuid (tHttp * pHttp, UINT1 *pu1Buf, INT4 i4Length)
{
    UINT1              *pu1Duid;
    INT4                i4Counter = 0;

    pu1Duid = pHttp->au1DataString;

    for (i4Counter = 0; i4Counter < i4Length; i4Counter++)
    {
        SPRINTF ((CHR1 *) (pu1Duid + STRLEN (pu1Duid)), "%02x", *(pu1Buf));
        pu1Buf = pu1Buf + 1;
    }
    return;
}

#endif /* DHCP6_SRV_WANTED */

#endif /* WEBNM_WANTED */
#endif
