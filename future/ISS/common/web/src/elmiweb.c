/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: elmiweb.c,v 1.8 2013/07/03 12:19:58 siva Exp $
 *
 * Description: This file contains routines for ELMI web Module
 *
 ********************************************************************/

#ifndef _ELMI_WEB_C_
#define _ELMI_WEB_C_

#ifdef WEBNM_WANTED
#include "webiss.h"
#include "webinc.h"
#include "isshttp.h"
#include "elm.h"
extern INT4         FsElmiUniSideSet (tSnmpIndex *, tRetVal *);
extern INT4         FsElmiPollingCounterValueSet (tSnmpIndex *, tRetVal *);
extern INT4         FsElmiStatusCounterSet (tSnmpIndex *, tRetVal *);
extern INT4         FsElmiPollingVerificationTimerValueSet (tSnmpIndex *,
                                                            tRetVal *);
extern INT4         FsElmiPollingTimerValueSet (tSnmpIndex *, tRetVal *);
extern INT4         FsElmiPortElmiStatusSet (tSnmpIndex *, tRetVal *);

/*********************************************************************
*  Function Name : IssProcessElmiPortConfPage
*  Description   : This function processes the request coming for the 
*                  port configuration page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessElmiPortConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessElmiPortConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessElmiPortConfPageSet (pHttp);
    }
    return;
}

/*******************************************************************************
*  Function Name : IssProcessElmiPortConfPageSet
*  Description   : This function processes the HTTP set request coming for the
*                  Elmi Port configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None.
*******************************************************************************/
VOID
IssProcessElmiPortConfPageSet (tHttp * pHttp)
{
    UINT4               u4ErrValue = 0;
    INT4                i4PortNo = 0;
    INT4                i4Status = 0;
    INT4                i4UniSide = 0;
    INT4                i4PollingCounter = 0;
    INT4                i4StatusCounter = 0;
    INT4                i4PollingTimer = 0;
    INT4                i4PollingVerTimer = 0;
    INT4                i4RetVal = 0;
    INT4                i4RetSysStatus = 0;

    STRCPY (pHttp->au1Name, "PORT");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PortNo = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "STATUS");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Status = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "UNISIDE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4UniSide = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "POLLING_COUNTER");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PollingCounter = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "STATUS_COUNTER");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4StatusCounter = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "POLLING_TIMER");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PollingTimer = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "POLLING_VERIFICATION_TIMER");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PollingVerTimer = ATOI (pHttp->au1Value);

    nmhGetFsElmiSystemControl (&i4RetSysStatus);
    if (i4RetSysStatus != 1)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Elmi is shutdown, no configuration possible");
        return;
    }

    i4RetVal = 0;
    nmhGetFsElmiUniSide (i4PortNo, &i4RetVal);
    if (i4RetVal != i4UniSide)
    {
        if (nmhTestv2FsElmiUniSide (&u4ErrValue, i4PortNo, i4UniSide) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unable to configure uniside on the port");
            return;
        }

        if (nmhSetFsElmiUniSide (i4PortNo, i4UniSide) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unable to configure uniside on the port");
            return;
        }
    }

    i4RetVal = 0;
    nmhGetFsElmiPollingCounterValue (i4PortNo, &i4RetVal);
    if ((i4PollingCounter != 0) && (i4RetVal != i4PollingCounter))
    {
        if (nmhTestv2FsElmiPollingCounterValue (&u4ErrValue,
                                                i4PortNo,
                                                i4PollingCounter) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to configure polling counter");
            return;
        }
        if (nmhSetFsElmiPollingCounterValue (i4PortNo, i4PollingCounter)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to configure polling counter");
            return;
        }
    }

    i4RetVal = 0;
    nmhGetFsElmiStatusCounter (i4PortNo, &i4RetVal);
    if ((i4StatusCounter != 0) && (i4RetVal != i4StatusCounter))
    {
        if (nmhTestv2FsElmiStatusCounter (&u4ErrValue,
                                          i4PortNo,
                                          i4StatusCounter) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to configure status counter");
            return;
        }
        if (nmhSetFsElmiStatusCounter (i4PortNo, i4StatusCounter)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to configure status counter");
            return;
        }
    }
    i4RetVal = 0;
    nmhGetFsElmiPollingTimerValue (i4PortNo, &i4RetVal);
    if ((i4PollingTimer != 0) && (i4RetVal != i4PollingTimer))
    {
        if (nmhTestv2FsElmiPollingTimerValue (&u4ErrValue,
                                              i4PortNo,
                                              i4PollingTimer) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to configure polling timer");
            return;
        }
        if (nmhSetFsElmiPollingTimerValue (i4PortNo, i4PollingTimer)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to configure polling timer");
            return;
        }
    }

    i4RetVal = 0;
    nmhGetFsElmiPollingVerificationTimerValue (i4PortNo, &i4RetVal);
    if (i4RetVal != i4PollingVerTimer)
    {
        if (nmhTestv2FsElmiPollingVerificationTimerValue (&u4ErrValue,
                                                          i4PortNo,
                                                          i4PollingVerTimer) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unable to configure polling verification timer");
            return;
        }
        if (nmhSetFsElmiPollingVerificationTimerValue
            (i4PortNo, i4PollingVerTimer) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unable to configure polling verification timer");
            return;
        }
    }

    i4RetVal = 0;
    nmhGetFsElmiPortElmiStatus (i4PortNo, &i4RetVal);
    if (i4RetVal != i4Status)
    {
        if (nmhTestv2FsElmiPortElmiStatus (&u4ErrValue,
                                           i4PortNo, i4Status) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unable to configure module status on the port");
            return;
        }
        if (nmhSetFsElmiPortElmiStatus (i4PortNo, i4Status) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unable to configure module status on the port");
            return;
        }
    }

    IssProcessElmiPortConfPageGet (pHttp);
}

/*******************************************************************************
 *  Function Name : IssProcessElmiPortConfPageGet
 *  Description   : This function processes the get request for the Elmi
 *                  Port configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessElmiPortConfPageGet (tHttp * pHttp)
{
    INT4                i4CurrPort = 0;
    INT4                i4NextPort = 0;
    INT4                i4RetVal = 0;
    INT4                i4RetSysStatus = 0;
    UINT4               u4Temp = 0;

    pHttp->i4Write = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    nmhGetFsElmiSystemControl (&i4RetSysStatus);
    if (i4RetSysStatus != 1)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    if (nmhGetFirstIndexFsElmiPortTable (&i4NextPort) == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    i4NextPort = 0;

    u4Temp = (UINT4) pHttp->i4Write;
    while (nmhGetNextIndexFsElmiPortTable (i4CurrPort, &i4NextPort) ==
           SNMP_SUCCESS)
    {
        pHttp->i4Write = (INT4) u4Temp;

        /* Port number */
        STRCPY (pHttp->au1KeyString, "PORT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextPort);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Module status */
        STRCPY (pHttp->au1KeyString, "STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsElmiPortElmiStatus (i4NextPort, &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Uniside Status */
        STRCPY (pHttp->au1KeyString, "UNISIDE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsElmiUniSide (i4NextPort, &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Polling counter */
        STRCPY (pHttp->au1KeyString, "POLLING_COUNTER_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsElmiPollingCounterValue (i4NextPort, &i4RetVal) ==
            SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Status counter */
        STRCPY (pHttp->au1KeyString, "STATUS_COUNTER_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsElmiStatusCounter (i4NextPort, &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Polling timer */
        STRCPY (pHttp->au1KeyString, "POLLING_TIMER_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsElmiPollingTimerValue (i4NextPort, &i4RetVal) ==
            SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Polling verification counter */
        STRCPY (pHttp->au1KeyString, "POLLING_VERIFICATION_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetFsElmiPollingVerificationTimerValue (i4NextPort, &i4RetVal) ==
            SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        i4CurrPort = i4NextPort;

    }
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
 *  Function Name : IssProcessElmiBasicStatsPage
 *  Description   : This function processes the request coming for the  
 *                  Elmi statistics page. 
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ********************************************************************/
VOID
IssProcessElmiBasicStatsPage (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    INT4                i4CurrPort = 0;
    INT4                i4NextPort = 0;
    INT4                i4ElmiUniSide = 0;
    INT4                i4RetVal = 0;
    INT4                i4OutCome = SNMP_FAILURE;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;
    if (nmhGetFirstIndexFsElmiPortTable (&i4NextPort) == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    /* Scanning the entire port table to display diff. values */
    i4OutCome = nmhGetFirstIndexFsElmiPortTable (&i4CurrPort);
    while (i4OutCome == SNMP_SUCCESS)
    {
        pHttp->i4Write = (INT4) u4Temp;

        STRCPY (pHttp->au1KeyString, "PORT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4CurrPort);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "MODE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsElmiUniSide (i4CurrPort, &i4ElmiUniSide);
        switch (i4ElmiUniSide)
        {
            case 1:
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Customer");
                break;
            case 2:
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Network");
                break;
            default:
                break;
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "N391_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiPollingCounterValue (i4CurrPort, &i4RetVal);
        /* If mode is ELMI_UNI_C then display the resp. counter value otherwise
         * print none */
        if (i4ElmiUniSide == 1)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "N393_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiStatusCounter (i4CurrPort, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "T391_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiPollingTimerValue (i4CurrPort, &i4RetVal);
        /* If mode is ELMI_UNI_C then display the resp. counter value otherwise
         * print none */
        if (i4ElmiUniSide == 1)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "T392_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiPollingVerificationTimerValue (i4CurrPort, &i4RetVal);
        /* If mode is ELMI_UNI_N then display the resp. timer value otherwise
         * print none */
        if (i4ElmiUniSide == 2)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "NO_OF_EVCS_KEY");

        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiNoOfConfiguredEvcs (i4CurrPort, &i4RetVal);

        /* If mode is ELMI_UNI_C then display the resp. timer value otherwise
         * print none */
        if (i4ElmiUniSide == 1)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        i4OutCome = nmhGetNextIndexFsElmiPortTable (i4CurrPort, &i4NextPort);
        if (i4OutCome == SNMP_SUCCESS)
        {
            i4CurrPort = i4NextPort;
        }
    }

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
 *  Function Name : IssProcessElmiPortStatsPage
 *  Description   : This function processes the request coming for the  
 *                  Elmi port statistics page. 
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ********************************************************************/
VOID
IssProcessElmiPortStatsPage (tHttp * pHttp)
{
    INT4                i4CurrPort = 0;
    INT4                i4NextPort = 0;
    INT4                i4ElmiUniSide = 0;
    INT4                i4RetVal = 0;
    UINT4               u4Temp = 0;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    u4Temp = (UINT4) pHttp->i4Write;

    if (nmhGetFirstIndexFsElmiPortTable (&i4NextPort) == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    /* Scanning the entire port table to display diff. values */
    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        STRCPY (pHttp->au1KeyString, "PORT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextPort);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "MODE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsElmiUniSide (i4NextPort, &i4ElmiUniSide);
        switch (i4ElmiUniSide)
        {
            case 1:
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Customer");
                break;
            case 2:
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Network");
                break;
            default:
                break;
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "TX_CHK_ENQ_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiTxElmiCheckEnqMsgCount (i4NextPort, &i4RetVal);
        /* If mode is ELMI_UNI_C then display the resp. value otherwise
         * print none */
        if (i4ElmiUniSide == 1)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "TX_FULL_STATUS_ENQ_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiTxFullStatusEnqMsgCount (i4NextPort, &i4RetVal);
        /* If mode is ELMI_UNI_C then display the resp. value otherwise
         * print none */
        if (i4ElmiUniSide == 1)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "TX_FULL_CONT_ENQ_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiTxFullStatusContEnqMsgCount (i4NextPort, &i4RetVal);
        /* If mode is ELMI_UNI_C then display the resp. value otherwise
         * print none */
        if (i4ElmiUniSide == 1)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "TX_CHK_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiTxElmiCheckMsgCount (i4NextPort, &i4RetVal);
        /* If mode is ELMI_UNI_N then display the resp. value otherwise
         * print none */
        if (i4ElmiUniSide == 2)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "TX_FULL_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiTxFullStatusMsgCount (i4NextPort, &i4RetVal);
        /* If mode is ELMI_UNI_N then display the resp. value otherwise
         * print none */
        if (i4ElmiUniSide == 2)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "TX_FULL_CONT_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiTxFullStatusContMsgCount (i4NextPort, &i4RetVal);
        /* If mode is ELMI_UNI_N then display the resp. value otherwise
         * print none */
        if (i4ElmiUniSide == 2)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "TX_ASYNC_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiTxAsyncStatusMsgCount (i4NextPort, &i4RetVal);
        /* If mode is ELMI_UNI_N then display the resp. value otherwise
         * print none */
        if (i4ElmiUniSide == 2)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "RX_CHK_ENQ_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiRxElmiCheckEnqMsgCount (i4NextPort, &i4RetVal);
        /* If mode is ELMI_UNI_N then display the resp. value otherwise
         * print none */
        if (i4ElmiUniSide == 2)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "RX_FULL_STATUS_ENQ_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiRxFullStatusEnqMsgCount (i4NextPort, &i4RetVal);
        /* If mode is ELMI_UNI_N then display the resp. value otherwise
         * print none */
        if (i4ElmiUniSide == 2)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "RX_FULL_CONT_ENQ_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiRxFullStatusContEnqMsgCount (i4NextPort, &i4RetVal);
        /* If mode is ELMI_UNI_N then display the resp. value otherwise
         * print none */
        if (i4ElmiUniSide == 2)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "RX_CHK_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiRxElmiCheckMsgCount (i4NextPort, &i4RetVal);
        /* If mode is ELMI_UNI_C then display the resp. value otherwise
         * print none */
        if (i4ElmiUniSide == 1)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "RX_FULL_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiRxFullStatusMsgCount (i4NextPort, &i4RetVal);
        /* If mode is ELMI_UNI_C then display the resp. value otherwise
         * print none */
        if (i4ElmiUniSide == 1)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "RX_FULL_CONT_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiRxFullStatusContMsgCount (i4NextPort, &i4RetVal);
        /* If mode is ELMI_UNI_C then display the resp. value otherwise
         * print none */
        if (i4ElmiUniSide == 1)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "RX_ASYNC_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiRxAsyncStatusMsgCount (i4NextPort, &i4RetVal);
        /* If mode is ELMI_UNI_C then display the resp. value otherwise
         * print none */
        if (i4ElmiUniSide == 1)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "RX_VALID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiRxValidMsgCount (i4NextPort, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "RX_INVALID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiRxInvalidMsgCount (i4NextPort, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        i4CurrPort = i4NextPort;

    }
    while (nmhGetNextIndexFsElmiPortTable (i4CurrPort, &i4NextPort)
           == SNMP_SUCCESS);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
 *  Function Name : IssProcessElmiErrorStatsPage
 *  Description   : This function processes the request coming for the  
 *                  Elmi errors on a port page. 
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ********************************************************************/
VOID
IssProcessElmiErrorStatsPage (tHttp * pHttp)
{
    INT4                i4CurrPort = 0;
    INT4                i4NextPort = 0;
    INT4                i4ElmiUniSide = 0;
    INT4                i4RetVal = 0;
    UINT4               u4Temp = 0;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    u4Temp = (UINT4) pHttp->i4Write;

    if (nmhGetFirstIndexFsElmiPortTable (&i4NextPort) == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    /* Scanning the entire port table to display diff. values */
    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        STRCPY (pHttp->au1KeyString, "PORT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextPort);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "MODE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsElmiUniSide (i4NextPort, &i4ElmiUniSide);
        switch (i4ElmiUniSide)
        {
            case 1:
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Customer");
                break;
            case 2:
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Network");
                break;
            default:
                break;
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "STATUS_TIMEOUT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiRelErrStatusTimeOutCount (i4NextPort, &i4RetVal);
        /* If mode is ELMI_UNI_C then display the resp. value otherwise
         * print none */
        if (i4ElmiUniSide == 1)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "INVD_SEQ_NO_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiRelErrInvalidSeqNumCount (i4NextPort, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "INVD_STATUS_RESPONSE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiRelErrInvalidStatusRespCount (i4NextPort, &i4RetVal);
        /* If mode is ELMI_UNI_C then display the resp. value otherwise
         * print none */
        if (i4ElmiUniSide == 1)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "INVD_PROTO_VER_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiProErrInvalidProtVerCount (i4NextPort, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "INVD_REF_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiProErrInvalidEvcRefIdCount (i4NextPort, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "INVD_MSG_TYPE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiProErrInvalidMessageTypeCount (i4NextPort, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "OUT_OF_SEQ_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiProErrOutOfSequenceInfoEleCount (i4NextPort, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "DUP_IE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiProErrDuplicateInfoEleCount (i4NextPort, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "RX_UNSOLICIT_MSG_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiRelErrRxUnSolicitedStatusCount (i4NextPort, &i4RetVal);
        /* If mode is ELMI_UNI_C then display the resp. value otherwise
         * print none */
        if (i4ElmiUniSide == 1)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "RX_MANDATORY_ELE_MISS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiProErrMandatoryInfoEleMissingCount (i4NextPort, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "RX_INVD_MANDATORY_ELE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiProErrInvalidMandatoryInfoEleCount (i4NextPort, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "RX_INVD_NON_MANDATORY_ELE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiProErrInvalidNonMandatoryInfoEleCount (i4NextPort,
                                                           &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "RX_UNRECOGNISED_ELE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiProErrUnrecognizedInfoEleCount (i4NextPort, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "RX_UNEXPECTED_ELE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiProErrUnexpectedInfoEleCount (i4NextPort, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "RX_SHORT_MSG_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        nmhGetFsElmiProErrShortMessageCount (i4NextPort, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

        i4CurrPort = i4NextPort;

    }
    while (nmhGetNextIndexFsElmiPortTable (i4CurrPort, &i4NextPort)
           == SNMP_SUCCESS);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}
#endif
#endif
