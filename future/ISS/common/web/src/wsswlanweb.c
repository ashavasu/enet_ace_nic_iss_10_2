/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: wsswlanweb.c,v 1.35.2.1 2018/03/19 14:21:08 siva Exp $
 *
 * Description: Routines for WSS WLAN WEB Module
 *******************************************************************/

#ifndef _WSS_WLAN_WEB_C_
#define _WSS_WLAN_WEB_C_

#include "webiss.h"
#include "isshttp.h"
#include "webinc.h"
#include "issmacro.h"
#include "snmputil.h"
#include "utilcli.h"

#include "capwapcliinc.h"
#include "capwapclidefg.h"
#include "capwaptdfs.h"
#include "dtls.h"
#include "capwapprot.h"

#include "wsscfgtmr.h"
#include "wsscfgprot.h"
#include "wsscfgcli.h"
#include "wsscfgtdfsg.h"
#include "wsscfgtdfs.h"
#include "wsscfglwg.h"
#include "std802lw.h"
#include "wsscfgprotg.h"
#include "wsscfgwlanproto.h"
#include "rsna.h"
#include "fsrsnalw.h"
#include "wssifinc.h"

INT4                gi4WlanProfileId = 0;
UINT1               gAuthProfileName[OCTETSTR_SIZE + 1];
UINT1               gQosProfileName[OCTETSTR_SIZE];
UINT1               gCapProfileName[OCTETSTR_SIZE];
extern tWssClientSummary gaWssClientSummary[MAX_STA_SUPP_PER_WLC];
extern UINT4        gu4ClientWalkIndex;
extern INT1
 
 
 
 
RsnaGetFsRSNAConfigAuthenticationSuite (UINT4 u4ProfileId,
                                        UINT4
                                        u4Dot11RSNAConfigAuthenticationSuiteIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pRetValDot11RSNAConfigAuthenticationSuite);
extern INT1
 
 
 
 
WpaTestv2FsRSNAConfigAuthenticationSuiteEnabled (UINT4
                                                 *pu4ErrorCode,
                                                 UINT4
                                                 u4FsRSNAConfigAuthenticationSuiteIndex,
                                                 INT4
                                                 i4TestValFsRSNAConfigAuthenticationSuiteEnabled);
extern INT1
 
 
 
 
WpaSetFsRSNAConfigAuthenticationSuiteEnabled (UINT4 u4ProfileId,
                                              UINT4
                                              u4FsRSNAConfigAuthenticationSuiteIndex,
                                              INT4
                                              i4SetValFsRSNAConfigAuthenticationSuiteEnabled);
extern INT1
 
 
 
 
WpaGetFsRSNAConfigAuthenticationSuiteEnabled (INT4 i4IfIndex,
                                              UINT4
                                              u4Dot11RSNAConfigAuthenticationSuiteIndex,
                                              INT4
                                              *pi4RetValFsRSNAConfigAuthenticationSuiteEnabled);

#define wss_web_trace_enable 0
#define WSS_WEB_TRC   if(wss_web_trace_enable)  printf

#define WSS_AUTH_OPEN               1
#define WSS_AUTH_SHARED             2
#define WSS_AUTH_WEB                3
#define WSS_AUTH_WEP_KEY_LENTH_40   40

#define WSS_QOS_TRAFFIC_PLATINUM    1
#define WSS_QOS_TRAFFIC_GOLD        2
#define WSS_QOS_TRAFFIC_SILVER      3
#define WSS_QOS_TRAFFIC_BRONZE      4

#define WSS_ENABLED                 1
#define WSS_DISABLED                0

#ifdef WLC_WANTED
#endif
/*********************************************************************
*  Function Name : IssProcessWlanTablePage
*  Description   : This function processes the request coming for the  
*                  Wlan Table Page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessWlanTablePage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessWlanTablePageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessWlanTablePageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessWlanTablePageGet
*  Description   : This function processes the get request coming for the  
*                  Wlan Table page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessWlanTablePageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0, nextWlanProfileId =
        0, currentWlanProfileId = 0;
    INT4                i4OutCome = 0, i4IfIndex = 0;
    INT4                i4WlanAdminStatus = 0;
    tSNMP_OCTET_STRING_TYPE *Security = NULL;
    tSNMP_MULTI_DATA_TYPE *pu1DataString = NULL;
    UINT1               WLANSSID[OCTETSTR_SIZE];
    INT4                i4Authentication = 0;
    INT4                i4TrapStatus = 0;
#ifdef RSNA_WANTED
    INT4                i4RsnaStatus = RSNA_DISABLED;
#endif
    tWsscfgFsDot11WlanAuthenticationProfileEntry
        wsscfgFsDot11WlanAuthenticationProfileEntry;

    MEMSET (&wsscfgFsDot11WlanAuthenticationProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanAuthenticationProfileEntry));

    /* This is to reset global variable so that the edit page corresponding to global variable works fine */

    if (CapwapCheckModuleStatus () == OSIX_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Capwap module is shutdown. Can't configure!");
        return;
    }

    Security = (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (Security == NULL)
    {
        return;
    }
    MEMSET (Security->pu1_OctetList, 0, OCTETSTR_SIZE);

    if ((pu1DataString = WebnmAllocMultiData ()) == NULL)
    {
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        free_octetstring (Security);
        return;
    }

    WebnmRegisterLock (pHttp, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;
    i4OutCome = nmhGetFirstIndexCapwapDot11WlanTable (&nextWlanProfileId);

    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        nmhGetFsWlanStationTrapStatus (&i4TrapStatus);
        STRCPY (pHttp->au1KeyString, "WLAN_TRAP_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4TrapStatus);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));

        WSSCFG_UNLOCK;
        WebnmUnRegisterLock (pHttp);
        free_octetstring (Security);
        return;
    }
    else
    {
        do
        {
            pHttp->i4Write = (INT4) u4Temp;
            MEMSET (WLANSSID, 0, OCTETSTR_SIZE);

            pu1DataString->i4_SLongValue = (INT4) nextWlanProfileId;

            STRCPY (pHttp->au1KeyString, "WLAN_PROFILE_ID");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            WebnmSendToSocket (pHttp, "WLAN_PROFILE_ID", pu1DataString,
                               ENM_INTEGER32);

            nmhGetCapwapDot11WlanProfileIfIndex (nextWlanProfileId, &i4IfIndex);
            WssCfGetDot11DesiredSSID ((UINT4) i4IfIndex, WLANSSID);

            STRCPY (pHttp->au1KeyString, "WLAN_SSID");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            STRCPY (pHttp->au1DataString, WLANSSID);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            STRCPY (pHttp->au1KeyString, "ADMIN_STATUS");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            nmhGetIfMainAdminStatus (i4IfIndex, &i4WlanAdminStatus);
            if (i4WlanAdminStatus == 1)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Enable");
            }
            else
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Disable");
            }
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) (HTTP_STRLEN (pHttp->au1DataString)));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            STRCPY (pHttp->au1KeyString, "SECURITY_POLICY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetCapwapDot11WlanProfileIfIndex (nextWlanProfileId, &i4IfIndex);
            wsscfgFsDot11WlanAuthenticationProfileEntry.MibObject.i4IfIndex =
                i4IfIndex;

            if (WsscfgGetAllFsDot11WlanAuthenticationProfileTable
                (&wsscfgFsDot11WlanAuthenticationProfileEntry) == OSIX_SUCCESS)
            {
                i4Authentication =
                    wsscfgFsDot11WlanAuthenticationProfileEntry.MibObject.
                    i4FsDot11WlanAuthenticationAlgorithm;
            }
            if (i4Authentication == 1)
            {
#ifdef RSNA_WANTED
                if (RsnaGetDot11RSNAEnabled (i4IfIndex,
                                             &i4RsnaStatus) == SNMP_SUCCESS)
                {
                    if (i4RsnaStatus != RSNA_ENABLED)
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Open System");
                    }
                    else
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "WPA2");
                    }
                }
#else
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Open System");
#endif
            }
            else
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         "Static WEP(Shared Key)");
            }
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) (HTTP_STRLEN (pHttp->au1DataString)));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

            /* Copy next index to current index */
            currentWlanProfileId = nextWlanProfileId;
        }
        while (nmhGetNextIndexCapwapDot11WlanTable
               (currentWlanProfileId, &nextWlanProfileId) == SNMP_SUCCESS);

        nmhGetFsWlanStationTrapStatus (&i4TrapStatus);
        STRCPY (pHttp->au1KeyString, "WLAN_TRAP_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4TrapStatus);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WSSCFG_UNLOCK;
        WebnmUnRegisterLock (pHttp);
    }

    free_octetstring (Security);
}

/*********************************************************************
*  Function Name : IssProcessWlanTablePageSet
*  Description   : This function processes the set request coming for the  
*                  Wlan  Table page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessWlanTablePageSet (tHttp * pHttp)
{
    INT4                i4WlanProfileId = 0;
    INT4                i4WlanRowStatus = 0;
    INT4                i4FsWlanStationTrapStatus = 0;
    UINT4               u4ErrorCode = 0;

    STRCPY (pHttp->au1Name, "WLAN_PROFILE_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4WlanProfileId = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "WLAN_TRAP_STATUS");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4FsWlanStationTrapStatus = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    WebnmRegisterLock (pHttp, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    WSS_WEB_TRC ("WLAN Profile Id:%d\n", i4WlanProfileId);

    if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {
        i4WlanRowStatus = DESTROY;
        WSS_WEB_TRC ("Deleting WLAN Profile Entry\n");
        nmhSetCapwapDot11WlanRowStatus ((UINT4) i4WlanProfileId,
                                        i4WlanRowStatus);
    }
    else if (STRCMP (pHttp->au1Value, "Edit") == 0)
    {
        WSS_WEB_TRC ("Edit.....just store the Profile Id:%d\n",
                     i4WlanProfileId);
        gi4WlanProfileId = (INT4) i4WlanProfileId;
    }

    else if (STRCMP (pHttp->au1Value, "Apply") == 0)
    {
        if (nmhTestv2FsWlanStationTrapStatus
            (&u4ErrorCode, i4FsWlanStationTrapStatus) != SNMP_SUCCESS)
        {
            WSS_WEB_TRC ("Wlan Trap Status Test Failed\n");
            return;
        }

        if (nmhSetFsWlanStationTrapStatus (i4FsWlanStationTrapStatus) !=
            SNMP_SUCCESS)
        {
            WSS_WEB_TRC ("WLAN Trap Status Set Failed\n");
            return;
        }
    }
    else
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Operation\n");
    }

    WSSCFG_UNLOCK;
    WebnmUnRegisterLock (pHttp);
    IssProcessWlanTablePageGet (pHttp);

    return;
}

/*********************************************************************
*  Function Name : IssProcessWlanCreatePage 
*  Description   : This function processes the request coming for the 
*                  WLAN Profile Table Page 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessWlanCreatePage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessWlanCreatePageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessWlanCreatePageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessWLANCreatePageGet 
*  Description   : This function processes the get request coming for the  
*                  WLAN Profile Table Entry page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessWlanCreatePageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE *pCurrentProfileName = NULL, *pNextProfileName =
        NULL;
    INT4                i4OutCome = 0;

    if (CapwapCheckModuleStatus () == OSIX_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Capwap module is shutdown. Can't configure!");
        return;
    }

    pCurrentProfileName =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (pCurrentProfileName == NULL)
    {
        WSS_WEB_TRC
            ("IssProcessWlanCreatePageGet failed due to memory allocation\n");
        return;
    }
    pNextProfileName =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (pNextProfileName == NULL)
    {
        WSS_WEB_TRC
            ("IssProcessWlanCreatePageGet failed due to memory allocation\n");
        free_octetstring (pCurrentProfileName);
        return;
    }
    MEMSET (pCurrentProfileName->pu1_OctetList, 0, OCTETSTR_SIZE);
    pCurrentProfileName->i4_Length = 0;

    MEMSET (pNextProfileName->pu1_OctetList, 0, OCTETSTR_SIZE);
    pNextProfileName->i4_Length = 0;

    STRCPY (pHttp->au1KeyString, "<! CAPABILITY PROFILES>");

    WebnmSendString (pHttp, pHttp->au1KeyString);

    WebnmRegisterLock (pHttp, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;

    i4OutCome =
        nmhGetFirstIndexFsDot11CapabilityProfileTable (pNextProfileName);

    if (i4OutCome == SNMP_SUCCESS)
    {

        do
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString,
                     "<option value = \"%s\" >%s \n",
                     pNextProfileName->pu1_OctetList,
                     pNextProfileName->pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) (STRLEN (pHttp->au1DataString)));

            WSS_WEB_TRC ("pNextProfileName : %s\n",
                         pNextProfileName->pu1_OctetList);

            /* Copy next index to current index */
            STRCPY (pCurrentProfileName->pu1_OctetList,
                    pNextProfileName->pu1_OctetList);
            pCurrentProfileName->i4_Length = pNextProfileName->i4_Length;
            MEMSET (pNextProfileName->pu1_OctetList, 0, OCTETSTR_SIZE);
            pNextProfileName->i4_Length = 0;

        }
        while (nmhGetNextIndexFsDot11CapabilityProfileTable
               (pCurrentProfileName, pNextProfileName) == SNMP_SUCCESS);
    }
    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "<! AUTHENTICATION PROFILES>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    i4OutCome =
        nmhGetFirstIndexFsDot11AuthenticationProfileTable (pNextProfileName);

    if (i4OutCome == SNMP_SUCCESS)
    {

        do
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString,
                     "<option value = \"%s\" >%s \n",
                     pNextProfileName->pu1_OctetList,
                     pNextProfileName->pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) (STRLEN (pHttp->au1DataString)));

            WSS_WEB_TRC ("pNextProfileName : %s\n",
                         pNextProfileName->pu1_OctetList);

            /* Copy next index to current index */
            STRCPY (pCurrentProfileName->pu1_OctetList,
                    pNextProfileName->pu1_OctetList);
            pCurrentProfileName->i4_Length = pNextProfileName->i4_Length;
            MEMSET (pNextProfileName->pu1_OctetList, 0, OCTETSTR_SIZE);
            pNextProfileName->i4_Length = 0;
        }
        while (nmhGetNextIndexFsDot11AuthenticationProfileTable
               (pCurrentProfileName, pNextProfileName) == SNMP_SUCCESS);
    }
    else
    {
        /* do nothing */
    }

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "<! QOS PROFILES>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    i4OutCome = nmhGetFirstIndexFsDot11QosProfileTable (pNextProfileName);

    if (i4OutCome == SNMP_SUCCESS)
    {

        do
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString,
                     "<option value = \"%s\" >%s \n",
                     pNextProfileName->pu1_OctetList,
                     pNextProfileName->pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) (STRLEN (pHttp->au1DataString)));

            WSS_WEB_TRC ("pNextProfileName : %s\n",
                         pNextProfileName->pu1_OctetList);

            /* Copy next index to current index */
            STRCPY (pCurrentProfileName->pu1_OctetList,
                    pNextProfileName->pu1_OctetList);
            pCurrentProfileName->i4_Length = pNextProfileName->i4_Length;
            MEMSET (pNextProfileName->pu1_OctetList, 0, OCTETSTR_SIZE);
            pNextProfileName->i4_Length = 0;
        }
        while (nmhGetNextIndexFsDot11QosProfileTable (pCurrentProfileName,
                                                      pNextProfileName) ==
               SNMP_SUCCESS);

    }
    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    WebnmUnRegisterLock (pHttp);
    WSSCFG_UNLOCK;
    free_octetstring (pNextProfileName);
    free_octetstring (pCurrentProfileName);
}

/*********************************************************************
*  Function Name : IssProcessWLANCreatePageSet 
*  Description   : This function processes the set request coming for the  
*                  WLAN Profile Table Entry page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessWlanCreatePageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE *pWlanSSID = NULL;
    UINT1               tunnelMode[OCTETSTR_SIZE];
    UINT1               au1QosProfileName[OCTETSTR_SIZE];
    UINT1               au1AuthProfileName[OCTETSTR_SIZE];
    UINT1               au1CapProfileName[OCTETSTR_SIZE];
    UINT1               wlanSSID[CLI_MAX_SSID_LEN + 1];
    UINT1               tunnelModeBit = 0;
    UINT4               u4ProfileId = 0, u4ErrorCode = 0;
    INT4                i4WlanIfIndex = 0, i4MacType = 0, i4TunnelModeLen = 1;
    INT4                i4WlanRowStatus = NOT_IN_SERVICE, i4VlanId = 0;

    tWsscfgFsDot11CapabilityMappingEntry wsscfgFsDot11CapabilityMappingEntry;
    tWsscfgIsSetFsDot11CapabilityMappingEntry
        wsscfgIsSetFsDot11CapabilityMappingEntry;
    tWsscfgFsDot11AuthMappingEntry wsscfgFsDot11AuthMappingEntry;
    tWsscfgIsSetFsDot11AuthMappingEntry wsscfgIsSetFsDot11AuthMappingEntry;
    tWsscfgFsDot11QosMappingEntry wsscfgFsDot11QosMappingEntry;
    tWsscfgIsSetFsDot11QosMappingEntry wsscfgIsSetFsDot11QosMappingEntry;
    tWsscfgFsDot11StationConfigEntry wsscfgFsDot11StationConfigEntry;
    tWsscfgIsSetFsDot11StationConfigEntry wsscfgIsSetFsDot11StationConfigEntry;
    tWsscfgCapwapDot11WlanEntry wsscfgCapwapDot11WlanEntry;
    tWsscfgIsSetCapwapDot11WlanEntry wsscfgIsSetCapwapDot11WlanEntry;

    MEMSET (&wsscfgFsDot11CapabilityMappingEntry, 0,
            sizeof (tWsscfgFsDot11CapabilityMappingEntry));
    MEMSET (&wsscfgIsSetFsDot11CapabilityMappingEntry, 0,
            sizeof (tWsscfgIsSetFsDot11CapabilityMappingEntry));

    MEMSET (&wsscfgFsDot11AuthMappingEntry, 0,
            sizeof (tWsscfgFsDot11AuthMappingEntry));
    MEMSET (&wsscfgIsSetFsDot11AuthMappingEntry, 0,
            sizeof (tWsscfgIsSetFsDot11AuthMappingEntry));
    MEMSET (&wsscfgFsDot11AuthMappingEntry, 0,
            sizeof (tWsscfgFsDot11AuthMappingEntry));

    MEMSET (&wsscfgFsDot11QosMappingEntry, 0,
            sizeof (tWsscfgFsDot11QosMappingEntry));
    MEMSET (&wsscfgIsSetFsDot11QosMappingEntry, 0,
            sizeof (tWsscfgIsSetFsDot11QosMappingEntry));

    MEMSET (&wsscfgFsDot11StationConfigEntry, 0,
            sizeof (tWsscfgFsDot11StationConfigEntry));
    MEMSET (&wsscfgIsSetFsDot11StationConfigEntry, 0,
            sizeof (tWsscfgIsSetFsDot11StationConfigEntry));

    MEMSET (&wsscfgCapwapDot11WlanEntry, 0,
            sizeof (tWsscfgCapwapDot11WlanEntry));
    MEMSET (&wsscfgIsSetCapwapDot11WlanEntry, 0,
            sizeof (tWsscfgIsSetCapwapDot11WlanEntry));

    if (CapwapCheckModuleStatus () == OSIX_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Capwap module is shutdown. Can't configure!");
        return;
    }

    pWlanSSID =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (pWlanSSID == NULL)
    {
        WSS_WEB_TRC
            ("nmhTestv2CapwapDot11WlanRowStatus FAILED due to memory allcoation.\n");
        return;
    }
    MEMSET (tunnelMode, 0, OCTETSTR_SIZE);
    MEMSET (wlanSSID, 0, CLI_MAX_SSID_LEN + 1);
    MEMSET (pWlanSSID->pu1_OctetList, 0, OCTETSTR_SIZE);
    MEMSET (au1QosProfileName, 0, OCTETSTR_SIZE);
    MEMSET (au1AuthProfileName, 0, OCTETSTR_SIZE);
    MEMSET (au1CapProfileName, 0, OCTETSTR_SIZE);

    WebnmRegisterLock (pHttp, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;

    STRCPY (pHttp->au1Name, "WLAN_SSID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (wlanSSID, pHttp->au1Value, (sizeof (wlanSSID) - 1));

    STRCPY (pHttp->au1Name, "WLAN_PFLID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4ProfileId = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "WLAN_MAC_TYPE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4MacType = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "WLAN_TUNNEL_MODE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    tunnelModeBit = (UINT1) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "VLAN_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4VlanId = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "WLAN_CAPABILITY");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1CapProfileName, pHttp->au1Value,
             (sizeof (au1CapProfileName) - 1));

    STRCPY (pHttp->au1Name, "WLAN_AUTH");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1AuthProfileName, pHttp->au1Value,
             (sizeof (au1AuthProfileName) - 1));

    STRCPY (pHttp->au1Name, "WLAN_QOS_PROFILE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1QosProfileName, pHttp->au1Value,
             (sizeof (au1QosProfileName) - 1));

    WSS_WEB_TRC ("Values from Wlan Table\n****************************\n");
    WSS_WEB_TRC (" wlanSSID     :%s\n", wlanSSID);
    WSS_WEB_TRC (" u4ProfileId  :%d\n", u4ProfileId);
    WSS_WEB_TRC (" i4MacType :%d\n", i4MacType);
    WSS_WEB_TRC (" tunnelModeBit :%d\n", tunnelModeBit);
    WSS_WEB_TRC (" i4VlanId    :%d\n", i4VlanId);
    WSS_WEB_TRC (" au1CapProfileName :%s\n", au1CapProfileName);
    WSS_WEB_TRC (" au1AuthProfileName :%s\n", au1AuthProfileName);
    WSS_WEB_TRC (" au1QosProfileName :%s\n", au1QosProfileName);
    if ((nmhGetCapwapDot11WlanRowStatus (u4ProfileId, &i4WlanRowStatus)) !=
        SNMP_SUCCESS)
    {
        /* Profile NOT present - CREATE */

        if (nmhTestv2CapwapDot11WlanRowStatus (&u4ErrorCode, u4ProfileId,
                                               CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            WSS_WEB_TRC ("nmhTestv2CapwapDot11WlanRowStatus FAILED\n");
            IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
            WebnmUnRegisterLock (pHttp);
            WSSCFG_UNLOCK;
            free_octetstring (pWlanSSID);
            return;
        }

        /*
           CREATE Profile
         */

        if (nmhSetCapwapDot11WlanRowStatus (u4ProfileId, CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            WSS_WEB_TRC ("nmhSetCapwapDot11WlanRowStatus FAILED\n");
            IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
            WebnmUnRegisterLock (pHttp);
            WSSCFG_UNLOCK;
            free_octetstring (pWlanSSID);
            return;
        }

        pWlanSSID->i4_Length = (INT4) (STRLEN (wlanSSID));
        MEMCPY (pWlanSSID->pu1_OctetList, wlanSSID, pWlanSSID->i4_Length);
        /* WSS_WEB_TRC("WLAN SSID : %s\n",pWlanSSID->pu1_OctetList); */

        if (nmhGetCapwapDot11WlanProfileIfIndex (u4ProfileId, &i4WlanIfIndex) !=
            SNMP_SUCCESS)
        {
            WSS_WEB_TRC ("nmhGetCapwapDot11WlanProfileIfIndex FAILED\n");
            IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
            WebnmUnRegisterLock (pHttp);
            WSSCFG_UNLOCK;
            free_octetstring (pWlanSSID);
            return;
        }

        if (i4WlanIfIndex == 0)
        {
            WSS_WEB_TRC ("i4WlanIfIndex is 0\n");
            IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
            WebnmUnRegisterLock (pHttp);
            WSSCFG_UNLOCK;
            free_octetstring (pWlanSSID);
            return;
        }

        if ((nmhTestv2IfMainAdminStatus (&u4ErrorCode, i4WlanIfIndex, 1)) !=
            SNMP_SUCCESS)
        {
            WSS_WEB_TRC ("nmhTestv2IfMainAdminStatus FAILED\n");
            IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
            WebnmUnRegisterLock (pHttp);
            WSSCFG_UNLOCK;
            free_octetstring (pWlanSSID);
            return;
        }

        if ((nmhSetIfMainAdminStatus (i4WlanIfIndex, 1)) != SNMP_SUCCESS)
        {
            WSS_WEB_TRC ("nmhSetIfMainAdminStatus FAILED\n");
            IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
            WebnmUnRegisterLock (pHttp);
            WSSCFG_UNLOCK;
            free_octetstring (pWlanSSID);
            return;
        }

        nmhSetDot11DesiredSSID (i4WlanIfIndex, pWlanSSID);

        /* Create WLAN Profile Table */
        /* Fill the fields */

        wsscfgCapwapDot11WlanEntry.MibObject.u4CapwapDot11WlanProfileId =
            u4ProfileId;
        wsscfgCapwapDot11WlanEntry.MibObject.i4CapwapDot11WlanMacType =
            i4MacType;
        wsscfgCapwapDot11WlanEntry.MibObject.i4CapwapDot11WlanRowStatus =
            ACTIVE;
        wsscfgCapwapDot11WlanEntry.MibObject.au1CapwapDot11WlanTunnelMode[0] =
            tunnelModeBit;
        wsscfgCapwapDot11WlanEntry.MibObject.i4CapwapDot11WlanTunnelModeLen =
            i4TunnelModeLen;

        /* Fill the booleans indicating presence of corresponding field */
        wsscfgIsSetCapwapDot11WlanEntry.bCapwapDot11WlanProfileId = OSIX_TRUE;
        wsscfgIsSetCapwapDot11WlanEntry.bCapwapDot11WlanMacType = OSIX_TRUE;
        wsscfgIsSetCapwapDot11WlanEntry.bCapwapDot11WlanTunnelMode = OSIX_TRUE;
        wsscfgIsSetCapwapDot11WlanEntry.bCapwapDot11WlanRowStatus = OSIX_TRUE;

        if (WsscfgTestAllCapwapDot11WlanTable
            (&u4ErrorCode, &wsscfgCapwapDot11WlanEntry,
             &wsscfgIsSetCapwapDot11WlanEntry, OSIX_TRUE,
             OSIX_TRUE) != OSIX_SUCCESS)
        {
            WSS_WEB_TRC ("WsscfgTestAllCapwapDot11WlanTable FAILED!!!!1\n");
            IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
            WebnmUnRegisterLock (pHttp);
            WSSCFG_UNLOCK;
            free_octetstring (pWlanSSID);
            return;
        }

        if (WsscfgSetAllCapwapDot11WlanTable (&wsscfgCapwapDot11WlanEntry,
                                              &wsscfgIsSetCapwapDot11WlanEntry,
                                              OSIX_TRUE,
                                              OSIX_TRUE) != OSIX_SUCCESS)
        {
            WSS_WEB_TRC ("CapwapSetAllFsWtpModelTable FAILED!\n");
            IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
            WebnmUnRegisterLock (pHttp);
            WSSCFG_UNLOCK;
            free_octetstring (pWlanSSID);
            return;
        }

        /* Capability Profile Association with WLAN */
        if (STRCMP (au1CapProfileName, "default"))
        {
            wsscfgFsDot11CapabilityMappingEntry.MibObject.
                i4FsDot11CapabilityMappingRowStatus = ACTIVE;
            wsscfgFsDot11CapabilityMappingEntry.MibObject.i4IfIndex =
                i4WlanIfIndex;
            wsscfgFsDot11CapabilityMappingEntry.MibObject.
                i4FsDot11CapabilityMappingProfileNameLen =
                (INT4) (STRLEN (au1CapProfileName));
            STRNCPY (wsscfgFsDot11CapabilityMappingEntry.MibObject.
                     au1FsDot11CapabilityMappingProfileName, au1CapProfileName,
                     (sizeof
                      (wsscfgFsDot11CapabilityMappingEntry.MibObject.
                       au1FsDot11CapabilityMappingProfileName) - 1));

            wsscfgIsSetFsDot11CapabilityMappingEntry.
                bFsDot11CapabilityMappingRowStatus = OSIX_TRUE;
            wsscfgIsSetFsDot11CapabilityMappingEntry.bIfIndex = OSIX_TRUE;
            wsscfgIsSetFsDot11CapabilityMappingEntry.
                bFsDot11CapabilityMappingProfileName = OSIX_TRUE;

            if (WsscfgTestAllFsDot11CapabilityMappingTable
                (&u4ErrorCode, &wsscfgFsDot11CapabilityMappingEntry,
                 &wsscfgIsSetFsDot11CapabilityMappingEntry, OSIX_TRUE,
                 OSIX_TRUE) != OSIX_SUCCESS)
            {
                WSS_WEB_TRC
                    (" WsscfgTestAllFsDot11CapabilityMappingTable FAILED\n");
                IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
                WebnmUnRegisterLock (pHttp);
                WSSCFG_UNLOCK;
                free_octetstring (pWlanSSID);
                return;
            }

            if (WsscfgSetAllFsDot11CapabilityMappingTable
                (&wsscfgFsDot11CapabilityMappingEntry,
                 &wsscfgIsSetFsDot11CapabilityMappingEntry, OSIX_TRUE,
                 OSIX_TRUE) != OSIX_SUCCESS)
            {
                WSS_WEB_TRC
                    (" WsscfgSetAllFsDot11CapabilityMappingTable FAILED\n");
                IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
                WebnmUnRegisterLock (pHttp);
                WSSCFG_UNLOCK;
                free_octetstring (pWlanSSID);
                return;
            }

        }

        /* Authentication Profile Association with WLAN */
        if (STRCMP (au1AuthProfileName, "default"))
        {
            wsscfgFsDot11AuthMappingEntry.MibObject.
                i4FsDot11AuthMappingRowStatus = ACTIVE;
            wsscfgFsDot11AuthMappingEntry.MibObject.i4IfIndex = i4WlanIfIndex;
            wsscfgFsDot11AuthMappingEntry.MibObject.
                i4FsDot11AuthMappingProfileNameLen =
                (INT4) (STRLEN (au1AuthProfileName));
            STRNCPY (wsscfgFsDot11AuthMappingEntry.MibObject.
                     au1FsDot11AuthMappingProfileName, au1AuthProfileName,
                     (sizeof
                      (wsscfgFsDot11AuthMappingEntry.MibObject.
                       au1FsDot11AuthMappingProfileName) - 1));

            wsscfgIsSetFsDot11AuthMappingEntry.bFsDot11AuthMappingRowStatus =
                OSIX_TRUE;
            wsscfgIsSetFsDot11AuthMappingEntry.bIfIndex = OSIX_TRUE;
            wsscfgIsSetFsDot11AuthMappingEntry.bFsDot11AuthMappingProfileName =
                OSIX_TRUE;

            if (WsscfgTestAllFsDot11AuthMappingTable
                (&u4ErrorCode, &wsscfgFsDot11AuthMappingEntry,
                 &wsscfgIsSetFsDot11AuthMappingEntry, OSIX_TRUE,
                 OSIX_TRUE) != OSIX_SUCCESS)
            {
                WSS_WEB_TRC (" WsscfgTestAllFsDot11AuthMappingTable FAILED\n");
                IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
                WebnmUnRegisterLock (pHttp);
                WSSCFG_UNLOCK;
                free_octetstring (pWlanSSID);
                return;
            }
            if (WsscfgSetAllFsDot11AuthMappingTable
                (&wsscfgFsDot11AuthMappingEntry,
                 &wsscfgIsSetFsDot11AuthMappingEntry, OSIX_TRUE,
                 OSIX_TRUE) != OSIX_SUCCESS)
            {
                WSS_WEB_TRC (" WsscfgSetAllFsDot11AuthMappingTable FAILED\n");
                IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
                WebnmUnRegisterLock (pHttp);
                WSSCFG_UNLOCK;
                free_octetstring (pWlanSSID);
                return;
            }

        }

        /* QOS Profile Association with WLAN */
        if (STRCMP (au1QosProfileName, "default"))
        {
            wsscfgFsDot11QosMappingEntry.MibObject.
                i4FsDot11QosMappingRowStatus = ACTIVE;
            wsscfgFsDot11QosMappingEntry.MibObject.i4IfIndex = i4WlanIfIndex;
            wsscfgFsDot11QosMappingEntry.MibObject.
                i4FsDot11QosMappingProfileNameLen =
                (INT4) (STRLEN (au1QosProfileName));
            STRNCPY (wsscfgFsDot11QosMappingEntry.MibObject.
                     au1FsDot11QosMappingProfileName, au1QosProfileName,
                     (sizeof
                      (wsscfgFsDot11QosMappingEntry.MibObject.
                       au1FsDot11QosMappingProfileName) - 1));

            wsscfgIsSetFsDot11QosMappingEntry.bFsDot11QosMappingRowStatus =
                OSIX_TRUE;
            wsscfgIsSetFsDot11QosMappingEntry.bIfIndex = OSIX_TRUE;
            wsscfgIsSetFsDot11QosMappingEntry.bFsDot11QosMappingProfileName =
                OSIX_TRUE;

            if (WsscfgTestAllFsDot11QosMappingTable
                (&u4ErrorCode, &wsscfgFsDot11QosMappingEntry,
                 &wsscfgIsSetFsDot11QosMappingEntry, OSIX_TRUE,
                 OSIX_TRUE) != OSIX_SUCCESS)
            {
                WSS_WEB_TRC (" WsscfgTestAllFsDot11QosMappingTable FAILED\n");
                IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
                WebnmUnRegisterLock (pHttp);
                WSSCFG_UNLOCK;
                free_octetstring (pWlanSSID);
                return;
            }
            if (WsscfgSetAllFsDot11QosMappingTable
                (&wsscfgFsDot11QosMappingEntry,
                 &wsscfgIsSetFsDot11QosMappingEntry, OSIX_TRUE,
                 OSIX_TRUE) != OSIX_SUCCESS)
            {
                WSS_WEB_TRC (" WsscfgSetAllFsDot11QosMappingTable FAILED\n");
                IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
                WebnmUnRegisterLock (pHttp);
                WSSCFG_UNLOCK;
                free_octetstring (pWlanSSID);
                return;
            }
        }

        /* VLAN ID Configuration for the WLAN */
        wsscfgFsDot11StationConfigEntry.MibObject.i4FsDot11SupressSSID =
            WSS_DISABLED;
        wsscfgFsDot11StationConfigEntry.MibObject.i4FsDot11VlanId = i4VlanId;
        wsscfgFsDot11StationConfigEntry.MibObject.i4FsDot11BandwidthThresh =
            WSS_WLAN_DEF_BANDWIDTH_THRESH;
        wsscfgFsDot11StationConfigEntry.MibObject.i4IfIndex = i4WlanIfIndex;

        wsscfgIsSetFsDot11StationConfigEntry.bFsDot11SupressSSID = OSIX_TRUE;
        wsscfgIsSetFsDot11StationConfigEntry.bFsDot11VlanId = OSIX_TRUE;
        wsscfgIsSetFsDot11StationConfigEntry.bIfIndex = OSIX_TRUE;
        wsscfgIsSetFsDot11StationConfigEntry.bFsDot11BandwidthThresh
            = OSIX_TRUE;

        if (WsscfgTestAllFsDot11StationConfigTable
            (&u4ErrorCode, &wsscfgFsDot11StationConfigEntry,
             &wsscfgIsSetFsDot11StationConfigEntry) != OSIX_SUCCESS)
        {
            WSS_WEB_TRC ("WsscfgTestAllFsDot11StationConfigTable FAILED\n");
            IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
            WebnmUnRegisterLock (pHttp);
            WSSCFG_UNLOCK;
            free_octetstring (pWlanSSID);
            return;
        }

        if (WsscfgSetAllFsDot11StationConfigTable
            (&wsscfgFsDot11StationConfigEntry,
             &wsscfgIsSetFsDot11StationConfigEntry) != OSIX_SUCCESS)
        {
            WSS_WEB_TRC ("WsscfgSetAllFsDot11StationConfigTable FAILED\n");
            IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
            WebnmUnRegisterLock (pHttp);
            WSSCFG_UNLOCK;
            free_octetstring (pWlanSSID);
            return;
        }
    }
    else
    {
        WSS_WEB_TRC
            ("nmhGetCapwapDot11WlanRowStatus   returned SNMP_SUCCESS\n");
        IssSendError (pHttp, (CONST INT1 *) "Entry Already Exists!");
        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        free_octetstring (pWlanSSID);
        return;
    }
    gi4WlanProfileId = (INT4) u4ProfileId;

    WebnmUnRegisterLock (pHttp);
    WSSCFG_UNLOCK;
    IssProcessWlanCreatePageGet (pHttp);
    free_octetstring (pWlanSSID);
    return;

}

/*********************************************************************
*  Function Name : IssProcessWlanGeneralPage 
*  Description   : This function processes the request coming for the 
*                  WLAN Profile Table Page 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessWlanGeneralPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessWlanGeneralPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessWlanGeneralPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessWlanGeneralPageGet 
*  Description   : This function processes the get request coming for the  
*                  WLAN General page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessWlanGeneralPageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE *wlanSSID = NULL;
    INT4                i4Status = 0, i4SuppressSSID = 0;
    INT4                i4VlanId = 0, i4L2Isolation = 0, i4WlanIfIndex =
        0, i4PowerConstraint = 0, i4Dtimperiod = 1, i4ManagmentSSID =
        0, i4ManagmentSSIDCheck = 0;
    UINT4               u4Temp = 0;
    UINT4               u4MaxClientCount = 0;
    UINT4               u4WlanProfileId = 0;
    UINT1               au1WlanSSID[OCTETSTR_SIZE];
    UINT4               u4WlanIfIndex = 0;
    UINT4               u4WlanIfIndex1 = 0;
    UINT4               u4StaCount = 0;
    UINT1               u1Index = 0;
    INT4                i4BandwidthThresh = 0;
    tWssWlanDB         *pWssWlanDB = NULL;
    tWsscfgCapwapDot11WlanEntry WsscfgGetCapwapDot11WlanEntry;
    tWsscfgFsDot11StationConfigEntry wsscfgGetFsDot11StationConfigEntry;
    tWsscfgFsVlanIsolationEntry wsscfgFsVlanIsolationEntry;
    tWsscfgDot11SpectrumManagementEntry wsscfgDot11SpectrumManagementEntry;
    tWsscfgDot11StationConfigEntry WsscfgGetDot11StationConfigEntry;
    u4WlanProfileId = (UINT4) gi4WlanProfileId;

    {
        STRCPY (pHttp->au1Name, "WlanProfileId");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        issDecodeSpecialChar (pHttp->au1Value);
        u4WlanProfileId = (UINT4) ATOI ((INT1 *) pHttp->au1Value);
        if (((UINT4) gi4WlanProfileId != u4WlanProfileId)
            && (u4WlanProfileId != 0))
        {
            gi4WlanProfileId = (INT4) u4WlanProfileId;
        }
    }
    u4Temp = (UINT4) pHttp->i4Write;

    WSS_WEB_TRC ("Current profile Id is %d \n", gi4WlanProfileId);
    wlanSSID = (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (wlanSSID == NULL)
    {
        return;
    }
    MEMSET (wlanSSID->pu1_OctetList, 0, OCTETSTR_SIZE);
    MEMSET (au1WlanSSID, 0, OCTETSTR_SIZE);

    MEMSET (&WsscfgGetCapwapDot11WlanEntry, 0,
            sizeof (tWsscfgCapwapDot11WlanEntry));
    MEMSET (&wsscfgGetFsDot11StationConfigEntry, 0,
            sizeof (tWsscfgFsDot11StationConfigEntry));
    MEMSET (&wsscfgFsVlanIsolationEntry, 0,
            sizeof (tWsscfgFsVlanIsolationEntry));
    MEMSET (&wsscfgDot11SpectrumManagementEntry, 0,
            sizeof (tWsscfgDot11SpectrumManagementEntry));
    MEMSET (&WsscfgGetDot11StationConfigEntry, 0,
            sizeof (tWsscfgDot11StationConfigEntry));
    WebnmRegisterLock (pHttp, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;
    if (nmhGetCapwapDot11WlanProfileIfIndex
        ((UINT4) gi4WlanProfileId, &i4WlanIfIndex) != SNMP_SUCCESS)
    {
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
    }
    else
    {

        WsscfgGetCapwapDot11WlanEntry.MibObject.u4CapwapDot11WlanProfileId =
            (UINT4) gi4WlanProfileId;
        if (WsscfgGetAllCapwapDot11WlanTable (&WsscfgGetCapwapDot11WlanEntry) !=
            OSIX_SUCCESS)
        {
            WebnmUnRegisterLock (pHttp);
            WSSCFG_UNLOCK;
            free_octetstring (wlanSSID);
            return;
        }
        if (WsscfgGetCapwapDot11WlanEntry.MibObject.
            i4CapwapDot11WlanRowStatus != ACTIVE)
            i4Status = NOT_IN_SERVICE;
        else
            i4Status = ACTIVE;

        WssCfGetDot11DesiredSSID ((UINT4) i4WlanIfIndex, au1WlanSSID);

        wsscfgGetFsDot11StationConfigEntry.MibObject.i4IfIndex = i4WlanIfIndex;
        WsscfgGetAllFsDot11StationConfigTable
            (&wsscfgGetFsDot11StationConfigEntry);
        i4VlanId = wsscfgGetFsDot11StationConfigEntry.MibObject.i4FsDot11VlanId;
        i4SuppressSSID =
            wsscfgGetFsDot11StationConfigEntry.MibObject.i4FsDot11SupressSSID;
        i4BandwidthThresh =
            wsscfgGetFsDot11StationConfigEntry.MibObject.
            i4FsDot11BandwidthThresh;

        WsscfgGetDot11StationConfigEntry.MibObject.i4IfIndex = i4WlanIfIndex;
        if (WsscfgGetAllDot11StationConfigTable
            (&WsscfgGetDot11StationConfigEntry) == OSIX_FAILURE)
        {
            WebnmUnRegisterLock (pHttp);
            WSSCFG_UNLOCK;
            free_octetstring (wlanSSID);
            return;

        }

        i4Dtimperiod =
            WsscfgGetDot11StationConfigEntry.MibObject.i4Dot11DTIMPeriod;

        wsscfgFsVlanIsolationEntry.MibObject.i4IfIndex = i4WlanIfIndex;
        WsscfgGetAllFsVlanIsolationTable (&wsscfgFsVlanIsolationEntry);
        i4L2Isolation = wsscfgFsVlanIsolationEntry.MibObject.i4FsVlanIsolation;

        wsscfgDot11SpectrumManagementEntry.MibObject.i4IfIndex = i4WlanIfIndex;
        if (WsscfgGetAllDot11SpectrumManagementTable
            (&wsscfgDot11SpectrumManagementEntry) == OSIX_FAILURE)
        {
            WebnmUnRegisterLock (pHttp);
            WSSCFG_UNLOCK;
            free_octetstring (wlanSSID);
            return;
        }
        i4PowerConstraint =
            wsscfgDot11SpectrumManagementEntry.MibObject.
            i4Dot11MitigationRequirement;

        WsscfgGetFsDot11ManagmentSSID ((UINT4 *) &i4ManagmentSSID);

        nmhGetFsWlanSSIDStatsMaxClientCount (u4WlanProfileId,
                                             &u4MaxClientCount);

        WssStaShowClient ();

        pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
        if (pWssWlanDB == NULL)
        {
            WebnmUnRegisterLock (pHttp);
            WSSCFG_UNLOCK;
            free_octetstring (wlanSSID);
            return;
        }

        MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
        pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId =
            (UINT2) u4WlanProfileId;
        pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
        pWssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_PROFILE_ENTRY,
                                      pWssWlanDB) != OSIX_SUCCESS)
        {
        }
        u4WlanIfIndex = pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;

        for (u1Index = 0; u1Index < gu4ClientWalkIndex; u1Index++)
        {
            MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
            pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex =
                gaWssClientSummary[u1Index].u4BssIfIndex;
            pWssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
            pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
            }
            u4WlanIfIndex1 = pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;
            if (u4WlanIfIndex1 == u4WlanIfIndex)
            {
                u4StaCount++;
            }
        }

        WSS_WEB_TRC (" i4WlanIfIndex:  %d \n", i4WlanIfIndex);
        WSS_WEB_TRC (" i4Status  :  %d \n", i4Status);
        WSS_WEB_TRC ("  mac type :  %d \n",
                     WsscfgGetCapwapDot11WlanEntry.MibObject.
                     i4CapwapDot11WlanMacType);
        WSS_WEB_TRC (" tunnel mode :  %d \n",
                     WsscfgGetCapwapDot11WlanEntry.MibObject.
                     au1CapwapDot11WlanTunnelMode[0]);
        WSS_WEB_TRC ("  wlanSSID :  %s \n", au1WlanSSID);
        WSS_WEB_TRC (" i4SuppressSSID :  %d \n", i4SuppressSSID);
        WSS_WEB_TRC (" i4VlanId :  %d \n", i4VlanId);
        WSS_WEB_TRC (" i4L2Isolation :  %d \n", i4L2Isolation);
        WSS_WEB_TRC (" i4PowerConstraint :  %d \n", i4PowerConstraint);
        WSS_WEB_TRC (" i4Dtimperiod :  %d \n", i4Dtimperiod);
        WSS_WEB_TRC (" Associate clients :  %d \n", u4StaCount);
        WSS_WEB_TRC (" i4ManagmentSsid :  %d \n", i4ManagmentSSID);
        WSS_WEB_TRC (" BandwidthThresh :  %d \n", i4BandwidthThresh);

        STRCPY (pHttp->au1KeyString, "WLAN_SSID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1WlanSSID);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (HTTP_STRLEN (pHttp->au1DataString)));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString, "WLAN_MAC_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                 WsscfgGetCapwapDot11WlanEntry.MibObject.
                 i4CapwapDot11WlanMacType);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (HTTP_STRLEN (pHttp->au1DataString)));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString, "WLAN_TUNNEL_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                 WsscfgGetCapwapDot11WlanEntry.MibObject.
                 au1CapwapDot11WlanTunnelMode[0]);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (HTTP_STRLEN (pHttp->au1DataString)));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString, "StatusKey");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Status);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (HTTP_STRLEN (pHttp->au1DataString)));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString, "SuppressSSIDKey");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4SuppressSSID);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (HTTP_STRLEN (pHttp->au1DataString)));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString, "VLAN_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4VlanId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (HTTP_STRLEN (pHttp->au1DataString)));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString, "L2IsolationKey");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4L2Isolation);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (HTTP_STRLEN (pHttp->au1DataString)));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString, "PowerConstraintKey");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4PowerConstraint);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (HTTP_STRLEN (pHttp->au1DataString)));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString, "DtimPeriodKey");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Dtimperiod);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (HTTP_STRLEN (pHttp->au1DataString)));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        if (i4ManagmentSSID == gi4WlanProfileId)
        {
            i4ManagmentSSIDCheck = 1;
        }
        STRCPY (pHttp->au1KeyString, "ManagmentSSIDKey");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4ManagmentSSIDCheck);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (HTTP_STRLEN (pHttp->au1DataString)));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString, "MAX_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4MaxClientCount);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

        STRCPY (pHttp->au1KeyString, "ASSOC_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4StaCount);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

        STRCPY (pHttp->au1KeyString, "BANDWIDTH_THRESH_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4BandwidthThresh);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (HTTP_STRLEN (pHttp->au1DataString)));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
    }

    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    WebnmUnRegisterLock (pHttp);
    WSSCFG_UNLOCK;

    free_octetstring (wlanSSID);

    UNUSED_PARAM (u4Temp);
    return;
}

/*********************************************************************
*  Function Name : IssProcessWlanGeneralPageSet
*  Description   : This function processes the set request coming for the  
*                  WLAN Profile Table Entry page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessWlanGeneralPageSet (tHttp * pHttp)
{
    INT4                i4MacType = 0, i4TunnelModeLen = 1;
    UINT1               au1SSID[VLAN_STATIC_MAX_NAME_LEN + 1];
    UINT1               au1Interface[VLAN_STATIC_MAX_NAME_LEN + 1];
    UINT1               au1Security[VLAN_STATIC_MAX_NAME_LEN + 1];
    UINT1               tunnelModeBit = 0;
    INT4                i4Status = 0;
    INT4                i4BroadCastSSID = 0;
    INT4                i4L2Isolation = 0;
    INT4                i4VlanId = 0;
    INT4                i4IfIndex = 0;
    INT4                i4PowerConstraint = 0;
    INT4                i4DtimPeriod = 0;
    UINT4               u4ManagmentSSID = 0;
    INT4                i4ManagmentSSIDCheck = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4WlanProfileId = 0;
    UINT4               u4MaxClientCount = 0;
    UINT4               u4BandwidthThresh = 0;
    tWsscfgFsDot11WlanAuthenticationProfileEntry
        wsscfgFsDot11WlanAuthenticationProfileEntry;
    tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry
        wsscfgIsSetFsDot11WlanAuthenticationProfileEntry;
    tWsscfgFsDot11StationConfigEntry wsscfgFsDot11StationConfigEntry;
    tWsscfgIsSetFsDot11StationConfigEntry wsscfgIsSetFsDot11StationConfigEntry;
    tWsscfgFsVlanIsolationEntry wsscfgSetFsVlanIsolationEntry;
    tWsscfgIsSetFsVlanIsolationEntry wsscfgIsSetFsVlanIsolationEntry;
    tWsscfgDot11SpectrumManagementEntry wsscfgDot11SpectrumManagementEntry;
    tWsscfgIsSetDot11SpectrumManagementEntry
        wsscfgIsSetDot11SpectrumManagementEntry;
    tWsscfgDot11StationConfigEntry WsscfgSetDot11StationConfigEntry;
    tWsscfgIsSetDot11StationConfigEntry WsscfgIsSetDot11StationConfigEntry;
    tWsscfgCapwapDot11WlanEntry wsscfgCapwapDot11WlanEntry;
    tWsscfgIsSetCapwapDot11WlanEntry wsscfgIsSetCapwapDot11WlanEntry;
    tWsscfgFsDot11WlanCapabilityProfileEntry
        wsscfgFsDot11WlanCapabilityProfileEntry;

    MEMSET (&wsscfgIsSetFsDot11StationConfigEntry, 0,
            sizeof (tWsscfgIsSetFsDot11StationConfigEntry));
    MEMSET (&wsscfgFsDot11StationConfigEntry, 0,
            sizeof (tWsscfgFsDot11StationConfigEntry));
    MEMSET (&wsscfgFsDot11WlanAuthenticationProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanAuthenticationProfileEntry));
    MEMSET (&wsscfgIsSetFsDot11WlanAuthenticationProfileEntry, 0,
            sizeof (tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry));
    MEMSET (&wsscfgSetFsVlanIsolationEntry, 0,
            sizeof (tWsscfgFsVlanIsolationEntry));
    MEMSET (&wsscfgIsSetFsVlanIsolationEntry, 0,
            sizeof (tWsscfgIsSetFsVlanIsolationEntry));
    MEMSET (&wsscfgDot11SpectrumManagementEntry, 0,
            sizeof (tWsscfgDot11SpectrumManagementEntry));
    MEMSET (&wsscfgIsSetDot11SpectrumManagementEntry, 0,
            sizeof (tWsscfgIsSetDot11SpectrumManagementEntry));
    MEMSET (&WsscfgSetDot11StationConfigEntry, 0,
            sizeof (tWsscfgDot11StationConfigEntry));
    MEMSET (&WsscfgIsSetDot11StationConfigEntry, 0,
            sizeof (tWsscfgIsSetDot11StationConfigEntry));
    MEMSET (&wsscfgCapwapDot11WlanEntry, 0,
            sizeof (tWsscfgCapwapDot11WlanEntry));
    MEMSET (&wsscfgIsSetCapwapDot11WlanEntry, 0,
            sizeof (tWsscfgIsSetCapwapDot11WlanEntry));
    MEMSET (&wsscfgFsDot11WlanCapabilityProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanCapabilityProfileEntry));

    MEMSET (&au1SSID, 0, VLAN_STATIC_MAX_NAME_LEN + 1);
    MEMSET (&au1Security, 0, VLAN_STATIC_MAX_NAME_LEN + 1);
    MEMSET (&au1Interface, 0, VLAN_STATIC_MAX_NAME_LEN + 1);

    WebnmRegisterLock (pHttp, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;

    STRCPY (pHttp->au1Name, "WLAN_SSID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1SSID, pHttp->au1Value, (sizeof (au1SSID) - 1));

    STRCPY (pHttp->au1Name, "WLAN_MAC_TYPE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4MacType = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "WLAN_TUNNEL_MODE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    tunnelModeBit = (UINT1) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Status");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Status = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "SuppressSSID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4BroadCastSSID = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "VlanId");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4VlanId = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "L2Isolation");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4L2Isolation = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "PowerConstraint");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PowerConstraint = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "DtimPeriod");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4DtimPeriod = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ManagmentSSIDCheck");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4ManagmentSSIDCheck = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Maximum_Client_Count");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4MaxClientCount = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Bandwidth_Threshold");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4BandwidthThresh = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    WSS_WEB_TRC ("Values from General table\n");
    WSS_WEB_TRC ("*************************\n");

    WSS_WEB_TRC ("SSID   : %s \n", au1SSID);
    WSS_WEB_TRC ("Status  : %d \n", i4Status);
    WSS_WEB_TRC ("Vlan Id : %d \n", i4VlanId);
    WSS_WEB_TRC ("BraodCast SSID  : %d\n", i4BroadCastSSID);
    WSS_WEB_TRC ("L2 Isolation: %d\n", i4L2Isolation);
    WSS_WEB_TRC ("Power Constraint: %d\n", i4PowerConstraint);
    WSS_WEB_TRC (" Maximum Client Count :%d\n", u4MaxClientCount);
    WSS_WEB_TRC ("Managment SSID Check: %d\n", i4ManagmentSSIDCheck);
    WSS_WEB_TRC ("Dtim Period: %d\n", i4DtimPeriod);

    nmhGetCapwapDot11WlanProfileIfIndex ((UINT4) gi4WlanProfileId, &i4IfIndex);

    /* VLAN ID Configuration for the WLAN */
    wsscfgFsDot11StationConfigEntry.MibObject.i4FsDot11SupressSSID =
        i4BroadCastSSID;
    wsscfgFsDot11StationConfigEntry.MibObject.i4FsDot11VlanId = i4VlanId;
    wsscfgFsDot11StationConfigEntry.MibObject.i4IfIndex = i4IfIndex;
    wsscfgFsDot11StationConfigEntry.MibObject.i4FsDot11BandwidthThresh =
        (INT4) u4BandwidthThresh;

    wsscfgIsSetFsDot11StationConfigEntry.bFsDot11SupressSSID = OSIX_TRUE;
    wsscfgIsSetFsDot11StationConfigEntry.bFsDot11VlanId = OSIX_TRUE;
    wsscfgIsSetFsDot11StationConfigEntry.bFsDot11BandwidthThresh = OSIX_TRUE;
    wsscfgIsSetFsDot11StationConfigEntry.bIfIndex = OSIX_TRUE;

    if (WsscfgTestAllFsDot11StationConfigTable
        (&u4ErrorCode, &wsscfgFsDot11StationConfigEntry,
         &wsscfgIsSetFsDot11StationConfigEntry) != OSIX_SUCCESS)
    {
        WSS_WEB_TRC ("WsscfgTestAllFsDot11StationConfigTable FAILED\n");
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        return;
    }

    if (WsscfgSetAllFsDot11StationConfigTable (&wsscfgFsDot11StationConfigEntry,
                                               &wsscfgIsSetFsDot11StationConfigEntry)
        != OSIX_SUCCESS)
    {
        WSS_WEB_TRC ("WsscfgSetAllFsDot11StationConfigTable FAILED\n");
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        return;
    }

    /*Client count per SSID */

    u4WlanProfileId = (UINT4) gi4WlanProfileId;

    if (nmhTestv2FsWlanSSIDStatsMaxClientCount (&u4ErrorCode, u4WlanProfileId,
                                                u4MaxClientCount) !=
        SNMP_SUCCESS)
    {
        WSS_WEB_TRC ("nmhTestv2FsWlanSSIDStatsMaxClientCount FAILED!!!!1\n");
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        return;

    }

    if (nmhSetFsWlanSSIDStatsMaxClientCount (u4WlanProfileId,
                                             u4MaxClientCount) != SNMP_SUCCESS)
    {
        WSS_WEB_TRC ("nmhSetFsWlanSSIDStatsMaxClientCount FAILED!\n");
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        return;
    }

    WsscfgSetDot11StationConfigEntry.MibObject.i4IfIndex = i4IfIndex;
    WsscfgSetDot11StationConfigEntry.MibObject.i4Dot11DTIMPeriod = i4DtimPeriod;
    WsscfgIsSetDot11StationConfigEntry.bDot11DTIMPeriod = OSIX_TRUE;

    if (WsscfgTestAllDot11StationConfigTable (&u4ErrorCode,
                                              &WsscfgSetDot11StationConfigEntry,
                                              &WsscfgIsSetDot11StationConfigEntry)
        != OSIX_SUCCESS)
    {
        WSS_WEB_TRC ("WsscfgTestAllDot11StationConfigTable FAILED\n");
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        return;
    }

    if (WsscfgSetAllDot11StationConfigTable (&WsscfgSetDot11StationConfigEntry,
                                             &WsscfgIsSetDot11StationConfigEntry)
        != OSIX_SUCCESS)
    {
        WSS_WEB_TRC ("WsscfgSetAllDot11StationConfigTable FAILED\n");
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        return;
    }
    wsscfgSetFsVlanIsolationEntry.MibObject.i4IfIndex = i4IfIndex;
    wsscfgSetFsVlanIsolationEntry.MibObject.i4FsVlanIsolation = i4L2Isolation;

    wsscfgIsSetFsVlanIsolationEntry.bFsVlanIsolation = OSIX_TRUE;
    wsscfgIsSetFsVlanIsolationEntry.bIfIndex = OSIX_TRUE;

    if (WsscfgTestAllFsVlanIsolationTable
        (&u4ErrorCode, &wsscfgSetFsVlanIsolationEntry,
         &wsscfgIsSetFsVlanIsolationEntry) != OSIX_SUCCESS)
    {
        WSS_WEB_TRC ("WsscfgTestAllFsVlanIsolationTable FAILED\n");
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        return;
    }
    WsscfgSetAllFsVlanIsolationTable (&wsscfgSetFsVlanIsolationEntry,
                                      &wsscfgIsSetFsVlanIsolationEntry);

    wsscfgDot11SpectrumManagementEntry.MibObject.i4IfIndex = i4IfIndex;
    wsscfgDot11SpectrumManagementEntry.MibObject.i4Dot11MitigationRequirement =
        i4PowerConstraint;

    wsscfgIsSetDot11SpectrumManagementEntry.bDot11MitigationRequirement =
        OSIX_TRUE;
    wsscfgIsSetDot11SpectrumManagementEntry.bIfIndex = OSIX_TRUE;

    if (WsscfgTestAllDot11SpectrumManagementTable
        (&u4ErrorCode, &wsscfgDot11SpectrumManagementEntry,
         &wsscfgIsSetDot11SpectrumManagementEntry) != OSIX_SUCCESS)
    {
        WSS_WEB_TRC ("WsscfgTestAllDot11SpectrumManagementTable FAILED\n");
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        return;
    }
    if (WsscfgSetAllDot11SpectrumManagementTable
        (&wsscfgDot11SpectrumManagementEntry,
         &wsscfgIsSetDot11SpectrumManagementEntry) != OSIX_SUCCESS)
    {
        WSS_WEB_TRC ("WsscfgSetAllFsVlanIsolationTable FAILED\n");
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        return;
    }

    nmhSetDot11MitigationRequirement (i4IfIndex, 0, i4PowerConstraint);

    if (i4ManagmentSSIDCheck == 1)
    {
        nmhSetFsDot11ManagmentSSID ((UINT4) gi4WlanProfileId);
    }
    else
    {
        nmhGetFsDot11ManagmentSSID (&u4ManagmentSSID);

        if (gi4WlanProfileId == (INT4) u4ManagmentSSID)
        {

            WSS_WEB_TRC ("Disabling Managment SSID FAILED\n");
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Invalid Configuration:Disabling Managment SSID not allowed\n");
            WebnmUnRegisterLock (pHttp);
            WSSCFG_UNLOCK;
            return;
        }
    }
    if (i4Status == NOT_IN_SERVICE)
    {
        wsscfgCapwapDot11WlanEntry.MibObject.u4CapwapDot11WlanProfileId =
            (UINT4) gi4WlanProfileId;
        wsscfgCapwapDot11WlanEntry.MibObject.i4CapwapDot11WlanMacType =
            i4MacType;
        wsscfgCapwapDot11WlanEntry.MibObject.i4CapwapDot11WlanRowStatus =
            NOT_IN_SERVICE;
        wsscfgCapwapDot11WlanEntry.MibObject.au1CapwapDot11WlanTunnelMode[0] =
            tunnelModeBit;
        wsscfgCapwapDot11WlanEntry.MibObject.i4CapwapDot11WlanTunnelModeLen =
            i4TunnelModeLen;

        /* Fill the booleans indicating presence of corresponding field */
        wsscfgIsSetCapwapDot11WlanEntry.bCapwapDot11WlanProfileId = OSIX_TRUE;
        wsscfgIsSetCapwapDot11WlanEntry.bCapwapDot11WlanMacType = OSIX_TRUE;
        wsscfgIsSetCapwapDot11WlanEntry.bCapwapDot11WlanTunnelMode = OSIX_TRUE;
        wsscfgIsSetCapwapDot11WlanEntry.bCapwapDot11WlanRowStatus = OSIX_TRUE;

        if (WsscfgTestAllCapwapDot11WlanTable
            (&u4ErrorCode, &wsscfgCapwapDot11WlanEntry,
             &wsscfgIsSetCapwapDot11WlanEntry, OSIX_TRUE,
             OSIX_TRUE) != OSIX_SUCCESS)
        {
            WSS_WEB_TRC ("WsscfgTestAllCapwapDot11WlanTable FAILED!!!!1\n");
            IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
            WebnmUnRegisterLock (pHttp);
            WSSCFG_UNLOCK;
            return;
        }

        if (WsscfgSetAllCapwapDot11WlanTable (&wsscfgCapwapDot11WlanEntry,
                                              &wsscfgIsSetCapwapDot11WlanEntry,
                                              OSIX_TRUE,
                                              OSIX_TRUE) != OSIX_SUCCESS)
        {
            WSS_WEB_TRC ("WsscfgSetAllCapwapDot11WlanTable FAILED!\n");
            IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
            WebnmUnRegisterLock (pHttp);
            WSSCFG_UNLOCK;
            return;
        }
    }
    if (i4Status == ACTIVE)
    {
        MEMSET (&wsscfgIsSetCapwapDot11WlanEntry, 0,
                sizeof (tWsscfgIsSetCapwapDot11WlanEntry));
        MEMSET (&wsscfgCapwapDot11WlanEntry, 0,
                sizeof (tWsscfgCapwapDot11WlanEntry));

        wsscfgCapwapDot11WlanEntry.MibObject.u4CapwapDot11WlanProfileId =
            (UINT4) gi4WlanProfileId;
        wsscfgCapwapDot11WlanEntry.MibObject.i4CapwapDot11WlanMacType =
            i4MacType;
        wsscfgCapwapDot11WlanEntry.MibObject.i4CapwapDot11WlanRowStatus =
            ACTIVE;
        wsscfgCapwapDot11WlanEntry.MibObject.au1CapwapDot11WlanTunnelMode[0] =
            tunnelModeBit;
        wsscfgCapwapDot11WlanEntry.MibObject.i4CapwapDot11WlanTunnelModeLen =
            i4TunnelModeLen;

        /* Fill the booleans indicating presence of corresponding field */
        wsscfgIsSetCapwapDot11WlanEntry.bCapwapDot11WlanProfileId = OSIX_TRUE;
        wsscfgIsSetCapwapDot11WlanEntry.bCapwapDot11WlanMacType = OSIX_TRUE;
        wsscfgIsSetCapwapDot11WlanEntry.bCapwapDot11WlanTunnelMode = OSIX_TRUE;
        wsscfgIsSetCapwapDot11WlanEntry.bCapwapDot11WlanRowStatus = OSIX_TRUE;

        if (WsscfgTestAllCapwapDot11WlanTable
            (&u4ErrorCode, &wsscfgCapwapDot11WlanEntry,
             &wsscfgIsSetCapwapDot11WlanEntry, OSIX_TRUE,
             OSIX_TRUE) != OSIX_SUCCESS)
        {
            WSS_WEB_TRC ("WsscfgTestAllCapwapDot11WlanTable FAILED!!!!1\n");
            IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
            WebnmUnRegisterLock (pHttp);
            WSSCFG_UNLOCK;
            return;
        }

        if (WsscfgSetAllCapwapDot11WlanTable (&wsscfgCapwapDot11WlanEntry,
                                              &wsscfgIsSetCapwapDot11WlanEntry,
                                              OSIX_TRUE,
                                              OSIX_TRUE) != OSIX_SUCCESS)
        {
            WSS_WEB_TRC ("WsscfgSetAllCapwapDot11WlanTable FAILED!\n");
            IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
            WebnmUnRegisterLock (pHttp);
            WSSCFG_UNLOCK;
            return;
        }

    }
    WebnmUnRegisterLock (pHttp);
    WSSCFG_UNLOCK;
    IssProcessWlanGeneralPageGet (pHttp);
    return;
}

/*********************************************************************
*  Function Name : IssProcessWlanSecurityPage 
*  Description   : This function processes the request coming for the 
*                  WLAN Security  Page 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessWlanSecurityPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessWlanSecurityPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessWlanSecurityPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessWlanSecurityPageGet 
*  Description   : This function processes the get request coming for the  
*                  WLAN Security Entry page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessWlanSecurityPageGet (tHttp * pHttp)
{
    INT4                i4Authentication = 0, i4WepKeyType = 0, i4WepKeyLength =
        0, i4WlanIfIndex = 0, i4WebAuth = 0, i4WepKeyIndex = 0;
    tSNMP_OCTET_STRING_TYPE FsDot11WlanWebAuthRedirectFileName;
    INT4                i4SetValFsDot11WlanLoginAuthentication = 0;
    tWsscfgFsDot11WlanAuthenticationProfileEntry
        wsscfgFsDot11WlanAuthenticationProfileEntry;
    UINT1               au1FileName[ISS_WEB_AUTH_FILE_NAME];

    MEMSET (&FsDot11WlanWebAuthRedirectFileName, 0,
            sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1FileName, 0, ISS_WEB_AUTH_FILE_NAME);
    MEMSET (&wsscfgFsDot11WlanAuthenticationProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanAuthenticationProfileEntry));

    WebnmRegisterLock (pHttp, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;
    /* Get the WLAN IFINDEX */
    nmhGetCapwapDot11WlanProfileIfIndex ((UINT4) gi4WlanProfileId,
                                         &i4WlanIfIndex);
    FsDot11WlanWebAuthRedirectFileName.pu1_OctetList = au1FileName;
    nmhGetFsDot11WlanWebAuthRedirectFileName (i4WlanIfIndex,
                                              &FsDot11WlanWebAuthRedirectFileName);
    wsscfgFsDot11WlanAuthenticationProfileEntry.MibObject.i4IfIndex =
        i4WlanIfIndex;
    nmhGetFsDot11WlanLoginAuthentication (i4WlanIfIndex,
                                          &i4SetValFsDot11WlanLoginAuthentication);

    if (WsscfgGetAllFsDot11WlanAuthenticationProfileTable
        (&wsscfgFsDot11WlanAuthenticationProfileEntry) == OSIX_SUCCESS)
    {
        i4Authentication =
            wsscfgFsDot11WlanAuthenticationProfileEntry.MibObject.
            i4FsDot11WlanAuthenticationAlgorithm;
        i4WepKeyType =
            wsscfgFsDot11WlanAuthenticationProfileEntry.MibObject.
            i4FsDot11WlanWepKeyType;
        i4WepKeyLength =
            wsscfgFsDot11WlanAuthenticationProfileEntry.MibObject.
            i4FsDot11WlanWepKeyLength;
        i4WebAuth =
            wsscfgFsDot11WlanAuthenticationProfileEntry.MibObject.
            i4FsDot11WlanWebAuthentication;
        i4WepKeyIndex =
            wsscfgFsDot11WlanAuthenticationProfileEntry.MibObject.
            i4FsDot11WlanWepKeyIndex;
    }

    WSS_WEB_TRC ("i4WlanIfIndex : %d\n", i4WlanIfIndex);
    WSS_WEB_TRC ("i4AuthAlgo :%d\n", i4Authentication);
    WSS_WEB_TRC ("i4WepKeyType :%d\n", i4WepKeyType);
    WSS_WEB_TRC ("i4WepKeyLength :%d\n", i4WepKeyLength);
    if (i4WebAuth == 2)
    {
        i4WebAuth = 0;
    }
    WSS_WEB_TRC ("i4WebAuth :%d\n", i4WebAuth);
    WSS_WEB_TRC ("i4WepKeyIndex :%d\n", i4WepKeyIndex);

    STRCPY (pHttp->au1KeyString, "AuthAlgoKey");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Authentication);

    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "WebAuthKey");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4WebAuth);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "LoginAuthKey");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
             i4SetValFsDot11WlanLoginAuthentication);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "KeyIndexKey");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4WepKeyIndex);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "KeyTypeKey");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4WepKeyType);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "KeyLengthKey");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4WepKeyLength);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "KEY_VALUE");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "");
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "FILE_NAME_VALUE");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
             FsDot11WlanWebAuthRedirectFileName.pu1_OctetList);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    WebnmUnRegisterLock (pHttp);
    WSSCFG_UNLOCK;

}

/*********************************************************************
*  Function Name : IssProcessWlanSecurityPageSet 
*  Description   : This function processes the set request coming for the  
*                  WLAN Security page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessWlanSecurityPageSet (tHttp * pHttp)
{

    INT4                i4Security = 0, i4KeyIndex = 0, i4WlanIfIndex =
        0, i4KeyType = 0;
    INT4                i4Privacy = 0, i4KeyLength = 0, i4WebAuth = 0;
    INT4                i4SetValFsDot11WlanLoginAuthentication = 0;
    UINT1               au1Key[13];
    UINT1               au1FileName[ISS_WEB_AUTH_FILE_NAME];
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE FsDot11WlanWebAuthRedirectFileName;
    tWsscfgFsDot11WlanAuthenticationProfileEntry
        wsscfgFsDot11WlanAuthenticationProfileEntry;
    tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry
        wsscfgIsSetFsDot11WlanAuthenticationProfileEntry;
    tWsscfgFsDot11WlanCapabilityProfileEntry
        wsscfgFsDot11WlanCapabilityProfileEntry;

    MEMSET (&FsDot11WlanWebAuthRedirectFileName, 0,
            sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&wsscfgFsDot11WlanAuthenticationProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanAuthenticationProfileEntry));
    MEMSET (&wsscfgIsSetFsDot11WlanAuthenticationProfileEntry, 0,
            sizeof (tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry));
    MEMSET (&wsscfgFsDot11WlanCapabilityProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanCapabilityProfileEntry));
    MEMSET (au1Key, 0, 13);
    MEMSET (au1FileName, 0, ISS_WEB_AUTH_FILE_NAME);

    WebnmRegisterLock (pHttp, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;

    FsDot11WlanWebAuthRedirectFileName.pu1_OctetList = au1FileName;
    STRCPY (pHttp->au1Name, "AuthAlgo");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Security = (INT4) ATOI ((INT1 *) pHttp->au1Value);
    if (i4Security == CLI_AUTH_ALGO_SHARED)
    {
        /* Get the WLAN IFINDEX */
        nmhGetCapwapDot11WlanProfileIfIndex ((UINT4) gi4WlanProfileId,
                                             &i4WlanIfIndex);
        wsscfgFsDot11WlanCapabilityProfileEntry.MibObject.i4IfIndex =
            i4WlanIfIndex;
        if (WsscfgGetAllFsDot11WlanCapabilityProfileTable
            (&wsscfgFsDot11WlanCapabilityProfileEntry) == OSIX_FAILURE)
        {
            WebnmUnRegisterLock (pHttp);
            WSSCFG_UNLOCK;
            return;
        }
        i4Privacy =
            wsscfgFsDot11WlanCapabilityProfileEntry.MibObject.
            i4FsDot11WlanPrivacyOptionImplemented;
        if (i4Privacy == CLI_WSSCFG_DISABLE)
        {
            WSS_WEB_TRC ("No privacy option enabled, Inconsistent input\n");
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "No privacy option enabled, Inconsistent input\n");
            WebnmUnRegisterLock (pHttp);
            WSSCFG_UNLOCK;
            return;
        }
    }

    STRCPY (pHttp->au1Name, "WebAuth");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4WebAuth = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "LoginAuth");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4SetValFsDot11WlanLoginAuthentication =
        (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "KeyIndex");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4KeyIndex = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "KeyType");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4KeyType = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "KeyLength");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4KeyLength = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "KeyValue");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1Key, pHttp->au1Value, (sizeof (au1Key) - 1));

    STRCPY (pHttp->au1Name, "FileNameValue");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1FileName, pHttp->au1Value, (sizeof (au1FileName) - 1));
    FsDot11WlanWebAuthRedirectFileName.i4_Length = STRLEN (au1FileName);

    WSS_WEB_TRC ("Values from  Security Table\n");
    WSS_WEB_TRC ("*************************\n");

    WSS_WEB_TRC ("Security : %d\n", i4Security);
    if (i4WebAuth == 0)
    {
        i4WebAuth = 2;
    }
    WSS_WEB_TRC ("WebAuth : %d\n", i4WebAuth);
    WSS_WEB_TRC ("KeyIndex : %d\n", i4KeyIndex);
    WSS_WEB_TRC ("KeyType : %d\n", i4KeyType);
    WSS_WEB_TRC ("KeyLength : %d\n", i4KeyLength);
    WSS_WEB_TRC ("ProfileId: %d\n", gi4WlanProfileId);

    nmhGetCapwapDot11WlanProfileIfIndex ((UINT4) gi4WlanProfileId,
                                         &i4WlanIfIndex);
    WSS_WEB_TRC ("Wlan IfIndex: %d\n", i4WlanIfIndex);

    wsscfgFsDot11WlanAuthenticationProfileEntry.MibObject.
        i4FsDot11WlanAuthenticationAlgorithm = i4Security;
    wsscfgFsDot11WlanAuthenticationProfileEntry.MibObject.
        i4FsDot11WlanWepKeyIndex = i4KeyIndex;
    wsscfgFsDot11WlanAuthenticationProfileEntry.MibObject.
        i4FsDot11WlanWepKeyType = i4KeyType;
    wsscfgFsDot11WlanAuthenticationProfileEntry.MibObject.i4IfIndex =
        i4WlanIfIndex;
    wsscfgFsDot11WlanAuthenticationProfileEntry.MibObject.
        i4FsDot11WlanWepKeyLength = i4KeyLength;
    wsscfgFsDot11WlanAuthenticationProfileEntry.MibObject.
        i4FsDot11WlanWebAuthentication = i4WebAuth;
    if (STRLEN (au1Key))
    {
        STRNCPY (wsscfgFsDot11WlanAuthenticationProfileEntry.MibObject.
                 au1FsDot11WlanWepKey, au1Key, (sizeof (au1Key) - 1));
        wsscfgFsDot11WlanAuthenticationProfileEntry.MibObject.
            i4FsDot11WlanWepKeyLen = (INT4) (STRLEN (au1Key));
    }
    wsscfgFsDot11WlanAuthenticationProfileEntry.MibObject.
        i4FsDot11WlanAuthenticationRowStatus = ACTIVE;

    wsscfgIsSetFsDot11WlanAuthenticationProfileEntry.
        bFsDot11WlanAuthenticationAlgorithm = OSIX_TRUE;
    wsscfgIsSetFsDot11WlanAuthenticationProfileEntry.bFsDot11WlanWepKeyIndex = OSIX_TRUE;    /*Key Index is not editable */
    wsscfgIsSetFsDot11WlanAuthenticationProfileEntry.bFsDot11WlanWepKeyLength =
        OSIX_TRUE;
    wsscfgIsSetFsDot11WlanAuthenticationProfileEntry.bFsDot11WlanWepKeyType =
        OSIX_TRUE;
    wsscfgIsSetFsDot11WlanAuthenticationProfileEntry.bIfIndex = OSIX_TRUE;
    wsscfgIsSetFsDot11WlanAuthenticationProfileEntry.
        bFsDot11WlanWebAuthentication = OSIX_TRUE;
    if (STRLEN (au1Key))
    {
        wsscfgIsSetFsDot11WlanAuthenticationProfileEntry.bFsDot11WlanWepKey =
            OSIX_TRUE;
    }
    wsscfgIsSetFsDot11WlanAuthenticationProfileEntry.
        bFsDot11WlanAuthenticationRowStatus = OSIX_TRUE;

    if (wsscfgFsDot11WlanAuthenticationProfileEntry.MibObject.
        i4FsDot11WlanAuthenticationAlgorithm == CLI_AUTH_ALGO_OPEN)
    {
        wsscfgIsSetFsDot11WlanAuthenticationProfileEntry.
            bFsDot11WlanWepKeyIndex = OSIX_FALSE;
        wsscfgIsSetFsDot11WlanAuthenticationProfileEntry.
            bFsDot11WlanWepKeyLength = OSIX_FALSE;
        wsscfgIsSetFsDot11WlanAuthenticationProfileEntry.
            bFsDot11WlanWepKeyType = OSIX_FALSE;
        wsscfgIsSetFsDot11WlanAuthenticationProfileEntry.
            bFsDot11WlanAuthenticationRowStatus = OSIX_FALSE;
    }

    if (WsscfgTestAllFsDot11WlanAuthenticationProfileTable
        (&u4ErrorCode, &wsscfgFsDot11WlanAuthenticationProfileEntry,
         &wsscfgIsSetFsDot11WlanAuthenticationProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        return;
    }

    wsscfgFsDot11WlanAuthenticationProfileEntry.MibObject.
        i4FsDot11WlanAuthenticationRowStatus = NOT_IN_SERVICE;
    if (WsscfgSetAllFsDot11WlanAuthenticationProfileTable
        (&wsscfgFsDot11WlanAuthenticationProfileEntry,
         &wsscfgIsSetFsDot11WlanAuthenticationProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        return;
    }

    wsscfgFsDot11WlanAuthenticationProfileEntry.MibObject.
        i4FsDot11WlanAuthenticationRowStatus = ACTIVE;

    if (WsscfgSetAllFsDot11WlanAuthenticationProfileTable
        (&wsscfgFsDot11WlanAuthenticationProfileEntry,
         &wsscfgIsSetFsDot11WlanAuthenticationProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        return;
    }
    if (nmhTestv2FsDot11WlanWebAuthRedirectFileName
        (&u4ErrorCode, i4WlanIfIndex,
         &FsDot11WlanWebAuthRedirectFileName) != SNMP_FAILURE)
    {
        if (nmhSetFsDot11WlanWebAuthRedirectFileName (i4WlanIfIndex,
                                                      &FsDot11WlanWebAuthRedirectFileName)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
            WebnmUnRegisterLock (pHttp);
            WSSCFG_UNLOCK;
            return;
        }
    }
    if (nmhTestv2FsDot11WlanLoginAuthentication
        (&u4ErrorCode, i4WlanIfIndex,
         i4SetValFsDot11WlanLoginAuthentication) != SNMP_FAILURE)
    {
        if (nmhSetFsDot11WlanLoginAuthentication
            (i4WlanIfIndex,
             i4SetValFsDot11WlanLoginAuthentication) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
            WebnmUnRegisterLock (pHttp);
            WSSCFG_UNLOCK;
            return;
        }
    }

    WebnmUnRegisterLock (pHttp);
    WSSCFG_UNLOCK;
    IssProcessWlanSecurityPageGet (pHttp);
    return;
}

/*********************************************************************
*  Function Name : IssProcessWlanQosPage 
*  Description   : This function processes the request coming for the 
*                  WLAN Qos Page 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessWlanQosPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessWlanQosPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessWlanQosPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessWlanQosPageSet 
*  Description   : This function processes the set request coming for the  
*                  WLAN Qos Entry page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessWlanQosPageSet (tHttp * pHttp)
{

    INT4                i4QosTraffic = 0, i4Trust = 0, i4WlanIfIndex =
        0, i4RateLimit;
    INT4                i4UpstreamCIR = 0, i4UpstreamEIR = 0, i4UpstreamCBS =
        0, i4UpstreamEBS = 0;
    INT4                i4DownstreamCIR = 0, i4DownstreamEIR =
        0, i4DownstreamCBS = 0, i4DownstreamEBS = 0;
    UINT4               u4ErrorCode = 0;
    tWsscfgFsDot11WlanQosProfileEntry wsscfgFsDot11WlanQosProfileEntry;
    tWsscfgIsSetFsDot11WlanQosProfileEntry
        wsscfgIsSetFsDot11WlanQosProfileEntry;

    MEMSET (&wsscfgFsDot11WlanQosProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanQosProfileEntry));
    MEMSET (&wsscfgIsSetFsDot11WlanQosProfileEntry, 0,
            sizeof (tWsscfgIsSetFsDot11WlanQosProfileEntry));

    WebnmRegisterLock (pHttp, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;

    STRCPY (pHttp->au1Name, "QoSTraffic");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4QosTraffic = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "QosTrustMode");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Trust = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "QosRateLimit");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4RateLimit = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "UpCmtInfoRate");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4UpstreamCIR = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "UpCmtBstSize");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4UpstreamCBS = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "UpExInfoRate");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4UpstreamEIR = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "UpExBstSize");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4UpstreamEBS = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "DownCmtInfoRate");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4DownstreamCIR = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "DownCmtBstSize");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4DownstreamCBS = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "DownExInfoRate");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4DownstreamEIR = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "DownExBstSize");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4DownstreamEBS = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    WSS_WEB_TRC ("Values from Qos Table \n*********************************\n");
    WSS_WEB_TRC ("Qos : %d\n", i4QosTraffic);
    WSS_WEB_TRC ("Trust: %d\n", i4Trust);
    WSS_WEB_TRC ("RateLimit : %d\n", i4RateLimit);
    WSS_WEB_TRC ("UpCmtInfoRate : %d\n", i4UpstreamCIR);
    WSS_WEB_TRC ("UpCmtBstSize : %d\n", i4UpstreamCBS);
    WSS_WEB_TRC ("UpExInfoRate : %d\n", i4UpstreamEIR);
    WSS_WEB_TRC ("UpExBstSize : %d\n", i4UpstreamEBS);
    WSS_WEB_TRC ("DownCmtInfoRate : %d\n", i4DownstreamCIR);
    WSS_WEB_TRC ("DownCmtBstSize : %d\n", i4DownstreamCBS);
    WSS_WEB_TRC ("DownExInfoRate : %d\n", i4DownstreamEIR);
    WSS_WEB_TRC ("DownExBstSize : %d\n", i4DownstreamEBS);
    WSS_WEB_TRC ("ProfileId: %d\n", gi4WlanProfileId);

    nmhGetCapwapDot11WlanProfileIfIndex ((UINT4) gi4WlanProfileId,
                                         &i4WlanIfIndex);
    WSS_WEB_TRC ("Wlan IfIndex: %d\n", i4WlanIfIndex);

    wsscfgFsDot11WlanQosProfileEntry.MibObject.i4FsDot11WlanQosTraffic =
        i4QosTraffic;
    wsscfgFsDot11WlanQosProfileEntry.MibObject.
        i4FsDot11WlanQosPassengerTrustMode = i4Trust;
    wsscfgFsDot11WlanQosProfileEntry.MibObject.i4FsDot11WlanQosRateLimit =
        i4RateLimit;
    wsscfgFsDot11WlanQosProfileEntry.MibObject.i4FsDot11WlanUpStreamCIR =
        i4UpstreamCIR;
    wsscfgFsDot11WlanQosProfileEntry.MibObject.i4FsDot11WlanUpStreamCBS =
        i4UpstreamCBS;
    wsscfgFsDot11WlanQosProfileEntry.MibObject.i4FsDot11WlanUpStreamEIR =
        i4UpstreamEIR;
    wsscfgFsDot11WlanQosProfileEntry.MibObject.i4FsDot11WlanUpStreamEBS =
        i4UpstreamEBS;
    wsscfgFsDot11WlanQosProfileEntry.MibObject.i4FsDot11WlanDownStreamCIR =
        i4DownstreamCIR;
    wsscfgFsDot11WlanQosProfileEntry.MibObject.i4FsDot11WlanDownStreamCBS =
        i4DownstreamCBS;
    wsscfgFsDot11WlanQosProfileEntry.MibObject.i4FsDot11WlanDownStreamEIR =
        i4DownstreamEIR;
    wsscfgFsDot11WlanQosProfileEntry.MibObject.i4FsDot11WlanDownStreamEBS =
        i4DownstreamEBS;
    wsscfgFsDot11WlanQosProfileEntry.MibObject.i4IfIndex = i4WlanIfIndex;
    wsscfgFsDot11WlanQosProfileEntry.MibObject.i4FsDot11WlanQosRowStatus =
        ACTIVE;

    wsscfgIsSetFsDot11WlanQosProfileEntry.bFsDot11WlanQosTraffic = OSIX_TRUE;
    wsscfgIsSetFsDot11WlanQosProfileEntry.bFsDot11WlanQosPassengerTrustMode =
        OSIX_TRUE;
    wsscfgIsSetFsDot11WlanQosProfileEntry.bFsDot11WlanQosRateLimit = OSIX_TRUE;
    wsscfgIsSetFsDot11WlanQosProfileEntry.bFsDot11WlanUpStreamCIR = OSIX_TRUE;
    wsscfgIsSetFsDot11WlanQosProfileEntry.bFsDot11WlanUpStreamCBS = OSIX_TRUE;
    wsscfgIsSetFsDot11WlanQosProfileEntry.bFsDot11WlanUpStreamEIR = OSIX_TRUE;
    wsscfgIsSetFsDot11WlanQosProfileEntry.bFsDot11WlanUpStreamEBS = OSIX_TRUE;
    wsscfgIsSetFsDot11WlanQosProfileEntry.bFsDot11WlanDownStreamCIR = OSIX_TRUE;
    wsscfgIsSetFsDot11WlanQosProfileEntry.bFsDot11WlanDownStreamCBS = OSIX_TRUE;
    wsscfgIsSetFsDot11WlanQosProfileEntry.bFsDot11WlanDownStreamEIR = OSIX_TRUE;
    wsscfgIsSetFsDot11WlanQosProfileEntry.bFsDot11WlanDownStreamEBS = OSIX_TRUE;
    wsscfgIsSetFsDot11WlanQosProfileEntry.bFsDot11WlanQosRowStatus = OSIX_TRUE;
    wsscfgIsSetFsDot11WlanQosProfileEntry.bIfIndex = OSIX_TRUE;

    if (WsscfgTestAllFsDot11WlanQosProfileTable
        (&u4ErrorCode, &wsscfgFsDot11WlanQosProfileEntry,
         &wsscfgIsSetFsDot11WlanQosProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        return;
    }

    if (WsscfgSetAllFsDot11WlanQosProfileTable
        (&wsscfgFsDot11WlanQosProfileEntry,
         &wsscfgIsSetFsDot11WlanQosProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        return;
    }
    WebnmUnRegisterLock (pHttp);
    WSSCFG_UNLOCK;
    IssProcessWlanQosPageGet (pHttp);
    return;

}

/*********************************************************************
*  Function Name : IssProcessWlanQosPageGet 
*  Description   : This function processes the get request coming for the  
*                  WLAN Qos page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessWlanQosPageGet (tHttp * pHttp)
{
    INT4                i4UpstreamCIR = 0, i4UpstreamEIR = 0, i4UpstreamCBS =
        0, i4UpstreamEBS = 0;
    INT4                i4DownstreamCIR = 0, i4DownstreamEIR =
        0, i4DownstreamCBS = 0, i4DownstreamEBS = 0;
    INT4                i4QosTraffic = 0, i4QosTrustMode = 0, i4QosRateLimit =
        0, i4WlanIfIndex = 0;
    tWsscfgFsDot11WlanQosProfileEntry wsscfgFsDot11WlanQosProfileEntry;

    MEMSET (&wsscfgFsDot11WlanQosProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanQosProfileEntry));

    WebnmRegisterLock (pHttp, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;
    /* Set the Index */
    nmhGetCapwapDot11WlanProfileIfIndex ((UINT4) gi4WlanProfileId,
                                         &i4WlanIfIndex);
    wsscfgFsDot11WlanQosProfileEntry.MibObject.i4IfIndex = i4WlanIfIndex;

    if (WsscfgGetAllFsDot11WlanQosProfileTable
        (&wsscfgFsDot11WlanQosProfileEntry) != OSIX_SUCCESS)
    {

        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        return;
    }

    i4QosTraffic =
        wsscfgFsDot11WlanQosProfileEntry.MibObject.i4FsDot11WlanQosTraffic;
    i4QosTrustMode =
        wsscfgFsDot11WlanQosProfileEntry.MibObject.
        i4FsDot11WlanQosPassengerTrustMode;
    i4QosRateLimit =
        wsscfgFsDot11WlanQosProfileEntry.MibObject.i4FsDot11WlanQosRateLimit;
    i4UpstreamCIR =
        wsscfgFsDot11WlanQosProfileEntry.MibObject.i4FsDot11WlanUpStreamCIR;
    i4UpstreamCBS =
        wsscfgFsDot11WlanQosProfileEntry.MibObject.i4FsDot11WlanUpStreamCBS;
    i4UpstreamEIR =
        wsscfgFsDot11WlanQosProfileEntry.MibObject.i4FsDot11WlanUpStreamEIR;
    i4UpstreamEBS =
        wsscfgFsDot11WlanQosProfileEntry.MibObject.i4FsDot11WlanUpStreamEBS;
    i4DownstreamCIR =
        wsscfgFsDot11WlanQosProfileEntry.MibObject.i4FsDot11WlanDownStreamCIR;
    i4DownstreamCBS =
        wsscfgFsDot11WlanQosProfileEntry.MibObject.i4FsDot11WlanDownStreamCBS;
    i4DownstreamEIR =
        wsscfgFsDot11WlanQosProfileEntry.MibObject.i4FsDot11WlanDownStreamEIR;
    i4DownstreamEBS =
        wsscfgFsDot11WlanQosProfileEntry.MibObject.i4FsDot11WlanDownStreamEBS;

    WSS_WEB_TRC ("Values from Qos Table \n*********************************\n");
    WSS_WEB_TRC ("Qos Traffic: %d\n", i4QosTraffic);
    WSS_WEB_TRC ("Qos TrustMode: %d\n", i4QosTrustMode);
    WSS_WEB_TRC ("Qos RateLimit: %d\n", i4QosRateLimit);
    WSS_WEB_TRC ("i4UpstreamCIR: %d\n", i4UpstreamCIR);
    WSS_WEB_TRC ("i4UpstreamCBS: %d\n", i4UpstreamCBS);
    WSS_WEB_TRC ("i4UpstreamEIR: %d\n", i4UpstreamEIR);
    WSS_WEB_TRC ("i4UpstreamEBS: %d\n", i4UpstreamEBS);
    WSS_WEB_TRC ("i4DownstreamCIR: %d\n", i4DownstreamCIR);
    WSS_WEB_TRC ("i4DownstreamCBS: %d\n", i4DownstreamCBS);
    WSS_WEB_TRC ("i4DownstreamEIR: %d\n", i4DownstreamEIR);
    WSS_WEB_TRC ("i4DownstreamEBS: %d\n", i4DownstreamEBS);

    STRCPY (pHttp->au1KeyString, "QOS_TRAFFIC_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4QosTraffic);

    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "QOS_TRUST_MODE_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4QosTrustMode);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "QOS_RATE_LIMIT_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4QosRateLimit);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    WebnmSendString (pHttp, (UINT1 *) "UpCIR");
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UpstreamCIR);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    WebnmSendString (pHttp, (UINT1 *) "UpCBS");
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UpstreamCBS);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    WebnmSendString (pHttp, (UINT1 *) "UpEIR");
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UpstreamEIR);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    WebnmSendString (pHttp, (UINT1 *) "UpEBS");
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UpstreamEBS);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    WebnmSendString (pHttp, (UINT1 *) "DownCIR");
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4DownstreamCIR);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    WebnmSendString (pHttp, (UINT1 *) "DownCBS");
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4DownstreamCBS);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));
    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    WebnmSendString (pHttp, (UINT1 *) "DownEIR");
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4DownstreamEIR);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    WebnmSendString (pHttp, (UINT1 *) "DownEBS");
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4DownstreamEBS);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    WebnmUnRegisterLock (pHttp);
    WSSCFG_UNLOCK;
}

/*********************************************************************
*  Function Name : IssProcessWlanCapabilityPage
*  Description   : This function processes the set request coming for the  
*                  WLAN Capability page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessWlanCapabilityPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessWlanCapabilityPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessWlanCapabilityPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessWlanCapabilityPageSet 
*  Description   : This function processes the set request coming for the  
*                  WLAN Capability page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessWlanCapabilityPageSet (tHttp * pHttp)
{
    INT4                i4Pollable = 0, i4PollRequest = 0, i4Privacy =
        0, i4Preamble = 0;
    INT4                i4Modulation = 0, i4Agility = 0, i4Qos = 0;
    INT4                i4Spectrum = 0, i4SlotTime = 0, i4AutoPower =
        0, i4Ofdm = 0, i4Delayed = 0, i4ACK = 0;
    INT4                i4QACK = 0, i4QReq = 0, i4TXOP = 0, i4RSNA =
        0, i4PreAuth = 0, i4WlanIfIndex = 0;
    UINT4               u4ErrorCode = 0;

    tWsscfgFsDot11WlanCapabilityProfileEntry
        WsscfgSetFsDot11WlanCapabilityProfileEntry;
    tWsscfgIsSetFsDot11WlanCapabilityProfileEntry
        WsscfgIsSetFsDot11WlanCapabilityProfileEntry;

    WebnmRegisterLock (pHttp, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;

    STRCPY (pHttp->au1Name, "Pollable");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Pollable = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "PollRequest");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PollRequest = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Privacy");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Privacy = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Preamble");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Preamble = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Modulation");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Modulation = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Agility");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Agility = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Qos");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Qos = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Spectrum");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Spectrum = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "SlotTime");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4SlotTime = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "AutoPower");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4AutoPower = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Ofdm");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Ofdm = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Delayed");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Delayed = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ACK");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4ACK = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "QACK");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4QACK = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "QReq");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4QReq = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "TXOP");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4TXOP = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "RSNA");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4RSNA = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "PreAuth");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PreAuth = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    WSS_WEB_TRC ("Values from  Capability Table\n");
    WSS_WEB_TRC ("*************************\n");

    WSS_WEB_TRC (" Pollable : %d\n", i4Pollable);
    WSS_WEB_TRC (" PollRequest : %d\n", i4PollRequest);
    WSS_WEB_TRC (" Privacy : %d\n", i4Privacy);
    WSS_WEB_TRC (" Premable : %d\n", i4Preamble);
    WSS_WEB_TRC (" Modulation  : %d\n", i4Modulation);
    WSS_WEB_TRC (" Agility : %d\n", i4Agility);
    WSS_WEB_TRC (" Spectrum : %d\n", i4Spectrum);
    WSS_WEB_TRC (" SlotTime : %d\n", i4SlotTime);
    WSS_WEB_TRC (" AutoPower : %d\n", i4AutoPower);
    WSS_WEB_TRC (" Ofdm: %d\n", i4Ofdm);
    WSS_WEB_TRC (" Delayed : %d\n", i4Delayed);
    WSS_WEB_TRC (" ACK : %d\n", i4ACK);
    WSS_WEB_TRC (" QACK : %d\n", i4QACK);
    WSS_WEB_TRC (" QReq: %d\n", i4QReq);
    WSS_WEB_TRC (" TXOP : %d\n", i4TXOP);
    WSS_WEB_TRC (" RSNA : %d\n", i4RSNA);
    WSS_WEB_TRC ("  PreAuth: %d\n", i4PreAuth);
    WSS_WEB_TRC (" ProfileId: %d\n", gi4WlanProfileId);

    MEMSET (&WsscfgSetFsDot11WlanCapabilityProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanCapabilityProfileEntry));
    MEMSET (&WsscfgIsSetFsDot11WlanCapabilityProfileEntry, 0,
            sizeof (tWsscfgIsSetFsDot11WlanCapabilityProfileEntry));

    nmhGetCapwapDot11WlanProfileIfIndex ((UINT4) gi4WlanProfileId,
                                         &i4WlanIfIndex);
    /*Set the attributes */
    WsscfgSetFsDot11WlanCapabilityProfileEntry.MibObject.
        i4FsDot11WlanCFPollable = i4Pollable;
    WsscfgSetFsDot11WlanCapabilityProfileEntry.MibObject.
        i4FsDot11WlanCFPollRequest = i4PollRequest;
    WsscfgSetFsDot11WlanCapabilityProfileEntry.MibObject.
        i4FsDot11WlanPrivacyOptionImplemented = i4Privacy;
    WsscfgSetFsDot11WlanCapabilityProfileEntry.MibObject.
        i4FsDot11WlanShortPreambleOptionImplemented = i4Preamble;
    WsscfgSetFsDot11WlanCapabilityProfileEntry.MibObject.
        i4FsDot11WlanPBCCOptionImplemented = i4Modulation;
    WsscfgSetFsDot11WlanCapabilityProfileEntry.MibObject.
        i4FsDot11WlanChannelAgilityPresent = i4Agility;
    WsscfgSetFsDot11WlanCapabilityProfileEntry.MibObject.
        i4FsDot11WlanQosOptionImplemented = i4Qos;
    WsscfgSetFsDot11WlanCapabilityProfileEntry.MibObject.
        i4FsDot11WlanSpectrumManagementRequired = i4Spectrum;
    WsscfgSetFsDot11WlanCapabilityProfileEntry.MibObject.
        i4FsDot11WlanShortSlotTimeOptionImplemented = i4SlotTime;
    WsscfgSetFsDot11WlanCapabilityProfileEntry.MibObject.
        i4FsDot11WlanAPSDOptionImplemented = i4AutoPower;
    WsscfgSetFsDot11WlanCapabilityProfileEntry.MibObject.
        i4FsDot11WlanDSSSOFDMOptionEnabled = i4Ofdm;
    WsscfgSetFsDot11WlanCapabilityProfileEntry.MibObject.
        i4FsDot11WlanDelayedBlockAckOptionImplemented = i4Delayed;
    WsscfgSetFsDot11WlanCapabilityProfileEntry.MibObject.
        i4FsDot11WlanImmediateBlockAckOptionImplemented = i4ACK;
    WsscfgSetFsDot11WlanCapabilityProfileEntry.MibObject.
        i4FsDot11WlanQAckOptionImplemented = i4QACK;
    WsscfgSetFsDot11WlanCapabilityProfileEntry.MibObject.
        i4FsDot11WlanQueueRequestOptionImplemented = i4QReq;
    WsscfgSetFsDot11WlanCapabilityProfileEntry.MibObject.
        i4FsDot11WlanTXOPRequestOptionImplemented = i4TXOP;
    WsscfgSetFsDot11WlanCapabilityProfileEntry.MibObject.
        i4FsDot11WlanRSNAOptionImplemented = i4RSNA;
    WsscfgSetFsDot11WlanCapabilityProfileEntry.MibObject.
        i4FsDot11WlanRSNAPreauthenticationImplemented = i4PreAuth;
    WsscfgSetFsDot11WlanCapabilityProfileEntry.MibObject.
        i4FsDot11WlanCapabilityRowStatus = ACTIVE;
    WsscfgSetFsDot11WlanCapabilityProfileEntry.MibObject.i4IfIndex =
        i4WlanIfIndex;

    /* Set the booleans */
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.bFsDot11WlanCFPollable =
        OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.bFsDot11WlanCFPollRequest =
        OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanPrivacyOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanShortPreambleOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanPBCCOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanChannelAgilityPresent = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanQosOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanSpectrumManagementRequired = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanShortSlotTimeOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanAPSDOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanDSSSOFDMOptionEnabled = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanDelayedBlockAckOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanImmediateBlockAckOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanQAckOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanQueueRequestOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanTXOPRequestOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanRSNAOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanRSNAPreauthenticationImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanCapabilityRowStatus = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.bIfIndex = OSIX_TRUE;

    if (WsscfgTestAllFsDot11WlanCapabilityProfileTable
        (&u4ErrorCode, &WsscfgSetFsDot11WlanCapabilityProfileEntry,
         &WsscfgIsSetFsDot11WlanCapabilityProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        return;
    }

    if (WsscfgSetAllFsDot11WlanCapabilityProfileTable
        (&WsscfgSetFsDot11WlanCapabilityProfileEntry,
         &WsscfgIsSetFsDot11WlanCapabilityProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        return;
    }

    WebnmUnRegisterLock (pHttp);
    WSSCFG_UNLOCK;
    IssProcessWlanCapabilityPageGet (pHttp);
    return;
}

/*********************************************************************
*  Function Name : IssProcessWlanCapabilityPageSet 
*  Description   : This function processes the get request coming for the  
*                  WLAN Capability page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessWlanCapabilityPageGet (tHttp * pHttp)
{
    INT4 
         
         
         
         
         
         
         
        i4Pollable = 0, i4PollRequest = 0, i4Privacy = 0, i4Modulation =
        0, i4Agility = 0, i4Qos = 0;
    INT4                i4Spectrum = 0, i4SlotTime = 0, i4AutoPower =
        0, i4Ofdm = 0, i4Delayed = 0, i4ACK = 0, i4Preamble = 0;
    INT4                i4QACK = 0, i4QReq = 0, i4TXOP = 0, i4RSNA =
        0, i4PreAuth = 0, i4WlanIfIndex = 0;
    INT4                attribute_value[18], no_of_attributes = 0, count = 0;
    tWsscfgFsDot11WlanCapabilityProfileEntry
        wsscfgFsDot11WlanCapabilityProfileEntry;
    UINT1               attribute_enabled[OCTETSTR_SIZE],
        attribute_disabled[OCTETSTR_SIZE];
    UINT1               attribute_name[18][12] = { "Pollable",
        "PollRequest",
        "Privacy",
        "Preamble",
        "Modulation",
        "Agility",
        "Qos",
        "Spectrum",
        "SlotTime",
        "AutoPower",
        "Ofdm",
        "Delayed",
        "ACK",
        "QACK",
        "QReq",
        "TXOP",
        "RSNA",
        "PreAuth"
    };

    no_of_attributes = sizeof (attribute_name) / sizeof (attribute_name[0]);

    MEMSET (attribute_enabled, 0, OCTETSTR_SIZE);
    MEMSET (attribute_disabled, 0, OCTETSTR_SIZE);
    WebnmRegisterLock (pHttp, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;

    /* Get the WLAN IFINDEX */
    nmhGetCapwapDot11WlanProfileIfIndex ((UINT4) gi4WlanProfileId,
                                         &i4WlanIfIndex);
    MEMSET (&wsscfgFsDot11WlanCapabilityProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanCapabilityProfileEntry));
    wsscfgFsDot11WlanCapabilityProfileEntry.MibObject.i4IfIndex = i4WlanIfIndex;

    if (WsscfgGetAllFsDot11WlanCapabilityProfileTable
        (&wsscfgFsDot11WlanCapabilityProfileEntry) == OSIX_SUCCESS)
    {
        i4Pollable =
            wsscfgFsDot11WlanCapabilityProfileEntry.MibObject.
            i4FsDot11WlanCFPollable;
        i4PollRequest =
            wsscfgFsDot11WlanCapabilityProfileEntry.MibObject.
            i4FsDot11WlanCFPollRequest;
        i4Privacy =
            wsscfgFsDot11WlanCapabilityProfileEntry.MibObject.
            i4FsDot11WlanPrivacyOptionImplemented;
        i4Preamble =
            wsscfgFsDot11WlanCapabilityProfileEntry.MibObject.
            i4FsDot11WlanShortPreambleOptionImplemented;
        i4Modulation =
            wsscfgFsDot11WlanCapabilityProfileEntry.MibObject.
            i4FsDot11WlanPBCCOptionImplemented;
        i4Agility =
            wsscfgFsDot11WlanCapabilityProfileEntry.MibObject.
            i4FsDot11WlanChannelAgilityPresent;
        i4Qos =
            wsscfgFsDot11WlanCapabilityProfileEntry.MibObject.
            i4FsDot11WlanQosOptionImplemented;
        i4Spectrum =
            wsscfgFsDot11WlanCapabilityProfileEntry.MibObject.
            i4FsDot11WlanSpectrumManagementRequired;
        i4SlotTime =
            wsscfgFsDot11WlanCapabilityProfileEntry.MibObject.
            i4FsDot11WlanShortSlotTimeOptionImplemented;
        i4AutoPower =
            wsscfgFsDot11WlanCapabilityProfileEntry.MibObject.
            i4FsDot11WlanAPSDOptionImplemented;
        i4Ofdm =
            wsscfgFsDot11WlanCapabilityProfileEntry.MibObject.
            i4FsDot11WlanDSSSOFDMOptionEnabled;
        i4Delayed =
            wsscfgFsDot11WlanCapabilityProfileEntry.MibObject.
            i4FsDot11WlanDelayedBlockAckOptionImplemented;
        i4ACK =
            wsscfgFsDot11WlanCapabilityProfileEntry.MibObject.
            i4FsDot11WlanImmediateBlockAckOptionImplemented;
        i4QACK =
            wsscfgFsDot11WlanCapabilityProfileEntry.MibObject.
            i4FsDot11WlanQAckOptionImplemented;
        i4QReq =
            wsscfgFsDot11WlanCapabilityProfileEntry.MibObject.
            i4FsDot11WlanQueueRequestOptionImplemented;
        i4TXOP =
            wsscfgFsDot11WlanCapabilityProfileEntry.MibObject.
            i4FsDot11WlanTXOPRequestOptionImplemented;
        i4RSNA =
            wsscfgFsDot11WlanCapabilityProfileEntry.MibObject.
            i4FsDot11WlanRSNAOptionImplemented;
        i4PreAuth =
            wsscfgFsDot11WlanCapabilityProfileEntry.MibObject.
            i4FsDot11WlanRSNAPreauthenticationImplemented;
    }

    WSS_WEB_TRC (" Pollable : %d\n", i4Pollable);
    WSS_WEB_TRC (" PollRequest : %d\n", i4PollRequest);
    WSS_WEB_TRC (" Privacy : %d\n", i4Privacy);
    WSS_WEB_TRC (" Premable : %d\n", i4Preamble);
    WSS_WEB_TRC (" Modulation  : %d\n", i4Modulation);
    WSS_WEB_TRC (" Agility : %d\n", i4Agility);
    WSS_WEB_TRC (" i4Qos  : %d\n", i4Qos);
    WSS_WEB_TRC (" Spectrum : %d\n", i4Spectrum);
    WSS_WEB_TRC (" SlotTime : %d\n", i4SlotTime);
    WSS_WEB_TRC (" AutoPower : %d\n", i4AutoPower);
    WSS_WEB_TRC (" Ofdm: %d\n", i4Ofdm);
    WSS_WEB_TRC (" Delayed : %d\n", i4Delayed);
    WSS_WEB_TRC (" ACK : %d\n", i4ACK);
    WSS_WEB_TRC (" QACK : %d\n", i4QACK);
    WSS_WEB_TRC (" QReq: %d\n", i4QReq);
    WSS_WEB_TRC (" TXOP : %d\n", i4TXOP);
    WSS_WEB_TRC (" RSNA : %d\n", i4RSNA);
    WSS_WEB_TRC (" PreAuth : %d\n", i4PreAuth);

    attribute_value[0] = i4Pollable;
    attribute_value[1] = i4PollRequest;
    attribute_value[2] = i4Privacy;
    attribute_value[3] = i4Preamble;
    attribute_value[4] = i4Modulation;
    attribute_value[5] = i4Agility;
    attribute_value[6] = i4Qos;
    attribute_value[7] = i4Spectrum;
    attribute_value[8] = i4SlotTime;
    attribute_value[9] = i4AutoPower;
    attribute_value[10] = i4Ofdm;
    attribute_value[11] = i4Delayed;
    attribute_value[12] = i4ACK;
    attribute_value[13] = i4QACK;
    attribute_value[14] = i4QReq;
    attribute_value[15] = i4TXOP;
    attribute_value[16] = i4RSNA;
    attribute_value[17] = i4PreAuth;

    do
    {
        SPRINTF ((CHR1 *) pHttp->au1KeyString, "%sKey", attribute_name[count]);
        WebnmSendString (pHttp, pHttp->au1KeyString);

        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", attribute_value[count]);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));

        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        count++;
    }
    while (count < no_of_attributes);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    WebnmUnRegisterLock (pHttp);
    WSSCFG_UNLOCK;

}

/*********************************************************************
*  Function Name : IssProcessCapabilityProfilePage 
*  Description   : This function processes the request coming for the 
*                  Capability Profile Table Page 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessCapabilityProfilePage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessCapabilityProfilePageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessCapabilityProfilePageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessCapabilityProfilePageGet 
*  Description   : This function processes the get request coming for the  
*                  Capability Profile Table Entry page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessCapabilityProfilePageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE *pCurrentProfileName = NULL, *pNextProfileName =
        NULL;
    tSNMP_MULTI_DATA_TYPE *pu1DataString = NULL;
    UINT4               u4Temp = 0;
    INT4                i4OutCome = 0;
    UINT1               au1ProfileName[256];
    MEMSET (au1ProfileName, 0, 256);

    if ((pu1DataString = WebnmAllocMultiData ()) == NULL)
    {
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        return;
    }

    pCurrentProfileName =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (pCurrentProfileName == NULL)
    {
        WSS_WEB_TRC
            ("IssProcessCapabilityProfilePageGet failed due to memory allocation\n");
        return;
    }
    pNextProfileName =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (pNextProfileName == NULL)
    {
        WSS_WEB_TRC
            ("IssProcessCapabilityProfilePageGet failed due to memory allocation\n");
        free_octetstring (pCurrentProfileName);
        return;
    }

    MEMSET (pCurrentProfileName->pu1_OctetList, 0, OCTETSTR_SIZE);
    pCurrentProfileName->i4_Length = 0;
    MEMSET (pNextProfileName->pu1_OctetList, 0, OCTETSTR_SIZE);
    pNextProfileName->i4_Length = 0;

    WebnmRegisterLock (pHttp, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;
    i4OutCome =
        nmhGetFirstIndexFsDot11CapabilityProfileTable (pNextProfileName);

    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    else
    {
        do
        {
            pHttp->i4Write = (INT4) u4Temp;

            STRNCPY (pu1DataString->pOctetStrValue->pu1_OctetList,
                     pNextProfileName->pu1_OctetList,
                     pNextProfileName->i4_Length);
            pu1DataString->pOctetStrValue->i4_Length =
                pNextProfileName->i4_Length;

            /*    STRCPY (pHttp->au1KeyString,"CAPABILITY_PROFILE_NAME");
               WebnmSendString (pHttp, pHttp->au1KeyString); */
            WebnmSendToSocket (pHttp, "CAPABILITY_PROFILE_NAME", pu1DataString,
                               ENM_DISPLAYSTRING);

            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

            /*Copy next index to current index */
            STRNCPY (pCurrentProfileName->pu1_OctetList,
                     pNextProfileName->pu1_OctetList,
                     pNextProfileName->i4_Length);
            pCurrentProfileName->i4_Length = pNextProfileName->i4_Length;

        }
        while (nmhGetNextIndexFsDot11CapabilityProfileTable
               (pCurrentProfileName, pNextProfileName) == SNMP_SUCCESS);

    }

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    WebnmUnRegisterLock (pHttp);
    WSSCFG_UNLOCK;
    free_octetstring (pCurrentProfileName);
    free_octetstring (pNextProfileName);
}

/*********************************************************************
*  Function Name : IssProcessCapabilityProfilePageSet 
*  Description   : This function processes the set request coming for the  
*                  Capabilty Profile Table Entry page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessCapabilityProfilePageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE *CapabilityName = NULL;
    UINT1               au1ProfileName[VLAN_STATIC_MAX_NAME_LEN + 2];

    MEMSET (&au1ProfileName, 0, (VLAN_STATIC_MAX_NAME_LEN + 2));
    CapabilityName =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (CapabilityName == NULL)
    {
        WSS_WEB_TRC
            ("IssProcessCapabilityProfilePageSet failed due to memory allocation\n");
        return;
    }
    MEMSET (CapabilityName->pu1_OctetList, 0, OCTETSTR_SIZE);

    WebnmRegisterLock (pHttp, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;

    STRCPY (pHttp->au1Name, "PROFILE_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    MEMCPY (au1ProfileName, pHttp->au1Value, sizeof (au1ProfileName));

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {
        WSS_WEB_TRC ("Deleting Profile Entry\n");

        CapabilityName->i4_Length = (INT4) (STRLEN (au1ProfileName));
        MEMCPY (CapabilityName->pu1_OctetList, au1ProfileName,
                CapabilityName->i4_Length);

        if (STRCMP (au1ProfileName, "default") == 0)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Default Profile Cannot Be Deleted\n");
            WebnmUnRegisterLock (pHttp);
            WSSCFG_UNLOCK;
            free_octetstring (CapabilityName);
            return;
        }

        if (nmhSetFsDot11CapabilityRowStatus (CapabilityName, DESTROY) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
            WebnmUnRegisterLock (pHttp);
            WSSCFG_UNLOCK;
            free_octetstring (CapabilityName);
            return;
        }

    }
    else if (STRCMP (pHttp->au1Value, "Edit") == 0)
    {
        MEMCPY (gCapProfileName, au1ProfileName, (sizeof (au1ProfileName)));

    }
    else
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Operation\n");
    }
    WebnmUnRegisterLock (pHttp);
    WSSCFG_UNLOCK;
    free_octetstring (CapabilityName);
    IssProcessCapabilityProfilePageGet (pHttp);
    return;
}

/*********************************************************************
*  Function Name : IssProcessCapabilityProfileCreatePage
*  Description   : This function processes the  request coming for the  
*                  Capabiity Profile  Entry page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessCapabilityProfileCreatePage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessCapabilityProfileCreatePageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessCapabilityProfileCreatePageSet (pHttp);
    }
}

/*********************************************************************
 *  Function Name : IssProcessCapabilityProfileCreatePageGet
 *  Description   : This function processes the get request coming for the  
 *                  Capabiity Profile Entry page. 
 *                     
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessCapabilityProfileCreatePageGet (tHttp * pHttp)
{
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

}

/*********************************************************************
*  Function Name : IssProcessCapabilityProfileCreatePageSet
*  Description   : This function processes the set request coming for the  
*                  Capabiity Profile Entry page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessCapabilityProfileCreatePageSet (tHttp * pHttp)
{

    UINT1               au1CapabilityProfile[VLAN_STATIC_MAX_NAME_LEN];
    INT4                i4Pollable = 0, i4PollRequest = 0, i4Privacy =
        0, i4Preamble = 0, i4Modulation = 0, i4Agility = 0, i4Qos = 0;
    INT4                i4Spectrum = 0, i4SlotTime = 0, i4AutoPower =
        0, i4Ofdm = 0, i4Delayed = 0, i4ACK = 0;
    INT4                i4QACK = 0, i4QReq = 0, i4TXOP = 0, i4RSNA =
        0, i4PreAuth = 0;
    UINT4               u4ErrorCode = 0;

    tWsscfgFsDot11CapabilityProfileEntry WsscfgSetFsDot11CapabilityProfileEntry;
    tWsscfgIsSetFsDot11CapabilityProfileEntry
        WsscfgIsSetFsDot11CapabilityProfileEntry;

    MEMSET (&WsscfgSetFsDot11CapabilityProfileEntry, 0,
            sizeof (tWsscfgFsDot11CapabilityProfileEntry));
    MEMSET (&WsscfgIsSetFsDot11CapabilityProfileEntry, 0,
            sizeof (tWsscfgIsSetFsDot11CapabilityProfileEntry));

    MEMSET (au1CapabilityProfile, '\0', VLAN_STATIC_MAX_NAME_LEN);

    WebnmRegisterLock (pHttp, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;

    STRCPY (pHttp->au1Name, "CapabilityName");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    MEMCPY (au1CapabilityProfile, pHttp->au1Value,
            sizeof (au1CapabilityProfile));

    au1CapabilityProfile[VLAN_STATIC_MAX_NAME_LEN - 1] = '\0';

    STRCPY (pHttp->au1Name, "Pollable");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Pollable = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "PollRequest");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PollRequest = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Privacy");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Privacy = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Preamble");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Preamble = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Modulation");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Modulation = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Agility");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Agility = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Qos");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Qos = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Spectrum");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Spectrum = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "SlotTime");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4SlotTime = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "AutoPower");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4AutoPower = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Ofdm");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Ofdm = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Delayed");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Delayed = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ACK");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4ACK = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "QACK");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4QACK = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "QReq");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4QReq = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "TXOP");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4TXOP = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "RSNA");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4RSNA = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "PreAuth");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PreAuth = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    MEMSET (&WsscfgSetFsDot11CapabilityProfileEntry, 0,
            sizeof (tWsscfgFsDot11CapabilityProfileEntry));
    MEMSET (&WsscfgIsSetFsDot11CapabilityProfileEntry, 0,
            sizeof (tWsscfgIsSetFsDot11CapabilityProfileEntry));

    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.i4FsDot11CFPollable =
        i4Pollable;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.i4FsDot11CFPollRequest =
        i4PollRequest;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11PrivacyOptionImplemented = i4Privacy;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11ShortPreambleOptionImplemented = i4Preamble;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11PBCCOptionImplemented = i4Modulation;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11ChannelAgilityPresent = i4Agility;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11QosOptionImplemented = i4Qos;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11SpectrumManagementRequired = i4Spectrum;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11ShortSlotTimeOptionImplemented = i4SlotTime;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11APSDOptionImplemented = i4AutoPower;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11DSSSOFDMOptionEnabled = i4Ofdm;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11DelayedBlockAckOptionImplemented = i4Delayed;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11ImmediateBlockAckOptionImplemented = i4ACK;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11QAckOptionImplemented = i4QACK;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11QueueRequestOptionImplemented = i4QReq;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11TXOPRequestOptionImplemented = i4TXOP;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11RSNAOptionImplemented = i4RSNA;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11RSNAPreauthenticationImplemented = i4PreAuth;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11CapabilityRowStatus = CREATE_AND_GO;

    STRNCPY (WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
             au1FsDot11CapabilityProfileName, au1CapabilityProfile,
             (STRLEN (au1CapabilityProfile)));
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11CapabilityProfileNameLen =
        (INT4) (STRLEN (au1CapabilityProfile));

    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11CapabilityProfileName =
        OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11CFPollable = OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11CFPollRequest = OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11PrivacyOptionImplemented =
        OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.
        bFsDot11ShortPreambleOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11PBCCOptionImplemented =
        OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11ChannelAgilityPresent =
        OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11QosOptionImplemented =
        OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.
        bFsDot11SpectrumManagementRequired = OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.
        bFsDot11ShortSlotTimeOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11APSDOptionImplemented =
        OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11DSSSOFDMOptionEnabled =
        OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.
        bFsDot11DelayedBlockAckOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.
        bFsDot11ImmediateBlockAckOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11QAckOptionImplemented =
        OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.
        bFsDot11QueueRequestOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.
        bFsDot11TXOPRequestOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11RSNAOptionImplemented =
        OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.
        bFsDot11RSNAPreauthenticationImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11CapabilityRowStatus =
        OSIX_TRUE;

    if (WsscfgTestAllFsDot11CapabilityProfileTable
        (&u4ErrorCode, &WsscfgSetFsDot11CapabilityProfileEntry,
         &WsscfgIsSetFsDot11CapabilityProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {

        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        return;
    }

    if (WsscfgSetAllFsDot11CapabilityProfileTable
        (&WsscfgSetFsDot11CapabilityProfileEntry,
         &WsscfgIsSetFsDot11CapabilityProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        return;
    }
    WebnmUnRegisterLock (pHttp);
    WSSCFG_UNLOCK;
    return;
}

/*********************************************************************
*  Function Name : IssProcessCapabilityProfileEditPage
*  Description   : This function processes the set request coming for the  
*                  Capabiity Profile  Entry Edit page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessCapabilityProfileEditPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessCapabilityProfileEditPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessCapabilityProfileEditPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessCapabilityProfileEditPageGet 
*  Description   : This function processes the get request coming for the  
*                  Capabiity Profile Entry Edit page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessCapabilityProfileEditPageGet (tHttp * pHttp)
{
    INT4                i4Pollable = 0, i4PollRequest = 0, i4Privacy =
        0, i4Modulation = 0, i4Agility = 0, i4Qos = 0;
    INT4                i4Spectrum = 0, i4SlotTime = 0, i4AutoPower =
        0, i4Ofdm = 0, i4Delayed = 0, i4ACK = 0, i4Preamble = 0;
    INT4                i4QACK = 0, i4QReq = 0, i4TXOP = 0, i4RSNA =
        0, i4PreAuth = 0;
    INT4                attribute_value[18], no_of_attributes = 0, count = 0;
    tSNMP_OCTET_STRING_TYPE *CapabilityProfileName = NULL;
    UINT1               attribute_enabled[OCTETSTR_SIZE],
        attribute_disabled[OCTETSTR_SIZE];
    tWsscfgFsDot11CapabilityProfileEntry wsscfgFsDot11CapabilityProfileEntry;
    UINT1               attribute_name[18][12] = { "Pollable",
        "PollRequest",
        "Privacy",
        "Preamble",
        "Modulation",
        "Agility",
        "Qos",
        "Spectrum",
        "SlotTime",
        "AutoPower",
        "Ofdm",
        "Delayed",
        "ACK",
        "QACK",
        "QReq",
        "TXOP",
        "RSNA",
        "PreAuth"
    };

    MEMSET (&wsscfgFsDot11CapabilityProfileEntry, 0,
            sizeof (tWsscfgFsDot11CapabilityProfileEntry));
    no_of_attributes = sizeof (attribute_name) / sizeof (attribute_name[0]);

    CapabilityProfileName =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (CapabilityProfileName == NULL)
    {
        WSS_WEB_TRC
            ("IssProcessCapabilityProfileEditPageGet failed due to memory allocation\n");
        return;
    }
    MEMSET (CapabilityProfileName->pu1_OctetList, 0, OCTETSTR_SIZE);
    MEMSET (attribute_enabled, 0, OCTETSTR_SIZE);
    MEMSET (attribute_disabled, 0, OCTETSTR_SIZE);

    CapabilityProfileName->i4_Length = (INT4) (STRLEN (gCapProfileName));
    MEMCPY (CapabilityProfileName->pu1_OctetList, gCapProfileName,
            MEM_MAX_BYTES (OCTETSTR_SIZE, STRLEN (gCapProfileName)));

    wsscfgFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11CapabilityProfileNameLen = CapabilityProfileName->i4_Length;
    MEMCPY (wsscfgFsDot11CapabilityProfileEntry.MibObject.
            au1FsDot11CapabilityProfileName,
            CapabilityProfileName->pu1_OctetList,
            MEM_MAX_BYTES (sizeof
                           (wsscfgFsDot11CapabilityProfileEntry.MibObject.
                            i4FsDot11CapabilityProfileNameLen),
                           (UINT4) CapabilityProfileName->i4_Length));

    if (WsscfgGetAllFsDot11CapabilityProfileTable
        (&wsscfgFsDot11CapabilityProfileEntry) == OSIX_SUCCESS)
    {
        i4Pollable =
            wsscfgFsDot11CapabilityProfileEntry.MibObject.i4FsDot11CFPollable;
        i4PollRequest =
            wsscfgFsDot11CapabilityProfileEntry.MibObject.
            i4FsDot11CFPollRequest;
        i4Privacy =
            wsscfgFsDot11CapabilityProfileEntry.MibObject.
            i4FsDot11PrivacyOptionImplemented;
        i4Preamble =
            wsscfgFsDot11CapabilityProfileEntry.MibObject.
            i4FsDot11ShortPreambleOptionImplemented;
        i4Modulation =
            wsscfgFsDot11CapabilityProfileEntry.MibObject.
            i4FsDot11PBCCOptionImplemented;
        i4Agility =
            wsscfgFsDot11CapabilityProfileEntry.MibObject.
            i4FsDot11ChannelAgilityPresent;
        i4Qos =
            wsscfgFsDot11CapabilityProfileEntry.MibObject.
            i4FsDot11QosOptionImplemented;
        i4Spectrum =
            wsscfgFsDot11CapabilityProfileEntry.MibObject.
            i4FsDot11SpectrumManagementRequired;
        i4SlotTime =
            wsscfgFsDot11CapabilityProfileEntry.MibObject.
            i4FsDot11ShortSlotTimeOptionImplemented;
        i4AutoPower =
            wsscfgFsDot11CapabilityProfileEntry.MibObject.
            i4FsDot11APSDOptionImplemented;
        i4Ofdm =
            wsscfgFsDot11CapabilityProfileEntry.MibObject.
            i4FsDot11DSSSOFDMOptionEnabled;
        i4Delayed =
            wsscfgFsDot11CapabilityProfileEntry.MibObject.
            i4FsDot11DelayedBlockAckOptionImplemented;
        i4ACK =
            wsscfgFsDot11CapabilityProfileEntry.MibObject.
            i4FsDot11ImmediateBlockAckOptionImplemented;
        i4QACK =
            wsscfgFsDot11CapabilityProfileEntry.MibObject.
            i4FsDot11QAckOptionImplemented;
        i4QReq =
            wsscfgFsDot11CapabilityProfileEntry.MibObject.
            i4FsDot11QueueRequestOptionImplemented;
        i4TXOP =
            wsscfgFsDot11CapabilityProfileEntry.MibObject.
            i4FsDot11TXOPRequestOptionImplemented;
        i4RSNA =
            wsscfgFsDot11CapabilityProfileEntry.MibObject.
            i4FsDot11RSNAOptionImplemented;
        i4PreAuth =
            wsscfgFsDot11CapabilityProfileEntry.MibObject.
            i4FsDot11RSNAPreauthenticationImplemented;
    }

    WSS_WEB_TRC (" Pollable : %d\n", i4Pollable);
    WSS_WEB_TRC (" PollRequest : %d\n", i4PollRequest);
    WSS_WEB_TRC (" Privacy : %d\n", i4Privacy);
    WSS_WEB_TRC (" Premable : %d\n", i4Preamble);
    WSS_WEB_TRC (" Modulation  : %d\n", i4Modulation);
    WSS_WEB_TRC (" Agility : %d\n", i4Agility);
    WSS_WEB_TRC (" i4Qos  : %d\n", i4Qos);
    WSS_WEB_TRC (" Spectrum : %d\n", i4Spectrum);
    WSS_WEB_TRC (" SlotTime : %d\n", i4SlotTime);
    WSS_WEB_TRC (" AutoPower : %d\n", i4AutoPower);
    WSS_WEB_TRC (" Ofdm: %d\n", i4Ofdm);
    WSS_WEB_TRC (" Delayed : %d\n", i4Delayed);
    WSS_WEB_TRC (" ACK : %d\n", i4ACK);
    WSS_WEB_TRC (" QACK : %d\n", i4QACK);
    WSS_WEB_TRC (" QReq: %d\n", i4QReq);
    WSS_WEB_TRC (" TXOP : %d\n", i4TXOP);
    WSS_WEB_TRC (" RSNA : %d\n", i4RSNA);
    WSS_WEB_TRC (" PreAuth: %d\n", i4PreAuth);

    attribute_value[0] = i4Pollable;
    attribute_value[1] = i4PollRequest;
    attribute_value[2] = i4Privacy;
    attribute_value[3] = i4Preamble;
    attribute_value[4] = i4Modulation;
    attribute_value[5] = i4Agility;
    attribute_value[6] = i4Qos;
    attribute_value[7] = i4Spectrum;
    attribute_value[8] = i4SlotTime;
    attribute_value[9] = i4AutoPower;
    attribute_value[10] = i4Ofdm;
    attribute_value[11] = i4Delayed;
    attribute_value[12] = i4ACK;
    attribute_value[13] = i4QACK;
    attribute_value[14] = i4QReq;
    attribute_value[15] = i4TXOP;
    attribute_value[16] = i4RSNA;
    attribute_value[17] = i4PreAuth;

    WebnmRegisterLock (pHttp, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;
    STRCPY (pHttp->au1KeyString, "CAPABILITY_PROFILE_NAME");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    STRNCPY (pHttp->au1DataString, CapabilityProfileName->pu1_OctetList,
             CapabilityProfileName->i4_Length);
    pHttp->au1DataString[CapabilityProfileName->i4_Length] = '\0';
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (STRLEN (pHttp->au1DataString)));
    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    do
    {
        SPRINTF ((CHR1 *) pHttp->au1KeyString, "%sKey", attribute_name[count]);
        WebnmSendString (pHttp, pHttp->au1KeyString);

        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", attribute_value[count]);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        count++;
    }
    while (count < no_of_attributes);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    WebnmUnRegisterLock (pHttp);
    WSSCFG_UNLOCK;

    free_octetstring (CapabilityProfileName);

}

/*********************************************************************
*  Function Name : IssProcessCapabilityProfileEditPageSet 
*  Description   : This function processes the set request coming for the  
*                  Capabiity Profile Entry Edit page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessCapabilityProfileEditPageSet (tHttp * pHttp)
{

    UINT1               au1CapabilityProfile[VLAN_STATIC_MAX_NAME_LEN + 1];
    INT4                i4Pollable = 0, i4PollRequest = 0, i4Privacy =
        0, i4Preamble = 0, i4Modulation = 0, i4Agility = 0, i4Qos = 0;
    INT4                i4Spectrum = 0, i4SlotTime = 0, i4AutoPower =
        0, i4Ofdm = 0, i4Delayed = 0, i4ACK = 0;
    INT4                i4QACK = 0, i4QReq = 0, i4TXOP = 0, i4RSNA =
        0, i4PreAuth = 0;
    UINT4               u4ErrorCode = 0;

    tWsscfgFsDot11CapabilityProfileEntry WsscfgSetFsDot11CapabilityProfileEntry;
    tWsscfgIsSetFsDot11CapabilityProfileEntry
        WsscfgIsSetFsDot11CapabilityProfileEntry;

    MEMSET (&WsscfgSetFsDot11CapabilityProfileEntry, 0,
            sizeof (tWsscfgFsDot11CapabilityProfileEntry));
    MEMSET (&WsscfgIsSetFsDot11CapabilityProfileEntry, 0,
            sizeof (tWsscfgIsSetFsDot11CapabilityProfileEntry));

    MEMSET (au1CapabilityProfile, 0, VLAN_STATIC_MAX_NAME_LEN + 1);

    WebnmRegisterLock (pHttp, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;

    STRCPY (pHttp->au1Name, "ProfileName");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1CapabilityProfile, pHttp->au1Value,
             (sizeof (au1CapabilityProfile) - 1));

    STRCPY (pHttp->au1Name, "Pollable");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Pollable = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "PollRequest");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PollRequest = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Privacy");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Privacy = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Preamble");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Preamble = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Modulation");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Modulation = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Agility");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Agility = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Qos");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Qos = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Spectrum");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Spectrum = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "SlotTime");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4SlotTime = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "AutoPower");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4AutoPower = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Ofdm");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Ofdm = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Delayed");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Delayed = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ACK");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4ACK = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "QACK");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4QACK = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "QReq");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4QReq = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "TXOP");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4TXOP = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "RSNA");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4RSNA = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "PreAuth");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PreAuth = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    MEMSET (&WsscfgSetFsDot11CapabilityProfileEntry, 0,
            sizeof (tWsscfgFsDot11CapabilityProfileEntry));
    MEMSET (&WsscfgIsSetFsDot11CapabilityProfileEntry, 0,
            sizeof (tWsscfgIsSetFsDot11CapabilityProfileEntry));

    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.i4FsDot11CFPollable =
        i4Pollable;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.i4FsDot11CFPollRequest =
        i4PollRequest;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11PrivacyOptionImplemented = i4Privacy;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11ShortPreambleOptionImplemented = i4Preamble;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11PBCCOptionImplemented = i4Modulation;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11ChannelAgilityPresent = i4Agility;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11QosOptionImplemented = i4Qos;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11SpectrumManagementRequired = i4Spectrum;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11ShortSlotTimeOptionImplemented = i4SlotTime;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11APSDOptionImplemented = i4AutoPower;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11DSSSOFDMOptionEnabled = i4Ofdm;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11DelayedBlockAckOptionImplemented = i4Delayed;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11ImmediateBlockAckOptionImplemented = i4ACK;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11QAckOptionImplemented = i4QACK;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11QueueRequestOptionImplemented = i4QReq;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11TXOPRequestOptionImplemented = i4TXOP;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11RSNAOptionImplemented = i4RSNA;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11RSNAPreauthenticationImplemented = i4PreAuth;
    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11CapabilityRowStatus = ACTIVE;

    STRNCPY (WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
             au1FsDot11CapabilityProfileName, au1CapabilityProfile,
             (sizeof
              (WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
               au1FsDot11CapabilityProfileName)));

    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11CapabilityProfileNameLen =
        (INT4) (STRLEN (au1CapabilityProfile));

    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11CapabilityProfileName =
        OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11CFPollable = OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11CFPollRequest = OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11PrivacyOptionImplemented =
        OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.
        bFsDot11ShortPreambleOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11PBCCOptionImplemented =
        OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11ChannelAgilityPresent =
        OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11QosOptionImplemented =
        OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.
        bFsDot11SpectrumManagementRequired = OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.
        bFsDot11ShortSlotTimeOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11APSDOptionImplemented =
        OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11DSSSOFDMOptionEnabled =
        OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.
        bFsDot11DelayedBlockAckOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.
        bFsDot11ImmediateBlockAckOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11QAckOptionImplemented =
        OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.
        bFsDot11QueueRequestOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.
        bFsDot11TXOPRequestOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11RSNAOptionImplemented =
        OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.
        bFsDot11RSNAPreauthenticationImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11CapabilityRowStatus =
        OSIX_TRUE;

    if (WsscfgTestAllFsDot11CapabilityProfileTable
        (&u4ErrorCode, &WsscfgSetFsDot11CapabilityProfileEntry,
         &WsscfgIsSetFsDot11CapabilityProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {

        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        return;
    }

    if (WsscfgSetAllFsDot11CapabilityProfileTable
        (&WsscfgSetFsDot11CapabilityProfileEntry,
         &WsscfgIsSetFsDot11CapabilityProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        return;
    }
    WebnmUnRegisterLock (pHttp);
    WSSCFG_UNLOCK;
    IssProcessCapabilityProfileEditPageGet (pHttp);
    return;
}

/*********************************************************************
*  Function Name : IssProcessRsnaConfigEditPage
*  Description   : This function processes the request coming for the
*                  Rsna Config Table Edit Page
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessRsnaConfigEditPage (tHttp * pHttp)
{
#ifdef RSNA_WANTED
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessRsnaConfigEditPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessRsnaConfigEditPageSet (pHttp);
    }
#else
    UNUSED_PARAM (pHttp);
#endif
}

#ifdef RSNA_WANTED
/*********************************************************************
*  Function Name : IssProcessRsnaConfigEditPageGet
*  Description   : This function processes the get request coming for the
*                  Rsna Config Table Edit page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessRsnaConfigEditPageGet (tHttp * pHttp)
{
    UINT4               u4ProfileId = 0;
    tSNMP_OCTET_STRING_TYPE GroupCipher;
    UINT1               au1GroupCipher[RSNA_CIPHER_SUITE_LEN];
    UINT4               u4GroupRekeyMethod = 0;
    UINT4               u4GroupRekeyTime = 0;
    UINT4               u4GroupRekeyPkts = 0;
    UINT4               u4GroupRekeyStrict = 0;
    UINT4               u4GroupUpdateCount = 0;
    UINT4               u4PairwiseUpdateCount = 0;
    UINT4               u4PMKLifetime = 0;
    UINT4               u4PMKReAuthThreshold = 0;
    UINT4               u4SATimeOut = 0;
    UINT1               u1Cipher = 0;
    INT4                i4RsnaEnabled = 0;
    INT4                i4PreauthenticationEnabled = 0;
    INT4                OKCEnabled = 0;

    MEMSET (au1GroupCipher, '\0', sizeof (RSNA_CIPHER_SUITE_LEN));
    GroupCipher.pu1_OctetList = au1GroupCipher;

    WebnmRegisterLock (pHttp, PnacLock, PnacUnLock);
    PNAC_LOCK ();
    if (nmhGetCapwapDot11WlanProfileIfIndex
        ((UINT4) gi4WlanProfileId, (INT4 *) &u4ProfileId) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));

    }
    else
    {
        if (u4ProfileId != 0)
        {

            nmhGetDot11RSNAActivated ((INT4) u4ProfileId, &i4RsnaEnabled);

            nmhGetDot11RSNAPreauthenticationActivated ((INT4) u4ProfileId,
                                                       &i4PreauthenticationEnabled);
            nmhGetFsRSNAOKCEnabled ((INT4) u4ProfileId, &OKCEnabled);

            RsnaGetDot11RSNAConfigGroupCipher ((INT4) u4ProfileId,
                                               &GroupCipher);

            RsnaGetDot11RSNAConfigGroupRekeyMethod ((INT4) u4ProfileId,
                                                    (INT4 *)
                                                    &u4GroupRekeyMethod);

            RsnaGetDot11RSNAConfigGroupRekeyTime ((INT4) u4ProfileId,
                                                  &u4GroupRekeyTime);

            RsnaGetDot11RSNAConfigGroupRekeyPackets ((INT4) u4ProfileId,
                                                     &u4GroupRekeyPkts);

            RsnaGetDot11RSNAConfigGroupRekeyStrict ((INT4) u4ProfileId,
                                                    (INT4 *)
                                                    &u4GroupRekeyStrict);
            RsnaGetDot11RSNAConfigGroupUpdateCount ((INT4) u4ProfileId,
                                                    &u4GroupUpdateCount);

            RsnaGetDot11RSNAConfigPairwiseUpdateCount ((INT4) u4ProfileId,
                                                       &u4PairwiseUpdateCount);

            RsnaGetDot11RSNAConfigPMKLifetime ((INT4) u4ProfileId,
                                               &u4PMKLifetime);

            RsnaGetDot11RSNAConfigPMKReauthThreshold ((INT4) u4ProfileId,
                                                      &u4PMKReAuthThreshold);

            RsnaGetDot11RSNAConfigSATimeout ((INT4) u4ProfileId, &u4SATimeOut);

            STRCPY (pHttp->au1KeyString, "SSID_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", gi4WlanProfileId);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "rsna_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RsnaEnabled);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "preauthentication_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                     i4PreauthenticationEnabled);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "okc_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", OKCEnabled);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "groupcipher_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            if (MEMCMP
                (GroupCipher.pu1_OctetList, gau1RsnaCipherSuiteTKIP,
                 RSNA_CIPHER_SUITE_LEN) == 0)
            {
                u1Cipher = RSNA_TKIP;

            }
            else if (MEMCMP
                     (GroupCipher.pu1_OctetList, gau1RsnaCipherSuiteCCMP,
                      RSNA_CIPHER_SUITE_LEN) == 0)
            {
                u1Cipher = RSNA_CCMP;

            }
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u1Cipher);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "grouprekey_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4GroupRekeyMethod);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "grouprekeytime_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4GroupRekeyTime);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "grouprekeypackets_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4GroupRekeyPkts);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "grouprekeystrict_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4GroupRekeyStrict);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "psk_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "");
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "groupupdatecount_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4GroupUpdateCount);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "pairwiseupdatecount_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                     u4PairwiseUpdateCount);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "pmklifetime_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4PMKLifetime);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "pmkreauththreshold_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4PMKReAuthThreshold);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "satimeout_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4SATimeOut);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            PNAC_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));

        }
        else
        {
            PNAC_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
        }
    }

}

/*********************************************************************
*  Function Name : IssProcessRsnaConfigEditPageSet
*  Description   : This function processes the set request coming for the
*                  Rsna Config Table Edit page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessRsnaConfigEditPageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE tCipherType;
    tSNMP_OCTET_STRING_TYPE PSK;
    UINT4               u4WlanId = 0;
    UINT4               u4TimeValue = 0;
    INT1                i1RsnaStatus = 0;
#ifdef RSNA_PRE_AUTH
    INT1                i1Status = 0;
#endif
    INT1                okcStatus = 0;
    UINT1               u1CipherType = 0;
    INT1                i1GroupRekeyMethod = 0;
    INT4                i4GroupRekeyInterval = 0;
    INT4                i4NumberOfPackets = 0;
    UINT1               u1StrictStatus = 0;
    UINT1               u1PskType = 0;
    UINT4               u4RetryCount = 0;
    UINT4               u4PairwiseRetryCount = 0;
    UINT4               u4LifeTime = 0;
    UINT4               u4ThresholdValue = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4ProfileId = 0;
    UINT1               u1Index = 0;
    UINT4               u4KeyLength = 0;
    UINT1               au1TempBuf[RSNA_PSK_TEMP_SIZE] = { 0 };
    UINT1               au1Output[RSNA_PSK_LEN];

    UINT1               au1Buf[RSNA_CIPHER_SUITE_LEN];
    UINT1               au1psk[RSNA_PASS_PHRASE_LEN];
    MEMSET (au1Buf, 0, sizeof (au1Buf));
    MEMSET (au1psk, 0, sizeof (au1psk));
    MEMSET (au1Output, 0, sizeof (au1Output));
    PSK.pu1_OctetList = au1psk;
    tCipherType.pu1_OctetList = au1Buf;

    STRCPY (pHttp->au1Name, "SSID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4WlanId = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "rsna");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i1RsnaStatus = (INT1) ATOI ((INT1 *) pHttp->au1Value);
#ifdef RSNA_PRE_AUTH
    STRCPY (pHttp->au1Name, "preauthentication");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i1Status = (INT1) ATOI ((INT1 *) pHttp->au1Value);
#endif

    STRCPY (pHttp->au1Name, "okc");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    okcStatus = (INT1) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "GroupCipher");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u1CipherType = (UINT1) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "GroupRekey");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i1GroupRekeyMethod = (INT1) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "GroupRekeyTime");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4GroupRekeyInterval = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "GroupRekeyPackets");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4NumberOfPackets = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "GroupRekeyStrict");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u1StrictStatus = (UINT1) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "PSKType");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u1PskType = (UINT1) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "PSK");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1psk, pHttp->au1Value, (sizeof (au1psk) - 1));
    PSK.i4_Length = (INT4) STRLEN (au1psk);
    u4KeyLength = (UINT4) STRLEN (au1psk);

    STRCPY (pHttp->au1Name, "GroupUpdateCount");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4RetryCount = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "PairwiseUpdateCount");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4PairwiseRetryCount = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "PMKLifetime");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4LifeTime = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "PMKReauthThreshold");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4ThresholdValue = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "SATimeout");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4TimeValue = (UINT4) ATOI ((INT1 *) pHttp->au1Value);
    WebnmRegisterLock (pHttp, PnacLock, PnacUnLock);
    PNAC_LOCK ();

    if (nmhGetCapwapDot11WlanProfileIfIndex
        (u4WlanId, (INT4 *) &u4ProfileId) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in getting WlanIfIndex\n");
        return;
    }
/*Pre Authentication feature is disabled as of now and
 *  hence the following are defined with this compilation switch.
 *  It will be removed when the actual feature is implemented */
#ifdef RSNA_PRE_AUTH

    if (nmhTestv2Dot11RSNAPreauthenticationEnabled (&u4ErrorCode,
                                                    (INT4) u4ProfileId,
                                                    i1Status) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Invalid Configuration - Preauthentication\n");
        return;
    }
    if (nmhSetDot11RSNAPreauthenticationEnabled ((INT4) u4ProfileId,
                                                 i1Status) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Error in setting Preauthentication\n");
        return;
    }
#endif

    if (nmhTestv2FsRSNAOKCEnabled (&u4ErrorCode,
                                   (INT4) u4ProfileId,
                                   okcStatus) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration - OKC\n");
        return;
    }
    if (nmhSetFsRSNAOKCEnabled ((INT4) u4ProfileId, okcStatus) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting OKC\n");
        return;
    }

    if (nmhTestv2Dot11RSNAActivated (&u4ErrorCode, (INT4) u4ProfileId,
                                     i1RsnaStatus) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration - RSNA\n");
        return;
    }
    if (nmhSetDot11RSNAActivated ((INT4) u4ProfileId,
                                  i1RsnaStatus) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting  RSNA\n");
        return;
    }
    if (u1CipherType == RSNA_TKIP)
    {
        MEMCPY (tCipherType.pu1_OctetList, gau1RsnaCipherSuiteTKIP,
                RSNA_CIPHER_SUITE_LEN);
        tCipherType.i4_Length = RSNA_CIPHER_SUITE_LEN;
    }

    else if (u1CipherType == RSNA_CCMP)
    {
        MEMCPY (tCipherType.pu1_OctetList, gau1RsnaCipherSuiteCCMP,
                RSNA_CIPHER_SUITE_LEN);
        tCipherType.i4_Length = RSNA_CIPHER_SUITE_LEN;
    }
    if (RsnaTestv2Dot11RSNAConfigGroupCipher (&u4ErrorCode, (INT4) u4ProfileId,
                                              &tCipherType) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Invalid Configuration - Group Cipher\n");
        return;
    }
    if (RsnaSetDot11RSNAConfigGroupCipher ((INT4) u4ProfileId, &tCipherType)
        != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting Group Cipher\n");
        return;
    }
    if (RsnaTestv2Dot11RSNAConfigGroupRekeyMethod (&u4ErrorCode,
                                                   (INT4) u4ProfileId,
                                                   i1GroupRekeyMethod) !=
        SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Invalid Configuration - Group Rekey\n");
        return;
    }

    if (RsnaSetDot11RSNAConfigGroupRekeyMethod ((INT4) u4ProfileId,
                                                i1GroupRekeyMethod) !=
        SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting Group Rekey\n");

        return;
    }

    if (RsnaTestv2Dot11RSNAConfigGroupRekeyTime (&u4ErrorCode,
                                                 (INT4) u4ProfileId,
                                                 (UINT4) i4GroupRekeyInterval)
        != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Invalid Configuration - Group Rekey Time\n");

        return;
    }
    if (RsnaSetDot11RSNAConfigGroupRekeyTime ((INT4) u4ProfileId,
                                              (UINT4) i4GroupRekeyInterval) !=
        SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Error in setting Group Rekey Time\n");

        return;
    }
    if (RsnaTestv2Dot11RSNAConfigGroupRekeyPackets (&u4ErrorCode,
                                                    (INT4) u4ProfileId,
                                                    (UINT4) i4NumberOfPackets)
        != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Invalid Configuration - Group Rekey Packets\n");

        return;
    }

    if (RsnaSetDot11RSNAConfigGroupRekeyPackets ((INT4) u4ProfileId,
                                                 (UINT4) i4NumberOfPackets) !=
        SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Error in setting Group Rekey Packets\n");

        return;
    }
    if (RsnaTestv2Dot11RSNAConfigGroupRekeyStrict (&u4ErrorCode,
                                                   (INT4) u4ProfileId,
                                                   u1StrictStatus) !=
        SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Invalid Configuration - Group Rekey Strict\n");

        return;
    }

    if (RsnaSetDot11RSNAConfigGroupRekeyStrict ((INT4) u4ProfileId,
                                                u1StrictStatus) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Error in setting Group Rekey Strict\n");

        return;
    }

    if (STRLEN (au1psk))
    {
        if (u1PskType == RSNA_AKM_PSK_SETKEY_ASCII)
        {

            if ((RsnaTestv2Dot11RSNAConfigPSKPassPhrase
                 (&u4ErrorCode, (INT4) u4ProfileId, &PSK)) != SNMP_SUCCESS)
            {
                PNAC_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Invalid Configuration - PSK Pass Phrase\n");

                return;
            }
            if ((RsnaSetDot11RSNAConfigPSKPassPhrase ((INT4) u4ProfileId, &PSK))
                != SNMP_SUCCESS)
            {
                PNAC_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Error in setting PSK Pass Phrase\n");

                return;
            }
        }
        else
        {
            for (u1Index = 0; u1Index < u4KeyLength; u1Index += 2)
            {
                /* Assemble a digit pair into the hexbyte string */
                au1TempBuf[0] = au1psk[u1Index];
                au1TempBuf[1] = au1psk[u1Index + 1];

                /* Convert the hex pair to an integer */
                SSCANF ((const CHR1 *) au1TempBuf, "%x",
                        (UINT4 *) &au1Output[u1Index / 2]);
            }
            MEMCPY (PSK.pu1_OctetList, au1Output, (u4KeyLength / 2));
            PSK.i4_Length = (INT4) (u4KeyLength / 2);

            if ((RsnaTestv2Dot11RSNAConfigPSKValue
                 (&u4ErrorCode, (INT4) u4ProfileId, &PSK)) != SNMP_SUCCESS)
            {
                PNAC_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *) "Invalid Configuration - PSK\n");

                return;
            }

            if ((RsnaSetDot11RSNAConfigPSKValue ((INT4) u4ProfileId, &PSK)) !=
                SNMP_SUCCESS)
            {
                PNAC_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Error in setting PSK\n");

                return;
            }
        }

    }
    if (RsnaTestv2Dot11RSNAConfigGroupUpdateCount (&u4ErrorCode,
                                                   (INT4) u4ProfileId,
                                                   u4RetryCount) !=
        SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Invalid Configuration - Group Update Count\n");

        return;
    }

    if (RsnaSetDot11RSNAConfigGroupUpdateCount ((INT4) u4ProfileId,
                                                u4RetryCount) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Error in setting Group Update Count\n");

        return;
    }

    if (RsnaTestv2Dot11RSNAConfigPairwiseUpdateCount (&u4ErrorCode,
                                                      (INT4) u4ProfileId,
                                                      u4PairwiseRetryCount) !=
        SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Invalid Configuration - Pairwise Update Count\n");

        return;
    }

    if (RsnaSetDot11RSNAConfigPairwiseUpdateCount
        ((INT4) u4ProfileId, u4PairwiseRetryCount) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Error in setting Pairwise Update Count\n");

        return;
    }

    if (RsnaTestv2Dot11RSNAConfigPMKLifetime (&u4ErrorCode, (INT4) u4ProfileId,
                                              u4LifeTime) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Invalid Configuration - PMK Lifetime\n");

        return;
    }
    if (RsnaSetDot11RSNAConfigPMKLifetime ((INT4) u4ProfileId, u4LifeTime)
        != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting PMK Lifetime\n");

        return;
    }

    if (RsnaTestv2Dot11RSNAConfigPMKReauthThreshold (&u4ErrorCode,
                                                     (INT4) u4ProfileId,
                                                     u4ThresholdValue) !=
        SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Invalid Configuration - PMK Reauth Threshold\n");

        return;
    }

    if (RsnaSetDot11RSNAConfigPMKReauthThreshold
        ((INT4) u4ProfileId, u4ThresholdValue) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Error in setting PMK Reauth Threshold\n");

        return;
    }

    if (RsnaTestv2Dot11RSNAConfigSATimeout (&u4ErrorCode, (INT4) u4ProfileId,
                                            u4TimeValue) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Invalid Configuration - SA Timeout\n");

        return;
    }
    if (RsnaSetDot11RSNAConfigSATimeout ((INT4) u4ProfileId, u4TimeValue) !=
        SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting SA Timeout\n");

        return;
    }
    PNAC_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    IssProcessRsnaConfigEditPageGet (pHttp);
}
#endif

/*********************************************************************
*  Function Name : IssProcessRsnaConfigPage
*  Description   : This function processes the request coming for the
*                  Rsna Config Table Page
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessRsnaConfigPage (tHttp * pHttp)
{
#ifdef RSNA_WANTED
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessRsnaConfigPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessRsnaConfigPageSet (pHttp);
    }
#else
    UNUSED_PARAM (pHttp);
#endif
}

#ifdef RSNA_WANTED
/*********************************************************************
*  Function Name : IssProcessRsnaConfigPageGet
*  Description   : This function processes the get request coming for the
*                  Rsna Config Table  page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessRsnaConfigPageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    UINT4               u4currentWlanId = 0;
    UINT4               u4nextWlanId = 0;
    UINT4               u4ProfileId = 0;
    INT4                i4OutCome = 0;
    INT4                i4RsnaEnabled = 0;
    INT4                i4PreauthenticationEnabled = 0;
    INT4                OKCEnabled = 0;
    tSNMP_OCTET_STRING_TYPE GroupCipher;
    tSNMP_OCTET_STRING_TYPE AuthSuiteSelected;
    tSNMP_OCTET_STRING_TYPE PairwiseCipherSelected;
    tSNMP_OCTET_STRING_TYPE GroupwiseCipherSelected;
    tSNMP_OCTET_STRING_TYPE PmkIdUsed;
    tSNMP_OCTET_STRING_TYPE AuthSuiteRequested;
    tSNMP_OCTET_STRING_TYPE PairwiseCipherRequested;
    tSNMP_OCTET_STRING_TYPE GroupwiseCipherRequested;
    UINT1               au1GroupCipher[RSNA_CIPHER_SUITE_LEN];
    UINT1               au1AuthSuiteSelected[RSNA_CIPHER_SUITE_LEN];
    UINT1               au1PairwiseCipherSelected[RSNA_CIPHER_SUITE_LEN];
    UINT1               au1GroupCipherSelected[RSNA_CIPHER_SUITE_LEN];
    UINT1               au1PMKIDUsed[RSNA_PMKID_DATA_LEN];
    UINT1               au1AuthSuiteRequested[RSNA_CIPHER_SUITE_LEN];
    UINT1               au1PairwiseCipherRequested[RSNA_CIPHER_SUITE_LEN];
    UINT1               au1GroupwiseCipherRequested[RSNA_CIPHER_SUITE_LEN];
    UINT4               u4ConfigVersion = 0;
    UINT4               u4PairwiseKeySupported = 0;
    UINT4               u4GroupRekeyMethod = 0;
    UINT4               u4GroupRekeyTime = 0;
    UINT4               u4GroupRekeyPkts = 0;
    UINT4               u4GroupRekeyStrict = 0;
    UINT4               u4GroupUpdateCount = 0;
    UINT4               u4PairwiseUpdateCount = 0;
    UINT4               u4GroupCipherSize = 0;
    UINT4               u4PMKLifetime = 0;
    UINT4               u4PMKReAuthThreshold = 0;
    UINT4               u4PTKSAReplayCounter = 0;
    UINT4               u4SATimeOut = 0;
    UINT4               u4TKIPCounterMeasuresInvoked = 0;
    UINT4               u4FourWayHandshakeFailures = 0;
    UINT4               u4GTKSAReplayCountersCount = 0;

    MEMSET (au1GroupCipher, '\0', sizeof (RSNA_CIPHER_SUITE_LEN));
    MEMSET (au1AuthSuiteSelected, '\0', (RSNA_CIPHER_SUITE_LEN));
    MEMSET (au1PairwiseCipherSelected, '\0', (RSNA_CIPHER_SUITE_LEN));
    MEMSET (au1GroupCipherSelected, '\0', (RSNA_CIPHER_SUITE_LEN));
    MEMSET (au1PMKIDUsed, '\0', (RSNA_CIPHER_SUITE_LEN));
    MEMSET (au1AuthSuiteRequested, '\0', (RSNA_CIPHER_SUITE_LEN));
    MEMSET (au1PairwiseCipherRequested, '\0', (RSNA_CIPHER_SUITE_LEN));
    MEMSET (au1GroupwiseCipherRequested, '\0', (RSNA_CIPHER_SUITE_LEN));

    GroupCipher.pu1_OctetList = au1GroupCipher;
    AuthSuiteSelected.pu1_OctetList = au1AuthSuiteSelected;
    PairwiseCipherSelected.pu1_OctetList = au1PairwiseCipherSelected;
    GroupwiseCipherSelected.pu1_OctetList = au1GroupCipherSelected;
    PmkIdUsed.pu1_OctetList = au1PMKIDUsed;
    AuthSuiteRequested.pu1_OctetList = au1AuthSuiteRequested;
    PairwiseCipherRequested.pu1_OctetList = au1PairwiseCipherRequested;
    GroupwiseCipherRequested.pu1_OctetList = au1GroupwiseCipherRequested;

    if (WebnmAllocMultiData () == NULL)

    {
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        return;
    }

    u4currentWlanId = 0;
    u4nextWlanId = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;
    WebnmRegisterLock (pHttp, PnacLock, PnacUnLock);
    PNAC_LOCK ();
    i4OutCome = nmhGetFirstIndexCapwapDot11WlanTable (&u4nextWlanId);
    if (i4OutCome == SNMP_FAILURE)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    do
    {

        pHttp->i4Write = (INT4) u4Temp;
        u4currentWlanId = u4nextWlanId;
        if (u4currentWlanId != 0)
        {
            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4currentWlanId, (INT4 *) &u4ProfileId) == SNMP_SUCCESS)
            {
                if (u4ProfileId != 0)
                {
                    nmhGetDot11RSNAActivated ((INT4) u4ProfileId,
                                              &i4RsnaEnabled);

                    nmhGetDot11RSNAPreauthenticationActivated ((INT4)
                                                               u4ProfileId,
                                                               &i4PreauthenticationEnabled);
                    nmhGetFsRSNAOKCEnabled ((INT4) u4ProfileId, &OKCEnabled);

                    RsnaGetDot11RSNAConfigVersion ((INT4) u4ProfileId,
                                                   (INT4 *) &u4ConfigVersion);

                    RsnaGetDot11RSNAConfigPairwiseKeysSupported ((INT4)
                                                                 u4ProfileId,
                                                                 &u4PairwiseKeySupported);

                    RsnaGetDot11RSNAConfigGroupCipher ((INT4) u4ProfileId,
                                                       &GroupCipher);

                    RsnaGetDot11RSNAConfigGroupRekeyMethod ((INT4) u4ProfileId,
                                                            (INT4 *)
                                                            &u4GroupRekeyMethod);

                    RsnaGetDot11RSNAConfigGroupRekeyTime ((INT4) u4ProfileId,
                                                          &u4GroupRekeyTime);

                    RsnaGetDot11RSNAConfigGroupRekeyPackets ((INT4) u4ProfileId,
                                                             &u4GroupRekeyPkts);

                    RsnaGetDot11RSNAConfigGroupRekeyStrict ((INT4) u4ProfileId,
                                                            (INT4 *)
                                                            &u4GroupRekeyStrict);

                    RsnaGetDot11RSNAConfigGroupUpdateCount ((INT4) u4ProfileId,
                                                            &u4GroupUpdateCount);

                    RsnaGetDot11RSNAConfigPairwiseUpdateCount ((INT4)
                                                               u4ProfileId,
                                                               &u4PairwiseUpdateCount);

                    RsnaGetDot11RSNAConfigGroupCipherSize ((INT4) u4ProfileId,
                                                           &u4GroupCipherSize);

                    RsnaGetDot11RSNAConfigPMKLifetime ((INT4) u4ProfileId,
                                                       &u4PMKLifetime);

                    RsnaGetDot11RSNAConfigPMKReauthThreshold ((INT4)
                                                              u4ProfileId,
                                                              &u4PMKReAuthThreshold);

                    RsnaGetDot11RSNAConfigNumberOfPTKSAReplayCounters ((INT4)
                                                                       u4ProfileId,
                                                                       (INT4 *)
                                                                       &u4PTKSAReplayCounter);

                    RsnaGetDot11RSNAConfigSATimeout ((INT4) u4ProfileId,
                                                     &u4SATimeOut);

                    RsnaGetDot11RSNAAuthenticationSuiteSelected ((INT4)
                                                                 u4ProfileId,
                                                                 &AuthSuiteSelected);

                    RsnaGetDot11RSNAPairwiseCipherSelected ((INT4) u4ProfileId,
                                                            &PairwiseCipherSelected);

                    RsnaGetDot11RSNAGroupCipherSelected ((INT4) u4ProfileId,
                                                         &GroupwiseCipherSelected);

                    RsnaGetDot11RSNAPMKIDUsed ((INT4) u4ProfileId, &PmkIdUsed);

                    RsnaGetDot11RSNAAuthenticationSuiteRequested ((INT4)
                                                                  u4ProfileId,
                                                                  &AuthSuiteRequested);

                    RsnaGetDot11RSNAPairwiseCipherRequested ((INT4) u4ProfileId,
                                                             &PairwiseCipherRequested);

                    RsnaGetDot11RSNAGroupCipherRequested ((INT4) u4ProfileId,
                                                          &GroupwiseCipherRequested);

                    RsnaGetDot11RSNATKIPCounterMeasuresInvoked ((INT4)
                                                                u4ProfileId,
                                                                &u4TKIPCounterMeasuresInvoked);

                    RsnaGetDot11RSNA4WayHandshakeFailures ((INT4) u4ProfileId,
                                                           &u4FourWayHandshakeFailures);

                    RsnaGetDot11RSNAConfigNumberOfGTKSAReplayCounters ((INT4)
                                                                       u4ProfileId,
                                                                       (INT4 *)
                                                                       &u4GTKSAReplayCountersCount);

                    STRCPY (pHttp->au1KeyString, "Rsnastatus_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    if (i4RsnaEnabled == RSNA_ENABLED)
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Enabled");
                    }
                    else
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Disabled");
                    }
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "Preauthentication_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    if (i4PreauthenticationEnabled == RSNA_ENABLED)
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Enabled");
                    }
                    else
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Disabled");
                    }

                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "okc_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    if (OKCEnabled == RSNA_ENABLED)
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Enabled");
                    }
                    else
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Disabled");
                    }

                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "wlan_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                             u4currentWlanId);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "RSNAVersion_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                             u4ConfigVersion);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString,
                            "RSNAPairwiseKeysSupported_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                             u4PairwiseKeySupported);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "RSNAGroupCipher_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%X:%X:%X:%X",
                             GroupCipher.pu1_OctetList[0],
                             GroupCipher.pu1_OctetList[1],
                             GroupCipher.pu1_OctetList[2],
                             GroupCipher.pu1_OctetList[3]);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "RSNAGroupRekeyMethod_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    if (u4GroupRekeyMethod == RSNA_GROUP_REKEY_DISABLED)
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Disabled");
                    }
                    else if (u4GroupRekeyMethod == RSNA_GROUP_REKEY_TIMEBASED)
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "TimeBased");
                    }
                    else if (u4GroupRekeyMethod == RSNA_GROUP_REKEY_PACKETBASED)
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "PackedBased");
                    }
                    else if (u4GroupRekeyMethod ==
                             RSNA_GROUP_REKEY_TIMEPACKETBASED)
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "TimePacketBased");
                    }
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "RSNAGroupRekeyTime_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                             u4GroupRekeyTime);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "RSNAGroupRekeyPackets_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                             u4GroupRekeyPkts);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "RSNAGroupRekeyStrict_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    if (u4GroupRekeyStrict == RSNA_ENABLED)
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Enable");
                    }
                    else if (u4GroupRekeyStrict == RSNA_DISABLED)
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Disable");
                    }
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "RSNAGroupUpdateCount_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%u",
                             u4GroupUpdateCount);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "RSNAPairwiseUpdateCount_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                             u4PairwiseUpdateCount);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "RSNAGroupCipherSize_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                             u4GroupCipherSize);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "RSNAPMKLifetime_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                             u4PMKLifetime);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "RSNAPMKReauthThreshold_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                             u4PMKReAuthThreshold);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString,
                            "RSNANumberOfPTKSAReplayCounters_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                             u4PTKSAReplayCounter);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "RSNASATimeout_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4SATimeOut);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString,
                            "RSNAAuthenticationSuiteSelected_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%x:%x:%x:%x",
                             AuthSuiteSelected.pu1_OctetList[0],
                             AuthSuiteSelected.pu1_OctetList[1],
                             AuthSuiteSelected.pu1_OctetList[2],
                             AuthSuiteSelected.pu1_OctetList[3]);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString,
                            "RSNAPairwiseCipherSelected_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%x:%x:%x:%x",
                             PairwiseCipherSelected.pu1_OctetList[0],
                             PairwiseCipherSelected.pu1_OctetList[1],
                             PairwiseCipherSelected.pu1_OctetList[2],
                             PairwiseCipherSelected.pu1_OctetList[3]);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "RSNAGroupCipherSelected_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%x:%x:%x:%x",
                             GroupwiseCipherSelected.pu1_OctetList[0],
                             GroupwiseCipherSelected.pu1_OctetList[1],
                             GroupwiseCipherSelected.pu1_OctetList[2],
                             GroupwiseCipherSelected.pu1_OctetList[3]);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "RSNAPMKIDUsed_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%x:%x:%x:%x",
                             PmkIdUsed.pu1_OctetList[0],
                             PmkIdUsed.pu1_OctetList[1],
                             PmkIdUsed.pu1_OctetList[2],
                             PmkIdUsed.pu1_OctetList[3]);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString,
                            "RSNAAuthenticationSuiteRequested_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%x:%x:%x:%x",
                             AuthSuiteRequested.pu1_OctetList[0],
                             AuthSuiteRequested.pu1_OctetList[1],
                             AuthSuiteRequested.pu1_OctetList[2],
                             AuthSuiteRequested.pu1_OctetList[3]);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString,
                            "RSNAPairwiseCipherSelected_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%x:%x:%x:%x",
                             PairwiseCipherRequested.pu1_OctetList[0],
                             PairwiseCipherRequested.pu1_OctetList[1],
                             PairwiseCipherRequested.pu1_OctetList[2],
                             PairwiseCipherRequested.pu1_OctetList[3]);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString,
                            "RSNAGroupCipherRequested_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%x:%x:%x:%x",
                             GroupwiseCipherRequested.pu1_OctetList[0],
                             GroupwiseCipherRequested.pu1_OctetList[1],
                             GroupwiseCipherRequested.pu1_OctetList[2],
                             GroupwiseCipherRequested.pu1_OctetList[3]);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString,
                            "RSNATKIPCounterMeasuresInvoked_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                             u4TKIPCounterMeasuresInvoked);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString,
                            "RSNA4WayHandshakeFailures_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                             u4FourWayHandshakeFailures);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString,
                            "RSNANumberOfGTKSAReplayCounters_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                             u4GTKSAReplayCountersCount);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                }

            }
        }
    }
    while (nmhGetNextIndexCapwapDot11WlanTable
           (u4currentWlanId, &u4nextWlanId));

    PNAC_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
*  Function Name : IssProcessRsnaConfigPageSet
*  Description   : This function processes the set request coming for the
*                  Rsna Config Table page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessRsnaConfigPageSet (tHttp * pHttp)
{
    INT4                i4WlanProfileId = 0;

    STRCPY (pHttp->au1Name, "WLAN_PROFILE_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4WlanProfileId = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    WSS_WEB_TRC ("WLAN Profile Id:%d\n", i4WlanProfileId);

    if (STRCMP (pHttp->au1Value, "Edit") == 0)
    {
        WSS_WEB_TRC ("Edit.....just store the Profile Id:%d\n",
                     i4WlanProfileId);
        gi4WlanProfileId = (INT4) i4WlanProfileId;
    }
    else
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Operation\n");
    }
    return;

}
#endif
/*********************************************************************
*  Function Name : IssProcessRsnaCipherPage
*  Description   : This function processes the request coming for the
*                  Rsna Cipher Table Page
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessRsnaCipherPage (tHttp * pHttp)
{
#ifdef RSNA_WANTED
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessRsnaCipherPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessRsnaCipherPageSet (pHttp);
    }
#else
    UNUSED_PARAM (pHttp);
#endif
}

#ifdef RSNA_WANTED
/*********************************************************************
*  Function Name : IssProcessRsnaCipherPageGet
*  Description   : This function processes the get request coming for the
*                  Rsna Cipher Table page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessRsnaCipherPageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    UINT4               u4currentWlanId = 0;
    UINT4               u4nextWlanId = 0;
    UINT4               u4WlanId = 0;
    INT4                i4OutCome = 0;
    UINT4               u4CurrConfigPairwiseCipherIndex = 0;
    UINT4               u4NextRSNAConfigPairwiseCipherIndex = 0;
    tSNMP_OCTET_STRING_TYPE RSNAConfigPairwiseCipher;
    UINT1               au1RSNAConfigCipher[4];
    UINT4               u4PairwiseCipherEnabled = 0;
    UINT4               u4PairwiseCipherSize = 0;

    MEMSET (au1RSNAConfigCipher, 0, sizeof (au1RSNAConfigCipher));
    RSNAConfigPairwiseCipher.pu1_OctetList = au1RSNAConfigCipher;
    RSNAConfigPairwiseCipher.i4_Length = (INT4) STRLEN (au1RSNAConfigCipher);

    if (WebnmAllocMultiData () == NULL)

    {
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;
    i4OutCome = nmhGetFirstIndexCapwapDot11WlanTable (&u4nextWlanId);

    STRCPY (pHttp->au1KeyString, "<! SSID>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        u4currentWlanId = u4nextWlanId;
        if (u4currentWlanId != 0)
        {
            STRCPY (pHttp->au1Name, "SSID");
            SPRINTF ((CHR1 *) pHttp->au1DataString,
                     "<option value = \"%d\" selected>%d \n",
                     u4currentWlanId, u4currentWlanId);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
    }
    while (nmhGetNextIndexCapwapDot11WlanTable
           (u4currentWlanId, &u4nextWlanId));

    u4currentWlanId = 0;
    u4nextWlanId = 0;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;
    WebnmRegisterLock (pHttp, PnacLock, PnacUnLock);
    PNAC_LOCK ();
    i4OutCome =
        RsnaGetFirstIndexDot11RSNAConfigPairwiseCiphersTable ((INT4 *)
                                                              &u4nextWlanId,
                                                              &u4NextRSNAConfigPairwiseCipherIndex);
    if (i4OutCome == SNMP_FAILURE)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }
    else
    {
        do
        {
            pHttp->i4Write = (INT4) u4Temp;
            u4CurrConfigPairwiseCipherIndex =
                u4NextRSNAConfigPairwiseCipherIndex;
            u4currentWlanId = u4nextWlanId;

            WssGetCapwapWlanId (u4currentWlanId, &u4WlanId);

            RsnaGetDot11RSNAConfigPairwiseCipher ((INT4) u4currentWlanId,
                                                  u4CurrConfigPairwiseCipherIndex,
                                                  &RSNAConfigPairwiseCipher);
            RsnaGetDot11RSNAConfigPairwiseCipherEnabled ((INT4) u4currentWlanId,
                                                         u4CurrConfigPairwiseCipherIndex,
                                                         (INT4 *)
                                                         &u4PairwiseCipherEnabled);
            RsnaGetDot11RSNAConfigPairwiseCipherSize ((INT4) u4currentWlanId,
                                                      u4CurrConfigPairwiseCipherIndex,
                                                      &u4PairwiseCipherSize);

            STRCPY (pHttp->au1KeyString, "wlan_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4WlanId);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            STRCPY (pHttp->au1KeyString, "RSNAPairwiseCipherIndex_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (u4CurrConfigPairwiseCipherIndex == RSNA_TKIP)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "TKIP");
            }
            else if (u4CurrConfigPairwiseCipherIndex == RSNA_CCMP)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "AES");
            }
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            STRCPY (pHttp->au1KeyString, "RSNAPairwiseCipher_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%X:%X:%X:%X",
                     RSNAConfigPairwiseCipher.pu1_OctetList[0],
                     RSNAConfigPairwiseCipher.pu1_OctetList[1],
                     RSNAConfigPairwiseCipher.pu1_OctetList[2],
                     RSNAConfigPairwiseCipher.pu1_OctetList[3]);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            STRCPY (pHttp->au1KeyString, "RSNAPairwiseCipherEnabled_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (u4PairwiseCipherEnabled == RSNA_ENABLED)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Enable");
            }
            else
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Disable");
            }

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            STRCPY (pHttp->au1KeyString, "RSNAPairwiseCipherSize_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4PairwiseCipherSize);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        }
        while (RsnaGetNextIndexDot11RSNAConfigPairwiseCiphersTable
               ((INT4) u4currentWlanId, (INT4 *) &u4nextWlanId,
                u4CurrConfigPairwiseCipherIndex,
                &u4NextRSNAConfigPairwiseCipherIndex) == SNMP_SUCCESS);
    }

    PNAC_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
*  Function Name : IssProcessRsnaCipherPageSet
*  Description   : This function processes the set request coming for the
*                  Rsna Cipher Table page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessRsnaCipherPageSet (tHttp * pHttp)
{
    UINT4               u4WlanId = 0;
    UINT1               u1CipherType = 0;
    INT1                i1Status = 0;
    UINT4               u4ProfileId = 0;
    UINT4               u4ErrorCode = 0;

    STRCPY (pHttp->au1Name, "SSID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4WlanId = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "pairwisecipher");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u1CipherType = (UINT1) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "CipherStat");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i1Status = (INT1) ATOI ((INT1 *) pHttp->au1Value);

    WebnmRegisterLock (pHttp, PnacLock, PnacUnLock);
    PNAC_LOCK ();

    if (nmhGetCapwapDot11WlanProfileIfIndex
        (u4WlanId, (INT4 *) &u4ProfileId) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in getting WlanIfIndex\n");
        return;
    }

    if (RsnaTestv2Dot11RSNAConfigPairwiseCipherEnabled (&u4ErrorCode,
                                                        (INT4) u4ProfileId,
                                                        u1CipherType,
                                                        i1Status) !=
        SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Invalid configuration - Cipher Status\n");
        return;
    }

    if (RsnaSetDot11RSNAConfigPairwiseCipherEnabled ((INT4) u4ProfileId,
                                                     u1CipherType,
                                                     i1Status) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting cipher status\n");
        return;
    }
    PNAC_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    IssProcessRsnaCipherPageGet (pHttp);
}
#endif

/*********************************************************************
*  Function Name : IssProcessRsnaAuthsuitePage
*  Description   : This function processes the request coming for the
*                  Rsna Authsuite Table Page
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessRsnaAuthsuitePage (tHttp * pHttp)
{
#ifdef RSNA_WANTED
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessRsnaAuthsuitePageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessRsnaAuthsuitePageSet (pHttp);
    }
#else
    UNUSED_PARAM (pHttp);
#endif
}

#ifdef RSNA_WANTED
/*********************************************************************
*  Function Name : IssProcessRsnaAuthsuitePageGet
*  Description   : This function processes the get request coming for the
*                  Rsna Authsuite Table page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessRsnaAuthsuitePageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    INT4                i4OutCome = 0;
    UINT4               u4currentWlanId = 0;
    UINT4               u4nextWlanId = 0;
    UINT4               u4WlanId = 0;
    tSNMP_OCTET_STRING_TYPE AuthenticationSuite;
    UINT4               u4CurrAuthSuiteIndex = 0;
    UINT4               u4NextAuthSuiteIndex = 0;
    UINT4               u4AuthSuiteStatus = 0;
    UINT1               au1AuthSuitetype[RSNA_MAX_AUTH_SUITE_TYPE_LENGTH];

    MEMSET (au1AuthSuitetype, 0, sizeof (au1AuthSuitetype));
    AuthenticationSuite.pu1_OctetList = au1AuthSuitetype;
    AuthenticationSuite.i4_Length = (INT4) (STRLEN (au1AuthSuitetype));

    if (WebnmAllocMultiData () == NULL)

    {
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;
    i4OutCome = nmhGetFirstIndexCapwapDot11WlanTable (&u4nextWlanId);

    STRCPY (pHttp->au1KeyString, "<! SSID>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        u4currentWlanId = u4nextWlanId;
        if (u4currentWlanId != 0)
        {
            STRCPY (pHttp->au1Name, "SSID");
            SPRINTF ((CHR1 *) pHttp->au1DataString,
                     "<option value = \"%d\" selected>%d \n",
                     u4currentWlanId, u4currentWlanId);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
    }
    while (nmhGetNextIndexCapwapDot11WlanTable
           (u4currentWlanId, &u4nextWlanId));

    u4currentWlanId = 0;
    u4nextWlanId = 0;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;
    WebnmRegisterLock (pHttp, PnacLock, PnacUnLock);
    PNAC_LOCK ();

    i4OutCome =
        RsnaGetFirstIndexDot11RSNAConfigAuthenticationSuitesTable
        ((INT4 *) &u4nextWlanId, &u4NextAuthSuiteIndex);

    if (i4OutCome == SNMP_FAILURE)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }
    else
    {
        do
        {
            pHttp->i4Write = (INT4) u4Temp;
            u4CurrAuthSuiteIndex = u4NextAuthSuiteIndex;
            u4currentWlanId = u4nextWlanId;
            WssGetCapwapWlanId (u4currentWlanId, &u4WlanId);

            RsnaGetDot11RSNAConfigAuthenticationSuite (u4currentWlanId,
                                                       u4CurrAuthSuiteIndex,
                                                       &AuthenticationSuite);
            RsnaGetDot11RSNAConfigAuthenticationSuiteEnabled
                ((UINT4) u4currentWlanId, u4CurrAuthSuiteIndex,
                 (INT4 *) &u4AuthSuiteStatus);

            STRCPY (pHttp->au1KeyString, "wlan_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4WlanId);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            STRCPY (pHttp->au1KeyString, "AuthenticationSuiteIndex_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (u4CurrAuthSuiteIndex == RSNA_AUTHSUITE_PSK)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "PSK");
            }
            else if (u4CurrAuthSuiteIndex == RSNA_AUTHSUITE_8021X)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "8021X");
            }
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            STRCPY (pHttp->au1KeyString, "AuthenticationSuite_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%X:%X:%X:%X",
                     AuthenticationSuite.pu1_OctetList[0],
                     AuthenticationSuite.pu1_OctetList[1],
                     AuthenticationSuite.pu1_OctetList[2],
                     AuthenticationSuite.pu1_OctetList[3]);

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            STRCPY (pHttp->au1KeyString, "AuthenticationSuiteStatus_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (u4AuthSuiteStatus == RSNA_ENABLED)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Enable");
            }
            else
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Disable");
            }

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        }
        while (RsnaGetNextIndexDot11RSNAConfigAuthenticationSuitesTable
               ((INT4) u4currentWlanId, (INT4 *) &u4nextWlanId,
                u4CurrAuthSuiteIndex, &u4NextAuthSuiteIndex) == SNMP_SUCCESS);
    }

    PNAC_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
*  Function Name : IssProcessRsnaAuthsuitePageSet
*  Description   : This function processes the set request coming for the
*                  Rsna Authsuite Table page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessRsnaAuthsuitePageSet (tHttp * pHttp)
{
    UINT1               u1AuthType = 0;
    UINT1               u1Status = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4ProfileId = 0;
    UINT4               u4WlanId = 0;

    STRCPY (pHttp->au1Name, "SSID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4WlanId = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "AuthType");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u1AuthType = (UINT1) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "AdminStat");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u1Status = (UINT1) ATOI ((INT1 *) pHttp->au1Value);
    WebnmRegisterLock (pHttp, PnacLock, PnacUnLock);
    PNAC_LOCK ();

    if (nmhGetCapwapDot11WlanProfileIfIndex
        (u4WlanId, (INT4 *) &u4ProfileId) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in getting WlanIfIndex\n");
        return;
    }

    if (RsnaTestv2Dot11RSNAConfigAuthenticationSuiteEnabled (&u4ErrorCode,
                                                             (UINT4) u1AuthType,
                                                             (INT4) u1Status) !=
        SNMP_SUCCESS)
    {

        WebnmUnRegisterLock (pHttp);
        PNAC_UNLOCK ();
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Invalid configuration - Authsuite Status\n");
        return;

    }

    if (RsnaSetDot11RSNAConfigAuthenticationSuiteEnabled (u4ProfileId,
                                                          (UINT4) u1AuthType,
                                                          (INT1) u1Status) !=
        SNMP_SUCCESS)
    {

        WebnmUnRegisterLock (pHttp);
        PNAC_UNLOCK ();
        IssSendError (pHttp,
                      (CONST INT1 *) "Error in setting Authsuite Status\n");
        return;

    }

    WebnmUnRegisterLock (pHttp);
    PNAC_UNLOCK ();
    IssProcessRsnaAuthsuitePageGet (pHttp);
}
#endif
#ifdef BAND_SELECT_WANTED
/*********************************************************************
*  Function Name : IssProcessWlanBandSelectGlobal
*  Description   : This function processes the request coming for the
*                  Rsna Protected Mngmt Table Page
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessWlanBandSelectGlobal (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessWlanBandSelectGlobalGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessWlanBandSelectGlobalSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessWlanBandSelectGlobalGet
*  Description   : This function processes the get request coming for the
*                  Rsna Protected Mngmt Table page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessWlanBandSelectGlobalGet (tHttp * pHttp)
{
    INT4                i4BandSelectStatus = 0;

    nmhGetFsBandSelectStatus (&i4BandSelectStatus);
    STRCPY (pHttp->au1KeyString, "bandselectstatus_key");
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4BandSelectStatus);
    WebnmSendString (pHttp, pHttp->au1KeyString);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    return;

}

/*********************************************************************
*  Function Name : IssProcessWlanBandSelectGlobalSet
*  Description   : This function processes the set request coming for the
*                  Rsna Protected Mngmt Table page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessWlanBandSelectGlobalSet (tHttp * pHttp)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4BandSelectStatus = 0;

    STRCPY (pHttp->au1Name, "bandselectstatus");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4BandSelectStatus = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    if (nmhTestv2FsBandSelectStatus (&u4ErrorCode, i4BandSelectStatus) !=
        SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Testing Failure");
        return;
    }

    if (nmhSetFsBandSelectStatus (i4BandSelectStatus) != SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Not able to set");
        return;
    }
    IssProcessWlanBandSelectGlobalGet (pHttp);
    return;
}

/*********************************************************************
*  Function Name : IssProcessWlanBandSelectLocal
*  Description   : This function processes the request coming for the
*                  Rsna Protected Mngmt Table Page
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessWlanBandSelectLocal (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessWlanBandSelectLocalGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessWlanBandSelectLocalSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessWlanBandSelectLocalGet
*  Description   : This function processes the get request coming for the
*                  Rsna Protected Mngmt Table page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessWlanBandSelectLocalGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    UINT4               u4NextWlanProfID = 0;
    UINT4               u4WlanProfID = 0;
    INT4                i4BandSelectStatus = 0;
    UINT4               u4AssocCount = 0;
    UINT4               u4AgeOutTimer = 0;
    UINT4               u4FlushTimer = 0;
    INT4                i4OutCome;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    i4OutCome = nmhGetFirstIndexFsWlanBandSelectTable (&u4NextWlanProfID);
    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html +
                                          pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WebnmUnRegisterLock (pHttp);
        return;

    }

    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        STRCPY (pHttp->au1KeyString, "WLAN_ID_KEY");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4NextWlanProfID);
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsBandSelectPerWlanStatus (u4NextWlanProfID, &i4BandSelectStatus);
        STRCPY (pHttp->au1KeyString, "BAND_SELECT_STATUS_KEY");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4BandSelectStatus);
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsBandSelectAssocCount (u4NextWlanProfID, &u4AssocCount);
        STRCPY (pHttp->au1KeyString, "ASSOC_COUNT_KEY");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4AssocCount);
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsBandSelectAgeOutSuppTimer (u4NextWlanProfID, &u4AgeOutTimer);
        STRCPY (pHttp->au1KeyString, "AGE_OUT_KEY");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4AgeOutTimer);
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsBandSelectAssocCountFlushTimer (u4NextWlanProfID,
                                                &u4FlushTimer);
        STRCPY (pHttp->au1KeyString, "FLUSH_TIMER_KEY");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4FlushTimer);
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        u4WlanProfID = u4NextWlanProfID;
    }
    while (nmhGetNextIndexFsWlanBandSelectTable
           (u4WlanProfID, &u4NextWlanProfID) == SNMP_SUCCESS);

    WebnmUnRegisterLock (pHttp);
    WebnmUnRegisterContext (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    return;
}

/*********************************************************************
*  Function Name : IssProcessWlanBandSelectLocalSet
*  Description   : This function processes the set request coming for the
*                  Rsna Protected Mngmt Table page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessWlanBandSelectLocalSet (tHttp * pHttp)
{
    UINT4               u4WlanProfID = 0;
    INT4                i4BandSelectStatus = 0;
    UINT4               u4AssocCount = 0;
    UINT4               u4AgeOutTimer = 0;
    UINT4               u4FlushTimer = 0;
    UINT4               u4ErrorCode = 0;

    STRCPY (pHttp->au1Name, "WLAN_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4WlanProfID = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "BAND_SELECT_STATUS");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4BandSelectStatus = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ASSOC_COUNT");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4AssocCount = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "AGE_OUT");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4AgeOutTimer = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "FLUSH_TIMER");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4FlushTimer = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    if (nmhTestv2FsBandSelectPerWlanStatus
        (&u4ErrorCode, u4WlanProfID, i4BandSelectStatus) != SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Invalid Configuration - Band Select Status\n");
        return;
    }

    if (nmhSetFsBandSelectPerWlanStatus (u4WlanProfID, i4BandSelectStatus) !=
        SNMP_SUCCESS)
    {

        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Invalid Configuration - Band Select Status\n");
        return;
    }
    if (nmhTestv2FsBandSelectAssocCount
        (&u4ErrorCode, u4WlanProfID, u4AssocCount) != SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Invalid Configuration - Assoc Count\n");
        return;
    }

    if (nmhSetFsBandSelectAssocCount (u4WlanProfID, u4AssocCount) !=
        SNMP_SUCCESS)
    {

        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Invalid Configuration - Assoc Count\n");
        return;
    }
    if (nmhTestv2FsBandSelectAgeOutSuppTimer
        (&u4ErrorCode, u4WlanProfID, u4AgeOutTimer) != SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Invalid Configuration - AgeOut Supp Timer\n");
        return;
    }

    if (nmhSetFsBandSelectAgeOutSuppTimer (u4WlanProfID, u4AgeOutTimer) !=
        SNMP_SUCCESS)
    {

        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Invalid Configuration - AgeOut Supp Timer\n");
        return;
    }
    if (nmhTestv2FsBandSelectAssocCountFlushTimer
        (&u4ErrorCode, u4WlanProfID, u4FlushTimer) != SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Invalid Configuration - Flush Timer\n");
        return;
    }

    if (nmhSetFsBandSelectAssocCountFlushTimer (u4WlanProfID, u4FlushTimer) !=
        SNMP_SUCCESS)
    {

        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Invalid Configuration - Flush Timer\n");
        return;
    }
    IssProcessWlanBandSelectLocalGet (pHttp);
    return;

}

/*********************************************************************
*  Function Name : IssProcessWlanBandSelectLocal
*  Description   : This function processes the request coming for the
*                  Rsna Protected Mngmt Table Page
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessWlanBandSelectClientTable (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessWlanBandSelectClientTableGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessWlanBandSelectClientTableSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessWlanBandSelectClientTableGet
*  Description   : This function processes the get request coming for the
*                  Rsna Protected Mngmt Table page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessWlanBandSelectClientTableGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    tMacAddr            NextClientMac;
    tMacAddr            ClientMac;
    tMacAddr            NextEnableBSSID;
    tMacAddr            EnableBSSID;
    INT4                i4ActiveStatus = 0;
    INT4                i4OutCome;

    MEMSET (NextClientMac, 0, sizeof (tMacAddr));
    MEMSET (ClientMac, 0, sizeof (tMacAddr));
    MEMSET (NextEnableBSSID, 0, sizeof (tMacAddr));
    MEMSET (EnableBSSID, 0, sizeof (tMacAddr));

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    i4OutCome =
        nmhGetFirstIndexFsWlanBandSelectClientTable (&NextClientMac,
                                                     &NextEnableBSSID);
    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html +
                                          pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WebnmUnRegisterLock (pHttp);
        return;

    }

    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        STRCPY (pHttp->au1KeyString, "CLIENT_MAC_KEY");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", NextClientMac);
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "ENABLE_BSSID_KEY");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", NextEnableBSSID);
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsWlanBandSelectStationActiveStatus (NextClientMac,
                                                   NextEnableBSSID,
                                                   &i4ActiveStatus);
        STRCPY (pHttp->au1KeyString, "ACTIVE_STATUS_KEY");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4ActiveStatus);
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        MEMCPY (ClientMac, NextClientMac, sizeof (tMacAddr));
        MEMCPY (EnableBSSID, NextEnableBSSID, sizeof (tMacAddr));
    }
    while (nmhGetNextIndexFsWlanBandSelectClientTable
           (ClientMac, &NextClientMac, EnableBSSID,
            &NextEnableBSSID) == SNMP_SUCCESS);

    WebnmUnRegisterLock (pHttp);
    WebnmUnRegisterContext (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    return;
}

/*********************************************************************
*  Function Name : IssProcessWlanBandSelectClientTableSet
*  Description   : This function processes the set request coming for the
*                  Rsna Protected Mngmt Table page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessWlanBandSelectClientTableSet (tHttp * pHttp)
{
    UNUSED_PARAM (pHttp);
}
#endif
#ifdef PMF_WANTED
/*********************************************************************
*  Function Name : IssProcessRsnaProtectedMgmtPage
*  Description   : This function processes the request coming for the
*                  Rsna Protected Mngmt Table Page
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessRsnaProtectedMgmtPage (tHttp * pHttp)
{
#ifdef RSNA_WANTED
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessRsnaProtectedMgmtPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessRsnaProtectedMgmtPageSet (pHttp);
    }
#else
    UNUSED_PARAM (pHttp);
#endif
}

#ifdef RSNA_WANTED

/*********************************************************************
*  Function Name : IssProcessRsnaProtectedMgmtPageGet
*  Description   : This function processes the get request coming for the
*                  Rsna Protected Mngmt Table page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessRsnaProtectedMgmtPageGet (tHttp * pHttp)
{
    INT4                i4OutCome = 0;
    INT4                i4ProtMgmtFrameEnabled = 0;
    INT4                i4UnProtMgmtFramAllowed = 0;
    UINT4               u4Temp = 0;
    UINT4               u4currentWlanId = 0;
    UINT4               u4nextWlanId = 0;
    UINT4               u4firstWlanId = 0;
    UINT4               u4ProfileId = 0;
    UINT4               u4AssocSAQueryMaxTImeOut = 0;
    UINT4               u4AssocSAQueryRetryTimeOut = 0;
    UINT4               u4AssocComeBackTime = 0;

    u4Temp = (UINT4) pHttp->i4Write;
    WebnmRegisterLock (pHttp, PnacLock, PnacUnLock);
    PNAC_LOCK ();
    i4OutCome = nmhGetFirstIndexCapwapDot11WlanTable (&u4firstWlanId);
    if (i4OutCome == SNMP_FAILURE)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    u4currentWlanId = u4firstWlanId;
    u4nextWlanId = u4firstWlanId;
    STRCPY (pHttp->au1KeyString, "<! SSID>");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    do
    {
        u4currentWlanId = u4nextWlanId;
        if (u4currentWlanId != 0)
        {
            STRCPY (pHttp->au1Name, "SSID");
            SPRINTF ((CHR1 *) pHttp->au1DataString,
                     "<option value = \"%d\">%d \n",
                     u4currentWlanId, u4currentWlanId);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
    }
    while (nmhGetNextIndexCapwapDot11WlanTable
           (u4currentWlanId, &u4nextWlanId));

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;
    u4currentWlanId = u4firstWlanId;
    u4nextWlanId = u4firstWlanId;
    do
    {
        pHttp->i4Write = (INT4) u4Temp;
        u4currentWlanId = u4nextWlanId;
        if (nmhGetCapwapDot11WlanProfileIfIndex (u4currentWlanId,
                                                 (INT4 *) &u4ProfileId) !=
            SNMP_SUCCESS)
        {
            PNAC_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Error in getting WlanIfIndex\n");
            return;
        }

        RsnaGetDot11RSNAProtectedManagementFramesActivated ((INT4) u4ProfileId,
                                                            &i4ProtMgmtFrameEnabled);
        RsnaGetDot11RSNAUnprotectedManagementFramesAllowed ((INT4) u4ProfileId,
                                                            &i4UnProtMgmtFramAllowed);
        RsnaGetDot11AssociationSAQueryMaximumTimeout ((INT4) u4ProfileId,
                                                      &u4AssocSAQueryMaxTImeOut);
        RsnaGetDot11AssociationSAQueryRetryTimeout ((INT4) u4ProfileId,
                                                    &u4AssocSAQueryRetryTimeOut);
        nmhGetFsRSNAAssociationComeBackTime ((INT4) u4ProfileId,
                                             &u4AssocComeBackTime);

        STRCPY (pHttp->au1KeyString, "wlan_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4currentWlanId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString, "ProtectedManagementFramesEnabled_key");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4ProtMgmtFrameEnabled == RSNA_ENABLED)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Enable");
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Disable");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString, "UnprotectedManagementFramesAllowed_Key");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4UnProtMgmtFramAllowed == RSNA_ENABLED)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Enable");
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Disable");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString, "AssociationSAQueryMaximumTimeout_Key");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4AssocSAQueryMaxTImeOut);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString, "AssociationSAQueryRetryTimeout_Key");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                 u4AssocSAQueryRetryTimeOut);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString, "AssociationComebacktime_Key");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4AssocComeBackTime);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    while (nmhGetNextIndexCapwapDot11WlanTable
           (u4currentWlanId, &u4nextWlanId));
    PNAC_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
*  Function Name : IssProcessRsnaProtectedMgmtPageSet
*  Description   : This function processes the set request coming for the
*                  Rsna Protected Mngmt Table page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessRsnaProtectedMgmtPageSet (tHttp * pHttp)
{
    INT4                i4ProtMgmtFrameEnabled = 0;
    INT4                i4UnProtMgmtFramAllowed = 0;
    UINT4               u4WlanId = 0;
    UINT4               u4ProfileId = 0;
    UINT4               u4AssocSAQueryMaxTImeOut = 0;
    UINT4               u4AssocSAQueryRetryTimeOut = 0;
    UINT4               u4AssocComeBackTime = 0;
    UINT4               u4ErrorCode = 0;

    STRCPY (pHttp->au1Name, "SSID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4WlanId = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ProtectedManagementFramesEnabled");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4ProtMgmtFrameEnabled = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "UnprotectedManagementFramesAllowed");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4UnProtMgmtFramAllowed = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "AssociationSAQueryMaximumTimeout");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4AssocSAQueryMaxTImeOut = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "AssociationSAQueryRetryTimeout");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4AssocSAQueryRetryTimeOut = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "AssociationComebacktime");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4AssocComeBackTime = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    WebnmRegisterLock (pHttp, PnacLock, PnacUnLock);
    PNAC_LOCK ();

    if (nmhGetCapwapDot11WlanProfileIfIndex
        (u4WlanId, (INT4 *) &u4ProfileId) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in getting WlanIfIndex\n");
        return;
    }

    if (RsnaTestv2Dot11RSNAProtectedManagementFramesActivated (&u4ErrorCode,
                                                               (INT4)
                                                               u4ProfileId,
                                                               i4ProtMgmtFrameEnabled)
        != SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        PNAC_UNLOCK ();
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Invalid configuration - ProtectedManagementFramesEnabled\n");
        return;
    }

    if (RsnaSetDot11RSNAProtectedManagementFramesActivated ((INT4) u4ProfileId,
                                                            i4ProtMgmtFrameEnabled)
        != SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        PNAC_UNLOCK ();
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Error in setting - ProtectedManagementFramesEnabled \n");
        return;
    }

    if (RsnaTestv2Dot11RSNAUnprotectedManagementFramesAllowed (&u4ErrorCode,
                                                               (INT4)
                                                               u4ProfileId,
                                                               i4UnProtMgmtFramAllowed)
        != SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        PNAC_UNLOCK ();
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Invalid configuration - UnprotectedManagementFramesAllowed\n");
        return;
    }

    if (RsnaSetDot11RSNAUnprotectedManagementFramesAllowed ((INT4) u4ProfileId,
                                                            i4UnProtMgmtFramAllowed)
        != SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        PNAC_UNLOCK ();
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Error in setting - UnprotectedManagementFramesAllowed \n");
        return;
    }

    if (RsnaTestv2Dot11AssociationSAQueryMaximumTimeout (&u4ErrorCode,
                                                         (INT4) u4ProfileId,
                                                         u4AssocSAQueryMaxTImeOut)
        != SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        PNAC_UNLOCK ();
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Invalid configuration - AssociationSAQueryMaximumTimeout\n");
        return;
    }

    if (RsnaSetDot11AssociationSAQueryMaximumTimeout ((INT4) u4ProfileId,
                                                      u4AssocSAQueryMaxTImeOut)
        != SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        PNAC_UNLOCK ();
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Error in setting - AssociationSAQueryMaximumTimeout\n");
        return;
    }

    if (RsnaTestv2Dot11AssociationSAQueryRetryTimeout (&u4ErrorCode,
                                                       (INT4) u4ProfileId,
                                                       u4AssocSAQueryRetryTimeOut)
        != SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        PNAC_UNLOCK ();
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Invalid configuration - AssociationSAQueryRetryTimeout\n");
        return;
    }

    if (RsnaSetDot11AssociationSAQueryRetryTimeout ((INT4) u4ProfileId,
                                                    u4AssocSAQueryRetryTimeOut)
        != SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        PNAC_UNLOCK ();
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Error in setting - AssociationSAQueryRetryTimeout\n");
        return;
    }

    if (nmhTestv2FsRSNAAssociationComeBackTime (&u4ErrorCode,
                                                (INT4) u4ProfileId,
                                                u4AssocComeBackTime) !=
        SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        PNAC_UNLOCK ();
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Invalid configuration - AssociationComebacktime\n");
        return;
    }

    if (nmhSetFsRSNAAssociationComeBackTime ((INT4) u4ProfileId,
                                             u4AssocComeBackTime) !=
        SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        PNAC_UNLOCK ();
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Error in setting - AssociationComebacktime\n");
        return;
    }

    WebnmUnRegisterLock (pHttp);
    PNAC_UNLOCK ();
    IssProcessRsnaProtectedMgmtPageGet (pHttp);
}
#endif
#endif

/*********************************************************************
*  Function Name : IssProcessAuthenticationProfilePage
*  Description   : This function processes the request coming for the 
*                  Authentication Profile Table Page 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessAuthenticationProfilePage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessAuthenticationProfilePageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessAuthenticationProfilePageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessAuthenticationProfilePageGet 
*  Description   : This function processes the get request coming for the  
*                  Authentication Profile Table  page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessAuthenticationProfilePageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    INT4                i4OutCome = 0;
    tSNMP_OCTET_STRING_TYPE *pCurrentProfileName = NULL, *pNextProfileName =
        NULL;
    tSNMP_MULTI_DATA_TYPE *pu1DataString = NULL;

    if ((pu1DataString = WebnmAllocMultiData ()) == NULL)
    {
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        return;
    }

    pCurrentProfileName =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (pCurrentProfileName == NULL)
    {
        WSS_WEB_TRC
            ("IssProcessAuthenticationProfilePageGet failed due to memory allocation\n");
        return;
    }
    pNextProfileName =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (pNextProfileName == NULL)
    {
        WSS_WEB_TRC
            ("IssProcessAuthenticationProfilePageGet failed due to memory allocation\n");
        free_octetstring (pCurrentProfileName);
        return;
    }

    MEMSET (pCurrentProfileName->pu1_OctetList, 0, OCTETSTR_SIZE);
    pCurrentProfileName->i4_Length = 0;
    MEMSET (pNextProfileName->pu1_OctetList, 0, OCTETSTR_SIZE);
    pNextProfileName->i4_Length = 0;

    WebnmRegisterLock (pHttp, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;
    i4OutCome =
        nmhGetFirstIndexFsDot11AuthenticationProfileTable (pNextProfileName);

    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html +
                                          pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        free_octetstring (pCurrentProfileName);
        free_octetstring (pNextProfileName);
        return;
    }
    else
    {
        do
        {
            pHttp->i4Write = (INT4) u4Temp;

            STRNCPY (pu1DataString->pOctetStrValue->pu1_OctetList,
                     pNextProfileName->pu1_OctetList,
                     pNextProfileName->i4_Length);
            pu1DataString->pOctetStrValue->i4_Length =
                pNextProfileName->i4_Length;

            STRCPY (pHttp->au1KeyString, "AUTH_NAME");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            WebnmSendToSocket (pHttp, "AUTH_NAME", pu1DataString,
                               ENM_DISPLAYSTRING);

            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

            /* Copy next index to current index */
            STRNCPY (pCurrentProfileName->pu1_OctetList,
                     pNextProfileName->pu1_OctetList,
                     pNextProfileName->i4_Length);
            pCurrentProfileName->i4_Length = pNextProfileName->i4_Length;
        }
        while (nmhGetNextIndexFsDot11AuthenticationProfileTable
               (pCurrentProfileName, pNextProfileName) == SNMP_SUCCESS);

        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));

    }
    WebnmUnRegisterLock (pHttp);
    WSSCFG_UNLOCK;
    free_octetstring (pCurrentProfileName);
    free_octetstring (pNextProfileName);
}

/*********************************************************************
*  Function Name : IssProcessAuthenticationProfilePageSet 
*  Description   : This function processes the set request coming for the  
*                  Authentication Profile  Entry page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessAuthenticationProfilePageSet (tHttp * pHttp)
{

    tSNMP_OCTET_STRING_TYPE *pAuthProfileName;
    UINT1               au1ProfileName[OCTETSTR_SIZE];

    MEMSET (&au1ProfileName, 0, OCTETSTR_SIZE);
    pAuthProfileName =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (pAuthProfileName == NULL)
    {
        WSS_WEB_TRC
            ("IssProcessAuthenticationProfilePageGet failed due to memory allocation\n");
        return;
    }
    MEMSET (pAuthProfileName->pu1_OctetList, 0, OCTETSTR_SIZE);

    WebnmRegisterLock (pHttp, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;

    STRCPY (pHttp->au1Name, "PROFILE_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    MEMCPY (au1ProfileName, pHttp->au1Value, sizeof (au1ProfileName));

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    MEMSET (gAuthProfileName, 0, sizeof (gAuthProfileName));

    /*WSS_WEB_TRC("Profile Name : %s\n",au1ProfileName); WSS_WEB_TRC("Action : %s\n",pHttp->au1Value); */

    if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {
        WSS_WEB_TRC ("Deleting Profile Entry\n");

        pAuthProfileName->i4_Length = (INT4) (STRLEN (au1ProfileName));
        MEMCPY (pAuthProfileName->pu1_OctetList, au1ProfileName,
                pAuthProfileName->i4_Length);

        if (STRCMP (au1ProfileName, "default") == 0)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Default Profile Cannot Be Deleted\n");
            WebnmUnRegisterLock (pHttp);
            WSSCFG_UNLOCK;
            free_octetstring (pAuthProfileName);
            return;
        }

        if (nmhSetFsDot11AuthenticationRowStatus (pAuthProfileName, DESTROY) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
            WebnmUnRegisterLock (pHttp);
            WSSCFG_UNLOCK;
            free_octetstring (pAuthProfileName);
            return;
        }

    }
    else if (STRCMP (pHttp->au1Value, "Edit") == 0)
    {
        STRNCPY (gAuthProfileName, au1ProfileName, OCTETSTR_SIZE);
    }
    else
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Operation\n");
    }
    WebnmUnRegisterLock (pHttp);
    WSSCFG_UNLOCK;
    free_octetstring (pAuthProfileName);
    IssProcessAuthenticationProfilePageGet (pHttp);
    return;
}

/*********************************************************************
*  Function Name : IssProcessAuthenticationProfileCreatePage
*  Description   : This function processes the request coming for the 
*                  Authentication Profile Entry Page 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessAuthenticationProfileCreatePage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessAuthenticationProfileCreatePageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessAuthenticationProfileCreatePageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessAuthenticationProfileCreatePageGet
*  Description   : This function processes the get request coming for the  
*                  Authentication Profile Table Entry page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessAuthenticationProfileCreatePageGet (tHttp * pHttp)
{
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
*  Function Name : IssProcessAuthenticationProfileCreatePageSet 
*  Description   : This function processes the set request coming for the  
*                  Authentication Profile Entry page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessAuthenticationProfileCreatePageSet (tHttp * pHttp)
{
    tWsscfgFsDot11AuthenticationProfileEntry
        wsscfgFsDot11AuthenticationProfileEntry;
    tWsscfgIsSetFsDot11AuthenticationProfileEntry
        wsscfgIsSetFsDot11AuthenticationProfileEntry;
    tWsscfgFsDot11WlanCapabilityProfileEntry
        wsscfgFsDot11WlanCapabilityProfileEntry;
    INT4                i4Security = 0, i4KeyIndex = 0, i4KeyLength =
        0, i4KeyType = 0, i4WebAuth = 0;
    UINT1               au1Key[105];
    UINT1               au1ProfileName[OCTETSTR_SIZE];
    UINT4               u4ErrorCode = 0;

    MEMSET (&wsscfgFsDot11AuthenticationProfileEntry, 0,
            sizeof (tWsscfgFsDot11AuthenticationProfileEntry));
    MEMSET (&wsscfgIsSetFsDot11AuthenticationProfileEntry, 0,
            sizeof (tWsscfgIsSetFsDot11AuthenticationProfileEntry));
    MEMSET (&wsscfgFsDot11WlanCapabilityProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanCapabilityProfileEntry));
    MEMSET (au1Key, 0, sizeof (au1Key));
    MEMSET (au1ProfileName, 0, OCTETSTR_SIZE);

    WebnmRegisterLock (pHttp, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;

    STRCPY (pHttp->au1Name, "ProfileName");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ProfileName, pHttp->au1Value, (sizeof (au1ProfileName)));

    STRCPY (pHttp->au1Name, "security");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Security = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "WebAuth");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4WebAuth = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "KeyIndex");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4KeyIndex = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "KeyType");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4KeyType = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "KeyLength");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4KeyLength = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "KeyValue");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1Key, pHttp->au1Value, (sizeof (au1Key) - 1));

    WSS_WEB_TRC ("Values from  Security Table\n");
    WSS_WEB_TRC ("*************************\n");

    WSS_WEB_TRC ("Security : %d\n", i4Security);
    WSS_WEB_TRC ("WebAuth : %d\n", i4WebAuth);
    WSS_WEB_TRC ("KeyIndex : %d\n", i4KeyIndex);
    WSS_WEB_TRC ("Keyi4KeyType : %d\n", i4KeyType);
    WSS_WEB_TRC ("KeyLength : %d\n", i4KeyLength);
    WSS_WEB_TRC ("au1ProfileName : %s\n", au1ProfileName);

    wsscfgFsDot11AuthenticationProfileEntry.MibObject.
        i4FsDot11AuthenticationAlgorithm = i4Security;
    wsscfgFsDot11AuthenticationProfileEntry.MibObject.i4FsDot11WepKeyIndex =
        i4KeyIndex;
    wsscfgFsDot11AuthenticationProfileEntry.MibObject.i4FsDot11WepKeyType =
        i4KeyType;
    if (STRLEN (au1Key))
    {
        STRNCPY (wsscfgFsDot11AuthenticationProfileEntry.MibObject.
                 au1FsDot11WepKey, au1Key, STRLEN (au1Key));
        wsscfgFsDot11AuthenticationProfileEntry.MibObject.i4FsDot11WepKeyLen =
            (INT4) STRLEN (au1Key);
    }
    wsscfgFsDot11AuthenticationProfileEntry.MibObject.i4FsDot11WepKeyLength =
        i4KeyLength;
    wsscfgFsDot11AuthenticationProfileEntry.MibObject.
        i4FsDot11WebAuthentication = i4WebAuth;
    wsscfgFsDot11AuthenticationProfileEntry.MibObject.
        i4FsDot11AuthenticationRowStatus = CREATE_AND_GO;
    STRNCPY (wsscfgFsDot11AuthenticationProfileEntry.MibObject.
             au1FsDot11AuthenticationProfileName, au1ProfileName,
             (sizeof
              (wsscfgFsDot11AuthenticationProfileEntry.MibObject.
               au1FsDot11AuthenticationProfileName) - 1));
    wsscfgFsDot11AuthenticationProfileEntry.MibObject.
        i4FsDot11AuthenticationProfileNameLen = (INT4) STRLEN (au1ProfileName);

    wsscfgIsSetFsDot11AuthenticationProfileEntry.
        bFsDot11AuthenticationAlgorithm = OSIX_TRUE;
    wsscfgIsSetFsDot11AuthenticationProfileEntry.bFsDot11WepKeyIndex =
        OSIX_TRUE;
    wsscfgIsSetFsDot11AuthenticationProfileEntry.bFsDot11WepKeyType = OSIX_TRUE;
    if (STRLEN (au1Key))
    {
        wsscfgIsSetFsDot11AuthenticationProfileEntry.bFsDot11WepKey = OSIX_TRUE;
    }
    wsscfgIsSetFsDot11AuthenticationProfileEntry.bFsDot11WepKeyLength =
        OSIX_TRUE;
    wsscfgIsSetFsDot11AuthenticationProfileEntry.
        bFsDot11AuthenticationProfileName = OSIX_TRUE;
    wsscfgIsSetFsDot11AuthenticationProfileEntry.bFsDot11WebAuthentication =
        OSIX_TRUE;
    wsscfgIsSetFsDot11AuthenticationProfileEntry.
        bFsDot11AuthenticationRowStatus = OSIX_TRUE;

    if (wsscfgFsDot11AuthenticationProfileEntry.MibObject.
        i4FsDot11AuthenticationAlgorithm == CLI_AUTH_ALGO_OPEN)
    {
        wsscfgIsSetFsDot11AuthenticationProfileEntry.bFsDot11WepKeyIndex =
            OSIX_FALSE;
        wsscfgIsSetFsDot11AuthenticationProfileEntry.bFsDot11WepKeyType =
            OSIX_FALSE;
        wsscfgIsSetFsDot11AuthenticationProfileEntry.bFsDot11WepKeyLength =
            OSIX_FALSE;
        wsscfgIsSetFsDot11AuthenticationProfileEntry.
            bFsDot11AuthenticationRowStatus = OSIX_FALSE;
    }

    if (WsscfgTestAllFsDot11AuthenticationProfileTable
        (&u4ErrorCode, &wsscfgFsDot11AuthenticationProfileEntry,
         &wsscfgIsSetFsDot11AuthenticationProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        return;
    }

    if (WsscfgSetAllFsDot11AuthenticationProfileTable
        (&wsscfgFsDot11AuthenticationProfileEntry,
         &wsscfgIsSetFsDot11AuthenticationProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        return;
    }
    WebnmUnRegisterLock (pHttp);
    WSSCFG_UNLOCK;
    IssProcessAuthenticationProfileCreatePageGet (pHttp);
    return;
}

/*********************************************************************
*  Function Name : IssProcessAuthenticationProfileEditPage
*  Description   : This function processes the request coming for the 
*                  Authentication Profile Entry Edit Page 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessAuthenticationProfileEditPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessAuthenticationProfileEditPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessAuthenticationProfileEditPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessAuthenticationProfileEditPageGet 
*  Description   : This function processes the get request coming for the  
*                  Authentication Profile  Entry Edit page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessAuthenticationProfileEditPageGet (tHttp * pHttp)
{
    INT4                i4Authentication = 0, i4WepKeyLength = 0;
    INT4                i4WepKeyIndex = 0, i4WepKeyType = 0, i4WebAuth = 0;
    tSNMP_OCTET_STRING_TYPE *pProfileName = NULL;
    tWsscfgFsDot11AuthenticationProfileEntry
        wsscfgFsDot11AuthenticationProfileEntry;

    MEMSET (&wsscfgFsDot11AuthenticationProfileEntry, 0,
            sizeof (tWsscfgFsDot11AuthenticationProfileEntry));
    pProfileName =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (pProfileName == NULL)
    {
        WSS_WEB_TRC
            ("IssProcessAuthenticationProfileEditPageGet failed due to memory allocation\n");
        return;
    }
    MEMSET (pProfileName->pu1_OctetList, 0, OCTETSTR_SIZE);

    pProfileName->i4_Length = (INT4) STRLEN (gAuthProfileName);
    STRNCPY (pProfileName->pu1_OctetList, gAuthProfileName,
             pProfileName->i4_Length);

    wsscfgFsDot11AuthenticationProfileEntry.MibObject.
        i4FsDot11AuthenticationProfileNameLen = pProfileName->i4_Length;
    MEMCPY (wsscfgFsDot11AuthenticationProfileEntry.MibObject.
            au1FsDot11AuthenticationProfileName, pProfileName->pu1_OctetList,
            MEM_MAX_BYTES (sizeof
                           (wsscfgFsDot11AuthenticationProfileEntry.MibObject.
                            au1FsDot11AuthenticationProfileName),
                           (UINT4) pProfileName->i4_Length));

    if (WsscfgGetAllFsDot11AuthenticationProfileTable
        (&wsscfgFsDot11AuthenticationProfileEntry) == OSIX_SUCCESS)
    {
        i4Authentication =
            wsscfgFsDot11AuthenticationProfileEntry.MibObject.
            i4FsDot11AuthenticationAlgorithm;
        i4WebAuth =
            wsscfgFsDot11AuthenticationProfileEntry.MibObject.
            i4FsDot11WebAuthentication;
        i4WepKeyType =
            wsscfgFsDot11AuthenticationProfileEntry.MibObject.
            i4FsDot11WepKeyType;
        i4WepKeyLength =
            wsscfgFsDot11AuthenticationProfileEntry.MibObject.
            i4FsDot11WepKeyLength;
        i4WepKeyIndex =
            wsscfgFsDot11AuthenticationProfileEntry.MibObject.
            i4FsDot11WepKeyIndex;
    }

    WSS_WEB_TRC (" i4Authentication :%d \n", i4Authentication);
    WSS_WEB_TRC (" i4WebAuth :%d \n", i4WebAuth);
    WSS_WEB_TRC (" i4WepKeyType: %d \n", i4WepKeyType);
    WSS_WEB_TRC (" i4WepKeyLength : %d \n", i4WepKeyLength);
    WSS_WEB_TRC (" i4WepKeyIndex : %d \n", i4WepKeyIndex);

    WebnmRegisterLock (pHttp, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;
    STRCPY (pHttp->au1KeyString, "AUTH_PROFILE_NAME");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pProfileName->pu1_OctetList);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "AuthAlgoKey");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Authentication);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "WebAuthKey");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4WebAuth);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "KeyIndexKey");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4WepKeyIndex);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "KeyTypeKey");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4WepKeyType);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "KeyLengthKey");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4WepKeyLength);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "KEY_VALUE");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "");
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    WebnmUnRegisterLock (pHttp);
    WSSCFG_UNLOCK;
    free_octetstring (pProfileName);
}

/*********************************************************************
*  Function Name : IssProcessAuthenticationProfileEditPageSet 
*  Description   : This function processes the set request coming for the  
*                  Authentication Profile Entry Edit page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessAuthenticationProfileEditPageSet (tHttp * pHttp)
{

    tWsscfgFsDot11AuthenticationProfileEntry
        wsscfgFsDot11AuthenticationProfileEntry;
    tWsscfgIsSetFsDot11AuthenticationProfileEntry
        wsscfgIsSetFsDot11AuthenticationProfileEntry;
    tWsscfgFsDot11WlanCapabilityProfileEntry
        wsscfgFsDot11WlanCapabilityProfileEntry;
    INT4                i4Security = 0, i4KeyIndex = 0, i4KeyLength =
        0, i4KeyType = 0, i4WebAuth = 0;
    UINT1               au1Key[13];
    UINT1               au1ProfileName[OCTETSTR_SIZE];
    UINT4               u4ErrorCode = 0;

    MEMSET (&wsscfgFsDot11AuthenticationProfileEntry, 0,
            sizeof (tWsscfgFsDot11AuthenticationProfileEntry));
    MEMSET (&wsscfgIsSetFsDot11AuthenticationProfileEntry, 0,
            sizeof (tWsscfgIsSetFsDot11AuthenticationProfileEntry));
    MEMSET (&wsscfgFsDot11WlanCapabilityProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanCapabilityProfileEntry));
    MEMSET (au1Key, 0, 13);
    MEMSET (au1ProfileName, 0, sizeof (au1ProfileName));

    WebnmRegisterLock (pHttp, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;

    STRCPY (pHttp->au1Name, "ProfileName");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ProfileName, pHttp->au1Value, (sizeof (au1ProfileName) - 1));

    STRCPY (pHttp->au1Name, "AuthAlgo");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Security = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "WebAuth");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4WebAuth = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "KeyIndex");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4KeyIndex = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "KeyType");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4KeyType = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "KeyLength");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4KeyLength = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "KeyValue");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1Key, pHttp->au1Value, (sizeof (au1Key) - 1));

    WSS_WEB_TRC ("Values from  Security Table\n");
    WSS_WEB_TRC ("*************************\n");

    WSS_WEB_TRC ("Security       : %d\n", i4Security);
    WSS_WEB_TRC ("WebAuth        : %d\n", i4WebAuth);
    WSS_WEB_TRC ("KeyIndex       : %d\n", i4KeyIndex);
    WSS_WEB_TRC ("Keyi4KeyType   : %d\n", i4KeyType);
    WSS_WEB_TRC ("KeyLength      : %d\n", i4KeyLength);
    WSS_WEB_TRC ("au1ProfileName : %s\n", gAuthProfileName);

    wsscfgFsDot11AuthenticationProfileEntry.MibObject.
        i4FsDot11AuthenticationAlgorithm = i4Security;
    wsscfgFsDot11AuthenticationProfileEntry.MibObject.
        i4FsDot11WebAuthentication = i4WebAuth;
    wsscfgFsDot11AuthenticationProfileEntry.MibObject.i4FsDot11WepKeyIndex =
        i4KeyIndex;
    wsscfgFsDot11AuthenticationProfileEntry.MibObject.i4FsDot11WepKeyType =
        i4KeyType;
    wsscfgFsDot11AuthenticationProfileEntry.MibObject.i4FsDot11WepKeyLength =
        i4KeyLength;
    if (STRLEN (au1Key))
    {
        STRNCPY (wsscfgFsDot11AuthenticationProfileEntry.MibObject.
                 au1FsDot11WepKey, au1Key, STRLEN (au1Key));
        wsscfgFsDot11AuthenticationProfileEntry.MibObject.i4FsDot11WepKeyLen =
            (INT4) STRLEN (au1Key);
    }
    wsscfgFsDot11AuthenticationProfileEntry.MibObject.
        i4FsDot11AuthenticationRowStatus = ACTIVE;
    STRNCPY (wsscfgFsDot11AuthenticationProfileEntry.MibObject.
             au1FsDot11AuthenticationProfileName, gAuthProfileName,
             (sizeof
              (wsscfgFsDot11AuthenticationProfileEntry.MibObject.
               au1FsDot11AuthenticationProfileName) - 1));
    wsscfgFsDot11AuthenticationProfileEntry.MibObject.
        i4FsDot11AuthenticationProfileNameLen =
        (INT4) STRLEN (gAuthProfileName);

    wsscfgIsSetFsDot11AuthenticationProfileEntry.
        bFsDot11AuthenticationAlgorithm = OSIX_TRUE;
    wsscfgIsSetFsDot11AuthenticationProfileEntry.bFsDot11WepKeyIndex =
        OSIX_TRUE;
    wsscfgIsSetFsDot11AuthenticationProfileEntry.bFsDot11WepKeyType = OSIX_TRUE;
    wsscfgIsSetFsDot11AuthenticationProfileEntry.bFsDot11WepKeyLength =
        OSIX_TRUE;
    if (STRLEN (au1Key))
    {
        wsscfgIsSetFsDot11AuthenticationProfileEntry.bFsDot11WepKey = OSIX_TRUE;
    }
    wsscfgIsSetFsDot11AuthenticationProfileEntry.
        bFsDot11AuthenticationProfileName = OSIX_TRUE;
    wsscfgIsSetFsDot11AuthenticationProfileEntry.bFsDot11WebAuthentication =
        OSIX_TRUE;
    wsscfgIsSetFsDot11AuthenticationProfileEntry.
        bFsDot11AuthenticationRowStatus = OSIX_TRUE;

    if (wsscfgFsDot11AuthenticationProfileEntry.MibObject.
        i4FsDot11AuthenticationAlgorithm == CLI_AUTH_ALGO_OPEN)
    {
        wsscfgIsSetFsDot11AuthenticationProfileEntry.bFsDot11WepKeyIndex =
            OSIX_FALSE;
        wsscfgIsSetFsDot11AuthenticationProfileEntry.bFsDot11WepKeyType =
            OSIX_FALSE;
        wsscfgIsSetFsDot11AuthenticationProfileEntry.bFsDot11WepKeyLength =
            OSIX_FALSE;
        wsscfgIsSetFsDot11AuthenticationProfileEntry.
            bFsDot11AuthenticationRowStatus = OSIX_FALSE;
    }

    if (WsscfgTestAllFsDot11AuthenticationProfileTable
        (&u4ErrorCode, &wsscfgFsDot11AuthenticationProfileEntry,
         &wsscfgIsSetFsDot11AuthenticationProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        return;
    }

    if (WsscfgSetAllFsDot11AuthenticationProfileTable
        (&wsscfgFsDot11AuthenticationProfileEntry,
         &wsscfgIsSetFsDot11AuthenticationProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        return;
    }
    WebnmUnRegisterLock (pHttp);
    WSSCFG_UNLOCK;
    IssProcessAuthenticationProfileEditPageGet (pHttp);
    return;
}

/*********************************************************************
*  Function Name : IssProcessQosProfilePage 
*  Description   : This function processes the request coming for the 
*                  Qos Profile Table Page 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessQosProfilePage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessQosProfilePageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessQosProfilePageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessQosProfilePageGet 
*  Description   : This function processes the get request coming for the  
*                  Qos  Profile Table Entry page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessQosProfilePageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE *pCurrentProfileName = NULL, *pNextProfileName =
        NULL;
    tSNMP_MULTI_DATA_TYPE *pu1DataString = NULL;
    UINT4               u4Temp = 0;
    INT4                i4OutCome = 0;

    if ((pu1DataString = WebnmAllocMultiData ()) == NULL)
    {
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        return;
    }

    pCurrentProfileName =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (pCurrentProfileName == NULL)
    {
        WSS_WEB_TRC
            ("IssProcessQosProfilePageset failed due to memory allocation\n");
        return;
    }
    pNextProfileName =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (pNextProfileName == NULL)
    {
        WSS_WEB_TRC
            ("IssProcessQosProfilePageset failed due to memory allocation\n");
        free_octetstring (pCurrentProfileName);
        return;
    }

    MEMSET (pCurrentProfileName->pu1_OctetList, 0, OCTETSTR_SIZE);
    pCurrentProfileName->i4_Length = 0;
    MEMSET (pNextProfileName->pu1_OctetList, 0, OCTETSTR_SIZE);
    pNextProfileName->i4_Length = 0;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;
    i4OutCome = nmhGetFirstIndexFsDot11QosProfileTable (pNextProfileName);
    WebnmRegisterLock (pHttp, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;

    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *)
                        (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        free_octetstring (pNextProfileName);
        free_octetstring (pCurrentProfileName);
        return;
    }
    else
    {
        do
        {
            pHttp->i4Write = (INT4) u4Temp;

            STRCPY (pu1DataString->pOctetStrValue->pu1_OctetList,
                    pNextProfileName->pu1_OctetList);
            pu1DataString->pOctetStrValue->i4_Length =
                pNextProfileName->i4_Length;

            STRCPY (pHttp->au1KeyString, "QOS_NAME");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            WebnmSendToSocket (pHttp, "QOS_NAME", pu1DataString,
                               ENM_DISPLAYSTRING);

            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

            /* Copy next index to current index */
            STRCPY (pCurrentProfileName->pu1_OctetList,
                    pNextProfileName->pu1_OctetList);
            pCurrentProfileName->i4_Length = pNextProfileName->i4_Length;
        }
        while (nmhGetNextIndexFsDot11QosProfileTable
               (pCurrentProfileName, pNextProfileName) == SNMP_SUCCESS);

        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));

    }
    WebnmUnRegisterLock (pHttp);
    WSSCFG_UNLOCK;
    free_octetstring (pNextProfileName);
    free_octetstring (pCurrentProfileName);
}

/*********************************************************************
*  Function Name : IssProcessQosProfilePageSet 
*  Description   : This function processes the set request coming for the  
*                  Qos  Profile Table Entry page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessQosProfilePageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE *QosName;
    UINT1               au1ProfileName[OCTETSTR_SIZE];

    QosName = (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (QosName == NULL)
    {
        WSS_WEB_TRC
            ("IssProcessQosProfilePageGet failed due to memory allocation\n");
        return;
    }
    MEMSET (QosName->pu1_OctetList, 0, OCTETSTR_SIZE);
    MEMSET (&au1ProfileName, 0, OCTETSTR_SIZE);

    WebnmRegisterLock (pHttp, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;

    STRCPY (pHttp->au1Name, "PROFILE_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    MEMCPY (au1ProfileName, pHttp->au1Value, sizeof (au1ProfileName));

    au1ProfileName[sizeof (au1ProfileName) - 1] = '\0';

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    /*WSS_WEB_TRC("Profile Name : %s\n",au1ProfileName);
       WSS_WEB_TRC("Action : %s\n",pHttp->au1Value); */
    if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {
        WSS_WEB_TRC ("Deleting Profile Entry\n");

        QosName->i4_Length = (INT4) STRLEN (au1ProfileName);
        STRNCPY (QosName->pu1_OctetList, au1ProfileName, QosName->i4_Length);

        if (STRCMP (au1ProfileName, "default") == 0)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Default Profile Cannot Be Deleted\n");
            WebnmUnRegisterLock (pHttp);
            WSSCFG_UNLOCK;
            free_octetstring (QosName);
            return;
        }

        if (nmhSetFsDot11QosRowStatus (QosName, DESTROY) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
            WebnmUnRegisterLock (pHttp);
            WSSCFG_UNLOCK;
            free_octetstring (QosName);
            return;
        }
    }
    else if (STRCMP (pHttp->au1Value, "Edit") == 0)
    {
        MEMCPY (gQosProfileName, au1ProfileName, sizeof (au1ProfileName));
    }
    else
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Operation\n");
    }
    WebnmUnRegisterLock (pHttp);
    WSSCFG_UNLOCK;
    free_octetstring (QosName);
    IssProcessQosProfilePageGet (pHttp);
    return;
}

/*********************************************************************
*  Function Name : IssProcessQosProfileCreatePage 
*  Description   : This function processes the request coming for the 
*                  Qos  Profile Entry Page 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessQosProfileCreatePage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessQosProfileCreatePageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessQosProfileCreatePageSet 
*  Description   : This function processes the set request coming for the  
*                  Qos  Profile Entry page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessQosProfileCreatePageSet (tHttp * pHttp)
{

    tWsscfgFsDot11QosProfileEntry wsscfgFsDot11QosProfileEntry;
    tWsscfgIsSetFsDot11QosProfileEntry wsscfgIsSetFsDot11QosProfileEntry;
    UINT1               au1ProfileName[VLAN_STATIC_MAX_NAME_LEN + 1];
    INT4                i4Qos = 0, i4Trust = 0, i4RateLimit = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4UpstreamCIR = 0, i4UpstreamEIR = 0, i4UpstreamCBS =
        0, i4UpstreamEBS = 0;
    INT4                i4DownstreamCIR = 0, i4DownstreamEIR =
        0, i4DownstreamCBS = 0, i4DownstreamEBS = 0;

    MEMSET (&au1ProfileName, 0, VLAN_STATIC_MAX_NAME_LEN + 1);
    STRNCPY (au1ProfileName, gQosProfileName, (sizeof (au1ProfileName) - 1));

    WebnmRegisterLock (pHttp, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;

    STRCPY (pHttp->au1Name, "QosName");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ProfileName, pHttp->au1Value, (sizeof (au1ProfileName) - 1));

    STRCPY (pHttp->au1Name, "QoSTraffic");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Qos = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "QosTrustMode");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Trust = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "QosRateLimit");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4RateLimit = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "UpCmtInfoRate");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4UpstreamCIR = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "UpCmtBstSize");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4UpstreamCBS = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "UpExInfoRate");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4UpstreamEIR = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "UpExBstSize");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4UpstreamEBS = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "DownCmtInfoRate");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4DownstreamCIR = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "DownCmtBstSize");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4DownstreamCBS = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "DownExInfoRate");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4DownstreamEIR = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "DownExBstSize");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4DownstreamEBS = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    WSS_WEB_TRC ("Values from  Qos Profile Table\n");
    WSS_WEB_TRC ("*********************************\n");

    WSS_WEB_TRC ("QosProfile Name %s\n", au1ProfileName);
    WSS_WEB_TRC ("Qos : %d\n", i4Qos);
    WSS_WEB_TRC ("RateLimit : %d\n", i4RateLimit);
    WSS_WEB_TRC ("Trust: %d\n", i4Trust);
    WSS_WEB_TRC ("UpCmtInfoRate : %d\n", i4UpstreamCIR);
    WSS_WEB_TRC ("UpCmtBstSize : %d\n", i4UpstreamCBS);
    WSS_WEB_TRC ("UpExInfoRate : %d\n", i4UpstreamEIR);
    WSS_WEB_TRC ("UpExBstSize : %d\n", i4UpstreamEBS);
    WSS_WEB_TRC ("DownCmtInfoRate : %d\n", i4DownstreamCIR);
    WSS_WEB_TRC ("DownCmtBstSize : %d\n", i4DownstreamCBS);
    WSS_WEB_TRC ("DownExInfoRate : %d\n", i4DownstreamEIR);
    WSS_WEB_TRC ("DownExBstSize : %d\n", i4DownstreamEBS);

    STRNCPY (wsscfgFsDot11QosProfileEntry.MibObject.au1FsDot11QosProfileName,
             au1ProfileName,
             (sizeof
              (wsscfgFsDot11QosProfileEntry.MibObject.
               au1FsDot11QosProfileName) - 1));
    wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11QosProfileNameLen =
        (INT4) STRLEN (au1ProfileName);
    wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11QosTraffic = i4Qos;
    wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11QosPassengerTrustMode =
        i4Trust;
    wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11QosRowStatus =
        CREATE_AND_GO;

    wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11QosRateLimit = i4RateLimit;
    wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11UpStreamCIR = i4UpstreamCIR;
    wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11UpStreamCBS = i4UpstreamCBS;
    wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11UpStreamEIR = i4UpstreamEIR;
    wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11UpStreamEBS = i4UpstreamEBS;
    wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11DownStreamCIR =
        i4DownstreamCIR;
    wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11DownStreamCBS =
        i4DownstreamCBS;
    wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11DownStreamEIR =
        i4DownstreamEIR;
    wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11DownStreamEBS =
        i4DownstreamEBS;

    wsscfgIsSetFsDot11QosProfileEntry.bFsDot11QosProfileName = OSIX_TRUE;
    wsscfgIsSetFsDot11QosProfileEntry.bFsDot11QosTraffic = OSIX_TRUE;
    wsscfgIsSetFsDot11QosProfileEntry.bFsDot11QosPassengerTrustMode = OSIX_TRUE;
    wsscfgIsSetFsDot11QosProfileEntry.bFsDot11QosRowStatus = OSIX_TRUE;
    wsscfgIsSetFsDot11QosProfileEntry.bFsDot11QosRateLimit = OSIX_TRUE;
    wsscfgIsSetFsDot11QosProfileEntry.bFsDot11UpStreamCIR = OSIX_TRUE;
    wsscfgIsSetFsDot11QosProfileEntry.bFsDot11UpStreamCBS = OSIX_TRUE;
    wsscfgIsSetFsDot11QosProfileEntry.bFsDot11UpStreamEIR = OSIX_TRUE;
    wsscfgIsSetFsDot11QosProfileEntry.bFsDot11UpStreamEBS = OSIX_TRUE;
    wsscfgIsSetFsDot11QosProfileEntry.bFsDot11DownStreamCIR = OSIX_TRUE;
    wsscfgIsSetFsDot11QosProfileEntry.bFsDot11DownStreamCBS = OSIX_TRUE;
    wsscfgIsSetFsDot11QosProfileEntry.bFsDot11DownStreamEIR = OSIX_TRUE;
    wsscfgIsSetFsDot11QosProfileEntry.bFsDot11DownStreamEBS = OSIX_TRUE;

    if (WsscfgTestAllFsDot11QosProfileTable
        (&u4ErrorCode, &wsscfgFsDot11QosProfileEntry,
         &wsscfgIsSetFsDot11QosProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        return;
    }

    if (WsscfgSetAllFsDot11QosProfileTable
        (&wsscfgFsDot11QosProfileEntry, &wsscfgIsSetFsDot11QosProfileEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        return;
    }
    WebnmUnRegisterLock (pHttp);
    WSSCFG_UNLOCK;
    return;

}

/*********************************************************************
*  Function Name : IssProcessQosProfileEditPage 
*  Description   : This function processes the request coming for the 
*                  Qos Profile Edit Page 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessQosProfileEditPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessQosProfileEditPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessQosProfileEditPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessQosProfileEditPageGet 
*  Description   : This function processes the get request coming for the  
*                  Qos Profile Entry Eidt page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessQosProfileEditPageGet (tHttp * pHttp)
{
    INT4                i4UpstreamCIR = 0, i4UpstreamEIR = 0, i4UpstreamCBS =
        0, i4UpstreamEBS = 0;
    INT4                i4DownstreamCIR = 0, i4DownstreamEIR =
        0, i4DownstreamCBS = 0, i4DownstreamEBS = 0;
    INT4                i4QosTraffic = 0, i4QosTrustMode = 0, i4QosRateLimit =
        0;
    tWsscfgFsDot11QosProfileEntry wsscfgFsDot11QosProfileEntry;
    tSNMP_OCTET_STRING_TYPE *QosProfileName = NULL;
    MEMSET (&wsscfgFsDot11QosProfileEntry, 0,
            sizeof (tWsscfgFsDot11QosProfileEntry));

    QosProfileName =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (QosProfileName == NULL)
    {
        WSS_WEB_TRC
            ("IssProcessQosProfileEditPageGet failed due to memory allocation\n");
        return;
    }
    MEMSET (QosProfileName->pu1_OctetList, 0, OCTETSTR_SIZE);

    QosProfileName->i4_Length = (INT4) (STRLEN (gQosProfileName));
    MEMCPY (QosProfileName->pu1_OctetList, gQosProfileName,
            MEM_MAX_BYTES (OCTETSTR_SIZE, STRLEN (gQosProfileName)));

    wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11QosProfileNameLen =
        QosProfileName->i4_Length;
    /* Set the Index */
    MEMCPY (wsscfgFsDot11QosProfileEntry.MibObject.au1FsDot11QosProfileName,
            QosProfileName->pu1_OctetList,
            MEM_MAX_BYTES ((UINT4) wsscfgFsDot11QosProfileEntry.MibObject.
                           i4FsDot11QosProfileNameLen,
                           sizeof (wsscfgFsDot11QosProfileEntry.MibObject.
                                   au1FsDot11QosProfileName)));

    if (WsscfgGetAllFsDot11QosProfileTable (&wsscfgFsDot11QosProfileEntry) !=
        OSIX_SUCCESS)
    {
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        free_octetstring (QosProfileName);
        return;
    }
    i4QosTraffic = wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11QosTraffic;
    i4QosTrustMode =
        wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11QosPassengerTrustMode;
    i4QosRateLimit =
        wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11QosRateLimit;
    i4UpstreamCIR = wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11UpStreamCIR;
    i4UpstreamCBS = wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11UpStreamCBS;
    i4UpstreamEIR = wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11UpStreamEIR;
    i4UpstreamEBS = wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11UpStreamEBS;
    i4DownstreamCIR =
        wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11DownStreamCIR;
    i4DownstreamCBS =
        wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11DownStreamCBS;
    i4DownstreamEIR =
        wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11DownStreamEIR;
    i4DownstreamEBS =
        wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11DownStreamEBS;
    WebnmRegisterLock (pHttp, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;

    STRCPY (pHttp->au1KeyString, "QOS_PROFILE_NAME");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    STRNCPY ((CHR1 *) pHttp->au1DataString, QosProfileName->pu1_OctetList,
             QosProfileName->i4_Length);
    pHttp->au1DataString[QosProfileName->i4_Length] = '\0';
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "QOS_TRAFFIC_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4QosTraffic);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "QOS_TRUST_MODE_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4QosTrustMode);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "QOS_RATE_LIMIT_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4QosRateLimit);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, (UINT1 *) "UPSTREAM_CIR_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UpstreamCIR);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, (UINT1 *) "UPSTREAM_CBS_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UpstreamCBS);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, (UINT1 *) "UPSTREAM_EIR_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UpstreamEIR);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, (UINT1 *) "UPSTREAM_EBS_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UpstreamEBS);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, (UINT1 *) "DOWNSTREAM_CIR_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4DownstreamCIR);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, (UINT1 *) "DOWNSTREAM_CBS_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4DownstreamCBS);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));
    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, (UINT1 *) "DOWNSTREAM_EIR_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4DownstreamEIR);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, (UINT1 *) "DOWNSTREAM_EBS_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4DownstreamEBS);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    WebnmUnRegisterLock (pHttp);
    WSSCFG_UNLOCK;
    free_octetstring (QosProfileName);

}

/*********************************************************************
*  Function Name : IssProcessQosProfileEditPageSet 
*  Description   : This function processes the set request coming for the  
*                  Qos Profile Entry Edit page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessQosProfileEditPageSet (tHttp * pHttp)
{

    tWsscfgFsDot11QosProfileEntry wsscfgFsDot11QosProfileEntry;
    tWsscfgIsSetFsDot11QosProfileEntry wsscfgIsSetFsDot11QosProfileEntry;
    UINT1               au1ProfileName[VLAN_STATIC_MAX_NAME_LEN + 1];
    INT4                i4Qos = 0, i4Trust, i4RateLimit = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4UpstreamCIR = 0, i4UpstreamEIR = 0, i4UpstreamCBS =
        0, i4UpstreamEBS = 0;
    INT4                i4DownstreamCIR = 0, i4DownstreamEIR =
        0, i4DownstreamCBS = 0, i4DownstreamEBS = 0;

    MEMSET (&wsscfgFsDot11QosProfileEntry, 0,
            sizeof (tWsscfgFsDot11QosProfileEntry));
    MEMSET (&wsscfgIsSetFsDot11QosProfileEntry, 0,
            sizeof (tWsscfgIsSetFsDot11QosProfileEntry));
    MEMSET (&au1ProfileName, 0, VLAN_STATIC_MAX_NAME_LEN + 1);

    WebnmRegisterLock (pHttp, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;

    STRCPY (pHttp->au1Name, "ProfileName");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ProfileName, pHttp->au1Value, (sizeof (au1ProfileName) - 1));

    STRCPY (pHttp->au1Name, "QoSTraffic");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Qos = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "QosTrustMode");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Trust = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "QosRateLimit");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4RateLimit = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "UpCmtInfoRate");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4UpstreamCIR = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "UpCmtBstSize");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4UpstreamCBS = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "UpExInfoRate");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4UpstreamEIR = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "UpExBstSize");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4UpstreamEBS = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "DownCmtInfoRate");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4DownstreamCIR = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "DownCmtBstSize");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4DownstreamCBS = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "DownExInfoRate");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4DownstreamEIR = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "DownExBstSize");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4DownstreamEBS = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    WSS_WEB_TRC ("Values from  Qos Profile Table\n");
    WSS_WEB_TRC ("*********************************\n");

    WSS_WEB_TRC ("QosProfile Name %s\n", au1ProfileName);
    WSS_WEB_TRC ("QosTraffic : %d\n", i4Qos);
    WSS_WEB_TRC ("RateLimit : %d\n", i4RateLimit);
    WSS_WEB_TRC ("Trust: %d\n", i4Trust);
    WSS_WEB_TRC ("UpCmtInfoRate : %d\n", i4UpstreamCIR);
    WSS_WEB_TRC ("UpCmtBstSize : %d\n", i4UpstreamCBS);
    WSS_WEB_TRC ("UpExInfoRate : %d\n", i4UpstreamEIR);
    WSS_WEB_TRC ("UpExBstSize : %d\n", i4UpstreamEBS);
    WSS_WEB_TRC ("DownCmtInfoRate : %d\n", i4DownstreamCIR);
    WSS_WEB_TRC ("DownCmtBstSize : %d\n", i4DownstreamCBS);
    WSS_WEB_TRC ("DownExInfoRate : %d\n", i4DownstreamEIR);
    WSS_WEB_TRC ("DownExBstSize : %d\n", i4DownstreamEBS);
    MEMSET (&wsscfgFsDot11QosProfileEntry, 0,
            sizeof (tWsscfgFsDot11QosProfileEntry));
    MEMSET (&wsscfgIsSetFsDot11QosProfileEntry, 0,
            sizeof (tWsscfgIsSetFsDot11QosProfileEntry));

    wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11QosTraffic = i4Qos;
    wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11QosPassengerTrustMode =
        i4Trust;
    wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11QosRowStatus = ACTIVE;

    wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11QosRateLimit = i4RateLimit;
    wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11UpStreamCIR = i4UpstreamCIR;
    wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11UpStreamCBS = i4UpstreamCBS;
    wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11UpStreamEIR = i4UpstreamEIR;
    wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11UpStreamEBS = i4UpstreamEBS;
    wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11DownStreamCIR =
        i4DownstreamCIR;
    wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11DownStreamCBS =
        i4DownstreamCBS;
    wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11DownStreamEIR =
        i4DownstreamEIR;
    wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11DownStreamEBS =
        i4DownstreamEBS;
    STRNCPY (wsscfgFsDot11QosProfileEntry.MibObject.au1FsDot11QosProfileName,
             au1ProfileName,
             (sizeof
              (wsscfgFsDot11QosProfileEntry.MibObject.
               au1FsDot11QosProfileName)));
    wsscfgFsDot11QosProfileEntry.MibObject.i4FsDot11QosProfileNameLen =
        (INT4) STRLEN (au1ProfileName);

    wsscfgIsSetFsDot11QosProfileEntry.bFsDot11QosProfileName = OSIX_TRUE;
    wsscfgIsSetFsDot11QosProfileEntry.bFsDot11QosTraffic = OSIX_TRUE;
    wsscfgIsSetFsDot11QosProfileEntry.bFsDot11QosPassengerTrustMode = OSIX_TRUE;
    wsscfgIsSetFsDot11QosProfileEntry.bFsDot11QosRowStatus = OSIX_TRUE;
    wsscfgIsSetFsDot11QosProfileEntry.bFsDot11QosRateLimit = OSIX_TRUE;
    wsscfgIsSetFsDot11QosProfileEntry.bFsDot11UpStreamCIR = OSIX_TRUE;
    wsscfgIsSetFsDot11QosProfileEntry.bFsDot11UpStreamCBS = OSIX_TRUE;
    wsscfgIsSetFsDot11QosProfileEntry.bFsDot11UpStreamEIR = OSIX_TRUE;
    wsscfgIsSetFsDot11QosProfileEntry.bFsDot11UpStreamEBS = OSIX_TRUE;
    wsscfgIsSetFsDot11QosProfileEntry.bFsDot11DownStreamCIR = OSIX_TRUE;
    wsscfgIsSetFsDot11QosProfileEntry.bFsDot11DownStreamCBS = OSIX_TRUE;
    wsscfgIsSetFsDot11QosProfileEntry.bFsDot11DownStreamEIR = OSIX_TRUE;
    wsscfgIsSetFsDot11QosProfileEntry.bFsDot11DownStreamEBS = OSIX_TRUE;

    if (WsscfgTestAllFsDot11QosProfileTable
        (&u4ErrorCode, &wsscfgFsDot11QosProfileEntry,
         &wsscfgIsSetFsDot11QosProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        return;
    }

    if (WsscfgSetAllFsDot11QosProfileTable
        (&wsscfgFsDot11QosProfileEntry, &wsscfgIsSetFsDot11QosProfileEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        WSSCFG_UNLOCK;
        return;
    }

    WebnmUnRegisterLock (pHttp);
    WSSCFG_UNLOCK;
    IssProcessQosProfileEditPageGet (pHttp);
    return;
}

/*********************************************************************
 * *  Function Name : IssProcessMcastSettingsPage
 * *  Description   : This function processes the request coming for the
 * *                  Multicast Settings Page
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/
VOID
IssProcessMcastSettingsPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessMcastSettingsPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessMcastSettingsPageSet (pHttp);
    }
}

/*********************************************************************
 * *  Function Name : IssProcessMcastSettingsPageGet
 * *  Description   : This function processes the get request coming for the
 * *                   Multicast Settings  page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/
VOID
IssProcessMcastSettingsPageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    UINT4               u4NextWlanProfileId = 0;
    UINT4               u4WlanProfileId = 0;
    UINT4               u4Timer = 0;
    UINT4               u4Timeout = 0;
    UINT4               u4SetValFsWlanMulticastSnoopTableLength = 0;
    INT4                i4MulticastMode = 0;
    INT4                i4OutCome = 0;

    i4OutCome = nmhGetFirstIndexFsWlanMulticastTable (&u4NextWlanProfileId);
    if (i4OutCome != SNMP_FAILURE)
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4Temp = (UINT4) pHttp->i4Write;

        do
        {

            pHttp->i4Write = (INT4) u4Temp;

            u4WlanProfileId = u4NextWlanProfileId;
            if (u4WlanProfileId != 0)
            {

                STRCPY (pHttp->au1KeyString, "SSID_KEY2");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4WlanProfileId);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "MCAST_KEY2");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsWlanMulticastMode (u4WlanProfileId, &i4MulticastMode);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4MulticastMode);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "MTABLE_KEY2");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsWlanMulticastSnoopTableLength (u4WlanProfileId,
                                                       &u4SetValFsWlanMulticastSnoopTableLength);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4SetValFsWlanMulticastSnoopTableLength);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "TIMER_KEY2");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsWlanMulticastSnoopTimer (u4WlanProfileId, &u4Timer);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4Timer);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "TIMEOUT_KEY2");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsWlanMulticastSnoopTimeout (u4WlanProfileId, &u4Timeout);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4Timeout);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

            }
        }
        while (nmhGetNextIndexFsWlanMulticastTable
               (u4WlanProfileId, &u4NextWlanProfileId) == SNMP_SUCCESS);
    }

    WebnmUnRegisterLock (pHttp);
    WebnmUnRegisterContext (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    return;
}

/*********************************************************************
 * *  Function Name : IssProcessMcastSettingsPageSet
 * *  Description   : This function processes the set request coming for the
 * *                   Multicast Settings page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/
VOID
IssProcessMcastSettingsPageSet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    UINT4               u4SSID = 0;
    INT4                i4McastMode = 0;
    UINT4               u4McastLen = 0;
    UINT4               u4Timer = 0;
    UINT4               u4Timeout = 0;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    pHttp->i4Write = (INT4) u4Temp;

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    /*
       if (STRCMP (pHttp->au1Value, "Add") == 0)
       {

       STRCPY(pHttp->au1Name,"SSID_NAME");
       HttpGetValuebyName(pHttp->au1Name,pHttp->au1Value,pHttp->au1PostQuery);
       u4SSID =  (UINT4)(ATOI (pHttp->au1Value));

       STRCPY(pHttp->au1Name,"MCAST_NAME");
       HttpGetValuebyName(pHttp->au1Name,pHttp->au1Value,pHttp->au1PostQuery);
       i4McastMode =  (INT4)(ATOI (pHttp->au1Value));
       nmhSetFsWlanMulticastMode(u4SSID, i4McastMode);          

       STRCPY(pHttp->au1Name,"MTABLE_NAME");
       HttpGetValuebyName(pHttp->au1Name,pHttp->au1Value,pHttp->au1PostQuery);
       u4McastLen =  (UINT4)(ATOI (pHttp->au1Value));
       if(u4McastLen != 0)
       {
       nmhSetFsWlanMulticastSnoopTableLength(u4SSID, u4McastLen);          
       }

       STRCPY(pHttp->au1Name,"TIMER");
       HttpGetValuebyName(pHttp->au1Name,pHttp->au1Value,pHttp->au1PostQuery);
       u4Timer =  (UINT4)(ATOI (pHttp->au1Value));
       if(u4Timer != 0)
       {
       nmhSetFsWlanMulticastSnoopTimer(u4SSID, u4Timer);          
       }

       STRCPY(pHttp->au1Name,"TIMEOUT");
       HttpGetValuebyName(pHttp->au1Name,pHttp->au1Value,pHttp->au1PostQuery);
       u4Timeout =  (UINT4)(ATOI (pHttp->au1Value));
       if(u4Timeout != 0)
       {
       nmhSetFsWlanMulticastSnoopTimeout(u4SSID, u4Timeout);          
       }

       IssProcessMcastSettingsPageGet (pHttp);
       return;
       }  
     */
    if (STRCMP (pHttp->au1Value, "Apply") == 0)
    {

        STRCPY (pHttp->au1Name, "SSID2");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4SSID = (UINT4) (ATOI (pHttp->au1Value));

        STRCPY (pHttp->au1Name, "MCAST_NAME");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4McastMode = (INT4) (ATOI (pHttp->au1Value));
        nmhSetFsWlanMulticastMode (u4SSID, i4McastMode);

        STRCPY (pHttp->au1Name, "MTABLE_NAME2");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4McastLen = (UINT4) (ATOI (pHttp->au1Value));
        nmhSetFsWlanMulticastSnoopTableLength (u4SSID, u4McastLen);

        STRCPY (pHttp->au1Name, "TIMER2");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Timer = (UINT4) (ATOI (pHttp->au1Value));
        nmhSetFsWlanMulticastSnoopTimer (u4SSID, u4Timer);

        STRCPY (pHttp->au1Name, "TIMEOUT2");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Timeout = (UINT4) (ATOI (pHttp->au1Value));
        nmhSetFsWlanMulticastSnoopTimeout (u4SSID, u4Timeout);

        IssProcessMcastSettingsPageGet (pHttp);
        return;
    }

}

#ifdef WPA_WANTED
/*********************************************************************
*  Function Name : IssProcessRsnaConfigPage
*  Description   : This function processes the request coming for the
*                  Rsna Config Table Page
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessWpaConfigPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessWpaConfigPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessWpaConfigPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessWpaConfigPageGet
*  Description   : This function processes the get request coming for the
*                  Wpa Config Table  page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessWpaConfigPageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    UINT4               u4currentWlanId = 0;
    UINT4               u4nextWlanId = 0;
    UINT4               u4AuthSuiteStatus = 0;
    UINT4               u4ProfileId = 0;
    INT4                i4OutCome = 0;
    INT4                i4RsnaEnabled = 0;
    INT4                i4PreauthenticationEnabled = 0;
    INT4                OKCEnabled = 0;
    UINT1               u1Psk[] = { 0x00, 0x50, 0xf2, 0x02 };
    UINT1               u1dot1x[] = { 0x00, 0x50, 0xf2, 0x01 };
    UINT1               u1Tkip[] = { 0x00, 0x50, 0xf2, 0x02 };
    tSNMP_OCTET_STRING_TYPE GroupCipher;
    tSNMP_OCTET_STRING_TYPE AuthSuiteSelected;
    tSNMP_OCTET_STRING_TYPE PairwiseCipherSelected;
    tSNMP_OCTET_STRING_TYPE GroupwiseCipherSelected;
    tSNMP_OCTET_STRING_TYPE PmkIdUsed;
    tSNMP_OCTET_STRING_TYPE AuthSuiteRequested;
    tSNMP_OCTET_STRING_TYPE PairwiseCipherRequested;
    tSNMP_OCTET_STRING_TYPE GroupwiseCipherRequested;
    UINT1               au1GroupCipher[RSNA_CIPHER_SUITE_LEN];
    UINT1               au1AuthSuiteSelected[RSNA_CIPHER_SUITE_LEN];
    UINT1               au1PairwiseCipherSelected[RSNA_CIPHER_SUITE_LEN];
    UINT1               au1GroupCipherSelected[RSNA_CIPHER_SUITE_LEN];
    UINT1               au1PMKIDUsed[RSNA_PMKID_DATA_LEN];
    UINT1               au1AuthSuiteRequested[RSNA_CIPHER_SUITE_LEN];
    UINT1               au1PairwiseCipherRequested[RSNA_CIPHER_SUITE_LEN];
    UINT1               au1GroupwiseCipherRequested[RSNA_CIPHER_SUITE_LEN];
    UINT4               u4ConfigVersion = 0;
    UINT4               u4PairwiseKeySupported = 1;
    UINT4               u4GroupRekeyMethod = 0;
    UINT4               u4GroupRekeyTime = 0;
    UINT4               u4GroupRekeyPkts = 0;
    UINT4               u4GroupRekeyStrict = 0;
    UINT4               u4GroupUpdateCount = 0;
    UINT4               u4PairwiseUpdateCount = 0;
    UINT4               u4GroupCipherSize = 0;
    UINT4               u4PMKLifetime = 0;
    UINT4               u4PMKReAuthThreshold = 0;
    UINT4               u4PTKSAReplayCounter = 0;
    UINT4               u4SATimeOut = 0;
    UINT4               u4TKIPCounterMeasuresInvoked = 0;
    UINT4               u4FourWayHandshakeFailures = 0;
    UINT4               u4GTKSAReplayCountersCount = 0;

    MEMSET (au1GroupCipher, '\0', sizeof (RSNA_CIPHER_SUITE_LEN));
    MEMSET (au1AuthSuiteSelected, '\0', (RSNA_CIPHER_SUITE_LEN));
    MEMSET (au1PairwiseCipherSelected, '\0', (RSNA_CIPHER_SUITE_LEN));
    MEMSET (au1GroupCipherSelected, '\0', (RSNA_CIPHER_SUITE_LEN));
    MEMSET (au1PMKIDUsed, '\0', (RSNA_CIPHER_SUITE_LEN));
    MEMSET (au1AuthSuiteRequested, '\0', (RSNA_CIPHER_SUITE_LEN));
    MEMSET (au1PairwiseCipherRequested, '\0', (RSNA_CIPHER_SUITE_LEN));
    MEMSET (au1GroupwiseCipherRequested, '\0', (RSNA_CIPHER_SUITE_LEN));

    GroupCipher.pu1_OctetList = au1GroupCipher;
    AuthSuiteSelected.pu1_OctetList = au1AuthSuiteSelected;
    PairwiseCipherSelected.pu1_OctetList = au1PairwiseCipherSelected;
    GroupwiseCipherSelected.pu1_OctetList = au1GroupCipherSelected;
    PmkIdUsed.pu1_OctetList = au1PMKIDUsed;
    AuthSuiteRequested.pu1_OctetList = au1AuthSuiteRequested;
    PairwiseCipherRequested.pu1_OctetList = au1PairwiseCipherRequested;
    GroupwiseCipherRequested.pu1_OctetList = au1GroupwiseCipherRequested;

    if (WebnmAllocMultiData () == NULL)

    {
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        return;
    }

    u4currentWlanId = 0;
    u4nextWlanId = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;
    WebnmRegisterLock (pHttp, PnacLock, PnacUnLock);
    PNAC_LOCK ();
    i4OutCome = nmhGetFirstIndexCapwapDot11WlanTable (&u4nextWlanId);
    if (i4OutCome == SNMP_FAILURE)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    do
    {

        pHttp->i4Write = (INT4) u4Temp;
        u4currentWlanId = u4nextWlanId;
        if (u4currentWlanId != 0)
        {
            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4currentWlanId, (INT4 *) &u4ProfileId) == SNMP_SUCCESS)
            {
                if (u4ProfileId != 0)
                {
                    nmhGetFsRSNAConfigActivated ((INT4) u4ProfileId,
                                                 &i4RsnaEnabled);

                    nmhGetFsRSNAOKCEnabled ((INT4) u4ProfileId, &OKCEnabled);

                    nmhGetFsRSNAConfigVersion ((INT4) u4ProfileId,
                                               &u4ConfigVersion);

                    nmhGetFsRSNAConfigGroupCipher ((INT4) u4ProfileId,
                                                   &GroupCipher);

                    nmhGetFsRSNAConfigGroupRekeyMethod ((INT4) u4ProfileId,
                                                        (INT4 *)
                                                        &u4GroupRekeyMethod);

                    nmhGetFsRSNAConfigGroupRekeyTime ((INT4) u4ProfileId,
                                                      &u4GroupRekeyTime);

                    nmhGetFsRSNAConfigGroupRekeyPackets ((INT4) u4ProfileId,
                                                         &u4GroupRekeyPkts);

                    nmhGetFsRSNAConfigGroupRekeyStrict ((INT4) u4ProfileId,
                                                        (INT4 *)
                                                        &u4GroupRekeyStrict);

                    nmhGetFsRSNAConfigPairwiseUpdateCount ((INT4) u4ProfileId,
                                                           &u4GroupUpdateCount);

                    nmhGetFsRSNAConfigPairwiseUpdateCount ((INT4)
                                                           u4ProfileId,
                                                           &u4PairwiseUpdateCount);

                    nmhGetFsRSNAConfigGroupCipherSize ((INT4) u4ProfileId,
                                                       &u4GroupCipherSize);

                    nmhGetFsRSNAConfigPMKLifetime ((INT4) u4ProfileId,
                                                   &u4PMKLifetime);

                    nmhGetFsRSNAConfigPMKReauthThreshold ((INT4)
                                                          u4ProfileId,
                                                          &u4PMKReAuthThreshold);

                    nmhGetFsRSNAConfigNumberOfPTKSAReplayCountersImplemented ((INT4) u4ProfileId, &u4PTKSAReplayCounter);

                    nmhGetFsRSNAConfigSATimeout ((INT4) u4ProfileId,
                                                 &u4SATimeOut);
                    {
                        MEMSET (AuthSuiteSelected.pu1_OctetList, 0,
                                RSNA_CIPHER_SUITE_LEN);
                        WpaGetFsRSNAConfigAuthenticationSuiteEnabled ((INT4)
                                                                      u4ProfileId,
                                                                      RSNA_AUTHSUITE_PSK,
                                                                      (INT4 *)
                                                                      &u4AuthSuiteStatus);
                        if (u4AuthSuiteStatus == WPA_ENABLED)
                        {
                            MEMCPY (AuthSuiteSelected.pu1_OctetList, u1Psk,
                                    RSNA_AKM_SELECTOR_LEN);
                        }
                        else
                        {
                            WpaGetFsRSNAConfigAuthenticationSuiteEnabled ((INT4)
                                                                          u4ProfileId,
                                                                          RSNA_AUTHSUITE_8021X,
                                                                          (INT4
                                                                           *)
                                                                          &u4AuthSuiteStatus);
                            if (u4AuthSuiteStatus == WPA_ENABLED)
                            {
                                MEMCPY (AuthSuiteSelected.pu1_OctetList,
                                        u1dot1x, RSNA_AKM_SELECTOR_LEN);
                            }
                        }
                    }

                    nmhGetFsRSNAGroupCipherSelected ((INT4) u4ProfileId,
                                                     &GroupwiseCipherSelected);

                    nmhGetFsRSNAPMKIDUsed ((INT4) u4ProfileId, &PmkIdUsed);

                    nmhGetFsRSNAAuthenticationSuiteRequested ((INT4)
                                                              u4ProfileId,
                                                              &AuthSuiteRequested);

                    nmhGetFsRSNAGroupCipherRequested ((INT4) u4ProfileId,
                                                      &PairwiseCipherRequested);

                    nmhGetFsRSNAGroupCipherRequested ((INT4) u4ProfileId,
                                                      &GroupwiseCipherRequested);

                    nmhGetFsRSNATKIPCounterMeasuresInvoked ((INT4)
                                                            u4ProfileId,
                                                            &u4TKIPCounterMeasuresInvoked);

                    nmhGetFsRSNA4WayHandshakeFailures ((INT4) u4ProfileId,
                                                       &u4FourWayHandshakeFailures);

                    nmhGetFsRSNAConfigNumberOfGTKSAReplayCountersImplemented ((INT4) u4ProfileId, &u4GTKSAReplayCountersCount);

                    STRCPY (pHttp->au1KeyString, "Wpastatus_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    if (i4RsnaEnabled == WPA_ENABLED)
                    {
                        MEMCPY (PairwiseCipherSelected.pu1_OctetList, u1Tkip,
                                RSNA_CIPHER_SUITE_LEN);
                        MEMCPY (GroupwiseCipherSelected.pu1_OctetList, u1Tkip,
                                RSNA_CIPHER_SUITE_LEN);
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Enabled");
                    }
                    else
                    {
                        MEMSET (PairwiseCipherSelected.pu1_OctetList, 0,
                                RSNA_CIPHER_SUITE_LEN);
                        MEMSET (GroupwiseCipherSelected.pu1_OctetList, 0,
                                RSNA_CIPHER_SUITE_LEN);
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Disabled");
                    }
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "Preauthentication_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    if (i4PreauthenticationEnabled == RSNA_ENABLED)
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Enabled");
                    }
                    else
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Disabled");
                    }

                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "okc_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    if (OKCEnabled == RSNA_ENABLED)
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Enabled");
                    }
                    else
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Disabled");
                    }

                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "wlan_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                             u4currentWlanId);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "WPAVersion_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                             u4ConfigVersion);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString,
                            "WPAPairwiseKeysSupported_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                             u4PairwiseKeySupported);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "WPAGroupCipher_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%X:%X:%X:%X",
                             GroupCipher.pu1_OctetList[0],
                             GroupCipher.pu1_OctetList[1],
                             GroupCipher.pu1_OctetList[WLC_OFFSET_2],
                             GroupCipher.pu1_OctetList[WLC_OFFSET_3]);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "WPAGroupRekeyMethod_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    if (u4GroupRekeyMethod == RSNA_GROUP_REKEY_DISABLED)
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Disabled");
                    }
                    else if (u4GroupRekeyMethod == RSNA_GROUP_REKEY_TIMEBASED)
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "TimeBased");
                    }
                    else if (u4GroupRekeyMethod == RSNA_GROUP_REKEY_PACKETBASED)
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "PackedBased");
                    }
                    else if (u4GroupRekeyMethod ==
                             RSNA_GROUP_REKEY_TIMEPACKETBASED)
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "TimePacketBased");
                    }
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "WPAGroupRekeyTime_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                             u4GroupRekeyTime);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "WPAGroupRekeyPackets_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                             u4GroupRekeyPkts);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "WPAGroupRekeyStrict_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    if (u4GroupRekeyStrict == RSNA_ENABLED)
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Enable");
                    }
                    else if (u4GroupRekeyStrict == RSNA_DISABLED)
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Disable");
                    }
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "WPAGroupUpdateCount_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%u",
                             u4GroupUpdateCount);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "WPAPairwiseUpdateCount_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                             u4PairwiseUpdateCount);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "WPAGroupCipherSize_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                             u4GroupCipherSize);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "WPAPMKLifetime_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                             u4PMKLifetime);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "WPAPMKReauthThreshold_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                             u4PMKReAuthThreshold);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString,
                            "WPANumberOfPTKSAReplayCounters_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                             u4PTKSAReplayCounter);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "WPASATimeout_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4SATimeOut);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString,
                            "WPAAuthenticationSuiteSelected_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%x:%x:%x:%x",
                             AuthSuiteSelected.pu1_OctetList[0],
                             AuthSuiteSelected.pu1_OctetList[1],
                             AuthSuiteSelected.pu1_OctetList[WLC_OFFSET_2],
                             AuthSuiteSelected.pu1_OctetList[WLC_OFFSET_3]);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString,
                            "WPAPairwiseCipherSelected_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%x:%x:%x:%x",
                             PairwiseCipherSelected.pu1_OctetList[0],
                             PairwiseCipherSelected.pu1_OctetList[1],
                             PairwiseCipherSelected.pu1_OctetList[WLC_OFFSET_2],
                             PairwiseCipherSelected.
                             pu1_OctetList[WLC_OFFSET_3]);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "WPAGroupCipherSelected_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%x:%x:%x:%x",
                             GroupwiseCipherSelected.pu1_OctetList[0],
                             GroupwiseCipherSelected.pu1_OctetList[1],
                             GroupwiseCipherSelected.
                             pu1_OctetList[WLC_OFFSET_2],
                             GroupwiseCipherSelected.
                             pu1_OctetList[WLC_OFFSET_3]);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "WPAPMKIDUsed_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%x:%x:%x:%x",
                             PmkIdUsed.pu1_OctetList[0],
                             PmkIdUsed.pu1_OctetList[1],
                             PmkIdUsed.pu1_OctetList[WLC_OFFSET_2],
                             PmkIdUsed.pu1_OctetList[WLC_OFFSET_3]);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString,
                            "WPAAuthenticationSuiteRequested_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%x:%x:%x:%x",
                             AuthSuiteRequested.pu1_OctetList[0],
                             AuthSuiteRequested.pu1_OctetList[1],
                             AuthSuiteRequested.pu1_OctetList[WLC_OFFSET_2],
                             AuthSuiteRequested.pu1_OctetList[WLC_OFFSET_3]);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString,
                            "WPAPairwiseCipherSelected_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%x:%x:%x:%x",
                             PairwiseCipherRequested.pu1_OctetList[0],
                             PairwiseCipherRequested.pu1_OctetList[1],
                             PairwiseCipherRequested.
                             pu1_OctetList[WLC_OFFSET_2],
                             PairwiseCipherRequested.
                             pu1_OctetList[WLC_OFFSET_3]);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "WPAGroupCipherRequested_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%x:%x:%x:%x",
                             GroupwiseCipherRequested.pu1_OctetList[0],
                             GroupwiseCipherRequested.pu1_OctetList[1],
                             GroupwiseCipherRequested.
                             pu1_OctetList[WLC_OFFSET_2],
                             GroupwiseCipherRequested.
                             pu1_OctetList[WLC_OFFSET_3]);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString,
                            "WPATKIPCounterMeasuresInvoked_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                             u4TKIPCounterMeasuresInvoked);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString,
                            "WPA4WayHandshakeFailures_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                             u4FourWayHandshakeFailures);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString,
                            "WPANumberOfGTKSAReplayCounters_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                             u4GTKSAReplayCountersCount);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                }

            }
        }
    }
    while (nmhGetNextIndexCapwapDot11WlanTable
           (u4currentWlanId, &u4nextWlanId));

    PNAC_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
*  Function Name : IssProcessWpaConfigPageSet
*  Description   : This function processes the set request coming for the
*                  Rsna Config Table page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessWpaConfigPageSet (tHttp * pHttp)
{
    INT4                i4WlanProfileId = 0;

    STRCPY (pHttp->au1Name, "WLAN_PROFILE_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4WlanProfileId = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    WSS_WEB_TRC ("WLAN Profile Id:%d\n", i4WlanProfileId);

    if (STRCMP (pHttp->au1Value, "Edit") == 0)
    {
        WSS_WEB_TRC ("Edit.....just store the Profile Id:%d\n",
                     i4WlanProfileId);
        gi4WlanProfileId = (INT4) i4WlanProfileId;
    }
    else
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Operation\n");
    }
    return;

}

/*********************************************************************
*  Function Name : IssProcessWpaConfigEditPage
*  Description   : This function processes the request coming for the
*                  Rsna Config Table Edit Page
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessWpaConfigEditPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessWpaConfigEditPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessWpaConfigEditPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessWpaConfigEditPageGet
*  Description   : This function processes the get request coming for the
*                  Rsna Config Table Edit page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessWpaConfigEditPageGet (tHttp * pHttp)
{
    UINT4               u4ProfileId = 0;
    tSNMP_OCTET_STRING_TYPE GroupCipher;
    UINT1               au1GroupCipher[RSNA_CIPHER_SUITE_LEN];
    UINT4               u4GroupRekeyMethod = 0;
    UINT4               u4GroupRekeyTime = 0;
    UINT4               u4GroupRekeyPkts = 0;
    UINT4               u4GroupRekeyStrict = 0;
    UINT4               u4GroupUpdateCount = 0;
    UINT4               u4PairwiseUpdateCount = 0;
    UINT4               u4PMKLifetime = 0;
    UINT4               u4PMKReAuthThreshold = 0;
    UINT4               u4SATimeOut = 0;
    UINT1               u1Cipher = 0;
    INT4                i4RsnaEnabled = 0;
    INT4                i4PreauthenticationEnabled = 0;
    INT4                OKCEnabled = 0;

    MEMSET (au1GroupCipher, '\0', sizeof (RSNA_CIPHER_SUITE_LEN));
    GroupCipher.pu1_OctetList = au1GroupCipher;

    WebnmRegisterLock (pHttp, PnacLock, PnacUnLock);
    PNAC_LOCK ();
    if (nmhGetCapwapDot11WlanProfileIfIndex
        ((UINT4) gi4WlanProfileId, (INT4 *) &u4ProfileId) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));

    }
    else
    {
        if (u4ProfileId != 0)
        {

            nmhGetFsRSNAConfigActivated ((INT4) u4ProfileId, &i4RsnaEnabled);

            nmhGetFsRSNAOKCEnabled ((INT4) u4ProfileId, &OKCEnabled);

            nmhGetFsRSNAConfigGroupCipher ((INT4) u4ProfileId, &GroupCipher);

            nmhGetFsRSNAConfigGroupRekeyMethod ((INT4) u4ProfileId,
                                                (INT4 *) &u4GroupRekeyMethod);

            nmhGetFsRSNAConfigGroupRekeyTime ((INT4) u4ProfileId,
                                              &u4GroupRekeyTime);

            nmhGetFsRSNAConfigGroupRekeyPackets ((INT4) u4ProfileId,
                                                 &u4GroupRekeyPkts);

            nmhGetFsRSNAConfigGroupRekeyStrict ((INT4) u4ProfileId,
                                                (INT4 *) &u4GroupRekeyStrict);
            nmhGetFsRSNAConfigGroupUpdateCount ((INT4) u4ProfileId,
                                                &u4GroupUpdateCount);

            nmhGetFsRSNAConfigPairwiseUpdateCount ((INT4) u4ProfileId,
                                                   &u4PairwiseUpdateCount);

            nmhGetFsRSNAConfigPMKLifetime ((INT4) u4ProfileId, &u4PMKLifetime);

            nmhGetFsRSNAConfigPMKReauthThreshold ((INT4) u4ProfileId,
                                                  &u4PMKReAuthThreshold);

            nmhGetFsRSNAConfigSATimeout ((INT4) u4ProfileId, &u4SATimeOut);

            STRCPY (pHttp->au1KeyString, "SSID_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", gi4WlanProfileId);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "wpa_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RsnaEnabled);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "preauthentication_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                     i4PreauthenticationEnabled);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "okc_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", OKCEnabled);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "groupcipher_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            if (MEMCMP
                (GroupCipher.pu1_OctetList, gau1RsnaCipherSuiteTKIP,
                 RSNA_CIPHER_SUITE_LEN) == 0)
            {
                u1Cipher = RSNA_TKIP;

            }
            else if (MEMCMP
                     (GroupCipher.pu1_OctetList, gau1RsnaCipherSuiteCCMP,
                      RSNA_CIPHER_SUITE_LEN) == 0)
            {
                u1Cipher = RSNA_CCMP;

            }
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u1Cipher);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "grouprekey_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4GroupRekeyMethod);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "grouprekeytime_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4GroupRekeyTime);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "grouprekeypackets_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4GroupRekeyPkts);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "grouprekeystrict_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4GroupRekeyStrict);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "psk_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "");
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "groupupdatecount_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4GroupUpdateCount);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "pairwiseupdatecount_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                     u4PairwiseUpdateCount);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "pmklifetime_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4PMKLifetime);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "pmkreauththreshold_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4PMKReAuthThreshold);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "satimeout_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4SATimeOut);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));

            PNAC_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));

        }
        else
        {
            PNAC_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
        }
    }

}

/*********************************************************************
*  Function Name : IssProcessWpaConfigEditPageSet
*  Description   : This function processes the set request coming for the
*                  Rsna Config Table Edit page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessWpaConfigEditPageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE tCipherType;
    tSNMP_OCTET_STRING_TYPE PSK;
    UINT4               u4WlanId = 0;
    UINT4               u4TimeValue = 0;
    INT1                i1RsnaStatus = 0;
    UINT1               au1WpaCipherSuiteTKIP[] = { 0x00, 0x50, 0xf2, 0x02 };
#ifdef RSNA_PRE_AUTH
    INT1                i1Status = 0;
#endif
    INT1                okcStatus = 0;
    UINT1               u1CipherType = 0;
    INT1                i1GroupRekeyMethod = 0;
    INT4                i4GroupRekeyInterval = 0;
    INT4                i4NumberOfPackets = 0;
    UINT1               u1StrictStatus = 0;
    UINT1               u1PskType = 0;
    UINT4               u4RetryCount = 0;
    UINT4               u4PairwiseRetryCount = 0;
    UINT4               u4LifeTime = 0;
    UINT4               u4ThresholdValue = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4ProfileId = 0;
    UINT1               u1Index = 0;
    UINT4               u4KeyLength = 0;
    UINT1               au1TempBuf[RSNA_PSK_TEMP_SIZE] = { 0 };
    UINT1               au1Output[RSNA_PSK_LEN];

    UINT1               au1Buf[RSNA_CIPHER_SUITE_LEN];
    UINT1               au1psk[RSNA_PASS_PHRASE_LEN];
    MEMSET (au1Buf, 0, sizeof (au1Buf));
    MEMSET (au1psk, 0, sizeof (au1psk));
    MEMSET (au1Output, 0, sizeof (au1Output));
    PSK.pu1_OctetList = au1psk;
    tCipherType.pu1_OctetList = au1Buf;

    STRCPY (pHttp->au1Name, "SSID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4WlanId = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "wpa");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i1RsnaStatus = (INT1) ATOI ((INT1 *) pHttp->au1Value);
#ifdef RSNA_PRE_AUTH
    STRCPY (pHttp->au1Name, "preauthentication");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i1Status = (INT1) ATOI ((INT1 *) pHttp->au1Value);
#endif

    STRCPY (pHttp->au1Name, "okc");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    okcStatus = (INT1) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "GroupCipher");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u1CipherType = (UINT1) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "GroupRekey");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i1GroupRekeyMethod = (INT1) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "GroupRekeyTime");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4GroupRekeyInterval = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "GroupRekeyPackets");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4NumberOfPackets = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "GroupRekeyStrict");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u1StrictStatus = (UINT1) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "PSKType");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u1PskType = (UINT1) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "PSK");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1psk, pHttp->au1Value, (sizeof (au1psk) - 1));
    PSK.i4_Length = (INT4) STRLEN (au1psk);
    u4KeyLength = (UINT4) STRLEN (au1psk);

    STRCPY (pHttp->au1Name, "GroupUpdateCount");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4RetryCount = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "PairwiseUpdateCount");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4PairwiseRetryCount = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "PMKLifetime");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4LifeTime = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "PMKReauthThreshold");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4ThresholdValue = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "SATimeout");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4TimeValue = (UINT4) ATOI ((INT1 *) pHttp->au1Value);
    WebnmRegisterLock (pHttp, PnacLock, PnacUnLock);
    PNAC_LOCK ();

    if (nmhGetCapwapDot11WlanProfileIfIndex
        (u4WlanId, (INT4 *) &u4ProfileId) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in getting WlanIfIndex\n");
        return;
    }
/*Pre Authentication feature is disabled as of now and
 *  hence the following are defined with this compilation switch.
 *  It will be removed when the actual feature is implemented */

    if (nmhTestv2FsRSNAOKCEnabled (&u4ErrorCode,
                                   (INT4) u4ProfileId,
                                   okcStatus) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration - OKC\n");
        return;
    }
    if (nmhSetFsRSNAOKCEnabled ((INT4) u4ProfileId, okcStatus) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting OKC\n");
        return;
    }

    if (nmhTestv2FsRSNAConfigActivated (&u4ErrorCode, (INT4) u4ProfileId,
                                        i1RsnaStatus) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration - RSNA\n");
        return;
    }
    if (nmhSetFsRSNAConfigActivated ((INT4) u4ProfileId,
                                     i1RsnaStatus) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting  RSNA\n");
        return;
    }
    if (u1CipherType == RSNA_TKIP)
    {
        MEMCPY (tCipherType.pu1_OctetList, au1WpaCipherSuiteTKIP,
                RSNA_CIPHER_SUITE_LEN);
        tCipherType.i4_Length = RSNA_CIPHER_SUITE_LEN;
    }

    if (nmhTestv2FsRSNAConfigGroupCipher (&u4ErrorCode, (INT4) u4ProfileId,
                                          &tCipherType) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Invalid Configuration - Group Cipher\n");
        return;
    }
    if (nmhGetFsRSNAConfigGroupCipher ((INT4) u4ProfileId, &tCipherType)
        != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting Group Cipher\n");
        return;
    }
    if (nmhTestv2FsRSNAConfigGroupRekeyMethod (&u4ErrorCode,
                                               (INT4) u4ProfileId,
                                               i1GroupRekeyMethod) !=
        SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Invalid Configuration - Group Rekey\n");
        return;
    }

    if (nmhSetFsRSNAConfigGroupRekeyMethod ((INT4) u4ProfileId,
                                            i1GroupRekeyMethod) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting Group Rekey\n");

        return;
    }

    if (nmhTestv2FsRSNAConfigGroupRekeyTime (&u4ErrorCode,
                                             (INT4) u4ProfileId,
                                             (UINT4) i4GroupRekeyInterval)
        != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Invalid Configuration - Group Rekey Time\n");

        return;
    }
    if (nmhSetFsRSNAConfigGroupRekeyTime ((INT4) u4ProfileId,
                                          (UINT4) i4GroupRekeyInterval) !=
        SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Error in setting Group Rekey Time\n");

        return;
    }
    if (nmhSetFsRSNAConfigGroupRekeyPackets ((INT4) u4ProfileId,
                                             (UINT4) i4NumberOfPackets) !=
        SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Error in setting Group Rekey Packets\n");

        return;
    }
    if (nmhTestv2FsRSNAConfigGroupRekeyStrict (&u4ErrorCode,
                                               (INT4) u4ProfileId,
                                               u1StrictStatus) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Invalid Configuration - Group Rekey Strict\n");

        return;
    }

    if (nmhSetFsRSNAConfigGroupRekeyStrict ((INT4) u4ProfileId,
                                            u1StrictStatus) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Error in setting Group Rekey Strict\n");

        return;
    }

    if (STRLEN (au1psk))
    {
        if (u1PskType == RSNA_AKM_PSK_SETKEY_ASCII)
        {

            if ((nmhTestv2FsRSNAConfigPSKPassPhrase
                 (&u4ErrorCode, (INT4) u4ProfileId, &PSK)) != SNMP_SUCCESS)
            {
                PNAC_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Invalid Configuration - PSK Pass Phrase\n");

                return;
            }
            if ((nmhSetFsRSNAConfigPSKPassPhrase ((INT4) u4ProfileId, &PSK))
                != SNMP_SUCCESS)
            {
                PNAC_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Error in setting PSK Pass Phrase\n");

                return;
            }
        }
        else
        {
            for (u1Index = 0; u1Index < u4KeyLength; u1Index += WLC_OFFSET_2)
            {
                /* Assemble a digit pair into the hexbyte string */
                au1TempBuf[0] = au1psk[u1Index];
                au1TempBuf[1] = au1psk[u1Index + 1];

                /* Convert the hex pair to an integer */
                SSCANF ((const CHR1 *) au1TempBuf, "%x",
                        (UINT4 *) &au1Output[u1Index / WLC_OFFSET_2]);
            }
            MEMCPY (PSK.pu1_OctetList, au1Output, (u4KeyLength / WLC_OFFSET_2));
            PSK.i4_Length = (INT4) (u4KeyLength / WLC_OFFSET_2);

            if ((nmhTestv2FsRSNAConfigPSKValue
                 (&u4ErrorCode, (INT4) u4ProfileId, &PSK)) != SNMP_SUCCESS)
            {
                PNAC_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *) "Invalid Configuration - PSK\n");

                return;
            }

            if ((nmhSetFsRSNAConfigPSKValue ((INT4) u4ProfileId, &PSK)) !=
                SNMP_SUCCESS)
            {
                PNAC_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Error in setting PSK\n");

                return;
            }
        }

    }
    if (nmhTestv2FsRSNAConfigGroupUpdateCount (&u4ErrorCode,
                                               (INT4) u4ProfileId,
                                               u4RetryCount) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Invalid Configuration - Group Update Count\n");

        return;
    }

    if (nmhSetFsRSNAConfigGroupUpdateCount ((INT4) u4ProfileId,
                                            u4RetryCount) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Error in setting Group Update Count\n");

        return;
    }

    if (nmhSetFsRSNAConfigPairwiseUpdateCount
        ((INT4) u4ProfileId, u4PairwiseRetryCount) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Error in setting Pairwise Update Count\n");

        return;
    }

    if (nmhTestv2FsRSNAConfigPMKLifetime (&u4ErrorCode, (INT4) u4ProfileId,
                                          u4LifeTime) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Invalid Configuration - PMK Lifetime\n");

        return;
    }
    if (nmhSetFsRSNAConfigPMKLifetime ((INT4) u4ProfileId, u4LifeTime)
        != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting PMK Lifetime\n");

        return;
    }

    if (nmhTestv2FsRSNAConfigPMKReauthThreshold (&u4ErrorCode,
                                                 (INT4) u4ProfileId,
                                                 u4ThresholdValue) !=
        SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Invalid Configuration - PMK Reauth Threshold\n");

        return;
    }

    if (nmhSetFsRSNAConfigPMKReauthThreshold
        ((INT4) u4ProfileId, u4ThresholdValue) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Error in setting PMK Reauth Threshold\n");

        return;
    }

    if (nmhTestv2FsRSNAConfigSATimeout (&u4ErrorCode, (INT4) u4ProfileId,
                                        u4TimeValue) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Invalid Configuration - SA Timeout\n");

        return;
    }
    if (nmhSetFsRSNAConfigSATimeout ((INT4) u4ProfileId, u4TimeValue) !=
        SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting SA Timeout\n");

        return;
    }
    PNAC_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    IssProcessWpaConfigEditPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssProcessWpaCipherPage
*  Description   : This function processes the request coming for the
*                  Rsna Cipher Table Page
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessWpaCipherPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessWpaCipherPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessWpaCipherPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessWpaCipherPageGet
*  Description   : This function processes the get request coming for the
*                  Rsna Cipher Table page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessWpaCipherPageGet (tHttp * pHttp)
{
    UINT4               u4IfIndex = 0;
    UINT4               u4Temp = 0;
    UINT4               u4currentWlanId = 0;
    UINT4               u4nextWlanId = 0;
    INT4                i4OutCome = 0;
    INT4                i4RsnaEnabled = 0;
    UINT1               au1RSNAConfigCipher[WLC_OFFSET_4] =
        { 0x0, 0x50, 0xf2, 0x02 };
    UINT4               u4PairwiseCipherSize = WLC_OFFSET_4;

    if (WebnmAllocMultiData () == NULL)

    {
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;
    i4OutCome = nmhGetFirstIndexCapwapDot11WlanTable (&u4nextWlanId);

    STRCPY (pHttp->au1KeyString, "<! SSID>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        u4currentWlanId = u4nextWlanId;
        if (u4currentWlanId != 0)
        {
            STRCPY (pHttp->au1Name, "SSID");
            SPRINTF ((CHR1 *) pHttp->au1DataString,
                     "<option value = \"%d\" selected>%d \n",
                     u4currentWlanId, u4currentWlanId);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
    }
    while (nmhGetNextIndexCapwapDot11WlanTable
           (u4currentWlanId, &u4nextWlanId));

    u4currentWlanId = 0;
    u4nextWlanId = 0;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;
    WebnmRegisterLock (pHttp, PnacLock, PnacUnLock);
    PNAC_LOCK ();
    i4OutCome = nmhGetFirstIndexCapwapDot11WlanTable (&u4nextWlanId);

    if (i4OutCome == SNMP_FAILURE)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }
    else
    {
        do
        {
            pHttp->i4Write = (INT4) u4Temp;
            u4currentWlanId = u4nextWlanId;
            if (u4currentWlanId != 0)
            {
                nmhGetCapwapDot11WlanProfileIfIndex (u4currentWlanId,
                                                     (INT4 *) &u4IfIndex);
                if (u4IfIndex != 0)
                {
                    nmhGetFsRSNAConfigActivated ((INT4) u4IfIndex,
                                                 &i4RsnaEnabled);

                    STRCPY (pHttp->au1KeyString, "wlan_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                             u4currentWlanId);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "WPAPairwiseCipher_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%2X:%2X:%2X:%2X",
                             au1RSNAConfigCipher[0],
                             au1RSNAConfigCipher[1],
                             au1RSNAConfigCipher[WLC_OFFSET_2],
                             au1RSNAConfigCipher[WLC_OFFSET_3]);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString,
                            "WPAPairwiseCipherEnabled_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    if (i4RsnaEnabled == RSNA_ENABLED)
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Enable");
                    }
                    else
                    {
                        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                 "Disable");
                    }

                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    STRCPY (pHttp->au1KeyString, "WPAPairwiseCipherSize_KEY");
                    WebnmSendString (pHttp, pHttp->au1KeyString);
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                             u4PairwiseCipherSize);
                    WebnmSockWrite (pHttp, pHttp->au1DataString,
                                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
                    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                    WebnmSendString (pHttp, pHttp->au1KeyString);

                    WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                }
            }
        }
        while (nmhGetNextIndexCapwapDot11WlanTable
               (u4currentWlanId, &u4nextWlanId));
    }

    PNAC_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
*  Function Name : IssProcessWpaCipherPageSet
*  Description   : This function processes the set request coming for the
*                  Rsna Cipher Table page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessWpaCipherPageSet (tHttp * pHttp)
{
    UINT4               u4WlanId = 0;
    UINT1               u1CipherType = 0;
    INT1                i1Status = 0;
    UINT4               u4ProfileId = 0;
    UINT4               u4ErrorCode = 0;

    STRCPY (pHttp->au1Name, "SSID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4WlanId = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "pairwisecipher");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u1CipherType = (UINT1) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "CipherStat");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i1Status = (INT1) ATOI ((INT1 *) pHttp->au1Value);

    WebnmRegisterLock (pHttp, PnacLock, PnacUnLock);
    PNAC_LOCK ();

    if (nmhGetCapwapDot11WlanProfileIfIndex
        (u4WlanId, (INT4 *) &u4ProfileId) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in getting WlanIfIndex\n");
        return;
    }

    if (nmhTestv2FsRSNAConfigPairwiseCipherActivated (&u4ErrorCode,
                                                      (INT4) u4ProfileId,
                                                      u1CipherType,
                                                      i1Status) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Invalid configuration - Cipher Status\n");
        return;
    }

    if (nmhSetFsRSNAConfigPairwiseCipherActivated ((INT4) u4ProfileId,
                                                   u1CipherType,
                                                   i1Status) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in setting cipher status\n");
        return;
    }
    PNAC_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    IssProcessWpaCipherPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssProcessWpaAuthsuitePage
*  Description   : This function processes the request coming for the
*                  Rsna Authsuite Table Page
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessWpaAuthsuitePage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessWpaAuthsuitePageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessWpaAuthsuitePageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessWpaAuthsuitePageGet
*  Description   : This function processes the get request coming for the
*                  Rsna Authsuite Table page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessWpaAuthsuitePageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE AuthenticationSuite;
    UINT4               u4Temp = 0;
    INT4                i4OutCome = 0;
    UINT4               u4currentWlanId = 0;
    UINT4               u4nextWlanId = 0;
    UINT4               u4WlanId = 0;
    UINT4               u4CurrAuthSuiteIndex = 0;
    UINT4               u4NextAuthSuiteIndex = 0;
    UINT4               u4AuthSuiteStatus = 0;
    UINT1               au1AuthSuitetype[RSNA_MAX_AUTH_SUITE_TYPE_LENGTH];

    MEMSET (au1AuthSuitetype, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    AuthenticationSuite.pu1_OctetList = au1AuthSuitetype;

    if (WebnmAllocMultiData () == NULL)

    {
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;
    i4OutCome = nmhGetFirstIndexCapwapDot11WlanTable (&u4nextWlanId);

    STRCPY (pHttp->au1KeyString, "<! SSID>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        u4currentWlanId = u4nextWlanId;
        if (u4currentWlanId != 0)
        {
            STRCPY (pHttp->au1Name, "SSID");
            SPRINTF ((CHR1 *) pHttp->au1DataString,
                     "<option value = \"%d\" selected>%d \n",
                     u4currentWlanId, u4currentWlanId);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
    }
    while (nmhGetNextIndexCapwapDot11WlanTable
           (u4currentWlanId, &u4nextWlanId));

    u4currentWlanId = 0;
    u4nextWlanId = 0;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;
    WebnmRegisterLock (pHttp, PnacLock, PnacUnLock);
    PNAC_LOCK ();

    i4OutCome =
        nmhGetFirstIndexFsWPAConfigAuthenticationSuitesTable
        ((INT4 *) &u4nextWlanId, &u4NextAuthSuiteIndex);

    if (i4OutCome == SNMP_FAILURE)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }
    else
    {
        do
        {
            pHttp->i4Write = (INT4) u4Temp;
            u4CurrAuthSuiteIndex = u4NextAuthSuiteIndex;
            u4currentWlanId = u4nextWlanId;
            WssGetCapwapWlanId (u4currentWlanId, &u4WlanId);

            RsnaGetFsRSNAConfigAuthenticationSuite ((UINT4) u4currentWlanId,
                                                    u4CurrAuthSuiteIndex,
                                                    &AuthenticationSuite);
            WpaGetFsRSNAConfigAuthenticationSuiteEnabled
                ((UINT4) u4currentWlanId, u4CurrAuthSuiteIndex,
                 (INT4 *) &u4AuthSuiteStatus);

            STRCPY (pHttp->au1KeyString, "wlan_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4WlanId);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            STRCPY (pHttp->au1KeyString, "AuthenticationSuiteIndex_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (u4CurrAuthSuiteIndex == RSNA_AUTHSUITE_PSK)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "PSK");
            }
            else if (u4CurrAuthSuiteIndex == RSNA_AUTHSUITE_8021X)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "8021X");
            }
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            STRCPY (pHttp->au1KeyString, "AuthenticationSuite_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%2X:%2X:%2X:%2X",
                     AuthenticationSuite.pu1_OctetList[0],
                     AuthenticationSuite.pu1_OctetList[1],
                     AuthenticationSuite.pu1_OctetList[WLC_OFFSET_2],
                     AuthenticationSuite.pu1_OctetList[WLC_OFFSET_3]);

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            STRCPY (pHttp->au1KeyString, "AuthenticationSuiteStatus_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            if (u4AuthSuiteStatus == RSNA_ENABLED)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Enable");
            }
            else
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Disable");
            }

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        }
        while (nmhGetNextIndexFsWPAConfigAuthenticationSuitesTable
               ((INT4) u4currentWlanId, (INT4 *) &u4nextWlanId,
                u4CurrAuthSuiteIndex, &u4NextAuthSuiteIndex) == SNMP_SUCCESS);
    }

    PNAC_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
*  Function Name : IssProcessWpaAuthsuitePageSet
*  Description   : This function processes the set request coming for the
*                  Rsna Authsuite Table page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessWpaAuthsuitePageSet (tHttp * pHttp)
{
    UINT1               u1AuthType = 0;
    UINT1               u1Status = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4ProfileId = 0;
    UINT4               u4WlanId = 0;

    STRCPY (pHttp->au1Name, "SSID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4WlanId = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "AuthType");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u1AuthType = (UINT1) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "AdminStat");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u1Status = (UINT1) ATOI ((INT1 *) pHttp->au1Value);
    WebnmRegisterLock (pHttp, PnacLock, PnacUnLock);
    PNAC_LOCK ();

    if (nmhGetCapwapDot11WlanProfileIfIndex
        (u4WlanId, (INT4 *) &u4ProfileId) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in getting WlanIfIndex\n");
        return;
    }

    if (WpaTestv2FsRSNAConfigAuthenticationSuiteEnabled (&u4ErrorCode,
                                                         (UINT4) u1AuthType,
                                                         (INT4) u1Status) !=
        SNMP_SUCCESS)
    {

        WebnmUnRegisterLock (pHttp);
        PNAC_UNLOCK ();
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Invalid configuration - Authsuite Status\n");
        return;

    }

    if (WpaSetFsRSNAConfigAuthenticationSuiteEnabled (u4ProfileId,
                                                      (UINT4) u1AuthType,
                                                      (INT1) u1Status) !=
        SNMP_SUCCESS)
    {

        WebnmUnRegisterLock (pHttp);
        PNAC_UNLOCK ();
        IssSendError (pHttp,
                      (CONST INT1 *) "Error in setting Authsuite Status\n");
        return;

    }

    WebnmUnRegisterLock (pHttp);
    PNAC_UNLOCK ();
    IssProcessWpaAuthsuitePageGet (pHttp);
}

#if RSNA_WANTED
#if PMF_WANTED
/*********************************************************************
*  Function Name : IssProcessWpaProtectedMgmtPage
*  Description   : This function processes the request coming for the
*                  Rsna Protected Mngmt Table Page
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessWpaProtectedMgmtPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessWpaProtectedMgmtPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessWpaProtectedMgmtPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessWpaProtectedMgmtPageGet
*  Description   : This function processes the get request coming for the
*                  Rsna Protected Mngmt Table page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessWpaProtectedMgmtPageGet (tHttp * pHttp)
{
    INT4                i4OutCome = 0;
    INT4                i4ProtMgmtFrameEnabled = 0;
    INT4                i4UnProtMgmtFramAllowed = 0;
    UINT4               u4Temp = 0;
    UINT4               u4currentWlanId = 0;
    UINT4               u4nextWlanId = 0;
    UINT4               u4firstWlanId = 0;
    UINT4               u4ProfileId = 0;
    UINT4               u4AssocSAQueryMaxTImeOut = 0;
    UINT4               u4AssocSAQueryRetryTimeOut = 0;
    UINT4               u4AssocComeBackTime = 0;

    u4Temp = (UINT4) pHttp->i4Write;
    WebnmRegisterLock (pHttp, PnacLock, PnacUnLock);
    PNAC_LOCK ();
    i4OutCome = nmhGetFirstIndexCapwapDot11WlanTable (&u4firstWlanId);
    if (i4OutCome == SNMP_FAILURE)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    u4currentWlanId = u4firstWlanId;
    u4nextWlanId = u4firstWlanId;
    STRCPY (pHttp->au1KeyString, "<! SSID>");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    do
    {
        u4currentWlanId = u4nextWlanId;
        if (u4currentWlanId != 0)
        {
            STRCPY (pHttp->au1Name, "SSID");
            SPRINTF ((CHR1 *) pHttp->au1DataString,
                     "<option value = \"%d\">%d \n",
                     u4currentWlanId, u4currentWlanId);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
    }
    while (nmhGetNextIndexCapwapDot11WlanTable
           (u4currentWlanId, &u4nextWlanId));

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;
    u4currentWlanId = u4firstWlanId;
    u4nextWlanId = u4firstWlanId;
    do
    {
        pHttp->i4Write = (INT4) u4Temp;
        u4currentWlanId = u4nextWlanId;
        if (nmhGetCapwapDot11WlanProfileIfIndex (u4currentWlanId,
                                                 (INT4 *) &u4ProfileId) !=
            SNMP_SUCCESS)
        {
            PNAC_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Error in getting WlanIfIndex\n");
            return;
        }

        RsnaGetDot11RSNAProtectedManagementFramesActivated ((INT4) u4ProfileId,
                                                            &i4ProtMgmtFrameEnabled);
        RsnaGetDot11RSNAUnprotectedManagementFramesAllowed ((INT4) u4ProfileId,
                                                            &i4UnProtMgmtFramAllowed);
        RsnaGetDot11AssociationSAQueryMaximumTimeout ((INT4) u4ProfileId,
                                                      &u4AssocSAQueryMaxTImeOut);
        RsnaGetDot11AssociationSAQueryRetryTimeout ((INT4) u4ProfileId,
                                                    &u4AssocSAQueryRetryTimeOut);
        nmhGetFsRSNAAssociationComeBackTime ((INT4) u4ProfileId,
                                             &u4AssocComeBackTime);

        STRCPY (pHttp->au1KeyString, "wlan_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4currentWlanId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString, "ProtectedManagementFramesEnabled_key");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4ProtMgmtFrameEnabled == RSNA_ENABLED)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Enable");
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Disable");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString, "UnprotectedManagementFramesAllowed_Key");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4UnProtMgmtFramAllowed == RSNA_ENABLED)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Enable");
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Disable");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString, "AssociationSAQueryMaximumTimeout_Key");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4AssocSAQueryMaxTImeOut);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString, "AssociationSAQueryRetryTimeout_Key");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                 u4AssocSAQueryRetryTimeOut);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString, "AssociationComebacktime_Key");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4AssocComeBackTime);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    while (nmhGetNextIndexCapwapDot11WlanTable
           (u4currentWlanId, &u4nextWlanId));
    PNAC_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
*  Function Name : IssProcessWpaProtectedMgmtPageSet
*  Description   : This function processes the set request coming for the
*                  Rsna Protected Mngmt Table page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessWpaProtectedMgmtPageSet (tHttp * pHttp)
{
    INT4                i4ProtMgmtFrameEnabled = 0;
    INT4                i4UnProtMgmtFramAllowed = 0;
    UINT4               u4WlanId = 0;
    UINT4               u4ProfileId = 0;
    UINT4               u4AssocSAQueryMaxTImeOut = 0;
    UINT4               u4AssocSAQueryRetryTimeOut = 0;
    UINT4               u4AssocComeBackTime = 0;
    UINT4               u4ErrorCode = 0;

    STRCPY (pHttp->au1Name, "SSID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4WlanId = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ProtectedManagementFramesEnabled");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4ProtMgmtFrameEnabled = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "UnprotectedManagementFramesAllowed");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4UnProtMgmtFramAllowed = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "AssociationSAQueryMaximumTimeout");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4AssocSAQueryMaxTImeOut = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "AssociationSAQueryRetryTimeout");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4AssocSAQueryRetryTimeOut = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "AssociationComebacktime");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4AssocComeBackTime = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    WebnmRegisterLock (pHttp, PnacLock, PnacUnLock);
    PNAC_LOCK ();

    if (nmhGetCapwapDot11WlanProfileIfIndex
        (u4WlanId, (INT4 *) &u4ProfileId) != SNMP_SUCCESS)
    {
        PNAC_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Error in getting WlanIfIndex\n");
        return;
    }

    if (RsnaTestv2Dot11RSNAProtectedManagementFramesActivated (&u4ErrorCode,
                                                               (INT4)
                                                               u4ProfileId,
                                                               i4ProtMgmtFrameEnabled)
        != SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        PNAC_UNLOCK ();
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Invalid configuration - ProtectedManagementFramesEnabled\n");
        return;
    }

    if (RsnaSetDot11RSNAProtectedManagementFramesActivated ((INT4) u4ProfileId,
                                                            i4ProtMgmtFrameEnabled)
        != SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        PNAC_UNLOCK ();
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Error in setting - ProtectedManagementFramesEnabled \n");
        return;
    }

    if (RsnaTestv2Dot11RSNAUnprotectedManagementFramesAllowed (&u4ErrorCode,
                                                               (INT4)
                                                               u4ProfileId,
                                                               i4UnProtMgmtFramAllowed)
        != SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        PNAC_UNLOCK ();
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Invalid configuration - UnprotectedManagementFramesAllowed\n");
        return;
    }

    if (RsnaSetDot11RSNAUnprotectedManagementFramesAllowed ((INT4) u4ProfileId,
                                                            i4UnProtMgmtFramAllowed)
        != SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        PNAC_UNLOCK ();
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Error in setting - UnprotectedManagementFramesAllowed \n");
        return;
    }

    if (RsnaTestv2Dot11AssociationSAQueryMaximumTimeout (&u4ErrorCode,
                                                         (INT4) u4ProfileId,
                                                         u4AssocSAQueryMaxTImeOut)
        != SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        PNAC_UNLOCK ();
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Invalid configuration - AssociationSAQueryMaximumTimeout\n");
        return;
    }

    if (RsnaSetDot11AssociationSAQueryMaximumTimeout ((INT4) u4ProfileId,
                                                      u4AssocSAQueryMaxTImeOut)
        != SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        PNAC_UNLOCK ();
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Error in setting - AssociationSAQueryMaximumTimeout\n");
        return;
    }

    if (RsnaTestv2Dot11AssociationSAQueryRetryTimeout (&u4ErrorCode,
                                                       (INT4) u4ProfileId,
                                                       u4AssocSAQueryRetryTimeOut)
        != SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        PNAC_UNLOCK ();
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Invalid configuration - AssociationSAQueryRetryTimeout\n");
        return;
    }

    if (RsnaSetDot11AssociationSAQueryRetryTimeout ((INT4) u4ProfileId,
                                                    u4AssocSAQueryRetryTimeOut)
        != SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        PNAC_UNLOCK ();
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Error in setting - AssociationSAQueryRetryTimeout\n");
        return;
    }

    if (nmhTestv2FsRSNAAssociationComeBackTime (&u4ErrorCode,
                                                (INT4) u4ProfileId,
                                                u4AssocComeBackTime) !=
        SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        PNAC_UNLOCK ();
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Invalid configuration - AssociationComebacktime\n");
        return;
    }

    if (nmhSetFsRSNAAssociationComeBackTime ((INT4) u4ProfileId,
                                             u4AssocComeBackTime) !=
        SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        PNAC_UNLOCK ();
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Error in setting - AssociationComebacktime\n");
        return;
    }

    WebnmUnRegisterLock (pHttp);
    PNAC_UNLOCK ();
    IssProcessWpaProtectedMgmtPageGet (pHttp);
}

#endif
#endif
#endif
#endif
