/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: wlcweb.c,v 1.25 2018/02/15 10:22:24 siva Exp $
 *
 * Description: Routines for WSS CONTROLLER WEB Module
 *******************************************************************/

#ifndef _WSS_CONTROLLER_WEB_C_
#define _WSS_CONTROLLER_WEB_C_

#include "webiss.h"
#include "isshttp.h"
#include "webinc.h"
#include "issmacro.h"
#include "snmputil.h"
#include "utilcli.h"
#include "wsscfgcli.h"
#include "capwapcliinc.h"
#include "capwapclidefg.h"

#include "capwapcliprot.h"
#include "capwapcli.h"
#include "capwaptdfs.h"
#include "dtls.h"
#include "capwapprot.h"

#include "wsscfgwlanproto.h"
#include "wsscfgcli.h"
#include "wsscfgtdfsg.h"
#include "wsscfgtdfs.h"
#include "wsscfglwg.h"
#include "fsuserlw.h"
#include "usermacr.h"
#include "usertdfs.h"
#include "wssifwlanconst.h"
#include "wsscfgextn.h"

#define    wss_web_trace_enable      0

#define    WSS_WEB_TRC               if(wss_web_trace_enable)  printf
#define    MAX_RADIO_TYPE            6

/* extern INT4 CapwapTestAllFsWtpModelTable PROTO ((UINT4 *, 
 * tCapwapFsWtpModelEntry *, tCapwapIsSetFsWtpModelEntry *, INT4 , INT4));
 extern INT4 CapwapSetAllFsWtpModelTable PROTO ((tCapwapFsWtpModelEntry *, 
 tCapwapIsSetFsWtpModelEntry *, INT4 , INT4 )); */
extern INT4         WsscfgGetAllFsWtpImageUpgradeTable
PROTO ((tWsscfgFsWtpImageUpgradeEntry *));
extern INT4         WsscfgTestAllFsWtpImageUpgradeTable
PROTO ((UINT4 *, tWsscfgFsWtpImageUpgradeEntry *,
        tWsscfgIsSetFsWtpImageUpgradeEntry *, INT4, INT4));
extern INT4         WsscfgSetAllFsWtpImageUpgradeTable
PROTO ((tWsscfgFsWtpImageUpgradeEntry *, tWsscfgIsSetFsWtpImageUpgradeEntry *,
        INT4, INT4));
extern UINT1        gau1ModelNumber[CLI_MODEL_NAME_LEN];
UINT1               gau1CurrentModelNumber[OCTETSTR_SIZE];
/* UINT1 gau1CurrentEditModelNumber[OCTETSTR_SIZE]; */
INT4                gi4CurrentMaxSSID;
UINT4               gu4CurrentNoOfRadios;
BOOL1               bIsEdit;
UINT1               gu1ApImageUpgradestatus = WSS_IMAGE_UPGRADE_NOT_INITIATED;

/*For sql command*/

UINT1               u1Op[] = ":=";

#ifdef WLC_WANTED
UINT1               WSS_IMG_DOWNLOAD_FAIL[] =
    "<font color=red size=+2>Image Download Failed !";
UINT1               WSS_IMG_DOWNLOAD_SUCCESS[] =
    "<font color=red size=+2>Image Download Successful";
UINT1               WSS_IMG_DOWNLOAD_PROGRESS[] =
    "<font color=red size=+2>Image Download In Progress...";
UINT1               WSS_IMG_DOWNLOAD_INITIATED[] =
    "<font color=red size=+2>Image Download Not Initiated";
#endif
/*********************************************************************
*  Function Name : IssProcessControllerGeneralPage
*  Description   : This function processes the request coming for the
*                  Controller General Page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessControllerGeneralPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessControllerGeneralPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessControllerGeneralPageSet (pHttp);
    }

}

/*********************************************************************
*  Function Name : IssProcessControllerGeneralPageGet
*  Description   : This function processes the set request coming for the
*                  AP Model  page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessControllerGeneralPageGet (tHttp * pHttp)
{

    UINT1               au1ModelNumber[OCTETSTR_SIZE];
    UINT1               au1APSwVersion[OCTETSTR_SIZE];
    UINT1               au1SelectAp[OCTETSTR_SIZE];
    UINT1               au1ImageServerIPVersion[OCTETSTR_SIZE];
    UINT1               au1ImageServerIP[OCTETSTR_SIZE];
    UINT1               au1ImageName[OCTETSTR_SIZE];
    UINT1               au1ApName[OCTETSTR_SIZE];
    INT4                i4OutCome = 0, i4AddressType = 0, i4RetVal = 0;
    UINT4               currentProfileId = 0, nextProfileId = 0;
    UINT1               ProfileId[OCTETSTR_SIZE];
    tSNMP_OCTET_STRING_TYPE *currentModelNumber = NULL, *nextModelNumber = NULL,
        *WtpName = NULL, *ImageName = NULL,
        *ImageServerIPVer = NULL, *SwVersion = NULL, *ServerIp = NULL;
    tSNMP_OCTET_STRING_TYPE ProfileName;
    UINT1               au1ProfileName[256];
    tCapwapFsWtpModelEntry capwapFsWtpModelEntry;
    tSNMP_OCTET_STRING_TYPE *Softwareversion = NULL;

    tWsscfgFsWtpImageUpgradeEntry wsscfgFsWtpImageUpgradeEntry;
    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1ProfileName, 0, 256);
    MEMSET (ProfileId, 0, OCTETSTR_SIZE);
    ProfileName.pu1_OctetList = au1ProfileName;

    currentModelNumber =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (currentModelNumber == NULL)
    {
        return;
    }
    nextModelNumber =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (nextModelNumber == NULL)
    {
        free_octetstring (currentModelNumber);
        return;
    }
    WtpName =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (CLI_MODEL_NAME_LEN);
    if (WtpName == NULL)
    {
        free_octetstring (currentModelNumber);
        free_octetstring (nextModelNumber);
        return;
    }
    ImageName =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (CLI_MODEL_NAME_LEN);
    if (ImageName == NULL)
    {
        free_octetstring (currentModelNumber);
        free_octetstring (nextModelNumber);
        free_octetstring (WtpName);
        return;
    }
    ImageServerIPVer =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (ImageServerIPVer == NULL)
    {
        free_octetstring (currentModelNumber);
        free_octetstring (nextModelNumber);
        free_octetstring (WtpName);
        free_octetstring (ImageName);
        return;
    }
    SwVersion =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (SwVersion == NULL)
    {
        free_octetstring (currentModelNumber);
        free_octetstring (nextModelNumber);
        free_octetstring (WtpName);
        free_octetstring (ImageName);
        free_octetstring (ImageServerIPVer);
        return;
    }
    ServerIp = (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);

    if (ServerIp == NULL)
    {
        free_octetstring (currentModelNumber);
        free_octetstring (nextModelNumber);
        free_octetstring (WtpName);
        free_octetstring (ImageName);
        free_octetstring (ImageServerIPVer);
        free_octetstring (SwVersion);
        return;
    }
    Softwareversion =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (Softwareversion == NULL)
    {
        free_octetstring (currentModelNumber);
        free_octetstring (nextModelNumber);
        free_octetstring (WtpName);
        free_octetstring (ImageName);
        free_octetstring (ImageServerIPVer);
        free_octetstring (SwVersion);
        free_octetstring (ServerIp);
        return;
    }

    MEMSET (currentModelNumber->pu1_OctetList, 0, OCTETSTR_SIZE);

    MEMSET (WtpName->pu1_OctetList, 0, CLI_MODEL_NAME_LEN);
    MEMSET (ImageName->pu1_OctetList, 0, CLI_MODEL_NAME_LEN);
    MEMSET (ImageServerIPVer->pu1_OctetList, 0, OCTETSTR_SIZE);
    MEMSET (SwVersion->pu1_OctetList, 0, OCTETSTR_SIZE);
    MEMSET (ServerIp->pu1_OctetList, 0, OCTETSTR_SIZE);
    MEMSET (nextModelNumber->pu1_OctetList, 0, OCTETSTR_SIZE);
    MEMSET (&wsscfgFsWtpImageUpgradeEntry, 0,
            sizeof (tWsscfgFsWtpImageUpgradeEntry));
    MEMSET (Softwareversion->pu1_OctetList, 0, OCTETSTR_SIZE);

    Softwareversion->i4_Length = 0;

    currentModelNumber->i4_Length = 0;
    nextModelNumber->i4_Length = 0;
    WtpName->i4_Length = 0;
    ImageName->i4_Length = 0;
    ImageServerIPVer->i4_Length = 0;
    SwVersion->i4_Length = 0;
    ServerIp->i4_Length = 0;

    MEMSET (&au1ModelNumber, 0, OCTETSTR_SIZE);
    MEMSET (&au1APSwVersion, 0, OCTETSTR_SIZE);
    MEMSET (&au1SelectAp, 0, OCTETSTR_SIZE);
    MEMSET (&au1ImageServerIPVersion, 0, OCTETSTR_SIZE);
    MEMSET (&au1ImageServerIP, 0, OCTETSTR_SIZE);
    MEMSET (&au1ImageName, 0, OCTETSTR_SIZE);
    MEMSET (&au1ApName, 0, OCTETSTR_SIZE);
    MEMSET (&capwapFsWtpModelEntry, 0, sizeof (tCapwapFsWtpModelEntry));

    WebnmRegisterLock (pHttp, CapwapMainTaskLock, CapwapMainTaskUnLock);
    CAPWAP_LOCK;
    MEMSET (currentModelNumber->pu1_OctetList, 0, OCTETSTR_SIZE);
    currentModelNumber->i4_Length = 0;

    STRCPY (pHttp->au1Name, "APModelName");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    MEMCPY (wsscfgFsWtpImageUpgradeEntry.MibObject.
            au1CapwapBaseWtpProfileWtpModelNumber, pHttp->au1Value,
            MEM_MAX_BYTES ((sizeof
                            (wsscfgFsWtpImageUpgradeEntry.MibObject.
                             au1CapwapBaseWtpProfileWtpModelNumber) - 1),
                           STRLEN (pHttp->au1Value)));

    wsscfgFsWtpImageUpgradeEntry.MibObject.
        i4CapwapBaseWtpProfileWtpModelNumberLen =
        (INT4) STRLEN (wsscfgFsWtpImageUpgradeEntry.MibObject.
                       au1CapwapBaseWtpProfileWtpModelNumber);

    STRNCPY (capwapFsWtpModelEntry.MibObject.au1FsCapwapWtpModelNumber,
             wsscfgFsWtpImageUpgradeEntry.MibObject.
             au1CapwapBaseWtpProfileWtpModelNumber,
             MEM_MAX_BYTES ((sizeof
                             (capwapFsWtpModelEntry.MibObject.
                              au1FsCapwapWtpModelNumber) - 1),
                            STRLEN (wsscfgFsWtpImageUpgradeEntry.MibObject.
                                    au1CapwapBaseWtpProfileWtpModelNumber)));

    capwapFsWtpModelEntry.MibObject.i4FsCapwapWtpModelNumberLen =
        (INT4) STRLEN (capwapFsWtpModelEntry.MibObject.
                       au1FsCapwapWtpModelNumber);

    if (WsscfgGetAllFsWtpImageUpgradeTable (&wsscfgFsWtpImageUpgradeEntry) ==
        OSIX_SUCCESS)
    {
        MEMCPY (WtpName->pu1_OctetList,
                wsscfgFsWtpImageUpgradeEntry.MibObject.au1FsWtpName,
                wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpNameLen);
        WtpName->i4_Length =
            wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpNameLen;
        MEMCPY (ImageName->pu1_OctetList,
                wsscfgFsWtpImageUpgradeEntry.MibObject.au1FsWtpImageName,
                wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpImageNameLen);
        ImageName->i4_Length =
            wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpImageNameLen;
        i4AddressType =
            wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpAddressType;
        MEMCPY (SwVersion->pu1_OctetList,
                wsscfgFsWtpImageUpgradeEntry.MibObject.au1FsWtpImageVersion,
                wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpImageVersionLen);
        SwVersion->i4_Length =
            wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpImageVersionLen;
        MEMCPY (ServerIp->pu1_OctetList,
                wsscfgFsWtpImageUpgradeEntry.MibObject.au1FsWtpServerIP,
                wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpServerIPLen);

        ServerIp->i4_Length =
            wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpServerIPLen;
    }

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "ModelNumberKey");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
             wsscfgFsWtpImageUpgradeEntry.MibObject.
             au1CapwapBaseWtpProfileWtpModelNumber);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "SelectApKey");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", WtpName->pu1_OctetList);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    i4OutCome = nmhGetFirstIndexCapwapBaseWtpProfileTable (&nextProfileId);
    if (i4OutCome == SNMP_FAILURE)
    {
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        free_octetstring (currentModelNumber);
        free_octetstring (nextModelNumber);
        free_octetstring (WtpName);
        free_octetstring (ImageName);
        free_octetstring (ImageServerIPVer);
        free_octetstring (SwVersion);
        free_octetstring (ServerIp);
        free_octetstring (Softwareversion);
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        return;
    }

    STRCPY (pHttp->au1KeyString, "<! AP_NAME>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        currentProfileId = nextProfileId;

        if (currentProfileId != 0)
        {
            MEMSET (&au1ProfileName, 0, 256);
            MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
            ProfileName.pu1_OctetList = au1ProfileName;

            i4RetVal = CapwapUtilGetBaseWtpProfileName (currentProfileId,
                                                        &ProfileName,
                                                        wsscfgFsWtpImageUpgradeEntry.
                                                        MibObject.
                                                        au1CapwapBaseWtpProfileWtpModelNumber);
            if (SNMP_FAILURE == i4RetVal)
            {
                continue;
            }

            STRCPY (pHttp->au1Name, "AP_Name");
            SPRINTF ((CHR1 *) pHttp->au1DataString,
                     "<option value = \"%s\" selected> %s \n",
                     ProfileName.pu1_OctetList, ProfileName.pu1_OctetList);

        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (STRLEN (pHttp->au1DataString)));

    }
    while (nmhGetNextIndexCapwapBaseWtpProfileTable (currentProfileId,
                                                     &nextProfileId));

    STRCPY (pHttp->au1KeyString, "IpVerKey");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4AddressType);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "ServerIpKey");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", ServerIp->pu1_OctetList);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "ImageNameKey");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", ImageName->pu1_OctetList);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "status_key");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", gu1ApImageUpgradestatus);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "<! UPGRADE_STATUS>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    switch (gu1ApImageUpgradestatus)
    {
        case (WSS_IMAGE_UPGRADE_WAITING):
        {
            WebnmSockWrite (pHttp, WSS_IMG_DOWNLOAD_PROGRESS,
                            (INT4) (STRLEN (WSS_IMG_DOWNLOAD_PROGRESS)));
            break;
        }
        case (WSS_IMAGE_UPGRADE_FAILURE):
        {
            WebnmSockWrite (pHttp, WSS_IMG_DOWNLOAD_FAIL,
                            (INT4) (STRLEN (WSS_IMG_DOWNLOAD_FAIL)));
            break;
        }
        case (WSS_IMAGE_UPGRADE_SUCCESS):
        {
            WebnmSockWrite (pHttp, WSS_IMG_DOWNLOAD_SUCCESS,
                            (INT4) (STRLEN (WSS_IMG_DOWNLOAD_SUCCESS)));
            break;
        }
        default:
        {
            WebnmSockWrite (pHttp, WSS_IMG_DOWNLOAD_INITIATED,
                            (INT4) (STRLEN (WSS_IMG_DOWNLOAD_INITIATED)));
            break;
        }
    }

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    WebnmUnRegisterLock (pHttp);
    free_octetstring (currentModelNumber);
    free_octetstring (nextModelNumber);
    free_octetstring (WtpName);
    free_octetstring (ImageName);
    free_octetstring (ImageServerIPVer);
    free_octetstring (SwVersion);
    free_octetstring (ServerIp);
    free_octetstring (Softwareversion);
    CAPWAP_UNLOCK;

}

/*********************************************************************
*  Function Name : IssProcessControllerGeneralPageSet
*  Description   : This function processes the set request coming for the
*                  AP Model  page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessControllerGeneralPageSet (tHttp * pHttp)
{

    UINT1               au1ModelNumber[CLI_MAX_PROFILE_NAME];
    UINT1               au1APSwVersion[CLI_MAX_PROFILE_NAME];
    UINT1               au1SelectAp[CLI_MAX_PROFILE_NAME];
    UINT1               au1ImageServerIPVersion[CLI_MAX_PROFILE_NAME];
    tIPvXAddr           sIpAddress;
    UINT1               au1ImageServerIP[CLI_MAX_PROFILE_NAME];
    UINT1               au1ImageName[CLI_MAX_PROFILE_NAME];
    UINT1               au1ApName[CLI_MAX_PROFILE_NAME];
    UINT4               u4ErrorCode = 0, u4ApOption = 0;
    UINT4               currentProfileId = 0, nextProfileId = 0;
    INT4                i4APSwVersion = 0, i4AddrType = 0, i4OutCome =
        0, i4RetVal = 0;
    UINT1               TftpStr[300];

    tSNMP_OCTET_STRING_TYPE ProfileName;
    UINT1               au1ProfileName[256];

    tWsscfgFsWtpImageUpgradeEntry wsscfgFsWtpImageUpgradeEntry;
    tWsscfgIsSetFsWtpImageUpgradeEntry wsscfgIsSetFsWtpImageUpgradeEntry;
    tSNMP_OCTET_STRING_TYPE FsWtpModelNumber;
    tSNMP_OCTET_STRING_TYPE SetValFsCapwapImageName;
    MEMSET (&FsWtpModelNumber, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&SetValFsCapwapImageName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    MEMSET (&au1ModelNumber, 0, CLI_MAX_PROFILE_NAME);
    MEMSET (&au1APSwVersion, 0, CLI_MAX_PROFILE_NAME);
    MEMSET (&au1SelectAp, 0, CLI_MAX_PROFILE_NAME);
    MEMSET (&au1ImageServerIPVersion, 0, CLI_MAX_PROFILE_NAME);
    MEMSET (&au1ImageServerIP, 0, CLI_MAX_PROFILE_NAME);
    MEMSET (&sIpAddress, 0, sizeof (tIPvXAddr));
    MEMSET (&au1ImageName, 0, CLI_MAX_PROFILE_NAME);
    MEMSET (&au1ApName, 0, CLI_MAX_PROFILE_NAME);
    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ProfileName, 0, 256);
    MEMSET (TftpStr, 0, 300);

    WSS_WEB_TRC (" Entering IssProcessControllerGeneralPageSet !!\n");
    MEMSET (&wsscfgFsWtpImageUpgradeEntry, 0,
            sizeof (tWsscfgFsWtpImageUpgradeEntry));
    MEMSET (&wsscfgIsSetFsWtpImageUpgradeEntry, 0,
            sizeof (tWsscfgIsSetFsWtpImageUpgradeEntry));
    WebnmRegisterLock (pHttp, CapwapMainTaskLock, CapwapMainTaskUnLock);
    CAPWAP_LOCK;

    STRCPY (pHttp->au1Name, "MODEL_NUM");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ModelNumber, pHttp->au1Value, (sizeof (au1ModelNumber) - 1));

    STRCPY (pHttp->au1Name, "SelectAp");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4ApOption = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "AP_Name");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ApName, pHttp->au1Value, (sizeof (au1ApName) - 1));

    STRCPY (pHttp->au1Name, "IpVer");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4AddrType = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "SERVER_IP");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    MEMCPY (sIpAddress.au1Addr, (UINT1 *) pHttp->au1Value,
            (sizeof (sIpAddress.au1Addr) - 1));

    STRCPY (pHttp->au1Name, "IMAGE_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    MEMCPY (au1ImageName, pHttp->au1Value, (sizeof (au1ImageName) - 1));

    au1ImageName[MEM_MAX_BYTES ((sizeof (au1ImageName) - 1),
                                STRLEN (pHttp->au1Value))] = '\0';

    WSS_WEB_TRC
        ("values from General Page\n=================================\n");
    WSS_WEB_TRC ("Model Number :%s\n", au1ModelNumber);
    WSS_WEB_TRC ("au1APSwVersion :%d\n", i4APSwVersion);
    WSS_WEB_TRC ("ApName :%s \n", au1ApName);
    WSS_WEB_TRC ("ImageName    :%s \n", au1ImageName);
    WSS_WEB_TRC ("ImageServerIPVer :%d \n", i4AddrType);
    WSS_WEB_TRC ("Server IP   :%s \n", sIpAddress.au1Addr);

    STRCPY (wsscfgFsWtpImageUpgradeEntry.MibObject.au1FsWtpImageVersion,
            au1ImageServerIPVersion);
    wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpImageVersionLen =
        (INT4) STRLEN (au1ImageServerIPVersion);
    STRNCPY (wsscfgFsWtpImageUpgradeEntry.MibObject.au1FsWtpName, au1ApName,
             STRLEN (au1ApName));
    wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpNameLen =
        (INT4) STRLEN (au1ApName);
    STRCPY (wsscfgFsWtpImageUpgradeEntry.MibObject.au1FsWtpImageName,
            au1ImageName);
    wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpImageNameLen =
        (INT4) STRLEN (au1ImageName);
    STRNCPY (wsscfgFsWtpImageUpgradeEntry.MibObject.au1FsWtpServerIP,
             sIpAddress.au1Addr,
             MEM_MAX_BYTES ((sizeof
                             (wsscfgFsWtpImageUpgradeEntry.MibObject.
                              au1FsWtpServerIP) - 1),
                            STRLEN (sIpAddress.au1Addr)));
    wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpServerIPLen =
        STRLEN (sIpAddress.au1Addr);
    wsscfgFsWtpImageUpgradeEntry.MibObject.
        au1FsWtpServerIP[STRLEN (sIpAddress.au1Addr)] = '\0';

    STRCPY (wsscfgFsWtpImageUpgradeEntry.MibObject.
            au1CapwapBaseWtpProfileWtpModelNumber, au1ModelNumber);
    wsscfgFsWtpImageUpgradeEntry.MibObject.
        i4CapwapBaseWtpProfileWtpModelNumberLen =
        (INT4) STRLEN (au1ModelNumber);
    wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpRowStatus = ACTIVE;
    wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpUpgradeDev =
        (INT4) u4ApOption;
    wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpAddressType = i4AddrType;

    wsscfgIsSetFsWtpImageUpgradeEntry.bFsWtpUpgradeDev = OSIX_TRUE;
    wsscfgIsSetFsWtpImageUpgradeEntry.bFsWtpAddressType = OSIX_TRUE;
    wsscfgIsSetFsWtpImageUpgradeEntry.bFsWtpImageVersion = OSIX_TRUE;
    wsscfgIsSetFsWtpImageUpgradeEntry.bFsWtpName = OSIX_TRUE;
    wsscfgIsSetFsWtpImageUpgradeEntry.bFsWtpImageName = OSIX_TRUE;
    wsscfgIsSetFsWtpImageUpgradeEntry.bFsWtpServerIP = OSIX_TRUE;
    wsscfgIsSetFsWtpImageUpgradeEntry.bFsWtpRowStatus = OSIX_TRUE;
    wsscfgIsSetFsWtpImageUpgradeEntry.bCapwapBaseWtpProfileWtpModelNumber =
        OSIX_TRUE;

    SPRINTF ((CHR1 *) TftpStr, "tftp://%s/%s", sIpAddress.au1Addr,
             au1ImageName);
    gu1ApImageUpgradestatus = WSS_IMAGE_UPGRADE_WAITING;
    i4RetVal =
        WsscfgUtilImageTransfer ((INT1 *) &TftpStr, (INT1 *) &au1ImageName,
                                 &sIpAddress);

    if (SNMP_FAILURE == i4RetVal)
    {
        gu1ApImageUpgradestatus = WSS_IMAGE_UPGRADE_FAILURE;
        IssSendError (pHttp, (CONST INT1 *) "Image Transfer Failed\n");
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        return;

    }

    if (u4ApOption == WSSCFG_VERSION_AP_NAME_NONE)
    {
        if (SNMP_SUCCESS == i4RetVal)
        {
            gu1ApImageUpgradestatus = WSS_IMAGE_UPGRADE_SUCCESS;
            WebnmUnRegisterLock (pHttp);
            CAPWAP_UNLOCK;
            return;
        }
    }

    if (u4ApOption == WSSCFG_VERSION_AP_NAME_ALL)
    {
        i4OutCome = nmhGetFirstIndexCapwapBaseWtpProfileTable (&nextProfileId);

        if (i4OutCome == SNMP_FAILURE)
        {
            SetValFsCapwapImageName.pu1_OctetList = au1ImageName;
            SetValFsCapwapImageName.i4_Length = STRLEN (au1ImageName);
            FsWtpModelNumber.pu1_OctetList = au1ModelNumber;
            FsWtpModelNumber.i4_Length = STRLEN (au1ModelNumber);

            if (nmhSetFsCapwapImageName
                (&FsWtpModelNumber, &SetValFsCapwapImageName) == SNMP_FAILURE)
            {

                gu1ApImageUpgradestatus = WSS_IMAGE_UPGRADE_FAILURE;
                WebnmUnRegisterLock (pHttp);
                CAPWAP_UNLOCK;
                return;
            }
            WebnmUnRegisterLock (pHttp);
            CAPWAP_UNLOCK;
            return;
        }

        do
        {
            currentProfileId = nextProfileId;

            if (currentProfileId != 0)
            {
                MEMSET (au1ProfileName, 0, 256);
                MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
                ProfileName.pu1_OctetList = au1ProfileName;

                i4RetVal = CapwapUtilGetBaseWtpProfileName (currentProfileId,
                                                            &ProfileName,
                                                            au1ModelNumber);
                if (SNMP_FAILURE == i4RetVal)
                {
                    continue;
                }
                STRCPY (wsscfgFsWtpImageUpgradeEntry.MibObject.au1FsWtpName,
                        ProfileName.pu1_OctetList);
                wsscfgFsWtpImageUpgradeEntry.MibObject.i4FsWtpNameLen =
                    (INT4) STRLEN (ProfileName.pu1_OctetList);

                if (WsscfgTestAllFsWtpImageUpgradeTable
                    (&u4ErrorCode, &wsscfgFsWtpImageUpgradeEntry,
                     &wsscfgIsSetFsWtpImageUpgradeEntry, OSIX_TRUE,
                     OSIX_TRUE) != OSIX_SUCCESS)
                {
                    gu1ApImageUpgradestatus = WSS_IMAGE_UPGRADE_FAILURE;
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Invalid Configuration\n");
                    WebnmUnRegisterLock (pHttp);
                    CAPWAP_UNLOCK;
                    return;
                }
                if (WsscfgSetAllFsWtpImageUpgradeTable
                    (&wsscfgFsWtpImageUpgradeEntry,
                     &wsscfgIsSetFsWtpImageUpgradeEntry, OSIX_FALSE,
                     OSIX_TRUE) != OSIX_SUCCESS)
                {
                    gu1ApImageUpgradestatus = WSS_IMAGE_UPGRADE_FAILURE;
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Invalid Configuration\n");
                    WebnmUnRegisterLock (pHttp);
                    CAPWAP_UNLOCK;
                    return;
                }
            }
        }
        while (nmhGetNextIndexCapwapBaseWtpProfileTable (currentProfileId,
                                                         &nextProfileId));
        gu1ApImageUpgradestatus = WSS_IMAGE_UPGRADE_SUCCESS;

    }
    else
    {
        if (WsscfgTestAllFsWtpImageUpgradeTable (&u4ErrorCode,
                                                 &wsscfgFsWtpImageUpgradeEntry,
                                                 &wsscfgIsSetFsWtpImageUpgradeEntry,
                                                 OSIX_TRUE,
                                                 OSIX_TRUE) != OSIX_SUCCESS)
        {
            gu1ApImageUpgradestatus = WSS_IMAGE_UPGRADE_FAILURE;
            IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
            WebnmUnRegisterLock (pHttp);
            CAPWAP_UNLOCK;
            return;
        }
        if (WsscfgSetAllFsWtpImageUpgradeTable (&wsscfgFsWtpImageUpgradeEntry,
                                                &wsscfgIsSetFsWtpImageUpgradeEntry,
                                                OSIX_FALSE,
                                                OSIX_TRUE) != OSIX_SUCCESS)
        {
            gu1ApImageUpgradestatus = WSS_IMAGE_UPGRADE_FAILURE;
            IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
            WebnmUnRegisterLock (pHttp);
            CAPWAP_UNLOCK;
            return;
        }
        gu1ApImageUpgradestatus = WSS_IMAGE_UPGRADE_SUCCESS;
    }
    WebnmUnRegisterLock (pHttp);
    CAPWAP_UNLOCK;
    return;
}

/*********************************************************************
*  Function Name : IssProcessAPModelEntryPage 
*  Description   : This function processes the request coming for the 
*                  AP Model Entry Page 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessAPModelEntryPage (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE *Model;
    Model = (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);;
    if (Model == NULL)
    {
        return;
    }
    MEMSET (Model->pu1_OctetList, 0, OCTETSTR_SIZE);
    Model->i4_Length = 0;
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessAPModelEntryPageGet (pHttp, Model);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessAPModelEntryPageSet (pHttp);
    }
    free_octetstring (Model);
}

/*********************************************************************
*  Function Name : IssProcessAPModelEntryPageGet 
*  Description   : This function processes the request coming for the 
*                  AP Model Entry Page 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessAPModelEntryPageGet (tHttp * pHttp,
                               tSNMP_OCTET_STRING_TYPE * ModelNumber)
{
    tSNMP_OCTET_STRING_TYPE *Tunnel = NULL;
    tSNMP_OCTET_STRING_TYPE *Softwareversion = NULL;
    tSNMP_OCTET_STRING_TYPE *Imageversion = NULL;
    tSNMP_OCTET_STRING_TYPE *QosProfile;
    tSNMP_OCTET_STRING_TYPE *pCurrentProfileName = NULL, *pNextProfileName =
        NULL;
    INT4                i4OutCome = 0, i4Mactype = 0;
    UINT4               u4NoOfRadios = 0;
    INT4                i4CurrentIndex = 0, i4NextIndex = 0;
    tCapwapFsWtpModelEntry capwapFsWtpModelEntry;

    if (CapwapCheckModuleStatus () == OSIX_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Capwap module is shutdown. Can't configure!");
        return;
    }

    pCurrentProfileName =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (pCurrentProfileName == NULL)
    {
        return;
    }
    pNextProfileName =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (pNextProfileName == NULL)
    {
        free_octetstring (pCurrentProfileName);
        return;
    }

    pCurrentProfileName->i4_Length = 0;

    pNextProfileName->i4_Length = 0;

    Imageversion =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (Imageversion == NULL)
    {
        free_octetstring (pCurrentProfileName);
        free_octetstring (pNextProfileName);
        return;
    }
    Softwareversion =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);

    if (Softwareversion == NULL)
    {
        free_octetstring (pCurrentProfileName);
        free_octetstring (pNextProfileName);
        free_octetstring (Imageversion);
        return;
    }
    QosProfile =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (QosProfile == NULL)
    {
        free_octetstring (pCurrentProfileName);
        free_octetstring (pNextProfileName);
        free_octetstring (Softwareversion);
        free_octetstring (Imageversion);
        return;
    }
    Tunnel = (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (Tunnel == NULL)
    {
        free_octetstring (pCurrentProfileName);
        free_octetstring (pNextProfileName);
        free_octetstring (Softwareversion);
        free_octetstring (Imageversion);
        free_octetstring (QosProfile);
        return;
    }
    MEMSET (pNextProfileName->pu1_OctetList, 0, OCTETSTR_SIZE);
    MEMSET (pCurrentProfileName->pu1_OctetList, 0, OCTETSTR_SIZE);
    MEMSET (&capwapFsWtpModelEntry, 0, sizeof (tCapwapFsWtpModelEntry));
    MEMSET (QosProfile->pu1_OctetList, 0, OCTETSTR_SIZE);

    QosProfile->i4_Length = 0;

    MEMSET (pCurrentProfileName->pu1_OctetList, 0, OCTETSTR_SIZE);
    pCurrentProfileName->i4_Length = 0;

    MEMSET (Softwareversion->pu1_OctetList, 0, OCTETSTR_SIZE);
    Softwareversion->i4_Length = 0;

    MEMSET (Imageversion->pu1_OctetList, 0, OCTETSTR_SIZE);
    Imageversion->i4_Length = 0;

    MEMSET (Tunnel->pu1_OctetList, 0, OCTETSTR_SIZE);
    Tunnel->i4_Length = 0;

    WebnmRegisterLock (pHttp, CapwapMainTaskLock, CapwapMainTaskUnLock);
    CAPWAP_LOCK;

    MEMCPY (capwapFsWtpModelEntry.MibObject.au1FsCapwapWtpModelNumber,
            ModelNumber->pu1_OctetList, ModelNumber->i4_Length);
    capwapFsWtpModelEntry.MibObject.i4FsCapwapWtpModelNumberLen =
        ModelNumber->i4_Length;
    if (CapwapGetAllFsWtpModelTable (&capwapFsWtpModelEntry) == OSIX_SUCCESS)
    {
        u4NoOfRadios = capwapFsWtpModelEntry.MibObject.u4FsNoOfRadio;
        i4Mactype = capwapFsWtpModelEntry.MibObject.i4FsCapwapWtpMacType;
        MEMCPY (Tunnel->pu1_OctetList,
                capwapFsWtpModelEntry.MibObject.au1FsCapwapWtpTunnelMode,
                capwapFsWtpModelEntry.MibObject.i4FsCapwapWtpTunnelModeLen);
        Tunnel->i4_Length =
            capwapFsWtpModelEntry.MibObject.i4FsCapwapWtpTunnelModeLen;

        MEMCPY (Imageversion->pu1_OctetList,
                capwapFsWtpModelEntry.MibObject.au1FsCapwapImageName,
                capwapFsWtpModelEntry.MibObject.i4FsCapwapImageNameLen);
        Imageversion->i4_Length =
            capwapFsWtpModelEntry.MibObject.i4FsCapwapImageNameLen;
        MEMCPY (QosProfile->pu1_OctetList,
                capwapFsWtpModelEntry.MibObject.au1FsCapwapQosProfileName,
                capwapFsWtpModelEntry.MibObject.i4FsCapwapQosProfileNameLen);
        QosProfile->i4_Length =
            capwapFsWtpModelEntry.MibObject.i4FsCapwapQosProfileNameLen;

    }

    STRCPY (pHttp->au1KeyString, "MODEL_NUM_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", ModelNumber->pu1_OctetList);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));
    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "NO_OF_RADIOS_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    if (u4NoOfRadios)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4NoOfRadios);
    }
    else
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "");
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));
    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "CAPWAP_MACTYPE_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Mactype);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));
    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "CAPWAP_TUNNELMODE_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", Tunnel->pu1_OctetList[0]);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));
    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    /*
       i4OutCome = nmhGetFirstIndexFsDot11QosProfileTable(pNextProfileName); */
    i4OutCome =
        nmhGetFirstIndexFsQAPProfileTable (pNextProfileName, &i4NextIndex);
    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }

    STRCPY (pHttp->au1KeyString, "<! QOS_PROFILES>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        if (STRCMP (pCurrentProfileName->pu1_OctetList,
                    pNextProfileName->pu1_OctetList) != 0)
        {
            STRCPY (pHttp->au1Name, "QosProfile");
            SPRINTF ((CHR1 *) pHttp->au1DataString,
                     "<option value = \"%s\" >%s \n",
                     pNextProfileName->pu1_OctetList,
                     pNextProfileName->pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) (STRLEN (pHttp->au1DataString)));

            WSS_WEB_TRC ("pNextProfileName : %s\n",
                         pNextProfileName->pu1_OctetList);
        }
        else
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        }
        /*Copy next index to current index */
        STRCPY (pCurrentProfileName->pu1_OctetList,
                pNextProfileName->pu1_OctetList);
        pCurrentProfileName->i4_Length = pNextProfileName->i4_Length;
        MEMSET (pNextProfileName->pu1_OctetList, 0, OCTETSTR_SIZE);
        pNextProfileName->i4_Length = 0;
        i4CurrentIndex = i4NextIndex;
    }
    while (nmhGetNextIndexFsQAPProfileTable (pCurrentProfileName,
                                             pNextProfileName, i4CurrentIndex,
                                             &i4NextIndex) == SNMP_SUCCESS);

    STRCPY (pHttp->au1KeyString, "IMAGE_VER_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", Imageversion->pu1_OctetList);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));
    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    WebnmUnRegisterLock (pHttp);
    CAPWAP_UNLOCK;
    free_octetstring (pNextProfileName);
    free_octetstring (pCurrentProfileName);
    free_octetstring (Softwareversion);
    free_octetstring (Imageversion);
    free_octetstring (QosProfile);
    free_octetstring (Tunnel);
}

/*********************************************************************
*  Function Name : IssProcessAPModelEntryEditPage 
*  Description   : This function processes the request coming for the 
*                  AP Model Entry Page 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessAPModelEntryEditPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessAPModelEntryEditPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessAPModelEntryEditPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessAPModelEntryEditPageGet 
*  Description   : This function processes the set request coming for the  
*                  AP Model Entry page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessAPModelEntryEditPageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE *ModelNumber = NULL;
    tSNMP_OCTET_STRING_TYPE *Tunnel = NULL;
    tSNMP_OCTET_STRING_TYPE *Softwareversion = NULL;
    tSNMP_OCTET_STRING_TYPE *Imageversion = NULL;
    tSNMP_OCTET_STRING_TYPE *QosProfile = NULL;
    tSNMP_OCTET_STRING_TYPE *HwVersion = NULL;
    INT4                i4Mactype = 0;
    tCapwapFsWtpModelEntry capwapFsWtpModelEntry;
    INT4                i4CurrentIndex = 0, i4NextIndex = 0;
    UINT1               NullModelNumber[OCTETSTR_SIZE];
    tSNMP_OCTET_STRING_TYPE *pCurrentProfileName = NULL,
        *pNextProfileName = NULL;
    INT4                i4OutCome = 0;

    pCurrentProfileName =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (pCurrentProfileName == NULL)
    {
        return;
    }
    pNextProfileName =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (pNextProfileName == NULL)
    {
        free_octetstring (pCurrentProfileName);
        return;
    }
    Softwareversion =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (Softwareversion == NULL)
    {
        free_octetstring (pCurrentProfileName);
        free_octetstring (pNextProfileName);
        return;
    }
    Imageversion =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (Imageversion == NULL)
    {
        free_octetstring (pCurrentProfileName);
        free_octetstring (pNextProfileName);
        free_octetstring (Softwareversion);
        return;
    }
    QosProfile =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (QosProfile == NULL)
    {
        free_octetstring (pCurrentProfileName);
        free_octetstring (pNextProfileName);
        free_octetstring (Softwareversion);
        free_octetstring (Imageversion);
        return;
    }
    HwVersion =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (HwVersion == NULL)
    {
        free_octetstring (pCurrentProfileName);
        free_octetstring (pNextProfileName);
        free_octetstring (Softwareversion);
        free_octetstring (Imageversion);
        free_octetstring (QosProfile);
        return;
    }
    MEMSET (QosProfile->pu1_OctetList, 0, OCTETSTR_SIZE);
    QosProfile->i4_Length = 0;

    MEMSET (pCurrentProfileName->pu1_OctetList, 0, OCTETSTR_SIZE);
    pCurrentProfileName->i4_Length = 0;

    MEMSET (Softwareversion->pu1_OctetList, 0, OCTETSTR_SIZE);
    Softwareversion->i4_Length = 0;

    MEMSET (Imageversion->pu1_OctetList, 0, OCTETSTR_SIZE);
    Imageversion->i4_Length = 0;

    MEMSET (HwVersion->pu1_OctetList, 0, OCTETSTR_SIZE);
    HwVersion->i4_Length = 0;

    MEMSET (&capwapFsWtpModelEntry, 0, sizeof (tCapwapFsWtpModelEntry));
    MEMSET (pNextProfileName->pu1_OctetList, 0, OCTETSTR_SIZE);
    bIsEdit = OSIX_TRUE;

    STRCPY (pHttp->au1Name, "AP_MODEL_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (gau1CurrentModelNumber, pHttp->au1Value,
             (sizeof (gau1CurrentModelNumber) - 1));

    STRCPY (pHttp->au1KeyString, "AP_MODEL_NAME_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    Tunnel = (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (Tunnel == NULL)
    {
        free_octetstring (pCurrentProfileName);
        free_octetstring (pNextProfileName);
        free_octetstring (Softwareversion);
        free_octetstring (Imageversion);
        free_octetstring (HwVersion);
        free_octetstring (QosProfile);
        return;
    }
    MEMSET (Tunnel->pu1_OctetList, 0, OCTETSTR_SIZE);
    MEMSET (NullModelNumber, '\0', OCTETSTR_SIZE);
    if ((STRCMP (gau1CurrentModelNumber, NullModelNumber)) != OSIX_FALSE)
    {
        ModelNumber =
            (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
        if (ModelNumber == NULL)
        {
            free_octetstring (pCurrentProfileName);
            free_octetstring (pNextProfileName);
            free_octetstring (Softwareversion);
            free_octetstring (Imageversion);
            free_octetstring (QosProfile);
            free_octetstring (HwVersion);
            free_octetstring (Tunnel);
            return;
        }
        MEMSET (ModelNumber->pu1_OctetList, 0, OCTETSTR_SIZE);
        STRCPY (ModelNumber->pu1_OctetList, gau1CurrentModelNumber);
        ModelNumber->i4_Length = (INT4) STRLEN (gau1CurrentModelNumber);
        MEMCPY (capwapFsWtpModelEntry.MibObject.au1FsCapwapWtpModelNumber,
                ModelNumber->pu1_OctetList, ModelNumber->i4_Length);
        capwapFsWtpModelEntry.MibObject.i4FsCapwapWtpModelNumberLen =
            ModelNumber->i4_Length;
        if (CapwapGetAllFsWtpModelTable (&capwapFsWtpModelEntry) ==
            OSIX_SUCCESS)
        {
            gu4CurrentNoOfRadios =
                capwapFsWtpModelEntry.MibObject.u4FsNoOfRadio;
            i4Mactype = capwapFsWtpModelEntry.MibObject.i4FsCapwapWtpMacType;
            MEMCPY (Tunnel->pu1_OctetList,
                    capwapFsWtpModelEntry.MibObject.au1FsCapwapWtpTunnelMode,
                    capwapFsWtpModelEntry.MibObject.i4FsCapwapWtpTunnelModeLen);
            Tunnel->i4_Length =
                capwapFsWtpModelEntry.MibObject.i4FsCapwapWtpTunnelModeLen;

            MEMCPY (QosProfile->pu1_OctetList,
                    capwapFsWtpModelEntry.MibObject.au1FsCapwapQosProfileName,
                    capwapFsWtpModelEntry.MibObject.
                    i4FsCapwapQosProfileNameLen);
            QosProfile->i4_Length =
                capwapFsWtpModelEntry.MibObject.i4FsCapwapQosProfileNameLen;

        }
    }
    WebnmRegisterLock (pHttp, CapwapMainTaskLock, CapwapMainTaskUnLock);
    CAPWAP_LOCK;

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", gau1CurrentModelNumber);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "NO_OF_RADIOS_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", gu4CurrentNoOfRadios);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "CAPWAP_MAC_TYPE_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Mactype);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "CAPWAP_TUNNEL_MODE_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    WSS_WEB_TRC ("Tunnel Mode[0]: %d\n", Tunnel->pu1_OctetList[0]);

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", Tunnel->pu1_OctetList[0]);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    i4OutCome =
        nmhGetFirstIndexFsQAPProfileTable (pNextProfileName, &i4NextIndex);
    if (i4OutCome == SNMP_SUCCESS)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }

    STRCPY (pHttp->au1KeyString, "<! QOS PROFILES>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        if (STRCMP (pCurrentProfileName->pu1_OctetList,
                    pNextProfileName->pu1_OctetList) != 0)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString,
                     "<option value = \"%s\" >%s \n",
                     pNextProfileName->pu1_OctetList,
                     pNextProfileName->pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) (STRLEN (pHttp->au1DataString)));
        }
        else
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        }
        WSS_WEB_TRC ("pNextProfileName : %s\n",
                     pNextProfileName->pu1_OctetList);

        /*Copy next index to current index */
        STRCPY (pCurrentProfileName->pu1_OctetList,
                pNextProfileName->pu1_OctetList);
        pCurrentProfileName->i4_Length = pNextProfileName->i4_Length;
        MEMSET (pNextProfileName->pu1_OctetList, 0, OCTETSTR_SIZE);
        pNextProfileName->i4_Length = 0;
        i4CurrentIndex = i4NextIndex;
    }
    while (nmhGetNextIndexFsQAPProfileTable
           (pCurrentProfileName, pNextProfileName, i4CurrentIndex,
            &i4NextIndex) == SNMP_SUCCESS);

    if (nmhGetFsCapwapImageName (ModelNumber, Imageversion) == SNMP_SUCCESS)
    {
        STRCPY (pHttp->au1KeyString, "IMAGE_VERSION_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                 Imageversion->pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (HTTP_STRLEN (pHttp->au1DataString)));
    }
    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    if (nmhGetFsCapwapSwVersion (ModelNumber, Softwareversion) == SNMP_SUCCESS)
    {
        STRCPY (pHttp->au1KeyString, "SOFTWARE_VERSION_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                 Softwareversion->pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
    }
    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    if (nmhGetFsCapwapHwVersion (ModelNumber, HwVersion) == SNMP_SUCCESS)
    {
        STRCPY (pHttp->au1KeyString, "HARDWARE_VERSION_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", HwVersion->pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
    }
    STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    WebnmUnRegisterLock (pHttp);
    CAPWAP_UNLOCK;
    free_octetstring (pCurrentProfileName);
    free_octetstring (pNextProfileName);
    free_octetstring (Softwareversion);
    free_octetstring (HwVersion);
    free_octetstring (Imageversion);
    free_octetstring (QosProfile);
    free_octetstring (Tunnel);
    free_octetstring (ModelNumber);

}

/*********************************************************************
*  Function Name : IssProcessAPModelEntryEditPageSet 
*  Description   : This function processes the set request coming for the  
*                  AP Model Entry page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessAPModelEntryEditPageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE *ModelNumber = NULL;
    tSNMP_OCTET_STRING_TYPE *Tunnel = NULL;
    INT4                macType = 0;
    INT4                i4MacType = 0;
    UINT1               tunnelMode[VLAN_STATIC_MAX_NAME_LEN + 1];
    UINT1               au1ModelName[CLI_MODEL_NAME_LEN];
    UINT1               qosProfile[CLI_MODEL_NAME_LEN];
    UINT1               swVer[CLI_MODEL_NAME_LEN];
    UINT1               imgVer[CLI_MODEL_NAME_LEN];
    UINT4               noOfRadio;
    UINT4               u4ErrorCode;
    INT4                i4ModelNameLen = 0;
    INT4                i4TunnelModeLen = 1;
    UINT1               tunnelModeBit = 0;
    UINT1               au1CurrentTime[ISS_PAGE_STRING_SIZE];
    tCapwapFsWtpModelEntry capwapSetFsWtpModelEntry;
    tCapwapFsWtpModelEntry capwapFsWtpModelEntry;
    tCapwapIsSetFsWtpModelEntry capwapIsSetFsWtpModelEntry;
    time_t              t = time (NULL);
    struct tm          *tm = NULL;

    tm = localtime (&t);

    if (tm == NULL)
    {
        return;
    }

    Tunnel = (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (Tunnel == NULL)
    {
        free_octetstring (Tunnel);
        return;
    }
    MEMSET (Tunnel->pu1_OctetList, 0, OCTETSTR_SIZE);

    ModelNumber =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (ModelNumber == NULL)
    {
        free_octetstring (Tunnel);
        return;
    }
    MEMSET (ModelNumber->pu1_OctetList, 0, OCTETSTR_SIZE);

    MEMSET (&capwapSetFsWtpModelEntry, 0, sizeof (tCapwapFsWtpModelEntry));
    MEMSET (&capwapFsWtpModelEntry, 0, sizeof (tCapwapFsWtpModelEntry));
    MEMSET (&capwapIsSetFsWtpModelEntry, 0,
            sizeof (tCapwapIsSetFsWtpModelEntry));

    MEMSET (&au1ModelName, 0, CLI_MODEL_NAME_LEN);
    MEMSET (&qosProfile, 0, CLI_MODEL_NAME_LEN);
    MEMSET (&swVer, 0, CLI_MODEL_NAME_LEN);
    MEMSET (&imgVer, 0, CLI_MODEL_NAME_LEN);
    MEMSET (&tunnelMode, 0, VLAN_STATIC_MAX_NAME_LEN + 1);
    MEMSET (&au1CurrentTime, 0, ISS_PAGE_STRING_SIZE);

    SPRINTF ((CHR1 *) au1CurrentTime, "%d-%02d-%02d %02d:%02d:%02d",
             tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday, tm->tm_hour,
             tm->tm_min, tm->tm_sec);

    WebnmRegisterLock (pHttp, CapwapMainTaskLock, CapwapMainTaskUnLock);
    CAPWAP_LOCK;

    STRCPY (pHttp->au1Name, "AP_MODEL_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ModelName, pHttp->au1Value, (sizeof (au1ModelName) - 1));
    STRNCPY (gau1CurrentModelNumber, au1ModelName,
             (sizeof (gau1CurrentModelNumber) - 1));
    STRCPY (pHttp->au1Name, "NO_OF_RADIOS");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    noOfRadio = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "CAPWAP_MAC_TYPE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    macType = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "CAPWAP_TUNNEL_MODE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    tunnelModeBit = (UINT1) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "QosProfile");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (qosProfile, pHttp->au1Value, (sizeof (qosProfile) - 1));

    STRCPY (pHttp->au1Name, "IMAGE_VERSION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (imgVer, pHttp->au1Value, (sizeof (imgVer) - 1));

    i4ModelNameLen = (INT4) STRLEN (au1ModelName);
    i4TunnelModeLen = 1;

    STRCPY (ModelNumber->pu1_OctetList, au1ModelName);
    ModelNumber->i4_Length = (INT4) STRLEN (au1ModelName);

    MEMCPY (capwapFsWtpModelEntry.MibObject.au1FsCapwapWtpModelNumber,
            ModelNumber->pu1_OctetList, ModelNumber->i4_Length);
    capwapFsWtpModelEntry.MibObject.i4FsCapwapWtpModelNumberLen =
        ModelNumber->i4_Length;

    if (CapwapGetAllFsWtpModelTable (&capwapFsWtpModelEntry) == OSIX_SUCCESS)
    {
    }

    i4MacType = capwapFsWtpModelEntry.MibObject.i4FsCapwapWtpMacType;

    capwapSetFsWtpModelEntry.MibObject.u4FsNoOfRadio = noOfRadio;
    capwapSetFsWtpModelEntry.MibObject.i4FsCapwapWtpMacType = macType;
    STRNCPY (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapWtpModelNumber,
             au1ModelName,
             (sizeof
              (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapWtpModelNumber) -
              1));
    capwapSetFsWtpModelEntry.MibObject.i4FsCapwapWtpModelNumberLen =
        i4ModelNameLen;

    if (capwapSetFsWtpModelEntry.MibObject.i4FsCapwapWtpMacType ==
        CLI_MAC_TYPE_SPLIT)
    {
        capwapSetFsWtpModelEntry.MibObject.au1FsCapwapWtpTunnelMode[0] =
            CLI_TUNNEL_MODE_NATIVE;
    }
    else
    {
        capwapSetFsWtpModelEntry.MibObject.au1FsCapwapWtpTunnelMode[0] =
            tunnelModeBit;
    }
    capwapSetFsWtpModelEntry.MibObject.i4FsCapwapWtpTunnelModeLen =
        i4TunnelModeLen;
    STRCPY (capwapSetFsWtpModelEntry.au1LastUpdated, au1CurrentTime);
    capwapSetFsWtpModelEntry.i4LastUpdatedLen = (INT4) STRLEN (au1CurrentTime);
    STRNCPY (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapImageName,
             imgVer,
             (sizeof (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapImageName) -
              1));
    capwapSetFsWtpModelEntry.MibObject.i4FsCapwapImageNameLen =
        (INT4) STRLEN (imgVer);
    STRNCPY (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapQosProfileName,
             qosProfile,
             (sizeof
              (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapQosProfileName) -
              1));
    capwapSetFsWtpModelEntry.MibObject.i4FsCapwapQosProfileNameLen =
        (INT4) STRLEN (qosProfile);

    /* Fill the booleans indicating presence of corresponding field */
    if ((gu4CurrentNoOfRadios != noOfRadio) || (i4MacType != macType))
    {
        capwapIsSetFsWtpModelEntry.bFsCapwapWtpModelNumber = OSIX_TRUE;
        capwapIsSetFsWtpModelEntry.bFsNoOfRadio = OSIX_TRUE;
        capwapIsSetFsWtpModelEntry.bFsCapwapWtpMacType = OSIX_TRUE;
        capwapIsSetFsWtpModelEntry.bFsCapwapWtpTunnelMode = OSIX_TRUE;
        capwapIsSetFsWtpModelEntry.bFsWtpModelRowStatus = OSIX_TRUE;
        capwapIsSetFsWtpModelEntry.bFsCapwapQosProfileName = OSIX_TRUE;
        capwapIsSetFsWtpModelEntry.bFsCapwapImageName = OSIX_TRUE;
    }
    else
    {
        capwapIsSetFsWtpModelEntry.bFsCapwapWtpModelNumber = OSIX_TRUE;
        capwapIsSetFsWtpModelEntry.bFsNoOfRadio = OSIX_FALSE;
        capwapIsSetFsWtpModelEntry.bFsCapwapWtpMacType = OSIX_FALSE;
        capwapIsSetFsWtpModelEntry.bFsCapwapWtpTunnelMode = OSIX_FALSE;
        capwapIsSetFsWtpModelEntry.bFsWtpModelRowStatus = OSIX_FALSE;
        capwapIsSetFsWtpModelEntry.bFsCapwapQosProfileName = OSIX_FALSE;
        capwapIsSetFsWtpModelEntry.bFsCapwapImageName = OSIX_TRUE;
    }

    capwapSetFsWtpModelEntry.MibObject.i4FsWtpModelRowStatus = NOT_IN_SERVICE;

    if (CapwapTestAllFsWtpModelTable (&u4ErrorCode, &capwapSetFsWtpModelEntry,
                                      &capwapIsSetFsWtpModelEntry, OSIX_TRUE,
                                      OSIX_TRUE) != OSIX_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        free_octetstring (Tunnel);
        free_octetstring (ModelNumber);
        return;
    }

    if (CapwapSetAllFsWtpModelTable (&capwapSetFsWtpModelEntry,
                                     &capwapIsSetFsWtpModelEntry, OSIX_TRUE,
                                     OSIX_TRUE) != OSIX_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        free_octetstring (Tunnel);
        free_octetstring (ModelNumber);
        return;
    }

    capwapSetFsWtpModelEntry.MibObject.i4FsWtpModelRowStatus = ACTIVE;

    if (CapwapTestAllFsWtpModelTable (&u4ErrorCode, &capwapSetFsWtpModelEntry,
                                      &capwapIsSetFsWtpModelEntry, OSIX_TRUE,
                                      OSIX_TRUE) != OSIX_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        free_octetstring (Tunnel);
        free_octetstring (ModelNumber);
        return;
    }

    if (CapwapSetAllFsWtpModelTable (&capwapSetFsWtpModelEntry,
                                     &capwapIsSetFsWtpModelEntry, OSIX_TRUE,
                                     OSIX_TRUE) != OSIX_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        free_octetstring (Tunnel);
        free_octetstring (ModelNumber);
        return;
    }

    WebnmUnRegisterLock (pHttp);
    free_octetstring (Tunnel);
    free_octetstring (ModelNumber);
    CAPWAP_UNLOCK;
    IssProcessAPModelEntryEditPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssProcessAPModelEntryPageSet 
*  Description   : This function processes the set request coming for the  
*                  AP Model Entry page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessAPModelEntryPageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE *pModelNumber = NULL, *pQos = NULL, *pSwVer = NULL;
    UINT1               au1ModelName[CLI_MODEL_NAME_LEN];
    UINT1               au1QosProfile[CLI_MODEL_NAME_LEN];
    UINT1               au1SwVer[CLI_MODEL_NAME_LEN];
    UINT1               au1ImgVer[CLI_MODEL_NAME_LEN];
    INT4                i4MacType = 0;
    UINT1               au1TunnelMode[OCTETSTR_SIZE];
    UINT1               au1CurrentTime[ISS_PAGE_STRING_SIZE];
    UINT4               u4NoOfRadios = 0;
    INT4                i4ModelNameLen = 0, i4TunnelModeLen = 0;
    UINT4               u4ErrorCode = 0;
    UINT1               u1TunnelModeBit = 0;
    INT4                i4FsWtpModelRowStatus = NOT_IN_SERVICE;

    time_t              t = time (NULL);
    struct tm          *tm = NULL;

    tm = localtime (&t);

    if (tm == NULL)
    {
        return;
    }

    WSS_WEB_TRC (" Entering IssProcessAPModelEntryPageSet \n");

    tCapwapFsWtpModelEntry capwapSetFsWtpModelEntry;
    tCapwapIsSetFsWtpModelEntry capwapIsSetFsWtpModelEntry;

    pModelNumber =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (pModelNumber == NULL)
    {
        return;
    }

    pQos = (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (pQos == NULL)
    {
        free_octetstring (pModelNumber);
        return;
    }

    pSwVer = (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (pSwVer == NULL)
    {
        free_octetstring (pModelNumber);
        free_octetstring (pQos);
        return;
    }

    MEMSET (pModelNumber->pu1_OctetList, 0, OCTETSTR_SIZE);
    MEMSET (pQos->pu1_OctetList, 0, OCTETSTR_SIZE);
    MEMSET (pSwVer->pu1_OctetList, 0, OCTETSTR_SIZE);

    pQos->i4_Length = 0;
    pSwVer->i4_Length = 0;
    MEMSET (&capwapSetFsWtpModelEntry, 0, sizeof (tCapwapFsWtpModelEntry));
    MEMSET (&capwapIsSetFsWtpModelEntry, 0,
            sizeof (tCapwapIsSetFsWtpModelEntry));

    MEMSET (&capwapSetFsWtpModelEntry, 0, sizeof (capwapSetFsWtpModelEntry));
    MEMSET (&capwapIsSetFsWtpModelEntry, 0,
            sizeof (capwapIsSetFsWtpModelEntry));

    MEMSET (&au1ModelName, 0, CLI_MODEL_NAME_LEN);
    MEMSET (&au1QosProfile, 0, CLI_MODEL_NAME_LEN);
    MEMSET (&au1SwVer, 0, CLI_MODEL_NAME_LEN);
    MEMSET (&au1ImgVer, 0, CLI_MODEL_NAME_LEN);
    MEMSET (&au1TunnelMode, 0, OCTETSTR_SIZE);
    MEMSET (&au1CurrentTime, 0, ISS_PAGE_STRING_SIZE);

    SPRINTF ((CHR1 *) au1CurrentTime, "%d-%02d-%02d %02d:%02d:%02d",
             tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday, tm->tm_hour,
             tm->tm_min, tm->tm_sec);

    WebnmRegisterLock (pHttp, CapwapMainTaskLock, CapwapMainTaskUnLock);
    CAPWAP_LOCK;

    STRCPY (pHttp->au1Name, "AP_MODEL_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ModelName, pHttp->au1Value, (sizeof (au1ModelName) - 1));

    STRNCPY (gau1CurrentModelNumber, au1ModelName,
             (sizeof (gau1CurrentModelNumber) - 1));

    STRCPY (pHttp->au1Name, "NO_OF_RADIOS");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4NoOfRadios = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "CAPWAP_MAC_TYPE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4MacType = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "CAPWAP_TUNNEL_MODE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u1TunnelModeBit = (UINT1) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "QosProfile");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1QosProfile, pHttp->au1Value, (sizeof (au1QosProfile) - 1));

    STRCPY (pHttp->au1Name, "IMAGE_VERSION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ImgVer, pHttp->au1Value, (sizeof (au1ImgVer) - 1));

    i4ModelNameLen = (INT4) STRLEN (au1ModelName);
    i4TunnelModeLen = 1;

    STRCPY (pModelNumber->pu1_OctetList, au1ModelName);
    pModelNumber->i4_Length = i4ModelNameLen;

    /* Check if the profile is already present */
    if ((nmhGetFsWtpModelRowStatus (pModelNumber, &i4FsWtpModelRowStatus)) ==
        SNMP_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Entry already Exists\n");
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        free_octetstring (pModelNumber);
        free_octetstring (pQos);
        free_octetstring (pSwVer);
        return;
    }
    /* Create Model Table */
    /* Fill the fields */
    capwapSetFsWtpModelEntry.MibObject.u4FsNoOfRadio = u4NoOfRadios;
    capwapSetFsWtpModelEntry.MibObject.i4FsCapwapWtpMacType = i4MacType;
    capwapSetFsWtpModelEntry.MibObject.i4FsWtpModelRowStatus = CREATE_AND_GO;
    STRNCPY (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapWtpModelNumber,
             au1ModelName,
             (sizeof
              (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapWtpModelNumber) -
              1));
    capwapSetFsWtpModelEntry.MibObject.i4FsCapwapWtpModelNumberLen =
        i4ModelNameLen;
    capwapSetFsWtpModelEntry.MibObject.au1FsCapwapWtpTunnelMode[0] =
        u1TunnelModeBit;
    capwapSetFsWtpModelEntry.MibObject.i4FsCapwapWtpTunnelModeLen =
        i4TunnelModeLen;
    STRCPY (capwapSetFsWtpModelEntry.au1LastUpdated, au1CurrentTime);
    capwapSetFsWtpModelEntry.i4LastUpdatedLen = (INT4) STRLEN (au1CurrentTime);
    STRNCPY (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapImageName, au1ImgVer,
             (sizeof (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapImageName) -
              1));
    capwapSetFsWtpModelEntry.MibObject.i4FsCapwapImageNameLen =
        (INT4) STRLEN (au1ImgVer);
    STRNCPY (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapQosProfileName,
             au1QosProfile,
             (sizeof
              (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapQosProfileName) -
              1));
    capwapSetFsWtpModelEntry.MibObject.i4FsCapwapQosProfileNameLen =
        (INT4) STRLEN (au1QosProfile);

    /* Fill the booleans indicating presence of corresponding field */
    capwapIsSetFsWtpModelEntry.bFsCapwapWtpModelNumber = OSIX_TRUE;
    capwapIsSetFsWtpModelEntry.bFsNoOfRadio = OSIX_TRUE;

    capwapIsSetFsWtpModelEntry.bFsCapwapQosProfileName = OSIX_TRUE;
    capwapIsSetFsWtpModelEntry.bFsCapwapImageName = OSIX_TRUE;

    if ((i4MacType == CLI_MAC_TYPE_SPLIT) || (i4MacType == CLI_MAC_TYPE_LOCAL))
    {
        capwapIsSetFsWtpModelEntry.bFsCapwapWtpMacType = OSIX_TRUE;
    }
    if ((capwapSetFsWtpModelEntry.MibObject.au1FsCapwapWtpTunnelMode[0] ==
         CLI_TUNNEL_MODE_BRIDGE) ||
        (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapWtpTunnelMode[0] ==
         CLI_TUNNEL_MODE_DOT3) ||
        (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapWtpTunnelMode[0] ==
         CLI_TUNNEL_MODE_NATIVE))
    {
        capwapIsSetFsWtpModelEntry.bFsCapwapWtpTunnelMode = OSIX_TRUE;
    }
    capwapIsSetFsWtpModelEntry.bFsWtpModelRowStatus = OSIX_TRUE;

    if (CapwapTestAllFsWtpModelTable (&u4ErrorCode, &capwapSetFsWtpModelEntry,
                                      &capwapIsSetFsWtpModelEntry, OSIX_TRUE,
                                      OSIX_TRUE) != OSIX_SUCCESS)
    {
        /* Delete Profile */
        nmhSetFsWtpModelRowStatus (pModelNumber, DESTROY);
        free_octetstring (pModelNumber);
        free_octetstring (pQos);
        free_octetstring (pSwVer);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        return;
    }

    if (CapwapSetAllFsWtpModelTable (&capwapSetFsWtpModelEntry,
                                     &capwapIsSetFsWtpModelEntry, OSIX_TRUE,
                                     OSIX_TRUE) != OSIX_SUCCESS)
    {
        /* Delete Profile */
        nmhSetFsWtpModelRowStatus (pModelNumber, DESTROY);
        free_octetstring (pModelNumber);
        free_octetstring (pQos);
        free_octetstring (pSwVer);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        return;
    }
    WebnmUnRegisterLock (pHttp);
    CAPWAP_UNLOCK;
    IssProcessAPModelEntryPageGet (pHttp, pModelNumber);
    free_octetstring (pModelNumber);
    free_octetstring (pQos);
    free_octetstring (pSwVer);
    return;
}

/*********************************************************************
*  Function Name : IssProcessAPModelTablePage 
*  Description   : This function processes the request coming for the
*                  AP Model Table page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessAPModelTablePage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessAPModelTablePageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessAPModelTablePageSet (pHttp);
    }
    return;
}

/*********************************************************************
*  Function Name : IssProcessAPModelTablePageGet
*  Description   : This function processes the get request coming for the  
*                  AP Model Table page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessAPModelTablePageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE *currentModelNumber = NULL, *nextModelNumber = NULL;
    tSNMP_MULTI_DATA_TYPE *pu1DataString = NULL;
    INT4                i4OutCome = 0;
    UINT4               u4Temp = 0, u4FsNoOfRadios = 0;
    tSNMP_OCTET_STRING_TYPE *pLastUpdated = NULL;
    tCapwapFsWtpModelEntry capwapFsWtpModelEntry;

    if ((pu1DataString = WebnmAllocMultiData ()) == NULL)
    {
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        free_octetstring (pLastUpdated);
        return;
    }

    currentModelNumber =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (currentModelNumber == NULL)
    {
        /*free_octetstring (currentModelNumber); */
        return;
    }
    nextModelNumber =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (nextModelNumber == NULL)
    {
        free_octetstring (currentModelNumber);
        return;
    }
    pLastUpdated =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (pLastUpdated == NULL)
    {
        free_octetstring (currentModelNumber);
        free_octetstring (nextModelNumber);
        return;
    }
    MEMSET (currentModelNumber->pu1_OctetList, 0, OCTETSTR_SIZE);
    MEMSET (nextModelNumber->pu1_OctetList, 0, OCTETSTR_SIZE);
    MEMSET (pLastUpdated->pu1_OctetList, 0, OCTETSTR_SIZE);
    MEMSET (&capwapFsWtpModelEntry, 0, sizeof (tCapwapFsWtpModelEntry));
    currentModelNumber->i4_Length = 0;
    nextModelNumber->i4_Length = 0;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;
    i4OutCome = nmhGetFirstIndexFsWtpModelTable (nextModelNumber);
    WebnmRegisterLock (pHttp, CapwapMainTaskLock, CapwapMainTaskUnLock);
    CAPWAP_LOCK;

    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html +
                                          pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        free_octetstring (currentModelNumber);
        free_octetstring (nextModelNumber);
        free_octetstring (pLastUpdated);
        return;
    }
    else
    {
        do
        {
            pHttp->i4Write = (INT4) u4Temp;

            STRCPY (pu1DataString->pOctetStrValue->pu1_OctetList,
                    nextModelNumber->pu1_OctetList);
            pu1DataString->pOctetStrValue->i4_Length =
                nextModelNumber->i4_Length;

            STRCPY (pHttp->au1KeyString, "AP_MODEL_NAME_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            WebnmSendToSocket (pHttp, "AP_MODEL_NAME_KEY", pu1DataString,
                               ENM_DISPLAYSTRING);

            STRCPY (pHttp->au1KeyString, "NO_OF_RADIOS_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            MEMCPY (capwapFsWtpModelEntry.MibObject.au1FsCapwapWtpModelNumber,
                    nextModelNumber->pu1_OctetList, nextModelNumber->i4_Length);

            capwapFsWtpModelEntry.MibObject.i4FsCapwapWtpModelNumberLen =
                nextModelNumber->i4_Length;
            capwapFsWtpModelEntry.MibObject.i4FsWtpModelRowStatus = ACTIVE;
            if (CapwapGetAllFsWtpModelTable (&capwapFsWtpModelEntry) ==
                OSIX_SUCCESS)
            {
                u4FsNoOfRadios = capwapFsWtpModelEntry.MibObject.u4FsNoOfRadio;

            }
            nmhGetLastUpdated (nextModelNumber, pLastUpdated);
            MEMSET (gau1CurrentModelNumber, 0, OCTETSTR_SIZE);
            STRNCPY (gau1CurrentModelNumber, nextModelNumber->pu1_OctetList,
                     nextModelNumber->i4_Length);
            MEMCPY (pLastUpdated->pu1_OctetList,
                    capwapFsWtpModelEntry.au1LastUpdated,
                    capwapFsWtpModelEntry.i4LastUpdatedLen);
            pLastUpdated->i4_Length = capwapFsWtpModelEntry.i4LastUpdatedLen;

            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4FsNoOfRadios);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            STRCPY (pHttp->au1KeyString, "LAST_UPDATED_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     pLastUpdated->pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) (HTTP_STRLEN (pHttp->au1DataString)));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

            /* Copy next index to current index */
            STRCPY (currentModelNumber->pu1_OctetList,
                    nextModelNumber->pu1_OctetList);
            currentModelNumber->i4_Length = nextModelNumber->i4_Length;

        }
        while (nmhGetNextIndexFsWtpModelTable
               (currentModelNumber, nextModelNumber) == SNMP_SUCCESS);

        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));

    }
    WebnmUnRegisterLock (pHttp);
    CAPWAP_UNLOCK;
    free_octetstring (currentModelNumber);
    free_octetstring (nextModelNumber);
    free_octetstring (pLastUpdated);
}

/*********************************************************************
*  Function Name : IssProcessAPModelTablePageSet 
*  Description   : This function processes the set request coming for
*                  AP Model Table page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessAPModelTablePageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE *ModelNumber;
    UINT1               au1ModelName[CLI_MODEL_NAME_LEN];
    UINT4               noOfRadio, lastUpdated;
    UINT4               u4ErrorCode = 0;
    INT4                i4ModelNameLen = 0, i4OutCome = 0;
    UINT4               currentProfileId = 0, nextProfileId = 0;
    BOOL1               matched_profile = FALSE;

    ModelNumber =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (ModelNumber == NULL)
    {
        return;
    }
    MEMSET (ModelNumber->pu1_OctetList, 0, OCTETSTR_SIZE);

    MEMSET (&au1ModelName, 0, CLI_MODEL_NAME_LEN);

    WebnmRegisterLock (pHttp, CapwapMainTaskLock, CapwapMainTaskUnLock);
    CAPWAP_LOCK;

    STRCPY (pHttp->au1Name, "AP_MODEL_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ModelName, pHttp->au1Value, (sizeof (au1ModelName) - 1));

    STRCPY (pHttp->au1Name, "NO_OF_RADIOS");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    noOfRadio = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "LAST_UPDATED");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    lastUpdated = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    WSS_WEB_TRC ("Model Name:%s\n", au1ModelName);
    WSS_WEB_TRC ("NoOfRadio:%d\n", noOfRadio);
    WSS_WEB_TRC ("LastUpdated:%d\n", lastUpdated);
    WSS_WEB_TRC ("Action:%s\n", pHttp->au1Value);

    if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {
        i4ModelNameLen = (INT4) STRLEN (au1ModelName);

        i4OutCome = nmhGetFirstIndexCapwapBaseWtpProfileTable (&nextProfileId);
        if (i4OutCome == SNMP_FAILURE)
        {
            matched_profile = FALSE;
        }
        else
        {
            do
            {
                currentProfileId = nextProfileId;

                if (currentProfileId != 0)
                {
                    if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                        (currentProfileId, ModelNumber) == SNMP_FAILURE)
                    {
                        WebnmUnRegisterLock (pHttp);
                        CAPWAP_UNLOCK;
                        free_octetstring (ModelNumber);
                        return;

                    }
                    if (!STRCMP (ModelNumber->pu1_OctetList, au1ModelName))
                    {
                        matched_profile = TRUE;
                    }
                }
                currentProfileId = nextProfileId;

            }
            while (nmhGetNextIndexCapwapBaseWtpProfileTable (currentProfileId,
                                                             &nextProfileId));
        }
        if (!matched_profile)
        {
            STRCPY (ModelNumber->pu1_OctetList, au1ModelName);
            ModelNumber->i4_Length = i4ModelNameLen;
            if (nmhTestv2FsWtpModelRowStatus (&u4ErrorCode,
                                              ModelNumber,
                                              DESTROY) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
                WebnmUnRegisterLock (pHttp);
                CAPWAP_UNLOCK;
                free_octetstring (ModelNumber);
                return;
            }
            if (nmhSetFsWtpModelRowStatus (ModelNumber, DESTROY) ==
                SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
                WebnmUnRegisterLock (pHttp);
                CAPWAP_UNLOCK;
                free_octetstring (ModelNumber);
                return;
            }

        }
        else
        {
            IssSendError (pHttp, (CONST INT1 *) "Associated Profile Exists\n");
            WebnmUnRegisterLock (pHttp);
            CAPWAP_UNLOCK;
            free_octetstring (ModelNumber);
            return;
        }
    }
    else if (STRCMP (pHttp->au1Value, "Edit") == 0)
    {
        MEMSET (gau1CurrentModelNumber, 0, OCTETSTR_SIZE);
        MEMCPY (gau1CurrentModelNumber, au1ModelName,
                MEM_MAX_BYTES (sizeof (gau1CurrentModelNumber),
                               STRLEN (au1ModelName)));
        gu4CurrentNoOfRadios = noOfRadio;
    }
    else
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Operation\n");
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        free_octetstring (ModelNumber);
        return;
    }
    WebnmUnRegisterLock (pHttp);
    CAPWAP_UNLOCK;
    free_octetstring (ModelNumber);
    IssProcessAPModelTablePageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssProcessRadioTablePage 
*  Description   : This function processes the request coming for the
*                 Radio Table page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessRadioTablePage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessRadioTablePageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessRadioTablePageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name :IssProcessRadioTablePageGet 
*  Description   : This function processes the get request coming for the  
*                  Radio Table page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessRadioTablePageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    UINT4               u4RadioType = 0;
    INT4                i4RadioStatus = 0;
    tSNMP_OCTET_STRING_TYPE *currentModelNumber = NULL, *nextModelNumber = NULL;
    UINT4               u4CurrentRadioId = 1, u4NextRadioId = 1;
    tSNMP_MULTI_DATA_TYPE *pu1DataString = NULL;
    tCapwapFsWtpRadioEntry capwapFsWtpRadioEntry;

    MEMSET (&capwapFsWtpRadioEntry, 0, sizeof (tCapwapFsWtpRadioEntry));

    if ((pu1DataString = WebnmAllocMultiData ()) == NULL)
    {
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        return;
    }

    currentModelNumber =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (currentModelNumber == NULL)
    {
        return;
    }
    nextModelNumber =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (nextModelNumber == NULL)
    {
        free_octetstring (currentModelNumber);
        return;
    }

    MEMSET (currentModelNumber->pu1_OctetList, 0, OCTETSTR_SIZE);
    MEMSET (nextModelNumber->pu1_OctetList, 0, OCTETSTR_SIZE);
    currentModelNumber->i4_Length = 0;
    nextModelNumber->i4_Length = 0;

    WSS_WEB_TRC ("CurrentModel Name : %s\n", gau1CurrentModelNumber);
    WSS_WEB_TRC ("NextModel Name Length : %d\n", OCTETSTR_SIZE);

    WebnmRegisterLock (pHttp, CapwapMainTaskLock, CapwapMainTaskUnLock);
    CAPWAP_LOCK;
    /*Copy the stored Model Number which corresponds to selected entry */
    if (bIsEdit == OSIX_TRUE)
    {
        STRCPY (nextModelNumber->pu1_OctetList, gau1CurrentModelNumber);
        nextModelNumber->i4_Length = (INT4) STRLEN (gau1CurrentModelNumber);
        bIsEdit = OSIX_FALSE;
    }
    else
    {
        STRCPY (nextModelNumber->pu1_OctetList, gau1CurrentModelNumber);
        nextModelNumber->i4_Length = (INT4) STRLEN (gau1CurrentModelNumber);
    }

    /* Copy next index to current index */
    STRCPY (currentModelNumber->pu1_OctetList, nextModelNumber->pu1_OctetList);
    currentModelNumber->i4_Length = nextModelNumber->i4_Length;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        /* Break out if the Current and Next Model Number differ */
        if (STRCMP
            (currentModelNumber->pu1_OctetList, nextModelNumber->pu1_OctetList))
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html +
                                              pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            break;
        }

        /*Sends the Value for the Index */
        pu1DataString->i4_SLongValue = (INT4) u4NextRadioId;
        STRCPY (pHttp->au1KeyString, "RADIO_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSendToSocket (pHttp, "RADIO_ID_KEY", pu1DataString, ENM_INTEGER32);

        MEMCPY (capwapFsWtpRadioEntry.MibObject.au1FsCapwapWtpModelNumber,
                nextModelNumber->pu1_OctetList, nextModelNumber->i4_Length);
        capwapFsWtpRadioEntry.MibObject.i4FsCapwapWtpModelNumberLen =
            nextModelNumber->i4_Length;
        capwapFsWtpRadioEntry.MibObject.u4FsNumOfRadio = u4NextRadioId;

        if (CapwapGetAllFsWtpRadioTable (&capwapFsWtpRadioEntry) ==
            OSIX_SUCCESS)
        {
            u4RadioType = capwapFsWtpRadioEntry.MibObject.u4FsWtpRadioType;
            i4RadioStatus =
                capwapFsWtpRadioEntry.MibObject.i4FsRadioAdminStatus;
        }

        STRCPY (pHttp->au1KeyString, "RADIO_TYPE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RadioType);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (HTTP_STRLEN (pHttp->au1DataString)));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString, "RADIO_ADMIN_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RadioStatus);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (HTTP_STRLEN (pHttp->au1DataString)));

        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        u4CurrentRadioId = u4NextRadioId;

    }
    while (nmhGetNextIndexFsWtpRadioTable (currentModelNumber, nextModelNumber,
                                           u4CurrentRadioId,
                                           &u4NextRadioId) == SNMP_SUCCESS);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    WebnmUnRegisterLock (pHttp);
    CAPWAP_UNLOCK;
    free_octetstring (currentModelNumber);
    free_octetstring (nextModelNumber);
}

/*********************************************************************
*  Function Name :IssProcessRadioTablePageSet  
*  Description   : This function processes the get request coming for the  
*                  Radio Table page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessRadioTablePageSet (tHttp * pHttp)
{
    INT4                radioId = 0, radioType = 0, radioAdminStatus = 0;
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE *currentModelNumber = NULL;
    INT4                gau1CurrentModelNumber_Len =
        (INT4) STRLEN (gau1CurrentModelNumber);
    tCapwapFsWtpModelEntry capwapSetFsWtpModelEntry;
    tCapwapIsSetFsWtpModelEntry capwapIsSetFsWtpModelEntry;
    UINT1               au1CurrentTime[ISS_PAGE_STRING_SIZE];

    time_t              t = time (NULL);
    struct tm          *tm = NULL;

    tm = localtime (&t);

    if (tm == NULL)
    {
        return;
    }

    MEMSET (&au1CurrentTime, 0, ISS_PAGE_STRING_SIZE);

    SPRINTF ((CHR1 *) au1CurrentTime, "%d-%02d-%02d %02d:%02d:%02d",
             tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday, tm->tm_hour,
             tm->tm_min, tm->tm_sec);

    currentModelNumber =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (OCTETSTR_SIZE);
    if (currentModelNumber == NULL)
    {
        return;
    }
    MEMSET (currentModelNumber->pu1_OctetList, 0, OCTETSTR_SIZE);
    currentModelNumber->i4_Length = 0;
    STRCPY (currentModelNumber->pu1_OctetList, gau1CurrentModelNumber);
    currentModelNumber->i4_Length = gau1CurrentModelNumber_Len;

    WebnmRegisterLock (pHttp, CapwapMainTaskLock, CapwapMainTaskUnLock);
    CAPWAP_LOCK;
    MEMSET (&capwapSetFsWtpModelEntry, 0, sizeof (tCapwapFsWtpModelEntry));
    MEMSET (&capwapIsSetFsWtpModelEntry, 0,
            sizeof (tCapwapIsSetFsWtpModelEntry));

    STRCPY (pHttp->au1Name, "RADIO_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    radioId = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "RADIO_TYPE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    radioType = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "RADIO_ADMIN_STATUS");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    radioAdminStatus = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    WSS_WEB_TRC ("radioId : %d\n", radioId);
    WSS_WEB_TRC ("radioType : %d\n", radioType);
    WSS_WEB_TRC ("radioAdminStatus : %d\n", radioAdminStatus);

    if (nmhTestv2FsWtpRadioType
        (&u4ErrorCode, currentModelNumber, (UINT4) radioId,
         radioType) != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        free_octetstring (currentModelNumber);
        return;
    }
    if (nmhSetFsWtpRadioType (currentModelNumber, (UINT4) radioId, radioType) !=
        SNMP_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        free_octetstring (currentModelNumber);
        return;
    }
    if (nmhTestv2FsRadioAdminStatus
        (&u4ErrorCode, currentModelNumber, (UINT4) radioId,
         radioAdminStatus) != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        free_octetstring (currentModelNumber);
        return;
    }
    if (nmhSetFsRadioAdminStatus
        (currentModelNumber, (UINT4) radioId, radioAdminStatus) != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        free_octetstring (currentModelNumber);
        return;
    }

    capwapSetFsWtpModelEntry.MibObject.i4FsWtpModelRowStatus = ACTIVE;
    MEMCPY
        (capwapSetFsWtpModelEntry.MibObject.au1FsCapwapWtpModelNumber,
         currentModelNumber->pu1_OctetList, currentModelNumber->i4_Length);
    capwapSetFsWtpModelEntry.MibObject.i4FsCapwapWtpModelNumberLen =
        (INT4) gau1CurrentModelNumber_Len;

    capwapIsSetFsWtpModelEntry.bFsWtpModelRowStatus = OSIX_TRUE;
    capwapIsSetFsWtpModelEntry.bFsCapwapWtpModelNumber = OSIX_TRUE;

    STRCPY (capwapSetFsWtpModelEntry.au1LastUpdated, au1CurrentTime);
    capwapSetFsWtpModelEntry.i4LastUpdatedLen = (INT4) STRLEN (au1CurrentTime);

    if (CapwapTestAllFsWtpModelTable (&u4ErrorCode, &capwapSetFsWtpModelEntry,
                                      &capwapIsSetFsWtpModelEntry, OSIX_TRUE,
                                      OSIX_TRUE) != OSIX_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        free_octetstring (currentModelNumber);
        return;
    }

    if (CapwapSetAllFsWtpModelTable (&capwapSetFsWtpModelEntry,
                                     &capwapIsSetFsWtpModelEntry, OSIX_TRUE,
                                     OSIX_TRUE) != OSIX_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WebnmUnRegisterLock (pHttp);
        CAPWAP_UNLOCK;
        free_octetstring (currentModelNumber);
        return;
    }

    WebnmUnRegisterLock (pHttp);
    CAPWAP_UNLOCK;
    free_octetstring (currentModelNumber);
}

#ifdef WSSUSER_WANTED
/*********************************************************************
*  Functon Name : IssProcessUrmBasicSettingspage
*  Description   : This function processes the request coming for the
*                  basic settings page
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessUrmBasicSettingsPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessUrmBasicSettingsPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessUrmBasicSettingsPageSet (pHttp);
    }
}

/*********************************************************************
*  Functon Name : IssProcessUrmBasicSettingspageGet
*  Description   : This function processes the request coming for the
*                  user Group Table page
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessUrmBasicSettingsPageGet (tHttp * pHttp)
{
    UINT4               u4LoggedUserCount = 0;
    UINT4               u4BlockedUserCount = 0;
    INT4                i4UserRoleStatus = WSSUSER_MODULE_DISABLED;
    INT4                i4UserTrapStatus = 0;

    nmhGetFsWssUserRoleStatus (&i4UserRoleStatus);
    STRCPY (pHttp->au1KeyString, "USER_ROLE_MANAGEMENT_KEY");
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UserRoleStatus);
    WebnmSendString (pHttp, pHttp->au1KeyString);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    nmhGetFsWssUserLoggedCount (&u4LoggedUserCount);
    STRCPY (pHttp->au1KeyString, "LOGGED_USER_COUNT_KEY");
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4LoggedUserCount);
    WebnmSendString (pHttp, pHttp->au1KeyString);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    nmhGetFsWssUserBlockedCount (&u4BlockedUserCount);
    STRCPY (pHttp->au1KeyString, "BLOCKED_USER_COUNT_KEY");
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4BlockedUserCount);
    WebnmSendString (pHttp, pHttp->au1KeyString);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    nmhGetFsWssUserRoleTrapStatus (&i4UserTrapStatus);
    STRCPY (pHttp->au1KeyString, "USER_ROLE_TRAPS_KEY");
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UserTrapStatus);
    WebnmSendString (pHttp, pHttp->au1KeyString);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    WebnmUnRegisterLock (pHttp);
    WebnmUnRegisterContext (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    return;
}

/*********************************************************************
*  Functon Name : IssProcessUrmBasicSettingsPageSet
*  Description   : This function processes the request coming for the
*                  user Group Table page
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessUrmBasicSettingsPageSet (tHttp * pHttp)
{
    INT4                i4RoleStatus = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4UserTrapStatus = 0;

    STRCPY (pHttp->au1Name, "USER_ROLE_MANAGEMENT");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4RoleStatus = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    if (nmhTestv2FsWssUserRoleStatus (&u4ErrorCode, i4RoleStatus) !=
        SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Testing Failure");
        return;
    }

    if (nmhSetFsWssUserRoleStatus (i4RoleStatus) != SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Not able to set");
        return;
    }

    STRCPY (pHttp->au1Name, "USER_ROLE_TRAPS");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4UserTrapStatus = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    if (nmhTestv2FsWssUserRoleStatus (&u4ErrorCode,
                                      i4UserTrapStatus) != SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Testing Failure");
        return;
    }

    if (nmhSetFsWssUserRoleTrapStatus (i4UserTrapStatus) != SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Not able to set");
        return;
    }

    IssProcessUrmBasicSettingsPageGet (pHttp);
    return;
}

/*********************************************************************
*  Functon Name : IssProcessUrmUserGroupPage
*  Description   : This function processes the request coming for the
*                  user Group Table page
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessUrmUserGroupPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessUrmUserGroupPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessUrmUserGroupPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessUrmUserGroupPageGet
*  Description   :This function processes the get request coming for the
*                  user group table page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessUrmUserGroupPageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE GroupName;
    UINT1               au1Name[MAX_GROUP_NAME_LENGTH];
    UINT4               u4Time = 0;
    UINT4               u4BandWidth = 0;
    UINT4               u4Volume = 0;
    UINT4               u4Temp = 0;
    UINT4               u4CurrentGroupID = 0;
    UINT4               u4NextGroupID = 0;
    INT4                i4OutCome;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    i4OutCome = nmhGetFirstIndexFsWssUserGroupTable (&u4NextGroupID);
    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html +
                                          pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WebnmUnRegisterLock (pHttp);
        return;

    }

    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        MEMSET (&GroupName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (&au1Name, 0, MAX_GROUP_NAME_LENGTH);
        GroupName.pu1_OctetList = (UINT1 *) au1Name;

        STRCPY (pHttp->au1KeyString, "USER_GROUP_ID_KEY");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4NextGroupID);
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsWssUserGroupName (u4NextGroupID, &GroupName);
        STRCPY (pHttp->au1KeyString, "USER_GROUP_NAME_KEY");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", GroupName.pu1_OctetList);
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsWssUserGroupBandWidth (u4NextGroupID, &u4BandWidth);
        STRCPY (pHttp->au1KeyString, "BANDWIDTH_KEY");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4BandWidth);
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsWssUserGroupVolume (u4NextGroupID, &u4Volume);
        STRCPY (pHttp->au1KeyString, "VOLUME_KEY");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4Volume);
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsWssUserGroupTime (u4NextGroupID, &u4Time);
        STRCPY (pHttp->au1KeyString, "DURATION_KEY");
        if (u4Time == 0)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Unlimited");
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d Seconds", u4Time);
        }
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        u4CurrentGroupID = u4NextGroupID;
    }
    while (nmhGetNextIndexFsWssUserGroupTable (u4CurrentGroupID, &u4NextGroupID)
           == SNMP_SUCCESS);

    WebnmUnRegisterLock (pHttp);
    WebnmUnRegisterContext (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    return;
}

/*********************************************************************
*  Function Name : IssProcessUrmUserGroupPageSet
*  Description   :This function processes the  request coming for the
*                 User Group Set page
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessUrmUserGroupPageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE tUserGroupName;
    UINT4               u4Volume = 0;
    UINT4               u4BandWidth = 0;
    UINT4               u4GroupId = 0;
    UINT4               u4Time = 0;
    UINT4               u4ErrorCode = 0;
    UINT1               au1Name[MAX_GROUP_NAME_LENGTH];

    MEMSET (&tUserGroupName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    MEMSET (&au1Name, 0, MAX_GROUP_NAME_LENGTH);
    tUserGroupName.pu1_OctetList = au1Name;

    STRCPY (pHttp->au1Name, "USER_GROUP_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4GroupId = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "USER_GROUP_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);
    MEMCPY (tUserGroupName.pu1_OctetList, pHttp->au1Value,
            STRLEN (pHttp->au1Value));
    tUserGroupName.i4_Length = (INT4) STRLEN (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "BAND_WIDTH");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4BandWidth = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "VOLUME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4Volume = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "DURATION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4Time = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {
        /* Test user group is present */
        if (nmhTestv2FsWssUserGroupRowStatus (&u4ErrorCode, u4GroupId,
                                              DESTROY) != SNMP_SUCCESS)
        {

            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "User is associated with the group");
            return;

        }

        /* Delete User Group */
        if (nmhSetFsWssUserGroupRowStatus (u4GroupId, DESTROY) != SNMP_SUCCESS)
        {

            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Not able to delete");
            return;
        }
        IssProcessUrmUserGroupPageGet (pHttp);
        return;
    }
/* For Updating */
    else if (STRCMP (pHttp->au1Value, "Edit") == 0)
    {
        if (nmhTestv2FsWssUserGroupRowStatus (&u4ErrorCode, u4GroupId,
                                              NOT_IN_SERVICE) != SNMP_SUCCESS)
        {

            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Test Failed");
            return;

        }
        if (nmhSetFsWssUserGroupRowStatus (u4GroupId,
                                           NOT_IN_SERVICE) != SNMP_SUCCESS)
        {
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Invalid Configuration - Row Status\n");
            return;
        }

        if (nmhTestv2FsWssUserGroupName (&u4ErrorCode, u4GroupId,
                                         &tUserGroupName) != SNMP_SUCCESS)
        {
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Invalid Configuration - Group Name\n");
            return;
        }

        if (nmhSetFsWssUserGroupName (u4GroupId, &tUserGroupName) !=
            SNMP_SUCCESS)
        {

            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Invalid Configuration - Group Name\n");
            return;
        }
        if (nmhTestv2FsWssUserGroupBandWidth (&u4ErrorCode, u4GroupId,
                                              u4BandWidth) != SNMP_SUCCESS)
        {
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Invalid Configuration - Bandwidth\n");
            return;

        }

        if (nmhSetFsWssUserGroupBandWidth (u4GroupId, u4BandWidth) !=
            SNMP_SUCCESS)
        {
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Invalid Configuration - Bandwidth\n");
            return;

        }

        if (nmhTestv2FsWssUserGroupVolume (&u4ErrorCode, u4GroupId,
                                           u4Volume) != SNMP_SUCCESS)
        {

            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Invalid Configuration - Volume\n");
            return;
        }

        if (nmhSetFsWssUserGroupVolume (u4GroupId, u4Volume) != SNMP_SUCCESS)
        {

            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Invalid Configuration - Volume\n");
            return;
        }

        if (nmhTestv2FsWssUserGroupTime (&u4ErrorCode, u4GroupId,
                                         u4Time) != SNMP_SUCCESS)
        {

            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Invalid Configuration - Time\n");
            return;
        }

        if (nmhSetFsWssUserGroupTime (u4GroupId, u4Time) != SNMP_SUCCESS)
        {

            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Invalid Configuration - Time\n");
            return;
        }
        if (nmhTestv2FsWssUserGroupRowStatus (&u4ErrorCode, u4GroupId,
                                              ACTIVE) != SNMP_SUCCESS)
        {

            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "User is associated with the group");
            return;

        }

        if (nmhSetFsWssUserGroupRowStatus (u4GroupId, ACTIVE) != SNMP_SUCCESS)
        {

            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Invalid Configuration - Row Status\n");
            return;
        }

        IssProcessUrmUserGroupPageGet (pHttp);
        return;
    }

/*For Adding a new group*/
    if (nmhTestv2FsWssUserGroupRowStatus (&u4ErrorCode, u4GroupId,
                                          CREATE_AND_WAIT) != SNMP_SUCCESS)
    {

        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "User is associated with the group");
        return;

    }

    if (nmhSetFsWssUserGroupRowStatus (u4GroupId,
                                       CREATE_AND_WAIT) != SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Invalid Configuration - Row Status\n");
        return;

    }

    if (nmhTestv2FsWssUserGroupName (&u4ErrorCode, u4GroupId,
                                     &tUserGroupName) != SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Invalid Configuration - Group Name\n");
        return;

    }

    if (nmhSetFsWssUserGroupName (u4GroupId, &tUserGroupName) != SNMP_SUCCESS)
    {

        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Invalid Configuration - Group Name\n");
        return;
    }
    if (nmhTestv2FsWssUserGroupBandWidth (&u4ErrorCode, u4GroupId,
                                          u4BandWidth) != SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Invalid Configuration - Bandwidth\n");
        return;

    }

    /* Update the User Group's Bandwidth value */

    if (nmhSetFsWssUserGroupBandWidth (u4GroupId, u4BandWidth) != SNMP_SUCCESS)
    {
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Invalid Configuration - Bandwidth\n");
        return;

    }

    if (nmhTestv2FsWssUserGroupVolume (&u4ErrorCode, u4GroupId,
                                       u4Volume) != SNMP_SUCCESS)
    {

        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration - Volume\n");
        return;

    }

    if (nmhSetFsWssUserGroupVolume (u4GroupId, u4Volume) != SNMP_SUCCESS)
    {

        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration - Volume\n");
        return;
    }

    if (nmhTestv2FsWssUserGroupTime (&u4ErrorCode, u4GroupId,
                                     u4Time) != SNMP_SUCCESS)
    {

        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration - Time\n");
        return;
    }

    if (nmhSetFsWssUserGroupTime (u4GroupId, u4Time) != SNMP_SUCCESS)
    {

        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration - Time\n");
        return;
    }
    if (nmhTestv2FsWssUserGroupRowStatus (&u4ErrorCode, u4GroupId,
                                          ACTIVE) != SNMP_SUCCESS)
    {

        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "User is associated with the group");
        return;

    }

    if (nmhSetFsWssUserGroupRowStatus (u4GroupId, ACTIVE) != SNMP_SUCCESS)
    {

        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Invalid Configuration - Row Status\n");
        return;
    }

    IssProcessUrmUserGroupPageGet (pHttp);

    return;
}

/*********************************************************************
*  Function Name : IssProcessUrmUserRolePage
*  Description   :This function processes the request coming for the
*                 User Role Table page
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessUrmUserRolePage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessUrmUserRolePageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessUrmUserRolePageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessUrmUserRolePageGet
*  Description   :This function processes the get request coming for the
*                 User Role table page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessUrmUserRolePageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE UserName;
    tSNMP_OCTET_STRING_TYPE NextUserName;
    UINT1               au1UserName[MAX_USER_NAME_LENGTH + 1];
    UINT1               au1NextUserName[MAX_USER_NAME_LENGTH + 1];
    UINT4               u4GroupId = 0;
    UINT4               u4ProfileId = 0;
    UINT4               u4NextProfileId = 0;
    UINT4               u4WlanId = 0;
    INT1                i1RetValue = SNMP_FAILURE;
    UINT4               u4Temp = 0;

    MEMSET (&NextUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1NextUserName, 0, MAX_USER_NAME_LENGTH + 1);

    NextUserName.pu1_OctetList = (UINT1 *) au1NextUserName;

    /* Get Group Id of all users */

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    i1RetValue =
        nmhGetFirstIndexFsWssUserRoleTable (&NextUserName, &u4NextProfileId);

    if (i1RetValue == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html +
                                          pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WebnmUnRegisterLock (pHttp);

        return;
    }

    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        u4ProfileId = u4NextProfileId;

        MEMSET (&UserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (&au1UserName, 0, MAX_USER_NAME_LENGTH + 1);

        UserName.pu1_OctetList = (UINT1 *) au1UserName;

        MEMCPY (UserName.pu1_OctetList, NextUserName.pu1_OctetList,
                NextUserName.i4_Length);
        UserName.i4_Length = NextUserName.i4_Length;

        u4GroupId = 0;
        if (nmhGetFsWssUserRoleGroupId (&UserName, u4ProfileId, &u4GroupId) !=
            SNMP_SUCCESS)
        {
            return;
        }

        WssGetCapwapWlanId (u4ProfileId, &u4WlanId);

        STRCPY (pHttp->au1KeyString, "USER_ROLE_NAME_KEY");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", UserName.pu1_OctetList);
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "USER_ROLE_WLAN_KEY");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4WlanId);
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "USER_ROLE_GROUPID_KEY");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4GroupId);
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

    }
    while (nmhGetNextIndexFsWssUserRoleTable (&UserName, &NextUserName,
                                              u4ProfileId,
                                              &u4NextProfileId) ==
           SNMP_SUCCESS);
    WebnmUnRegisterLock (pHttp);
    WebnmUnRegisterContext (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    return;
}

/*********************************************************************
*  Function Name : IssProcessUrmUserRolePageSet
*  Description   :This function processes the set request coming for the
*                 User Role Table page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessUrmUserRolePageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE UserName;
    INT4                i4UserGroupRowStatus = -1;
    INT4                i4UserRoleRowStatus = -1;
    UINT1               au1Name[MAX_GROUP_NAME_LENGTH];
    UINT4               u4ErrorCode = 0;
    UINT4               u4GroupId = 0;
    UINT4               u4WlanId = 0;
    UINT4               u4ProfileId = 0;

    MEMSET (&au1Name, 0, MAX_GROUP_NAME_LENGTH);
    UserName.pu1_OctetList = au1Name;

    STRCPY (pHttp->au1Name, "USER_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    MEMCPY (UserName.pu1_OctetList, pHttp->au1Value, STRLEN (pHttp->au1Value));
    UserName.i4_Length = (INT4) STRLEN (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "USER_WLAN");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4WlanId = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "USER_GROUPID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4GroupId = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    if (nmhGetCapwapDot11WlanProfileIfIndex
        (u4WlanId, (INT4 *) (&u4ProfileId)) != SNMP_SUCCESS)
    {
        return;
    }
    if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {
        /* Test user role is present */
        if (nmhTestv2FsWssUserRoleRowStatus (&u4ErrorCode, &UserName,
                                             u4ProfileId,
                                             DESTROY) != SNMP_SUCCESS)
        {
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Not able to delete");
            return;

        }

        /* Delete User Role */
        if (nmhSetFsWssUserRoleRowStatus (&UserName, u4ProfileId,
                                          DESTROY) != SNMP_SUCCESS)
        {

            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Not able to delete");
            return;
        }
        IssProcessUrmUserRolePageGet (pHttp);
        return;

    }

    if (u4GroupId != 0)
    {
        /* Check whether User Group of the corresponding GroupId is already created.
         * If not created, display error message saying "User Group is not created" */

        if (STRCMP (pHttp->au1Value, "Update") == 0)
        {
            if (nmhGetFsWssUserGroupRowStatus (u4GroupId, &i4UserGroupRowStatus)
                != SNMP_SUCCESS)
            {
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *) "Unable to fetch the Row status");
                return;
            }
            if (nmhGetFsWssUserRoleRowStatus
                (&UserName, u4ProfileId, &i4UserRoleRowStatus) != SNMP_SUCCESS)
            {
                /* Check whether the UserName and WlanId value is valid input */

                if (nmhTestv2FsWssUserRoleRowStatus (&u4ErrorCode, &UserName,
                                                     u4ProfileId,
                                                     CREATE_AND_WAIT) !=
                    SNMP_SUCCESS)
                {
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Invalid Configuration Test - User Role Row");
                    return;
                }
                /* Set the Row Status of User Role Table to CREATE_AND_WAIT -
                 * To add entry to User Role Table*/

                if (nmhSetFsWssUserRoleRowStatus (&UserName, u4ProfileId,
                                                  CREATE_AND_WAIT) !=
                    SNMP_SUCCESS)
                {

                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Invalid Configuration Set - User Role Row");
                    return;
                }
                if (nmhTestv2FsWssUserRoleGroupId
                    (&u4ErrorCode, &UserName, u4ProfileId,
                     u4GroupId) != SNMP_SUCCESS)
                {

                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Invalid Configuration Test - Group ID\n");
                    return;
                }
                /* Set the Row Status of User Role Table as NOT_IN_SERVICE -
                 *              * To update its field*/
                if (nmhSetFsWssUserRoleRowStatus (&UserName, u4ProfileId,
                                                  NOT_IN_SERVICE) !=
                    SNMP_SUCCESS)
                {
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Invalid Configuration set - User Role Row\n");
                    return;

                }

                if (nmhSetFsWssUserRoleGroupId (&UserName, u4ProfileId,
                                                u4GroupId) != SNMP_SUCCESS)
                {

                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Invalid Configuration - Group ID\n");
                    return;
                }
                if (nmhTestv2FsWssUserRoleRowStatus (&u4ErrorCode, &UserName,
                                                     u4ProfileId,
                                                     ACTIVE) != SNMP_SUCCESS)
                {
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Invalid Configuration Test - User Role Row \n");
                    return;
                }

                /* Change back the Row Status of User Role table as ACTIVE  */
                if (nmhSetFsWssUserRoleRowStatus (&UserName, u4ProfileId,
                                                  ACTIVE) != SNMP_SUCCESS)
                {
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Invalid Configuration Set- User Role Row\n");
                    return;
                }

            }
            else
            {

                /*creating the entries with the updated value */
                if (nmhTestv2FsWssUserRoleRowStatus (&u4ErrorCode, &UserName,
                                                     u4ProfileId,
                                                     NOT_IN_SERVICE) !=
                    SNMP_SUCCESS)
                {

                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Invalid Configuration - Row Status\n");
                    return;
                }
                if (nmhSetFsWssUserRoleRowStatus (&UserName,
                                                  u4ProfileId,
                                                  NOT_IN_SERVICE) !=
                    SNMP_SUCCESS)
                {

                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Invalid Configuration - Row Status\n");
                    return;
                }
                if (nmhTestv2FsWssUserRoleGroupId
                    (&u4ErrorCode, &UserName, u4ProfileId,
                     u4GroupId) != SNMP_SUCCESS)
                {

                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Invalid Configuration - Group ID\n");
                    return;

                }

                /* Update Group Id for the given User Name and WlanId */
                if (nmhSetFsWssUserRoleGroupId (&UserName, u4ProfileId,
                                                u4GroupId) != SNMP_SUCCESS)
                {
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Invalid Configuration - Group ID\n");
                    return;

                }

                if (nmhTestv2FsWssUserRoleRowStatus
                    (&u4ErrorCode, &UserName, u4ProfileId,
                     ACTIVE) != SNMP_SUCCESS)
                {
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Invalid Configuration - Row Status\n");
                    return;
                }

                if (nmhSetFsWssUserRoleRowStatus (&UserName, u4ProfileId,
                                                  ACTIVE) != SNMP_SUCCESS)
                {
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Invalid Configuration - Row Status\n");
                    return;
                }
                IssProcessUrmUserRolePageGet (pHttp);
                return;

            }
        }
        /* Create the Entry as it is not present */
        if (nmhTestv2FsWssUserRoleRowStatus (&u4ErrorCode, &UserName,
                                             u4ProfileId,
                                             CREATE_AND_WAIT) != SNMP_SUCCESS)
        {

            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Invalid Configuration - Row Status\n");
            return;
        }

        if (nmhSetFsWssUserRoleRowStatus (&UserName, u4ProfileId,
                                          CREATE_AND_WAIT) != SNMP_SUCCESS)
        {
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Invalid Configuration - Row Status\n");
            return;

        }

        if (nmhTestv2FsWssUserRoleGroupId (&u4ErrorCode, &UserName, u4ProfileId,
                                           u4GroupId) != SNMP_SUCCESS)
        {
            if (nmhSetFsWssUserRoleRowStatus (&UserName, u4ProfileId,
                                              DESTROY) != SNMP_SUCCESS)
            {
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Invalid Configuration Test - Row Status\n");
                return;
            }
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Invalid Configuration Set- Row Status\n");
            return;
        }

        if (nmhSetFsWssUserRoleRowStatus (&UserName, u4ProfileId,
                                          NOT_IN_SERVICE) != SNMP_SUCCESS)
        {
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Invalid Configuration - Row Status\n");
            return;

        }

        if (nmhTestv2FsWssUserRoleGroupId (&u4ErrorCode, &UserName, u4ProfileId,
                                           u4GroupId) != SNMP_SUCCESS)
        {

            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Invalid Configuration - Group ID\n");
            return;
        }

        if (nmhSetFsWssUserRoleGroupId (&UserName, u4ProfileId,
                                        u4GroupId) != SNMP_SUCCESS)
        {

            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Invalid Configuration - Group ID\n");
            return;
        }

        if (nmhTestv2FsWssUserRoleRowStatus (&u4ErrorCode, &UserName,
                                             u4ProfileId,
                                             ACTIVE) != SNMP_SUCCESS)
        {

            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Invalid Configuration - Row Status\n");
            return;
        }

        if (nmhSetFsWssUserRoleRowStatus (&UserName, u4ProfileId,
                                          ACTIVE) != SNMP_SUCCESS)
        {

            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Invalid Configuration - Row Status\n");
            return;
        }
    }
    else
    {

        /* User is created without specfiying User Group.
         * Let it be set to default User Group.
         */

        if (nmhTestv2FsWssUserRoleRowStatus (&u4ErrorCode, &UserName,
                                             u4ProfileId,
                                             CREATE_AND_GO) != SNMP_SUCCESS)
        {

            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Invalid Configuration - Row Status\n");
            return;
        }

        if (nmhSetFsWssUserRoleRowStatus (&UserName, u4ProfileId,
                                          CREATE_AND_GO) != SNMP_SUCCESS)
        {

            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Invalid Configuration - Row Status\n");
            return;
        }
    }

    IssProcessUrmUserRolePageGet (pHttp);
    return;
}

/*********************************************************************
*  Function Name : IssProcessUrmUsernameRestrictPage
*  Description   :This function processes the request coming for the
*                 user name Restrict Table page
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessUrmUsernameRestrictPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessUrmUsernameRestrictPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessUrmUsernameRestrictPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessUrmUsernameRestrictPageGet
*  Description   :This function processes the get request coming for the
*                 User Name Restrict table page
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessUrmUsernameRestrictPageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE UserName;
    tSNMP_OCTET_STRING_TYPE NextUserName;
    UINT4               u4Temp = 0;
    UINT1               au1UserName[MAX_USER_NAME_LENGTH];
    UINT1               au1NextUserName[MAX_USER_NAME_LENGTH];
    INT1                i1RetValue = SNMP_FAILURE;

    MEMSET (&au1UserName, 0, MAX_USER_NAME_LENGTH);
    MEMSET (&au1NextUserName, 0, MAX_USER_NAME_LENGTH);

    MEMSET (&UserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    UserName.pu1_OctetList = (UINT1 *) au1UserName;
    NextUserName.pu1_OctetList = (UINT1 *) au1NextUserName;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;
    /* Get all restricted user name */
    i1RetValue = nmhGetFirstIndexFsWssUserNameAccessListTable (&UserName);
    if (i1RetValue == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html +
                                          pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WebnmUnRegisterLock (pHttp);

        return;

    }

    while (1)
    {
        pHttp->i4Write = (INT4) u4Temp;
        STRCPY (pHttp->au1KeyString, "USER_NAME_KEY");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", UserName.pu1_OctetList);
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

        if (nmhGetNextIndexFsWssUserNameAccessListTable
            (&UserName, &NextUserName) != SNMP_SUCCESS)
        {
            break;
        }

        MEMCPY (UserName.pu1_OctetList, NextUserName.pu1_OctetList,
                NextUserName.i4_Length);
        UserName.i4_Length = NextUserName.i4_Length;

        MEMSET (&NextUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        NextUserName.pu1_OctetList = (UINT1 *) au1NextUserName;

    }
    while (nmhGetNextIndexFsWssUserNameAccessListTable
           (&UserName, &NextUserName) == SNMP_SUCCESS);

    WebnmUnRegisterLock (pHttp);
    WebnmUnRegisterContext (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    return;
}

/*********************************************************************
*  Function Name : IssProcessUrmUsernameRestrictPageSet
*  Description   ::This function processes the set request coming for the
*                  User Name Restrict Table page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessUrmUsernameRestrictPageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE UserName;
    UINT4               u4ErrorCode = 0;
    UINT1               au1UserName[MAX_USER_NAME_LENGTH];

    MEMSET (&UserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    UserName.pu1_OctetList = au1UserName;

    /* Restrict particular user with user name as index */
    STRCPY (pHttp->au1Name, "USER_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);

    MEMCPY (UserName.pu1_OctetList, pHttp->au1Value, STRLEN (pHttp->au1Value));
    UserName.i4_Length = (INT4) STRLEN (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {

        if (nmhTestv2FsWssUserNameAccessListRowStatus (&u4ErrorCode, &UserName,
                                                       DESTROY) != SNMP_SUCCESS)
        {
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Invalid Configuration - Row status\n");
            return;
        }

        if (nmhSetFsWssUserNameAccessListRowStatus (&UserName, DESTROY) !=
            SNMP_SUCCESS)
        {
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Invalid Configuration - Row status\n");
            return;
        }

    }
    else
    {

        if (nmhTestv2FsWssUserNameAccessListRowStatus (&u4ErrorCode, &UserName,
                                                       CREATE_AND_GO) !=
            SNMP_SUCCESS)
        {
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Invalid Configuration - Row status\n");
            return;
        }

        if (nmhSetFsWssUserNameAccessListRowStatus (&UserName, CREATE_AND_GO) !=
            SNMP_SUCCESS)
        {

            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Invalid Configuration - Row Status\n");
            return;
        }
    }
    IssProcessUrmUsernameRestrictPageGet (pHttp);
    return;
}

/*********************************************************************
*  Function Name : IssProcessUrmStationMacRestrictPage
*  Description   :This function processes the request coming for the
*                 station Mac Restrict Table page
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessUrmStationMacRestrictPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessUrmStationMacRestrictPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessUrmStationMacRestrictPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessUrmStationMacRestrictPageGet
*  Description   :This function processes the get request coming for the
*                 station Mac Restrict Table page
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessUrmStationMacRestrictPageGet (tHttp * pHttp)
{
    tMacAddr            MacAddr;
    tMacAddr            NextMacAddr;
    UINT1               au1MacAddr[21];
    INT1                i1RetValue = SNMP_FAILURE;
    UINT4               u4Temp = 0;

    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    MEMSET (&NextMacAddr, 0, sizeof (tMacAddr));
    MEMSET (au1MacAddr, 0, 21);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;
    /* Get all restricted station mac-address */
    i1RetValue = nmhGetFirstIndexFsWssUserMacAccessListTable (&MacAddr);
    if (i1RetValue == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html +
                                          pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WebnmUnRegisterLock (pHttp);

        return;
    }

    while (1)
    {
        pHttp->i4Write = (INT4) u4Temp;
        IssPrintMacAddress (MacAddr, au1MacAddr);

        STRCPY (pHttp->au1KeyString, "STATION_MAC_KEY");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1MacAddr);
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

        if (nmhGetNextIndexFsWssUserMacAccessListTable
            (MacAddr, &NextMacAddr) != SNMP_SUCCESS)
        {
            break;
        }

        MEMCPY (MacAddr, NextMacAddr, MAC_ADDR_LEN);
        MEMSET (&NextMacAddr, 0, sizeof (tMacAddr));

    }
    while (nmhGetNextIndexFsWssUserMacAccessListTable
           (MacAddr, &NextMacAddr) == SNMP_SUCCESS);

    WebnmUnRegisterLock (pHttp);
    WebnmUnRegisterContext (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    return;
}

/*********************************************************************
*  Function Name : IssProcessUrmStationMacRestrictPageSet
*  Description   :This function processes the set request coming for the
*                 station Mac Restrict Table page
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessUrmStationMacRestrictPageSet (tHttp * pHttp)
{
    tMacAddr            MacAddress;
    UINT4               u4ErrorCode = 0;

    MEMSET (&MacAddress, 0, sizeof (tMacAddr));

    STRCPY (pHttp->au1Name, "STATION_MAC");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);
    StrToMac (pHttp->au1Value, MacAddress);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {
        if (nmhTestv2FsWssUserMacAccessListRowStatus (&u4ErrorCode, MacAddress,
                                                      DESTROY) != SNMP_SUCCESS)
        {

            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to delete the MAC\n");
            return;
        }

        if (nmhSetFsWssUserMacAccessListRowStatus (MacAddress, DESTROY) !=
            SNMP_SUCCESS)
        {

            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to delete\n");
            return;
        }
        IssProcessUrmStationMacRestrictPageGet (pHttp);
        return;
    }

    /* Restrict particular user with mac address as index */
    if (nmhTestv2FsWssUserMacAccessListRowStatus (&u4ErrorCode, MacAddress,
                                                  CREATE_AND_GO) !=
        SNMP_SUCCESS)
    {

        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Invalid Configuration - Row status\n");
        return;
    }

    if (nmhSetFsWssUserMacAccessListRowStatus (MacAddress, CREATE_AND_GO) !=
        SNMP_SUCCESS)
    {

        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Invalid Configuration - Row status\n");
        return;
    }

    IssProcessUrmStationMacRestrictPageGet (pHttp);

    return;
}

/*********************************************************************
*  Function Name : IssProcessUrmUserMacMappingPage
*  Description   :This function processes the request coming for the
*                 Mac mapping Table page
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessUrmUserMacMappingPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessUrmUserMacMappingPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessUrmUserMacMappingPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessUrmUserMacMappingPageGet
*  Description   :This function processes the get request coming for the
*                 User Mac Mapping Table page
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*******************************************************************/

VOID
IssProcessUrmUserMacMappingPageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE UserName;
    tSNMP_OCTET_STRING_TYPE NextUserName;
    tMacAddr            MacAddr;
    tMacAddr            NextMacAddr;
    UINT1               au1UserName[MAX_USER_NAME_LENGTH];
    UINT1               au1NextUserName[MAX_USER_NAME_LENGTH];
    UINT4               u4Temp = 0;
    UINT1               au1String[21];
    INT1                i1RetValue = SNMP_FAILURE;

    MEMSET (&NextUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1NextUserName, 0, MAX_USER_NAME_LENGTH);
    NextUserName.pu1_OctetList = (UINT1 *) au1NextUserName;

    MEMSET (&NextMacAddr, 0, MAC_ADDR_LEN);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    /* Fetch the first User Mapping Table Entry */
    i1RetValue =
        nmhGetFirstIndexFsWssUserMappingTable (&NextUserName, &NextMacAddr);
    if (i1RetValue == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html +
                                          pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WebnmUnRegisterLock (pHttp);

        return;
    }

    /* Iterate through the User Mapping Table to fetch all entries \
     * from its table */

    do
    {
        pHttp->i4Write = (INT4) u4Temp;
        MEMSET (au1String, 0, 21);

        MEMCPY (MacAddr, NextMacAddr, MAC_ADDR_LEN);
        IssPrintMacAddress (MacAddr, au1String);

        STRCPY (pHttp->au1KeyString, "USER_NAME_KEY");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                 NextUserName.pu1_OctetList);

        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "STATION_MAC_KEY");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

        MEMSET (&UserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1UserName, 0, MAX_USER_NAME_LENGTH);
        UserName.pu1_OctetList = (UINT1 *) au1UserName;

        MEMCPY (UserName.pu1_OctetList, NextUserName.pu1_OctetList,
                NextUserName.i4_Length);
        UserName.i4_Length = NextUserName.i4_Length;

        MEMSET (&NextUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

        MEMSET (au1NextUserName, 0, MAX_USER_NAME_LENGTH);
        NextUserName.pu1_OctetList = (UINT1 *) au1NextUserName;

        MEMSET (&NextMacAddr, 0, MAC_ADDR_LEN);

    }
    while (nmhGetNextIndexFsWssUserMappingTable (&UserName, &NextUserName,
                                                 MacAddr,
                                                 &NextMacAddr) == SNMP_SUCCESS);

    WebnmUnRegisterLock (pHttp);
    WebnmUnRegisterContext (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    return;
}

/*********************************************************************
*  Function Name : IssProcessUrmUserMacMappingPageSet
*  Description   :This function processes the set request coming for the
*                 User Mac mapping Table
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessUrmUserMacMappingPageSet (tHttp * pHttp)
{

    tSNMP_OCTET_STRING_TYPE UserName;
    tMacAddr            MacAddress;
    UINT4               u4ErrorCode = 0;
    UINT1               au1UserName[MAX_USER_NAME_LENGTH];

    MEMSET (&UserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1UserName, 0, MAX_USER_NAME_LENGTH);
    UserName.pu1_OctetList = (UINT1 *) au1UserName;

    MEMSET (&MacAddress, 0, MAC_ADDR_LEN);

    STRCPY (pHttp->au1Name, "USER_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);
    MEMCPY (UserName.pu1_OctetList, pHttp->au1Value, STRLEN (pHttp->au1Value));
    UserName.i4_Length = (INT4) STRLEN (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "STATION_MAC");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);
    CliDotStrToMac (pHttp->au1Value, MacAddress);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {
        if (nmhTestv2FsWssUserMappingRowStatus (&u4ErrorCode, &UserName,
                                                MacAddress,
                                                DESTROY) != SNMP_SUCCESS)
        {

            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Invalid Configuration - Row Status\n");
            return;
        }

        if (nmhSetFsWssUserMappingRowStatus (&UserName, MacAddress,
                                             DESTROY) != SNMP_SUCCESS)
        {

            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Invalid Configuration - Row Status\n");
            return;
        }
        IssProcessUrmUserMacMappingPageGet (pHttp);
        return;
    }

    if (nmhTestv2FsWssUserMappingRowStatus (&u4ErrorCode, &UserName,
                                            MacAddress,
                                            CREATE_AND_WAIT) != SNMP_SUCCESS)
    {

        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Invalid Configuration - Row Status\n");
        return;
    }

    if (nmhSetFsWssUserMappingRowStatus (&UserName, MacAddress,
                                         CREATE_AND_WAIT) != SNMP_SUCCESS)
    {

        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Invalid Configuration - Row Status\n");
        return;
    }

    if (nmhTestv2FsWssUserMappingRowStatus (&u4ErrorCode, &UserName,
                                            MacAddress, ACTIVE) != SNMP_SUCCESS)
    {

        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Invalid Configuration - Row Status\n");
        return;
    }

    if (nmhSetFsWssUserMappingRowStatus (&UserName, MacAddress,
                                         ACTIVE) != SNMP_SUCCESS)
    {

        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Invalid Configuration - Row Status\n");
        return;
    }
    IssProcessUrmUserMacMappingPageGet (pHttp);
    return;
}

/*********************************************************************
*  Function Name :IssProcessUrmSessionDetailspage
*  Description   :This function processes the request coming for the
*                 Session details Table page
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessUrmSessionDetailsPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessUrmSessionDetailsPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessUrmSessionDetailsPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name :IssProcessUrmSessionDetailspageGet
*  Description   :This function processes the Get request coming for the
*                 Session details Table page
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessUrmSessionDetailsPageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE UserName;
    tSNMP_OCTET_STRING_TYPE NextUserName;
    tMacAddr            MacAddr;
    tMacAddr            NextMacAddr;
    UINT1               au1UserName[MAX_USER_NAME_LENGTH];
    UINT1               au1NextUserName[MAX_USER_NAME_LENGTH];
    UINT1               au1String[21];
    UINT4               u4WlanIndex = 0;
    UINT4               u4AllotedTime = 0;
    UINT4               u4UsedTime = 0;
    INT1                i1RetValue = SNMP_FAILURE;
    UINT4               u4AllotedVolume = 0;
    UINT4               u4UsedVolume = 0;
    UINT4               u4AllotedBandWidth = 0;
    UINT4               u4Temp = 0;
    MEMSET (&UserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1UserName, 0, MAX_USER_NAME_LENGTH);
    MEMSET (&au1NextUserName, 0, MAX_USER_NAME_LENGTH);
    MEMSET (&MacAddr, 0, MAC_ADDR_LEN);
    MEMSET (&NextMacAddr, 0, MAC_ADDR_LEN);

    UserName.pu1_OctetList = (UINT1 *) au1UserName;
    NextUserName.pu1_OctetList = (UINT1 *) au1NextUserName;

    /* Get User Name (Index) of the first entry in User Session Table */
    i1RetValue =
        nmhGetFirstIndexFsWssUserSessionTable (&NextUserName, &NextMacAddr);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;
    if (i1RetValue == SNMP_FAILURE)
    {
        /* Since no entry is available in the User session table, return */
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html +
                                          pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WebnmUnRegisterLock (pHttp);

        return;
    }

    do
    {
        pHttp->i4Write = (INT4) u4Temp;
        /* Since fetching entry from the User session table printing the elements */
        MEMCPY (UserName.pu1_OctetList, NextUserName.pu1_OctetList,
                NextUserName.i4_Length);
        UserName.i4_Length = NextUserName.i4_Length;
        MEMCPY (MacAddr, NextMacAddr, MAC_ADDR_LEN);

        MEMSET (&NextMacAddr, 0, MAC_ADDR_LEN);
        MEMSET (&NextUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (&au1NextUserName, 0, MAX_USER_NAME_LENGTH);
        NextUserName.pu1_OctetList = (UINT1 *) au1NextUserName;

        MEMSET (au1String, 0, 21);
        PrintMacAddress (MacAddr, au1String);

        STRCPY (pHttp->au1KeyString, "USER_NAME_KEY");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", UserName.pu1_OctetList);
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "STATION_MAC_KEY");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsWssUserWlanIndex (&UserName, MacAddr, &u4WlanIndex);
        STRCPY (pHttp->au1KeyString, "WLAN_INDEX_KEY");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4WlanIndex);
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsWssUserAllotedBandWidth (&UserName, MacAddr,
                                         &u4AllotedBandWidth);
        STRCPY (pHttp->au1KeyString, "BANDWIDTH_KEY");
        if (u4AllotedBandWidth >= WSSUSER_1024Kbps)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d Mbps",
                     u4AllotedBandWidth / WSSUSER_1024Kbps);
        }

        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d Kbps",
                     u4AllotedBandWidth);
        }
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsWssUserAllotedVolume (&UserName, MacAddr, &u4AllotedVolume);
        STRCPY (pHttp->au1KeyString, "VOLUME_KEY");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d KB", u4AllotedVolume);
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsWssUserAllotedTime (&UserName, MacAddr, &u4AllotedTime);
        STRCPY (pHttp->au1KeyString, "TIME_KEY");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d Seconds", u4AllotedTime);
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsWssUserUsedVolume (&UserName, MacAddr, &u4UsedVolume);
        STRCPY (pHttp->au1KeyString, "USED_VOLUME_KEY");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d KB", u4UsedVolume);
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsWssUserUsedTime (&UserName, MacAddr, &u4UsedTime);
        STRCPY (pHttp->au1KeyString, "USED_TIME_KEY");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d Seconds", u4UsedTime);
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    }
    /* Getting the next entry from the User session table */
    while (nmhGetNextIndexFsWssUserSessionTable
           (&UserName, &NextUserName, MacAddr, &NextMacAddr) == SNMP_SUCCESS);

    WebnmUnRegisterLock (pHttp);
    WebnmUnRegisterContext (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    return;
}

/*********************************************************************
*  Function Name :IssProcessUrmSessionDetailspageSet
*  Description   :This function processes the Get request coming for the
*                 Session details Table page
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessUrmSessionDetailsPageSet (tHttp * pHttp)
{
    UNUSED_PARAM (pHttp);
    return;
}

/*********************************************************************
*  Function Name :IssProcessUrmSqlBasicPage
*  Description   :This function processes the request coming for the
*                 Sql Basic page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessUrmSqlBasicPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessUrmSqlBasicPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessUrmSqlBasicPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name :IssProcessUrmSqlBasicPageGet
*  Description   :This function processes the Get request coming for the
*                  Sql Insert page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessUrmSqlBasicPageGet (tHttp * pHttp)
{

    UINT4               u4Temp = 0;

    UINT1               u1aDbName[DB_LEN];
    tSNMP_OCTET_STRING_TYPE DbName;

    DbName.pu1_OctetList = u1aDbName;
    MEMSET (u1aDbName, 0, sizeof (u1aDbName));

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    pHttp->i4Write = (INT4) u4Temp;

    STRCPY (pHttp->au1KeyString, "MSQL_DB_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", DbName.pu1_OctetList);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    return;

}

/*********************************************************************
*  Function Name :IssProcessUrmSqlBasicPageSet
*  Description   :This function processes the Get request coming for the
*                  Sql Basic page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessUrmSqlBasicPageSet (tHttp * pHttp)
{
#ifdef WSSCUST_WANTED

    INT4                i4TableName = 0;
    CHR1               *pu1Prompt;

    UINT1               au1Cmd[MAX_PROMPT_LEN];
    tSNMP_OCTET_STRING_TYPE DatabaseName;

    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);
    MEMSET (&DatabaseName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    STRCPY (pHttp->au1Name, "MSQL_DB_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    MEMCPY (au1DatabaseName, pHttp->au1Value, sizeof (au1DatabaseName));

    STRCPY (pHttp->au1Name, "MSQL_Table");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4TableName = (INT4) (ATOI (pHttp->au1Value));    /*1.radreply 2.radcheck 3.radacct */

    if (i4TableName == 1)
    {
        pu1Prompt = "radreply";
    }
    else if (i4TableName == WLC_OFFSET_2)
    {
        pu1Prompt = "radcheck";
    }
    else if (i4TableName == WLC_OFFSET_3)
    {
        pu1Prompt = "radacct";
    }
    else if (i4TableName == WLC_OFFSET_4)
        pu1Prompt = "radgroupreply";

    else if (i4TableName == WLC_OFFSET_5)
        pu1Prompt = "radusergroup";
    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Start") == 0)
    {
        WssStartRadiusServer ();
    }
    if (STRCMP (pHttp->au1Value, "Create") == 0)
    {
        WssCreateDatabase (au1DatabaseName);
    }
    if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {
        WssDeleteDatabase (au1DatabaseName);
    }
    if (STRCMP (pHttp->au1Value, "Import") == 0)
    {
        WssImportDatabase ();
    }
    if (STRCMP (pHttp->au1Value, "Export") == 0)
    {
        WssExportDatabase ();
    }

    IssProcessUrmSqlBasicPageGet (pHttp);
    UNUSED_PARAM (pu1Prompt);
    return;
#else
    UNUSED_PARAM (pHttp);
#endif
}

/*********************************************************************
*  Function Name :IssProcessUrmSqlInsertPage
*  Description   :This function processes the request coming for the
*                 Sql Insert page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessUrmSqlInsertPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessUrmSqlInsertPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessUrmSqlInsertPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name :IssProcessUrmSqlInsertPageGet
*  Description   :This function processes the Get request coming for the
*                  Sql Insert page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessUrmSqlInsertPageGet (tHttp * pHttp)
{

    INT4                i4TableName = 0;
    INT4                i4AttType = 0;
    UINT4               u4Temp = 0;

    UINT1               u1aDbName[DB_LEN];
    tSNMP_OCTET_STRING_TYPE DbName;

    UINT1               u1aUName[DB_LEN];
    tSNMP_OCTET_STRING_TYPE UName;

    UINT1               u1aGName[DB_LEN];
    tSNMP_OCTET_STRING_TYPE GName;

    DbName.pu1_OctetList = u1aDbName;
    MEMSET (u1aDbName, 0, sizeof (u1aDbName));

    UName.pu1_OctetList = u1aUName;
    MEMSET (u1aUName, 0, sizeof (u1aUName));

    GName.pu1_OctetList = u1aGName;
    MEMSET (u1aGName, 0, sizeof (u1aGName));

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    pHttp->i4Write = (INT4) u4Temp;

    STRCPY (pHttp->au1KeyString, "MSQL_DB_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", DbName.pu1_OctetList);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "MSQL_Table_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4TableName);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "MSQL_AttType_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4AttType);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "MSQL_Uname_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", UName.pu1_OctetList);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "MSQL_Gname_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", GName.pu1_OctetList);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "MSQL_SSID_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "");
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "MSQL_Value_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "");
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    return;

}

/*********************************************************************
*  Function Name :IssProcessUrmSqlInsertPageSet
*  Description   :This function processes the Get request coming for the
*                  Sql Insert page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessUrmSqlInsertPageSet (tHttp * pHttp)
{
#ifdef WSSCUST_WANTED

    INT4                i4TableName = 0;
    INT4                i4AttType = 0;
    UINT4               u4Value = 0;

    UINT1               u1UserName[FWL_INDEX_20];
    UINT1               u1GroupName[FWL_INDEX_20];
    UINT1               u1Password[FWL_INDEX_20];
    UINT1               u1SSID[FWL_INDEX_20];
    CHR1               *u1Attribute = NULL;
    CHR1               *pu1Prompt = NULL;

    UINT1               au1Cmd[MAX_PROMPT_LEN];
    tSNMP_OCTET_STRING_TYPE DatabaseName;

    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);
    MEMSET (&DatabaseName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (u1Password, 0, sizeof (u1Password));

    STRCPY (pHttp->au1Name, "MSQL_DB_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    MEMCPY (au1DatabaseName, pHttp->au1Value, sizeof (au1DatabaseName));

    STRCPY (pHttp->au1Name, "MSQL_Table");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4TableName = (INT4) (ATOI (pHttp->au1Value));    /*1.radreply 2.radcheck 3.radacct */

    STRCPY (pHttp->au1Name, "MSQL_AttType");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4AttType = (UINT4) (ATOI (pHttp->au1Value));    /*1.Vol 2.Timeout 3.Bndwdth 4.DLB 5.ULB */

    STRCPY (pHttp->au1Name, "MSQL_Uname");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    MEMCPY (u1UserName, pHttp->au1Value, sizeof (u1UserName));

    STRCPY (pHttp->au1Name, "MSQL_Gname");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    MEMCPY (u1GroupName, pHttp->au1Value, sizeof (u1GroupName));

    STRCPY (pHttp->au1Name, "MSQL_SSID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    MEMCPY (u1SSID, pHttp->au1Value, sizeof (u1SSID));

    if (i4AttType == WLC_OFFSET_6)
    {
        STRCPY (pHttp->au1Name, "MSQL_Value");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        MEMCPY (u1Password, pHttp->au1Value, sizeof (u1Password));

    }
    else
    {
        STRCPY (pHttp->au1Name, "MSQL_Value");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Value = (UINT4) (ATOI (pHttp->au1Value));
    }

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Configure") == 0)
    {

        if (i4TableName == 1)
        {
            pu1Prompt = "radreply";
        }
        else if (i4TableName == WLC_OFFSET_2)
        {
            pu1Prompt = "radcheck";
        }
        else if (i4TableName == WLC_OFFSET_3)
        {
            pu1Prompt = "radacct";
        }
        else if (i4TableName == WLC_OFFSET_4)
        {
            pu1Prompt = "radgroupreply";
        }
        else if (i4TableName == WLC_OFFSET_5)
        {
            pu1Prompt = "radusergroup";
        }
        DatabaseName.i4_Length = (INT4) (STRLEN (pu1Prompt));

        DatabaseName.pu1_OctetList = (UINT1 *) pu1Prompt;

        SPRINTF ((CHR1 *) au1Cmd, "%s%s", SQL_CLI_CONFIG_MODE, pu1Prompt);

    }
    if (STRCMP (pHttp->au1Value, "Insert") == 0)
    {
        if (i4TableName == 1)
        {
            switch (i4AttType)
            {
                case 1:
                    u1Attribute = "Volume";
                    WssSetVolume (u1UserName, u1SSID, u4Value);
                    break;

                case 2:
                    u1Attribute = "Timeout";
                    WssSetTimeout (u1UserName, u1SSID, u4Value);
                    break;

                case 3:
                    u1Attribute = "Bandwidth";
                    WssSetBandwidth (u1UserName, u1SSID, u4Value);
                    break;

                case 4:
                    u1Attribute = "DLBandwidth";
                    WssSetDownloadBandwidth (u1UserName, u1SSID, u4Value);
                    break;

                case 5:
                    u1Attribute = "ULBandwidth";
                    WssSetUploadbandwidth (u1UserName, u1SSID, u4Value);
                    break;

                default:
                    break;
            }
        }
        if (i4TableName == WLC_OFFSET_2)
        {
            switch (i4AttType)
            {
                case 6:
                    u1Attribute = "Cleartext-Password";
                    WssInsertRadCheck (u1UserName, u1SSID, u1Password);

                    break;

                default:
                    break;
            }
        }

        if (i4TableName == WLC_OFFSET_3)
        {

        }
        if (i4TableName == WLC_OFFSET_4)
        {
            switch (i4AttType)
            {
                case 1:
                    u1Attribute = "Volume";
                    WssGroupSetVolume (u1GroupName, u1SSID, u4Value);
                    break;

                case 2:
                    u1Attribute = "Timeout";
                    WssGroupSetTimeout (u1GroupName, u1SSID, u4Value);
                    break;

                case 3:
                    u1Attribute = "Bandwidth";
                    WssGroupSetBandwidth (u1GroupName, u1SSID, u4Value);
                    break;

                case 4:
                    u1Attribute = "DLBandwidth";
                    WssGroupSetDownloadBandwidth (u1GroupName, u1SSID, u4Value);
                    break;

                case 5:
                    u1Attribute = "ULBandwidth";
                    WssGroupSetUploadbandwidth (u1GroupName, u1SSID, u4Value);
                    break;

                default:
                    break;
            }
        }
        if (i4TableName == WLC_OFFSET_5)
        {
            WssGroupInsertRadUserGroup (u1UserName, u1GroupName, u1SSID,
                                        u4Value);
        }

    }

    IssProcessUrmSqlInsertPageGet (pHttp);

    UNUSED_PARAM (u1Attribute);
    return;
#else
    UNUSED_PARAM (pHttp);
#endif
}

/*********************************************************************
*  Function Name :IssProcessUrmSqlUpdatePage
*  Description   :This function processes the request coming for the
*                 Sql Update page.
**  
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessUrmSqlUpdatePage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessUrmSqlUpdatePageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessUrmSqlUpdatePageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name :IssProcessUrmSqlUpdatePageGet
*  Description   :This function processes the Get request coming for the
*                  Sql Update page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessUrmSqlUpdatePageGet (tHttp * pHttp)
{
    INT4                i4TableName = 0;
    INT4                i4AttType = 0;
    UINT4               u4Temp = 0;

    UINT1               u1aDbName[DB_LEN];
    tSNMP_OCTET_STRING_TYPE DbName;

    DbName.pu1_OctetList = u1aDbName;
    MEMSET (u1aDbName, 0, sizeof (u1aDbName));

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    pHttp->i4Write = (INT4) u4Temp;

    STRCPY (pHttp->au1KeyString, "MSQL_DB_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", DbName.pu1_OctetList);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "MSQL_Table_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4TableName);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "MSQL_AttType_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4AttType);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "MSQL_Uname_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "");
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "MSQL_Gname_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "");
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "MSQL_SSID_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "");
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "MSQL_Value_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "");
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    return;

}

/*********************************************************************
*  Function Name :IssProcessUrmSqlUpdatePageSet
*  Description   :This function processes the Get request coming for the
*                  Sql Update page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessUrmSqlUpdatePageSet (tHttp * pHttp)
{
#ifdef WSSCUST_WANTED

    INT4                i4TableName = 0;
    INT4                i4AttType = 0;
    UINT4               u4Value = 0;

    UINT1               u1UserName[FWL_INDEX_20];
    UINT1               u1GroupName[FWL_INDEX_20];
    UINT1               u1SSID[FWL_INDEX_20];

    UINT1               u1Password[FWL_INDEX_20];

    UINT1              *pu1UserName2 = NULL;
    UINT1              *pu1GroupName2 = NULL;
    UINT1              *pu1SSID2 = NULL;

    MEMSET (u1UserName, 0, sizeof (u1UserName));
    MEMSET (u1GroupName, 0, sizeof (u1GroupName));
    MEMSET (u1SSID, 0, sizeof (u1SSID));
    MEMSET (u1Password, 0, sizeof (u1Password));

    STRCPY (pHttp->au1Name, "MSQL_DB_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    MEMCPY (au1DatabaseName, pHttp->au1Value, sizeof (au1DatabaseName));

    STRCPY (pHttp->au1Name, "MSQL_Table");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4TableName = (INT4) (ATOI (pHttp->au1Value));    /*1.radreply 2.radcheck 3.radacct 4.radgroupreply 5.radusergroup */

    STRCPY (pHttp->au1Name, "MSQL_AttType");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4AttType = (UINT4) (ATOI (pHttp->au1Value));    /*1.Vol 2.Timeout 3.Bndwdth 4.DLB 5.ULB */
    if (i4AttType == WLC_OFFSET_6)
    {
        STRCPY (pHttp->au1Name, "MSQL_Value");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        MEMCPY (u1Password, pHttp->au1Value, sizeof (u1Password));

    }
    else
    {
        STRCPY (pHttp->au1Name, "MSQL_Value");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Value = (UINT4) (ATOI (pHttp->au1Value));
    }

    STRCPY (pHttp->au1Name, "MSQL_SSID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    MEMCPY (u1SSID, pHttp->au1Value, sizeof (u1SSID));
    if (u1SSID[0] != 0)
    {
        pu1SSID2 = u1SSID;
    }

    STRCPY (pHttp->au1Name, "MSQL_Uname");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    MEMCPY (u1UserName, pHttp->au1Value, sizeof (u1UserName));
    if (u1UserName[0] != 0)
    {
        pu1UserName2 = u1UserName;
    }

    STRCPY (pHttp->au1Name, "MSQL_Gname");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    MEMCPY (u1GroupName, pHttp->au1Value, sizeof (u1GroupName));
    if (u1GroupName[0] != 0)
    {
        pu1GroupName2 = u1GroupName;
    }

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Update") == 0)
    {
        if (i4TableName == 1)
        {
            switch (i4AttType)
            {
                case 1:
                    WssUpdateVolume (pu1UserName2, pu1SSID2, u4Value);
                    break;

                case 2:
                    WssUpdateTimeout (pu1UserName2, pu1SSID2, u4Value);
                    break;

                case 3:
                    WssUpdateBandwidth (pu1UserName2, pu1SSID2, u4Value);
                    break;

                case 4:
                    WssUpdateDownloadBandwidth (pu1UserName2, pu1SSID2,
                                                u4Value);
                    break;

                case 5:
                    WssUpdateUploadBandwidth (pu1UserName2, pu1SSID2, u4Value);
                    break;

                default:
                    break;
            }
        }

        if (i4TableName == WLC_OFFSET_2)
        {
            WssUpdateRadCheck (pu1UserName2, pu1SSID2, u1Password);

        }
        if (i4TableName == WLC_OFFSET_4)
        {
            switch (i4AttType)
            {
                case 1:
                    WssGroupUpdateVolume (pu1GroupName2, pu1SSID2, u4Value);
                    break;

                case 2:
                    WssGroupUpdateTimeout (pu1GroupName2, pu1SSID2, u4Value);
                    break;

                case 3:
                    WssGroupUpdateBandwidth (pu1GroupName2, pu1SSID2, u4Value);
                    break;

                case 4:
                    WssGroupUpdateDownloadBandwidth (pu1GroupName2, pu1SSID2,
                                                     u4Value);
                    break;

                case 5:
                    WssGroupUpdateUploadBandwidth (pu1GroupName2, pu1SSID2,
                                                   u4Value);
                    break;

                default:
                    printf ("In default\n");
                    break;
            }
        }

    }

    IssProcessUrmSqlUpdatePageGet (pHttp);
    return;
#else
    UNUSED_PARAM (pHttp);
#endif
}

/*********************************************************************
*  Function Name :IssProcessUrmSqlDeletePage
*  Description   :This function processes the request coming for the
*                 Sql Delete page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessUrmSqlDeletePage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessUrmSqlDeletePageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessUrmSqlDeletePageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name :IssProcessUrmSqlDeletePageGet
*  Description   :This function processes the Get request coming for the
*                  Sql Delete page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessUrmSqlDeletePageGet (tHttp * pHttp)
{
    INT4                i4TableName = 0;
    UINT4               u4Temp = 0;
    UINT1               u1aDbName[DB_LEN];
    tSNMP_OCTET_STRING_TYPE DbName;

    UINT1               u1aGName[DB_LEN];
    tSNMP_OCTET_STRING_TYPE GName;

    DbName.pu1_OctetList = u1aDbName;
    MEMSET (u1aDbName, 0, sizeof (u1aDbName));

    GName.pu1_OctetList = u1aGName;
    MEMSET (u1aGName, 0, sizeof (u1aGName));

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    pHttp->i4Write = (INT4) u4Temp;

    STRCPY (pHttp->au1KeyString, "MSQL_DB_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", DbName.pu1_OctetList);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "MSQL_Table_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4TableName);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "MSQL_Uname_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "");
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "MSQL_Gname_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", GName.pu1_OctetList);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "MSQL_SSID_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "");
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

}

/*********************************************************************
*  Function Name :IssProcessUrmSqlDeletePageSet
*  Description   :This function processes the Set request coming for the
*                  Sql Delete page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessUrmSqlDeletePageSet (tHttp * pHttp)
{
#ifdef WSSCUST_WANTED
    INT4                i4TableName = 0;
    INT4                i4AttType = 0;

    UINT1               u1UserName[FWL_INDEX_20];
    UINT1               u1GroupName[FWL_INDEX_20];
    UINT1               u1SSID[FWL_INDEX_20];
    UINT1               u1Attribute[FWL_INDEX_20];
    UINT1               u1Password[FWL_INDEX_20];

    UINT1              *pu1UserName = NULL;
    UINT1              *pu1SSID = NULL;
    UINT1              *pu1Attribute = NULL;

    MEMSET (u1Attribute, 0, sizeof (u1Attribute));
    MEMSET (u1UserName, 0, sizeof (u1UserName));
    MEMSET (u1SSID, 0, sizeof (u1SSID));
    MEMSET (u1Password, 0, sizeof (u1Password));

    STRCPY (pHttp->au1Name, "MSQL_SSID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    MEMCPY (u1SSID, pHttp->au1Value, sizeof (u1SSID));
    if (u1SSID[0] != 0)
    {
        pu1SSID = u1SSID;
    }

    STRCPY (pHttp->au1Name, "MSQL_DB_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    MEMCPY (au1DatabaseName, pHttp->au1Value, sizeof (au1DatabaseName));

    STRCPY (pHttp->au1Name, "MSQL_Table");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4TableName = (INT4) (ATOI (pHttp->au1Value));    /*1.radreply 2.radcheck 3.radacct 4.radgroupreply 5.radusergroup */

    STRCPY (pHttp->au1Name, "MSQL_AttType");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4AttType = (UINT4) (ATOI (pHttp->au1Value));    /*1.Vol 2.Timeout 3.Bndwdth 4.DLB 5.ULB */

    if (i4AttType == 1)
        MEMCPY (u1Attribute, "Volume", sizeof (u1Attribute));

    if (i4AttType == WLC_OFFSET_2)
        MEMCPY (u1Attribute, "Timeout", sizeof (u1Attribute));

    if (i4AttType == WLC_OFFSET_3)
        MEMCPY (u1Attribute, "Bandwidth", sizeof (u1Attribute));

    if (i4AttType == WLC_OFFSET_4)
        MEMCPY (u1Attribute, "DLBandwidh", sizeof (u1Attribute));

    if (i4AttType == WLC_OFFSET_5)
        MEMCPY (u1Attribute, "ULBandwidh", sizeof (u1Attribute));

    if (i4AttType == WLC_OFFSET_6)
        MEMCPY (u1Attribute, "Cleartext-Password", sizeof (u1Attribute));

    if (i4AttType == WLC_OFFSET_7)
        MEMCPY (u1Attribute, "All", sizeof (u1Attribute));

    if (STRNCMP (u1Attribute, "All", sizeof (u1Attribute)) != 0)
    {
        pu1Attribute = u1Attribute;
    }

    STRCPY (pHttp->au1Name, "MSQL_Uname");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    MEMCPY (u1UserName, pHttp->au1Value, sizeof (u1UserName));
    if (u1UserName[0] != 0)
    {
        pu1UserName = u1UserName;
    }

    STRCPY (pHttp->au1Name, "MSQL_Gname");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    MEMCPY (u1GroupName, pHttp->au1Value, sizeof (u1GroupName));

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {
        if (i4TableName == 1)
        {
            WssDeleteRadReply (pu1UserName, pu1SSID, pu1Attribute);
        }
        if (i4TableName == WLC_OFFSET_2)
        {
            WssDeleteRadCheck (pu1UserName, pu1SSID, pu1Attribute);
        }
        if (i4TableName == WLC_OFFSET_3)
        {
            WssDeleteRadAcct ();
        }
        if (i4TableName == WLC_OFFSET_4)
        {
            WssDeleteRadGroupReply (u1GroupName, pu1SSID, pu1Attribute);
        }
        if (i4TableName == WLC_OFFSET_5)
        {
            WssDeleteUserFromRadGroupReply (pu1UserName, u1GroupName, pu1SSID);
        }
    }

    IssProcessUrmSqlDeletePageGet (pHttp);
    return;
#else
    UNUSED_PARAM (pHttp);
#endif
}
#endif
#endif
