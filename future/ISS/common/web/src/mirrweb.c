/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved]
 *
 * $Id: mirrweb.c,v 1.12 2015/09/02 12:03:00 siva Exp $
 *
 * Description: This file contains routines for Mirroring web Module
 *
 ********************************************************************/

#ifndef _MIRR_WEB_C_
#define _MIRR_WEB_C_

#ifdef WEBNM_WANTED
#include "webiss.h"
#include "webinc.h"
#include "isshttp.h"
#include "snmputil.h"

#ifdef ISS_WANTED
/*********************************************************************
*  Function Name : IssProcessMirrCtrlExtPage
*  Description   : This function processes the request coming for the 
*                  PG information page.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessMirrCtrlExtPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessMirrCtrlExtPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessMirrCtrlExtPageSet (pHttp);
    }
    return;
}

/*******************************************************************************
 *  Function Name : IssProcessMirrCtrlExtPageGet
 *  Description   : This function processes the get request for the Mirroring
 *                  configuration page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ******************************************************************************/
VOID
IssProcessMirrCtrlExtPageGet (tHttp * pHttp)
{
    UINT1               au1Buf[ISS_MAX_ADDR_BUFFER];
    UINT1               au1VlnBuf[ISS_MAX_ADDR_BUFFER];
    UINT4               u4Temp = 0;
    UINT1              *pu1Ptr = NULL;
    UINT1              *pu1Ptr1 = NULL;
    UINT1               u1Index = 0;
    INT4                i4SessionIndex = 0;
    INT4                i4NextSessionIndex = 0;
    INT4                i4SrcIndex = 1;
    INT4                i4SrcId = 0;
    INT4                i4NextSrcIndex = 0;
    INT4                i4NextSrcId = 0;
    INT4                i4DescId = 0;
    INT4                i4NextDescId = 0;
    INT4                i4VlnContext = 0;
    INT4                i4NextVlnContext = 0;
    INT4                i4VlanId = 0;
    INT4                i4NextVlanId = 0;
    INT4                i4PortMode = 0;
    INT4                i4VlanMode = 0;
    INT4                i4MirrType = 0;
    INT4                i4RetVal = 0;

    tPortList           IfSourcePortList;
    tPortList           IfDestPortList;
    tSNMP_MULTI_DATA_TYPE *pu1DataString = NULL;

    if ((pu1DataString = WebnmAllocMultiData ()) == NULL)
    {
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        return;
    }

    WebnmRegisterLock (pHttp, IssLock, IssUnLock);
    ISS_LOCK ();

    pHttp->i4Write = 0;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    if (nmhGetFirstIndexIssMirrorCtrlExtnTable (&i4SessionIndex)
        == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;

    i4SessionIndex = 0;

    while (nmhGetNextIndexIssMirrorCtrlExtnTable (i4SessionIndex,
                                                  &i4NextSessionIndex)
           == SNMP_SUCCESS)
    {
        MEMSET (au1Buf, 0, ISS_MAX_ADDR_BUFFER);

        pHttp->i4Write = (INT4) u4Temp;

        /* Context ID */
        STRCPY (pHttp->au1KeyString, "SESSION_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextSessionIndex);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Mirror Type */
        STRCPY (pHttp->au1KeyString, "MIRROR_TYPE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetIssMirrorCtrlExtnMirrType (i4NextSessionIndex,
                                             &i4MirrType) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4MirrType);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        if (i4MirrType != ISS_MIRR_VLAN_BASED)
        {
            /* Source Entity */
            i4SrcIndex = 1;
            u1Index = 0;
            pu1Ptr = au1Buf;
            while (nmhGetNextIndexIssMirrorCtrlExtnSrcTable (i4SrcIndex,
                                                             &i4NextSrcIndex,
                                                             i4SrcId,
                                                             &i4NextSrcId)
                   == SNMP_SUCCESS)
            {
                i4SrcIndex = i4NextSrcIndex;
                i4SrcId = i4NextSrcId;

                if (i4SrcIndex != i4NextSessionIndex)
                {
                    continue;
                }
                if (nmhGetIssMirrorCtrlExtnSrcCfg (i4NextSrcIndex,
                                                   i4NextSrcId,
                                                   &i4RetVal) == SNMP_SUCCESS)
                {
                    if (u1Index++ != 0)
                    {
                        SPRINTF ((CHR1 *) pu1Ptr + STRLEN (pu1Ptr), "%c", ',');
                    }
                    SPRINTF ((CHR1 *) pu1Ptr + STRLEN (pu1Ptr), "%d",
                             i4NextSrcId);
                }
                nmhGetIssMirrorCtrlExtnSrcMode (i4NextSrcIndex,
                                                i4NextSrcId, &i4PortMode);
            }
        }
        MEMSET (IfSourcePortList, 0, BRG_PORT_LIST_SIZE);
        STRCPY (pHttp->au1KeyString, "SRC_ENTITY_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if ((i4MirrType == ISS_MIRR_MAC_FLOW_BASED)
            || (i4MirrType == ISS_MIRR_IP_FLOW_BASED))
        {
            STRCPY ((CHR1 *) pHttp->au1DataString, au1Buf);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

        }
        else if (i4MirrType == ISS_MIRR_PORT_BASED)
        {
            ConvertStrToPortList (au1Buf, IfSourcePortList, BRG_PORT_LIST_SIZE,
                                  BRG_MAX_PHY_PLUS_LOG_PORTS);
            IssConvertToIfPortList (pHttp, pu1DataString->pOctetStrValue,
                                    IfSourcePortList);
            pu1DataString->pOctetStrValue->i4_Length = CONTEXT_PORT_LIST_SIZE;
            MEMCPY (pu1DataString->pOctetStrValue->pu1_OctetList,
                    IfSourcePortList, pu1DataString->pOctetStrValue->i4_Length);
            WebnmSendToSocket (pHttp, "SRC_ENTITY_KEY", pu1DataString,
                               ENM_PORT_LIST);
            MEMSET (pu1DataString->pOctetStrValue->pu1_OctetList, 0,
                    pu1DataString->pOctetStrValue->i4_Length);
        }

        /* Destination Entity */
        MEMSET (au1Buf, 0, sizeof (au1Buf));
        i4SrcIndex = 1;
        u1Index = 0;
        pu1Ptr = au1Buf;
        while (nmhGetNextIndexIssMirrorCtrlExtnDestinationTable (i4SrcIndex,
                                                                 &i4NextSrcIndex,
                                                                 i4DescId,
                                                                 &i4NextDescId)
               == SNMP_SUCCESS)
        {
            i4SrcIndex = i4NextSrcIndex;
            i4DescId = i4NextDescId;
            if (i4SrcIndex != i4NextSessionIndex)
            {
                continue;
            }
            if (nmhGetIssMirrorCtrlExtnDestCfg (i4NextSrcIndex,
                                                i4NextDescId,
                                                &i4RetVal) == SNMP_SUCCESS)
            {
                if (u1Index++ != 0)
                {
                    SPRINTF ((CHR1 *) pu1Ptr + STRLEN (pu1Ptr), "%c", ',');
                }
                SPRINTF ((CHR1 *) pu1Ptr + STRLEN (pu1Ptr), "%d", i4DescId);
            }
        }
        MEMSET (IfDestPortList, 0, BRG_PORT_LIST_SIZE);
        STRCPY (pHttp->au1KeyString, "DEST_ENTITY_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        ConvertStrToPortList (au1Buf, IfDestPortList, BRG_PORT_LIST_SIZE,
                              SYS_DEF_MAX_PHYSICAL_INTERFACES);
        IssConvertToIfPortList (pHttp, pu1DataString->pOctetStrValue,
                                IfDestPortList);
        pu1DataString->pOctetStrValue->i4_Length = CONTEXT_PORT_LIST_SIZE;
        MEMCPY (pu1DataString->pOctetStrValue->pu1_OctetList, IfDestPortList,
                pu1DataString->pOctetStrValue->i4_Length);
        WebnmSendToSocket (pHttp, "DEST_ENTITY_KEY", pu1DataString,
                           ENM_PORT_LIST);
        MEMSET (pu1DataString->pOctetStrValue->pu1_OctetList, 0,
                pu1DataString->pOctetStrValue->i4_Length);

        /* Vlan */
        MEMSET (au1Buf, 0, sizeof (au1Buf));
        MEMSET (au1VlnBuf, 0, sizeof (au1VlnBuf));
        i4SrcIndex = 1;
        u1Index = 0;
        pu1Ptr = au1Buf;
        pu1Ptr1 = au1VlnBuf;
        while (nmhGetNextIndexIssMirrorCtrlExtnSrcVlanTable (i4SrcIndex,
                                                             &i4NextSrcIndex,
                                                             i4VlnContext,
                                                             &i4NextVlnContext,
                                                             i4VlanId,
                                                             &i4NextVlanId)
               == SNMP_SUCCESS)
        {
            i4SrcIndex = i4NextSrcIndex;
            i4VlnContext = i4NextVlnContext;
            i4VlanId = i4NextVlanId;

            if (i4SrcIndex != i4NextSessionIndex)
            {
                continue;
            }
            if (nmhGetIssMirrorCtrlExtnSrcVlanCfg (i4NextSrcIndex,
                                                   i4NextVlnContext,
                                                   i4NextVlanId,
                                                   &i4RetVal) == SNMP_SUCCESS)
            {
                if (u1Index++ != 0)
                {
                    SPRINTF ((CHR1 *) pu1Ptr + STRLEN (pu1Ptr), "%c", ',');
                    SPRINTF ((CHR1 *) pu1Ptr1 + STRLEN (pu1Ptr1), "%c", ',');
                }
                SPRINTF ((CHR1 *) pu1Ptr + STRLEN (pu1Ptr), "%d",
                         i4NextVlnContext);
                SPRINTF ((CHR1 *) pu1Ptr1 + STRLEN (pu1Ptr1), "%d",
                         i4NextVlanId);
            }
            nmhGetIssMirrorCtrlExtnSrcVlanMode (i4NextSrcIndex,
                                                i4NextVlnContext,
                                                i4NextVlanId, &i4VlanMode);
        }

        /* Mode */
        STRCPY (pHttp->au1KeyString, "MODE_TYPE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4MirrType == ISS_MIRR_PORT_BASED)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4PortMode);
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4VlanMode);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Vlan Context */
        STRCPY (pHttp->au1KeyString, "CONTEXT_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Buf);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Vlan ID */
        STRCPY (pHttp->au1KeyString, "VLAN_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1VlnBuf);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* RSpan Status */
        STRCPY (pHttp->au1KeyString, "RSPAN_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetIssMirrorCtrlExtnRSpanStatus (i4NextSessionIndex,
                                                &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Rspan Vlan ID */
        STRCPY (pHttp->au1KeyString, "RSPAN_VLANID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetIssMirrorCtrlExtnRSpanVlanId (i4NextSessionIndex,
                                                &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* RSpan Contextt */
        STRCPY (pHttp->au1KeyString, "RSPAN_CONTEXT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetIssMirrorCtrlExtnRSpanContext (i4NextSessionIndex,
                                                 &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        /* Rowstatus */
        STRCPY (pHttp->au1KeyString, "ROW_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4RetVal = 0;
        if (nmhGetIssMirrorCtrlExtnStatus (i4NextSessionIndex,
                                           &i4RetVal) == SNMP_SUCCESS)
        {
            if (i4RetVal == 1)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Up");
            }
            else if (i4RetVal == 2)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Down");
            }
            else if (i4RetVal == 3)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Under creation");
            }
            else
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "--");
            }
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        i4SessionIndex = i4NextSessionIndex;
    }

    ISS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*******************************************************************************
*  Function Name : IssProcessMirrCtrlExtPageSet
*  Description   : This function processes the HTTP set request coming for the
*                  Elps Protection Group configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None.
*******************************************************************************/
VOID
IssProcessMirrCtrlExtPageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE *pSrcId = NULL;
    tSNMP_OCTET_STRING_TYPE *pDescId = NULL;
    tSNMP_OCTET_STRING_TYPE *pCxtId = NULL;
    tSNMP_OCTET_STRING_TYPE *pVlanId = NULL;
    UINT4               u4ErrValue;
    CHR1               *pc1SrcTok = NULL;
    CHR1               *pc1DescTok = NULL;
    CHR1               *pc1CxtTok = NULL;
    CHR1               *pc1VlnTok = NULL;
    CHR1               *pc1SavCxt = NULL;
    CHR1               *pc1SavVln = NULL;
    INT1                ai1IfName[3];
    INT4                i4SessionIndex = 0;
    INT4                i4MirrType = 0;
    INT4                i4RSpanStatus = 0;
    INT4                i4RSpanVlanId = 0;
    INT4                i4RSpanContext = 0;
    UINT4               u4SrcId = 0;
    UINT4               u4DescId = 0;
    INT4                i4ModeType = 0;
    INT4                i4ContextId = 0;
    INT4                i4VlanId = 0;
    INT4                i4AddOrDelete = 0;
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = 0;
    INT4                i4OctetAllocCnt = 0;

    WebnmRegisterLock (pHttp, IssLock, IssUnLock);
    ISS_LOCK ();

    STRCPY (pHttp->au1Name, "SESSION_ID");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4SessionIndex = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "MIRROR_TYPE");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4MirrType = ATOI (pHttp->au1Value);
    }

    pSrcId = allocmem_octetstring (ISS_MAX_ADDR_BUFFER);
    if (pSrcId == NULL)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to allocate memory");
        return;
    }
    i4OctetAllocCnt++;
    MEMSET (pSrcId->pu1_OctetList, 0, ISS_MAX_ADDR_BUFFER);
    pSrcId->i4_Length = 0;

    STRCPY (pHttp->au1Name, "SRC_ENTITY");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        pSrcId->i4_Length = (INT4) STRLEN (pHttp->au1Value);

        if ((UINT4) pSrcId->i4_Length >= ISS_MAX_ADDR_BUFFER)
        {
            pSrcId->i4_Length = ISS_MAX_ADDR_BUFFER - 1;
        }
        MEMCPY (pSrcId->pu1_OctetList, pHttp->au1Value, pSrcId->i4_Length);
    }

    pDescId = allocmem_octetstring (ISS_MAX_ADDR_BUFFER);
    if (pDescId == NULL)
    {
        MirrWebMemRelease (i4OctetAllocCnt, pSrcId);
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to allocate memory");
        return;
    }
    i4OctetAllocCnt++;
    MEMSET (pDescId->pu1_OctetList, 0, ISS_MAX_ADDR_BUFFER);
    pDescId->i4_Length = 0;

    STRCPY (pHttp->au1Name, "DEST_ENTITY");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        pDescId->i4_Length = (INT4) STRLEN (pHttp->au1Value);
        if ((UINT4) pDescId->i4_Length >= ISS_MAX_ADDR_BUFFER)
        {
            pDescId->i4_Length = ISS_MAX_ADDR_BUFFER - 1;
        }
        MEMCPY (pDescId->pu1_OctetList, pHttp->au1Value, pDescId->i4_Length);
    }

    STRCPY (pHttp->au1Name, "MODE_TYPE");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4ModeType = ATOI (pHttp->au1Value);
    }
    pCxtId = allocmem_octetstring (ISS_MAX_ADDR_BUFFER);
    if (pCxtId == NULL)
    {
        MirrWebMemRelease (i4OctetAllocCnt, pSrcId, pDescId);
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to allocate memory");
        return;
    }
    i4OctetAllocCnt++;
    MEMSET (pCxtId->pu1_OctetList, 0, ISS_MAX_ADDR_BUFFER);
    pCxtId->i4_Length = 0;

    STRCPY (pHttp->au1Name, "CONTEXT_ID");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        pCxtId->i4_Length = (INT4) STRLEN (pHttp->au1Value);
        if ((UINT4) pCxtId->i4_Length >= ISS_MAX_ADDR_BUFFER)
        {
            pCxtId->i4_Length = ISS_MAX_ADDR_BUFFER - 1;
        }
        MEMCPY (pCxtId->pu1_OctetList, pHttp->au1Value, pCxtId->i4_Length);
    }
    pVlanId = allocmem_octetstring (ISS_MAX_ADDR_BUFFER);
    if (pVlanId == NULL)
    {
        MirrWebMemRelease (i4OctetAllocCnt, pSrcId, pDescId, pCxtId);
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to allocate memory");
        return;
    }
    i4OctetAllocCnt++;
    MEMSET (pVlanId->pu1_OctetList, 0, ISS_MAX_ADDR_BUFFER);
    pVlanId->i4_Length = 0;

    STRCPY (pHttp->au1Name, "VLAN_ID");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        pVlanId->i4_Length = (INT4) STRLEN (pHttp->au1Value);
        if ((UINT4) pVlanId->i4_Length >= ISS_MAX_ADDR_BUFFER)
        {
            pVlanId->i4_Length = ISS_MAX_ADDR_BUFFER - 1;
        }
        MEMCPY (pVlanId->pu1_OctetList, pHttp->au1Value, pVlanId->i4_Length);
    }

    STRCPY (pHttp->au1Name, "RSPAN_STATUS");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4RSpanStatus = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "RSPAN_VLANID");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4RSpanVlanId = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "RSPAN_CONTEXT");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4RSpanContext = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "ADD_DELETE");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        i4AddOrDelete = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "ACTION");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) == ENM_SUCCESS)
    {
        if (STRCMP (pHttp->au1Value, "Add") == 0)
        {
            /* Set Interface table rowstatus as Create And Wait */

            i4RetVal = nmhGetIssMirrorCtrlExtnStatus (i4SessionIndex,
                                                      &i4RowStatus);
            if (i4RetVal != SNMP_SUCCESS)
            {
                if (nmhTestv2IssMirrorCtrlExtnStatus (&u4ErrValue,
                                                      i4SessionIndex,
                                                      CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    MirrWebMemRelease (i4OctetAllocCnt, pSrcId,
                                       pDescId, pCxtId, pVlanId);
                    ISS_UNLOCK ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                                  " needed to create table");
                    return;
                }
                if (nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex,
                                                   CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    MirrWebMemRelease (i4OctetAllocCnt, pSrcId,
                                       pDescId, pCxtId, pVlanId);
                    ISS_UNLOCK ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Entry creation failed");
                    return;
                }
            }
            else
            {
                MirrWebMemRelease (i4OctetAllocCnt, pSrcId,
                                   pDescId, pCxtId, pVlanId);
                ISS_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Entry Already Exists");
                return;
            }
        }

        if (STRCMP (pHttp->au1Value, "Apply") == 0)
        {

            if (nmhTestv2IssMirrorCtrlExtnStatus (&u4ErrValue, i4SessionIndex,
                                                  NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                MirrWebMemRelease (i4OctetAllocCnt, pSrcId,
                                   pDescId, pCxtId, pVlanId);
                ISS_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                              "needed to activate");
                return;
            }
            if (nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex,
                                               NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                MirrWebMemRelease (i4OctetAllocCnt, pSrcId,
                                   pDescId, pCxtId, pVlanId);
                ISS_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Entry creation failed");
                return;
            }
        }
        if (STRCMP (pHttp->au1Value, "Delete") == 0)
        {
            /* Delete the PgCfm Rowstatus */
            if (nmhTestv2IssMirrorCtrlExtnStatus (&u4ErrValue, i4SessionIndex,
                                                  DESTROY) == SNMP_FAILURE)
            {
                MirrWebMemRelease (i4OctetAllocCnt, pSrcId,
                                   pDescId, pCxtId, pVlanId);
                ISS_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "InValid Input");
                return;
            }
            if (nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex,
                                               DESTROY) == SNMP_FAILURE)
            {
                MirrWebMemRelease (i4OctetAllocCnt, pSrcId,
                                   pDescId, pCxtId, pVlanId);
                ISS_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Entry creation failed");
                return;
            }
            ISS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssProcessMirrCtrlExtPageGet (pHttp);
            free_octetstring (pVlanId);
            free_octetstring (pSrcId);
            free_octetstring (pDescId);
            free_octetstring (pCxtId);
            return;
        }
    }
    else
    {
        MirrWebMemRelease (i4OctetAllocCnt, pSrcId, pDescId, pCxtId, pVlanId);
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Input");
        return;
    }

    /* Mirror Type */
    if (nmhTestv2IssMirrorCtrlExtnMirrType (&u4ErrValue, i4SessionIndex,
                                            i4MirrType) == SNMP_FAILURE)
    {
        MirrWebMemRelease (i4OctetAllocCnt, pSrcId, pDescId, pCxtId, pVlanId);
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to configure Mirror Type");
        return;
    }

    if (nmhSetIssMirrorCtrlExtnMirrType (i4SessionIndex,
                                         i4MirrType) == SNMP_FAILURE)
    {
        MirrWebMemRelease (i4OctetAllocCnt, pSrcId, pDescId, pCxtId, pVlanId);
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set Mirror Type");
        return;
    }

    if (pSrcId->i4_Length != 0)
    {
        pc1SrcTok = STRTOK (pSrcId->pu1_OctetList, ",");

        while (pc1SrcTok != NULL)
        {
            if (i4MirrType == ISS_MIRR_PORT_BASED)
            {
                /* source entity value gigabitethernet or fastethernet or port-channel
                 * ex: gi0/1 or fa0/1 or ex0/1 or po1*/
                if ((STRSTR (pc1SrcTok, "gi") != NULL)
                    || (STRSTR (pc1SrcTok, "Gi") != NULL))
                {
                    STRCPY (ai1IfName, "gi");
                    CfaCliGetIfIndex (ai1IfName, (INT1 *) (&pc1SrcTok[2]),
                                      &u4SrcId);
                }
                else if ((STRSTR (pc1SrcTok, "fa") != NULL)
                         || (STRSTR (pc1SrcTok, "Fa") != NULL))
                {
                    STRCPY (ai1IfName, "fa");
                    CfaCliGetIfIndex (ai1IfName, (INT1 *) (&pc1SrcTok[2]),
                                      &u4SrcId);
                }
                else if ((STRSTR (pc1SrcTok, "ex") != NULL)
                         || (STRSTR (pc1SrcTok, "Ex") != NULL))
                {
                    STRCPY (ai1IfName, "ex");
                    CfaCliGetIfIndex (ai1IfName, (INT1 *) (&pc1SrcTok[2]),
                                      &u4SrcId);
                }
                else if (STRSTR (pc1SrcTok, "po") != NULL)
                {
                    STRCPY (ai1IfName, "po");
                    CfaCliGetIfIndex (ai1IfName, (INT1 *) (&pc1SrcTok[2]),
                                      &u4SrcId);
                }
                else
                {
                    MirrWebMemRelease (i4OctetAllocCnt, pSrcId,
                                       pDescId, pCxtId, pVlanId);
                    ISS_UNLOCK ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *)
                                  "Unable to configure Source Entity. Enter valid ports Ex:gi0/1,fa0/1,po1");
                    if (STRCMP (pHttp->au1Value, "Add") == 0)
                    {
                        nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, DESTROY);
                    }
                    else
                    {
                        nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, ACTIVE);
                    }
                    return;
                }
                if (u4SrcId == 0)
                {
                    IssSendError (pHttp, (CONST INT1 *)
                                  "Unable to configure Source Entity."
                                  "Madatory parameters needed to activate");
                    if (STRCMP (pHttp->au1Value, "Add") == 0)
                    {
                        nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, DESTROY);
                    }
                    else
                    {
                        nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, ACTIVE);
                    }
                    ISS_UNLOCK ();
                    WebnmUnRegisterLock (pHttp);
                    free_octetstring (pVlanId);
                    free_octetstring (pSrcId);
                    free_octetstring (pDescId);
                    free_octetstring (pCxtId);
                    return;
                }

            }
            else if ((i4MirrType == ISS_MIRR_MAC_FLOW_BASED)
                     || (i4MirrType == ISS_MIRR_IP_FLOW_BASED))
            {
                u4SrcId = (UINT4) ATOI (&pc1SrcTok[0]);
                if (u4SrcId == 0)
                {
                    ISS_UNLOCK ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *)
                                  "Unable to configure Source Entity. Input should be between 1 and 65535");
                    if (STRCMP (pHttp->au1Value, "Add") == 0)
                    {
                        nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, DESTROY);
                    }
                    else
                    {
                        nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, ACTIVE);
                    }
                    free_octetstring (pVlanId);
                    free_octetstring (pSrcId);
                    free_octetstring (pDescId);
                    free_octetstring (pCxtId);
                    return;
                }

            }
            else
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to configure Source Entity. Invalid mirror-type");
                MirrWebMemRelease (i4OctetAllocCnt, pSrcId,
                                   pDescId, pCxtId, pVlanId);
                ISS_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to configure Source Entity. Input should be between 1 and 65535");
                if (STRCMP (pHttp->au1Value, "Add") == 0)
                {
                    nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, DESTROY);
                }
                else
                {
                    nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, ACTIVE);
                }
                free_octetstring (pVlanId);
                free_octetstring (pCxtId);
                free_octetstring (pDescId);
                free_octetstring (pSrcId);
                return;
            }

            if (nmhTestv2IssMirrorCtrlExtnSrcCfg (&u4ErrValue,
                                                  i4SessionIndex,
                                                  (INT4) u4SrcId, i4AddOrDelete)
                == SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to configure Source Entity");
                if (STRCMP (pHttp->au1Value, "Add") == 0)
                {
                    nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, DESTROY);
                }
                else
                {
                    nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, ACTIVE);
                }
                free_octetstring (pVlanId);
                free_octetstring (pCxtId);
                free_octetstring (pDescId);
                free_octetstring (pSrcId);
                return;
            }

            if (nmhSetIssMirrorCtrlExtnSrcCfg (i4SessionIndex,
                                               (INT4) u4SrcId,
                                               i4AddOrDelete) == SNMP_FAILURE)
            {
                MirrWebMemRelease (i4OctetAllocCnt, pSrcId,
                                   pDescId, pCxtId, pVlanId);
                ISS_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Unable to set "
                              "Source Entity");
                if (STRCMP (pHttp->au1Value, "Add") == 0)
                {
                    nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, DESTROY);
                }
                else
                {
                    nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, ACTIVE);
                }
                return;
            }
            if (i4MirrType == ISS_MIRR_PORT_BASED &&
                i4AddOrDelete == ISS_MIRR_ADD)
            {
                /* Port Mode */
                if (nmhSetIssMirrorCtrlExtnSrcMode (i4SessionIndex,
                                                    (INT4) u4SrcId,
                                                    i4ModeType) == SNMP_FAILURE)
                {
                    IssSendError (pHttp, (CONST INT1 *) "Unable to set "
                                  " Port Mode");
                    if (STRCMP (pHttp->au1Value, "Add") == 0)
                    {
                        nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, DESTROY);
                    }
                    else
                    {
                        nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, ACTIVE);
                    }
                    MirrWebMemRelease (i4OctetAllocCnt, pSrcId,
                                       pDescId, pCxtId, pVlanId);
                    ISS_UNLOCK ();
                    WebnmUnRegisterLock (pHttp);
                    return;
                }
            }
            u4SrcId = 0;
            pc1SrcTok = STRTOK (NULL, ",");
        }
    }
    else if (i4MirrType == ISS_MIRR_VLAN_BASED) /* Vlan Based */
    {
        /*Skipping for Vlan based*/
    }

    else if (i4AddOrDelete == ISS_MIRR_ADD)
    {
        MirrWebMemRelease (i4OctetAllocCnt, pSrcId, pDescId, pCxtId, pVlanId);
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Mandatory parameters needed to activate");
        if (STRCMP (pHttp->au1Value, "Add") == 0)
        {
            nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, DESTROY);
        }
        else
        {
            nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, ACTIVE);
        }
        return;
    }

    free_octetstring (pSrcId);
    i4OctetAllocCnt--;
    if (pDescId->i4_Length != 0)
    {
        pc1DescTok = STRTOK (pDescId->pu1_OctetList, ",");

        while (pc1DescTok != NULL)
        {
            /* Destination entity value gigabitethernet or fastethernet 
             * ex: gi0/1 or fa0/1 */
            if ((STRSTR (pc1DescTok, "gi") != NULL)
                || (STRSTR (pc1DescTok, "Gi") != NULL))
            {
                STRCPY (ai1IfName, "gi");
                CfaCliGetIfIndex (ai1IfName, (INT1 *) (&pc1DescTok[2]),
                                  &u4DescId);
            }
            else if ((STRSTR (pc1DescTok, "fa") != NULL)
                     || (STRSTR (pc1DescTok, "Fa") != NULL))
            {
                STRCPY (ai1IfName, "fa");
                CfaCliGetIfIndex (ai1IfName, (INT1 *) (&pc1DescTok[2]),
                                  &u4DescId);
            }
            else if ((STRSTR (pc1DescTok, "ex") != NULL)
                     || (STRSTR (pc1DescTok, "Ex") != NULL))
            {
                STRCPY (ai1IfName, "ex");
                CfaCliGetIfIndex (ai1IfName, (INT1 *) (&pc1DescTok[2]),
                                  &u4DescId);
            }
            else
            {
                MirrWebMemRelease (i4OctetAllocCnt, pDescId, pCxtId, pVlanId);
                ISS_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to configure Destination Entity. Enter valid ports Ex:gi0/1,fa0/1");
                if (STRCMP (pHttp->au1Value, "Add") == 0)
                {
                    nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, DESTROY);
                }
                else
                {
                    nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, ACTIVE);
                }
                return;
            }

            /* Destination Entity */
            if (u4DescId != 0)
            {
                if (nmhTestv2IssMirrorCtrlExtnDestCfg (&u4ErrValue,
                                                       i4SessionIndex,
                                                       (INT4) u4DescId,
                                                       i4AddOrDelete) ==
                    SNMP_FAILURE)
                {
                    ISS_UNLOCK ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *)
                                  "Unable to configure Destination Entity");
                    if (STRCMP (pHttp->au1Value, "Add") == 0)
                    {
                        nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, DESTROY);
                    }
                    else
                    {
                        nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, ACTIVE);
                    }
                     free_octetstring (pVlanId);
                      free_octetstring (pCxtId);
                      free_octetstring (pDescId);
                    return;
                }

                if (nmhSetIssMirrorCtrlExtnDestCfg (i4SessionIndex,
                                                    (INT4) u4DescId,
                                                    i4AddOrDelete)
                    == SNMP_FAILURE)
                {
                    MirrWebMemRelease (i4OctetAllocCnt, pDescId,
                                       pCxtId, pVlanId);
                    ISS_UNLOCK ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *) "Unable to set "
                                  "Destination Entity");
                    if (STRCMP (pHttp->au1Value, "Add") == 0)
                    {
                        nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, DESTROY);
                    }
                    else
                    {
                        nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, ACTIVE);
                    }
                    return;
                }
            }
            else
            {
                ISS_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to configure Destination Entity. Enter valid ports Ex:gi0/1,fa0/1");
                if (STRCMP (pHttp->au1Value, "Add") == 0)
                {
                    nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, DESTROY);
                }
                else
                {
                    nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, ACTIVE);
                }
                     free_octetstring (pVlanId);
                      free_octetstring (pCxtId);
                      free_octetstring (pDescId);
                return;
            }

            u4DescId = 0;
            pc1DescTok = STRTOK (NULL, ",");
        }
    }
    else if (i4AddOrDelete == ISS_MIRR_ADD)
    {
        MirrWebMemRelease (i4OctetAllocCnt, pDescId, pCxtId, pVlanId);
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp,
                      (CONST INT1 *) "Mandatory parameters needed to activate");
        if (STRCMP (pHttp->au1Value, "Add") == 0)
        {
            nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, DESTROY);
        }
        else
        {
            nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, ACTIVE);
        }
        return;
    }
    free_octetstring (pDescId);
    i4OctetAllocCnt--;
    /* Vlan Based */
    if (i4MirrType == ISS_MIRR_VLAN_BASED)
    {
        if (pCxtId->i4_Length != pVlanId->i4_Length)
        {
            MirrWebMemRelease (i4OctetAllocCnt, pCxtId, pVlanId);
            ISS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Provide Correct vlan and context Id");
            return;
        }
        pc1CxtTok = STRTOK_R (pCxtId->pu1_OctetList, ",", &pc1SavCxt);
        pc1VlnTok = STRTOK_R (pVlanId->pu1_OctetList, ",", &pc1SavVln);

        while ((pc1VlnTok != NULL) && (pc1CxtTok != NULL))
        {
            i4ContextId = ATOI (pc1CxtTok);
            i4VlanId = ATOI (pc1VlnTok);
            /* VLAN CFG */
            if (nmhTestv2IssMirrorCtrlExtnSrcVlanCfg (&u4ErrValue,
                                                      i4SessionIndex,
                                                      i4ContextId, i4VlanId,
                                                      i4AddOrDelete)
                == SNMP_FAILURE)
            {
                MirrWebMemRelease (i4OctetAllocCnt, pCxtId, pVlanId);
                ISS_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to configure Vlan Configurations");
                return;
            }

            if (nmhSetIssMirrorCtrlExtnSrcVlanCfg (i4SessionIndex,
                                                   i4ContextId,
                                                   i4VlanId,
                                                   i4AddOrDelete)
                == SNMP_FAILURE)
            {
                MirrWebMemRelease (i4OctetAllocCnt, pCxtId, pVlanId);
                ISS_UNLOCK ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp, (CONST INT1 *) "Unable to set "
                              " Vlan Configurations");
                free_octetstring (pVlanId);
                free_octetstring (pCxtId);
                return;
            }

            if (i4AddOrDelete == ISS_MIRR_ADD)
            {
                /* VLAN Mode */
                if (nmhTestv2IssMirrorCtrlExtnSrcVlanMode (&u4ErrValue,
                                                           i4SessionIndex,
                                                           i4ContextId,
                                                           i4VlanId,
                                                           i4ModeType)
                    == SNMP_FAILURE)
                {
                    MirrWebMemRelease (i4OctetAllocCnt, pCxtId, pVlanId);
                    ISS_UNLOCK ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *)
                                  "Unable to configure Vlan Mode");
                    return;
                }

                if (nmhSetIssMirrorCtrlExtnSrcVlanMode (i4SessionIndex,
                                                        i4ContextId, i4VlanId,
                                                        i4ModeType)
                    == SNMP_FAILURE)
                {
                    MirrWebMemRelease (i4OctetAllocCnt, pCxtId, pVlanId);
                    ISS_UNLOCK ();
                    WebnmUnRegisterLock (pHttp);
                    IssSendError (pHttp, (CONST INT1 *) "Unable to set "
                                  " Vlan Mode");
                    return;
                }
            }
            pc1CxtTok = STRTOK_R (NULL, ",", &pc1SavCxt);
            pc1VlnTok = STRTOK_R (NULL, ",", &pc1SavVln);
        }
    }
    MirrWebMemRelease (i4OctetAllocCnt, pCxtId, pVlanId);
    i4OctetAllocCnt = i4OctetAllocCnt - 2;
    /* RSpan Status */
    if (nmhTestv2IssMirrorCtrlExtnRSpanStatus (&u4ErrValue, i4SessionIndex,
                                               i4RSpanStatus) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to configure Rspan Status");
        return;
    }

    if (nmhSetIssMirrorCtrlExtnRSpanStatus (i4SessionIndex,
                                            i4RSpanStatus) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set Mirror Type");
        return;
    }

    /* RSpan VlanId */
    if (i4RSpanVlanId != 0)
    {
        if (nmhTestv2IssMirrorCtrlExtnRSpanVlanId (&u4ErrValue, i4SessionIndex,
                                                   i4RSpanVlanId)
            == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure Rspan Vlan Id");
            return;
        }

        if (nmhSetIssMirrorCtrlExtnRSpanVlanId (i4SessionIndex,
                                                i4RSpanVlanId) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set Vlan Id");
            return;
        }
    }

    /* RSpan Context */
    if (nmhTestv2IssMirrorCtrlExtnRSpanContext (&u4ErrValue,
                                                i4SessionIndex,
                                                i4RSpanContext) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to configure Rspan Context");
        return;
    }

    if (nmhSetIssMirrorCtrlExtnRSpanContext (i4SessionIndex,
                                             i4RSpanContext) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set Rspan context");
        return;
    }

    /* Row Status */
    if (nmhTestv2IssMirrorCtrlExtnStatus (&u4ErrValue, i4SessionIndex,
                                          ACTIVE) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Mandatory parameters"
                      " needed to activate");
        free_octetstring (pVlanId);
        free_octetstring (pCxtId);
        return;
    }
    if (nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, ACTIVE) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Entry creation failed");
        return;
    }

    ISS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    IssProcessMirrCtrlExtPageGet (pHttp);
}

/*******************************************************************************
 * Function Name : MirrWebMemRelease
 * Description   : This function releases the memory allocated for Octet Strings 
 * Input(s)      : u4RelCount - No of Release Count
 *                 ...        - Variable command argument list
 * Output(s)     : None.
 * Return Values : None
 ********************************************************************************/
VOID
MirrWebMemRelease (INT4 u4RelCount, ...)
{
    va_list             ReleaseList;
    INT4                u4Index = 0;
    tSNMP_OCTET_STRING_TYPE *pRelease = NULL;

    va_start (ReleaseList, u4RelCount);
    for (u4Index = 0; u4Index < u4RelCount; u4Index++)
    {
        pRelease = va_arg (ReleaseList, tSNMP_OCTET_STRING_TYPE *);
        free_octetstring (pRelease);
    }
    va_end (ReleaseList);
    return;
}

#endif /* ISS_WANTED */

#endif /* WEBNM_WANTED */
#endif
