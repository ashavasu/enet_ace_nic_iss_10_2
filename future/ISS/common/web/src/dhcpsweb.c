/***************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: dhcpsweb.c,v 1.6 2014/07/18 12:33:01 siva Exp $ 
 *
 *
 * Description : This file Contains all the WEBUI related functions of 
 *               DHCP server.
 *******************************************************************/
#ifndef __DHCPSWEB_C__
#define __DHCPSWEB_C__

#include "webiss.h"
#include "fswebnm.h"

/*********************************************************************
 *  Function Name : IssProcessDhcpPoolOptConfPage
 *  Description   : This function processes the request coming for the
 *                  DHCP Configuration page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessDhcpPoolOptConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessDhcpPoolOptConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessDhcpPoolOptConfPageSet (pHttp);
    }
}

/*********************************************************************
 *  Function Name : IssProcessDhcpPoolOptConfPageGet
 *  Description   : This function processes the request coming for the
 *                  DHCP Configuration page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessDhcpPoolOptConfPageGet (tHttp * pHttp)
{
    INT4                i4OutCome = 0, i4CurrPoolId = 0;
    INT4                i4CurrOpt = 0, i4NextOpt = 0;
    INT4                i4NextPoolId = 0;
    UINT4               u4Temp = 0;
    UINT4               u4Length = 0;
    UINT1               au1OptValue[ISS_DHCP_MAX_OPT_LEN + 1];
    UINT1               au1OptionStr[ISS_DHCP_MAX_OPT_LEN + 1];
    UINT1               au1PoolName[ISS_STRING_LENGTH + 1];
    tSNMP_OCTET_STRING_TYPE OptionValue, PoolName;

    OptionValue.pu1_OctetList = au1OptValue;
    PoolName.pu1_OctetList = au1PoolName;
    pHttp->i4Write = 0;
    IssPrintDhcpPools (pHttp);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    i4OutCome =
        nmhGetFirstIndexDhcpSrvSubnetOptTable (&i4NextPoolId, &i4NextOpt);

    if (i4OutCome == SNMP_FAILURE)
    {
        return;
    }
    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        MEMSET (PoolName.pu1_OctetList, 0, sizeof (au1PoolName));
        MEMSET (OptionValue.pu1_OctetList, 0, ISS_DHCP_MAX_OPT_LEN + 1);

        nmhGetDhcpSrvSubnetPoolName (i4NextPoolId, &PoolName);
        nmhGetDhcpSrvSubnetOptLen (i4NextPoolId, i4NextOpt, (INT4 *) &u4Length);
        nmhGetDhcpSrvSubnetOptVal (i4NextPoolId, i4NextOpt, &OptionValue);

        MEMSET (au1OptionStr, 0, ISS_DHCP_MAX_OPT_LEN + 1);
        IssProcessDhcpSrvOption (pHttp, (UINT1) i4NextOpt,
                                 &u4Length,
                                 OptionValue.pu1_OctetList,
                                 FALSE, au1OptionStr);

        SPRINTF ((CHR1 *) pHttp->au1KeyString,
                 "1.3.6.1.4.1.2076.84.1.9.1.3_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", PoolName.pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        SPRINTF ((CHR1 *) pHttp->au1KeyString,
                 "1.3.6.1.4.1.2076.84.1.12.1.1_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextOpt);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString, "OPT_TYPE_DUMMY_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextOpt);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        SPRINTF ((CHR1 *) pHttp->au1KeyString,
                 "1.3.6.1.4.1.2076.84.1.12.1.3_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1OptionStr);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "POOL_DUMMY_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextPoolId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

        i4CurrOpt = i4NextOpt;
        i4CurrPoolId = i4NextPoolId;
    }
    while (nmhGetNextIndexDhcpSrvSubnetOptTable (i4CurrPoolId, &i4NextPoolId,
                                                 i4CurrOpt, &i4NextOpt)
           == SNMP_SUCCESS);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
 *  Function Name : IssProcessDhcpPoolOptConfPageSet
 *  Description   : This function processes the request coming for the
 *                  DHCP Configuration page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessDhcpPoolOptConfPageSet (tHttp * pHttp)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4OptionType = 0;
    UINT4               u4PoolId = 0;
    UINT4               u4Length = 0;
    UINT1               u1IsCreated = FALSE;
    UINT1               au1OptValue[ISS_DHCP_MAX_OPT_LEN + 1];
    UINT1               au1OptionStr[ISS_DHCP_MAX_OPT_LEN + 1];
    tSNMP_OCTET_STRING_TYPE OptionValue;

    OptionValue.pu1_OctetList = au1OptValue;

    MEMSET (au1OptValue, 0, ISS_DHCP_MAX_OPT_LEN + 1);
    MEMSET (au1OptionStr, 0, ISS_DHCP_MAX_OPT_LEN + 1);

    SPRINTF ((CHR1 *) pHttp->au1Name, "1.3.6.1.4.1.2076.84.1.9.1.1");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4PoolId = (UINT4) ATOI (pHttp->au1Value);

    SPRINTF ((CHR1 *) pHttp->au1Name, "1.3.6.1.4.1.2076.84.1.12.1.1");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);
    u4OptionType = (UINT4) ATOI (pHttp->au1Value);

    SPRINTF ((CHR1 *) pHttp->au1Name, "1.3.6.1.4.1.2076.84.1.12.1.3");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);

    u4Length = (STRLEN (pHttp->au1Value) < ISS_DHCP_MAX_OPT_LEN) ?
        STRLEN (pHttp->au1Value) : ISS_DHCP_MAX_OPT_LEN;
    MEMCPY (&au1OptionStr[0], pHttp->au1Value, u4Length);

    if (IssProcessDhcpSrvOption (pHttp, (UINT1) u4OptionType,
                                 &u4Length,
                                 &au1OptionStr[0], TRUE,
                                 OptionValue.pu1_OctetList) != OSIX_SUCCESS)
    {
        return;
    }
    OptionValue.i4_Length = (INT4) u4Length;

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {
        if (u4OptionType == 51)
        {
            IssSendError (pHttp, (INT1 *) "Cannot delete the entry");
            return;
        }

        if (nmhTestv2DhcpSrvSubnetOptRowStatus (&u4ErrorCode, (INT4) u4PoolId,
                                                (INT4) u4OptionType, DESTROY)
            != SNMP_SUCCESS)
        {
            IssSendError (pHttp, (INT1 *) "Cannot delete the entry");
            return;
        }
        if (nmhSetDhcpSrvSubnetOptRowStatus
            ((INT4) u4PoolId, (INT4) u4OptionType, DESTROY) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (INT1 *) "Cannot delete the entry");
            return;
        }
        IssProcessDhcpPoolOptConfPageGet (pHttp);
        return;
    }
    else if (STRCMP (pHttp->au1Value, "Add") == 0)
    {
        u1IsCreated = TRUE;
        if (nmhTestv2DhcpSrvSubnetOptRowStatus (&u4ErrorCode, (INT4) u4PoolId,
                                                (INT4) u4OptionType,
                                                CREATE_AND_WAIT) !=
            SNMP_SUCCESS)
        {
            IssSendError (pHttp, (INT1 *) "Cannot add the entry");
            return;
        }
        if (nmhSetDhcpSrvSubnetOptRowStatus
            ((INT4) u4PoolId, (INT4) u4OptionType,
             CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (INT1 *) "Cannot add the entry");
            return;
        }
    }
    else if (STRCMP (pHttp->au1Value, "Apply") == 0)
    {
        if (nmhTestv2DhcpSrvSubnetOptRowStatus (&u4ErrorCode, (INT4) u4PoolId,
                                                (INT4) u4OptionType,
                                                NOT_IN_SERVICE) != SNMP_SUCCESS)
        {
            IssSendError (pHttp, (INT1 *) "Cannot modify the entry");
            return;
        }
        if (nmhSetDhcpSrvSubnetOptRowStatus
            ((INT4) u4PoolId, (INT4) u4OptionType,
             NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (INT1 *) "Cannot modify the entry");
            return;
        }
    }

    if (nmhTestv2DhcpSrvSubnetOptLen
        (&u4ErrorCode, (INT4) u4PoolId, (INT4) u4OptionType,
         (INT4) u4Length) != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (INT1 *) "Cannot set the Option Length");
        return;
    }
    if (nmhTestv2DhcpSrvSubnetOptVal
        (&u4ErrorCode, (INT4) u4PoolId, (INT4) u4OptionType,
         &OptionValue) != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (INT1 *) "Cannot set the Option Value");
        return;
    }
    if (nmhSetDhcpSrvSubnetOptLen
        ((INT4) u4PoolId, (INT4) u4OptionType, (INT4) u4Length) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (INT1 *) "Cannot set the Option Length");
        return;
    }
    if (nmhSetDhcpSrvSubnetOptVal
        ((INT4) u4PoolId, (INT4) u4OptionType, &OptionValue) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (INT1 *) "Cannot set the Option Length");
        return;
    }
    if (nmhTestv2DhcpSrvSubnetOptRowStatus (&u4ErrorCode, (INT4) u4PoolId,
                                            (INT4) u4OptionType, ACTIVE)
        != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (INT1 *) "Cannot enable the entry");
        if (u1IsCreated == TRUE)
        {
            nmhSetDhcpSrvSubnetOptRowStatus ((INT4) u4PoolId,
                                             (INT4) u4OptionType, DESTROY);
        }
        return;
    }
    if (nmhSetDhcpSrvSubnetOptRowStatus
        ((INT4) u4PoolId, (INT4) u4OptionType, ACTIVE) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (INT1 *) "Cannot enable the entry");
        if (u1IsCreated == TRUE)
        {
            nmhSetDhcpSrvSubnetOptRowStatus ((INT4) u4PoolId,
                                             (INT4) u4OptionType, DESTROY);
        }
        return;
    }
    IssProcessDhcpPoolOptConfPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssPrintDhcpPools
*  Description   : This function prints all DHCP Ip Pools
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssPrintDhcpPools (tHttp * pHttp)
{
    INT4                i4OutCome = 0, i4NextPoolId = 0, i4CurrPoolId = 0;
    INT4                i4RetVal;
    UINT1               au1OptValue[ISS_STRING_LENGTH + 1];
    tSNMP_OCTET_STRING_TYPE OptionValue;

    OptionValue.pu1_OctetList = &au1OptValue[0];

    i4OutCome = nmhGetFirstIndexDhcpSrvSubnetPoolConfigTable (&i4NextPoolId);

    if (i4OutCome == SNMP_FAILURE)
    {
        return;
    }

    STRCPY (pHttp->au1KeyString, "<! DHCP_POOL>");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    do
    {
        nmhGetDhcpSrvSubnetPoolRowStatus (i4NextPoolId, &i4RetVal);
        if (i4RetVal == ACTIVE)
        {
            MEMSET (OptionValue.pu1_OctetList, 0, sizeof (au1OptValue));
            nmhGetDhcpSrvSubnetPoolName (i4NextPoolId, &OptionValue);
            SPRINTF ((CHR1 *) pHttp->au1DataString,
                     "<option value = \"%d\" selected>%s \n", i4NextPoolId,
                     OptionValue.pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
        }
        i4CurrPoolId = i4NextPoolId;
    }
    while (nmhGetNextIndexDhcpSrvSubnetPoolConfigTable (i4CurrPoolId,
                                                        &i4NextPoolId) ==
           SNMP_SUCCESS);
}

/******************************************************************************/
/* Function Name     : IssProcessDhcpSrvOption                           */
/*                                                                            */
/* Description       : This function validate the options depending           */
/*                     upon the length and type as specified in RFC 2132      */
/*                                                                            */
/* Input Parameters  : pHttp -  HTTP Pointer to print error                   */
/*                     u1Type - type of the option                            */
/*                     pu4Len - Length of the option                          */
/*                     pu1InStr - Option Value                                */
/*                     u1ConfFlag - Flag to indicate whether it's for display */
/*                     pRetStr  - Formatted Option Value                      */
/*                                                                            */
/* Output Parameters : Returns the length of the option in pu4Len for         */
/*                     specific option types.                                 */
/*                                                                            */
/* Return Value      : OSIX_SUCCESS/OSIX_FAILURE                              */
/******************************************************************************/

INT4
IssProcessDhcpSrvOption (tHttp * pHttp, UINT1 u1Type, UINT4 *pu4Len,
                         UINT1 *pu1InStr, UINT1 u1ConfFlag, UINT1 *pRetStr)
{
    UINT1              *pu1TempPtr;
    CHR1               *pu1TempIp = NULL;
    UINT4               u4IpAddr = 0;
    UINT4               u4OptVal = 0;
    INT4                i4OptVal = 0;
    tUtlInAddr          IpAddr;
    UINT1               u1TempLen = 0;
    /* NEW MULTI IP HANDLING VARIABLES */
    UINT1               au1OutData[DHCP_MAX_OPT_LEN];
    UINT1               au1TempIp[DHCP_MAX_STR_IP_LEN];
    UINT1               u1CurIndex;
    UINT1               u1OutBufIndex;
    UINT1               u1LenIndex;
    INT1                u1LenCurstr;
    UINT4               u4InStrLen = *pu4Len;
    pu1TempPtr = pRetStr;

    MEMSET (au1OutData, 0, DHCP_MAX_OPT_LEN);
    MEMSET (au1TempIp, 0, DHCP_MAX_STR_IP_LEN);

    switch (u1Type)
    {
            /* Options With Specific Values */
        case 19:
        case 20:
        case 27:
        case 29:
        case 30:
        case 31:
        case 34:
        case 36:
        case 39:
            if (*pu4Len != 1)
            {
                IssSendError (pHttp,
                              (INT1 *)
                              "Configurable Option's Length must be 1");

                return OSIX_FAILURE;
            }

            if (u1ConfFlag)
            {
                pu1InStr[0] = (UINT1) ATOI (pu1InStr);

                if ((pu1InStr[0] == 0) || (pu1InStr[0] == 1))
                {
                    MEMCPY (pRetStr, &pu1InStr[0], 1);
                    *(pRetStr + 1) = '\0';
                    return (OSIX_SUCCESS);
                }
                return (OSIX_FAILURE);
            }
            else
            {
                SPRINTF ((CHR1 *) pu1TempPtr, "%d\r\n", pu1InStr[0]);
                return (OSIX_SUCCESS);
            }
            break;
        case 51:

            if (u1ConfFlag != 0)
            {
                u4OptVal = (UINT4) ATOI (pu1InStr);
                if (u4OptVal > DHCP_INFINITE_LEASE)
                {
                    u4OptVal = DHCP_INFINITE_LEASE;
                }
                MEMCPY (pu1TempPtr, &u4OptVal, sizeof (UINT4));
            }
            else
            {
                MEMCPY (&u4OptVal, pu1InStr, sizeof (UINT4));
                SPRINTF ((CHR1 *) pu1TempPtr, "%d \r\n", u4OptVal);
            }
            *pu4Len = sizeof (UINT4);
            return (OSIX_SUCCESS);

        case 46:

            if (*pu4Len != 1)
            {
                IssSendError (pHttp,
                              (INT1 *)
                              " Configurable Option's Length must be 1 ");
                return (OSIX_FAILURE);
            }

            if (u1ConfFlag)
            {
                pu1InStr[0] = (UINT1) ATOI (pu1InStr);

                MEMCPY (pRetStr, &pu1InStr[0], 1);
                *(pRetStr + 1) = '\0';
            }
            else
            {
                SPRINTF ((CHR1 *) pu1TempPtr, "%d\r\n", pu1InStr[0]);
            }
            return (OSIX_SUCCESS);

        case 12:
        case 14:
        case 15:
        case 17:
        case 18:
        case 40:
        case 47:
        case 64:
        case 66:
        case 67:
        case 240:
        case 43:
            if (*pu4Len < 1)
            {
                IssSendError (pHttp, (INT1 *) " Option's Length must be >= 1");
                return (OSIX_FAILURE);
            }
            if (u1Type == 66)
            {
                /* If the option is tftp server address, then the value is
                 * stored as IP address string in dotted decimal format */
                *pu4Len = STRLEN (pu1InStr);
            }

            if (u1ConfFlag)
            {
                MEMCPY (pRetStr, pu1InStr, *pu4Len);
            }
            else
            {
                SPRINTF ((CHR1 *) pu1TempPtr, "%s\r\n", pu1InStr);
            }
            return (OSIX_SUCCESS);

            /* Options With Minimum Length 4 */
        case 16:
            if (u1ConfFlag == TRUE)
            {
                if (!(WEB_INET_ATON (pu1InStr, &IpAddr)))
                {
                    IssSendError (pHttp,
                                  (INT1 *)
                                  "Invalid option value. Value should be an IP address");
                    return (OSIX_FAILURE);
                }
                u4IpAddr = IpAddr.u4Addr;
                WEB_CONVERT_VAL_TO_STR (pu1TempPtr, u4IpAddr);

                /* return the length of the option in the pointer
                 * as it should be the length of the option value and 
                 * not the length of the given string
                 */
                *pu4Len = sizeof (UINT4);
            }
            else
            {
                MEMCPY (&u4IpAddr, pu1InStr, sizeof (UINT4));

                IpAddr.u4Addr = u4IpAddr;
                pu1TempIp = UtlInetNtoa (IpAddr);

                SPRINTF ((CHR1 *) pu1TempPtr, "%s\r\n", pu1TempIp);
            }
            return (OSIX_SUCCESS);
            /* Options With Length 2 and (2*n) */

        case 25:

            if ((*pu4Len != 0) && (*pu4Len % 2 == 0))
            {
                if (u1ConfFlag == TRUE)
                {
                    MEMCPY (pu1TempPtr, pu1InStr, *pu4Len);
                }
                else
                {
                    SPRINTF ((CHR1 *) pu1TempPtr, "%s\r\n", pu1InStr);

                }
                return (OSIX_SUCCESS);
            }
            else
            {
                IssSendError (pHttp,
                              (INT1 *) "Option's Length must be Multiple of 2");
                return (OSIX_FAILURE);
            }

            /* Options With Minimum Length 0 and (4*n) */

        case 68:
            if (u1ConfFlag == TRUE)
            {
                if (!(WEB_INET_ATON (pu1InStr, &IpAddr)))
                {
                    IssSendError (pHttp,
                                  (INT1 *)
                                  " Invalid option value. Value should be an IP address");
                    return (OSIX_FAILURE);
                }
                u4IpAddr = IpAddr.u4Addr;

                WEB_CONVERT_VAL_TO_STR (pu1TempPtr, u4IpAddr);

                /* return the length of the option in the pointer
                 * as it should be the length of the option value and 
                 * not the length of the given string
                 */
                *pu4Len = sizeof (UINT4);
            }
            else
            {
                MEMCPY (&u4IpAddr, pu1InStr, sizeof (UINT4));
                IpAddr.u4Addr = u4IpAddr;
                pu1TempIp = UtlInetNtoa (IpAddr);

                SPRINTF ((CHR1 *) pu1TempPtr, "%s\r\n", pu1TempIp);
            }
            return (OSIX_SUCCESS);
            /* With Minimum Length 4 and (4*n) */

        case 1:
        case 3:
        case 4:
        case 5:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 41:
        case 44:
        case 45:
        case 48:
        case 49:
        case 65:
        case 69:
        case 70:
        case 71:
        case 72:
        case 73:
        case 74:
        case 75:
        case 76:
            /* Vendor specific options */
        case 151:
            if (u1ConfFlag == TRUE)
            {
                if (!(WEB_INET_ATON (pu1InStr, &IpAddr)))
                {
                    IssSendError (pHttp,
                                  (INT1 *)
                                  "Invalid option value.Value should be an IP address");
                    return (OSIX_FAILURE);
                }
                u4IpAddr = IpAddr.u4Addr;

                WEB_CONVERT_VAL_TO_STR (pu1TempPtr, u4IpAddr);

                /* return the length of the option in the pointer
                 * as it should be the length of the option value and 
                 * not the length of the given string
                 */
                *pu4Len = sizeof (UINT4);
            }
            else
            {
                MEMCPY (&u4IpAddr, pu1InStr, sizeof (UINT4));

                IpAddr.u4Addr = u4IpAddr;
                pu1TempIp = UtlInetNtoa (IpAddr);

                SPRINTF ((CHR1 *) pu1TempPtr, "%s\r\n", pu1TempIp);
            }
            return (OSIX_SUCCESS);

            /* With Minimum Length 8 and (8*n) */
        case 21:
        case 33:
            if ((*pu4Len != 0) && (*pu4Len % 8 == 0))
            {
                if (u1ConfFlag == TRUE)
                {
                    MEMCPY (pu1TempPtr, pu1InStr, *pu4Len);
                }
                else
                {
                    SPRINTF ((CHR1 *) pu1TempPtr, "%s\r\n", pu1InStr);

                }
                return (OSIX_SUCCESS);
            }
            else
            {
                IssSendError (pHttp,
                              (INT1 *)
                              " Option's Length must be Multiple of 8\r\n");
                return (OSIX_FAILURE);
            }
            /*Vendor Specific options */
        case 2:
            if (CLI_INET_ATON (pu1InStr, &IpAddr))
            {
                IssSendError (pHttp,
                              (INT1 *)
                              "Invalid option value. Value shouldn't be an IP address");
                return (OSIX_FAILURE);
            }
            if (u1ConfFlag == TRUE)
            {
                MEMSET (pu1TempPtr, 0, ISS_DHCP_MAX_OPT_LEN + 1);

                if ((i4OptVal = CliHexStrToDecimal (pu1InStr)) == HEX_FAILURE)
                {
                    *pu4Len = sizeof (UINT4);
                    u4OptVal = (UINT4) ATOI (pu1InStr);
                    u4OptVal = OSIX_HTONL (u4OptVal);
                    MEMCPY (pu1TempPtr, (UINT1 *) &u4OptVal, *pu4Len);
                }
                else
                {
                    *pu4Len = sizeof (UINT4);
                    i4OptVal = (INT4) OSIX_HTONL ((UINT4) i4OptVal);
                    MEMCPY (pu1TempPtr, &i4OptVal, *pu4Len);
                }
            }
            else
            {
                MEMCPY (&u4OptVal, pu1InStr, sizeof (UINT4));
                u4OptVal = OSIX_NTOHL (u4OptVal);
                SPRINTF ((CHR1 *) pu1TempPtr, "%d\r\n", u4OptVal);
            }
            return (CLI_SUCCESS);

        case 128:
        case 191:
            if (u1ConfFlag)
            {
                MEMCPY (pRetStr, pu1InStr, *pu4Len);
            }
            else
            {
                SPRINTF ((CHR1 *) pu1TempPtr, "%s\r\n", pu1InStr);
            }
            return (OSIX_SUCCESS);

            /* Should Not Allow Configuration */
        case 6:
        case 42:
            /* DNS SERVER HANDLED HERE */
            /* SNTP ALSO HANDLED HERE  */
            if (u1ConfFlag == TRUE)
            {                    /* CONFIGURE */
                /* Handle any ',' */
                for (u1CurIndex = 0; u1CurIndex < u4InStrLen; u1CurIndex++)
                {
                    if (pu1InStr[u1CurIndex] == ',')
                    {            /* if ',' is encountered replace with '\0' */
                        pu1InStr[u1CurIndex] = '\0';
                    }
                }
                *pu4Len = 0;
                for (u1CurIndex = 0; u1CurIndex < u4InStrLen;
                     u1CurIndex =
                     (UINT1) (u1CurIndex + (STRLEN (au1TempIp) + 1)))
                {                /* check each IP to see if it is an IP */
                    MEMSET (&IpAddr, 0, sizeof (tUtlInAddr));
                    MEMSET (au1TempIp, 0, DHCP_MAX_STR_IP_LEN);
                    MEMCPY (au1TempIp, &pu1InStr[u1CurIndex],
                            STRLEN (&pu1InStr[u1CurIndex]));
                    if (!(CLI_INET_ATON (au1TempIp, &IpAddr)))
                    {
                        IssSendError (pHttp,
                                      (INT1 *)
                                      "Invalid option value. Value should be an IP address");
                        return (OSIX_FAILURE);
                    }
                    u4IpAddr = IpAddr.u4Addr;
                    CLI_CONVERT_VAL_TO_STR (&au1OutData[(*pu4Len)], u4IpAddr);
                    *pu4Len += sizeof (UINT4);
                }
                /* COPY OUTPUT TO RETURN STRING */
                MEMCPY (pu1TempPtr, au1OutData, *pu4Len);
            }
            else
            {                    /* PRINT */
                /* pu1TempPtr is the printed value */
                u1OutBufIndex = 0;
                MEMSET (&IpAddr, 0, sizeof (tUtlInAddr));
                for (u1CurIndex = 0; u1CurIndex < STRLEN (pu1InStr);
                     u1CurIndex = (UINT1) (u1CurIndex + sizeof (UINT4)))
                {
                    if (u1OutBufIndex >= DHCP_MAX_ALLOWED_LEN)
                    {
                        /* Exceeded output buffer allowed size */
                        break;
                    }
                    if (u1OutBufIndex != 0)
                    {
                        au1OutData[u1OutBufIndex] = ',';
                        u1OutBufIndex++;
                    }
                    MEMCPY (&u4IpAddr, &pu1InStr[u1CurIndex], sizeof (UINT4));
                    IpAddr.u4Addr = u4IpAddr;
                    pu1TempIp = UtlInetNtoa (IpAddr);
                    MEMCPY (&au1OutData[u1OutBufIndex], pu1TempIp,
                            STRLEN (pu1TempIp));
                    u1OutBufIndex =
                        (UINT1) (u1OutBufIndex + STRLEN (pu1TempIp));
                }
                SPRINTF ((CHR1 *) pu1TempPtr, "%s\r\n", au1OutData);
            }
            return (CLI_SUCCESS);
        case 120:
            if (u1ConfFlag == TRUE)
            {
                /* Handle any ',' */
                for (u1CurIndex = 0; u1CurIndex < u4InStrLen; u1CurIndex++)
                {
                    if (pu1InStr[u1CurIndex] == ',')
                    {            /* if ',' is encountered replace with '\0' */
                        pu1InStr[u1CurIndex] = '\0';
                    }
                }
                if (pu1InStr[0] == '1')
                {
                    /* SET FIRST BIT AS 1 because it is IP */
                    au1OutData[0] = 1;
                    *pu4Len = 1;    /* UPDATE LENGTH ALSO */
                    for (u1CurIndex = 1; u1CurIndex < u4InStrLen;
                         u1CurIndex = (UINT1) (u1CurIndex + (u1TempLen + 1)))
                    {            /* check each IP to see if it is an IP */
                        MEMSET (au1TempIp, 0, DHCP_MAX_STR_IP_LEN);
                        MEMCPY (au1TempIp, &pu1InStr[u1CurIndex],
                                STRLEN (&pu1InStr[u1CurIndex]));
                        u1TempLen = (UINT1) (STRLEN (&pu1InStr[u1CurIndex]));
                        if (!(CLI_INET_ATON (au1TempIp, &IpAddr)))
                        {
                            IssSendError (pHttp,
                                          (INT1 *)
                                          "Invalid option value. Value should be an IP address");
                            return (OSIX_FAILURE);
                        }
                        u4IpAddr = IpAddr.u4Addr;
                        CLI_CONVERT_VAL_TO_STR (&au1OutData[(*pu4Len)],
                                                u4IpAddr);
                        *pu4Len += sizeof (UINT4);
                    }
                    /* COPY OUTPUT TO RETURN STRING */
                    MEMCPY (pu1TempPtr, au1OutData, *pu4Len);
                }
                else if (pu1InStr[0] == '0')
                {
                    /* CONFIG DOMAIN NAME */
                    if (u4InStrLen <= 1)
                    {
                        IssSendError (pHttp,
                                      (INT1 *)
                                      " Configurable Option's Length must be 1 ");
                        return (OSIX_FAILURE);
                    }
                    /* if (u1ConfFlag) */
                    u1LenIndex = 0;
                    u1OutBufIndex = 1;
                    u1CurIndex = 1;
                    u1LenCurstr = 0;    /* reset len of cur str */

                    au1OutData[0] = 0;    /* Set enc bit for string */
                    /* appending length for first substring */
                    u1LenIndex = u1OutBufIndex;
                    /* au1OutData[u1LenIndex]=0; */
                    u1OutBufIndex++;

                    while (u1CurIndex < (u4InStrLen + 1))
                    {
                        /* process each char */
                        /* read from in_buf to au1OutData till reach \0 or . */
                        if (pu1InStr[u1CurIndex] == '.')
                        {
                            /* in this case we replace '.' so we set u1LenIndex to location OF '.' */
                            au1OutData[u1LenIndex] = (UINT1) (u1LenCurstr);
                            u1LenCurstr = -1;    /* reset len of cur str *//* start @ -1 becos it incremenets every iteration */
                            u1LenIndex = u1OutBufIndex;
                        }
                        if (pu1InStr[u1CurIndex] == 0)
                        {
                            /* in this case we dont replace '0' so we set u1LenIndex to location AFTER '0' */
                            au1OutData[u1LenIndex] = (UINT1) (u1LenCurstr);    /* UINT1 48 -- '0' */
                            u1LenCurstr = -1;    /* reset len of cur str */
                            u1LenIndex = (UINT1) (u1OutBufIndex + 1);
                            u1OutBufIndex++;
                        }
                        au1OutData[u1OutBufIndex] = pu1InStr[u1CurIndex];
                        u1CurIndex++;
                        u1OutBufIndex++;
                        u1LenCurstr++;    /* increment len of current string */
                    }
                    /* length of au1OutData is u1OutBufIndex */
                    *pu4Len = u1OutBufIndex;
                    MEMCPY (pRetStr, au1OutData, u1OutBufIndex);
                    return (CLI_SUCCESS);
                }
                else
                {
                    IssSendError (pHttp, (INT1 *) " Invalid Input ");
                    return (OSIX_FAILURE);
                }
            }
            else
            {
                if (pu1InStr[0] == 1)
                {
                    /* pu1TempPtr is the printed value */
                    u1OutBufIndex = 0;
                    for (u1CurIndex = 0; u1CurIndex < STRLEN (&pu1InStr[1]);
                         u1CurIndex = (UINT1) (u1CurIndex + sizeof (UINT4)))
                    {
                        if (u1OutBufIndex != 0)
                        {
                            au1OutData[u1OutBufIndex] = ',';
                            u1OutBufIndex++;
                        }
                        MEMCPY (&u4IpAddr, &pu1InStr[(u1CurIndex + 1)],
                                sizeof (UINT4));
                        IpAddr.u4Addr = u4IpAddr;
                        pu1TempIp = UtlInetNtoa (IpAddr);
                        MEMCPY (&au1OutData[u1OutBufIndex], pu1TempIp,
                                STRLEN (pu1TempIp));
                        u1OutBufIndex =
                            (UINT1) (u1OutBufIndex + STRLEN (pu1TempIp));
                    }
                    SPRINTF ((CHR1 *) pu1TempPtr, "%s\r\n", au1OutData);
                }
                else
                {
                    /* PRINT DOMAIN NAME */
                    /* modded code for multi input */
                    MEMSET (au1OutData, 0, DHCP_MAX_OPT_LEN);
                    u1OutBufIndex = 0;
                    u1CurIndex = 1;
                    u1LenCurstr = 0;
                    while (u1CurIndex < (*pu4Len))
                    {
                        u1LenCurstr = (INT1) (pu1InStr[u1CurIndex]);
                        u1CurIndex++;
                        MEMCPY (&au1OutData[u1OutBufIndex],
                                &pu1InStr[u1CurIndex], u1LenCurstr);
                        u1OutBufIndex = (UINT1) (u1OutBufIndex + u1LenCurstr);
                        u1CurIndex = (UINT1) (u1CurIndex + u1LenCurstr);
                        if (pu1InStr[u1CurIndex] == 0)
                        {
                            if (au1OutData[u1OutBufIndex - 1] == ',')
                            {    /* If no inputs were recieved last time exit loop and remove ',' */
                                au1OutData[u1OutBufIndex - 1] = 0;
                                break;
                            }
                            au1OutData[u1OutBufIndex] = ',';
                            u1OutBufIndex++;
                            u1CurIndex++;
                        }
                        else
                        {
                            au1OutData[u1OutBufIndex] = '.';
                            u1OutBufIndex++;
                        }
                    }
                    SPRINTF ((CHR1 *) pu1TempPtr, "%s\r\n", au1OutData);

                }
            }
            return (CLI_SUCCESS);
        case 0:
        case 255:
        case 50:
        case 52:
        case 53:
        case 54:
        case 55:
        case 56:
        case 57:
        case 58:
        case 59:
        case 60:

            IssSendError (pHttp, (INT1 *) "Not Configurable Option");

            return (OSIX_FAILURE);

        default:
            IssSendError (pHttp, (INT1 *) "Not Supported Option");

            return (OSIX_FAILURE);
    }
    /* *INDENT-ON* */
}

/*********************************************************************
 **  Function Name : IssProcessDhcpBootfileConfPage
 **  Description   : This function processes the request coming for the
 **                  DHCP Configuration page.
 **  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 **  Output(s)     : None.
 **  Return Values : None
 **********************************************************************/

VOID
IssProcessDhcpBootfileConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessDhcpBootfileConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessDhcpBootfileConfPageSet (pHttp);
    }
}

/*********************************************************************
 **  Function Name : IssProcessDhcpBootfileConfPageGet
 **  Description   : This function processes the request coming for the
 **                  DHCP Configuration page.
 **  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 **  Output(s)     : None.
 **  Return Values : None
 **********************************************************************/
VOID
IssProcessDhcpBootfileConfPageGet (tHttp * pHttp)
{
    UINT1               bootFileName[DHCP_MAX_NAME_LEN];
    UINT1               html_data[DHCPWEB_MAX_OPT_LEN];
    UINT1               temp_data[DHCP_MAX_NAME_LEN];
    UINT1               au1String[DHCP_MAX_NAME_LEN];
    tSNMP_OCTET_STRING_TYPE BootfileName;

    MEMSET (au1String, 0, DHCP_MAX_NAME_LEN);
    pHttp->i4Write = 0;
    BootfileName.pu1_OctetList = bootFileName;
    MEMSET (BootfileName.pu1_OctetList, 0, DHCP_MAX_NAME_LEN);
    MEMSET (html_data, 0, DHCPWEB_MAX_OPT_LEN);
    MEMSET (temp_data, 0, DHCP_MAX_NAME_LEN);
    nmhGetDhcpSrvDefBootFilename (&BootfileName);
    SPRINTF ((CHR1 *) pHttp->au1KeyString, "1.3.6.1.4.1.2076.84.1.6_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", BootfileName.pu1_OctetList);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
 **  Function Name : IssProcessDhcpBootfileConfPageSet
 ** Description    : This function processes the request coming for the
 **                  DHCP Configuration page.
 **  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 **  Output(s)     : None.
 **  Return Values : None
 **********************************************************************/
VOID
IssProcessDhcpBootfileConfPageSet (tHttp * pHttp)
{
    UINT1               au1OptValue[ISS_DHCP_MAX_OPT_LEN + 1];
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE OctetFilename;
    OctetFilename.pu1_OctetList = au1OptValue;
    MEMSET (au1OptValue, 0, ISS_DHCP_MAX_OPT_LEN + 1);
    SPRINTF ((CHR1 *) pHttp->au1Name, "1.3.6.1.4.1.2076.84.1.6");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);
    OctetFilename.i4_Length = (INT4) STRLEN (pHttp->au1Value);
    MEMCPY (OctetFilename.pu1_OctetList, pHttp->au1Value,
            MEM_MAX_BYTES (ISS_DHCP_MAX_OPT_LEN + 1, sizeof (pHttp->au1Value)));
    if (nmhTestv2DhcpSrvDefBootFilename (&u4ErrorCode, &OctetFilename) ==
        SNMP_SUCCESS)
    {
        if (nmhSetDhcpSrvDefBootFilename (&OctetFilename) != SNMP_SUCCESS)
        {
            IssSendError (pHttp, (INT1 *) "Unable to set Bootfile");
        }
    }
    else
    {
        IssSendError (pHttp, (INT1 *) "Unable to set Bootfile");
    }
    IssProcessDhcpBootfileConfPageGet (pHttp);
}

/*********************************************************************
 *  Function Name : IssProcessDhcpGlobOptConfPage 
 *  Description   : This function processes the request coming for the  
 *                  DHCP Configuration page. 
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessDhcpGlobOptConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessDhcpGlobOptConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessDhcpGlobOptConfPageSet (pHttp);
    }
}

/*********************************************************************
 *  Function Name : IssProcessDhcpGlobOptConfPageGet 
 *  Description   : This function processes the request coming for the  
 *                  DHCP Configuration page. 
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessDhcpGlobOptConfPageGet (tHttp * pHttp)
{
    INT4                i4OutCome, i4CurrOpt, i4NextOpt;
    UINT4               u4Temp = 0;
    UINT4               u4Length = 0;
    UINT1               au1OptValue[DHCP_MAX_OPT_LEN + 1];
    UINT1               au1OptionStr[DHCP_MAX_OPT_LEN + 1];
    tSNMP_OCTET_STRING_TYPE OptionValue;

    OptionValue.pu1_OctetList = au1OptValue;

    pHttp->i4Write = 0;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    i4OutCome = nmhGetFirstIndexDhcpSrvGblOptTable (&i4NextOpt);
    if (i4OutCome == SNMP_FAILURE)
    {
        return;
    }
    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        MEMSET (OptionValue.pu1_OctetList, 0, DHCP_MAX_OPT_LEN + 1);
        MEMSET (au1OptionStr, 0, DHCP_MAX_OPT_LEN + 1);

        nmhGetDhcpSrvGblOptLen (i4NextOpt, (INT4 *) &u4Length);
        nmhGetDhcpSrvGblOptVal (i4NextOpt, &OptionValue);

        IssProcessDhcpSrvOption (pHttp, (UINT1) i4NextOpt,
                                 &u4Length,
                                 OptionValue.pu1_OctetList,
                                 FALSE, au1OptionStr);

        STRCPY (pHttp->au1KeyString, "1.3.6.1.4.1.2076.84.1.11.1.1_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextOpt);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString, "OPT_TYPE_DUMMY_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextOpt);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString, "1.3.6.1.4.1.2076.84.1.11.1.3_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1OptionStr);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

        i4CurrOpt = i4NextOpt;
    }
    while (nmhGetNextIndexDhcpSrvGblOptTable (i4CurrOpt,
                                              &i4NextOpt) == SNMP_SUCCESS);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
 *  Function Name : IssProcessDhcpGlobOptConfPageSet
 *  Description   : This function processes the request coming for the  
 *                  DHCP Configuration page. 
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessDhcpGlobOptConfPageSet (tHttp * pHttp)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4OptionType = 0;
    UINT4               u4Length = 0;
    UINT1               u1IsCreated = FALSE;
    UINT1               au1OptValue[DHCP_MAX_OPT_LEN + 1];
    UINT1               au1OptionStr[DHCP_MAX_OPT_LEN + 1];
    tSNMP_OCTET_STRING_TYPE OptionValue;

    OptionValue.pu1_OctetList = au1OptValue;

    MEMSET (au1OptValue, 0, DHCP_MAX_OPT_LEN + 1);
    MEMSET (au1OptionStr, 0, DHCP_MAX_OPT_LEN + 1);

    STRCPY (pHttp->au1Name, "1.3.6.1.4.1.2076.84.1.11.1.1");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);
    u4OptionType = (UINT4) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "1.3.6.1.4.1.2076.84.1.11.1.3");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);

    u4Length = (STRLEN (pHttp->au1Value) < DHCP_MAX_OPT_LEN) ?
        STRLEN (pHttp->au1Value) : DHCP_MAX_OPT_LEN;
    MEMCPY (&au1OptionStr[0], pHttp->au1Value, u4Length);

    if (IssProcessDhcpSrvOption (pHttp, (UINT1) u4OptionType,
                                 &u4Length,
                                 &au1OptionStr[0], TRUE,
                                 OptionValue.pu1_OctetList) != OSIX_SUCCESS)
    {
        return;
    }
    OptionValue.i4_Length = (INT4) u4Length;

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {
        if (nmhTestv2DhcpSrvGblOptRowStatus (&u4ErrorCode, (INT4) u4OptionType,
                                             DESTROY) != SNMP_SUCCESS)
        {
            IssSendError (pHttp, (INT1 *) "Cannot delete the entry");
            return;
        }
        if (nmhSetDhcpSrvGblOptRowStatus ((INT4) u4OptionType, DESTROY) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp, (INT1 *) "Cannot delete the entry");
            return;
        }
        IssProcessDhcpGlobOptConfPageGet (pHttp);
        return;
    }
    else if (STRCMP (pHttp->au1Value, "Add") == 0)
    {
        u1IsCreated = TRUE;
        if (nmhTestv2DhcpSrvGblOptRowStatus (&u4ErrorCode, (INT4) u4OptionType,
                                             CREATE_AND_WAIT) != SNMP_SUCCESS)
        {
            IssSendError (pHttp, (INT1 *) "Cannot create the entry");
            return;
        }
        if (nmhSetDhcpSrvGblOptRowStatus ((INT4) u4OptionType, CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp, (INT1 *) "Cannot create the entry");
            return;
        }
    }
    else if (STRCMP (pHttp->au1Value, "Apply") == 0)
    {
        if (nmhTestv2DhcpSrvGblOptRowStatus (&u4ErrorCode, (INT4) u4OptionType,
                                             NOT_IN_SERVICE) != SNMP_SUCCESS)
        {
            IssSendError (pHttp, (INT1 *) "Cannot modify the entry");
            return;
        }
        if (nmhSetDhcpSrvGblOptRowStatus ((INT4) u4OptionType, NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp, (INT1 *) "Cannot modify the entry");
            return;
        }
    }

    if (nmhTestv2DhcpSrvGblOptLen
        (&u4ErrorCode, (INT4) u4OptionType, (INT4) u4Length) != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (INT1 *) "Cannot set the Option Length");
        return;
    }
    if (nmhTestv2DhcpSrvGblOptVal
        (&u4ErrorCode, (INT4) u4OptionType, &OptionValue) != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (INT1 *) "Cannot set the Option Value");
        return;
    }
    if (nmhSetDhcpSrvGblOptLen ((INT4) u4OptionType, (INT4) u4Length) ==
        SNMP_FAILURE)
    {
        IssSendError (pHttp, (INT1 *) "Cannot set the Option Length");
        return;
    }
    if (nmhSetDhcpSrvGblOptVal ((INT4) u4OptionType, &OptionValue) ==
        SNMP_FAILURE)
    {
        IssSendError (pHttp, (INT1 *) "Cannot set the Option Length");
        return;
    }
    if (nmhTestv2DhcpSrvGblOptRowStatus (&u4ErrorCode, (INT4) u4OptionType,
                                         ACTIVE) != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (INT1 *) "Cannot enable the entry");
        if (u1IsCreated == TRUE)
        {
            nmhSetDhcpSrvGblOptRowStatus ((INT4) u4OptionType, DESTROY);
        }
        return;
    }
    if (nmhSetDhcpSrvGblOptRowStatus ((INT4) u4OptionType, ACTIVE) ==
        SNMP_FAILURE)
    {
        IssSendError (pHttp, (INT1 *) "Cannot enable the entry");
        if (u1IsCreated == TRUE)
        {
            nmhSetDhcpSrvGblOptRowStatus ((INT4) u4OptionType, DESTROY);
        }
        return;
    }
    IssProcessDhcpGlobOptConfPageGet (pHttp);
}

/*********************************************************************
 *  Function Name : IssProcessDhcpHostOptConfPage 
 *  Description   : This function processes the request coming for the  
 *                  DHCP Configuration page. 
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessDhcpHostOptConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessDhcpHostOptConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessDhcpHostOptConfPageSet (pHttp);
    }
}

/*********************************************************************
 *  Function Name : IssProcessDhcpHostOptConfPageGet
 *  Description   : This function processes the request coming for the  
 *                  DHCP Configuration page. 
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessDhcpHostOptConfPageGet (tHttp * pHttp)
{
    INT4                i4OutCome = 0, i4CurrPoolId = 0;
    INT4                i4CurrOpt = 0, i4NextOpt = 0;
    INT4                i4FitstHostType = 0, i4NextHostType = 0;
    INT4                i4NextPoolId = 0;
    UINT4               u4Temp = 0;
    UINT4               u4Length = 0;
    UINT1               au1MacString[DHCPWEB_MAX_OPT_LEN];
    UINT1               au1OptValue[DHCPWEB_MAX_OPT_LEN + 1];
    UINT1               au1OptionStr[DHCPWEB_MAX_OPT_LEN + 1];
    UINT1               au1FirstHostId[DHCPWEB_MAX_OPT_LEN];
    UINT1               au1NextHostId[DHCPWEB_MAX_OPT_LEN];
    UINT1               au1PoolName[DHCP_MAX_NAME_LEN];
    tSNMP_OCTET_STRING_TYPE OptionValue, PoolName;
    tSNMP_OCTET_STRING_TYPE FirstHostId, NextHostId;

    PoolName.pu1_OctetList = &au1PoolName[0];
    OptionValue.pu1_OctetList = &au1OptValue[0];
    FirstHostId.pu1_OctetList = &au1FirstHostId[0];
    NextHostId.pu1_OctetList = &au1NextHostId[0];

    pHttp->i4Write = 0;
    IssPrintDhcpPools (pHttp);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    MEMSET (NextHostId.pu1_OctetList, 0, DHCPWEB_MAX_OPT_LEN);
    i4OutCome =
        nmhGetFirstIndexDhcpSrvHostOptTable (&i4NextHostType, &NextHostId,
                                             &i4NextPoolId, &i4NextOpt);

    if (i4OutCome == SNMP_FAILURE)
    {
        return;
    }
    do
    {
        MEMSET (OptionValue.pu1_OctetList, 0, DHCPWEB_MAX_OPT_LEN + 1);
        MEMSET (FirstHostId.pu1_OctetList, 0, DHCPWEB_MAX_OPT_LEN);
        MEMSET (PoolName.pu1_OctetList, 0, DHCP_MAX_NAME_LEN);

        pHttp->i4Write = (INT4) u4Temp;

        nmhGetDhcpSrvSubnetPoolName (i4NextPoolId, &PoolName);
        nmhGetDhcpSrvHostOptLen (i4NextHostType, &NextHostId, i4NextPoolId,
                                 i4NextOpt, (INT4 *) &u4Length);

        nmhGetDhcpSrvHostOptVal (i4NextHostType, &NextHostId, i4NextPoolId,
                                 i4NextOpt, &OptionValue);

        MEMSET (au1OptionStr, 0, DHCPWEB_MAX_OPT_LEN + 1);
        IssProcessDhcpSrvOption (pHttp, (UINT1) i4NextOpt,
                                 &u4Length,
                                 OptionValue.pu1_OctetList,
                                 FALSE, au1OptionStr);

        STRCPY (pHttp->au1KeyString, "1.3.6.1.4.1.2076.84.1.13.1.2_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        MEMSET (au1MacString, 0, DHCPWEB_MAX_OPT_LEN);
        IssPrintMacAddress (NextHostId.pu1_OctetList, &au1MacString[0]);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1MacString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString, "1.3.6.1.4.1.2076.84.1.9.1.3_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", PoolName.pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString, "1.3.6.1.4.1.2076.84.1.13.1.3_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextOpt);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString, "OPT_TYPE_DUMMY_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextOpt);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString, "1.3.6.1.4.1.2076.84.1.13.1.5_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1OptionStr);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "POOL_DUMMY_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextPoolId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

        MEMCPY (FirstHostId.pu1_OctetList, NextHostId.pu1_OctetList,
                NextHostId.i4_Length);
        FirstHostId.i4_Length = NextHostId.i4_Length;
        i4CurrOpt = i4NextOpt;
        i4CurrPoolId = i4NextPoolId;
        i4FitstHostType = i4NextHostType;
        MEMSET (NextHostId.pu1_OctetList, 0, DHCPWEB_MAX_OPT_LEN);
        NextHostId.i4_Length = 0;
    }
    while (nmhGetNextIndexDhcpSrvHostOptTable (i4FitstHostType, &i4NextHostType,
                                               &FirstHostId, &NextHostId,
                                               i4CurrPoolId, &i4NextPoolId,
                                               i4CurrOpt, &i4NextOpt)
           == SNMP_SUCCESS);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
 *  Function Name : IssProcessDhcpHostOptConfPageSet
 *  Description   : This function processes the request coming for the  
 *                  DHCP Configuration page. 
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessDhcpHostOptConfPageSet (tHttp * pHttp)
{
    INT4                i4RowStatus = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4OptionType = 0;
    UINT4               u4PoolId = 0;
    UINT4               u4Length = 0;
    INT4                i4HostType = 1;
    UINT1               u1IsCreated = FALSE;
    UINT1               au1OptValue[DHCP_MAX_OPT_LEN + 1];
    UINT1               au1OptionStr[DHCP_MAX_OPT_LEN + 1];
    UINT1               au1HostId[MAC_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE OptionValue, HostId;

    HostId.pu1_OctetList = &au1HostId[0];
    OptionValue.pu1_OctetList = &au1OptValue[0];

    MEMSET (au1OptValue, 0, DHCP_MAX_OPT_LEN + 1);
    MEMSET (au1OptionStr, 0, DHCP_MAX_OPT_LEN + 1);
    MEMSET (au1HostId, 0, MAC_ADDR_LEN);

    STRCPY (pHttp->au1Name, "1.3.6.1.4.1.2076.84.1.13.1.2");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Value, "") != 0)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        StrToMac (pHttp->au1Value, HostId.pu1_OctetList);
        HostId.i4_Length = MAC_ADDR_LEN;
    }

    STRCPY (pHttp->au1Name, "1.3.6.1.4.1.2076.84.1.9.1.3");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4PoolId = (UINT4) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "1.3.6.1.4.1.2076.84.1.13.1.3");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);
    u4OptionType = (UINT4) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "1.3.6.1.4.1.2076.84.1.13.1.5");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);

    u4Length = (STRLEN (pHttp->au1Value) < DHCP_MAX_OPT_LEN) ?
        STRLEN (pHttp->au1Value) : DHCP_MAX_OPT_LEN;
    MEMCPY (&au1OptionStr[0], pHttp->au1Value, u4Length);

    if (IssProcessDhcpSrvOption (pHttp, (UINT1) u4OptionType,
                                 &u4Length,
                                 &au1OptionStr[0], TRUE,
                                 OptionValue.pu1_OctetList) != OSIX_SUCCESS)
    {
        return;
    }
    OptionValue.i4_Length = (INT4) u4Length;

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {

        if (nmhTestv2DhcpSrvHostOptRowStatus (&u4ErrorCode, i4HostType,
                                              &HostId, (INT4) u4PoolId,
                                              (INT4) u4OptionType, DESTROY)
            != SNMP_SUCCESS)
        {
            IssSendError (pHttp, (INT1 *) "Cannot delete the entry");
            return;
        }
        if (nmhSetDhcpSrvHostOptRowStatus (i4HostType, &HostId, (INT4) u4PoolId,
                                           (INT4) u4OptionType, DESTROY)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp, (INT1 *) "Cannot delete the entry");
            return;
        }
        IssProcessDhcpHostOptConfPageGet (pHttp);
        return;
    }
    if (nmhGetDhcpSrvHostConfigRowStatus (i4HostType, &HostId, (INT4) u4PoolId,
                                          &i4RowStatus) == SNMP_SUCCESS)
    {
        if (nmhSetDhcpSrvHostConfigRowStatus
            (i4HostType, &HostId, (INT4) u4PoolId,
             NOT_IN_SERVICE) != SNMP_SUCCESS)
        {
            IssSendError (pHttp, (INT1 *) "Cannot modify the entry");
            return;
        }
    }
    else
    {
        if (nmhSetDhcpSrvHostConfigRowStatus
            (i4HostType, &HostId, (INT4) u4PoolId,
             CREATE_AND_WAIT) != SNMP_SUCCESS)
        {
            IssSendError (pHttp, (INT1 *) "Cannot modify the entry");
            return;
        }
    }
    if (STRCMP (pHttp->au1Value, "Add") == 0)
    {
        u1IsCreated = TRUE;
        if (nmhTestv2DhcpSrvHostOptRowStatus (&u4ErrorCode, i4HostType,
                                              &HostId, (INT4) u4PoolId,
                                              (INT4) u4OptionType,
                                              CREATE_AND_WAIT) != SNMP_SUCCESS)
        {
            IssSendError (pHttp, (INT1 *) "Cannot add new the entry");
            return;
        }
        if (nmhSetDhcpSrvHostOptRowStatus (i4HostType, &HostId,
                                           (INT4) u4PoolId, (INT4) u4OptionType,
                                           CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (INT1 *) "Cannot add new the entry");
            return;
        }
    }
    else if (STRCMP (pHttp->au1Value, "Apply") == 0)
    {
        if (nmhTestv2DhcpSrvHostOptRowStatus (&u4ErrorCode, i4HostType,
                                              &HostId, (INT4) u4PoolId,
                                              (INT4) u4OptionType,
                                              NOT_IN_SERVICE) != SNMP_SUCCESS)
        {
            IssSendError (pHttp, (INT1 *) "Cannot modify the entry");
            return;
        }
        if (nmhSetDhcpSrvHostOptRowStatus (i4HostType, &HostId,
                                           (INT4) u4PoolId, (INT4) u4OptionType,
                                           NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (INT1 *) "Cannot modify the entry");
            return;
        }
    }

    if (nmhTestv2DhcpSrvHostOptLen (&u4ErrorCode, i4HostType, &HostId,
                                    (INT4) u4PoolId, (INT4) u4OptionType,
                                    (INT4) u4Length) != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (INT1 *) "Cannot set the Option Length");
        return;
    }
    if (nmhTestv2DhcpSrvHostOptVal (&u4ErrorCode, i4HostType, &HostId,
                                    (INT4) u4PoolId, (INT4) u4OptionType,
                                    &OptionValue) != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (INT1 *) "Cannot set the Option Value");
        return;
    }
    if (nmhSetDhcpSrvHostOptLen
        (i4HostType, &HostId, (INT4) u4PoolId, (INT4) u4OptionType,
         (INT4) u4Length) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (INT1 *) "Cannot set the Option Length");
        return;
    }
    if (nmhSetDhcpSrvHostOptVal
        (i4HostType, &HostId, (INT4) u4PoolId, (INT4) u4OptionType,
         &OptionValue) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (INT1 *) "Cannot set the Option Length");
        return;
    }
    if (nmhTestv2DhcpSrvHostOptRowStatus (&u4ErrorCode, i4HostType, &HostId,
                                          (INT4) u4PoolId, (INT4) u4OptionType,
                                          ACTIVE) != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (INT1 *) "Cannot enable the entry");
        if (u1IsCreated == TRUE)
        {
            nmhSetDhcpSrvHostOptRowStatus (i4HostType, &HostId, (INT4) u4PoolId,
                                           (INT4) u4OptionType, DESTROY);
        }
        return;
    }
    if (nmhSetDhcpSrvHostConfigRowStatus (i4HostType, &HostId, (INT4) u4PoolId,
                                          ACTIVE) != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (INT1 *) "Cannot enable the entry");
        return;
    }
    if (nmhSetDhcpSrvHostOptRowStatus (i4HostType, &HostId, (INT4) u4PoolId,
                                       (INT4) u4OptionType,
                                       ACTIVE) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (INT1 *) "Cannot enable the entry");
        if (u1IsCreated == TRUE)
        {
            nmhSetDhcpSrvHostOptRowStatus (i4HostType, &HostId, (INT4) u4PoolId,
                                           (INT4) u4OptionType, DESTROY);
        }
        return;
    }
    IssProcessDhcpHostOptConfPageGet (pHttp);
}

/*********************************************************************
 *  Function Name : IssProcessDhcpHostIpConfPage
 *  Description   : This function processes the request coming for the
 *                  DHCP Configuration page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessDhcpHostIpConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessDhcpHostIpConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessDhcpHostIpConfPageSet (pHttp);
    }
}

/*********************************************************************
 *  Function Name : IssProcessDhcpHostIPConfPageGet
 *  Description   : This function processes the request coming for the
 *                  DHCP Configuration page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessDhcpHostIpConfPageGet (tHttp * pHttp)
{
    INT4                i4OutCome = 0;
    INT4                i4FirstHostType = 0, i4NextHostType = 0;
    INT4                i4TempFirstPoolIndex, i4TempNextPoolIndex;
    UINT4               u4Temp = 0;
    UINT4               u4HostIpAddress;
    UINT1               au1MacString[DHCPWEB_MAX_OPT_LEN];
    UINT1               au1FirstHostId[DHCPWEB_MAX_OPT_LEN];
    UINT1               au1NextHostId[DHCPWEB_MAX_OPT_LEN];
    UINT1               au1PoolName[DHCP_MAX_NAME_LEN];
    UINT1               au1HostConfigName[DHCPWEB_MAX_OPT_LEN];
    UINT1              *pu1String = NULL;
    tSNMP_OCTET_STRING_TYPE PoolName;
    tSNMP_OCTET_STRING_TYPE FirstHostId, NextHostId;

    PoolName.pu1_OctetList = &au1PoolName[0];
    FirstHostId.pu1_OctetList = &au1FirstHostId[0];
    NextHostId.pu1_OctetList = &au1NextHostId[0];

    pHttp->i4Write = 0;
    IssPrintDhcpPools (pHttp);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    MEMSET (NextHostId.pu1_OctetList, 0, DHCPWEB_MAX_OPT_LEN);

    /* Display Host configuration table */
    i4OutCome = nmhGetFirstIndexDhcpSrvHostConfigTable (&i4FirstHostType,
                                                        &FirstHostId,
                                                        &i4TempFirstPoolIndex);
    if (i4OutCome == SNMP_FAILURE)
    {
        return;
    }
    else
    {
        i4TempNextPoolIndex = i4TempFirstPoolIndex;
        i4NextHostType = i4FirstHostType;

        NextHostId.i4_Length = FirstHostId.i4_Length;
        MEMCPY (NextHostId.pu1_OctetList, FirstHostId.pu1_OctetList,
                FirstHostId.i4_Length);

        do
        {
            pHttp->i4Write = (INT4) u4Temp;

            i4TempFirstPoolIndex = i4TempNextPoolIndex;
            i4FirstHostType = i4NextHostType;
            FirstHostId.i4_Length = NextHostId.i4_Length;
            MEMCPY (FirstHostId.pu1_OctetList,
                    NextHostId.pu1_OctetList, NextHostId.i4_Length);
            u4HostIpAddress = 0;
            MEMSET (au1HostConfigName, 0, DHCPWEB_MAX_OPT_LEN);

            MEMSET (PoolName.pu1_OctetList, 0, DHCP_MAX_NAME_LEN);
            nmhGetDhcpSrvSubnetPoolName (i4TempFirstPoolIndex, &PoolName);
            nmhGetDhcpSrvHostIpAddress (i4FirstHostType,
                                        &FirstHostId,
                                        i4TempFirstPoolIndex, &u4HostIpAddress);
            STRCPY (pHttp->au1KeyString, "HOST_MAC_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMSET (au1MacString, 0, DHCPWEB_MAX_OPT_LEN);
            IssPrintMacAddress (NextHostId.pu1_OctetList, &au1MacString[0]);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1MacString);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            STRCPY (pHttp->au1KeyString, "POOL_INDEX_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     PoolName.pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "DHCP_POOL_INDEX");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4TempFirstPoolIndex);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));

            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            STRCPY (pHttp->au1KeyString, "HOST_IP_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4HostIpAddress);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        }

        while (nmhGetNextIndexDhcpSrvHostConfigTable (i4FirstHostType,
                                                      &i4NextHostType,
                                                      &FirstHostId,
                                                      &NextHostId,
                                                      i4TempFirstPoolIndex,
                                                      &i4TempNextPoolIndex)
               == SNMP_SUCCESS);
    }
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
 *  Function Name : IssProcessDhcpHostIpConfPageSet
 *  Description   : This function processes the request coming for the  
 *                  DHCP Configuration page. 
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
IssProcessDhcpHostIpConfPageSet (tHttp * pHttp)
{
    INT4                i4RowStatus = 0, i4NewRowStatus;
    UINT4               u4ErrorCode = 0;
    UINT4               u4PoolId = 0;
    UINT4               u4IpAddress = 0;
    UINT4               u4HostType = 1;
    INT4                i4RetVal = 0;
    UINT1               au1HostId[MAC_ADDR_LEN];
    UINT1               au1MapString[DHCP_MAX_NAME_LEN];
    tSNMP_OCTET_STRING_TYPE HostId;
    HostId.pu1_OctetList = &au1HostId[0];
    HostId.i4_Length = 0;
    MEMSET (au1HostId, 0, MAC_ADDR_LEN);
    MEMSET (au1MapString, 0, DHCP_MAX_NAME_LEN);

    STRCPY (pHttp->au1Name, "HOST_MAC");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Value, "") != 0)
    {
        issDecodeSpecialChar (pHttp->au1Value);
        StrToMac (pHttp->au1Value, HostId.pu1_OctetList);
        HostId.i4_Length = MAC_ADDR_LEN;
    }

    STRCPY (pHttp->au1Name, "POOL_INDEX");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4PoolId = (UINT4) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "HOST_IP");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    u4IpAddress = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Reset") == 0)
    {
        i4RetVal = nmhGetDhcpSrvHostConfigRowStatus ((INT4) u4HostType, &HostId,
                                                     (INT4) u4PoolId,
                                                     &i4RowStatus);

        /* Scenario when the row doesnt exist */
        if (i4RetVal == SNMP_FAILURE)
        {
            IssSendError (pHttp, (INT1 *)
                          " Host configuration deletion failed. Entry does not exist ");
            return;
        }
        if (nmhTestv2DhcpSrvHostConfigRowStatus
            (&u4ErrorCode, (INT4) u4HostType, &HostId, (INT4) u4PoolId,
             NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (INT1 *)
                          " Host configuration entry deletion failed");
            return;
        }
        if (nmhSetDhcpSrvHostConfigRowStatus ((INT4) u4HostType, &HostId,
                                              (INT4) u4PoolId,
                                              NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (INT1 *)
                          " Host configuration entry deletion failed");
            return;
        }
        if (nmhSetDhcpSrvHostIpAddress
            ((INT4) u4HostType, &HostId, (INT4) u4PoolId, 0) == SNMP_FAILURE)
        {
            /*Restore the row status to i4RowStatus */
            nmhSetDhcpSrvHostConfigRowStatus ((INT4) u4HostType, &HostId,
                                              (INT4) u4PoolId, ACTIVE);
            return;
        }

        if (nmhTestv2DhcpSrvHostConfigRowStatus
            (&u4ErrorCode, (INT4) u4HostType, &HostId, (INT4) u4PoolId,
             ACTIVE) == SNMP_FAILURE)
        {
            nmhSetDhcpSrvHostConfigRowStatus ((INT4) u4HostType, &HostId,
                                              (INT4) u4PoolId, i4RowStatus);
            IssSendError (pHttp, (INT1 *)
                          "Host configuration entry creation failed");
            return;
        }
        if (nmhSetDhcpSrvHostConfigRowStatus ((INT4) u4HostType, &HostId,
                                              (INT4) u4PoolId,
                                              ACTIVE) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (INT1 *)
                          " Host configuration entry creation failed");
            return;
        }

        IssProcessDhcpHostIpConfPageGet (pHttp);
        return;
    }
    if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {
        if (nmhTestv2DhcpSrvHostConfigRowStatus
            (&u4ErrorCode, (INT4) u4HostType, &HostId, (INT4) u4PoolId,
             DESTROY) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (INT1 *)
                          "Host configuration entry deletion failed");
            return;
        }
        if (nmhSetDhcpSrvHostConfigRowStatus ((INT4) u4HostType, &HostId,
                                              (INT4) u4PoolId, DESTROY) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp, (INT1 *)
                          " Host configuration entry deletion failed");
            return;
        }

        IssProcessDhcpHostIpConfPageGet (pHttp);
        return;
    }
    i4RetVal = nmhGetDhcpSrvHostConfigRowStatus ((INT4) u4HostType, &HostId,
                                                 (INT4) u4PoolId, &i4RowStatus);

    if (STRCMP (pHttp->au1Value, "Add") == 0)
    {
        if (nmhTestv2DhcpSrvHostConfigRowStatus
            (&u4ErrorCode, (INT4) u4HostType, &HostId, (INT4) u4PoolId,
             CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (INT1 *) "Cannot add new the entry");
            return;
        }
        if (nmhSetDhcpSrvHostConfigRowStatus ((INT4) u4HostType, &HostId,
                                              (INT4) u4PoolId,
                                              CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (INT1 *) "Cannot add new the entry");
            return;
        }
    }
    else if (STRCMP (pHttp->au1Value, "Apply") == 0)
    {
        if (nmhTestv2DhcpSrvHostConfigRowStatus
            (&u4ErrorCode, (INT4) u4HostType, &HostId, (INT4) u4PoolId,
             NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (INT1 *) "Cannot add new the entry");
            return;
        }
        if (nmhSetDhcpSrvHostConfigRowStatus ((INT4) u4HostType, &HostId,
                                              (INT4) u4PoolId,
                                              NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (INT1 *) "Cannot add new the entry");
            return;
        }
    }

    /*Row status for restoration of the row in configuration failure cases */
    i4NewRowStatus = (i4RetVal == SNMP_SUCCESS) ? i4RowStatus : DESTROY;

    if (nmhTestv2DhcpSrvHostIpAddress (&u4ErrorCode, (INT4) u4HostType,
                                       &HostId, (INT4) u4PoolId,
                                       u4IpAddress) == SNMP_FAILURE)
    {
        nmhSetDhcpSrvHostConfigRowStatus ((INT4) u4HostType, &HostId,
                                          (INT4) u4PoolId, i4NewRowStatus);
        if (u4ErrorCode == SNMP_ERR_WRONG_VALUE)
        {
            IssSendError (pHttp, (INT1 *) " IP address Already allocated."
                          "Release that allocation first");
        }
        else
        {
            IssSendError (pHttp, (INT1 *)
                          "Configuring IP address to host failed");
        }
        return;
    }

    if (nmhSetDhcpSrvHostIpAddress ((INT4) u4HostType, &HostId, (INT4) u4PoolId,
                                    u4IpAddress) == SNMP_FAILURE)
    {
        nmhSetDhcpSrvHostConfigRowStatus ((INT4) u4HostType, &HostId,
                                          (INT4) u4PoolId, i4NewRowStatus);
        IssSendError (pHttp, (INT1 *) "Configuring IP address to host failed");
        return;
    }

    if (nmhTestv2DhcpSrvHostConfigRowStatus (&u4ErrorCode, (INT4) u4HostType,
                                             &HostId,
                                             (INT4) u4PoolId,
                                             ACTIVE) == SNMP_FAILURE)
    {
        nmhSetDhcpSrvHostConfigRowStatus ((INT4) u4HostType, &HostId,
                                          (INT4) u4PoolId, i4NewRowStatus);
        IssSendError (pHttp, (INT1 *)
                      " Host configuration entry creation failed");
        return;
    }

    if (nmhSetDhcpSrvHostConfigRowStatus ((INT4) u4HostType, &HostId,
                                          (INT4) u4PoolId,
                                          ACTIVE) == SNMP_FAILURE)
    {
        nmhSetDhcpSrvHostConfigRowStatus ((INT4) u4HostType, &HostId,
                                          (INT4) u4PoolId, i4NewRowStatus);
        IssSendError (pHttp, (INT1 *)
                      "Host configuration entry creation failed");
        return;
    }
    IssProcessDhcpHostIpConfPageGet (pHttp);
    return;
}
#endif /* __DHCPSWEB_C__ */
