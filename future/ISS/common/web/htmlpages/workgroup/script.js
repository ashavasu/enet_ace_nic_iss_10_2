/* $Id: script.js,v 1.50 2013/12/14 11:07:33 siva Exp $ */
function selectOption()
{
   var m=document.forms.length;
   var selectType = "select-one";
   for (i=0;i<m;i++)
   {
      var n = document.forms[i].elements.length;
      for (j=0;j<n;j++)
      {
         if((document.forms[i].elements[j].type == selectType) &&
            (document.forms[i].elements[j+1].value != ""))
         {
            document.forms[i].elements[j].value = 
                  document.forms[i].elements[j+1].value;
         }
      }
   }
}
function textOption()
{
   
   var m=document.forms.length;
   var selectType = "text";
   for (i=0;i<m;i++)
   {
      var n = document.forms[i].elements.length;
      for (j=0;j<n;j++)
      {
         if((document.forms[i].elements[j].type == selectType) &&
            (document.forms[i].elements[j+1].value != ""))
         {
            
            document.forms[i].elements[j].value =
                  document.forms[i].elements[j+1].value;
         }
      }
   }

}
function checkForUnicastIP(field) 
{
   var errorString = "";
   var ipPattern = /^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/;
   var ipArray = field.value.match(ipPattern);

   if( field.value == "" )
     return true;

   if (ipArray == null)
      errorString = "ERROR: " + field.value + " is not a valid IP address";
   else 
   {
      if(ipArray[1] > 223)
      {
         alert("ERROR: Enter unicast IP address");
         field.value = "";
         return false;
      }
      for (i = 1; i < 5; i++) 
      {
         thisSegment = ipArray[i];
         if (thisSegment > 255) 
         {
            errorString ="ERROR: " + field.value + " is not a valid IP address";
            break;
         }
      }
   }

   if (errorString == "")
      return true;
   else
   {
      alert (errorString);
      field.value = "";
      return false;
   }
}          
function checkForIP(field) 
{
   
   var errorString = "";
   var ipPattern = /^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/;
   var ipArray = field.value.match(ipPattern);
   var errCount = "0";

   if( field.value == "" )
      return true;

   if (ipArray == null)
      errorString = "ERROR: " + field.value + " is not a valid IP address";
   else {
      for (i = 1; i < 5; i++) 
      {
         thisSegment = ipArray[i];
         if (thisSegment == 0)
         {
            errCount++;
         } 
         if (thisSegment > 255)
         {
            errorString = "ERROR: " + field.value + " is not a valid IP address";
            break;
         }
      }
   }

   if (errorString == "")
      return true;
   else
   {
      alert (errorString);
      field.value = "";
      return false;
   }
}          
function checkForUnNumberedIP(field) 
{
   var errorString = "";
   var ipPattern = /^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/;
   var ipArray = field.value.match(ipPattern);

   if( field.value == "" )
      return true;

   if (ipArray == null)
      errorString = "ERROR: " + field.value + " is not a valid IP address";
   else {
      for (i = 1; i < 5; i++) 
      {
         thisSegment = ipArray[i];
         if (thisSegment > 255) 
         {
            errorString = "ERROR: " + field.value + " is not a valid IP address";
            break;
         }
      }
   }

   if (errorString == "")
      return true;
   else
   {
      alert (errorString);
      field.value = "";
      return false;
   }
}          
function checkForUnNumberedMask(field) 
{
   var errorString = "";
   var ipPattern = /^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/;
   var ipArray = field.value.match(ipPattern);

   if( field.value == "" )
      return true;

   if (ipArray == null)
      errorString = "ERROR: " + field.value + " is not a valid IP Mask";
   else {
      for (i = 1; i < 5; i++) 
      {
         thisSegment = ipArray[i];
         if (thisSegment > 255) 
         {
            errorString = "ERROR: " + field.value + " is not a valid IP mask";
            break;
         }
      }
   }

   if (errorString == "")
      return true;
   else
   {
      alert (errorString);
      field.value = "";
      return false;
   }
}
function checkForNumeric(field) 
{  
   var valid = "0123456789";
   var temp; 
   var n = field.value.length;

   for (var i=0; i<n; i++) 
   {  
      temp = "" + field.value.substring(i, i+1);
      if (valid.indexOf(temp) == "-1")
      {
         alert("ERROR: Only numbers are accepted!");
         field.value = "";
         return false;
      }
   }
   return true;
}
function checkForNumericAndComma(field) 
{  
   var valid = "0123456789,";
   var temp; 
   var n = field.value.length;

   for (var i=0; i<n; i++) 
   {  
      temp = "" + field.value.substring(i, i+1);
      if (valid.indexOf(temp) == "-1")
      {
         alert("ERROR: Only numbers separated by commas are accepted!");
         field.value = "";
         return false;
      }
   }
   return true;
}
function checkForNumericAndColon(field)
{
   var valid = "0123456789:";
   var temp;
   var n = field.value.length;

   for (var i=0; i<n; i++)
   {
      temp = "" + field.value.substring(i, i+1);
      if (valid.indexOf(temp) == "-1")
      {
         alert("ERROR: Only numbers separated by commas are accepted!");
         field.value = "";
         return false;
      }
   }
   return true;
}

/*********************************************************/
/* function to check whether the input is Alphabets      */
/*********************************************************/
function checkForAlphaPorts(field)
{
   var valid = "FfGgEepPAa";
   var valid1 = "AaIiXxCcWwOo";
   var temp;
   var temp1;
   var i=0;

   temp = "" + field.value.substring(i, i+1);
   temp1 = "" + field.value.substring(i+1, i+2);
   if (valid.indexOf(temp) == "-1")
   {
       alert("ERROR:Invalid Input ; Give  Gi1/1 or Ex1/1 or Fa1/1!");
       field.value = "";
       return false;
   }
   if (valid1.indexOf(temp1) == "-1")
   {
       alert("ERROR:Invalid Input ; Give  Gi1/1 or Ex1/1 or Fa1/1!");
       field.value = "";
       return false;
   }

   return true;
}
function checkForAlphaPortsCase(field)
{
   var valid = "FGEPA";
   var valid1 = "aixcwo";
   var temp;
   var temp1;
   var i=0;
   temp = "" + field.value.substring(i, i+1);
   temp1 = "" + field.value.substring(i+1, i+2);
   if (valid.indexOf(temp) == "-1")
   {
       alert("ERROR:Invalid Input ; Give  Gi0/1 or Ex0/1 or Fa0/1!");
       field.value = "";
       return false;
   }
   if (valid1.indexOf(temp1) == "-1")
   {
       alert("ERROR:Invalid Input ; Give  Gi1/1 or Ex1/1 or Fa1/1!");
       field.value = "";
       return false;
   }

   return true;
}

function checkForAlpha(field) 
{  
   var valid = "abcdefghijklmnopqrstuvwxyz";
   var temp; 
   var n = field.value.length;

   for (var i=0; i<n; i++) 
   {  
      temp = "" + field.value.substring(i, i+1);
      if (valid.indexOf(temp) == "-1")
      {
         alert("ERROR: Only characters are accepted!");
         field.value = "";
         return false;
      }
   }
   return true;
}
function checkForOID(field) 
{ 
   var valid = "0123456789.";
   var temp; 
   var n = field.value.length;

   for (var i=0; i<n; i++) 
   { 
      temp = "" + field.value.substring(i, i+1); 
      if (valid.indexOf(temp) == "-1") 
      {
         alert("ERROR: Only numbers are accepted!" + 
                  "eg: 1.3.6.1.12.1.1.0"); 
         field.value = "";
         return false;
      }
   } 
   return true;
}   
function checkForPerticularOID(field,oid)
{
   var str ,str1, n;
   n = field.value.lastIndexOf(".");
   str = field.value.substring(0,n);
   str1 = field.value.substring(n+1);
   if( oid != str || str1 == 0 )
   {
       alert("ERROR : Must be " + oid + ".Valid-Ifindex");
       field.value = "";
       return false;
   }
   else
       return true;
} 
function checkForAlphaNumeric(field) 
{ 
   var valid = "abcdefghijklmnopqrstuvwxyz0123456789";
   var temp; 
   var n = field.value.length;

   if( field.value == "" )
      return true;

   for (i=0; i<n; i++) 
   { 
      temp = "" + field.value.substring(i, i+1); 
      if (valid.indexOf(temp.toLowerCase()) == "-1") 
      {
         alert("ERROR: Only characters and numbers are accepted!"); 
         field.value = "";
         return false;
      }
   } 
   return true;
}    

function checkForFileName(field) 
{ 
   var valid = "abcdefghijklmnopqrstuvwxyz0123456789./";
   var temp; 
   var n = field.value.length;

   if( field.value == "" )
      return true;

   for (i=0; i<n; i++) 

   { 
      temp = "" + field.value.substring(i, i+1); 
      if (valid.indexOf(temp.toLowerCase()) == "-1") 
      {
         alert("ERROR: Only characters, numbers, / and . alone accepted!"); 

         field.value = "";
         return false;
      }
   } 
   return true;
}

function checkForRange(minValue, maxValue, field)
{
   /* if the actual value is empty */

   if(field.value == "" )
      return true;
  
   if( (field.value != "") &&
       (checkForNumeric(field)) &&
       (field.value >= minValue) && (field.value <= maxValue) )
   {
      return true;
   }
   else
   {
      alert("ERROR: Input should be between " + minValue + " and " + maxValue);
      field.value = "";
      return false;
   }
}
function checkFormAndSubmit(inputForm)
{
   var n=inputForm.elements.length;

   for(i=0; i<n; i++)
   {
      param = inputForm.elements[i];
      
      if((param.type == "hidden") || (param.disabled == true))
         continue; /* NOTE: unwanted hidden param must be disabled in page */

      if(param.name.substring(0, 4) == "reqd")
      {
         if(param.value == "")
         {
            alert("ERROR: Fill inputs which are marked '*'"); 
            return false;
         }
      }
      else
      {
         /* optional params, if NULL disabled it from input request */
         if(param.value == "")
         {
            param.disabled = true;
         }
      }
   }
   /* remove 'reqd' keyword from the inputs */
   for(i=0; i<n; i++)
   {
      param = inputForm.elements[i];
      if(param.name.substring(0, 4) == "reqd")
         param.name = param.name.substring(4, param.name.length);
   }
   return true;
}
function sendGenericForm(form1, form2,rowElements)
{
   var n=form2.elements.length;
   var m=form1.elements.length;
   var k=3;
   var j=0;
   var i=0;
   var rowFound = false;
   var temp = new Object(rowElements);
   var milisec = 0;

   /* copy the request to temp object */
   while((i < n) && (j < rowElements))
   {
      /* find which ROW is selected */
      if (form2.elements[i].checked == true)
      {
         rowFound = true;
         i++;
      }
      if((rowFound == true) && (form2.elements[i].type != "hidden"))
      {
         if(form2.elements[i].name == "reqdHelloTime")
         {
            milisec =  form2.elements[i].value * 100;
            form2.elements[i].value = milisec;
         }
         temp[j] = form2.elements[i];
         j++;
      }
      i++;
   };
   if(rowFound == false)
      return;

   /* There are 2 ways to change a row in table.               */
   /* 1. Using ROWSTATUS   2. Using AdminStatus 3. EntryStatus */
   if(temp[rowElements-1].name == "RowStatus")
   {
      if(temp[rowElements-1].value == "6")
      {
         /* set the ADMIN_STATUS/ROWSTATUS object to DESTROY */
         form1.elements[2].value = 6;
         /* set the index */
         form1.elements[3].value = temp[0].value;

         /* disable all other inputs */
         for ( i=4; i<(m-1); i++)
            form1.elements[i].disabled = true;

         form1.submit();
         return;
      }
      /* If 'Under creation' is selected, convert it */
      if(temp[rowElements-1].value == "3")
         temp[rowElements-1].value = "2";
   }
   else if((temp[rowElements-1].name == "EntryStatus") &&
                   (temp[rowElements-1].value == 4))
   {
           /* set the ADMIN_STATUS/ROWSTATUS object to DESTROY */
           form1.elements[2].value = 4;
           /* set the index */
           form1.elements[3].value = temp[0].value;

           /* disable all other inputs */
           for ( i=4; i<(m-1); i++)
               form1.elements[i].disabled = true;

           form1.submit();
           return;
   }
   else if((temp[rowElements-1].name == "EntryStatus") &&
                   (temp[rowElements-1].value == 3))
   {
      temp[rowElements-1].value = "";
   }
   /* copy the request to input form */
   for(i=0; i<j; i++)
   {
      param = temp[i];
      /* check whether input value is present */
      if( param.value == "" )
      {
              /* check whether it is mandatory param */
              if(param.name.substring(0, 4) == "reqd")
              {
                      /* give a alert and stop submit */
                      alert("ERROR: Enter mandatory inputs");
                      param.select();
                      return;
              }
              else
              {
                      /* optional param, disable this from input request */
                      form1.elements[k].disabled = true;
                      k = k + 1;
              }
      }
      else
      {
          if (param.disabled == true)
          {
              form1.elements[k].disabled = true;
          }
          else 
          {
              form1.elements[k].value = param.value;
          }
          k = k+1;
      }
   }
   form1.submit();
}
function sendForm(form1, form2,rowElements)
{
   var n=form2.elements.length;
   var m=form1.elements.length;
   var k=3;
   var j=0;
   var i=0;
   var rowFound = false;
   var temp = new Object(rowElements);

   /* copy the request to temp object */
   while((i < n) && (j < rowElements))
   {
      /* find which ROW is selected */
      if (form2.elements[i].checked == true)
      {
         rowFound = true;
         i++;
      }
      if( (rowFound == true) && (form2.elements[i].disabled != true))
      {
         temp[j] = form2.elements[i];
         j++;
      }
      i++;
   };
   if(rowFound == false)
      return;

   /* There are 2 ways to change a row in table.               */
   /* 1. Using ROWSTATUS   2. Using AdminStatus 3. EntryStatus */
   if(temp[rowElements-1].name == "RowStatus")
   {
      if(temp[rowElements-1].value == "6")
      {
         /* set the ADMIN_STATUS/ROWSTATUS object to DESTROY */
         form1.elements[2].value = 6;
         /* set the index */
         form1.elements[3].value = temp[0].value;

         /* disable all other inputs */
         for ( i=4; i<(m-1); i++)
            form1.elements[i].disabled = true;

         form1.submit();
         return;
      }
      /* If 'Under creation' is selected, convert it */
      if(temp[rowElements-1].value == "3")
         temp[rowElements-1].value = "2";
   }
   else if((temp[rowElements-1].name == "EntryStatus") &&
                   (temp[rowElements-1].value == 4))
   {
           /* set the ADMIN_STATUS/ROWSTATUS object to DESTROY */
           form1.elements[2].value = 4;
           /* set the index */
           form1.elements[3].value = temp[0].value;

           /* disable all other inputs */
           for ( i=4; i<(m-1); i++)
               form1.elements[i].disabled = true;

           form1.submit();
           return;
   }
   else if((temp[rowElements-1].name == "EntryStatus") &&
                   (temp[rowElements-1].value == 3))
   {
      temp[rowElements-1].value = "";
   }
   /* copy the request to input form */
   for(i=0; i<rowElements; i++)
   {
      param = temp[i];
      /* check whether input value is present */
      if( param.value == "" )
      {
              /* check whether it is mandatory param */
              if(param.name.substring(0, 4) == "reqd")
              {
                      /* give a alert and stop submit */
                      alert("ERROR: Enter mandatory inputs");
                      param.select();
                      return;
              }
              else
              {
                      /* optional param, disable this from input request */
                      form1.elements[k].disabled = true;
                      k = k + 1;
              }
      }
      else
      {
              form1.elements[k].value = param.value;
              k = k+1;
      }
   }
   if(rowFound == true) /* To avoid submit, when table is empty */
      form1.submit();
}
function showApply(form)
{
   var count = form.elements.length;
   
   if(count == 1)
      form.elements[0].disabled = true;
}
function setGambit()
{
   document.forms[1].elements[0].value = 
          document.forms[0].elements[0].value;
}
function checkForPortLists(param)
{
	var temp = new Object(300);
	var n = param.value.length;
	var count =0;
	var x = new Object();
   var y = new Object();
	var pattern = /^(\d{1,3})\-(\d{1,3})$/;
   var pattern1 = /^(po\d{1,3})$/;
   if(param.value == "" )
      return true;

	/* seperate the ',' values */
	for(i=0; i<n; i++)
	{
		x = "";
		while((param.value.charAt(i) != ",") && (i < n))
		{
			x += param.value.charAt(i);
			i++;
		}
		temp[count] = x;
		count++;
	}
   if( count == 0)
   {
      alert("ERROR: Enter valid values \n eg: 1,2,3,9-23,po1,po2");   
      param.value = "";
      return false;
   }

	/* get the '-' values */
	for (i=0; (temp[i] != null); i++)
	{
		x = temp[i].match(pattern);
      y = temp[i].match(pattern1)      
		if(x == null && y == null)
		{
         if(!checkForNum(temp[i]))
         {
            alert("ERROR: Check the port list\n eg: 1,2,5-10,23,po1,po2");
            param.value = "";
            return false;
         }
         continue;
		}
	}
   return true;
}
function checkForNum(value) 
{  
   var valid = "0123456789";
   var temp; 
   var n = value.length;

   for (var i=0; i<n; i++) 
   {  
      temp = "" + value.substring(i, i+1);
      if (valid.indexOf(temp) == "-1")
         return false;
   }
   return true;
}
function checkForUniMAC(field)
{
   var errorString = "";
   var macPattern = /^(\w{2})\:(\w{2})\:(\w{2})\:(\w{2})\:(\w{2})\:(\w{2})$/;
   var thisSegment;
   var temp = "";
   var valid = "1234567890abcdef";
   var macArray = field.value.match(macPattern);

    if( field.value == "" )
      return true;


   if (macArray == null)
      errorString = "ERROR: Enter valid MAC address";
   else 
   {
      /* check for unicast MAC address */
      if((parseInt(macArray[1], 16) & 0x01) == 1)
      {
         alert("ERROR: Entered MAC is not UNICAST type");
         field.value = "";
         return false;
      }
      if((macArray[1] == "00") && (macArray[2] == "00") && 
         (macArray[3] == "00") && (macArray[4] == "00") &&
         (macArray[5] == "00") && (macArray[6] == "00"))
      {
         alert("ERROR: Enter valid MAC address" +
                  "\n eg: 0a:11:1e:09:54:76");
         field.value = "";
         return false;
      }
      for (i = 1; i < 7; i++)
      {
         thisSegment = macArray[i];
         for(j = 0; j < 2; j++)
         {
            temp = thisSegment.charAt(j).toLowerCase();
            if(valid.indexOf(temp) == -1)
            {
               errorString = "ERROR: Enter valid MAC address";
               break;
            }
         }
         if (errorString != "")
            break;
      }
   }
   if (errorString == "")
      return true;
   else
   {
      alert (errorString + "\n eg: 0a:11:1e:09:54:76");
      field.value = "";
      return false;
   }
}
function submitSpecificForm(form1, form2, rowElements)
{
    var i = 0;
    var j = 0;
    var k = 0;
    var m=form1.elements.length;
    var n=form2.elements.length;

    while(i < n)
    {
        /* find which ROW is selected */
        if (form2.elements[i].checked == true)
        {
            rowFound = true;
            break;
        }
        i++;
    };
    if (rowFound == false)
        return false;

    /*Copying to Form 1*/
    for (j = 1; j < m; j++)
    {
        elemFound = true;
        /*From Form 2*/
        for (k = i+1; k < i+rowElements; k++)
        {
            if (form1.elements[j].name == form2.elements[k].name)
            {
                if (form2.elements[k].disabled == false)
                {
                    form1.elements[j].value = form2.elements[k].value;
                    elemFound = true;
                    break;
                }
            }
        }
        if (elemFound != true)
        {
            form1.elements[j].disabled = true;
        }
    }
    form1.elements[m-1].value = "Apply";
    form1.submit();
}
function submitDeleteForm(form1,form2,rowElements)
{
   var n=form2.elements.length;
   var m=form1.elements.length;
   var k=1;
   var j=0;
   
   for (i=0;i<n;i=i+rowElements)
   {
      k=1;
      j=0;
      if (form2.elements[i].checked == true)
      {
         j=i+1;
         while ((k<(m-1)) && (j<n))
         {
               form1.elements[k].value = form2.elements[j].value;
               k=k+1;
               j=j+1;
         }
         break;
      }
   } 
   form1.elements[m-1].value = "Delete";
   form1.submit();  
   return;
}
function ReloadfromISS(pagename,param)
{
    location.href=pagename+"?Gambit="+param;
}
function combineIPAddrFields(ipfield,field1,field2,field3,field4)
{
    ipfield.value =  parseInt(field1.value,10)+"."+ parseInt(field2.value,10)+"."+parseInt(field3.value,10)+"."+parseInt(field4.value,10);
    return (checkForIP(ipfield));
}


/*********************************************************/
/* function to check whether the input is valid          */
/* IP Mask address                                            */
/*********************************************************/

function IpMaskValidation(field)
{

    var error=0;
    var field1,field2,field3,field4;
    var ipPattern = /^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/;
    var ipArray = field.value.match(ipPattern);
    
    if (ipArray == null)
    {
       alert ("Invalid mask ");
       field.value = "";
       return;
    }

    field1 = ipArray[1];
    field2 = ipArray[2];
    field3 = ipArray[3];
    field4 = ipArray[4];


    if( field1 == 0 || field1 ==128 || field1 ==192 || field1 == 224 || field1 == 240 || field1 == 248 || field1 == 252 || field1== 254 || field1 == 255)
    {
        error=0;
    }
    else
    {
        alert ("ERROR : "+field.value+" is not a valid Subnet Mask field");
        field.value="";
        return false;
    }


    if(field2 == 0 || field2 == 128 || field2==192 || field2 ==224 || field2 ==240 || field2 ==248 || field2==252 || field2==254 || field2== 255)
    {
        error=0;
    }
    else
    {
        alert ("ERROR : "+field.value+" is not a valid Subnet Mask field");
        field.value="";
        return false;
    }

    if(field3==0||  field3==128 || field3==192 || field3 ==224 || field3 ==240 || field3 ==248 || field3==252 || field3==254 || field3== 255)
    {
        error=0;
    } 
    else
    {
        alert ("ERROR : "+field.value+" is not a valid Subnet Mask field");
        field.value="";
        return false;
    }

    if(field4==0||  field4==128 || field4==192 || field4 ==224 || field4 ==240 || field4 ==248 || field4==252 || field4 ==255)
    {
        error=0;
    }
    else
    {
        alert ("ERROR : "+field.value+" is not a valid Subnet Mask field");
        field.value="";
        return false;
    }


    if(field1!=255)
    {
        if(field2==0 && field3==0 && field4==0)
            error=0;
        else 
        {
            alert ("Error : Invalid Subnet Mask ");
            return false;
        }
    }

    if(field2!=255)
    {
        if(field3==0 && field4==0)
        {
            error=0;
        }
        else
        {
            alert ("Error : Invalid Subnet Mask ");
            return false;
        }
    }

    if(field3!=255)
    {
        if(field4==0)
        {
            error=0;
        }
        else
        {
            alert ("Error : Invalid Subnet Mask ");
            return false;
        }
    }
    /* Condition 3 *****************************************************/

    if(field2!=0)
    {
        if(field1==255)
            error=0;
        else
        {
            alert ("Error : Invalid Subnet Mask ");
            return false;
        }
    }

    if(field3!=0)
    {
        if(field2==255 && field1==255)
            error=0;
        else
        {
            alert ("Error : Invalid Subnet Mask ");
            return false;
        }
    }

    if(field4!=0)
    {
        if(field3==255 && field2==255 && field1==255)
            error =0;
        else
        {
            alert ("Error : Invalid Subnet Mask ");
            return false;
        }
    }  
    return true;
}

function checkForIPvx(formentry, element,field)
{
   var type = formentry.elements[element].value;
   var r =0;
   if(field.value == "")
      return true;
   
   if(type == 1)
   {
    checkForIP(field);
   }
   else if(type == 2)
   {
     r = validateIPv6(field);
     if (r == false)
     {
      return false;
     }
     else
     {
      return true;
     }
   }
   else
   {
    field.value= "";
    return false;
   }
}
function strpos (haystack, needle, offset)
{
    var i = (haystack+'').indexOf(needle, (offset || 0));
    return i === -1 ? false : i;
}
function strrpos(haystack,needle,offset)
{
  var i=-1;

  if(offset)
  {
     i= (haystack+'').slice(offset).lastIndexOf(needle);
     if(1!=-1)
     {
       i+=offset;
     }
  }
  else
  {
     i = (haystack+'').lastIndexOf(needle);
  }
   return (i >=0 ? i:false);

}
function substr_count (haystack, needle, offset, length)
{
   var pos = 0, cnt = 0;
     haystack += '';
    needle += '';
    if (isNaN(offset)) {offset = 0;}
    if (isNaN(length)) {length = 0;}
    offset--;
    while ((offset = haystack.indexOf(needle, offset+1)) != -1){
        if (length > 0 && (offset+needle.length) > length){
            return false;
        } else{            cnt++;
        }
    }

    return cnt;
}
function validateIPv6(ip)
{
     var lastcolon;
     var stringlen= ip.value.length;
     var temp=ip.value;
     var ipv6pattern = /^(?:[a-f0-9]{1,4}:){7}[a-f0-9]{1,4}$/i;
     var match_ipv6 = /^(?::|(?:[a-f0-9]{1,4}:)+):(?:(?:[a-f0-9]{1,4}:)*[a-f0-9]{1,4})?$/i;
     var errorString = "";
     var match;

     if(ip.value == "")
     {  
	     return true;
     }
    if (strpos(ip.value, '.'))
    {
       lastcolon = strrpos(ip.value, ':');
       ip.value = ip.value.substr(lastcolon + 1);
       /*checkForIP(ip); */
if (!(lastcolon && checkForIP(ip)))
       {
         ip.value="";
         alert(" The ipv6 address is not valid");
         return false;
       }
       ip.value = temp;
       ip.value = ip.value.substr(0,lastcolon) + ':0:0';
    }
 if(strpos(ip.value,'::')=== false)
    {
       match=ip.value.match(ipv6pattern);
       if(match == null)
       {
          errorString = "ERROR: " + ip.value + " is not a valid ipv6 address";
          alert(errorString);
          ip.value="";
           return false;
       }
       else
       {
        ip.value=temp;
        /*  valid  IPv6 address */
        return true;
       }
    }
    if(substr_count(ip.value,':')< 8)
    {
       match = ip.value.match(match_ipv6);

       if(match == null)
       {
        errorString = "ERROR: " + ip.value + " is not a valid ipv6 address";
        alert(errorString);
        ip.value="";
        return false;
       }
       else
       {
        ip.value=temp;
        /*  valid  IPv6 address */
        return true;
       }

    }
    alert("invalid ipv6 address format");
    ip.value="";
    return false;
}
function checkForPrefix(formentry, element, field)
{
   var inettype  = formentry.elements[element].value;
   
   if(field.value == "" )
      return true;

   if(inettype == 1)
   {
       if( (field.value != "") &&
         (checkForNumeric(field)) &&
         (field.value >= 0 ) && (field.value <= 32) )
       {
          return true;
       }
       else
       {
          alert("ERROR: Input should be between 0 and 32");
          field.value = "";
          return false;
       }
   }
   else if(inettype == 2)
   {

       if( (field.value != "") &&
         (checkForNumeric(field)) &&
         (field.value >= 0 ) && (field.value <= 128) )

       {
           return true;
       }
       else
       {

          alert("ERROR: Input should be between 0 and 128");
          field.value = "";
          return false;
       }
   }
   else
   {
      field.value="";
      return false;
   }
}

/**********************************************************************/
/* function to check whether the input is of interface local scope      */
/**********************************************************************/

function strncmp (str1, str2, len)
{
    var s1 = (str1 + '').substr(0, len);
    var s2 = (str2 + '').substr(0, len);

   if (s1 == s2)
      return true;

   return false;
/* return ((s1 == s2) ? 0 : ((s1 > s2) ? true : false));*/
}

function checkForZoneIndex(field,n,len)
{
    var zoneindex = (field + '').substr(n,len); 

     if (zoneindex == "")
        return false;

     if ((zoneindex < 1) || (zoneindex > 65535))
         return false;

    return true;
}


function checkForZoneNameandIndex(scope,field)
{
   var valid;
   var obj = field.value;
   var len = field.value.length;
    var n;

   
   if (field.value == "")
       return true;
    if (scope === 1)
    {
        valid = "interfacelocal";
    }
    else if (scope === 2)
    {
        valid = "linklocal";
    }
    else if (scope === 3)
    { 
        valid = "subnetlocal";
    }
    else if (scope === 4)
    {
        valid = "adminlocal";
    }
    else if (scope === 5)
    {
        valid = "sitelocal";
    }
    else if (scope === 6)
    {
        valid = "scope6";
    }
    else if (scope === 7)
    {
        valid = "scope7";
    }
    else if (scope === 8)
    {
        valid = "orglocal";
    }
    else if (scope === 9)
    {
        valid = "scope9";
    }
    else if (scope === 10)
    {
        valid = "scopeA";
    }
    else if (scope === 11)
    {
        valid = "scopeB";
    }
    else if (scope === 12)
    {
        valid = "scopeC";
    }
    else if (scope === 13)
    {
        valid = "scopeD";
    }
    else if (scope === 14)
    {
        valid = "global";
    }

    n = valid.length;

    if(strncmp(valid,obj,n)=== false)
    {
        alert(" Invalid Zone Name, Valid name is : " + valid +"1 to 65535" );
        field.value="";
        return false;
    }
    if(len == n)
    {
        alert("Zone index is not been provided");
        field.value="";
        return false;
    }
    else
 {
        if (checkForZoneIndex(obj,n,len) === false)
    {
            alert("Invalid Zone index value hsa been provided : Valid value is"+ valid+"1 to 65535");
            field.value="";
        return false;
    }
 }
    alert("validation successful");
    return true;

}
/*********************************************************/
/* function to check whether the input is valid          */
/* Oid for RMON Stats and History collection             */
/*********************************************************/

function RmoncheckForValidOID(field)
{
   var str ,str1, n;
   var ethOid = '1.3.6.1.2.1.2.2.1.1';
   var vlnOid = '1.3.6.1.2.1.17.7.1.4.2.1.2';

   var valid = "0123456789.";
   var temp; 
   var m = field.value.length;

   for (var i=0; i<m; i++) 
   { 
      temp = "" + field.value.substring(i, i+1); 
      if (valid.indexOf(temp) == "-1") 
      {
         alert("ERROR: Only numbers are accepted!" + 
                  "eg: 1.3.6.1.12.1.1.0"); 
         field.value = "";
         return false;
      }
   } 

   n = field.value.lastIndexOf(".");
   str = field.value.substring(0,n);
   str1 = field.value.substring(n+1);
   if( (ethOid != str && vlnOid != str) || str1 == 0 )
   {
       alert("ERROR : Must be " + ethOid + ".Valid-Ifindex or " + vlnOid + ".Valid-VlanId");
       field.value = "";
       return false;
   }
   else
       return true;
} 
