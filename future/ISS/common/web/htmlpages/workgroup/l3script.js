function enableOspfAreaSpecificFields1()
{
    
    if(document.forms[0].elements[5].options[0].selected == true)
    {
        document.forms[0].elements[7].options[1].selected = true;
        document.forms[0].elements[10].options[0].selected = true;
        for(i=7; i<=12; i++)
        {
            document.forms[0].elements[i].disabled=true;
        }
    }
    else if(document.forms[0].elements[5].options[1].selected == true) 
    {
        document.forms[0].elements[10].options[0].selected = true;
        document.forms[0].elements[7].disabled=false;
        for(i=9; i<=12; i++)
        {
            document.forms[0].elements[i].disabled=true;
        }
    }
    else  
    {
        for(i=7; i<=12; i++)
        {
            document.forms[0].elements[i].disabled=false;
        }
        if (document.forms[0].elements[7].value == 1)
        {
            document.forms[0].elements[10].options[0].selected = true;
            for(i=9; i<=12; i++)
            {
                document.forms[0].elements[i].disabled=true;
            }
        } 
        else
        {
            document.forms[0].elements[10].options[1].selected = true;
        }
        if (document.forms[0].elements[12].value != 0)
        {
            alert ("ERROR:Invalid TOS value");
            document.forms[0].elements[12].value = 0;
            return;
        }
    }
}
function enableOspfAreaSpecificFields2()
{
    var n=document.forms[2].elements.length;
    var i=0;
    var rowElements = 14;
    for (i=0;i<n;i=i+rowElements)
    {
        if(document.forms[2].elements[i+3].options[0].selected == true)
        {
            document.forms[2].elements[i+5].options[1].selected = true;
            document.forms[2].elements[i+8].options[0].selected = true;
            for(j=5; j<=10; j++)
            {
                document.forms[2].elements[i+j].disabled=true;
            }
        }
        else if(document.forms[2].elements[i+3].options[1].selected == true)
        {
            document.forms[2].elements[i+8].options[0].selected = true;
            document.forms[2].elements[i+5].disabled=false;
            for (j=6; j<=10; j++)
            {
                document.forms[2].elements[i+j].disabled=true;
            }
        }
        else
        {
            for(j=5; j<=10; j++)
            {
                document.forms[2].elements[i+j].disabled=false;
            }
	    if (document.forms[2].elements[i+5].value == 1)
	    {
		    document.forms[2].elements[i+8].options[0].selected = true;
		    for(j=7; j<=10; j++)
		    {
			    document.forms[2].elements[i+j].disabled=true;
		    }
	    }
	    else
	    {
		    if(document.forms[2].elements[i+8].options[0].selected == true)
		    {
			    alert ("ERROR:MetricType cannot be ospfMetric for NSSA when sending area summary routes");
			    document.forms[2].elements[i+8].options[1].selected = true;
			    return false;
		    }
	    }
            if (document.forms[2].elements[i+10].value != 0)
            {
                alert ("ERROR:Invalid TOS value");
                document.forms[2].elements[i+10].value = 0;
                return false;
            }
        }
    }
    return true;
}
function frameRrdBgpRequest(startPos, endPos, enField, disField)
{
  enableVal = 0;
  disableVal = 0;
   
   if(document.forms[0].elements[2].value != 1)
   {
      /* RRD disable is chosen, send as last input */
      n = document.forms[0].elements.length;
      document.forms[0].elements[n-2].disabled = false;
      document.forms[0].elements[1].disabled = true;
   }

  for(i=startPos; i<=endPos; i++)
  {
      obj = document.forms[0].elements[i];
      if(obj.checked == true)
         enableVal += parseInt(obj.value);
      else
         disableVal += parseInt(obj.value);
      
      obj.disabled = true;
  }
  /* If the value is ZERO, dont send as request */
  if(enableVal == 0)
      document.forms[0].elements[enField].disabled = true;
  else
      document.forms[0].elements[enField].value = enableVal;
      
  if(disableVal == 0)
      document.forms[0].elements[disField].disabled = true;
  else
      document.forms[0].elements[disField].value = disableVal;
}
function showRrdOptions()
{
   n = document.forms[0].elements.length;
   obj = document.forms[0].elements[2];
   temp = false;

   if(obj.value != 1)
      temp = true;
   for(i=2; i<(n-1); i++)
   {
      if(document.forms[0].elements[i].type != "hidden")
              document.forms[0].elements[i].disabled = temp;
   }
}
function ospfIfaceEnaElts()
{
    var AuthType = document.forms[0].elements[10];
    var Md5Key = document.forms[0].elements[11];
    var AuthKey = document.forms[0].elements[12];
    var AuthType1; 
    var Md5Key1;
    var AuthKey1;
    var i;
    var no_of_elements_form2 = document.forms[2].elements.length;
 
   if (AuthType.value == 0)
   {
	   Md5Key.disabled = true;
	   AuthKey.disabled = true;
   }
   else if (AuthType.value == 1)
   {
	   Md5Key.disabled = true;
	   AuthKey.disabled = false;
   }
   else
   {
	   Md5Key.disabled = false;
	   AuthKey.disabled = false;
   }
   for (i = 6;i<no_of_elements_form2;i+=22)
   {
	AuthType1 = document.forms[2].elements[i];
        Md5Key1 = document.forms[2].elements[i+2];	
	AuthKey1 = document.forms[2].elements[i+3];
	if (AuthType1.value == 0)
	{
		Md5Key1.value="";
		AuthKey1.value="";
		Md5Key1.disabled = true;
		AuthKey1.disabled = true;
	}
	else if (AuthType1.value == 1)
	{
		Md5Key1.value = "";
		Md5Key1.disabled = true;
		AuthKey1.disabled = false;
	}
	else
	{
		Md5Key1.disabled = false;
		AuthKey1.disabled = false;
	}
   }

}
function ospfVIfaceEnaElts()
{
    var AuthType = document.forms[0].elements[6];
    var Md5Key = document.forms[0].elements[7];
    var AuthKey = document.forms[0].elements[8];

    if (AuthType.options[0].selected == true)
    {
        Md5Key.disabled = true;
        AuthKey.disabled = true;
    }

    if (AuthType.options[1].selected == true)
    {
        Md5Key.disabled = true;
        AuthKey.disabled = false;
    }

    if (AuthType.options[2].selected == true)
    {
        Md5Key.disabled = false;
        AuthKey.disabled = false;
    }
}
function checkPreferenceValue(inputForm)
{                                                                                      if (false == checkForRange(0x1, 0xFF, inputForm.elements[2]))
    {
        return false;
    }
    if ("" == inputForm.elements[2].value)
    {
        alert("ERROR: Empty distance value is not allowed");
        return false;
    }
    return true;
}
function checkRMapFilterCreatingForm(inputForm)
{
    if (inputForm.elements[3].value == "")
    {
        alert("ERROR: Please fill out route-map name");
        return false;
    }
    if (false == checkForAlphaNumeric(inputForm.elements[3]))
    {
        return false;
    }

    if ((0 >= inputForm.elements[4].value) || ( 3 < inputForm.elements[4].value))
    {
        alert("ERROR: Please chose filter type");
        return false;
    }
    if (("1" == inputForm.elements[4].value) && ( "6" != inputForm.elements[6].value))
    {
        /*if Distance filter and not "no distance" command value should present*/
        if (false == checkForRange(0x1, 0xFF, inputForm.elements[4]))
        {
            return false;
        }
        if ("" == inputForm.elements[5].value)
        {
            alert("ERROR: Empty distance value is not allowed");
            return false;
        }
    }
    else
    {
        inputForm.elements[5].value = "0";
    }

    return true;
}
function sendRMapFilterModifyOrDelete(form1, form2, rowElements, rowstatusFlag)
{
    var n = form2.elements.length;
    var rowFound = false;
    for (i = 0; i < n; i = i + rowElements)
    {
        if (form2.elements[i].checked == true)
        {
            rowFound = true;
            form1.elements[1].value=form2.elements[i+1].value;
            form1.elements[3].value=form2.elements[i+2].value;
            form1.elements[4].value=form2.elements[i+3].value;
            form1.elements[5].value=form2.elements[i+5].value;
            form1.elements[6].value=form2.elements[i+6].value;
            if (rowstatusFlag == 2)
            {
                form1.elements[6].value = "6";
            }
            break;
        }
     }
     if (rowFound == true)
     {
         if (true == checkRMapFilterCreatingForm(form1))
         {
            form1.submit();
         }
     }
}
