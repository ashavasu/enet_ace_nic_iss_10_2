function frameDFSRequest()
{
   var globalStatus = document.forms[0].elements[1];
   var State = document.forms[0].elements[3];
   
   if(globalStatus.value == "2")
   {
      /* send only DISBABLE request, disable rest */
      State.value = "2";
      State.disabled = true;
   }
   return true;     
}
function CheckStatus()
{
   var ControlStatus = document.forms[0].elements[1];
   var State = document.forms[0].elements[3];
   
   if(ControlStatus.value == "2")
   {
      /* send only DISBABLE request, disable rest */
      State.value = "2";
      State.disabled = true;
   }
   else
   {
      State.disabled = false;
   }
   return true;     
}
function sendL2FilterForm(form1, form2,rowElements)
{
   var n=form2.elements.length;
   var m=form1.elements.length;
   var k=2;
   var j=0;
   var i=0;
   var rowFound = false;
   var temp = new Object(rowElements);

   /* copy the request to temp object */
   while((i < n) && (j < rowElements))
   {
      /* find which ROW is selected */
      if ((!rowFound) && (form2.elements[i].checked == true))
      {
         rowFound = true;
         i++;
      }
      if ((rowFound == true) && (form2.elements[i].disabled != true))
      {
         temp[j] = form2.elements[i];
         j++;
      }
      i++;
   };
   if(rowFound == false)
      return;
   /* copy the request to input form */
   for(i=0; i<rowElements; i++)
   {
      param = temp[i];
      form1.elements[k].value = param.value;
      k = k+1;
   }
   form1.ACTION.value="Delete";
   if(rowFound == true) /* To avoid submit, when table is empty */
      form1.submit();
}
function checkForMACFlt(field)
{
   var errorString = "";
   var macPattern = /^(\w{2})\:(\w{2})\:(\w{2})\:(\w{2})\:(\w{2})\:(\w{2})$/;
   var thisSegment;
   var temp = "";
   var valid = "1234567890abcdef";
   var macArray = field.value.match(macPattern);

    if( field.value == "" )
    {
      field.value = "00:00:00:00:00:00";
      return true;
    }  

   if (macArray == null)
      errorString = "ERROR: Enter valid MAC address";
   else 
   {
      /* check for unicast MAC address */
      if((parseInt(macArray[1], 16) & 0x01) == 1)
      {
         alert("ERROR: Entered MAC is not UNICAST type");
         field.value = "";
         return false;
      }
      for (i = 1; i < 7; i++)
      {
         thisSegment = macArray[i];
         for(j = 0; j < 2; j++)
         {
            temp = thisSegment.charAt(j).toLowerCase();
            if(valid.indexOf(temp) == -1)
            {
               errorString = "ERROR: Enter valid MAC address";
               break;
            }
         }
         if (errorString != "")
            break;
      }
   }
   if (errorString == "")
      return true;
   else
   {
      alert (errorString + "\n eg: 0a:11:1e:09:54:76");
      field.value = "";
      return false;
   }
}
function checkAndSubmitMacFlt(inputForm)
{
	if(inputForm.elements[2].value == "")
	{
		alert("ERROR: Enter Filter Number");
		inputForm.elements[2].select();
		return false;
	}
	if(inputForm.elements[3].value == "")
		inputForm.elements[3].disabled = true;
	if(inputForm.elements[4].value == "")
		inputForm.elements[4].disabled = true;
	if(inputForm.elements[7].value == "")
		inputForm.elements[7].value = 1;
	if(inputForm.elements[8].value == "0")
		inputForm.elements[8].disabled = true;
	if(inputForm.elements[10].value == "")
		inputForm.elements[10].disabled = true;
	if(inputForm.elements[11].value == "")
		inputForm.elements[11].disabled = true;
	if(inputForm.elements[12].value == "")
		inputForm.elements[12].disabled = true;

   return true;
}
function submitMacFltDelForm(form1,form2)
{
alert(" entering submitMacFltDelForm\n");
   var n=form1.elements.length;
   alert(n);
   var j=0;
   for (i=0;i<n; i+=18)
   {
      if (form1.elements[i].checked == true)
      {
          form2.elements[1].value = form1.elements[i+1].value;
          form2.elements[14].value = "DELETE";
          alert(form2.elements[14].name);
          form2.submit();  
          return true;
      }
	}
	return false;
}
function enableExtListOptions(val, number)
{
    var k = 0;
    form = document.forms[val];

    form.elements[11].disabled = false;
    form.elements[14].disabled = false;
    form.elements[16].disabled = false;
}
function checkAndSubmitIPStdFlt(inputForm)
{
	var i = 0;

	if(inputForm.elements[2].value == "")
	{
		alert("ERROR: Enter Filter Number");
		inputForm.elements[2].select();
		return false;
	}

	if(inputForm.elements[5].value == "") 
   {   
		/*alert("ERROR: Enter Source IP Address");
		inputForm.elements[5].select();
		return false;
        */
        inputForm.elements[5].value = "0.0.0.0";

	}
	for ( i = 6; i <= 10; i++)
	{
		if(inputForm.elements[i].value == "")
			inputForm.elements[i].disabled = true;
	}

	return true;
}
function submitIPStdFltDelForm(form1,form2)
{
alert (" Entering submitIPStdFltDelForm \n");
   var n=form1.elements.length;
   alert(n);
   var j=0;
   for (i=0;i<n; i+=13)
   {
       if (form1.elements[i].checked == true)
       {
           form2.elements[1].value = form1.elements[i+1].value;
           form2.elements[11].value = "DELETE";
           alert (form2.elements[11].name);
           form2.submit();  
           return;
       }
   }
   return false;
}
function submitIPStdFiltForm(form1, form2, rowElements)
{
    var n=form2.elements.length;
    var m=form1.elements.length;
    var k=1;
    var j=0;
    var i=0;
    var rowFound = false;
    var temp = new Object(rowElements);

    /* copy the request to temp object */
    while(i < n)
    {
        /* find which ROW is selected */
        if (form2.elements[i].checked == true)
        {
            rowFound = true;
            break;
        }
        i+=rowElements;
    };
    if (rowFound == false)
        return false;
    /* copy the request to input form */
    j = i + 1;
    while(j < i + rowElements)
    {
        /* Skip Action selection input and direction selection input*/
        if (j == 3 || j == 12) 
        {
            j++;
        }
        if(form2.elements[j].disabled != true)
        {
           if (form2.elements[j].value != "")
           {
            form1.elements[k].value = form2.elements[j].value;
           }
           else
           {
              form1.elements[k].disabled = true;
           }
            k++;
        }
        j++; 
    }
    while(j < i + rowElements)
    {
        form1.elements[k].disabled = true;
        k++;j++;
    }
    form1.elements[11].value = "Apply";
    form1.submit();
}
function submitFltDelForm(form1,form2)
{
   var m=form1.elements.length;
   var n=form2.elements.length;
   var j=0;
   for (i=0;i<n; i++)
   {
       if (form2.elements[i].checked == true)
       {
           form1.elements[1].value = form2.elements[i+1].value;
           form1.elements[m-1].value = "Delete";
           form1.submit();  
           return;
       }
   }
   return false;
}
function sendMacFilterForm(form1, form2, rowElements)
{
    var n=form2.elements.length;
    var m=form1.elements.length;
    var k=1;
    var j=0;
    var i=0;
    var rowFound = false;
    var temp = new Object(rowElements);

    /* copy the request to temp object */
    while((i < n) && (j < rowElements))
    {
        /* find which ROW is selected */
        if (form2.elements[i].checked == true)
        {
            rowFound = true;
            i++;
        }
        if( (rowFound == true) && (form2.elements[i].disabled != true))
        {
            temp[j] = form2.elements[i];
            j++;
        }
        i++;
    };
    if (rowFound == false)
        return false;
    /* copy the request to input form */
    for(i=0; i<rowElements; i++)
    {
        param = temp[i];
        /* check whether input value is present */
        if( param.value == "" )
        {
            /* check whether it is mandatory param */
            if(param.name.substring(0, 4) == "reqd")
            {
                /* give a alert and stop submit */
                alert("ERROR: Enter mandatory inputs");
                param.select();
                return false;
            }
            else
            {
                /* optional param, disable this from input request */
                form1.elements[k].disabled = true;
                k = k + 1;
            }
        }
        else
        {
            form1.elements[k].value = param.value;
            k = k+1;
        }
    }
    form1.elements[k].value = "Apply";
    if(rowFound == true) /* To avoid submit, when table is empty */
        form1.submit();
}
