function checkForMac(field)
{
   var errorString = "";
   var macPattern = /^(\w{2})\:(\w{2})\:(\w{2})\:(\w{2})\:(\w{2})\:(\w{2})$/;
   var thisSegment;
   var temp = "";
   var valid = "1234567890abcdef";
   
   if(field.value == "")
     return true;
   macArray = field.value.match(macPattern);

   if (macArray == null)
      errorString = "ERROR: " +'field.value' + "is not a valid MAC Address" + "\n Eg: 15:11:1e:09:54:76";
   else 
   {
      if((macArray[1] == "00") && (macArray[2] == "00") && 
         (macArray[3] == "00") && (macArray[4] == "00") &&
         (macArray[5] == "00") && (macArray[6] == "00"))
      {
         alert("ERROR: Enter valid MAC address" +
                  "\n eg: 15:11:1e:09:54:76");
         field.value = "";
         return false;
      }
      
      for (i = 1; i < 7; i++)
      {
         thisSegment = macArray[i];
         for(j = 0; j < 2; j++)
         {
            temp = thisSegment.charAt(j);
            if(valid.indexOf(temp) == -1)
            {
               errorString = "ERROR: " +'field.value' + "is not a valid input";
               break;
            }
         }
         if (errorString != "")
            break;
      }
   }
   if (errorString == "")
      return true;
   else
   {
      alert (errorString);
      field.value = "";
      return false;
   }
}
function checkForMcastMac(field)
{
   var errorString = "";
   var macPattern = /^(\w{2})\:(\w{2})\:(\w{2})\:(\w{2})\:(\w{2})\:(\w{2})$/;
   var thisSegment;
   var temp = "";
   var valid = "1234567890abcdef";
   
   if(field.value == "")
     return true;
   macArray = field.value.match(macPattern);

   if (macArray == null)
      errorString = "ERROR: " +'field.value' + "is not a valid Multicast MAC Address" + "\n Eg: 15:11:1e:09:54:76";
   else 
   {
      /* check for unicast MAC address */
      if((parseInt(macArray[1], 16) & 0x01) == 0)
      {
         alert("ERROR: Entered MAC is not MULTICAST type");
         field.value = "";
         return false;
      }
      if((macArray[1] == "00") && (macArray[2] == "00") && 
         (macArray[3] == "00") && (macArray[4] == "00") &&
         (macArray[5] == "00") && (macArray[6] == "00"))
      {
         alert("ERROR: Enter valid MAC address" +
                  "\n eg: 15:11:1e:09:54:76");
         field.value = "";
         return false;
      }
      
      for (i = 1; i < 7; i++)
      {
         thisSegment = macArray[i];
         for(j = 0; j < 2; j++)
         {
            temp = thisSegment.charAt(j);
            if(valid.indexOf(temp) == -1)
            {
               errorString = "ERROR: " +'field.value' + "is not a valid input";
               break;
            }
         }
         if (errorString != "")
            break;
      }
   }
   if (errorString == "")
      return true;
   else
   {
      alert (errorString);
      field.value = "";
      return false;
   }
}
function sendPNACRequest(form1, form2,rowElements)
{
   var n=form2.elements.length;
   var m=form1.elements.length;
   var k=2; /* no row status for this table */
   var j=0;

   for (i=0; i<n; i++)
   {
       /* find which ROW is selected */
       if (form2.elements[i].checked == true)
       {
           j = i + 1;
           while((k<m) && (j<n))
           {
               if(form2.elements[j].disabled != true)
               {
                   form1.elements[k].value = form2.elements[j].value;
                   k++;
               }
               j++;
           }
       }
   }
   form1.submit();
}
function submitVlanFiltForm(form1,form2,rowElements)
{
   var n=form2.elements.length;
   var m=form1.elements.length;
   var k=2;
   var j=0;
 
   var rowFound = false;

   for (i=0;i<n;i=i+rowElements)
   {
      if (form2.elements[i].checked == true)
      {
         rowFound = true;
         j=i+1;
         while ( k < m-4)
         {
            if(form2.elements[j].disabled != true)
            {
              form1.elements[k].value = form2.elements[j].value;
              k++;
            }
            j = j + 1;
         }
         break;
      }
   }
}
function submitVlanFltDelForm (form1,form2,rowElement,ActElement)
{
   var n=form2.elements.length;
   var m=form1.elements.length;
   var i=0;
   var j=0;
   var rowdel=false;

   for (i=0;i<n;i=i+rowElement)
   {
      if (form2.elements[i].checked == true)
      {
         rowdel = true;
         form1.elements[2].value = form2.elements[i+1].value;
         form1.elements[3].value = form2.elements[i+2].value;
         form1.elements[ActElement].value = "DELETE";

         for ( j=4; j<(m-4); j++)
         {
            form1.elements[j].disabled = true;
         }
         break;
      }
   }
   if (rowdel == true)
   form1.submit();  
}
function GetIpField(ipField, ipField1, ipField2, ipField3, ipField4)
{
    ipField.value = ipField1.value+"."+ipField2.value+".";
    ipField.value = ipField.value + ipField3.value+"."+ipField4.value;
}
function setNextField(field)
{
    if(field.value == "IN")
    {
        document.forms[0].elements[15].disabled = true;
        document.forms[0].elements[16].disabled = true;
        document.forms[0].elements[17].disabled = true;
        document.forms[0].elements[18].disabled = true;
        document.forms[0].elements[19].disabled = true;
        document.forms[0].elements[13].disabled = false;
    }
    else if(field.value == "OUT")
    {
        document.forms[0].elements[13].disabled = true;
        document.forms[0].elements[13].value = "-";
        document.forms[0].elements[15].disabled = false;
        document.forms[0].elements[16].disabled = false;
        document.forms[0].elements[17].disabled = false;
        document.forms[0].elements[18].disabled = false;
        document.forms[0].elements[19].disabled = false;
        document.forms[0].elements[16].select();
    }

    if(field.value == "OTHER")
    {
       document.forms[0].elements[4].disabled = false ;
       document.forms[0].elements[4].select();
    }
    else if(field.value == "EXPLICIT-NULL")
    {
       document.forms[0].elements[4].value = "0";
       document.forms[0].elements[4].disabled = true;
    }
    else if(field.value == "IMPLICIT-NULL")
    {
       document.forms[0].elements[4].value = "3";
       document.forms[0].elements[4].disabled = true;
    }

    if(field.value == "HEAD")
    {
        document.forms[0].elements[14].disabled = true;
        document.forms[0].elements[15].disabled = true;
        document.forms[0].elements[16].disabled = false;
        document.forms[0].elements[18].disabled = false;
        document.forms[0].elements[19].disabled = false;
        document.forms[0].elements[20].disabled = false;
        document.forms[0].elements[21].disabled = false;
    }
    else if(field.value == "TRANSIT")
    {
        document.forms[0].elements[14].disabled = false;
        document.forms[0].elements[15].disabled = false;
        document.forms[0].elements[16].disabled = false;
        document.forms[0].elements[18].disabled = false;
        document.forms[0].elements[19].disabled = false;
        document.forms[0].elements[20].disabled = false;
        document.forms[0].elements[21].disabled = false;
    }
    else if(field.value == "TAIL")
    {
        document.forms[0].elements[14].disabled = false;
        document.forms[0].elements[15].disabled = false;
        document.forms[0].elements[16].disabled = true;
        document.forms[0].elements[18].disabled = true;
        document.forms[0].elements[19].disabled = true;
        document.forms[0].elements[20].disabled = true;
        document.forms[0].elements[21].disabled = true;
    }

    if (field.value == "VPWS")
    {
        document.forms[0].elements[9].disabled = true;
    }
    else if (field.value == "VPLS")
    {
        document.forms[0].elements[9].disabled = false;
    }

    if (field.value == "TE")
    {
        document.forms[0].elements[11].disabled = false;
    }
    else if (field.value == "VCONLY")
    {
        document.forms[0].elements[11].disabled = true;
    }
    else if (field.value == "NONTE")
    {
        document.forms[0].elements[11].disabled = true;
    }

    if (field.value == "VLAN")
    {
        document.forms[0].elements[2].disabled = false;
        document.forms[0].elements[3].disabled = true;
    }
    else if (field.value == "PORT")
    {
        document.forms[0].elements[2].disabled = true;
        document.forms[0].elements[3].disabled = false;
    }
}
