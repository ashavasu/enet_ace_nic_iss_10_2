/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: msrtool.c,v 1.11 2017/12/07 09:32:24 siva Exp $
 *
 * Description: This file has routine to generate a mibsave.h
 ************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>

#include "main.h"
#include "msr.h"
#include "eoid.h"

#define MAX_OID_LEN     256
#define MAX_NAME_LEN    256

#define MAX_ENTERPRIE_OID_SIZE 8
#define OID_LENGTH_BEFORE_ENT_ID 12    /*strlen (1.3.6.1.4.1.) */

#define SCALAR_OBJ      1
#define TABLE_OBJ       2

UINT1               au1OldEoid[MAX_OID_LEN] = "1.3.6.1.4.1.";

INT1
MsrScan ()
{
    FILE               *fpout;
    UINT1               au1NewFileName[] = "mibsave.h";
    UINT1               au1Oid[MAX_OID_LEN];
    UINT4               u4Count = 0;
    UINT1               au1Name[MAX_NAME_LEN];
    UINT1               u1Type = 0;
    INT4                i4Index = 0;
    UINT4               u4Cnt = 0;
    UINT1               au1OldEntOid[MAX_ENTERPRIE_OID_SIZE];

    memset (au1OldEntOid, 0, MAX_ENTERPRIE_OID_SIZE);

    if ((fpout = fopen (au1NewFileName, "w")) == NULL)
    {
        printf ("\n Error! File mibsave.h open!");
        return FAILURE;
    }

    fprintf (fpout, "#ifndef _MIBSAVE_H\n");
    fprintf (fpout, "#define _MIBSAVE_H\n\n");
    fprintf (fpout,
             " /* This array contains the OIDs of all the mandatory MIB objects\n");
    fprintf (fpout,
             "  * to be saved. The objects will be saved in the order in which\n");
    fprintf (fpout, "  * they are stored in this array.\n  */\n\n");
    fprintf (fpout, " tSaveObject gaSaveArray[] =\n{\n");

    if (gaNameArray[0].pNameString != NULL)
    {
        for (u4Count = 0; gaNameArray[u4Count].pNameString != NULL; u4Count++)
        {
            strncpy (au1Name, gaNameArray[u4Count].pNameString, MAX_NAME_LEN);
            memset (au1Oid, 0, MAX_OID_LEN);
            GetOidStrFromName (au1Name, au1Oid);

            i4Index = GetIndexFromOidStr (au1Oid);

            /* Get the Old Enterprise Old from issConfigRestoreFileVersion object */
            if (u4Count == 0)
            {
                while ((au1Oid[OID_LENGTH_BEFORE_ENT_ID + u4Cnt]) != '.')
                {
                    au1OldEntOid[u4Cnt] =
                        au1Oid[OID_LENGTH_BEFORE_ENT_ID + u4Cnt];
                    u4Cnt++;
                }
                strncat (au1OldEoid, au1OldEntOid, strlen (au1OldEntOid));
            }

            if (i4Index == FAILURE)
            {
                printf ("Error! Invalid Object %s\n!", au1Name);
                fclose (fpout);
                return FAILURE;
            }
            else
            {
                if (MMIGetNodeType (i4Index) == TABLE)
                {
                    u1Type = TABLE_OBJ;
                }
                else
                {
                    u1Type = SCALAR_OBJ;
                }
            }
            if (u1Type == SCALAR_OBJ)
            {
                if (au1Oid[0] != '\0')
                {
                    fprintf (fpout, "\t{\n");
                    fprintf (fpout, "\t\t\"%s.0\",\t\t/* %s */\n",
                             au1Oid, au1Name);
                    if (gaNameArray[u4Count].pValFunc != NULL)
                    {
                        fprintf (fpout,
                                 "\t\tNULL,\n\t\t%s,\n\t\t%d,\n\t\t%d\n\t},\n",
                                 gaNameArray[u4Count].pValFunc, u1Type,
                                 gaNameArray[u4Count].i1PrimIndex);
                    }
                    else
                    {
                        fprintf (fpout,
                                 "\t\tNULL,\n\t\tNULL,\n\t\t%d,\n\t\t%d\n\t},\n",
                                 u1Type, gaNameArray[u4Count].i1PrimIndex);
                    }
                }
                else
                {
                    printf ("Error! Invalid Object %s\n!", au1Name);
                    fclose (fpout);
                    return FAILURE;
                }
            }
            else
            {
                if (au1Oid[0] != '\0')
                {
                    fprintf (fpout, "\t{\n");
                    fprintf (fpout, "\t\t\"%s\",\t\t/* %s */\n",
                             au1Oid, au1Name);
                    if (gaNameArray[u4Count].pRowStatus != NULL)
                    {
                        memset (au1Oid, 0, MAX_OID_LEN);
                        strncpy (au1Name, gaNameArray[u4Count].pRowStatus,
                                 MAX_NAME_LEN);
                        GetOidStrFromName (au1Name, au1Oid);

                        if (au1Oid[0] != '\0')
                        {
                            fprintf (fpout,
                                     "\t\t\"%s\",\t\t/* %s */\n",
                                     au1Oid, au1Name);
                            if (gaNameArray[u4Count].pValFunc != NULL)
                            {
                                fprintf (fpout,
                                         "\t\t%s,\n\t\t%d,\n\t\t%d\n\t},\n",
                                         gaNameArray[u4Count].pValFunc, u1Type,
                                         gaNameArray[u4Count].i1PrimIndex);
                            }
                            else
                            {
                                fprintf (fpout,
                                         "\t\tNULL,\n\t\t%d,\n\t\t%d\n\t},\n",
                                         u1Type,
                                         gaNameArray[u4Count].i1PrimIndex);
                            }
                        }
                        else
                        {
                            printf ("Error! Invalid Object %s!\n", au1Name);
                            fclose (fpout);
                            return FAILURE;
                        }
                    }
                    else
                    {
                        if (gaNameArray[u4Count].pValFunc != NULL)
                        {
                            fprintf (fpout,
                                     "\t\tNULL,\n\t\t%s,\n\t\t%d,\n\t\t%d\n\t},\n",
                                     gaNameArray[u4Count].pValFunc, u1Type,
                                     gaNameArray[u4Count].i1PrimIndex);
                        }
                        else
                        {
                            fprintf (fpout, "\t\tNULL,\n\t\tNULL,");
                            fprintf (fpout, "\n\t\t%d,\n\t\t%d\n\t},\n",
                                     u1Type, gaNameArray[u4Count].i1PrimIndex);
                        }
                    }
                }
                else
                {
                    printf ("Error! Invalid Object %s!\n", au1Name);
                    fclose (fpout);
                    return FAILURE;
                }
            }
        }
    }
    fprintf (fpout, "\t{\n\t\tNULL,\n\t\tNULL,\n\t\tNULL,\n\t\t0,\n\t\t0\n\t}");
    fprintf (fpout, "\n};\n\n");
    fprintf (fpout, "#endif /* _MIBSAVE_H */\n");
    fclose (fpout);
    return SUCCESS;
}

INT4
main (argc, argv)
     INT4                argc;
     INT1               *argv[];
{
    INT4                i4RetVal = 0;
    UINT1               au1DBPath[MAX_PATH_LEN];

    memset (au1DBPath, 0, sizeof (au1DBPath));
    if (argc != 2)
    {
        fprintf (stderr, "\nUsage: TOOLGEN <Directory of dbfiles> \n");
        exit (1);
    }
    if (STRLEN (argv[1]) >= MAX_PATH_LEN)
    {
        printf ("Path for tools directory is too long!!!\n");
        exit (1);
    }
    STRCPY (au1DBPath, argv[1]);

    i4RetVal = DBScan (&au1DBPath);
    if (i4RetVal == FAILURE)
    {
        exit (1);
    }

    if (pdb[0] == NULL)
    {
        printf ("Mib is Not Loaded \n");
        exit (1);
    }
    if (MsrScan () == SUCCESS)
    {
        printf (".......DONE\n");
        exit (0);
    }
    else
    {
        printf (".......Error\n");
    }
    return 0;
}
