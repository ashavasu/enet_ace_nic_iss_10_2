
/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: DBScan.c,v 1.10 2017/12/07 09:32:24 siva Exp $
 *
 * Description: This file has routine to generate a header file for DBfiles
 *
 ***********************************************************************/
#include "main.h"
#define   UNUSED_PARAM(x)   ((void)x)
#define   MAX_COUNT         2000

t_MMI_LINK_LIST    *main_list = 0;
t_MMI_LINK_LIST    *FirstNode = 0;

struct t_MMI_DB     MMI_List[] = {
    {"internet", "1.3.6.1", 0},
    {"mgmt", "1.3.6.1.2", 0},
    {"private", "1.3.6.1.4", 0},
    {"experimental", "1.3.6.1.6", 0},
    {"mib-2", "1.3.6.1.2.1", 0},
    {"transmission", "1.3.6.1.2.1.10", 0},
    {"enterprise", "1.3.6.1.4.1", 0},
    {"futuresoft", "1.3.6.1.4.1.2076", 0},
    {0, 0, 0}
};

INT4
DBScan (INT1 *path)
{
    INT4                i4Counter = 0;
    INT4                i4Check = 0;
    INT1               *pi1path;
    UINT2               u2Total;
    UINT4               u4Dummy = 0;

    UNUSED_PARAM (i4Counter);
    UNUSED_PARAM (i4Check);
    UNUSED_PARAM (u4Dummy);

    pi1path = MEM_MALLOC (MAX_PATH_LEN, INT1);
    if (pi1path == NULL)
    {
        printf ("\n Unable to allocate memory\n");
        return FAILURE;
    }
    strcpy (pi1path, "ls ");
    strcat (pi1path, path);
    strcat (pi1path, "/*.db >DBfiles");
    u2Total = (UINT2) system (pi1path);
    if (u2Total == 255)
    {
        printf ("\n Couldn't find HTML Files in this directory");
        exit (1);
    }

    MEM_FREE (pi1path);
    if (db_init () == FAILURE)
        return FAILURE;

    return SUCCESS;
}

INT1
db_init ()
{
    INT1               *pi1dbfiles[NO_OF_FILES + 1];

    if (ReadFileList (pi1dbfiles) == FAILURE)
        return FAILURE;
    if (Readdbfile (pi1dbfiles) == FAILURE)
        return FAILURE;
    RegisterDB ();
    return SUCCESS;
}

INT1
ReadFileList (INT1 **pi1List)
{
    UINT4               u4Count = 0;
    INT1                pi1FileName[FILE_NO_OF_CHAR_IN_A_LINE];
    INT4                i4Len;
    FILE               *fp;

    pi1List[0] = NULL;
    if ((fp = fopen (DB_FILES, "r")) == NULL)
    {
        printf ("\n Error dbfiles open");
        return FAILURE;
    }
    while ((fgets (pi1FileName, FILE_NO_OF_CHAR_IN_A_LINE, fp) != NULL) &&
           (u4Count <= NO_OF_FILES))
    {
        i4Len = strlen (pi1FileName);
        pi1FileName[i4Len - 1] = '\0';
        pi1List[u4Count] = MEM_MALLOC (i4Len, INT1);
        if (pi1List[u4Count] == NULL)
        {
            printf ("Memory alloc fails in ReadfileList Routine\n");
            fclose (fp);
            return FAILURE;
        }
        strncpy (pi1List[u4Count], pi1FileName, i4Len);
        u4Count++;
        if (u4Count >= NO_OF_FILES)
        {
            printf ("\n Number of Files exceeds,copies only first 500 files");
            pi1List[u4Count] = NULL;
            fclose (fp);
            return SUCCESS;
        }
        pi1List[u4Count] = NULL;
    }
    fclose (fp);
    unlink (DB_FILES);
    return SUCCESS;
}

INT1
Readdbfile (INT1 **pi1dbfiles)
{
    FILE               *dbfp;
    INT4                i4Count = 0;
    UINT4               u4FileCount = 0;
    UINT4               u4Count = 0;
    INT1                ai1String[FILE_NO_OF_CHAR_IN_A_LINE] = "";
    INT4                i4Len;
    while (pi1dbfiles[u4FileCount] != NULL)
    {
        if ((dbfp = fopen (pi1dbfiles[u4FileCount], "r")) == NULL)
        {
            printf ("unable to open \n");
            u4FileCount++;
            continue;
        }
        i4Count = 0;
        pdb[u4Count] = 0;
        while ((fscanf (dbfp, "%[^:]", ai1String) != EOF) &&
               (u4Count < NO_OF_FILES))
        {
            pdb[u4Count] =
                MEM_REALLOC (pdb[u4Count],
                             sizeof (t_MIB_DB) * (i4Count + 2), t_MIB_DB);
            if (pdb[u4Count] == NULL)
            {
                printf ("Memory Realloc fails in Readfile Routine\n");
                fclose (dbfp);
                return FAILURE;
            }
            fseek (dbfp, 1, 1);
            i4Len = strlen (ai1String);
            pdb[u4Count][i4Count].pi1name = MEM_MALLOC (i4Len + 1, INT1);
            if (pdb[u4Count][i4Count].pi1name == NULL)
            {
                printf ("Memory alloc fails in Readfile Routine:1\n");
                fclose (dbfp);
                return FAILURE;
            }
            strncpy (pdb[u4Count][i4Count].pi1name, ai1String, i4Len + 1);
            fscanf (dbfp, "%[^:]", ai1String);
            fseek (dbfp, 1, 1);
            sscanf (ai1String, "%u", &pdb[u4Count][i4Count].u4child);
            fscanf (dbfp, "%[^:]", ai1String);
            fseek (dbfp, 1, 1);
            sscanf (ai1String, "%u", &pdb[u4Count][i4Count].u4peer);

            fscanf (dbfp, "%[^:]", ai1String);
            fseek (dbfp, 1, 1);
            pdb[u4Count][i4Count].u1type = (UINT1) atoi (ai1String);
            fscanf (dbfp, "%[^:]", ai1String);
            fseek (dbfp, 1, 1);
            pdb[u4Count][i4Count].u1access = (UINT1) atoi (ai1String);
            fscanf (dbfp, "%[^\n]", ai1String);
            fseek (dbfp, 1, 1);
            i4Len = strlen (ai1String);
            pdb[u4Count][i4Count].pi1oid = MEM_MALLOC (i4Len + 1, INT1);
            if (pdb[u4Count][i4Count].pi1oid == NULL)
            {
                printf ("Memory alloc fails in Readfile Routine:2\n");
                fclose (dbfp);
                return FAILURE;
            }
            strncpy (pdb[u4Count][i4Count].pi1oid, ai1String, i4Len + 1);
            i4Count++;
            if (i4Count > MAX_COUNT)
            {
                printf ("OID count for db file %s exceeds %d OIDs\n",
                        pi1dbfiles[u4FileCount], MAX_COUNT);
                return FAILURE;
            }
        }
        fclose (dbfp);
        pdb[u4Count][i4Count].pi1name = MEM_MALLOC (strlen ("NULL") + 1, INT1);
        if (pdb[u4Count][i4Count].pi1name == NULL)
        {
            printf ("Memory alloc fails in Readfile Routine:3\n");
            return FAILURE;
        }
        strncpy (pdb[u4Count][i4Count].pi1name, "NULL", strlen ("NULL"));
        pdb[u4Count][i4Count].pi1name[strlen ("NULL")] = '\0';
        u4Count++;
        u4FileCount++;
    }
    if (u4Count >= NO_OF_FILES - 1)
        return FAILURE;
    pdb[++u4Count] = 0;
    return SUCCESS;
}

VOID
RegisterDB ()
{
    UINT4               u4Count = 0;
    INT4                i4Count = 0;
    INT1                i1check = 0;

    for (; ((u4Count < NO_OF_FILES) && (pdb[u4Count] != 0)); u4Count++)
    {
        for (i4Count = 0;
             STRCMP (pdb[u4Count][i4Count].pi1name, "NULL") != 0; i4Count++)
        {
            if (i1check == SUCCESS)
            {
                RegisterWithNM ((const INT1 *) pdb[u4Count][i4Count].pi1name,
                                (const INT1 *) pdb[u4Count][i4Count].pi1oid,
                                pdb[u4Count]);
            }
        }
    }
}

VOID
RegisterWithNM (const INT1 *pi1name, const INT1 *pi1oid, t_MIB_DB * mibptr)
{

    t_MMI_LINK_LIST    *temp, *newNode = NULL, *checkNode;

    checkNode = main_list;
    while (checkNode != NULL)
    {
        if (STRCMP (pi1oid, checkNode->pi1oid) == 0)
            return;
        checkNode = checkNode->link;
    }

    newNode = MEM_MALLOC (sizeof (t_MMI_LINK_LIST), t_MMI_LINK_LIST);
    if (newNode == NULL)
    {
        return;
    }                            /* mem alloc failed */
    newNode->pi1name = (char *) MEM_MALLOC (STRLEN (pi1name) + 1, INT1);
    if (newNode->pi1name == NULL)
    {
        if (newNode != NULL)
        {
            MEM_FREE (newNode);
            newNode = NULL;
        }
        return;
    }
    newNode->pi1oid = (char *) MEM_MALLOC (STRLEN (pi1oid) + 1, INT1);
    if (newNode->pi1oid == NULL)
    {
        if (newNode->pi1name != NULL)
        {
            MEM_FREE (newNode->pi1name);
            newNode->pi1name = NULL;
        }
        MEM_FREE (newNode);
        newNode = NULL;
        return;
    }
    STRNCPY (newNode->pi1name, pi1name, STRLEN (pi1name) + 1);
    STRNCPY (newNode->pi1oid, pi1oid, STRLEN (pi1oid) + 1);
    newNode->pointer = mibptr;
    newNode->link = 0;

    if (main_list == 0)
    {
        main_list = newNode;
    }
    else
    {
        temp = main_list;
        while (temp->link != 0)
            temp = temp->link;
        temp->link = newNode;
    }
}

VOID
RegisterMIB ()
{
    INT4                i4Count = 0;
    INT1                ai1name[100];
    INT1                ai1oid[100];

    RegisterRootMIB ((const INT1 *) MMI_List[0].pi1name,
                     (const INT1 *) MMI_List[0].pi1oid, MMI_List[0].pointer);
    for (i4Count = 1; MMI_List[i4Count].pi1name != 0; i4Count++)
    {
        memset (ai1name, 0, 100);
        memset (ai1oid, 0, 100);
        STRNCPY (ai1name, MMI_List[i4Count].pi1name,
                 STRLEN (MMI_List[i4Count].pi1name));
        STRNCPY (ai1oid, MMI_List[i4Count].pi1oid,
                 STRLEN (MMI_List[i4Count].pi1oid));
        RegisterWithNM (ai1name, ai1oid, MMI_List[i4Count].pointer);
    }
}
INT1
GetOidStrFromName (pi1MibName, pi1OidStr)
     INT1               *pi1MibName, *pi1OidStr;
{
    INT4                i4Count = 0, i4Len = 0;
    t_MMI_LINK_LIST    *MMILocalLinkList;
    while (pi1MibName[i4Count] != '\0')
    {
        if (pi1MibName[i4Count] == '.')
            break;
        i4Count++;
    }
    i4Len = i4Count;
    MMILocalLinkList = main_list;
    while (MMILocalLinkList != 0)
    {
        if (STRCMP (MMILocalLinkList->pi1name, pi1MibName) == 0)
        {
            STRCPY (pi1OidStr, MMILocalLinkList->pi1oid);
            STRCAT (pi1OidStr, pi1MibName + i4Len);
            return SUCCESS;
        }
        MMILocalLinkList = MMILocalLinkList->link;
    }
    MMILocalLinkList = main_list;
    while (MMILocalLinkList != 0)
    {
        if (MMILocalLinkList->pointer != 0)
        {
            if (GetOidStrFromNameInChildDB
                (MMILocalLinkList->pointer, pi1MibName, i4Len, pi1OidStr) == 0)
            {
                STRCAT (pi1OidStr, pi1MibName + i4Len);
                return SUCCESS;
            }
        }
        MMILocalLinkList = MMILocalLinkList->link;
    }
    return FAILURE;
}

INT1
GetOidStrFromNameInChildDB (MibDBMibPtr, pi1MibName, i4Len, pi1LocalOidStr)
     t_MIB_DB           *MibDBMibPtr;
     INT1               *pi1MibName;
     INT4                i4Len;
     INT1               *pi1LocalOidStr;
{
    INT4                i4Count;
    for (i4Count = 0;
         strncmp ((const char *) MibDBMibPtr[i4Count].pi1name,
                  (const char *) "NULL", STRLEN ("NULL")) != 0; i4Count++)
    {
        if (strncmp
            ((const char *) MibDBMibPtr[i4Count].pi1name, (char *) pi1MibName,
             i4Len) == 0)
        {
            strcpy (pi1LocalOidStr, (const char *) MibDBMibPtr[i4Count].pi1oid);
            return SUCCESS;
        }

    }
    return FAILURE;
}

UINT1
MMIGetNodeAccess (UINT4 u4Index)
{
    INT4                i4MainIndex = 0, i4SubIndex = 0, i4Count = 0;
    t_MIB_DB           *MibDBMibPtr = 0;
    t_MMI_LINK_LIST    *MMILocalLinkList = 0;
    if (u4Index != 0)
    {
        i4MainIndex = (INT4) (u4Index / 2000);
        i4SubIndex = (INT4) (u4Index % 2000);
    }
    if (main_list == NULL)
    {
        return 0;
    }
    MMILocalLinkList = main_list;
    while (i4Count < i4MainIndex)
    {
        MMILocalLinkList = MMILocalLinkList->link;
        if (MMILocalLinkList == NULL)
            return (BRANCH);
        i4Count++;
    }
    if (MMILocalLinkList->pointer != 0)
    {
        MibDBMibPtr = MMILocalLinkList->pointer;
        return (MibDBMibPtr[i4SubIndex].u1access);
    }
    return (MIB_READ_ONLY);
}

UINT1
MMIGetNodeType (UINT4 u4Index)
{
    INT4                i4MainIndex = 0, i4SubIndex = 0, i4Count = 0;
    t_MIB_DB           *MibDBMibPtr;
    t_MMI_LINK_LIST    *MMILocalLinkList;

    if (u4Index == 0)
        return (BRANCH);

    if (u4Index != 0)
    {
        i4MainIndex = (INT4) (u4Index / MAX_COUNT);
        i4SubIndex = (INT4) (u4Index % MAX_COUNT);
    }
    if (main_list == NULL)
    {
        return 0;
    }
    MMILocalLinkList = main_list;
    while (i4Count < i4MainIndex)
    {
        MMILocalLinkList = MMILocalLinkList->link;
        if (MMILocalLinkList == NULL)
            return (BRANCH);
        i4Count++;
    }
    if (MMILocalLinkList != 0 && MMILocalLinkList->pointer != 0)
    {
        MibDBMibPtr = MMILocalLinkList->pointer;
        return (MibDBMibPtr[i4SubIndex].u1type);
    }
    return (BRANCH);
}

INT4
GetIndexFromName (INT1 *pi1MibName)
{
    INT4                i4Count = 0, i4Count1;
    t_MIB_DB           *mibptr;
    t_MMI_LINK_LIST    *MMILocalLinkList;

    MMILocalLinkList = main_list;
    while (MMILocalLinkList != 0)
    {
        if (strcmp ((char *) pi1MibName, MMILocalLinkList->pi1name) == 0)
        {
            mibptr = MMILocalLinkList->pointer;
            if (mibptr != NULL)
            {
                for (i4Count1 = 0;
                     STRCMP (mibptr[i4Count1].pi1name, "NULL") != 0; i4Count1++)
                {
                    if (strcmp ((char *) pi1MibName, (const char *)
                                mibptr[i4Count1].pi1name) == 0)
                        return ((i4Count * MAX_COUNT) + i4Count1);
                }
                return FAILURE;
            }
            else
            {
                return (i4Count * MAX_COUNT);
            }
        }
        MMILocalLinkList = MMILocalLinkList->link;
        i4Count++;
    }
    i4Count = 0;
    MMILocalLinkList = main_list;
    while (MMILocalLinkList != 0)
    {
        if (MMILocalLinkList->pointer != 0)
        {
            mibptr = MMILocalLinkList->pointer;
            for (i4Count1 = 0; STRCMP (mibptr[i4Count1].pi1name, "NULL") != 0;
                 i4Count1++)
            {
                if (strcmp
                    ((char *) pi1MibName,
                     (const char *) mibptr[i4Count1].pi1name) == 0)
                    return ((i4Count * MAX_COUNT) + i4Count1);
            }
        }
        MMILocalLinkList = MMILocalLinkList->link;
        i4Count++;
    }
    return FAILURE;
}

VOID
RegisterRootMIB (const INT1 *pi1name, const INT1 *pi1oid, t_MIB_DB * mibptr)
{

    FirstNode = MEM_MALLOC (sizeof (t_MMI_LINK_LIST), t_MMI_LINK_LIST);
    if (FirstNode == NULL)
        return;
    FirstNode->pi1name = (char *) MEM_MALLOC (STRLEN (pi1name) + 1, INT1);
    if (FirstNode->pi1name == NULL)
    {
        if (FirstNode != NULL)
        {
            MEM_FREE (FirstNode);
            FirstNode = NULL;
        }
        return;
    }
    FirstNode->pi1oid = (char *) MEM_MALLOC (STRLEN (pi1oid) + 1, INT1);
    if (FirstNode->pi1oid == NULL)
    {
        if (FirstNode->pi1name != NULL)
        {
            MEM_FREE (FirstNode->pi1name);
            FirstNode->pi1name = NULL;
        }
        if (FirstNode != NULL)
        {
            MEM_FREE (FirstNode);
            FirstNode = NULL;
        }
        return;
    }
    STRNCPY (FirstNode->pi1name, pi1name, STRLEN (pi1name) + 1);
    STRNCPY (FirstNode->pi1oid, pi1oid, STRLEN (pi1oid) + 1);
    FirstNode->pointer = mibptr;
    FirstNode->link = main_list;
    main_list = FirstNode;
}

INT4
CheckParent (INT1 *pi1Name)
{
    INT4                i4Count = 0, i4DotCount = 0, i4LocalCount = 0, i4Index =
        0;
    INT1                ai1ParentOid[WEBNM_MAX_OID_LENGTH];
    INT1                pi1oidptr[WEBNM_MAX_OID_LENGTH];
    GetOidStrFromName (pi1Name, pi1oidptr);
    i4DotCount = CountDotsInOidString ((char *) pi1oidptr);
    while (1)
    {
        if (i4LocalCount == i4DotCount)
        {
            ai1ParentOid[i4Count] = '\0';
            break;
        }
        ai1ParentOid[i4Count] = pi1oidptr[i4Count];
        i4Count++;
        if (pi1oidptr[i4Count] == '.')
            i4LocalCount++;
    }
    i4Index = GetIndexFromOidStr (ai1ParentOid);
    if (i4Index == FAILURE)
        return FAILURE;
    return (MMIGetNodeType (i4Index));
}

INT1
CountDotsInOidString (pi1Str)
     char               *pi1Str;
{
    INT4                i4Count, i4Count1 = 0;
    for (i4Count = 0; pi1Str[i4Count] != '\0'; i4Count++)
    {
        if (pi1Str[i4Count] == '.')
            i4Count1++;
    }
    return i4Count1;
}

INT4
GetIndexFromOidStr (INT1 *Oid)
{
    t_MMI_LINK_LIST    *local_list;
    t_MIB_DB           *local_mibptr;
    INT4                i4Count = 0, i4Count1 = 0;
    local_list = main_list;
    while (local_list != 0)
    {
        if (local_list->pointer != 0)
        {
            local_mibptr = local_list->pointer;
            for (i4Count1 = 0;
                 STRCMP (local_mibptr[i4Count1].pi1name, "NULL") != 0;
                 i4Count1++)
            {
                if (STRCMP (local_mibptr[i4Count1].pi1oid, Oid) == 0)
                    return ((i4Count * MAX_COUNT) + i4Count1);
            }
        }
        local_list = local_list->link;
        i4Count++;
    }
    return FAILURE;
}
