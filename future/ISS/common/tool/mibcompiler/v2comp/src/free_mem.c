/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: free_mem.c,v 1.2 2007/02/01 14:53:55 iss Exp $
 * 
 * Description:  Contains routines to free dynamic memory.
 *
 ***********************************************************************/

#include "premosy.h"

/*****************************************************************************
 *      Function Name        : free_tc_memory                                *
 *      Role of the function : Frees memory allocated by malloc function.    *
 *      Formal Parameters    : p_head                                        *
 *      Global Variables     : None                                          *
 *      Use of Recursion     : None                                          *
 *      Return Value         : None                                          *
 *****************************************************************************/

void
free_tc_memory (tTC_TABLE * p_head)
{
    tTC_TABLE          *temp_tc;

    while ((temp_tc = p_head) != NULL)
    {
        free_name_values_memory (p_head->tc_name_value_list);
        p_head = p_head->next;
        free (temp_tc);
    }
    return;
}

/*****************************************************************************
 *      Function Name        : free_names_memory                             *
 *      Role of the function : Frees memory allocated by malloc function.    *
 *      Formal Parameters    : p_head                                        *
 *      Global Variables     : None                                          *
 *      Use of Recursion     : None                                          *
 *      Return Value         : None                                          *
 *****************************************************************************/

void
free_names_memory (tNAME_LIST * p_head)
{
    tNAME_LIST         *p_temp_name;

    while ((p_temp_name = p_head) != NULL)
    {
        p_head = p_head->next;
        free (p_temp_name);
    }
    return;
}

/*****************************************************************************
 *      Function Name        : free_name_values_memory                       *
 *      Role of the function : Frees memory allocated by malloc function.    *
 *      Formal Parameters    : p_head                                        *
 *      Global Variables     : None                                          *
 *      Use of Recursion     : None                                          *
 *      Return Value         : None                                          *
 *****************************************************************************/

void
free_name_values_memory (tNAME_VALUE * p_head)
{
    tNAME_VALUE        *p_temp_name_value;

    while ((p_temp_name_value = p_head) != NULL)
    {
        p_head = p_head->next;
        free (p_temp_name_value);
    }
    return;
}

/*****************************************************************************
 *      Function Name        : free_OID_memory                               *
 *      Role of the function : Frees memory allocated by malloc function.    *
 *      Formal Parameters    : p_head                                        *
 *      Global Variables     : None                                          *
 *      Use of Recursion     : None                                          *
 *      Return Value         : None                                          *
 *****************************************************************************/

void
free_OID_memory (tOID * p_head)
{
    tOID               *temp_oid;

    while ((temp_oid = p_head) != NULL)
    {
        p_head = p_head->next;
        free (temp_oid);
    }
    return;
}
