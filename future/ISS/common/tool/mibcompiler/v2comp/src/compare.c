/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: compare.c,v 1.2 2007/02/01 14:53:55 iss Exp $
 * 
 * Description: This file contains a function which compares two OIDs.
 *
 ***********************************************************************/

#include "postmosy.h"

/*****************************************************************************
 *      Function Name        : compare_oids                                  *
 *      Role of the function : This function compares two OIDs abd return a  *
 *                             value based on the result.                    *
 *      Formal Parameters    : pi1_oid1, pi1_oid2 - OIDs to be compared      *
 *      Global Variables     : None                                          *
 *      Use of Recursion     : None                                          *
 *      Return Value         : -1, if pi1_oid1 is less than pi1_oid2         *
 *                              0, if pi1_oid1 is equal to  pi1_oid2         *
 *                              1, if pi1_oid1 is greater than pi1_oid2      *
 *****************************************************************************/

INT4
compare_oids (INT1 *pi1_oid1, INT1 *pi1_oid2)
{
    INT1               *pi1_token1, *pi1_token2;
    INT1                i1_first[MAX_OID_LEN], i1_second[MAX_OID_LEN];

    do
    {
        strcpy (i1_first, pi1_oid1);
        strcpy (i1_second, pi1_oid2);
        pi1_token1 = strtok (i1_first, ".");
        pi1_token2 = strtok (i1_second, ".");
        pi1_oid1 = strchr (pi1_oid1, '.');
        pi1_oid2 = strchr (pi1_oid2, '.');
        if (pi1_oid1 != NULL)
        {
            pi1_oid1++;
        }
        if (pi1_oid2 != NULL)
        {
            pi1_oid2++;
        }
        if (atoi (pi1_token1) < atoi (pi1_token2))
        {
            return -1;
        }
        else if (atoi (pi1_token1) > atoi (pi1_token2))
        {
            return 1;
        }
    }
    while ((pi1_oid1 != NULL) && (pi1_oid2 != NULL));
    if (pi1_oid1 == NULL && pi1_oid2 != NULL)
    {
        return -1;
    }
    if (pi1_oid2 == NULL && pi1_oid1 != NULL)
    {
        return 1;
    }
    return 0;
}
