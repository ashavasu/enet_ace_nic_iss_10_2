/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: postmosy.c,v 1.3 2010/06/21 09:31:30 prabuc Exp $
 * 
 * Description: This file has routine to add OID in MIB database structure.
 *
 ***********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <ctype.h>

typedef char INT1;
typedef unsigned char UINT1;
typedef short INT2;
typedef unsigned short UINT2;
typedef int INT4;
typedef unsigned int UINT4;

#ifndef TRUE
#define TRUE 1
#define FALSE 0
#endif
#define VALID 1
#define INVALID 2
#define COMMENT 3
#define BLANK 4

#define MAX_LINE_LEN 256        /* max. length of line */
#define MAX_DESC_LEN 64            /* max. length of descriptor */
#define MAX_OID_LEN 8266        /* max. length of OID (127*64+10+127) */
#define ALLOC_ENTRY_MEM ((tOID_TABLE *) malloc (sizeof (tOID_TABLE)))
#define MAX_FILE_NAME_LEN 64    /* max. length of file name */

typedef struct oid_table
{
    INT1                object_desc[MAX_DESC_LEN];
    INT1                object_oid[MAX_OID_LEN];
    struct oid_table   *next;
}
tOID_TABLE;

INT4                initialise_oid_table (void);
INT4                add_entry (INT1 *, INT1 *);
INT4                compare_oids (INT1 *, INT1 *);
INT4                process_input (INT1 *);
INT4                check_line (INT1 *);
INT1               *get_next_component (INT1 *, INT1);
INT4                write_oid_list_to_file (INT1 *);
void                free_memory (tOID_TABLE *);

tOID_TABLE         *p_oid_list;
extern UINT4        u4_lineno;
INT1               *pi1_file_being_processed;    /* points to name of the file being processed */
tOID_TABLE          recent_oid;
/****************************************************************************
 *     File Name        : Postmosy.c                          *    
 *    Role Of Function : Process input file and update the linklist by    *
 *                 adding OID [MIBLOAD TOOL]                *
 *                                        *
 *                                        *
 ****************************************************************************

INT1 *pi1_temp;

/*****************************************************************************
 *      Function Name        : ExtractOid                                    *
 *      Role of the function : Process input file and update the linked list *
 *                             by adding one node for each name and OID pair.*
 *      Formal Parameters    : pi1_in_file                                   *
 *      Global Variables     : p_oid_list                                    *
 *      Use of Recursion     : None                                          *
 *      Return Value         : -1, if error occurs                           *
 *                              0, else                                      *
 *****************************************************************************/

/* INT4 ExtractOid (FILE *pi1_in_file, INT1 *pOidptr) */
INT4
ExtractOid (INT1 *ai1LineBuffer)
{
    INT1                i1_line[MAX_LINE_LEN];
    INT1               *pi1_line;
    INT1               *Oidptr;
    INT1               *pi1_in_file;
    INT1                i1_object_desc[MAX_DESC_LEN],
        i1_object_oid[MAX_OID_LEN];
    INT4                i4_retval;
/*      if ((pi1_in_file = fopen (pInputFilePtr, "r")) == NULL) {
      fprintf (stderr, "\nCan't kam open %s\n",Oidptr );
      return -1; 
   } */
    pi1_in_file = ai1LineBuffer;

/*   u4_lineno = 0;
   while (fgets (i1_line, MAX_LINE_LEN, pi1_in_file) != NULL) {
      u4_lineno++;*/

    i4_retval = check_line (pi1_in_file);

    while (i4_retval == BLANK || i4_retval == COMMENT)
    {
        continue;
    }
    if (i4_retval == INVALID)
    {
        fprintf (stderr, "\n%s(%u): Invalid line\n", pi1_in_file, u4_lineno);
        /*  fprintf (stderr, "%s\n", pOidptr); */
        return -1;
    }
    if (i4_retval == -1)
    {
        return -1;
    }
    for (pi1_line = pi1_in_file; *pi1_line == ' ' || *pi1_line == '\t';
         ++pi1_line);

    sscanf (pi1_line, "%s %s", i1_object_desc, i1_object_oid);
    if (add_entry (i1_object_desc, i1_object_oid) < 0)
    {
        return -1;
    }
    return 0;

}

/*****************************************************************************
 *      Function Name        : initialise_oid_table                          *
 *      Role of the function : This function initialises the linked list     *
 *                             with standard OIDs.                           *
 *      Formal Parameters    : None                                          *
 *      Global Variables     : None                                          *
 *      Use of Recursion     : None                                          *
 *      Return Value         : -1, if error occurs                           *
 *                              0, else                                      *
 *****************************************************************************/

INT4
initialise_oid_table (void)
{
    FILE               *fp;
    INT1                ai1_line[80];
    INT1                ai1Type[80];
    INT1                ai1OldName[80];
    INT1                ai1NewName[80];

    if ((fp = fopen ("info.def", "r")) == NULL)
    {
        printf ("\n Error in info.def file open");
        return -1;
    }

    if (add_entry ("iso", "1") < 0)
    {
        fclose (fp);
        return -1;
    }
    if (add_entry ("org", "1.3") < 0)
    {
        fclose (fp);
        return -1;
    }
    if (add_entry ("dod", "org.6") < 0)
    {
        fclose (fp);
        return -1;
    }
    if (add_entry ("internet", "dod.1") < 0)
    {
        fclose (fp);
        return -1;
    }
    if (add_entry ("mgmt", "internet.2") < 0)
    {
        fclose (fp);
        return -1;
    }
    if (add_entry ("mib-2", "mgmt.1") < 0)
    {
        fclose (fp);
        return -1;
    }
    if (add_entry ("transmission", "mib-2.10") < 0)
    {
        fclose (fp);
        return -1;
    }
    if (add_entry ("experimental", "internet.3") < 0)
    {
        fclose (fp);
        return -1;
    }
    if (add_entry ("private", "internet.4") < 0)
    {
        fclose (fp);
        return -1;
    }
    if (add_entry ("enterprises", "private.1") < 0)
    {
        fclose (fp);
        return -1;
    }
    if (add_entry ("snmpV2", "internet.6") < 0)
    {
        fclose (fp);
        return -1;
    }
    if (add_entry ("snmpModules", "snmpV2.3") < 0)
    {
        fclose (fp);
        return -1;
    }
    if (add_entry ("snmpMIB", "snmpModules.1") < 0)
    {
        fclose (fp);
        return -1;
    }
    if (add_entry ("snmpMIBObjects", "snmpMIB.1") < 0)
    {
        fclose (fp);
        return -1;
    }
    if (add_entry ("snmpTrap", "snmpMIBObjects.4") < 0)
    {
        fclose (fp);
        return -1;
    }
    while (fgets (ai1_line, 80, fp) != NULL)
    {
        sscanf (ai1_line, "%s%s%s%*s", ai1OldName, ai1Type, ai1NewName);

        if (strcmp (ai1Type, "OID") == 0)
        {
            add_entry (ai1OldName, ai1NewName);
        }
    }

    fclose (fp);
    return 0;
}

/*****************************************************************************
 *      Function Name        : check_line                                    *
 *      Role of the function : Checks line for comment, blank and valid.     *
 *      Formal Parameters    : pi1_line                                      *
 *      Global Variables     : None                                          *
 *      Use of Recursion     : None                                          *
 *      Return Value         : VALID, if contains the required fields        *
 *                             INVALID, if not contains the required fields  *
 *                             COMMENT, if comment line                      *
 *                             BLANK, if blank line                          *
 *****************************************************************************/

INT4
check_line (INT1 *pi1_line)
{
    INT1               *pi1_temp;

    if (strstr (pi1_line, "--") == pi1_line)
    {
        return COMMENT;
    }
    while (*pi1_line == ' ' || *pi1_line == '\t')
    {
        pi1_line++;
    }
    if (*pi1_line == '\n')
    {
        return BLANK;
    }
    else
    {
        if (!isalpha (*pi1_line) || isupper (*pi1_line))
        {
            fprintf (stderr,
                     "\n%s(%u): ObjectName should start with lowercase letter\n",
                     pi1_file_being_processed, u4_lineno);
            return -1;
        }
        pi1_temp = strchr (pi1_line, ' ');
        if (pi1_temp == NULL)
        {
            pi1_temp = strchr (pi1_line, '\t');
        }
        if (pi1_temp == NULL)
        {
            return INVALID;
        }
        if (pi1_temp - pi1_line > MAX_DESC_LEN)
        {
            fprintf (stderr, "\n%s(%u): Descriptor length exceeded 64\n",
                     pi1_file_being_processed, u4_lineno);

            return -1;
        }
        pi1_line = pi1_temp;
        while (*pi1_line == ' ' || *pi1_line == '\t')
        {
            pi1_line++;
        }
        if (*pi1_line == '\n')
        {
            return INVALID;
        }
        return VALID;
    }
}

/*****************************************************************************
 *      Function Name        : add_entry                                     *
 *      Role of the function : Place a node, comprises of Object Descriptor  *
 *                             and its OID, in the linked list pointed by    *
 *                             p_oid_list in the ascending order.            *
 *      Formal Parameters    : pi1_name, pi1_oid - Object name and its OID   *
 *      Global Variables     : p_oid_list, recent_oid                        *
 *      Use of Recursion     : None                                          *
 *      Return Value         : -1, if error occurs                           *
 *                              0, else                                      *
 *****************************************************************************/

INT4
add_entry (INT1 *pi1_name, INT1 *pi1_oid)
{
    INT1               *pi1_token, *pi1_orig_oid = pi1_oid;
    INT4                i4_retval;
    tOID_TABLE         *p_entry, *p_node = p_oid_list;
    tOID_TABLE         *p_temp, *p_prev_node = NULL;

    p_entry = ALLOC_ENTRY_MEM;
    if (p_entry == NULL)
    {
        fprintf (stderr, "\nMemory allocation failure\n");
        return -1;
    }

    strcpy (p_entry->object_desc, pi1_name);
    strcpy (p_entry->object_oid, "");
    if ((pi1_token = get_next_component (pi1_orig_oid, '.')) == NULL)
    {
        return -1;
    }

    /* resolve any name part of OID */
    while (1)
    {
        if (*pi1_token == '\0' || !isalnum (*pi1_token) || isupper (*pi1_token))
        {
            fprintf (stderr, "\n%s(%u): Invalid OID %s\n",
                     pi1_file_being_processed, u4_lineno, pi1_oid);
            return -1;
        }
        if (isalpha (*pi1_token))
        {
            if (strcmp (recent_oid.object_desc, pi1_token) == 0)
            {
                strcat (p_entry->object_oid, recent_oid.object_oid);
            }
            else
            {
                p_temp = p_oid_list;
                while (p_temp != NULL)
                {
                    if (strcmp (p_temp->object_desc, pi1_token) == 0)
                    {
                        strcat (p_entry->object_oid, p_temp->object_oid);
                        break;
                    }
                    p_temp = p_temp->next;
                }
                if (p_temp == NULL)
                {
                    fprintf (stderr, "\n%s(%u): ", pi1_file_being_processed,
                             u4_lineno);
                    fprintf (stderr, "Parental information for %s not found\n",
                             pi1_token);
                    return -1;
                }
            }
        }
        else
        {
            strcat (p_entry->object_oid, pi1_token);
        }
        strcat (p_entry->object_oid, ".");
        free (pi1_token);
        if (strchr (pi1_orig_oid, '.') == NULL)
        {
            break;
        }
        pi1_orig_oid = strchr (pi1_orig_oid, '.');
        ++pi1_orig_oid;
        if ((pi1_token = get_next_component (pi1_orig_oid, '.')) == NULL)
        {
            return -1;
        }
    }
    p_entry->object_oid[strlen (p_entry->object_oid) - 1] = '\0';
    p_entry->next = NULL;
    strcpy (recent_oid.object_desc, p_entry->object_desc);
    strcpy (recent_oid.object_oid, p_entry->object_oid);
    recent_oid.next = NULL;
    if (p_oid_list == NULL)
    {                            /* First node */
        p_oid_list = p_entry;
        return 0;
    }

    /* check for duplicate names with different OIDs */
    while (p_node != NULL)
    {
        if (strcmp (p_entry->object_desc, p_node->object_desc) == 0)
        {
            if (strcmp (p_entry->object_oid, p_node->object_oid) != 0)
            {

                /* Actually we have stored standard OID tree till the node        *
                 * "internet" in the linked list. We assume that in the future    *
                 * these standard nodes may get shifted to other node. So the     *
                 * tool must be able to handle this situation. Without that check *
                 * given below it gives the following error. In essence the user  *
                 * specified information takes the priority and overwrite the     *
                 * stored one.                                                    *
                 */

                if (!strcmp (p_entry->object_desc, "iso") ||
                    !strcmp (p_entry->object_desc, "org") ||
                    !strcmp (p_entry->object_desc, "dod") ||
                    !strcmp (p_entry->object_desc, "ccitt") ||
                    !strcmp (p_entry->object_desc, "internet") ||
                    !strcmp (p_entry->object_desc, "joint-iso-ccitt"))
                {
                    if (p_prev_node == NULL)
                    {
                        p_oid_list = p_node->next;
                    }
                    else
                    {
                        p_prev_node->next = p_node->next;
                    }
                    free (p_node);
                    break;
                }
                fprintf (stderr, "\nERROR: %s(%u):\n", pi1_file_being_processed,
                         u4_lineno);
                fprintf (stderr, "Duplicate name with different OIDs:\n");
                fprintf (stderr, "%s %s\n", p_entry->object_desc,
                         p_entry->object_oid);
                fprintf (stderr, "%s %s\n", p_node->object_desc,
                         p_node->object_oid);
                free (p_entry);
                return -1;
            }
            break;
        }
        p_prev_node = p_node;
        p_node = p_node->next;
    }
    p_node = p_oid_list;
    p_prev_node = NULL;

    /* insert new OID into the linked list at apprpriate position */
    while (p_node != NULL)
    {
        i4_retval = compare_oids (p_entry->object_oid, p_node->object_oid);
        if (i4_retval < 0)
        {
            p_entry->next = p_node;
            if (p_prev_node == NULL)
            {
                p_oid_list = p_entry;
            }
            else
            {
                p_prev_node->next = p_entry;
            }
            return 0;
        }
        if (i4_retval == 0)
        {
            if (strcmp (p_entry->object_desc, p_node->object_desc) == 0)
            {
                free (p_entry);
                return 0;
            }
            else
            {
                fprintf (stderr, "\nERROR: %s(%u):\n", pi1_file_being_processed,
                         u4_lineno);
                fprintf (stderr, "Duplicate OID assignment:\n");
                fprintf (stderr, "%s %s\n", p_node->object_desc,
                         p_node->object_oid);
                fprintf (stderr, "%s %s\n", p_entry->object_desc,
                         p_entry->object_oid);
                free (p_entry);
                return -1;
            }
        }
        p_prev_node = p_node;
        p_node = p_node->next;
    }

    /* end of linked list reached so append new node to the list */
    p_prev_node->next = p_entry;
    return 0;
}

/*****************************************************************************
 *      Function Name        : get_next_component                            *
 *      Role of the function : Returns the pattern until the delimeter.      *
 *      Formal Parameters    : pi1_str, i1_delim                             *
 *      Global Variables     : None                                          *
 *      Use of Recursion     : None                                          *
 *      Return Value         : -1, if error occurs                           *
 *                              0, else                                      *
 *****************************************************************************/

INT1               *
get_next_component (INT1 *pi1_str, INT1 i1_delim)
{
    INT1               *pi1_temp, *pi1_pattern;
    UINT1               u1_count;

    pi1_temp = strchr (pi1_str, i1_delim);
    if (pi1_temp == NULL)
    {
        if (strlen (pi1_str) > MAX_DESC_LEN)
        {
            fprintf (stderr, "\n%s(%u): Descriptor length exceeded 64\n",
                     pi1_file_being_processed, u4_lineno);

            return NULL;
        }
        pi1_pattern = (INT1 *) malloc (strlen (pi1_str) + 1);
        if (pi1_pattern == NULL)
        {
            fprintf (stderr, "Memory allocation failure\n");
            return NULL;
        }
        strcpy (pi1_pattern, pi1_str);
    }
    else
    {
        u1_count = pi1_temp - pi1_str;
        if (u1_count > MAX_DESC_LEN)
        {
            fprintf (stderr, "\n%s(%u): Descriptor length exceeded 64\n",
                     pi1_file_being_processed, u4_lineno);

            return NULL;
        }
        pi1_pattern = (INT1 *) malloc (u1_count + 1);
        if (pi1_pattern == NULL)
        {
            fprintf (stderr, "Memory allocation failure\n");
            return NULL;
        }
        strncpy (pi1_pattern, pi1_str, u1_count);
        pi1_pattern[u1_count] = '\0';
    }
    return pi1_pattern;
}

/*****************************************************************************
 *      Function Name        : free_memory                                   *
 *      Role of the function : Frees the memory allocated to linked list.    *
 *      Formal Parameters    : p_head                                        *
 *      Global Variables     : None                                          *
 *      Use of Recursion     : None                                          *
 *      Return Value         : None                                          *
 *****************************************************************************/

void
free_memory (tOID_TABLE * p_head)
{
    tOID_TABLE         *p_temp;

    while ((p_temp = p_head) != NULL)
    {
        p_head = p_head->next;
        free (p_temp);
    }
    return;
}

/*****************************************************************************
 *      Function Name        : compare_oids                                  *
 *      Role of the function : This function compares two OIDs abd return a  *
 *                             value based on the result.                    *
 *      Formal Parameters    : pi1_oid1, pi1_oid2 - OIDs to be compared      *
 *      Global Variables     : None                                          *
 *      Use of Recursion     : None                                          *
 *      Return Value         : -1, if pi1_oid1 is less than pi1_oid2         *
 *                              0, if pi1_oid1 is equal to  pi1_oid2         *
 *                              1, if pi1_oid1 is greater than pi1_oid2      *
 *****************************************************************************/

INT4
compare_oids (INT1 *pi1_oid1, INT1 *pi1_oid2)
{
    INT1               *pi1_token1, *pi1_token2;
    INT1                i1_first[MAX_OID_LEN], i1_second[MAX_OID_LEN];

    do
    {
        strcpy (i1_first, pi1_oid1);
        strcpy (i1_second, pi1_oid2);
        pi1_token1 = strtok (i1_first, ".");
        pi1_token2 = strtok (i1_second, ".");
        pi1_oid1 = strchr (pi1_oid1, '.');
        pi1_oid2 = strchr (pi1_oid2, '.');
        if (pi1_oid1 != NULL)
        {
            pi1_oid1++;
        }
        if (pi1_oid2 != NULL)
        {
            pi1_oid2++;
        }
        if (atoi (pi1_token1) < atoi (pi1_token2))
        {
            return -1;
        }
        else if (atoi (pi1_token1) > atoi (pi1_token2))
        {
            return 1;
        }
    }
    while ((pi1_oid1 != NULL) && (pi1_oid2 != NULL));
    if (pi1_oid1 == NULL && pi1_oid2 != NULL)
    {
        return -1;
    }
    if (pi1_oid2 == NULL && pi1_oid1 != NULL)
    {
        return 1;
    }
    return 0;
}
