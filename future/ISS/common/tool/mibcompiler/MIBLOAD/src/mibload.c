/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: mibload.c,v 1.7 2015/06/05 09:41:05 siva Exp $
 * 
 * Description: This file has routine to generate MIB DataBase structure 
 *
 ***********************************************************************/

#include "mibload.h"

static UINT4        u4Index = 0;
static tMibInfo     MibInfo[MVS_MAX_TABLE_ENTRIES];
static INT1         ai1LineBuffer[LINE_BUFFER_LENGTH];
static INT1         ai2LineBuffer[LINE_BUFFER_LENGTH];
static INT1         ai1Child[NAME_BUFFER_LENGTH];
static INT1         ai1ParentField[NAME_BUFFER_LENGTH];
static INT1         ai1TempParentField[NAME_BUFFER_LENGTH];
static INT1         ai1Syntax[SYNTAX_BUFFER_LENGTH];
static INT1         ai1Access[ACCESS_BUFFER_LENGTH];
static INT1         ai1Mandatory[MANDATORY_BUFFER_LENGTH];

/* For OID Declration */

tOID_TABLE         *p_oid_list;
tOID_TABLE         *p_temp;
INT4                u4_lineno;
INT1               *pi1_file_being_processed;

INT4                get_syntax_from_file (INT1 *);

#define OUTPUT_FILE  "mibload.h"    /* For Output File */
#define INFOFILE      "info.def"
/* 
 * ai1MibInfoLeafSyntax contains the list of Syntaxes for the
 * Mib objects.The order of the Mib table leaf syntax specified below
 * corresponds the syntax macros defined in mvspdu.h file
 * When this array is changed, this file must be referred.
 */
static INT1        *ai1MibInfoSyntax[] = {
    "Not defined",
    "",
    "",
    "INTEGER",
    "OCTET-STRING",
    "OctetString",
    "ObjectIdentifier",
    "NetworkAddress",
    "IpAddress",
    "TimeTicks",
    "Gauge",
    "Counter",
    "Opaque",
    "Integer32",
    "Gauge32",
    "Counter32",
    "Counter64",
    "Unsigned32",
    "TruthValue",
    "PhysAddress",
    "TimeStamp",
    "TAddress",
    "DisplayString",
    "InstancePointer",
    "TestAndIncr",
    "RowStatus",
    "Signed32",
    "Integer8",
    "Integer16",
    "OBJECT_IDENTIFIER",
    "RowPointer",
    "MacAddress",
    "StorageType",
    "VariablePointer",
    "AutonomousType",
    "TimeInterval",
    "DateAndTime",
    "TDomain",
    NULL
};

static INT1        *api1SyntaxSnmp[] = {
    "BRANCH",
    "TABLE",
    "INTEGER",
    "OCTET_STRING",
    "OCTET_STRING",
    "OBJECT_IDENTIFIER",
    "NETWORK_ADDRESS",
    "IP_ADDRESS",
    "TIME_TICKS",
    "GAUGE",
    "COUNTER",
    "OPAQUE",
    "INTEGER",
    "GAUGE",
    "COUNTER",
    "COUNTER64",
    "UNSIGNED32",
    "INTEGER",
    "OCTET_STRING",
    "TIME_TICKS",
    "OCTET_STRING",
    "DISPLAYSTRING",
    "OBJECT_IDENTIFIER",
    "INTEGER",
    "INTEGER",
    "INTEGER",
    "INTEGER",
    "INTEGER",
    "OBJECT_IDENTIFIER",
    "OBJECT_IDENTIFIER",
    "OCTET_STRING",
    "INTEGER",
    "OBJECT_IDENTIFIER",
    "OBJECT_IDENTIFIER",
    "INTEGER",
    "OCTET_STRING",
    "OCTET_STRING",
    "SYNTAX_NOT_DEF",
    NULL
};
static INT1         api1SyntaxSnmpNumber[] = {
    0,
    1,
    2,
    3,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    2,
    8,
    9,
    14,
    8,
    2,
    3,
    7,
    3,
    11,
    4,
    2,
    2,
    2,
    2,
    2,
    4,
    4,
    3,
    2,
    4,
    4,
    2,
    3,
    3,
    15,
    NULL
};

static INT1        *api1Access[] = {
    "MIB_READ_ONLY",
    "MIB_READ_WRITE",
    "MIB_READ_CREATE",
    "MIB_NOT_ACCESSIBLE",
    "MIB_ACCESSIBLE_FOR_NOTIFY"
};
static INT1         api1AccessNumber[] = {
    0,
    1,
    2,
    3,
    4
};

#define ML_OCTET_STRING_INDEX 3

/* 
 * ai1MibInfoAccessType contains the list of access permissions for the
 * Mib objects.The order of the Mib table leaf syntax specified below
 * corresponds the access permissions defined in mvspdu.h file
 * When this array is changed, this file must be referred.
 */
static UINT1       *ai1MibInfoAccessType[] = {
    "read-only",
    "read-write",
    "read-create",
    "not-accessible",
    "accessible-for-notify",
    NULL
};

VOID                MvsMtgGenerateSingleMibInfo ();

VOID
MvsMtgPrintErrorAndExit (ai1ErrorMessage)
     INT1               *ai1ErrorMessage;
{
    printf ("%s\n", ai1ErrorMessage);
    exit (1);
}

VOID
MvsMtgCopyNameToMibInfo (pi1NameBuff, u4Index)
     INT1               *pi1NameBuff;
     UINT4               u4Index;
{

    if (u4Index > MVS_MAX_TABLE_ENTRIES)
        MvsMtgPrintErrorAndExit ("Mib Table Overflow");

    MibInfo[u4Index].pi1Name = (INT1 *) malloc (strlen (pi1NameBuff) + 1);
    if (MibInfo[u4Index].pi1Name == NULL)
        MvsMtgPrintErrorAndExit ("malloc error");

    strcpy (MibInfo[u4Index].pi1Name, pi1NameBuff);
}

VOID
MvsMtgSplitParentField (pi1ParentField, pi1Parent, pu1Child)
     INT1               *pi1ParentField;
     INT1               *pi1Parent;
     UINT1              *pu1Child;
{
    if ((pi1ParentField == NULL) || (pi1Parent == NULL) || (pu1Child == NULL))
    {
        return;
    }

    strcpy (pi1Parent, strtok (pi1ParentField, "."));
    if (atoi (pi1ParentField))
    {
        /* Parent field contains absolute object id. 
           So just fill zero in parent */
        pi1Parent[0] = 0;
        return;
    }

    *pu1Child = (UINT1) atoi (strtok (NULL, "."));
}

UINT4
MvsMtgGetLastChild (u4ParentIndex)
     UINT4               u4ParentIndex;
{
    UINT4               u4ChildIndex;
    UINT4               u4PrevChildIndex;

    u4ChildIndex = MibInfo[u4ParentIndex].u4FirstChild;
    if (u4ChildIndex == 0)
        return 0;
    u4PrevChildIndex = u4ChildIndex;

    while ((u4ChildIndex = MibInfo[u4ChildIndex].u4NextPeer) != 0)
        u4PrevChildIndex = u4ChildIndex;
    return u4PrevChildIndex;

}

UINT4
MvsMtgGetTableIndex (ai1Parent)
     INT1               *ai1Parent;
{

    UINT4               u4Count;

    for (u4Count = 1; u4Count < u4Index; u4Count++)
    {
        if (strcmp (ai1Parent, MibInfo[u4Count].pi1Name) == 0)
            return u4Count;
    }

    if (u4Count == u4Index)
        return 0;
}

INT1
MvsMtgProcessLine (pInputFilePtr)
     FILE               *pInputFilePtr;
{
    INT1                ai1Parent[256];
    INT1                ai1ErrBuffer[256];
    UINT1               u1AbsoluteObjectIdFlag = 0;
    UINT4               u4ParentIndex = 0;
    UINT4               u4FirstChildIndex = 0;
    UINT4               u4LastChild = 0;
    INT1                u1Child;
    INT1               *Parent;
    INT1                Oid[512];
    INT1               *ptr;
    UINT1               u1Counter;
    ai1LineBuffer[0] = 0;
    ai1Child[0] = 0;
    ai1ParentField[0] = 0;
    ai1Syntax[0] = 0;
    ai1Access[0] = 0;
    ai1Mandatory[0] = 0;

    if (fgets (ai1LineBuffer, LINE_BUFFER_LENGTH, pInputFilePtr) != NULL)
    {
        memset (ai1ParentField, 0, sizeof (ai1ParentField));
        sscanf (ai1LineBuffer, "%s %s %s %s %s", ai1Child, ai1ParentField,
                ai1Syntax, ai1Access, ai1Mandatory);
        if ((ai1Child[0] != 0) && (ai1Child[0] != '-')
            && strcmp (ai1Child, "zeroDotZero") != 0)
        {
# ifdef DEBUG
            printf ("Child: %s  Parent: %s\n", ai1Child, ai1ParentField);
# endif

            MvsMtgCopyNameToMibInfo (ai1Child, u4Index);
            ai1Parent[0] = 0;
            u1Child = 0;
            strcpy (ai1TempParentField, ai1ParentField);
            MvsMtgSplitParentField (ai1TempParentField, ai1Parent, &u1Child);
            if (get_syntax_from_file (ai1Parent) == SUCCESS)
            {
                sprintf (ai1ParentField, "%s.%d", ai1Parent, (short) u1Child);
            }
           /** For Properity mibs starts with this - Kamaraj **/
            while ((strncmp (ai1ParentField, "mib-2", strlen ("mib-2")) == 0)
                   || (strncmp (ai1ParentField, "mgmt", strlen ("mgmt")) == 0)
                   || (strncmp (ai1ParentField, "iso", strlen ("iso")) == 0)
                   ||
                   (strncmp
                    (ai1ParentField, "enterprises",
                     strlen ("enterprises")) == 0)
                   ||
                   (strncmp
                    (ai1ParentField, "experimental",
                     strlen ("experimental"))) == 0
                   ||
                   (strncmp
                    (ai1ParentField, "snmpModules",
                     strlen ("snmpModules"))) == 0
                   ||
                   (strncmp (ai1ParentField, "internet", strlen ("internet")))
                   == 0
                   ||
                   (strncmp
                    (ai1ParentField, "transmission",
                     strlen ("transmission"))) == 0)

            {

                if (strncmp (ai1ParentField, "enterprises", 11) == 0)
                {
                    strcpy (Oid, "1.3.6.1.4.1");
                    ptr = strchr (ai1ParentField, '.');
                    strcat (Oid, ptr);
                    strcpy (ai1ParentField, Oid);
                    break;
                }
                if (strncmp (ai1ParentField, "iso", 3) == 0)
                {
                    strcpy (Oid, "1");
                    ptr = strchr (ai1ParentField, '.');
                    strcat (Oid, ptr);
                    strcpy (ai1ParentField, Oid);
                    break;
                }

                if (strncmp (ai1ParentField, "mgmt", 4) == 0)
                {
                    strcpy (Oid, "1.3.6.1.2");
                    ptr = strchr (ai1ParentField, '.');
                    strcat (Oid, ptr);
                    strcpy (ai1ParentField, Oid);
                    break;
                }
                if (strncmp (ai1ParentField, "mib-2", 5) == 0)
                {
                    strcpy (Oid, "1.3.6.1.2.1");
                    ptr = strchr (ai1ParentField, '.');
                    strcat (Oid, ptr);
                    strcpy (ai1ParentField, Oid);
                    break;
                }
                if (strncmp (ai1ParentField, "experimental", 12) == 0)
                {
                    strcpy (Oid, "1.3.6.1.3");
                    ptr = strchr (ai1ParentField, '.');
                    strcat (Oid, ptr);
                    strcpy (ai1ParentField, Oid);
                    break;
                }
                if (strncmp (ai1ParentField, "snmpModules", 11) == 0)
                {
                    strcpy (Oid, "1.3.6.1.6.3");
                    ptr = strchr (ai1ParentField, '.');
                    strcat (Oid, ptr);
                    strcpy (ai1ParentField, Oid);
                    break;
                }
                if (strncmp (ai1ParentField, "internet", 8) == 0)
                {
                    strcpy (Oid, "1.3.6.1");
                    ptr = strchr (ai1ParentField, '.');
                    strcat (Oid, ptr);
                    strcpy (ai1ParentField, Oid);
                    break;
                }
                if (strncmp
                    (ai1ParentField, "transmission",
                     strlen ("transmission")) == 0)
                {
                    strcpy (Oid, "1.3.6.1.2.1.10");
                    ptr = strchr (ai1ParentField, '.');
                    strcat (Oid, ptr);
                    strcpy (ai1ParentField, Oid);
                    break;
                }

            }
         /*** End ***/
            printf (" Parent: %s\n", ai1ParentField);
            ai1Parent[0] = 0;
            u1Child = 0;
            MvsMtgSplitParentField (ai1ParentField, ai1Parent, &u1Child);

            /* 
             * Branches have only two fields in the line. Table entries
             * and leaves ave five fields. To identify table entry or
             * leaf it is enough to check for non-NULL third field(ai1Syntax).
             */
            if (ai1Syntax[0])
            {
                /*
                 * For leaves and table entries.
                 */

                /*
                 * Store Syntax information.
                 */
                if (strcmp (ai1Syntax, "Aggregate") == 0)
                {
                    MibInfo[u4Index].u1Syntax = TABLE;
                }
                else
                {
                    for (u1Counter = SYNTAX_LEAF_START;
                         u1Counter < MAX_SYNTAXES; u1Counter++)
                    {
                        if (ai1MibInfoSyntax[u1Counter] == NULL)
                        {
                            /*  To Access the IMPORT DataType - Kamaraj */
                            if (get_syntax_from_file (ai1Syntax) != SUCCESS)
                            {
                                fprintf (stderr,
                                         "\nIMPORT Datatype syntax not found ");
                                fprintf (stderr,
                                         "\nPrimitive type of %s not found .\nPl. Enter the syntax in info.def file \n.Refer: ENM Product User Manual for more details ",
                                         ai1Syntax);
                                MvsMtgPrintErrorAndExit
                                    ("MIBLOAD Unable to Continue...\n");
                            }
                            else
                            {
                                u1Counter = 3;
                            }
                        }
                        if (strcmp (ai1MibInfoSyntax[u1Counter], ai1Syntax) ==
                            0)
                        {
                            MibInfo[u4Index].u1Syntax = u1Counter - 1;
                            break;
                        }
                    }

                    if ((u1Counter == MAX_SYNTAXES) ||
                        (ai1MibInfoSyntax[u1Counter] == NULL))
                    {
                        printf
                            ("MAX Syntaxes reached for '%s' with '%s' Syntax\n",
                             ai1Child, ai1Syntax);

                        MibInfo[u4Index].u1Syntax = SYNTAX_NOT_DEF;
                        if (!strcmp (ai1Syntax, "DisplayString"))
                        {
                            MibInfo[u4Index].u1Syntax = ML_OCTET_STRING_INDEX;
                            printf
                                ("Reassigning 'DisplayString' to 'OctetString'\n");
                        }
                        else
                            MibInfo[u4Index].u1Syntax = SYNTAX_NOT_DEF;
                    }
                }

                /*
                 * Store Access information.
                 */
                for (u1Counter = 0; u1Counter < MVS_MAX_ACCESS; u1Counter++)
                {
                    if (ai1MibInfoAccessType[u1Counter] == NULL)
                        break;
                    if (strcmp (ai1MibInfoAccessType[u1Counter], ai1Access) ==
                        0)
                    {
                        MibInfo[u4Index].u1Access = u1Counter;
                        break;
                    }
                }

                if ((u1Counter == MVS_MAX_ACCESS) ||
                    (ai1MibInfoAccessType[u1Counter] == NULL))
                    MvsMtgPrintErrorAndExit ("Invalid access permission");

                /*   
                 * Extract Oid from Postmosy tool o/p
                 */

                ExtractOid (ai1LineBuffer);

            }
            else
            {
                /*
                 * For Branches
                 */

                MibInfo[u4Index].u1Syntax = BRANCH;
                /*
                 * For Parent field containg absolute object id (ie. 3.2.1.5.0), 
                 * value of aiParent  is NULL. In this case just no need to
                 * store Parent's information.
                 */
                if (ai1Parent[0] == 0)
                {
                    u1AbsoluteObjectIdFlag = 1;
                    ExtractOid (ai1LineBuffer);    /*  For OID */
                }
            }

            if (u1AbsoluteObjectIdFlag == 0)
            {
                if ((u4ParentIndex = MvsMtgGetTableIndex (ai1Parent)) == 0)
                {
                    sscanf (ai1ErrBuffer, "%s: Object not found", ai1Parent);
                    MvsMtgPrintErrorAndExit (ai1ErrBuffer);
                }

                if (u1Child == 1)
                {                /* eg. ipx.1 */
                    MibInfo[u4ParentIndex].u4FirstChild = u4Index;
                    ExtractOid (ai1LineBuffer);
                                          /** OID **/

                }
                else
                {                /* eg. ipx.2 (or) ipx.3 */
                    if (MibInfo[u4ParentIndex].u4FirstChild == 0)
                    {
                        MibInfo[u4ParentIndex].u4FirstChild = u4Index;
                        ExtractOid (ai1LineBuffer);
                                             /*** OID ***/
                    }
                    else
                    {
                        u4LastChild = MvsMtgGetLastChild (u4ParentIndex);
                        MibInfo[u4LastChild].u4NextPeer = u4Index;
                        ExtractOid (ai1LineBuffer);
                                     /*** OID ***/
                    }
                }
            }

            u4Index++;
        }

        return SUCCESS;
    }
    else
        return FAILURE;
}

INT1
MvsMtgGenerateMibInfo (pi1FileNameList)
     INT1              **pi1FileNameList;
{
    INT1               *pi1FileName;
    UINT4               u4PrevIndex;
    UINT1               u1FirstFileFlag;
    UINT1               u1Counter;

    u4Index = 0;

    /* 
     *  Enter the Root node
     */
    MvsMtgCopyNameToMibInfo ("Root", u4Index);

    MibInfo[0].u1Syntax = BRANCH;

    u4Index++;
    MibInfo[0].u4FirstChild = u4Index;

    pi1FileName = (INT1 *) *pi1FileNameList;
    u4PrevIndex = 1;
    u1FirstFileFlag = 1;
    u1Counter = 0;
    while (pi1FileName)
    {
#ifdef DEBUG
        printf ("Processing the file %s\n", pi1FileName);
#endif

        if (u1FirstFileFlag == 0)
        {
            MibInfo[u4PrevIndex].u4NextPeer = u4Index;
            u4PrevIndex = u4Index;
        }
        else
            u1FirstFileFlag = 0;
        MvsMtgGenerateSingleMibInfo (pi1FileName);
        u1Counter++;
        pi1FileName = *(pi1FileNameList + u1Counter);
    }

}

VOID
MvsMtgGenerateSingleMibInfo (pi1FileName)
     INT1               *pi1FileName;
{
    FILE               *pInputFilePtr;

    if ((pInputFilePtr = fopen (pi1FileName, "r")) == NULL)
        MvsMtgPrintErrorAndExit ("File Open Error");

    while ((MvsMtgProcessLine (pInputFilePtr)) == SUCCESS)
        ;

    fclose (pInputFilePtr);
}

INT1
MvsMtgWriteMibInfoInFile (pi1OutputFileName, Name)
     INT1               *pi1OutputFileName;
     INT1               *Name;
{
    UINT4               u4Counter;
    FILE               *fpOut;
    INT1                i1FirstFlag = 1;
    tOID_TABLE         *p_temp;
    tOID_TABLE         *localoidptr;
    FILE               *dbfp;
    INT1                ai1dbfilename[80];

    strncpy (ai1dbfilename, Name, strlen (Name));
    ai1dbfilename[strlen (Name)] = '\0';
    strcat (ai1dbfilename, ".db");
    dbfp = fopen (ai1dbfilename, "w");
    if ((fpOut = fopen (pi1OutputFileName, "w")) == NULL)
        MvsMtgPrintErrorAndExit ("File Open Error");

    fprintf (fpOut, "# ifndef __MIBLOAD_TABLE_H__\n"
             "# define __MIBLOADTABLE_H__\n"
             "/* This file is generated by mibload utility */\n\n\n");
    fprintf (fpOut, " t_MIB_DB %s[]= {\n", Name);
    localoidptr = p_oid_list;
    for (u4Counter = 1; u4Counter < u4Index && ((p_temp = p_oid_list) != NULL);
         u4Counter++)
    {
        while (localoidptr != 0)
        {
            if (strcmp (localoidptr->object_desc, MibInfo[u4Counter].pi1Name)
                == 0)
                break;
            localoidptr = localoidptr->next;
        }
        if (MibInfo[u4Counter].u4FirstChild != 0)
            MibInfo[u4Counter].u4FirstChild--;
        if (MibInfo[u4Counter].u4NextPeer != 0)
            MibInfo[u4Counter].u4NextPeer--;
        fprintf (fpOut, " /* %2d */ { \"%s\", %d, %d, %s, %s,\"%s\" },\n",
                 u4Counter - 1,
                 MibInfo[u4Counter].pi1Name,
                 MibInfo[u4Counter].u4FirstChild,
                 MibInfo[u4Counter].u4NextPeer,
                 api1SyntaxSnmp[MibInfo[u4Counter].u1Syntax],
                 api1Access[MibInfo[u4Counter].u1Access],
                 localoidptr->object_oid);
        fprintf (dbfp, "%s:%d:%d:%d:%d:%s\n",
                 MibInfo[u4Counter].pi1Name,
                 MibInfo[u4Counter].u4FirstChild,
                 MibInfo[u4Counter].u4NextPeer,
                 api1SyntaxSnmpNumber[MibInfo[u4Counter].u1Syntax],
                 api1AccessNumber[MibInfo[u4Counter].u1Access],
                 localoidptr->object_oid);
        localoidptr = p_oid_list;
    }

    fprintf (fpOut, "{ \"NULL\",0,0,0,0,\"NULL\" } \n");
    fprintf (fpOut, "};\n\n");
    fprintf (fpOut, "UINT4   u4MibInfo = %u;\n\n", u4Counter);
    /*  fprintf( fpOut, "# endif\n" ); */
    fclose (dbfp);
    fclose (fpOut);
}

INT4
get_syntax_from_file (INT1 *ai1Syntax)
{
    FILE               *fp;
    INT1                ai1_line[80];
    INT1                ai1OldName[80];
    INT1                ai1MiddleName[80];
    INT1                ai1FourthName[80];
    INT1                ai1NewName[80];

    if ((fp = fopen (INFOFILE, "r")) == NULL)
    {
        printf ("\n Error in info.def file open");
        return FAILURE;
    }
    while (fgets (ai1_line, 80, fp) != NULL)
    {
        sscanf (ai1_line, "%s%s%s%s", ai1OldName, ai1MiddleName, ai1NewName,
                ai1FourthName);
        /* Syntax is returned only for valid OID. Ignore AUGMENT */
        if ((strcmp (ai1MiddleName, "::=") != 0)
            && (strcmp (ai1MiddleName, "OID") != 0)
            && (strcmp (ai1MiddleName, "IMPORT") != 0))
        {
            continue;
        }

        if (strcmp (ai1MiddleName, "IMPORT") == 0)
        {
            strcpy (ai1NewName, ai1FourthName);
        }

        if (strcmp (ai1Syntax, ai1OldName) == 0)
        {
            strcpy (ai1Syntax, ai1NewName);
            return SUCCESS;
        }
    }
    fclose (fp);
    return FAILURE;

}

int
main (argc, argv)
     int                 argc;
     char               *argv[];
{
    INT1                ProtoName[80];
    INT1                output_file[84];
    INT1               *Name;
    FILE               *p_in_file;
    INT4                i4_index;
    INT1                i1_line[MAX_LINE_LEN];

# ifdef DEBUG
    printf ("No of files %d\n", (argc - 1));
# endif
    if (argc <= 1)
    {

        fprintf (stderr, "\nUsage: mibload  <.def file(premosy output)>...\n");
        exit (-1);
    }

    strcpy (ProtoName, argv[1]);    /* For Getting Protocol Name --Added Kamaraj */
    Name = strtok (ProtoName, ".");

    /* store standard OID tree till the node "internet" */
    if (initialise_oid_table () < 0)
    {
        free_memory (p_oid_list);
        exit (-1);
    }

    MvsMtgGenerateMibInfo (argv + 1);

    strcpy (output_file, Name);
    strcat (output_file, "_mibload.h");
    MvsMtgWriteMibInfoInFile (output_file, Name);
}
