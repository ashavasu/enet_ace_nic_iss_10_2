/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: mibload.h,v 1.4 2007/11/15 09:45:30 iss Exp $
 * 
 * Description: This header file is used by mibload submodule
 *
 ***********************************************************************/

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <termio.h>
#include <signal.h>

#ifdef FALSE
#undef FALSE
#endif
#ifdef TRUE
#undef TRUE
#endif

typedef void VOID;
typedef char INT1;
typedef unsigned char UINT1;
typedef short INT2;
typedef unsigned short UINT2;
typedef long INT4;
typedef unsigned long UINT4;

#define  BRANCH              0x00
#define  TABLE               0x01
#define  INTEGER             0x02
#define  OCTET_STRING        0x03
#define  OBJECT_IDENTIFIER   0x04
#define  NETWORK_ADDRESS     0x05
#define  IP_ADDRESS          0x06
#define  TIME_TICKS          0x07
#define  GAUGE               0x08
#define  COUNTER             0x09
#define  OPAQUE              0x0a
#define  DISPLAYSTRING       0x0b
#define  COUNTER64           0x0c
#define  SYNTAX_NOT_DEF      0x0d

#define SYNTAX_LEAF_START        3
#define MVS_MAX_ACCESS       5

#define MVS_READ_ONLY               0x00
#define MVS_READ_WRITE              0x01
#define MVS_READ_CREATE             0x02
#define MVS_NOT_ACCESSIBLE          0x03
#define MVS_ACCESSIBLE_FOR_NOTIFY   0x04

#define MAX_SYNTAXES       512
#define MVS_MAX_TABLE_ENTRIES     5000

#define MAX_DESC_LEN 64            /* max. length of descriptor */
#define MAX_OID_LEN 8266        /* max. length of OID (127*64+10+127) */
#define ALLOC_ENTRY_MEM ((tOID_TABLE *) malloc (sizeof (tOID_TABLE)))
#define MAX_FILE_NAME_LEN 64    /* max. length of file name */

typedef enum
{
    FALSE,
    TRUE
}
t_MMI_BOOLEAN;

typedef struct MibInfo
{
    const INT1         *pi1Name;    /* Name of the MIB Object */
    UINT4         u4FirstChild;    /* Index of the first child in the next level
                                           of this object in the MIB table */
    UINT4         u4NextPeer;    /* Index of the next peer in the same level
                                       in the MIB table */
    UINT1         u1Syntax;    /* Syntax of this MIB object */
    UINT1         u1Access;    /* Access permission of this MIB Object */
    INT1               *object_oid;
}
tMibInfo;

 /****** Aded by WebNM team For Inserting OID*****/

typedef struct oid_table
{
    INT1                object_desc[MAX_DESC_LEN];
    INT1                object_oid[MAX_OID_LEN];
    struct oid_table   *next;
}
tOID_TABLE;

#define  OK                  0
#define  NOT_OK              -1

#define  VALID               1
#define  INVALID             0

#define  MAX_WORD_LEN       50
#define  MAX_LINE_LEN       200
#define  MAX_FILE_NAME_LEN  64

#define MALLOC(a)  malloc(a)
#define CALLOC(a)  calloc(a, 1)
#define MMI_FREE(a)    free(a)
#define MEMCPY(f,t,l) memcpy((t), (f), (l))

/********* Added by VELU A P *********/

#define  LINE_BUFFER_LENGTH 512
#define  NAME_BUFFER_LENGTH 256
#define  SYNTAX_BUFFER_LENGTH 48
#define  ACCESS_BUFFER_LENGTH  32
#define  MANDATORY_BUFFER_LENGTH 32

#define  FAIL    1
