/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: mibcompiler.c,v 1.3 2008/11/18 05:49:21 premap-iss Exp $
 * 
 * Description: This file has routine to generate Final MIB DataBase structure 
 *
 ***********************************************************************/

#include<stdio.h>
#include<string.h>
#include<stdlib.h>

#define MAXLEN 256
#define FILENAME 80

typedef char INT1;
typedef unsigned char UINT1;
typedef short INT2;
typedef unsigned short UINT2;
typedef int INT4;
typedef unsigned int UINT4;

INT4
main (argc, argv)
     INT4                argc;
     INT1               *argv[];
{
    FILE               *p_mibcompiler_out_file, *p_mibscan_out_file,
        *p_v2comp_out_file, *p_mibload_out_file;
    FILE               *p_v2comp_chk_file, *p_mibscan_chk_file,
        *p_mibload_chk_file;
    INT4                i4_Counter, i4_Counter1;
    INT1                i1_filename[80];
    INT1                i1_filename1[80];
    INT1                i1_filename2[80];

    INT1                i1_finalfile[FILENAME];
    INT1                i1_finalfile1[FILENAME];
    INT1                i1_finalfile2[FILENAME];
    INT1                i1_finalfile3[FILENAME];

    INT1                i1_file[FILENAME];
    INT1                i1_file1[FILENAME];
    INT1                i1_file2[FILENAME];

    INT1                i1_protoname[25];
    INT1               *pi1_name;

    INT1                i1_buffer[MAXLEN], i1_buffer1[MAXLEN];
    INT4                i4_pos1, i4_pos2, i4_pos3;
    INT1               *pi1_varptr;
    INT1                i1_variable[MAXLEN], i1_rangeIndex[MAXLEN],
        i1_enumIndex[MAXLEN];
    INT1                i1_finenum[MAXLEN], i1_finrange[MAXLEN];
                                            /** Cnage index list **/
    INT1                i1_command1[80], i1_command2[80], i1_command3[80],
        i1_command[80];

    if (argc < 1)
    {

        fprintf (stderr, "\nError Usage:    \n");
        exit (-1);
    }

    if (argc == 1)
    {

        fprintf (stderr, "\nUsage: mibcompiler  <source mib file>...\n");
        exit (1);
    }

    strcpy (i1_protoname, argv[1]);    /* For Getting Protocol Name */
    pi1_name = strtok (i1_protoname, ".");

    strcpy (i1_finenum, "&");
    strcat (i1_finenum, pi1_name);
    strcat (i1_finenum, "_index_list[");

    strcpy (i1_finrange, "&");
    strcat (i1_finrange, pi1_name);
    strcat (i1_finrange, "Range[");

    /** For v2comp **/
    strcpy (i1_file, argv[1]);
    strcat (i1_file, ".def");

    strcpy (i1_filename1, pi1_name);
    strcat (i1_filename1, ".def");
    strcpy (i1_filename, pi1_name);
    strcat (i1_filename, ".h");

    strcpy (i1_filename2, pi1_name);
    strcat (i1_filename2, "_enum.h");
    /** v2comp End **/

/*** For getting File names **/

    strcpy (i1_finalfile, pi1_name);
    strcat (i1_finalfile, "_final_db.h");

    strcpy (i1_finalfile1, pi1_name);
    strcat (i1_finalfile1, "_enum.h");
    strcpy (i1_file1, i1_finalfile1);
                                  /** For mibscan **/

    strcpy (i1_finalfile2, pi1_name);
    strcat (i1_finalfile2, "_range.h");

    strcpy (i1_finalfile3, pi1_name);
    strcat (i1_finalfile3, "_mibload.h");
    strcpy (i1_file2, i1_finalfile3);
                                  /** For mibload **/

/*** File Name ***/

/*** For Excuting File ***/

    strcpy (i1_command1, "./v2comp ");
    strcat (i1_command1, argv[1]);
    system (i1_command1);

    if ((p_v2comp_chk_file = fopen (i1_file, "r")) == NULL)
    {
        printf ("\n Error While Compiling with v2comp tool....\n");
        exit (1);
    }

    fclose (p_v2comp_chk_file);

/* strcpy( i1_command3, "./mibscan -c " );
strcat( i1_command3, argv[1] );
system(i1_command3);

if((p_mibscan_chk_file =fopen( i1_file1,"r")) == NULL)
{
        printf("\n Error While Compiling with mibscan tool ....\n");
    system("rm core");
    system("rm mmi_0*.* ");
        exit(1);
} 

fclose(p_mibscan_chk_file); */

    strcpy (i1_command2, "./mibload ");
    strcat (i1_command2, argv[1]);
    strcat (i1_command2, ".def");
    system (i1_command2);

    if ((p_mibload_chk_file = fopen (i1_file2, "r")) == NULL)
    {
        printf ("\n Error While Compiling with mibload tool ....\n");
        exit (1);
    }
    fclose (p_mibload_chk_file);

/*** File Excuting Over ***/

    printf ("\n Processing ....... ");

    p_mibcompiler_out_file = fopen (i1_finalfile, "w");
    fprintf (p_mibcompiler_out_file, "\n\n");
    fprintf (p_mibcompiler_out_file, "#ifdef  NM_WANTED \n");
    fprintf (p_mibcompiler_out_file, "#include \"nm.h\"  \n\n");

    if ((p_mibscan_out_file = fopen (i1_filename2, "r")) == NULL)
    {
        printf ("\n No %s file  ....Quit\n Run mibscan tool \n", i1_finalfile1);
        exit (1);
    }

    if ((p_v2comp_out_file = fopen (i1_finalfile2, "r")) == NULL)
    {
        printf ("\n No %s file \n ....Quit\n Run v2comp tool", i1_finalfile2);
        exit (1);
    }

    if ((p_mibload_out_file = fopen (i1_finalfile3, "r")) == NULL)
    {
        printf ("\n No %s file \n ....Quit\n Run mibload tool", i1_finalfile3);
        exit (1);
    }
/*** File ***/

    while (!feof (p_mibscan_out_file))
    {
        fgets (i1_buffer, MAXLEN, p_mibscan_out_file);
        if (strstr (i1_buffer, "t_MMI_INDEX_DESC"))
            break;
    }
    while (!strstr (i1_buffer, "End_Enum"))
    {
        fputs (i1_buffer, p_mibcompiler_out_file);
        fgets (i1_buffer, MAXLEN, p_mibscan_out_file);
    }
    fputs (i1_buffer, p_mibcompiler_out_file);
    fputs ("\n\n", p_mibcompiler_out_file);

    while (!feof (p_v2comp_out_file))
    {
        fgets (i1_buffer, MAXLEN, p_v2comp_out_file);
        if (strstr (i1_buffer, "t_MB_DB_Range"))
            break;
    }
    while (!strstr (i1_buffer, "End_Range"))
    {
        fputs (i1_buffer, p_mibcompiler_out_file);
        fgets (i1_buffer, MAXLEN, p_v2comp_out_file);
    }
    fputs (i1_buffer, p_mibcompiler_out_file);
    fputs ("\n\n", p_mibcompiler_out_file);
 /***/

    rewind (p_mibscan_out_file);
    rewind (p_v2comp_out_file);

    while (!feof (p_mibscan_out_file))
    {
        fgets (i1_buffer, MAXLEN, p_mibscan_out_file);
        if (strstr (i1_buffer, "t_MMI_VAR_DESC"))
            break;
    }

    i4_pos1 = ftell (p_mibscan_out_file);

    while (!feof (p_v2comp_out_file))
    {
        fgets (i1_buffer, MAXLEN, p_v2comp_out_file);
        if (strstr (i1_buffer, "t_MB_DB_Range"))
            break;
    }

    i4_pos2 = ftell (p_v2comp_out_file);

    while (!feof (p_mibload_out_file))
    {
        fgets (i1_buffer, MAXLEN, p_mibload_out_file);
        if (strstr (i1_buffer, "t_MIB_DB"))
            break;
    }

    fputs (i1_buffer, p_mibcompiler_out_file);

    while (!feof (p_mibload_out_file))
    {
        fgets (i1_buffer, MAXLEN, p_mibload_out_file);
/***    pi1_varptr=strtok(i1_buffer,"\""); ***/
        for (i4_Counter1 = 0; i1_buffer[i4_Counter1] != '\"'; i4_Counter1++);
        pi1_varptr = &i1_buffer[i4_Counter1];
        for (i4_Counter = 1; pi1_varptr[i4_Counter] != '\"'; i4_Counter++)
            i1_variable[i4_Counter - 1] = pi1_varptr[i4_Counter];
        i1_variable[i4_Counter - 1] = ',';
        i1_variable[i4_Counter] = '\0';
        if (strcmp (i1_variable, "NULL,") == 0)
        {
            fputs ("{ \"NULL\",0,0,0,0,\"NULL\",NULL,NULL}\n};",
                   p_mibcompiler_out_file);
            fputs ("\n\n", p_mibcompiler_out_file);
            break;
        }
        fseek (p_mibscan_out_file, i4_pos1, SEEK_SET);
        while (!feof (p_mibscan_out_file))
        {
            fgets (i1_buffer1, MAXLEN, p_mibscan_out_file);
            if (strstr (i1_buffer1, i1_variable))
                break;

        }
        if (feof (p_mibscan_out_file))
            strcpy (i1_enumIndex, "-1");
        else
        {
    /**    pi1_varptr=strtok(i1_buffer1,","); **/

            for (i4_Counter1 = 0; i1_buffer1[i4_Counter1] != ',';
                 i4_Counter1++);
            pi1_varptr = &i1_buffer1[i4_Counter1];

            for (i4_Counter1 = 1; pi1_varptr[i4_Counter1] != '}'; i4_Counter1++)
                i1_enumIndex[i4_Counter1 - 1] = pi1_varptr[i4_Counter1];
            i1_enumIndex[i4_Counter1 - 1] = '\0';
        }
        fseek (p_v2comp_out_file, i4_pos2, SEEK_SET);
        i1_variable[i4_Counter - 1] = '\"';
        i1_variable[i4_Counter] = '\0';
        while (!feof (p_v2comp_out_file))
        {
            fgets (i1_buffer1, MAXLEN, p_v2comp_out_file);
            if (strstr (i1_buffer1, i1_variable))
                break;
        }
        if (feof (p_v2comp_out_file))
            strcpy (i1_rangeIndex, "-1,");
        else
        {

            for (i4_Counter1 = 0; i1_buffer1[i4_Counter1] != '{';
                 i4_Counter1++);
            pi1_varptr = &i1_buffer1[i4_Counter1];

            for (i4_Counter1 = 1; pi1_varptr[i4_Counter1] != ','; i4_Counter1++)
                i1_rangeIndex[i4_Counter1 - 1] = pi1_varptr[i4_Counter1];
            i1_rangeIndex[i4_Counter1 - 1] = '\0';
/***    i1_rangeIndex[i4_Counter1]='\0';***saleem***/
        }

        for (i4_Counter1 = 0; i1_buffer[i4_Counter1] != '}'; i4_Counter1++);
        pi1_varptr = &i1_buffer[i4_Counter1];
        pi1_varptr[0] = ',';
        pi1_varptr[1] = '\0';

        if (strstr (i1_rangeIndex, "-1"))
        {
            strcat (i1_buffer, "NULL,");
            if (strstr (i1_enumIndex, "-1"))
            {
                strcat (i1_buffer, "NULL");

            }
            else
            {
                strcat (i1_buffer, i1_finenum);
                strcat (i1_buffer, i1_enumIndex);
                strcat (i1_buffer, "]");
            }

        }
        else
        {
            strcat (i1_buffer, i1_finrange);
            strcat (i1_buffer, i1_rangeIndex);
            strcat (i1_buffer, "],");
            strcat (i1_buffer, "NULL");

        }

        strcat (i1_buffer, "},\n");
        fputs (i1_buffer, p_mibcompiler_out_file);

    }

    fputs ("\n", p_mibcompiler_out_file);
    fputs ("#endif", p_mibcompiler_out_file);
    fputs ("\n\n", p_mibcompiler_out_file);

    printf ("...Done \n");

    fclose (p_mibcompiler_out_file);
    fclose (p_mibscan_out_file);
    fclose (p_v2comp_out_file);
    fclose (p_mibload_out_file);

    unlink (i1_file);
    unlink (i1_filename);
    unlink (i1_filename1);
    unlink (i1_finalfile1);
    unlink (i1_finalfile2);
    unlink (i1_finalfile3);

}
