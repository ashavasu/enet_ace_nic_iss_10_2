## $Id: README,v 1.8 2011/10/04 10:32:32 siva Exp $  ##
	The Mibcomplier Utility can be generated as below:

   A) give "./make_all" from this directory for making v2comp, mibload, and
      mibcompiler tools.
   
                  ( or )
      
   B) alternatively you may follow the following steps ( B-1 to B-4 )
      for building the tools individually.
      

   1. goto the MIBCOMPILER directory.
   2. ./mk_mibcompiler command will generate the mibcompiler utility.


	The Mibcomplier Utility generates the Final MIB database structure in
   header file. This Header file is included in the final integration
   Stage.

      mibcompiler < Input MIB Filename >

   The user should have the following files in the MIBCOMPILER directory :

   3. v2comp

      The v2comp utility is generated with the Makefile available in the 
      corresponding directory. The utility generated in the v2comp/exe path
      should be copied into the MIBCOMPILER directory.

   4. Mibload

      The mibload utility is generated with the Makefile available in the 
      corresponding directory. The utility generated in the mibload/exe path
      should be copied into the MIBCOMPILER directory.

   5. MIB File

      The MIB file fow which the final "db" file has to be generated.

   4. info.def File
	
   The user should make sure that sufficient disk space is available in the
   hard disk.  The disk space required is dependent on the input from the
   MIB file.

   The user should ensure that permissions for creating the directories
   and files are allowed.

   New data types may be defined in MIB or imported from other MIB modules.
   For mibcompiler to process the input, it needs all objects syntax types
   to be known.

   All syntax types that are imported must be entered in the info.def file.

      Example:

         mibcompiler ospf.mib    Creates ospf.db


   mibcompiler will internally call the v2comp and mibload tools for final 
   database.

   In case of any error, it will report the user in the tool which had caused 
   the error.


NOTE:
=====
Due to tool restriction, following changes to be made:

stdbrgext.mib
-------------

1. Add the following Under IMPORTS,
   
   dot1dRegenUserPriority
       FROM P-BRIDGE-MIB;


2. Line number: 20
   MODULE-IDENTITY of pBridgeMIB has to be changed from 

   {dot1dBridge 6 } to  { mib-2 17 6 }  

3. Include the assignment for the oid dot1dTp as shown below at line number 40.

   dot1dTp OBJECT IDENTIFIER ::= { mib-2 17 4 }


stdvlan.mib
-----------
1. Add the following Lines Under IMPORTS

   dot1qVlanIndex
   	   FROM Q-BRIDGE-MIB

2. Line number: 44
   MODULE-IDENTITY of QBridgeMIB has to be changed from 

   { dot1dBridge 7 } to  { mib-2 17 7 }

 
stdrst.mib
----------
1. Add the following assignment at line number : 36

   dot1dBridge          OBJECT IDENTIFIER ::= { mib-2 17}

     
stdsnnot.mib
------------
1.Line Number:16
  Under IMPORTS
   Include the following lines

         snmpNotifyFilterProfileName
             FROM SNMP-NOTIFICATION-MIB
                   
stdsnusm.mib
------------
1. Line Number:84
   Under Administrative assignments 
    Include the following lines
   
        snmpAuthProtocols OBJECT IDENTIFIER ::= { snmpModules 10 1 1 }
        snmpPrivProtocols OBJECT IDENTIFIER ::= { snmpModules 10 1 2 }

fsdot1ad.mib
------------
1. (i) Change the INDEX of dot1adPepEntry as following:
        INDEX {dot1adPortNum,
               dot1adPepSVid
              }
   (ii) Add dot1adPepSVid object to Dot1adPepEntry as following:
   Dot1adPepEntry ::=
   SEQUENCE {
       dot1adPepPvid                VlanId,
       dot1adPepDefaultUserPriority INTEGER,
       dot1adPepAccptableFrameTypes INTEGER,
       dot1adPepIngressFiltering    TruthValue,
       dot1adPepSVid                VlanId
   }

   (iii) Add desctription for the object dot1adPepSVid as following:
   dot1adPepSVid OBJECT-TYPE
   SYNTAX      VlanId
   MAX-ACCESS  not-accessible
   STATUS      current
   DESCRIPTION
   "S-VLAN Id for the service instance to which this port and this
   C-VLAN are mapped to.
   ie) CEP,C-VID ===> S-VID."
   ::= { dot1adPepEntry  5 }

   (iv) Don't baseline the above changes. These changes are required only
        for generating the fsdot1ad.db file. The tool has to be updated for
        this (not the mibs).

2. (i) Change the INDEX of dot1adServicePriorityRegenerationEntry as following:
     INDEX { dot1adPortNum,
         dot1adSpSVid,
         dot1adServicePriorityRegenReceivedPriority }

   (ii) Add dot1adSpSVid object to Dot1adServicePriorityRegenerationEntry as 
   following:
   Dot1adServicePriorityRegenerationEntry  ::=
   SEQUENCE {
       dot1adServicePriorityRegenReceivedPriority      INTEGER,
       dot1adServicePriorityRegenRegeneratedPriority   INTEGER,
       dot1adSpSVid                                    VlanId
   }

   (iii) Add desctription for the object dot1adSpSVid as following:
   dot1adSpSVid OBJECT-TYPE
   SYNTAX      VlanId
   MAX-ACCESS  not-accessible
   STATUS      current
   DESCRIPTION
   "S-VLAN Id for the service instance to which this port and this
   C-VLAN are mapped to.
   ie) CEP,C-VID ===> S-VID."
   ::= { dot1adServicePriorityRegenerationEntry  3 }

   (iv) Don't baseline the above changes. These changes are required only
        for generating the fsdot1ad.db file. The tool has to be updated for
        this (not the mibs).
                             
fsmiy1731.mib
-------------
1. Remove one curly braces from the Line number 1463. i.e change it from  DEFVAL {{ useHWonly }}
   to DEFVAL { useHWonly }      
                        
fsm1ad.mib
----------
1. (i) Change the INDEX of dot1adMIPepEntry as following:
        INDEX {dot1adMIPortNum,
               dot1adMIPepSVid
              }
   (ii) Add dot1adPepSVid object to Dot1adMIPepEntry as following:
   Dot1adMIPepEntry ::=
   SEQUENCE {
       dot1adMIPepPvid                VlanId,
       dot1adMIPepDefaultUserPriority INTEGER,
       dot1adMIPepAccptableFrameTypes INTEGER,
       dot1adMIPepIngressFiltering    TruthValue,
       dot1adMIPepSVid                VlanId
   }

   (iii) Add desctription for the object dot1adMIPepSVid as following:
   dot1adMIPepSVid OBJECT-TYPE
   SYNTAX      VlanId
   MAX-ACCESS  not-accessible
   STATUS      current
   DESCRIPTION
   "S-VLAN Id for the service instance to which this port and this
   C-VLAN are mapped to.
   ie) CEP,C-VID ===> S-VID."
   ::= { dot1adMIPepEntry  5 }

   (iv) Don't baseline the above changes. These changes are required only
        for generating the fsdot1ad.db file. The tool has to be updated for
        this (not the mibs).

2. (i) Change the INDEX of dot1adMIServicePriorityRegenerationEntry as 
     following:
     INDEX { dot1adMIPortNum,
         dot1adMISpSVid,
         dot1adMIServicePriorityRegenReceivedPriority }

   (ii) Add dot1adMISpSVid object to Dot1adServicePriorityRegenerationEntry as 
   following:
   Dot1adMIServicePriorityRegenerationEntry  ::=
   SEQUENCE {
       dot1adMIServicePriorityRegenReceivedPriority      INTEGER,
       dot1adMIServicePriorityRegenRegeneratedPriority   INTEGER,
       dot1adMISpSVid                                    VlanId
   }

   (iii) Add desctription for the object dot1adMISpSVid as following:
   dot1adMISpSVid OBJECT-TYPE
   SYNTAX      VlanId
   MAX-ACCESS  not-accessible
   STATUS      current
   DESCRIPTION
   "S-VLAN Id for the service instance to which this port and this
   C-VLAN are mapped to.
   ie) CEP,C-VID ===> S-VID."
   ::= { dot1adMIServicePriorityRegenerationEntry  3 }

   (iv) Don't baseline the above changes. These changes are required only
        for generating the fsdot1ad.db file. The tool has to be updated for
        this (not the mibs).

fsmsbext.mib
------------
1. Line number: 40
   Change { fsDot1dBridge 6 } to { enterprises futuresoftware(2076) 116 6 }

2. Add the following assignment in Line Number: 41

   fsDot1dTp OBJECT IDENTIFIER ::= { enterprises futuresoftware(2076) 116 2}

3. (i) Change the INDEX of fsDot1dPortOutboundAccessPriorityEntry as 
     following:
     INDEX   { fsDot1dBasePort, fsDot1dTRegenUserPriority }

   (ii) Add fsDot1dTRegenUserPriority object to fsDot1dPortOutboundAccessPriorityEntry as 
   following:
  
   FsDot1dPortOutboundAccessPriorityEntry ::=
      SEQUENCE {
          fsDot1dPortOutboundAccessPriority
             INTEGER,
          fsDot1dTRegenUserPriority INTEGER
   }

   (iii) Add desctription for the object as following:

   fsDot1dTRegenUserPriority OBJECT-TYPE
     SYNTAX      INTEGER (0..7)
     MAX-ACCESS  read-write
     STATUS      current
     DESCRIPTION
      "The Regenerated User Priority the incoming User
      Priority is mapped to for this port."
     ::= { fsDot1dPortOutboundAccessPriorityEntry 2 }

   (iv) Don't baseline the above changes. These changes are required only
        for generating the fsmsbext.db file. The tool has to be updated for
        this (not the mibs).

fsmsrst.mib
------------
1. Line number: 35
   Change { fsDot1dBridge 11 } to { enterprises futuresoftware(2076) 116 11 }

2. Uncomment the following line at Line number : 44
   --- fsDot1dStp    OBJECT IDENTIFIER ::= { fsRstpMIBObjects 1 }


fsmsvlan.mib
------------
1. Line number: 39
   Change { fsDot1dBridge 7 } to { enterprises futuresoftware(2076) 116 7 }

ospf3.mib
---------
1. Line number: 2517
   Comment { ipv6(2) } Portion in InetAddressType.
2. Line number: 2523
   Comment { ipv6(2) } Portion in InetAddressType.

stdecfm.mib
-----------
1. Remove one curly braces from the Line number 2416. i.e change it from  DEFVAL {{ useFDBonly }}
   to DEFVAL { useFDBonly }

stdgnldp.mib
------------
1. Line number: 62
   Change { mplsStdMIB 7 } to { transmission 166 7 }
2. Line number: 262
   Comment { active(1) } Portion in RowStatus.
3. Line number: 263
   Comment { createAndGo(4), destroy(6) } in RowStatus.
4. Line number: 301
   Comment { active(1) } in RowStatus.

stdldp.mib
------------
1. Line number: 74
   Change { mplsStdMIB 4 } to { transmission 166 4 }
2. Line number: 2080
   Comment { unknown(0), ipv4(1), ipv6(2) } Portion in InetAddressType.
3. Line number: 2092
   Comment { active(1) } in RowStatus.
4. Line number: 2093
   Comment { createAndGo(4), destroy(6) } in RowStatus.
5. Line number: 2099
   Comment { unknown(0), ipv4(1), ipv6(2) } Portion in InetAddressType.
6. Line number: 2126
   Comment { unknown(0), ipv4(1), ipv6(2) } Portion in InetAddressType.
7. Line number: 2233
   Comment { unknown(0), ipv4(1), ipv6(2) } Portion in InetAddressType.
8. Line number: 2259
   Comment { active(1) } in RowStatus.
9. Line number: 2277
   Comment { unknown(0), ipv4(1), ipv6(2) } Portion in InetAddressType.
10. Line number: 2298
   Comment { active(1) } in RowStatus.
11. Line number: 2310
    Comment { active(1) } in RowStatus.
12. Line number: 2317
    Comment { unknown(0), ipv4(1), ipv6(2) } Portion in InetAddressType.

stdlldp.mib
-----------
1. Line Number: 633
   Comment the DEFVAL for the Object lldpPortConfigTLVsTxEnable.
2. Copy the Entire Table lldpLocManAddrTable and its objects and paste 
   it before the Table lldpConfigManAddrTable. This is because
   lldpConfigManAddrTable AUGMENTS lldpLocManAddrTable. Tool should be updated
   for this. So, don't baseline these changes.

stdmpftn.mib
----------- 
1. Line number: 71
   Change { mplsStdMIB 8 } to { transmission 166 8 }
2. Line number: 649
   Comment { active(1), createAndGo(4), destroy(6)} Portion in RowStatus.
3. Line number: 806
   Comment { ipv4(1), ipv6(2) } in InetAddressType.
4. Line number: 876
   Comment { active(1) } in RowStatus.
5. Line number: 893
   Comment { ipv4(1), ipv6(2) } Portion in InetAddressType.
6. Line number: 975
   Comment { active(1) } in RowStatus.

stdmplsr.mib
------------
1. Line number: 63
   Change { mplsStdMIB 2 } to { transmission 166 2 }
2. Line number: 1779
   Comment { active(1), notInService(2) } Portion in RowStatus.
3. Line number: 1780
   Comment { active(1), notInService(2), createAndGo(4), destroy(6) } in RowStatus.
4. Line number: 1787
   Comment { unknown(0), ipv4(1), ipv6(2) } in InetAddressType.
5. Line number: 1797
   Comment { active(1), notInService(2) } Portion in RowStatus.
6. Line number: 1798
   Comment { active(1), notInService(2), createAndGo(4), destroy(6) } 
   Portion in InetAddressType.
7. Line number: 1805
   Comment { active(1), notInService(2) } in RowStatus.
8. Line number: 1806
   Comment { active(1), notInService(2), createAndGo(4), destroy(6) } Portion in 
   InetAddressType.
9. Line number: 1813
   Comment { active(1), notInService(2) } in RowStatus.
10. Line number: 1814
    Comment { active(1), notInService(2), createAndGo(4), destroy(6) } in RowStatus.
11. Line number: 1898
    Comment { active(1) } Portion in RowStatus.
12. Line number: 1924
    Comment { unknown(0), ipv4(1), ipv6(2) } in InetAddressType.
13. Line number: 1938
    Comment { active(1) } in RowStatus.
14. Line number: 1956
    Comment { active(1) } in RowStatus.

stdpwe.mib
---------
1. Line number: 486
   Comment { active(1), notInService(2), notReady(3) } Portion in RowStatus.
2. Line number: 488
   Comment { active(1), notInService(2), notReady(3) } Portion in RowStatus.
3. Line number: 500
   Comment { active(1), notInService(2), notReady(3) } Portion in RowStatus.
4. Line number: 502
   Comment { active(1), notInService(2), notReady(3) } Portion in RowStatus.
5. Line number: 559
   Comment { active(1), notInService(2), notReady(3) } Portion in RowStatus.
6. Line number: 575
   Comment { active(1), notInService(2), notReady(3) } Portion in RowStatus.

stdpw.mib
---------
1. Line number: 1647
   Comment { active(1), notInService(2), notReady(3) } Portion in RowStatus.
2. Line number: 1649
   Comment { active(1), notInService(2), notReady(3) } Portion in RowStatus.
3. Line number: 1663
   Comment { unknown(0), ipv4(1) } in InetAddressType.
4. Line number: 1786
   Comment { unknown(0), ipv4(1) } in InetAddressType.
5. Line number: 1877
   Comment { active(1) } in RowStatus. 


stdpwm.mib
-----------
1. Remove one curly braces from the Line number 2416. i.e change it from  DEFVAL { { mplsNonTe } }
   to DEFVAL { mplsNonTe }

stdvacm.mib
------------
1. (i) Change the INDEX of vacmAccessEntry as 
     following:
     INDEX       { vacmAccessGroupName,
                   vacmAccessContextPrefix,
                   vacmAccessSecurityModel,
                   vacmAccessSecurityLevel
     }

   (ii) Add vacmAccessGroupName object to vacmAccessEntry as 
   following:

   VacmAccessEntry ::= SEQUENCE
    {
        vacmAccessContextPrefix    SnmpAdminString,
        vacmAccessSecurityModel    SnmpSecurityModel,
        vacmAccessSecurityLevel    SnmpSecurityLevel,
        vacmAccessContextMatch     INTEGER,
        vacmAccessReadViewName     SnmpAdminString,
        vacmAccessWriteViewName    SnmpAdminString,

        vacmAccessNotifyViewName   SnmpAdminString,
        vacmAccessStorageType      StorageType,
        vacmAccessStatus           RowStatus,
        vacmAccessGroupName        SnmpAdminString
    }
  
   (iii) Add desctription for the object as following:

   vacmAccessGroupName    OBJECT-TYPE
    SYNTAX       SnmpAdminString (SIZE(1..32))
    MAX-ACCESS   read-create
    STATUS       current
    DESCRIPTION "The name of the group to which this entry (e.g., the
                 combination of securityModel and securityName)
                 belongs.

                 This groupName is used as index into the
                 vacmAccessTable to select an access control policy.
                 However, a value in this table does not imply that an
                 instance with the value exists in table vacmAccesTable.
                "
    ::= { vacmAccessEntry 10 }
   
   (iv) Don't baseline the above changes. These changes are required only
        for generating the stdvacm.db file. The tool has to be updated for
        this (not the mibs).

12.In stdmgmd.mib,
     Line number: 130,296,555,666,835,901,958,1049
<<    SYNTAX     InetAddressType { ipv4(1), ipv6(2) }
>>    SYNTAX     InetAddressType

Don't Compile stdmptc.mib with this mibcompiler tool since it is textual-convention
mib.

Also, don't compile stdldatm.mib with this mibcompiler tool as we do not support 
this mib.

info.def
--------

The info.def file in the path future/ISS/tool/mibcompiler/MIBCOMPILER/ contains the syntax type OCTET-STRING.

The info.def file in the path future/snmp_2/tools/midgen_2/exe contains the syntax type OCTET STRING.

Both these info.def files are identical except for the difference in the syntax type. The user must ensure that the correct info.def file is present in the corresponding directory to avoid compilation problems.

 
---------------------------------------------------------------------------
