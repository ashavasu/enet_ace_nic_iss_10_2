/* $Id: aclcli.c,v 1.8 2013/02/15 13:24:03 siva Exp $ */
/************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                 */
/* Licensee Aricent Inc., 2004-2005       */
/*                                                          */
/*  FILE NAME             : aclcli.c                        */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                 */
/*  SUBSYSTEM NAME        : ISS                             */
/*  MODULE NAME           : CLI                             */
/*  LANGUAGE              : C                               */
/*  TARGET ENVIRONMENT    :                                 */
/*  DATE OF FIRST RELEASE :                                 */
/*  AUTHOR                : Aricent Inc.                 */
/*  DESCRIPTION           : This file contains CLI routines */
/*                          related to system commands      */
/*                                                          */
/************************************************************/
#ifndef __ACLCLI_C__
#define __ACLCLI_C__

#include "lr.h"
#include "issexinc.h"
#include "fsisselw.h"
#include "aclmrvlcli.h"
#include "isscli.h"
#include "aclmcli.h"
#include "fsissewr.h"
#include "fsissecli.h"

/***************************************************************/
/*  Function Name   : cli_process_acl_cmd                      */
/*  Description     : This function servers as the handler for */
/*                    all system related CLI commands          */
/*  Input(s)        :                                          */
/*                    CliHandle - CLI Handle                   */
/*                    u4Command - Command given by user        */
/*                    Variable set of inputs depending on      */
/*                    the user command                         */
/*  Output(s)       : Error message - on failure               */
/*  Returns         : None                                     */
/***************************************************************/
INT4
cli_process_acl_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[CLI_ACL_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    INT4                i4Action;
    INT4                i4FilterNo = 0;
    INT4                i4FilterType;
    tMacAddr            DestMacAddr;
    tMacAddr            SrcMacAddr;
    UINT4               u4VlanId = 0;
    INT4                i4tmpFilterNo = 0;
    va_start (ap, u4Command);

    /* Third arguement is always interface name/index */
    va_arg (ap, UINT1 *);

    /* Walk through the rest of the arguements and store in args array.  */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == CLI_ACL_MAX_ARGS)
            break;
    }

    va_end (ap);

    CLI_SET_ERR (0);

    CliRegisterLock (CliHandle, IssLock, IssUnLock);

    ISS_LOCK ();

    switch (u4Command)
    {
        case CLI_MAC_ACL:
            MEMCPY (&i4tmpFilterNo, args[0], sizeof (INT4));
            i4RetStatus = AclCreateMacFilter (CliHandle, i4tmpFilterNo);
            break;

        case CLI_NO_MAC_ACL:
            MEMCPY (&i4tmpFilterNo, args[0], sizeof (INT4));
            i4RetStatus = AclDestroyMacFilter (CliHandle, i4tmpFilterNo);
            break;

        case CLI_PERMIT_DST_MAC_ACL:
        case CLI_DENY_DST_MAC_ACL:

            if (u4Command == CLI_PERMIT_DST_MAC_ACL)
            {
                i4Action = ISS_ALLOW;
            }
            else
            {
                i4Action = ISS_DROP;
            }

            if (args[1] != NULL)
            {
                MEMCPY (&u4VlanId, args[1], sizeof (UINT4));
            }

            /* Get the Source and destination MAC address */

            if ((UINT1 *) (args[0]) != NULL)
            {
                StrToMac (args[0], DestMacAddr);
            }

            i4RetStatus =
                AclExtDstMacFilterConfig (CliHandle, i4Action,
                                          DestMacAddr, u4VlanId);

            break;

        case CLI_PERMIT_SRC_MAC_ACL:
        case CLI_DENY_SRC_MAC_ACL:

            if (u4Command == CLI_PERMIT_SRC_MAC_ACL)
            {
                i4Action = ISS_ALLOW;
            }
            else
            {
                i4Action = ISS_DROP;
            }

            if (args[1] != NULL)
            {
                MEMCPY (&u4VlanId, args[1], sizeof (UINT4));
            }

            /* Get the Source and destination MAC address */

            if ((UINT1 *) (args[0]) != NULL)
            {
                StrToMac (args[0], SrcMacAddr);
            }

            i4RetStatus =
                AclExtSrcMacFilterConfig (CliHandle, i4Action,
                                          SrcMacAddr, u4VlanId);
            break;

        case CLI_ACL_SHOW:
            i4FilterType = CLI_PTR_TO_I4 (args[0]);
            if (args[1] != NULL)
            {
                MEMCPY (&i4FilterNo, args[1], sizeof (INT4));
            }
            i4RetStatus =
                AclShowAccessLists (CliHandle, i4FilterType, i4FilterNo);
            break;

    }

    if ((CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_ACL_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", AclCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }

    CliUnRegisterLock (CliHandle);

    ISS_UNLOCK ();

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclCreateMacFilter                                 */
/*                                                                           */
/*     DESCRIPTION      : This function creates an MAC ACL filter and enters */
/*                        the corresponding configuration mode               */
/*                                                                           */
/*     INPUT            : i4Filterno - MAC ACL number                        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
AclCreateMacFilter (tCliHandle CliHandle, INT4 i4FilterNo)
{
    INT4                i4Status;
    UINT4               u4ErrCode;
    UINT1               au1IfName[ACL_MAX_NAME_LENGTH];

    /* if filter already exists, do nothing and enter the configuration mode */

    if (nmhGetIssExtL2FilterStatus (i4FilterNo, &i4Status) == SNMP_FAILURE)
    {
        /* Create a filter */
        if (nmhTestv2IssExtL2FilterStatus (&u4ErrCode,
                                           i4FilterNo,
                                           ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }

    /* Enter MAC ACL configuration mode */

    /* Set the mode information for CLI */
    CLI_SET_MACACL (i4FilterNo);
    SNPRINTF ((CHR1 *) au1IfName, ACL_MAX_NAME_LENGTH, "%s%d",
              CLI_MACACL_MODE, i4FilterNo);
    /* Return ACL configuration prompt */
    CliChangePath ((CHR1 *) au1IfName);

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclDestroyMacFilter                                */
/*                                                                           */
/*     DESCRIPTION      : This function destroys a MAC ACL filter and enters */
/*                        the corresponding configuration mode               */
/*                                                                           */
/*     INPUT            : i4Filterno - MAC ACL number                        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
AclDestroyMacFilter (tCliHandle CliHandle, INT4 i4FilterNo)
{
    INT4                i4Status;
    UINT4               u4ErrCode;

    if (nmhGetIssExtL2FilterStatus (i4FilterNo, &i4Status) == SNMP_FAILURE)
    {
        /* Filter does not exist */
        CliPrintf (CliHandle, "\r%% Invalid Filter number \r\n");
        return (CLI_FAILURE);
    }

    else

    {
        if (nmhTestv2IssExtL2FilterStatus (&u4ErrCode,
                                           i4FilterNo,
                                           ISS_DESTROY) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclShowAccessLists                                 */
/*                                                                           */
/*     DESCRIPTION      : This function displays all the acls conifgured in  */
/*                        switch                                             */
/*                                                                           */
/*     INPUT            : CliHandle - CLI HAndler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclShowAccessLists (tCliHandle CliHandle, INT4 i4FilterType, INT4 i4FilterNo)
{
    INT4                i4NextFilter;
    INT4                i4PrevFilter;
    UINT1               u1Quit = CLI_SUCCESS;

    if (i4FilterNo != 0)
    {
        if (i4FilterType == CLI_MAC_ACL)
        {
            if (AclShowL2Filter (CliHandle, i4FilterNo) == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nInvalid Mac Access List \r\n");
                return CLI_FAILURE;
            }
        }
        else
        {
            if (AclShowL2Filter (CliHandle, i4FilterNo) == CLI_SUCCESS)
            {
                u1Quit = CLI_SUCCESS;
            }
            if (u1Quit == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nInvalid Access List \r\n");
                return CLI_FAILURE;
            }
        }
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\nMAC ACCESS LISTS\r\n");
    CliPrintf (CliHandle, "-----------------\r\n");

    if (nmhGetFirstIndexIssExtL2FilterTable (&i4NextFilter) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n No MAC Access Lists have been configured\r\n\r\n");
    }
    else
    {
        do
        {
            AclShowL2Filter (CliHandle, i4NextFilter);
            i4PrevFilter = i4NextFilter;
            u1Quit = (UINT1) CliPrintf (CliHandle, "\r\n\r\n");
        }
        while ((INT4) (nmhGetNextIndexIssExtL2FilterTable
                       (i4PrevFilter, &i4NextFilter) != SNMP_FAILURE)
               && (u1Quit != CLI_FAILURE));

        if (u1Quit == CLI_FAILURE)
        {
            return (u1Quit);
        }
    }
    return (u1Quit);
}

INT4
AclShowL2Filter (tCliHandle CliHandle, INT4 i4NextFilter)
{
    tMacAddr            DstMacAddr;
    tMacAddr            SrcMacAddr;
    INT4                i4VlanId;
    INT4                i4FltrAction;
    INT4                i4RowStatus;
    CHR1               *pu1String;
    INT1               *piIfName;
    UINT1               au1String[ISS_MAX_LEN];
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];

    pu1String = (CHR1 *) & au1String[0];
    piIfName = (INT1 *) &au1IfName[0];

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1String, 0, ISS_MAX_LEN);

    if (nmhValidateIndexInstanceIssExtL2FilterTable (i4NextFilter) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    nmhGetIssExtL2FilterVlanId (i4NextFilter, &i4VlanId);
    nmhGetIssExtL2FilterDstMacAddr (i4NextFilter, (tMacAddr *) & DstMacAddr);
    nmhGetIssExtL2FilterSrcMacAddr (i4NextFilter, (tMacAddr *) & SrcMacAddr);

    nmhGetIssExtL2FilterAction (i4NextFilter, &i4FltrAction);
    nmhGetIssExtL2FilterStatus (i4NextFilter, &i4RowStatus);

    CliPrintf (CliHandle, "\r\nExtended MAC Access List %d\r\n", i4NextFilter);
    CliPrintf (CliHandle, "-----------------------------\r\n");

    CliPrintf (CliHandle, "%-33s : %d\r\n", " Vlan Id", i4VlanId);

    CliPrintf (CliHandle, "%-33s : ", " Destination MAC Address");
    PrintMacAddress (DstMacAddr, (UINT1 *) pu1String);
    CliPrintf (CliHandle, "%s", pu1String);
    MEMSET (au1String, 0, ISS_MAX_LEN);
    CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle, "%-33s : ", " Source MAC Address");
    PrintMacAddress (SrcMacAddr, (UINT1 *) pu1String);
    CliPrintf (CliHandle, "%s", pu1String);
    MEMSET (au1String, 0, ISS_MAX_LEN);
    CliPrintf (CliHandle, "\r\n");

    if (i4FltrAction == ISS_ALLOW)
    {
        CliPrintf (CliHandle, "%-33s : Permit\r\n", " Filter Action");
    }
    else if (i4FltrAction == ISS_DROP)
    {
        CliPrintf (CliHandle, "%-33s : Deny\r\n", " Filter Action");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : Unknown\r\n", " Filter Action");
    }

    if (i4RowStatus == ISS_ACTIVE)
    {
        CliPrintf (CliHandle, "%-33s : Active", " Status");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : InActive", " Status");
    }
    CliPrintf (CliHandle, "\r\n\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclShowRunningConfig                               */
/*                                                                           */
/*     DESCRIPTION      : This function  displays the  RSTP Configuration    */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
AclShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{

    AclShowRunningConfigTables (CliHandle);
    UNUSED_PARAM (u4Module);

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*     FUNCTION NAME    : AclShowRunningConfigTables                         */
/*                                                                           */
/*     DESCRIPTION      : This function displays table  objects in ACL  for  */
/*                        show running configuration.                        */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
AclShowRunningConfigTables (tCliHandle CliHandle)
{

    tMacAddr            zeroAddr;
    tMacAddr            DstMacAddr;
    tMacAddr            SrcMacAddr;
    INT4                i4NextFilter;
    INT4                i4PrevFilter;
    INT4                i4FltrAction;
    INT4                i4VlanId;
    INT4                i4FilterStatus;
    INT1                i1OutCome;
    CHR1               *pu1String;
    UINT4               u4Quit = CLI_SUCCESS;
    CHR1                au1String[ISS_ADDR_LEN];
    pu1String = &au1String[0];

    CLI_MEMSET (au1String, 0, ISS_ADDR_LEN);

    MEMSET (zeroAddr, 0, sizeof (tMacAddr));
    MEMSET (DstMacAddr, 0, sizeof (tMacAddr));
    MEMSET (SrcMacAddr, 0, sizeof (tMacAddr));

    CliRegisterLock (CliHandle, IssLock, IssUnLock);
    ISS_LOCK ();

    /*Mac Access lists */

    CliPrintf (CliHandle, "\r\n");

    i1OutCome = nmhGetFirstIndexIssExtL2FilterTable (&i4NextFilter);

    while (i1OutCome != SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "mac access-list extended %d\r\n", i4NextFilter);

        nmhGetIssExtL2FilterVlanId (i4NextFilter, &i4VlanId);
        nmhGetIssExtL2FilterDstMacAddr (i4NextFilter,
                                        (tMacAddr *) & DstMacAddr);
        nmhGetIssExtL2FilterSrcMacAddr (i4NextFilter,
                                        (tMacAddr *) & SrcMacAddr);
        nmhGetIssExtL2FilterAction (i4NextFilter, &i4FltrAction);
        nmhGetIssExtL2FilterStatus (i4NextFilter, &i4FilterStatus);

        if (i4FilterStatus == ISS_ACTIVE)
        {
            CLI_MEMSET (au1String, 0, ISS_ADDR_LEN);

            if (CLI_MEMCMP (DstMacAddr, zeroAddr, MAC_ADDR_LEN) != 0)
            {
                if (i4FltrAction == ISS_ALLOW)
                {
                    CliPrintf (CliHandle, "permit ");
                }
                else if (i4FltrAction == ISS_DROP)
                {
                    CliPrintf (CliHandle, "deny ");
                }
                PrintMacAddress (DstMacAddr, (UINT1 *) pu1String);
                CliPrintf (CliHandle, "host dest-mac %s", pu1String);
            }
            else if (CLI_MEMCMP (SrcMacAddr, zeroAddr, MAC_ADDR_LEN) != 0)
            {
                if (i4FltrAction == ISS_ALLOW)
                {
                    CliPrintf (CliHandle, "permit ");
                }
                else if (i4FltrAction == ISS_DROP)
                {
                    CliPrintf (CliHandle, "deny ");
                }
                PrintMacAddress (SrcMacAddr, (UINT1 *) pu1String);
                CliPrintf (CliHandle, "host src-mac %s", pu1String);
            }
            if (i4VlanId != 0)
            {
                CliPrintf (CliHandle, "vlan %d ", i4VlanId);
            }

            u4Quit = CliPrintf (CliHandle, "\n!\r\n");

            if (u4Quit == CLI_FAILURE)
            {
                ISS_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return;
            }
        }

        i4PrevFilter = i4NextFilter;

        i1OutCome =
            nmhGetNextIndexIssExtL2FilterTable (i4PrevFilter, &i4NextFilter);

    }
    ISS_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : StdAclGetCfgPrompt                                 */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Class map prompt in pi1DispStr if  */
/*                        valid.                                             */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/

INT1
StdAclGetCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;
    INT4                i4StdAclIndex;

    if (!pi1DispStr)
    {
        return FALSE;
    }

    /* NULL is passed to return "config-std-nacl" as the prompt 
     * for the mode  */
    if (pi1ModeName == NULL)
    {
        STRCPY (pi1DispStr, "config-std-nacl");
        return TRUE;
    }

    u4Len = STRLEN (CLI_STDACL_MODE);

    if (STRNCMP (pi1ModeName, CLI_STDACL_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    i4StdAclIndex = CLI_ATOI (pi1ModeName);

    /* Set the mode information for CLI */
    CLI_SET_STDACLID (i4StdAclIndex);

    STRCPY (pi1DispStr, "(config-std-nacl)#");
    return TRUE;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ExtAclGetCfgPrompt                                 */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Class map prompt in pi1DispStr if  */
/*                        valid.                                             */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/

INT1
ExtAclGetCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;
    INT4                i4ExtAclIndex;

    if (!pi1DispStr)
    {
        return FALSE;
    }

    /* NULL is passed to return "config-std-nacl" as the prompt 
     * for the mode  */
    if (pi1ModeName == NULL)
    {
        STRCPY (pi1DispStr, "config-ext-nacl");
        return TRUE;
    }

    u4Len = STRLEN (CLI_EXTACL_MODE);
    if (STRNCMP (pi1ModeName, CLI_EXTACL_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    i4ExtAclIndex = CLI_ATOI (pi1ModeName);

    /* Set the mode information for CLI */
    CLI_SET_EXTACL (i4ExtAclIndex);

    STRCPY (pi1DispStr, "(config-ext-nacl)#");
    return TRUE;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MacAclGetCfgPrompt                                 */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Class map prompt in pi1DispStr if  */
/*                        valid.                                             */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/

INT1
MacAclGetCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;
    INT4                i4MacAclIndex;

    if (!pi1DispStr)
    {
        return FALSE;
    }

    /* NULL is passed to return "config-std-nacl" as the prompt 
     * for the mode  */
    if (pi1ModeName == NULL)
    {
        STRCPY (pi1DispStr, "config-ext-macl");
        return TRUE;
    }

    u4Len = STRLEN (CLI_MACACL_MODE);
    if (STRNCMP (pi1ModeName, CLI_MACACL_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    i4MacAclIndex = CLI_ATOI (pi1ModeName);

    /* Set the mode information for CLI */
    CLI_SET_MACACL (i4MacAclIndex);

    STRCPY (pi1DispStr, "(config-ext-macl)#");
    return TRUE;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclExtDstMacFilterConfig                           */
/*                                                                           */
/*     DESCRIPTION      : This function configure the ACL in hardware        */
/*                        for the destination addr/vlan                      */
/*                                                                           */
/*     INPUT            : CliHandle - Cli Handle                             */
/*                        i4Action - ISS_ALLOW / ISS_DROP                    */
/*                        DestMacAddr - Destination MAC Address              */
/*                        u4VlanId - Vlan Id                                 */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
AclExtDstMacFilterConfig (tCliHandle CliHandle, INT4 i4Action,
                          tMacAddr DestMacAddr, UINT4 u4VlanId)
{
    INT4                i4Status;
    UINT4               u4ErrCode;
    INT4                i4FilterNo;
    INT4                i4PrevAction;
    INT4                i4PrevVlanId;
    INT4                i4PrevFilterStatus;
    tMacAddr            PrevDstMacAddr;
    tMacAddr            PrevSrcMacAddr;

    i4FilterNo = CLI_GET_MACACL ();
    /* if filter already exists, do nothing and enter the configuration mode */

    if (nmhGetIssExtL2FilterStatus (i4FilterNo, &i4Status) == SNMP_FAILURE)
    {
        /* Create a filter */
        if (nmhTestv2IssExtL2FilterStatus (&u4ErrCode,
                                           i4FilterNo,
                                           ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }
    nmhGetIssExtL2FilterAction (i4FilterNo, &i4PrevAction);
    nmhGetIssExtL2FilterDstMacAddr (i4FilterNo, (tMacAddr *) & PrevDstMacAddr);
    nmhGetIssExtL2FilterSrcMacAddr (i4FilterNo, (tMacAddr *) & PrevSrcMacAddr);
    nmhGetIssExtL2FilterVlanId (i4FilterNo, &i4PrevVlanId);
    nmhGetIssExtL2FilterStatus (i4FilterNo, &i4PrevFilterStatus);

    if (i4Status != ISS_NOT_READY)
    {
        /* Filter has previously been configured. This must be over-written
         * First destroy the filter and create again
         */

        if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }
    if (nmhTestv2IssExtL2FilterAction (&u4ErrCode,
                                       i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        nmhSetIssExtL2FilterAction (i4FilterNo, i4PrevAction);
        nmhSetIssExtL2FilterDstMacAddr (i4FilterNo, PrevDstMacAddr);
        nmhSetIssExtL2FilterSrcMacAddr (i4FilterNo, PrevSrcMacAddr);
        nmhSetIssExtL2FilterVlanId (i4FilterNo, i4PrevVlanId);
        nmhSetIssExtL2FilterStatus (i4FilterNo, i4PrevFilterStatus);
        return (CLI_FAILURE);
    }
    if (nmhTestv2IssExtL2FilterDstMacAddr (&u4ErrCode, i4FilterNo,
                                           DestMacAddr) == SNMP_FAILURE)
    {
        nmhSetIssExtL2FilterAction (i4FilterNo, i4PrevAction);
        nmhSetIssExtL2FilterDstMacAddr (i4FilterNo, PrevDstMacAddr);
        nmhSetIssExtL2FilterSrcMacAddr (i4FilterNo, PrevSrcMacAddr);
        nmhSetIssExtL2FilterVlanId (i4FilterNo, i4PrevVlanId);
        nmhSetIssExtL2FilterStatus (i4FilterNo, i4PrevFilterStatus);
        return (CLI_FAILURE);
    }
    if (nmhTestv2IssExtL2FilterVlanId (&u4ErrCode,
                                       i4FilterNo, u4VlanId) == SNMP_FAILURE)
    {
        nmhSetIssExtL2FilterAction (i4FilterNo, i4PrevAction);
        nmhSetIssExtL2FilterDstMacAddr (i4FilterNo, PrevDstMacAddr);
        nmhSetIssExtL2FilterSrcMacAddr (i4FilterNo, PrevSrcMacAddr);
        nmhSetIssExtL2FilterVlanId (i4FilterNo, i4PrevVlanId);
        nmhSetIssExtL2FilterStatus (i4FilterNo, i4PrevFilterStatus);
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL2FilterAction (i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    if (nmhSetIssExtL2FilterDstMacAddr (i4FilterNo, DestMacAddr) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL2FilterVlanId (i4FilterNo, u4VlanId) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_ACTIVE) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
        nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclExtSrcMacFilterConfig                           */
/*                                                                           */
/*     DESCRIPTION      : This function configure the ACL in hardware        */
/*                        for the source addr/vlan                           */
/*                                                                           */
/*     INPUT            : CliHandle - Cli Handle                             */
/*                        i4Action - ISS_ALLOW / ISS_DROP                    */
/*                        SrcMacAddr - Source MAC Address                    */
/*                        u4VlanId - Vlan Id                                 */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
AclExtSrcMacFilterConfig (tCliHandle CliHandle, INT4 i4Action,
                          tMacAddr SrcMacAddr, UINT4 u4VlanId)
{
    INT4                i4Status;
    UINT4               u4ErrCode;
    INT4                i4FilterNo;
    INT4                i4PrevAction;
    INT4                i4PrevVlanId;
    INT4                i4PrevFilterStatus;
    tMacAddr            PrevDstMacAddr;
    tMacAddr            PrevSrcMacAddr;

    i4FilterNo = CLI_GET_MACACL ();

    /* if filter already exists, do nothing and enter the configuration mode */
    if (nmhGetIssExtL2FilterStatus (i4FilterNo, &i4Status) == SNMP_FAILURE)
    {
        /* Create a filter */
        if (nmhTestv2IssExtL2FilterStatus (&u4ErrCode,
                                           i4FilterNo,
                                           ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }
    nmhGetIssExtL2FilterAction (i4FilterNo, &i4PrevAction);
    nmhGetIssExtL2FilterDstMacAddr (i4FilterNo, (tMacAddr *) & PrevDstMacAddr);
    nmhGetIssExtL2FilterSrcMacAddr (i4FilterNo, (tMacAddr *) & PrevSrcMacAddr);
    nmhGetIssExtL2FilterVlanId (i4FilterNo, &i4PrevVlanId);
    nmhGetIssExtL2FilterStatus (i4FilterNo, &i4PrevFilterStatus);

    if (i4Status != ISS_NOT_READY)
    {
        /* Filter has previously been configured. This must be over-written
         * First destroy the filter and create again
         */
        if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }

    if (nmhTestv2IssExtL2FilterAction (&u4ErrCode, i4FilterNo, i4Action)
        == SNMP_FAILURE)
    {
        nmhSetIssExtL2FilterAction (i4FilterNo, i4PrevAction);
        nmhSetIssExtL2FilterDstMacAddr (i4FilterNo, PrevDstMacAddr);
        nmhSetIssExtL2FilterSrcMacAddr (i4FilterNo, PrevSrcMacAddr);
        nmhSetIssExtL2FilterVlanId (i4FilterNo, i4PrevVlanId);
        nmhSetIssExtL2FilterStatus (i4FilterNo, i4PrevFilterStatus);
        return (CLI_FAILURE);
    }
    if (nmhTestv2IssExtL2FilterSrcMacAddr (&u4ErrCode, i4FilterNo,
                                           SrcMacAddr) == SNMP_FAILURE)
    {
        nmhSetIssExtL2FilterAction (i4FilterNo, i4PrevAction);
        nmhSetIssExtL2FilterDstMacAddr (i4FilterNo, PrevDstMacAddr);
        nmhSetIssExtL2FilterSrcMacAddr (i4FilterNo, PrevSrcMacAddr);
        nmhSetIssExtL2FilterVlanId (i4FilterNo, i4PrevVlanId);
        nmhSetIssExtL2FilterStatus (i4FilterNo, i4PrevFilterStatus);
        return (CLI_FAILURE);
    }
    if (nmhTestv2IssExtL2FilterVlanId (&u4ErrCode,
                                       i4FilterNo, u4VlanId) == SNMP_FAILURE)
    {
        nmhSetIssExtL2FilterAction (i4FilterNo, i4PrevAction);
        nmhSetIssExtL2FilterDstMacAddr (i4FilterNo, PrevDstMacAddr);
        nmhSetIssExtL2FilterSrcMacAddr (i4FilterNo, PrevSrcMacAddr);
        nmhSetIssExtL2FilterVlanId (i4FilterNo, i4PrevVlanId);
        nmhSetIssExtL2FilterStatus (i4FilterNo, i4PrevFilterStatus);
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL2FilterAction (i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL2FilterSrcMacAddr (i4FilterNo, SrcMacAddr) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL2FilterVlanId (i4FilterNo, u4VlanId) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_ACTIVE) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);

        nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : AclIPv6GetCfgPrompt                                */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Class map prompt in pi1DispStr if  */
/*                        valid.                                             */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/
INT1
AclIPv6GetCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = 0;
    INT4                i4IPv6AclIndex = 0;

    if (pi1DispStr == NULL)
    {
        return FALSE;
    }

    /* NULL is passed to return "config-ipv6-acl" as the prompt
     *  *      * for the mode  */
    if (pi1ModeName == NULL)
    {
        STRCPY (pi1DispStr, "config-ipv6-acl");
        return TRUE;
    }

    u4Len = STRLEN (CLI_IPV6ACL_MODE);
    if (STRNCMP (pi1ModeName, CLI_IPV6ACL_MODE, u4Len) != 0)
    {
        return FALSE;
    }

    pi1ModeName = pi1ModeName + u4Len;

    i4IPv6AclIndex = CLI_ATOI (pi1ModeName);

    /* Set the mode information for CLI */
    CLI_SET_IPV6ACL (i4IPv6AclIndex);
    STRCPY (pi1DispStr, "(config-ipv6-acl)#");
    return TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : UserDefAclGetCfgPrompt                             */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Class map prompt in pi1DispStr if  */
/*                        valid.                                             */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/

INT1
UserDefAclGetCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;
    INT4                i4UserDefAclIndex;

    if (!pi1DispStr)
    {
        return FALSE;
    }

    /* NULL is passed to return "config-std-nacl" as the prompt 
     * for the mode  */
    if (pi1ModeName == NULL)
    {
        STRCPY (pi1DispStr, "config-userdef-acl");
        return TRUE;
    }

    u4Len = STRLEN (CLI_USRDEFACL_MODE);
    if (STRNCMP (pi1ModeName, CLI_USRDEFACL_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    i4UserDefAclIndex = CLI_ATOI (pi1ModeName);

    /* Set the mode information for CLI */
    CLI_SET_USERDEFFACL (i4UserDefAclIndex);

    STRCPY (pi1DispStr, "(config-userdef-acl)#");
    return TRUE;
}
#endif
