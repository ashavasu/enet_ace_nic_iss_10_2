/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: issexmbsm.c,v 1.5 2012/10/31 09:46:18 siva Exp $
 *
 * Description: 
 *
 *******************************************************************/

#ifndef _ISSEXMBSM_C
#define _ISSEXMBSM_C

#include "issexinc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : IssMbsmL2FilterCardInsert                                  */
/*                                                                           */
/* Description  : This function updates the L2 Filter HW configuration when  */
/*                a card is inserted into the MBSM System.                   */
/*                                                                           */
/* Input        : pPortInfo  - Pointer to Port Related Info.                 */
/*              : pSlotInfo  - Pointer to Slot Related Info.                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
IssMbsmL2FilterCardInsert (tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo)
{

    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;
    tIssL2FilterEntry   tempIssL2FilterEntry;
    INT4                i4RetVal;

    if (pPortInfo->u4PortCount == 0)
    {
        return MBSM_SUCCESS;
    }

    /* Scan Global L2 Filter List */
    TMO_SLL_Scan (&ISS_L2FILTER_EX_LIST, pIssL2FilterEntry, tIssL2FilterEntry *)
    {

        if (pIssL2FilterEntry->u1IssL2FilterStatus != ISS_ACTIVE)
        {
            continue;
        }

        ISS_MEMCPY (&tempIssL2FilterEntry, pIssL2FilterEntry,
                    sizeof (tIssL2FilterEntry));

        /* Call the hardware API to Add the filter */
        i4RetVal =
            IsssysIssMbsmHwUpdateL2Filter (&tempIssL2FilterEntry,
                                           ISS_L2FILTER_ADD, pSlotInfo);

        if (i4RetVal != FNP_SUCCESS)
        {
            return MBSM_FAILURE;
        }
    }

    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IssMbsmL2FilterCardRemove                                  */
/*                                                                           */
/* Description  : This function updates the L2 Filter HW configuration when  */
/*                a card is removed into the MBSM System.                    */
/*                                                                           */
/* Input        : pPortInfo  - Pointer to Port Related Info.                 */
/*              : pSlotInfo  - Pointer to Slot Related Info.                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
IssMbsmL2FilterCardRemove (tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo)
{
    /* Filter deletion for L2 filters is not needed when Card Removal event
     * is received. Since we are maintaining a global L2 filter list for every 
     * L2 filter handle (via BCMX), no need to clean-up the filter list when a 
     * line card is removed. Just ignore the card removal event for this case.
     */

    UNUSED_PARAM (pPortInfo);
    UNUSED_PARAM (pSlotInfo);
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IssMbsmCardInsertRateCtrlTable                             */
/*                                                                           */
/* Description  : This function updates the port related HW configuration    */
/*                for Rate Control Table when                                */
/*                a card is inserted into the MBSM System.                   */
/*                                                                           */
/* Input        : pPortInfo  - Pointer to Port Related Info.                 */
/*              : pSlotInfo  - Pointer to Slot Related Info.                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
IssMbsmCardInsertRateCtrlTable (tMbsmPortInfo * pPortInfo,
                                tMbsmSlotInfo * pSlotInfo)
{

    tIssRateCtrlEntry  *pIssRateCtrlEntry = NULL;
    INT4                i4RateCtrlIndex = 0;
    UINT4               u4StartPortIndex = 0;
    UINT4               u4EndPortIndex = 0;
    INT4                i4RetVal;
    UINT1               u1IsSetInPortList = OSIX_FALSE;
    UINT1               u1IsSetInPortListStatus = OSIX_FALSE;

    if ((pSlotInfo->i1IsPreConfigured == MBSM_FALSE) ||
        (pPortInfo->u4PortCount == 0))
    {
        return MBSM_SUCCESS;
    }

    u4StartPortIndex = MBSM_PORT_INFO_STARTINDEX (pPortInfo);
    u4EndPortIndex = MBSM_PORT_INFO_STARTINDEX (pPortInfo) +
        MBSM_PORT_INFO_PORTCOUNT (pPortInfo);
    i4RateCtrlIndex = u4StartPortIndex;

    while (u4StartPortIndex != u4EndPortIndex)
    {
        OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST (pPortInfo),
                                 i4RateCtrlIndex, sizeof (tPortList),
                                 u1IsSetInPortList);
        OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST_STATUS (pPortInfo),
                                 i4RateCtrlIndex, sizeof (tPortList),
                                 u1IsSetInPortListStatus);

        if ((u1IsSetInPortList == OSIX_TRUE)
            && (OSIX_FALSE == u1IsSetInPortListStatus))
        {

            pIssRateCtrlEntry = gIssExtGlobalInfo.
                apIssRateCtrlEntry[i4RateCtrlIndex];

            if (pIssRateCtrlEntry != NULL)
            {

                if ((pIssRateCtrlEntry->u4IssRateCtrlPackets != 0) &&
                    (pIssRateCtrlEntry->u4IssRateCtrlLimitValue != 0))
                {
                    i4RetVal =
                        IsssysIssHwSetRateLimitingValue ((UINT4)
                                                         i4RateCtrlIndex,
                                                         pIssRateCtrlEntry->
                                                         u4IssRateCtrlPackets,
                                                         pIssRateCtrlEntry->
                                                         u4IssRateCtrlLimitValue);

                    if (i4RetVal != FNP_SUCCESS)
                    {
                        return MBSM_FAILURE;
                    }
                }
            }
        }
        u4StartPortIndex++;
        i4RateCtrlIndex++;
    }

    return MBSM_SUCCESS;
}

#endif /* _ISSEXMBSM_C */
