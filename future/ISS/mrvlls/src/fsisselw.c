/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsisselw.c,v 1.9 2015/04/07 15:01:45 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "iss.h"
# include  "issexinc.h"
# include  "fsisselw.h"
# include  "isscli.h"
# include  "aclmrvlcli.h"
# include  "diffsrv.h"
#ifdef NPAPI_WANTED
# include  "issnpwr.h"
#endif

UINT4               gu4AclOverLa = 1;
/* LOW LEVEL Routines for Table : IssExtRateCtrlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssExtRateCtrlTable
 Input       :  The Indices
                IssExtRateCtrlIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssExtRateCtrlTable (INT4 i4IssExtRateCtrlIndex)
{

    if (IssExtSnmpLowValidateIndexPortRateTable (i4IssExtRateCtrlIndex) ==
        ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssExtRateCtrlTable
 Input       :  The Indices
                IssExtRateCtrlIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssExtRateCtrlTable (INT4 *pi4IssExtRateCtrlIndex)
{
    return (nmhGetNextIndexIssExtRateCtrlTable (0, pi4IssExtRateCtrlIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIssExtRateCtrlTable
 Input       :  The Indices
                IssExtRateCtrlIndex
                nextIssExtRateCtrlIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIssExtRateCtrlTable (INT4 i4IssExtRateCtrlIndex,
                                    INT4 *pi4NextIssExtRateCtrlIndex)
{
    INT4                i4Count = 0;

    if (i4IssExtRateCtrlIndex < 0)
    {
        return SNMP_FAILURE;
    }

    for (i4Count = i4IssExtRateCtrlIndex + 1; i4Count <= ISS_MAX_PORTS;
         i4Count++)
    {
        if ((gIssExtGlobalInfo.apIssRateCtrlEntry[i4Count]) != NULL)
        {
            *pi4NextIssExtRateCtrlIndex = i4Count;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIssExtRateCtrlPackets
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                retValIssExtRateCtrlPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtRateCtrlPackets (INT4 i4IssExtRateCtrlIndex,
                             INT4 *pi4RetValIssExtRateCtrlPackets)
{
    tIssExRateCtrlEntry *pIssExRateCtrlEntry;

    if (IssExtValidateRateCtrlEntry (i4IssExtRateCtrlIndex) != ISS_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pIssExRateCtrlEntry =
        gIssExtGlobalInfo.apIssRateCtrlEntry[i4IssExtRateCtrlIndex];

    *pi4RetValIssExtRateCtrlPackets =
        (INT4) pIssExRateCtrlEntry->u4IssRateCtrlPackets;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIssExtRateCtrlLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                retValIssExtRateCtrlLimitValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtRateCtrlLimitValue (INT4 i4IssExtRateCtrlIndex,
                                INT4 *pi4RetValIssExtRateCtrlLimitValue)
{
    tIssExRateCtrlEntry *pIssExRateCtrlEntry;

    if (IssExtValidateRateCtrlEntry (i4IssExtRateCtrlIndex) != ISS_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pIssExRateCtrlEntry =
        gIssExtGlobalInfo.apIssRateCtrlEntry[i4IssExtRateCtrlIndex];

    *pi4RetValIssExtRateCtrlLimitValue =
        (INT4) pIssExRateCtrlEntry->u4IssRateCtrlLimitValue;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIssExtRateCtrlPortRateLimit
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                retValIssExtRateCtrlPortRateLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtRateCtrlPortRateLimit (INT4 i4IssExtRateCtrlIndex,
                                   INT4 *pi4RetValIssExtRateCtrlPortRateLimit)
{
    tIssExRateCtrlEntry *pIssExRateCtrlEntry = NULL;

    pIssExRateCtrlEntry =
        gIssExtGlobalInfo.apIssRateCtrlEntry[i4IssExtRateCtrlIndex];

    if (pIssExRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIssExtRateCtrlPortRateLimit =
        pIssExRateCtrlEntry->i4IssRateCtrlPortLimitRate;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIssExtRateCtrlPortBurstSize
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                retValIssExtRateCtrlPortBurstSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtRateCtrlPortBurstSize (INT4 i4IssExtRateCtrlIndex,
                                   INT4 *pi4RetValIssExtRateCtrlPortBurstSize)
{
    tIssExRateCtrlEntry *pIssExRateCtrlEntry = NULL;

    pIssExRateCtrlEntry =
        gIssExtGlobalInfo.apIssRateCtrlEntry[i4IssExtRateCtrlIndex];

    if (pIssExRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIssExtRateCtrlPortBurstSize =
        pIssExRateCtrlEntry->i4IssRateCtrlPortBurstRate;

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIssExtRateCtrlPackets
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                setValIssExtRateCtrlPackets
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtRateCtrlPackets (INT4 i4IssExtRateCtrlIndex,
                             INT4 i4SetValIssExtRateCtrlPackets)
{
    tIssExRateCtrlEntry *pIssExRateCtrlEntry = NULL;

    pIssExRateCtrlEntry =
        gIssExtGlobalInfo.apIssRateCtrlEntry[i4IssExtRateCtrlIndex];

    if (pIssExRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssExRateCtrlEntry->u4IssRateCtrlPackets == (UINT4)
        i4SetValIssExtRateCtrlPackets)
    {
        return SNMP_SUCCESS;
    }

    pIssExRateCtrlEntry->u4IssRateCtrlPackets =
        (UINT4) i4SetValIssExtRateCtrlPackets;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssExtRateCtrlLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                setValIssExtRateCtrlLimitValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtRateCtrlLimitValue (INT4 i4IssExtRateCtrlIndex,
                                INT4 i4SetValIssExtRateCtrlLimitValue)
{
    tIssExRateCtrlEntry *pIssExRateCtrlEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif

    pIssExRateCtrlEntry =
        gIssExtGlobalInfo.apIssRateCtrlEntry[i4IssExtRateCtrlIndex];

    if (pIssExRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Check whether the packet type was specified or not */
    if (i4SetValIssExtRateCtrlLimitValue != 0 &&
        pIssExRateCtrlEntry->u4IssRateCtrlPackets <= 0)
    {
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    i4RetVal = IsssysIssHwSetRateLimitingValue ((UINT4) i4IssExtRateCtrlIndex,
                                                pIssExRateCtrlEntry->
                                                u4IssRateCtrlPackets,
                                                i4SetValIssExtRateCtrlLimitValue);

    if (i4RetVal != FNP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif

    pIssExRateCtrlEntry->u4IssRateCtrlLimitValue = (UINT4)
        i4SetValIssExtRateCtrlLimitValue;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssExtRateCtrlPortRateLimit
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                setValIssExtRateCtrlPortRateLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtRateCtrlPortRateLimit (INT4 i4IssExtRateCtrlIndex,
                                   INT4 i4SetValIssExtRateCtrlPortRateLimit)
{
    tIssExRateCtrlEntry *pIssExRateCtrlEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif
    INT4                i4SetValIssExRateCtrlPortBurstSize = 0;

    pIssExRateCtrlEntry =
        gIssExtGlobalInfo.apIssRateCtrlEntry[i4IssExtRateCtrlIndex];

    if (pIssExRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((INT4) (pIssExRateCtrlEntry->i4IssRateCtrlPortLimitRate) ==
        i4SetValIssExtRateCtrlPortRateLimit)
    {
        return SNMP_SUCCESS;
    }

    i4SetValIssExRateCtrlPortBurstSize =
        pIssExRateCtrlEntry->i4IssRateCtrlPortBurstRate;

#ifdef NPAPI_WANTED
    i4RetVal = IsssysIssHwSetPortEgressPktRate
        ((UINT4) i4IssExtRateCtrlIndex, i4SetValIssExtRateCtrlPortRateLimit,
         i4SetValIssExRateCtrlPortBurstSize);

    if (i4RetVal != FNP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif
    pIssExRateCtrlEntry->i4IssRateCtrlPortLimitRate =
        i4SetValIssExtRateCtrlPortRateLimit;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssExtRateCtrlPortBurstSize
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                setValIssExtRateCtrlPortBurstSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtRateCtrlPortBurstSize (INT4 i4IssExtRateCtrlIndex,
                                   INT4 i4SetValIssExtRateCtrlPortBurstSize)
{
    tIssExRateCtrlEntry *pIssExRateCtrlEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif
    INT4                i4SetValIssExRateCtrlPortRateLimit = 0;

    pIssExRateCtrlEntry =
        gIssExtGlobalInfo.apIssRateCtrlEntry[i4IssExtRateCtrlIndex];

    if (pIssExRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((INT4) (pIssExRateCtrlEntry->i4IssRateCtrlPortBurstRate) ==
        i4SetValIssExtRateCtrlPortBurstSize)
    {
        return SNMP_SUCCESS;
    }

    i4SetValIssExRateCtrlPortRateLimit =
        pIssExRateCtrlEntry->i4IssRateCtrlPortLimitRate;

#ifdef NPAPI_WANTED
    i4RetVal = IsssysIssHwSetPortEgressPktRate
        ((UINT4) i4IssExtRateCtrlIndex, i4SetValIssExRateCtrlPortRateLimit,
         i4SetValIssExtRateCtrlPortBurstSize);

    if (i4RetVal != FNP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif

    pIssExRateCtrlEntry->i4IssRateCtrlPortBurstRate =
        i4SetValIssExtRateCtrlPortBurstSize;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2IssExtRateCtrlPackets
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                testValIssExtRateCtrlPackets
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtRateCtrlPackets (UINT4 *pu4ErrorCode, INT4 i4IssExtRateCtrlIndex,
                                INT4 i4TestValIssExtRateCtrlPackets)
{

    if (IssExtValidateRateCtrlEntry (i4IssExtRateCtrlIndex) != ISS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssExtRateCtrlPackets < 0) ||
        (i4TestValIssExtRateCtrlPackets > ISS_RATE_ALL_FRAMES_LIMIT))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtRateCtrlLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                testValIssExtRateCtrlLimitValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtRateCtrlLimitValue (UINT4 *pu4ErrorCode,
                                   INT4 i4IssExtRateCtrlIndex,
                                   INT4 i4TestValIssExtRateCtrlLimitValue)
{

    if (IssExtValidateRateCtrlEntry (i4IssExtRateCtrlIndex) != ISS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssExtRateCtrlLimitValue < 0)
        || (i4TestValIssExtRateCtrlLimitValue > 12))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtRateCtrlPortRateLimit
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                testValIssExtRateCtrlPortRateLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtRateCtrlPortRateLimit (UINT4 *pu4ErrorCode,
                                      INT4 i4IssExtRateCtrlIndex,
                                      INT4 i4TestValIssExtRateCtrlPortRateLimit)
{
    tIssExRateCtrlEntry *pIssExRateCtrlEntry;

    if (IssExtValidateRateCtrlEntry (i4IssExtRateCtrlIndex) != ISS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pIssExRateCtrlEntry =
        gIssExtGlobalInfo.apIssRateCtrlEntry[i4IssExtRateCtrlIndex];

    if (pIssExRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValIssExtRateCtrlPortRateLimit < 0) ||
        (i4TestValIssExtRateCtrlPortRateLimit > 80000000))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtRateCtrlPortBurstSize
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                testValIssExtRateCtrlPortBurstSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtRateCtrlPortBurstSize (UINT4 *pu4ErrorCode,
                                      INT4 i4IssExtRateCtrlIndex,
                                      INT4 i4TestValIssExtRateCtrlPortBurstSize)
{
    tIssExRateCtrlEntry *pIssExRateCtrlEntry;

    if (IssExtValidateRateCtrlEntry (i4IssExtRateCtrlIndex) != ISS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pIssExRateCtrlEntry =
        gIssExtGlobalInfo.apIssRateCtrlEntry[i4IssExtRateCtrlIndex];

    if (pIssExRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValIssExtRateCtrlPortBurstSize < 0) ||
        (i4TestValIssExtRateCtrlPortBurstSize > 80000000))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IssExtRateCtrlTable
 Input       :  The Indices
                IssExtRateCtrlIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IssExtRateCtrlTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IssExtL2FilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssExtL2FilterTable
 Input       :  The Indices
                IssExtL2FilterNo
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssExtL2FilterTable (INT4 i4IssExtL2FilterNo)
{
    tIssSllNode        *pSllNode = NULL;
    tIssL2FilterEntry  *pIssExL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssExtL2FilterNo) != ISS_TRUE)
    {
        return SNMP_FAILURE;
    }
    pSllNode = ISS_SLL_FIRST (&ISS_L2FILTER_EX_LIST);
    while (pSllNode != NULL)
    {
        pIssExL2FilterEntry = (tIssL2FilterEntry *) pSllNode;
        if (i4IssExtL2FilterNo == pIssExL2FilterEntry->i4IssL2FilterNo)
        {
            return SNMP_SUCCESS;
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L2FILTER_EX_LIST, pSllNode);
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterDstMacAddr
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterDstMacAddr (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                                   tMacAddr pElement)
{
    tIssL2FilterEntry  *pIssExL2FilterEntry = NULL;
    tMacAddr            zeroAddr;
    tMacAddr            BaseMacAddr;

    ISS_MEMSET (zeroAddr, 0, ISS_ETHERNET_ADDR_SIZE);

    if (ISS_IS_L2FILTER_ID_VALID (i4IssExtL2FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssExL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExL2FilterEntry == NULL)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Check for DstMacAddr is not NULL */
    if (!ISS_MEMCMP (pElement, zeroAddr, ISS_ETHERNET_ADDR_SIZE))
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Check whether SrcMacAddr is not used. Either Src Mac or Dst Mac Based ACL 
     * can be enabled in a single Filter*/
    if ((ISS_MEMCMP (pIssExL2FilterEntry->IssL2FilterSrcMacAddr, zeroAddr,
                     sizeof (tMacAddr))) != 0)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    CfaGetSysMacAddress (BaseMacAddr);
    if (ISS_ARE_MAC_ADDR_EQUAL (pElement, BaseMacAddr) == ISS_TRUE)
    {
        CLI_SET_ERR (CLI_ACL_IVALID_MAC);
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterDstMacAddr
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterDstMacAddr (INT4 i4IssExtL2FilterNo, tMacAddr MacAddress)
{
    tIssL2FilterEntry  *pIssExL2FilterEntry = NULL;

    pIssExL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExL2FilterEntry != NULL)
    {
        ISS_MEMSET (pIssExL2FilterEntry->IssL2FilterDstMacAddr, 0,
                    ISS_ETHERNET_ADDR_SIZE);
        ISS_MEMCPY (pIssExL2FilterEntry->IssL2FilterDstMacAddr,
                    MacAddress, ISS_ETHERNET_ADDR_SIZE);

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterDstMacAddr
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterDstMacAddr (INT4 i4IssExtL2FilterNo, tMacAddr * pMacAddress)
{
    tIssL2FilterEntry  *pIssExL2FilterEntry = NULL;

    pIssExL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExL2FilterEntry != NULL)
    {
        ISS_MEMCPY ((UINT1 *) pMacAddress,
                    pIssExL2FilterEntry->IssL2FilterDstMacAddr,
                    ISS_ETHERNET_ADDR_SIZE);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterSrcMacAddr
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                testValIssExtL2FilterSrcMacAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterSrcMacAddr (UINT4 *pu4ErrorCode, INT4 i4IssExtL2FilterNo,
                                   tMacAddr TestValIssExtL2FilterSrcMacAddr)
{
    tIssL2FilterEntry  *pIssExL2FilterEntry = NULL;
    tMacAddr            zeroAddr;
    tMacAddr            BaseMacAddr;

    ISS_MEMSET (zeroAddr, 0, sizeof (tMacAddr));

    if (ISS_IS_L2FILTER_ID_VALID (i4IssExtL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssExL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExL2FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Check for SrcMacAddr is not NULL */
    if (!ISS_MEMCMP
        (TestValIssExtL2FilterSrcMacAddr, zeroAddr, sizeof (tMacAddr)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Check whether DstMacAddr is not used. Either Src Mac or Dst Mac Based ACL 
     * can be enabled in a single Filter*/
    if ((ISS_MEMCMP (pIssExL2FilterEntry->IssL2FilterDstMacAddr, zeroAddr,
                     sizeof (tMacAddr))) != 0)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    CfaGetSysMacAddress (BaseMacAddr);
    if (ISS_ARE_MAC_ADDR_EQUAL (TestValIssExtL2FilterSrcMacAddr,
                                BaseMacAddr) == ISS_TRUE)
    {
        CLI_SET_ERR (CLI_ACL_IVALID_MAC);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterSrcMacAddr
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                setValIssExtL2FilterSrcMacAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterSrcMacAddr (INT4 i4IssExtL2FilterNo, tMacAddr SrcMacAddress)
{
    tIssL2FilterEntry  *pIssExL2FilterEntry = NULL;

    pIssExL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExL2FilterEntry != NULL)
    {
        ISS_MEMSET (pIssExL2FilterEntry->IssL2FilterSrcMacAddr, 0,
                    sizeof (tMacAddr));

        ISS_MEMCPY (pIssExL2FilterEntry->IssL2FilterSrcMacAddr,
                    SrcMacAddress, sizeof (tMacAddr));

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterSrcMacAddr
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                retValIssExtL2FilterSrcMacAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterSrcMacAddr (INT4 i4IssExtL2FilterNo, tMacAddr * pMacAddress)
{
    tIssL2FilterEntry  *pIssExL2FilterEntry = NULL;

    pIssExL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExL2FilterEntry != NULL)
    {
        ISS_MEMCPY ((UINT1 *) pMacAddress,
                    pIssExL2FilterEntry->IssL2FilterSrcMacAddr,
                    sizeof (tMacAddr));
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterVlanId
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterVlanId (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                               INT4 i4TestValIssExtL2FilterVlanId)
{
    tIssL2FilterEntry  *pIssExL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssExtL2FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssExtL2FilterVlanId <= 0) ||
        (i4TestValIssExtL2FilterVlanId > VLAN_DEV_MAX_VLAN_ID))
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIssExL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExL2FilterEntry == NULL)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterVlanId
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterVlanId (INT4 i4IssExtL2FilterNo, INT4 i4Element)
{
    tIssL2FilterEntry  *pIssExL2FilterEntry = NULL;

    pIssExL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExL2FilterEntry != NULL)
    {
        pIssExL2FilterEntry->u4IssL2FilterCustomerVlanId = (UINT4) i4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterVlanId
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterVlanId (INT4 i4IssExtL2FilterNo, INT4 *pElement)
{
    tIssL2FilterEntry  *pIssExL2FilterEntry = NULL;

    pIssExL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExL2FilterEntry != NULL)
    {
        *pElement = pIssExL2FilterEntry->u4IssL2FilterCustomerVlanId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterAction
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterAction (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                               INT4 i4Element)
{
    tIssL2FilterEntry  *pIssExL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssExtL2FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssExL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExL2FilterEntry == NULL)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4Element == ISS_ALLOW) || (i4Element == ISS_DROP))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterAction
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterAction (INT4 i4IssExtL2FilterNo, INT4 i4Element)
{
    tIssL2FilterEntry  *pIssExL2FilterEntry = NULL;

    pIssExL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExL2FilterEntry != NULL)
    {
        pIssExL2FilterEntry->IssL2FilterAction = i4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterAction
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterAction (INT4 i4IssExtL2FilterNo, INT4 *pElement)
{
    tIssL2FilterEntry  *pIssExL2FilterEntry = NULL;

    pIssExL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExL2FilterEntry != NULL)
    {
        *pElement = pIssExL2FilterEntry->IssL2FilterAction;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterStatus
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterStatus (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                               INT4 i4Element)
{
    tIssL2FilterEntry  *pIssExL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssExtL2FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4Element < ISS_ACTIVE) || (i4Element > ISS_DESTROY))
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIssExL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExL2FilterEntry == NULL &&
        (i4Element != ISS_CREATE_AND_WAIT && i4Element != ISS_CREATE_AND_GO))
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pIssExL2FilterEntry != NULL &&
        (i4Element == ISS_CREATE_AND_WAIT || i4Element == ISS_CREATE_AND_GO))
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pIssExL2FilterEntry != NULL &&
        (pIssExL2FilterEntry->u1IssL2FilterStatus == ISS_NOT_READY &&
         i4Element == ISS_NOT_IN_SERVICE))
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pIssExL2FilterEntry != NULL &&
        (pIssExL2FilterEntry->u1IssL2FilterStatus != ISS_ACTIVE &&
         i4Element == ISS_NOT_IN_SERVICE))
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterStatus
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterStatus (INT4 i4IssExtL2FilterNo, INT4 i4Element)
{
    tIssL2FilterEntry  *pIssExL2FilterEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif

    pIssExL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExL2FilterEntry != NULL)
    {
        if (pIssExL2FilterEntry->u1IssL2FilterStatus == (UINT1) i4Element)
        {
            return SNMP_SUCCESS;
        }

    }
    else
    {
        /* This Filter Entry is not there in the Database */
        if ((i4Element != ISS_CREATE_AND_WAIT) &&
            (i4Element != ISS_CREATE_AND_GO))
        {
            return SNMP_FAILURE;
        }
    }

    switch (i4Element)
    {

        case ISS_CREATE_AND_WAIT:
        case ISS_CREATE_AND_GO:

            if (ISS_L2FILTERENTRY_EX_ALLOC_MEM_BLOCK (&pIssExL2FilterEntry)
                == MEM_FAILURE)
            {
                ISS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "No Free L2 Filter Entry"
                         "or h/w init failed at startup\n");
                return SNMP_FAILURE;
            }

            ISS_MEMSET (pIssExL2FilterEntry, 0, sizeof (tIssL2FilterEntry));

            /* Setting all the default values for the objects */
            pIssExL2FilterEntry->i4IssL2FilterNo = i4IssExtL2FilterNo;

            pIssExL2FilterEntry->IssL2FilterAction = ISS_ALLOW;

            /* Adding the Entry to the Filter List */
            ISS_SLL_ADD (&(ISS_L2FILTER_EX_LIST),
                         &(pIssExL2FilterEntry->IssNextNode));

            /* Setting the RowStatus to NOT_READY if the
             * filter if i4Element is ISS_CREATE_AND_WAIT*/

            pIssExL2FilterEntry->u1IssL2FilterStatus = ISS_NOT_READY;

            break;

        case ISS_NOT_IN_SERVICE:

#ifdef NPAPI_WANTED
            if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
            {
                /* Call the hardware API to update the filter */
                i4RetVal =
                    IsssysIssHwUpdateL2Filter (pIssExL2FilterEntry,
                                               ISS_L2FILTER_DELETE);

                if (i4RetVal != FNP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
#endif

            pIssExL2FilterEntry->u1IssL2FilterStatus = ISS_NOT_IN_SERVICE;

            break;

        case ISS_ACTIVE:

            if (IssExtCompareL2FilterInfo (pIssExL2FilterEntry) == SNMP_SUCCESS)
            {
                return SNMP_FAILURE;
            }

            pIssExL2FilterEntry->u1IssL2FilterStatus = ISS_ACTIVE;
#ifdef NPAPI_WANTED
            if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
            {
                /* Call the hardware API to add the L2 filter */
                i4RetVal =
                    IsssysIssHwUpdateL2Filter (pIssExL2FilterEntry,
                                               ISS_L2FILTER_ADD);

                if (i4RetVal != FNP_SUCCESS)
                {
                    pIssExL2FilterEntry->u1IssL2FilterStatus =
                        ISS_NOT_IN_SERVICE;
                    return SNMP_FAILURE;
                }
            }
#endif

            pIssExL2FilterEntry->u1IssL2FilterStatus = ISS_ACTIVE;

            break;

        case ISS_DESTROY:

            /* Deleting the node from the list */
            ISS_SLL_DELETE (&(ISS_L2FILTER_EX_LIST),
                            &(pIssExL2FilterEntry->IssNextNode));

            if (pIssExL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
            {
#ifdef NPAPI_WANTED
                if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                {
                    /* Call the hardware API to delete the filter */
                    i4RetVal =
                        IsssysIssHwUpdateL2Filter (pIssExL2FilterEntry,
                                                   ISS_L2FILTER_DELETE);
                    if (i4RetVal != FNP_SUCCESS)
                    {
                        /* Delete the Entry from the Software */
                        ISS_L2FILTERENTRY_EX_FREE_MEM_BLOCK
                            (pIssExL2FilterEntry);
                        return SNMP_FAILURE;
                    }
                }
#endif
            }

            /* Delete the Entry from the Software */
            if (ISS_L2FILTERENTRY_EX_FREE_MEM_BLOCK (pIssExL2FilterEntry)
                != MEM_SUCCESS)
            {
                ISS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "L2 Filter Entry Free Failure\n");
                return SNMP_FAILURE;
            }

            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterStatus
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterStatus (INT4 i4IssExtL2FilterNo, INT4 *pElement)
{
    tIssL2FilterEntry  *pIssExL2FilterEntry = NULL;

    pIssExL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExL2FilterEntry != NULL)
    {
        *pElement = (INT4) pIssExL2FilterEntry->u1IssL2FilterStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIssExtL2FilterTable
 Input       :  The Indices
                IssExtL2FilterNo
                nextIssExtL2FilterNo
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIssExtL2FilterTable (INT4 i4IssExtL2FilterNo,
                                    INT4 *pi4IssExtL2FilterNoOUT)
{

    if (i4IssExtL2FilterNo < 0)
    {
        return SNMP_FAILURE;
    }

    if ((IssExtSnmpLowGetNextValidL2FilterTableIndex
         (i4IssExtL2FilterNo, pi4IssExtL2FilterNoOUT)) == ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssExtL2FilterTable
 Input       :  The Indices
                IssExtFilterL2No
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssExtL2FilterTable (INT4 *pi4IssExtL2FilterNo)
{
    if ((IssExtSnmpLowGetFirstValidL2FilterTableIndex (pi4IssExtL2FilterNo)) ==
        ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IssExtL2FilterTable
 Input       :  The Indices
                IssExtL2FilterNo
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IssExtL2FilterTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
* Function    :  IssExtSnmpLowValidateIndexPortRateTable
* Input       :  i4CtrlIndex(PortCtrl Table Index or Rate Ctrl Table Index)
* Output      :  None
* Returns     :  ISS_SUCCESS/ISS_FAILURE
*****************************************************************************/
INT4
IssExtSnmpLowValidateIndexPortRateTable (INT4 i4PortIndex)
{
    /* This routine is only for Port Ctrl Table */
    if (i4PortIndex > ISS_ZERO_ENTRY && i4PortIndex <= ISS_MAX_PORTS)
    {
        return ISS_SUCCESS;
    }
    return ISS_FAILURE;
}

/****************************************************************************
* Function    :  IssExtValidateRateCtrlEntry
* Input       :  i4RateCtrlIndex
* Output      :  None
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssExtValidateRateCtrlEntry (INT4 i4RateCtrlIndex)
{
    /* Validating the entry */
    if ((i4RateCtrlIndex <= ISS_ZERO_ENTRY) ||
        (i4RateCtrlIndex > ISS_MAX_PORTS))
    {
        return ISS_FAILURE;
    }

    if ((gIssExtGlobalInfo.apIssRateCtrlEntry[i4RateCtrlIndex] == NULL))
    {
        ISS_TRC_ARG1 (DATA_PATH_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "ISS: RATE CTRL ENTRY %d DOES NOT EXIST\n",
                      i4RateCtrlIndex);
        return ISS_FAILURE;
    }
    return ISS_SUCCESS;
}

/****************************************************************************
* Function    :  IssExtSnmpLowGetFirstValidL2FilterTableIndex
* Input       :  None
* Output      :  INT4 *pi4FirstL2FilterIndex
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssExtSnmpLowGetFirstValidL2FilterTableIndex (INT4 *pi4FirstL2FilterIndex)
{
    tIssSllNode        *pSllNode = NULL;
    tIssL2FilterEntry  *pIssExL2FilterEntry = NULL;
    tIssL2FilterEntry  *pFirstL2FilterEntry = NULL;
    UINT1               u1FoundFlag = ISS_FALSE;

    pSllNode = ISS_SLL_FIRST (&ISS_L2FILTER_EX_LIST);

    while (pSllNode != NULL)
    {
        pIssExL2FilterEntry = (tIssL2FilterEntry *) pSllNode;
        if (u1FoundFlag == ISS_FALSE)
        {
            pFirstL2FilterEntry = pIssExL2FilterEntry;
            u1FoundFlag = ISS_TRUE;
        }
        else
        {
            if (pFirstL2FilterEntry->i4IssL2FilterNo >
                pIssExL2FilterEntry->i4IssL2FilterNo)
            {
                pFirstL2FilterEntry = pIssExL2FilterEntry;
            }
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L2FILTER_EX_LIST, pSllNode);
    }

    if (u1FoundFlag == ISS_TRUE)
    {
        *pi4FirstL2FilterIndex = pFirstL2FilterEntry->i4IssL2FilterNo;
        return ISS_SUCCESS;
    }

    return ISS_FAILURE;
}

/****************************************************************************
* Function    :  IssExtSnmpLowGetFirstValidL2FilterTableIndex
* Input       :  INT4 i4IssExtL2FilterNo
* Output      :  INT4 *pi4NextL2FilterNo
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssExtSnmpLowGetNextValidL2FilterTableIndex (INT4 i4IssExtL2FilterNo,
                                             INT4 *pi4NextL2FilterNo)
{
    tIssSllNode        *pSllNode = NULL;
    tIssL2FilterEntry  *pIssExL2FilterEntry = NULL;
    tIssL2FilterEntry  *pNextL2FilterEntry = NULL;
    UINT1               u1FoundFlag = ISS_FALSE;

    pSllNode = ISS_SLL_FIRST (&ISS_L2FILTER_EX_LIST);

    while (pSllNode != NULL)
    {
        pIssExL2FilterEntry = (tIssL2FilterEntry *) pSllNode;

        if (i4IssExtL2FilterNo < pIssExL2FilterEntry->i4IssL2FilterNo)
        {
            if (u1FoundFlag == ISS_FALSE)
            {
                pNextL2FilterEntry = pIssExL2FilterEntry;
                u1FoundFlag = ISS_TRUE;
            }
            else
            {
                if (pNextL2FilterEntry->i4IssL2FilterNo >
                    pIssExL2FilterEntry->i4IssL2FilterNo)
                {
                    pNextL2FilterEntry = pIssExL2FilterEntry;
                }
            }
        }

        pSllNode = ISS_SLL_NEXT (&ISS_L2FILTER_EX_LIST, pSllNode);
    }

    if (u1FoundFlag == ISS_TRUE)
    {
        *pi4NextL2FilterNo = pNextL2FilterEntry->i4IssL2FilterNo;
        return ISS_SUCCESS;
    }
    else
    {
        return ISS_FAILURE;
    }
}

/****************************************************************************
* Function    :  IssExtGetL2FilterEntry
* Input       :  INT4 i4IssExtL2FilterNo
* Output      :  None
* Returns     :  tIssL2FilterEntry * 
*****************************************************************************/
tIssL2FilterEntry  *
IssExtGetL2FilterEntry (INT4 i4IssExtL2FilterNo)
{

    tIssSllNode        *pSllNode = NULL;
    tIssL2FilterEntry  *pIssExL2FilterEntry = NULL;

    pSllNode = ISS_SLL_FIRST (&ISS_L2FILTER_EX_LIST);

    while (pSllNode != NULL)
    {
        pIssExL2FilterEntry = (tIssL2FilterEntry *) pSllNode;

        if (pIssExL2FilterEntry->i4IssL2FilterNo == i4IssExtL2FilterNo)
        {
            return pIssExL2FilterEntry;
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L2FILTER_EX_LIST, pSllNode);
    }
    return NULL;
}

/****************************************************************************
* Function    :  IssExtGetL3FilterEntry
* Input       :  INT4 i4IssExtL3FilterNo
* Output      :  None
* Returns     :  tIssL3FilterEntry * 
*****************************************************************************/
tIssL3FilterEntry  *
IssExtGetL3FilterEntry (INT4 i4IssExtL3FilterNo)
{
    UNUSED_PARAM (i4IssExtL3FilterNo);

    /* Stub added for compilation */
    return NULL;
}

/****************************************************************************
* Function    :  IssExtQualifyL2FilterData 
* Input       :  tIssL2FilterEntry **ppIssExtL2FilterEntry
* Output      :  None
* Returns     :  INT4
*****************************************************************************/
INT4
IssExtQualifyL2FilterData (tIssL2FilterEntry ** ppIssExtL2FilterEntry)
{
    if ((*ppIssExtL2FilterEntry)->IssL2FilterAction != 0)
    {
        (*ppIssExtL2FilterEntry)->u1IssL2FilterStatus = ISS_NOT_IN_SERVICE;
        return ISS_SUCCESS;
    }
    else
    {
        (*ppIssExtL2FilterEntry)->u1IssL2FilterStatus = ISS_NOT_READY;
        return ISS_FAILURE;
    }
}

/****************************************************************************
* Function    :  AclCheckDenyRule
* Usage       :  This function will be called whenever a new MAC address was learnt
*                in ISS software. This function checks whether ACL deny rule was
*                present for that Source MAC Address. If so, do not program the 
*                Source Mac in hardware. Else program the MAC in hardware
* Input       :  VlanId, MacAddr, MacType
* Output      :  None
* Returns     :  UINT1
*****************************************************************************/
UINT1
AclCheckDenyRule (UINT2 VlanId, tMacAddr MacAddr, UINT1 MacType)
{
    tIssSllNode        *pSllNode = NULL;
    tIssL2FilterEntry  *pIssExL2FilterEntry = NULL;
    INT4               i4RetVal;

    if (MacType == DST_MAC_BASED_ACL)
    {
        return ISS_ALLOW;
    }
    pSllNode = ISS_SLL_FIRST (&ISS_L2FILTER_EX_LIST);

    while (pSllNode != NULL)
    {
        pIssExL2FilterEntry = (tIssL2FilterEntry *) pSllNode;

        /* Check the row status is ACTIVE */
        if (pIssExL2FilterEntry->u1IssL2FilterStatus != ISS_ACTIVE)
        {
            pSllNode = ISS_SLL_NEXT (&ISS_L2FILTER_EX_LIST, pSllNode);
            continue;
        }

        /*Check whether MAC addr and Vlan was present in the ACL */
        if ((pIssExL2FilterEntry->u4IssL2FilterCustomerVlanId == VlanId) &&
            (ISS_MEMCMP (pIssExL2FilterEntry->IssL2FilterSrcMacAddr, MacAddr,
                         sizeof (tMacAddr)) == 0))
        {
            /* Check whether the corresponding ACL action was DROP */
            if (pIssExL2FilterEntry->IssL2FilterAction == ISS_DROP)
            {
#ifdef NPAPI_WANTED
                if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                {
                    /* Call the hardware API to add the L2 filter */
                    i4RetVal =
                        IsssysIssHwUpdateL2Filter (pIssExL2FilterEntry,
                                ISS_L2FILTER_SRCMAC_DENY);

                    if (i4RetVal != FNP_SUCCESS)
                    {
                        pIssExL2FilterEntry->u1IssL2FilterStatus =
                            ISS_NOT_IN_SERVICE;
                        return SNMP_FAILURE;
                    }
                }
#endif

                return ISS_DROP;
            }
            else if (pIssExL2FilterEntry->IssL2FilterAction == ISS_ALLOW)
            {
                return ISS_ALLOW;
            }
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L2FILTER_EX_LIST, pSllNode);
    }
    return ISS_ALLOW;
}

/*****************************************************************************/
/* Function Name      : IssExtCompareL2FilterInfo                            */
/*                                                                           */
/* Description        : This function compares the given filter information  */
/*                      with the existing filter information.                */
/*                                                                           */
/* Input(s)           : u2PortIndex                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS - On success                             */
/*                      ISS_FAILURE - On failure                             */
/*****************************************************************************/
INT4
IssExtCompareL2FilterInfo (tIssL2FilterEntry * pL2FilterEntry)
{

    tIssSllNode        *pSllNode = NULL;
    tIssL2FilterEntry  *pIssExL2FilterEntry = NULL;
    tMacAddr            zeroAddr;

    MEMSET (zeroAddr, 0, sizeof (tMacAddr));

    pSllNode = ISS_SLL_FIRST (&ISS_L2FILTER_EX_LIST);
    while (pSllNode != NULL)
    {
        pIssExL2FilterEntry = (tIssL2FilterEntry *) pSllNode;
        if (pL2FilterEntry->i4IssL2FilterNo !=
            pIssExL2FilterEntry->i4IssL2FilterNo)
        {
            if ((MEMCMP (pIssExL2FilterEntry->IssL2FilterDstMacAddr, zeroAddr,
                         sizeof (tMacAddr))) != 0)
            {
                if ((MEMCMP (pIssExL2FilterEntry->IssL2FilterDstMacAddr,
                             pL2FilterEntry->IssL2FilterDstMacAddr,
                             sizeof (tMacAddr)) == 0))
                {
                    if (pIssExL2FilterEntry->u4IssL2FilterCustomerVlanId ==
                        pL2FilterEntry->u4IssL2FilterCustomerVlanId)
                    {
                        return SNMP_SUCCESS;
                    }
                }
            }
            else if ((MEMCMP (pIssExL2FilterEntry->IssL2FilterSrcMacAddr,
                              zeroAddr, sizeof (tMacAddr))) != 0)
            {
                if ((MEMCMP (pIssExL2FilterEntry->IssL2FilterSrcMacAddr,
                             pL2FilterEntry->IssL2FilterSrcMacAddr,
                             sizeof (tMacAddr)) == 0))
                {
                    if (pIssExL2FilterEntry->u4IssL2FilterCustomerVlanId ==
                        pL2FilterEntry->u4IssL2FilterCustomerVlanId)
                    {
                        return SNMP_SUCCESS;
                    }
                }
            }

        }

        pSllNode = ISS_SLL_NEXT (&ISS_L2FILTER_EX_LIST, pSllNode);
    }
    return SNMP_FAILURE;
}
