#ifdef WEBNM_WANTED
#include "issexweb.h"

/*********************************************************************
*  Function Name : IssProcessCustomPages
*  Description   : This function processes the Pages specific to
*                  target
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
INT4
IssProcessCustomPages (tHttp * pHttp)
{
    UINT4               u4Count;

    for (u4Count = 0; asIssTargetpages[u4Count].au1Page[0] != '\0'; u4Count++)
    {
        if (STRCMP (pHttp->ai1HtmlName, asIssTargetpages[u4Count].au1Page) == 0)
        {
            asIssTargetpages[u4Count].pfunctPtr (pHttp);
            return ISS_SUCCESS;
        }
    }
    return ISS_FAILURE;
}

/************************************************************************
*  Function Name   : IssRedirectMacFilterPage
*  Description     : This function will redirect Mac Filter page
*  Input           : pHttp - Pointer to http entry where the html file
*                    name needs to be changed(copied)
*  Output          : None
*  Returns         : None
************************************************************************/
VOID
IssRedirectMacFilterPage (tHttp * pHttp)
{
    STRCPY (pHttp->ai1HtmlName, "mrvlls_mac_filterconf.html");
}

/************************************************************************
*  Function Name   : IssRedirectRateCtrlPage
*  Description     : This function will redirect Mac Filter page
*  Input           : pHttp - Pointer to http entry where the html file
*                    name needs to be changed(copied)
*  Output          : None
*  Returns         : None
************************************************************************/
VOID
IssRedirectRateCtrlPage (tHttp * pHttp)
{
    STRCPY (pHttp->ai1HtmlName, "mrvlls_port_ratectrl.html");
}

/*********************************************************************
*  Function Name : IssProcessMACFilterPage
*  Description   : This function processes the MAC Based ACL Filter
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
VOID
IssProcessMACFilterPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        if (IssProcessPost (pHttp) == SUCCESS)
        {
            IssProcessGet (pHttp);
        }
    }
    return;
}

/*********************************************************************
*  Function Name : IssProcessPortRateCtrlPage
*  Description   : This function processes the Port Rate Control
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
VOID
IssProcessPortRateCtrlPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        STRCPY (pHttp->ai1HtmlName, "port_ratectrl.html");
        IssProcessGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        if (IssProcessPost (pHttp) == SUCCESS)
        {
            IssProcessGet (pHttp);
        }
    }
    return;
}

/************************************************************************ 
*  Function Name   : IssRedirectDiffSrvPage 
*  Description     : This function will redirect Diffsrv page 
*  Input           : pHttp - Pointer to http entry where the html file 
*                    name needs to be changed(copied) 
*  Output          : None 
*  Returns         : None 
************************************************************************/
VOID
IssRedirectDiffSrvPage (tHttp * pHttp)
{
    STRCPY (pHttp->ai1HtmlName, "mrvlls_dfs_globalconf.html");
}

#endif
