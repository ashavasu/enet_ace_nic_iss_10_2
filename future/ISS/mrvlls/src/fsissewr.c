/********************************************************************
 * * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * *
 * * $Id: fsissewr.c,v 1.3 2016/03/19 13:10:52 siva Exp $
 * *
 * * Description: Proto types for Low Level  Routines
 * *********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "iss.h"
# include  "fsisselw.h"
# include  "fsissewr.h"
# include  "fsissedb.h"

INT4
GetNextIndexIssExtRateCtrlTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIssExtRateCtrlTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIssExtRateCtrlTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

VOID
RegisterFSISSE ()
{
    SNMPRegisterMibWithLock (&fsisseOID, &fsisseEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fsisseOID, (const UINT1 *) "fsissext");
}

VOID
UnRegisterFSISSE ()
{
    SNMPUnRegisterMib (&fsisseOID, &fsisseEntry);
    SNMPDelSysorEntry (&fsisseOID, (const UINT1 *) "fsissext");
}

INT4
IssExtRateCtrlPacketsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtRateCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtRateCtrlPackets (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
IssExtRateCtrlLimitValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtRateCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtRateCtrlLimitValue
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssExtRateCtrlPortRateLimitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtRateCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtRateCtrlPortRateLimit
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssExtRateCtrlPortBurstSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtRateCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtRateCtrlPortBurstSize
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssExtRateCtrlPacketsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtRateCtrlPackets (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
IssExtRateCtrlLimitValueSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtRateCtrlLimitValue
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssExtRateCtrlPortRateLimitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtRateCtrlPortRateLimit
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssExtRateCtrlPortBurstSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtRateCtrlPortBurstSize
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssExtRateCtrlPacketsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2IssExtRateCtrlPackets (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
IssExtRateCtrlLimitValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2IssExtRateCtrlLimitValue (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
IssExtRateCtrlPortRateLimitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2IssExtRateCtrlPortRateLimit (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
IssExtRateCtrlPortBurstSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2IssExtRateCtrlPortBurstSize (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
IssExtRateCtrlTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssExtRateCtrlTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIssExtL2FilterTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIssExtL2FilterTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIssExtL2FilterTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
IssExtL2FilterDstMacAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    return (nmhGetIssExtL2FilterDstMacAddr
            (pMultiIndex->pIndex[0].i4_SLongValue,
             (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList));

}

INT4
IssExtL2FilterSrcMacAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    return (nmhGetIssExtL2FilterSrcMacAddr
            (pMultiIndex->pIndex[0].i4_SLongValue,
             (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList));

}

INT4
IssExtL2FilterVlanIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL2FilterVlanId (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
IssExtL2FilterActionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL2FilterAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
IssExtL2FilterStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL2FilterStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
IssExtL2FilterDstMacAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL2FilterDstMacAddr
            (pMultiIndex->pIndex[0].i4_SLongValue,
             (*(tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList)));

}

INT4
IssExtL2FilterSrcMacAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL2FilterSrcMacAddr
            (pMultiIndex->pIndex[0].i4_SLongValue,
             (*(tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList)));

}

INT4
IssExtL2FilterVlanIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL2FilterVlanId (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
IssExtL2FilterActionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL2FilterAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
IssExtL2FilterStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL2FilterStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
IssExtL2FilterDstMacAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return (nmhTestv2IssExtL2FilterDstMacAddr (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               (*(tMacAddr *) pMultiData->
                                                pOctetStrValue->
                                                pu1_OctetList)));

}

INT4
IssExtL2FilterSrcMacAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return (nmhTestv2IssExtL2FilterSrcMacAddr (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               (*(tMacAddr *) pMultiData->
                                                pOctetStrValue->
                                                pu1_OctetList)));

}

INT4
IssExtL2FilterVlanIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL2FilterVlanId (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
IssExtL2FilterActionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL2FilterAction (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
IssExtL2FilterStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL2FilterStatus (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
IssExtL2FilterTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssExtL2FilterTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
