/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: aclmstub.c,v 1.1.1.1 2008/11/03 12:48:38 premap-iss Exp $
 *
 * Description: This file contains Metro ACL configuration routines
 *********************************************************************/
#ifndef __ACLMSTUB_C__
#define __ACLMSTUB_C__

#include "lr.h"
#include "issexinc.h"
#include "aclmcli.h"

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclPbTestL3Filter                                  */
/*                                                                           */
/*     DESCRIPTION      : This function Tests Provider Bribdge L3 Filter     */
/*                        objects                                            */
/*                                                                           */
/*     INPUT            : i4Filter - MAC ACL number                          */
/*                        i4SVlan - service vlan id                          */
/*                        i4SVlanPrio - Service Vlan Priority                */
/*                        i4CVlan     - Customer vlan id                     */
/*                        i4CVlanPrio - Customer Vlan Priority               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/

INT4
AclPbTestL3Filter (UINT4 *pu4ErrCode, tCliHandle CliHandle, INT4 i4Filter,
                   INT4 i4SVlan, INT4 i4SVlanPrio, INT4 i4CVlan,
                   INT4 i4CVlanPrio)
{
    UNUSED_PARAM (pu4ErrCode);
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4Filter);
    UNUSED_PARAM (i4SVlan);
    UNUSED_PARAM (i4SVlanPrio);
    UNUSED_PARAM (i4CVlan);
    UNUSED_PARAM (i4CVlanPrio);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclPbGetL3Filter                                   */
/*                                                                           */
/*     DESCRIPTION      : This function Get Provider Bribdge L3 Filter       */
/*                        objects                                            */
/*                                                                           */
/*     INPUT            : i4Filter - MAC ACL number                          */
/*                        i4SVlan - service vlan id                          */
/*                        i4SVlanPrio - Service Vlan Priority                */
/*                        i4CVlan     - Customer vlan id                     */
/*                        i4CVlanPrio - Customer Vlan Priority               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/

INT4
AclPbGetL3Filter (INT4 i4Filter, INT4 *pi4SVlan, INT4 *pi4SVlanPrio,
                  INT4 *pi4CVlan, INT4 *pi4CVlanPrio, INT4 *pi4TagType)
{
    UNUSED_PARAM (i4Filter);
    UNUSED_PARAM (pi4SVlan);
    UNUSED_PARAM (pi4SVlanPrio);
    UNUSED_PARAM (pi4CVlan);
    UNUSED_PARAM (pi4CVlanPrio);
    UNUSED_PARAM (pi4TagType);
    return SNMP_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclPbGetL2Filter                                   */
/*                                                                           */
/*     DESCRIPTION      : This function Get Provider Bribdge L2 Filter       */
/*                        objects                                            */
/*                                                                           */
/*     INPUT            : i4Filter - MAC ACL number                          */
/*                                                                           */
/*     OUTPUT             pu4OuterEType - Outer EtherType                    */
/*                        pu4InnerEType - Inner EtherType                    */
/*                        pi4SVlan - service vlan id                         */
/*                        pi4SVlanPrio - Service Vlan Priority               */
/*                        pi4CVlanPrio - Customer Vlan Priority              */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/

INT4
AclPbGetL2Filter (INT4 i4Filter, INT4 *pi4OuterEType, INT4 *pi4InnerEType,
                  INT4 *pi4SVlan, INT4 *pi4SVlanPrio, INT4 *pi4CVlanPrio)
{
    UNUSED_PARAM (i4Filter);
    UNUSED_PARAM (pi4OuterEType);
    UNUSED_PARAM (pi4InnerEType);
    UNUSED_PARAM (pi4SVlan);
    UNUSED_PARAM (pi4SVlanPrio);
    UNUSED_PARAM (pi4CVlanPrio);

    return SNMP_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclPbSetL3Filter                                   */
/*                                                                           */
/*     DESCRIPTION      : This function Set Provider Bribdge L3 Filter       */
/*                        objects                                            */
/*                                                                           */
/*     INPUT            : i4Filter - MAC ACL number                          */
/*                        i4SVlan - service vlan id                          */
/*                        i4SVlanPrio - Service Vlan Priority                */
/*                        i4CVlan     - Customer vlan id                     */
/*                        i4CVlanPrio - Customer Vlan Priority               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/

INT4
AclPbSetL3Filter (tCliHandle CliHandle, INT4 i4Filter, INT4 i4SVlan,
                  INT4 i4SVlanPrio, INT4 i4CVlan, INT4 i4CVlanPrio)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4Filter);
    UNUSED_PARAM (i4SVlan);
    UNUSED_PARAM (i4SVlanPrio);
    UNUSED_PARAM (i4CVlan);
    UNUSED_PARAM (i4CVlanPrio);
    return SNMP_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclPbTestSetL3TagType                              */
/*                                                                           */
/*     DESCRIPTION      : This function validates and sets L3 filter packet  */
/*                        Tag Type for which the filter will be applied      */
/*                                                                           */
/*     INPUT            : i4Filter - MAC ACL number                          */
/*                        i4TagType - Packet Tag Type on which the filter    */
/*                                    be applied                             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclPbTestSetL3TagType (UINT4 *pu4ErrCode, tCliHandle CliHandle,
                       INT4 i4Filter, INT4 i4TagType)
{
    UNUSED_PARAM (pu4ErrCode);
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4Filter);
    UNUSED_PARAM (i4TagType);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclPbTestL2Filter                                  */
/*                                                                           */
/*     DESCRIPTION      : This function Test Provider Bribdge L2 Filter      */
/*                        objects                                            */
/*                                                                           */
/*     INPUT            : i4Filter - MAC ACL number                          */
/*                        i4OuterEType - Outer EtherType                     */
/*                        i4SVlan - service vlan id                          */
/*                        i4SVlanPrio - Service Vlan Priority                */
/*                        i4CVlanPrio - Customer Vlan Priority               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/

INT4
AclPbTestL2Filter (UINT4 *pu4ErrCode, tCliHandle CliHandle, INT4 i4Filter,
                   INT4 i4OuterEType, INT4 i4SVlan, INT4 i4SVlanPrio,
                   INT4 i4CVlanPrio)
{
    UNUSED_PARAM (pu4ErrCode);
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4Filter);
    UNUSED_PARAM (i4OuterEType);
    UNUSED_PARAM (i4SVlan);
    UNUSED_PARAM (i4SVlanPrio);
    UNUSED_PARAM (i4CVlanPrio);
    return SNMP_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclPbSetL2Filter                                   */
/*                                                                           */
/*     DESCRIPTION      : This function Set Provider Bribdge L2 Filter       */
/*                        objects                                            */
/*                                                                           */
/*     INPUT            : i4Filter - MAC ACL number                          */
/*                        i4OuterEType - Outer EtherType                     */
/*                        i4SVlan - service vlan id                          */
/*                        i4SVlanPrio - Service Vlan Priority                */
/*                        i4CVlanPrio - Customer Vlan Priority               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/

INT4
AclPbSetL2Filter (tCliHandle CliHandle, INT4 i4Filter, INT4 i4OuterEType,
                  INT4 i4SVlan, INT4 i4SVlanPrio, INT4 i4CVlanPrio)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4Filter);
    UNUSED_PARAM (i4OuterEType);
    UNUSED_PARAM (i4SVlan);
    UNUSED_PARAM (i4SVlanPrio);
    UNUSED_PARAM (i4CVlanPrio);
    return SNMP_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclPbTestSetL2TagType                              */
/*                                                                           */
/*     DESCRIPTION      : This function tests and set Provider Bribdge L2    */
/*                        Filter Tag Type                                    */
/*                                                                           */
/*     INPUT            : i4Filter - MAC ACL number                          */
/*                        i4TagType - Packet Tag Type on which the filter    */
/*                                    be applied                             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclPbTestSetL2TagType (UINT4 *pu4ErrCode, tCliHandle CliHandle,
                       INT4 i4Filter, INT4 i4TagType)
{
    UNUSED_PARAM (pu4ErrCode);
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4Filter);
    UNUSED_PARAM (i4TagType);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclPbShowL3Filter                                  */
/*                                                                           */
/*     DESCRIPTION      : This function Disaplys Provider Bribdge L3 Filter  */
/*                        objects                                            */
/*                                                                           */
/*     INPUT            : i4SVlan - service vlan id                          */
/*                        i4SVlanPrio - Service Vlan Priority                */
/*                        i4CVlan     - Customer vlan id                     */
/*                        i4CVlanPrio - Customer Vlan Priority               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclPbShowL3Filter (tCliHandle CliHandle, INT4 i4SVlan, INT4 i4SVlanPrio,
                   INT4 i4CVlan, INT4 i4CVlanPrio, INT4 i4TagType)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4SVlan);
    UNUSED_PARAM (i4SVlanPrio);
    UNUSED_PARAM (i4CVlan);
    UNUSED_PARAM (i4CVlanPrio);
    UNUSED_PARAM (i4TagType);

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclPbShowL2Filter                                  */
/*                                                                           */
/*     DESCRIPTION      : This function Displays Provider Bribdge L2 Filter  */
/*                        objects                                            */
/*                                                                           */
/*     INPUT            : i4Filter - MAC ACL number                          */
/*                                                                           */
/*     OUTPUT             i4OuterEType - Outer EtherType                     */
/*                        i4InnerEType - Inner EtherType                     */
/*                        i4SVlan - service vlan id                          */
/*                        i4SVlanPrio - Service Vlan Priority                */
/*                        i4CVlanPrio - Customer Vlan Priority               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclPbShowL2Filter (tCliHandle CliHandle, INT4 i4OuterEType, INT4 i4SVlan,
                   INT4 i4SVlanPrio, INT4 i4InnerEType, INT4 i4CVlanPrio)
{

    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4OuterEType);
    UNUSED_PARAM (i4InnerEType);
    UNUSED_PARAM (i4SVlan);
    UNUSED_PARAM (i4SVlanPrio);
    UNUSED_PARAM (i4CVlanPrio);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RegisterFSISSM                                     */
/*                                                                           */
/*     DESCRIPTION      : This function registers metro mib                  */
/*                                                                           */
/*     INPUT            : None.                                              */
/*                                                                           */
/*     OUTPUT             None.                                              */
/*                                                                           */
/*     RETURNS          : None.                                              */
/*****************************************************************************/
VOID
RegisterFSISSM (VOID)
{
    return;
}
#endif
