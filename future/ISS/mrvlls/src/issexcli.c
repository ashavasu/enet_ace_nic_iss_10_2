/* $Id: issexcli.c,v 1.5 2013/08/29 15:08:46 siva Exp $   */
#ifndef  __ISSEXCLI_C__
#define  __ISSEXCLI_C__

#include "lr.h"
#include "fssnmp.h"
#include "iss.h"
#include "msr.h"
#include "issexinc.h"
#include "fsisselw.h"
#include "isscli.h"
#include "fsissewr.h"
#include "fsissecli.h"

/***************************************************************/
/*  Function Name   : cli_process_iss_ext_cmd                  */
/*  Description     : This function servers as the handler for */
/*                    extended system CLI commands             */
/*  Input(s)        :                                          */
/*                    CliHandle - CLI Handle                   */
/*                    u4Command - Command given by user        */
/*                    Variable set of inputs depending on      */
/*                    the user command                         */
/*  Output(s)       : Error message - on failure               */
/*  Returns         : None                                     */
/***************************************************************/

VOID
cli_process_iss_ext_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[CLI_ISS_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4Inst;
    UINT4               u4IfIndex;
    INT4                i4RetStatus = CLI_SUCCESS;
    INT4                i4RateLimit = ISS_CLI_RATE_LIMIT_INVALID;
    INT4                i4BurstSize = ISS_CLI_RATE_LIMIT_INVALID;

    va_start (ap, u4Command);

    /* Third arguement is always interface name/index */
    i4Inst = va_arg (ap, INT4);

    /* Walk through the rest of the arguements and store in args array.  */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == CLI_ISS_MAX_ARGS)
            break;
    }
    va_end (ap);

    CLI_SET_ERR (0);

    CliRegisterLock (CliHandle, IssLock, IssUnLock);

    ISS_LOCK ();

    switch (u4Command)
    {
        case CLI_ISS_STORM_CONTROL:
            u4IfIndex = CLI_GET_IFINDEX ();
            CliSetIssStormControl (CliHandle, u4IfIndex,
                                   CLI_PTR_TO_U4 (args[0]),
                                   CLI_PTR_TO_U4 (args[1]));
            break;

        case CLI_ISS_NO_STORM_CONTROL:
            u4IfIndex = CLI_GET_IFINDEX ();
            CliSetIssStormControl (CliHandle, u4IfIndex, 0, 0);
            break;

        case CLI_ISS_PORT_RATE_LIMIT:
            u4IfIndex = CLI_GET_IFINDEX ();

            i4RetStatus = CliSetIssConfPortRateLimit (CliHandle, u4IfIndex,
                                                      CLI_PTR_TO_U4 (args[0]),
                                                      i4BurstSize);
            break;

        case CLI_ISS_NO_PORT_RATE_LIMIT:
            u4IfIndex = CLI_GET_IFINDEX ();

            i4RateLimit = ISS_ZERO_ENTRY;

            i4RetStatus = CliSetIssConfPortRateLimit (CliHandle, u4IfIndex,
                                                      i4RateLimit, i4BurstSize);
            break;

    }
    if ((CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode >= CLI_ERR_START_ID_ISS) && 
            (u4ErrCode < CLI_ISS_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", 
                       IssCliErrString[CLI_ERR_OFFSET_ISS (u4ErrCode)]);
        }

        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetStatus);

    CliUnRegisterLock (CliHandle);

    ISS_UNLOCK ();
}

/******************************************************************/
/*  Function Name   : CliSetIssStormControl                       */
/*  Description     : This function is used to set the            */
/*                    strom control limit                         */
/*  Input(s)        :                                             */
/*                    CliHandle   - CLI Handle                    */
/*                    u4IfIndex   - Port on which Storm control   */
/*                                  is applied.                   */
/*                    u4Mode      - Mode (Bcast/Mcast_Bcast/      */
/*                                        DLF_Mcast_Bcast)        */
/*                    u4Rate      - Rate value                    */
/*  Output(s)       : None                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                     */
/******************************************************************/

INT4
CliSetIssStormControl (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4Mode,
                       UINT4 u4Rate)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2IssExtRateCtrlPackets (&u4ErrorCode, u4IfIndex, u4Mode)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetIssExtRateCtrlPackets (u4IfIndex, u4Mode) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2IssExtRateCtrlLimitValue (&u4ErrorCode, u4IfIndex, u4Rate)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIssExtRateCtrlLimitValue (u4IfIndex, u4Rate) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliSetIssConfPortRateLimit               */
/*  Description     : This function is used to configure rate  */
/*                    limit for a port                         */
/*  Input(s)        : CliHandle        -  CLI Handle           */
/*                    u4IfIndex        -  Interface Index      */
/*                    i4PortRateLimit  -  Port rate limit      */
/*                    i4PortBurstSize  -  Burst size limit     */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
INT4
CliSetIssConfPortRateLimit (tCliHandle CliHandle, UINT4 u4IfIndex,
                            INT4 i4PortRateLimit, INT4 i4PortBurstSize)
{
    UINT4               u4ErrorCode;

    UNUSED_PARAM (i4PortBurstSize);

    if (i4PortRateLimit != ISS_CLI_RATE_LIMIT_INVALID)
    {
        if (nmhTestv2IssExtRateCtrlPortRateLimit (&u4ErrorCode, u4IfIndex,
                                                  i4PortRateLimit) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetIssExtRateCtrlPortRateLimit (u4IfIndex, i4PortRateLimit) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

#endif /* __ISSEXCLI_C__ */
