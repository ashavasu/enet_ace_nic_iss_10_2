
#ifndef _ISSEXGLOB_H        
#define _ISSEXGLOB_H        

#ifdef _ISSEXSYS_C
tIssExGlobalInfo                gIssExtGlobalInfo;

#else
extern tIssExGlobalInfo         gIssExtGlobalInfo;

/* This is already defined in issglob.h in ISS/common/system */ 
extern UINT4                  gu4IssCidrSubnetMask[ISS_MAX_CIDR + 1];
#endif

extern UINT4                  gu4IssDebugFlags;

#ifdef _ISSEXSYS_C
tIssExtGlobalInfo                gIssExGlobalInfo;

#else
extern tIssExtGlobalInfo         gIssExGlobalInfo;

/* This is already defined in issglob.h in ISS/common/system */ 
extern UINT4                  gu4IssCidrSubnetMask[ISS_MAX_CIDR + 1];
#endif

#endif
