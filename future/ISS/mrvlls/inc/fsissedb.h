/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissedb.h,v 1.3 2012/02/09 11:22:46 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSISSEDB_H
#define _FSISSEDB_H

UINT1 IssExtRateCtrlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 IssExtL2FilterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 fsisse [] ={1,3,6,1,4,1,2076,81,8};
tSNMP_OID_TYPE fsisseOID = {9, fsisse};


UINT4 IssExtRateCtrlIndex [ ] ={1,3,6,1,4,1,2076,81,8,1,1,1,1};
UINT4 IssExtRateCtrlPackets [ ] ={1,3,6,1,4,1,2076,81,8,1,1,1,2};
UINT4 IssExtRateCtrlLimitValue [ ] ={1,3,6,1,4,1,2076,81,8,1,1,1,3};
UINT4 IssExtRateCtrlPortRateLimit [ ] ={1,3,6,1,4,1,2076,81,8,1,1,1,4};
UINT4 IssExtRateCtrlPortBurstSize [ ] ={1,3,6,1,4,1,2076,81,8,1,1,1,5};
UINT4 IssExtL2FilterNo [ ] ={1,3,6,1,4,1,2076,81,8,2,1,1,1};
UINT4 IssExtL2FilterDstMacAddr [ ] ={1,3,6,1,4,1,2076,81,8,2,1,1,2};
UINT4 IssExtL2FilterSrcMacAddr [ ] ={1,3,6,1,4,1,2076,81,8,2,1,1,3};
UINT4 IssExtL2FilterVlanId [ ] ={1,3,6,1,4,1,2076,81,8,2,1,1,4};
UINT4 IssExtL2FilterAction [ ] ={1,3,6,1,4,1,2076,81,8,2,1,1,5};
UINT4 IssExtL2FilterStatus [ ] ={1,3,6,1,4,1,2076,81,8,2,1,1,6};




tMbDbEntry fsisseMibEntry[]= {

{{13,IssExtRateCtrlIndex}, GetNextIndexIssExtRateCtrlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssExtRateCtrlTableINDEX, 1, 0, 0, NULL},

{{13,IssExtRateCtrlPackets}, GetNextIndexIssExtRateCtrlTable, IssExtRateCtrlPacketsGet, IssExtRateCtrlPacketsSet, IssExtRateCtrlPacketsTest, IssExtRateCtrlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssExtRateCtrlTableINDEX, 1, 0, 0, "0"},

{{13,IssExtRateCtrlLimitValue}, GetNextIndexIssExtRateCtrlTable, IssExtRateCtrlLimitValueGet, IssExtRateCtrlLimitValueSet, IssExtRateCtrlLimitValueTest, IssExtRateCtrlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssExtRateCtrlTableINDEX, 1, 0, 0, "0"},

{{13,IssExtRateCtrlPortRateLimit}, GetNextIndexIssExtRateCtrlTable, IssExtRateCtrlPortRateLimitGet, IssExtRateCtrlPortRateLimitSet, IssExtRateCtrlPortRateLimitTest, IssExtRateCtrlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssExtRateCtrlTableINDEX, 1, 0, 0, "0"},

{{13,IssExtRateCtrlPortBurstSize}, GetNextIndexIssExtRateCtrlTable, IssExtRateCtrlPortBurstSizeGet, IssExtRateCtrlPortBurstSizeSet, IssExtRateCtrlPortBurstSizeTest, IssExtRateCtrlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssExtRateCtrlTableINDEX, 1, 0, 0, NULL},

{{13,IssExtL2FilterNo}, GetNextIndexIssExtL2FilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssExtL2FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssExtL2FilterDstMacAddr}, GetNextIndexIssExtL2FilterTable, IssExtL2FilterDstMacAddrGet, IssExtL2FilterDstMacAddrSet, IssExtL2FilterDstMacAddrTest, IssExtL2FilterTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, IssExtL2FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssExtL2FilterSrcMacAddr}, GetNextIndexIssExtL2FilterTable, IssExtL2FilterSrcMacAddrGet, IssExtL2FilterSrcMacAddrSet, IssExtL2FilterSrcMacAddrTest, IssExtL2FilterTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, IssExtL2FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssExtL2FilterVlanId}, GetNextIndexIssExtL2FilterTable, IssExtL2FilterVlanIdGet, IssExtL2FilterVlanIdSet, IssExtL2FilterVlanIdTest, IssExtL2FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssExtL2FilterTableINDEX, 1, 0, 0, "0"},

{{13,IssExtL2FilterAction}, GetNextIndexIssExtL2FilterTable, IssExtL2FilterActionGet, IssExtL2FilterActionSet, IssExtL2FilterActionTest, IssExtL2FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssExtL2FilterTableINDEX, 1, 0, 0, "1"},

{{13,IssExtL2FilterStatus}, GetNextIndexIssExtL2FilterTable, IssExtL2FilterStatusGet, IssExtL2FilterStatusSet, IssExtL2FilterStatusTest, IssExtL2FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssExtL2FilterTableINDEX, 1, 0, 1, NULL},
};
tMibData fsisseEntry = { 11, fsisseMibEntry };

#endif /* _FSISSEDB_H */

