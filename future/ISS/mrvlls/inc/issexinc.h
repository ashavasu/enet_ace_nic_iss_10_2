
#ifndef _ISSEXINC_H
#define _ISSEXINC_H

/* Common Includes */
#include "lr.h"
#include "cfa.h"
#include "cli.h"
#include "fssocket.h"
#include "rmgr.h"
#include "iss.h"
#include "msr.h"
#include "ip.h"

#include "snmccons.h"
#include "snmcdefn.h"
#include "fssnmp.h"


#ifdef NPAPI_WANTED
#include "npapi.h"
#include "issnp.h"
#endif

/* ISS includes */
#include "issmacro.h"
#include "issexmacr.h"
#include "issextdfs.h"
#include "issexglob.h"
#include "issexprot.h"

#ifdef MBSM_WANTED
#include "mbsm.h"
#include "issmbsm.h"
#endif

#ifdef ISS_METRO_WANTED
#include "fsvlan.h"
#endif

#endif /* _ISSEXINC_H */
