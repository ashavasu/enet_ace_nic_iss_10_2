/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsisselw.h,v 1.3 2010/11/19 05:18:14 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for IssExtRateCtrlTable. */
INT1
nmhValidateIndexInstanceIssExtRateCtrlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IssExtRateCtrlTable  */

INT1
nmhGetFirstIndexIssExtRateCtrlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssExtRateCtrlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssExtRateCtrlPackets ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssExtRateCtrlLimitValue ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssExtRateCtrlPortRateLimit ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssExtRateCtrlPortBurstSize ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssExtRateCtrlPackets ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssExtRateCtrlLimitValue ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssExtRateCtrlPortRateLimit ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssExtRateCtrlPortBurstSize ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssExtRateCtrlPackets ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssExtRateCtrlLimitValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssExtRateCtrlPortRateLimit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssExtRateCtrlPortBurstSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IssExtRateCtrlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IssExtL2FilterTable. */
INT1
nmhValidateIndexInstanceIssExtL2FilterTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IssExtL2FilterTable  */

INT1
nmhGetFirstIndexIssExtL2FilterTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssExtL2FilterTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssExtL2FilterDstMacAddr ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetIssExtL2FilterSrcMacAddr ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetIssExtL2FilterVlanId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssExtL2FilterAction ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssExtL2FilterStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssExtL2FilterDstMacAddr ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetIssExtL2FilterSrcMacAddr ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetIssExtL2FilterVlanId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssExtL2FilterAction ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssExtL2FilterStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssExtL2FilterDstMacAddr ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2IssExtL2FilterSrcMacAddr ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2IssExtL2FilterVlanId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssExtL2FilterAction ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssExtL2FilterStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IssExtL2FilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT4 IssExtSnmpLowValidateIndexPortRateTable (INT4 i4PortIndex);

INT4 IssExtValidateRateCtrlEntry (INT4 i4RateCtrlIndex);

INT4 IssExtSnmpLowGetFirstValidL2FilterTableIndex (INT4 *pi4FirstL2FilterIndex);

INT4 IssExtSnmpLowGetNextValidL2FilterTableIndex (INT4 i4IssExL2FilterNo,
                                               INT4 *pi4NextL2FilterNo);

INT4 IssExtCompareL2FilterInfo PROTO ((tIssL2FilterEntry * pL2FilterEntry));

tIssL3FilterEntry  * IssExtGetL3FilterEntry PROTO ((INT4 i4IssExtL3FilterNo));

INT4 
IssExtQualifyL2FilterData PROTO ((tIssL2FilterEntry **ppIssExtL2FilterEntry));

