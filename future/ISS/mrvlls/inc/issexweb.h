/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: issexweb.h,v 1.2 2010/11/19 05:18:14 siva Exp $
*
* Description: Proto types for Marvell specific web pages
*********************************************************************/
#ifndef _ISSEXWEB_H
#define _ISSEXWEB_H

#include "webiss.h"
#include "iss.h"

tSpecificPage asIssTargetpages [] ={
    {"", NULL}
};

INT4 IssProcessCustomPages (tHttp * pHttp);
VOID IssProcessMACFilterPage (tHttp * pHttp);
VOID IssProcessMACFilterPage (tHttp * pHttp);
VOID IssProcessPortRateCtrlPage (tHttp * pHttp);
VOID IssRedirectMacFilterPage (tHttp *pHttp);
VOID IssRedirectDiffSrvPage (tHttp *pHttp);
VOID IssRedirectRateCtrlPage (tHttp * pHttp);

#endif
