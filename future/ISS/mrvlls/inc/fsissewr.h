#ifndef _FSISSEWR_H
#define _FSISSEWR_H
INT4 GetNextIndexIssExtRateCtrlTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSISSE(VOID);

VOID UnRegisterFSISSE(VOID);
INT4 IssExtRateCtrlPacketsGet(tSnmpIndex *, tRetVal *);
INT4 IssExtRateCtrlLimitValueGet(tSnmpIndex *, tRetVal *);
INT4 IssExtRateCtrlPortRateLimitGet(tSnmpIndex *, tRetVal *);
INT4 IssExtRateCtrlPortBurstSizeGet(tSnmpIndex *, tRetVal *);
INT4 IssExtRateCtrlPacketsSet(tSnmpIndex *, tRetVal *);
INT4 IssExtRateCtrlLimitValueSet(tSnmpIndex *, tRetVal *);
INT4 IssExtRateCtrlPortRateLimitSet(tSnmpIndex *, tRetVal *);
INT4 IssExtRateCtrlPortBurstSizeSet(tSnmpIndex *, tRetVal *);
INT4 IssExtRateCtrlPacketsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IssExtRateCtrlLimitValueTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IssExtRateCtrlPortRateLimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IssExtRateCtrlPortBurstSizeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IssExtRateCtrlTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexIssExtL2FilterTable(tSnmpIndex *, tSnmpIndex *);
INT4 IssExtL2FilterDstMacAddrGet(tSnmpIndex *, tRetVal *);
INT4 IssExtL2FilterSrcMacAddrGet(tSnmpIndex *, tRetVal *);
INT4 IssExtL2FilterVlanIdGet(tSnmpIndex *, tRetVal *);
INT4 IssExtL2FilterActionGet(tSnmpIndex *, tRetVal *);
INT4 IssExtL2FilterStatusGet(tSnmpIndex *, tRetVal *);
INT4 IssExtL2FilterDstMacAddrSet(tSnmpIndex *, tRetVal *);
INT4 IssExtL2FilterSrcMacAddrSet(tSnmpIndex *, tRetVal *);
INT4 IssExtL2FilterVlanIdSet(tSnmpIndex *, tRetVal *);
INT4 IssExtL2FilterActionSet(tSnmpIndex *, tRetVal *);
INT4 IssExtL2FilterStatusSet(tSnmpIndex *, tRetVal *);
INT4 IssExtL2FilterDstMacAddrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IssExtL2FilterSrcMacAddrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IssExtL2FilterVlanIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IssExtL2FilterActionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IssExtL2FilterStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IssExtL2FilterTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _FSISSEWR_H */
