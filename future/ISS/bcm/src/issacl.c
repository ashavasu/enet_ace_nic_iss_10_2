/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: issacl.c,v 1.12 2016/07/09 09:40:39 siva Exp $
 *
 * Description: This file contains routines for traffic separation and 
 *              protection against control plane CPU overloading, when 
 *              multiple protocol control traffic is directed to CPU for 
 *              processing and software controlled MAC learning and ageing 
 *              mechanism. This feature early detects the unwanted traffic 
 *              flow to control plane and avoids or rate limits this traffic 
 *              reaching to control plane with the help of ACL and QoS 
 *              features. Thus provides predictability in processing of 
 *              traffic at control plane.
 *********************************************************************/
#include "issexinc.h"
#include "fsissalw.h"
#include "fsissmlw.h"
#include "fsisselw.h"
#include "qosxtd.h"
#include "issacl.h"
#include "webiss.h"
#include "iss.h"
#include "aclcli.h"                /* To support ACL Counters */
/*****************************************************************************/
/* Function Name      :  IssAclInstallL2Filter                               */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                      to install new L2 ACL rule.                          */
/*                                                                           */
/* Input(s)           : pIssL2Filter - Pointer to ACL L2 Filter entry        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
IssAclInstallL2Filter (tIssL2FilterEntry * pIssL2Filter)
{
    INT4                i4Status = ISS_ZERO_ENTRY;
    UINT4               u4ErrCode = ISS_ZERO_ENTRY;
    tSNMP_OCTET_STRING_TYPE PortList;
    tIssL2FilterEntry  *pIssL2FilterInfo = NULL;
    INT1               *pu1String = NULL;
    UINT1               au1String[ISS_ACL_MAX_LEN];
    UINT1               au1InPortList[ISS_PORT_LIST_SIZE];
    UINT1               au1OutPortList[ISS_PORT_LIST_SIZE];
    UINT1               au1MacAddr[CFA_ENET_ADDR_LEN];

    MEMSET (&PortList, 0x0, sizeof (PortList));
    MEMSET (au1String, 0x0, ISS_ACL_MAX_LEN);
    MEMSET (au1InPortList, 0, ISS_PORT_LIST_SIZE);
    MEMSET (au1OutPortList, 0, ISS_PORT_LIST_SIZE);

    ISS_TRC (INIT_SHUT_TRC, "\nACL:Entry IssAclInstallL2Filter function \n");
    pu1String = (INT1 *) &au1String[0];

    if (pIssL2Filter == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nACL:pIssL2Filter is NULL \n");
        return (ISS_FAILURE);
    }

    /* if filter already exists, do nothing and enter the configuration mode */

    if (nmhGetIssAclL2FilterStatus (pIssL2Filter->i4IssL2FilterNo, &i4Status)
        == SNMP_FAILURE)
    {
        /* Create a filter */
        if (nmhTestv2IssAclL2FilterStatus (&u4ErrCode,
                                           pIssL2Filter->i4IssL2FilterNo,
                                           ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nACL:Failed to test L2 Filter Status for filter ID %d  with error code as %d \n",
                          pIssL2Filter->i4IssL2FilterNo, u4ErrCode);
            return (ISS_FAILURE);
        }

        if (nmhSetIssAclL2FilterStatus (pIssL2Filter->i4IssL2FilterNo,
                                        ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nACL:Failed to set L2 Filter Status for filter ID %d \n",
                          pIssL2Filter->i4IssL2FilterNo);
            return (ISS_FAILURE);
        }
    }
    else if (i4Status != ISS_NOT_READY)
    {
        /* Filter has previously been configured, it must be overwriten */
        if (nmhSetIssAclL2FilterStatus (pIssL2Filter->i4IssL2FilterNo,
                                        ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nACL:Failed to set L2 Filter Status for filter ID %d \n",
                          pIssL2Filter->i4IssL2FilterNo);
            return (ISS_FAILURE);
        }
        if (nmhSetIssAclL2FilterStatus (pIssL2Filter->i4IssL2FilterNo,
                                        ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nACL:Failed to set L2 Filter Status for filter ID %d \n",
                          pIssL2Filter->i4IssL2FilterNo);
            return (ISS_FAILURE);
        }
    }

    /* OID:issAclL2FilterPriority */
    if (nmhTestv2IssAclL2FilterPriority (&u4ErrCode,
                                         pIssL2Filter->i4IssL2FilterNo,
                                         pIssL2Filter->i4IssL2FilterPriority)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test  L2 Filter Priority %d for Filter ID %dwith error code as %d\n",
                      pIssL2Filter->i4IssL2FilterPriority,
                      pIssL2Filter->i4IssL2FilterNo, u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL2FilterPriority (pIssL2Filter->i4IssL2FilterNo,
                                  pIssL2Filter->i4IssL2FilterPriority);

    /* OID:issAclL2FilterEtherType */
    if (nmhTestv2IssAclL2FilterEtherType (&u4ErrCode,
                                          pIssL2Filter->i4IssL2FilterNo,
                                          pIssL2Filter->u2InnerEtherType)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test  L2 Filter Inner Ether Type %d for Filter ID %dwith error code as %d\n",
                      pIssL2Filter->u2InnerEtherType,
                      pIssL2Filter->i4IssL2FilterNo, u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL2FilterEtherType (pIssL2Filter->i4IssL2FilterNo,
                                   pIssL2Filter->u2InnerEtherType);

    /* OID:issAclL2FilterProtocolType */
    if (nmhTestv2IssAclL2FilterProtocolType
        (&u4ErrCode,
         pIssL2Filter->i4IssL2FilterNo,
         pIssL2Filter->u4IssL2FilterProtocolType) == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test  L2 Filter Inner Ether Type %d for Filter ID %dwith error code as %d\n",
                      pIssL2Filter->u4IssL2FilterProtocolType,
                      pIssL2Filter->i4IssL2FilterNo, u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL2FilterProtocolType
        (pIssL2Filter->i4IssL2FilterNo,
         pIssL2Filter->u4IssL2FilterProtocolType);

    /* OID:issAclL2FilterSrcMacAddr */
    MEMSET (au1MacAddr, 0, CFA_ENET_ADDR_LEN);

    if (MEMCMP (au1MacAddr, pIssL2Filter->IssL2FilterSrcMacAddr,
                CFA_ENET_ADDR_LEN) != 0)
    {

        if (nmhTestv2IssAclL2FilterSrcMacAddr (&u4ErrCode,
                                               pIssL2Filter->i4IssL2FilterNo,
                                               pIssL2Filter->
                                               IssL2FilterSrcMacAddr) ==
            SNMP_FAILURE)
        {
            printf ("ACLL2:Failure of"
                    "nmhTestv2IssAclL2FilterSrcMacAddr in %s, %d\n",
                    __FUNCTION__, __LINE__);
            PrintMacAddress (pIssL2Filter->IssL2FilterDstMacAddr,
                             (UINT1 *) pu1String);
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nACL:Failed to test  L2 Filter SrcMacAddr %s for Filter No %d   with error code as %d\n",
                          pu1String, pIssL2Filter->i4IssL2FilterNo, u4ErrCode);
            MEMSET (au1String, 0x0, ISS_ACL_MAX_LEN);
            return (ISS_FAILURE);
        }

        nmhSetIssAclL2FilterSrcMacAddr (pIssL2Filter->i4IssL2FilterNo,
                                        pIssL2Filter->IssL2FilterSrcMacAddr);
    }
    /*OID:issAclL2FilterVlanId */
    if (nmhTestv2IssAclL2FilterVlanId
        (&u4ErrCode,
         pIssL2Filter->i4IssL2FilterNo,
         pIssL2Filter->u4IssL2FilterCustomerVlanId) == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test  L2 Filter Customer Vlan Id %d for Filter No %d                                             with error code as %d\n",
                      pIssL2Filter->u4IssL2FilterCustomerVlanId,
                      pIssL2Filter->i4IssL2FilterNo, u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL2FilterVlanId (pIssL2Filter->i4IssL2FilterNo,
                                pIssL2Filter->u4IssL2FilterCustomerVlanId);

    /* L2FilterInPortList */
    PortList.pu1_OctetList = &au1InPortList[0];
    MEMSET (PortList.pu1_OctetList, 0x0, sizeof (tIssPortList));
    MEMCPY (PortList.pu1_OctetList, pIssL2Filter->IssL2FilterInPortList,
            ISS_PORT_LIST_SIZE);
    PortList.i4_Length = ISS_PORT_LIST_SIZE;

    /* OID:issAclL2FilterInPortList */
    if (nmhTestv2IssAclL2FilterInPortList (&u4ErrCode,
                                           pIssL2Filter->i4IssL2FilterNo,
                                           &PortList) == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test  L2 Filter InPortList %x for Filter No %d                                             with error code as %d\n",
                      *(PortList.pu1_OctetList), pIssL2Filter->i4IssL2FilterNo,
                      u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL2FilterInPortList (pIssL2Filter->i4IssL2FilterNo, &PortList);

    /* OID:issAclL2FilterAction */
    if (nmhTestv2IssAclL2FilterAction (&u4ErrCode,
                                       pIssL2Filter->i4IssL2FilterNo,
                                       pIssL2Filter->IssL2FilterAction)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test  L2 Filter action %d for Filter No %d                                             with error code as %d\n",
                      pIssL2Filter->IssL2FilterAction,
                      pIssL2Filter->i4IssL2FilterNo, u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL2FilterAction (pIssL2Filter->i4IssL2FilterNo,
                                pIssL2Filter->IssL2FilterAction);

    /* OID:issAclL2FilterMatchCount --- Read-Only */

    /* OID:issAclL2FilterOutPortList */
    PortList.pu1_OctetList = &au1OutPortList[0];
    MEMSET (PortList.pu1_OctetList, 0x0, sizeof (tIssPortList));
    MEMCPY (PortList.pu1_OctetList, pIssL2Filter->IssL2FilterOutPortList,
            ISS_PORT_LIST_SIZE);
    PortList.i4_Length = ISS_PORT_LIST_SIZE;
    if (nmhTestv2IssExtL2FilterOutPortList (&u4ErrCode,
                                            pIssL2Filter->i4IssL2FilterNo,
                                            &PortList) == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test  L2 Filter Out PortList %x for Filter No %d                                             with error code as %d\n",
                      *(PortList.pu1_OctetList), pIssL2Filter->i4IssL2FilterNo,
                      u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssExtL2FilterOutPortList (pIssL2Filter->i4IssL2FilterNo, &PortList);

    /* OID:issAclL2FilterDirection */
    if (nmhTestv2IssExtL2FilterDirection (&u4ErrCode,
                                          pIssL2Filter->i4IssL2FilterNo,
                                          pIssL2Filter->u1FilterDirection)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test  L2 Filter Direction %d for Filter No %d                                             with error code as %d\n",
                      pIssL2Filter->u1FilterDirection,
                      pIssL2Filter->i4IssL2FilterNo, u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssExtL2FilterDirection (pIssL2Filter->i4IssL2FilterNo,
                                   pIssL2Filter->u1FilterDirection);

    /* Updating Metro L2 parameters */
    pIssL2FilterInfo = IssExtGetL2FilterEntry (pIssL2Filter->i4IssL2FilterNo);

    /* OID:issAclL2FilterCreationMode */
    pIssL2FilterInfo->u1IssL2FilterCreationMode
        = pIssL2Filter->u1IssL2FilterCreationMode;
#ifdef ISS_METRO_WANTED
    /* OID: issMetroL2FilterOuterEtherType from "fsissmet.mib" */
    if (nmhTestv2IssMetroL2FilterOuterEtherType (&u4ErrCode,
                                                 pIssL2Filter->i4IssL2FilterNo,
                                                 pIssL2Filter->
                                                 u2OuterEtherType) ==
        SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test  L2 Filter Outer Ether Type %d for Filter No %d                                             with error code as %d\n",
                      pIssL2Filter->u2OuterEtherType,
                      pIssL2Filter->i4IssL2FilterNo, u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssMetroL2FilterOuterEtherType (pIssL2Filter->i4IssL2FilterNo,
                                          pIssL2Filter->u2OuterEtherType);

    /* OID: issMetroL2FilterSVlanId from "fsissmet.mib" */
    if (nmhTestv2IssMetroL2FilterSVlanId (&u4ErrCode,
                                          pIssL2Filter->i4IssL2FilterNo,
                                          pIssL2Filter->
                                          u4IssL2FilterServiceVlanId) ==
        SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test  L2 Filter Service Vlan Id %d for Filter No %d                                             with error code as %d\n",
                      pIssL2Filter->u4IssL2FilterServiceVlanId,
                      pIssL2Filter->i4IssL2FilterNo, u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssMetroL2FilterSVlanId (pIssL2Filter->i4IssL2FilterNo,
                                   pIssL2Filter->u4IssL2FilterServiceVlanId);

    /* OID: issMetroL2FilterSVlanPriority from "fsissmet.mib" */
    if (nmhTestv2IssMetroL2FilterSVlanPriority (&u4ErrCode,
                                                pIssL2Filter->i4IssL2FilterNo,
                                                pIssL2Filter->
                                                i1IssL2FilterSVlanPriority) ==
        SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test  L2 Filter Service Vlan Priority %d for Filter No %d                                             with error code as %d\n",
                      pIssL2Filter->i1IssL2FilterSVlanPriority,
                      pIssL2Filter->i4IssL2FilterNo, u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssMetroL2FilterSVlanPriority
        (pIssL2Filter->i4IssL2FilterNo,
         pIssL2Filter->i1IssL2FilterSVlanPriority);

    /* OID: issMetroL2FilterCVlanPriority from "fsissmet.mib" */
    if (nmhTestv2IssMetroL2FilterCVlanPriority (&u4ErrCode,
                                                pIssL2Filter->i4IssL2FilterNo,
                                                pIssL2Filter->
                                                i1IssL2FilterCVlanPriority) ==
        SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test  L2 Filter customer Vlan Priority %d for Filter No %d                                             with error code as %d\n",
                      pIssL2Filter->i1IssL2FilterCVlanPriority,
                      pIssL2Filter->i4IssL2FilterNo, u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssMetroL2FilterCVlanPriority
        (pIssL2Filter->i4IssL2FilterNo,
         pIssL2Filter->i1IssL2FilterCVlanPriority);

    /* OID: issMetroL2FilterPacketTagType from "fsissmet.mib" */
    if (nmhTestv2IssMetroL2FilterPacketTagType (&u4ErrCode,
                                                pIssL2Filter->i4IssL2FilterNo,
                                                pIssL2Filter->
                                                u1IssL2FilterTagType) ==
        SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test  L2 Filter Packet TagType %d for Filter No %d                                             with error code as %d\n",
                      pIssL2Filter->u1IssL2FilterTagType,
                      pIssL2Filter->i4IssL2FilterNo, u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssMetroL2FilterPacketTagType (pIssL2Filter->i4IssL2FilterNo,
                                         pIssL2Filter->u1IssL2FilterTagType);
#endif /*ISS_METRO_WANTED */

    /* OID:issAclL2FilterStatus */
    nmhSetIssAclL2FilterStatus (pIssL2Filter->i4IssL2FilterNo, ACTIVE);

    ISS_TRC (INIT_SHUT_TRC, "\nACL:Exit IssAclInstallL2Filter function \n");
    return (ISS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      :  IssAclUnInstallL2Filter                             */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                            to delete the added L2 ACL rule.               */
/*                                                                           */
/* Input(s)           : pIssL2Filter - Pointer to ACL L2 Filter entry        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
IssAclUnInstallL2Filter (tIssL2FilterEntry * pIssL2Filter)
{
    INT4                i4Status = ISS_ZERO_ENTRY;
    UINT4               u4ErrCode = ISS_ZERO_ENTRY;
    ISS_TRC (INIT_SHUT_TRC, "\nACL:Entry IssAclUnInstallL2Filter  function \n");
    if (pIssL2Filter == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nACL:pIssL2Filter is NULL \n");
        return (ISS_FAILURE);
    }

    if (nmhGetIssAclL2FilterStatus (pIssL2Filter->i4IssL2FilterNo, &i4Status)
        == SNMP_FAILURE)
    {
        /* Filter does not exist */
        return (ISS_SUCCESS);
    }
    else
    {
        if (nmhTestv2IssAclL2FilterStatus (&u4ErrCode,
                                           pIssL2Filter->i4IssL2FilterNo,
                                           ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nACL:Failed to test  L2 Filter status for Filter ID %d                                                  with error code as %d\n",
                          pIssL2Filter->i4IssL2FilterNo, u4ErrCode);
            return (ISS_FAILURE);
        }

        if (nmhSetIssAclL2FilterStatus (pIssL2Filter->i4IssL2FilterNo,
                                        ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nACL:Failed to set L2 Filter status for Filter ID %d\n",
                          pIssL2Filter->i4IssL2FilterNo);
            return (ISS_FAILURE);
        }
    }
    ISS_TRC (INIT_SHUT_TRC, "\nACL:Exit IssAclUnInstallL2Filter  function \n");
    return (ISS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      :  IssAclInstallL3Filter                               */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                            to install new L3 ACL rule.                    */
/*                                                                           */
/* Input(s)           : pIssL3Filter - Pointer to ACL L3 Filter entry        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
IssAclInstallL3Filter (tIssL3FilterEntry * pIssL3Filter)
{
    INT4                i4Status = ISS_ZERO_ENTRY;
    UINT4               u4ErrCode = ISS_ZERO_ENTRY;
    tSNMP_OCTET_STRING_TYPE IpAddr;
    tSNMP_OCTET_STRING_TYPE PortList;
    tIssL3FilterEntry  *pIssMetroL3Filter = NULL;
    UINT1               au1InPortList[ISS_PORT_LIST_SIZE];
    UINT1               au1OutPortList[ISS_PORT_LIST_SIZE];

    ISS_TRC (INIT_SHUT_TRC, "\nACL:Entry IssAclInstallL3Filter function \n");
    /* While adding these value check for the default values 
     * in NOT only call the function to configure
     */
    MEMSET (&IpAddr, 0x0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PortList, 0x0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1InPortList, 0, ISS_PORT_LIST_SIZE);
    MEMSET (au1OutPortList, 0, ISS_PORT_LIST_SIZE);

    if (pIssL3Filter == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nACL:pIssL3Filter is NULL \n");
        return (ISS_FAILURE);
    }

    /* if filter already exists, do nothing and enter the configuration mode */
    if (nmhGetIssAclL3FilterStatus (pIssL3Filter->i4IssL3FilterNo, &i4Status)
        == SNMP_FAILURE)
    {
        /* Create a filter */
        if (nmhTestv2IssAclL3FilterStatus (&u4ErrCode,
                                           pIssL3Filter->i4IssL3FilterNo,
                                           ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nACL:Failed to test  L3 Filter status for Filter ID %dwith error code as %d\n",
                          pIssL3Filter->i4IssL3FilterNo, u4ErrCode);
            return (ISS_FAILURE);
        }

        if (nmhSetIssAclL3FilterStatus (pIssL3Filter->i4IssL3FilterNo,
                                        ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nACL:Failed to set  L3 Filter status for Filter ID %d\n",
                          pIssL3Filter->i4IssL3FilterNo);
            return (ISS_FAILURE);
        }

    }
    else if (ISS_NOT_READY != i4Status)
    {
        /* Filter has previously been configured. This must be over-written 
         * First destroy the filter and create again */

        if (nmhSetIssAclL3FilterStatus (pIssL3Filter->i4IssL3FilterNo,
                                        ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nACL:Failed to set  L3 Filter status for Filter ID %d\n",
                          pIssL3Filter->i4IssL3FilterNo);
            return (ISS_FAILURE);
        }

        if (nmhSetIssAclL3FilterStatus (pIssL3Filter->i4IssL3FilterNo,
                                        ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nACL:Failed to set  L3 Filter status for Filter ID %d\n",
                          pIssL3Filter->i4IssL3FilterNo);
            return (ISS_FAILURE);
        }
    }

    /* OID:issAclL3FilterPriority */
    if (nmhTestv2IssAclL3FilterPriority (&u4ErrCode,
                                         pIssL3Filter->i4IssL3FilterNo,
                                         pIssL3Filter->i4IssL3FilterPriority)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test  L3 Filter Priority %d for Filter ID %dwith error code as %d\n",
                      pIssL3Filter->i4IssL3FilterPriority,
                      pIssL3Filter->i4IssL3FilterNo, u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL3FilterPriority (pIssL3Filter->i4IssL3FilterNo,
                                  pIssL3Filter->i4IssL3FilterPriority);

    /* OID:issAclL3FilterProtocol */
    if (nmhTestv2IssAclL3FilterProtocol (&u4ErrCode,
                                         pIssL3Filter->i4IssL3FilterNo,
                                         pIssL3Filter->IssL3FilterProtocol)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test  L3 Filter Priority %d for Filter ID %d\n",
                      pIssL3Filter->i4IssL3FilterPriority,
                      pIssL3Filter->i4IssL3FilterNo);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL3FilterProtocol (pIssL3Filter->i4IssL3FilterNo,
                                  pIssL3Filter->IssL3FilterProtocol);

    /* OID:issAclL3FilterMessageType */
    if ((pIssL3Filter->IssL3FilterProtocol == ISS_PROT_ICMP) ||
        (pIssL3Filter->IssL3FilterProtocol == ISS_PROT_ICMP6))
    {
        if (nmhTestv2IssAclL3FilterMessageType
                (&u4ErrCode,
                 pIssL3Filter->i4IssL3FilterNo,
                 pIssL3Filter->i4IssL3FilterMessageType) == SNMP_FAILURE)
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                    "\nACL:Failed to test  L3 Filter Message Type %d for Filter ID %dwith error code as %d\n",
                    pIssL3Filter->i4IssL3FilterMessageType,
                    pIssL3Filter->i4IssL3FilterNo, u4ErrCode);
            return (ISS_FAILURE);
        }

        nmhSetIssAclL3FilterMessageType (pIssL3Filter->i4IssL3FilterNo,
                pIssL3Filter->i4IssL3FilterMessageType);

        /* OID:issAclL3FilterMessageCode */
        if (nmhTestv2IssAclL3FilterMessageCode
                (&u4ErrCode,
                 pIssL3Filter->i4IssL3FilterNo,
                 pIssL3Filter->i4IssL3FilterMessageCode) == SNMP_FAILURE)
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                    "\nACL:Failed to test  L3 Filter Message code %d for Filter ID %dwith error code as %d\n",
                    pIssL3Filter->i4IssL3FilterMessageCode,
                    pIssL3Filter->i4IssL3FilterNo, u4ErrCode);
            return (ISS_FAILURE);
        }

        nmhSetIssAclL3FilterMessageCode (pIssL3Filter->i4IssL3FilterNo,
                pIssL3Filter->i4IssL3FilterMessageCode);
    }

    /* OID:issAclL3FilteAddrType */
    if (nmhTestv2IssAclL3FilteAddrType
        (&u4ErrCode,
         pIssL3Filter->i4IssL3FilterNo,
         pIssL3Filter->i4IssL3MultiFieldClfrAddrType) == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test  L3 Filter Addr Type %d for Filter ID %dwith error code as %d\n",
                      pIssL3Filter->i4IssL3MultiFieldClfrAddrType,
                      pIssL3Filter->i4IssL3FilterNo, u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL3FilteAddrType (pIssL3Filter->i4IssL3FilterNo,
                                 pIssL3Filter->i4IssL3MultiFieldClfrAddrType);

    if (pIssL3Filter->i4IssL3MultiFieldClfrAddrType == QOS_IPV4)
    {
        IpAddr.pu1_OctetList = MEM_MALLOC (QOS_IPV4_LEN, UINT1);
        if (IpAddr.pu1_OctetList == NULL)
        {
            ISS_TRC_ARG2 (BUFFER_TRC,
                          "Mem-alloc failed for IpAddr.pu1_OctetList"
                          "Function[%d] Line[%d]\n", __FUNCTION__, __LINE__);
            return (ISS_FAILURE);
        }
        MEMCPY (IpAddr.pu1_OctetList, &(pIssL3Filter->u4IssL3FilterDstIpAddr),
                QOS_IPV4_LEN);
        IpAddr.i4_Length = QOS_IPV4_LEN;
    }
    else if (pIssL3Filter->i4IssL3MultiFieldClfrAddrType == QOS_IPV6)
    {
        IpAddr.pu1_OctetList = MEM_MALLOC (sizeof (tIp6Addr), UINT1);
        if (IpAddr.pu1_OctetList == NULL)
        {
            ISS_TRC_ARG2 (BUFFER_TRC,
                          "Mem-alloc failed for IpAddr.pu1_OctetList"
                          "Function[%d] Line[%d]\n", __FUNCTION__, __LINE__);
            return (ISS_FAILURE);
        }
        Ip6AddrCopy ((tIp6Addr *) (VOID *) IpAddr.pu1_OctetList,
                     &(pIssL3Filter->ipv6DstIpAddress));
        IpAddr.i4_Length = IP6_ADDR_SIZE;
    }
    /* OID:issAclL3FilterDstIpAddr */
    if (pIssL3Filter->u4IssL3FilterDstIpAddr != ISS_ZERO_ENTRY)
    {
        if (nmhTestv2IssAclL3FilterDstIpAddr (&u4ErrCode,
                                              pIssL3Filter->i4IssL3FilterNo,
                                              &IpAddr) == SNMP_FAILURE)
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nACL:Failed to test L3 fiter Dst IpAddr %x for filter Id %d                            with  error code as %d\n",
                          *(IpAddr.pu1_OctetList),
                          pIssL3Filter->i4IssL3FilterNo, u4ErrCode);
            MEM_FREE ((UINT1 *) IpAddr.pu1_OctetList);
            return (ISS_FAILURE);
        }
    }

    nmhSetIssAclL3FilterDstIpAddr (pIssL3Filter->i4IssL3FilterNo, &IpAddr);

    if (pIssL3Filter->i4IssL3MultiFieldClfrAddrType == QOS_IPV4)
    {
        MEMCPY (IpAddr.pu1_OctetList, &(pIssL3Filter->u4IssL3FilterSrcIpAddr),
                QOS_IPV4_LEN);
        IpAddr.i4_Length = QOS_IPV4_LEN;
    }
    else if (pIssL3Filter->i4IssL3MultiFieldClfrAddrType == QOS_IPV6)
    {
        Ip6AddrCopy ((tIp6Addr *) (VOID *) IpAddr.pu1_OctetList,
                     &(pIssL3Filter->ipv6SrcIpAddress));
        IpAddr.i4_Length = IP6_ADDR_SIZE;
    }

    /* OID:issAclL3FilterSrcIpAddr */
    if (pIssL3Filter->u4IssL3FilterSrcIpAddr != ISS_ZERO_ENTRY)
    {
        if (nmhTestv2IssAclL3FilterSrcIpAddr (&u4ErrCode,
                                              pIssL3Filter->i4IssL3FilterNo,
                                              &IpAddr) == SNMP_FAILURE)
        {
            printf ("ACLL3:Failure of "
                    "nmhTestv2IssAclL3FilterSrcIpAddr in %s, %d\n",
                    __FUNCTION__, __LINE__);
            printf ("ACLL3: ErrCode[%d] FilterNo[%d]"
                    "IpAddr[%x]\n", u4ErrCode,
                    pIssL3Filter->i4IssL3FilterNo, *(IpAddr.pu1_OctetList));
            MEM_FREE ((UINT1 *) IpAddr.pu1_OctetList);
            return (ISS_FAILURE);
        }
    }

    nmhSetIssAclL3FilterSrcIpAddr (pIssL3Filter->i4IssL3FilterNo, &IpAddr);

    MEM_FREE ((UINT1 *) IpAddr.pu1_OctetList);

    /* OID:issAclL3FilterDstIpAddrPrefixLength */
    if (nmhTestv2IssAclL3FilterDstIpAddrPrefixLength
        (&u4ErrCode,
         pIssL3Filter->i4IssL3FilterNo,
         pIssL3Filter->u4IssL3FilterMultiFieldClfrDstPrefixLength)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test  L3 Filter Dst PrefixLength %d for Filter ID %dwith error code as %d\n",
                      pIssL3Filter->u4IssL3FilterMultiFieldClfrDstPrefixLength,
                      pIssL3Filter->i4IssL3FilterNo, u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL3FilterDstIpAddrPrefixLength
        (pIssL3Filter->i4IssL3FilterNo,
         pIssL3Filter->u4IssL3FilterMultiFieldClfrDstPrefixLength);

    /* OID:issAclL3FilterSrcIpAddrPrefixLength */
    if (nmhTestv2IssAclL3FilterSrcIpAddrPrefixLength
        (&u4ErrCode,
         pIssL3Filter->i4IssL3FilterNo,
         pIssL3Filter->u4IssL3FilterMultiFieldClfrSrcPrefixLength)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test  L3 Filter Src PrefixLength %d for Filter ID %dwith error code as %d\n",
                      pIssL3Filter->u4IssL3FilterMultiFieldClfrSrcPrefixLength,
                      pIssL3Filter->i4IssL3FilterNo, u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL3FilterSrcIpAddrPrefixLength
        (pIssL3Filter->i4IssL3FilterNo,
         pIssL3Filter->u4IssL3FilterMultiFieldClfrSrcPrefixLength);

    /* OID:issAclL3FilterMinDstProtPort */
    if ((pIssL3Filter->IssL3FilterProtocol == ISS_PROT_TCP) ||
            (pIssL3Filter->IssL3FilterProtocol == ISS_PROT_UDP))
    {
        if (nmhTestv2IssAclL3FilterMinDstProtPort
                (&u4ErrCode,
                 pIssL3Filter->i4IssL3FilterNo,
                 pIssL3Filter->u4IssL3FilterMinDstProtPort) == SNMP_FAILURE)
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                    "\nACL:Failed to test  L3 filter Minimum DstPort %d for Filter ID %dwith error code as %d\n",
                    pIssL3Filter->u4IssL3FilterMinDstProtPort,
                    pIssL3Filter->i4IssL3FilterNo, u4ErrCode);
            return (ISS_FAILURE);
        }

        nmhSetIssAclL3FilterMinDstProtPort
            (pIssL3Filter->i4IssL3FilterNo,
             pIssL3Filter->u4IssL3FilterMinDstProtPort);

        /* OID:issAclL3FilterMaxDstProtPort */
        if (nmhTestv2IssAclL3FilterMaxDstProtPort
                (&u4ErrCode,
                 pIssL3Filter->i4IssL3FilterNo,
                 pIssL3Filter->u4IssL3FilterMaxDstProtPort) == SNMP_FAILURE)
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                    "\nACL:Failed to test  L3 filter Maximum DstPort %d for Filter ID %dwith error code as %d\n",
                    pIssL3Filter->u4IssL3FilterMaxDstProtPort,
                    pIssL3Filter->i4IssL3FilterNo, u4ErrCode);
            return (ISS_FAILURE);
        }

        nmhSetIssAclL3FilterMaxDstProtPort (pIssL3Filter->i4IssL3FilterNo,
                pIssL3Filter->
                u4IssL3FilterMaxDstProtPort);

        /* OID:issAclL3FilterMinSrcProtPort */
        if (nmhTestv2IssAclL3FilterMinSrcProtPort
                (&u4ErrCode,
                 pIssL3Filter->i4IssL3FilterNo,
                 pIssL3Filter->u4IssL3FilterMinSrcProtPort) == SNMP_FAILURE)
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                    "\nACL:Failed to test  L3 filter Minimum Src Port %d for Filter ID %dwith error code as %d\n",
                    pIssL3Filter->u4IssL3FilterMinSrcProtPort,
                    pIssL3Filter->i4IssL3FilterNo, u4ErrCode);
            return (ISS_FAILURE);
        }

        nmhSetIssAclL3FilterMinSrcProtPort
            (pIssL3Filter->i4IssL3FilterNo,
             pIssL3Filter->u4IssL3FilterMinSrcProtPort);

        /* OID:issAclL3FilterMaxSrcProtPort */
        if (nmhTestv2IssAclL3FilterMaxSrcProtPort
                (&u4ErrCode,
                 pIssL3Filter->i4IssL3FilterNo,
                 pIssL3Filter->u4IssL3FilterMaxSrcProtPort) == SNMP_FAILURE)
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                    "\nACL:Failed to test  L3 filter Maximum SrcPort %d for Filter ID %dwith error code as %d\n",
                    pIssL3Filter->u4IssL3FilterMaxSrcProtPort,
                    pIssL3Filter->i4IssL3FilterNo, u4ErrCode);
            return (ISS_FAILURE);
        }

        nmhSetIssAclL3FilterMaxSrcProtPort
            (pIssL3Filter->i4IssL3FilterNo,
             pIssL3Filter->u4IssL3FilterMaxSrcProtPort);
    }

    PortList.pu1_OctetList = &au1InPortList[0];
    MEMSET (PortList.pu1_OctetList, 0x0, sizeof (tIssPortList));
    MEMCPY (PortList.pu1_OctetList, pIssL3Filter->IssL3FilterInPortList,
            ISS_PORT_LIST_SIZE);
    PortList.i4_Length = ISS_PORT_LIST_SIZE;

    /* OID:issAclL3FilterInPortList */
    if (nmhTestv2IssAclL3FilterInPortList (&u4ErrCode,
                                           pIssL3Filter->i4IssL3FilterNo,
                                           &PortList) == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test L3 Filter InPortList %x for Filter ID %d with  error code as %d\n",
                      *(PortList.pu1_OctetList), pIssL3Filter->i4IssL3FilterNo,
                      u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL3FilterInPortList (pIssL3Filter->i4IssL3FilterNo, &PortList);

    PortList.pu1_OctetList = &au1OutPortList[0];
    MEMSET (PortList.pu1_OctetList, 0x0, sizeof (tIssPortList));
    MEMCPY (PortList.pu1_OctetList, pIssL3Filter->IssL3FilterOutPortList,
            ISS_PORT_LIST_SIZE);
    PortList.i4_Length = ISS_PORT_LIST_SIZE;

    /* OID:issAclL3FilterOutPortList */
    if (nmhTestv2IssAclL3FilterOutPortList (&u4ErrCode,
                                            pIssL3Filter->i4IssL3FilterNo,
                                            &PortList) == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test L3 Filter out PortList %x for Filter ID %d with  error code as %d\n",
                      *(PortList.pu1_OctetList), pIssL3Filter->i4IssL3FilterNo,
                      u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL3FilterOutPortList (pIssL3Filter->i4IssL3FilterNo, &PortList);

    /* OID:issAclL3FilterAckBit */
    if (pIssL3Filter->IssL3FilterProtocol == ISS_PROT_TCP)
    {
        if (nmhTestv2IssAclL3FilterAckBit (&u4ErrCode,
                    pIssL3Filter->i4IssL3FilterNo,
                    pIssL3Filter->IssL3FilterAckBit)
                == SNMP_FAILURE)
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                    "\nACL:Failed to test  L3 filter Ack Bit %d for Filter ID %dwith error code as %d\n",
                    pIssL3Filter->IssL3FilterAckBit,
                    pIssL3Filter->i4IssL3FilterNo, u4ErrCode);
            return (ISS_FAILURE);
        }

        nmhSetIssAclL3FilterAckBit (pIssL3Filter->i4IssL3FilterNo,
                pIssL3Filter->IssL3FilterAckBit);

        /* OID:issAclL3FilterRstBit */
        if (nmhTestv2IssAclL3FilterRstBit (&u4ErrCode,
                    pIssL3Filter->i4IssL3FilterNo,
                    pIssL3Filter->IssL3FilterRstBit)
                == SNMP_FAILURE)
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                    "\nACL:Failed to test  L3 filter Rst Bit %d for Filter ID %dwith error code as %d\n",
                    pIssL3Filter->IssL3FilterRstBit,
                    pIssL3Filter->i4IssL3FilterNo, u4ErrCode);

            return (ISS_FAILURE);
        }
        nmhSetIssAclL3FilterRstBit (pIssL3Filter->i4IssL3FilterNo,
                pIssL3Filter->IssL3FilterRstBit);
    }

    /* OID:issAclL3FilterTos */
    if (pIssL3Filter->i4IssL3FilterNo > ACL_STD_MAX_VALUE)
    {
        if (pIssL3Filter->IssL3FilterTos != ISS_TOS_INVALID)
        {
            if (nmhTestv2IssAclL3FilterTos (&u4ErrCode,
                        pIssL3Filter->i4IssL3FilterNo,
                        pIssL3Filter->IssL3FilterTos)
                    == SNMP_FAILURE)
            {
                ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                        "\nACL:Failed to test  L3 filter Tos %d for Filter ID %dwith error code as %d\n",
                        pIssL3Filter->IssL3FilterTos,
                        pIssL3Filter->i4IssL3FilterNo, u4ErrCode);
                return (ISS_FAILURE);
            }
        }

        nmhSetIssAclL3FilterTos (pIssL3Filter->i4IssL3FilterNo,
                pIssL3Filter->IssL3FilterTos);

        /* OID:issAclL3FilterDscp */
        if (nmhTestv2IssAclL3FilterDscp (&u4ErrCode,
                    pIssL3Filter->i4IssL3FilterNo,
                    pIssL3Filter->i4IssL3FilterDscp)
                == SNMP_FAILURE)
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                    "\nACL:Failed to test  L3 filter Dscp %d for Filter ID %dwith error code as %d\n",
                    pIssL3Filter->i4IssL3FilterDscp,
                    pIssL3Filter->i4IssL3FilterNo, u4ErrCode);
            return (ISS_FAILURE);
        }

        nmhSetIssAclL3FilterDscp (pIssL3Filter->i4IssL3FilterNo,
                pIssL3Filter->i4IssL3FilterDscp);
    }

    /* OID:issAclL3FilterDirection */
    if (nmhTestv2IssAclL3FilterDirection (&u4ErrCode,
                                          pIssL3Filter->i4IssL3FilterNo,
                                          pIssL3Filter->IssL3FilterDirection)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test  L3 filter Direction %d for Filter ID %dwith error code as %d\n",
                      pIssL3Filter->IssL3FilterDirection,
                      pIssL3Filter->i4IssL3FilterNo, u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL3FilterDirection (pIssL3Filter->i4IssL3FilterNo,
                                   pIssL3Filter->IssL3FilterDirection);

    /* OID:issAclL3FilterAction */
    if (nmhTestv2IssAclL3FilterAction (&u4ErrCode,
                                       pIssL3Filter->i4IssL3FilterNo,
                                       pIssL3Filter->IssL3FilterAction)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test  L3 filter Action %d for Filter ID %dwith error code as %d\n",
                      pIssL3Filter->IssL3FilterAction,
                      pIssL3Filter->i4IssL3FilterNo, u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL3FilterAction (pIssL3Filter->i4IssL3FilterNo,
                                pIssL3Filter->IssL3FilterAction);

    /*OID: issAclL3FilterMatchCount -- Read-only */

    /* OID:issAclL3FilterFlowId */
    if (pIssL3Filter->i4IssL3MultiFieldClfrAddrType == QOS_IPV6)
    {
        if (nmhTestv2IssAclL3FilterFlowId
            (&u4ErrCode,
             pIssL3Filter->i4IssL3FilterNo,
             pIssL3Filter->u4IssL3MultiFieldClfrFlowId) == SNMP_FAILURE)
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nACL:Failed to test  L3 filter Flow Id %d for Filter ID %dwith error code as %d\n",
                          pIssL3Filter->u4IssL3MultiFieldClfrFlowId,
                          pIssL3Filter->i4IssL3FilterNo, u4ErrCode);
            return (ISS_FAILURE);
        }

        nmhSetIssAclL3FilterFlowId (pIssL3Filter->i4IssL3FilterNo,
                                    pIssL3Filter->u4IssL3MultiFieldClfrFlowId);
    }
    /* IssAclL3FilterCreationMode is Read-Only */
    pIssMetroL3Filter = IssExtGetL3FilterEntry (pIssL3Filter->i4IssL3FilterNo);

    /* OID:issAclL3FilterCreationMode */
    pIssMetroL3Filter->u1IssL3FilterCreationMode
        = pIssL3Filter->u1IssL3FilterCreationMode;
#ifdef ISS_METRO_WANTED
    /* OID:issMetroL3FilterSVlanId from fsissmet.mib */
    if (nmhTestv2IssMetroL3FilterSVlanId (&u4ErrCode,
                                          pIssL3Filter->i4IssL3FilterNo,
                                          pIssL3Filter->u4SVlanId) ==
        SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test  L3 filter service Vlan Id %d for Filter ID %dwith error code as %d\n",
                      pIssL3Filter->u4SVlanId, pIssL3Filter->i4IssL3FilterNo,
                      u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssMetroL3FilterSVlanId (pIssL3Filter->i4IssL3FilterNo,
                                   pIssL3Filter->u4SVlanId);

    /* OID:issMetroL3FilterSVlanPriority from fsissmet.mib */
    if (nmhTestv2IssMetroL3FilterSVlanPriority (&u4ErrCode,
                                                pIssL3Filter->i4IssL3FilterNo,
                                                pIssL3Filter->
                                                i1SVlanPriority) ==
        SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test  L3 filter service Vlan Priority %d for Filter ID %dwith error code as %d\n",
                      pIssL3Filter->i1SVlanPriority,
                      pIssL3Filter->i4IssL3FilterNo, u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssMetroL3FilterSVlanPriority (pIssL3Filter->i4IssL3FilterNo,
                                         pIssL3Filter->i1SVlanPriority);

    /* OID:issMetroL3FilterCVlanId from fsissmet.mib */
    if (nmhTestv2IssMetroL3FilterCVlanId (&u4ErrCode,
                                          pIssL3Filter->i4IssL3FilterNo,
                                          pIssL3Filter->u4CVlanId) ==
        SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test  L3 filter customer Vlan Id %d for Filter ID %dwith error code as %d\n",
                      pIssL3Filter->u4CVlanId, pIssL3Filter->i4IssL3FilterNo,
                      u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssMetroL3FilterCVlanId (pIssL3Filter->i4IssL3FilterNo,
                                   pIssL3Filter->u4CVlanId);

    /* OID:issMetroL3FilterCVlanPriority from fsissmet.mib */
    if (nmhTestv2IssMetroL3FilterCVlanPriority (&u4ErrCode,
                                                pIssL3Filter->i4IssL3FilterNo,
                                                pIssL3Filter->
                                                i1CVlanPriority) ==
        SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test  L3 filter customer Vlan Priority %d for Filter ID %dwith error code as %d\n",
                      pIssL3Filter->i1CVlanPriority,
                      pIssL3Filter->i4IssL3FilterNo, u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssMetroL3FilterCVlanPriority (pIssL3Filter->i4IssL3FilterNo,
                                         pIssL3Filter->i1CVlanPriority);

    /* OID:issMetroL3FilterPacketTagType from fsissmet.mib */
    if (nmhTestv2IssMetroL3FilterPacketTagType (&u4ErrCode,
                                                pIssL3Filter->i4IssL3FilterNo,
                                                pIssL3Filter->
                                                u1IssL3FilterTagType) ==
        SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test  L3 filter Packet TagType %d for Filter ID %dwith error code as %d\n",
                      pIssL3Filter->u1IssL3FilterTagType,
                      pIssL3Filter->i4IssL3FilterNo, u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssMetroL3FilterPacketTagType (pIssL3Filter->i4IssL3FilterNo,
                                         pIssL3Filter->u1IssL3FilterTagType);
#endif /* ISS_METRO_WANTED */
    /* OID:issAclL3FilterStatus */
    nmhSetIssAclL3FilterStatus (pIssL3Filter->i4IssL3FilterNo, ACTIVE);

    ISS_TRC (INIT_SHUT_TRC, "\nACLL3:Exit IssAclInstallL3Filter function \n");

    return (ISS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      :  IssAclUnInstallL3Filter                             */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                            to delete the added L3 ACL rule.               */
/*                                                                           */
/* Input(s)           : pIssL3Filter - Pointer to ACL L3 Filter entry        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
IssAclUnInstallL3Filter (tIssL3FilterEntry * pIssL3Filter)
{
    INT4                i4Status = ISS_ZERO_ENTRY;
    UINT4               u4ErrCode = ISS_ZERO_ENTRY;

    ISS_TRC (INIT_SHUT_TRC, "\nACL:Entry IssAclUnInstallL3Filter function \n");
    if (pIssL3Filter == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nACL:pIssL3Filter is NULL \n");
        return (ISS_FAILURE);
    }

    if (nmhGetIssAclL3FilterStatus (pIssL3Filter->i4IssL3FilterNo, &i4Status)
        == SNMP_FAILURE)
    {
        /* Filter does not exist */
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nACL:Failed to Retrieve L3 filter status%d for filter ID %d\n",
                      i4Status, pIssL3Filter->i4IssL3FilterNo);
        return (ISS_SUCCESS);
    }
    else
    {
        if (nmhTestv2IssAclL3FilterStatus (&u4ErrCode,
                                           pIssL3Filter->i4IssL3FilterNo,
                                           ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nACL:Failed to test L3 filter status for filter ID %d                                         with  error code as %d\n",
                          pIssL3Filter->i4IssL3FilterNo, u4ErrCode);
            return (ISS_FAILURE);
        }

        if (nmhSetIssAclL3FilterStatus (pIssL3Filter->i4IssL3FilterNo,
                                        ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nACL:Failed to set L3 filter status for filter ID %d\n",
                          pIssL3Filter->i4IssL3FilterNo);
            return (ISS_FAILURE);
        }
    }
    ISS_TRC (INIT_SHUT_TRC, "\nACL:Exit IssAclUnInstallL3Filter function \n");
    return (ISS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      :  IssACLInstallFilter                                 */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                      to install L2 and L3 ACL rules.                      */
/*                                                                           */
/* Input(s)           :  u4FilterType -- Acl filter type (L2 or L3)          */
/*                       pIssL2Filter - Pointer to ACL L2 Filter entry       */
/*                       pIssL3Filter - Pointer to ACL L3 Filter entry       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
IssACLInstallFilter (UINT4 u4FilterType, tIssL2FilterEntry * pIssL2Filter,
                     tIssL3FilterEntry * pIssL3Filter)
{
    ISS_TRC (INIT_SHUT_TRC, "\nACL:Entry IssACLInstallFilter function \n");

    if (u4FilterType == QOS_ACL_L2_CONFIG)
    {
        if (IssAclInstallL2Filter (pIssL2Filter) == ISS_FAILURE)
        {
            ISS_TRC (ALL_FAILURE_TRC, "\nACL:Failed to Install L2 Filter \n");
            return (ISS_FAILURE);
        }
        ISS_TRC (INIT_SHUT_TRC, "\nACL:Exit IssACLInstallFilter function\n");
        return (ISS_SUCCESS);
    }
    else if (u4FilterType == QOS_ACL_L3_CONFIG)
    {
        if (IssAclInstallL3Filter (pIssL3Filter) == ISS_FAILURE)
        {
            ISS_TRC (ALL_FAILURE_TRC, "\nACL:Failed to Install L3 Filter\n");
            return (ISS_FAILURE);
        }

        ISS_TRC (INIT_SHUT_TRC, "\nACL:Exit IssACLInstallFilter function\n");
        return (ISS_SUCCESS);
    }

    /* No filters types to configure */
    ISS_TRC (INIT_SHUT_TRC, "\nACL:Exit IssACLInstallFilter function\n");
    return (ISS_FAILURE);
}

/*****************************************************************************/
/* Function Name      :  IssAclQosProcessL2Proto                             */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                      to install L2 ACL rules and QoS configurations.      */
/* Input(s)           :                                                      */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
IssAclQosProcessL2Proto (VOID)
{
    UINT4               u4FilterType = QOS_ACL_L2_CONFIG;
    UINT4               u4L2Protocols = ISS_ZERO_ENTRY;
    tAclQosMapInfo      AclQosL2MapInfo;

    ISS_TRC (INIT_SHUT_TRC, "\nACL:Entry IssAclQosProcessL2Proto  function \n");
    MEMSET (&AclQosL2MapInfo, 0x0, sizeof (tAclQosMapInfo));

    for (u4L2Protocols = ACL_MIN_L2_SYSTEM_PROTOCOLS;
         u4L2Protocols <= ACL_MAX_L2_SYSTEM_PROTOCOLS; u4L2Protocols++)
    {
        /* L2 Filter Configuration */
        if (IssACLInstallFilter
            (u4FilterType,
             &gaQoSAcLInfoL2Protocols[u4L2Protocols].L2FilterInfo, NULL)
            == ISS_FAILURE)
        {
            ISS_TRC (ALL_FAILURE_TRC, "\nACL:Failed to Install L2 Filter\n");
            return (ISS_FAILURE);
        }

        AclQosL2MapInfo.QosIds.u4ClassMapId
            = gaQoSAcLInfoL2Protocols[u4L2Protocols].QosIds.u4ClassMapId;
        AclQosL2MapInfo.QosIds.u4ClassId
            = gaQoSAcLInfoL2Protocols[u4L2Protocols].QosIds.u4ClassId;
        AclQosL2MapInfo.QosIds.u4MeterId
            = gaQoSAcLInfoL2Protocols[u4L2Protocols].QosIds.u4MeterId;
        AclQosL2MapInfo.QosIds.u4PolicyMapId
            = gaQoSAcLInfoL2Protocols[u4L2Protocols].QosIds.u4PolicyMapId;
        AclQosL2MapInfo.u4FilterId
            =
            gaQoSAcLInfoL2Protocols[u4L2Protocols].L2FilterInfo.i4IssL2FilterNo;
        AclQosL2MapInfo.u4ProtocolRate =
            gaQoSAcLInfoL2Protocols[u4L2Protocols].u4ProtocolRate;
        AclQosL2MapInfo.u1CpuQueueNo =
            gaQoSAcLInfoL2Protocols[u4L2Protocols].u1CpuQueueNo;
        /* QoS parameter configurations */
        ISS_UNLOCK ();
        if (QosApiAddMfcRateLimiter (u4FilterType, &AclQosL2MapInfo) ==
            QOS_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "ACL:Failure of QosAddMfcFilter in %s, %d\n",
                          __FUNCTION__, __LINE__);
            ISS_LOCK ();
            return (ISS_FAILURE);
        }
        ISS_LOCK ();
    }

    ISS_TRC (INIT_SHUT_TRC, "\nACL:Exit IssAclQosProcessL2Proto function \n");

    return (ISS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      :  IssAclQosProcessL3Proto                             */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                            to install L3 ACL rules and QoS configurations.*/
/*                                                                           */
/* Input(s)           :                                                      */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
IssAclQosProcessL3Proto (VOID)
{
    UINT4               u4FilterType = QOS_ACL_L3_CONFIG;
    UINT4               u4L3Protocols = ISS_ZERO_ENTRY;
    tAclQosMapInfo      AclQosL3MapInfo;

    MEMSET (&AclQosL3MapInfo, 0x0, sizeof (tAclQosMapInfo));

    ISS_TRC (INIT_SHUT_TRC, "\nACL:Entry IssAclQosProcessL3Proto function \n");

    for (u4L3Protocols = ACL_MIN_L3_SYSTEM_PROTOCOLS;
         u4L3Protocols <= ACL_MAX_L3_SYSTEM_PROTOCOLS; u4L3Protocols++)
    {
        /* L3 Filter Configuration */
        if (IssACLInstallFilter
            (u4FilterType, NULL,
             &(gaQoSAcLInfoL3Protocols[u4L3Protocols].L3FilterInfo))
            == ISS_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "ACL:Failure of IssACLInstallFilter in %s, %d\n",
                          __FUNCTION__, __LINE__);
            return (ISS_FAILURE);
        }

        AclQosL3MapInfo.QosIds.u4ClassMapId
            = gaQoSAcLInfoL3Protocols[u4L3Protocols].QosIds.u4ClassMapId;
        AclQosL3MapInfo.QosIds.u4ClassId
            = gaQoSAcLInfoL3Protocols[u4L3Protocols].QosIds.u4ClassId;
        AclQosL3MapInfo.QosIds.u4MeterId
            = gaQoSAcLInfoL3Protocols[u4L3Protocols].QosIds.u4MeterId;
        AclQosL3MapInfo.QosIds.u4PolicyMapId
            = gaQoSAcLInfoL3Protocols[u4L3Protocols].QosIds.u4PolicyMapId;
        AclQosL3MapInfo.u4FilterId
            =
            gaQoSAcLInfoL3Protocols[u4L3Protocols].L3FilterInfo.i4IssL3FilterNo;
        AclQosL3MapInfo.u4ProtocolRate =
            gaQoSAcLInfoL3Protocols[u4L3Protocols].u4ProtocolRate;
        AclQosL3MapInfo.u1CpuQueueNo =
            gaQoSAcLInfoL3Protocols[u4L3Protocols].u1CpuQueueNo;

        /* QoS parameter configurations */
        ISS_UNLOCK ();
        if (QosApiAddMfcRateLimiter (u4FilterType, &AclQosL3MapInfo) ==
            QOS_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "ACL:Failure of QosAddMfcFilter in %s, %d\n",
                          __FUNCTION__, __LINE__);
            ISS_LOCK ();
            return (ISS_FAILURE);
        }
        ISS_LOCK ();
    }

    ISS_TRC (INIT_SHUT_TRC, "\nACL:Exit IssAclQosProcessL3Proto function \n");
    return (ISS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      :  IssAclQosProcessL2L3Proto                           */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                      to install L2, L3 and DoS attack control ACL rules   */
/*                      and QoS configurations.                              */
/*                                                                           */
/* Input(s)           :                                                      */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
IssAclQosProcessL2L3Proto (VOID)
{
    INT4                i4RetVal = ISS_SUCCESS;

    ISS_TRC (INIT_SHUT_TRC, "\nACL:Entry IssAclQosProcessL2L3Proto function\n");

    if (gu4TrafficSeprtnControl == ACL_TRAFFICSEPRTN_CTRL_USER_DEFINED)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nACL:Acl traffic sepration macro value assigned to gu4TrafficSeprtnControl \n");
        return (ISS_FAILURE);
    }

    if (IssAclQosProcessL2Proto () == ISS_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nACL:failed to install L2 ACL rules and QoS configurations \n");
        i4RetVal = ISS_FAILURE;
    }

    if (IssAclQosProcessL3Proto () == ISS_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nACL:failed to install L3 ACL rules and QoS configurations \n");
        i4RetVal = ISS_FAILURE;
    }

    /* TO be called after implementation of protection against DoS attack to CP 
     *  if (QoSProcessDoSAttackControl () == ISS_FAILURE) 
     */
    ISS_TRC (INIT_SHUT_TRC, "\nACL:Exit IssAclQosProcessL2L3Proto function\n");

    return (i4RetVal);
}

/*****************************************************************************/
/* Function Name      :  IssAclQosDeleteL2L3Filter                           */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                      to uninstall L2, L3  ACL rules and                   */
/*                      QoS configuartions.                                  */
/*                                                                           */
/* Input(s)           :                                                      */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
IssAclQosDeleteL2L3Filter (VOID)
{
    UINT4               u4FilterType = ISS_ZERO_ENTRY;
    UINT4               u4L2Protocols = ISS_ZERO_ENTRY;
    INT4                i4IssAclFilterCreationMode = ISS_ZERO_ENTRY;
    tAclQosMapInfo      AclQosL2MapInfo;
    tAclQosMapInfo      AclQosL3MapInfo;

    MEMSET (&AclQosL2MapInfo, 0x0, sizeof (tAclQosMapInfo));
    MEMSET (&AclQosL3MapInfo, 0x0, sizeof (tAclQosMapInfo));

    ISS_TRC (INIT_SHUT_TRC,
             "\nACL:Entry IssAclQosDeleteL2L3Filter function \n");

    for (u4L2Protocols = ACL_MIN_L2_SYSTEM_PROTOCOLS,
         u4FilterType = QOS_ACL_L2_CONFIG;
         u4L2Protocols <= ACL_MAX_L2_SYSTEM_PROTOCOLS; u4L2Protocols++)
    {
        AclQosL2MapInfo.QosIds.u4ClassMapId
            = gaQoSAcLInfoL2Protocols[u4L2Protocols].QosIds.u4ClassMapId;
        AclQosL2MapInfo.QosIds.u4ClassId
            = gaQoSAcLInfoL2Protocols[u4L2Protocols].QosIds.u4ClassId;
        AclQosL2MapInfo.QosIds.u4MeterId
            = gaQoSAcLInfoL2Protocols[u4L2Protocols].QosIds.u4MeterId;
        AclQosL2MapInfo.QosIds.u4PolicyMapId
            = gaQoSAcLInfoL2Protocols[u4L2Protocols].QosIds.u4PolicyMapId;
        AclQosL2MapInfo.u4FilterId
            =
            gaQoSAcLInfoL2Protocols[u4L2Protocols].L2FilterInfo.i4IssL2FilterNo;
        AclQosL2MapInfo.u4ProtocolRate =
            gaQoSAcLInfoL2Protocols[u4L2Protocols].u4ProtocolRate;
        AclQosL2MapInfo.u1CpuQueueNo =
            gaQoSAcLInfoL2Protocols[u4L2Protocols].u1CpuQueueNo;

        nmhGetIssAclL2FilterCreationMode (AclQosL2MapInfo.u4FilterId,
                                          &i4IssAclFilterCreationMode);

        if (i4IssAclFilterCreationMode == ISS_ACL_CREATION_INTERNAL)
        {
            /* QoS parameter configurations */
            ISS_UNLOCK ();
            if (QosApiDeleteMfcRateLimiter (u4FilterType, &AclQosL2MapInfo)
                == QOS_FAILURE)
            {
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "ACL:Failure of QosDeleteMfcFilter in %s, %d\n",
                              __FUNCTION__, __LINE__);
                ISS_LOCK ();
                return (ISS_FAILURE);
            }
            ISS_LOCK ();
            /* L2 Filter deletion Configuration */
            if (IssAclUninstallFilter
                (u4FilterType,
                 &(gaQoSAcLInfoL2Protocols[u4L2Protocols].L2FilterInfo),
                 NULL) == ISS_FAILURE)
            {
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "ACL:Failure of IssAclUninstallFilter in %s, %d\n",
                              __FUNCTION__, __LINE__);
                return (ISS_FAILURE);
            }
        }
    }

    for (u4L2Protocols = ACL_MIN_L3_SYSTEM_PROTOCOLS,
         u4FilterType = QOS_ACL_L3_CONFIG;
         u4L2Protocols <= ACL_MAX_L3_SYSTEM_PROTOCOLS; u4L2Protocols++)
    {

        AclQosL3MapInfo.QosIds.u4ClassMapId
            = gaQoSAcLInfoL3Protocols[u4L2Protocols].QosIds.u4ClassMapId;
        AclQosL3MapInfo.QosIds.u4ClassId
            = gaQoSAcLInfoL3Protocols[u4L2Protocols].QosIds.u4ClassId;
        AclQosL3MapInfo.QosIds.u4MeterId
            = gaQoSAcLInfoL3Protocols[u4L2Protocols].QosIds.u4MeterId;
        AclQosL3MapInfo.QosIds.u4PolicyMapId
            = gaQoSAcLInfoL3Protocols[u4L2Protocols].QosIds.u4PolicyMapId;
        AclQosL3MapInfo.u4FilterId
            =
            gaQoSAcLInfoL3Protocols[u4L2Protocols].L3FilterInfo.i4IssL3FilterNo;
        AclQosL3MapInfo.u4ProtocolRate =
            gaQoSAcLInfoL3Protocols[u4L2Protocols].u4ProtocolRate;
        AclQosL3MapInfo.u1CpuQueueNo =
            gaQoSAcLInfoL3Protocols[u4L2Protocols].u1CpuQueueNo;

        nmhGetIssAclL3FilterCreationMode (AclQosL3MapInfo.u4FilterId,
                                          &i4IssAclFilterCreationMode);

        if (i4IssAclFilterCreationMode == ISS_ACL_CREATION_INTERNAL)
        {
            /* QoS parameter configurations */
            ISS_UNLOCK ();
            if (QosApiDeleteMfcRateLimiter (u4FilterType, &AclQosL3MapInfo)
                == QOS_FAILURE)
            {
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "ACL:Failure of QosDeleteMfcFilter in %s, %d\n",
                              __FUNCTION__, __LINE__);
                ISS_LOCK ();
                return (ISS_FAILURE);
            }
            ISS_LOCK ();

            /* L3 Filter Configuration */
            if (IssAclUninstallFilter
                (u4FilterType, NULL,
                 &(gaQoSAcLInfoL3Protocols[u4L2Protocols].L3FilterInfo))
                == ISS_FAILURE)
            {
                ISS_TRC (ALL_FAILURE_TRC,
                         "\nACL:Failed to Un Install L3 Filter\n");
                return (ISS_FAILURE);
            }
        }
    }

    ISS_TRC (INIT_SHUT_TRC, "\nACL:Exit IssAclQosDeleteL2L3Filter function \n");

    return (ISS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      :  IssAclUninstallFilter                               */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                            to uninstall L2, L3  ACL rules.                */
/*                                                                           */
/* Input(s)           :                                                      */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
IssAclUninstallFilter (UINT4 u4FilterType, tIssL2FilterEntry * pIssL2Filter,
                       tIssL3FilterEntry * pIssL3Filter)
{
    ISS_TRC (INIT_SHUT_TRC, "\nACL:Entry IssAclUninstallFilter function \n");
    /* Set RowStatus destroy for issAclL2FilterTable or 
     * issAclL3FilterTable based on the filter type.
     */

    if ((u4FilterType == QOS_ACL_L2_CONFIG) && (pIssL2Filter != NULL))
    {
        if (IssAclUnInstallL2Filter (pIssL2Filter) == ISS_FAILURE)
        {
            ISS_TRC (ALL_FAILURE_TRC, "\nACL:Failed to UnInstall L2 Filter\n");
            return (ISS_FAILURE);
        }
        IssAclUnInstallL2Filter (pIssL2Filter);
        ISS_TRC (INIT_SHUT_TRC, "\nACL:Exit IssAclUninstallFilter function \n");
        return (ISS_SUCCESS);
    }
    else if ((u4FilterType == QOS_ACL_L3_CONFIG) && (pIssL3Filter != NULL))
    {
        if (IssAclUnInstallL3Filter (pIssL3Filter) == ISS_FAILURE)
        {
            ISS_TRC (ALL_FAILURE_TRC, "\nACL:Failed to UnInstall L2 Filter\n");
            return (ISS_FAILURE);
        }
        IssAclUnInstallL3Filter (pIssL3Filter);
        ISS_TRC (INIT_SHUT_TRC, "\nACL:Exit IssAclUninstallFilter function \n");
        return (ISS_SUCCESS);
    }

    /* No filters types to configure */
    ISS_TRC (INIT_SHUT_TRC, "\nACL:Fail IssAclUninstallFilter function \n");
    return (ISS_FAILURE);
}

/*****************************************************************************/
/* Function Name      : IssAclInstallUserDefFilter                           */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                      to install new user defined ACL rule.                */
/*                                                                           */
/* Input(s)           : pIssUserDefFilter - Pointer to user defined          */
/*                                          ACL entry.                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
IssAclInstallUserDefFilter (tIssUserDefinedFilterTable * pIssUserDefFilter)
{
    tIssUDBFilterEntry *pAccessFilterEntry = NULL;
    INT4                i4Status = ISS_ZERO_ENTRY;
    UINT4               u4ErrCode = ISS_ZERO_ENTRY;
    UINT4               u4UserDefFilterId = ISS_ZERO_ENTRY;
    tSNMP_OCTET_STRING_TYPE OffsetValue;
    tSNMP_OCTET_STRING_TYPE OffsetMask;
    tSNMP_OCTET_STRING_TYPE InPortList;
    UINT1               au1OffsetValue[ISS_UDB_MAX_OFFSET] = { 0 };
    UINT1               au1OffsetMask[ISS_UDB_MAX_OFFSET] = { 0 };
    UINT1               au1InPortList[ISS_PORT_LIST_SIZE] = { 0 };

    ISS_TRC (INIT_SHUT_TRC,
             "\nACL:Entry IssAclInstallUserDefFilter function \n");
    if (pIssUserDefFilter == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "ACL:UserDefFilter---pIssUserDefFilter is NULL \n");
        return (ISS_FAILURE);
    }

    pAccessFilterEntry = pIssUserDefFilter->pAccessFilterEntry;

    u4UserDefFilterId = pAccessFilterEntry->u4UDBFilterId;

    /* if filter already exists, do nothing and enter the configuration mode */
    if (nmhGetIssAclUserDefinedFilterStatus (u4UserDefFilterId, &i4Status)
        == SNMP_FAILURE)
    {
        /* Create a filter */
        if (nmhTestv2IssAclUserDefinedFilterStatus
            (&u4ErrCode, u4UserDefFilterId, ISS_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nACL:Failed to test User Defined Filter Status for filter ID %d                       with  error code as %d\n",
                          u4UserDefFilterId, u4ErrCode);
            return (ISS_FAILURE);
        }

        if (nmhSetIssAclUserDefinedFilterStatus (u4UserDefFilterId,
                                                 ISS_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nACL:Failed to set User Defined Filter Status for filter ID %d\n",
                          u4UserDefFilterId);
            return (ISS_FAILURE);
        }
    }
    else if (i4Status != ISS_NOT_READY)
    {
        /* Filter has previously been configured, it must be overwriten */
        if (nmhSetIssAclUserDefinedFilterStatus (u4UserDefFilterId,
                                                 ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nACL:Failed to set User Defined Filter Status for filter ID %d\n",
                          u4UserDefFilterId);
            return (ISS_FAILURE);
        }
        if (nmhSetIssAclUserDefinedFilterStatus (u4UserDefFilterId,
                                                 ISS_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nACL:Failed to set User Defined Filter Status for filter ID %d\n",
                          u4UserDefFilterId);
            return (ISS_FAILURE);
        }
    }

    /* OID: issAclUserDefinedFilterPktType */
    if (nmhTestv2IssAclUserDefinedFilterPktType
        (&u4ErrCode, u4UserDefFilterId,
         pAccessFilterEntry->u1AccessFilterPktType) == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test User Defined Filter PktType%d for filterID %d                             with  error code as %d\n",
                      pAccessFilterEntry->u1AccessFilterPktType,
                      u4UserDefFilterId, u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssAclUserDefinedFilterPktType
        (u4UserDefFilterId, pAccessFilterEntry->u1AccessFilterPktType);

    /* OID: issAclUserDefinedFilterOffSetBase */
    if (nmhTestv2IssAclUserDefinedFilterOffSetBase
        (&u4ErrCode, u4UserDefFilterId,
         pAccessFilterEntry->u2AccessFilterOffset) == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test User Defined Filter OffSet-Base %d for Filter ID %d with  error code as %d\n",
                      pAccessFilterEntry->u2AccessFilterOffset,
                      u4UserDefFilterId, u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssAclUserDefinedFilterOffSetBase
        (u4UserDefFilterId, pAccessFilterEntry->u2AccessFilterOffset);

    /* OID : issAclUserDefinedFilterOffSetValue */
    OffsetValue.pu1_OctetList = &au1OffsetValue[0];

    MEMSET (OffsetValue.pu1_OctetList, 0x0, ISS_UDB_MAX_OFFSET);

    MEMCPY (OffsetValue.pu1_OctetList,
            pAccessFilterEntry->au1AccessFilterOffsetValue, ISS_UDB_MAX_OFFSET);
    OffsetValue.i4_Length = ISS_UDB_MAX_OFFSET;

    if (nmhTestv2IssAclUserDefinedFilterOffSetValue
        (&u4ErrCode, u4UserDefFilterId, &OffsetValue) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test User Defined Filter OffSet-value for Filter ID %d with  e     rror code as %d\n",
                      u4UserDefFilterId, u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssAclUserDefinedFilterOffSetValue (u4UserDefFilterId, &OffsetValue);

    /* OID : issAclUserDefinedFilterOffSetMask */
    OffsetMask.pu1_OctetList = &au1OffsetMask[0];

    MEMSET (OffsetValue.pu1_OctetList, 0x0, ISS_UDB_MAX_OFFSET);

    MEMCPY (OffsetMask.pu1_OctetList,
            pAccessFilterEntry->au1AccessFilterOffsetMask, ISS_UDB_MAX_OFFSET);

    OffsetMask.i4_Length = ISS_UDB_MAX_OFFSET;

    if (nmhTestv2IssAclUserDefinedFilterOffSetMask
        (&u4ErrCode, u4UserDefFilterId, &OffsetMask) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test  UserDefinedFilter OffSetMask for filter Id %d with error code as %d\n",
                      u4UserDefFilterId, u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssAclUserDefinedFilterOffSetMask (u4UserDefFilterId, &OffsetMask);

    /* OID : issAclUserDefinedFilterPriority   */

    if (nmhTestv2IssAclUserDefinedFilterPriority
        (&u4ErrCode, u4UserDefFilterId,
         pAccessFilterEntry->u1AccessFilterPriority) == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test UserDefinedFilter Priority %d for Filter ID %d                              with  error code as %d\n",
                      pAccessFilterEntry->u1AccessFilterPriority,
                      u4UserDefFilterId, u4ErrCode);
        return (ISS_FAILURE);
    }
    nmhSetIssAclUserDefinedFilterPriority
        (u4UserDefFilterId, pAccessFilterEntry->u1AccessFilterPriority);

    /* OID : issAclUserDefinedFilterAction */
    if (nmhTestv2IssAclUserDefinedFilterAction
        (&u4ErrCode, u4UserDefFilterId,
         pAccessFilterEntry->IssAccessFilterAction) == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test User Defined Filter Action  %d for Filter ID %d                     with  error code as %d\n",
                      pAccessFilterEntry->IssAccessFilterAction,
                      u4UserDefFilterId, u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssAclUserDefinedFilterAction
        (u4UserDefFilterId, pAccessFilterEntry->IssAccessFilterAction);

    /* OID :  issAclUserDefinedFilterInPortList */
    InPortList.pu1_OctetList = &au1InPortList[0];
    MEMSET (InPortList.pu1_OctetList, 0x0, sizeof (tIssPortList));
    MEMCPY (InPortList.pu1_OctetList,
            pAccessFilterEntry->IssUdbFilterInPortList, ISS_PORT_LIST_SIZE);
    InPortList.i4_Length = ISS_PORT_LIST_SIZE;

    if (nmhTestv2IssAclUserDefinedFilterInPortList
        (&u4ErrCode, u4UserDefFilterId, &InPortList) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACL-UDB:Failure of "
                      "nmhSetIssAclUserDefinedFilterInPortList in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACL-UDB: ErrCode[%d] FilterNo[%d]\n",
                      u4ErrCode, u4UserDefFilterId);
        return (ISS_FAILURE);
    }

    nmhSetIssAclUserDefinedFilterInPortList (u4UserDefFilterId, &InPortList);

    /* OID :  issAclUserDefinedFilterIdOneType  */
    if (nmhTestv2IssAclUserDefinedFilterIdOneType
        (&u4ErrCode, u4UserDefFilterId,
         pIssUserDefFilter->u1AclOneType) == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test User Defined Filter one type  %d for Filter ID %d                     with  error code as %d\n",
                      pIssUserDefFilter->u4BaseAclOne, u4UserDefFilterId,
                      u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssAclUserDefinedFilterIdOneType
        (u4UserDefFilterId, pIssUserDefFilter->u1AclOneType);

    /* OID :  issAclUserDefinedFilterIdOne      */
    if (nmhTestv2IssAclUserDefinedFilterIdOne
        (&u4ErrCode, u4UserDefFilterId,
         pIssUserDefFilter->u1AclOneType) == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test User Defined Filter one  %d for Filter ID %d                     with  error code as %d\n",
                      pIssUserDefFilter->u4BaseAclOne, u4UserDefFilterId,
                      u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssAclUserDefinedFilterIdOne
        (u4UserDefFilterId, pIssUserDefFilter->u4BaseAclOne);

    /* OID :  issAclUserDefinedFilterIdTwoType  */
    if (nmhTestv2IssAclUserDefinedFilterIdTwoType
        (&u4ErrCode, u4UserDefFilterId,
         pIssUserDefFilter->u1AclTwoType) == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test User Defined Filter Two type %d for Filter ID %d                     with  error code as %d\n",
                      pIssUserDefFilter->u1AclTwoType, u4UserDefFilterId,
                      u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssAclUserDefinedFilterIdTwoType
        (u4UserDefFilterId, pIssUserDefFilter->u1AclTwoType);

    /* OID :  issAclUserDefinedFilterIdTwo      */
    if (nmhTestv2IssAclUserDefinedFilterIdTwo
        (&u4ErrCode, u4UserDefFilterId,
         pIssUserDefFilter->u1AclTwoType) == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test User Defined Filter Two  %d for Filter ID %d                     with  error code as %d\n",
                      pIssUserDefFilter->u4BaseAclTwo, u4UserDefFilterId,
                      u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssAclUserDefinedFilterIdTwo
        (u4UserDefFilterId, pIssUserDefFilter->u4BaseAclTwo);

    /* OID :  issAclUserDefinedFilterSubAction  */
    if (nmhTestv2IssAclUserDefinedFilterSubAction
        (&u4ErrCode, u4UserDefFilterId,
         pAccessFilterEntry->u1IssUdbSubAction) == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test User Defined Filter Sub Action %d for Filter ID %d                     with  error code as %d\n",
                      pAccessFilterEntry->u1IssUdbSubAction, u4UserDefFilterId,
                      u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssAclUserDefinedFilterSubAction
        (u4UserDefFilterId, pAccessFilterEntry->u1IssUdbSubAction);

    /* OID :  issAclUserDefinedFilterSubActionId */
    if (nmhTestv2IssAclUserDefinedFilterSubActionId
        (&u4ErrCode, u4UserDefFilterId,
         pAccessFilterEntry->u2IssUdbSubActionId) == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nACL:Failed to test User Defined Filter SubActionId %d for Filter ID %d                     with  error code as %d\n",
                      pAccessFilterEntry->u2IssUdbSubActionId,
                      u4UserDefFilterId, u4ErrCode);
        return (ISS_FAILURE);
    }

    nmhSetIssAclUserDefinedFilterSubActionId
        (u4UserDefFilterId, pAccessFilterEntry->u2IssUdbSubActionId);

    /* OID :  issAclUserDefinedFilterStatus     */
    nmhSetIssAclUserDefinedFilterStatus (u4UserDefFilterId, ACTIVE);

    ISS_TRC (INIT_SHUT_TRC, "\nACL-UDB:Exit IssAclInstallUserDefFilter "
             "function \n");

    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssAclUnInstallUserDefFilter                         */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                      to delete the added user defined ACL rule.           */
/*                                                                           */
/* Input(s)           : pIssUserDefFilter - Pointer to user defined          */
/*                                          filter entry.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
IssAclUnInstallUserDefFilter (tIssUserDefinedFilterTable * pIssUserDefFilter)
{
    INT4                i4Status = ISS_ZERO_ENTRY;
    UINT4               u4ErrCode = ISS_ZERO_ENTRY;
    UINT4               u4UserDefFilterId = ISS_ZERO_ENTRY;

    ISS_TRC (INIT_SHUT_TRC,
             "\nACL:Entry IssAclUnInstallUserDefFilter  function \n");
    if (pIssUserDefFilter == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "ACL:UDB---pIssUserDefFilter is Null\n");
        return (ISS_FAILURE);
    }

    u4UserDefFilterId = pIssUserDefFilter->pAccessFilterEntry->u4UDBFilterId;

    if (nmhGetIssAclUserDefinedFilterStatus (u4UserDefFilterId, &i4Status)
        == SNMP_FAILURE)
    {
        /* Filter does not exist */
        return (ISS_SUCCESS);
    }
    else
    {
        if (nmhTestv2IssAclUserDefinedFilterStatus (&u4ErrCode,
                                                    u4UserDefFilterId,
                                                    ISS_DESTROY)
            == SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nACL:Failed to test UserDefined Filter Status  for Filter ID %d                               with  error code as %d\n",
                          u4UserDefFilterId, u4ErrCode);
            return (ISS_FAILURE);
        }

        if (nmhSetIssAclUserDefinedFilterStatus (u4UserDefFilterId,
                                                 ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nACL:Failed to set UserDefined Filter Status  for Filter ID %d\n",
                          u4UserDefFilterId);
            return (ISS_FAILURE);
        }
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nACL:Exit IssAclUnInstallUserDefFilter  function \n");
    return (ISS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : IssAclUpdateL2FilterEntry                            */
/*                                                                           */
/* Description        : This function is called to update the pL2FilterEntry */
/*                      and invoke the installation of L2 filter entry.      */
/*                                                                           */
/* Input(s)           : pAclFilterInfo - Pointer to ACL filter information   */
/*                      u4L2FilterId - L2 Filter ID                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
IssAclUpdateL2FilterEntry (tAclFilterInfo * pAclFilterInfo, UINT4 u4L2FilterId)
{
    tIssL2FilterEntry   IssL2FilterEntry;

    MEMSET (&IssL2FilterEntry, 0x0, sizeof (tIssL2FilterEntry));

    ISS_TRC (INIT_SHUT_TRC,
             "\nACL:Entry IssAclUpdateL2FilterEntry  function \n");

    IssL2FilterEntry.i4IssL2FilterNo = u4L2FilterId;
    IssL2FilterEntry.i4IssL2FilterPriority = pAclFilterInfo->u1Priority;
    IssL2FilterEntry.i4IssL2FilterEncapType = ISS_ZERO_ENTRY;
    IssL2FilterEntry.u4IssL2FilterProtocolType = ISS_ZERO_ENTRY;
#ifdef ISS_METRO_WANTED
    IssL2FilterEntry.u4IssL2FilterCustomerVlanId = ISS_ZERO_ENTRY;
    IssL2FilterEntry.u4IssL2FilterServiceVlanId = pAclFilterInfo->u2VlanId;
#else
    IssL2FilterEntry.u4IssL2FilterCustomerVlanId = pAclFilterInfo->u2VlanId;
    IssL2FilterEntry.u4IssL2FilterServiceVlanId = ISS_ZERO_ENTRY;
#endif
    IssL2FilterEntry.u4IssL2FilterMatchCount = ISS_ZERO_ENTRY;
    IssL2FilterEntry.u4RefCount = ISS_ZERO_ENTRY;
    IssL2FilterEntry.IssL2FilterAction = pAclFilterInfo->u1Action;
    MEMCPY (IssL2FilterEntry.IssL2FilterSrcMacAddr,
            pAclFilterInfo->HostMac, MAC_ADDR_LEN);
    MEMCPY (IssL2FilterEntry.IssL2FilterInPortList,
            pAclFilterInfo->PortList, ISS_PORT_LIST_SIZE);
    IssL2FilterEntry.u2InnerEtherType = ISS_ZERO_ENTRY;
    IssL2FilterEntry.u2OuterEtherType = ISS_ZERO_ENTRY;
    IssL2FilterEntry.i1IssL2FilterCVlanPriority = ISS_DEFAULT_VLAN_PRIORITY;
    IssL2FilterEntry.i1IssL2FilterSVlanPriority = ISS_DEFAULT_VLAN_PRIORITY;
    IssL2FilterEntry.u1IssL2FilterTagType = ISS_FILTER_SINGLE_TAG;
    IssL2FilterEntry.u1FilterDirection = ISS_DIRECTION_IN;
    IssL2FilterEntry.u1IssL2FilterStatus = ISS_NOT_READY;
    IssL2FilterEntry.u1IssL2FilterCreationMode = ISS_ACL_CREATION_INTERNAL;

    if (IssAclInstallL2Filter (&IssL2FilterEntry) == ISS_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nACL:Failed to  Install L2 Filter \n");
        return ISS_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nACL:Exit IssAclUpdateL2FilterEntry  function \n");
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssAclUpdateL3FilterEntry                            */
/*                                                                           */
/* Description        : This function is called to update the pL3FilterEntry */
/*                      and invoke the installation of L3 filter entry.      */
/*                                                                           */
/* Input(s)           : pAclFilterInfo - Pointer to ACL filter information   */
/*                      u4L3FilterId - L3 Filter ID                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
IssAclUpdateL3FilterEntry (tAclFilterInfo * pAclFilterInfo, UINT4 u4L3FilterId)
{
    tIssL3FilterEntry   IssL3FilterEntry;

    MEMSET (&IssL3FilterEntry, 0x0, sizeof (tIssL3FilterEntry));

    ISS_TRC (INIT_SHUT_TRC,
             "\nACL:Entry IssAclUpdateL3FilterEntry function \n");
    IssL3FilterEntry.i4IssL3FilterNo = u4L3FilterId;
    IssL3FilterEntry.i4IssL3FilterPriority = pAclFilterInfo->u1Priority;
    IssL3FilterEntry.IssL3FilterProtocol = pAclFilterInfo->u1ProtocolId;
    IssL3FilterEntry.i4IssL3FilterMessageType = ISS_DEFAULT_MSG_TYPE;
    IssL3FilterEntry.i4IssL3FilterMessageCode = ISS_DEFAULT_MSG_CODE;
#ifdef ISS_METRO_WANTED
    IssL3FilterEntry.u4CVlanId = ISS_ZERO_ENTRY;
    IssL3FilterEntry.u4SVlanId = pAclFilterInfo->u2VlanId;
#else
    IssL3FilterEntry.u4CVlanId = pAclFilterInfo->u2VlanId;
    IssL3FilterEntry.u4SVlanId = ISS_ZERO_ENTRY;
#endif
    IssL3FilterEntry.u4IssL3FilterDstIpAddr = ISS_ZERO_ENTRY;
    IssL3FilterEntry.u4IssL3FilterSrcIpAddr =
        OSIX_HTONL (pAclFilterInfo->u4HostIp);
    IssL3FilterEntry.u4IssL3FilterDstIpAddrMask = ISS_DEF_FILTER_MASK;
    IssL3FilterEntry.u4IssL3FilterSrcIpAddrMask = ISS_DEF_FILTER_MASK;
    IssL3FilterEntry.u4IssL3FilterMinDstProtPort = ISS_ZERO_ENTRY;
    IssL3FilterEntry.u4IssL3FilterMaxDstProtPort = ISS_MAX_PORT_VALUE;
    IssL3FilterEntry.u4IssL3FilterMinSrcProtPort = ISS_ZERO_ENTRY;
    IssL3FilterEntry.u4IssL3FilterMaxSrcProtPort = ISS_MAX_PORT_VALUE;
    IssL3FilterEntry.IssL3FilterAckBit = ISS_ACK_ANY;
    IssL3FilterEntry.IssL3FilterRstBit = ISS_ACK_ANY;
    IssL3FilterEntry.IssL3FilterTos = ISS_TOS_INVALID;
    IssL3FilterEntry.i4IssL3FilterDscp = ISS_DSCP_INVALID;
    IssL3FilterEntry.IssL3FilterDirection = ISS_DIRECTION_IN;
    IssL3FilterEntry.IssL3FilterAction = pAclFilterInfo->u1Action;
    IssL3FilterEntry.u4IssL3FilterMatchCount = ISS_ZERO_ENTRY;
    IssL3FilterEntry.u4RefCount = ISS_ZERO_ENTRY;
    MEMCPY (IssL3FilterEntry.IssL3FilterInPortList,
            pAclFilterInfo->PortList, ISS_PORT_LIST_SIZE);
    IssL3FilterEntry.u4IssL3FilterMultiFieldClfrDstPrefixLength =
        ISS_ZERO_ENTRY;
    IssL3FilterEntry.u4IssL3FilterMultiFieldClfrSrcPrefixLength =
        ISS_ZERO_ENTRY;
    IssL3FilterEntry.u4IssL3MultiFieldClfrFlowId = ISS_ZERO_ENTRY;
    IssL3FilterEntry.i4IssL3MultiFieldClfrAddrType = QOS_IPV4;
    IssL3FilterEntry.i4StorageType = ISS_ZERO_ENTRY;
    IssL3FilterEntry.i1CVlanPriority = ISS_DEFAULT_VLAN_PRIORITY;
    IssL3FilterEntry.i1SVlanPriority = ISS_DEFAULT_VLAN_PRIORITY;
    IssL3FilterEntry.u1IssL3FilterTagType = ISS_FILTER_SINGLE_TAG;
    IssL3FilterEntry.u1IssL3FilterStatus = NOT_READY;
    IssL3FilterEntry.u1IssL3FilterCreationMode = ISS_ACL_CREATION_INTERNAL;

    if (IssAclInstallL3Filter (&IssL3FilterEntry) == ISS_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nACL:Failed to  Install L3 Filter \n");
        return ISS_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC, "\nACL:Exit IssAclUpdateL3FilterEntry function \n");
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssAclUpdateUDBFilterEntry                           */
/*                                                                           */
/* Description        : This function is called to update the                */
/*                      pIssUserDefFilterEntry and invoke the installation   */
/*                      user defined filter entry.                           */
/*                                                                           */
/* Input(s)           : pAclFilterInfo - Pointer to ACL filter information   */
/*                      u4L2FilterId - L2 filter ID                          */
/*                      u4L3FilterId - L3 filter ID                          */
/*                      u4UDBFilterId - User defined filter ID               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
IssAclUpdateUDBFilterEntry (tAclFilterInfo * pAclFilterInfo,
                            UINT4 u4L2FilterId, UINT4 u4L3FilterId,
                            UINT4 u4UDBFilterId)
{
    tIssUserDefinedFilterTable IssUserDefFilterEntry;
    tIssUDBFilterEntry  AccessFilterEntry;

    MEMSET (&IssUserDefFilterEntry, 0, sizeof (tIssUserDefinedFilterTable));

    MEMSET (&AccessFilterEntry, 0, sizeof (tIssUDBFilterEntry));

    IssUserDefFilterEntry.pAccessFilterEntry = &AccessFilterEntry;

    ISS_TRC (INIT_SHUT_TRC,
             "\nACL:Entry IssAclUpdateUDBFilterEntry  function \n");
    IssUserDefFilterEntry.pAccessFilterEntry->IssAccessFilterAction = ISS_AND;
    MEMCPY (IssUserDefFilterEntry.pAccessFilterEntry->
            IssUdbFilterInPortList,
            pAclFilterInfo->PortList, ISS_PORT_LIST_SIZE);

    IssUserDefFilterEntry.pAccessFilterEntry->u4UDBFilterId = u4UDBFilterId;
    IssUserDefFilterEntry.pAccessFilterEntry->u2IssUdbSubActionId =
        ISS_ZERO_ENTRY;
    IssUserDefFilterEntry.pAccessFilterEntry->u2AccessFilterOffset =
        ISS_ZERO_ENTRY;
    IssUserDefFilterEntry.pAccessFilterEntry->
        u1AccessFilterPktType = ISS_UDB_PKT_TYPE_USER_DEF;
    IssUserDefFilterEntry.pAccessFilterEntry->
        u1AccessFilterPriority = pAclFilterInfo->u1Priority;
    IssUserDefFilterEntry.pAccessFilterEntry->u1IssUdbSubAction = ISS_NONE;
    IssUserDefFilterEntry.u4BaseAclOne = u4L2FilterId;
    IssUserDefFilterEntry.u4BaseAclTwo = u4L3FilterId;
    IssUserDefFilterEntry.u1Operation = pAclFilterInfo->u1Action;
    IssUserDefFilterEntry.u1AclOneType = ISS_L2_FILTER;
    IssUserDefFilterEntry.u1AclTwoType = ISS_L3_FILTER;
    IssUserDefFilterEntry.u1RowStatus = NOT_READY;
    IssUserDefFilterEntry.u1PriorityFlag = ISS_FALSE;

    if (IssAclInstallUserDefFilter (&IssUserDefFilterEntry) == ISS_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nACL:Failed to  Install User DefFilter \n");
        return ISS_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nACL:Exit IssAclUpdateUDBFilterEntry  function \n");
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssAclGetValidL2FilterId                             */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                      to get a valid L2 filter ID.                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu4L2FilterId - Pointer to L2 Filter ID.             */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
VOID
IssAclGetValidL2FilterId (UINT4 *pu4L2FilterId)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;
    UINT4               u4L2FilterId = ISS_ZERO_ENTRY;

    ISS_TRC (INIT_SHUT_TRC,
             "\nACL:Entry IssAclGetValidL2FilterId  function \n");
    /* Scan through the filter ID's from 0 to MAX_FILTER's. */
    ISS_L2_FILTER_ID_SCAN (u4L2FilterId)
    {
        /* Get the L2 filter entry for 'u4L2FilterId' */
        pIssL2FilterEntry = IssExtGetL2FilterEntry ((INT4) u4L2FilterId);

        if (pIssL2FilterEntry == NULL)
        {
            /* If L2 filter entry is not present for 'u4L2FilterId'
             * fill this ID as the valid free ID */
            *pu4L2FilterId = u4L2FilterId;
            break;
        }
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nACL:Failure of IssAclGetValidL2FilterId  function \n");
    return;
}

/*****************************************************************************/
/* Function Name      : IssAclGetValidL3FilterId                             */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                      to get a valid L3 filter ID.                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu4L3FilterId - Pointer to L3 Filter ID.             */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
VOID
IssAclGetValidL3FilterId (UINT4 *pu4L3FilterId)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    UINT4               u4L3FilterId = ISS_ZERO_ENTRY;
    ISS_TRC (INIT_SHUT_TRC,
             "\nACL:Entry IssAclGetValidL3FilterId  function \n");
    /* Scan through the filter ID's from 0 to MAX_FILTER's. */
    ISS_L3_FILTER_ID_SCAN (u4L3FilterId)
    {
        /* Get the L3 filter entry for 'u4L3FilterId' */
        pIssL3FilterEntry = IssExtGetL3FilterEntry ((INT4) u4L3FilterId);

        if (pIssL3FilterEntry == NULL)
        {
            /* If L3 filter entry is not present for 'u4L3FilterId'
             * fill this ID as the valid free ID */
            *pu4L3FilterId = u4L3FilterId;
            break;
        }
    }
    ISS_TRC (INIT_SHUT_TRC, "\nACL:Exit IssAclGetValidL3FilterId  function \n");
    return;
}

/*****************************************************************************/
/* Function Name      : IssAclGetValidUDBFilterId                            */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                      to get a valid user defined filter ID.               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu4UDBFilterId - Pointer to user defined filter ID   */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
VOID
IssAclGetValidUDBFilterId (UINT4 *pu4UDBFilterId)
{
    tIssUserDefinedFilterTable *pIssUDBFilterEntry = NULL;
    UINT4               u4UserDefFilterId = ISS_ZERO_ENTRY;

    ISS_TRC (INIT_SHUT_TRC,
             "\nACL:Entry IssAclGetValidUDBFilterId function \n");
    /* Scan through the filter ID's from 0 to MAX_FILTER's. */
    ISS_UDB_FILTER_ID_SCAN (u4UserDefFilterId)
    {
        /* Get the User defined filter entry for 'u4UserDefFilterId' */
        pIssUDBFilterEntry =
            IssExtGetUdbFilterTableEntry ((INT4) u4UserDefFilterId);

        if (pIssUDBFilterEntry == NULL)
        {
            /* If UDB filter entry is not present for 'u4UserDefFilterId'
             * fill this ID as the valid free ID */
            *pu4UDBFilterId = u4UserDefFilterId;
            break;
        }
    }
    ISS_TRC (INIT_SHUT_TRC, "\nACL:Exit IssAclGetValidUDBFilterId function \n");
    return;
}

/*****************************************************************************/
/* Function Name      : IssAclUpdateHwByL3Priority                           */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                      to update the filter entries in hardware by L3       */
/*                      Priority.                                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          :                                                      */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
VOID
IssAclUpdateHwByL3Priority (tIssL3FilterEntry * pIssL3FilterEntry)
{
    tIssL3FilterEntry  *pIssL3FilterCurrentNode = NULL;
    tIssL3FilterEntry  *pIssL3FilterNode = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT2               au2L3Filter[ISS_MAX_L3_FILTERS];
    UINT2               u2FilterNo = 0;
    INT4                i4RetVal = FNP_FAILURE;

    ISS_TRC (INIT_SHUT_TRC,
             "\nACL:Entry IssAclUpdateHwByL3Priority  function \n");
    /*Adding the entry to the sorted L3 Filter list */
    IssAclAddTosortL3FilterLst (pIssL3FilterEntry);
    /* Initialize the FIlter Array.
     * This Filter Array will be used to store the list of filters that are 
     * deleted in h/w.
     * */
    for (u2FilterNo = 0; u2FilterNo < ISS_MAX_L3_FILTERS; u2FilterNo++)
    {
        au2L3Filter[u2FilterNo] = 0;
    }

    /* Scan the entire L3 Sorted Filter List and then sort the
     * list based on priority - If the priority of the new Filter Entry
     * (pIssL3FilterEntry) is lesser than an existing entry in the
     * ISS_L3FILTER_SORTED_LIST, then the existing entry is
     * deleted in h/w .Then it is updated in the h/w in the respective
     * sorted order.*/
    u2FilterNo = 0;
    TMO_SLL_Scan (&(ISS_L3FILTER_SORTED_LIST), pLstNode, tTMO_SLL_NODE *)
    {
        pIssL3FilterCurrentNode = GET_L3FILTER_PTR_FROM_SORT_LST (pLstNode);
        /*Check if incoming filter's priority is less than the filter's
           priority from the sorted filter list. If yes, delete the entry from
           h/w. Else continue scanning the sorted filter list. */
        if (pIssL3FilterEntry->i4IssL3FilterPriority <
            pIssL3FilterCurrentNode->i4IssL3FilterPriority)
        {
            /* If counter is enabled then set flag to detach
             * and reattach counter after row status becomes ACTIVE */
            if (pIssL3FilterCurrentNode->i4IssL3FilterStatsEnabledStatus
                 == ACL_STAT_ENABLE)
            {
                i4RetVal =
                    IsssysIssHwUpdateL3Filter (pIssL3FilterCurrentNode,
                                               ISS_L3FILTER_STAT_DISABLE);

                if (i4RetVal != FNP_SUCCESS)
                {
                    ISS_TRC (ALL_FAILURE_TRC,
                             "\nLOW:L3 filter cannot disable hardware stats \n");
                }
            }
            /*Deleting the filter entry from hardware whose priority is
             * greater*/
            IsssysIssHwUpdateL3Filter (pIssL3FilterCurrentNode,
                                       ISS_L3FILTER_DELETE);
            /*Store the deleted filter no. in the array */
            au2L3Filter[u2FilterNo] = pIssL3FilterCurrentNode->i4IssL3FilterNo;
            u2FilterNo++;
        }

        else
        {
            continue;
        }
    }

    /* All the deleted entries in H/w are added again in sorted order */
    if (u2FilterNo > 0)
    {
        for (u2FilterNo = 0; u2FilterNo < ISS_MAX_L3_FILTERS; u2FilterNo++)
        {
            if (au2L3Filter[u2FilterNo] != 0)
            {
                pIssL3FilterNode =
                    IssExtGetL3FilterEntry (au2L3Filter[u2FilterNo]);
                /*Adding the deleted filter entry to hardware in correct order */
                IsssysIssHwUpdateL3Filter (pIssL3FilterNode, ISS_L3FILTER_ADD);
                /* Enable counter if required */
                if (pIssL3FilterNode->i4IssL3FilterStatsEnabledStatus
                     == ACL_STAT_ENABLE)
                {
                    i4RetVal =
                        IsssysIssHwUpdateL3Filter (pIssL3FilterNode,
                                                   ISS_L3FILTER_STAT_ENABLE);

                    if (i4RetVal != FNP_SUCCESS)
                    {
                        ISS_TRC (ALL_FAILURE_TRC,
                                 "\nLOW:L3 filter cannot enable hardware stats \n");
                    }
                }
            }
        }
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nACL:Exit IssAclUpdateHwByL3Priority  function \n");
    return;
}

/******************************************************************************
 * Function Name      : IssAclAddTosortL3FilterLst
 *
 * Description        : This routine is used to add the L3 Filter to
 *                      the shadow database of HW L3 Filter list.
 *                      This list is sorted based on priority and stored
 *
 * Input(s)           : pIssL3FilterEntry - Pointer to the L3 Filter to be added
 *
 * Output(s)          : None
 *
 * Return Value(s)    : VOID
 *****************************************************************************/
VOID
IssAclAddTosortL3FilterLst (tIssL3FilterEntry * pIssL3FilterEntry)
{

    tIssL3FilterEntry  *pIssL3FilterCurrentNode = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pPrevLstNode = NULL;

    ISS_TRC (INIT_SHUT_TRC,
             "\nACL:Entry IssAclAddTosortL3FilterLst  function \n");
    TMO_SLL_Scan (&(ISS_L3FILTER_SORTED_LIST), pLstNode, tTMO_SLL_NODE *)
    {
        pIssL3FilterCurrentNode = GET_L3FILTER_PTR_FROM_SORT_LST (pLstNode);
        if (pIssL3FilterEntry->i4IssL3FilterPriority <
            pIssL3FilterCurrentNode->i4IssL3FilterPriority)
        {
            break;
        }
        /*Set the node after which entry is to be added */
        pPrevLstNode = pLstNode;
    }
    /* Adding the filter to the sorted filter list */
    TMO_SLL_Insert (&(ISS_L3FILTER_SORTED_LIST), pPrevLstNode,
                    &(pIssL3FilterEntry->IssSortedNextNode));

    ISS_TRC (INIT_SHUT_TRC,
             "\nACL:Exit IssAclAddTosortL3FilterLst  function \n");
    return;
}

/*****************************************************************************/
/* Function Name      : IssAclUpdateHwByL2Priority                           */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                      to update the filter entries in hardware by L2       */
/*                      Priority.                                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          :                                                      */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
VOID
IssAclUpdateHwByL2Priority (tIssL2FilterEntry * pIssL2FilterEntry)
{
    tIssL2FilterEntry  *pIssL2FilterCurrentNode = NULL;
    tIssL2FilterEntry  *pIssL2FilterNode = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT2               au2L2Filter[ISS_MAX_L2_FILTERS];
    UINT2               u2FilterNo = 0;
    INT4                i4RetVal = FNP_FAILURE;

    ISS_TRC (INIT_SHUT_TRC,
             "\nACL:Entry IssAclUpdateHwByL2Priority function \n");
    /*Adding the entry to the sorted L2 Filter list */
    IssAclAddTosortL2FilterLst (pIssL2FilterEntry);
    /* Initialize the FIlter Array.
     * This Filter Array will be used to store the list of filters that are 
     * deleted in h/w.
     * */
    for (u2FilterNo = 0; u2FilterNo < ISS_MAX_L2_FILTERS; u2FilterNo++)
    {
        au2L2Filter[u2FilterNo] = 0;
    }

    /* Scan the entire L2 Sorted Filter List and then sort the
     * list based on priority - If the priority of the new Filter Entry
     * (pIssL2FilterEntry) is lesser than an existing entry in the
     * ISS_L2FILTER_SORTED_LIST, then the existing entry is
     * deleted in h/w .Then it is updated in the h/w in the respective
     * sorted order.*/
    u2FilterNo = 0;
    TMO_SLL_Scan (&(ISS_L2FILTER_SORTED_LIST), pLstNode, tTMO_SLL_NODE *)
    {
        pIssL2FilterCurrentNode = GET_L2FILTER_PTR_FROM_SORT_LST (pLstNode);
        /*Check if incoming filter's priority is less than the filter's
           priority from the sorted filter list. If yes, delete the entry from
           h/w. Else continue scanning the sorted filter list. */
        if (pIssL2FilterEntry->i4IssL2FilterPriority <
            pIssL2FilterCurrentNode->i4IssL2FilterPriority)
        {
            /* If counter is enabled then set flag to detach
             * and reattach counter after row status becomes ACTIVE */
            if (pIssL2FilterCurrentNode->i4IssL2FilterStatsEnabledStatus
                 == ACL_STAT_ENABLE)
            {
                i4RetVal =
                    IsssysIssHwUpdateL2Filter (pIssL2FilterCurrentNode,
                                               ISS_L2FILTER_STAT_DISABLE);

                if (i4RetVal != FNP_SUCCESS)
                {
                    ISS_TRC (ALL_FAILURE_TRC,
                             "\nLOW:L2 filter cannot disable hardware stats \n");
                }
            }
            /*Deleting the filter entry from hardware whose priority is
             * greater*/
            IsssysIssHwUpdateL2Filter (pIssL2FilterCurrentNode,
                                       ISS_L2FILTER_DELETE);
            /*Store the deleted filter no. in the array */
            au2L2Filter[u2FilterNo] = pIssL2FilterCurrentNode->i4IssL2FilterNo;
            u2FilterNo++;
        }

        else
        {
            continue;
        }
    }

    /* All the deleted entries in H/w are added again in sorted order */
    if (u2FilterNo > 0)
    {
        for (u2FilterNo = 0; u2FilterNo < ISS_MAX_L2_FILTERS; u2FilterNo++)
        {
            if (au2L2Filter[u2FilterNo] != 0)
            {
                pIssL2FilterNode =
                    IssExtGetL2FilterEntry (au2L2Filter[u2FilterNo]);
                /*Adding the deleted filter entry to hardware in correct order */
                IsssysIssHwUpdateL2Filter (pIssL2FilterNode, ISS_L2FILTER_ADD);
                /* Enable counter if required */
                if (pIssL2FilterNode->i4IssL2FilterStatsEnabledStatus
                     == ACL_STAT_ENABLE)
                {
                    i4RetVal =
                        IsssysIssHwUpdateL2Filter (pIssL2FilterNode,
                                                   ISS_L2FILTER_STAT_ENABLE);

                    if (i4RetVal != FNP_SUCCESS)
                    {
                        ISS_TRC (ALL_FAILURE_TRC,
                                 "\nLOW:L2 filter cannot enable hardware stats \n");
                    }
                }
            }
        }
    }

    ISS_TRC (INIT_SHUT_TRC,
             "\nACL:Exit IssAclUpdateHwByL2Priority function \n");
    return;
}

/******************************************************************************
 * Function Name      : IssAclAddTosortL2FilterLst
 *
 * Description        : This routine is used to add the L2 Filter to
 *                      the shadow database of HW L2 Filter list.
 *                      This list is sorted based on priority and stored
 *
 * Input(s)           : pIssL2FilterEntry - Pointer to the L2 Filter to be added
 *
 * Output(s)          : None
 *
 * Return Value(s)    : VOID
 *****************************************************************************/
VOID
IssAclAddTosortL2FilterLst (tIssL2FilterEntry * pIssL2FilterEntry)
{

    tIssL2FilterEntry  *pIssL2FilterCurrentNode = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pPrevLstNode = NULL;

    ISS_TRC (INIT_SHUT_TRC,
             "\nACL:Entry IssAclAddTosortL2FilterLst function \n");
    TMO_SLL_Scan (&(ISS_L2FILTER_SORTED_LIST), pLstNode, tTMO_SLL_NODE *)
    {
        pIssL2FilterCurrentNode = GET_L2FILTER_PTR_FROM_SORT_LST (pLstNode);
        if (pIssL2FilterEntry->i4IssL2FilterPriority <
            pIssL2FilterCurrentNode->i4IssL2FilterPriority)
        {
            break;
        }
        /*Set the node after which entry is to be added */
        pPrevLstNode = pLstNode;
    }
    /* Adding the filter to the sorted filter list */
    TMO_SLL_Insert (&(ISS_L2FILTER_SORTED_LIST), pPrevLstNode,
                    &(pIssL2FilterEntry->IssSortedNextNode));

    ISS_TRC (INIT_SHUT_TRC,
             "\nACL:Exit of IssAclAddTosortL2FilterLst function \n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclValidateMemberPort                            */
/*                                                                           */
/*     DESCRIPTION      : This function checks wheather the filter is        */
/*                   attached with the physical port                    */
/*                                                                           */
/*     INPUT            : u4IfIndex - Physical port number                   */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
AclValidateMemberPort (UINT4 u4IfIndex)
{
    tSNMP_OCTET_STRING_TYPE InPortList;
    tSNMP_OCTET_STRING_TYPE OutPortList;
    UINT1               au1InPortList[ISS_PORT_LIST_SIZE];
    UINT1               au1OutPortList[ISS_PORT_LIST_SIZE];
    INT4                i4CurrentFilter = 0;
    INT4                i4FilterNo = 0;

    MEMSET (au1InPortList, 0, ISS_PORT_LIST_SIZE);
    MEMSET (au1OutPortList, 0, ISS_PORT_LIST_SIZE);

    InPortList.i4_Length = ISS_PORT_LIST_SIZE;
    InPortList.pu1_OctetList = &au1InPortList[0];

    OutPortList.i4_Length = ISS_PORT_LIST_SIZE;
    OutPortList.pu1_OctetList = &au1OutPortList[0];

    /* Check the L2 filter table */
    if (nmhGetFirstIndexIssExtL2FilterTable (&i4FilterNo) == SNMP_SUCCESS)
    {
        do
        {
            nmhGetIssExtL2FilterInPortList (i4FilterNo, &InPortList);
            nmhGetIssExtL2FilterOutPortList (i4FilterNo, &OutPortList);

            if (CliIsMemberPort (InPortList.pu1_OctetList,
                                 ISS_PORT_LIST_SIZE, u4IfIndex) == CLI_SUCCESS)
            {
                return (CLI_FAILURE);
            }
            if (CliIsMemberPort (OutPortList.pu1_OctetList,
                                 ISS_PORT_LIST_SIZE, u4IfIndex) == CLI_SUCCESS)
            {
                return (CLI_FAILURE);
            }
            i4CurrentFilter = i4FilterNo;
        }
        while ((nmhGetNextIndexIssExtL2FilterTable
                (i4CurrentFilter, &i4FilterNo) == SNMP_SUCCESS));
    }

    /* Check the L3 filter table */
    i4FilterNo = 0;
    i4CurrentFilter = 0;
    if (nmhGetFirstIndexIssExtL3FilterTable (&i4FilterNo) == SNMP_SUCCESS)
    {
        do
        {
            nmhGetIssExtL3FilterInPortList (i4FilterNo, &InPortList);
            nmhGetIssExtL3FilterOutPortList (i4FilterNo, &OutPortList);

            if (CliIsMemberPort (InPortList.pu1_OctetList,
                                 ISS_PORT_LIST_SIZE, u4IfIndex) == CLI_SUCCESS)
            {
                return (CLI_FAILURE);
            }
            if (CliIsMemberPort (OutPortList.pu1_OctetList,
                                 ISS_PORT_LIST_SIZE, u4IfIndex) == CLI_SUCCESS)
            {
                return (CLI_FAILURE);
            }
            i4CurrentFilter = i4FilterNo;

        }
        while ((nmhGetNextIndexIssExtL3FilterTable
                (i4CurrentFilter, &i4FilterNo) == SNMP_SUCCESS));
    }
    return (CLI_SUCCESS);

}



/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclValidateIfMemberoOfPortChannel                  */
/*                                                                           */
/*     DESCRIPTION      : This function checks wheather the  port channel    */
/*                        is configured with ACL group                       */
/*                                                                           */
/*     INPUT            : u4IfIndex - Physical port number                   */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4 AclValidateIfMemberoOfPortChannel(UINT4  u4IfIndex)
{

    tSNMP_OCTET_STRING_TYPE InPortChannelList;
    tSNMP_OCTET_STRING_TYPE OutPortChannelList;
    UINT1               au1InPortList[ISS_PORT_CHANNEL_LIST_SIZE];
    UINT1               au1OutPortList[ISS_PORT_CHANNEL_LIST_SIZE];
    INT4                i4CurrentFilter = 0;
    INT4                i4FilterNo = 0;
    UINT2               pu2AggPortIndex;
    UINT1               u1Flag = FALSE;
    MEMSET (au1InPortList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    MEMSET (au1OutPortList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    InPortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;
    InPortChannelList.pu1_OctetList = &au1InPortList[0];
    OutPortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;
    OutPortChannelList.pu1_OctetList = &au1OutPortList[0];

    ISS_TRC (INIT_SHUT_TRC,
             "\nACL:Entry AclValidateIfMemberoOfPortChannel function \n");

    if (L2IwfIsPortInPortChannel (u4IfIndex) == L2IWF_SUCCESS)
    {
        L2IwfGetPortChannelForPort (u4IfIndex,&pu2AggPortIndex);
        /* Check the L2 filter table */
        if (nmhGetFirstIndexIssExtL2FilterTable (&i4FilterNo) == SNMP_SUCCESS)
        {

            do
            {
                nmhGetIssExtL2FilterInPortChannelList (i4FilterNo, &InPortChannelList);
                nmhGetIssExtL2FilterOutPortChannelList (i4FilterNo, &OutPortChannelList);

                if (CliIsMemberPort (InPortChannelList.pu1_OctetList,
                                     ISS_PORT_CHANNEL_LIST_SIZE,pu2AggPortIndex) == CLI_SUCCESS)
                {
                    u1Flag = TRUE;
                }
                if (CliIsMemberPort (OutPortChannelList.pu1_OctetList,
                                     ISS_PORT_CHANNEL_LIST_SIZE, pu2AggPortIndex) == CLI_SUCCESS)
                {
                    u1Flag = TRUE;
                }
                i4CurrentFilter = i4FilterNo;
                if(u1Flag  == TRUE)
                {
                    break;
                }
            }
            while ((nmhGetNextIndexIssExtL2FilterTable
                    (i4CurrentFilter, &i4FilterNo) == SNMP_SUCCESS));
        }
        if(u1Flag != TRUE)
        {
            /* Check the L3 filter table */
            i4FilterNo = 0;
            i4CurrentFilter = 0;
            if (nmhGetFirstIndexIssExtL3FilterTable (&i4FilterNo) == SNMP_SUCCESS)
            {
                do
                {
                    nmhGetIssExtL3FilterInPortChannelList (i4FilterNo, &InPortChannelList);
                    nmhGetIssExtL3FilterOutPortChannelList (i4FilterNo, &OutPortChannelList);

                    if (CliIsMemberPort (InPortChannelList.pu1_OctetList,
                                         ISS_PORT_CHANNEL_LIST_SIZE,pu2AggPortIndex) == CLI_SUCCESS)
                    {
                         u1Flag = TRUE;
                    }
                    if (CliIsMemberPort (OutPortChannelList.pu1_OctetList,
                                         ISS_PORT_CHANNEL_LIST_SIZE,pu2AggPortIndex) == CLI_SUCCESS)
                    {
                        u1Flag = TRUE;
                    }
                    i4CurrentFilter = i4FilterNo;
                    if( u1Flag == TRUE)
                    {
                        break;
                    }

                }
                while ((nmhGetNextIndexIssExtL3FilterTable
                        (i4CurrentFilter, &i4FilterNo) == SNMP_SUCCESS));
            }
        }

        if( u1Flag == TRUE)
        {
            return (CLI_FAILURE);
        }

    }

    ISS_TRC (INIT_SHUT_TRC,
             "\nACL:Exit AclValidateIfMemberoOfPortChannel function \n");
    return (CLI_SUCCESS);

}


/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclValidatePortFromPortChannel                     */
/*                                                                           */
/*     DESCRIPTION      : This function checks wheather the  port in         */
/*                        port-channel are configured with ACL GROUP         */
/*                                                                           */
/*     INPUT            : u4IfIndex - Physical port number                   */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4 AclValidatePortFromPortChannel(UINT4  u4IfIndex)
{

    UINT2               au2ActivePorts[L2IWF_MAX_PORTS_PER_CONTEXT];
    UINT2               u2Index;
    UINT2               u2NumPorts = 0;
    UINT1               u1IfType;
    UINT1               u1RetVal;
    UINT1               u1Flag = FALSE;

    ISS_TRC (INIT_SHUT_TRC,
             "\nACL:Entry AclValidatePortFromPortChannel function \n");

    CfaGetIfType (u4IfIndex, &u1IfType);
    if (u1IfType == CFA_LAGG)
    {
        if (L2IwfGetActivePortsForPortChannel( (UINT2) u4IfIndex,
                                               au2ActivePorts,
                                               &u2NumPorts)!= L2IWF_FAILURE)
        {

            for (u2Index = 0; u2Index < u2NumPorts; u2Index++)
            {
                u1RetVal = AclValidateMemberPort (au2ActivePorts[u2Index]);
                if(u1RetVal == CLI_FAILURE)
                {
                    u1Flag = TRUE;
                    break;
                }

            }
            if(u1Flag == TRUE)
            {
                return CLI_FAILURE;
            }

        }

    }

    ISS_TRC (INIT_SHUT_TRC,
             "\nACL:Exit AclValidatePortFromPortChannel function \n");
    return CLI_SUCCESS;
}
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssAclVlanSetSChFilterStatus                       */
/*                                                                           */
/*     DESCRIPTION      : This function sets the filter status to
 *                        enable/disable for the S-Channel Interface         */
/*                                                                           */
/*     INPUT            : u4SChIfIndex  - S-Channel Index                    */
/*                   u4RetValFsMIEvbSChannelFilterStatus - Filter status   */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/FAILURE                                */
/*                                                                           */
/*****************************************************************************/

INT4
IssAclVlanSetSChFilterStatus (UINT4 u4SChIfIndex,UINT4 u4RetValFsMIEvbSChannelFilterStatus)
{
    INT4 i4Action = 0;
    tIssL2FilterEntry   IssExtL2FilterEntry;
    MEMSET (&IssExtL2FilterEntry, 0, sizeof (tIssL2FilterEntry));

    ISS_TRC (INIT_SHUT_TRC,
            "\nACL:Entry IssAclVlanSetSChFilterStatus function \n");

    if(u4RetValFsMIEvbSChannelFilterStatus == ACL_SCH_FILTER_ENABLE)
    {
        IssExtL2FilterEntry.u1IssL2FilterStatus = ISS_ACL_FILTER_ENABLE;
        i4Action = ISS_L2FILTER_ADD;
    }

    IssExtL2FilterEntry.u4SChannelIfIndex = u4SChIfIndex;
    if (IsssysIssHwUpdateL2Filter (&IssExtL2FilterEntry,i4Action) 
            == OSIX_FAILURE)
    {
        return (CLI_FAILURE);
    }

    ISS_TRC (INIT_SHUT_TRC,
            "\nACL:Exit of IssAclVlanSetSChFilterStatus function \n");
    return (CLI_SUCCESS);

}

