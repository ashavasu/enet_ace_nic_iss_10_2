/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: issexsys.c,v 1.28 2016/06/02 12:48:41 siva Exp $
 *
 * Description: This file contains the ACL module initialisation routines
 *****************************************************************************/

#ifndef _ISSEXSYS_C
#define _ISSEXSYS_C

#include "issexinc.h"
#include "fsissewr.h"
#include "fsissawr.h"
#include "fsissalw.h"
#include "webiss.h"
extern double       UtilPow (double, double);
/* Port list bit mask list */
UINT1               gau1IssPortBitMask[ISS_REDIRECT_PORTS_PER_BYTE] =
    { 0x01, 0x80, 0x40, 0x20, 0x10,
    0x08, 0x04, 0x02
};

tIssPriorityFilterTable *gaPriorityTable[ISS_MAX_FILTER_PRIORITY];
 INT4 gISSOutFilterCount;

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssExInit                                        */
/*                                                                          */
/*    Description        : This function initialises the memory pools used  */
/*                         by the ISS Extension Module.                     */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/ISS_FAILURE                          */
/****************************************************************************/
INT4
IssExInit ()
{
    ISS_TRC (INIT_SHUT_TRC, "\nSYS:Entry IssExInit  function \n");
    /* Allocate memory pool for Rate ctrl table */
    if (ISS_CREATE_RATEENTRY_MEM_POOL (ISS_RATEENTRY_MEMBLK_SIZE,
                                       ISS_RATEENTRY_MEMBLK_COUNT,
                                       &(ISS_RATEENTRY_POOL_ID)) != MEM_SUCCESS)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "SYS:Failed to Allocate memory pool for Rate ctrl table with MEMBLK_SIZE %d, and MEMBLK_COUNT %d for POOL_ID %d\n",
                      ISS_RATEENTRY_MEMBLK_SIZE, ISS_RATEENTRY_MEMBLK_COUNT,
                      ISS_RATEENTRY_POOL_ID);
        return ISS_FAILURE;
    }

    /* Call the Hardware routine to initialise filters in hardware */

    /* Allocate memory pool for L2 Filter table */
    if (ISS_CREATE_L2FILTERENTRY_MEM_POOL (ISS_L2FILTERENTRY_MEMBLK_SIZE,
                                           ISS_L2FILTERENTRY_MEMBLK_COUNT,
                                           &(ISS_L2FILTERENTRY_POOL_ID))
        != MEM_SUCCESS)
    {
        ISS_DELETE_RATEENTRY_MEM_POOL (ISS_RATEENTRY_POOL_ID);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "SYS:Failed to Allocate  memory pool for  L2 filters MEMBLK_SIZE %d, and MEMBLK_COUNT %d for POOL_ID %d\n",
                      ISS_L2FILTERENTRY_MEMBLK_SIZE,
                      ISS_L2FILTERENTRY_MEMBLK_COUNT,
                      ISS_L2FILTERENTRY_POOL_ID);
        return ISS_FAILURE;
    }

    /* Initialise the SLL lists for L2 Filter Table */
    ISS_SLL_INIT (&(ISS_L2FILTER_LIST));
    ISS_SLL_INIT (&(ISS_L2FILTER_SORTED_LIST));

    /* Allocate memory pool for L3 Filter table */
    if (ISS_CREATE_L3FILTERENTRY_MEM_POOL (ISS_L3FILTERENTRY_MEMBLK_SIZE,
                                           ISS_L3FILTERENTRY_MEMBLK_COUNT,
                                           &(ISS_L3FILTERENTRY_POOL_ID))
        != MEM_SUCCESS)
    {
        ISS_DELETE_RATEENTRY_MEM_POOL (ISS_RATEENTRY_POOL_ID);
        ISS_DELETE_L2FILTERENTRY_MEM_POOL (ISS_L2FILTERENTRY_POOL_ID);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "SYS:Failed to Allocate  memory pool for  L3 filters MEMBLK_SIZE %d, and MEMBLK_COUNT %d for POOL_ID %d\n",
                      ISS_L3FILTERENTRY_MEMBLK_SIZE,
                      ISS_L3FILTERENTRY_MEMBLK_COUNT,
                      ISS_L3FILTERENTRY_POOL_ID);
        return ISS_FAILURE;
    }

    if (ISS_CREATE_ACL_MSG_QUEUE_MEM_POOL (sizeof (tAclQMsg),
                                           ISS_QUEUE_SIZE,
                                           &(ISS_ACL_MSG_QUEUE_POOL_ID))
        != MEM_SUCCESS)
    {
        ISS_DELETE_RATEENTRY_MEM_POOL (ISS_RATEENTRY_POOL_ID);
        ISS_DELETE_L2FILTERENTRY_MEM_POOL (ISS_L2FILTERENTRY_POOL_ID);
        ISS_DELETE_L3FILTERENTRY_MEM_POOL (ISS_L3FILTERENTRY_POOL_ID);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "SYS:Failed to Allocate memory pool to tAclQMsg %d with QUEUE_SIZE %d for POOL_ID %d\n",
                      sizeof (tAclQMsg), ISS_QUEUE_SIZE,
                      ISS_ACL_MSG_QUEUE_POOL_ID);
        return ISS_FAILURE;
    }

    if (ISS_CREATE_FILTER_SHADOW_MEM_POOL (sizeof (tIssFilterShadowTable),
                                           ISS_FILTER_SHADOW_POOL_SIZE,
                                           &(ISS_FILTER_SHADOW_POOL_ID))
        != MEM_SUCCESS)
    {
        ISS_DELETE_RATEENTRY_MEM_POOL (ISS_RATEENTRY_POOL_ID);
        ISS_DELETE_L2FILTERENTRY_MEM_POOL (ISS_L2FILTERENTRY_POOL_ID);
        ISS_DELETE_L3FILTERENTRY_MEM_POOL (ISS_L3FILTERENTRY_POOL_ID);
	ISS_DELETE_ACL_MSG_QUEUE_MEM_POOL (ISS_ACL_MSG_QUEUE_POOL_ID);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "SYS:Failed to Allocate memory pool to FilterShadowTable %d with POOL_SIZE %d for POOL_ID %d\n",
                      sizeof (tIssFilterShadowTable),
                      ISS_FILTER_SHADOW_POOL_SIZE, ISS_FILTER_SHADOW_POOL_ID);
        return ISS_FAILURE;
    }

    if (ISS_CREATE_RESERV_FRAME_MEM_POOL(sizeof (tIssReservFrmCtrlTable),
                                           ISS_RESERV_FRAME_POOL_SIZE,
                                           &(ISS_RESERV_FRAME_CTRL_ENTRY_POOL_ID))
        != MEM_SUCCESS)
    {
        ISS_DELETE_RATEENTRY_MEM_POOL (ISS_RATEENTRY_POOL_ID);
        ISS_DELETE_L2FILTERENTRY_MEM_POOL (ISS_L2FILTERENTRY_POOL_ID);
        ISS_DELETE_L3FILTERENTRY_MEM_POOL (ISS_L3FILTERENTRY_POOL_ID);
	ISS_DELETE_ACL_MSG_QUEUE_MEM_POOL (ISS_ACL_MSG_QUEUE_POOL_ID);
	ISS_DELETE_FILTER_SHADOW_MEM_POOL (ISS_FILTER_SHADOW_POOL_ID);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "SYS:Failed to Allocate memory pool to Reserv Frame control Table %d with POOL_SIZE %d for POOL_ID %d\n",
                     sizeof (tIssReservFrmCtrlTable), 
                      ISS_RESERV_FRAME_POOL_SIZE, ISS_RESERV_FRAME_CTRL_ENTRY_POOL_ID);
        return ISS_FAILURE;
    }

    if (OsixQueCrt ((UINT1 *) ISS_SYS_ACL_QUEUE, OSIX_MAX_Q_MSG_LEN,
                    ISS_QUEUE_SIZE, &(ISS_ACL_MSG_QUE_ID)) != OSIX_SUCCESS)
    {
        ISS_DELETE_RATEENTRY_MEM_POOL (ISS_RATEENTRY_POOL_ID);
        ISS_DELETE_L2FILTERENTRY_MEM_POOL (ISS_L2FILTERENTRY_POOL_ID);
        ISS_DELETE_L3FILTERENTRY_MEM_POOL (ISS_L3FILTERENTRY_POOL_ID);
        ISS_DELETE_ACL_MSG_QUEUE_MEM_POOL (ISS_ACL_MSG_QUEUE_POOL_ID);
        ISS_DELETE_FILTER_SHADOW_MEM_POOL (ISS_FILTER_SHADOW_POOL_ID);
        ISS_DELETE_RESERVE_FRAME_MEM_POOL (ISS_RESERV_FRAME_CTRL_ENTRY_POOL_ID);
        ISS_TRC_ARG4 (ALL_FAILURE_TRC,
                      "SYS:Failed to Allocate memory pool to ACL_QUEUE %dof MAX_Q_MSG_LEN %dwith QUEUE_SIZE %d for POOL_ID %d\n",
                      ISS_SYS_ACL_QUEUE, OSIX_MAX_Q_MSG_LEN, ISS_QUEUE_SIZE,
                      ISS_ACL_MSG_QUE_ID);
        return ISS_FAILURE;
    }

    /* Initialise the SLL lists for L3 Filter Table */
    ISS_SLL_INIT (&(ISS_L3FILTER_LIST));
    TMO_SLL_Init (&(ISS_L3FILTER_SORTED_LIST));

    /* Allocate memory for redirect global info */
    gpIssRedirectIntfInfo = MEM_MALLOC ((sizeof (tIssRedirectIntfGrpTable) *
                                         ISS_MAX_REDIRECT_GRP_ID),
                                        tIssRedirectIntfGrpTable);

    if (gpIssRedirectIntfInfo != NULL)
    {
        ISS_MEMSET (gpIssRedirectIntfInfo, 0,
                    (sizeof (tIssRedirectIntfGrpTable) *
                     ISS_MAX_REDIRECT_GRP_ID));
    }
    else
    {
        ISS_DELETE_RATEENTRY_MEM_POOL (ISS_RATEENTRY_POOL_ID);
        ISS_DELETE_L2FILTERENTRY_MEM_POOL (ISS_L2FILTERENTRY_POOL_ID);
        ISS_DELETE_L3FILTERENTRY_MEM_POOL (ISS_L3FILTERENTRY_POOL_ID);
        ISS_DELETE_ACL_MSG_QUEUE_MEM_POOL (ISS_ACL_MSG_QUEUE_POOL_ID);
        ISS_DELETE_FILTER_SHADOW_MEM_POOL (ISS_FILTER_SHADOW_POOL_ID);
        ISS_DELETE_RESERVE_FRAME_MEM_POOL (ISS_RESERV_FRAME_CTRL_ENTRY_POOL_ID);
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "SYS:Failed to Allocate memory pool RedirectIntfInfo for Redirect group ID %d\n",
                      ISS_MAX_REDIRECT_GRP_ID);
        return ISS_FAILURE;
    }

    /* Allocate memory pool for UDB Filter table and Entry */

    if (ISS_CREATE_UDB_FILTER_TABLE_MEM_POOL (ISS_UDB_FILTER_TABLE_MEMBLK_SIZE,
                                              ISS_UDB_FILTER_TABLE_MEMBLK_COUNT,
                                              &(ISS_UDB_FILTER_TABLE_POOL_ID))
        != MEM_SUCCESS)
    {
        ISS_DELETE_RATEENTRY_MEM_POOL (ISS_RATEENTRY_POOL_ID);
        ISS_DELETE_L2FILTERENTRY_MEM_POOL (ISS_L2FILTERENTRY_POOL_ID);
        ISS_DELETE_L3FILTERENTRY_MEM_POOL (ISS_L3FILTERENTRY_POOL_ID);
        ISS_DELETE_ACL_MSG_QUEUE_MEM_POOL (ISS_ACL_MSG_QUEUE_POOL_ID);
        ISS_DELETE_FILTER_SHADOW_MEM_POOL (ISS_FILTER_SHADOW_POOL_ID);
        ISS_DELETE_RESERVE_FRAME_MEM_POOL (ISS_RESERV_FRAME_CTRL_ENTRY_POOL_ID);
        ISS_TRC (INIT_SHUT_TRC,
                 "UDB Filter Table Memory Pool Creation FAILED\n");
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "SYS:Failed to Allocate  Memory Pool to UDB filter table with MEMBLK_SIZE %d, and MEMBLK_COUNT %d for POOL_ID %d\n",
                      ISS_UDB_FILTER_TABLE_MEMBLK_SIZE,
                      ISS_UDB_FILTER_TABLE_MEMBLK_COUNT,
                      ISS_UDB_FILTER_TABLE_POOL_ID);
        return ISS_FAILURE;
    }

    if (ISS_CREATE_UDB_FILTER_ENTRY_MEM_POOL (ISS_UDB_FILTER_ENTRY_MEMBLK_SIZE,
                                              ISS_UDB_FILTERENTRY_MEMBLK_COUNT,
                                              &(ISS_UDB_FILTERENTRY_POOL_ID))
        != MEM_SUCCESS)
    {
        ISS_DELETE_RATEENTRY_MEM_POOL (ISS_RATEENTRY_POOL_ID);
        ISS_DELETE_L2FILTERENTRY_MEM_POOL (ISS_L2FILTERENTRY_POOL_ID);
        ISS_DELETE_L3FILTERENTRY_MEM_POOL (ISS_L3FILTERENTRY_POOL_ID);
        ISS_DELETE_ACL_MSG_QUEUE_MEM_POOL (ISS_ACL_MSG_QUEUE_POOL_ID);
        ISS_DELETE_FILTER_SHADOW_MEM_POOL (ISS_FILTER_SHADOW_POOL_ID);
        ISS_DELETE_RESERVE_FRAME_MEM_POOL (ISS_RESERV_FRAME_CTRL_ENTRY_POOL_ID);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "SYS:Failed to Allocate  Memory Pool to UDB filter entry with MEMBLK_SIZE %d, and MEMBLK_COUNT %d for POOL_ID %d\n",
                      ISS_UDB_FILTER_ENTRY_MEMBLK_SIZE,
                      ISS_UDB_FILTERENTRY_MEMBLK_COUNT,
                      ISS_UDB_FILTERENTRY_POOL_ID);
        return ISS_FAILURE;
    }

    /* Initialise the SLL lists for UDB Filter Table */
    ISS_SLL_INIT (&(ISS_UDB_FILTER_TABLE_LIST));

#ifdef SNMP_2_WANTED
    RegisterFSISSE ();
    RegisterFSISSM ();
    RegisterFSISSA ();
#endif

    AclRedInitGlobalInfo ();

    ISS_TRC (INIT_SHUT_TRC, "\nSYS:Exit IssExInit  function \n");
    return ISS_SUCCESS;
}

/****************************************************************************/
/*    Function Name      : AclShutdown                                      */
/*                                                                          */
/*    Description        : This function shutdown the ISS Extension Module. */
/*                   And will be used only by RM Modue for HA support.*/
/*                    in case of communication lost scenario        */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : OSIX_SUCCESS/OSIX_FAILURE                          */
/****************************************************************************/
PUBLIC INT4
AclStart ()
{
    ISS_TRC (INIT_SHUT_TRC, "\nSYS:Entry AclStart  function \n");
    /* Allocate memory pool for Rate ctrl table */
    if (ISS_CREATE_RATEENTRY_MEM_POOL (ISS_RATEENTRY_MEMBLK_SIZE,
                                       ISS_RATEENTRY_MEMBLK_COUNT,
                                       &(ISS_RATEENTRY_POOL_ID)) != MEM_SUCCESS)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "SYS:Failed to Allocate memory pool for Rate ctrl table with MEMBLK_SIZE %d and MEMBLK_COUNT %d for POOL_ID %d\n",
                      ISS_RATEENTRY_MEMBLK_SIZE, ISS_RATEENTRY_MEMBLK_COUNT,
                      ISS_RATEENTRY_POOL_ID);
        return OSIX_FAILURE;
    }

    /* Call the Hardware routine to initialise filters in hardware */

    /* Allocate memory pool for L2 Filter table */
    if (ISS_CREATE_L2FILTERENTRY_MEM_POOL (ISS_L2FILTERENTRY_MEMBLK_SIZE,
                                           ISS_L2FILTERENTRY_MEMBLK_COUNT,
                                           &(ISS_L2FILTERENTRY_POOL_ID))
        != MEM_SUCCESS)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "SYS:Failed to Allocate memory pool for L2 Filter table with MEMBLK_SIZE %d and MEMBLK_COUNT %d for POOL_ID %d\n",
                      ISS_L2FILTERENTRY_MEMBLK_SIZE,
                      ISS_L2FILTERENTRY_MEMBLK_COUNT,
                      ISS_L2FILTERENTRY_POOL_ID);
        return OSIX_FAILURE;
    }

    /* Initialise the SLL lists for L2 Filter Table */
    ISS_SLL_INIT (&(ISS_L2FILTER_LIST));

    /* Allocate memory pool for L3 Filter table */
    if (ISS_CREATE_L3FILTERENTRY_MEM_POOL (ISS_L3FILTERENTRY_MEMBLK_SIZE,
                                           ISS_L3FILTERENTRY_MEMBLK_COUNT,
                                           &(ISS_L3FILTERENTRY_POOL_ID))
        != MEM_SUCCESS)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "SYS:Failed to Allocate memory pool for L3 Filter table with MEMBLK_SIZE %d and MEMBLK_COUNT %d for POOL_ID %d\n",
                      ISS_L3FILTERENTRY_MEMBLK_SIZE,
                      ISS_L3FILTERENTRY_MEMBLK_COUNT,
                      ISS_L3FILTERENTRY_POOL_ID);
        return OSIX_FAILURE;
    }

    /* Initialise the SLL lists for L3 Filter Table */
    ISS_SLL_INIT (&(ISS_L3FILTER_LIST));

    if (ISS_CREATE_FILTER_SHADOW_MEM_POOL (sizeof (tIssFilterShadowTable),
                                           ISS_FILTER_SHADOW_POOL_SIZE,
                                           &(ISS_FILTER_SHADOW_POOL_ID))
        != MEM_SUCCESS)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "SYS:Failed to  Initialise the SLL lists for L3 FilterShadowTable %d with POOL_SIZE %d for POOL_ID %d\n",
                      sizeof (tIssFilterShadowTable),
                      ISS_FILTER_SHADOW_POOL_SIZE, ISS_FILTER_SHADOW_POOL_ID);
        return OSIX_FAILURE;
    }

    AclRedInitGlobalInfo ();
    ISS_TRC (INIT_SHUT_TRC, "\nSYS:Exit AclStart  function \n");
    return OSIX_SUCCESS;
}

/****************************************************************************/
/*    Function Name      : AclShutdown                                      */
/*                                                                          */
/*    Description        : This function shutdown the ISS Extension Module. */
/*                   And will be used only by RM Modue for HA support.*/
/*                    in case of communication lost scenario        */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : OSIX_SUCCESS/OSIX_FAILURE                          */
/****************************************************************************/
PUBLIC INT4
AclShutdown ()
{

    tTMO_SLL_NODE      *pCurrNode = NULL;
    tTMO_SLL_NODE      *pNextNode = NULL;
    tIssRateCtrlEntry  *pIssRateCtrlEntry = NULL;
    tIssSllNode        *pSllNode = NULL;
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    tIssReservFrmCtrlTable  *pIssReservFrmCtrlInfo = NULL;
    UINT4		u4FrameControlIndex = 0;
    UINT2               u2PortIndex = 0;
    INT4                i4RetStatus = 0;

    ISS_TRC (INIT_SHUT_TRC, "\nSYS:Entry AclShutdown function \n");
    /* Release all Rate Control Entries */
    for (u2PortIndex = 1; u2PortIndex < ISS_RATEENTRY_MEMBLK_COUNT;
         u2PortIndex++)
    {
        pIssRateCtrlEntry = gIssExGlobalInfo.apIssRateCtrlEntry[u2PortIndex];
        /* Release the RateCtrl Mem Block to the Pool */
        if (pIssRateCtrlEntry != NULL)
        {
            if (ISS_RATEENTRY_FREE_MEM_BLOCK (pIssRateCtrlEntry) != MEM_SUCCESS)
            {
                ISS_TRC (INIT_SHUT_TRC,
                         "ISS_RATEENTRY_FREE_MEM_BLOCK() FAILED\n");
            }
        }
        gIssExGlobalInfo.apIssRateCtrlEntry[u2PortIndex] = NULL;
    }

    /* Release all Reserved frame control Entries */
    for (u4FrameControlIndex = 1; u4FrameControlIndex < ISS_RESERV_FRAME_POOL_SIZE;
         u2PortIndex++)
    {
        pIssReservFrmCtrlInfo = GET_RESERVE_FRAME_CTRL_INFO (u4FrameControlIndex);

        /* Release the RateCtrl Mem Block to the Pool */
        if (pIssReservFrmCtrlInfo != NULL)
        {
            if (ISS_RESERVE_FRAME_FREE_MEM_BLOCK (pIssReservFrmCtrlInfo) != MEM_SUCCESS)
            {
                ISS_TRC (INIT_SHUT_TRC,
                         "ISS_RESERVE_FRAME_FREE_MEM_BLOCK () FAILED\n");
            }
        }
        GET_RESERVE_FRAME_CTRL_INFO (u4FrameControlIndex) = NULL;
    }


    /* Delete Rate Control Memory Pool */
    ISS_DELETE_RATEENTRY_MEM_POOL (ISS_RATEENTRY_POOL_ID);
    ISS_RATEENTRY_POOL_ID = 0;

    pSllNode = ISS_SLL_FIRST (&ISS_L2FILTER_LIST);

    while (pSllNode != NULL)
    {
        pIssL2FilterEntry = (tIssL2FilterEntry *) pSllNode;

        /* Delete the Entry from the Software */
        if (ISS_FILTER_SHADOW_FREE_MEM_BLOCK
            (pIssL2FilterEntry->pIssFilterShadowInfo) != MEM_SUCCESS)
        {
            ISS_TRC (INIT_SHUT_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                     "Filter Shadow Entry Free Failure\n");
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L2FILTER_LIST, pSllNode);
    }

    if ((TMO_SLL_Count (&(ISS_L2FILTER_LIST))) > 0)
    {
        UTL_SLL_OFFSET_SCAN (&(ISS_L2FILTER_LIST),
                             pCurrNode, pNextNode, tTMO_SLL_NODE *)
        {
            TMO_SLL_Delete (&(ISS_L2FILTER_LIST), pCurrNode);

            i4RetStatus =
                MemReleaseMemBlock (ISS_L2FILTERENTRY_POOL_ID,
                                    (UINT1 *) pCurrNode);

            if (i4RetStatus != MEM_SUCCESS)
            {
                ISS_TRC (BUFFER_TRC | INIT_SHUT_TRC | ALL_FAILURE_TRC,
                         "ReleaseMemBlok L2 filter Entry Failed\n");
            }

        }                        /* End of Scan */

    }
    ISS_DELETE_L2FILTERENTRY_MEM_POOL (ISS_L2FILTERENTRY_POOL_ID);
    ISS_L2FILTERENTRY_POOL_ID = 0;

    pSllNode = ISS_SLL_FIRST (&ISS_L3FILTER_LIST);

    while (pSllNode != NULL)
    {
        pIssL3FilterEntry = (tIssL3FilterEntry *) pSllNode;

        /* Delete the Entry from the Software */
        if (ISS_FILTER_SHADOW_FREE_MEM_BLOCK
            (pIssL3FilterEntry->pIssFilterShadowInfo) != MEM_SUCCESS)
        {
            ISS_TRC (BUFFER_TRC | INIT_SHUT_TRC | ALL_FAILURE_TRC,
                     "Filter Shadow Entry Free Failure\n");
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L3FILTER_LIST, pSllNode);
    }
    if ((TMO_SLL_Count (&(ISS_L3FILTER_LIST))) > 0)
    {
        UTL_SLL_OFFSET_SCAN (&(ISS_L3FILTER_LIST),
                             pCurrNode, pNextNode, tTMO_SLL_NODE *)
        {
            TMO_SLL_Delete (&(ISS_L3FILTER_LIST), pCurrNode);

            i4RetStatus =
                MemReleaseMemBlock (ISS_L3FILTERENTRY_POOL_ID,
                                    (UINT1 *) pCurrNode);

            if (i4RetStatus != MEM_SUCCESS)
            {
                ISS_TRC (BUFFER_TRC | INIT_SHUT_TRC | ALL_FAILURE_TRC,
                         "ReleaseMemBlock L3 filter Entry Failed\n");
            }

        }                        /* End of Scan */

    }
    ISS_DELETE_L3FILTERENTRY_MEM_POOL (ISS_L3FILTERENTRY_POOL_ID);
    ISS_L3FILTERENTRY_POOL_ID = 0;

    ISS_DELETE_FILTER_SHADOW_MEM_POOL (ISS_FILTER_SHADOW_POOL_ID);
    ISS_FILTER_SHADOW_POOL_ID = 0;

    /* Do memset the global variable - TODO */
    ISS_DELETE_RESERVE_FRAME_MEM_POOL (ISS_RESERV_FRAME_CTRL_ENTRY_POOL_ID);
    ISS_RESERV_FRAME_CTRL_ENTRY_POOL_ID = 0;

    AclRedDeInitGlobalInfo ();
    ISS_TRC (INIT_SHUT_TRC, "\nSYS:Exit AclShutdown function \n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssSetDefaultRateCtrlValues                          */
/*                                                                           */
/* Description        : This function is called from IssCreatePort() to      */
/*                      initialise the Rate table entry with default values  */
/*                                                                           */
/* Input(s)           : tIssRateCtrlEntry *                                  */
/*                      u2PortIndex - port index                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IssSetDefaultRateCtrlValues (UINT2 u2PortIndex,
                             tIssRateCtrlEntry * pIssRateCtrlEntry)
{
    /* Setting the default values for the Rate Entry */
    pIssRateCtrlEntry->u4IssRateCtrlDLFLimitValue = ISS_RATE_ZEROVAL;
    pIssRateCtrlEntry->u4IssRateCtrlBCASTLimitValue = ISS_RATE_ZEROVAL;
    pIssRateCtrlEntry->u4IssRateCtrlMCASTLimitValue = ISS_RATE_ZEROVAL;
    pIssRateCtrlEntry->i4IssRateCtrlPortLimitRate = ISS_RATE_ZEROVAL;
    pIssRateCtrlEntry->i4IssRateCtrlPortBurstRate = ISS_RATE_ZEROVAL;
    pIssRateCtrlEntry->u4IssRateCtrlEnabledStatus = ISS_STORM_CONTROL_DISABLE; 
    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Entry IssSetDefaultRateCtrlValues function \n");
    if(pIssRateCtrlEntry->u4IssRateCtrlEnabledStatus == ISS_STORM_CONTROL_ENABLE)
    {
#ifdef NPAPI_WANTED
    AclRedHwAuditIncBlkCounter ();
    IssHwSetRateLimitingValue ((UINT4) u2PortIndex,
                               ISS_RATE_DLF,
                               (INT4) pIssRateCtrlEntry->
                               u4IssRateCtrlDLFLimitValue);

    IssHwSetRateLimitingValue ((UINT4) u2PortIndex,
                               ISS_RATE_BCAST,
                               (INT4) pIssRateCtrlEntry->
                               u4IssRateCtrlBCASTLimitValue);

    IssHwSetRateLimitingValue ((UINT4) u2PortIndex,
                               ISS_RATE_MCAST,
                               (INT4) pIssRateCtrlEntry->
                               u4IssRateCtrlMCASTLimitValue);

    IssHwSetPortEgressPktRate ((UINT4) u2PortIndex, pIssRateCtrlEntry->
                               i4IssRateCtrlPortLimitRate, pIssRateCtrlEntry->
                               i4IssRateCtrlPortBurstRate);
    AclRedHwAuditDecBlkCounter ();
#else
    UNUSED_PARAM (u2PortIndex);
#endif
     }
    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Exit IssSetDefaultRateCtrlValues function \n");
}

/*****************************************************************************/
/* Function Name      : AclProcessQMsgEvent                     */
/*                                                                           */
/* Description        : This function process the ACL Queue message posted in*/
/*            ACl Queue                          */
/*                                                                           */
/* Input(s)           : None                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
VOID
AclProcessQMsgEvent (VOID)
{
    tAclQMsg           *pAclQMsg = NULL;

    ISS_TRC (INIT_SHUT_TRC, "\nSYS:Entry AclProcessQMsgEvent function \n");

    while (OsixQueRecv (ISS_ACL_MSG_QUE_ID, (UINT1 *) &pAclQMsg,
                        OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
    {
        switch (pAclQMsg->u4MsgType)
        {
            case ISS_ACL_RM_MESSAGE:
                AclRedHandleRmEvents (pAclQMsg);
                break;
            default:
                ISS_TRC (ALL_FAILURE_TRC,
                         "Wrong Message Type Received in the Message Queue\n");
                break;
        }
        ISS_ACL_MSG_QUEUE_FREE_MEM_BLOCK (pAclQMsg);
    }
    ISS_TRC (INIT_SHUT_TRC, "\nSYS:Exit AclProcessQMsgEvent function \n");
    return;
}

/*****************************************************************************/
/* Function Name      : IssExCreatePortRateCtrl                              */
/*                                                                           */
/* Description        : This function allocates memory blocks for the port   */
/*                      based rate control info and sets the default values  */
/*                      for the entries.                                     */
/*                                                                           */
/* Input(s)           : u2PortIndex                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS - On success                             */
/*                      ISS_FAILURE - On failure                             */
/*****************************************************************************/
INT4
IssExCreatePortRateCtrl (UINT2 u2PortIndex, tIssTableName IssTableFlag)
{
    tIssRateCtrlEntry  *pIssRateCtrlEntry = NULL;

    ISS_TRC (INIT_SHUT_TRC, "\nSYS:Entry IssExCreatePortRateCtrl function \n");
    if ((IssTableFlag == ISS_ALL_TABLES)
        || (IssTableFlag == ISS_RATECTRL_TABLE))
    {
        /* Check if an entry for Rate ctrl index exists */
        if (gIssExGlobalInfo.apIssRateCtrlEntry[u2PortIndex] != NULL)
        {
            ISS_TRC (INIT_SHUT_TRC,
                     "Port with same Index EXISTS Another Rate Ctrl Entry with"
                     " same Index cannot be created\n");
            return ISS_SUCCESS;
        }

        /* Allocate a block for Rate Ctrl entry */
        if ((ISS_RATEENTRY_ALLOC_MEM_BLOCK (pIssRateCtrlEntry)) == NULL)
        {
            ISS_TRC (ALL_FAILURE_TRC,
                     "SYS:Memory allocated to IssRateCtrlEntry gets failed\n");
            return ISS_FAILURE;
        }
        /* Memset the Rate Ctrl entry to zero */
        ISS_MEMSET (pIssRateCtrlEntry, 0, sizeof (tIssRateCtrlEntry));
        gIssExGlobalInfo.apIssRateCtrlEntry[u2PortIndex] = pIssRateCtrlEntry;
        IssSetDefaultRateCtrlValues (u2PortIndex, pIssRateCtrlEntry);
    }
    ISS_TRC (INIT_SHUT_TRC, "\nSYS:Exit IssExCreatePortRateCtrl function \n");
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssExDeletePortRateCtrl                              */
/*                                                                           */
/* Description        : This function releases memory blocks of the port     */
/*                      based rate control info                              */
/*                                                                           */
/* Input(s)           : u2PortIndex                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS - On success                             */
/*                      ISS_FAILURE - On failure                             */
/*****************************************************************************/
INT4
IssExDeletePortRateCtrl (UINT2 u2PortIndex)
{
    tIssRateCtrlEntry  *pIssRateCtrlEntry = NULL;

    pIssRateCtrlEntry = gIssExGlobalInfo.apIssRateCtrlEntry[u2PortIndex];

    ISS_TRC (INIT_SHUT_TRC, "\nSYS:Entry IssExDeletePortRateCtrl function \n");
    /* Release the RateCtrl Mem Block to the Pool */
    if (pIssRateCtrlEntry != NULL)
    {
        if (ISS_RATEENTRY_FREE_MEM_BLOCK (pIssRateCtrlEntry) != MEM_SUCCESS)
        {
            ISS_TRC (INIT_SHUT_TRC, "ISS_RATEENTRY_FREE_MEM_BLOCK() FAILED\n");
        }
    }

    gIssExGlobalInfo.apIssRateCtrlEntry[u2PortIndex] = NULL;

    ISS_TRC (INIT_SHUT_TRC, "\nSYS:Exit IssExDeletePortRateCtrl function \n");
    return ISS_SUCCESS;
}

/****************************************************************************
* Function    :  IssGetL3FilterAddrType
* Input       :  INT4 i4IssL3FilterNo
* Output      :  None
* Returns     :  tIssL3FilterEntry * 
*****************************************************************************/
UINT1
IssGetL3FilterAddrType (UINT4 u4IssL3FilterNo, INT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    ISS_TRC (INIT_SHUT_TRC, "\nSYS:Entry IssGetL3FilterAddrType function \n");
    pIssL3FilterEntry = IssExtGetL3FilterEntry ((INT4) u4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement = (INT4) pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType;
        return SNMP_SUCCESS;
    }
    ISS_TRC (INIT_SHUT_TRC, "\nSYS:Exit IssGetL3FilterAddrType function \n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  IssSetL3FilterAddrType
 Input       :  The Indices
               i4IssL3FilterNo

                The Object
               
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IssSetL3FilterAddrType (INT4 i4IssL3FilterNo, INT4 i4AddrType)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssL3FilterNo);

    ISS_TRC (INIT_SHUT_TRC, "\nSYS:Entry IssSetL3FilterAddrType function \n");

    if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == i4AddrType)
    {
        return SNMP_SUCCESS;
    }
    if (pIssL3FilterEntry != NULL)
    {
        pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType = i4AddrType;
        return SNMP_SUCCESS;
    }
    ISS_TRC (INIT_SHUT_TRC, "\nSYS:Exit IssSetL3FilterAddrType function \n");
    return SNMP_FAILURE;
}

/**********************/
/****************************************************************************
* Function    :  IssGetL3FilterDstPrefixLength
* Input       :  INT4 i4IssL3FilterNo
* Output      :  None
* Returns     :  tIssL3FilterEntry *
*****************************************************************************/
UINT1
IssGetL3FilterDstPrefixLength (INT4 i4IssL3FilterNo, INT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Entry IssGetL3FilterDstPrefixLength function \n");
    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement =
            (INT4) pIssL3FilterEntry->
            u4IssL3FilterMultiFieldClfrDstPrefixLength;
        return SNMP_SUCCESS;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Exit IssGetL3FilterDstPrefixLength function \n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  IssSetL3FilterDstPrefixLength
 Input       :  The Indices
               i4IssL3FilterNo

                The Object

 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IssSetL3FilterDstPrefixLength (INT4 i4IssL3FilterNo, INT4 i4DstPrefixLength)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    UINT4               u4Mask;

    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Entry IssSetL3FilterDstPrefixLength function \n");
    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry->u4IssL3FilterMultiFieldClfrDstPrefixLength ==
        (UINT4) i4DstPrefixLength)
    {
        return SNMP_SUCCESS;
    }
    if (pIssL3FilterEntry != NULL)
    {
        pIssL3FilterEntry->u4IssL3FilterMultiFieldClfrDstPrefixLength =
            i4DstPrefixLength;
        IssUtlConvertPrefixToMask (i4DstPrefixLength, &u4Mask);
        pIssL3FilterEntry->u4IssL3FilterDstIpAddrMask = u4Mask;
        return SNMP_SUCCESS;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Exit IssSetL3FilterDstPrefixLength function \n");
    return SNMP_FAILURE;
}

/****************************************************************************
* Function    :  IssGetL3FilterSrcPrefixLength
* Input       :  INT4 i4IssL3FilterNo
* Output      :  None
* Returns     :  tIssL3FilterEntry *
*****************************************************************************/
UINT1
IssGetL3FilterSrcPrefixLength (INT4 i4IssL3FilterNo, INT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Entry IssGetL3FilterSrcPrefixLength function \n");
    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement =
            (INT4) pIssL3FilterEntry->
            u4IssL3FilterMultiFieldClfrSrcPrefixLength;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Exit IssGetL3FilterSrcPrefixLength function \n");
}

/****************************************************************************
 Function    :  IssSetL3FilterSrcPrefixLength
 Input       :  The Indices
               i4IssL3FilterNo

                The Object

 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IssSetL3FilterSrcPrefixLength (INT4 i4IssL3FilterNo, INT4 i4SrcPrefixLength)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    UINT4               u4Mask;

    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Entry IssSetL3FilterSrcPrefixLength function \n");
    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry->u4IssL3FilterMultiFieldClfrSrcPrefixLength ==
        (UINT4) i4SrcPrefixLength)
    {
        return ISS_SUCCESS;
    }
    if (pIssL3FilterEntry != NULL)
    {
        pIssL3FilterEntry->u4IssL3FilterMultiFieldClfrSrcPrefixLength =
            i4SrcPrefixLength;
        IssUtlConvertPrefixToMask (i4SrcPrefixLength, &u4Mask);
        pIssL3FilterEntry->u4IssL3FilterSrcIpAddrMask = u4Mask;
        return ISS_SUCCESS;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Exit IssSetL3FilterSrcPrefixLength function \n");
    return ISS_FAILURE;
}

/****************************************************************************
* Function    :  IssGetL3FilterControlFlowId
* Input       :  INT4 i4IssL3FilterNo
* Output      :  None
* Returns     :  tIssL3FilterEntry *
*****************************************************************************/
UINT1
IssGetL3FilterControlFlowId (INT4 i4IssL3FilterNo, UINT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Entry IssGetL3FilterControlFlowId  function \n");

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement = (INT4) pIssL3FilterEntry->u4IssL3MultiFieldClfrFlowId;
        return SNMP_SUCCESS;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Exit IssGetL3FilterControlFlowId  function \n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  IssSetL3FilterControlFlowId
 Input       :  The Indices
               i4IssL3FilterNo

                The Object

 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IssSetL3FilterControlFlowId (INT4 i4IssL3FilterNo, UINT4 u4FlowId)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Entry IssSetL3FilterControlFlowId function \n");

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry->u4IssL3MultiFieldClfrFlowId == u4FlowId)
    {
        return SNMP_SUCCESS;
    }
    if (pIssL3FilterEntry != NULL)
    {
        pIssL3FilterEntry->u4IssL3MultiFieldClfrFlowId = u4FlowId;
        return SNMP_SUCCESS;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Exit IssSetL3FilterControlFlowId function \n");
    return SNMP_FAILURE;
}

/****************************************************************************
* Function    :  IssGetL3FilterStorageType
* Input       :  INT4 i4IssL3FilterNo
* Output      :  None
* Returns     :  tIssL3FilterEntry *
*****************************************************************************/
UINT1
IssGetL3FilterStorageType (INT4 i4IssL3FilterNo, UINT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Entry IssGetL3FilterStorageType function \n");

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement = (INT4) pIssL3FilterEntry->i4StorageType;
        return SNMP_SUCCESS;
    }
    ISS_TRC (INIT_SHUT_TRC, "\nSYS:Exit IssGetL3FilterStorageType function \n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  IssSetL3FilterStorageType
 Input       :  The Indices
               i4IssL3FilterNo

                The Object

 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IssSetL3FilterStorageType (INT4 i4IssL3FilterNo, INT4 i4StorageType)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Entry IssSetL3FilterStorageType function \n");

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry->i4StorageType == i4StorageType)
    {
        return SNMP_SUCCESS;
    }
    if (pIssL3FilterEntry != NULL)
    {
        pIssL3FilterEntry->i4StorageType = i4StorageType;
        return SNMP_SUCCESS;
    }
    ISS_TRC (INIT_SHUT_TRC, "\nSYS:Exit IssSetL3FilterStorageType function \n");
    return SNMP_FAILURE;
}

/*****************************************************************************/
/* Function Name      : IssUtlConvertPrefixToMask                             */
/*                                                                           */
/* Description        : This function converts the give prefix to Mssk       */
/*                                                                           */
/* Input(s)           : u4Prefix,: Prefix                                    */
/*                                                                           */
/* Output(s)           : u4Mask : Mask                                       */
/*                                                                           */
/* Return Value(s)    :                              */
/*****************************************************************************/
VOID
IssUtlConvertPrefixToMask (UINT4 u4Prefix, UINT4 *pu4Mask)
{
    INT4                i4Count = 0;

    *pu4Mask = 0;
    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Entry IssUtlConvertPrefixToMask function \n");

    for (i4Count = 0; i4Count < (INT4) u4Prefix; i4Count++)
    {
        *pu4Mask = *pu4Mask >> 1;
        *pu4Mask |= 0x80000000;
    }
    ISS_TRC (INIT_SHUT_TRC, "\nSYS:Exit IssUtlConvertPrefixToMask function \n");
}

/*****************************************************************************/
/* Function Name      : IssUtlConvertMaskToPrefix                             */
/*                                                                           */
/* Description        : This function converts the give prefix to Mssk       */
/*                                                                           */
/* Input(s)           : u4Prefix,: Prefix                                    */
/*                                                                           */
/* Output(s)           : u4Mask : Mask                                       */
/*                                                                           */
/* Return Value(s)    :                              */
/*****************************************************************************/
VOID
IssUtlConvertMaskToPrefix (UINT4 u4Mask, UINT4 *pu4Prefix)
{
    UINT4               u4Count = 0;

    *pu4Prefix = 0;
    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Entry IssUtlConvertMaskToPrefix  function \n");

    while (u4Mask)
    {
        u4Count++;
        u4Mask = u4Mask << 1;
    }

    *pu4Prefix = u4Count;
    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Exit IssUtlConvertMaskToPrefix  function \n");
}

/**************************************************************************/
/* Function Name       : IssUpdatePortLinkStatus                          */
/*                                                                        */
/* Description         : This function Updates the port link status for   */
/*                       recalculation of hashing logic                   */
/*                                                                        */
/* Input(s)            : LocalPortList - Local port list                  */
/*                                                                        */
/* Output(s)           : pu4IfPortArray, u1NumPorts                       */
/*                                                                        */
/* Global Variables                                                       */
/* Referred            : None                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Modified            : None                                             */
/*                                                                        */
/* Exceptions or OS                                                       */
/* Error Handling      : None                                             */
/*                                                                        */
/* Use of Recursion    : None                                             */
/*                                                                        */
/* Returns             : None                                             */
/*                                                                        */
/**************************************************************************/
VOID
IssUpdatePortLinkStatus (UINT4 u4IfIndex, UINT1 u1IfType, UINT1 u1OperStatus)
{
    ISS_TRC (INIT_SHUT_TRC, "\nSYS:Entry IssUpdatePortLinkStatus  function \n");
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1IfType);
    UNUSED_PARAM (u1OperStatus);
    ISS_TRC (INIT_SHUT_TRC, "\nSYS:Exit IssUpdatePortLinkStatus  function \n");
    return;
}

INT4
IssExPrgAclsToNpWithPriority (VOID)
{
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssACLCreateFilter                                   */
/*                                                                           */
/* Description        : This function is called from the other modules       */
/*                      to create L2, L3 and user defined filter.            */
/*                                                                           */
/* Input(s)           : pAclFilterInfo - Pointer to ACL filter information   */
/*                                                                           */
/* Output(s)          : pu4L2FilterId  - Pointer to L2 filter ID             */
/*                      pu4L3FilterId  - Pointer to L3 filter ID             */
/*                      pu4UDBFilterId - Pointer to User defined filter ID   */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
IssACLCreateFilter (tAclFilterInfo * pAclFilterInfo, UINT4 *pu4L2FilterId,
                    UINT4 *pu4L3FilterId, UINT4 *pu4UDBFilterId)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    UINT4               u4L2FilterId = ISS_ZERO_ENTRY;
    UINT4               u4L3FilterId = ISS_ZERO_ENTRY;
    UINT4               u4UDBFilterId = ISS_ZERO_ENTRY;

    *pu4L2FilterId = ISS_ZERO_ENTRY;
    *pu4L3FilterId = ISS_ZERO_ENTRY;
    *pu4UDBFilterId = ISS_ZERO_ENTRY;
    INT4                i4RetVal = ISS_FAILURE;
    ISS_TRC (INIT_SHUT_TRC, "\nSYS:Entry IssACLCreateFilter function \n");
    ISS_LOCK ();
    if (pAclFilterInfo->u1FilterType == ISS_L3FILTER)
    {
        /* Get a valid L3 filter ID by scanning through the
         *          * L3 filter table */
        IssAclGetValidL3FilterId (&u4L3FilterId);

        /* Update the L3 filter Entry from the pAclFilterInfo and install
         *          * the L3 filter entry */
        if (IssAclUpdateL3FilterEntry (pAclFilterInfo, u4L3FilterId)
            == ISS_FAILURE)
        {
            ISS_UNLOCK ();
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "SYS:Failed to update  L3 FilterId %d \n",
                          u4L3FilterId);
            return ISS_FAILURE;
        }

        *pu4L3FilterId = u4L3FilterId;
    }

    if (pAclFilterInfo->u1FilterType == ISS_UDBFILTER)
    {

        /* Get a valid L2 filter ID by scanning through the
         *          * L2 filter table */
        IssAclGetValidL2FilterId (&u4L2FilterId);

        /* Update the L2 filter Entry from the pAclFilterInfo and install
         *          * the L2 filter entry */
        if (IssAclUpdateL2FilterEntry (pAclFilterInfo, u4L2FilterId)
            == ISS_FAILURE)
        {
            ISS_UNLOCK ();
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "SYS:Failed to update  L2 FilterId %d\n",
                          u4L2FilterId);
            return ISS_FAILURE;
        }

        /* Get a valid L3 filter ID by scanning through the
         *          * L3 filter table */
        IssAclGetValidL3FilterId (&u4L3FilterId);

        /* Update the L3 filter Entry from the pAclFilterInfo and install
         *          * the L3 filter entry */
        if (IssAclUpdateL3FilterEntry (pAclFilterInfo, u4L3FilterId)
            == ISS_FAILURE)
        {
            /*Uninstall the already created L2Filter entry as installation of
             *              * L3 filter entry fails */
            if ((pIssL2FilterEntry = IssExtGetL2FilterEntry (u4L2FilterId))
                != NULL)
            {
                IssAclUnInstallL2Filter (pIssL2FilterEntry);
            }
            ISS_UNLOCK ();
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "SYS:Failed to update  L3 FilterId %d\n",
                          u4L3FilterId);

            return ISS_FAILURE;
        }

        /* Get a valid User-defined filter ID by scanning through the
         *          * user-defined filter table */
        IssAclGetValidUDBFilterId (&u4UDBFilterId);

/* Update the user-defined filter Entry from the pAclFilterInfo and
           * install the user-defined filter entry */
        i4RetVal = IssAclUpdateUDBFilterEntry (pAclFilterInfo, u4L2FilterId,
                                               u4L3FilterId, u4UDBFilterId);

        /*  Uninstall the already created L2 & L3 Filter entry */
        if ((pIssL2FilterEntry = IssExtGetL2FilterEntry (u4L2FilterId)) != NULL)
        {
            IssAclUnInstallL2Filter (pIssL2FilterEntry);
        }
        if ((pIssL3FilterEntry = IssExtGetL3FilterEntry (u4L3FilterId)) != NULL)
        {
            IssAclUnInstallL3Filter (pIssL3FilterEntry);
        }
        if (i4RetVal == ISS_FAILURE)
        {
            ISS_UNLOCK ();
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "SYS:Failed to Uninstall the already created L2FilterId  %d and L3FilterId %d\n",
                          u4L2FilterId, u4L3FilterId);
            return ISS_FAILURE;
        }
        /* If type is UDF Filter,L2 and L3 Filter ID will be set to Zero */
        *pu4UDBFilterId = u4UDBFilterId;
    }
    ISS_UNLOCK ();
    ISS_TRC (INIT_SHUT_TRC, "\nSYS:Exit IssACLCreateFilter function \n");
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssACLDeleteFilter                                   */
/*                                                                           */
/* Description        : This function is called from the other Modules       */
/*                      to delete L2, L3 and user defined filter.            */
/*                                                                           */
/* Input(s)           : u1FilterType - Filter Type (L2/L3/User-defined)      */
/*                      u4L2FilterId - L2 filter ID                          */
/*                      u4L3FilterId - L3 filter ID                          */
/*                      u4UserDefFilterId - User defined filter ID           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
IssACLDeleteFilter (UINT1 u1FilterType, UINT4 u4L2FilterId,
                    UINT4 u4L3FilterId, UINT4 u4UserDefFilterId)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    tIssUserDefinedFilterTable *pIssUserDefFilterEntry = NULL;
    UNUSED_PARAM (u4L2FilterId);
    ISS_TRC (INIT_SHUT_TRC, "\nSYS:Entry IssACLDeleteFilter function \n");
    ISS_LOCK ();

    if (u1FilterType == ISS_L3FILTER)
    {
        /* Delete the L3 filter entry if the entry exists */
        if ((pIssL3FilterEntry = IssExtGetL3FilterEntry (u4L3FilterId)) != NULL)
        {
            IssAclUnInstallL3Filter (pIssL3FilterEntry);
        }
    }

    if (u1FilterType == ISS_UDBFILTER)
    {
        if ((pIssUserDefFilterEntry =
             IssExtGetUdbFilterTableEntry ((INT4) u4UserDefFilterId)) != NULL)
        {
            IssAclUnInstallUserDefFilter (pIssUserDefFilterEntry);
        }
    }

    ISS_UNLOCK ();
    ISS_TRC (INIT_SHUT_TRC, "\nSYS:Exit IssACLDeleteFilter function \n");
    return;
}

/*****************************************************************************/
/* Function Name      : IssACLApiValidateL3AclConfigFlag                     */
/*                                                                           */
/* Description        : This function is called from msrval for validating   */
/*                      L3 Acl Config Flag (which is set based on            */
/*                      configuration) done through ACL or other protocol.   */
/*                                                                           */
/* Input(s)           : i4L3AclFilterId - filter id                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
IssACLApiValidateL3AclConfigFlag (INT4 i4L3AclFilterId, 
                                BOOL1 *pb1IsL3AclConfFromExt)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4L3AclFilterId);
    if (pIssL3FilterEntry != NULL)
    {
        *pb1IsL3AclConfFromExt = pIssL3FilterEntry->b1IsL3AclConfFromExt;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : IssACLApiValidateL2AclConfigFlag                     */
/*                                                                           */
/* Description        : This function is called from msrval for validating   */
/*                      L2 Acl Config Flag (which is set based on            */
/*                      configuration) done through ACL or other protocol    */
/*                      a protocol.                                          */
/*                                                                           */
/* Input(s)           : i4L2AclFilterId - filter id                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/

VOID
IssACLApiValidateL2AclConfigFlag (INT4 i4L2AclFilterId, 
                            BOOL1 *pb1IsL2AclConfFromExt)
 {
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    pIssL2FilterEntry = IssExtGetL2FilterEntry (i4L2AclFilterId);
    if (pIssL2FilterEntry != NULL)
    {
        *pb1IsL2AclConfFromExt = pIssL2FilterEntry->b1IsL2AclConfFromExt;
    }

    return;
}

/*****************************************************************************/
/* Function Name      : IssACLApiGetFilterEntry                              */
/*                                                                           */
/* Description        : This function is called from the DCBx Application    */
/*                      priority module to get the FilterId for              */
/*                      a protocol.                                          */
/*                                                                           */
/* Input(s)           :                                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
IssACLApiGetFilterEntry (tIssAclHwFilterInfo *pAppPriAclInfo, 
        UINT1 u1FilterType, UINT1 u1Status)
{
    tIssSllNode        *pL2SllNode = NULL;
    tIssSllNode        *pL3SllNode = NULL;
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
    tIssPortList        IssL2FilterInPortList;
    tIssPortList        IssL3FilterInPortList;
    INT1                i1Result = OSIX_FALSE;
    tIssPortList        PortList;

    MEMSET (&PortList,         0, sizeof (tIssPortList));
    if (u1FilterType == ISS_L2_FILTER)
    {
        pL2SllNode = ISS_SLL_FIRST (&ISS_L2FILTER_LIST);
        while (pL2SllNode != NULL)
        {
            pIssAclL2FilterEntry = (tIssL2FilterEntry *) pL2SllNode;
            if (pIssAclL2FilterEntry == NULL)
            {                    
                ISS_TRC_ARG1 (MGMT_TRC, "In %s: "
                        "returned Failure due to no entry "
                        "exists.\r\n", __FUNCTION__);
                return ISS_FAILURE;
            }

            if ((pAppPriAclInfo->u4Protocol == (UINT4)
                        pIssAclL2FilterEntry->u4IssL2FilterProtocolType) && 
                    (pAppPriAclInfo->u1Priority == (UINT1)
                     pIssAclL2FilterEntry->i4IssL2FilterPriority))
            {
                /* Check whether the filter is present on this port */
                OSIX_BITLIST_IS_BIT_SET
                    (pIssAclL2FilterEntry->IssL2FilterInPortList,
                     pAppPriAclInfo->u4PortNo, sizeof (tIssPortList),
                     i1Result);

                if (i1Result == OSIX_TRUE)
                {
                    /* Update the FilterId */
                    pAppPriAclInfo->u4FilterId = (UINT4)
                        pIssAclL2FilterEntry->i4IssL2FilterNo;

                    /* Copy port list to local variable, then unset the bit in the list and compare */
                    MEMCPY (IssL2FilterInPortList,
                            pIssAclL2FilterEntry->IssL2FilterInPortList,
                            sizeof(tIssPortList));

                    OSIX_BITLIST_RESET_BIT (IssL2FilterInPortList,
                            pAppPriAclInfo->u4PortNo,
                            sizeof (tIssPortList));

                    /* If no other port is using this filter, un-install this filter */
                    if ((u1Status == FALSE) && (MEMCMP
                                (IssL2FilterInPortList,
                                 PortList, sizeof (tIssPortList)) == 0))
                    {
                        pAppPriAclInfo->u1FilterAction = ISS_FILTER_DELETE;
                    }
                    else
                    {
                        pAppPriAclInfo->u1FilterAction = ISS_FILTER_UPDATE; 
                    }
                    return ISS_SUCCESS;
                }
            }
            pL2SllNode = ISS_SLL_NEXT (&ISS_L2FILTER_LIST, pL2SllNode);
        }

        /* No entry present */                
        ISS_TRC_ARG1 (MGMT_TRC, "In %s: "        
                "returned Failure - No Entry Present\r\n", __FUNCTION__);
        return ISS_FAILURE;
    }
    else if(u1FilterType == ISS_L3_FILTER)
    {
        pL3SllNode = ISS_SLL_FIRST (&ISS_L3FILTER_LIST);
        while (pL3SllNode != NULL)
        {
            pIssAclL3FilterEntry = (tIssL3FilterEntry *) pL3SllNode;
            if (pIssAclL3FilterEntry == NULL)
            {
                ISS_TRC_ARG1 (MGMT_TRC, "In %s: "
                        "returned Failure\r\n", __FUNCTION__);
                return ISS_FAILURE;
            }
            if ((pAppPriAclInfo->u4Protocol ==  (UINT4)
                        pIssAclL3FilterEntry->IssL3FilterProtocol)  &&                                        
                    (pAppPriAclInfo->u4PortNo ==
                     pIssAclL3FilterEntry->u4IssL3FilterMinDstProtPort) &&
                    (pAppPriAclInfo->u4PortNo ==
                     pIssAclL3FilterEntry->u4IssL3FilterMaxDstProtPort) && 
                    (pAppPriAclInfo->u1Priority ==
                            pIssAclL3FilterEntry->i4IssL3FilterPriority))
            {
                /* Check whether the filter is present on this port */
                OSIX_BITLIST_IS_BIT_SET (pIssAclL3FilterEntry->
                        IssL3FilterInPortList,
                        pAppPriAclInfo->u4L3PortNo,
                        sizeof (tIssPortList), i1Result);

                if (i1Result == OSIX_TRUE)
                {
                    /* Update the FilterId */
                    pAppPriAclInfo->u4FilterId = 
                        (UINT4) pIssAclL3FilterEntry->i4IssL3FilterNo;

                    /* Copy port list to local variable, then unset the bit in the list and compare */
                    MEMCPY (IssL3FilterInPortList,
                            pIssAclL3FilterEntry->IssL3FilterInPortList,
                            sizeof(tIssPortList));

                    OSIX_BITLIST_RESET_BIT (IssL3FilterInPortList,
                            pAppPriAclInfo->u4L3PortNo,
                            sizeof (tIssPortList));

                    /* If no other port is using this filter, un-install this filter */
                    if ((u1Status == FALSE) && (MEMCMP
                                (IssL3FilterInPortList,
                                 PortList, sizeof (tIssPortList)) == 0))
                    {
                        pAppPriAclInfo->u1FilterAction = ISS_FILTER_DELETE;
                    }
                    else
                    {
                        pAppPriAclInfo->u1FilterAction = ISS_FILTER_UPDATE; 
                    }
                    return ISS_SUCCESS;
                }
            }
            pL3SllNode = ISS_SLL_NEXT (&ISS_L3FILTER_LIST, pL3SllNode);
        }
        /* No entry present */        
        ISS_TRC_ARG1 (MGMT_TRC, "In %s: "        
                "returned Failure - No Entry Present\r\n", __FUNCTION__);
        return ISS_FAILURE;
    }
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssACLApiModifyFilterEntry                           */
/*                                                                           */
/* Description        : This function is called from the DCBx Application    */
/*                      priority module to configure the priority for        */
/*                      a protocol.                                          */
/*                                                                           */
/* Input(s)           : pAppPriAclInfo - Pointer to the ACL filter info      */
/*                      u1FilterType - Filter for L2/L3                      */
/*                      u1Status - Update/Delete                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
IssACLApiModifyFilterEntry (tIssAclHwFilterInfo * pAppPriAclInfo,
                            UINT1 u1FilterType, UINT1 u1Status)
{
    tIssSllNode        *pL2SllNode = NULL;
    tIssSllNode        *pL3SllNode = NULL;
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    tIssL2FilterEntry   IssL2FilterEntry;
    tIssL3FilterEntry   IssL3FilterEntry;
    tIssPortList        PortList;
    tAclFilterInfo      AclFilterInfo;
    UINT4               u4L2FilterId = 0;
    UINT4               u4L3FilterId = 0;
    BOOL1               b1MatchFound = FALSE;
    INT1                i1Result = OSIX_FALSE;

    ISS_TRC (INIT_SHUT_TRC,
            "\nSYS:Entry IssACLApiModifyFilterEntry function \n");

    MEMSET (&IssL2FilterEntry, 0, sizeof (tIssL2FilterEntry));
    MEMSET (&IssL3FilterEntry, 0, sizeof (tIssL3FilterEntry));
    MEMSET (&AclFilterInfo,    0, sizeof (tAclFilterInfo));
    MEMSET (&PortList,         0, sizeof (tIssPortList));

    if (u1FilterType == ISS_L2_FILTER)
    {
        /* Scan from head node (&ISS_L2FILTER_LIST) */
        pL2SllNode = ISS_SLL_FIRST (&ISS_L2FILTER_LIST);
        while (pL2SllNode != NULL)
        {
            pIssL2FilterEntry = (tIssL2FilterEntry *) pL2SllNode;
            if (pIssL2FilterEntry == NULL)
            {                    
                /* If no entry present, create */
                b1MatchFound = FALSE;
                break;
            }

            /* Only one unique protocol entry will exist, match the protocol. */
            if (pAppPriAclInfo->u4Protocol == 
                    pIssL2FilterEntry->u4IssL2FilterProtocolType)
            {
                /* Check whether the filter is present on this port */
                OSIX_BITLIST_IS_BIT_SET (pIssL2FilterEntry->
                        IssL2FilterInPortList,
                        pAppPriAclInfo->u4PortNo,
                        sizeof (tIssPortList), i1Result);
                if (i1Result == OSIX_TRUE)
                {
                    /* Through dcbx application priority max priority (255) will set always */
                    /* Match the priority */
                    if (pAppPriAclInfo->u1Priority == 
                            pIssL2FilterEntry->i4IssL2FilterPriority)
                    {
                        b1MatchFound = TRUE;
                        break;
                    }
                }
            }
            pL2SllNode = ISS_SLL_NEXT (&ISS_L2FILTER_LIST, pL2SllNode);
        } /* end of while */


        if (b1MatchFound == TRUE)
        {
            /* Delete the filter */
            if (u1Status == FALSE)
            {
                ISS_TRC_ARG2 (MGMT_TRC, "In %s:UnInstall L2Filter on port[%d]\r\n",
                        __FUNCTION__, pAppPriAclInfo->u4PortNo);

                /* Check the Filter to be deleted present on this port */
                OSIX_BITLIST_IS_BIT_SET (pIssL2FilterEntry->
                        IssL2FilterInPortList,
                        pAppPriAclInfo->u4PortNo,
                        sizeof (tIssPortList), i1Result);
                if (i1Result == OSIX_FALSE)
                {
                    /* If Filter is not present on this port, return failure */
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC, "In %s:L2Filter not present "
                            "on port[%d]\r\n", __func__, pAppPriAclInfo->u4PortNo);
                    return ISS_FAILURE;
                }

                /* Checking the Filter is associated in any ports prior 
                 * uninstalling the L2 filter */
                MEMSET (&IssL2FilterEntry, 0, sizeof (tIssL2FilterEntry));
                MEMCPY (&IssL2FilterEntry, pIssL2FilterEntry, sizeof (tIssL2FilterEntry));

                OSIX_BITLIST_RESET_BIT (IssL2FilterEntry.IssL2FilterInPortList,
                        pAppPriAclInfo->u4PortNo,
                        sizeof (tIssPortList));

                /* If no other port is using this filter, un-install this filter */
                if (MEMCMP (IssL2FilterEntry.IssL2FilterInPortList, PortList,
                            sizeof (tIssPortList)) == 0)
                {
                    /* Update dcbx AppPri info. Used to delete QOS related */
                    pAppPriAclInfo->u1FilterAction = ISS_FILTER_DELETE;
                    pAppPriAclInfo->u4FilterId =
                        (UINT4) pIssL2FilterEntry->i4IssL2FilterNo;

                    if (IssAclUnInstallL2Filter (pIssL2FilterEntry) ==
                            ISS_FAILURE)
                    {
                        ISS_TRC_ARG1 (MGMT_TRC,
                                "In %s: IssAclUnInstallL2Filter "
                                "returned Failure\r\n", __FUNCTION__);
                        return ISS_FAILURE;
                    }
                }
                ISS_TRC_ARG2 (MGMT_TRC,
                        "In %s: IssAclUnInstallL2Filter "
                        "Successfully deleted the filter on port [%d]\r\n", 
                        __FUNCTION__, pAppPriAclInfo->u4PortNo);

                return ISS_SUCCESS;
            }
            else /* Update the filter */
            {
                /* From Application priority, as we loop from head to end, there is
                 * a chance that the same protocol will be passed to install the
                 * filter. So, in that case, do nothing and return failure. 
                 * Note: Failure is stop re-programming the HW for QOS. */
                ISS_TRC_ARG4 (MGMT_TRC, "In %s: Priorities are same, return.\n"
                        "pAppPriAclInfo->u1Priority = %d\n"
                        "pIssL2FilterEntry->i4IssL2FilterPriority = %d "
                        "on port [%d]\r\n", __FUNCTION__,
                        pAppPriAclInfo->u1Priority,
                        pIssL2FilterEntry->i4IssL2FilterPriority,
                        pAppPriAclInfo->u4PortNo); 
                return ISS_FAILURE;
            }
        }/* end of if -  Match found */
        else
        {
            /* If match not found and trying to delete. Return failure */
            if (u1Status == FALSE)
            {
                ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                        "In %s : L2Filter not found for protocol[%d] "
                        "on port[%d]\r\n",
                        __func__, pAppPriAclInfo->u4Protocol,
                        pAppPriAclInfo->u4PortNo);
                return ISS_FAILURE;
            }

            /* Create a new Filter entry */

            /* Get a valid L2 filter ID by scanning through the
             * L2 filter table */
            IssAclGetValidL2FilterId (&u4L2FilterId);
            pAppPriAclInfo->u4FilterId = u4L2FilterId;
            pAppPriAclInfo->u1FilterAction = ISS_FILTER_CREATE;
            IssL2FilterEntry.i4IssL2FilterNo = (INT4) u4L2FilterId;
            IssL2FilterEntry.u4IssL2FilterProtocolType =
                pAppPriAclInfo->u4Protocol;
            IssL2FilterEntry.u4RefCount = ISS_ZERO_ENTRY;
            IssL2FilterEntry.IssL2FilterAction = ISS_L2FILTER_ADD;
            IssL2FilterEntry.u1FilterDirection = ISS_DIRECTION_IN;
            IssL2FilterEntry.u1IssL2FilterStatus = ISS_NOT_READY;
            IssL2FilterEntry.u1IssL2FilterTagType = pAppPriAclInfo->u1TagType;
            IssL2FilterEntry.i1IssL2FilterCVlanPriority = ISS_DEF_CVLAN_PRIO;
            IssL2FilterEntry.i1IssL2FilterSVlanPriority = ISS_DEF_SVLAN_PRIO;
            IssL2FilterEntry.u4IssL2FilterCustomerVlanId = ISS_ZERO_ENTRY;
            IssL2FilterEntry.u4IssL2FilterServiceVlanId = ISS_ZERO_ENTRY;
            IssL2FilterEntry.u2InnerEtherType = ISS_ZERO_ENTRY;
            IssL2FilterEntry.u2OuterEtherType = ISS_ZERO_ENTRY;
            IssL2FilterEntry.i4IssL2FilterPriority = pAppPriAclInfo->u1Priority;

            OSIX_BITLIST_SET_BIT (IssL2FilterEntry.IssL2FilterInPortList,
                    pAppPriAclInfo->u4PortNo,
                    sizeof (tIssPortList));

            ISS_TRC_ARG6 (MGMT_TRC, "In %s : creating a filter with "
                    "FilterId = %d L2FilterNo = %d "
                    "L2FilterProtocolType = %d Direction = %d "
                    "L2FilterPriority = %d\r\n", __FUNCTION__, 
                    pAppPriAclInfo->u4FilterId, IssL2FilterEntry.i4IssL2FilterNo,
                    IssL2FilterEntry.u4IssL2FilterProtocolType,
                    IssL2FilterEntry.u1FilterDirection,
                    IssL2FilterEntry.i4IssL2FilterPriority);

            if (IssAclInstallL2Filter (&IssL2FilterEntry) == ISS_FAILURE)
            {
                ISS_TRC_ARG1 (MGMT_TRC, "In %s : IssAclInstallL2Filter "
                        "returned Failure\r\n", __FUNCTION__);
                return ISS_FAILURE;
            }
            pIssL2FilterEntry = IssExtGetL2FilterEntry ((INT4)u4L2FilterId);
            if (pIssL2FilterEntry != NULL)
            {
                pIssL2FilterEntry->b1IsL2AclConfFromExt = TRUE;
            }
        } /* end of else - Match not found - Create */
    } /* end of if - L2 FILTER */
    else if(u1FilterType == ISS_L3_FILTER)
    {
        /* Scan from head node (&ISS_L3FILTER_LIST) */
        pL3SllNode = ISS_SLL_FIRST (&ISS_L3FILTER_LIST);
        while (pL3SllNode != NULL)
        {
            pIssL3FilterEntry = (tIssL3FilterEntry *) pL3SllNode;
            if (pIssL3FilterEntry == NULL)
            {
                /* If no entry exists, create filter */
                b1MatchFound = FALSE;
                break;
            }

            /* Match the exact protocol and destination port */
            if ((pAppPriAclInfo->u4Protocol ==  (UINT4)
                        pIssL3FilterEntry->IssL3FilterProtocol)  &&                                        
                    (pAppPriAclInfo->u4PortNo ==
                     pIssL3FilterEntry->u4IssL3FilterMinDstProtPort) &&
                    (pAppPriAclInfo->u4PortNo ==
                     pIssL3FilterEntry->u4IssL3FilterMaxDstProtPort))
            {
                /* Check whether the filter is present on this port */
                OSIX_BITLIST_IS_BIT_SET (pIssL3FilterEntry->
                        IssL3FilterInPortList,
                        pAppPriAclInfo->u4L3PortNo,
                        sizeof (tIssPortList), i1Result);
                if (i1Result == OSIX_TRUE)
                {
                    /* Match the priority */
                    if (pAppPriAclInfo->u1Priority ==
                            pIssL3FilterEntry->i4IssL3FilterPriority)
                    {
                        b1MatchFound = TRUE;
                        break;
                    }
                }
            }
            pL3SllNode = ISS_SLL_NEXT (&ISS_L3FILTER_LIST, pL3SllNode);
        } /* end of while */

        if (b1MatchFound == TRUE)
        {
            /* Delete the filter */
            if (u1Status == FALSE)
            {
                /* Check the Filter is associated in any ports prior 
                 * uninstalling the L3 filter */
                MEMSET (&IssL3FilterEntry, 0, sizeof (tIssL3FilterEntry));
                MEMCPY (&IssL3FilterEntry, pIssL3FilterEntry, sizeof (tIssL3FilterEntry));

                OSIX_BITLIST_RESET_BIT (IssL3FilterEntry.IssL3FilterInPortList,
                        pAppPriAclInfo->u4L3PortNo,
                        sizeof (tIssPortList));

                /* If no other port is using this filter entry, delete the filter */
                if (MEMCMP (IssL3FilterEntry.IssL3FilterInPortList, PortList,
                            sizeof (tIssPortList)) == 0)
                {
                    /* Populate AppPri info, will be used by QOS post
                     * successful return */
                    pAppPriAclInfo->u1FilterAction = ISS_FILTER_DELETE;
                    pAppPriAclInfo->u4FilterId =
                        (UINT4) pIssL3FilterEntry->i4IssL3FilterNo;

                    if (IssAclUnInstallL3Filter (pIssL3FilterEntry) ==
                            ISS_FAILURE)
                    {
                        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                "In %s : IssAclUnInstallL3Filter "
                                "returned Failure for port [%d]\r\n", 
                                __FUNCTION__, pAppPriAclInfo->u4L3PortNo);
                        return ISS_FAILURE;
                    }
                }
            } /* end of if - delete filter */
            else /* Update the filter */
            {
                /* From Application priority, as we loop from head to end, there is
                 * a chance that the same protocol will be passed to install the
                 * filter. So, in that case, do nothing and return failure. 
                 * Note: Failure is to stop re-programming the HW for QOS. */

                ISS_TRC_ARG4 (MGMT_TRC, "In %s: Priorities are same, return.\n"
                        "pAppPriAclInfo->u1Priority = %d\n"
                        "pIssL3FilterEntry->i4IssL3FilterPriority = %d "
                        "on port [%d]\r\n", __func__, pAppPriAclInfo->u1Priority,
                        pIssL3FilterEntry->i4IssL3FilterPriority,
                        pAppPriAclInfo->u4L3PortNo);
                return ISS_FAILURE;
            }
        } /* end of MATCH FOUND  */
        else /* install filter */ 
        {
            /* If match not found and trying to delete. Return failure */
            if (u1Status == FALSE)
            {
                ISS_TRC_ARG3 (ALL_FAILURE_TRC, "In %s : L3Filter "
                        "not found for protocol [%d] on port[%d]\r\n", 
                        __FUNCTION__, pAppPriAclInfo->u4Protocol,
                        pAppPriAclInfo->u4L3PortNo);
                return ISS_FAILURE;
            }

            /* Create the L3 Filter */

            /* Get Free FilterId */
            IssAclGetValidL3FilterId (&u4L3FilterId);

            /* Populate the AppPri info, will be used for QOS */
            pAppPriAclInfo->u4FilterId = u4L3FilterId;
            pAppPriAclInfo->u1FilterAction = ISS_FILTER_CREATE;

            IssL3FilterEntry.i4IssL3FilterNo = (INT4) u4L3FilterId;
            IssL3FilterEntry.IssL3FilterProtocol = pAppPriAclInfo->u4Protocol;
            IssL3FilterEntry.u4IssL3FilterMinDstProtPort =
                pAppPriAclInfo->u4PortNo;
            IssL3FilterEntry.u4IssL3FilterMaxDstProtPort =
                pAppPriAclInfo->u4PortNo;
            IssL3FilterEntry.i4IssL3FilterPriority = pAppPriAclInfo->u1Priority;
            IssL3FilterEntry.IssL3FilterAckBit = ISS_ACK_ANY;
            IssL3FilterEntry.IssL3FilterRstBit = ISS_RST_ANY;
            IssL3FilterEntry.IssL3FilterTos = ISS_TOS_INVALID;
            IssL3FilterEntry.i4IssL3FilterDscp = ISS_DSCP_INVALID;
            IssL3FilterEntry.IssL3FilterDirection = ISS_DIRECTION_IN;
            IssL3FilterEntry.u4IssL3FilterMultiFieldClfrDstPrefixLength =
                ISS_ZERO_ENTRY;
            IssL3FilterEntry.u4IssL3FilterMultiFieldClfrSrcPrefixLength =
                ISS_ZERO_ENTRY;
            IssL3FilterEntry.u4IssL3MultiFieldClfrFlowId = ISS_ZERO_ENTRY;
            IssL3FilterEntry.i4IssL3MultiFieldClfrAddrType = QOS_IPV4;
            IssL3FilterEntry.i4StorageType = ISS_ZERO_ENTRY;
            IssL3FilterEntry.i1CVlanPriority = ISS_DEFAULT_VLAN_PRIORITY;
            IssL3FilterEntry.i1SVlanPriority = ISS_DEFAULT_VLAN_PRIORITY;
            IssL3FilterEntry.u1IssL3FilterTagType = ISS_FILTER_SINGLE_TAG;
            IssL3FilterEntry.u1IssL3FilterStatus = NOT_READY;
            IssL3FilterEntry.u1IssL3FilterCreationMode = ISS_ACL_CREATION_INTERNAL;
            IssL3FilterEntry.IssL3FilterAction = ISS_ALLOW;
            IssL3FilterEntry.u4IssL3FilterMaxSrcProtPort = ISS_MAX_PORT_VALUE;
            IssL3FilterEntry.i4IssL3FilterMessageType = ISS_DEFAULT_MSG_TYPE;
            IssL3FilterEntry.i4IssL3FilterMessageCode = ISS_DEFAULT_MSG_CODE;

            OSIX_BITLIST_SET_BIT (IssL3FilterEntry.IssL3FilterInPortList,
                    pAppPriAclInfo->u4L3PortNo,
                    sizeof (tIssPortList));

            ISS_TRC_ARG6 (MGMT_TRC, "In %s : creating a filter with "
                    "FilterId = %d L3FilterNo = %d "
                    "L3FilterProtocol = %d Direction = %d "
                    "L3FilterPriority = %d\r\n",
                    __FUNCTION__, pAppPriAclInfo->u4FilterId,
                    IssL3FilterEntry.i4IssL3FilterNo,
                    IssL3FilterEntry.IssL3FilterProtocol,
                    IssL3FilterEntry.IssL3FilterDirection,
                    IssL3FilterEntry.i4IssL3FilterPriority);

            if (IssAclInstallL3Filter (&IssL3FilterEntry) == ISS_FAILURE)
            {
                ISS_TRC_ARG1 (MGMT_TRC, "In %s : IssAclInstallL3Filter "
                        "returned Failure\r\n", __FUNCTION__);

                return ISS_FAILURE;
            }
            pIssL3FilterEntry = IssExtGetL3FilterEntry ((INT4)u4L3FilterId);
            if (pIssL3FilterEntry != NULL)
            {
                pIssL3FilterEntry->b1IsL3AclConfFromExt = TRUE;
            }
        } /* end of else - Install filter */
    } /* end of ISS_L3_FILTER */

    ISS_TRC (INIT_SHUT_TRC,
            "\nSYS:Exit IssACLApiModifyFilterEntry function \n");
    return ISS_SUCCESS;
}

/***************************************************************************
 *                                                                            
 *   Function Name : IssExDeleteAclPriorityFilterTable                    *
 *                                                                              
 *   Description   : The function is used to delete filter from           *
 *                            priority table                                      
 *                                                                              
 *   Input(s)      : i4Priority,pFilterNode,u1FilterType                  *
 *   
 *   Output(s)     : NULL                                                 *
 *                                                                                     
 *   Returns       :  NONE                                                *
 *****************************************************************************/

VOID
IssExDeleteAclPriorityFilterTable (INT4 i4Priority, tTMO_SLL_NODE * pFilterNode,
                                   UINT1 u1FilterType)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Entry IssExDeleteAclPriorityFilterTable function \n");
    gIssExGlobalInfo.u4PriorityTableSet = ISS_TRUE;
    i4Priority = i4Priority - 1;
    switch (u1FilterType)
    {
        case ISS_L2FILTER:
        case ISS_L2_REDIRECT:

            TMO_DLL_Delete (&(gaPriorityTable[i4Priority]->
                              PriFilterList),
                            &((tIssL2FilterEntry *)
                              pFilterNode)->PriorityNode.PriNode);
            break;

        case ISS_L3FILTER:
        case ISS_L3_REDIRECT:

            TMO_DLL_Delete (&(gaPriorityTable[i4Priority]->
                              PriFilterList),
                            &((tIssL3FilterEntry *) pFilterNode)->
                            PriorityNode.PriNode);
            break;
        case ISS_UDBFILTER:
        case ISS_UDB_REDIRECT:

            TMO_DLL_Delete (&(gaPriorityTable[i4Priority]->PriFilterList),
                            &((tIssUserDefinedFilterTable *) pFilterNode)->
                            PriorityNode.PriNode);
            break;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Exit IssExDeleteAclPriorityFilterTable function \n");
    return;
}

/****************************************************************************
 *                                                                            
 *   Function Name : IssExGetIssCommitSupportImmediate                    
 *   
 *   Description   : This function specifies if Immediate is configured   
 *                                                                               
 *   Input(s)      : NULL                                                 
 *   
 *   Output(s)     : NULL                                                 
 *   
 *   Returns       : NONE                                                                                                                        *
 * ****************************************************************************/
INT4
IssExGetIssCommitSupportImmediate (VOID)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Entry IssExGetIssCommitSupportImmediate function \n");
    if (gIssSysGroupInfo.i4ProvisionMode == ISS_IMMEDIATE)
    {
        return ISS_TRUE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Exit IssExGetIssCommitSupportImmediate function \n");
    return ISS_FALSE;

}

/**************************************************************************
 *                                                                        *
 *   Function Name : IssGetTriggerCommit                                  *
 *                                                                           
 *   Description   : This function specifies if trigger commit is true    *
 *                                                                                
 *   Input(s)      : NULL                                                 *
 *                                                                                 
 *   Output(s)     : NULL                                                 *                                
 *                                                                                                                                                  
 *   Returns       : NONE                                                 *                                                                   
 **************************************************************************/
INT4
IssGetTriggerCommit (VOID)
{
    ISS_TRC (INIT_SHUT_TRC, "\nSYS:Entry IssGetTriggerCommit function \n");
    if (gIssSysGroupInfo.i4TriggerCommit == ISS_COMMIT_ACTION_TRUE)
    {

        return ISS_TRUE;

    }
    ISS_TRC (INIT_SHUT_TRC, "\nSYS:Exit IssGetTriggerCommit function \n");
    return ISS_FALSE;
}

/****************************************************************************
 *                                                                          *
 *     Function Name : IssInterfaceGrpRedirectIdNextFree                    *
 *                                                                          *
 *     Description   : The function is used to get the next free index for  *
 *                      the redirect interface group.                       *
 *                                                                          *
 *     Input(s)      : pu4RetValIssRedirectInterfaceGrpIdNextFree           *
 *                                                                          *
 *     Output(s)     : NULL                                                 *
 *                                                                          *
 *     Returns       :  NONE                                                *
 *                                                                          *
 *                                                                          *
 ****************************************************************************/

VOID
IssInterfaceGrpRedirectIdNextFree (UINT4
                                   *pu4RetValIssRedirectInterfaceGrpIdNextFree)
{
    UINT4               u4Index = 0;

    /* Calculate the Redirect Group Index to be Used */

    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Entry IssInterfaceGrpRedirectIdNextFree function \n");
    for (u4Index = 0; u4Index < ISS_MAX_REDIRECT_GRP_ID; u4Index++)
    {
        if (gpIssRedirectIntfInfo[u4Index].u1RowStatus == 0)

        {
            *pu4RetValIssRedirectInterfaceGrpIdNextFree = u4Index + 1;
            break;
        }
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Exit IssInterfaceGrpRedirectIdNextFree function \n");
    return;
}

/****************************************************************************
 *                                                                          *
 *     Function Name : IssReservFramCtrlIdNextFree                          *
 *                                                                          *
 *     Description   : The function is used to get the next free index for  *
 *                      the reserv frame control table entry.               *
 *                                                                          *
 *     Input(s)      : pu4RetValIssReservFramCtrlIdNextFree                 *
 *                                                                          *
 *     Output(s)     : NULL                                                 *
 *                                                                          *
 *     Returns       :  NONE                                                *
 *                                                                          *
 *                                                                          *
 ****************************************************************************/

INT4 IssReservFramCtrlIdNextFree (UINT4 u4IssReservFramCtrlIdNextFree)
{
    tIssReservFrmCtrlTable  *pIssReservFrmCtrlInfo = NULL;
    UINT4               u4LocalCtrlIndex = 0;

    /* Calculate the Reserve frame control Index to be Used */

    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Entry IssReservFramCtrlIdNextFree function \n");
    for (u4LocalCtrlIndex = 1;u4LocalCtrlIndex <= ISS_MAX_RESERV_FRM_CTRL_ID; u4LocalCtrlIndex++)
    {
        pIssReservFrmCtrlInfo = GET_RESERVE_FRAME_CTRL_INFO (u4LocalCtrlIndex);
        if(pIssReservFrmCtrlInfo == NULL)
        {

		return u4LocalCtrlIndex;
	}	
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Exit IssReservFramCtrlIdNextFree function \n");
    return u4IssReservFramCtrlIdNextFree;
}
/****************************************************************************
 *                                                                          *
 *     Function Name : IssClearResrvFrameCtrlEntry                          *
 *                                                                          *
 *     Description   : The function is used to clear the 		    *
 *                      the reserv frame control table entry.               *
 *                                                                          *
 *     Input(s)      : pIssReservFrmCtrlInfo				    *
 *                                                                          *
 *     Output(s)     : NULL                                                 *
 *                                                                          *
 *     Returns       :  NONE                                                *
 *                                                                          *
 *                                                                          *
 ****************************************************************************/
INT4 IssClearResrvFrameCtrlEntry (tIssReservFrmCtrlTable  *pIssReservFrmCtrlInfo,
			UINT4	u4IssReservedFrameCtrlId)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Entry IssClearResrvFrameCtrlEntry function \n");
	if (pIssReservFrmCtrlInfo != NULL)
	{
		if (ISS_RESERVE_FRAME_FREE_MEM_BLOCK(pIssReservFrmCtrlInfo) != MEM_SUCCESS)
		{
			ISS_TRC (INIT_SHUT_TRC, "ISS_RATEENTRY_FREE_MEM_BLOCK() FAILED\n");
			return ISS_FAILURE; 
		}
	}
	GET_RESERVE_FRAME_CTRL_INFO (u4IssReservedFrameCtrlId) = NULL;
    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Exit IssClearResrvFrameCtrlEntry function \n");

	return ISS_SUCCESS;
}

/****************************************************************************
 *                                                                          *
 *     Function Name : IssCheckResrvFrameCtrlEntry                          *
 *                                                                          *
 *     Description   : The function is used to check the                    *
 *                      the reserv frame control table entry.               *
 *                                                                          *
 *     Input(s)      : u4ControlPktType,MacAddr,u4MacMask,u1ControlAction   *
 *                                                                          *
 *     Output(s)     : NULL                                                 *
 *                                                                          *
 *     Returns       :  ISS_SUCCESS,ISS_FAILURE*
 *                                                                          *
 *                                                                          *
 ****************************************************************************/
INT4 IssCheckResrvFrameCtrlEntry (UINT4 u4ControlPktType,tMacAddr MacAddr,
				UINT4 u4MacMask,UINT1 u1ControlAction)
{
    tIssReservFrmCtrlTable *pIssExistReservFrmCtrlInfo = NULL;
    UINT4               u4RservFrmTableIndex = 0;
    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Entry IssCheckResrvFrameCtrlEntry function \n");
    
    for (u4RservFrmTableIndex = 1;u4RservFrmTableIndex < ISS_MAX_RESERV_FRM_CTRL_ID; u4RservFrmTableIndex++)
    {
	pIssExistReservFrmCtrlInfo = GET_RESERVE_FRAME_CTRL_INFO(u4RservFrmTableIndex);
        if ((pIssExistReservFrmCtrlInfo != NULL)&&
	   (pIssExistReservFrmCtrlInfo->u1RowStatus == ISS_ACTIVE))
        {
	   if((MEMCMP(MacAddr,
           pIssExistReservFrmCtrlInfo->MacAddr,sizeof(tMacAddr)) == 0)&&
	    (u1ControlAction == pIssExistReservFrmCtrlInfo->u1ControlAction)&&
	    (u4ControlPktType == pIssExistReservFrmCtrlInfo->u4ControlPktType)&&
	    (u4MacMask == pIssExistReservFrmCtrlInfo->u4MacMask))		
		{
		/*Entry already exist in the table.So need not add it.*/
		return ISS_SUCCESS;	
		}
        }
   }
        ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Exit  IssCheckResrvFrameCtrlEntry function \n");

        return ISS_FAILURE;
}

/****************************************************************************
 *                                                                          *
 *     Function Name : IssResrvFrmIsAllProtoActionDone 		            *
 *                                                                          *
 *     Description   : The function is used to check the                    *
 *                      the reserv frame control table entry.               *
 *                                                                          *
 *     Input(s)      : pIssReservFrmCtrlInfo                                *
 *                                                                          *
 *     Output(s)     : NULL                                                 *
 *                                                                          *
 *     Returns       :  NONE                                                *
 *                                                                          *
 *                                                                          *
 ****************************************************************************/
INT4 IssResrvFrmIsAllProtoActionDone (tIssReservFrmCtrlTable  *pIssReservFrmCtrlInfo)
{
    tIssReservFrmCtrlTable *pIssExistReservFrmCtrlInfo = NULL;
    UINT4               u4RservFrmTableIndex = 0;

    UNUSED_PARAM(pIssReservFrmCtrlInfo);
    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Entry IssResrvFrmIsAllProtoActionDone function \n");

    for (u4RservFrmTableIndex = 1;u4RservFrmTableIndex < ISS_MAX_RESERV_FRM_CTRL_ID; u4RservFrmTableIndex++)
    {
        pIssExistReservFrmCtrlInfo = GET_RESERVE_FRAME_CTRL_INFO(u4RservFrmTableIndex);
        if ((pIssExistReservFrmCtrlInfo != NULL)&&
	    (pIssExistReservFrmCtrlInfo->u1RowStatus == ISS_ACTIVE))
        {
                if(pIssExistReservFrmCtrlInfo->u4ControlPktType == ISS_RESERVED_FRAME_ALL)
                {
                /*Entry already exist in the table.So need not add it.*/
                return ISS_SUCCESS;
                }
        }
   }
        ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Exit IssResrvFrmIsAllProtoActionDone function \n");

        return ISS_FAILURE;
}

/****************************************************************************
 *                                                                          *
 *     Function Name : IssResrvFrmIsAnyEntryExist 			    *
 *                                                                          *
 *     Description   : The function is used to check the                    *
 *                      the reserv frame control table entry.               *
 *                                                                          *
 *     Input(s)      : pIssReservFrmCtrlInfo                                *
 *                                                                          *
 *     Output(s)     : NULL                                                 *
 *                                                                          *
 *     Returns       :  NONE                                                *
 *                                                                          *
 *                                                                          *
 ****************************************************************************/
INT4 IssResrvFrmIsAnyEntryExist(tIssReservFrmCtrlTable  *pIssReservFrmCtrlInfo)
{
    tIssReservFrmCtrlTable *pIssExistReservFrmCtrlInfo = NULL;
    UINT4               u4RservFrmTableIndex = 0;

    UNUSED_PARAM(pIssReservFrmCtrlInfo);
    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Entry IssResrvFrmIsAllProtoActionDone function \n");

    for (u4RservFrmTableIndex = 1;u4RservFrmTableIndex < ISS_MAX_RESERV_FRM_CTRL_ID; u4RservFrmTableIndex++)
    {
        pIssExistReservFrmCtrlInfo = GET_RESERVE_FRAME_CTRL_INFO(u4RservFrmTableIndex);
        if ((pIssExistReservFrmCtrlInfo != NULL)&&
	    (pIssExistReservFrmCtrlInfo->u1RowStatus == ISS_ACTIVE))
        {
                /*Entry is exist in the table*/
                return ISS_FAILURE;
        }
   }
        ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Exit IssResrvFrmIsAllProtoActionDone function \n");
	
        return ISS_SUCCESS;
}

/****************************************************************************
 *                                                                          *
 *     Function Name : IssResrvFrmIsOtherProtoMacMatched 		    *
 *                                                                          *
 *     Description   : The function is used to check the                    *
 *                      the reserv frame control table entry.               *
 *                                                                          *
 *     Input(s)      : pIssReservFrmCtrlInfo                                *
 *                                                                          *
 *     Output(s)     : NULL                                                 *
 *                                                                          *
 *     Returns       :  NONE                                                *
 *                                                                          *
 *                                                                          *
 ****************************************************************************/
INT4 IssResrvFrmIsOtherProtoMacMatched (tIssReservFrmCtrlTable  *pIssReservFrmCtrlInfo)
{
    tIssReservFrmCtrlTable *pIssExistReservFrmCtrlInfo = NULL;
    UINT4               u4RservFrmTableIndex = 0;
    tMacAddr        ResrvMacAddresses[ISS_MAX_RESERV_FRM_CTRL_ID];
    tMacAddr        ResrvExistMacAddresses[ISS_MAX_RESERV_FRM_CTRL_ID];
    INT4            i4TotalMacAddresses = 0;
    INT4            i4TotalExistMacAddresses = 0;
    INT4                i4Index = 0;
    INT4                i4Index1 = 0;

    ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Entry IssResrvFrmIsOtherProtoMacMatches function \n");

    for (u4RservFrmTableIndex = 1;u4RservFrmTableIndex < ISS_MAX_RESERV_FRM_CTRL_ID; u4RservFrmTableIndex++)
    {
        pIssExistReservFrmCtrlInfo = GET_RESERVE_FRAME_CTRL_INFO(u4RservFrmTableIndex);

        if(pIssExistReservFrmCtrlInfo != NULL)
        {
            IssFetchMacAddressesFrmMask(ResrvMacAddresses,
                                        pIssReservFrmCtrlInfo->MacAddr,pIssReservFrmCtrlInfo->u4MacMask,
                                        &i4TotalMacAddresses);
            for(i4Index = 0;i4Index < i4TotalMacAddresses;i4Index++)
            {
                IssFetchMacAddressesFrmMask(ResrvExistMacAddresses,
                                            pIssExistReservFrmCtrlInfo->MacAddr,pIssExistReservFrmCtrlInfo->u4MacMask,
                                            &i4TotalExistMacAddresses);

                for(i4Index1 = 0;i4Index1 < i4TotalExistMacAddresses;i4Index1++)
                {
                    if ((MEMCMP(&ResrvMacAddresses[i4Index],
                                &ResrvExistMacAddresses[i4Index1],sizeof(tMacAddr)) == 0)&&
                        (pIssExistReservFrmCtrlInfo->u1RowStatus == ISS_ACTIVE))
                    {
                        /*Entry is exist in the table*/
                        return ISS_FAILURE;
                    }
                }
            }
        }
    }
        ISS_TRC (INIT_SHUT_TRC,
             "\nSYS:Exit IssResrvFrmIsOtherProtoMacMatches function \n");

        return ISS_SUCCESS;
}

#endif /* _ISSEXCLI_C */
