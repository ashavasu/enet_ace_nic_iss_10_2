/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsisselw.c,v 1.39 2016/07/20 09:54:17 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "iss.h"
# include  "issexinc.h"
# include  "fsisselw.h"
# include  "fsissawr.h"
# include  "fsissalw.h"
# include  "isscli.h"
# include  "aclcli.h"
# include  "diffsrv.h"
# include  "qosxtd.h"
# include  "fsissacli.h"
UINT4               gu4AclOverLa = 1;
extern INT4 gISSOutFilterCount;

/* LOW LEVEL Routines for Table : IssExtRateCtrlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssExtRateCtrlTable
 Input       :  The Indices
                IssExtRateCtrlIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssExtRateCtrlTable (INT4 i4IssExtRateCtrlIndex)
{
    return (nmhValidateIndexInstanceIssAclRateCtrlTable
            (i4IssExtRateCtrlIndex));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssExtRateCtrlTable
 Input       :  The Indices
                IssExtRateCtrlIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssExtRateCtrlTable (INT4 *pi4IssExtRateCtrlIndex)
{
    return (nmhGetFirstIndexIssAclRateCtrlTable (pi4IssExtRateCtrlIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIssExtRateCtrlTable
 Input       :  The Indices
                IssExtRateCtrlIndex
                nextIssRateCtrlIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIssExtRateCtrlTable (INT4 i4IssExtRateCtrlIndex,
                                    INT4 *pi4NextIssRateCtrlIndex)
{
    return (nmhGetNextIndexIssAclRateCtrlTable
            (i4IssExtRateCtrlIndex, pi4NextIssRateCtrlIndex));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIssExtRateCtrlDLFLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                retValIssExtRateCtrlDLFLimitValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtRateCtrlDLFLimitValue (INT4 i4IssExtRateCtrlIndex,
                                   INT4 *pi4RetValIssExtRateCtrlDLFLimitValue)
{
    return (nmhGetIssAclRateCtrlDLFLimitValue
            (i4IssExtRateCtrlIndex, pi4RetValIssExtRateCtrlDLFLimitValue));
}

/****************************************************************************
 Function    :  nmhGetIssExtRateCtrlBCASTLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                retValIssExtRateCtrlBCASTLimitValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtRateCtrlBCASTLimitValue (INT4 i4IssExtRateCtrlIndex,
                                     INT4
                                     *pi4RetValIssExtRateCtrlBCASTLimitValue)
{
    return (nmhGetIssAclRateCtrlBCASTLimitValue
            (i4IssExtRateCtrlIndex, pi4RetValIssExtRateCtrlBCASTLimitValue));
}

/****************************************************************************
 Function    :  nmhGetIssExtRateCtrlMCASTLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                retValIssExtRateCtrlMCASTLimitValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtRateCtrlMCASTLimitValue (INT4 i4IssExtRateCtrlIndex,
                                     INT4
                                     *pi4RetValIssExtRateCtrlMCASTLimitValue)
{
    return (nmhGetIssAclRateCtrlMCASTLimitValue
            (i4IssExtRateCtrlIndex, pi4RetValIssExtRateCtrlMCASTLimitValue));
}

/****************************************************************************
 Function    :  nmhGetIssExtRateCtrlPortRateLimit
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                retValIssExtRateCtrlPortRateLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtRateCtrlPortRateLimit (INT4 i4IssExtRateCtrlIndex,
                                   INT4 *pi4RetValIssExtRateCtrlPortRateLimit)
{
    return (nmhGetIssAclRateCtrlPortRateLimit
            (i4IssExtRateCtrlIndex, pi4RetValIssExtRateCtrlPortRateLimit));
}

/****************************************************************************
 Function    :  nmhGetIssExtRateCtrlPortBurstSize
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                retValIssExtRateCtrlPortBurstSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtRateCtrlPortBurstSize (INT4 i4IssExtRateCtrlIndex,
                                   INT4 *pi4RetValIssExtRateCtrlPortBurstSize)
{
    return (nmhGetIssAclRateCtrlPortBurstSize
            (i4IssExtRateCtrlIndex, pi4RetValIssExtRateCtrlPortBurstSize));
}
/****************************************************************************
 Function    :  nmhGetIssExtDefaultRateCtrlStatus
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                retValIssExtDefaultRateCtrlStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetIssExtDefaultRateCtrlStatus(INT4 i4IssExtRateCtrlIndex , INT4 *pi4RetValIssExtDefaultRateCtrlStatus)
{
     tIssRateCtrlEntry  *pIssAclRateCtrlEntry = NULL;
     
     pIssAclRateCtrlEntry =
               gIssExGlobalInfo.apIssRateCtrlEntry[i4IssExtRateCtrlIndex];
     *pi4RetValIssExtDefaultRateCtrlStatus =  pIssAclRateCtrlEntry->u4IssRateCtrlEnabledStatus;
     
     return SNMP_SUCCESS;

}
 

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIssExtRateCtrlDLFLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                setValIssExtRateCtrlDLFLimitValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtRateCtrlDLFLimitValue (INT4 i4IssExtRateCtrlIndex,
                                   INT4 i4SetValIssExtRateCtrlDLFLimitValue)
{
    return (nmhSetIssAclRateCtrlDLFLimitValue
            (i4IssExtRateCtrlIndex, i4SetValIssExtRateCtrlDLFLimitValue));
}

/****************************************************************************
 Function    :  nmhSetIssExtRateCtrlBCASTLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                setValIssExtRateCtrlBCASTLimitValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtRateCtrlBCASTLimitValue (INT4 i4IssExtRateCtrlIndex,
                                     INT4 i4SetValIssExtRateCtrlBCASTLimitValue)
{
    return (nmhSetIssAclRateCtrlBCASTLimitValue
            (i4IssExtRateCtrlIndex, i4SetValIssExtRateCtrlBCASTLimitValue));
}

/****************************************************************************
 Function    :  nmhSetIssExtRateCtrlMCASTLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                setValIssExtRateCtrlMCASTLimitValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtRateCtrlMCASTLimitValue (INT4 i4IssExtRateCtrlIndex,
                                     INT4 i4SetValIssExtRateCtrlMCASTLimitValue)
{
    return (nmhSetIssAclRateCtrlMCASTLimitValue
            (i4IssExtRateCtrlIndex, i4SetValIssExtRateCtrlMCASTLimitValue));
}

/****************************************************************************
 Function    :  nmhSetIssExtRateCtrlPortRateLimit
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                setValIssExtRateCtrlPortRateLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtRateCtrlPortRateLimit (INT4 i4IssExtRateCtrlIndex,
                                   INT4 i4SetValIssExtRateCtrlPortRateLimit)
{
    return (nmhSetIssAclRateCtrlPortRateLimit
            (i4IssExtRateCtrlIndex, i4SetValIssExtRateCtrlPortRateLimit));
}

/****************************************************************************
 Function    :  nmhSetIssExtRateCtrlPortBurstSize
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                setValIssExtRateCtrlPortBurstSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtRateCtrlPortBurstSize (INT4 i4IssExtRateCtrlIndex,
                                   INT4 i4SetValIssExtRateCtrlPortBurstSize)
{
    return (nmhSetIssAclRateCtrlPortBurstSize
            (i4IssExtRateCtrlIndex, i4SetValIssExtRateCtrlPortBurstSize));
}

/****************************************************************************
 Function    :  nmhSetIssExtDefaultRateCtrlStatus
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                setValIssExtDefaultRateCtrlStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetIssExtDefaultRateCtrlStatus(INT4 i4IssExtRateCtrlIndex , INT4 i4SetValIssExtDefaultRateCtrlStatus)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry = NULL;
    INT4                i4RetVal;
    UINT4               u4OldValBCAST = 0;
    UINT4               u4OldValMCAST = 0;
    UINT4               u4OldValDLF = 0;
    pIssAclRateCtrlEntry = 
               gIssExGlobalInfo.apIssRateCtrlEntry[i4IssExtRateCtrlIndex];

   
    if (pIssAclRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    
    if(i4SetValIssExtDefaultRateCtrlStatus == ISS_STORM_CONTROL_ENABLE) 
    {
        if (pIssAclRateCtrlEntry->u4IssRateCtrlDLFLimitValue == ISS_RATE_ZEROVAL)
        {
             pIssAclRateCtrlEntry->u4IssRateCtrlDLFLimitValue = ISS_RATE_DEFVAL;   
        }
     
        if ( pIssAclRateCtrlEntry->u4IssRateCtrlBCASTLimitValue == ISS_RATE_ZEROVAL)
        {
            pIssAclRateCtrlEntry->u4IssRateCtrlBCASTLimitValue = ISS_RATE_DEFVAL;
        }

        if ( pIssAclRateCtrlEntry->u4IssRateCtrlMCASTLimitValue == ISS_RATE_ZEROVAL)
        {
            pIssAclRateCtrlEntry->u4IssRateCtrlMCASTLimitValue   = ISS_RATE_DEFVAL;
        } 
         pIssAclRateCtrlEntry->u4IssRateCtrlEnabledStatus   = ISS_STORM_CONTROL_ENABLE;
    }
    else 
    {
        u4OldValBCAST = pIssAclRateCtrlEntry->u4IssRateCtrlBCASTLimitValue;
        u4OldValMCAST = pIssAclRateCtrlEntry->u4IssRateCtrlMCASTLimitValue;
        u4OldValDLF   = pIssAclRateCtrlEntry->u4IssRateCtrlDLFLimitValue;
        pIssAclRateCtrlEntry->u4IssRateCtrlBCASTLimitValue = ISS_RATE_ZEROVAL;
        pIssAclRateCtrlEntry->u4IssRateCtrlMCASTLimitValue = ISS_RATE_ZEROVAL;
        pIssAclRateCtrlEntry->u4IssRateCtrlDLFLimitValue   = ISS_RATE_ZEROVAL;
        pIssAclRateCtrlEntry->u4IssRateCtrlEnabledStatus   = ISS_STORM_CONTROL_DISABLE; 

    }
#ifdef NPAPI_WANTED
    i4RetVal = IssHwSetRateLimitingValue (i4IssExtRateCtrlIndex,
                               ISS_RATE_DLF,
                               pIssAclRateCtrlEntry->u4IssRateCtrlDLFLimitValue);

    if (i4RetVal != FNP_SUCCESS)
    {
         if(i4RetVal == FNP_NOT_SUPPORTED)
         {
             CLI_SET_ERR (CLI_ISS_INVALID_RATE);
         }

        return SNMP_FAILURE;
    }

    i4RetVal = IssHwSetRateLimitingValue (i4IssExtRateCtrlIndex,
                               ISS_RATE_BCAST,
                               pIssAclRateCtrlEntry->u4IssRateCtrlBCASTLimitValue);
    
    if (i4RetVal != FNP_SUCCESS)
    {
         if(i4RetVal == FNP_NOT_SUPPORTED)
         {
             CLI_SET_ERR (CLI_ISS_INVALID_RATE);
         }

        return SNMP_FAILURE;
    }

    i4RetVal = IssHwSetRateLimitingValue (i4IssExtRateCtrlIndex,
                               ISS_RATE_MCAST,
                               pIssAclRateCtrlEntry->u4IssRateCtrlMCASTLimitValue);
    
    if (i4RetVal != FNP_SUCCESS)
    {
         if(i4RetVal == FNP_NOT_SUPPORTED)
         {
             CLI_SET_ERR (CLI_ISS_INVALID_RATE);
         }

        return SNMP_FAILURE;
    }
    if(i4SetValIssExtDefaultRateCtrlStatus == ISS_STORM_CONTROL_DISABLE)
    {
        /*need to retain the rate values for next enable */
        pIssAclRateCtrlEntry->u4IssRateCtrlMCASTLimitValue = u4OldValMCAST;
        pIssAclRateCtrlEntry->u4IssRateCtrlBCASTLimitValue = u4OldValBCAST;
        pIssAclRateCtrlEntry->u4IssRateCtrlDLFLimitValue = u4OldValDLF;
    }
    return SNMP_SUCCESS;


#endif
}
/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2IssExtRateCtrlDLFLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                testValIssExtRateCtrlDLFLimitValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtRateCtrlDLFLimitValue (UINT4 *pu4ErrorCode,
                                      INT4 i4IssExtRateCtrlIndex,
                                      INT4 i4TestValIssExtRateCtrlDLFLimitValue)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssExtRateCtrlDLFLimitValue function \n");
    if (IssExtValidateRateCtrlEntry (i4IssExtRateCtrlIndex) != ISS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:failed to validate the RateCtrlIndex %d \n",
                      i4IssExtRateCtrlIndex);
        return SNMP_FAILURE;
    }

    if ((i4TestValIssExtRateCtrlDLFLimitValue < 0)
        || (i4TestValIssExtRateCtrlDLFLimitValue > RATE_LIMIT_MAX_VALUE))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nLOW:Rate Ctrl DLFLimitValue is less than zero and greater than RATE_LIMIT_MAX_VALUE%d               with errorcode  %d \n",
                      RATE_LIMIT_MAX_VALUE, *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssExtRateCtrlDLFLimitValue function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtRateCtrlBCASTLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                testValIssExtRateCtrlBCASTLimitValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtRateCtrlBCASTLimitValue (UINT4 *pu4ErrorCode,
                                        INT4 i4IssExtRateCtrlIndex,
                                        INT4
                                        i4TestValIssExtRateCtrlBCASTLimitValue)
{
    return (nmhTestv2IssAclRateCtrlBCASTLimitValue
            (pu4ErrorCode, i4IssExtRateCtrlIndex,
             i4TestValIssExtRateCtrlBCASTLimitValue));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtRateCtrlMCASTLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                testValIssExtRateCtrlMCASTLimitValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtRateCtrlMCASTLimitValue (UINT4 *pu4ErrorCode,
                                        INT4 i4IssExtRateCtrlIndex,
                                        INT4
                                        i4TestValIssExtRateCtrlMCASTLimitValue)
{
    return (nmhTestv2IssAclRateCtrlMCASTLimitValue
            (pu4ErrorCode, i4IssExtRateCtrlIndex,
             i4TestValIssExtRateCtrlMCASTLimitValue));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtRateCtrlPortRateLimit
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                testValIssExtRateCtrlPortRateLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtRateCtrlPortRateLimit (UINT4 *pu4ErrorCode,
                                      INT4 i4IssExtRateCtrlIndex,
                                      INT4 i4TestValIssExtRateCtrlPortRateLimit)
{
    return (nmhTestv2IssAclRateCtrlPortRateLimit
            (pu4ErrorCode, i4IssExtRateCtrlIndex,
             i4TestValIssExtRateCtrlPortRateLimit));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtRateCtrlPortBurstSize
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                testValIssExtRateCtrlPortBurstSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtRateCtrlPortBurstSize (UINT4 *pu4ErrorCode,
                                      INT4 i4IssExtRateCtrlIndex,
                                      INT4 i4TestValIssExtRateCtrlPortBurstSize)
{
    return (nmhTestv2IssAclRateCtrlPortBurstSize
            (pu4ErrorCode, i4IssExtRateCtrlIndex,
             i4TestValIssExtRateCtrlPortBurstSize));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtDefaultRateCtrlStatus
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                testValIssExtDefaultRateCtrlStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2IssExtDefaultRateCtrlStatus(UINT4 *pu4ErrorCode , INT4 i4IssExtRateCtrlIndex , INT4 i4TestValIssExtDefaultRateCtrlStatus)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssExtDefaultRateCtrlStatus \n");
    if (IssExtValidateRateCtrlEntry (i4IssExtRateCtrlIndex) != ISS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:failed to validate the RateCtrlIndex %d \n",
                      i4IssExtRateCtrlIndex);
        return SNMP_FAILURE;
    }
    if ((i4TestValIssExtDefaultRateCtrlStatus < ISS_STORM_CONTROL_DISABLE)
        || (i4TestValIssExtDefaultRateCtrlStatus > ISS_STORM_CONTROL_ENABLE)) {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssExtDefaultRateCtrlStatus \n");

    return SNMP_SUCCESS;

}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IssExtRateCtrlTable
 Input       :  The Indices
                IssExtRateCtrlIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IssExtRateCtrlTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IssExtL2FilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssExtL2FilterTable
 Input       :  The Indices
                IssExtL2FilterNo
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssExtL2FilterTable (INT4 i4IssExtL2FilterNo)
{
    return (nmhValidateIndexInstanceIssAclL2FilterTable (i4IssExtL2FilterNo));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterPriority
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterPriority (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                                 INT4 i4Element)
{
    return (nmhTestv2IssAclL2FilterPriority
            (pu4Error, i4IssExtL2FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterPriority
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterPriority (INT4 i4IssExtL2FilterNo, INT4 i4Element)
{
    return (nmhSetIssAclL2FilterPriority (i4IssExtL2FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterPriority
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterPriority (INT4 i4IssExtL2FilterNo, INT4 *pElement)
{
    return (nmhGetIssAclL2FilterPriority (i4IssExtL2FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterEtherType
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                testValIssExtL2FilterEtherType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterEtherType (UINT4 *pu4ErrorCode,
                                  INT4 i4IssExtL2FilterNo, INT4 i4Element)
{
    return (nmhTestv2IssAclL2FilterEtherType
            (pu4ErrorCode, i4IssExtL2FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterEtherType
 Input       :  The Indices
               IssExtL2FilterNo

                The Object 
                setValIssExtL2FilterEtherType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterEtherType (INT4 i4IssExtL2FilterNo,
                               INT4 i4SetValIssExtL2FilterEtherType)
{
    return (nmhSetIssAclL2FilterEtherType
            (i4IssExtL2FilterNo, i4SetValIssExtL2FilterEtherType));
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterEtherType
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                retValIssExtL2FilterEtherType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterEtherType (INT4 i4IssExtL2FilterNo,
                               INT4 *pi4RetValIssExtL2FilterEtherType)
{
    return (nmhGetIssAclL2FilterEtherType
            (i4IssExtL2FilterNo, pi4RetValIssExtL2FilterEtherType));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterProtocolType
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterProtocolType (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                                     UINT4 u4Element)
{
    return (nmhTestv2IssAclL2FilterProtocolType
            (pu4Error, i4IssExtL2FilterNo, u4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterProtocolType
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterProtocolType (INT4 i4IssExtL2FilterNo, UINT4 u4Element)
{
    return (nmhSetIssAclL2FilterProtocolType (i4IssExtL2FilterNo, u4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterProtocolType
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterProtocolType (INT4 i4IssExtL2FilterNo, UINT4 *pElement)
{
    return (nmhGetIssAclL2FilterProtocolType (i4IssExtL2FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterDstMacAddr
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterDstMacAddr (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                                   tMacAddr pElement)
{
    return (nmhTestv2IssAclL2FilterDstMacAddr
            (pu4Error, i4IssExtL2FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterDstMacAddr
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterDstMacAddr (INT4 i4IssExtL2FilterNo, tMacAddr MacAddress)
{
    return (nmhSetIssAclL2FilterDstMacAddr (i4IssExtL2FilterNo, MacAddress));
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterDstMacAddr
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterDstMacAddr (INT4 i4IssExtL2FilterNo, tMacAddr * pMacAddress)
{
    return (nmhGetIssAclL2FilterDstMacAddr (i4IssExtL2FilterNo, pMacAddress));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterSrcMacAddr
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterSrcMacAddr (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                                   tMacAddr pElement)
{
    return (nmhTestv2IssAclL2FilterSrcMacAddr
            (pu4Error, i4IssExtL2FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterSrcMacAddr
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterSrcMacAddr (INT4 i4IssExtL2FilterNo, tMacAddr pElement)
{
    return (nmhSetIssAclL2FilterSrcMacAddr (i4IssExtL2FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterSrcMacAddr
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterSrcMacAddr (INT4 i4IssExtL2FilterNo, tMacAddr * pElement)
{
    return (nmhGetIssAclL2FilterSrcMacAddr (i4IssExtL2FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterVlanId
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterVlanId (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                               INT4 i4TestValIssExtL2FilterCustomerVlanId)
{
    return (nmhTestv2IssAclL2FilterVlanId
            (pu4Error, i4IssExtL2FilterNo,
             i4TestValIssExtL2FilterCustomerVlanId));
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterVlanId
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterVlanId (INT4 i4IssExtL2FilterNo, INT4 i4Element)
{
    return (nmhSetIssAclL2FilterVlanId (i4IssExtL2FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterVlanId
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterVlanId (INT4 i4IssExtL2FilterNo, INT4 *pElement)
{
    return (nmhGetIssAclL2FilterVlanId (i4IssExtL2FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterInPortList
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterInPortList (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                                   tSNMP_OCTET_STRING_TYPE * pElement)
{
    return (nmhTestv2IssAclL2FilterInPortList
            (pu4Error, i4IssExtL2FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterInPortList
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterInPortList (INT4 i4IssExtL2FilterNo,
                                tSNMP_OCTET_STRING_TYPE * pElement)
{
    return (nmhSetIssAclL2FilterInPortList (i4IssExtL2FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterInPortList
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterInPortList (INT4 i4IssExtL2FilterNo,
                                tSNMP_OCTET_STRING_TYPE * pElement)
{
    return (nmhGetIssAclL2FilterInPortList (i4IssExtL2FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterOutPortList
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                testValIssExtL2FilterOutPortList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterOutPortList (UINT4 *pu4ErrorCode,
                                    INT4 i4IssExtL2FilterNo,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValOutPortList)
{
    UINT4       u4IfIndex = 0;
    UINT2       u2BitIndex = 0;
    UINT2       u2Port = 0;
    UINT1       u1PortFlag = 0;
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssExtL2FilterOutPortList  function \n");
    if (ISS_IS_L2FILTER_ID_VALID (i4IssExtL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nLOW:failed to validate the L2filter ID %d with error code as %d\n",
                      i4IssExtL2FilterNo, *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    for (u4IfIndex = 0;u4IfIndex < ISS_PORT_LIST_SIZE; u4IfIndex++)
    {
        u1PortFlag = (UINT1) pTestValOutPortList->pu1_OctetList[u4IfIndex];

        for (u2BitIndex = 0; ((u2BitIndex < BITS_PER_BYTE) && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & ISS_BIT8) != 0)
            {
                u2Port = (UINT2) ((u4IfIndex  * 8) + u2BitIndex + 1);
                if (CfaValidateIfIndex ((UINT4) u2Port) == CFA_SUCCESS)
                {

                    if(AclValidateIfMemberoOfPortChannel(u2Port) == CLI_FAILURE)
                    {
                        CLI_SET_ERR (CLI_ACL_SET_ERROR_ON_PORT_CHANNEL);
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        return SNMP_FAILURE;
                    }

                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }
    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L2 filter Entry is NULL  with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if (pIssExtL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L2filter status is ACTIVE with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((pTestValOutPortList->i4_Length <= 0) ||
        (pTestValOutPortList->i4_Length > ISS_PORT_LIST_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nLOW:L2 filter OutPortList length is less than or equal to zero or greater than       value of ISS_PORT_LIST_SIZE %d with errorcode as %d\n",
                      ISS_PORT_LIST_SIZE, *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    /* This function checks if any port greater than the maximum number of
     * ports in the system is in the port list.*/

    if (IssIsPortListValid (pTestValOutPortList->pu1_OctetList,
                            pTestValOutPortList->i4_Length) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:failed to validate the OutPortList with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssExtL2FilterOutPortList function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterOutPortList
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                setValIssExtL2FilterOutPortList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterOutPortList (INT4 i4IssExtL2FilterNo,
                                 tSNMP_OCTET_STRING_TYPE * pSetValOutPortList)
{
    return (nmhSetIssAclL2FilterOutPortList
            (i4IssExtL2FilterNo, pSetValOutPortList));
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterOutPortList
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                retValIssExtL2FilterOutPortList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterOutPortList (INT4 i4IssExtL2FilterNo,
                                 tSNMP_OCTET_STRING_TYPE * pGetValOutPortList)
{
    return (nmhGetIssAclL2FilterOutPortList
            (i4IssExtL2FilterNo, pGetValOutPortList));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterDirection
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                testValIssExtL2FilterDirection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterDirection (UINT4 *pu4ErrorCode,
                                  INT4 i4IssExtL2FilterNo,
                                  INT4 i4TestValIssExtL2FilterDirection)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssExtL2FilterDirection function \n");
    if (ISS_IS_L2FILTER_ID_VALID (i4IssExtL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nLOW:failed to validate the L2filter ID %d with error code as %d\n",
                      i4IssExtL2FilterNo, *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L2 filter Entry is NULL  with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if (pIssExtL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L2filter status is ACTIVE with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if (i4TestValIssExtL2FilterDirection != ISS_DIRECTION_IN &&
        i4TestValIssExtL2FilterDirection != ISS_DIRECTION_OUT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:failed to test L2filter Direction  with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((gISSOutFilterCount  >= ISS_MAX_L2_L3_OUT_FILTERS) &&
        (i4TestValIssExtL2FilterDirection == ISS_DIRECTION_OUT))
    {
        CLI_SET_ERR (CLI_ACL_MAX_OUT_DIRECTION);
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssAclL2FilterEntry Max OUT direction limit reached %d\n",
                      *pu4ErrorCode);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry  nmhTestv2IssExtL2FilterDirection function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterDirection
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                setValIssExtL2FilterDirection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterDirection (INT4 i4IssExtL2FilterNo,
                               INT4 i4SetValIssExtL2FilterDirection)
{
    return (nmhSetIssAclL2FilterDirection
            (i4IssExtL2FilterNo, i4SetValIssExtL2FilterDirection));
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterDirection
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                retValIssExtL2FilterDirection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterDirection (INT4 i4IssExtL2FilterNo,
                               INT4 *pi4RetValIssExtL2FilterDirection)
{
    return (nmhGetIssAclL2FilterDirection (i4IssExtL2FilterNo,
                                           pi4RetValIssExtL2FilterDirection));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterAction
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterAction (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                               INT4 i4Element)
{
    return (nmhTestv2IssAclL2FilterAction
            (pu4Error, i4IssExtL2FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterAction
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterAction (INT4 i4IssExtL2FilterNo, INT4 i4Element)
{
    return (nmhSetIssAclL2FilterAction (i4IssExtL2FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterAction
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterAction (INT4 i4IssExtL2FilterNo, INT4 *pElement)
{
    return (nmhGetIssAclL2FilterAction (i4IssExtL2FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterMatchCount
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterMatchCount (INT4 i4IssExtL2FilterNo, UINT4 *pElement)
{
    return (nmhGetIssAclL2FilterMatchCount (i4IssExtL2FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterStatus
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterStatus (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                               INT4 i4Element)
{
    return (nmhTestv2IssAclL2FilterStatus
            (pu4Error, i4IssExtL2FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterStatus
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterStatus (INT4 i4IssExtL2FilterNo, INT4 i4Element)
{
    return (nmhSetIssAclL2FilterStatus (i4IssExtL2FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterStatus
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterStatus (INT4 i4IssExtL2FilterNo, INT4 *pElement)
{
    return (nmhGetIssAclL2FilterStatus (i4IssExtL2FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterInPortChannelList
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                testValIssExtL2FilterInPortChannelList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterInPortChannelList (UINT4 *pu4ErrorCode,
                                          INT4 i4IssExtL2FilterNo,
                                          tSNMP_OCTET_STRING_TYPE
                                          *
                                          pTestValIssExtL2FilterInPortChannelList)
{
    return (nmhTestv2IssAclL2FilterInPortChannelList
            (pu4ErrorCode, i4IssExtL2FilterNo,
             pTestValIssExtL2FilterInPortChannelList));
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterInPortChannelList
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                setValIssExtL2FilterInPortChannelList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterInPortChannelList (INT4 i4IssExtL2FilterNo,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pSetValIssExtL2FilterInPortChannelList)
{
    return (nmhSetIssAclL2FilterInPortChannelList (i4IssExtL2FilterNo,
                                                   pSetValIssExtL2FilterInPortChannelList));
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterInPortChannelList
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                retValIssExtL2FilterInPortChannelList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterInPortChannelList (INT4 i4IssExtL2FilterNo,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pRetValIssExtL2FilterInPortChannelList)
{
    return (nmhGetIssAclL2FilterInPortChannelList (i4IssExtL2FilterNo,
                                                   pRetValIssExtL2FilterInPortChannelList));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterOutPortChannelList
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                testValIssExtL2FilterOutPortChannelList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterOutPortChannelList (UINT4 *pu4ErrorCode,
                                           INT4 i4IssExtL2FilterNo,
                                           tSNMP_OCTET_STRING_TYPE
                                           *
                                           pTestValIssExtL2FilterOutPortChannelList)
{

    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;
    UINT4       u4IfIndex = 0;
    UINT2       u2BitIndex = 0;
    UINT2       u2Port = 0;
    UINT1       u1PortFlag = 0;
    if (ISS_IS_L2FILTER_ID_VALID (i4IssExtL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }


    for (u4IfIndex = 0;u4IfIndex < ISS_PORT_LIST_SIZE; u4IfIndex++)
    {
        u1PortFlag = (UINT1) pTestValIssExtL2FilterOutPortChannelList->pu1_OctetList[u4IfIndex];

        for (u2BitIndex = 0; ((u2BitIndex < BITS_PER_BYTE) && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & ISS_BIT8) != 0)
            {
                u2Port = (UINT2) ((u4IfIndex  * 8) + u2BitIndex + 1);
                if (CfaValidateIfIndex ((UINT4) u2Port) == CFA_SUCCESS)
                {

                    if(AclValidatePortFromPortChannel(u2Port) == CLI_FAILURE)
                    {
                        CLI_SET_ERR (CLI_ACL_SET_ERROR_ON_PORT);
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        return SNMP_FAILURE;

                    }

                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }
    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExtL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((pTestValIssExtL2FilterOutPortChannelList->i4_Length <= 0) ||
        (pTestValIssExtL2FilterOutPortChannelList->i4_Length >
         ISS_PORT_CHANNEL_LIST_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    /* This function checks if any port greater than the maximum number of
     * ports in the system is in the port list.*/

    if (IssIsPortListValid
        (pTestValIssExtL2FilterOutPortChannelList->pu1_OctetList,
         pTestValIssExtL2FilterOutPortChannelList->i4_Length) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterOutPortChannelList
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                setValIssExtL2FilterOutPortChannelList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterOutPortChannelList (INT4 i4IssExtL2FilterNo,
                                        tSNMP_OCTET_STRING_TYPE
                                        *
                                        pSetValIssExtL2FilterOutPortChannelList)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry != NULL)
    {
        ISS_MEMSET (pIssExtL2FilterEntry->IssL2FilterOutPortChannelList, 0,
                    ISS_PORT_CHANNEL_LIST_SIZE);
        ISS_MEMCPY (pIssExtL2FilterEntry->IssL2FilterOutPortChannelList,
                    pSetValIssExtL2FilterOutPortChannelList->pu1_OctetList,
                    pSetValIssExtL2FilterOutPortChannelList->i4_Length);

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  mhGetIssExtL2FilterOutPortChannelList
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                retValIssExtL2FilterOutPortChannelList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterOutPortChannelList (INT4 i4IssExtL2FilterNo,
                                        tSNMP_OCTET_STRING_TYPE
                                        *
                                        pRetValIssExtL2FilterOutPortChannelList)
{
    return (nmhGetIssAclL2FilterOutPortChannelList
            (i4IssExtL2FilterNo, pRetValIssExtL2FilterOutPortChannelList));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IssExtL2FilterTable
 Input       :  The Indices
                IssExtL2FilterNo
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IssExtL2FilterTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IssExtL3FilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssExtL3FilterTable
 Input       :  The Indices
                IssExtL3FilterNo
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssExtL3FilterTable (INT4 i4IssExtL3FilterNo)
{
    return (nmhValidateIndexInstanceIssAclL3FilterTable (i4IssExtL3FilterNo));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterPriority
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterPriority (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                 INT4 i4Element)
{
    return (nmhTestv2IssAclL3FilterPriority
            (pu4Error, i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterPriority
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterPriority (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    return (nmhSetIssAclL3FilterPriority (i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterPriority
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterPriority (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    return (nmhGetIssAclL3FilterPriority (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterProtocol
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterProtocol (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                 INT4 i4Element)
{
    return (nmhTestv2IssAclL3FilterProtocol
            (pu4Error, i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterProtocol
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterProtocol (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    return (nmhSetIssAclL3FilterProtocol (i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterProtocol
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterProtocol (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    return (nmhGetIssAclL3FilterProtocol (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterMessageType
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterMessageType (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                    INT4 i4Element)
{
    return (nmhTestv2IssAclL3FilterMessageType
            (pu4Error, i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterMessageType
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterMessageType (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    return (nmhSetIssAclL3FilterMessageType (i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterMessageType
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterMessageType (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    return (nmhGetIssAclL3FilterMessageType (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterMessageCode
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterMessageCode (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                    INT4 i4Element)
{
    return (nmhTestv2IssAclL3FilterMessageCode
            (pu4Error, i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterMessageCode
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterMessageCode (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    return (nmhSetIssAclL3FilterMessageCode (i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterMessageCode
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterMessageCode (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    return (nmhGetIssAclL3FilterMessageCode (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterDstIpAddr
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterDstIpAddr (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                  UINT4 u4Element)
{
    tSNMP_OCTET_STRING_TYPE IpvAddrOctet;
    UINT1               au1Tmp[4];

    IpvAddrOctet.pu1_OctetList = &au1Tmp[0];
    PTR_ASSIGN4 (IpvAddrOctet.pu1_OctetList, u4Element);

    IpvAddrOctet.i4_Length = QOS_IPV4_LEN;

    return (nmhTestv2IssAclL3FilterDstIpAddr
            (pu4Error, i4IssExtL3FilterNo, &IpvAddrOctet));

}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterDstIpAddr
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterDstIpAddr (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    tSNMP_OCTET_STRING_TYPE IpvAddrOctet;
    UINT1               au1Tmp[16];

    IpvAddrOctet.pu1_OctetList = au1Tmp;

    PTR_ASSIGN4 (IpvAddrOctet.pu1_OctetList, u4Element);

    IpvAddrOctet.i4_Length = QOS_IPV4_LEN;

    return (nmhSetIssAclL3FilterDstIpAddr (i4IssExtL3FilterNo, &IpvAddrOctet));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterDstIpAddr
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterDstIpAddr (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    tSNMP_OCTET_STRING_TYPE IpvAddrOctet;
    UINT1               au1Tmp[16];

    IpvAddrOctet.pu1_OctetList = &au1Tmp[0];

    if (nmhGetIssAclL3FilterDstIpAddr (i4IssExtL3FilterNo, &IpvAddrOctet) ==
        SNMP_SUCCESS)
    {
        PTR_FETCH4 (*pElement, IpvAddrOctet.pu1_OctetList);
        return SNMP_SUCCESS;

    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterSrcIpAddr
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterSrcIpAddr (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                  UINT4 u4Element)
{
    tSNMP_OCTET_STRING_TYPE IpvAddrOctet;
    UINT1               au1Tmp[4];

    IpvAddrOctet.pu1_OctetList = &au1Tmp[0];
    PTR_ASSIGN4 (IpvAddrOctet.pu1_OctetList, u4Element);
    IpvAddrOctet.i4_Length = QOS_IPV4_LEN;

    return (nmhTestv2IssAclL3FilterSrcIpAddr
            (pu4Error, i4IssExtL3FilterNo, &IpvAddrOctet));

}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterSrcIpAddr
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterSrcIpAddr (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    tSNMP_OCTET_STRING_TYPE IpvAddrOctet;
    UINT1               au1Tmp[4];

    IpvAddrOctet.pu1_OctetList = &au1Tmp[0];
    IpvAddrOctet.i4_Length = QOS_IPV4_LEN;

    PTR_ASSIGN4 (IpvAddrOctet.pu1_OctetList, u4Element);

    return (nmhSetIssAclL3FilterSrcIpAddr (i4IssExtL3FilterNo, &IpvAddrOctet));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterSrcIpAddr
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterSrcIpAddr (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    tSNMP_OCTET_STRING_TYPE IpvAddrOctet;
    UINT1               au1Tmp[IP6_ADDR_SIZE];

    IpvAddrOctet.pu1_OctetList = &au1Tmp[0];

    if (nmhGetIssAclL3FilterSrcIpAddr (i4IssExtL3FilterNo, &IpvAddrOctet) ==
        SNMP_SUCCESS)
    {
        PTR_FETCH4 (*pElement, IpvAddrOctet.pu1_OctetList);
        return SNMP_SUCCESS;

    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterDstIpAddrMask
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterDstIpAddrMask (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                      UINT4 u4Element)
{
    UINT4               u4Prefix;

    IssUtlConvertMaskToPrefix (u4Element, &u4Prefix);

    return (nmhTestv2IssAclL3FilterDstIpAddrPrefixLength
            (pu4Error, i4IssExtL3FilterNo, u4Prefix));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterDstIpAddrMask
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterDstIpAddrMask (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    UINT4               u4Prefix;
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);
    if (pIssL3FilterEntry != NULL)
    {
        pIssL3FilterEntry->u4IssL3FilterDstIpAddrMask = u4Element;
    }

    IssUtlConvertMaskToPrefix (u4Element, &u4Prefix);
    return (nmhSetIssAclL3FilterDstIpAddrPrefixLength
            (i4IssExtL3FilterNo, u4Prefix));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterDstIpAddrMask
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterDstIpAddrMask (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    UINT4               u4Prefix;

    if (nmhGetIssAclL3FilterDstIpAddrPrefixLength
        (i4IssExtL3FilterNo, &u4Prefix) == SNMP_SUCCESS)
    {
        IssUtlConvertPrefixToMask (u4Prefix, pElement);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterSrcIpAddrMask
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterSrcIpAddrMask (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                      UINT4 u4Element)
{
    UINT4               u4Prefix;

    IssUtlConvertMaskToPrefix (u4Element, &u4Prefix);

    return (nmhTestv2IssAclL3FilterSrcIpAddrPrefixLength
            (pu4Error, i4IssExtL3FilterNo, u4Prefix));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterSrcIpAddrMask
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterSrcIpAddrMask (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    UINT4               u4Prefix;
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);
    if (pIssL3FilterEntry != NULL)
    {
        pIssL3FilterEntry->u4IssL3FilterSrcIpAddrMask = u4Element;
    }

    IssUtlConvertMaskToPrefix (u4Element, &u4Prefix);

    return (nmhSetIssAclL3FilterSrcIpAddrPrefixLength
            (i4IssExtL3FilterNo, u4Prefix));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterSrcIpAddrMask
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterSrcIpAddrMask (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    UINT4               u4Prefix;

    if (nmhGetIssAclL3FilterSrcIpAddrPrefixLength
        (i4IssExtL3FilterNo, &u4Prefix) == SNMP_SUCCESS)
    {
        IssUtlConvertPrefixToMask (u4Prefix, pElement);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterMinDstProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterMinDstProtPort (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                       UINT4 u4Element)
{
    return (nmhTestv2IssAclL3FilterMinDstProtPort
            (pu4Error, i4IssExtL3FilterNo, u4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterMinDstProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterMinDstProtPort (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    return (nmhSetIssAclL3FilterMinDstProtPort (i4IssExtL3FilterNo, u4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterMinDstProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterMinDstProtPort (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    return (nmhGetIssAclL3FilterMinDstProtPort (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterMaxDstProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterMaxDstProtPort (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                       UINT4 u4Element)
{
    return (nmhTestv2IssAclL3FilterMaxDstProtPort
            (pu4Error, i4IssExtL3FilterNo, u4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterMaxDstProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterMaxDstProtPort (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    return (nmhSetIssAclL3FilterMaxDstProtPort (i4IssExtL3FilterNo, u4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterMaxDstProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterMaxDstProtPort (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    return (nmhGetIssAclL3FilterMaxDstProtPort (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterMinSrcProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterMinSrcProtPort (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                       UINT4 u4Element)
{
    return (nmhTestv2IssAclL3FilterMinSrcProtPort
            (pu4Error, i4IssExtL3FilterNo, u4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterMinSrcProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterMinSrcProtPort (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    return (nmhSetIssAclL3FilterMinSrcProtPort (i4IssExtL3FilterNo, u4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterMinSrcProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterMinSrcProtPort (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    return (nmhGetIssAclL3FilterMinSrcProtPort (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterMaxSrcProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterMaxSrcProtPort (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                       UINT4 u4Element)
{
    return (nmhTestv2IssAclL3FilterMaxSrcProtPort
            (pu4Error, i4IssExtL3FilterNo, u4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterMaxSrcProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterMaxSrcProtPort (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    return (nmhSetIssAclL3FilterMaxSrcProtPort (i4IssExtL3FilterNo, u4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterMaxSrcProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterMaxSrcProtPort (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    return (nmhGetIssAclL3FilterMaxSrcProtPort (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterInPortList
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterInPortList (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                   tSNMP_OCTET_STRING_TYPE * pElement)
{
    return (nmhTestv2IssAclL3FilterInPortList
            (pu4Error, i4IssExtL3FilterNo, pElement));

}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterInPortList
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterInPortList (INT4 i4IssExtL3FilterNo,
                                tSNMP_OCTET_STRING_TYPE * pElement)
{
    return (nmhSetIssAclL3FilterInPortList (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterInPortList
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterInPortList (INT4 i4IssExtL3FilterNo,
                                tSNMP_OCTET_STRING_TYPE * pElement)
{
    return (nmhGetIssAclL3FilterInPortList (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterOutPortList
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterOutPortList (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                    tSNMP_OCTET_STRING_TYPE * pElement)
{
    return (nmhTestv2IssAclL3FilterOutPortList
            (pu4Error, i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterOutPortList
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterOutPortList (INT4 i4IssExtL3FilterNo,
                                 tSNMP_OCTET_STRING_TYPE * pElement)
{
    return (nmhSetIssAclL3FilterOutPortList (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterOutPortList
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterOutPortList (INT4 i4IssExtL3FilterNo,
                                 tSNMP_OCTET_STRING_TYPE * pElement)
{
    return (nmhGetIssAclL3FilterOutPortList (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterAckBit
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterAckBit (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                               INT4 i4Element)
{
    return (nmhTestv2IssAclL3FilterAckBit
            (pu4Error, i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterAckBit
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterAckBit (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    return (nmhSetIssAclL3FilterAckBit (i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterAckBit
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterAckBit (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    return (nmhGetIssAclL3FilterAckBit (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterRstBit
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterRstBit (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                               INT4 i4Element)
{
    return (nmhTestv2IssAclL3FilterRstBit
            (pu4Error, i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterRstBit
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterRstBit (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    return (nmhSetIssAclL3FilterRstBit (i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterRstBit
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterRstBit (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    return (nmhGetIssAclL3FilterRstBit (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterTos
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterTos (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                            INT4 i4Element)
{
    return (nmhTestv2IssAclL3FilterTos
            (pu4Error, i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterTos
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterTos (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    return (nmhSetIssAclL3FilterTos (i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterTos
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterTos (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    return (nmhGetIssAclL3FilterTos (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterDscp
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                testValIssExtL3FilterDscp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterDscp (UINT4 *pu4ErrorCode, INT4 i4IssExtL3FilterNo,
                             INT4 i4TestValIssExtL3FilterDscp)
{
    return (nmhTestv2IssAclL3FilterDscp
            (pu4ErrorCode, i4IssExtL3FilterNo, i4TestValIssExtL3FilterDscp));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterDscp
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                setValIssExtL3FilterDscp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterDscp (INT4 i4IssExtL3FilterNo,
                          INT4 i4SetValIssExtL3FilterDscp)
{
    return (nmhSetIssAclL3FilterDscp
            (i4IssExtL3FilterNo, i4SetValIssExtL3FilterDscp));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterDscp
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                retValIssExtL3FilterDscp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterDscp (INT4 i4IssExtL3FilterNo,
                          INT4 *pi4RetValIssExtL3FilterDscp)
{
    return (nmhGetIssAclL3FilterDscp
            (i4IssExtL3FilterNo, pi4RetValIssExtL3FilterDscp));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterDirection
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterDirection (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                  INT4 i4Element)
{
    return (nmhTestv2IssAclL3FilterDirection
            (pu4Error, i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterDirection
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterDirection (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    return (nmhSetIssAclL3FilterDirection (i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterDirection
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterDirection (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    return (nmhGetIssAclL3FilterDirection (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterAction
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterAction (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                               INT4 i4Element)
{
    return (nmhTestv2IssAclL3FilterAction
            (pu4Error, i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterAction
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterAction (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    return (nmhSetIssAclL3FilterAction (i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterAction
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterAction (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    return (nmhGetIssAclL3FilterAction (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterMatchCount
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterMatchCount (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    return (nmhGetIssAclL3FilterMatchCount (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterStatus
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterStatus (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                               INT4 i4Element)
{
    return (nmhTestv2IssAclL3FilterStatus
            (pu4Error, i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterStatus
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterStatus (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    return (nmhSetIssAclL3FilterStatus (i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterStatus
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterStatus (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    return (nmhGetIssAclL3FilterStatus (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterInPortChannelList
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                testValIssExtL3FilterInPortChannelList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterInPortChannelList (UINT4 *pu4ErrorCode,
                                          INT4 i4IssExtL3FilterNo,
                                          tSNMP_OCTET_STRING_TYPE
                                          *
                                          pTestValIssExtL3FilterInPortChannelList)
{
    return (nmhTestv2IssAclL3FilterInPortChannelList
            (pu4ErrorCode, i4IssExtL3FilterNo,
             pTestValIssExtL3FilterInPortChannelList));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterInPortChannelList
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                setValIssExtL3FilterInPortChannelList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterInPortChannelList (INT4 i4IssExtL3FilterNo,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pSetValIssExtL3FilterInPortChannelList)
{
    return (nmhSetIssAclL3FilterInPortChannelList (i4IssExtL3FilterNo,
                                                   pSetValIssExtL3FilterInPortChannelList));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterInPortChannelList
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                retValIssExtL3FilterInPortChannelList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterInPortChannelList (INT4 i4IssExtL3FilterNo,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pRetValIssExtL3FilterInPortChannelList)
{
    return (nmhGetIssAclL3FilterInPortChannelList (i4IssExtL3FilterNo,
                                                   pRetValIssExtL3FilterInPortChannelList));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterOutPortChannelList
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                testValIssExtL3FilterOutPortChannelList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterOutPortChannelList (UINT4 *pu4ErrorCode,
                                           INT4 i4IssExtL3FilterNo,
                                           tSNMP_OCTET_STRING_TYPE
                                           *
                                           pTestValIssExtL3FilterOutPortChannelList)
{
    return (nmhTestv2IssAclL3FilterOutPortChannelList
            (pu4ErrorCode, i4IssExtL3FilterNo,
             pTestValIssExtL3FilterOutPortChannelList));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterOutPortChannelList
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                setValIssExtL3FilterOutPortChannelList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterOutPortChannelList (INT4 i4IssExtL3FilterNo,
                                        tSNMP_OCTET_STRING_TYPE
                                        *
                                        pSetValIssExtL3FilterOutPortChannelList)
{
    return (nmhSetIssAclL3FilterOutPortChannelList (i4IssExtL3FilterNo,
                                                    pSetValIssExtL3FilterOutPortChannelList));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterOutPortChannelList
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                retValIssExtL3FilterOutPortChannelList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterOutPortChannelList (INT4 i4IssExtL3FilterNo,
                                        tSNMP_OCTET_STRING_TYPE
                                        *
                                        pRetValIssExtL3FilterOutPortChannelList)
{
    return (nmhGetIssAclL3FilterOutPortChannelList (i4IssExtL3FilterNo,
                                                    pRetValIssExtL3FilterOutPortChannelList));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssExtL3FilterTable
 Input       :  The Indices
                IssExtFilterL3No
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssExtL3FilterTable (INT4 *pi4IssExtL3FilterNo)
{
    return (nmhGetFirstIndexIssAclL3FilterTable (pi4IssExtL3FilterNo));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIssExtL3FilterTable
 Input       :  The Indices
                IssExtL3FilterNo
                nextIssL3FilterNo
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssExtL3FilterTable (INT4 i4IssExtL3FilterNo,
                                    INT4 *pi4IssExtL3FilterNoOUT)
{
    return (nmhGetNextIndexIssAclL3FilterTable
            (i4IssExtL3FilterNo, pi4IssExtL3FilterNoOUT));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIssExtL2FilterTable
 Input       :  The Indices
                IssExtL2FilterNo
                nextIssL2FilterNo
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIssExtL2FilterTable (INT4 i4IssExtL2FilterNo,
                                    INT4 *pi4IssExtL2FilterNoOUT)
{

    if (i4IssExtL2FilterNo < 0)
    {
        return SNMP_FAILURE;
    }

    if ((IssExtSnmpLowGetNextValidL2FilterTableIndex
         (i4IssExtL2FilterNo, pi4IssExtL2FilterNoOUT)) == ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssExtL2FilterTable
 Input       :  The Indices
                IssExtFilterL2No
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssExtL2FilterTable (INT4 *pi4IssExtL2FilterNo)
{
    if ((IssExtSnmpLowGetFirstValidL2FilterTableIndex (pi4IssExtL2FilterNo)) ==
        ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
* Function    :  IssExtSnmpLowValidateIndexPortRateTable
* Input       :  i4CtrlIndex(PortCtrl Table Index or Rate Ctrl Table Index)
* Output      :  None
* Returns     :  ISS_SUCCESS/ISS_FAILURE
*****************************************************************************/
INT4
IssExtSnmpLowValidateIndexPortRateTable (INT4 i4PortIndex)
{
    /* This routine is only for Port Ctrl Table */
    if ((i4PortIndex > ISS_ZERO_ENTRY)
        && (i4PortIndex <= BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        return ISS_SUCCESS;
    }
    return ISS_FAILURE;
}

/****************************************************************************
* Function    :  IssExtValidateRateCtrlEntry
* Input       :  i4RateCtrlIndex
* Output      :  None
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssExtValidateRateCtrlEntry (INT4 i4RateCtrlIndex)
{
    /* Validating the entry */
    if ((i4RateCtrlIndex <= ISS_ZERO_ENTRY) ||
        (i4RateCtrlIndex > BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        return ISS_FAILURE;
    }

    if ((gIssExGlobalInfo.apIssRateCtrlEntry[i4RateCtrlIndex] == NULL))
    {
        ISS_TRC_ARG1 (DATA_PATH_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "ISS: RATE CTRL ENTRY %d DOES NOT EXIST\n",
                      i4RateCtrlIndex);
        return ISS_FAILURE;
    }
    return ISS_SUCCESS;
}

/****************************************************************************
* Function    :  IssExtSnmpLowGetFirstValidL3FilterTableIndex
* Input       :  None
* Output      :  INT4 *pi4FirstL3FilterIndex
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssExtSnmpLowGetFirstValidL3FilterTableIndex (INT4 *pi4FirstL3FilterIndex)
{
    tIssSllNode        *pSllNode = NULL;
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;
    tIssL3FilterEntry  *pFirstL3FilterEntry = NULL;
    UINT1               u1FoundFlag = ISS_FALSE;

    pSllNode = ISS_SLL_FIRST (&ISS_L3FILTER_LIST);

    while (pSllNode != NULL)
    {
        pIssExtL3FilterEntry = (tIssL3FilterEntry *) pSllNode;
        if (u1FoundFlag == ISS_FALSE)
        {
            pFirstL3FilterEntry = pIssExtL3FilterEntry;
            u1FoundFlag = ISS_TRUE;
        }
        else
        {
            if (pFirstL3FilterEntry->i4IssL3FilterNo >
                pIssExtL3FilterEntry->i4IssL3FilterNo)
            {
                pFirstL3FilterEntry = pIssExtL3FilterEntry;
            }
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L3FILTER_LIST, pSllNode);
    }

    if (u1FoundFlag == ISS_TRUE)
    {
        *pi4FirstL3FilterIndex = pFirstL3FilterEntry->i4IssL3FilterNo;
        return ISS_SUCCESS;
    }

    return ISS_FAILURE;
}

/****************************************************************************
* Function    :  IssExtSnmpLowGetFirstValidL3FilterTableIndex
* Input       :  INT4 i4IssExt3FilterNo
* Output      :  INT4 *pi4NextL3FilterNo
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssExtSnmpLowGetNextValidL3FilterTableIndex (INT4 i4IssExtL3FilterNo,
                                             INT4 *pi4NextL3FilterNo)
{
    tIssSllNode        *pSllNode = NULL;
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;
    tIssL3FilterEntry  *pNextL3FilterEntry = NULL;
    UINT1               u1FoundFlag = ISS_FALSE;

    pSllNode = ISS_SLL_FIRST (&ISS_L3FILTER_LIST);

    while (pSllNode != NULL)
    {
        pIssExtL3FilterEntry = (tIssL3FilterEntry *) pSllNode;

        if (i4IssExtL3FilterNo < pIssExtL3FilterEntry->i4IssL3FilterNo)
        {
            if (u1FoundFlag == ISS_FALSE)
            {
                pNextL3FilterEntry = pIssExtL3FilterEntry;
                u1FoundFlag = ISS_TRUE;
            }
            else
            {
                if (pNextL3FilterEntry->i4IssL3FilterNo >
                    pIssExtL3FilterEntry->i4IssL3FilterNo)
                {
                    pNextL3FilterEntry = pIssExtL3FilterEntry;
                }
            }
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L3FILTER_LIST, pSllNode);
    }

    if (u1FoundFlag == ISS_TRUE)
    {
        *pi4NextL3FilterNo = pNextL3FilterEntry->i4IssL3FilterNo;
        return ISS_SUCCESS;
    }
    else
    {
        return ISS_FAILURE;
    }
}

/****************************************************************************
* Function    :  IssExtSnmpLowGetFirstValidL2FilterTableIndex
* Input       :  None
* Output      :  INT4 *pi4FirstL2FilterIndex
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssExtSnmpLowGetFirstValidL2FilterTableIndex (INT4 *pi4FirstL2FilterIndex)
{
    tIssSllNode        *pSllNode = NULL;
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;
    tIssL2FilterEntry  *pFirstL2FilterEntry = NULL;
    UINT1               u1FoundFlag = ISS_FALSE;

    pSllNode = ISS_SLL_FIRST (&ISS_L2FILTER_LIST);

    while (pSllNode != NULL)
    {
        pIssExtL2FilterEntry = (tIssL2FilterEntry *) pSllNode;
        if (u1FoundFlag == ISS_FALSE)
        {
            pFirstL2FilterEntry = pIssExtL2FilterEntry;
            u1FoundFlag = ISS_TRUE;
        }
        else
        {
            if (pFirstL2FilterEntry->i4IssL2FilterNo >
                pIssExtL2FilterEntry->i4IssL2FilterNo)
            {
                pFirstL2FilterEntry = pIssExtL2FilterEntry;
            }
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L2FILTER_LIST, pSllNode);
    }

    if (u1FoundFlag == ISS_TRUE)
    {
        *pi4FirstL2FilterIndex = pFirstL2FilterEntry->i4IssL2FilterNo;
        return ISS_SUCCESS;
    }

    return ISS_FAILURE;
}

/****************************************************************************
* Function    :  IssExtSnmpLowGetFirstValidL2FilterTableIndex
* Input       :  INT4 i4IssExtL2FilterNo
* Output      :  INT4 *pi4NextL2FilterNo
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssExtSnmpLowGetNextValidL2FilterTableIndex (INT4 i4IssExtL2FilterNo,
                                             INT4 *pi4NextL2FilterNo)
{
    tIssSllNode        *pSllNode = NULL;
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;
    tIssL2FilterEntry  *pNextL2FilterEntry = NULL;
    UINT1               u1FoundFlag = ISS_FALSE;

    pSllNode = ISS_SLL_FIRST (&ISS_L2FILTER_LIST);

    while (pSllNode != NULL)
    {
        pIssExtL2FilterEntry = (tIssL2FilterEntry *) pSllNode;

        if (i4IssExtL2FilterNo < pIssExtL2FilterEntry->i4IssL2FilterNo)
        {
            if (u1FoundFlag == ISS_FALSE)
            {
                pNextL2FilterEntry = pIssExtL2FilterEntry;
                u1FoundFlag = ISS_TRUE;
            }
            else
            {
                if (pNextL2FilterEntry->i4IssL2FilterNo >
                    pIssExtL2FilterEntry->i4IssL2FilterNo)
                {
                    pNextL2FilterEntry = pIssExtL2FilterEntry;
                }
            }
        }

        pSllNode = ISS_SLL_NEXT (&ISS_L2FILTER_LIST, pSllNode);
    }

    if (u1FoundFlag == ISS_TRUE)
    {
        *pi4NextL2FilterNo = pNextL2FilterEntry->i4IssL2FilterNo;
        return ISS_SUCCESS;
    }
    else
    {
        return ISS_FAILURE;
    }
}

/****************************************************************************
* Function    :  IssExtGetL2FilterEntry
* Input       :  INT4 i4IssExtL2FilterNo
* Output      :  None
* Returns     :  tIssL2FilterEntry * 
*****************************************************************************/
tIssL2FilterEntry  *
IssExtGetL2FilterEntry (INT4 i4IssExtL2FilterNo)
{

    tIssSllNode        *pSllNode = NULL;
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    pSllNode = ISS_SLL_FIRST (&ISS_L2FILTER_LIST);

    while (pSllNode != NULL)
    {
        pIssExtL2FilterEntry = (tIssL2FilterEntry *) pSllNode;

        if (pIssExtL2FilterEntry->i4IssL2FilterNo == i4IssExtL2FilterNo)
        {
            return pIssExtL2FilterEntry;
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L2FILTER_LIST, pSllNode);
    }
    return NULL;
}

/****************************************************************************
* Function    :  IssExtQualifyL2FilterData 
* Input       :  tIssL2FilterEntry **ppIssExtL2FilterEntry
* Output      :  None
* Returns     :  INT4
*****************************************************************************/
INT4
IssExtQualifyL2FilterData (tIssL2FilterEntry ** ppIssExtL2FilterEntry)
{
    if ((*ppIssExtL2FilterEntry)->IssL2FilterAction != 0)
    {
        (*ppIssExtL2FilterEntry)->u1IssL2FilterStatus = ISS_NOT_IN_SERVICE;
        return ISS_SUCCESS;
    }
    else
    {
        (*ppIssExtL2FilterEntry)->u1IssL2FilterStatus = ISS_NOT_READY;
        return ISS_FAILURE;
    }
}

/****************************************************************************
* Function    :  IssExtGetL3FilterEntry
* Input       :  INT4 i4IssExtL3FilterNo
* Output      :  None
* Returns     :  tIssL3FilterEntry * 
*****************************************************************************/
tIssL3FilterEntry  *
IssExtGetL3FilterEntry (INT4 i4IssExtL3FilterNo)
{

    tIssSllNode        *pSllNode = NULL;
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pSllNode = ISS_SLL_FIRST (&ISS_L3FILTER_LIST);

    while (pSllNode != NULL)
    {
        pIssExtL3FilterEntry = (tIssL3FilterEntry *) pSllNode;

        if (pIssExtL3FilterEntry->i4IssL3FilterNo == i4IssExtL3FilterNo)
        {
            return pIssExtL3FilterEntry;
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L3FILTER_LIST, pSllNode);
    }
    return NULL;
}

/****************************************************************************
* Function    :  IssExtQualifyL3FilterData 
* Input       :  tIssL3FilterEntry **ppIssExtL3FilterEntry
* Output      :  None
* Returns     :  INT4
*****************************************************************************/
INT4
IssExtQualifyL3FilterData (tIssL3FilterEntry ** ppIssExtL3FilterEntry)
{
    if (((*ppIssExtL3FilterEntry)->IssL3FilterAction != 0) &&
        ((*ppIssExtL3FilterEntry)->IssL3FilterDirection != 0))
    {
        (*ppIssExtL3FilterEntry)->u1IssL3FilterStatus = ISS_NOT_IN_SERVICE;
        return ISS_SUCCESS;
    }
    else
    {
        (*ppIssExtL3FilterEntry)->u1IssL3FilterStatus = ISS_NOT_READY;
        return ISS_FAILURE;
    }
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IssExtL3FilterTable
 Input       :  The Indices
                IssExtL3FilterNo
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IssExtL3FilterTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
* Function    :  IssExtGetUdbFilterTableEntry
* Input       :  INT4 i4IssExtL3FilterNo
* Output      :  None
* Returns     :  tIssL3FilterEntry *
*****************************************************************************/
tIssUserDefinedFilterTable *
IssExtGetUdbFilterTableEntry (UINT4 u4IssUdbFilterNo)
{

    tIssSllNode        *pSllNode = NULL;
    tIssUserDefinedFilterTable *pIssUdbFilterTableEntry = NULL;
    pSllNode = ISS_SLL_FIRST (&ISS_UDB_FILTER_TABLE_LIST);

    while (pSllNode != NULL)
    {
        pIssUdbFilterTableEntry = (tIssUserDefinedFilterTable *) pSllNode;
        if (pIssUdbFilterTableEntry == NULL
            || pIssUdbFilterTableEntry->pAccessFilterEntry == NULL)
        {
            return NULL;
        }

        if (pIssUdbFilterTableEntry->pAccessFilterEntry->
            u4UDBFilterId == u4IssUdbFilterNo)
        {
            return pIssUdbFilterTableEntry;
        }
        pSllNode = ISS_SLL_NEXT (&ISS_UDB_FILTER_TABLE_LIST, pSllNode);
    }
    return NULL;
}

/****************************************************************************
* Function    :  IssExtSnmpLowGetFirstValidUDBFilterTableIndex
* Input       :  None
* Output      :  UINT4 *pu4FirstUdbFilterIndex
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssExtSnmpLowGetFirstValidUDBFilterTableIndex (INT4 *pu4FirstUdbFilterIndex)
{
    tIssSllNode        *pSllNode = NULL;
    tIssUserDefinedFilterTable *pIssUdbFilterEntry = NULL;
    tIssUserDefinedFilterTable *pFirstUdbFilterEntry = NULL;
    UINT1               u1FoundFlag = ISS_FALSE;

    pSllNode = ISS_SLL_FIRST (&ISS_UDB_FILTER_TABLE_LIST);

    while (pSllNode != NULL)
    {
        pIssUdbFilterEntry = (tIssUserDefinedFilterTable *) pSllNode;

        if (pIssUdbFilterEntry->pAccessFilterEntry == NULL)
        {
            return ISS_FAILURE;
        }

        if (u1FoundFlag == ISS_FALSE)
        {
            pFirstUdbFilterEntry = pIssUdbFilterEntry;
            u1FoundFlag = ISS_TRUE;
        }
        else
        {
            if (pFirstUdbFilterEntry->pAccessFilterEntry->u4UDBFilterId >
                pIssUdbFilterEntry->pAccessFilterEntry->u4UDBFilterId)
            {
                pFirstUdbFilterEntry = pIssUdbFilterEntry;
            }
        }
        pSllNode = ISS_SLL_NEXT (&ISS_UDB_FILTER_TABLE_LIST, pSllNode);
    }

    if (u1FoundFlag == ISS_TRUE)
    {
        *pu4FirstUdbFilterIndex = pFirstUdbFilterEntry->
            pAccessFilterEntry->u4UDBFilterId;
        return ISS_SUCCESS;
    }

    return ISS_FAILURE;
}

/****************************************************************************
* Function    :  IssSnmpLowGetNextValidUdbFilterTableIndex
* Input       :  UINT4 u4IssUdbFilterNo
* Output      :  INT4 *pu4NextUdbFilterNo
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssSnmpLowGetNextValidUdbFilterTableIndex (UINT4 u4IssUdbFilterNo,
                                           UINT4 *pu4NextUdbFilterNo)
{
    tIssSllNode        *pSllNode = NULL;
    tIssUserDefinedFilterTable *pIssUdbFilterTableEntry = NULL;
    tIssUserDefinedFilterTable *pNextUdbFilterTableEntry = NULL;
    UINT1               u1FoundFlag = ISS_FALSE;

    pSllNode = ISS_SLL_FIRST (&ISS_UDB_FILTER_TABLE_LIST);

    while (pSllNode != NULL)
    {
        pIssUdbFilterTableEntry = (tIssUserDefinedFilterTable *) pSllNode;

        if (pIssUdbFilterTableEntry->pAccessFilterEntry == NULL)
        {
            return ISS_FAILURE;
        }

        if (u4IssUdbFilterNo <
            pIssUdbFilterTableEntry->pAccessFilterEntry->u4UDBFilterId)
        {
            if (u1FoundFlag == ISS_FALSE)
            {
                pNextUdbFilterTableEntry = pIssUdbFilterTableEntry;
                u1FoundFlag = ISS_TRUE;
            }
            else
            {
                if (pNextUdbFilterTableEntry->
                    pAccessFilterEntry->u4UDBFilterId >
                    pIssUdbFilterTableEntry->pAccessFilterEntry->u4UDBFilterId)
                {
                    pNextUdbFilterTableEntry = pIssUdbFilterTableEntry;
                }
            }
        }
        pSllNode = ISS_SLL_NEXT (&ISS_UDB_FILTER_TABLE_LIST, pSllNode);
    }

    if (u1FoundFlag == ISS_TRUE)
    {
        *pu4NextUdbFilterNo = pNextUdbFilterTableEntry->
            pAccessFilterEntry->u4UDBFilterId;
        return ISS_SUCCESS;
    }
    else
    {
        return ISS_FAILURE;
    }
}
