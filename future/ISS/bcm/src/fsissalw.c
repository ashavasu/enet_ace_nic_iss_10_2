/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissalw.c,v 1.63 2017/09/12 13:24:11 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsissalw.h"
# include  "issexinc.h"
# include  "fsisselw.h"
# include  "isscli.h"
# include  "aclcli.h"
# include  "diffsrv.h"
# include  "qosxtd.h"
# include "ipv6.h"
# include  "issexmacr.h"

extern INT1 nmhGetIssAclTrafficSeperationCtrl ARG_LIST ((INT4 *));
extern INT4         gISSOutFilterCount;

/* LOW LEVEL Routines for Table : IssAclRateCtrlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssAclRateCtrlTable
 Input       :  The Indices
                IssAclRateCtrlIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssAclRateCtrlTable (INT4 i4IssAclRateCtrlIndex)
{
    if (IssExtSnmpLowValidateIndexPortRateTable (i4IssAclRateCtrlIndex) ==
        ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssAclRateCtrlTable
 Input       :  The Indices
                IssAclRateCtrlIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssAclRateCtrlTable (INT4 *pi4IssAclRateCtrlIndex)
{
    return (nmhGetNextIndexIssAclRateCtrlTable (0, pi4IssAclRateCtrlIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIssAclRateCtrlTable
 Input       :  The Indices
                IssAclRateCtrlIndex
                nextIssAclRateCtrlIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIssAclRateCtrlTable (INT4 i4IssAclRateCtrlIndex,
                                    INT4 *pi4NextIssAclRateCtrlIndex)
{
    INT4                i4Count = 0;

    if ((i4IssAclRateCtrlIndex < 0)
        || (i4IssAclRateCtrlIndex >= BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        return SNMP_FAILURE;
    }

    for (i4Count = i4IssAclRateCtrlIndex + 1;
         i4Count <= BRG_MAX_PHY_PLUS_LOG_PORTS; i4Count++)
    {
        if ((gIssExGlobalInfo.apIssRateCtrlEntry[i4Count]) != NULL)
        {
            *pi4NextIssAclRateCtrlIndex = i4Count;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIssAclRateCtrlDLFLimitValue
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                retValIssAclRateCtrlDLFLimitValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclRateCtrlDLFLimitValue (INT4 i4IssAclRateCtrlIndex,
                                   INT4 *pi4RetValIssAclRateCtrlDLFLimitValue)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry;

    if (IssExtValidateRateCtrlEntry (i4IssAclRateCtrlIndex) != ISS_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    *pi4RetValIssAclRateCtrlDLFLimitValue =
        (INT4) pIssAclRateCtrlEntry->u4IssRateCtrlDLFLimitValue;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIssAclRateCtrlBCASTLimitValue
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                retValIssAclRateCtrlBCASTLimitValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclRateCtrlBCASTLimitValue (INT4 i4IssAclRateCtrlIndex,
                                     INT4
                                     *pi4RetValIssAclRateCtrlBCASTLimitValue)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry;

    if (IssExtValidateRateCtrlEntry (i4IssAclRateCtrlIndex) != ISS_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    *pi4RetValIssAclRateCtrlBCASTLimitValue =
        (INT4) pIssAclRateCtrlEntry->u4IssRateCtrlBCASTLimitValue;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIssAclRateCtrlMCASTLimitValue
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                retValIssAclRateCtrlMCASTLimitValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclRateCtrlMCASTLimitValue (INT4 i4IssAclRateCtrlIndex,
                                     INT4
                                     *pi4RetValIssAclRateCtrlMCASTLimitValue)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry;

    if (IssExtValidateRateCtrlEntry (i4IssAclRateCtrlIndex) != ISS_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    *pi4RetValIssAclRateCtrlMCASTLimitValue =
        (INT4) pIssAclRateCtrlEntry->u4IssRateCtrlMCASTLimitValue;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIssAclRateCtrlPortRateLimit
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                retValIssAclRateCtrlPortRateLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclRateCtrlPortRateLimit (INT4 i4IssAclRateCtrlIndex,
                                   INT4 *pi4RetValIssAclRateCtrlPortRateLimit)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry = NULL;

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    if (pIssAclRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIssAclRateCtrlPortRateLimit =
        pIssAclRateCtrlEntry->i4IssRateCtrlPortLimitRate;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIssAclRateCtrlPortBurstSize
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                retValIssAclRateCtrlPortBurstSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclRateCtrlPortBurstSize (INT4 i4IssAclRateCtrlIndex,
                                   INT4 *pi4RetValIssAclRateCtrlPortBurstSize)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry = NULL;

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    if (pIssAclRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIssAclRateCtrlPortBurstSize =
        pIssAclRateCtrlEntry->i4IssRateCtrlPortBurstRate;

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIssAclRateCtrlDLFLimitValue
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                setValIssAclRateCtrlDLFLimitValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclRateCtrlDLFLimitValue (INT4 i4IssAclRateCtrlIndex,
                                   INT4 i4SetValIssAclRateCtrlDLFLimitValue)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    if (pIssAclRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclRateCtrlEntry->u4IssRateCtrlDLFLimitValue == (UINT4)
        i4SetValIssAclRateCtrlDLFLimitValue)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    i4RetVal = IsssysIssHwSetRateLimitingValue ((UINT4) i4IssAclRateCtrlIndex,
                                                ISS_RATE_DLF,
                                                i4SetValIssAclRateCtrlDLFLimitValue);

    if (i4RetVal != FNP_SUCCESS)
    {
        if (i4RetVal == FNP_NOT_SUPPORTED)
        {
            CLI_SET_ERR (CLI_ISS_INVALID_RATE);
        }

        return SNMP_FAILURE;
    }
#endif

    pIssAclRateCtrlEntry->u4IssRateCtrlDLFLimitValue = (UINT4)
        i4SetValIssAclRateCtrlDLFLimitValue;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssAclRateCtrlBCASTLimitValue
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                setValIssAclRateCtrlBCASTLimitValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclRateCtrlBCASTLimitValue (INT4 i4IssAclRateCtrlIndex,
                                     INT4 i4SetValIssAclRateCtrlBCASTLimitValue)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    if (pIssAclRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclRateCtrlEntry->u4IssRateCtrlBCASTLimitValue == (UINT4)
        i4SetValIssAclRateCtrlBCASTLimitValue)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    i4RetVal = IsssysIssHwSetRateLimitingValue ((UINT4) i4IssAclRateCtrlIndex,
                                                ISS_RATE_BCAST,
                                                i4SetValIssAclRateCtrlBCASTLimitValue);

    if (i4RetVal != FNP_SUCCESS)
    {
        if (i4RetVal == FNP_NOT_SUPPORTED)
        {
            CLI_SET_ERR (CLI_ISS_INVALID_RATE);
        }

        return SNMP_FAILURE;
    }
#endif

    pIssAclRateCtrlEntry->u4IssRateCtrlBCASTLimitValue = (UINT4)
        i4SetValIssAclRateCtrlBCASTLimitValue;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssAclRateCtrlMCASTLimitValue
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                setValIssAclRateCtrlMCASTLimitValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclRateCtrlMCASTLimitValue (INT4 i4IssAclRateCtrlIndex,
                                     INT4 i4SetValIssAclRateCtrlMCASTLimitValue)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    if (pIssAclRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclRateCtrlEntry->u4IssRateCtrlMCASTLimitValue == (UINT4)
        i4SetValIssAclRateCtrlMCASTLimitValue)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    i4RetVal = IsssysIssHwSetRateLimitingValue ((UINT4) i4IssAclRateCtrlIndex,
                                                ISS_RATE_MCAST,
                                                i4SetValIssAclRateCtrlMCASTLimitValue);

    if (i4RetVal != FNP_SUCCESS)
    {
        if (i4RetVal == FNP_NOT_SUPPORTED)
        {
            CLI_SET_ERR (CLI_ISS_INVALID_RATE);
        }
        return SNMP_FAILURE;
    }
#endif

    pIssAclRateCtrlEntry->u4IssRateCtrlMCASTLimitValue = (UINT4)
        i4SetValIssAclRateCtrlMCASTLimitValue;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssAclRateCtrlPortRateLimit
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                setValIssAclRateCtrlPortRateLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclRateCtrlPortRateLimit (INT4 i4IssAclRateCtrlIndex,
                                   INT4 i4SetValIssAclRateCtrlPortRateLimit)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif
    INT4                i4SetValIssAclRateCtrlPortBurstSize = 0;

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    if (pIssAclRateCtrlEntry == NULL)
    {

        return SNMP_FAILURE;
    }

    if ((INT4) (pIssAclRateCtrlEntry->i4IssRateCtrlPortLimitRate) ==
        i4SetValIssAclRateCtrlPortRateLimit)
    {
        return SNMP_SUCCESS;
    }

    i4SetValIssAclRateCtrlPortBurstSize =
        pIssAclRateCtrlEntry->i4IssRateCtrlPortBurstRate;

#ifdef NPAPI_WANTED
    i4RetVal = IsssysIssHwSetPortEgressPktRate ((UINT4) i4IssAclRateCtrlIndex,
                                                i4SetValIssAclRateCtrlPortRateLimit,
                                                i4SetValIssAclRateCtrlPortBurstSize);

    if (i4RetVal != FNP_SUCCESS)
    {
        if (i4RetVal == FNP_NOT_SUPPORTED)
        {
            CLI_SET_ERR (CLI_ISS_INVALID_RATE);
        }
        return SNMP_FAILURE;
    }
#endif
    pIssAclRateCtrlEntry->i4IssRateCtrlPortLimitRate =
        i4SetValIssAclRateCtrlPortRateLimit;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssAclRateCtrlPortBurstSize
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                setValIssAclRateCtrlPortBurstSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclRateCtrlPortBurstSize (INT4 i4IssAclRateCtrlIndex,
                                   INT4 i4SetValIssAclRateCtrlPortBurstSize)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif
    INT4                i4SetValIssAclRateCtrlPortRateLimit = 0;

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    if (pIssAclRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((INT4) (pIssAclRateCtrlEntry->i4IssRateCtrlPortBurstRate) ==
        i4SetValIssAclRateCtrlPortBurstSize)
    {
        return SNMP_SUCCESS;
    }

    i4SetValIssAclRateCtrlPortRateLimit =
        pIssAclRateCtrlEntry->i4IssRateCtrlPortLimitRate;

#ifdef NPAPI_WANTED
    i4RetVal = IsssysIssHwSetPortEgressPktRate
        ((UINT4) i4IssAclRateCtrlIndex, i4SetValIssAclRateCtrlPortRateLimit,
         i4SetValIssAclRateCtrlPortBurstSize);

    if (i4RetVal != FNP_SUCCESS)
    {
        if (i4RetVal == FNP_NOT_SUPPORTED)
        {
            CLI_SET_ERR (CLI_ISS_INVALID_RATE);
        }
        return SNMP_FAILURE;
    }
#endif

    pIssAclRateCtrlEntry->i4IssRateCtrlPortBurstRate =
        i4SetValIssAclRateCtrlPortBurstSize;
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IssAclRateCtrlDLFLimitValue
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                testValIssAclRateCtrlDLFLimitValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclRateCtrlDLFLimitValue (UINT4 *pu4ErrorCode,
                                      INT4 i4IssAclRateCtrlIndex,
                                      INT4 i4TestValIssAclRateCtrlDLFLimitValue)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclRateCtrlDLFLimitValue function \n");
    if (IssExtValidateRateCtrlEntry (i4IssAclRateCtrlIndex) != ISS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nLOW:Function IssExtValidateRateCtrlEntry failed with RateCtrl Index %d and                 error code as %d\n",
                      i4IssAclRateCtrlIndex, *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclRateCtrlDLFLimitValue < 0)
        || (i4TestValIssAclRateCtrlDLFLimitValue > RATE_LIMIT_MAX_VALUE))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nLOW:Rate Ctrl LimitValue  is less than zero or exits the maximum value %d                        with errorcode as %d\n",
                      RATE_LIMIT_MAX_VALUE, *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclRateCtrlDLFLimitValue function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclRateCtrlBCASTLimitValue
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                testValIssAclRateCtrlBCASTLimitValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclRateCtrlBCASTLimitValue (UINT4 *pu4ErrorCode,
                                        INT4 i4IssAclRateCtrlIndex,
                                        INT4
                                        i4TestValIssAclRateCtrlBCASTLimitValue)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclRateCtrlBCASTLimitValue function \n");
    if (IssExtValidateRateCtrlEntry (i4IssAclRateCtrlIndex) != ISS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nLOW:Function IssExtValidateRateCtrlEntry"
                      "failed with RateCtrl Index %d and" "error code as %d\n",
                      i4IssAclRateCtrlIndex, *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclRateCtrlBCASTLimitValue < 0)
        || (i4TestValIssAclRateCtrlBCASTLimitValue > RATE_LIMIT_MAX_VALUE))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nLOW:Rate Ctrl BCAST LimitValue  is less than zero or exits the maximum value %d                        with errorcode as %d\n",
                      RATE_LIMIT_MAX_VALUE, *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclRateCtrlBCASTLimitValue function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclRateCtrlMCASTLimitValue
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                testValIssAclRateCtrlMCASTLimitValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclRateCtrlMCASTLimitValue (UINT4 *pu4ErrorCode,
                                        INT4 i4IssAclRateCtrlIndex,
                                        INT4
                                        i4TestValIssAclRateCtrlMCASTLimitValue)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclRateCtrlMCASTLimitValue function \n");
    if (IssExtValidateRateCtrlEntry (i4IssAclRateCtrlIndex) != ISS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nLOW:Function IssExtValidateRateCtrlEntry failed with RateCtrl Index %d and                 error code as %d\n",
                      i4IssAclRateCtrlIndex, *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclRateCtrlMCASTLimitValue < 0)
        || (i4TestValIssAclRateCtrlMCASTLimitValue > RATE_LIMIT_MAX_VALUE))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nLOW:Rate Ctrl MCAST LimitValue  is less than zero or exits the maximum value %d                        with errorcode as %d\n",
                      RATE_LIMIT_MAX_VALUE, *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclRateCtrlMCASTLimitValue function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclRateCtrlPortRateLimit
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                testValIssAclRateCtrlPortRateLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclRateCtrlPortRateLimit (UINT4 *pu4ErrorCode,
                                      INT4 i4IssAclRateCtrlIndex,
                                      INT4 i4TestValIssAclRateCtrlPortRateLimit)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclRateCtrlPortRateLimit function \n");
    if (IssExtValidateRateCtrlEntry (i4IssAclRateCtrlIndex) != ISS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nLOW:Function IssExtValidateRateCtrlEntry failed with RateCtrl Index %d and                 error code as %d\n",
                      i4IssAclRateCtrlIndex, *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    if (pIssAclRateCtrlEntry == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nLOW:pIssAclRateCtrlEntry value is NULL");
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclRateCtrlPortRateLimit < 0) ||
        (i4TestValIssAclRateCtrlPortRateLimit > 80000000))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:Rate Ctrl Port LimitValue  is less than zero or exits the  value 80000000                         with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclRateCtrlPortRateLimit function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclRateCtrlPortBurstSize
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                testValIssAclRateCtrlPortBurstSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclRateCtrlPortBurstSize (UINT4 *pu4ErrorCode,
                                      INT4 i4IssAclRateCtrlIndex,
                                      INT4 i4TestValIssAclRateCtrlPortBurstSize)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclRateCtrlPortBurstSize function \n");
    if (IssExtValidateRateCtrlEntry (i4IssAclRateCtrlIndex) != ISS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nLOW:Function IssExtValidateRateCtrlEntry failed with RateCtrl Index %d and                 error code as %d\n",
                      i4IssAclRateCtrlIndex, *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    if (pIssAclRateCtrlEntry == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nLOW:pIssAclRateCtrlEntry value is NULL");
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclRateCtrlPortBurstSize < 0) ||
        (i4TestValIssAclRateCtrlPortBurstSize > 80000000))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:Rate Ctrl Port Burst size  is less than zero or exits the  value 80000000                         with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclRateCtrlPortBurstSize function \n");
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IssAclRateCtrlTable
 Input       :  The Indices
                IssAclRateCtrlIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IssAclRateCtrlTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IssAclL2FilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssAclL2FilterTable
 Input       :  The Indices
                IssAclL2FilterNo
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssAclL2FilterTable (INT4 i4IssAclL2FilterNo)
{
    tIssSllNode        *pSllNode = NULL;
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) != ISS_TRUE)
    {
        return SNMP_FAILURE;
    }
    pSllNode = ISS_SLL_FIRST (&ISS_L2FILTER_LIST);
    while (pSllNode != NULL)
    {
        pIssAclL2FilterEntry = (tIssL2FilterEntry *) pSllNode;
        if (i4IssAclL2FilterNo == pIssAclL2FilterEntry->i4IssL2FilterNo)
        {
            return SNMP_SUCCESS;
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L2FILTER_LIST, pSllNode);
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssAclL2FilterTable
 Input       :  The Indices
                IssAclL2FilterNo
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssAclL2FilterTable (INT4 *pi4IssAclL2FilterNo)
{
    if ((IssExtSnmpLowGetFirstValidL2FilterTableIndex (pi4IssAclL2FilterNo)) ==
        ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhGetNextIndexIssAclL2FilterTable
 Input       :  The Indices
                IssAclL2FilterNo
                nextIssAclL2FilterNo
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIssAclL2FilterTable (INT4 i4IssAclL2FilterNo,
                                    INT4 *pi4NextIssAclL2FilterNo)
{
    if (i4IssAclL2FilterNo < 0)
    {
        return SNMP_FAILURE;
    }

    if ((IssExtSnmpLowGetNextValidL2FilterTableIndex
         (i4IssAclL2FilterNo, pi4NextIssAclL2FilterNo)) == ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterPriority
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterPriority (INT4 i4IssAclL2FilterNo,
                              INT4 *pi4RetValIssAclL2FilterPriority)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        *pi4RetValIssAclL2FilterPriority =
            pIssAclL2FilterEntry->i4IssL2FilterPriority;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterEtherType
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterEtherType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterEtherType (INT4 i4IssAclL2FilterNo,
                               INT4 *pi4RetValIssAclL2FilterEtherType)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        *pi4RetValIssAclL2FilterEtherType =
            pIssAclL2FilterEntry->u2InnerEtherType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterProtocolType
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterProtocolType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterProtocolType (INT4 i4IssAclL2FilterNo,
                                  UINT4 *pu4RetValIssAclL2FilterProtocolType)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        *pu4RetValIssAclL2FilterProtocolType =
            pIssAclL2FilterEntry->u4IssL2FilterProtocolType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterDstMacAddr
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterDstMacAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterDstMacAddr (INT4 i4IssAclL2FilterNo,
                                tMacAddr * pRetValIssAclL2FilterDstMacAddr)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        ISS_MEMCPY ((UINT1 *) pRetValIssAclL2FilterDstMacAddr,
                    pIssAclL2FilterEntry->IssL2FilterDstMacAddr,
                    ISS_ETHERNET_ADDR_SIZE);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterSrcMacAddr
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterSrcMacAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterSrcMacAddr (INT4 i4IssAclL2FilterNo,
                                tMacAddr * pRetValIssAclL2FilterSrcMacAddr)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        ISS_MEMCPY ((UINT1 *) pRetValIssAclL2FilterSrcMacAddr,
                    pIssAclL2FilterEntry->IssL2FilterSrcMacAddr,
                    ISS_ETHERNET_ADDR_SIZE);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterVlanId
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterVlanId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterVlanId (INT4 i4IssAclL2FilterNo,
                            INT4 *pi4RetValIssAclL2FilterVlanId)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        *pi4RetValIssAclL2FilterVlanId =
            (INT4) pIssAclL2FilterEntry->u4IssL2FilterCustomerVlanId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterInPortList
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterInPortList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterInPortList (INT4 i4IssAclL2FilterNo,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValIssAclL2FilterInPortList)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        ISS_MEMSET (pRetValIssAclL2FilterInPortList->pu1_OctetList, 0,
                    ISS_PORT_LIST_SIZE);
        ISS_ADD_PORT_LIST (pRetValIssAclL2FilterInPortList->pu1_OctetList,
                           pIssAclL2FilterEntry->IssL2FilterInPortList);
        pRetValIssAclL2FilterInPortList->i4_Length = ISS_PORT_LIST_SIZE;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterAction
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterAction (INT4 i4IssAclL2FilterNo,
                            INT4 *pi4RetValIssAclL2FilterAction)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        *pi4RetValIssAclL2FilterAction =
            pIssAclL2FilterEntry->IssL2FilterAction;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterMatchCount
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterMatchCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterMatchCount (INT4 i4IssAclL2FilterNo,
                                UINT4 *pu4RetValIssAclL2FilterMatchCount)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

#ifdef NPAPI_WANTED
    INT4                i4RetVal = FNP_FAILURE;
#endif

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhGetIssAclL2FilterMatchCount function \n");

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nLOW:pIssAclL2FilterEntry is NULL \n");
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    if ((pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE) &&
        (pIssAclL2FilterEntry->i4IssL2FilterStatsEnabledStatus
         == ACL_STAT_ENABLE))
    {
        i4RetVal = IsssysIssHwUpdateL2Filter (pIssAclL2FilterEntry,
                                              ISS_L2FILTER_STAT_GET);
        if (i4RetVal != FNP_SUCCESS)
        {
            *pu4RetValIssAclL2FilterMatchCount = 0;
            ISS_TRC (ALL_FAILURE_TRC,
                     "\nLOW:L2 filter failed to get hardware stats \n");
            return SNMP_SUCCESS;
        }
    }
#endif
    *pu4RetValIssAclL2FilterMatchCount =
        pIssAclL2FilterEntry->u4IssL2FilterMatchCount;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhGetIssAclL2FilterMatchCount function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterDirection
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterDirection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterDirection (INT4 i4IssAclL2FilterNo,
                               INT4 *pi4RetValIssAclL2FilterDirection)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        *pi4RetValIssAclL2FilterDirection =
            (INT4) pIssAclL2FilterEntry->u1FilterDirection;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterStatus
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterStatus (INT4 i4IssAclL2FilterNo,
                            INT4 *pi4RetValIssAclL2FilterStatus)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        *pi4RetValIssAclL2FilterStatus =
            (INT4) pIssAclL2FilterEntry->u1IssL2FilterStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterOutPortList
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterOutPortList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterOutPortList (INT4 i4IssAclL2FilterNo,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValIssAclL2FilterOutPortList)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        ISS_MEMSET (pRetValIssAclL2FilterOutPortList->pu1_OctetList, 0,
                    ISS_PORT_LIST_SIZE);
        ISS_ADD_PORT_LIST (pRetValIssAclL2FilterOutPortList->pu1_OctetList,
                           pIssAclL2FilterEntry->IssL2FilterOutPortList);
        pRetValIssAclL2FilterOutPortList->i4_Length = ISS_PORT_LIST_SIZE;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterCreationMode
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterCreationMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterCreationMode (INT4 i4IssAclL2FilterNo,
                                  INT4 *pi4RetValIssAclL2FilterCreationMode)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        *pi4RetValIssAclL2FilterCreationMode =
            (INT4) pIssAclL2FilterEntry->u1IssL2FilterCreationMode;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterRedirectId
 Input       :  The Indices
                IssAclL2FilterNo

                The Object
                retValIssAclL2FilterRedirectId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterRedirectId (INT4 i4IssAclL2FilterNo,
                                INT4 *pi4RetValIssAclL2FilterRedirectId)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        *pi4RetValIssAclL2FilterRedirectId =
            (INT4) pIssAclL2FilterEntry->RedirectIfGrp.u4RedirectGrpId;
        return SNMP_SUCCESS;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterInPortChannelList
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterInPortChannelList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterInPortChannelList (INT4 i4IssAclL2FilterNo,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pRetValIssAclL2FilterInPortChannelList)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        ISS_MEMSET (pRetValIssAclL2FilterInPortChannelList->pu1_OctetList, 0,
                    ISS_PORT_CHANNEL_LIST_SIZE);
        ISS_ADD_PORT_CHANNEL_LIST (pRetValIssAclL2FilterInPortChannelList->
                                   pu1_OctetList,
                                   pIssAclL2FilterEntry->
                                   IssL2FilterInPortChannelList);
        pRetValIssAclL2FilterInPortChannelList->i4_Length =
            ISS_PORT_CHANNEL_LIST_SIZE;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterOutPortChannelList
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterOutPortChannelList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterOutPortChannelList (INT4 i4IssAclL2FilterNo,
                                        tSNMP_OCTET_STRING_TYPE
                                        *
                                        pRetValIssAclL2FilterOutPortChannelList)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        ISS_MEMSET (pRetValIssAclL2FilterOutPortChannelList->pu1_OctetList, 0,
                    ISS_PORT_CHANNEL_LIST_SIZE);
        ISS_ADD_PORT_CHANNEL_LIST (pRetValIssAclL2FilterOutPortChannelList->
                                   pu1_OctetList,
                                   pIssAclL2FilterEntry->
                                   IssL2FilterOutPortChannelList);
        pRetValIssAclL2FilterOutPortChannelList->i4_Length =
            ISS_PORT_CHANNEL_LIST_SIZE;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterStatsEnabledStatus
 Input       :  The Indices
                IssAclL2FilterNo

                The Object
                retValIssAclL2FilterStatsEnabledStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterStatsEnabledStatus (INT4 i4IssAclL2FilterNo,
                                        INT4
                                        *pi4RetValIssAclL2FilterStatsEnabledStatus)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhGetIssAclL2FilterStatsEnabledStatus function \n");

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nLOW:pIssAclL2FilterEntry is NULL \n");
        return SNMP_FAILURE;
    }

    *pi4RetValIssAclL2FilterStatsEnabledStatus =
        pIssAclL2FilterEntry->i4IssL2FilterStatsEnabledStatus;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhGetIssAclL2FilterStatsEnabledStatus function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIssAclClearL2FilterStats
 Input       :  The Indices
                IssAclL2FilterNo

                The Object
                retValIssAclClearL2FilterStats
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclClearL2FilterStats (INT4 i4IssAclL2FilterNo,
                                INT4 *pi4RetValIssAclClearL2FilterStats)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhGetIssAclClearL2FilterStats function \n");

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nLOW:pIssAclL2FilterEntry is NULL \n");
        return SNMP_FAILURE;
    }

    *pi4RetValIssAclClearL2FilterStats =
        pIssAclL2FilterEntry->i4IssClearL2FilterStats;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhGetIssAclClearL2FilterStats function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterSChannelIfIndex
 Input       :  The Indices
                IssAclL2FilterNo

                The Object
                pu4RetValIssAclL2FilterSChannelIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterSChannelIfIndex (INT4 i4IssAclL2FilterNo,
                                     INT4
                                     *pi4RetValIssAclL2FilterSChannelIfIndex)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhGetIssAclL2FilterSChannelIfIndex function \n");

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nLOW:pIssAclL2FilterEntry is NULL \n");
        return SNMP_FAILURE;
    }

    *pi4RetValIssAclL2FilterSChannelIfIndex =
        (INT4) pIssAclL2FilterEntry->u4SChannelIfIndex;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhGetIssAclL2FilterSChannelIfIndex function \n");

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterPriority
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterPriority (INT4 i4IssAclL2FilterNo,
                              INT4 i4SetValIssAclL2FilterPriority)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL2FilterEntry->i4IssL2FilterPriority =
            i4SetValIssAclL2FilterPriority;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterEtherType
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterEtherType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterEtherType (INT4 i4IssAclL2FilterNo,
                               INT4 i4SetValIssAclL2FilterEtherType)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL2FilterEntry->u2InnerEtherType = (UINT2)
            i4SetValIssAclL2FilterEtherType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterProtocolType
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterProtocolType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterProtocolType (INT4 i4IssAclL2FilterNo,
                                  UINT4 u4SetValIssAclL2FilterProtocolType)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL2FilterEntry->u4IssL2FilterProtocolType =
            u4SetValIssAclL2FilterProtocolType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterDstMacAddr
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterDstMacAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterDstMacAddr (INT4 i4IssAclL2FilterNo,
                                tMacAddr SetValIssAclL2FilterDstMacAddr)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        ISS_MEMSET (pIssAclL2FilterEntry->IssL2FilterDstMacAddr, 0,
                    ISS_ETHERNET_ADDR_SIZE);
        ISS_MEMCPY (pIssAclL2FilterEntry->IssL2FilterDstMacAddr,
                    SetValIssAclL2FilterDstMacAddr, ISS_ETHERNET_ADDR_SIZE);

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterSrcMacAddr
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterSrcMacAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterSrcMacAddr (INT4 i4IssAclL2FilterNo,
                                tMacAddr SetValIssAclL2FilterSrcMacAddr)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {

        if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        ISS_MEMSET (pIssAclL2FilterEntry->IssL2FilterSrcMacAddr, 0,
                    ISS_ETHERNET_ADDR_SIZE);
        ISS_MEMCPY (pIssAclL2FilterEntry->IssL2FilterSrcMacAddr,
                    SetValIssAclL2FilterSrcMacAddr, ISS_ETHERNET_ADDR_SIZE);

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterVlanId
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterVlanId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterVlanId (INT4 i4IssAclL2FilterNo,
                            INT4 i4SetValIssAclL2FilterVlanId)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL2FilterEntry->u4IssL2FilterCustomerVlanId =
            (UINT4) i4SetValIssAclL2FilterVlanId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterInPortList
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterInPortList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterInPortList (INT4 i4IssAclL2FilterNo,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValIssAclL2FilterInPortList)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        ISS_MEMSET (pIssAclL2FilterEntry->IssL2FilterInPortList, 0,
                    ISS_PORT_LIST_SIZE);
        ISS_MEMCPY (pIssAclL2FilterEntry->IssL2FilterInPortList,
                    pSetValIssAclL2FilterInPortList->pu1_OctetList,
                    pSetValIssAclL2FilterInPortList->i4_Length);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterAction
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterAction (INT4 i4IssAclL2FilterNo,
                            INT4 i4SetValIssAclL2FilterAction)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL2FilterEntry->IssL2FilterAction = i4SetValIssAclL2FilterAction;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterStatus
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterStatus (INT4 i4IssAclL2FilterNo,
                            INT4 i4SetValIssAclL2FilterStatus)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    UINT4               u4UapIfIndex = 0;
    UINT2               u2SVId = 0;
    INT4                i4SchL2FilterStatus = 0;
    UINT1               u1L3FilterStatus = FALSE;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif

    tIssPortList        IssL2FilterNullPortList;
    tIssPortChannelList IssL2FilterNullPortChannelList;

    ISS_MEMSET (IssL2FilterNullPortList, 0, sizeof (tIssPortList));
    ISS_MEMSET (IssL2FilterNullPortChannelList, 0,
                sizeof (tIssPortChannelList));
    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        if (pIssAclL2FilterEntry->u1IssL2FilterStatus ==
            (UINT1) i4SetValIssAclL2FilterStatus)
        {
            return SNMP_SUCCESS;
        }

        if (i4SetValIssAclL2FilterStatus == ISS_CREATE_AND_WAIT)
        {
            CLI_SET_ERR (CLI_ACL_FILTER_EXISTS);
            return SNMP_FAILURE;
        }

    }
    else
    {
        /* This Filter Entry is not there in the Database */
        if ((i4SetValIssAclL2FilterStatus != ISS_CREATE_AND_WAIT))
        {
            CLI_SET_ERR (CLI_ACL_NO_FILTER);
            return SNMP_FAILURE;
        }
    }

    switch (i4SetValIssAclL2FilterStatus)
    {

        case ISS_CREATE_AND_WAIT:

            if ((ISS_L2FILTERENTRY_ALLOC_MEM_BLOCK (pIssAclL2FilterEntry)) ==
                NULL)
            {
                ISS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "No Free L2 Filter Entry"
                         "or h/w init failed at startup\n");
                return SNMP_FAILURE;
            }

            ISS_MEMSET (pIssAclL2FilterEntry, 0, sizeof (tIssL2FilterEntry));

            if (ISS_FILTER_SHADOW_ALLOC_MEM_BLOCK
                (pIssAclL2FilterEntry->pIssFilterShadowInfo) == NULL)
            {
                ISS_L2FILTERENTRY_FREE_MEM_BLOCK (pIssAclL2FilterEntry);
                ISS_TRC (BUFFER_TRC, "No Free Filter Shadow Entry\n");
                return SNMP_FAILURE;
            }

            ISS_MEMSET (pIssAclL2FilterEntry->pIssFilterShadowInfo,
                        ISS_ZERO_ENTRY, sizeof (tIssFilterShadowTable));

            /* Setting all the default values for the objects */
            pIssAclL2FilterEntry->i4IssL2FilterNo = i4IssAclL2FilterNo;
            pIssAclL2FilterEntry->i4IssL2FilterPriority =
                ISS_DEFAULT_FILTER_PRIORITY;

            pIssAclL2FilterEntry->u4IssL2FilterProtocolType =
                ISS_DEFAULT_PROTOCOL_TYPE;

            pIssAclL2FilterEntry->i1IssL2FilterCVlanPriority =
                ISS_DEFAULT_VLAN_PRIORITY;
            pIssAclL2FilterEntry->i1IssL2FilterSVlanPriority =
                ISS_DEFAULT_VLAN_PRIORITY;

            pIssAclL2FilterEntry->u4IssL2FilterCustomerVlanId = 0;
            pIssAclL2FilterEntry->u4IssL2FilterServiceVlanId = 0;

            pIssAclL2FilterEntry->u2InnerEtherType = 0;
            pIssAclL2FilterEntry->u2OuterEtherType = 0;
            pIssAclL2FilterEntry->u1IssL2FilterTagType = ISS_FILTER_SINGLE_TAG;

            pIssAclL2FilterEntry->u1FilterDirection = ISS_DIRECTION_IN;

            pIssAclL2FilterEntry->IssL2FilterAction = ISS_ALLOW;

            pIssAclL2FilterEntry->u4IssL2FilterMatchCount = 0;
            pIssAclL2FilterEntry->u1IssL2FilterCreationMode
                = ISS_ACL_CREATION_EXTERNAL;
            pIssAclL2FilterEntry->u4IssL2RedirectId = 0;
            pIssAclL2FilterEntry->i4IssClearL2FilterStats = ISS_FALSE;
            pIssAclL2FilterEntry->i4IssL2FilterStatsEnabledStatus =
                ACL_STAT_DISABLE;
            pIssAclL2FilterEntry->u4StatsTransitFlag = ISS_FALSE;
            pIssAclL2FilterEntry->u1IssL2OutFilterCount = ISS_FALSE;

#ifdef NPAPI_WANTED
            i4RetVal =
                IsssysIssHwUpdateL2Filter (pIssAclL2FilterEntry,
                                           ISS_L2FILTER_STAT_DISABLE);
            if (i4RetVal != FNP_SUCCESS)
            {
                ISS_TRC (ALL_FAILURE_TRC,
                         "\nLOW:L2 filter cannot disable hardware stats \n");
            }
#endif
            /* Adding the Entry to the Filter List */
            ISS_SLL_ADD (&(ISS_L2FILTER_LIST),
                         &(pIssAclL2FilterEntry->IssNextNode));

            /* Setting the RowStatus to NOT_READY if the
             * filter if i4SetValIssAclL2FilterStatus is ISS_CREATE_AND_WAIT*/

            pIssAclL2FilterEntry->u1IssL2FilterStatus = ISS_NOT_READY;

            break;

        case ISS_NOT_IN_SERVICE:

            if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
            {
#ifdef NPAPI_WANTED

                /* If counter is enabled then set flag to detach
                 * and reattach counter after row status becomes ACTIVE */
                if (pIssAclL2FilterEntry->i4IssL2FilterStatsEnabledStatus
                    == ACL_STAT_ENABLE)
                {
                    i4RetVal =
                        IsssysIssHwUpdateL2Filter (pIssAclL2FilterEntry,
                                                   ISS_L2FILTER_STAT_DISABLE);

                    if (i4RetVal != FNP_SUCCESS)
                    {
                        ISS_TRC (ALL_FAILURE_TRC,
                                 "\nLOW:L2 filter cannot disable hardware stats \n");
                        CLI_SET_ERR (CLI_ACL_HW_UPDATE_FAILED);
                        return SNMP_FAILURE;
                    }
                }

                /* Call the hardware API to update the filter */
                i4RetVal =
                    IsssysIssHwUpdateL2Filter (pIssAclL2FilterEntry,
                                               ISS_L2FILTER_DELETE);

                if (i4RetVal != FNP_SUCCESS)
                {
                    CLI_SET_ERR (CLI_ACL_HW_UPDATE_FAILED);
                    return SNMP_FAILURE;
                }
                if (pIssAclL2FilterEntry->u1FilterDirection == ISS_DIRECTION_OUT
                    && pIssAclL2FilterEntry->u1IssL2OutFilterCount == ISS_TRUE)
                {
                    gISSOutFilterCount--;
                    pIssAclL2FilterEntry->u1IssL2OutFilterCount = ISS_FALSE;
                }
#endif

                if (pIssAclL2FilterEntry->u4SChannelIfIndex != 0)
                {
                    /* When the Deletion of L2 Filter for S-Channel is Successful , 
                     * Disable L2 Filter Status for the S-Channel Interface */

                    /* Before setting the S-Ch Filter Status to DISABLE,
                     * Scan through the L3 table to check if L3 Filter was installed
                     * in the S-Ch Interface */

                    TMO_SLL_Scan (&ISS_L3FILTER_LIST, pIssL3FilterEntry,
                                  tIssL3FilterEntry *)
                    {
                        if (pIssL3FilterEntry->u1IssL3FilterStatus !=
                            ISS_ACTIVE)
                        {
                            continue;
                        }

                        if (pIssL3FilterEntry->u4SChannelIfIndex ==
                            pIssAclL2FilterEntry->u4SChannelIfIndex)
                        {
                            u1L3FilterStatus = TRUE;
                            break;
                        }
                    }

                    if (u1L3FilterStatus == FALSE)
                    {
                        if (VlanApiGetSChInfoFromSChIfIndex
                            (pIssAclL2FilterEntry->u4SChannelIfIndex,
                             &u4UapIfIndex, &u2SVId) != VLAN_FAILURE)
                        {
                            /* Set the S-Channel Filter Status to DISABLE */
                            VlanSetSChFilterStatus (u4UapIfIndex, u2SVId,
                                                    ACL_SCH_FILTER_DISABLE);
                        }
                    }
                }

                ISS_SLL_DELETE (&(ISS_L2FILTER_SORTED_LIST),
                                &(pIssAclL2FilterEntry->IssSortedNextNode));
                pIssAclL2FilterEntry->u1IssL2FilterStatus = ISS_NOT_IN_SERVICE;
                return SNMP_SUCCESS;
            }
            else
            {
                CLI_SET_ERR (CLI_ACL_FILTER_NOT_ACTIVE);
                return SNMP_FAILURE;
            }
            break;

        case ISS_ACTIVE:

            if (((ISS_MEMCMP (IssL2FilterNullPortList,
                              pIssAclL2FilterEntry->IssL2FilterInPortList,
                              sizeof (tIssPortList)) == 0)
                 && (ISS_MEMCMP (IssL2FilterNullPortList,
                                 pIssAclL2FilterEntry->IssL2FilterOutPortList,
                                 sizeof (tIssPortList)) == 0)) &&
                ((ISS_MEMCMP (IssL2FilterNullPortChannelList,
                              pIssAclL2FilterEntry->
                              IssL2FilterInPortChannelList,
                              sizeof (tIssPortChannelList)) == 0)
                 &&
                 (ISS_MEMCMP
                  (IssL2FilterNullPortChannelList,
                   pIssAclL2FilterEntry->IssL2FilterOutPortChannelList,
                   sizeof (tIssPortChannelList)) == 0))
                && (pIssAclL2FilterEntry->u4SChannelIfIndex == 0))
            {
                /* In Portlist and Port channel list is NULL.
                 * Filter cannot be made active
                 */
                CLI_SET_ERR (CLI_ACL_FILTER_NO_MEMBERS);
                return SNMP_FAILURE;
            }

            if (pIssAclL2FilterEntry->u4SChannelIfIndex != 0)
            {
                if (VlanApiGetSChInfoFromSChIfIndex
                    (pIssAclL2FilterEntry->u4SChannelIfIndex, &u4UapIfIndex,
                     &u2SVId) != VLAN_FAILURE)
                {
                    /* Get the Filter Status for the S-Channel Intf Index  */

                    i4SchL2FilterStatus =
                        VlanGetSChFilterStatus (u4UapIfIndex, u2SVId);
                    if (i4SchL2FilterStatus != ACL_SCH_FILTER_ENABLE)
                    {
                        CLI_SET_ERR (CLI_ACL_SCH_FILTER_ERROR);
                        return SNMP_FAILURE;
                    }

                }
            }
            /* Checking the L2 filter for the data sufficiency */
            if (IssExtQualifyL2FilterData (&pIssAclL2FilterEntry) !=
                ISS_SUCCESS)
            {
                CLI_SET_ERR (CLI_ACL_FILTER_DATA_INSUFFICIENCY);
                return SNMP_FAILURE;
            }

            if (pIssAclL2FilterEntry->u1IssL2FilterStatus != ISS_NOT_IN_SERVICE)
            {
                CLI_SET_ERR (CLI_ACL_FILTER_ACTIVE_FAILED);
                return SNMP_FAILURE;
            }

#ifdef NPAPI_WANTED
            {

                /* Call the hardware API to add the L2 filter */
                i4RetVal =
                    IsssysIssHwUpdateL2Filter (pIssAclL2FilterEntry,
                                               ISS_L2FILTER_ADD);
                if (i4RetVal != FNP_SUCCESS)
                {
                    if (i4RetVal == FNP_NOT_SUPPORTED)
                    {
                        CLI_SET_ERR (CLI_ACL_OUT_MULTIPLE_PORTS);
                    }
                    return SNMP_FAILURE;
                }
                if (pIssAclL2FilterEntry->u1FilterDirection == ISS_DIRECTION_OUT
                    && pIssAclL2FilterEntry->u1IssL2OutFilterCount == ISS_FALSE)
                {
                    gISSOutFilterCount++;
                    pIssAclL2FilterEntry->u1IssL2OutFilterCount = ISS_TRUE;
                }

            }
#endif

            pIssAclL2FilterEntry->u1IssL2FilterStatus = ISS_ACTIVE;

            /*Update Mirroring database */
            IssMirrCreateAcl (i4IssAclL2FilterNo, ISS_MIRR_L2_ACL);

#ifdef NPAPI_WANTED
            /* Enable counter if required */
            if (pIssAclL2FilterEntry->i4IssL2FilterStatsEnabledStatus
                == ACL_STAT_ENABLE)
            {

                i4RetVal =
                    IsssysIssHwUpdateL2Filter (pIssAclL2FilterEntry,
                                               ISS_L2FILTER_STAT_ENABLE);

                if (i4RetVal != FNP_SUCCESS)
                {
                    ISS_TRC (ALL_FAILURE_TRC,
                             "\nLOW:L2 filter cannot enable hardware stats \n");
                    CLI_SET_ERR (CLI_ACL_HW_UPDATE_FAILED);
                    return SNMP_FAILURE;
                }
            }
#endif

            break;

        case ISS_DESTROY:

#ifdef NPAPI_WANTED
            if (pIssAclL2FilterEntry->i4IssL2FilterStatsEnabledStatus
                == ACL_STAT_ENABLE)
            {
                pIssAclL2FilterEntry->i4IssL2FilterStatsEnabledStatus
                    = ACL_STAT_DISABLE;

                i4RetVal =
                    IsssysIssHwUpdateL2Filter (pIssAclL2FilterEntry,
                                               ISS_L2FILTER_STAT_DISABLE);

                if (i4RetVal != FNP_SUCCESS)
                {
                    ISS_TRC (ALL_FAILURE_TRC,
                             "\nLOW:L2 filter cannot disable hardware stats \n");
                }
            }
#endif

            if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
            {

#ifdef NPAPI_WANTED
                /* Call the hardware API to delete the filter */
                i4RetVal =
                    IsssysIssHwUpdateL2Filter (pIssAclL2FilterEntry,
                                               ISS_L2FILTER_DELETE);

                if (i4RetVal != FNP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
#endif

            }

            if ((pIssAclL2FilterEntry->u1FilterDirection == ISS_DIRECTION_OUT)
                && (pIssAclL2FilterEntry->u1IssL2OutFilterCount = ISS_TRUE))
            {
                gISSOutFilterCount--;
                pIssAclL2FilterEntry->u1IssL2OutFilterCount = ISS_FALSE;
            }
            /* Deleting the node from the list */
            ISS_SLL_DELETE (&(ISS_L2FILTER_LIST),
                            &(pIssAclL2FilterEntry->IssNextNode));
            /*Since ISS_L2FILTER_SORTED_LIST is copy of hardwre list,
             * removing entry from here also */

            ISS_SLL_DELETE (&(ISS_L2FILTER_SORTED_LIST),
                            &(pIssAclL2FilterEntry->IssSortedNextNode));
            if (ISS_FILTER_SHADOW_FREE_MEM_BLOCK
                (pIssAclL2FilterEntry->pIssFilterShadowInfo) != MEM_SUCCESS)
            {
                ISS_TRC (BUFFER_TRC | ALL_FAILURE_TRC,
                         "Filter Shadow Entry Free Failure\n");
                return SNMP_FAILURE;
            }
            /* Delete the Entry from the Software */
            if (ISS_L2FILTERENTRY_FREE_MEM_BLOCK (pIssAclL2FilterEntry)
                != MEM_SUCCESS)
            {
                ISS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "L2 Filter Entry Free Failure\n");
                return SNMP_FAILURE;
            }

            break;

        default:

            return SNMP_FAILURE;

    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterOutPortList
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterOutPortList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterOutPortList (INT4 i4IssAclL2FilterNo,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValIssAclL2FilterOutPortList)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;
    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        ISS_MEMSET (pIssAclL2FilterEntry->IssL2FilterOutPortList, 0,
                    ISS_PORT_LIST_SIZE);
        ISS_MEMCPY (pIssAclL2FilterEntry->IssL2FilterOutPortList,
                    pSetValIssAclL2FilterOutPortList->pu1_OctetList,
                    pSetValIssAclL2FilterOutPortList->i4_Length);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterDirection
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterDirection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterDirection (INT4 i4IssAclL2FilterNo,
                               INT4 i4SetValIssAclL2FilterDirection)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal = FNP_FAILURE;
    INT1                i1FilterDirectionOld = ACL_ACCESS_IN;
#endif

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
#ifdef NPAPI_WANTED
        i1FilterDirectionOld = (INT1) pIssAclL2FilterEntry->u1FilterDirection;
        if (i1FilterDirectionOld != i4SetValIssAclL2FilterDirection)
        {
            pIssAclL2FilterEntry->u4StatsTransitFlag = ISS_TRUE;
            i4RetVal =
                IsssysIssHwUpdateL2Filter (pIssAclL2FilterEntry,
                                           ISS_L2FILTER_STAT_DISABLE);
            if (i4RetVal != FNP_SUCCESS)
            {
                ISS_TRC (ALL_FAILURE_TRC,
                         "\nLOW:L2 filter cannot destroy hardware stats "
                         "during direction change\n");
                CLI_SET_ERR (CLI_ACL_HW_UPDATE_FAILED);
                return SNMP_FAILURE;
            }
            pIssAclL2FilterEntry->u4StatsTransitFlag = ISS_FALSE;
        }
#endif
        pIssAclL2FilterEntry->u1FilterDirection =
            (UINT1) i4SetValIssAclL2FilterDirection;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterInPortChannelList
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterInPortChannelList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterInPortChannelList (INT4 i4IssAclL2FilterNo,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pSetValIssAclL2FilterInPortChannelList)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        ISS_MEMSET (pIssAclL2FilterEntry->IssL2FilterInPortChannelList, 0,
                    ISS_PORT_CHANNEL_LIST_SIZE);
        ISS_MEMCPY (pIssAclL2FilterEntry->IssL2FilterInPortChannelList,
                    pSetValIssAclL2FilterInPortChannelList->pu1_OctetList,
                    pSetValIssAclL2FilterInPortChannelList->i4_Length);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterOutPortChannelList
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterOutPortChannelList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterOutPortChannelList (INT4 i4IssAclL2FilterNo,
                                        tSNMP_OCTET_STRING_TYPE
                                        *
                                        pSetValIssAclL2FilterOutPortChannelList)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;
    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        ISS_MEMSET (pIssAclL2FilterEntry->IssL2FilterOutPortChannelList, 0,
                    ISS_PORT_CHANNEL_LIST_SIZE);
        ISS_MEMCPY (pIssAclL2FilterEntry->IssL2FilterOutPortChannelList,
                    pSetValIssAclL2FilterOutPortChannelList->pu1_OctetList,
                    pSetValIssAclL2FilterOutPortChannelList->i4_Length);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterStatsEnabledStatus
 Input       :  The Indices
                IssAclL2FilterNo

                The Object
                setValIssAclL2FilterStatsEnabledStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterStatsEnabledStatus (INT4 i4IssAclL2FilterNo,
                                        INT4
                                        i4SetValIssAclL2FilterStatsEnabledStatus)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

#ifdef NPAPI_WANTED
    INT4                i4RetVal = FNP_FAILURE;
    INT4                i4HwStatsFlag;
    INT4                i4StatusOld;
#endif

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhSetIssAclL2FilterStatsEnabledStatus function \n");

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nLOW:pIssAclL2FilterEntry is NULL \n");
        CLI_SET_ERR (CLI_ACL_NO_FILTER);
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    i4StatusOld = pIssAclL2FilterEntry->i4IssL2FilterStatsEnabledStatus;
#endif
    pIssAclL2FilterEntry->i4IssL2FilterStatsEnabledStatus =
        i4SetValIssAclL2FilterStatsEnabledStatus;

#ifdef NPAPI_WANTED
    if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        if (i4SetValIssAclL2FilterStatsEnabledStatus == ACL_STAT_ENABLE)
        {
            i4HwStatsFlag = ISS_L2FILTER_STAT_ENABLE;
        }
        else
        {
            i4HwStatsFlag = ISS_L2FILTER_STAT_DISABLE;
        }

        i4RetVal = IsssysIssHwUpdateL2Filter (pIssAclL2FilterEntry,
                                              i4HwStatsFlag);
        if (i4RetVal != FNP_SUCCESS)
        {
            pIssAclL2FilterEntry->i4IssL2FilterStatsEnabledStatus = i4StatusOld;
            ISS_TRC (ALL_FAILURE_TRC,
                     "\nLOW:L2 filter cannot enable hardware stats \n");
            CLI_SET_ERR (CLI_ACL_HW_UPDATE_FAILED);
            return SNMP_FAILURE;
        }
    }
#endif

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhSetIssAclL2FilterStatsEnabledStatus function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssAclClearL2FilterStats
 Input       :  The Indices
                IssAclL2FilterNo

                The Object
                setValIssAclClearL2FilterStats
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclClearL2FilterStats (INT4 i4IssAclL2FilterNo,
                                INT4 i4SetValIssAclClearL2FilterStats)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

#ifdef NPAPI_WANTED
    INT4                i4RetVal = FNP_FAILURE;
#endif

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhSetIssAclClearL2FilterStats function \n");

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nLOW:pIssAclL2FilterEntry is NULL \n");
        CLI_SET_ERR (CLI_ACL_FILTER_DATA_INSUFFICIENCY);
        return SNMP_FAILURE;
    }

    pIssAclL2FilterEntry->i4IssClearL2FilterStats =
        i4SetValIssAclClearL2FilterStats;
#ifdef NPAPI_WANTED
    if (pIssAclL2FilterEntry->i4IssL2FilterStatsEnabledStatus
        == ACL_STAT_ENABLE)
    {
        i4RetVal = IsssysIssHwUpdateL2Filter (pIssAclL2FilterEntry,
                                              ISS_L2FILTER_STAT_CLEAR);

        if (i4RetVal != FNP_SUCCESS)
        {
            pIssAclL2FilterEntry->i4IssClearL2FilterStats = ISS_FALSE;
            ISS_TRC (ALL_FAILURE_TRC,
                     "\nLOW:L2 filter failed to clear hardware stats \n");
            CLI_SET_ERR (CLI_ACL_HW_UPDATE_FAILED);
            return SNMP_FAILURE;
        }
    }
#endif
    pIssAclL2FilterEntry->u4IssL2FilterMatchCount = 0;
    pIssAclL2FilterEntry->i4IssClearL2FilterStats = ISS_FALSE;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhSetIssAclClearL2FilterStats function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterSChannelIfIndex
 Input       :  The Indices
                IssAclL2FilterNo

                The Object
                u4SetValIssAclL2FilterSChannelIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterSChannelIfIndex (INT4 i4IssAclL2FilterNo,
                                     UINT4
                                     u4SetValIssAclL2FilterSChannelIfIndex)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhSetIssAclL2FilterSChannelIfIndex function \n");

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nLOW:pIssAclL2FilterEntry is NULL \n");
        CLI_SET_ERR (CLI_ACL_FILTER_DATA_INSUFFICIENCY);
        return SNMP_FAILURE;
    }

    pIssAclL2FilterEntry->u4SChannelIfIndex =
        u4SetValIssAclL2FilterSChannelIfIndex;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhSetIssAclL2FilterSChannelIfIndex function \n");
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterPriority
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterPriority (UINT4 *pu4ErrorCode, INT4 i4IssAclL2FilterNo,
                                 INT4 i4TestValIssAclL2FilterPriority)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL2FilterPriority function \n");
    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L2 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL2FilterPriority <= ISS_MAX_FILTER_PRIORITY) &&
        (i4TestValIssAclL2FilterPriority >= ISS_DEFAULT_FILTER_PRIORITY))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L2 filter priority level above the maximum range    with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL2FilterPriority function \n");
}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterEtherType
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterEtherType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterEtherType (UINT4 *pu4ErrorCode, INT4 i4IssAclL2FilterNo,
                                  INT4 i4TestValIssAclL2FilterEtherType)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry tv2IssAclL2FilterEtherType function \n");
    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L2 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL2FilterEtherType < ISS_MIN_ETHER_TYPE_VAL)
        || (i4TestValIssAclL2FilterEtherType > ISS_MAX_ETHER_TYPE_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L2 filter ether type range  is less than zero or exits the  value 65535                          with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit tv2IssAclL2FilterEtherType function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterProtocolType
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterProtocolType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterProtocolType (UINT4 *pu4ErrorCode,
                                     INT4 i4IssAclL2FilterNo,
                                     UINT4 u4TestValIssAclL2FilterProtocolType)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL2FilterProtocolType function \n");
    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L2 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    if (((u4TestValIssAclL2FilterProtocolType < ISS_MIN_PROTOCOL_ID)
         || (u4TestValIssAclL2FilterProtocolType > ISS_MAX_PROTOCOL_ID))
        && (u4TestValIssAclL2FilterProtocolType != ISS_ZERO_ENTRY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nLOW:L2 filter protocol type is above minimum protocol ID %d                                      with errorcode as %d\n",
                      ISS_MIN_PROTOCOL_ID, *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL2FilterProtocolType function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterDstMacAddr
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterDstMacAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterDstMacAddr (UINT4 *pu4ErrorCode, INT4 i4IssAclL2FilterNo,
                                   tMacAddr TestValIssAclL2FilterDstMacAddr)
{
    tMacAddr            zeroAddr;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL2FilterDstMacAddr function \n");
    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L2 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    ISS_MEMSET (zeroAddr, 0, ISS_ETHERNET_ADDR_SIZE);

    if (!ISS_MEMCMP
        (TestValIssAclL2FilterDstMacAddr, zeroAddr, ISS_ETHERNET_ADDR_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL2FilterDstMacAddr function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterSrcMacAddr
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterSrcMacAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterSrcMacAddr (UINT4 *pu4ErrorCode, INT4 i4IssAclL2FilterNo,
                                   tMacAddr TestValIssAclL2FilterSrcMacAddr)
{
    tMacAddr            zeroAddr;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL2FilterSrcMacAddr function \n");
    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L2 filter ID is Invalid ID with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    ISS_MEMSET (zeroAddr, 0, ISS_ETHERNET_ADDR_SIZE);

    if (!ISS_MEMCMP
        (TestValIssAclL2FilterSrcMacAddr, zeroAddr, ISS_ETHERNET_ADDR_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL2FilterSrcMacAddr function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterVlanId
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterVlanId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterVlanId (UINT4 *pu4ErrorCode, INT4 i4IssAclL2FilterNo,
                               INT4 i4TestValIssAclL2FilterVlanId)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL2FilterVlanId function \n");
    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L2 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL2FilterVlanId < 0) ||
        (i4TestValIssAclL2FilterVlanId > VLAN_DEV_MAX_VLAN_ID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nLOW:L2 filter VlanId is less than zero or above the max vlan ID %d  with errorcode as %d\n",
                      VLAN_DEV_MAX_VLAN_ID, *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL2FilterVlanId function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterInPortList
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterInPortList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterInPortList (UINT4 *pu4ErrorCode, INT4 i4IssAclL2FilterNo,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValIssAclL2FilterInPortList)
{
    UINT4               u4IfIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2Port = 0;
    UINT1               u1PortFlag = 0;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL2FilterInPortList function \n");
    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L2 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    for (u4IfIndex = 0; u4IfIndex < ISS_PORT_LIST_SIZE; u4IfIndex++)
    {
        u1PortFlag =
            (UINT1) pTestValIssAclL2FilterInPortList->pu1_OctetList[u4IfIndex];

        for (u2BitIndex = 0;
             ((u2BitIndex < BITS_PER_BYTE) && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & ISS_BIT8) != 0)
            {
                u2Port = (UINT2) ((u4IfIndex * 8) + u2BitIndex + 1);
                if (CfaValidateIfIndex ((UINT4) u2Port) == CFA_SUCCESS)
                {

                    if (AclValidateIfMemberoOfPortChannel (u2Port) ==
                        CLI_FAILURE)
                    {
                        CLI_SET_ERR (CLI_ACL_SET_ERROR_ON_PORT_CHANNEL);
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        return SNMP_FAILURE;
                    }

                }

            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }
    if ((pTestValIssAclL2FilterInPortList->i4_Length <= 0) ||
        (pTestValIssAclL2FilterInPortList->i4_Length > ISS_PORT_LIST_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:failed to test L2 filter InPortList length is less than zero with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    /* This function checks if any port greater than the maximum number of
     * ports in the system is in the port list.*/

    if (IssIsPortListValid
        (pTestValIssAclL2FilterInPortList->pu1_OctetList,
         pTestValIssAclL2FilterInPortList->i4_Length) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:failed to test L2 filter InPortList with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL2FilterInPortList function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterAction
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterAction (UINT4 *pu4ErrorCode, INT4 i4IssAclL2FilterNo,
                               INT4 i4TestValIssAclL2FilterAction)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL2FilterAction function \n");
    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L2 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    /* Order should not be changed for the values of Enum tIssFilterAction  */

    if ((i4TestValIssAclL2FilterAction >= ISS_ALLOW)
        || (i4TestValIssAclL2FilterAction <= ISS_DROP_COPYTOCPU))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:failed to test L2 filter action with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL2FilterAction function \n");
}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterStatus
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterStatus (UINT4 *pu4ErrorCode, INT4 i4IssAclL2FilterNo,
                               INT4 i4TestValIssAclL2FilterStatus)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;
    INT4                i4RetStatus = ISS_ZERO_ENTRY;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL2FilterStatus function \n");
    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L2 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        CLI_SET_ERR (CLI_ACL_INVALID_FILTER_ID);
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL2FilterStatus < ISS_ACTIVE)
        || (i4TestValIssAclL2FilterStatus > ISS_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nLOW:L2 filter status is below the  value of ISS_ACTIVE %d with errorcode as %d\n",
                      ISS_ACTIVE, *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if ((pIssAclL2FilterEntry == NULL)
        && (ISS_L2FILTER_LIST_COUNT == ISS_MAX_L2_FILTERS))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssAclL2FilterEntry value is  NULL with errorcode as %d\n",
                      *pu4ErrorCode);
        CLI_SET_ERR (CLI_ACL_FILTER_MAX);
        return SNMP_FAILURE;
    }

    if (pIssAclL2FilterEntry == NULL &&
        (i4TestValIssAclL2FilterStatus != ISS_CREATE_AND_WAIT
         && i4TestValIssAclL2FilterStatus != ISS_CREATE_AND_GO))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssAclL2FilterEntry value is not NULL with errorcode as %d\n",
                      *pu4ErrorCode);
        CLI_SET_ERR (CLI_ACL_NO_FILTER);
        return SNMP_FAILURE;
    }

    if (pIssAclL2FilterEntry != NULL &&
        (i4TestValIssAclL2FilterStatus == ISS_CREATE_AND_WAIT
         || i4TestValIssAclL2FilterStatus == ISS_CREATE_AND_GO))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssAclL2FilterEntry value is not equal to  NULL with errorcode as %d\n",
                      *pu4ErrorCode);
        CLI_SET_ERR (CLI_ACL_FILTER_EXISTS);
        return SNMP_FAILURE;
    }

    if (nmhGetIssAclTrafficSeperationCtrl (&i4RetStatus) == SNMP_SUCCESS)
    {
        if (i4RetStatus == ACL_TRAFFICSEPRTN_CTRL_SYSTEM_DEFAULT)
        {
            if ((pIssAclL2FilterEntry != NULL) &&
                (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE &&
                 i4TestValIssAclL2FilterStatus == ISS_DESTROY) &&
                (pIssAclL2FilterEntry->u1IssL2FilterCreationMode ==
                 ISS_ACL_CREATION_INTERNAL))
            {
                CLI_SET_ERR (CLI_ACL_DELETION_FAILED);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nLOW:pIssAclL2FilterEntry value is not equal to NULL with errorcode as %d\n",
                              *pu4ErrorCode);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
    }

#ifdef QOSX_WANTED
    /* Check if the filter is mapped to CLass Map entry. If its mapped 
     * filter should not be deleted. */
    if ((i4TestValIssAclL2FilterStatus == ISS_DESTROY)
        || (i4TestValIssAclL2FilterStatus == ISS_NOT_IN_SERVICE))
    {
        if (QoSUtlIsFilterMapToClsMapEntry (QOS_L2FILTER, i4IssAclL2FilterNo)
            == TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ACL_FILTER_MAPPED_TO_CLSMAP);
            return SNMP_FAILURE;
        }
    }
#endif

#if defined (DIFFSRV_WANTED)
    if ((i4TestValIssAclL2FilterStatus == ISS_DESTROY)
        || (i4TestValIssAclL2FilterStatus == ISS_NOT_IN_SERVICE))
    {
        ISS_UNLOCK ();

        if (DsCheckMFClfrTable (i4IssAclL2FilterNo, DS_MAC_FILTER) ==
            DS_FAILURE)
        {
            CLI_SET_ERR (CLI_ACL_FILTER_NO_CHANGE);
            ISS_LOCK ();
            return SNMP_FAILURE;
        }

        ISS_LOCK ();
    }
#endif
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL2FilterStatus function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterOutPortList
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterOutPortList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterOutPortList (UINT4 *pu4ErrorCode,
                                    INT4 i4IssAclL2FilterNo,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValIssAclL2FilterOutPortList)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL2FilterOutPortList function \n");
    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L2 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssAclL2FilterEntry value is  NULL with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L2FilterStatus is Active with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if (pTestValIssAclL2FilterOutPortList->i4_Length <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L2 L2FilterOutPortList length is below the zero range with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    /* This function checks if any port greater than the maximum number of
     * ports in the system is in the port list.*/

    if (IssIsPortListValid (pTestValIssAclL2FilterOutPortList->pu1_OctetList,
                            pTestValIssAclL2FilterOutPortList->i4_Length) ==
        ISS_FALSE)
    {
        if (pTestValIssAclL2FilterOutPortList->i4_Length > ISS_PORT_LIST_SIZE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        }
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nLOW:failed to test L2 filter outPortList\n");
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL2FilterOutPortList function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterDirection
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterDirection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterDirection (UINT4 *pu4ErrorCode, INT4 i4IssAclL2FilterNo,
                                  INT4 i4TestValIssAclL2FilterDirection)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL2FilterDirection function \n");
    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L2 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssAclL2FilterEntry value is  NULL with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L2FilterStatus is Active with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if (i4TestValIssAclL2FilterDirection != ISS_DIRECTION_IN &&
        i4TestValIssAclL2FilterDirection != ISS_DIRECTION_OUT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:Validate L2 filter Direction with errorcode as %d\n",
                      *pu4ErrorCode);

        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL2FilterDirection function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterInPortChannelList
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterInPortChannelList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterInPortChannelList (UINT4 *pu4ErrorCode,
                                          INT4 i4IssAclL2FilterNo,
                                          tSNMP_OCTET_STRING_TYPE
                                          *
                                          pTestValIssAclL2FilterInPortChannelList)
{

    UINT4               u4IfIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2Port = 0;
    UINT1               u1PortFlag = 0;
    UINT1               u1IfType = 0;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL2FilterInPortChannelList function \n");
    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L2 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    for (u4IfIndex = 0; u4IfIndex < ISS_PORT_LIST_SIZE; u4IfIndex++)
    {
        u1PortFlag =
            (UINT1) pTestValIssAclL2FilterInPortChannelList->
            pu1_OctetList[u4IfIndex];

        for (u2BitIndex = 0;
             ((u2BitIndex < BITS_PER_BYTE) && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & ISS_BIT8) != 0)
            {
                u2Port = (UINT2) ((u4IfIndex * 8) + u2BitIndex + 1);
                if (CfaValidateIfIndex ((UINT4) u2Port) == CFA_SUCCESS)
                {
                    CfaGetIfType ((UINT4) u2Port, &u1IfType);
                    if (u1IfType != CFA_LAGG)
                    {
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        return SNMP_FAILURE;
                    }

                    if (AclValidatePortFromPortChannel (u2Port) == CLI_FAILURE)
                    {
                        CLI_SET_ERR (CLI_ACL_SET_ERROR_ON_PORT);
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        return SNMP_FAILURE;

                    }

                }
                else
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }

    if ((pTestValIssAclL2FilterInPortChannelList->i4_Length <= 0) ||
        (pTestValIssAclL2FilterInPortChannelList->i4_Length >
         ISS_PORT_CHANNEL_LIST_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L2 L2FilterOutPorChanneltList length is below the zero range or above the channel list size with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    /* This function checks if any port greater than the maximum number of
     * ports in the system is in the port channel list.*/

    if (IssIsPortListValid
        (pTestValIssAclL2FilterInPortChannelList->pu1_OctetList,
         pTestValIssAclL2FilterInPortChannelList->i4_Length) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL2FilterInPortChannelList function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterOutPortChannelList
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterOutPortChannelList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterOutPortChannelList (UINT4 *pu4ErrorCode,
                                           INT4 i4IssAclL2FilterNo,
                                           tSNMP_OCTET_STRING_TYPE
                                           *
                                           pTestValIssAclL2FilterOutPortChannelList)
{
    UINT4               u4IfIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2Port = 0;
    UINT1               u1PortFlag = 0;
    UINT1               u1IfType = 0;

    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL2FilterOutPortChannelList function \n");
    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L2 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssAclL2FilterEntry value is  NULL with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L2FilterStatus is Active with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    for (u4IfIndex = 0; u4IfIndex < ISS_PORT_LIST_SIZE; u4IfIndex++)
    {
        u1PortFlag =
            (UINT1) pTestValIssAclL2FilterOutPortChannelList->
            pu1_OctetList[u4IfIndex];

        for (u2BitIndex = 0;
             ((u2BitIndex < BITS_PER_BYTE) && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & ISS_BIT8) != 0)
            {
                u2Port = (UINT2) ((u4IfIndex * 8) + u2BitIndex + 1);
                if (CfaValidateIfIndex ((UINT4) u2Port) == CFA_SUCCESS)
                {
                    CfaGetIfType ((UINT4) u2Port, &u1IfType);
                    if (u1IfType != CFA_LAGG)
                    {
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        return SNMP_FAILURE;
                    }
                }
                else
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }

    if (pTestValIssAclL2FilterOutPortChannelList->i4_Length <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L2 L2FilterOutPortList length is below the zero range with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    /* This function checks if any port greater than the maximum number of
     * ports in the system is in the port list.*/

    if (IssIsPortListValid
        (pTestValIssAclL2FilterOutPortChannelList->pu1_OctetList,
         pTestValIssAclL2FilterOutPortChannelList->i4_Length) == ISS_FALSE)
    {
        if (pTestValIssAclL2FilterOutPortChannelList->i4_Length >
            ISS_PORT_CHANNEL_LIST_SIZE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        }
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL2FilterOutPortChannelList function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterSChannelIfIndex
 Input       :  The Indices
                IssAclL2FilterNo

                The Object
                u4TestValIssAclL2FilterSChannelIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterSChannelIfIndex (UINT4 *pu4ErrorCode,
                                        INT4 i4IssAclL2FilterNo,
                                        UINT4
                                        u4TestValIssAclL2FilterSChannelIfIndex)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL2FilterSChannelIfIndex function \n");

    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L2 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssAclL2FilterEntry value is  NULL with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L2FilterStatus is Active with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((u4TestValIssAclL2FilterSChannelIfIndex < CFA_MIN_EVB_SBP_INDEX) &&
        (u4TestValIssAclL2FilterSChannelIfIndex > CFA_MAX_EVB_SBP_INDEX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:Invalid S-Channel Index with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterStatsEnabledStatus
 Input       :  The Indices
                IssAclL2FilterNo

                The Object
                testValIssAclL2FilterStatsEnabledStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterStatsEnabledStatus (UINT4 *pu4ErrorCode,
                                           INT4 i4IssAclL2FilterNo,
                                           INT4
                                           i4TestValIssAclL2FilterStatsEnabledStatus)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL2FilterStatsEnabledStatus function \n");

    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L2 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        CLI_SET_ERR (CLI_ACL_INVALID_FILTER_ID);
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL2FilterStatsEnabledStatus != ACL_STAT_ENABLE)
        && (i4TestValIssAclL2FilterStatsEnabledStatus != ACL_STAT_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L2 filter hardware stats enabled status is invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        CLI_SET_ERR (CLI_ACL_FILTER_DATA_INSUFFICIENCY);
        return SNMP_FAILURE;
    }

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL2FilterStatsEnabledStatus function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssAclClearL2FilterStats
 Input       :  The Indices
                IssAclL2FilterNo

                The Object
                testValIssAclClearL2FilterStats
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclClearL2FilterStats (UINT4 *pu4ErrorCode,
                                   INT4 i4IssAclL2FilterNo,
                                   INT4 i4TestValIssAclClearL2FilterStats)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclClearL2FilterStats function \n");

    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L2 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        CLI_SET_ERR (CLI_ACL_INVALID_FILTER_ID);
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclClearL2FilterStats != ISS_TRUE) &&
        (i4TestValIssAclClearL2FilterStats != ISS_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L2 filter hardware stats clear status is invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        CLI_SET_ERR (CLI_ACL_FILTER_DATA_INSUFFICIENCY);
        return SNMP_FAILURE;
    }

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclClearL2FilterStats function \n");
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IssAclL2FilterTable
 Input       :  The Indices
                IssAclL2FilterNo
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IssAclL2FilterTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IssAclL3FilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssAclL3FilterTable
 Input       :  The Indices
                IssAclL3FilterNo
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssAclL3FilterTable (INT4 i4IssAclL3FilterNo)
{
    tIssSllNode        *pSllNode = NULL;
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pSllNode = ISS_SLL_FIRST (&ISS_L3FILTER_LIST);

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) != ISS_TRUE)
    {
        return SNMP_FAILURE;
    }
    while (pSllNode != NULL)
    {
        pIssAclL3FilterEntry = (tIssL3FilterEntry *) pSllNode;
        if (pIssAclL3FilterEntry->i4IssL3FilterNo == i4IssAclL3FilterNo)
        {
            return SNMP_SUCCESS;
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L3FILTER_LIST, pSllNode);
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssAclL3FilterTable
 Input       :  The Indices
                IssAclL3FilterNo
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssAclL3FilterTable (INT4 *pi4IssAclL3FilterNo)
{
    if ((IssExtSnmpLowGetFirstValidL3FilterTableIndex (pi4IssAclL3FilterNo)) ==
        ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhGetNextIndexIssAclL3FilterTable
 Input       :  The Indices
                IssAclL3FilterNo
                nextIssAclL3FilterNo
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIssAclL3FilterTable (INT4 i4IssAclL3FilterNo,
                                    INT4 *pi4NextIssAclL3FilterNo)
{
    if (i4IssAclL3FilterNo < 0)
    {
        return SNMP_FAILURE;
    }

    if ((IssExtSnmpLowGetNextValidL3FilterTableIndex
         (i4IssAclL3FilterNo, pi4NextIssAclL3FilterNo)) == ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterPriority
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterPriority (INT4 i4IssAclL3FilterNo,
                              INT4 *pi4RetValIssAclL3FilterPriority)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterPriority =
            pIssAclL3FilterEntry->i4IssL3FilterPriority;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterProtocol
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterProtocol (INT4 i4IssAclL3FilterNo,
                              INT4 *pi4RetValIssAclL3FilterProtocol)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterProtocol =
            pIssAclL3FilterEntry->IssL3FilterProtocol;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterMessageType
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterMessageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterMessageType (INT4 i4IssAclL3FilterNo,
                                 INT4 *pi4RetValIssAclL3FilterMessageType)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterMessageType =
            pIssAclL3FilterEntry->i4IssL3FilterMessageType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterMessageCode
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterMessageCode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterMessageCode (INT4 i4IssAclL3FilterNo,
                                 INT4 *pi4RetValIssAclL3FilterMessageCode)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterMessageCode =
            pIssAclL3FilterEntry->i4IssL3FilterMessageCode;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilteAddrType
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilteAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilteAddrType (INT4 i4IssAclL3FilterNo,
                             INT4 *pi4RetValIssAclL3FilteAddrType)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValIssAclL3FilteAddrType =
        pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterDstIpAddr
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterDstIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterDstIpAddr (INT4 i4IssAclL3FilterNo,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValIssAclL3FilterDstIpAddr)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV4)
        {
            PTR_ASSIGN4 (pRetValIssAclL3FilterDstIpAddr->pu1_OctetList,
                         pIssL3FilterEntry->u4IssL3FilterDstIpAddr);
            pRetValIssAclL3FilterDstIpAddr->i4_Length = QOS_IPV4_LEN;
            return SNMP_SUCCESS;
        }
        else if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV6)
        {
            Ip6AddrCopy ((tIp6Addr *) (VOID *)
                         pRetValIssAclL3FilterDstIpAddr->pu1_OctetList,
                         &pIssL3FilterEntry->ipv6DstIpAddress);

            pRetValIssAclL3FilterDstIpAddr->i4_Length = QOS_IPV6_LEN;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterSrcIpAddr
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterSrcIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterSrcIpAddr (INT4 i4IssAclL3FilterNo,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValIssAclL3FilterSrcIpAddr)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV4)
        {
            PTR_ASSIGN4 (pRetValIssAclL3FilterSrcIpAddr->pu1_OctetList,
                         pIssL3FilterEntry->u4IssL3FilterSrcIpAddr);
            pRetValIssAclL3FilterSrcIpAddr->i4_Length = QOS_IPV4_LEN;
            return SNMP_SUCCESS;
        }
        else if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV6)
        {
            Ip6AddrCopy ((tIp6Addr *) (VOID *) pRetValIssAclL3FilterSrcIpAddr->
                         pu1_OctetList, &pIssL3FilterEntry->ipv6SrcIpAddress);

            pRetValIssAclL3FilterSrcIpAddr->i4_Length = QOS_IPV6_LEN;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterDstIpAddrPrefixLength
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterDstIpAddrPrefixLength
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterDstIpAddrPrefixLength (INT4 i4IssAclL3FilterNo,
                                           UINT4
                                           *pu4RetValIssAclL3FilterDstIpAddrPrefixLength)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pu4RetValIssAclL3FilterDstIpAddrPrefixLength =
            pIssL3FilterEntry->u4IssL3FilterMultiFieldClfrDstPrefixLength;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterSrcIpAddrPrefixLength
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterSrcIpAddrPrefixLength
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterSrcIpAddrPrefixLength (INT4 i4IssAclL3FilterNo,
                                           UINT4
                                           *pu4RetValIssAclL3FilterSrcIpAddrPrefixLength)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);
    if (pIssL3FilterEntry != NULL)
    {
        *pu4RetValIssAclL3FilterSrcIpAddrPrefixLength =
            pIssL3FilterEntry->u4IssL3FilterMultiFieldClfrSrcPrefixLength;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterMinDstProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterMinDstProtPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterMinDstProtPort (INT4 i4IssAclL3FilterNo,
                                    UINT4
                                    *pu4RetValIssAclL3FilterMinDstProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pu4RetValIssAclL3FilterMinDstProtPort =
            pIssAclL3FilterEntry->u4IssL3FilterMinDstProtPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterMaxDstProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterMaxDstProtPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterMaxDstProtPort (INT4 i4IssAclL3FilterNo,
                                    UINT4
                                    *pu4RetValIssAclL3FilterMaxDstProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pu4RetValIssAclL3FilterMaxDstProtPort =
            pIssAclL3FilterEntry->u4IssL3FilterMaxDstProtPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterMinSrcProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterMinSrcProtPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterMinSrcProtPort (INT4 i4IssAclL3FilterNo,
                                    UINT4
                                    *pu4RetValIssAclL3FilterMinSrcProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pu4RetValIssAclL3FilterMinSrcProtPort =
            pIssAclL3FilterEntry->u4IssL3FilterMinSrcProtPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterMaxSrcProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterMaxSrcProtPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterMaxSrcProtPort (INT4 i4IssAclL3FilterNo,
                                    UINT4
                                    *pu4RetValIssAclL3FilterMaxSrcProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pu4RetValIssAclL3FilterMaxSrcProtPort =
            pIssAclL3FilterEntry->u4IssL3FilterMaxSrcProtPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterInPortList
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterInPortList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterInPortList (INT4 i4IssAclL3FilterNo,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValIssAclL3FilterInPortList)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        ISS_MEMSET (pRetValIssAclL3FilterInPortList->pu1_OctetList, 0,
                    ISS_PORT_LIST_SIZE);
        ISS_ADD_PORT_LIST (pRetValIssAclL3FilterInPortList->pu1_OctetList,
                           pIssAclL3FilterEntry->IssL3FilterInPortList);
        pRetValIssAclL3FilterInPortList->i4_Length = ISS_PORT_LIST_SIZE;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterOutPortList
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterOutPortList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterOutPortList (INT4 i4IssAclL3FilterNo,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValIssAclL3FilterOutPortList)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        ISS_MEMSET (pRetValIssAclL3FilterOutPortList->pu1_OctetList, 0,
                    ISS_PORT_LIST_SIZE);
        ISS_ADD_PORT_LIST (pRetValIssAclL3FilterOutPortList->pu1_OctetList,
                           pIssAclL3FilterEntry->IssL3FilterOutPortList);
        pRetValIssAclL3FilterOutPortList->i4_Length = ISS_PORT_LIST_SIZE;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterAckBit
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterAckBit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterAckBit (INT4 i4IssAclL3FilterNo,
                            INT4 *pi4RetValIssAclL3FilterAckBit)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterAckBit =
            pIssAclL3FilterEntry->IssL3FilterAckBit;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterRstBit
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterRstBit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterRstBit (INT4 i4IssAclL3FilterNo,
                            INT4 *pi4RetValIssAclL3FilterRstBit)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterRstBit =
            pIssAclL3FilterEntry->IssL3FilterRstBit;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterTos
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterTos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterTos (INT4 i4IssAclL3FilterNo,
                         INT4 *pi4RetValIssAclL3FilterTos)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterTos = pIssAclL3FilterEntry->IssL3FilterTos;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterDscp
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterDscp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterDscp (INT4 i4IssAclL3FilterNo,
                          INT4 *pi4RetValIssAclL3FilterDscp)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterDscp = pIssAclL3FilterEntry->i4IssL3FilterDscp;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterDirection
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterDirection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterDirection (INT4 i4IssAclL3FilterNo,
                               INT4 *pi4RetValIssAclL3FilterDirection)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterDirection =
            pIssAclL3FilterEntry->IssL3FilterDirection;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterAction
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterAction (INT4 i4IssAclL3FilterNo,
                            INT4 *pi4RetValIssAclL3FilterAction)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterAction =
            pIssAclL3FilterEntry->IssL3FilterAction;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterMatchCount
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterMatchCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterMatchCount (INT4 i4IssAclL3FilterNo,
                                UINT4 *pu4RetValIssAclL3FilterMatchCount)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

#ifdef NPAPI_WANTED
    INT4                i4RetVal = FNP_FAILURE;
#endif

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhGetIssAclL3FilterMatchCount function \n");

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nLOW:pIssAclL3FilterEntry is NULL \n");
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    if ((pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE) &&
        (pIssAclL3FilterEntry->i4IssL3FilterStatsEnabledStatus
         == ACL_STAT_ENABLE))
    {
        i4RetVal = IsssysIssHwUpdateL3Filter (pIssAclL3FilterEntry,
                                              ISS_L3FILTER_STAT_GET);
        if (i4RetVal != FNP_SUCCESS)
        {
            *pu4RetValIssAclL3FilterMatchCount = 0;
            ISS_TRC (ALL_FAILURE_TRC,
                     "\nLOW:L3 filter failed to get hardware stats \n");
            return SNMP_SUCCESS;
        }
    }
#endif
    *pu4RetValIssAclL3FilterMatchCount =
        pIssAclL3FilterEntry->u4IssL3FilterMatchCount;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhGetIssAclL3FilterMatchCount function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterFlowId
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterFlowId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterFlowId (INT4 i4IssAclL3FilterNo,
                            UINT4 *pu4RetValIssAclL3FilterFlowId)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);
    if (pIssL3FilterEntry != NULL)
    {
        *pu4RetValIssAclL3FilterFlowId =
            pIssL3FilterEntry->u4IssL3MultiFieldClfrFlowId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterStatus
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterStatus (INT4 i4IssAclL3FilterNo,
                            INT4 *pi4RetValIssAclL3FilterStatus)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterStatus =
            (INT4) pIssAclL3FilterEntry->u1IssL3FilterStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterCreationMode
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterCreationMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterCreationMode (INT4 i4IssAclL3FilterNo,
                                  INT4 *pi4RetValIssAclL3FilterCreationMode)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterCreationMode =
            (INT4) pIssAclL3FilterEntry->u1IssL3FilterCreationMode;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterRedirectId
 Input       :  The Indices
                IssAclL3FilterNo

                The Object
                retValIssAclL3FilterRedirectId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterRedirectId (INT4 i4IssAclL3FilterNo,
                                INT4 *pi4RetValIssAclL3FilterRedirectId)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterRedirectId =
            (INT4) pIssAclL3FilterEntry->RedirectIfGrp.u4RedirectGrpId;
        return SNMP_SUCCESS;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterInPortChannelList
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterInPortChannelList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterInPortChannelList (INT4 i4IssAclL3FilterNo,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pRetValIssAclL3FilterInPortChannelList)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        ISS_MEMSET (pRetValIssAclL3FilterInPortChannelList->pu1_OctetList, 0,
                    ISS_PORT_CHANNEL_LIST_SIZE);
        ISS_ADD_PORT_CHANNEL_LIST (pRetValIssAclL3FilterInPortChannelList->
                                   pu1_OctetList,
                                   pIssAclL3FilterEntry->
                                   IssL3FilterInPortChannelList);
        pRetValIssAclL3FilterInPortChannelList->i4_Length =
            ISS_PORT_CHANNEL_LIST_SIZE;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterOutPortChannelList
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterOutPortChannelList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterOutPortChannelList (INT4 i4IssAclL3FilterNo,
                                        tSNMP_OCTET_STRING_TYPE
                                        *
                                        pRetValIssAclL3FilterOutPortChannelList)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        ISS_MEMSET (pRetValIssAclL3FilterOutPortChannelList->pu1_OctetList, 0,
                    ISS_PORT_CHANNEL_LIST_SIZE);
        ISS_ADD_PORT_CHANNEL_LIST (pRetValIssAclL3FilterOutPortChannelList->
                                   pu1_OctetList,
                                   pIssAclL3FilterEntry->
                                   IssL3FilterOutPortChannelList);
        pRetValIssAclL3FilterOutPortChannelList->i4_Length =
            ISS_PORT_CHANNEL_LIST_SIZE;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterStatsEnabledStatus
 Input       :  The Indices
                IssAclL3FilterNo

                The Object
                retValIssAclL3FilterStatsEnabledStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterStatsEnabledStatus (INT4 i4IssAclL3FilterNo,
                                        INT4
                                        *pi4RetValIssAclL3FilterStatsEnabledStatus)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhGetIssAclL3FilterStatsEnabledStatus function \n");

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nLOW:pIssAclL3FilterEntry is NULL \n");
        return SNMP_FAILURE;
    }

    *pi4RetValIssAclL3FilterStatsEnabledStatus =
        pIssAclL3FilterEntry->i4IssL3FilterStatsEnabledStatus;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhGetIssAclL3FilterStatsEnabledStatus function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIssAclClearL3FilterStats
 Input       :  The Indices
                IssAclL3FilterNo

                The Object
                retValIssAclClearL3FilterStats
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclClearL3FilterStats (INT4 i4IssAclL3FilterNo,
                                INT4 *pi4RetValIssAclClearL3FilterStats)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhGetIssAclClearL3FilterStats function \n");

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nLOW:pIssAclL3FilterEntry is NULL \n");
        return SNMP_FAILURE;
    }

    *pi4RetValIssAclClearL3FilterStats =
        pIssAclL3FilterEntry->i4IssClearL3FilterStats;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhGetIssAclClearL3FilterStats function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterSChannelIfIndex
 Input       :  The Indices
                IssAclL3FilterNo

                The Object
                pu4retValIssAclL3FilterSChannelIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterSChannelIfIndex (INT4 i4IssAclL3FilterNo,
                                     INT4
                                     *pi4RetValIssAclL3FilterSChannelIfIndex)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhGetIssAclL3FilterSChannelIfIndex function \n");

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nLOW:pIssAclL3FilterEntry is NULL \n");
        return SNMP_FAILURE;
    }

    *pi4RetValIssAclL3FilterSChannelIfIndex =
        (INT4) pIssAclL3FilterEntry->u4SChannelIfIndex;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhGetIssAclL3FilterSChannelIfIndex function \n");

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterPriority
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterPriority (INT4 i4IssAclL3FilterNo,
                              INT4 i4SetValIssAclL3FilterPriority)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL3FilterEntry->i4IssL3FilterPriority =
            i4SetValIssAclL3FilterPriority;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterProtocol
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterProtocol
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterProtocol (INT4 i4IssAclL3FilterNo,
                              INT4 i4SetValIssAclL3FilterProtocol)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL3FilterEntry->IssL3FilterProtocol =
            i4SetValIssAclL3FilterProtocol;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterMessageType
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterMessageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterMessageType (INT4 i4IssAclL3FilterNo,
                                 INT4 i4SetValIssAclL3FilterMessageType)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL3FilterEntry->i4IssL3FilterMessageType =
            i4SetValIssAclL3FilterMessageType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterMessageCode
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterMessageCode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterMessageCode (INT4 i4IssAclL3FilterNo,
                                 INT4 i4SetValIssAclL3FilterMessageCode)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL3FilterEntry->i4IssL3FilterMessageCode =
            i4SetValIssAclL3FilterMessageCode;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilteAddrType
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilteAddrType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilteAddrType (INT4 i4IssAclL3FilterNo,
                             INT4 i4SetValIssAclL3FilteAddrType)
{
    if (IssSetL3FilterAddrType (i4IssAclL3FilterNo,
                                i4SetValIssAclL3FilteAddrType) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterDstIpAddr
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterDstIpAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterDstIpAddr (INT4 i4IssAclL3FilterNo,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValIssAclL3FilterDstIpAddr)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    INT4                i4RetStatus = SNMP_FAILURE;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);
    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV6)
        {
            Ip6AddrCopy (&pIssL3FilterEntry->ipv6DstIpAddress,
                         (tIp6Addr *) (VOID *) pSetValIssAclL3FilterDstIpAddr->
                         pu1_OctetList);
            i4RetStatus = SNMP_SUCCESS;
        }
        if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV4)
        {
            PTR_FETCH4 (pIssL3FilterEntry->u4IssL3FilterDstIpAddr,
                        pSetValIssAclL3FilterDstIpAddr->pu1_OctetList);

            i4RetStatus = SNMP_SUCCESS;
        }
    }
    if (i4RetStatus == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterSrcIpAddr
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterSrcIpAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterSrcIpAddr (INT4 i4IssAclL3FilterNo,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValIssAclL3FilterSrcIpAddr)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    INT4                i4RetStatus = SNMP_FAILURE;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV6)
        {
            Ip6AddrCopy (&pIssL3FilterEntry->ipv6SrcIpAddress,
                         (tIp6Addr *) (VOID *) pSetValIssAclL3FilterSrcIpAddr->
                         pu1_OctetList);
            i4RetStatus = SNMP_SUCCESS;
        }
        if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV4)
        {
            PTR_FETCH4 (pIssL3FilterEntry->u4IssL3FilterSrcIpAddr,
                        pSetValIssAclL3FilterSrcIpAddr->pu1_OctetList);

            i4RetStatus = SNMP_SUCCESS;
        }
    }

    if (i4RetStatus == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterDstIpAddrPrefixLength
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterDstIpAddrPrefixLength
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterDstIpAddrPrefixLength (INT4 i4IssAclL3FilterNo,
                                           UINT4
                                           u4SetValIssAclL3FilterDstIpAddrPrefixLength)
{
    if (IssSetL3FilterDstPrefixLength (i4IssAclL3FilterNo,
                                       u4SetValIssAclL3FilterDstIpAddrPrefixLength)
        != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterSrcIpAddrPrefixLength
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterSrcIpAddrPrefixLength
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterSrcIpAddrPrefixLength (INT4 i4IssAclL3FilterNo,
                                           UINT4
                                           u4SetValIssAclL3FilterSrcIpAddrPrefixLength)
{
    if (IssSetL3FilterSrcPrefixLength (i4IssAclL3FilterNo,
                                       u4SetValIssAclL3FilterSrcIpAddrPrefixLength)
        != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterMinDstProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterMinDstProtPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterMinDstProtPort (INT4 i4IssAclL3FilterNo,
                                    UINT4 u4SetValIssAclL3FilterMinDstProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL3FilterEntry->u4IssL3FilterMinDstProtPort =
            u4SetValIssAclL3FilterMinDstProtPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterMaxDstProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterMaxDstProtPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterMaxDstProtPort (INT4 i4IssAclL3FilterNo,
                                    UINT4 u4SetValIssAclL3FilterMaxDstProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL3FilterEntry->u4IssL3FilterMaxDstProtPort =
            u4SetValIssAclL3FilterMaxDstProtPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterMinSrcProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterMinSrcProtPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterMinSrcProtPort (INT4 i4IssAclL3FilterNo,
                                    UINT4 u4SetValIssAclL3FilterMinSrcProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL3FilterEntry->u4IssL3FilterMinSrcProtPort =
            u4SetValIssAclL3FilterMinSrcProtPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterMaxSrcProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterMaxSrcProtPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterMaxSrcProtPort (INT4 i4IssAclL3FilterNo,
                                    UINT4 u4SetValIssAclL3FilterMaxSrcProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL3FilterEntry->u4IssL3FilterMaxSrcProtPort =
            u4SetValIssAclL3FilterMaxSrcProtPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterInPortList
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterInPortList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterInPortList (INT4 i4IssAclL3FilterNo,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValIssAclL3FilterInPortList)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        ISS_MEMSET (pIssAclL3FilterEntry->IssL3FilterInPortList, 0,
                    ISS_PORT_LIST_SIZE);
        ISS_MEMCPY (pIssAclL3FilterEntry->IssL3FilterInPortList,
                    pSetValIssAclL3FilterInPortList->pu1_OctetList,
                    pSetValIssAclL3FilterInPortList->i4_Length);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterOutPortList
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterOutPortList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterOutPortList (INT4 i4IssAclL3FilterNo,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValIssAclL3FilterOutPortList)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        ISS_MEMSET (pIssAclL3FilterEntry->IssL3FilterOutPortList, 0,
                    ISS_PORT_LIST_SIZE);
        ISS_MEMCPY (pIssAclL3FilterEntry->IssL3FilterOutPortList,
                    pSetValIssAclL3FilterOutPortList->pu1_OctetList,
                    pSetValIssAclL3FilterOutPortList->i4_Length);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterAckBit
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterAckBit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterAckBit (INT4 i4IssAclL3FilterNo,
                            INT4 i4SetValIssAclL3FilterAckBit)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL3FilterEntry->IssL3FilterAckBit = i4SetValIssAclL3FilterAckBit;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterRstBit
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterRstBit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterRstBit (INT4 i4IssAclL3FilterNo,
                            INT4 i4SetValIssAclL3FilterRstBit)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL3FilterEntry->IssL3FilterRstBit = i4SetValIssAclL3FilterRstBit;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterTos
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterTos
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterTos (INT4 i4IssAclL3FilterNo,
                         INT4 i4SetValIssAclL3FilterTos)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL3FilterEntry->IssL3FilterTos = i4SetValIssAclL3FilterTos;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterDscp
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterDscp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterDscp (INT4 i4IssAclL3FilterNo,
                          INT4 i4SetValIssAclL3FilterDscp)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL3FilterEntry->i4IssL3FilterDscp = i4SetValIssAclL3FilterDscp;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterDirection
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterDirection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterDirection (INT4 i4IssAclL3FilterNo,
                               INT4 i4SetValIssAclL3FilterDirection)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal = FNP_FAILURE;
    INT1                i1FilterDirectionOld = ACL_ACCESS_IN;
#endif

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

#ifdef NPAPI_WANTED
        i1FilterDirectionOld =
            (INT1) pIssAclL3FilterEntry->IssL3FilterDirection;
        if (i1FilterDirectionOld != i4SetValIssAclL3FilterDirection)
        {
            pIssAclL3FilterEntry->u4StatsTransitFlag = ISS_TRUE;
            i4RetVal =
                IsssysIssHwUpdateL3Filter (pIssAclL3FilterEntry,
                                           ISS_L3FILTER_STAT_DISABLE);
            if (i4RetVal != FNP_SUCCESS)
            {
                ISS_TRC (ALL_FAILURE_TRC,
                         "\nLOW:L3 filter cannot destroy hardware stats "
                         "during direction change\n");
                CLI_SET_ERR (CLI_ACL_HW_UPDATE_FAILED);
                return SNMP_FAILURE;
            }
            pIssAclL3FilterEntry->u4StatsTransitFlag = ISS_FALSE;
        }
#endif

        pIssAclL3FilterEntry->IssL3FilterDirection =
            i4SetValIssAclL3FilterDirection;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterAction
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterAction (INT4 i4IssAclL3FilterNo,
                            INT4 i4SetValIssAclL3FilterAction)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL3FilterEntry->IssL3FilterAction = i4SetValIssAclL3FilterAction;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterFlowId
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterFlowId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterFlowId (INT4 i4IssAclL3FilterNo,
                            UINT4 u4SetValIssAclL3FilterFlowId)
{
    if (IssSetL3FilterControlFlowId (i4IssAclL3FilterNo,
                                     u4SetValIssAclL3FilterFlowId) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterStatus
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterStatus (INT4 i4IssAclL3FilterNo,
                            INT4 i4SetValIssAclL3FilterStatus)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;
    UINT4               u4UapIfIndex = 0;
    UINT2               u2SVId = 0;
    INT4                i4SchL3FilterStatus = 0;
    UINT1               u1L2FilterStatus = FALSE;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif
    tIssPortList        IssL3FilterNullPortList;
    tIssPortChannelList IssL3FilterNullPortChannelList;

    ISS_MEMSET (IssL3FilterNullPortList, 0, sizeof (tIssPortList));
    ISS_MEMSET (IssL3FilterNullPortChannelList, 0,
                sizeof (tIssPortChannelList));

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus ==
            (UINT1) i4SetValIssAclL3FilterStatus)
        {
            return SNMP_SUCCESS;
        }

    }
    else
    {
        /* This Filter Entry is not there in the Database */
        if (i4SetValIssAclL3FilterStatus != ISS_CREATE_AND_WAIT)
        {
            CLI_SET_ERR (CLI_ACL_NO_FILTER);
            return SNMP_FAILURE;
        }
    }

    switch (i4SetValIssAclL3FilterStatus)
    {

        case ISS_CREATE_AND_WAIT:

            if ((ISS_L3FILTERENTRY_ALLOC_MEM_BLOCK (pIssAclL3FilterEntry)) ==
                NULL)
            {
                /* No Free L3 Filter Entry or h/w init failed at startup */
                CLI_SET_ERR (CLI_ACL_FILTER_MAX);
                return SNMP_FAILURE;
            }

            ISS_MEMSET (pIssAclL3FilterEntry, 0, sizeof (tIssL3FilterEntry));

            if (ISS_FILTER_SHADOW_ALLOC_MEM_BLOCK
                (pIssAclL3FilterEntry->pIssFilterShadowInfo) == NULL)
            {
                ISS_L3FILTERENTRY_FREE_MEM_BLOCK (pIssAclL3FilterEntry);
                ISS_TRC (BUFFER_TRC, "No Free Filter Shadow Entry\n");
                return SNMP_FAILURE;
            }

            ISS_MEMSET (pIssAclL3FilterEntry->pIssFilterShadowInfo,
                        ISS_ZERO_ENTRY, sizeof (tIssFilterShadowTable));

            /* Setting all the default values for the objects */
            pIssAclL3FilterEntry->i4IssL3FilterNo = i4IssAclL3FilterNo;

            pIssAclL3FilterEntry->i4IssL3FilterPriority =
                ISS_DEFAULT_FILTER_PRIORITY;

            pIssAclL3FilterEntry->IssL3FilterProtocol = ISS_IP_PROTO_DEF;
            pIssAclL3FilterEntry->u4SVlanId = 0;
            pIssAclL3FilterEntry->i1SVlanPriority = ISS_DEFAULT_VLAN_PRIORITY;
            pIssAclL3FilterEntry->u4CVlanId = 0;
            pIssAclL3FilterEntry->i1CVlanPriority = ISS_DEFAULT_VLAN_PRIORITY;
            pIssAclL3FilterEntry->u1IssL3FilterTagType = ISS_FILTER_SINGLE_TAG;
            pIssAclL3FilterEntry->i4IssL3FilterMessageType =
                ISS_DEFAULT_MSG_TYPE;
            pIssAclL3FilterEntry->i4IssL3FilterMessageCode =
                ISS_DEFAULT_MSG_CODE;
            pIssAclL3FilterEntry->u4IssL3FilterDstIpAddr = ISS_ZERO_ENTRY;
            pIssAclL3FilterEntry->u4IssL3FilterSrcIpAddr = ISS_ZERO_ENTRY;
            pIssAclL3FilterEntry->u4IssL3FilterDstIpAddrMask =
                ISS_DEF_FILTER_MASK;
            pIssAclL3FilterEntry->u4IssL3FilterSrcIpAddrMask =
                ISS_DEF_FILTER_MASK;
            pIssAclL3FilterEntry->u4IssL3FilterMinDstProtPort = ISS_ZERO_ENTRY;
            pIssAclL3FilterEntry->u4IssL3FilterMaxDstProtPort =
                ISS_MAX_PORT_VALUE;
            pIssAclL3FilterEntry->u4IssL3FilterMinSrcProtPort = ISS_ZERO_ENTRY;
            pIssAclL3FilterEntry->u4IssL3FilterMaxSrcProtPort =
                ISS_MAX_PORT_VALUE;
            pIssAclL3FilterEntry->IssL3FilterAckBit = ISS_ACK_ANY;
            pIssAclL3FilterEntry->IssL3FilterRstBit = ISS_ACK_ANY;
            pIssAclL3FilterEntry->IssL3FilterTos = ISS_TOS_INVALID;
            pIssAclL3FilterEntry->i4IssL3FilterDscp = ISS_DSCP_INVALID;
            pIssAclL3FilterEntry->IssL3FilterDirection = ISS_DIRECTION_IN;
            pIssAclL3FilterEntry->IssL3FilterAction = ISS_ALLOW;
            pIssAclL3FilterEntry->u4IssL3FilterMatchCount = ISS_ZERO_ENTRY;
            pIssAclL3FilterEntry->u4IssL3FilterMultiFieldClfrDstPrefixLength =
                ISS_ZERO_ENTRY;
            pIssAclL3FilterEntry->u4IssL3FilterMultiFieldClfrSrcPrefixLength =
                ISS_ZERO_ENTRY;
            pIssAclL3FilterEntry->u4IssL3MultiFieldClfrFlowId = ISS_ZERO_ENTRY;
            pIssAclL3FilterEntry->i4IssL3MultiFieldClfrAddrType = QOS_IPV4;
            pIssAclL3FilterEntry->u4RefCount = 0;
            pIssAclL3FilterEntry->u1IssL3FilterStatus = 0;
            pIssAclL3FilterEntry->u1IssL3FilterCreationMode
                = ISS_ACL_CREATION_EXTERNAL;
            pIssAclL3FilterEntry->u4IssL3RedirectId = 0;
            pIssAclL3FilterEntry->i4IssClearL3FilterStats = ISS_FALSE;
            pIssAclL3FilterEntry->i4IssL3FilterStatsEnabledStatus =
                ACL_STAT_DISABLE;
            pIssAclL3FilterEntry->u4StatsTransitFlag = ISS_FALSE;

            /* Adding the Entry to the Filter List */
            ISS_SLL_ADD (&(ISS_L3FILTER_LIST),
                         &(pIssAclL3FilterEntry->IssNextNode));

            /* Setting the RowStatus to NOT_READY as all 
             * mandatory objects are not set taken. */
            pIssAclL3FilterEntry->u1IssL3FilterStatus = ISS_NOT_READY;
            pIssAclL3FilterEntry->u1IssL3OutFilterCount = ISS_FALSE;

            break;

        case ISS_NOT_IN_SERVICE:

            if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
            {
#ifdef NPAPI_WANTED
                /* If counter is enabled then set flag to detach
                 * and reattach counter after row status becomes ACTIVE */
                if (pIssAclL3FilterEntry->i4IssL3FilterStatsEnabledStatus
                    == ACL_STAT_ENABLE)
                {
                    i4RetVal =
                        IsssysIssHwUpdateL3Filter (pIssAclL3FilterEntry,
                                                   ISS_L3FILTER_STAT_DISABLE);

                    if (i4RetVal != FNP_SUCCESS)
                    {
                        ISS_TRC (ALL_FAILURE_TRC,
                                 "\nLOW:L3 filter cannot disable hardware stats \n");
                        CLI_SET_ERR (CLI_ACL_HW_UPDATE_FAILED);
                        return SNMP_FAILURE;
                    }
                }

                /* Call the hardware API to update the filter */
                i4RetVal =
                    IsssysIssHwUpdateL3Filter (pIssAclL3FilterEntry,
                                               ISS_L3FILTER_DELETE);

                if (i4RetVal != FNP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
#endif
                /*Since ISS_L3FILTER_SORTED_LIST is copy of hardwre list,
                 * removing entry from here also */

                if ((pIssAclL3FilterEntry->IssL3FilterDirection ==
                     ISS_DIRECTION_OUT)
                    && (pIssAclL3FilterEntry->u1IssL3OutFilterCount ==
                        ISS_TRUE))
                {
                    gISSOutFilterCount--;

                    pIssAclL3FilterEntry->u1IssL3OutFilterCount = ISS_FALSE;
                }
                if (pIssAclL3FilterEntry->u4SChannelIfIndex != 0)
                {
                    /* Scan the L2 Filter tabel to check if L2 Filter entry is installed
                     *  for the S-Channel Intf. If it is so , dont disable the S-Ch Intf filter
                     * status in VLAN Module */
                    TMO_SLL_Scan (&ISS_L2FILTER_LIST, pIssL2FilterEntry,
                                  tIssL2FilterEntry *)
                    {

                        if (pIssL2FilterEntry->u1IssL2FilterStatus !=
                            ISS_ACTIVE)
                        {
                            continue;
                        }

                        if (pIssL2FilterEntry->u4SChannelIfIndex ==
                            pIssAclL3FilterEntry->u4SChannelIfIndex)
                        {
                            u1L2FilterStatus = TRUE;
                            break;
                        }
                    }
                    if (u1L2FilterStatus == FALSE)
                    {

                        if (VlanApiGetSChInfoFromSChIfIndex
                            (pIssAclL3FilterEntry->u4SChannelIfIndex,
                             &u4UapIfIndex, &u2SVId) != VLAN_FAILURE)
                        {
                            /* Set the S-Channel Filter Status to DISABLE */
                            VlanSetSChFilterStatus (u4UapIfIndex, u2SVId,
                                                    ACL_SCH_FILTER_DISABLE);
                        }

                    }

                }
                ISS_SLL_DELETE (&(ISS_L3FILTER_SORTED_LIST),
                                &(pIssAclL3FilterEntry->IssSortedNextNode));

                pIssAclL3FilterEntry->u1IssL3FilterStatus = ISS_NOT_IN_SERVICE;

                return SNMP_SUCCESS;
            }
            else
            {
                CLI_SET_ERR (CLI_ACL_FILTER_NOT_ACTIVE);
                return SNMP_FAILURE;
            }

            break;

        case ISS_ACTIVE:

            if (((ISS_MEMCMP (IssL3FilterNullPortList,
                              pIssAclL3FilterEntry->IssL3FilterInPortList,
                              sizeof (tIssPortList)) == 0)
                 && (ISS_MEMCMP (IssL3FilterNullPortList,
                                 pIssAclL3FilterEntry->IssL3FilterOutPortList,
                                 sizeof (tIssPortList)) == 0)) &&
                ((ISS_MEMCMP (IssL3FilterNullPortChannelList,
                              pIssAclL3FilterEntry->
                              IssL3FilterInPortChannelList,
                              sizeof (tIssPortChannelList)) == 0)
                 &&
                 (ISS_MEMCMP
                  (IssL3FilterNullPortChannelList,
                   pIssAclL3FilterEntry->IssL3FilterOutPortChannelList,
                   sizeof (tIssPortChannelList)) == 0)) &&
                (pIssAclL3FilterEntry->u4SChannelIfIndex == 0))
            {
                /* In Portlist and Portchannel list is NULL.
                 * Filter cannot be made active
                 */
                CLI_SET_ERR (CLI_ACL_FILTER_NO_MEMBERS);
                return SNMP_FAILURE;
            }
            /* Checking the L3 filter for the data sufficiency */
            if (IssExtQualifyL3FilterData (&pIssAclL3FilterEntry) !=
                ISS_SUCCESS)
            {
                CLI_SET_ERR (CLI_ACL_FILTER_DATA_INSUFFICIENCY);
                return SNMP_FAILURE;
            }

            if (pIssAclL3FilterEntry->u1IssL3FilterStatus != ISS_NOT_IN_SERVICE)
            {
                CLI_SET_ERR (CLI_ACL_FILTER_ACTIVE_FAILED);
                return SNMP_FAILURE;
            }

            if (pIssAclL3FilterEntry->u4SChannelIfIndex != 0)
            {
                if (VlanApiGetSChInfoFromSChIfIndex
                    (pIssAclL3FilterEntry->u4SChannelIfIndex, &u4UapIfIndex,
                     &u2SVId) != VLAN_FAILURE)
                {
                    /* Get the L2Filter Status */

                    i4SchL3FilterStatus =
                        VlanGetSChFilterStatus (u4UapIfIndex, u2SVId);
                    if (i4SchL3FilterStatus != ACL_SCH_FILTER_ENABLE)
                    {
                        CLI_SET_ERR (CLI_ACL_SCH_FILTER_ERROR);
                        return SNMP_FAILURE;
                    }

                }
            }

#ifdef NPAPI_WANTED
            {
                /* Call the hardware API to add the L3 filter */
                i4RetVal =
                    IsssysIssHwUpdateL3Filter (pIssAclL3FilterEntry,
                                               ISS_L3FILTER_ADD);

                if (FNP_SUCCESS != i4RetVal)
                {
                    if (FNP_NOT_SUPPORTED == i4RetVal)
                    {
                        CLI_SET_ERR (CLI_ACL_EGRESS_FILTER_ALREADY_MAPPED);
                    }
                    else
                    {
                        if (ISS_DIRECTION_OUT ==
                            pIssAclL3FilterEntry->IssL3FilterDirection)
                        {
                            CLI_SET_ERR (CLI_ACL_OUT_MULTIPLE_PORTS);
                        }
                        else
                        {
                            CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
                        }
                    }
                    return SNMP_FAILURE;
                }

            }
#endif
            /* Sorting the L3 filter list in h/w based on priority.The new
             * entry is added in hardware based on priority. The existing
             * entries with greater priority are removed from h/w and then
             * added again based on priority */

            pIssAclL3FilterEntry->u1IssL3FilterStatus = ISS_ACTIVE;

            /*Update Mirroring database */
            IssMirrCreateAcl (i4IssAclL3FilterNo, ISS_MIRR_L3_ACL);

#ifdef NPAPI_WANTED
            /* Enable counter if required */
            if (pIssAclL3FilterEntry->i4IssL3FilterStatsEnabledStatus
                == ACL_STAT_ENABLE)
            {

                i4RetVal =
                    IsssysIssHwUpdateL3Filter (pIssAclL3FilterEntry,
                                               ISS_L3FILTER_STAT_ENABLE);

                if (i4RetVal != FNP_SUCCESS)
                {
                    ISS_TRC (ALL_FAILURE_TRC,
                             "\nLOW:L3 filter cannot enable hardware stats \n");
                    CLI_SET_ERR (CLI_ACL_HW_UPDATE_FAILED);
                    return SNMP_FAILURE;
                }
            }

            if ((pIssAclL3FilterEntry->IssL3FilterDirection ==
                 ISS_DIRECTION_OUT)
                && (pIssAclL3FilterEntry->u1IssL3OutFilterCount == ISS_FALSE))
            {
                gISSOutFilterCount++;
                pIssAclL3FilterEntry->u1IssL3OutFilterCount = ISS_TRUE;
            }
#endif

            break;

        case ISS_DESTROY:

#ifdef NPAPI_WANTED
            if (pIssAclL3FilterEntry->i4IssL3FilterStatsEnabledStatus
                == ACL_STAT_ENABLE)
            {
                pIssAclL3FilterEntry->i4IssL3FilterStatsEnabledStatus
                    = ACL_STAT_DISABLE;

                i4RetVal =
                    IsssysIssHwUpdateL3Filter (pIssAclL3FilterEntry,
                                               ISS_L3FILTER_STAT_DISABLE);

                if (i4RetVal != FNP_SUCCESS)
                {
                    ISS_TRC (ALL_FAILURE_TRC,
                             "\nLOW:L3 filter cannot disable hardware stats \n");
                }
            }
#endif

            if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
            {

#ifdef NPAPI_WANTED

                /* Call the hardware API to delete the filter */
                i4RetVal =
                    IsssysIssHwUpdateL3Filter (pIssAclL3FilterEntry,
                                               ISS_L3FILTER_DELETE);

                if (i4RetVal != FNP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
#endif

            }
            /* Deleting the node from the list */
            ISS_SLL_DELETE (&(ISS_L3FILTER_LIST),
                            &(pIssAclL3FilterEntry->IssNextNode));
            /*Since ISS_L3FILTER_SORTED_LIST is copy of hardwre list,
             * removing entry from here also */
            ISS_SLL_DELETE (&(ISS_L3FILTER_SORTED_LIST),
                            &(pIssAclL3FilterEntry->IssSortedNextNode));
            /* Delete the Entry from the Software */
            if (ISS_FILTER_SHADOW_FREE_MEM_BLOCK
                (pIssAclL3FilterEntry->pIssFilterShadowInfo) != MEM_SUCCESS)
            {
                ISS_TRC (BUFFER_TRC | ALL_FAILURE_TRC,
                         "Filter Shadow Entry Free Failure\n");
                return SNMP_FAILURE;
            }
            if (ISS_L3FILTERENTRY_FREE_MEM_BLOCK (pIssAclL3FilterEntry)
                != MEM_SUCCESS)
            {
                /* L3 Filter Entry Free Failure */
                CLI_SET_ERR (CLI_ACL_DELETION_FAILED);
                return SNMP_FAILURE;
            }
            if ((pIssAclL3FilterEntry->IssL3FilterDirection ==
                 ISS_DIRECTION_OUT)
                && (pIssAclL3FilterEntry->u1IssL3OutFilterCount == ISS_TRUE))
            {
                gISSOutFilterCount--;
                pIssAclL3FilterEntry->u1IssL3OutFilterCount = ISS_FALSE;
            }

            break;

        default:
            return SNMP_FAILURE;
    }
#ifdef QOSX_WANTED
    QosUpdateDiffServMFCNextFree (i4IssAclL3FilterNo,
                                  i4SetValIssAclL3FilterStatus);
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterInPortChannelList
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterInPortChannelList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterInPortChannelList (INT4 i4IssAclL3FilterNo,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pSetValIssAclL3FilterInPortChannelList)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        ISS_MEMSET (pIssAclL3FilterEntry->IssL3FilterInPortChannelList, 0,
                    ISS_PORT_CHANNEL_LIST_SIZE);
        ISS_MEMCPY (pIssAclL3FilterEntry->IssL3FilterInPortChannelList,
                    pSetValIssAclL3FilterInPortChannelList->pu1_OctetList,
                    pSetValIssAclL3FilterInPortChannelList->i4_Length);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterOutPortChannelList
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterOutPortChannelList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterOutPortChannelList (INT4 i4IssAclL3FilterNo,
                                        tSNMP_OCTET_STRING_TYPE
                                        *
                                        pSetValIssAclL3FilterOutPortChannelList)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        ISS_MEMSET (pIssAclL3FilterEntry->IssL3FilterOutPortChannelList, 0,
                    ISS_PORT_CHANNEL_LIST_SIZE);
        ISS_MEMCPY (pIssAclL3FilterEntry->IssL3FilterOutPortChannelList,
                    pSetValIssAclL3FilterOutPortChannelList->pu1_OctetList,
                    pSetValIssAclL3FilterOutPortChannelList->i4_Length);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterStatsEnabledStatus
 Input       :  The Indices
                IssAclL3FilterNo

                The Object
                setValIssAclL3FilterStatsEnabledStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterStatsEnabledStatus (INT4 i4IssAclL3FilterNo,
                                        INT4
                                        i4SetValIssAclL3FilterStatsEnabledStatus)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

#ifdef NPAPI_WANTED
    INT4                i4RetVal = FNP_FAILURE;
    INT4                i4HwStatsFlag;
    INT4                i4StatusOld;
#endif

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhSetIssAclL3FilterStatsEnabledStatus function \n");

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nLOW:pIssAclL3FilterEntry is NULL \n");
        CLI_SET_ERR (CLI_ACL_NO_FILTER);
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    i4StatusOld = pIssAclL3FilterEntry->i4IssL3FilterStatsEnabledStatus;
#endif
    pIssAclL3FilterEntry->i4IssL3FilterStatsEnabledStatus =
        i4SetValIssAclL3FilterStatsEnabledStatus;

#ifdef NPAPI_WANTED
    if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        if (i4SetValIssAclL3FilterStatsEnabledStatus == ACL_STAT_ENABLE)
        {
            i4HwStatsFlag = ISS_L3FILTER_STAT_ENABLE;
        }
        else
        {
            i4HwStatsFlag = ISS_L3FILTER_STAT_DISABLE;
        }

        i4RetVal = IsssysIssHwUpdateL3Filter (pIssAclL3FilterEntry,
                                              i4HwStatsFlag);
        if (i4RetVal != FNP_SUCCESS)
        {
            pIssAclL3FilterEntry->i4IssL3FilterStatsEnabledStatus = i4StatusOld;
            ISS_TRC (ALL_FAILURE_TRC,
                     "\nLOW:L3 filter cannot enable hardware stats \n");
            CLI_SET_ERR (CLI_ACL_HW_UPDATE_FAILED);
            return SNMP_FAILURE;
        }
    }
#endif

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhSetIssAclL3FilterStatsEnabledStatus function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssAclClearL3FilterStats
 Input       :  The Indices
                IssAclL3FilterNo

                The Object
                setValIssAclClearL3FilterStats
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclClearL3FilterStats (INT4 i4IssAclL3FilterNo,
                                INT4 i4SetValIssAclClearL3FilterStats)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

#ifdef NPAPI_WANTED
    INT4                i4RetVal = FNP_FAILURE;
#endif

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhSetIssAclClearL3FilterStats function \n");

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nLOW:pIssAclL3FilterEntry is NULL \n");
        CLI_SET_ERR (CLI_ACL_FILTER_DATA_INSUFFICIENCY);
        return SNMP_FAILURE;
    }

    pIssAclL3FilterEntry->i4IssClearL3FilterStats =
        i4SetValIssAclClearL3FilterStats;
#ifdef NPAPI_WANTED
    if (pIssAclL3FilterEntry->i4IssL3FilterStatsEnabledStatus
        == ACL_STAT_ENABLE)
    {
        i4RetVal = IsssysIssHwUpdateL3Filter (pIssAclL3FilterEntry,
                                              ISS_L3FILTER_STAT_CLEAR);

        if (i4RetVal != FNP_SUCCESS)
        {
            pIssAclL3FilterEntry->i4IssClearL3FilterStats = ISS_FALSE;
            ISS_TRC (ALL_FAILURE_TRC,
                     "\nLOW:L3 filter failed to clear hardware stats \n");
            CLI_SET_ERR (CLI_ACL_HW_UPDATE_FAILED);
            return SNMP_FAILURE;
        }
    }
#endif
    pIssAclL3FilterEntry->u4IssL3FilterMatchCount = 0;
    pIssAclL3FilterEntry->i4IssClearL3FilterStats = ISS_FALSE;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhSetIssAclClearL3FilterStats function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterSChannelIfIndex
 Input       :  The Indices
                IssAclL3FilterNo

                The Object
                setValIssAclL3FilterSChannelIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterSChannelIfIndex (INT4 i4IssAclL3FilterNo,
                                     UINT4
                                     u4SetValIssAclL3FilterSChannelIfIndex)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhSetIssAclL3FilterSChannelIfIndex function \n");

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nLOW:pIssAclL3FilterEntry is NULL \n");
        CLI_SET_ERR (CLI_ACL_FILTER_DATA_INSUFFICIENCY);
        return SNMP_FAILURE;
    }

    pIssAclL3FilterEntry->u4SChannelIfIndex =
        u4SetValIssAclL3FilterSChannelIfIndex;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhSetIssAclL3FilterSChannelIfIndex function \n");
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterPriority
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterPriority (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                                 INT4 i4TestValIssAclL3FilterPriority)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL3FilterPriority function \n");
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL3FilterPriority <= ISS_MAX_FILTER_PRIORITY) &&
        (i4TestValIssAclL3FilterPriority >= ISS_DEFAULT_FILTER_PRIORITY))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:Validate L2 filter Priority with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL3FilterPriority function \n");
}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterProtocol
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterProtocol
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterProtocol (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                                 INT4 i4TestValIssAclL3FilterProtocol)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL3FilterProtocol function \n");
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry != NULL)
    {
        /* Message type and message code are supported only for ICMP/ICMP6 Packets */
        if ((i4TestValIssAclL3FilterProtocol != ISS_PROT_ICMP) &&
            (i4TestValIssAclL3FilterProtocol != ISS_PROT_ICMP6))
        {
            if ((pIssAclL3FilterEntry->i4IssL3FilterMessageType !=
                 ISS_DEFAULT_MSG_TYPE)
                || (pIssAclL3FilterEntry->i4IssL3FilterMessageType !=
                    ISS_DEFAULT_MSG_CODE))
            {
                *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nLOW: L3 filter MessageType is supported only for ICMP Packets with errorcode as %d\n",
                              *pu4ErrorCode);
                return SNMP_FAILURE;
            }
        }

        /* Min/Max src/Dst Port are supported only for TCP/UDP Packets */
        if ((i4TestValIssAclL3FilterProtocol != ISS_PROT_TCP) &&
            (i4TestValIssAclL3FilterProtocol != ISS_PROT_UDP))
        {
            if ((pIssAclL3FilterEntry->u4IssL3FilterMinDstProtPort !=
                 ISS_ZERO_ENTRY)
                || (pIssAclL3FilterEntry->u4IssL3FilterMinSrcProtPort !=
                    ISS_ZERO_ENTRY)
                || (pIssAclL3FilterEntry->u4IssL3FilterMaxSrcProtPort !=
                    ISS_MAX_PORT_VALUE)
                || (pIssAclL3FilterEntry->u4IssL3FilterMaxSrcProtPort !=
                    ISS_MAX_PORT_VALUE))
            {
                *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nLOW: L3 filter Min/Max Src/Dst port is supported only for TCP/UDP Packets with errorcode as %d\n",
                              *pu4ErrorCode);
                return SNMP_FAILURE;
            }
        }

        /* Ack/Rst Bits are supported only for TCP Packets */
        if (i4TestValIssAclL3FilterProtocol != ISS_PROT_TCP)
        {
            if ((pIssAclL3FilterEntry->IssL3FilterAckBit !=
                 (tIssAckBit) ISS_ACK_ANY)
                || (pIssAclL3FilterEntry->IssL3FilterRstBit !=
                    (tIssRstBit) ISS_ACK_ANY))
            {
                *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nLOW: L3 filter Ack/Rst Bit is supported only for TCP Packets with errorcode as %d\n",
                              *pu4ErrorCode);
                return SNMP_FAILURE;
            }
        }
    }

    if ((i4TestValIssAclL3FilterProtocol < 0)
        || (i4TestValIssAclL3FilterProtocol > 255))
    {
        if (i4TestValIssAclL3FilterProtocol != ISS_IP_PROTO_DEF)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nLOW: L3 filter Protocol value is less than zero or greater than 255 with errorcode as %d\n",
                          *pu4ErrorCode);
            return SNMP_FAILURE;
        }
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL3FilterProtocol function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterMessageType
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterMessageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterMessageType (UINT4 *pu4ErrorCode,
                                    INT4 i4IssAclL3FilterNo,
                                    INT4 i4TestValIssAclL3FilterMessageType)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL3FilterMessageType function \n");
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry != NULL)
    {
        if ((pIssAclL3FilterEntry->IssL3FilterProtocol != ISS_PROT_ICMP) &&
            (pIssAclL3FilterEntry->IssL3FilterProtocol != ISS_PROT_ICMP6))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nLOW: L3 filter MessageType is supported only for ICMP Packets with errorcode as %d\n",
                          *pu4ErrorCode);
            return SNMP_FAILURE;
        }
    }

    if ((i4TestValIssAclL3FilterMessageType < 0)
        || (i4TestValIssAclL3FilterMessageType > 255))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW: L3 filter MessageType value is less than zero or greater than 255 with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL3FilterMessageType function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterMessageCode
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterMessageCode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterMessageCode (UINT4 *pu4ErrorCode,
                                    INT4 i4IssAclL3FilterNo,
                                    INT4 i4TestValIssAclL3FilterMessageCode)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL3FilterMessageCode function \n");
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry != NULL)
    {
        if ((pIssAclL3FilterEntry->IssL3FilterProtocol != ISS_PROT_ICMP) &&
            (pIssAclL3FilterEntry->IssL3FilterProtocol != ISS_PROT_ICMP6))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nLOW: L3 filter MessageCode is supported only for ICMP Packets with errorcode as %d\n",
                          *pu4ErrorCode);
            return SNMP_FAILURE;
        }
    }

    if ((i4TestValIssAclL3FilterMessageCode < 0)
        || (i4TestValIssAclL3FilterMessageCode > 255))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW: L3 filter Messagecode value is less than zero or greater than 255 with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL3FilterMessageCode function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilteAddrType
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilteAddrType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilteAddrType (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                                INT4 i4TestValIssAclL3FilteAddrType)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL3FilteAddrType function \n");
    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssL3FilterEntry value is NULL with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    if (pIssL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3FilterStatus is Active with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    if ((i4TestValIssAclL3FilteAddrType == QOS_IPV4) ||
        (i4TestValIssAclL3FilteAddrType == QOS_IPV6))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL3FilteAddrType function \n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterDstIpAddr
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterDstIpAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterDstIpAddr (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValIssAclL3FilterDstIpAddr)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    UINT4               u4DstIpAddr = 0;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL3FilterDstIpAddr function \n");
    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssL3FilterEntry value is NULL with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if (pIssL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3FilterStatus is Active with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV4)
    {
        if (pTestValIssAclL3FilterDstIpAddr->i4_Length != QOS_IPV4_LEN)
        {
            ISS_TRC (ALL_FAILURE_TRC,
                     "\nLOW:L3 filter DstIpAddr length is equql to the macro value of QOS_IPV4_LEN\n");
            return SNMP_FAILURE;
        }

        PTR_FETCH4 (u4DstIpAddr,
                    pTestValIssAclL3FilterDstIpAddr->pu1_OctetList);

        /* Allow IP address which are Class A, B, C or D */
        if (!
            ((ISS_IS_ADDR_CLASS_A (u4DstIpAddr))
             ||
             (ISS_IS_ADDR_CLASS_B (u4DstIpAddr))
             ||
             (ISS_IS_ADDR_CLASS_C (u4DstIpAddr))
             || (ISS_IS_ADDR_CLASS_D (u4DstIpAddr))))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nLOW:Invalid L3 filter DstIpAddr with errorcode as %d\n",
                          *pu4ErrorCode);
            return SNMP_FAILURE;
        }
    }

    if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV6)
    {
        if (pTestValIssAclL3FilterDstIpAddr->i4_Length != QOS_IPV6_LEN)
        {
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nLOW:L3 filter DstIpAddr length is not equal to the value of QOS_IPV6_LEN%d \n",
                          QOS_IPV6_LEN);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL3FilterDstIpAddr function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterSrcIpAddr
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterSrcIpAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterSrcIpAddr (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValIssAclL3FilterSrcIpAddr)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    UINT4               u4SrcIpAddr = 0;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL3FilterSrcIpAddr function \n");
    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssL3FilterEntry value is NULL with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV4)
    {
        if (pTestValIssAclL3FilterSrcIpAddr->i4_Length != QOS_IPV4_LEN)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nLOW:L3 filter SrcIpAddr length is not equql to the  value of QOS_IPV4_LEN%d\n",
                          QOS_IPV4_LEN);
            return SNMP_FAILURE;
        }

        PTR_FETCH4 (u4SrcIpAddr,
                    pTestValIssAclL3FilterSrcIpAddr->pu1_OctetList);

        /* Allow IP address which are Class A, B, C or D */
        if (!
            ((ISS_IS_ADDR_CLASS_A (u4SrcIpAddr))
             ||
             (ISS_IS_ADDR_CLASS_B (u4SrcIpAddr))
             ||
             (ISS_IS_ADDR_CLASS_C (u4SrcIpAddr))
             || (ISS_IS_ADDR_CLASS_D (u4SrcIpAddr))))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nLOW:Invalid L3 filter src Ip Addr with errorcode as %d\n",
                          *pu4ErrorCode);
            return SNMP_FAILURE;
        }

    }
    if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV6)
    {
        if (pTestValIssAclL3FilterSrcIpAddr->i4_Length != QOS_IPV6_LEN)
        {
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nLOW:L3 filter SrcIpAddr length is not equql to the value of QOS_IPV6_LEN%d\n",
                          QOS_IPV6_LEN);
            return SNMP_FAILURE;
        }
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL3FilterSrcIpAddr function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterDstIpAddrPrefixLength
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterDstIpAddrPrefixLength
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterDstIpAddrPrefixLength (UINT4 *pu4ErrorCode,
                                              INT4 i4IssAclL3FilterNo,
                                              UINT4
                                              u4TestValIssAclL3FilterDstIpAddrPrefixLength)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL3FilterDstIpAddrPrefixLength function \n");
    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssL3FilterEntry value is NULL with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV4)
    {
        if (u4TestValIssAclL3FilterDstIpAddrPrefixLength >
            QOS_MF_PERFIX_IPV4_MAX)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nLOW: L3 filter Dst IpAddr PrefixLength is above the PrefixLength of IPV4 max range%d with errorcode as %d\n",
                          QOS_MF_PERFIX_IPV4_MAX, *pu4ErrorCode);
            return SNMP_FAILURE;
        }
    }
    else if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV6)
    {
        if ((u4TestValIssAclL3FilterDstIpAddrPrefixLength >
             QOS_MF_PERFIX_IPV6_MAX))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nLOW: L3 filter Dst IpAddr PrefixLength is above the PrefixLength of  IPV6 max range%d with errorcode as %d\n",
                          QOS_MF_PERFIX_IPV6_MAX, *pu4ErrorCode);
            return SNMP_FAILURE;
        }
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL3FilterDstIpAddrPrefixLength function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterSrcIpAddrPrefixLength
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterSrcIpAddrPrefixLength
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterSrcIpAddrPrefixLength (UINT4 *pu4ErrorCode,
                                              INT4 i4IssAclL3FilterNo,
                                              UINT4
                                              u4TestValIssAclL3FilterSrcIpAddrPrefixLength)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL3FilterSrcIpAddrPrefixLength function \n");
    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssL3FilterEntry value is NULL with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV4)
    {
        if (u4TestValIssAclL3FilterSrcIpAddrPrefixLength >
            QOS_MF_PERFIX_IPV4_MAX)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nLOW: L3 filter src IpAddr PrefixLength is above the PrefixLength of IPV4 max range %d with errorcode as %d\n",
                          QOS_MF_PERFIX_IPV4_MAX, *pu4ErrorCode);
            return SNMP_FAILURE;
        }
    }
    else if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV6)
    {
        if ((u4TestValIssAclL3FilterSrcIpAddrPrefixLength >
             QOS_MF_PERFIX_IPV6_MAX))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nLOW: L3 filter Dst IpAddr PrefixLength is above the PrefixLength of IPV6 max range %d with errorcode as %d\n",
                          QOS_MF_PERFIX_IPV6_MAX, *pu4ErrorCode);
            return SNMP_FAILURE;
        }
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL3FilterSrcIpAddrPrefixLength function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterMinDstProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterMinDstProtPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterMinDstProtPort (UINT4 *pu4ErrorCode,
                                       INT4 i4IssAclL3FilterNo,
                                       UINT4
                                       u4TestValIssAclL3FilterMinDstProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL3FilterMinDstProtPort function \n");
    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nLOW:pIssL3FilterEntry value is NULL \n");
        return SNMP_FAILURE;
    }

    if ((pIssAclL3FilterEntry->IssL3FilterProtocol != ISS_PROT_TCP) &&
        (pIssAclL3FilterEntry->IssL3FilterProtocol != ISS_PROT_UDP))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((pIssAclL3FilterEntry->u4IssL3FilterMaxDstProtPort != 0) &&
        (pIssAclL3FilterEntry->u4IssL3FilterMaxDstProtPort <
         u4TestValIssAclL3FilterMinDstProtPort))
    {
        CLI_SET_ERR (CLI_ACL_FILTER_INVALID_L4_PORT_RANGE);
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nLOW:u4IssL3FilterMaxDstProtPort value is not equal to zero \n");
        return SNMP_FAILURE;
    }

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((u4TestValIssAclL3FilterMinDstProtPort >= 1) &&
        (u4TestValIssAclL3FilterMinDstProtPort <= ISS_MAX_PORT_VALUE))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        CLI_SET_ERR (CLI_ACL_FILTER_INVALID_L4_PORT_RANGE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW: L3 filter MinDstProt level is above 65535 range with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL3FilterMinDstProtPort function \n");
}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterMaxDstProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterMaxDstProtPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterMaxDstProtPort (UINT4 *pu4ErrorCode,
                                       INT4 i4IssAclL3FilterNo,
                                       UINT4
                                       u4TestValIssAclL3FilterMaxDstProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL3FilterMaxDstProtPort function \n");
    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nLOW:pIssL3FilterEntry value is NULL \n");
        return SNMP_FAILURE;
    }

    if ((pIssAclL3FilterEntry->IssL3FilterProtocol != ISS_PROT_TCP) &&
        (pIssAclL3FilterEntry->IssL3FilterProtocol != ISS_PROT_UDP))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry->u4IssL3FilterMinDstProtPort >
        u4TestValIssAclL3FilterMaxDstProtPort)
    {
        CLI_SET_ERR (CLI_ACL_FILTER_INVALID_L4_PORT_RANGE);
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nLOW:u4IssL3FilterMinDstProtPort value is greater than u4TestValIssAclL3FilterMaxDstProtPort \n");
        return SNMP_FAILURE;
    }

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((u4TestValIssAclL3FilterMaxDstProtPort >= 1)
        && (u4TestValIssAclL3FilterMaxDstProtPort <= ISS_MAX_PORT_VALUE))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        CLI_SET_ERR (CLI_ACL_FILTER_INVALID_L4_PORT_RANGE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW: L3 filter MaxDstProt level is above 65535 range with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL3FilterMaxDstProtPort function \n");
}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterMinSrcProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterMinSrcProtPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterMinSrcProtPort (UINT4 *pu4ErrorCode,
                                       INT4 i4IssAclL3FilterNo,
                                       UINT4
                                       u4TestValIssAclL3FilterMinSrcProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL3FilterMinSrcProtPort  function \n");
    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nLOW:pIssL3FilterEntry value is NULL \n");
        return SNMP_FAILURE;
    }

    if ((pIssAclL3FilterEntry->IssL3FilterProtocol != ISS_PROT_TCP) &&
        (pIssAclL3FilterEntry->IssL3FilterProtocol != ISS_PROT_UDP))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((pIssAclL3FilterEntry->u4IssL3FilterMaxSrcProtPort != 0) &&
        (pIssAclL3FilterEntry->u4IssL3FilterMaxSrcProtPort <
         u4TestValIssAclL3FilterMinSrcProtPort))
    {
        CLI_SET_ERR (CLI_ACL_FILTER_INVALID_L4_PORT_RANGE);
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nLOW:u4TestValIssAclL3FilterMinSrcProtPort value is greater than u4IssL3FilterMaxSrcProtPort \n");
        return SNMP_FAILURE;
    }

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    if ((u4TestValIssAclL3FilterMinSrcProtPort >= 1)
        && (u4TestValIssAclL3FilterMinSrcProtPort <= ISS_MAX_PORT_VALUE))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW: L3 filter Min Src Prot level is above 65535 range with errorcode as %d\n",
                      *pu4ErrorCode);
        CLI_SET_ERR (CLI_ACL_FILTER_SRC_PORT_ERR);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL3FilterMinSrcProtPort  function \n");
}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterMaxSrcProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterMaxSrcProtPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterMaxSrcProtPort (UINT4 *pu4ErrorCode,
                                       INT4 i4IssAclL3FilterNo,
                                       UINT4
                                       u4TestValIssAclL3FilterMaxSrcProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL3FilterMaxSrcProtPort function \n");
    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nLOW:pIssL3FilterEntry value is NULL \n");
        return SNMP_FAILURE;
    }

    if ((pIssAclL3FilterEntry->IssL3FilterProtocol != ISS_PROT_TCP) &&
        (pIssAclL3FilterEntry->IssL3FilterProtocol != ISS_PROT_UDP))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry->u4IssL3FilterMinSrcProtPort >
        u4TestValIssAclL3FilterMaxSrcProtPort)
    {
        CLI_SET_ERR (CLI_ACL_FILTER_SRC_PORT_ERR);
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nLOW:u4IssL3FilterMinSrcProtPort value is greater than                                           u4TestValIssAclL3FilterMaxSrcProtPort \n");
        return SNMP_FAILURE;
    }

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nLOW:u4TestValIssAclL3FilterMinDstProtPort value is Less than 65535 \n");
        return SNMP_FAILURE;
    }

    if ((u4TestValIssAclL3FilterMaxSrcProtPort >= 1)
        && (u4TestValIssAclL3FilterMaxSrcProtPort <= ISS_MAX_PORT_VALUE))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW: L3 filter Max Src Prot level is above 65535 range with errorcode as %d\n",
                      *pu4ErrorCode);
        CLI_SET_ERR (CLI_ACL_FILTER_SRC_PORT_ERR);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL3FilterMaxSrcProtPort function \n");
}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterInPortList
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterInPortList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterInPortList (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValIssAclL3FilterInPortList)
{
    UINT4               u4IfIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2Port = 0;
    UINT1               u1PortFlag = 0;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL3FilterInPortList function \n");
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    for (u4IfIndex = 0; u4IfIndex < ISS_PORT_LIST_SIZE; u4IfIndex++)
    {
        u1PortFlag =
            (UINT1) pTestValIssAclL3FilterInPortList->pu1_OctetList[u4IfIndex];

        for (u2BitIndex = 0;
             ((u2BitIndex < BITS_PER_BYTE) && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & ISS_BIT8) != 0)
            {
                u2Port = (UINT2) ((u4IfIndex * 8) + u2BitIndex + 1);
                if (CfaValidateIfIndex ((UINT4) u2Port) == CFA_SUCCESS)
                {
                    if (AclValidateIfMemberoOfPortChannel (u2Port) ==
                        CLI_FAILURE)
                    {

                        CLI_SET_ERR (CLI_ACL_SET_ERROR_ON_PORT_CHANNEL);
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        return SNMP_FAILURE;
                    }
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }
    if ((pTestValIssAclL3FilterInPortList->i4_Length <= 0) ||
        (pTestValIssAclL3FilterInPortList->i4_Length > ISS_PORT_LIST_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nLOW:L2 L3FilterInPortList length is less  the zero or  below the  valueof  ISS_PORT_LIST_SIZE%d  with errorcode as %d\n",
                      ISS_PORT_LIST_SIZE, *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if (IssIsPortListValid
        (pTestValIssAclL3FilterInPortList->pu1_OctetList,
         pTestValIssAclL3FilterInPortList->i4_Length) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:Failed to validate the L3 filter InPortList with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL3FilterInPortList function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterOutPortList
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterOutPortList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterOutPortList (UINT4 *pu4ErrorCode,
                                    INT4 i4IssAclL3FilterNo,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValIssAclL3FilterOutPortList)
{
    UINT4               u4IfIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2Port = 0;
    UINT1               u1PortFlag = 0;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL3FilterOutPortList function \n");
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    for (u4IfIndex = 0; u4IfIndex < ISS_PORT_LIST_SIZE; u4IfIndex++)
    {
        u1PortFlag =
            (UINT1) pTestValIssAclL3FilterOutPortList->pu1_OctetList[u4IfIndex];

        for (u2BitIndex = 0;
             ((u2BitIndex < BITS_PER_BYTE) && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & ISS_BIT8) != 0)
            {
                u2Port = (UINT2) ((u4IfIndex * 8) + u2BitIndex + 1);
                if (CfaValidateIfIndex ((UINT4) u2Port) == CFA_SUCCESS)
                {
                    if (AclValidateIfMemberoOfPortChannel (u2Port) ==
                        CLI_FAILURE)
                    {

                        CLI_SET_ERR (CLI_ACL_SET_ERROR_ON_PORT_CHANNEL);
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        return SNMP_FAILURE;
                    }
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }
    if ((pTestValIssAclL3FilterOutPortList->i4_Length <= 0) ||
        (pTestValIssAclL3FilterOutPortList->i4_Length > ISS_PORT_LIST_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nLOW:L2 L3FilterOutPortList length is less the zero or above the value of ISS_PORT_LIST_SIZE%d  with errorcode as %d\n",
                      ISS_PORT_LIST_SIZE, *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if (IssIsPortListValid
        (pTestValIssAclL3FilterOutPortList->pu1_OctetList,
         pTestValIssAclL3FilterOutPortList->i4_Length) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:Failed to validate the L3 filter OutPortList                                            with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL3FilterOutPortList function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterAckBit
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterAckBit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterAckBit (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                               INT4 i4TestValIssAclL3FilterAckBit)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL3FilterAckBit function \n");

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nLOW:pIssL3FilterEntry value is NULL \n");
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry->IssL3FilterProtocol != ISS_PROT_TCP)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL3FilterAckBit >= ISS_ACK_ESTABLISH)
        && (i4TestValIssAclL3FilterAckBit <= ISS_ACK_ANY))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter AckBit is below the value of  ISS_ACK_ESTABLISH %d                                           with errorcode as %d\n",
                      ISS_ACK_ESTABLISH, *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL3FilterAckBit function \n");
}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterRstBit
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterRstBit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterRstBit (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                               INT4 i4TestValIssAclL3FilterRstBit)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL3FilterRstBit function \n");
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nLOW:pIssL3FilterEntry value is NULL \n");
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry->IssL3FilterProtocol != ISS_PROT_TCP)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL3FilterRstBit >= ISS_RST_SET)
        && (i4TestValIssAclL3FilterRstBit <= ISS_RST_ANY))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter RstBit is below the value of ISS_RST_SET%d                                            with errorcode as %d\n",
                      ISS_RST_SET, *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL3FilterRstBit function \n");
}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterTos
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterTos
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterTos (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                            INT4 i4TestValIssAclL3FilterTos)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL3FilterTos function \n");
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->i4IssL3FilterDscp != ISS_DSCP_INVALID)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nLOW:L3 filter Dscp is  valid  with errorcode as %d\n",
                          *pu4ErrorCode);
            return SNMP_FAILURE;
        }
    }

    if (i4IssAclL3FilterNo < ACL_STANDARD_MIN_VALUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL3FilterTos >= ISS_TOS_NONE)
        && (i4TestValIssAclL3FilterTos < ISS_TOS_INVALID))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter Tos range is below the value of ISS_TOS_NONE%d  with errorcode as %d\n",
                      ISS_TOS_NONE, *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL3FilterTos function \n");
}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterDscp
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterDscp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterDscp (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                             INT4 i4TestValIssAclL3FilterDscp)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL3FilterDscp function \n");
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if ((pIssAclL3FilterEntry->IssL3FilterTos != ISS_TOS_INVALID) &&
            (pIssAclL3FilterEntry->IssL3FilterTos != ISS_TOS_NONE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nLOW:L3 filter Tos is  Invalid  with errorcode as %d\n",
                          *pu4ErrorCode);
            return SNMP_FAILURE;
        }
    }

    if (i4IssAclL3FilterNo < ACL_STANDARD_MIN_VALUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        return SNMP_FAILURE;
    }

    if (((i4TestValIssAclL3FilterDscp >= ISS_MIN_DSCP_VALUE) &&
         (i4TestValIssAclL3FilterDscp <= ISS_MAX_DSCP_VALUE)) ||
        (i4TestValIssAclL3FilterDscp == -1))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter Dscp is  above the  value of ISS_MIN_DSCP_VALUE %d with errorcode as %d\n",
                      ISS_MIN_DSCP_VALUE, *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL3FilterDscp function \n");
}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterDirection
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterDirection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterDirection (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                                  INT4 i4TestValIssAclL3FilterDirection)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL3FilterDirection function \n");
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    if ((gISSOutFilterCount >= ISS_MAX_L2_L3_OUT_FILTERS) &&
        (i4TestValIssAclL3FilterDirection == ISS_DIRECTION_OUT))
    {
        CLI_SET_ERR (CLI_ACL_MAX_OUT_DIRECTION);
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssAclL2FilterEntry Max OUT direction limit reached %d\n",
                      *pu4ErrorCode);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL3FilterDirection == ISS_DIRECTION_IN)
        || (i4TestValIssAclL3FilterDirection == ISS_DIRECTION_OUT))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:Validate the L3 filter Direction  with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL3FilterDirection function \n");
}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterAction
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterAction (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                               INT4 i4TestValIssAclL3FilterAction)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL3FilterAction function \n");
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    /* Order should not be changed for the values of Enum tIssFilterAction */

    if ((i4TestValIssAclL3FilterAction >= ISS_ALLOW)
        && (i4TestValIssAclL3FilterAction <= ISS_DROP_COPYTOCPU))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter Action  is below the value of ISS_ALLOW %dwith errorcode as %d\n",
                      ISS_ALLOW, *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL3FilterAction function \n");
}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterFlowId
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterFlowId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterFlowId (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                               UINT4 u4TestValIssAclL3FilterFlowId)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL3FilterFlowId function \n");
    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType != QOS_IPV6)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nLOW:i4IssL3MultiFieldClfrAddrTypeis value is not eual to the value of QOS_IPV6%d with errorcode as %d\n",
                      QOS_IPV6, *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if (pIssL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3FilterStatus is Active with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    if ((u4TestValIssAclL3FilterFlowId == QOS_FLOW_ID_MIN) ||
        ((u4TestValIssAclL3FilterFlowId >= 1) &&
         (u4TestValIssAclL3FilterFlowId <= QOS_FLOW_ID_MAX)))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL3FilterFlowId function \n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterStatus
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterStatus (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                               INT4 i4TestValIssAclL3FilterStatus)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
    INT4                i4RetStatus = ISS_ZERO_ENTRY;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL3FilterStatus function \n");
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        CLI_SET_ERR (CLI_ACL_INVALID_FILTER_ID);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL3FilterStatus < ISS_ACTIVE)
        || (i4TestValIssAclL3FilterStatus > ISS_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter status  is below the value of ISS_ACTIVE%d or above the value ISS_DESTROY%d with errorcode as %d\n",
                      ISS_ACTIVE, ISS_DESTROY, *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if ((pIssAclL3FilterEntry == NULL)
        && (ISS_L3FILTER_LIST_COUNT == ISS_MAX_L3_FILTERS))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter entry is not Null value with errorcode as %d\n",
                      *pu4ErrorCode);
        CLI_SET_ERR (CLI_ACL_FILTER_MAX);
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry == NULL &&
        (i4TestValIssAclL3FilterStatus != ISS_CREATE_AND_WAIT
         && i4TestValIssAclL3FilterStatus != ISS_CREATE_AND_GO))
    {
        CLI_SET_ERR (CLI_ACL_NO_FILTER);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter entry is not Null value with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry != NULL &&
        (i4TestValIssAclL3FilterStatus == ISS_CREATE_AND_WAIT
         || i4TestValIssAclL3FilterStatus == ISS_CREATE_AND_GO))
    {
        CLI_SET_ERR (CLI_ACL_FILTER_EXISTS);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nLOW:pIssAclL3FilterEntry value is not NULL and Filter status equal to the value of ISS_CREATE_AND_WAIT %d  with errorcode as %d\n",
                      ISS_CREATE_AND_WAIT, *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if (nmhGetIssAclTrafficSeperationCtrl (&i4RetStatus) == SNMP_SUCCESS)
    {
        if (i4RetStatus == ACL_TRAFFICSEPRTN_CTRL_SYSTEM_DEFAULT)
        {
            if ((pIssAclL3FilterEntry != NULL) &&
                (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE &&
                 i4TestValIssAclL3FilterStatus == ISS_DESTROY) &&
                (pIssAclL3FilterEntry->u1IssL3FilterCreationMode ==
                 ISS_ACL_CREATION_INTERNAL))
            {
                CLI_SET_ERR (CLI_ACL_DELETION_FAILED);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nLOW:pIssAclL3FilterEntry value is not NULL and Filter status is ACTIVE with errorcode as %d\n",
                              *pu4ErrorCode);
                return SNMP_FAILURE;
            }
        }
    }

#ifdef QOSX_WANTED
    /* Check if the filter is mapped to CLass Map entry. If its mapped 
     * filter should not be deleted. */
    if ((i4TestValIssAclL3FilterStatus == ISS_DESTROY)
        || (i4TestValIssAclL3FilterStatus == ISS_NOT_IN_SERVICE))
    {
        if (QoSUtlIsFilterMapToClsMapEntry (QOS_L3FILTER, i4IssAclL3FilterNo)
            == TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ACL_FILTER_MAPPED_TO_CLSMAP);
            return SNMP_FAILURE;
        }
    }
#endif

#if defined (DIFFSRV_WANTED)
    if ((i4TestValIssAclL3FilterStatus == ISS_DESTROY)
        || (i4TestValIssAclL3FilterStatus == ISS_NOT_IN_SERVICE))
    {
        ISS_UNLOCK ();

        if (DsCheckMFClfrTable (i4IssAclL3FilterNo, DS_IP_FILTER) == DS_FAILURE)
        {
            ISS_LOCK ();
            CLI_SET_ERR (CLI_ACL_FILTER_NO_CHANGE);
            return SNMP_FAILURE;
        }

        ISS_LOCK ();
    }
#endif
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL3FilterStatus function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterInPortChannelList
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterInPortChannelList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterInPortChannelList (UINT4 *pu4ErrorCode,
                                          INT4 i4IssAclL3FilterNo,
                                          tSNMP_OCTET_STRING_TYPE
                                          *
                                          pTestValIssAclL3FilterInPortChannelList)
{
    UINT4               u4IfIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2Port = 0;
    UINT1               u1PortFlag = 0;
    UINT1               u1IfType = 0;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL3FilterInPortChannelList function \n");
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    for (u4IfIndex = 0; u4IfIndex < ISS_PORT_LIST_SIZE; u4IfIndex++)
    {
        u1PortFlag =
            (UINT1) pTestValIssAclL3FilterInPortChannelList->
            pu1_OctetList[u4IfIndex];

        for (u2BitIndex = 0;
             ((u2BitIndex < BITS_PER_BYTE) && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & ISS_BIT8) != 0)
            {
                u2Port = (UINT2) ((u4IfIndex * 8) + u2BitIndex + 1);
                if (CfaValidateIfIndex ((UINT4) u2Port) == CFA_SUCCESS)
                {
                    CfaGetIfType ((UINT4) u2Port, &u1IfType);
                    if (u1IfType != CFA_LAGG)
                    {
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        return SNMP_FAILURE;
                    }

                    if (AclValidatePortFromPortChannel (u2Port) == CLI_FAILURE)
                    {
                        CLI_SET_ERR (CLI_ACL_SET_ERROR_ON_PORT);
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        return (SNMP_FAILURE);

                    }
                }
                else
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }

    if ((pTestValIssAclL3FilterInPortChannelList->i4_Length <= 0) ||
        (pTestValIssAclL3FilterInPortChannelList->i4_Length >
         ISS_PORT_CHANNEL_LIST_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nLOW:L2 L3FilterOutPortList length is less the zero or above the value of ISS_PORT_CHANNEL_LIST_SIZE%d  with errorcode as %d\n",
                      ISS_PORT_CHANNEL_LIST_SIZE, *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if (IssIsPortListValid
        (pTestValIssAclL3FilterInPortChannelList->pu1_OctetList,
         pTestValIssAclL3FilterInPortChannelList->i4_Length) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL3FilterInPortChannelList function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterOutPortChannelList
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterOutPortChannelList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterOutPortChannelList (UINT4 *pu4ErrorCode,
                                           INT4 i4IssAclL3FilterNo,
                                           tSNMP_OCTET_STRING_TYPE
                                           *
                                           pTestValIssAclL3FilterOutPortChannelList)
{
    UINT4               u4IfIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2Port = 0;
    UINT1               u1PortFlag = 0;
    UINT1               u1IfType = 0;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL3FilterOutPortChannelList function \n");
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    for (u4IfIndex = 0; u4IfIndex < ISS_PORT_LIST_SIZE; u4IfIndex++)
    {
        u1PortFlag =
            (UINT1) pTestValIssAclL3FilterOutPortChannelList->
            pu1_OctetList[u4IfIndex];

        for (u2BitIndex = 0;
             ((u2BitIndex < BITS_PER_BYTE) && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & ISS_BIT8) != 0)
            {
                u2Port = (UINT2) ((u4IfIndex * 8) + u2BitIndex + 1);
                if (CfaValidateIfIndex ((UINT4) u2Port) == CFA_SUCCESS)
                {
                    CfaGetIfType ((UINT4) u2Port, &u1IfType);
                    if (u1IfType != CFA_LAGG)
                    {
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        return SNMP_FAILURE;
                    }

                    if (AclValidatePortFromPortChannel (u2Port) == CLI_FAILURE)
                    {
                        CLI_SET_ERR (CLI_ACL_SET_ERROR_ON_PORT);
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        return SNMP_FAILURE;

                    }
                }
                else
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }

    if ((pTestValIssAclL3FilterOutPortChannelList->i4_Length <= 0) ||
        (pTestValIssAclL3FilterOutPortChannelList->i4_Length >
         ISS_PORT_CHANNEL_LIST_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nLOW:L2 L3FilterOutPortList length is less the zero or above the value of ISS_PORT_CHANNEL_LIST_SIZE%d  with errorcode as %d\n",
                      ISS_PORT_CHANNEL_LIST_SIZE, *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if (IssIsPortListValid
        (pTestValIssAclL3FilterOutPortChannelList->pu1_OctetList,
         pTestValIssAclL3FilterOutPortChannelList->i4_Length) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL3FilterOutPortChannelList function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterStatsEnabledStatus
 Input       :  The Indices
                IssAclL3FilterNo

                The Object
                testValIssAclL3FilterStatsEnabledStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterStatsEnabledStatus (UINT4 *pu4ErrorCode,
                                           INT4 i4IssAclL3FilterNo,
                                           INT4
                                           i4TestValIssAclL3FilterStatsEnabledStatus)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL3FilterStatsEnabledStatus function \n");

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        CLI_SET_ERR (CLI_ACL_INVALID_FILTER_ID);
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL3FilterStatsEnabledStatus != ACL_STAT_ENABLE)
        && (i4TestValIssAclL3FilterStatsEnabledStatus != ACL_STAT_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter hardware stats enabled status is invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        CLI_SET_ERR (CLI_ACL_FILTER_DATA_INSUFFICIENCY);
        return SNMP_FAILURE;
    }

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclL3FilterStatsEnabledStatus function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssAclClearL3FilterStats
 Input       :  The Indices
                IssAclL3FilterNo

                The Object
                testValIssAclClearL3FilterStats
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclClearL3FilterStats (UINT4 *pu4ErrorCode,
                                   INT4 i4IssAclL3FilterNo,
                                   INT4 i4TestValIssAclClearL3FilterStats)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclClearL3FilterStats function \n");

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        CLI_SET_ERR (CLI_ACL_INVALID_FILTER_ID);
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclClearL3FilterStats != ISS_TRUE) &&
        (i4TestValIssAclClearL3FilterStats != ISS_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter hardware stats clear status is invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        CLI_SET_ERR (CLI_ACL_FILTER_DATA_INSUFFICIENCY);
        return SNMP_FAILURE;
    }

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclClearL3FilterStats function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterSChannelIfIndex
 Input       :  The Indices
                IssAclL3FilterNo

                The Object
                u4TestValIssAclL2FilterSChannelIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterSChannelIfIndex (UINT4 *pu4ErrorCode,
                                        INT4 i4IssAclL3FilterNo,
                                        UINT4
                                        u4TestValIssAclL3FilterSChannelIfIndex)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclL3FilterSChannelIfIndex function \n");

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3 filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssAclL3FilterEntry value is  NULL with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:L3FilterStatus is Active with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((u4TestValIssAclL3FilterSChannelIfIndex < CFA_MIN_EVB_SBP_INDEX) &&
        (u4TestValIssAclL3FilterSChannelIfIndex > CFA_MAX_EVB_SBP_INDEX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:Invalid S-Channel Index with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IssAclL3FilterTable
 Input       :  The Indices
                IssAclL3FilterNo
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IssAclL3FilterTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IssAclUserDefinedFilterTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceIssAclUserDefinedFilterTable
Input       :  The Indices
IssAclUserDefinedFilterId
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssAclUserDefinedFilterTable (UINT4
                                                      u4IssAclUserDefinedFilterId)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        return SNMP_FAILURE;
    }

    pIssAclUdbFilterTableEntry =
        IssExtGetUdbFilterTableEntry (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFirstIndexIssAclUserDefinedFilterTable
Input       :  The Indices
IssAclUserDefinedFilterId
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssAclUserDefinedFilterTable (UINT4
                                              *pu4IssAclUserDefinedFilterId)
{
    if (IssExtSnmpLowGetFirstValidUDBFilterTableIndex
        ((INT4 *) pu4IssAclUserDefinedFilterId) == ISS_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetNextIndexIssAclUserDefinedFilterTable
Input       :  The Indices
IssAclUserDefinedFilterId
nextIssAclUserDefinedFilterId
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIssAclUserDefinedFilterTable (UINT4
                                             u4IssAclUserDefinedFilterId,
                                             UINT4
                                             *pu4NextIssAclUserDefinedFilterId)
{
    if (u4IssAclUserDefinedFilterId == 0)
    {
        return SNMP_FAILURE;
    }

    if ((IssSnmpLowGetNextValidUdbFilterTableIndex
         (u4IssAclUserDefinedFilterId,
          pu4NextIssAclUserDefinedFilterId)) != ISS_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
Function    :  nmhGetIssAclUserDefinedFilterPktType
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
retValIssAclUserDefinedFilterPktType
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetIssAclUserDefinedFilterPktType (UINT4
                                      u4IssAclUserDefinedFilterId,
                                      INT4
                                      *pi4RetValIssAclUserDefinedFilterPktType)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry != NULL)
    {
        *pi4RetValIssAclUserDefinedFilterPktType =
            pIssAclUdbFilterTableEntry->pAccessFilterEntry->
            u1AccessFilterPktType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetIssAclUserDefinedFilterOffSetBase
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
retValIssAclUserDefinedFilterOffSetBase
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetIssAclUserDefinedFilterOffSetBase (UINT4 u4IssAclUserDefinedFilterId,
                                         INT4
                                         *pi4RetValIssAclUserDefinedFilterOffSetBase)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry != NULL)
    {
        *pi4RetValIssAclUserDefinedFilterOffSetBase =
            pIssAclUdbFilterTableEntry->pAccessFilterEntry->
            u2AccessFilterOffset;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetIssAclUserDefinedFilterOffSetValue
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
retValIssAclUserDefinedFilterOffSetValue
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetIssAclUserDefinedFilterOffSetValue (UINT4 u4IssAclUserDefinedFilterId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pRetValIssAclUserDefinedFilterOffSetValue)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry != NULL)
    {

        ISS_MEMCPY (pRetValIssAclUserDefinedFilterOffSetValue->pu1_OctetList,
                    pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                    au1AccessFilterOffsetValue,
                    sizeof (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                            au1AccessFilterOffsetValue));

        pRetValIssAclUserDefinedFilterOffSetValue->i4_Length =
            ISS_UDB_MAX_OFFSET;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetIssAclUserDefinedFilterOffSetMask
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
retValIssAclUserDefinedFilterOffSetMask
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetIssAclUserDefinedFilterOffSetMask (UINT4 u4IssAclUserDefinedFilterId,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pRetValIssAclUserDefinedFilterOffSetMask)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry != NULL)
    {

        ISS_MEMCPY (pRetValIssAclUserDefinedFilterOffSetMask->pu1_OctetList,
                    (pIssAclUdbFilterTableEntry->
                     pAccessFilterEntry->au1AccessFilterOffsetMask),
                    sizeof (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                            au1AccessFilterOffsetMask));

        pRetValIssAclUserDefinedFilterOffSetMask->i4_Length =
            ISS_UDB_MAX_OFFSET;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetIssAclUserDefinedFilterPriority
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
retValIssAclUserDefinedFilterPriority
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetIssAclUserDefinedFilterPriority (UINT4
                                       u4IssAclUserDefinedFilterId,
                                       INT4
                                       *pi4RetValIssAclUserDefinedFilterPriority)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry != NULL)
    {
        *pi4RetValIssAclUserDefinedFilterPriority =
            pIssAclUdbFilterTableEntry->
            pAccessFilterEntry->u1AccessFilterPriority;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetIssAclUserDefinedFilterAction
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
retValIssAclUserDefinedFilterAction
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetIssAclUserDefinedFilterAction (UINT4
                                     u4IssAclUserDefinedFilterId,
                                     INT4
                                     *pi4RetValIssAclUserDefinedFilterAction)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->pAccessFilterEntry != NULL))
    {
        *pi4RetValIssAclUserDefinedFilterAction =
            pIssAclUdbFilterTableEntry->pAccessFilterEntry->
            IssAccessFilterAction;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetIssAclUserDefinedFilterInPortList
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
retValIssAclUserDefinedFilterInPortList
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetIssAclUserDefinedFilterInPortList (UINT4
                                         u4IssAclUserDefinedFilterId,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pRetValIssAclUserDefinedFilterInPortList)
{

    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry != NULL)
    {

        if (pIssAclUdbFilterTableEntry->pAccessFilterEntry != NULL)
        {
            ISS_MEMSET (pRetValIssAclUserDefinedFilterInPortList->pu1_OctetList,
                        0, ISS_PORT_LIST_SIZE);

            MEMCPY (pRetValIssAclUserDefinedFilterInPortList->pu1_OctetList,
                    pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                    IssUdbFilterInPortList, ISS_PORT_LIST_SIZE);

            pRetValIssAclUserDefinedFilterInPortList->i4_Length =
                ISS_PORT_LIST_SIZE;

            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetIssAclUserDefinedFilterIdOneType
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
retValIssAclUserDefinedFilterIdOneType
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetIssAclUserDefinedFilterIdOneType (UINT4
                                        u4IssAclUserDefinedFilterId,
                                        INT4
                                        *pi4RetValIssAclUserDefinedFilterIdOneType)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->pAccessFilterEntry != NULL))
    {
        *pi4RetValIssAclUserDefinedFilterIdOneType =
            pIssAclUdbFilterTableEntry->u1AclOneType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetIssAclUserDefinedFilterIdOne
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
retValIssAclUserDefinedFilterIdOne
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetIssAclUserDefinedFilterIdOne (UINT4
                                    u4IssAclUserDefinedFilterId,
                                    UINT4
                                    *pu4RetValIssAclUserDefinedFilterIdOne)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->pAccessFilterEntry != NULL))
    {
        *pu4RetValIssAclUserDefinedFilterIdOne =
            pIssAclUdbFilterTableEntry->u4BaseAclOne;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetIssAclUserDefinedFilterIdTwoType
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
retValIssAclUserDefinedFilterIdTwoType
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetIssAclUserDefinedFilterIdTwoType (UINT4
                                        u4IssAclUserDefinedFilterId,
                                        INT4
                                        *pi4RetValIssAclUserDefinedFilterIdTwoType)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->pAccessFilterEntry != NULL))
    {
        *pi4RetValIssAclUserDefinedFilterIdTwoType =
            pIssAclUdbFilterTableEntry->u1AclTwoType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetIssAclUserDefinedFilterIdTwo
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
retValIssAclUserDefinedFilterIdTwo
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetIssAclUserDefinedFilterIdTwo (UINT4
                                    u4IssAclUserDefinedFilterId,
                                    UINT4
                                    *pu4RetValIssAclUserDefinedFilterIdTwo)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->pAccessFilterEntry != NULL))
    {
        *pu4RetValIssAclUserDefinedFilterIdTwo =
            pIssAclUdbFilterTableEntry->u4BaseAclTwo;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetIssAclUserDefinedFilterSubAction
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
retValIssAclUserDefinedFilterSubAction
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetIssAclUserDefinedFilterSubAction (UINT4
                                        u4IssAclUserDefinedFilterId,
                                        INT4
                                        *pi4RetValIssAclUserDefinedFilterSubAction)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->pAccessFilterEntry != NULL))
    {
        *pi4RetValIssAclUserDefinedFilterSubAction =
            (INT4) pIssAclUdbFilterTableEntry->pAccessFilterEntry->
            u1IssUdbSubAction;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetIssAclUserDefinedFilterSubActionId
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
retValIssAclUserDefinedFilterSubActionId
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetIssAclUserDefinedFilterSubActionId (UINT4
                                          u4IssAclUserDefinedFilterId,
                                          INT4
                                          *pi4RetValIssAclUserDefinedFilterSubActionId)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->pAccessFilterEntry != NULL))
    {
        *pi4RetValIssAclUserDefinedFilterSubActionId =
            (INT4) pIssAclUdbFilterTableEntry->pAccessFilterEntry->
            u2IssUdbSubActionId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetIssAclUserDefinedFilterRedirectId
Input       :  The Indices
IssAclUserDefinedFilterId

The Object
retValIssAclUserDefinedFilterRedirectId
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetIssAclUserDefinedFilterRedirectId (UINT4 u4IssAclUserDefinedFilterId,
                                         INT4
                                         *pi4RetValIssAclUserDefinedFilterRedirectId)
{
    UNUSED_PARAM (u4IssAclUserDefinedFilterId);
    UNUSED_PARAM (pi4RetValIssAclUserDefinedFilterRedirectId);
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetIssAclUserDefinedFilterStatus
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
retValIssAclUserDefinedFilterStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
 /*ANU*/ INT1
nmhGetIssAclUserDefinedFilterStatus (UINT4
                                     u4IssAclUserDefinedFilterId,
                                     INT4
                                     *pi4RetValIssAclUserDefinedFilterStatus)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;
    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->pAccessFilterEntry != NULL))
    {
        *pi4RetValIssAclUserDefinedFilterStatus =
            pIssAclUdbFilterTableEntry->u1RowStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssAclUserDefinedFilterMatchCount
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object
                retValIssAclUserDefinedFilterMatchCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclUserDefinedFilterMatchCount (UINT4
                                         u4IssAclUserDefinedFilterId,
                                         UINT4
                                         *pu4RetValIssAclUserDefinedFilterMatchCount)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

#ifdef NPAPI_WANTED
    tIssL2FilterEntry  *pIssAclL2FilterEntryOne = NULL;
    tIssL3FilterEntry  *pIssAclL3FilterEntryTwo = NULL;
    INT4                i4RetVal = FNP_FAILURE;
#endif

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhGetIssAclUserDefinedFilterMatchCount function \n");

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry == NULL) ||
        (pIssAclUdbFilterTableEntry->pAccessFilterEntry == NULL))
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nLOW:pIssAclUdbFilterTableEntry is NULL \n");
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    pIssAclL2FilterEntryOne = IssExtGetL2FilterEntry
        ((INT4) pIssAclUdbFilterTableEntry->u4BaseAclOne);
    if (pIssAclL2FilterEntryOne == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nLOW:pIssAclL2FilterEntryOne is NULL \n");
        return SNMP_FAILURE;
    }

    pIssAclL3FilterEntryTwo = IssExtGetL3FilterEntry
        ((INT4) pIssAclUdbFilterTableEntry->u4BaseAclTwo);
    if (pIssAclL3FilterEntryTwo == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nLOW:pIssAclL3FilterEntryTwo is NULL \n");
        return SNMP_FAILURE;
    }

    if ((pIssAclUdbFilterTableEntry->u1RowStatus == ISS_ACTIVE) &&
        (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
         i4IssUdbFilterStatsEnabledStatus == ACL_STAT_ENABLE))
    {
        i4RetVal = IsssysIssHwUpdateUserDefinedFilter
            (pIssAclUdbFilterTableEntry->pAccessFilterEntry,
             pIssAclL2FilterEntryOne, pIssAclL3FilterEntryTwo,
             ISS_USERDEFINED_L2L3FILTER_STAT_GET);

        if (i4RetVal != FNP_SUCCESS)
        {
            *pu4RetValIssAclUserDefinedFilterMatchCount = 0;
            ISS_TRC (ALL_FAILURE_TRC,
                     "\nLOW:Udb filter failed to get hardware stats \n");
            return SNMP_SUCCESS;
        }
    }
#endif
    *pu4RetValIssAclUserDefinedFilterMatchCount =
        pIssAclUdbFilterTableEntry->pAccessFilterEntry->
        u4IssUdbFilterMatchCount;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhGetIssAclUserDefinedFilterMatchCount function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIssAclUserDefinedFilterStatsEnabledStatus
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object
                retValIssAclUserDefinedFilterStatsEnabledStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclUserDefinedFilterStatsEnabledStatus (UINT4
                                                 u4IssAclUserDefinedFilterId,
                                                 INT4
                                                 *pi4RetValIssAclUserDefinedFilterStatsEnabledStatus)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhGetIssAclUserDefinedFilterStatsEnabledStatus function \n");

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry == NULL) ||
        (pIssAclUdbFilterTableEntry->pAccessFilterEntry == NULL))
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nLOW:pIssAclUdbFilterTableEntry is NULL \n");
        return SNMP_FAILURE;
    }

    *pi4RetValIssAclUserDefinedFilterStatsEnabledStatus =
        pIssAclUdbFilterTableEntry->pAccessFilterEntry->
        i4IssUdbFilterStatsEnabledStatus;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhGetIssAclUserDefinedFilterStatsEnabledStatus function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIssAclClearUserDefinedFilterStats
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object
                retValIssAclClearUserDefinedFilterStats
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclClearUserDefinedFilterStats (UINT4 u4IssAclUserDefinedFilterId,
                                         INT4
                                         *pi4RetValIssAclClearUserDefinedFilterStats)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhGetIssAclClearUserDefinedFilterStats function \n");

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry == NULL) ||
        (pIssAclUdbFilterTableEntry->pAccessFilterEntry == NULL))
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nLOW:pIssAclUdbFilterTableEntry is NULL \n");
        return SNMP_FAILURE;
    }

    *pi4RetValIssAclClearUserDefinedFilterStats =
        pIssAclUdbFilterTableEntry->pAccessFilterEntry->
        i4IssClearUserDefinedFilterStats;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhGetIssAclClearUserDefinedFilterStats function \n");
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetIssAclUserDefinedFilterPktType
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
setValIssAclUserDefinedFilterPktType
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetIssAclUserDefinedFilterPktType (UINT4
                                      u4IssAclUserDefinedFilterId,
                                      INT4
                                      i4SetValIssAclUserDefinedFilterPktType)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->pAccessFilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->pAccessFilterEntry->u1AccessFilterPktType
        == i4SetValIssAclUserDefinedFilterPktType)
    {
        return SNMP_SUCCESS;
    }

    pIssAclUdbFilterTableEntry->pAccessFilterEntry->u1AccessFilterPktType
        = (UINT1) i4SetValIssAclUserDefinedFilterPktType;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetIssAclUserDefinedFilterOffSetBase
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
setValIssAclUserDefinedFilterOffSetBase
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetIssAclUserDefinedFilterOffSetBase (UINT4
                                         u4IssAclUserDefinedFilterId,
                                         INT4
                                         i4SetValIssAclUserDefinedFilterOffSetBase)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->pAccessFilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->pAccessFilterEntry->u2AccessFilterOffset
        == i4SetValIssAclUserDefinedFilterOffSetBase)
    {
        return SNMP_SUCCESS;
    }

    pIssAclUdbFilterTableEntry->pAccessFilterEntry->u2AccessFilterOffset
        = (UINT2) i4SetValIssAclUserDefinedFilterOffSetBase;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetIssAclUserDefinedFilterOffSetValue
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
setValIssAclUserDefinedFilterOffSetValue
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetIssAclUserDefinedFilterOffSetValue (UINT4
                                          u4IssAclUserDefinedFilterId,
                                          tSNMP_OCTET_STRING_TYPE
                                          *
                                          pSetValIssAclUserDefinedFilterOffSetValue)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->pAccessFilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    ISS_MEMCPY ((pIssAclUdbFilterTableEntry->
                 pAccessFilterEntry->au1AccessFilterOffsetValue),
                pSetValIssAclUserDefinedFilterOffSetValue->pu1_OctetList,
                pSetValIssAclUserDefinedFilterOffSetValue->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetIssAclUserDefinedFilterOffSetMask
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
setValIssAclUserDefinedFilterOffSetMask
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetIssAclUserDefinedFilterOffSetMask (UINT4
                                         u4IssAclUserDefinedFilterId,
                                         tSNMP_OCTET_STRING_TYPE
                                         *
                                         pSetValIssAclUserDefinedFilterOffSetMask)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->pAccessFilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    ISS_MEMCPY ((pIssAclUdbFilterTableEntry->
                 pAccessFilterEntry->au1AccessFilterOffsetMask),
                pSetValIssAclUserDefinedFilterOffSetMask->pu1_OctetList,
                pSetValIssAclUserDefinedFilterOffSetMask->i4_Length);

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetIssAclUserDefinedFilterPriority
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
setValIssAclUserDefinedFilterPriority
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetIssAclUserDefinedFilterPriority (UINT4
                                       u4IssAclUserDefinedFilterId,
                                       INT4
                                       i4SetValIssAclUserDefinedFilterPriority)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->pAccessFilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->pAccessFilterEntry->u1AccessFilterPriority
        == i4SetValIssAclUserDefinedFilterPriority)
    {
        return SNMP_SUCCESS;
    }

    pIssAclUdbFilterTableEntry->pAccessFilterEntry->u1AccessFilterPriority
        = (UINT1) i4SetValIssAclUserDefinedFilterPriority;

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetIssAclUserDefinedFilterAction
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
setValIssAclUserDefinedFilterAction
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetIssAclUserDefinedFilterAction (UINT4
                                     u4IssAclUserDefinedFilterId,
                                     INT4 i4SetValIssAclUserDefinedFilterAction)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->pAccessFilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->pAccessFilterEntry->IssAccessFilterAction
        == (tIssFilterAction) i4SetValIssAclUserDefinedFilterAction)
    {
        return SNMP_SUCCESS;
    }

    pIssAclUdbFilterTableEntry->pAccessFilterEntry->IssAccessFilterAction
        = i4SetValIssAclUserDefinedFilterAction;

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetIssAclUserDefinedFilterInPortList
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
setValIssAclUserDefinedFilterInPortList
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetIssAclUserDefinedFilterInPortList (UINT4
                                         u4IssAclUserDefinedFilterId,
                                         tSNMP_OCTET_STRING_TYPE
                                         *
                                         pSetValIssAclUserDefinedFilterInPortList)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry != NULL)
    {
        if (pIssAclUdbFilterTableEntry->pAccessFilterEntry != NULL)
        {
            ISS_MEMSET (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                        IssUdbFilterInPortList, 0, ISS_PORT_LIST_SIZE);
            ISS_MEMCPY (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                        IssUdbFilterInPortList,
                        pSetValIssAclUserDefinedFilterInPortList->pu1_OctetList,
                        pSetValIssAclUserDefinedFilterInPortList->i4_Length);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetIssAclUserDefinedFilterIdOneType
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
setValIssAclUserDefinedFilterIdOneType
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetIssAclUserDefinedFilterIdOneType (UINT4
                                        u4IssAclUserDefinedFilterId,
                                        INT4
                                        i4SetValIssAclUserDefinedFilterIdOneType)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->u1AclOneType
        == i4SetValIssAclUserDefinedFilterIdOneType)
    {
        return SNMP_SUCCESS;
    }

    pIssAclUdbFilterTableEntry->u1AclOneType
        = (UINT1) i4SetValIssAclUserDefinedFilterIdOneType;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetIssAclUserDefinedFilterIdOne
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
setValIssAclUserDefinedFilterIdOne
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetIssAclUserDefinedFilterIdOne (UINT4
                                    u4IssAclUserDefinedFilterId,
                                    UINT4 u4SetValIssAclUserDefinedFilterIdOne)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->u4BaseAclOne
        == u4SetValIssAclUserDefinedFilterIdOne)
    {
        return SNMP_SUCCESS;
    }

    pIssAclUdbFilterTableEntry->u4BaseAclOne
        = u4SetValIssAclUserDefinedFilterIdOne;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetIssAclUserDefinedFilterIdTwoType
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
setValIssAclUserDefinedFilterIdTwoType
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetIssAclUserDefinedFilterIdTwoType (UINT4
                                        u4IssAclUserDefinedFilterId,
                                        INT4
                                        i4SetValIssAclUserDefinedFilterIdTwoType)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->u1AclTwoType
        == i4SetValIssAclUserDefinedFilterIdTwoType)
    {
        return SNMP_SUCCESS;
    }

    pIssAclUdbFilterTableEntry->u1AclTwoType
        = (UINT1) i4SetValIssAclUserDefinedFilterIdTwoType;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetIssAclUserDefinedFilterIdTwo
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
setValIssAclUserDefinedFilterIdTwo
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetIssAclUserDefinedFilterIdTwo (UINT4
                                    u4IssAclUserDefinedFilterId,
                                    UINT4 u4SetValIssAclUserDefinedFilterIdTwo)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->u4BaseAclTwo
        == u4SetValIssAclUserDefinedFilterIdTwo)
    {
        return SNMP_SUCCESS;
    }

    pIssAclUdbFilterTableEntry->u4BaseAclTwo
        = u4SetValIssAclUserDefinedFilterIdTwo;

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetIssAclUserDefinedFilterSubAction
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
setValIssAclUserDefinedFilterSubAction
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetIssAclUserDefinedFilterSubAction (UINT4
                                        u4IssAclUserDefinedFilterId,
                                        INT4
                                        i4SetValIssAclUserDefinedFilterSubAction)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pIssAclUdbFilterTableEntry->pAccessFilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->pAccessFilterEntry->u1IssUdbSubAction
        == i4SetValIssAclUserDefinedFilterSubAction)
    {
        return SNMP_SUCCESS;
    }

    pIssAclUdbFilterTableEntry->pAccessFilterEntry->u1IssUdbSubAction
        = (UINT1) i4SetValIssAclUserDefinedFilterSubAction;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetIssAclUserDefinedFilterSubActionId
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
setValIssAclUserDefinedFilterSubActionId
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetIssAclUserDefinedFilterSubActionId (UINT4
                                          u4IssAclUserDefinedFilterId,
                                          INT4
                                          i4SetValIssAclUserDefinedFilterSubActionId)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pIssAclUdbFilterTableEntry->pAccessFilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->pAccessFilterEntry->u2IssUdbSubActionId
        == i4SetValIssAclUserDefinedFilterSubActionId)
    {
        return SNMP_SUCCESS;
    }

    pIssAclUdbFilterTableEntry->pAccessFilterEntry->u2IssUdbSubActionId
        = (UINT2) i4SetValIssAclUserDefinedFilterSubActionId;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetIssAclUserDefinedFilterStatus
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
setValIssAclUserDefinedFilterStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetIssAclUserDefinedFilterStatus (UINT4
                                     u4IssAclUserDefinedFilterId,
                                     INT4 i4SetValIssAclUserDefinedFilterStatus)
{
#ifdef NPAPI_WANTED
    INT4                i4RetVal = 0x0;
    tIssL2FilterEntry  *pIssAclL2FilterEntryOne = NULL;
    tIssL3FilterEntry  *pIssAclL3FilterEntryTwo = NULL;
    tIssUDBFilterEntry *pIssAclUdbFilterEntry = NULL;
    INT1                u1Action;
#endif
    INT4                i4CommitFlag = 0;
    INT4                i4CommitAction = 0;
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);
    if (pIssAclUdbFilterTableEntry != NULL)
    {
        if (pIssAclUdbFilterTableEntry->u1RowStatus ==
            (UINT1) i4SetValIssAclUserDefinedFilterStatus)
        {
            if (pIssAclUdbFilterTableEntry->u1PriorityFlag == ISS_FALSE)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    else
    {
        /* This Filter Entry is not there in the Database */
        if ((i4SetValIssAclUserDefinedFilterStatus != ISS_CREATE_AND_WAIT) &&
            (i4SetValIssAclUserDefinedFilterStatus != ISS_CREATE_AND_GO))
        {
            return SNMP_FAILURE;
        }
    }

    switch (i4SetValIssAclUserDefinedFilterStatus)
    {

        case ISS_CREATE_AND_WAIT:
        case ISS_CREATE_AND_GO:
            if (pIssAclUdbFilterTableEntry == NULL)
            {
                if (ISS_UDB_FILTER_TABLE_ALLOC_MEM_BLOCK
                    (pIssAclUdbFilterTableEntry) == NULL)
                {
                    return SNMP_FAILURE;
                }
            }
            ISS_MEMSET (pIssAclUdbFilterTableEntry, 0,
                        sizeof (tIssUserDefinedFilterTable));
            if (pIssAclUdbFilterTableEntry->pAccessFilterEntry == NULL)
            {
                if (ISS_UDB_FILTER_ENTRY_ALLOC_MEM_BLOCK
                    (pIssAclUdbFilterTableEntry->pAccessFilterEntry) == NULL)
                {
                    return SNMP_FAILURE;
                }
            }

            ISS_MEMSET (pIssAclUdbFilterTableEntry->pAccessFilterEntry, 0,
                        sizeof (tIssUDBFilterEntry));

            ISS_MEMSET (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                        au1AccessFilterOffsetValue, 0, ISS_UDB_MAX_OFFSET);

            ISS_MEMSET (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                        au1AccessFilterOffsetMask, 0, ISS_UDB_MAX_OFFSET);

            /* Setting the RowStatus to NOT_READY if the
             * filter if i4SetValIssAclL2FilterStatus is ISS_CREATE_AND_WAIT*/
            pIssAclUdbFilterTableEntry->pAccessFilterEntry->u4UDBFilterId
                = u4IssAclUserDefinedFilterId;
            pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                u1AccessFilterPktType = ISS_UDB_PKT_TYPE_USER_DEF;
            pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                u1AccessFilterPriority = ISS_DEFAULT_FILTER_PRIORITY;
            pIssAclUdbFilterTableEntry->u1AclOneType = 0;
            pIssAclUdbFilterTableEntry->u1AclTwoType = 0;
            pIssAclUdbFilterTableEntry->u1PriorityFlag = ISS_FALSE;
            pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                RedirectIfGrp.u1PriorityFlag = ISS_FALSE;
            pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                u4IssUdbFilterMatchCount = 0;
            pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                i4IssClearUserDefinedFilterStats = ISS_FALSE;
            pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                i4IssUdbFilterStatsEnabledStatus = ACL_STAT_DISABLE;
            /* Adding the Entry to the Filter List */
            ISS_SLL_ADD (&(ISS_UDB_FILTER_TABLE_LIST),
                         &(pIssAclUdbFilterTableEntry->IssNextNode));

            pIssAclUdbFilterTableEntry->u1RowStatus = ISS_NOT_IN_SERVICE;
            break;

        case ISS_NOT_IN_SERVICE:

            if (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                IssAccessFilterAction == ISS_REDIRECT_TO)
            {
                pIssAclUdbFilterTableEntry->u1RowStatus = ISS_NOT_IN_SERVICE;
                return (SNMP_SUCCESS);
            }

#ifdef NPAPI_WANTED
            pIssAclUdbFilterEntry =
                pIssAclUdbFilterTableEntry->pAccessFilterEntry;

            if ((pIssAclUdbFilterTableEntry->u1RowStatus == ISS_ACTIVE) &&
                (pIssAclUdbFilterEntry != NULL) &&
                (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE))
            {
                /* If counter is enabled then set flag to detach and
                 * reattach counter after row status becomes ACTIVE */
                if (pIssAclUdbFilterEntry->i4IssUdbFilterStatsEnabledStatus
                    == ACL_STAT_ENABLE)
                {
                    i4RetVal =
                        IsssysIssHwUpdateUserDefinedFilter
                        (pIssAclUdbFilterEntry, NULL, NULL,
                         ISS_USERDEFINED_L2L3FILTER_STAT_DISABLE);

                    if (i4RetVal != FNP_SUCCESS)
                    {
                        ISS_TRC (ALL_FAILURE_TRC,
                                 "\nLOW:UDB filter cannot disable hardware stats \n");
                        CLI_SET_ERR (CLI_ACL_HW_UPDATE_FAILED);
                        return SNMP_FAILURE;
                    }
                }
            }
#endif

            IssExDeleteAclPriorityFilterTable (pIssAclUdbFilterTableEntry->
                                               pAccessFilterEntry->
                                               u1AccessFilterPriority,
                                               &(pIssAclUdbFilterTableEntry->
                                                 IssNextNode),
                                               ISS_UDB_REDIRECT);

            i4CommitFlag = IssExGetIssCommitSupportImmediate ();
            i4CommitAction = IssGetTriggerCommit ();

            if ((i4CommitFlag == ISS_TRUE) || (i4CommitAction == ISS_TRUE))
            {
#ifdef NPAPI_WANTED
                if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                {

                    if ((pIssAclUdbFilterTableEntry->
                         pAccessFilterEntry->IssAccessFilterAction
                         == ISS_ALLOW) ||
                        (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                         IssAccessFilterAction == ISS_DROP))
                    {
                        IsssysIssHwUpdateUserDefinedFilter
                            (pIssAclUdbFilterTableEntry->pAccessFilterEntry,
                             NULL, NULL, ISS_USERDEFINED_ACCESSFILTER_DELETE);
                    }
                    else if (pIssAclUdbFilterTableEntry->
                             pAccessFilterEntry->IssAccessFilterAction ==
                             ISS_NOT)
                    {

                        if (pIssAclUdbFilterTableEntry->u1AclOneType ==
                            ISS_L2FILTER)
                        {
                            IsssysIssHwUpdateUserDefinedFilter
                                (pIssAclUdbFilterTableEntry->pAccessFilterEntry,
                                 NULL, NULL, ISS_USERDEFINED_L2L2FILTER_DELETE);
                        }
                        else if (pIssAclUdbFilterTableEntry->u1AclOneType ==
                                 ISS_L3_FILTER)
                        {
                            IsssysIssHwUpdateUserDefinedFilter
                                (pIssAclUdbFilterTableEntry->pAccessFilterEntry,
                                 NULL, NULL, ISS_USERDEFINED_L3L3FILTER_DELETE);
                        }
                    }
                    else if ((pIssAclUdbFilterTableEntry->
                              pAccessFilterEntry->IssAccessFilterAction ==
                              ISS_AND) || (pIssAclUdbFilterTableEntry->
                                           pAccessFilterEntry->
                                           IssAccessFilterAction == ISS_OR))
                    {

                        if ((pIssAclUdbFilterTableEntry->u1AclOneType ==
                             ISS_L2FILTER) &&
                            (pIssAclUdbFilterTableEntry->u1AclTwoType ==
                             ISS_L2FILTER))
                        {
                            IsssysIssHwUpdateUserDefinedFilter
                                (pIssAclUdbFilterTableEntry->pAccessFilterEntry,
                                 NULL, NULL, ISS_USERDEFINED_L2L2FILTER_DELETE);
                        }
                        else if ((pIssAclUdbFilterTableEntry->u1AclOneType ==
                                  ISS_L3FILTER) &&
                                 (pIssAclUdbFilterTableEntry->u1AclTwoType ==
                                  ISS_L3FILTER))
                        {

                            IsssysIssHwUpdateUserDefinedFilter
                                (pIssAclUdbFilterTableEntry->pAccessFilterEntry,
                                 NULL, NULL, ISS_USERDEFINED_L3L3FILTER_DELETE);
                        }
                        else if (pIssAclUdbFilterTableEntry->u1AclOneType !=
                                 pIssAclUdbFilterTableEntry->u1AclTwoType)

                        {
                            IsssysIssHwUpdateUserDefinedFilter
                                (pIssAclUdbFilterTableEntry->pAccessFilterEntry,
                                 NULL, NULL, ISS_USERDEFINED_L2L3FILTER_DELETE);

                        }

                    }
                }
#endif
            }
#ifdef NPAPI_WANTED
            if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
            {
                i4RetVal =
                    IssDeInstallUserDefinedFilter (pIssAclUdbFilterTableEntry->
                                                   pAccessFilterEntry);

                if (i4RetVal != FNP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
#endif
            pIssAclUdbFilterTableEntry->u1RowStatus = ISS_NOT_IN_SERVICE;

            break;

        case ISS_ACTIVE:

#ifdef NPAPI_WANTED
            pIssAclUdbFilterEntry =
                pIssAclUdbFilterTableEntry->pAccessFilterEntry;

            pIssAclL2FilterEntryOne =
                IssExtGetL2FilterEntry
                (pIssAclUdbFilterTableEntry->u4BaseAclOne);
            if (pIssAclL2FilterEntryOne == NULL)
            {
                return SNMP_FAILURE;
            }

            pIssAclL3FilterEntryTwo =
                IssExtGetL3FilterEntry
                (pIssAclUdbFilterTableEntry->u4BaseAclTwo);

            if (pIssAclL3FilterEntryTwo == NULL)
            {
                return ISS_FAILURE;
            }

            if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
            {
                if (pIssAclL2FilterEntryOne->i4IssL2FilterStatsEnabledStatus
                    == ACL_STAT_ENABLE)
                {
                    pIssAclL2FilterEntryOne->i4IssL2FilterStatsEnabledStatus
                        = ACL_STAT_DISABLE;
                    i4RetVal =
                        IsssysIssHwUpdateL2Filter
                        (pIssAclL2FilterEntryOne, ISS_L2FILTER_STAT_DISABLE);
                    if (i4RetVal != FNP_SUCCESS)
                    {
                        ISS_TRC (ALL_FAILURE_TRC,
                                 "\nLOW:L2 filter cannot disable hardware stats \n");
                    }
                }
                i4RetVal =
                    IsssysIssHwUpdateL2Filter
                    (pIssAclL2FilterEntryOne, ISS_L2FILTER_DELETE);
                if (i4RetVal != FNP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }

                pIssAclL2FilterEntryOne->u1IssL2HwStatus = ISS_NOTOK;

                if (pIssAclL3FilterEntryTwo->i4IssL3FilterStatsEnabledStatus
                    == ACL_STAT_ENABLE)
                {
                    pIssAclL3FilterEntryTwo->i4IssL3FilterStatsEnabledStatus
                        = ACL_STAT_DISABLE;
                    i4RetVal =
                        IsssysIssHwUpdateL3Filter
                        (pIssAclL3FilterEntryTwo, ISS_L3FILTER_STAT_DISABLE);
                    if (i4RetVal != FNP_SUCCESS)
                    {
                        ISS_TRC (ALL_FAILURE_TRC,
                                 "\nLOW:L3 filter cannot disable hardware stats \n");
                    }
                }
                i4RetVal =
                    IsssysIssHwUpdateL3Filter
                    (pIssAclL3FilterEntryTwo, ISS_L3FILTER_DELETE);
                if (i4RetVal != FNP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
                pIssAclL3FilterEntryTwo->u1IssL3HwStatus = ISS_NOTOK;

                u1Action = ISS_USERDEFINED_L2L3FILTER_APPLY_AND_OP;

                i4RetVal =
                    IsssysIssHwUpdateUserDefinedFilter
                    (pIssAclUdbFilterTableEntry->
                     pAccessFilterEntry,
                     pIssAclL2FilterEntryOne,
                     pIssAclL3FilterEntryTwo, (UINT4) u1Action);
                if (i4RetVal != FNP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
#endif
            pIssAclUdbFilterTableEntry->u1RowStatus = ISS_ACTIVE;

#ifdef NPAPI_WANTED
            if ((ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE) &&
                (pIssAclUdbFilterEntry != NULL))
            {
                /* Enable counter if required */
                if (pIssAclUdbFilterEntry->i4IssUdbFilterStatsEnabledStatus
                    == ACL_STAT_ENABLE)
                {
                    i4RetVal =
                        IsssysIssHwUpdateUserDefinedFilter
                        (pIssAclUdbFilterEntry, NULL, NULL,
                         ISS_USERDEFINED_L2L3FILTER_STAT_ENABLE);

                    if (i4RetVal != FNP_SUCCESS)
                    {
                        ISS_TRC (ALL_FAILURE_TRC,
                                 "\nLOW:UDB filter cannot enable hardware stats \n");
                        CLI_SET_ERR (CLI_ACL_HW_UPDATE_FAILED);
                        return SNMP_FAILURE;
                    }
                }
            }
#endif
            break;

        case ISS_DESTROY:

#ifdef NPAPI_WANTED

            if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
            {
                pIssAclUdbFilterEntry =
                    pIssAclUdbFilterTableEntry->pAccessFilterEntry;
                if ((pIssAclUdbFilterEntry != NULL)
                    && (pIssAclUdbFilterEntry->
                        i4IssUdbFilterStatsEnabledStatus == ACL_STAT_ENABLE))
                {
                    pIssAclUdbFilterEntry->i4IssUdbFilterStatsEnabledStatus
                        = ACL_STAT_DISABLE;
                    i4RetVal =
                        IsssysIssHwUpdateUserDefinedFilter
                        (pIssAclUdbFilterEntry, NULL, NULL,
                         ISS_USERDEFINED_L2L3FILTER_STAT_DISABLE);

                    if (i4RetVal != FNP_SUCCESS)
                    {
                        ISS_TRC (ALL_FAILURE_TRC,
                                 "\nLOW:UDB filter cannot disable hardware stats \n");
                    }
                }

                i4RetVal =
                    IssDeInstallUserDefinedFilter (pIssAclUdbFilterTableEntry->
                                                   pAccessFilterEntry);

                if (i4RetVal != FNP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
#endif
            if (pIssAclUdbFilterTableEntry->u1PriorityFlag == ISS_FALSE)
            {
                /* Deleting the node from the list */
                ISS_SLL_DELETE (&(ISS_UDB_FILTER_TABLE_LIST),
                                &(pIssAclUdbFilterTableEntry->IssNextNode));

                /* Delete the Entry from the Software */
                if (ISS_UDB_FILTERENTRY_FREE_MEM_BLOCK
                    (pIssAclUdbFilterTableEntry->pAccessFilterEntry) !=
                    MEM_SUCCESS)
                {
                    ISS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                             "UDB Filter Entry Free Failure\n");
                    return SNMP_FAILURE;
                }

                /* Delete the Entry from the Software */
                if (ISS_UDB_FILTERTABLE_FREE_MEM_BLOCK
                    (pIssAclUdbFilterTableEntry) != MEM_SUCCESS)
                {
                    ISS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                             "UDB Filter Table Free Failure\n");
                    return SNMP_FAILURE;
                }
            }
            break;

        default:

            return SNMP_FAILURE;

    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssAclUserDefinedFilterStatsEnabledStatus
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object
                setValIssAclUserDefinedFilterStatsEnabledStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclUserDefinedFilterStatsEnabledStatus (UINT4
                                                 u4IssAclUserDefinedFilterId,
                                                 INT4
                                                 i4SetValIssAclUserDefinedFilterStatsEnabledStatus)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

#ifdef NPAPI_WANTED
    tIssL2FilterEntry  *pIssAclL2FilterEntryOne = NULL;
    tIssL3FilterEntry  *pIssAclL3FilterEntryTwo = NULL;
    INT4                i4RetVal = FNP_FAILURE;
    INT4                i4HwStatsFlag;
    INT4                i4StatusOld;
#endif

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhSetIssAclUserDefinedFilterStatsEnabledStatus function \n");

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry == NULL) ||
        (pIssAclUdbFilterTableEntry->pAccessFilterEntry == NULL))
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nLOW:pIssAclUdbFilterTableEntry is NULL \n");
        CLI_SET_ERR (CLI_ACL_NO_FILTER);
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    pIssAclL2FilterEntryOne = IssExtGetL2FilterEntry
        ((INT4) pIssAclUdbFilterTableEntry->u4BaseAclOne);
    if (pIssAclL2FilterEntryOne == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nLOW:pIssAclL2FilterEntryOne is NULL \n");
        CLI_SET_ERR (CLI_ACL_FILTER_DATA_INSUFFICIENCY);
        return SNMP_FAILURE;
    }

    pIssAclL3FilterEntryTwo = IssExtGetL3FilterEntry
        ((INT4) pIssAclUdbFilterTableEntry->u4BaseAclTwo);
    if (pIssAclL3FilterEntryTwo == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nLOW:pIssAclL3FilterEntryTwo is NULL \n");
        CLI_SET_ERR (CLI_ACL_FILTER_DATA_INSUFFICIENCY);
        return SNMP_FAILURE;
    }

    i4StatusOld = pIssAclUdbFilterTableEntry->pAccessFilterEntry->
        i4IssUdbFilterStatsEnabledStatus;
#endif
    pIssAclUdbFilterTableEntry->pAccessFilterEntry->
        i4IssUdbFilterStatsEnabledStatus =
        i4SetValIssAclUserDefinedFilterStatsEnabledStatus;
#ifdef NPAPI_WANTED
    if (pIssAclUdbFilterTableEntry->u1RowStatus == ISS_ACTIVE)
    {
        if (i4SetValIssAclUserDefinedFilterStatsEnabledStatus ==
            ACL_STAT_ENABLE)
        {
            i4HwStatsFlag = ISS_USERDEFINED_L2L3FILTER_STAT_ENABLE;
        }
        else
        {
            i4HwStatsFlag = ISS_USERDEFINED_L2L3FILTER_STAT_DISABLE;
        }

        i4RetVal = IsssysIssHwUpdateUserDefinedFilter
            (pIssAclUdbFilterTableEntry->pAccessFilterEntry,
             pIssAclL2FilterEntryOne, pIssAclL3FilterEntryTwo, i4HwStatsFlag);

        if (i4RetVal != FNP_SUCCESS)
        {
            pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                i4IssUdbFilterStatsEnabledStatus = i4StatusOld;
            ISS_TRC (ALL_FAILURE_TRC,
                     "\nLOW:Udb filter failed to enable hardware stats \n");
            CLI_SET_ERR (CLI_ACL_HW_UPDATE_FAILED);
            return SNMP_FAILURE;
        }
    }
#endif

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhSetIssAclUserDefinedFilterStatsEnabledStatus function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssAclClearUserDefinedFilterStats
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object
                setValIssAclClearUserDefinedFilterStats
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclClearUserDefinedFilterStats (UINT4 u4IssAclUserDefinedFilterId,
                                         INT4
                                         i4SetValIssAclClearUserDefinedFilterStats)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

#ifdef NPAPI_WANTED
    tIssL2FilterEntry  *pIssAclL2FilterEntryOne = NULL;
    tIssL3FilterEntry  *pIssAclL3FilterEntryTwo = NULL;
    INT4                i4RetVal = FNP_FAILURE;
#endif

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhSetIssAclClearUserDefinedFilterStats function \n");

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry == NULL) ||
        (pIssAclUdbFilterTableEntry->pAccessFilterEntry == NULL))
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nLOW:pIssAclUdbFilterTableEntry is NULL \n");
        CLI_SET_ERR (CLI_ACL_FILTER_DATA_INSUFFICIENCY);
        return SNMP_FAILURE;
    }

    pIssAclUdbFilterTableEntry->pAccessFilterEntry->
        i4IssClearUserDefinedFilterStats =
        i4SetValIssAclClearUserDefinedFilterStats;

#ifdef NPAPI_WANTED
    pIssAclL2FilterEntryOne = IssExtGetL2FilterEntry
        ((INT4) pIssAclUdbFilterTableEntry->u4BaseAclOne);
    if (pIssAclL2FilterEntryOne == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nLOW:pIssAclL2FilterEntryOne is NULL \n");
        CLI_SET_ERR (CLI_ACL_FILTER_DATA_INSUFFICIENCY);
        return SNMP_FAILURE;
    }

    pIssAclL3FilterEntryTwo = IssExtGetL3FilterEntry
        ((INT4) pIssAclUdbFilterTableEntry->u4BaseAclTwo);
    if (pIssAclL3FilterEntryTwo == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\nLOW:pIssAclL3FilterEntryTwo is NULL \n");
        CLI_SET_ERR (CLI_ACL_FILTER_DATA_INSUFFICIENCY);
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
        i4IssUdbFilterStatsEnabledStatus == ACL_STAT_ENABLE)
    {
        i4RetVal = IsssysIssHwUpdateUserDefinedFilter
            (pIssAclUdbFilterTableEntry->pAccessFilterEntry,
             pIssAclL2FilterEntryOne, pIssAclL3FilterEntryTwo,
             ISS_USERDEFINED_L2L3FILTER_STAT_CLEAR);

        if (i4RetVal != FNP_SUCCESS)
        {
            pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                i4IssClearUserDefinedFilterStats = ISS_FALSE;
            ISS_TRC (ALL_FAILURE_TRC,
                     "\nLOW:Udb filter failed to clear hardware stats \n");
            CLI_SET_ERR (CLI_ACL_HW_UPDATE_FAILED);
            return SNMP_FAILURE;
        }
    }
#endif
    pIssAclUdbFilterTableEntry->pAccessFilterEntry->
        u4IssUdbFilterMatchCount = 0;
    pIssAclUdbFilterTableEntry->pAccessFilterEntry->
        i4IssClearUserDefinedFilterStats = ISS_FALSE;

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhSetIssAclClearUserDefinedFilterStats function \n");
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2IssAclUserDefinedFilterPktType
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
testValIssAclUserDefinedFilterPktType
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2IssAclUserDefinedFilterPktType (UINT4
                                         *pu4ErrorCode,
                                         UINT4
                                         u4IssAclUserDefinedFilterId,
                                         INT4
                                         i4TestValIssAclUserDefinedFilterPktType)
{

    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclUserDefinedFilterPktType function \n");
    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:UserDefined filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->u1RowStatus == ISS_ACTIVE))
    {
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssAclUdbFilterTableEntry value is not NULL and row status is ACTIVE with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if (i4TestValIssAclUserDefinedFilterPktType > ISS_UDB_PKT_TYPE_IPV6_UDP)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nLOW:UserDefinedFilterPktType value is above tha  value of ISS_UDB_PKT_TYPE_IPV6_UDP %dwith errorcode as %d\n",
                      ISS_UDB_PKT_TYPE_IPV6_UDP, *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclUserDefinedFilterPktType function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2IssAclUserDefinedFilterOffSetBase
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
testValIssAclUserDefinedFilterOffSetBase
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2IssAclUserDefinedFilterOffSetBase (UINT4
                                            *pu4ErrorCode,
                                            UINT4
                                            u4IssAclUserDefinedFilterId,
                                            INT4
                                            i4TestValIssAclUserDefinedFilterOffSetBase)
{

    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclUserDefinedFilterOffSetBase function \n");
    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:UserDefined filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->u1RowStatus == ISS_ACTIVE))
    {
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssAclUdbFilterTableEntry value is not NULL and row status is ACTIVE with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclUserDefinedFilterOffSetBase < 0) ||
        (i4TestValIssAclUserDefinedFilterOffSetBase >
         ISS_UDB_OFFSET_MPLS_MINUS_2))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:UserDefinedFilter OffSetBase value is less than zero with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclUserDefinedFilterOffSetBase function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2IssAclUserDefinedFilterOffSetValue
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
testValIssAclUserDefinedFilterOffSetValue
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2IssAclUserDefinedFilterOffSetValue (UINT4
                                             *pu4ErrorCode,
                                             UINT4
                                             u4IssAclUserDefinedFilterId,
                                             tSNMP_OCTET_STRING_TYPE
                                             *
                                             pTestValIssAclUserDefinedFilterOffSetValue)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclUserDefinedFilterOffSetValue function \n");
    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:UserDefined filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->u1RowStatus == ISS_ACTIVE))
    {
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssAclUdbFilterTableEntry value is not NULL and row status is ACTIVE with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    if ((pTestValIssAclUserDefinedFilterOffSetValue->i4_Length <= 0) ||
        (pTestValIssAclUserDefinedFilterOffSetValue->i4_Length >
         ISS_UDB_MAX_OFFSET))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nLOW:UserDefined filter OffSetValue length is less than or equal to zero or greater than  value of ISS_UDB_MAX_OFFSET%d with errorcode as %d\n",
                      ISS_UDB_MAX_OFFSET, *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclUserDefinedFilterOffSetValue function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2IssAclUserDefinedFilterOffSetMask
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
testValIssAclUserDefinedFilterOffSetMask
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2IssAclUserDefinedFilterOffSetMask (UINT4
                                            *pu4ErrorCode,
                                            UINT4
                                            u4IssAclUserDefinedFilterId,
                                            tSNMP_OCTET_STRING_TYPE
                                            *
                                            pTestValIssAclUserDefinedFilterOffSetMask)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclUserDefinedFilterOffSetMask function \n");
    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:UserDefined filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);
    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->u1RowStatus == ISS_ACTIVE))
    {
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssAclUdbFilterTableEntry value is not NULL and row status is ACTIVE with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    if ((pTestValIssAclUserDefinedFilterOffSetMask->i4_Length <= 0) ||
        (pTestValIssAclUserDefinedFilterOffSetMask->i4_Length >
         ISS_UDB_MAX_OFFSET))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nLOW:UserDefined filter OffSetmask length is less than or eual to zero and greater than value of ISS_UDB_MAX_OFFSET %d  with errorcode as %d\n",
                      ISS_UDB_MAX_OFFSET, *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclUserDefinedFilterOffSetMask function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2IssAclUserDefinedFilterPriority
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
testValIssAclUserDefinedFilterPriority
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2IssAclUserDefinedFilterPriority (UINT4
                                          *pu4ErrorCode,
                                          UINT4
                                          u4IssAclUserDefinedFilterId,
                                          INT4
                                          i4TestValIssAclUserDefinedFilterPriority)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclUserDefinedFilterPriority function \n");
    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:UserDefined filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->u1RowStatus == ISS_ACTIVE))
    {
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssAclUdbFilterTableEntry value is not NULL and row status is ACTIVE with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclUserDefinedFilterPriority <= 0) ||
        (i4TestValIssAclUserDefinedFilterPriority > 255))
    {
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:i4TestValIssAclUserDefinedFilterPriority value less than or equal to zero or           greater than 255 with errorcode as %d\n",
                      *pu4ErrorCode);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclUserDefinedFilterPriority function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2IssAclUserDefinedFilterAction
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
testValIssAclUserDefinedFilterAction
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2IssAclUserDefinedFilterAction (UINT4
                                        *pu4ErrorCode,
                                        UINT4
                                        u4IssAclUserDefinedFilterId,
                                        INT4
                                        i4TestValIssAclUserDefinedFilterAction)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclUserDefinedFilterAction function \n");
    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:UserDefined filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->u1RowStatus == ISS_ACTIVE))
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nLOW:pIssAclUdbFilterTableEntry value is not NULL and row status is ACTIVE\n");
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclUserDefinedFilterAction != ISS_ALLOW) &&
        (i4TestValIssAclUserDefinedFilterAction != ISS_DROP) &&
        (i4TestValIssAclUserDefinedFilterAction != ISS_REDIRECT_TO) &&
        (i4TestValIssAclUserDefinedFilterAction != ISS_AND) &&
        (i4TestValIssAclUserDefinedFilterAction != ISS_OR) &&
        (i4TestValIssAclUserDefinedFilterAction != ISS_NOT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclUserDefinedFilterAction function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2IssAclUserDefinedFilterInPortList
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
testValIssAclUserDefinedFilterInPortList
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2IssAclUserDefinedFilterInPortList (UINT4
                                            *pu4ErrorCode,
                                            UINT4
                                            u4IssAclUserDefinedFilterId,
                                            tSNMP_OCTET_STRING_TYPE
                                            *
                                            pTestValIssAclUserDefinedFilterInPortList)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclUserDefinedFilterInPortList function \n");
    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:UserDefined filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssAclUdbFilterTableEntry value is NULL with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->u1RowStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssAclUdbFilterTableEntry RowStatus active errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((pTestValIssAclUserDefinedFilterInPortList->i4_Length <= 0) ||
        (pTestValIssAclUserDefinedFilterInPortList->i4_Length >
         ISS_PORT_LIST_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nLOW:UserDefined filter InPortList length is less than or equal to zero or greater than       value of ISS_PORT_LIST_SIZE %d with errorcode as %d\n",
                      ISS_PORT_LIST_SIZE, *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    /* This function checks if any port greater than the maximum number of
     * ports in the system is in the port list.*/

    if (IssIsPortListValid
        (pTestValIssAclUserDefinedFilterInPortList->pu1_OctetList,
         pTestValIssAclUserDefinedFilterInPortList->i4_Length) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssAclUdbFilterTableEntry RowStatus active errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclUserDefinedFilterInPortList function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2IssAclUserDefinedFilterIdOneType
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
testValIssAclUserDefinedFilterIdOneType
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2IssAclUserDefinedFilterIdOneType (UINT4
                                           *pu4ErrorCode,
                                           UINT4
                                           u4IssAclUserDefinedFilterId,
                                           INT4
                                           i4TestValIssAclUserDefinedFilterIdOneType)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclUserDefinedFilterIdOneType function \n");
    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:UserDefined filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->u1RowStatus == ISS_ACTIVE))
    {
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssAclUdbFilterTableEntry value is not NULL and row status is ACTIVE with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclUserDefinedFilterIdOneType != ISS_L2_FILTER) &&
        (i4TestValIssAclUserDefinedFilterIdOneType != ISS_L3_FILTER))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nLOW:i4TestValIssAclUserDefinedFilterIdOneType value not is not equql to value of            ISS_L2_FILTER%d ISS_L3_FILTER%d value e errorcode as %d\n",
                      ISS_L2_FILTER, ISS_L3_FILTER, *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclUserDefinedFilterIdOneType function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2IssAclUserDefinedFilterIdOne
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
testValIssAclUserDefinedFilterIdOne
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2IssAclUserDefinedFilterIdOne (UINT4
                                       *pu4ErrorCode,
                                       UINT4
                                       u4IssAclUserDefinedFilterId,
                                       UINT4
                                       u4TestValIssAclUserDefinedFilterIdOne)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclUserDefinedFilterIdOne function \n");
    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:UserDefined filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->u1RowStatus == ISS_ACTIVE))
    {
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssAclUdbFilterTableEntry value is not NULL and row status is ACTIVE with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if (u4TestValIssAclUserDefinedFilterIdOne == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:u4TestValIssAclUserDefinedFilterIdOne value is zero\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclUserDefinedFilterIdOne function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2IssAclUserDefinedFilterSubAction
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
testValIssAclUserDefinedFilterSubAction
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2IssAclUserDefinedFilterSubAction (UINT4
                                           *pu4ErrorCode,
                                           UINT4
                                           u4IssAclUserDefinedFilterId,
                                           INT4
                                           i4TestValIssAclUserDefinedFilterSubAction)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclUserDefinedFilterSubAction function \n");
    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:UserDefined filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry != NULL &&
        pIssAclUdbFilterTableEntry->u1RowStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssAclUdbFilterTableEntry value is not NULL and row status is ACTIVE with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclUserDefinedFilterSubAction != ISS_NONE) &&
        (i4TestValIssAclUserDefinedFilterSubAction != ISS_MODIFY_VLAN) &&
        (i4TestValIssAclUserDefinedFilterSubAction != ISS_NESTED_VLAN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclUserDefinedFilterSubAction function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2IssAclUserDefinedFilterSubActionId
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
testValIssAclUserDefinedFilterSubActionId
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2IssAclUserDefinedFilterSubActionId (UINT4
                                             *pu4ErrorCode,
                                             UINT4
                                             u4IssAclUserDefinedFilterId,
                                             INT4
                                             i4TestValIssAclUserDefinedFilterSubActionId)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclUserDefinedFilterSubActionId function \n");
    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:UserDefined filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry != NULL &&
        pIssAclUdbFilterTableEntry->u1RowStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssAclUdbFilterTableEntry value is not NULL and row status is ACTIVE with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclUserDefinedFilterSubActionId < 0) ||
        (i4TestValIssAclUserDefinedFilterSubActionId > 4094))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:i4TestValIssAclUserDefinedFilterSubActionId value is less than zero or greater than 4094 with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclUserDefinedFilterSubActionId function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2IssAclUserDefinedFilterIdTwoType
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
testValIssAclUserDefinedFilterIdTwoType
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2IssAclUserDefinedFilterIdTwoType (UINT4
                                           *pu4ErrorCode,
                                           UINT4
                                           u4IssAclUserDefinedFilterId,
                                           INT4
                                           i4TestValIssAclUserDefinedFilterIdTwoType)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclUserDefinedFilterIdTwoType function \n");
    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:UserDefined filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->u1RowStatus == ISS_ACTIVE))
    {
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssAclUdbFilterTableEntry value is not NULL and row status is ACTIVE with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclUserDefinedFilterIdTwoType != ISS_L2_FILTER) &&
        (i4TestValIssAclUserDefinedFilterIdTwoType != ISS_L3_FILTER))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nLOW:i4TestValIssAclUserDefinedFilterIdOneType value not is not equql to value of            ISS_L2_FILTER%d ISS_L3_FILTER%d value e errorcode as %d\n",
                      ISS_L2_FILTER, ISS_L3_FILTER, *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclUserDefinedFilterIdTwoType function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2IssAclUserDefinedFilterIdTwo
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
testValIssAclUserDefinedFilterIdTwo
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2IssAclUserDefinedFilterIdTwo (UINT4
                                       *pu4ErrorCode,
                                       UINT4
                                       u4IssAclUserDefinedFilterId,
                                       UINT4
                                       u4TestValIssAclUserDefinedFilterIdTwo)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclUserDefinedFilterIdTwo function \n");
    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:UserDefined filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->u1RowStatus == ISS_ACTIVE))
    {
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssAclUdbFilterTableEntry value is not NULL and row status is ACTIVE with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if (u4TestValIssAclUserDefinedFilterIdTwo == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:u4TestValIssAclUserDefinedFilterIdTwo value is zero\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclUserDefinedFilterIdTwo function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2IssAclUserDefinedFilterStatus
Input       :  The Indices
IssAclUserDefinedFilterId

The Object 
testValIssAclUserDefinedFilterStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2IssAclUserDefinedFilterStatus (UINT4
                                        *pu4ErrorCode,
                                        UINT4
                                        u4IssAclUserDefinedFilterId,
                                        INT4
                                        i4TestValIssAclUserDefinedFilterStatus)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;
    UINT4               u4RedirectIntfGrp = 0x0;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclUserDefinedFilterStatus function \n");
    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:UserDefined filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclUserDefinedFilterStatus < ISS_ACTIVE)
        || (i4TestValIssAclUserDefinedFilterStatus > ISS_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nLOW:i4TestValIssAclUserDefinedFilterStatusvalue is less than ISS_ACTIVE%d or greater than ISS_DESTROY%d with errorcode as %d\n",
                      ISS_ACTIVE, ISS_DESTROY, *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry == NULL)
        && (ISS_UDB_FILTER_TABLE_LIST_COUNT == ISS_MAX_UDB_FILTERS))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssAclUdbFilterTableEntry value is  NULL with errorcode as %d\n",
                      *pu4ErrorCode);
        CLI_SET_ERR (CLI_ACL_FILTER_MAX);
        return SNMP_FAILURE;
    }

    if ((pIssAclUdbFilterTableEntry == NULL) &&
        ((i4TestValIssAclUserDefinedFilterStatus != ISS_CREATE_AND_WAIT)
         && (i4TestValIssAclUserDefinedFilterStatus != ISS_CREATE_AND_GO)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssAclUdbFilterTableEntry value is NULL with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        ((i4TestValIssAclUserDefinedFilterStatus == ISS_CREATE_AND_WAIT)
         || (i4TestValIssAclUserDefinedFilterStatus == ISS_CREATE_AND_GO)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssAclUdbFilterTableEntry value is not equal NULL with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->u1RowStatus != ISS_ACTIVE) &&
        (i4TestValIssAclUserDefinedFilterStatus == ISS_NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:pIssAclUdbFilterTableEntry value is not equal NULL with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (i4TestValIssAclUserDefinedFilterStatus == ISS_ACTIVE))

    {
        /* Before Making Row Status Following 
         * Things Has to Be Taken Care in Case of UDB Filter */

        if (pIssAclUdbFilterTableEntry->pAccessFilterEntry == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nLOW:pAccessFilterEntry value is NULL with errorcode as %d\n",
                          *pu4ErrorCode);
            return SNMP_FAILURE;
        }

        if (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
            IssAccessFilterAction == 0x0)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nLOW:IssAccessFilterAction value is 0x0 with errorcode as %d\n",
                          *pu4ErrorCode);
            return SNMP_FAILURE;
        }
    }

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
         IssAccessFilterAction == ISS_REDIRECT_TO) &&
        (i4TestValIssAclUserDefinedFilterStatus == ISS_DESTROY))
    {
        u4RedirectIntfGrp =
            pIssAclUdbFilterTableEntry->pAccessFilterEntry->RedirectIfGrp.
            u4RedirectGrpId;

        if (u4RedirectIntfGrp != 0x0)
        {
            if (gpIssRedirectIntfInfo[u4RedirectIntfGrp - 1].u1RowStatus != 0)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclUserDefinedFilterStatus function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssAclUserDefinedFilterStatsEnabledStatus
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object
                testValIssAclUserDefinedFilterStatsEnabledStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclUserDefinedFilterStatsEnabledStatus (UINT4 *pu4ErrorCode,
                                                    UINT4
                                                    u4IssAclUserDefinedFilterId,
                                                    INT4
                                                    i4TestValIssAclUserDefinedFilterStatsEnabledStatus)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclUserDefinedFilterStatsEnabledStatus function \n");

    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:UserDefined filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        CLI_SET_ERR (CLI_ACL_INVALID_FILTER_ID);
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclUserDefinedFilterStatsEnabledStatus != ACL_STAT_ENABLE)
        && (i4TestValIssAclUserDefinedFilterStatsEnabledStatus !=
            ACL_STAT_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:UserDefined filter hardware stats enables status is invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        CLI_SET_ERR (CLI_ACL_FILTER_DATA_INSUFFICIENCY);
        return SNMP_FAILURE;
    }

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclUserDefinedFilterStatsEnabledStatus function \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssAclClearUserDefinedFilterStats
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object
                testValIssAclClearUserDefinedFilterStats
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclClearUserDefinedFilterStats (UINT4 *pu4ErrorCode,
                                            UINT4 u4IssAclUserDefinedFilterId,
                                            INT4
                                            i4TestValIssAclClearUserDefinedFilterStats)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssAclClearUserDefinedFilterStats function \n");

    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:UserDefined filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        CLI_SET_ERR (CLI_ACL_INVALID_FILTER_ID);
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclClearUserDefinedFilterStats != ISS_TRUE) &&
        (i4TestValIssAclClearUserDefinedFilterStats != ISS_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:UserDefined filter hardware stats clear status is invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        CLI_SET_ERR (CLI_ACL_FILTER_DATA_INSUFFICIENCY);
        return SNMP_FAILURE;
    }

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssAclClearUserDefinedFilterStats function \n");
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2IssAclUserDefinedFilterTable
Input       :  The Indices
IssAclUserDefinedFilterId
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2IssAclUserDefinedFilterTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IssRedirectInterfaceGrpTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceIssRedirectInterfaceGrpTable
Input       :  The Indices
IssRedirectInterfaceGrpId
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssRedirectInterfaceGrpTable (UINT4
                                                      u4IssRedirectInterfaceGrpId)
{

    if (ISS_IS_REDIRECT_GRP_ID_VALID (u4IssRedirectInterfaceGrpId) != ISS_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexIssRedirectInterfaceGrpTable
Input       :  The Indices
IssRedirectInterfaceGrpId
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssRedirectInterfaceGrpTable (UINT4
                                              *pu4IssRedirectInterfaceGrpId)
{
    UINT4               u4FirstIndex = 0x0;

    if (nmhGetNextIndexIssRedirectInterfaceGrpTable (0, &u4FirstIndex) ==
        SNMP_FAILURE)
    {

        return SNMP_FAILURE;

    }

    *pu4IssRedirectInterfaceGrpId = u4FirstIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetNextIndexIssRedirectInterfaceGrpTable
Input       :  The Indices
IssRedirectInterfaceGrpId
nextIssRedirectInterfaceGrpId
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIssRedirectInterfaceGrpTable (UINT4 u4IssRedirectInterfaceGrpId,
                                             UINT4
                                             *pu4NextIssRedirectInterfaceGrpId)
{
    UINT4               u4ControlIndex = 0x0;

    if ((u4IssRedirectInterfaceGrpId == ISS_MAX_REDIRECT_GRP_ID))
    {
        return SNMP_FAILURE;
    }
    for (u4ControlIndex = u4IssRedirectInterfaceGrpId + 1;
         u4ControlIndex <= ISS_MAX_REDIRECT_GRP_ID; u4ControlIndex++)
    {
        if (gpIssRedirectIntfInfo[u4ControlIndex - 1].u1RowStatus == 0)
        {
            continue;
        }
        else
        {
            *pu4NextIssRedirectInterfaceGrpId = u4ControlIndex;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetIssRedirectInterfaceGrpFilterType
Input       :  The Indices
IssRedirectInterfaceGrpId

The Object 
retValIssRedirectInterfaceGrpFilterType
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetIssRedirectInterfaceGrpFilterType (UINT4 u4IssRedirectInterfaceGrpId,
                                         INT4
                                         *pi4RetValIssRedirectInterfaceGrpFilterType)
{
    *pi4RetValIssRedirectInterfaceGrpFilterType =
        gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1AclIdType;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetIssRedirectInterfaceGrpFilterId
Input       :  The Indices
IssRedirectInterfaceGrpId

The Object 
retValIssRedirectInterfaceGrpFilterId
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetIssRedirectInterfaceGrpFilterId (UINT4 u4IssRedirectInterfaceGrpId,
                                       UINT4
                                       *pu4RetValIssRedirectInterfaceGrpFilterId)
{
    *pu4RetValIssRedirectInterfaceGrpFilterId =
        gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u4AclId;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetIssRedirectInterfaceGrpDistByte
Input       :  The Indices
IssRedirectInterfaceGrpId

The Object 
retValIssRedirectInterfaceGrpDistByte
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetIssRedirectInterfaceGrpDistByte (UINT4 u4IssRedirectInterfaceGrpId,
                                       INT4
                                       *pi4RetValIssRedirectInterfaceGrpDistByte)
{
    UNUSED_PARAM (u4IssRedirectInterfaceGrpId);
    UNUSED_PARAM (pi4RetValIssRedirectInterfaceGrpDistByte);
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetIssRedirectInterfaceGrpPortList
Input       :  The Indices
IssRedirectInterfaceGrpId

The Object 
retValIssRedirectInterfaceGrpPortList
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetIssRedirectInterfaceGrpPortList (UINT4 u4IssRedirectInterfaceGrpId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pRetValIssRedirectInterfaceGrpPortList)
{
    MEMCPY (pRetValIssRedirectInterfaceGrpPortList->pu1_OctetList,
            gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].PortList,
            sizeof (tPortList));

    pRetValIssRedirectInterfaceGrpPortList->i4_Length = sizeof (tPortList);

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetIssRedirectInterfaceGrpType
Input       :  The Indices
IssRedirectInterfaceGrpId

The Object 
retValIssRedirectInterfaceGrpType
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetIssRedirectInterfaceGrpType (UINT4 u4IssRedirectInterfaceGrpId,
                                   INT4 *pi4RetValIssRedirectInterfaceGrpType)
{
    *pi4RetValIssRedirectInterfaceGrpType =
        gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1PortListType;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetIssRedirectInterfaceGrpUdbPosition
Input       :  The Indices
IssRedirectInterfaceGrpId

The Object 
retValIssRedirectInterfaceGrpUdbPosition
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetIssRedirectInterfaceGrpUdbPosition (UINT4 u4IssRedirectInterfaceGrpId,
                                          INT4
                                          *pi4RetValIssRedirectInterfaceGrpUdbPosition)
{
    UNUSED_PARAM (u4IssRedirectInterfaceGrpId);
    UNUSED_PARAM (pi4RetValIssRedirectInterfaceGrpUdbPosition);
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetIssRedirectInterfaceGrpStatus
Input       :  The Indices
IssRedirectInterfaceGrpId

The Object 
retValIssRedirectInterfaceGrpStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetIssRedirectInterfaceGrpStatus (UINT4 u4IssRedirectInterfaceGrpId,
                                     INT4
                                     *pi4RetValIssRedirectInterfaceGrpStatus)
{
    *pi4RetValIssRedirectInterfaceGrpStatus =
        gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1RowStatus;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetIssRedirectInterfaceGrpFilterType
Input       :  The Indices
IssRedirectInterfaceGrpId

The Object 
setValIssRedirectInterfaceGrpFilterType
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetIssRedirectInterfaceGrpFilterType (UINT4 u4IssRedirectInterfaceGrpId,
                                         INT4
                                         i4SetValIssRedirectInterfaceGrpFilterType)
{

    gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1AclIdType
        = (UINT1) i4SetValIssRedirectInterfaceGrpFilterType;

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetIssRedirectInterfaceGrpFilterId
Input       :  The Indices
IssRedirectInterfaceGrpId

The Object 
setValIssRedirectInterfaceGrpFilterId
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetIssRedirectInterfaceGrpFilterId (UINT4 u4IssRedirectInterfaceGrpId,
                                       UINT4
                                       u4SetValIssRedirectInterfaceGrpFilterId)
{
    gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u4AclId
        = u4SetValIssRedirectInterfaceGrpFilterId;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetIssRedirectInterfaceGrpDistByte
Input       :  The Indices
IssRedirectInterfaceGrpId

The Object 
setValIssRedirectInterfaceGrpDistByte
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetIssRedirectInterfaceGrpDistByte (UINT4 u4IssRedirectInterfaceGrpId,
                                       INT4
                                       i4SetValIssRedirectInterfaceGrpDistByte)
{
    UNUSED_PARAM (u4IssRedirectInterfaceGrpId);
    UNUSED_PARAM (i4SetValIssRedirectInterfaceGrpDistByte);
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetIssRedirectInterfaceGrpPortList
Input       :  The Indices
IssRedirectInterfaceGrpId

The Object 
setValIssRedirectInterfaceGrpPortList
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetIssRedirectInterfaceGrpPortList (UINT4 u4IssRedirectInterfaceGrpId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pSetValIssRedirectInterfaceGrpPortList)
{

    ISS_MEMCPY (gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].
                PortList, pSetValIssRedirectInterfaceGrpPortList->pu1_OctetList,
                pSetValIssRedirectInterfaceGrpPortList->i4_Length);

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetIssRedirectInterfaceGrpType
Input       :  The Indices
IssRedirectInterfaceGrpId

The Object 
setValIssRedirectInterfaceGrpType
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetIssRedirectInterfaceGrpType (UINT4 u4IssRedirectInterfaceGrpId,
                                   INT4 i4SetValIssRedirectInterfaceGrpType)
{

    gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1PortListType
        = (UINT1) i4SetValIssRedirectInterfaceGrpType;

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetIssRedirectInterfaceGrpUdbPosition
Input       :  The Indices
IssRedirectInterfaceGrpId

The Object 
setValIssRedirectInterfaceGrpUdbPosition
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetIssRedirectInterfaceGrpUdbPosition (UINT4 u4IssRedirectInterfaceGrpId,
                                          INT4
                                          i4SetValIssRedirectInterfaceGrpUdbPosition)
{
    UNUSED_PARAM (u4IssRedirectInterfaceGrpId);
    UNUSED_PARAM (i4SetValIssRedirectInterfaceGrpUdbPosition);
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetIssRedirectInterfaceGrpStatus
Input       :  The Indices
IssRedirectInterfaceGrpId

The Object 
setValIssRedirectInterfaceGrpStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetIssRedirectInterfaceGrpStatus (UINT4 u4IssRedirectInterfaceGrpId,
                                     INT4 i4SetValIssRedirectInterfaceGrpStatus)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    UINT4               au4PortList[ISS_REDIRECT_MAX_PORTS];
    UINT4               u4AclId = 0x0;
    UINT4               u4NumPorts = 0x0;
    UINT1               u1AclIdType = 0x0;
    UINT1               u1CurrRowStatus = 0;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif
    ISS_MEMSET (au4PortList, 0, sizeof (au4PortList));

    u1CurrRowStatus =
        gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1RowStatus;

    if (u1CurrRowStatus == i4SetValIssRedirectInterfaceGrpStatus)
    {
        return SNMP_SUCCESS;
    }

    u4AclId = gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u4AclId;

    u1AclIdType = gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].
        u1AclIdType;
    switch (i4SetValIssRedirectInterfaceGrpStatus)
    {
        case ISS_CREATE_AND_WAIT:
        case ISS_CREATE_AND_GO:

            ISS_MEMSET (&gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1],
                        0, sizeof (tIssRedirectIntfGrpTable));

            gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].
                u4RedirectIntfGrpId = u4IssRedirectInterfaceGrpId;

            gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1RowStatus
                = ISS_NOT_READY;
            break;

        case ISS_NOT_IN_SERVICE:

            /* Delete only the Data Plane Information */
            switch (u1AclIdType)
            {

                case ISS_L2_REDIRECT:

                    pIssL2FilterEntry = IssExtGetL2FilterEntry (u4AclId);
                    if (NULL == pIssL2FilterEntry)
                    {
                        ISS_TRC (ALL_FAILURE_TRC,
                                 "\nLOW:L2 filter entry value is NULL \n");
                        return SNMP_FAILURE;
                    }
#ifdef NPAPI_WANTED
                    if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                    {
                        /* If counter is enabled then set flag to detach
                         * and reattach counter after row status becomes ACTIVE */
                        if (pIssL2FilterEntry->i4IssL2FilterStatsEnabledStatus
                            == ACL_STAT_ENABLE)
                        {
                            i4RetVal =
                                IsssysIssHwUpdateL2Filter (pIssL2FilterEntry,
                                                           ISS_L2FILTER_STAT_DISABLE);

                            if (i4RetVal != FNP_SUCCESS)
                            {
                                ISS_TRC (ALL_FAILURE_TRC,
                                         "\nLOW:L2 filter cannot disable hardware stats \n");
                                CLI_SET_ERR (CLI_ACL_HW_UPDATE_FAILED);
                                return SNMP_FAILURE;
                            }
                        }
                        i4RetVal =
                            IsssysIssHwUpdateL2Filter (pIssL2FilterEntry,
                                                       ISS_L2FILTER_DELETE);
                        if (i4RetVal != FNP_SUCCESS)
                        {
                            return SNMP_FAILURE;
                        }
                    }
#endif

                    break;
                case ISS_L3_REDIRECT:
                    pIssL3FilterEntry = IssExtGetL3FilterEntry (u4AclId);
                    if (NULL == pIssL3FilterEntry)
                    {
                        ISS_TRC (ALL_FAILURE_TRC,
                                 "\nLOW:L3 Filter Entry value is NULL\n");
                        return SNMP_FAILURE;
                    }

#ifdef NPAPI_WANTED
                    if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                    {
                        /* If counter is enabled then set flag to detach
                         * and reattach counter after row status becomes ACTIVE */
                        if (pIssL3FilterEntry->i4IssL3FilterStatsEnabledStatus
                            == ACL_STAT_ENABLE)
                        {
                            i4RetVal =
                                IsssysIssHwUpdateL3Filter (pIssL3FilterEntry,
                                                           ISS_L3FILTER_STAT_DISABLE);

                            if (i4RetVal != FNP_SUCCESS)
                            {
                                ISS_TRC (ALL_FAILURE_TRC,
                                         "\nLOW:L3 filter cannot disable hardware stats \n");
                                CLI_SET_ERR (CLI_ACL_HW_UPDATE_FAILED);
                                return SNMP_FAILURE;
                            }
                        }
                        i4RetVal =
                            IsssysIssHwUpdateL3Filter (pIssL3FilterEntry,
                                                       ISS_L3FILTER_DELETE);
                        if (i4RetVal != FNP_SUCCESS)
                        {
                            return SNMP_FAILURE;
                        }
                    }
#endif
                    break;
            }

            gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1RowStatus
                = ISS_NOT_IN_SERVICE;

            break;
        case ISS_ACTIVE:

            if (u4AclId != '\0')
            {
                /* PortList it self Contains Port Number
                 * So Update the Array with Port IfIndex */
                ISS_MEMCPY
                    (&au4PortList[u4NumPorts],
                     gpIssRedirectIntfInfo
                     [u4IssRedirectInterfaceGrpId - 1].PortList,
                     sizeof (UINT4));
                u4NumPorts = 1;

                switch (u1AclIdType)
                {

                    case ISS_L2_REDIRECT:

                        pIssL2FilterEntry = IssExtGetL2FilterEntry (u4AclId);
                        if (NULL == pIssL2FilterEntry)
                        {
                            ISS_TRC (ALL_FAILURE_TRC,
                                     "\nLOW:L2 filter entry value is NULL \n");
                            return SNMP_FAILURE;
                        }

                        /* For the Redirect Filter Installation
                         *  of Port / Trunk fill the parameters
                         *  of (Interface Type and Interface IfIndex) */

                        pIssL2FilterEntry->RedirectIfGrp.
                            u4EgressIfIndex = au4PortList[u4NumPorts - 1];

                        /* Currently MPLS Tunnel Redirection is Not
                         * being done . Only Ethernet Port /Trunk
                         *  is being taken care of */

                        if (pIssL2FilterEntry->RedirectIfGrp.
                            u4EgressIfIndex <= SYS_DEF_MAX_PHYSICAL_INTERFACES)
                        {
                            pIssL2FilterEntry->RedirectIfGrp.
                                u1EgressIfType = ISS_REDIRECT_TO_ETHERNET;

                        }
                        else
                        {
                            pIssL2FilterEntry->RedirectIfGrp.
                                u1EgressIfType = ISS_REDIRECT_TO_TRUNK;
                        }

                        pIssL2FilterEntry->RedirectIfGrp.
                            u4RedirectGrpId = u4IssRedirectInterfaceGrpId;

#ifdef NPAPI_WANTED
                        if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                        {
                            i4RetVal =
                                IsssysIssHwUpdateL2Filter
                                (pIssL2FilterEntry, ISS_L2FILTER_ADD);

                            if (i4RetVal != FNP_SUCCESS)
                            {
                                if (i4RetVal == FNP_NOT_SUPPORTED)
                                {
                                    CLI_SET_ERR (CLI_ACL_OUT_MULTIPLE_PORTS);
                                }

                                return SNMP_FAILURE;
                            }
                            /* Enable counter if required */
                            if (pIssL2FilterEntry->
                                i4IssL2FilterStatsEnabledStatus ==
                                ACL_STAT_ENABLE)
                            {
                                i4RetVal =
                                    IsssysIssHwUpdateL2Filter
                                    (pIssL2FilterEntry,
                                     ISS_L2FILTER_STAT_ENABLE);

                                if (i4RetVal != FNP_SUCCESS)
                                {
                                    ISS_TRC (ALL_FAILURE_TRC,
                                             "\nLOW:L2 filter cannot enable hardware stats \n");
                                    CLI_SET_ERR (CLI_ACL_HW_UPDATE_FAILED);
                                    return SNMP_FAILURE;
                                }
                            }
                        }
#endif

                        /* Fill the Redirect Group Id also
                         * as it will be used in Kepping Tack
                         * of Same Filter is mapped to this
                         * Redirect Group . USeful from CLI
                         * Perpective. From SNMP Perspective
                         * Manager needs to take of ACL-
                         * Redirect Group Mapping */
                        gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].
                            u1RowStatus = ISS_ACTIVE;
                        break;
                    case ISS_L3_REDIRECT:

                        pIssL3FilterEntry = IssExtGetL3FilterEntry (u4AclId);
                        if (NULL == pIssL3FilterEntry)
                        {
                            ISS_TRC (ALL_FAILURE_TRC,
                                     "\nLOW:L3 Filter Entry value is NULL\n");
                            return SNMP_FAILURE;
                        }

                        /* For the Redirect Filter Installation
                         *  of Port / Trunk fill the parameters
                         *  of (Interface Type and Interface IfIndex) */

                        pIssL3FilterEntry->RedirectIfGrp.
                            u4EgressIfIndex = au4PortList[u4NumPorts - 1];

                        /* Currently MPLS Tunnel Redirection is Not
                         * being done . Only Ethernet Port /Trunk
                         *  is being taken care of */

                        if (pIssL3FilterEntry->RedirectIfGrp.
                            u4EgressIfIndex <= SYS_DEF_MAX_PHYSICAL_INTERFACES)
                        {
                            pIssL3FilterEntry->RedirectIfGrp.
                                u1EgressIfType = ISS_REDIRECT_TO_ETHERNET;

                        }
                        else
                        {
                            pIssL3FilterEntry->RedirectIfGrp.
                                u1EgressIfType = ISS_REDIRECT_TO_TRUNK;
                        }

                        pIssL3FilterEntry->RedirectIfGrp.
                            u4RedirectGrpId = u4IssRedirectInterfaceGrpId;

#ifdef NPAPI_WANTED
                        if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                        {

                            i4RetVal = IsssysIssHwUpdateL3Filter
                                (pIssL3FilterEntry, ISS_L3FILTER_ADD);
                            if (i4RetVal != FNP_SUCCESS)
                            {
                                if (i4RetVal == FNP_NOT_SUPPORTED)
                                {
                                    CLI_SET_ERR (CLI_ACL_OUT_MULTIPLE_PORTS);
                                }
                                return SNMP_FAILURE;
                            }
                            /* Enable counter if required */
                            if (pIssL3FilterEntry->
                                i4IssL3FilterStatsEnabledStatus ==
                                ACL_STAT_ENABLE)
                            {
                                i4RetVal =
                                    IsssysIssHwUpdateL3Filter
                                    (pIssL3FilterEntry,
                                     ISS_L3FILTER_STAT_ENABLE);

                                if (i4RetVal != FNP_SUCCESS)
                                {
                                    ISS_TRC (ALL_FAILURE_TRC,
                                             "\nLOW:L3 filter cannot enable hardware stats \n");
                                    CLI_SET_ERR (CLI_ACL_HW_UPDATE_FAILED);
                                    return SNMP_FAILURE;
                                }
                            }
                        }
#endif

                        /* Fill the Redirect Group Id also
                         * as it will be used in Kepping Tack
                         * of Same Filter is mapped to this
                         * Redirect Group . USeful from CLI
                         * Perpective. From SNMP Perspective
                         * Manager needs to take of ACL-
                         * Redirect Group Mapping */

                        gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].
                            u1RowStatus = ISS_ACTIVE;
                        break;
                    default:
                        return SNMP_FAILURE;
                }
            }
            break;
        case ISS_DESTROY:

            if ((u1AclIdType == ISS_L2_REDIRECT) &&
                (u1CurrRowStatus == ISS_ACTIVE))
            {
                pIssL2FilterEntry = IssExtGetL2FilterEntry (u4AclId);
                if (pIssL2FilterEntry != NULL)
                {
                    pIssL2FilterEntry->RedirectIfGrp.u4RedirectGrpId = 0;
                }
                else
                {
                    ISS_TRC (ALL_FAILURE_TRC,
                             "\nLOW:L2 filter entry value is NULL \n");
                    return SNMP_FAILURE;
                }

#ifdef NPAPI_WANTED
                if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                {
                    if (pIssL2FilterEntry->i4IssL2FilterStatsEnabledStatus
                        == ACL_STAT_ENABLE)
                    {
                        pIssL2FilterEntry->i4IssL2FilterStatsEnabledStatus
                            = ACL_STAT_DISABLE;
                        i4RetVal =
                            IsssysIssHwUpdateL2Filter (pIssL2FilterEntry,
                                                       ISS_L2FILTER_STAT_DISABLE);

                        if (i4RetVal != FNP_SUCCESS)
                        {
                            ISS_TRC (ALL_FAILURE_TRC,
                                     "\nLOW:L2 filter cannot disable hardware stats \n");
                        }
                    }
                    i4RetVal =
                        IsssysIssHwUpdateL2Filter (pIssL2FilterEntry,
                                                   ISS_L2FILTER_DELETE);
                    if (i4RetVal != FNP_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                }
#endif

            }
            else if ((u1AclIdType == ISS_L3_REDIRECT) &&
                     (u1CurrRowStatus == ISS_ACTIVE))
            {
                pIssL3FilterEntry = IssExtGetL3FilterEntry (u4AclId);

                if (pIssL3FilterEntry != NULL)
                {
                    pIssL3FilterEntry->RedirectIfGrp.u4RedirectGrpId = 0;
                }
                else
                {
                    ISS_TRC (ALL_FAILURE_TRC,
                             "\nLOW:L3 Filter Entry value is NULL\n");
                    return SNMP_FAILURE;
                }
#ifdef NPAPI_WANTED

                if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                {
                    if (pIssL3FilterEntry->i4IssL3FilterStatsEnabledStatus
                        == ACL_STAT_ENABLE)
                    {
                        pIssL3FilterEntry->i4IssL3FilterStatsEnabledStatus
                            = ACL_STAT_DISABLE;
                        i4RetVal =
                            IsssysIssHwUpdateL3Filter (pIssL3FilterEntry,
                                                       ISS_L3FILTER_STAT_DISABLE);

                        if (i4RetVal != FNP_SUCCESS)
                        {
                            ISS_TRC (ALL_FAILURE_TRC,
                                     "\nLOW:L3 filter cannot disable hardware stats \n");
                        }
                    }
                    i4RetVal =
                        IsssysIssHwUpdateL3Filter (pIssL3FilterEntry,
                                                   ISS_L3FILTER_DELETE);
                    if (i4RetVal != FNP_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                }
#endif
            }

            ISS_MEMSET (&gpIssRedirectIntfInfo
                        [u4IssRedirectInterfaceGrpId - 1], 0,
                        sizeof (tIssRedirectIntfGrpTable));
            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2IssRedirectInterfaceGrpFilterType
Input       :  The Indices
IssRedirectInterfaceGrpId

The Object 
testValIssRedirectInterfaceGrpFilterType
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2IssRedirectInterfaceGrpFilterType (UINT4 *pu4ErrorCode,
                                            UINT4 u4IssRedirectInterfaceGrpId,
                                            INT4
                                            i4TestValIssRedirectInterfaceGrpFilterType)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssRedirectInterfaceGrpFilterType function \n");
    if (ISS_IS_REDIRECT_GRP_ID_VALID (u4IssRedirectInterfaceGrpId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:RedirectInterfaceGrp filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1RowStatus
         == ISS_ACTIVE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssRedirectInterfaceGrpFilterType != ISS_L2_REDIRECT) &&
        (i4TestValIssRedirectInterfaceGrpFilterType != ISS_L3_REDIRECT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssRedirectInterfaceGrpFilterType function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2IssRedirectInterfaceGrpFilterId
Input       :  The Indices
IssRedirectInterfaceGrpId

The Object 
testValIssRedirectInterfaceGrpFilterId
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2IssRedirectInterfaceGrpFilterId (UINT4 *pu4ErrorCode,
                                          UINT4 u4IssRedirectInterfaceGrpId,
                                          UINT4
                                          u4TestValIssRedirectInterfaceGrpFilterId)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssRedirectInterfaceGrpFilterId function \n");
    if (ISS_IS_REDIRECT_GRP_ID_VALID (u4IssRedirectInterfaceGrpId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:RedirectInterfaceGrp filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if (gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1RowStatus
        == ISS_ACTIVE)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4TestValIssRedirectInterfaceGrpFilterId == 0)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:u4TestValIssRedirectInterfaceGrpFilterId value is zero with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssRedirectInterfaceGrpFilterId function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2IssRedirectInterfaceGrpDistByte
Input       :  The Indices
IssRedirectInterfaceGrpId

The Object 
testValIssRedirectInterfaceGrpDistByte
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2IssRedirectInterfaceGrpDistByte (UINT4 *pu4ErrorCode,
                                          UINT4 u4IssRedirectInterfaceGrpId,
                                          INT4
                                          i4TestValIssRedirectInterfaceGrpDistByte)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4IssRedirectInterfaceGrpId);
    UNUSED_PARAM (i4TestValIssRedirectInterfaceGrpDistByte);
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2IssRedirectInterfaceGrpPortList
Input       :  The Indices
IssRedirectInterfaceGrpId

The Object 
testValIssRedirectInterfaceGrpPortList
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2IssRedirectInterfaceGrpPortList (UINT4 *pu4ErrorCode,
                                          UINT4 u4IssRedirectInterfaceGrpId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pTestValIssRedirectInterfaceGrpPortList)
{
    pTestValIssRedirectInterfaceGrpPortList = NULL;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssRedirectInterfaceGrpPortList function \n");
    if (ISS_IS_REDIRECT_GRP_ID_VALID (u4IssRedirectInterfaceGrpId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:RedirectInterfaceGrp filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1RowStatus
         == ISS_ACTIVE))

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Currently Port or Trunk will be be taking care as Port List
     *  is availbalr for it */

    /* This function checks if any port greater than the maximum number of
     * ports in the system is in the port list.*/

    UNUSED_PARAM (pTestValIssRedirectInterfaceGrpPortList);
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssRedirectInterfaceGrpPortList function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2IssRedirectInterfaceGrpType
Input       :  The Indices
IssRedirectInterfaceGrpId

The Object 
testValIssRedirectInterfaceGrpType
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2IssRedirectInterfaceGrpType (UINT4 *pu4ErrorCode,
                                      UINT4 u4IssRedirectInterfaceGrpId,
                                      INT4 i4TestValIssRedirectInterfaceGrpType)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssRedirectInterfaceGrpType function \n");
    if (ISS_IS_REDIRECT_GRP_ID_VALID (u4IssRedirectInterfaceGrpId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:RedirectInterfaceGrp filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1RowStatus
         == ISS_ACTIVE))

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssRedirectInterfaceGrpType != ISS_REDIRECT_TO_PORT) &&
        (i4TestValIssRedirectInterfaceGrpType != ISS_REDIRECT_TO_PORTLIST))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;

    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssRedirectInterfaceGrpType function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2IssRedirectInterfaceGrpUdbPosition
Input       :  The Indices
IssRedirectInterfaceGrpId

The Object 
testValIssRedirectInterfaceGrpUdbPosition
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2IssRedirectInterfaceGrpUdbPosition (UINT4 *pu4ErrorCode,
                                             UINT4 u4IssRedirectInterfaceGrpId,
                                             INT4
                                             i4TestValIssRedirectInterfaceGrpUdbPosition)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4IssRedirectInterfaceGrpId);
    UNUSED_PARAM (i4TestValIssRedirectInterfaceGrpUdbPosition);
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2IssRedirectInterfaceGrpStatus
Input       :  The Indices
IssRedirectInterfaceGrpId

The Object 
testValIssRedirectInterfaceGrpStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2IssRedirectInterfaceGrpStatus (UINT4 *pu4ErrorCode,
                                        UINT4 u4IssRedirectInterfaceGrpId,
                                        INT4
                                        i4TestValIssRedirectInterfaceGrpStatus)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Entry nmhTestv2IssRedirectInterfaceGrpStatus function \n");
    if (ISS_IS_REDIRECT_GRP_ID_VALID (u4IssRedirectInterfaceGrpId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:RedirectInterfaceGrp filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1RowStatus
         != ISS_ACTIVE) &&
        (i4TestValIssRedirectInterfaceGrpStatus == ISS_NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].
         u1RowStatus == 0x0) &&
        ((i4TestValIssRedirectInterfaceGrpStatus != ISS_CREATE_AND_WAIT)
         && (i4TestValIssRedirectInterfaceGrpStatus != ISS_CREATE_AND_GO)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (i4TestValIssRedirectInterfaceGrpStatus == ISS_ACTIVE)
    {
        if (gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].
            u4AclId == 0x0)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssRedirectInterfaceGrpStatus function \n");
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2IssRedirectInterfaceGrpTable
Input       :  The Indices
IssRedirectInterfaceGrpId
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2IssRedirectInterfaceGrpTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIssRedirectInterfaceGrpIdNextFree
 Input       :  The Indices

                The Object
                retValIssRedirectInterfaceGrpIdNextFree
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssRedirectInterfaceGrpIdNextFree (UINT4
                                         *pu4RetValIssRedirectInterfaceGrpIdNextFree)
{
    IssInterfaceGrpRedirectIdNextFree
        (pu4RetValIssRedirectInterfaceGrpIdNextFree);
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : IssReservedFrameCtrlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssReservedFrameCtrlTable
 Input       :  The Indices
                IssReservedFrameCtrlId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssReservedFrameCtrlTable (UINT4
                                                   u4IssReservedFrameCtrlId)
{
    ISS_TRC_FN_ENTRY ();
    if (ISS_IS_RESERV_FRM_CTRL_ID_VALID (u4IssReservedFrameCtrlId) != ISS_TRUE)
    {
        ISS_TRC (ISS_TRC_ALL,
                 "\n nmhValidateIndexInstanceIssReservedFrameCtrlTable returned Failure");
        ISS_TRC_FN_EXIT ();

        return SNMP_FAILURE;
    }
    ISS_TRC (ISS_TRC_ALL,
             "\n nmhValidateIndexInstanceIssReservedFrameCtrlTable returned Success");
    ISS_TRC_FN_EXIT ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssReservedFrameCtrlTable
 Input       :  The Indices
                IssReservedFrameCtrlId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssReservedFrameCtrlTable (UINT4 *pu4IssReservedFrameCtrlId)
{
    UINT4               u4FirstIndex = 0x0;

    ISS_TRC_FN_ENTRY ();
    if (nmhGetNextIndexIssReservedFrameCtrlTable (0, &u4FirstIndex) ==
        SNMP_FAILURE)
    {
        ISS_TRC (ISS_TRC_ALL,
                 "\n nmhGetFirstIndexIssReservedFrameCtrlTable returned Failure");
        ISS_TRC_FN_EXIT ();
        return SNMP_FAILURE;

    }

    *pu4IssReservedFrameCtrlId = u4FirstIndex;
    ISS_TRC (ISS_TRC_ALL,
             "\n nmhGetFirstIndexIssReservedFrameCtrlTable returned Success");
    ISS_TRC_FN_EXIT ();

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIssReservedFrameCtrlTable
 Input       :  The Indices
                IssReservedFrameCtrlId
                nextIssReservedFrameCtrlId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIssReservedFrameCtrlTable (UINT4 u4IssReservedFrameCtrlId,
                                          UINT4 *pu4NextIssReservedFrameCtrlId)
{
    UINT4               u4ControlIndex = 0;

    ISS_TRC_FN_ENTRY ();
    if (u4IssReservedFrameCtrlId >= ISS_MAX_RESERV_FRM_CTRL_ID)
    {
        ISS_TRC (ISS_TRC_ALL,
                 "\n nmhGetNextIndexIssReservedFrameCtrlTable returned Failure");
        ISS_TRC_FN_EXIT ();
        return SNMP_FAILURE;
    }

    /*Traversing to the next index */
    for (u4ControlIndex = u4IssReservedFrameCtrlId + 1;
         u4ControlIndex <= ISS_MAX_RESERV_FRM_CTRL_ID; u4ControlIndex++)
    {
        if (GET_RESERVE_FRAME_CTRL_INFO (u4ControlIndex) != NULL)
        {
            *pu4NextIssReservedFrameCtrlId = u4ControlIndex;
            ISS_TRC (ISS_TRC_ALL,
                     "\n nmhGetNextIndexIssReservedFrameCtrlTable returned Success");
            ISS_TRC_FN_EXIT ();

            return SNMP_SUCCESS;
        }
    }
    ISS_TRC (ISS_TRC_ALL,
             "\n nmhGetNextIndexIssReservedFrameCtrlTable returned Failure");
    ISS_TRC_FN_EXIT ();

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIssReservedFrameCtrlPktType
 Input       :  The Indices
                IssReservedFrameCtrlId

                The Object
                retValIssReservedFrameCtrlPktType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssReservedFrameCtrlPktType (UINT4 u4IssReservedFrameCtrlId,
                                   INT4 *pi4RetValIssReservedFrameCtrlPktType)
{
    tIssReservFrmCtrlTable *pIssReservFrmCtrlInfo = NULL;
    ISS_TRC_FN_ENTRY ();

    pIssReservFrmCtrlInfo =
        GET_RESERVE_FRAME_CTRL_INFO (u4IssReservedFrameCtrlId);

    if (pIssReservFrmCtrlInfo != NULL)
    {
        *pi4RetValIssReservedFrameCtrlPktType =
            pIssReservFrmCtrlInfo->u4ControlPktType;
        ISS_TRC (ISS_TRC_ALL,
                 "\n nmhGetIssReservedFrameCtrlPktType returned Success");
        ISS_TRC_FN_EXIT ();

        return SNMP_SUCCESS;
    }
    ISS_TRC (ISS_TRC_ALL,
             "\n nmhGetIssReservedFrameCtrlPktType returned Failure");
    ISS_TRC_FN_EXIT ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssReservedFrameCtrlAction
 Input       :  The Indices
                IssReservedFrameCtrlId

                The Object
                retValIssReservedFrameCtrlAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssReservedFrameCtrlAction (UINT4 u4IssReservedFrameCtrlId,
                                  INT4 *pi4RetValIssReservedFrameCtrlAction)
{
    tIssReservFrmCtrlTable *pIssReservFrmCtrlInfo = NULL;

    ISS_TRC_FN_ENTRY ();

    pIssReservFrmCtrlInfo =
        GET_RESERVE_FRAME_CTRL_INFO (u4IssReservedFrameCtrlId);

    if (pIssReservFrmCtrlInfo != NULL)
    {
        *pi4RetValIssReservedFrameCtrlAction =
            pIssReservFrmCtrlInfo->u1ControlAction;
        ISS_TRC (ISS_TRC_ALL,
                 "\n nmhGetIssReservedFrameCtrlAction returned Success");
        ISS_TRC_FN_EXIT ();

        return SNMP_SUCCESS;
    }
    ISS_TRC (ISS_TRC_ALL,
             "\n nmhGetIssReservedFrameCtrlPktType returned Failure");
    ISS_TRC_FN_EXIT ();

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssReservedFrameCtrlOtherMacAddr
 Input       :  The Indices
                IssReservedFrameCtrlId

                The Object
                retValIssReservedFrameCtrlOtherMacAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssReservedFrameCtrlOtherMacAddr (UINT4 u4IssReservedFrameCtrlId,
                                        tMacAddr *
                                        pRetValIssReservedFrameCtrlOtherMacAddr)
{
    tIssReservFrmCtrlTable *pIssReservFrmCtrlInfo = NULL;
    tMacAddr            ZeroMac;

    ISS_TRC_FN_ENTRY ();
    MEMSET (ZeroMac, 0, sizeof (tMacAddr));

    pIssReservFrmCtrlInfo =
        GET_RESERVE_FRAME_CTRL_INFO (u4IssReservedFrameCtrlId);

    if (pIssReservFrmCtrlInfo != NULL)
    {
        if (MEMCMP (pIssReservFrmCtrlInfo->MacAddr,
                    ZeroMac, sizeof (tMacAddr)) != 0)
        {
            MEMCPY (pRetValIssReservedFrameCtrlOtherMacAddr,
                    pIssReservFrmCtrlInfo->MacAddr, sizeof (tMacAddr));
            ISS_TRC (ISS_TRC_ALL,
                     "\n nmhGetIssReservedFrameCtrlOtherMacAddr returned Success");
            ISS_TRC_FN_EXIT ();

            return SNMP_SUCCESS;
        }
    }
    ISS_TRC (ISS_TRC_ALL,
             "\n nmhGetIssReservedFrameCtrlOtherMacAddr returned Failure");
    ISS_TRC_FN_EXIT ();
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    : nmhGetIssReservedFrameCtrlOtherMacMask
 Input       :  The Indices
                IssReservedFrameCtrlId

                The Object
                retValIssReservedFrameCtrlOtherMacMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssReservedFrameCtrlOtherMacMask (UINT4 u4IssReservedFrameCtrlId,
                                        INT4
                                        *pi4RetValIssReservedFrameCtrlOtherMacMask)
{
    tIssReservFrmCtrlTable *pIssReservFrmCtrlInfo = NULL;

    ISS_TRC_FN_ENTRY ();

    pIssReservFrmCtrlInfo =
        GET_RESERVE_FRAME_CTRL_INFO (u4IssReservedFrameCtrlId);

    if (pIssReservFrmCtrlInfo != NULL)
    {
        if (pIssReservFrmCtrlInfo->u4MacMask != 0)
        {
            *pi4RetValIssReservedFrameCtrlOtherMacMask =
                pIssReservFrmCtrlInfo->u4MacMask;
            ISS_TRC (ISS_TRC_ALL,
                     "\n nmhGetIssReservedFrameCtrlOtherMacMask returned Success");
            ISS_TRC_FN_EXIT ();

            return SNMP_SUCCESS;
        }
    }
    ISS_TRC (ISS_TRC_ALL,
             "\n nmhGetIssReservedFrameCtrlOtherMacMask returned Failure");
    ISS_TRC_FN_EXIT ();

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssReservedFrameCtrlMatchCount
 Input       :  The Indices
                IssReservedFrameCtrlId

                The Object
                retValIssReservedFrameCtrlMatchCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssReservedFrameCtrlMatchCount (UINT4 u4IssReservedFrameCtrlId,
                                      UINT4
                                      *pu4RetValIssReservedFrameCtrlMatchCount)
{
    UNUSED_PARAM (u4IssReservedFrameCtrlId);
    *pu4RetValIssReservedFrameCtrlMatchCount = 0;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIssReservedFrameCtrlStatsEnabledStatus
 Input       :  The Indices
                IssReservedFrameCtrlId

                The Object
                retValIssReservedFrameCtrlStatsEnabledStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssReservedFrameCtrlStatsEnabledStatus (UINT4 u4IssReservedFrameCtrlId,
                                              INT4
                                              *pi4RetValIssReservedFrameCtrlStatsEnabledStatus)
{
    UNUSED_PARAM (pi4RetValIssReservedFrameCtrlStatsEnabledStatus);
    UNUSED_PARAM (u4IssReservedFrameCtrlId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIssClearReservedFrameCtrlStats
 Input       :  The Indices
                IssReservedFrameCtrlId

                The Object
                retValIssClearReservedFrameCtrlStats
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssClearReservedFrameCtrlStats (UINT4 u4IssReservedFrameCtrlId,
                                      INT4
                                      *pi4RetValIssClearReservedFrameCtrlStats)
{
    UNUSED_PARAM (u4IssReservedFrameCtrlId);
    UNUSED_PARAM (pi4RetValIssClearReservedFrameCtrlStats);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIssReservedFrameCtrlStatus
 Input       :  The Indices
                IssReservedFrameCtrlId

                The Object
                retValIssReservedFrameCtrlStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssReservedFrameCtrlStatus (UINT4 u4IssReservedFrameCtrlId,
                                  INT4 *pi4RetValIssReservedFrameCtrlStatus)
{
    tIssReservFrmCtrlTable *pIssReservFrmCtrlInfo = NULL;

    ISS_TRC_FN_ENTRY ();

    pIssReservFrmCtrlInfo =
        GET_RESERVE_FRAME_CTRL_INFO (u4IssReservedFrameCtrlId);
    if (pIssReservFrmCtrlInfo != NULL)
    {
        if (pIssReservFrmCtrlInfo->u1RowStatus != 0)
        {
            *pi4RetValIssReservedFrameCtrlStatus =
                pIssReservFrmCtrlInfo->u1RowStatus;
            ISS_TRC (ISS_TRC_ALL,
                     "\n  nmhGetIssReservedFrameCtrlStatus returned success");
            ISS_TRC_FN_EXIT ();

            return SNMP_SUCCESS;
        }
    }
    ISS_TRC (ISS_TRC_ALL,
             "\n  nmhGetIssReservedFrameCtrlStatus returned Failure");
    ISS_TRC_FN_EXIT ();

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIssReservedFrameCtrlPktType
 Input       :  The Indices
                IssReservedFrameCtrlId

                The Object
                setValIssReservedFrameCtrlPktType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssReservedFrameCtrlPktType (UINT4 u4IssReservedFrameCtrlId,
                                   INT4 i4SetValIssReservedFrameCtrlPktType)
{
    tIssReservFrmCtrlTable *pIssReservFrmCtrlInfo = NULL;

    pIssReservFrmCtrlInfo =
        GET_RESERVE_FRAME_CTRL_INFO (u4IssReservedFrameCtrlId);

    if (pIssReservFrmCtrlInfo == NULL)
    {
        CLI_SET_ERR (CLI_ACL_RESERV_FRAME_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pIssReservFrmCtrlInfo->u4ControlPktType =
        (UINT4) i4SetValIssReservedFrameCtrlPktType;

    MEMSET (pIssReservFrmCtrlInfo->MacAddr, 0, sizeof (tMacAddr));

    switch (i4SetValIssReservedFrameCtrlPktType)
    {
        case ISS_RESERVED_FRAME_BPDU:
            MEMCPY (pIssReservFrmCtrlInfo->MacAddr, ISS_BPDU_MAC_ADDRESS,
                    sizeof (tMacAddr));
            pIssReservFrmCtrlInfo->u4MacMask = 0xFF;
            break;
        case ISS_RESERVED_FRAME_LACPDU:
            MEMCPY (pIssReservFrmCtrlInfo->MacAddr, ISS_LACPDU_MAC_ADDRESS,
                    sizeof (tMacAddr));
            pIssReservFrmCtrlInfo->u4MacMask = 0xFF;
            break;
        case ISS_RESERVED_FRAME_EAP:
            MEMCPY (pIssReservFrmCtrlInfo->MacAddr, ISS_EAP_MAC_ADDRESS,
                    sizeof (tMacAddr));
            pIssReservFrmCtrlInfo->u4MacMask = 0xFF;
            break;
        case ISS_RESERVED_FRAME_LLDPDU:
            MEMCPY (pIssReservFrmCtrlInfo->MacAddr, ISS_LLDPDU_MAC_ADDRESS,
                    sizeof (tMacAddr));
            pIssReservFrmCtrlInfo->u4MacMask = 0xFF;
            break;
        case ISS_RESERVED_FRAME_OTHER:
            /* The specified Mac address will set through the OtherMac address MIB */
            break;
        case ISS_RESERVED_FRAME_ALL:

/*
 * while setting All mac addresses to be transmitted or suppressed based on the action
 *performed,STP mac address copied(since it is a starting MAC address) and MASK will be
 *set as "0xE0" (maximum range of 32 addresses).
 */

            MEMCPY (pIssReservFrmCtrlInfo->MacAddr, ISS_BPDU_MAC_ADDRESS,
                    sizeof (tMacAddr));
            pIssReservFrmCtrlInfo->u4MacMask = 0xE0;
            break;
        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssReservedFrameCtrlAction
 Input       :  The Indices
                IssReservedFrameCtrlId

                The Object
                setValIssReservedFrameCtrlAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssReservedFrameCtrlAction (UINT4 u4IssReservedFrameCtrlId,
                                  INT4 i4SetValIssReservedFrameCtrlAction)
{
    tIssReservFrmCtrlTable *pIssReservFrmCtrlInfo = NULL;

    ISS_TRC_FN_ENTRY ();

    pIssReservFrmCtrlInfo =
        GET_RESERVE_FRAME_CTRL_INFO (u4IssReservedFrameCtrlId);

    if (pIssReservFrmCtrlInfo == NULL)
    {
        CLI_SET_ERR (CLI_ACL_RESERV_FRAME_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pIssReservFrmCtrlInfo->u1ControlAction =
        (UINT1) i4SetValIssReservedFrameCtrlAction;
    ISS_TRC (ISS_TRC_ALL,
             "\n nmhSetIssReservedFrameCtrlAction returned success");
    ISS_TRC_FN_EXIT ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssReservedFrameCtrlOtherMacAddr
 Input       :  The Indices
                IssReservedFrameCtrlId

                The Object
                setValIssReservedFrameCtrlOtherMacAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssReservedFrameCtrlOtherMacAddr (UINT4 u4IssReservedFrameCtrlId,
                                        tMacAddr
                                        SetValIssReservedFrameCtrlOtherMacAddr)
{
    tIssReservFrmCtrlTable *pIssReservFrmCtrlInfo = NULL;

    ISS_TRC_FN_ENTRY ();

    pIssReservFrmCtrlInfo =
        GET_RESERVE_FRAME_CTRL_INFO (u4IssReservedFrameCtrlId);

    if (pIssReservFrmCtrlInfo == NULL)
    {
        CLI_SET_ERR (CLI_ACL_RESERV_FRAME_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    MEMSET (pIssReservFrmCtrlInfo->MacAddr, 0, sizeof (tMacAddr));

    MEMCPY (pIssReservFrmCtrlInfo->MacAddr,
            SetValIssReservedFrameCtrlOtherMacAddr, sizeof (tMacAddr));

    ISS_TRC (ISS_TRC_ALL,
             "\n nmhSetIssReservedFrameCtrlOtherMacAddr returned success");
    ISS_TRC_FN_EXIT ();

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssReservedFrameCtrlOtherMacMask
 Input       :  The Indices
                IssReservedFrameCtrlId

                The Object
                setValIssReservedFrameCtrlOtherMacMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssReservedFrameCtrlOtherMacMask (UINT4 u4IssReservedFrameCtrlId,
                                        INT4
                                        i4SetValIssReservedFrameCtrlOtherMacMask)
{
    tIssReservFrmCtrlTable *pIssReservFrmCtrlInfo = NULL;

    ISS_TRC_FN_ENTRY ();

    pIssReservFrmCtrlInfo =
        GET_RESERVE_FRAME_CTRL_INFO (u4IssReservedFrameCtrlId);

    if (pIssReservFrmCtrlInfo == NULL)
    {
        CLI_SET_ERR (CLI_ACL_RESERV_FRAME_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pIssReservFrmCtrlInfo->u4MacMask =
        (UINT4) i4SetValIssReservedFrameCtrlOtherMacMask;

    ISS_TRC (ISS_TRC_ALL,
             "\n nmhSetIssReservedFrameCtrlOtherMacAddr returned success");

    ISS_TRC_FN_EXIT ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssReservedFrameCtrlStatsEnabledStatus
 Input       :  The Indices
                IssReservedFrameCtrlId

                The Object
                setValIssReservedFrameCtrlStatsEnabledStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssReservedFrameCtrlStatsEnabledStatus (UINT4 u4IssReservedFrameCtrlId,
                                              INT4
                                              i4SetValIssReservedFrameCtrlStatsEnabledStatus)
{
    UNUSED_PARAM (u4IssReservedFrameCtrlId);
    UNUSED_PARAM (i4SetValIssReservedFrameCtrlStatsEnabledStatus);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIssClearReservedFrameCtrlStats
 Input       :  The Indices
                IssReservedFrameCtrlId

                The Object
                setValIssClearReservedFrameCtrlStats
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssClearReservedFrameCtrlStats (UINT4 u4IssReservedFrameCtrlId,
                                      INT4
                                      i4SetValIssClearReservedFrameCtrlStats)
{
    UNUSED_PARAM (u4IssReservedFrameCtrlId);
    UNUSED_PARAM (i4SetValIssClearReservedFrameCtrlStats);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssReservedFrameCtrlStatus
 Input       :  The Indices
                IssReservedFrameCtrlId

                The Object
                setValIssReservedFrameCtrlStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssReservedFrameCtrlStatus (UINT4 u4IssReservedFrameCtrlId,
                                  INT4 i4SetValIssReservedFrameCtrlStatus)
{
    tIssReservFrmCtrlTable *pIssReservFrmCtrlInfo = NULL;
    UINT1               u1CurrRowStatus = 0;
#ifdef NPAPI_WANTED
    INT4                i4RetVal = 0;
#endif

    pIssReservFrmCtrlInfo =
        GET_RESERVE_FRAME_CTRL_INFO (u4IssReservedFrameCtrlId);

    if (pIssReservFrmCtrlInfo != NULL)
    {
        if (pIssReservFrmCtrlInfo->u1RowStatus ==
            (UINT1) i4SetValIssReservedFrameCtrlStatus)
        {
            return SNMP_SUCCESS;
        }

        if (i4SetValIssReservedFrameCtrlStatus == ISS_CREATE_AND_WAIT)
        {
            CLI_SET_ERR (CLI_ACL_RESERV_FRAME_ENTRY_EXIST);
            return SNMP_FAILURE;
        }

        u1CurrRowStatus = pIssReservFrmCtrlInfo->u1RowStatus;
    }
    else
    {
        /* This Filter Entry is not there in the Database */
        if ((i4SetValIssReservedFrameCtrlStatus != ISS_CREATE_AND_WAIT))
        {
            CLI_SET_ERR (CLI_ACL_NO_FILTER);
            return SNMP_FAILURE;
        }
    }

    if (u1CurrRowStatus == i4SetValIssReservedFrameCtrlStatus)
    {
        return SNMP_SUCCESS;
    }

    switch (i4SetValIssReservedFrameCtrlStatus)
    {
        case ISS_CREATE_AND_WAIT:
        case ISS_CREATE_AND_GO:
            if ((ISS_RESERVE_FRAME_ALLOC_MEM_BLOCK (pIssReservFrmCtrlInfo)) ==
                NULL)
            {
                ISS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "No Free Reserv Frame Control  Entry\n");
                return SNMP_FAILURE;
            }

            ISS_MEMSET (pIssReservFrmCtrlInfo, 0,
                        sizeof (tIssReservFrmCtrlTable));

            GET_RESERVE_FRAME_CTRL_INFO (u4IssReservedFrameCtrlId) =
                pIssReservFrmCtrlInfo;

            pIssReservFrmCtrlInfo->u4ReservedFrameCtrlId =
                u4IssReservedFrameCtrlId;
            pIssReservFrmCtrlInfo->u1RowStatus = ISS_NOT_READY;

            break;

        case ISS_NOT_IN_SERVICE:
            if (pIssReservFrmCtrlInfo->u1RowStatus == ISS_ACTIVE)
            {
#ifdef NPAPI_WANTED
                if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                {
                    /*Updating reserv frame action to the hardware */
                    i4RetVal =
                        IsssysIssHwSetResrvFrameFilter (pIssReservFrmCtrlInfo,
                                                        ISS_RESERV_FRAME_DELETE);
                    if (i4RetVal != FNP_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                }
#endif
            }
            pIssReservFrmCtrlInfo->u1RowStatus = ISS_NOT_IN_SERVICE;

            break;

        case ISS_ACTIVE:
#ifdef NPAPI_WANTED
            if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
            {
                /*Updating reserv frame action to the hardware */
                i4RetVal =
                    IsssysIssHwSetResrvFrameFilter (pIssReservFrmCtrlInfo,
                                                    ISS_RESERV_FRAME_ADD);
                if (i4RetVal != FNP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
#endif
            pIssReservFrmCtrlInfo->u1RowStatus = ISS_ACTIVE;

            break;
        case ISS_DESTROY:
#ifdef NPAPI_WANTED
            if (pIssReservFrmCtrlInfo->u1RowStatus == ISS_ACTIVE)
            {
                if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                {
                    /*Updating reserv frame action to the hardware */
                    i4RetVal =
                        IsssysIssHwSetResrvFrameFilter (pIssReservFrmCtrlInfo,
                                                        ISS_RESERV_FRAME_DELETE);
                    if (i4RetVal != FNP_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                }
            }
#endif
            if (pIssReservFrmCtrlInfo != NULL)
            {
                if (ISS_RESERVE_FRAME_FREE_MEM_BLOCK (pIssReservFrmCtrlInfo) !=
                    MEM_SUCCESS)
                {
                    ISS_TRC (INIT_SHUT_TRC,
                             "ISS_RATEENTRY_FREE_MEM_BLOCK() FAILED\n");
                }
            }
            GET_RESERVE_FRAME_CTRL_INFO (u4IssReservedFrameCtrlId) = NULL;
            break;
        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IssReservedFrameCtrlPktType
 Input       :  The Indices
                IssReservedFrameCtrlId

                The Object
                testValIssReservedFrameCtrlPktType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssReservedFrameCtrlPktType (UINT4 *pu4ErrorCode,
                                      UINT4 u4IssReservedFrameCtrlId,
                                      INT4 i4TestValIssReservedFrameCtrlPktType)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Enter nmhTestv2IssReservedFrameCtrlPktType function \n");

    if (ISS_IS_RESERV_FRM_CTRL_ID_VALID (u4IssReservedFrameCtrlId) != ISS_TRUE)
    {
        CLI_SET_ERR (CLI_ACL_INVALID_RESER_FRAME_CTRL_ID);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:ReservCtrl filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((GET_RESERVE_FRAME_CTRL_INFO (u4IssReservedFrameCtrlId)->u1RowStatus
         == ISS_ACTIVE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (!((i4TestValIssReservedFrameCtrlPktType == RESERVED_FRAME_BPDU) ||
          (i4TestValIssReservedFrameCtrlPktType == RESERVED_FRAME_LACPDU) ||
          (i4TestValIssReservedFrameCtrlPktType == RESERVED_FRAME_EAP) ||
          (i4TestValIssReservedFrameCtrlPktType == RESERVED_FRAME_LLDPDU) ||
          (i4TestValIssReservedFrameCtrlPktType == RESERVED_FRAME_OTHER) ||
          (i4TestValIssReservedFrameCtrlPktType == RESERVED_FRAME_ALL)))
    {
        CLI_SET_ERR (CLI_ACL_INVALID_RESERV_FRAME_PKT_TYPE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssReservedFrameCtrlPktType function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssReservedFrameCtrlAction
 Input       :  The Indices
                IssReservedFrameCtrlId

                The Object
                testValIssReservedFrameCtrlAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssReservedFrameCtrlAction (UINT4 *pu4ErrorCode,
                                     UINT4 u4IssReservedFrameCtrlId,
                                     INT4 i4TestValIssReservedFrameCtrlAction)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Enter nmhTestv2IssReservedFrameCtrlAction function \n");

    if (ISS_IS_RESERV_FRM_CTRL_ID_VALID (u4IssReservedFrameCtrlId) != ISS_TRUE)
    {
        CLI_SET_ERR (CLI_ACL_INVALID_RESER_FRAME_CTRL_ID);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:ReservCtrl filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((GET_RESERVE_FRAME_CTRL_INFO (u4IssReservedFrameCtrlId)->u1RowStatus
         == ISS_ACTIVE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssReservedFrameCtrlAction != RESERVED_FRAME_ACTION_FWD) &&
        (i4TestValIssReservedFrameCtrlAction != RESERVED_FRAME_ACTION_DROP))
    {
        CLI_SET_ERR (CLI_ACL_INVALID_RESERV_FRAME_CONTROL_ACTION);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssReservedFrameCtrlAction function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssReservedFrameCtrlOtherMacAddr
 Input       :  The Indices
                IssReservedFrameCtrlId

                The Object
                testValIssReservedFrameCtrlOtherMacAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2IssReservedFrameCtrlOtherMacAddr (UINT4 *pu4ErrorCode,
                                           UINT4 u4IssReservedFrameCtrlId,
                                           tMacAddr
                                           TestValIssReservedFrameCtrlOtherMacAddr)
{

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Enter nmhTestv2IssReservedFrameCtrlAction function \n");

    if (ISS_IS_RESERV_FRM_CTRL_ID_VALID (u4IssReservedFrameCtrlId) != ISS_TRUE)
    {
        CLI_SET_ERR (CLI_ACL_INVALID_RESER_FRAME_CTRL_ID);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:ReservCtrl filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((GET_RESERVE_FRAME_CTRL_INFO (u4IssReservedFrameCtrlId)->u1RowStatus
         == ISS_ACTIVE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (!
        ((TestValIssReservedFrameCtrlOtherMacAddr[0] == 0x01)
         && (TestValIssReservedFrameCtrlOtherMacAddr[1] == 0x80)
         && (TestValIssReservedFrameCtrlOtherMacAddr[2] == 0xc2)
         && (TestValIssReservedFrameCtrlOtherMacAddr[3] == 0x00)
         && (TestValIssReservedFrameCtrlOtherMacAddr[4] == 0x00)
         && ((TestValIssReservedFrameCtrlOtherMacAddr[5] <= 0x0f)
             || ((TestValIssReservedFrameCtrlOtherMacAddr[5] >= 0x20)
                 && (TestValIssReservedFrameCtrlOtherMacAddr[5] <= 0x2f)))))
    {
        CLI_SET_ERR (CLI_ACL_INVALID_RESERV_FRAME_MAC_ADDRESS);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:Reserved MAC address is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssReservedFrameCtrlOtherMacAddr function \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssReservedFrameCtrlOtherMacMask
 Input       :  The Indices
                IssReservedFrameCtrlId

                The Object
                testValIssReservedFrameCtrlPktType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssReservedFrameCtrlOtherMacMask (UINT4 *pu4ErrorCode,
                                           UINT4 u4IssReservedFrameCtrlId,
                                           INT4
                                           i4TestValIssReservedFrameCtrlOtherMacMask)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Enter nmhTestv2IssReservedFrameCtrlOtherMacMask function \n");

    if (ISS_IS_RESERV_FRM_CTRL_ID_VALID (u4IssReservedFrameCtrlId) != ISS_TRUE)
    {
        CLI_SET_ERR (CLI_ACL_INVALID_RESER_FRAME_CTRL_ID);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:ReservCtrl filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }

    if ((GET_RESERVE_FRAME_CTRL_INFO (u4IssReservedFrameCtrlId)->u1RowStatus
         == ISS_ACTIVE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (!
        ((i4TestValIssReservedFrameCtrlOtherMacMask == 0xff)
         || (i4TestValIssReservedFrameCtrlOtherMacMask == 0xfe)
         || (i4TestValIssReservedFrameCtrlOtherMacMask == 0xfc)
         || (i4TestValIssReservedFrameCtrlOtherMacMask == 0xf8)
         || (i4TestValIssReservedFrameCtrlOtherMacMask == 0xf0)
         || (i4TestValIssReservedFrameCtrlOtherMacMask == 0xe0)))
    {
        CLI_SET_ERR (CLI_ACL_INVALID_RESERV_FRAME_MAC_MASK);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:ReservCtrl Mac mask is invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssReservedFrameCtrlStatsEnabledStatus
 Input       :  The Indices
                IssReservedFrameCtrlId

                The Object
                testValIssReservedFrameCtrlStatsEnabledStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssReservedFrameCtrlStatsEnabledStatus (UINT4 *pu4ErrorCode,
                                                 UINT4 u4IssReservedFrameCtrlId,
                                                 INT4
                                                 i4TestValIssReservedFrameCtrlStatsEnabledStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4IssReservedFrameCtrlId);
    UNUSED_PARAM (i4TestValIssReservedFrameCtrlStatsEnabledStatus);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssClearReservedFrameCtrlStats
 Input       :  The Indices
                IssReservedFrameCtrlId

                The Object
                testValIssClearReservedFrameCtrlStats
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssClearReservedFrameCtrlStats (UINT4 *pu4ErrorCode,
                                         UINT4 u4IssReservedFrameCtrlId,
                                         INT4
                                         i4TestValIssClearReservedFrameCtrlStats)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4IssReservedFrameCtrlId);
    UNUSED_PARAM (i4TestValIssClearReservedFrameCtrlStats);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssReservedFrameCtrlStatus
 Input       :  The Indices
                IssReservedFrameCtrlId

                The Object
                testValIssReservedFrameCtrlStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssReservedFrameCtrlStatus (UINT4 *pu4ErrorCode,
                                     UINT4 u4IssReservedFrameCtrlId,
                                     INT4 i4TestValIssReservedFrameCtrlStatus)
{
    tIssReservFrmCtrlTable *pIssReservFrmCtrlInfo = NULL;
    INT4                i4Retval = 0;
    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Enter nmhTestv2IssReservedFrameCtrlStatus function \n");

    if (ISS_IS_RESERV_FRM_CTRL_ID_VALID (u4IssReservedFrameCtrlId) != ISS_TRUE)
    {
        CLI_SET_ERR (CLI_ACL_INVALID_RESER_FRAME_CTRL_ID);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nLOW:ReservCtrl filter ID is Invalid with errorcode as %d\n",
                      *pu4ErrorCode);
        return SNMP_FAILURE;
    }
    pIssReservFrmCtrlInfo =
        GET_RESERVE_FRAME_CTRL_INFO (u4IssReservedFrameCtrlId);
    if (pIssReservFrmCtrlInfo == NULL)
    {
        /*Entry is not yet created */
        return SNMP_SUCCESS;
    }

    if ((pIssReservFrmCtrlInfo->u1RowStatus == 0) &&
        ((i4TestValIssReservedFrameCtrlStatus != ISS_CREATE_AND_WAIT)
         && (i4TestValIssReservedFrameCtrlStatus != ISS_CREATE_AND_GO)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (i4TestValIssReservedFrameCtrlStatus == ISS_ACTIVE)
    {
        if (GET_RESERVE_FRAME_CTRL_INFO (u4IssReservedFrameCtrlId)->
            u4ReservedFrameCtrlId == 0)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        /*checking whether any entry exist in control table */
        if ((pIssReservFrmCtrlInfo != NULL) &&
            (pIssReservFrmCtrlInfo->u4ControlPktType == ISS_RESERVED_FRAME_ALL))
        {
            i4Retval = IssResrvFrmIsAnyEntryExist (pIssReservFrmCtrlInfo);
            if (i4Retval == SNMP_FAILURE)
            {
                /*clearing the Memory allocated for this entry since it is already exist */
                i4Retval =
                    IssClearResrvFrameCtrlEntry (pIssReservFrmCtrlInfo,
                                                 u4IssReservedFrameCtrlId);
                if (i4Retval == SNMP_FAILURE)
                {
                    /*Return value handling is for coverity */
                }
                CLI_SET_ERR (CLI_ACL_RESRV_FRM_ENTRY_EXIST);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }

        /*Checking whether other Protocol MAC address matches with the existing entry
           and vice versa. */
        if (pIssReservFrmCtrlInfo != NULL)
        {
            i4Retval =
                IssResrvFrmIsOtherProtoMacMatched (pIssReservFrmCtrlInfo);
            if (i4Retval == SNMP_FAILURE)
            {
                /*clearing the Memory allocated for this entry since it is already exist */
                i4Retval =
                    IssClearResrvFrameCtrlEntry (pIssReservFrmCtrlInfo,
                                                 u4IssReservedFrameCtrlId);
                if (i4Retval == SNMP_FAILURE)
                {
                    /*Return value handling is for coverity */
                }

                CLI_SET_ERR (CLI_ACL_RESRV_FRM_ENTRY_EXIST);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }

        /*checking whether the all protocols action has been done already in the control table */
        i4Retval = IssResrvFrmIsAllProtoActionDone (pIssReservFrmCtrlInfo);
        if (i4Retval == SNMP_SUCCESS)
        {
            i4Retval =
                IssClearResrvFrameCtrlEntry (pIssReservFrmCtrlInfo,
                                             u4IssReservedFrameCtrlId);
            if (i4Retval == SNMP_FAILURE)
            {
                /*Return value handling is for coverity */
            }

            CLI_SET_ERR (CLI_ACL_ALL_PROTO_ACT_DONE);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

    }
    if ((i4TestValIssReservedFrameCtrlStatus == ISS_CREATE_AND_WAIT)
        && (MemGetFreeUnits (ISS_RESERV_FRAME_CTRL_ENTRY_POOL_ID) == 0))
    {
        CLI_SET_ERR (CLI_ACL_MAX_RESERV_FRAME_ENTRY);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return CLI_FAILURE;
    }

    ISS_TRC (INIT_SHUT_TRC,
             "\nLOW:Exit nmhTestv2IssReservedFrameCtrlStatus function \n");
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IssReservedFrameCtrlTable
 Input       :  The Indices
                IssReservedFrameCtrlId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IssReservedFrameCtrlTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
