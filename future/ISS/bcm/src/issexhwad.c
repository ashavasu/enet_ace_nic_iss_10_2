/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: issexhwad.c,v 1.6 2016/07/09 09:40:39 siva Exp $
 *
 * Description: This file contains the  NP SYNC handling functions 
 * for hardware audit process.
 *****************************************************************************/

#include "issexinc.h"

/*****************************************************************************/
/* Function Name      : NpSyncIssHwUpdateL2Filter                 */
/*                                                                           */
/* Description        : This function sends the NP Sync message and          */
/*            calls the corresponding NPAPI function and updates   */
/*             the NP Sync buffer table in case of Standby Node.    */
/*                                                                           */
/* Input(s)           : pIssL2FilterEntry- Pointer to the structure of       */
/*                    L2 Filter Entry              */
/*            i4Value - Filter Add/Delete                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                 */
/*****************************************************************************/
PUBLIC INT4
NpSyncIssHwUpdateL2Filter (tIssL2FilterEntry * pIssL2FilterEntry, INT4 i4Value)
{
    unNpSync            NpSync;
    INT4                i4RetVal = OSIX_SUCCESS;
    INT4                i4FnpRetVal = FNP_SUCCESS;
    INT4                i4Action = 0;
    UINT4               u4NodeState = ISS_ZERO_ENTRY;

    MEMSET (&NpSync, ISS_ZERO_ENTRY, sizeof (unNpSync));

#ifdef NPAPI_WANTED
#undef IsssysIssHwUpdateL2Filter
if (pIssL2FilterEntry->u4SChannelIfIndex != 0 )
{
    if (pIssL2FilterEntry->u1IssL2FilterStatus == ISS_ACL_FILTER_ENABLE) 
    {
       i4Action = ISS_L2FILTER_ADD; 

       i4FnpRetVal = IsssysIssHwUpdateL2Filter (pIssL2FilterEntry, i4Action);
        if (i4FnpRetVal == FNP_FAILURE)
        {
            i4RetVal = OSIX_FAILURE;
        }
        else if (i4FnpRetVal == FNP_NOT_SUPPORTED)
        {
            i4RetVal = FNP_NOT_SUPPORTED;
        }
        else
        {
            i4RetVal = OSIX_SUCCESS;
        }

   return i4RetVal; 
    }
}
#endif


    NpSync.IssHwUpdateL2Filter.i4IssL2FilterNo =
        pIssL2FilterEntry->i4IssL2FilterNo;
    MEMCPY (&(NpSync.IssHwUpdateL2Filter.IssFilterShadowTable),
            pIssL2FilterEntry->pIssFilterShadowInfo,
            sizeof (tIssFilterShadowTable));
    NpSync.IssHwUpdateL2Filter.i4Value = i4Value;

    u4NodeState = AclPortRmGetNodeState ();

    if (u4NodeState == RM_ACTIVE)
    {
        if (AclPortRmGetStandbyNodeCount () != ISS_ZERO_ENTRY)
        {
            if (ACL_NPSYNC_BLK () == ISS_ZERO_ENTRY)
            {
                NpSyncIssHwUpdateL2FilterSync (NpSync.IssHwUpdateL2Filter,
                                               RM_ACL_APP_ID,
                                               NPSYNC_ISS_HW_UPDATE_L2_FILTER,
                                               ISS_ACL_NP_SYNC_UPDATE);
            }
        }
#ifdef NPAPI_WANTED
#undef IsssysIssHwUpdateL2Filter
        i4FnpRetVal = IsssysIssHwUpdateL2Filter (pIssL2FilterEntry, i4Value);
        if (i4FnpRetVal == FNP_FAILURE)
        {
            i4RetVal = OSIX_FAILURE;
        }
        else if (i4FnpRetVal == FNP_NOT_SUPPORTED)
        {
            i4RetVal = FNP_NOT_SUPPORTED;
        }
#endif

        if (AclPortRmGetStandbyNodeCount () != ISS_ZERO_ENTRY)
        {
            if (ACL_NPSYNC_BLK () == ISS_ZERO_ENTRY)
            {
                if (i4RetVal == OSIX_FAILURE)
                {
                    /* if NP call is failure, send the NP sync message to the 
                       standby to delete the entry from the Np Sync Buffer table */
                    NpSyncIssHwUpdateL2FilterSync (NpSync.IssHwUpdateL2Filter,
                                                   RM_ACL_APP_ID,
                                                   NPSYNC_ISS_HW_UPDATE_L2_FILTER,
                                                   ISS_ACL_NP_SYNC_FAILURE);
                }
                else
                {
                    /* if NP call is success, update the Hardware Handle in the
                       Np Sync message and send the Sync message to the standby */
                    MEMCPY (&(NpSync.IssHwUpdateL2Filter.
                              IssFilterShadowTable), pIssL2FilterEntry->
                            pIssFilterShadowInfo,
                            sizeof (tIssFilterShadowTable));
                    NpSyncIssHwUpdateL2FilterSync (NpSync.IssHwUpdateL2Filter,
                                                   RM_ACL_APP_ID,
                                                   NPSYNC_ISS_HW_UPDATE_L2_FILTER,
                                                   ISS_ACL_NP_SYNC_SUCCESS);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (ACL_NPSYNC_BLK () == ISS_ZERO_ENTRY)
        {
            AclRedUpdateNpSyncBufferTable
                (&NpSync, NPSYNC_ISS_HW_UPDATE_L2_FILTER,
                 ISS_ACL_NP_SYNC_UPDATE);
        }
    }
    return (i4FnpRetVal);
}

/*****************************************************************************/
/* Function Name      : NpSyncIssHwUpdateL3Filter                 */
/*                                                                           */
/* Description        : This function sends the NP Sync message and          */
/*            calls the corresponding NPAPI function and updates   */
/*             the NP Sync buffer table in case of Standby Node.    */
/*                                                                           */
/* Input(s)           : pIssL3FilterEntry- Pointer to the structure of       */
/*                    L3 Filter Entry              */
/*            i4Value - Filter Add/Delete                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                 */
/*****************************************************************************/
PUBLIC INT4
NpSyncIssHwUpdateL3Filter (tIssL3FilterEntry * pIssL3FilterEntry, INT4 i4Value)
{
    unNpSync            NpSync;
    INT4                i4RetVal = OSIX_SUCCESS;
    INT4                i4FnpRetVal = FNP_SUCCESS;
    UINT4               u4NodeState = ISS_ZERO_ENTRY;

    MEMSET (&NpSync, ISS_ZERO_ENTRY, sizeof (unNpSync));
    NpSync.IssHwUpdateL3Filter.i4IssL3FilterNo =
        pIssL3FilterEntry->i4IssL3FilterNo;
    MEMCPY (&(NpSync.IssHwUpdateL3Filter.IssFilterShadowTable),
            pIssL3FilterEntry->pIssFilterShadowInfo,
            sizeof (tIssFilterShadowTable));
    NpSync.IssHwUpdateL3Filter.i4Value = i4Value;

    u4NodeState = AclPortRmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (AclPortRmGetStandbyNodeCount () != ISS_ZERO_ENTRY)
        {
            if (ACL_NPSYNC_BLK () == ISS_ZERO_ENTRY)
            {
                NpSyncIssHwUpdateL3FilterSync (NpSync.IssHwUpdateL3Filter,
                                               RM_ACL_APP_ID,
                                               NPSYNC_ISS_HW_UPDATE_L3_FILTER,
                                               ISS_ACL_NP_SYNC_UPDATE);
            }
        }
#ifdef NPAPI_WANTED
#undef IsssysIssHwUpdateL3Filter
        i4FnpRetVal = IsssysIssHwUpdateL3Filter (pIssL3FilterEntry, i4Value);
        if (i4FnpRetVal == FNP_FAILURE)
        {
            i4RetVal = OSIX_FAILURE;
        }
        else if (i4FnpRetVal == FNP_NOT_SUPPORTED)
        {
            i4RetVal = FNP_NOT_SUPPORTED;
        }

#endif
        if (AclPortRmGetStandbyNodeCount () != ISS_ZERO_ENTRY)
        {
            if (ACL_NPSYNC_BLK () == ISS_ZERO_ENTRY)
            {
                if (i4RetVal == OSIX_FAILURE)
                {
                    /* if NP call is failure, send the NP sync message to the 
                       standby to delete the entry from the Np Sync Buffer table */
                    NpSyncIssHwUpdateL3FilterSync (NpSync.IssHwUpdateL3Filter,
                                                   RM_ACL_APP_ID,
                                                   NPSYNC_ISS_HW_UPDATE_L3_FILTER,
                                                   ISS_ACL_NP_SYNC_FAILURE);
                }
                else
                {
                    /* if NP call is success, update the Hardware Handle in the
                       Np Sync message and send the Sync message to the standby */
                    MEMCPY (&(NpSync.IssHwUpdateL3Filter.
                              IssFilterShadowTable), pIssL3FilterEntry->
                            pIssFilterShadowInfo,
                            sizeof (tIssFilterShadowTable));
                    NpSyncIssHwUpdateL3FilterSync (NpSync.IssHwUpdateL3Filter,
                                                   RM_ACL_APP_ID,
                                                   NPSYNC_ISS_HW_UPDATE_L3_FILTER,
                                                   ISS_ACL_NP_SYNC_SUCCESS);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (ACL_NPSYNC_BLK () == ISS_ZERO_ENTRY)
        {
            AclRedUpdateNpSyncBufferTable
                (&NpSync, NPSYNC_ISS_HW_UPDATE_L3_FILTER,
                 ISS_ACL_NP_SYNC_UPDATE);
        }
    }
    return (i4FnpRetVal);
}

/*****************************************************************************/
/* Function Name      : NpSyncIssHwSetRateLimitingValue                 */
/*                                                                           */
/* Description        : This function sends the NP Sync message and          */
/*            calls the corresponding NPAPI function and updates   */
/*             the NP Sync buffer table in case of Standby Node.    */
/*                                                                           */
/* Input(s)           : Inputs to the NPAPI calls                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                 */
/*****************************************************************************/
PUBLIC INT4
NpSyncIssHwSetRateLimitingValue (UINT4 u4IfIndex,
                                 UINT1 u1PacketType, INT4 i4RateLimitVal)
{
    unNpSync            NpSync;
    INT4                i4RetVal = OSIX_SUCCESS;
    INT4                i4FnpRetVal = FNP_SUCCESS;
    UINT4               u4NodeState = ISS_ZERO_ENTRY;

    MEMSET (&NpSync, ISS_ZERO_ENTRY, sizeof (unNpSync));
    NpSync.IssHwSetRateLimitingValue.u4IfIndex = u4IfIndex;
    NpSync.IssHwSetRateLimitingValue.u1PacketType = u1PacketType;
    NpSync.IssHwSetRateLimitingValue.i4RateLimitVal = i4RateLimitVal;

    u4NodeState = AclPortRmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (AclPortRmGetStandbyNodeCount () != ISS_ZERO_ENTRY)
        {
            if (ACL_NPSYNC_BLK () == ISS_ZERO_ENTRY)
            {
                NpSyncIssHwSetRateLimitingValueSync
                    (NpSync.IssHwSetRateLimitingValue, RM_ACL_APP_ID,
                     NPSYNC_ISS_HW_SET_RATE_LIMITING_VALUE);
            }
        }
#ifdef NPAPI_WANTED
#undef IsssysIssHwSetRateLimitingValue
        i4FnpRetVal = IsssysIssHwSetRateLimitingValue (u4IfIndex, u1PacketType,
                                                 i4RateLimitVal);
        if (i4FnpRetVal == FNP_FAILURE)
        {
            i4RetVal = OSIX_FAILURE;
        }
#endif
        if (i4RetVal == OSIX_FAILURE)
        {
            if (AclPortRmGetStandbyNodeCount () != ISS_ZERO_ENTRY)
            {
                if (ACL_NPSYNC_BLK () == ISS_ZERO_ENTRY)
                {
                    NpSyncIssHwSetRateLimitingValueSync
                        (NpSync.IssHwSetRateLimitingValue, RM_ACL_APP_ID,
                         NPSYNC_ISS_HW_SET_RATE_LIMITING_VALUE);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (ACL_NPSYNC_BLK () == ISS_ZERO_ENTRY)
        {
            AclRedUpdateNpSyncBufferTable
                (&NpSync, NPSYNC_ISS_HW_SET_RATE_LIMITING_VALUE,
                 ISS_ZERO_ENTRY);
        }

    }
    return (i4FnpRetVal);
}

/*****************************************************************************/
/* Function Name      : NpSyncIssHwSetPortEgressPktRate                 */
/*                                                                           */
/* Description        : This function sends the NP Sync message and          */
/*            calls the corresponding NPAPI function and updates   */
/*             the NP Sync buffer table in case of Standby Node.    */
/*                                                                           */
/* Input(s)           : Inputs to the NPAPI calls                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                 */
/*****************************************************************************/
PUBLIC INT4
NpSyncIssHwSetPortEgressPktRate (UINT4 u4IfIndex,
                                 INT4 i4PktRate, INT4 i4BurstRate)
{
    unNpSync            NpSync;
    INT4                i4RetVal = OSIX_SUCCESS;
    INT4                i4FnpRetVal = FNP_SUCCESS;
    UINT4               u4NodeState = ISS_ZERO_ENTRY;

    MEMSET (&NpSync, ISS_ZERO_ENTRY, sizeof (unNpSync));
    NpSync.IssHwSetPortEgressPktRate.u4IfIndex = u4IfIndex;
    NpSync.IssHwSetPortEgressPktRate.i4PktRate = i4PktRate;
    NpSync.IssHwSetPortEgressPktRate.i4BurstRate = i4BurstRate;

    u4NodeState = AclPortRmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (AclPortRmGetStandbyNodeCount () != ISS_ZERO_ENTRY)
        {
            if (ACL_NPSYNC_BLK () == ISS_ZERO_ENTRY)
            {
                NpSyncIssHwSetPortEgressPktRateSync
                    (NpSync.IssHwSetPortEgressPktRate, RM_ACL_APP_ID,
                     NPSYNC_ISS_HW_SET_PORT_EGRESS_PKT_RATE);
            }
        }
#ifdef NPAPI_WANTED
#undef IsssysIssHwSetPortEgressPktRate
        i4FnpRetVal = IsssysIssHwSetPortEgressPktRate (u4IfIndex, i4PktRate,
                                                 i4BurstRate);
        if (i4FnpRetVal == FNP_FAILURE)
        {
            i4RetVal = OSIX_FAILURE;
        }
#endif
        if (i4RetVal == OSIX_FAILURE)
        {
            if (AclPortRmGetStandbyNodeCount () != ISS_ZERO_ENTRY)
            {
                if (ACL_NPSYNC_BLK () == ISS_ZERO_ENTRY)
                {
                    NpSyncIssHwSetPortEgressPktRateSync
                        (NpSync.IssHwSetPortEgressPktRate, RM_ACL_APP_ID,
                         NPSYNC_ISS_HW_SET_PORT_EGRESS_PKT_RATE);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (ACL_NPSYNC_BLK () == ISS_ZERO_ENTRY)
        {
            AclRedUpdateNpSyncBufferTable
                (&NpSync, NPSYNC_ISS_HW_SET_PORT_EGRESS_PKT_RATE,
                 ISS_ZERO_ENTRY);
        }
    }
    return (i4FnpRetVal);
}

/*****************************************************************************/
/* Function Name      : NpSyncIssHwUpdateL2FilterSync                 */
/*                                                                           */
/* Description        : This function forms the the NP Sync message and      */
/*            post the message to RM Module.                  */
/*                                                                           */
/* Input(s)           : Union to the NP Sync Entry, NPAPI ID, Application ID */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
VOID
NpSyncIssHwUpdateL2FilterSync (tNpSyncIssHwUpdateL2Filter
                               wUpdateL2Filter, UINT4 u4AppId,
                               UINT4 u4NpApiId, UINT4 u4Status)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT4               u4Count = ISS_ZERO_ENTRY;
    UINT2               u2OffSet = ISS_ZERO_ENTRY;
    UINT2               u2MsgSize = ISS_ZERO_ENTRY;

    MEMSET (&ProtoEvt, ISS_ZERO_ENTRY, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize = NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH +
        NPSYNC_MSG_NPAPI_ID_SIZE + sizeof (INT4) + sizeof (INT4) +
        sizeof (tIssFilterShadowTable) + sizeof (INT4);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        AclPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = ISS_ZERO_ENTRY;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4Status);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, wUpdateL2Filter.i4IssL2FilterNo);
        for (u4Count = ISS_ZERO_ENTRY; u4Count < ISS_FILTER_SHADOW_MEM_SIZE;
             u4Count++)
        {
            NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                                  wUpdateL2Filter.IssFilterShadowTable.
                                  au4HwHandle[u4Count]);
        }
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, wUpdateL2Filter.i4Value);

        if (AclPortRmEnqMsgToRm (pMsg, u2OffSet, u4AppId,
                                 u4AppId) == OSIX_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            AclPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name      : NpSyncIssHwUpdateL3FilterSync                 */
/*                                                                           */
/* Description        : This function forms the the NP Sync message and      */
/*            post the message to RM Module.                  */
/*                                                                           */
/* Input(s)           : Union to the NP Sync Entry, NPAPI ID, Application ID */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
VOID
NpSyncIssHwUpdateL3FilterSync (tNpSyncIssHwUpdateL3Filter
                               wUpdateL3Filter, UINT4 u4AppId,
                               UINT4 u4NpApiId, UINT4 u4Status)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT4               u4Count = ISS_ZERO_ENTRY;
    UINT2               u2OffSet = ISS_ZERO_ENTRY;
    UINT2               u2MsgSize = ISS_ZERO_ENTRY;

    MEMSET (&ProtoEvt, ISS_ZERO_ENTRY, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize = NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH +
        NPSYNC_MSG_NPAPI_ID_SIZE + sizeof (INT4) + sizeof (INT4) +
        sizeof (tIssFilterShadowTable) + sizeof (INT4);

    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        AclPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = ISS_ZERO_ENTRY;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4Status);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, wUpdateL3Filter.i4IssL3FilterNo);
        for (u4Count = ISS_ZERO_ENTRY; u4Count < ISS_FILTER_SHADOW_MEM_SIZE;
             u4Count++)
        {
            NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                                  wUpdateL3Filter.IssFilterShadowTable.
                                  au4HwHandle[u4Count]);
        }
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, wUpdateL3Filter.i4Value);

        if (AclPortRmEnqMsgToRm (pMsg, u2OffSet, u4AppId,
                                 u4AppId) == OSIX_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            AclPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name      : NpSyncIssHwSetRateLimitingValueSync             */
/*                                                                           */
/* Description        : This function forms the the NP Sync message and      */
/*            post the message to RM Module.                  */
/*                                                                           */
/* Input(s)           : Union to the NP Sync Entry, NPAPI ID, Application ID */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
VOID
NpSyncIssHwSetRateLimitingValueSync (tNpSyncIssHwSetRateLimitingValue
                                     wSetRateLimitingValue,
                                     UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = ISS_ZERO_ENTRY;
    UINT2               u2MsgSize = ISS_ZERO_ENTRY;

    MEMSET (&ProtoEvt, ISS_ZERO_ENTRY, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize = NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH +
        NPSYNC_MSG_NPAPI_ID_SIZE + sizeof (UINT4) + sizeof (UINT1) +
        sizeof (INT4);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        AclPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = ISS_ZERO_ENTRY;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, wSetRateLimitingValue.u4IfIndex);
        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet,
                              wSetRateLimitingValue.u1PacketType);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              wSetRateLimitingValue.i4RateLimitVal);
        if (AclPortRmEnqMsgToRm (pMsg, u2OffSet, u4AppId,
                                 u4AppId) == OSIX_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            AclPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name      : NpSyncIssHwSetPortEgressPktRateSync             */
/*                                                                           */
/* Description        : This function forms the the NP Sync message and      */
/*            post the message to RM Module.                  */
/*                                                                           */
/* Input(s)           : Union to the NP Sync Entry, NPAPI ID, Application ID */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
VOID
NpSyncIssHwSetPortEgressPktRateSync (tNpSyncIssHwSetPortEgressPktRate
                                     wSetPortEgressPktRate,
                                     UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = ISS_ZERO_ENTRY;
    UINT2               u2MsgSize = ISS_ZERO_ENTRY;

    MEMSET (&ProtoEvt, ISS_ZERO_ENTRY, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize = NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH +
        NPSYNC_MSG_NPAPI_ID_SIZE + sizeof (UINT4) + sizeof (INT4) +
        sizeof (INT4);

    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        AclPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = ISS_ZERO_ENTRY;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, wSetPortEgressPktRate.u4IfIndex);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, wSetPortEgressPktRate.i4PktRate);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              wSetPortEgressPktRate.i4BurstRate);

        if (AclPortRmEnqMsgToRm (pMsg, u2OffSet, u4AppId,
                                 u4AppId) == OSIX_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            AclPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name      : AclProcessNpSyncMsg                     */
/*                                                                           */
/* Description        : This function process the NP Sync message sent by the*/
/*            Peer node                          */
/*                                                                           */
/* Input(s)           : Pointer to RM message and Offset             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
VOID
AclProcessNpSyncMsg (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    INT4                i4NpApiId = ISS_ZERO_ENTRY;
    UINT4               u4Status = ISS_ZERO_ENTRY;
    UINT4               u4Count = ISS_ZERO_ENTRY;
    unNpSync            NpSync;

    MEMSET (&NpSync, ISS_ZERO_ENTRY, sizeof (unNpSync));
    NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet, i4NpApiId);

    switch (i4NpApiId)
    {
        case NPSYNC_ISS_HW_UPDATE_L2_FILTER:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet, u4Status);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.IssHwUpdateL2Filter.i4IssL2FilterNo);
            for (u4Count = ISS_ZERO_ENTRY; u4Count < ISS_FILTER_SHADOW_MEM_SIZE;
                 u4Count++)
            {
                NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                      NpSync.IssHwUpdateL2Filter.
                                      IssFilterShadowTable.
                                      au4HwHandle[u4Count]);
            }
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.IssHwUpdateL2Filter.i4Value);
            AclRedUpdateNpSyncBufferTable
                (&NpSync, NPSYNC_ISS_HW_UPDATE_L2_FILTER, u4Status);
            break;

        case NPSYNC_ISS_HW_UPDATE_L3_FILTER:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet, u4Status);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.IssHwUpdateL3Filter.i4IssL3FilterNo);
            for (u4Count = ISS_ZERO_ENTRY; u4Count < ISS_FILTER_SHADOW_MEM_SIZE;
                 u4Count++)
            {
                NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                      NpSync.IssHwUpdateL3Filter.
                                      IssFilterShadowTable.
                                      au4HwHandle[u4Count]);
            }
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.IssHwUpdateL3Filter.i4Value);
            AclRedUpdateNpSyncBufferTable
                (&NpSync, NPSYNC_ISS_HW_UPDATE_L3_FILTER, u4Status);
            break;

        case NPSYNC_ISS_HW_SET_RATE_LIMITING_VALUE:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.IssHwSetRateLimitingValue.u4IfIndex);
            NPSYNC_RM_GET_1_BYTE (pMsg, pu2OffSet,
                                  NpSync.IssHwSetRateLimitingValue.
                                  u1PacketType);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.IssHwSetRateLimitingValue.
                                  i4RateLimitVal);
            AclRedUpdateNpSyncBufferTable (&NpSync,
                                           NPSYNC_ISS_HW_SET_RATE_LIMITING_VALUE,
                                           ISS_ZERO_ENTRY);
            break;

        case NPSYNC_ISS_HW_SET_PORT_EGRESS_PKT_RATE:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.IssHwSetPortEgressPktRate.u4IfIndex);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.IssHwSetPortEgressPktRate.i4PktRate);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.IssHwSetPortEgressPktRate.i4BurstRate);
            AclRedUpdateNpSyncBufferTable
                (&NpSync, NPSYNC_ISS_HW_SET_PORT_EGRESS_PKT_RATE,
                 ISS_ZERO_ENTRY);
            break;

        default:
            break;
    }                            /* switch */

}
