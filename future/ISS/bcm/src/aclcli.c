    /* $Id: aclcli.c,v 1.125 2017/12/21 10:22:27 siva Exp $ */
/************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                 */
/* Licensee Aricent Inc., 2004-2005       */
/*                                                          */
/*  FILE NAME             : aclcli.c                        */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                 */
/*  SUBSYSTEM NAME        : ISS                             */
/*  MODULE NAME           : CLI                             */
/*  LANGUAGE              : C                               */
/*  TARGET ENVIRONMENT    :                                 */
/*  DATE OF FIRST RELEASE :                                 */
/*  AUTHOR                : Aricent Inc.                 */
/*  DESCRIPTION           : This file contains CLI routines */
/*                          related to system commands      */
/*                                                          */
/************************************************************/
#ifndef __ACLCLI_C__
#define __ACLCLI_C__

#include "lr.h"
#include "issexinc.h"
#include "fsissewr.h"
#include "fsisselw.h"
#include "fsissawr.h"
/* Since fsissaclcli.h uses the Wrapper routines of fsissacl mib, 
 * fsissawr.h is included here */
#include "fsissalw.h"
#include "aclcli.h"
#include "isscli.h"
#include "aclmcli.h"
#include "fsissacli.h"
#include "fsissewr.h"
#include "fssnmp.h"
#include "fsissmcli.h"
#include "fsissmwr.h"
/***************************************************************/
/*  Function Name   : cli_process_acl_cmd                      */
/*  Description     : This function servers as the handler for */
/*                    all system related CLI commands          */
/*  Input(s)        :                                          */
/*                    CliHandle - CLI Handle                   */
/*                    u4Command - Command given by user        */
/*                    Variable set of inputs depending on      */
/*                    the user command                         */
/*  Output(s)       : Error message - on failure               */
/*  Returns         : None                                     */
/***************************************************************/
INT4
cli_process_acl_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[CLI_ACL_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    UINT4               u4SrcPermit = 0;
    UINT4               u4DstPermit = 0;
    UINT4               u4SrcIpAddr = 0;
    UINT4               u4SrcIpMask = 0;
    UINT4               u4SrcPrefixLen = 0;
    UINT4               u4DstPrefixLen = 0;
    UINT4               u4DestIpAddr = 0;
    UINT4               u4DestIpMask = 0;
    UINT4               u4MaxSrcPort = 65535;
    UINT4               u4MinSrcPort = 1;
    UINT4               u4MinDstPort = 1;
    UINT4               u4MaxDstPort = 65535;
    UINT4               u4FlowId = ISS_FLOWID_INVALID;
    INT4                i4Tos = 0;
    UINT4               u4BitType = 0;
    INT4                i4Priority = 0;
    INT4                i4Action = 0;
    INT4                i4Protocol = ISS_ANY;
    INT4                i4MsgType = ISS_DEFAULT_MSG_TYPE;
    INT4                i4MsgCode = ISS_DEFAULT_MSG_CODE;
    INT4                i4FilterNo = 0;
    INT4                i4FilterType = 0;
    INT4                i4SVlan = 0;
    INT4                i4SVlanPrio = -1;
    INT4                i4CVlan = 0;
    INT4                i4CVlanPrio = -1;
    tMacAddr            SrcMacAddr;
    tMacAddr            DestMacAddr;
    tMacAddr            MacAddr;
    UINT4               u4IsValidSrcPort;
    UINT4               u4IsValidDestPort;
    UINT4               u4Protocol = 0;
    INT4                i4OuterEType = 0;
    INT4                i4EtherType = 0;
    INT4                i4TagType = 0;
    tIp6Addr            SrcIp6Addr;
    tIp6Addr            DstIp6Addr;
    INT4                i4Dscp = 0;
    INT4                i4FilterOper = 0;
    UINT4               u4Filter1Id = 0;
    UINT4               u4Filter2Id = 0;
    UINT4               u4Priority = 0;
    UINT1               u1ResrvFrameStatus = 0;
    UINT4               u4MacMask = 0xFFFFFFFF;
    UINT4               u4ControlAction = 0;
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry cli_process_acl_cmd  function \n");
    va_start (ap, u4Command);

    /* Third arguement is always interface name/index */
    va_arg (ap, UINT1 *);

    /* Walk through the rest of the arguements and store in args array.  */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == CLI_ACL_MAX_ARGS)
            break;
    }

    va_end (ap);

    CLI_SET_ERR (0);

    CliRegisterLock (CliHandle, IssLock, IssUnLock);

    ISS_LOCK ();

    switch (u4Command)
    {
        case CLI_IP_ACL:
            i4RetStatus =
                AclCreateIPFilter (CliHandle, CLI_PTR_TO_U4 (args[0]),
                                   CLI_PTR_TO_I4 (args[1]));
            break;

        case CLI_NO_IP_ACL:
            i4RetStatus = AclDestroyIPFilter (CliHandle,
                                              CLI_PTR_TO_I4 (args[1]));
            break;

        case CLI_MAC_ACL:
            i4RetStatus =
                AclCreateMacFilter (CliHandle, *(INT4 *) (VOID *) (args[0]));
            break;
        case CLI_USR_DEF_ACL:
            i4RetStatus =
                AclCreateUserDefFilter (CliHandle,
                                        *(INT4 *) (VOID *) (args[0]));
            break;

        case CLI_NO_MAC_ACL:
            i4RetStatus =
                AclDestroyMacFilter (CliHandle, *(INT4 *) (VOID *) (args[0]));
            break;
        case CLI_NO_USR_DEF_ACL:
            i4RetStatus =
                AclDestroyUserDefFilter (CliHandle,
                                         *(INT4 *) (VOID *) (args[0]));
            break;

        case CLI_ACL_PERMIT:
        case CLI_ACL_DENY:
        case CLI_ACL_COPYTOCPU:
            /* Get the source IP,source mask, destination IP and destination mask
             */

            if ((UINT4 *) (VOID *) (args[1]) != NULL)
            {
                u4SrcIpAddr = *(UINT4 *) (VOID *) (args[1]);
            }
            if ((UINT4 *) (VOID *) (args[2]) != NULL)
            {
                u4SrcIpMask = *(UINT4 *) (VOID *) (args[2]);
            }
            if ((UINT4 *) (VOID *) (args[4]) != NULL)
            {
                u4DestIpAddr = *(UINT4 *) (VOID *) (args[4]);
            }
            if ((UINT4 *) (VOID *) (args[5]) != NULL)
            {
                u4DestIpMask = *(UINT4 *) (VOID *) (args[5]);
            }
            if (u4Command == CLI_ACL_DENY)
            {
                i4Action = ISS_DROP;
                i4Priority = (*(UINT4 *) (VOID *) (args[6]));
            }
            /* Arguments for command copy-to-cpu */
            else if (u4Command == CLI_ACL_COPYTOCPU)
            {
                if (CLI_PTR_TO_I4 (args[6]) == ACL_NO_SWITCH)
                {
                    i4Action = ISS_DROP_COPYTOCPU;
                }
                else if (CLI_PTR_TO_I4 (args[6]) == ACL_SWITCH)
                {
                    i4Action = ISS_SWITCH_COPYTOCPU;
                }
                i4Priority = (*(UINT4 *) (VOID *) (args[7]));
            }

            else
            {
                if ((UINT4 *) (VOID *) (args[9]) != NULL)
                {
                    i4Priority = (*(UINT4 *) (VOID *) (args[9]));
                }
                if (CLI_PTR_TO_I4 (args[6]) == ISS_REDIRECT_TO
                    && u4Command == CLI_ACL_PERMIT)
                {
                    i4Action = ISS_REDIRECT_TO;
                    i4FilterType = ISS_L3_REDIRECT;
                }
                else
                {
                    i4Action = ISS_ALLOW;
                }
            }

            i4RetStatus =
                AclStdIpFilterConfig (CliHandle, i4Action,
                                      CLI_PTR_TO_U4 (args[0]),
                                      u4SrcIpAddr, u4SrcIpMask,
                                      CLI_PTR_TO_U4 (args[3]), u4DestIpAddr,
                                      u4DestIpMask, i4Priority);
            if (i4Action == ISS_REDIRECT_TO)
            {
                AclRedirectFilterConfig (CliHandle, i4FilterType,
                                         ISS_REDIRECT_TO_PORT,
                                         (INT1 *) args[7], (INT1 *) args[8]);
            }
            break;
        case CLI_USR_DEFANDORNOT_FILTER:
            if (args[0] != NULL)
            {
                i4FilterOper = CLI_PTR_TO_I4 (args[0]);
            }
            if (args[1] != NULL)
            {
                u4Filter1Id = *(UINT4 *) (VOID *) args[1];
            }
            if (args[2] != NULL)
            {
                u4Filter2Id = *(UINT4 *) (VOID *) args[2];
            }
            if (args[3] != NULL)
            {
                u4Priority = *(UINT4 *) (VOID *) args[3];
            }
            i4RetStatus = AclUserDefinedOperations (CliHandle, i4FilterOper,
                                                    u4Filter1Id, u4Filter2Id,
                                                    u4Priority);
            break;
        case CLI_ACL_PERMIT_PROTO:
        case CLI_ACL_DENY_PROTO:
        case CLI_ACL_COPYTOCPU_PROTO:
        case CLI_ACL_PB_PERMIT_PROTO:
        case CLI_ACL_PB_DENY_PROTO:

            /* Get the source IP,source mask, destination IP and destination mask
             */
            if ((UINT4 *) (VOID *) (args[2]) != NULL)
            {
                u4SrcIpAddr = *(UINT4 *) (VOID *) (args[2]);
            }
            if ((UINT4 *) (VOID *) (args[3]) != NULL)
            {
                u4SrcIpMask = *(UINT4 *) (VOID *) (args[3]);
            }

            if ((UINT4 *) (VOID *) (args[5]) != NULL)
            {
                u4DestIpAddr = *(UINT4 *) (VOID *) (args[5]);
            }
            if ((UINT4 *) (VOID *) (args[6]) != NULL)
            {
                u4DestIpMask = *(UINT4 *) (VOID *) (args[6]);
            }

            if ((UINT4 *) (VOID *) (args[9]) != NULL)
            {
                i4Priority = (*(UINT4 *) (VOID *) (args[9]));
            }
            else
            {
                i4Priority = ISS_DEFAULT_FILTER_PRIORITY;
            }

            if ((u4Command == CLI_ACL_PB_PERMIT_PROTO) ||
                (u4Command == CLI_ACL_PB_DENY_PROTO))
            {
                if ((INT4 *) (VOID *) (args[10]) != NULL)
                {
                    i4SVlan = *(INT4 *) (VOID *) (args[10]);
                }
                if ((INT4 *) (VOID *) (args[11]) != NULL)
                {
                    i4SVlanPrio = *(INT4 *) (VOID *) (args[11]);
                }

                if ((INT4 *) (VOID *) (args[12]) != NULL)
                {
                    i4CVlan = *(INT4 *) (VOID *) (args[12]);
                }
                if ((INT4 *) (VOID *) (args[13]) != NULL)
                {
                    i4CVlanPrio = *(INT4 *) (VOID *) (args[13]);
                }
                i4TagType = CLI_PTR_TO_I4 (args[14]);
            }

            if ((u4Command == CLI_ACL_DENY_PROTO) ||
                (u4Command == CLI_ACL_PB_DENY_PROTO))
            {
                i4Action = ISS_DROP;
            }
            else
            {
                if ((CLI_PTR_TO_I4 (args[10]) == ISS_REDIRECT_TO
                     && u4Command == CLI_ACL_PERMIT_PROTO)
                    || (CLI_PTR_TO_I4 (args[15]) == ISS_REDIRECT_TO
                        && u4Command == CLI_ACL_PB_PERMIT_PROTO))
                {
                    i4Action = ISS_REDIRECT_TO;
                    i4FilterType = ISS_L3_REDIRECT;
                }
                else if (u4Command == CLI_ACL_PERMIT_PROTO
                         || u4Command == CLI_ACL_PB_PERMIT_PROTO)
                {
                    i4Action = ISS_ALLOW;
                }
            }

            /* Arguments processing for copy-to-cpu */
            if (u4Command == CLI_ACL_COPYTOCPU_PROTO)
            {
                if (CLI_PTR_TO_I4 (args[10]) == ACL_NO_SWITCH)
                {
                    i4Action = ISS_DROP_COPYTOCPU;
                }
                else
                {
                    i4Action = ISS_SWITCH_COPYTOCPU;
                }
            }

            i4RetStatus =
                AclExtIpFilterConfig
                (CliHandle, i4Action, CLI_PTR_TO_I4 (args[0]),
                 CLI_PTR_TO_U4 (args[1]),
                 u4SrcIpAddr, u4SrcIpMask, CLI_PTR_TO_U4 (args[4]),
                 u4DestIpAddr, u4DestIpMask, CLI_PTR_TO_I4 (args[7]),
                 CLI_PTR_TO_I4 (args[8]), i4Priority);

            if ((i4RetStatus == CLI_SUCCESS) &&
                ((u4Command == CLI_ACL_PB_PERMIT_PROTO) ||
                 (u4Command == CLI_ACL_PB_DENY_PROTO)))
            {
                i4RetStatus =
                    AclExtPbL3FilterConfig (CliHandle, i4SVlan,
                                            i4SVlanPrio, i4CVlan,
                                            i4CVlanPrio, i4TagType);
            }
            if ((i4Action == ISS_REDIRECT_TO)
                && (u4Command == CLI_ACL_PERMIT_PROTO))
            {
                AclRedirectFilterConfig (CliHandle, i4FilterType,
                                         ISS_REDIRECT_TO_PORT,
                                         (INT1 *) args[11], (INT1 *) args[12]);
            }
            else if ((i4Action == ISS_REDIRECT_TO)
                     && (u4Command == CLI_ACL_PB_PERMIT_PROTO))
            {
                AclRedirectFilterConfig (CliHandle, i4FilterType,
                                         ISS_REDIRECT_TO_PORT,
                                         (INT1 *) args[16], (INT1 *) args[17]);
            }

            break;

        case CLI_ACL_PERMIT_IPV6_PROTO:
        case CLI_ACL_DENY_IPV6_PROTO:
        case CLI_ACL_COPYTOCPU_IPV6_PROTO:

            u4SrcPermit = CLI_PTR_TO_U4 (args[0]);
            if (u4SrcPermit == ACL_ANY)
            {
                MEMSET (&SrcIp6Addr, 0, sizeof (tIp6Addr));
            }
            if ((UINT4 *) (VOID *) (args[1]) != NULL)
            {
                MEMSET (&SrcIp6Addr, 0, sizeof (tIp6Addr));
                INET_ATON6 (args[1], &SrcIp6Addr);
            }
            if ((UINT4 *) (VOID *) (args[2]) != NULL)
            {
                u4SrcPrefixLen = *(UINT4 *) (VOID *) (args[2]);
            }
            u4DstPermit = CLI_PTR_TO_U4 (args[3]);
            if (u4DstPermit == ACL_ANY)
            {
                MEMSET (&DstIp6Addr, 0, sizeof (tIp6Addr));
            }
            if ((UINT4 *) (VOID *) (args[4]) != NULL)
            {
                MEMSET (&DstIp6Addr, 0, sizeof (tIp6Addr));
                INET_ATON6 (args[4], &DstIp6Addr);
            }
            if ((UINT4 *) (VOID *) (args[5]) != NULL)
            {
                u4DstPrefixLen = *(UINT4 *) (VOID *) (args[5]);
            }
            if (((UINT4 *) (VOID *) (args[6]) != NULL) &&
                CLI_PTR_TO_U4 ((UINT4 *) (VOID *) (args[6])) !=
                ISS_FLOWID_INVALID)
            {
                u4FlowId = *(UINT4 *) (VOID *) (args[6]);
            }

            if (u4Command == CLI_ACL_PERMIT_IPV6_PROTO)
            {
                i4Priority = *(INT4 *) (VOID *) (args[10]);
                if (CLI_PTR_TO_I4 (args[7]) == ISS_REDIRECT_TO)
                {
                    i4Action = ISS_REDIRECT_TO;
                    i4FilterType = ISS_L3_REDIRECT;
                }
                else
                {
                    i4Action = ISS_ALLOW;
                }
            }
            else if (u4Command == CLI_ACL_DENY_IPV6_PROTO)
            {
                i4Action = ISS_DROP;
                i4Priority = *(INT4 *) (VOID *) (args[7]);
            }

            if (u4Command == CLI_ACL_COPYTOCPU_IPV6_PROTO)
            {
                i4Priority = *(INT4 *) (VOID *) (args[8]);
                if (CLI_PTR_TO_I4 (args[7]) == ACL_NO_SWITCH)
                {
                    i4Action = ISS_DROP_COPYTOCPU;
                }
                else
                {
                    i4Action = ISS_SWITCH_COPYTOCPU;
                }
            }

            i4RetStatus =
                AclExtIp6FilterConfig (CliHandle, u4SrcPermit, SrcIp6Addr,
                                       u4SrcPrefixLen, u4DstPermit, DstIp6Addr,
                                       u4DstPrefixLen, u4FlowId, i4Action,
                                       i4Priority);
            if (i4Action == ISS_REDIRECT_TO)
            {
                AclRedirectFilterConfig (CliHandle, i4FilterType,
                                         ISS_REDIRECT_TO_PORT,
                                         (INT1 *) args[8], (INT1 *) args[9]);
            }
            break;

        case CLI_ACL_PERMIT_TCP:
        case CLI_ACL_DENY_TCP:
        case CLI_ACL_COPYTOCPU_TCP:
        case CLI_ACL_PERMIT_UDP:
        case CLI_ACL_DENY_UDP:
        case CLI_ACL_COPYTOCPU_UDP:
        case CLI_ACL_PB_PERMIT_TCP:
        case CLI_ACL_PB_DENY_TCP:
        case CLI_ACL_PB_PERMIT_UDP:
        case CLI_ACL_PB_DENY_UDP:

            if ((u4Command == CLI_ACL_DENY_TCP) ||
                (u4Command == CLI_ACL_DENY_UDP) ||
                (u4Command == CLI_ACL_PB_DENY_TCP) ||
                (u4Command == CLI_ACL_PB_DENY_UDP))
            {
                i4Action = ISS_DROP;
            }

            else if ((u4Command == CLI_ACL_COPYTOCPU_TCP) ||
                     (u4Command == CLI_ACL_COPYTOCPU_UDP))
            {
                if (CLI_PTR_TO_I4 (args[16]) == ACL_NO_SWITCH)
                {
                    i4Action = ISS_DROP_COPYTOCPU;
                }
                else
                {
                    i4Action = ISS_SWITCH_COPYTOCPU;
                }

            }

            else
            {
                if ((CLI_PTR_TO_I4 (args[21]) == ISS_REDIRECT_TO)
                    || (CLI_PTR_TO_I4 (args[16]) == ISS_REDIRECT_TO))
                {
                    i4Action = ISS_REDIRECT_TO;
                    i4FilterType = ISS_L3_REDIRECT;
                }
                else
                {
                    i4Action = ISS_ALLOW;
                }
            }
            /* Get the protocol type - This can be TCP or UDP only */

            if ((u4Command == CLI_ACL_PERMIT_TCP) ||
                (u4Command == CLI_ACL_DENY_TCP) ||
                (u4Command == CLI_ACL_PB_PERMIT_TCP) ||
                (u4Command == CLI_ACL_PB_DENY_TCP) ||
                (u4Command == CLI_ACL_COPYTOCPU_TCP))
            {
                i4Protocol = ISS_PROT_TCP;
            }
            else
            {
                i4Protocol = ISS_PROT_UDP;
            }

            /* Get the source IP,source mask, destination IP and destination mask
             */

            if ((UINT4 *) (VOID *) (args[1]) != NULL)
            {
                u4SrcIpAddr = *(UINT4 *) (VOID *) (args[1]);
            }
            if ((UINT4 *) (VOID *) (args[2]) != NULL)
            {
                u4SrcIpMask = *(UINT4 *) (VOID *) (args[2]);
            }

            if ((UINT4 *) (VOID *) (args[7]) != NULL)
            {
                u4DestIpAddr = *(UINT4 *) (VOID *) (args[7]);
            }
            if ((UINT4 *) (VOID *) (args[8]) != NULL)
            {
                u4DestIpMask = *(UINT4 *) (VOID *) (args[8]);
            }

            u4IsValidSrcPort = FALSE;
            /* Processing for Source ports */

            if (CLI_PTR_TO_U4 (args[3]) == ACL_GREATER_THAN_PORT)
            {
                u4MinSrcPort = (*(UINT4 *) (VOID *) (args[4])) + 1;
                u4MaxSrcPort = ISS_MAX_PORT_VALUE;
                u4IsValidSrcPort = TRUE;
            }
            else if (CLI_PTR_TO_U4 (args[3]) == ACL_LESSER_THAN_PORT)
            {
                u4MinSrcPort = 1;
                u4MaxSrcPort = (*(UINT4 *) (VOID *) (args[4])) - 1;
                u4IsValidSrcPort = TRUE;
            }
            else if (CLI_PTR_TO_U4 (args[3]) == ACL_EQUAL_TO_PORT)
            {
                u4MinSrcPort = *(UINT4 *) (VOID *) (args[4]);
                u4MaxSrcPort = *(UINT4 *) (VOID *) (args[4]);
                u4IsValidSrcPort = TRUE;
            }
            else if (CLI_PTR_TO_U4 (args[3]) == ACL_RANGE_PORT)
            {
                u4MinSrcPort = *(UINT4 *) (VOID *) (args[4]);
                u4MaxSrcPort = *(UINT4 *) (VOID *) (args[5]);
                u4IsValidSrcPort = TRUE;
            }

            /* Processing for destination ports */

            u4IsValidDestPort = FALSE;

            if (CLI_PTR_TO_U4 (args[9]) == ACL_GREATER_THAN_PORT)
            {
                u4MinDstPort = (*(UINT4 *) (VOID *) (args[10])) + 1;
                u4MaxDstPort = ISS_MAX_PORT_VALUE;
                u4IsValidDestPort = TRUE;
            }
            else if (CLI_PTR_TO_U4 (args[9]) == ACL_LESSER_THAN_PORT)
            {
                u4MinDstPort = 1;
                u4MaxDstPort = (*(UINT4 *) (VOID *) (args[10])) - 1;
                u4IsValidDestPort = TRUE;
            }
            else if (CLI_PTR_TO_U4 (args[9]) == ACL_EQUAL_TO_PORT)
            {
                u4MinDstPort = *(UINT4 *) (VOID *) (args[10]);
                u4MaxDstPort = *(UINT4 *) (VOID *) (args[10]);
                u4IsValidDestPort = TRUE;
            }
            else if (CLI_PTR_TO_U4 (args[9]) == ACL_RANGE_PORT)
            {
                u4MinDstPort = *(UINT4 *) (VOID *) (args[10]);
                u4MaxDstPort = *(UINT4 *) (VOID *) (args[11]);
                u4IsValidDestPort = TRUE;
            }

            /* Processing for RST/ACK BIT */

            if ((UINT4 *) (VOID *) (args[12]) != NULL)
            {
                u4BitType = CLI_PTR_TO_U4 (args[12]);
            }

            i4Tos = CLI_PTR_TO_I4 (args[13]);

            if ((UINT4 *) (VOID *) (args[15]) != NULL)
            {
                i4Priority = *(INT4 *) (VOID *) (args[15]);
            }
            else
            {
                i4Priority = ISS_DEFAULT_FILTER_PRIORITY;
            }

            if ((u4Command == CLI_ACL_PB_PERMIT_TCP) ||
                (u4Command == CLI_ACL_PB_DENY_TCP) ||
                (u4Command == CLI_ACL_PB_PERMIT_UDP) ||
                (u4Command == CLI_ACL_PB_DENY_UDP))
            {
                if ((INT4 *) (VOID *) (args[16]) != NULL)
                {
                    i4SVlan = *(INT4 *) (VOID *) (args[16]);
                }
                if ((INT4 *) (VOID *) (args[17]) != NULL)
                {
                    i4SVlanPrio = *(INT4 *) (VOID *) (args[17]);
                }

                if ((INT4 *) (VOID *) (args[18]) != NULL)
                {
                    i4CVlan = *(INT4 *) (VOID *) (args[18]);
                }
                if ((INT4 *) (VOID *) (args[19]) != NULL)
                {
                    i4CVlanPrio = *(INT4 *) (VOID *) (args[19]);
                }
                i4TagType = CLI_PTR_TO_I4 (args[20]);
            }

            /* Copy to CPU commands */
            if ((u4Command == CLI_ACL_COPYTOCPU_TCP) ||
                (u4Command == CLI_ACL_COPYTOCPU_UDP))
            {
                if (CLI_PTR_TO_I4 (args[16]) == ACL_NO_SWITCH)
                {
                    i4Action = ISS_DROP_COPYTOCPU;
                }
                else
                {
                    i4Action = ISS_SWITCH_COPYTOCPU;
                }
            }

            i4RetStatus = AclExtIpFilterTcpUdpConfig
                (CliHandle, i4Action, i4Protocol,
                 CLI_PTR_TO_U4 (args[0]), u4SrcIpAddr,
                 u4SrcIpMask, u4IsValidSrcPort,
                 u4MinSrcPort, u4MaxSrcPort,
                 CLI_PTR_TO_U4 (args[6]), u4DestIpAddr,
                 u4DestIpMask, u4IsValidDestPort,
                 u4MinDstPort, u4MaxDstPort, u4BitType, i4Tos,
                 CLI_PTR_TO_I4 (args[14]), i4Priority);
            if ((i4RetStatus == CLI_SUCCESS) &&
                ((u4Command == CLI_ACL_PB_PERMIT_TCP) ||
                 (u4Command == CLI_ACL_PB_DENY_TCP) ||
                 (u4Command == CLI_ACL_PB_PERMIT_UDP) ||
                 (u4Command == CLI_ACL_PB_DENY_UDP)))
            {
                i4RetStatus =
                    AclExtPbL3FilterConfig (CliHandle, i4SVlan,
                                            i4SVlanPrio, i4CVlan,
                                            i4CVlanPrio, i4TagType);

            }
            if (i4Action == ISS_REDIRECT_TO)
            {
                if ((u4Command == CLI_ACL_PERMIT_TCP) ||
                    (u4Command == CLI_ACL_PERMIT_UDP))
                {
                    AclRedirectFilterConfig (CliHandle, i4FilterType,
                                             ISS_REDIRECT_TO_PORT,
                                             (INT1 *) args[17],
                                             (INT1 *) args[18]);
                }
                else if ((u4Command == CLI_ACL_PB_PERMIT_TCP) ||
                         (u4Command == CLI_ACL_PB_PERMIT_UDP))
                {
                    AclRedirectFilterConfig (CliHandle, i4FilterType,
                                             ISS_REDIRECT_TO_PORT,
                                             (INT1 *) args[22],
                                             (INT1 *) args[23]);
                }
            }
            break;

        case CLI_ACL_PERMIT_ICMP:
        case CLI_ACL_DENY_ICMP:
        case CLI_ACL_COPYTOCPU_ICMP:
        case CLI_ACL_PB_PERMIT_ICMP:
        case CLI_ACL_PB_DENY_ICMP:

            /* Get the source IP,source mask, destination IP and destination mask
             */
            if ((UINT4 *) (VOID *) (args[1]) != NULL)
            {
                u4SrcIpAddr = *(UINT4 *) (VOID *) (args[1]);
            }
            if ((UINT4 *) (VOID *) (args[2]) != NULL)
            {
                u4SrcIpMask = *(UINT4 *) (VOID *) (args[2]);
            }

            if ((UINT4 *) (VOID *) (args[4]) != NULL)
            {
                u4DestIpAddr = *(UINT4 *) (VOID *) (args[4]);
            }
            if ((UINT4 *) (VOID *) (args[5]) != NULL)
            {
                u4DestIpMask = *(UINT4 *) (VOID *) (args[5]);
            }

            if ((u4Command == CLI_ACL_DENY_ICMP) ||
                (u4Command == CLI_ACL_PB_DENY_ICMP))
            {
                i4Action = ISS_DROP;
            }

            else if (u4Command == CLI_ACL_COPYTOCPU_ICMP)
            {

                if (CLI_PTR_TO_I4 (args[9]) == ACL_NO_SWITCH)
                {
                    i4Action = ISS_DROP_COPYTOCPU;
                }

                else
                {
                    i4Action = ISS_SWITCH_COPYTOCPU;
                }
            }

            else
            {
                if (((CLI_PTR_TO_I4 (args[14]) == ISS_REDIRECT_TO)
                     && (u4Command == CLI_ACL_PB_PERMIT_ICMP))
                    || ((CLI_PTR_TO_I4 (args[9]) == ISS_REDIRECT_TO)
                        && (u4Command == CLI_ACL_PERMIT_ICMP)))
                {
                    i4Action = ISS_REDIRECT_TO;
                    i4FilterType = ISS_L3_REDIRECT;
                }
                else
                {
                    i4Action = ISS_ALLOW;
                }

            }

            /* Get the ICMP Message Type and code */

            if (((INT4 *) (VOID *) (args[6]) != NULL) &&
                CLI_PTR_TO_I4 ((INT4 *) (VOID *) (args[6])) !=
                ISS_DEFAULT_MSG_TYPE)
            {
                i4MsgType = *(INT4 *) (VOID *) (args[6]);
            }
            if ((UINT4 *) (VOID *) (args[7]) != NULL)
            {
                i4MsgCode = *(UINT4 *) (VOID *) (args[7]);
            }

            if ((INT4 *) (VOID *) (args[8]) != NULL)
            {
                i4Priority = *(INT4 *) (VOID *) (args[8]);
            }
            else
            {
                i4Priority = ISS_DEFAULT_FILTER_PRIORITY;
            }

            if ((u4Command == CLI_ACL_PB_PERMIT_ICMP) ||
                (u4Command == CLI_ACL_PB_DENY_ICMP))
            {
                if ((INT4 *) (VOID *) (args[9]) != NULL)
                {
                    i4SVlan = *(INT4 *) (VOID *) (args[9]);
                }
                if ((INT4 *) (VOID *) (args[10]) != NULL)
                {
                    i4SVlanPrio = *(INT4 *) (VOID *) (args[10]);
                }

                if ((INT4 *) (VOID *) (args[11]) != NULL)
                {
                    i4CVlan = *(INT4 *) (VOID *) (args[11]);
                }
                if ((INT4 *) (VOID *) (args[12]) != NULL)
                {
                    i4CVlanPrio = *(INT4 *) (VOID *) (args[12]);
                }
                i4TagType = CLI_PTR_TO_I4 (args[13]);
            }

            if (u4Command == CLI_ACL_COPYTOCPU_ICMP)
            {
                if (CLI_PTR_TO_I4 (args[9]) == ACL_NO_SWITCH)
                {
                    i4Action = ISS_DROP_COPYTOCPU;
                }
                else
                {
                    i4Action = ISS_SWITCH_COPYTOCPU;
                }
            }

            i4RetStatus =
                AclExtIpFilterIcmpConfig
                (CliHandle, i4Action,
                 CLI_PTR_TO_U4 (args[0]), u4SrcIpAddr, u4SrcIpMask,
                 CLI_PTR_TO_U4 (args[3]), u4DestIpAddr, u4DestIpMask,
                 i4MsgType, i4MsgCode, i4Priority);

            if ((i4RetStatus == CLI_SUCCESS) &&
                ((u4Command == CLI_ACL_PB_PERMIT_ICMP) ||
                 (u4Command == CLI_ACL_PB_DENY_ICMP)))
            {
                i4RetStatus =
                    AclExtPbL3FilterConfig (CliHandle, i4SVlan,
                                            i4SVlanPrio, i4CVlan,
                                            i4CVlanPrio, i4TagType);
            }
            if (i4Action == ISS_REDIRECT_TO)
            {
                if (u4Command == CLI_ACL_PERMIT_ICMP)
                {
                    AclRedirectFilterConfig (CliHandle, i4FilterType,
                                             ISS_REDIRECT_TO_PORT,
                                             (INT1 *) args[10],
                                             (INT1 *) args[11]);
                }
                else if (u4Command == CLI_ACL_PB_PERMIT_ICMP)
                {
                    AclRedirectFilterConfig (CliHandle, i4FilterType,
                                             ISS_REDIRECT_TO_PORT,
                                             (INT1 *) args[15],
                                             (INT1 *) args[16]);
                }
            }
            break;

        case CLI_COPYTOCPU_MAC_ACL:
        case CLI_PERMIT_MAC_ACL:
        case CLI_DENY_MAC_ACL:
        case CLI_PB_PERMIT_MAC_ACL:
        case CLI_PB_DENY_MAC_ACL:

            if (((u4Command == CLI_PERMIT_MAC_ACL)
                 && (CLI_PTR_TO_I4 (args[8]) == ISS_REDIRECT_TO))
                || ((u4Command == CLI_PB_PERMIT_MAC_ACL)
                    && (CLI_PTR_TO_I4 (args[13]) == ISS_REDIRECT_TO)))
            {
                i4Action = ISS_REDIRECT_TO;
                i4FilterType = ISS_L2_REDIRECT;
            }
            else if ((u4Command == CLI_PERMIT_MAC_ACL) ||
                     (u4Command == CLI_PB_PERMIT_MAC_ACL))
            {
                i4Action = ISS_ALLOW;
            }

            else if (u4Command == CLI_COPYTOCPU_MAC_ACL)
            {

                if (CLI_PTR_TO_I4 (args[8]) == ACL_NO_SWITCH)
                {
                    i4Action = ISS_DROP_COPYTOCPU;
                }
                else
                {
                    i4Action = ISS_SWITCH_COPYTOCPU;
                }

            }
            else
            {
                i4Action = ISS_DROP;
            }

            /* Get the Source and destination MAC address */

            if ((UINT1 *) (args[1]) != NULL)
            {
                StrToMac (args[1], SrcMacAddr);
            }
            if ((UINT1 *) (args[3]) != NULL)
            {
                StrToMac (args[3], DestMacAddr);
            }

            /* Get the Ether Type and filter priority */

            if ((INT4 *) (VOID *) (args[5]) != NULL)
            {
                i4EtherType = *(INT4 *) (VOID *) (args[5]);
            }
            if ((INT4 *) (VOID *) (args[6]) != NULL)
            {
                i4CVlan = CLI_PTR_TO_I4 (args[6]);
            }
            if ((INT4 *) (VOID *) (args[7]) != NULL)
            {
                i4Priority = *(INT4 *) (VOID *) (args[7]);
            }
            else
            {
                i4Priority = ISS_DEFAULT_FILTER_PRIORITY;
            }

            if ((u4Command == CLI_PB_PERMIT_MAC_ACL) ||
                (u4Command == CLI_PB_DENY_MAC_ACL))
            {
                if ((INT4 *) (VOID *) (args[8]) != NULL)
                {
                    i4OuterEType = *(INT4 *) (VOID *) (args[8]);
                }
                if ((INT4 *) (VOID *) (args[9]) != NULL)
                {
                    i4SVlan = *(INT4 *) (VOID *) (args[9]);
                }
                if ((INT4 *) (VOID *) (args[10]) != NULL)
                {
                    i4CVlanPrio = *(INT4 *) (VOID *) (args[10]);
                }

                if ((INT4 *) (VOID *) (args[11]) != NULL)
                {
                    i4SVlanPrio = *(INT4 *) (VOID *) (args[11]);
                }
                i4TagType = CLI_PTR_TO_I4 (args[12]);
            }
            i4RetStatus =
                AclExtMacFilterConfig (CliHandle, i4Action,
                                       CLI_PTR_TO_U4 (args[0]),
                                       SrcMacAddr, CLI_PTR_TO_U4 (args[2]),
                                       DestMacAddr, CLI_PTR_TO_I4 (args[4]),
                                       i4EtherType, i4CVlan, i4Priority);

            if ((i4RetStatus == CLI_SUCCESS) &&
                ((u4Command == CLI_PB_PERMIT_MAC_ACL) ||
                 (u4Command == CLI_PB_DENY_MAC_ACL)))
            {
                i4RetStatus =
                    AclExtPbL2FilterConfig (CliHandle, i4OuterEType,
                                            i4SVlan, i4SVlanPrio,
                                            i4CVlanPrio, i4TagType);

            }
            if ((i4Action == ISS_REDIRECT_TO)
                && (u4Command == CLI_PERMIT_MAC_ACL))
            {
                AclRedirectFilterConfig (CliHandle, i4FilterType,
                                         ISS_REDIRECT_TO_PORT,
                                         (INT1 *) args[9], (INT1 *) args[10]);
            }
            else if ((i4Action == ISS_REDIRECT_TO)
                     && (u4Command == CLI_PB_PERMIT_MAC_ACL))
            {
                AclRedirectFilterConfig (CliHandle, i4FilterType,
                                         ISS_REDIRECT_TO_PORT,
                                         (INT1 *) args[14], (INT1 *) args[15]);
            }
            break;

        case CLI_ACL_IP_ACCESS_GRP:

            i4RetStatus =
                AclIpAccessGroup (CliHandle, *(INT4 *) (VOID *) (args[0]),
                                  CLI_PTR_TO_I4 (args[1]),
                                  CLI_PTR_TO_I4 (args[2]));
            break;

        case CLI_ACL_IP_NO_ACCESS_GRP:

            if ((INT4 *) (VOID *) (args[0]) != NULL)
            {
                i4FilterNo = (*(INT4 *) (VOID *) (args[0]));
            }
            i4RetStatus =
                AclNoIpAccessGroup (CliHandle, i4FilterNo,
                                    CLI_PTR_TO_I4 (args[1]),
                                    CLI_PTR_TO_I4 (args[2]), 0);
            break;

        case CLI_ACL_MAC_ACCESS_GRP:

            i4RetStatus =
                AclMacExtAccessGroup (CliHandle, *(INT4 *) (VOID *) (args[0]),
                                      CLI_PTR_TO_I4 (args[1]),
                                      CLI_PTR_TO_I4 (args[2]));
            break;

        case CLI_ACL_MAC_NO_ACCESS_GRP:

            if ((INT4 *) (VOID *) (args[0]) != NULL)
            {
                i4FilterNo = (*(INT4 *) (VOID *) (args[0]));
            }
            i4RetStatus =
                AclNoMacExtAccessGroup (CliHandle, i4FilterNo,
                                        CLI_PTR_TO_I4 (args[1]),
                                        CLI_PTR_TO_I4 (args[2]), 0);
            break;

        case CLI_ACL_UDB_ACCESS_GRP:

            i4RetStatus =
                AclUserDefinedAccessGroup (CliHandle,
                                           *(INT4 *) (VOID *) (args[0]),
                                           CLI_PTR_TO_I4 (args[1]),
                                           CLI_PTR_TO_I4 (args[2]));
            break;

        case CLI_ACL_UDB_NO_ACCESS_GRP:

            if ((INT4 *) (VOID *) (args[0]) != NULL)
            {
                i4FilterNo = (*(INT4 *) (VOID *) (args[0]));
            }
            i4RetStatus =
                AclNoUserDefinedAccessGroup (CliHandle, i4FilterNo,
                                             ACL_ACCESS_IN,
                                             CLI_PTR_TO_I4 (args[2]));
            break;

        case CLI_ACL_SHOW:
            i4FilterType = CLI_PTR_TO_I4 (args[0]);
            if ((INT4 *) (VOID *) (args[1]) != NULL)
            {
                i4FilterNo = (*(INT4 *) (VOID *) (args[1]));
            }
            i4RetStatus =
                AclShowAccessLists (CliHandle, i4FilterType, i4FilterNo);
            break;

        case CLI_ACL_PERMIT_IPV6_ICMP:
        case CLI_ACL_DENY_IPV6_ICMP:
        case CLI_ACL_COPYTOCPU_IPV6_ICMP:

            u4SrcPermit = CLI_PTR_TO_U4 (args[0]);
            if (u4SrcPermit == ACL_ANY)
            {
                MEMSET (&SrcIp6Addr, 0, sizeof (tIp6Addr));
            }
            if (args[1] != NULL)
            {
                MEMSET (&SrcIp6Addr, 0, sizeof (tIp6Addr));
                INET_ATON6 (args[1], &SrcIp6Addr);
            }
            if (args[2] != NULL)
            {
                u4SrcPrefixLen = CLI_PTR_TO_U4 (args[2]);
            }
            u4DstPermit = CLI_PTR_TO_U4 (args[3]);
            if (u4DstPermit == ACL_ANY)
            {
                MEMSET (&DstIp6Addr, 0, sizeof (tIp6Addr));
            }
            if (args[4] != NULL)
            {
                MEMSET (&DstIp6Addr, 0, sizeof (tIp6Addr));
                INET_ATON6 (args[4], &DstIp6Addr);
            }
            if (args[5] != NULL)
            {
                u4DstPrefixLen = CLI_PTR_TO_U4 (args[5]);
            }

            /* Get the ICMP Message Type and code */
            if (args[6] != NULL)
            {
                i4MsgType = CLI_PTR_TO_I4 (args[6]);
            }
            if (args[7] != NULL)
            {
                i4MsgCode = CLI_PTR_TO_I4 (args[7]);
            }

            if (args[8] != NULL)
            {
                i4Dscp = CLI_PTR_TO_I4 (args[8]);
            }

            if (((UINT4 *) (VOID *) (args[6]) != NULL) &&
                CLI_PTR_TO_U4 (args[9]) != ISS_FLOWID_INVALID)
            {
                u4FlowId = CLI_PTR_TO_U4 (args[9]);
            }

            if (args[10] != NULL)
            {
                i4Priority = CLI_PTR_TO_I4 (args[10]);
            }
            else
            {
                i4Priority = ISS_DEFAULT_FILTER_PRIORITY;
            }

            if (u4Command == CLI_ACL_DENY_IPV6_ICMP)
            {
                i4Action = ISS_DROP;
            }
            else if (u4Command == CLI_ACL_COPYTOCPU_IPV6_ICMP)
            {
                if (CLI_PTR_TO_I4 (args[11]) == ACL_NO_SWITCH)
                {
                    i4Action = ISS_DROP_COPYTOCPU;
                }
                else
                {
                    i4Action = ISS_SWITCH_COPYTOCPU;
                }
            }
            else
            {
                if (CLI_PTR_TO_I4 (args[11]) == ISS_REDIRECT_TO)
                {
                    i4Action = ISS_REDIRECT_TO;
                    i4FilterType = ISS_L3_REDIRECT;
                }
                else
                {
                    i4Action = ISS_ALLOW;
                }
            }

            i4RetStatus =
                AclExtIp6FilterIcmpConfig (CliHandle,
                                           u4SrcPermit, SrcIp6Addr,
                                           u4SrcPrefixLen, u4DstPermit,
                                           DstIp6Addr, u4DstPrefixLen,
                                           i4MsgType, i4MsgCode, i4Dscp,
                                           u4FlowId, i4Priority, i4Action);
            if (i4Action == ISS_REDIRECT_TO)
            {
                AclRedirectFilterConfig (CliHandle, i4FilterType,
                                         ISS_REDIRECT_TO_PORT,
                                         (INT1 *) args[12], (INT1 *) args[13]);
            }

            break;

        case CLI_CLR_MAC_ACL_COUNT:
            if ((INT4 *) (VOID *) (args[0]) != NULL)
            {
                i4FilterNo = (*(INT4 *) (VOID *) (args[0]));
            }
            i4RetStatus = AclMacStatsClear (CliHandle, i4FilterNo);
            break;

        case CLI_CLR_IP_ACL_COUNT:
            if ((INT4 *) (VOID *) (args[0]) != NULL)
            {
                i4FilterNo = (*(INT4 *) (VOID *) (args[0]));
            }
            i4RetStatus = AclIpStatsClear (CliHandle, i4FilterNo);
            break;

        case CLI_CLR_USR_DEF_ACL_COUNT:
            if ((INT4 *) (VOID *) (args[0]) != NULL)
            {
                i4FilterNo = (*(INT4 *) (VOID *) (args[0]));
            }
            i4RetStatus = AclUserDefinedStatsClear (CliHandle, i4FilterNo);
            break;
        case CLI_RESR_FRM_TRNS:

            /*
             *Copying  MAC address for reserved frames
             */
            MEMSET (MacAddr, 0, sizeof (tMacAddr));
            if ((UINT1 *) (args[1]) != NULL)
            {
                StrToMac (args[1], MacAddr);
            }

            if (args[0] != NULL)
            {
                u4Protocol = CLI_PTR_TO_U4 (args[0]);
            }
            if (args[2] != NULL)
            {
                u4MacMask = CLI_PTR_TO_U4 (args[2]);
            }
            if (args[3] != NULL)
            {
                u4ControlAction = CLI_PTR_TO_U4 (args[3]);
            }

            u1ResrvFrameStatus = ISS_RESERV_FRAME_ADD;
            i4RetStatus =
                AclReservFrameFilterConfig (CliHandle, u4Protocol, MacAddr,
                                            (UINT1) u4MacMask,
                                            (UINT1) u4ControlAction,
                                            u1ResrvFrameStatus);
            break;
        case CLI_NO_RESR_FRM_TRNS:

            /*
             *Copying  MAC address for reserved frames
             */
            MEMSET (MacAddr, 0, sizeof (tMacAddr));
            if ((UINT1 *) (args[1]) != NULL)
            {
                StrToMac (args[1], MacAddr);
            }
            if (args[0] != NULL)
            {
                u4Protocol = CLI_PTR_TO_U4 (args[0]);
            }
            if (args[2] != NULL)
            {
                u4MacMask = CLI_PTR_TO_U4 (args[2]);
            }

            u1ResrvFrameStatus = ISS_RESERV_FRAME_DELETE;
            i4RetStatus =
                AclReservFrameFilterConfig (CliHandle, u4Protocol, MacAddr,
                                            (UINT1) u4MacMask,
                                            (UINT1) u4ControlAction,
                                            u1ResrvFrameStatus);
            break;

        case CLI_ACL_RESRV_FRAME_SHOW:
            i4RetStatus = AclReservFrameShow (CliHandle);
            break;
    }

    if ((CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_ACL_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", AclCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }

    CliUnRegisterLock (CliHandle);

    ISS_UNLOCK ();

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit cli_process_acl_cmd  function \n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclCreateIPFilter                                  */
/*                                                                           */
/*     DESCRIPTION      : This function creates an IP ACL filter and enters  */
/*                        the corresponding configuration mode               */
/*                                                                           */
/*     INPUT            : u4Type  - Standard /Extended ACL                   */
/*                        i4Filterno - IP ACL number                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
AclCreateIPFilter (tCliHandle CliHandle, UINT4 u4Type, INT4 i4FilterNo)
{
    INT4                i4Status = 0;
    UINT4               u4ErrCode;
    UINT1               au1IfName[ACL_MAX_NAME_LENGTH];

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclCreateIPFilter function \n");

    /* if filter already exists, do nothing and enter the configuration mode */

    if (nmhGetIssExtL3FilterStatus (i4FilterNo, &i4Status) == SNMP_FAILURE)
    {
        /* Create a filter */
        if (nmhTestv2IssExtL3FilterStatus (&u4ErrCode,
                                           i4FilterNo,
                                           ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test L3 FilterStatus for filter ID %d with  error code as %d\n",
                          i4FilterNo, u4ErrCode);
            return (CLI_FAILURE);
        }

        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3 FilterStatus for filter ID %d \n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }

    }

    /* Enter IP ACL configuration mode */

    if (u4Type == ACL_STANDARD)
    {
        /* Set the mode information for CLI */
        CLI_SET_STDACLID (i4FilterNo);
        SNPRINTF ((CHR1 *) au1IfName, ACL_MAX_NAME_LENGTH,
                  "%s%d", CLI_STDACL_MODE, i4FilterNo);
    }

    else

    {
        /* Set the mode information for CLI */
        CLI_SET_EXTACL (i4FilterNo);
        SNPRINTF ((CHR1 *) au1IfName, ACL_MAX_NAME_LENGTH,
                  "%s%d", CLI_EXTACL_MODE, i4FilterNo);
    }

    /* Return ACL configuration prompt */
    CliChangePath ((CHR1 *) au1IfName);

    UNUSED_PARAM (CliHandle);
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclCreateIPFilter function \n");
    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclDestroyIPFilter                                 */
/*                                                                           */
/*     DESCRIPTION      : This function destroys a IP ACL filter and enters  */
/*                        the corresponding configuration mode               */
/*                                                                           */
/*     INPUT            : i4Filterno - IP ACL number                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
AclDestroyIPFilter (tCliHandle CliHandle, INT4 i4FilterNo)
{
    INT4                i4Status;
    INT4                i4ActionOld = 0;
    INT4                i4RedirectStatus = 0;
    UINT4               u4RedirectGrpId = 0;
    UINT4               u4ErrCode;

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclDestroyIPFilter  function \n");

    if (nmhGetIssExtL3FilterStatus (i4FilterNo, &i4Status) == SNMP_FAILURE)
    {
        /* Filter does not exist */
        CliPrintf (CliHandle, "\r%% Invalid Filter number \r\n");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to retrieve L3 Filter Status %d for Filter Id %d \n",
                      i4Status, i4FilterNo);
        return (CLI_FAILURE);
    }

    else

    {
        if (nmhGetIssExtL3FilterAction (i4FilterNo, &i4ActionOld) ==
            SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to retrieve L3 Filter Action %d for Filter Id %d \n",
                          i4ActionOld, i4FilterNo);
            return (CLI_FAILURE);
        }

        if (i4ActionOld == ISS_REDIRECT_TO)
        {
            nmhGetIssAclL3FilterRedirectId (i4FilterNo,
                                            (INT4 *) &u4RedirectGrpId);

            if (u4RedirectGrpId != 0)
            {
                if (nmhGetIssRedirectInterfaceGrpStatus (u4RedirectGrpId,
                                                         &i4RedirectStatus) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to retrieve RedirectInterface Status %d for Group Id %d \n",
                                  i4RedirectStatus, u4RedirectGrpId);
                    return (CLI_FAILURE);
                }

                if (i4RedirectStatus != 0)
                {
                    if (nmhSetIssRedirectInterfaceGrpStatus
                        (u4RedirectGrpId, ISS_DESTROY) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to set RedirectInterface Status  for Group Id %d \n",
                                      u4RedirectGrpId);
                        return (CLI_FAILURE);
                    }
                }
            }
        }

        if (nmhTestv2IssExtL3FilterStatus (&u4ErrCode,
                                           i4FilterNo,
                                           ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test L3Filter Status for filter ID %d with  error code as %d\n",
                          i4FilterNo, u4ErrCode);
            return (CLI_FAILURE);
        }

        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3Filter Status for filter ID %d\n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }
    }
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclDestroyIPFilter function \n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclCreateMacFilter                                 */
/*                                                                           */
/*     DESCRIPTION      : This function creates an MAC ACL filter and enters */
/*                        the corresponding configuration mode               */
/*                                                                           */
/*     INPUT            : i4Filterno - MAC ACL number                        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
AclCreateMacFilter (tCliHandle CliHandle, INT4 i4FilterNo)
{
    INT4                i4Status;
    UINT4               u4ErrCode;
    UINT1               au1IfName[ACL_MAX_NAME_LENGTH];

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclCreateMacFilter function \n");
    /* if filter already exists, do nothing and enter the configuration mode */

    if (nmhGetIssExtL2FilterStatus (i4FilterNo, &i4Status) == SNMP_FAILURE)
    {
        /* Create a filter */
        if (nmhTestv2IssExtL2FilterStatus (&u4ErrCode,
                                           i4FilterNo,
                                           ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test L2 Filter Status for filter ID %d with  error code as %d\n",
                          i4FilterNo, u4ErrCode);
            return (CLI_FAILURE);
        }

        if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L2 FilterStatus for filter ID %d \n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }

    }

    /* Enter MAC ACL configuration mode */

    /* Set the mode information for CLI */
    CLI_SET_MACACL (i4FilterNo);
    SNPRINTF ((CHR1 *) au1IfName, ACL_MAX_NAME_LENGTH, "%s%d",
              CLI_MACACL_MODE, i4FilterNo);
    /* Return ACL configuration prompt */
    CliChangePath ((CHR1 *) au1IfName);
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclCreateMacFilter  function \n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclDestroyMacFilter                                */
/*                                                                           */
/*     DESCRIPTION      : This function destroys a MAC ACL filter and enters */
/*                        the corresponding configuration mode               */
/*                                                                           */
/*     INPUT            : i4Filterno - MAC ACL number                        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
AclDestroyMacFilter (tCliHandle CliHandle, INT4 i4FilterNo)
{
    INT4                i4Status;
    INT4                i4ActionOld = 0;
    INT4                i4RedirectStatus = 0;
    UINT4               u4RedirectGrpId = 0;
    UINT4               u4ErrCode;

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclDestroyMacFilter function \n");

    if (nmhGetIssExtL2FilterStatus (i4FilterNo, &i4Status) == SNMP_FAILURE)
    {
        /* Filter does not exist */
        CliPrintf (CliHandle, "\r%% Invalid Filter number \r\n");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to retrieve L2 Filter Status %d for filter ID %d\n",
                      i4Status, i4FilterNo);
        return (CLI_FAILURE);
    }

    else

    {
        if (nmhTestv2IssExtL2FilterStatus (&u4ErrCode,
                                           i4FilterNo,
                                           ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test L2 FilterStatus for filter ID %d with  error code as %d\n",
                          i4FilterNo, u4ErrCode);
            return (CLI_FAILURE);
        }

        if (nmhGetIssExtL2FilterAction (i4FilterNo, &i4ActionOld) ==
            SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to retrieve L2 Filter Action %d for Filter Id %d \n",
                          i4ActionOld, i4FilterNo);
            return (CLI_FAILURE);
        }

        if (i4ActionOld == ISS_REDIRECT_TO)
        {
            nmhGetIssAclL2FilterRedirectId (i4FilterNo,
                                            (INT4 *) &u4RedirectGrpId);
            if (u4RedirectGrpId != 0)
            {

                if (nmhGetIssRedirectInterfaceGrpStatus (u4RedirectGrpId,
                                                         &i4RedirectStatus) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to retrieve RedirectInterface Status  for Group Id %d\n",
                                  u4RedirectGrpId);
                    return (CLI_FAILURE);
                }

                if (i4RedirectStatus != 0)
                {
                    if (nmhSetIssRedirectInterfaceGrpStatus
                        (u4RedirectGrpId, ISS_DESTROY) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to set RedirectInterface Status  for Group Id %d \n",
                                      u4RedirectGrpId);
                        return (CLI_FAILURE);
                    }
                }
            }
        }

#ifdef ISS_METRO_WANTED
        /* Before setting the filter status to ISS_DESTROY, the mib objects
         * associated with issMetroL2FilterTable should be set to default value*/
        if (nmhSetIssMetroL2FilterOuterEtherType (i4FilterNo, 0) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
        if (nmhSetIssMetroL2FilterSVlanId (i4FilterNo, 0) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
        if (nmhSetIssMetroL2FilterSVlanPriority
            (i4FilterNo, ISS_DEFAULT_VLAN_PRIORITY) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
        if (nmhSetIssMetroL2FilterCVlanPriority
            (i4FilterNo, ISS_DEFAULT_VLAN_PRIORITY) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
        if (nmhSetIssMetroL2FilterPacketTagType (i4FilterNo,
                                                 ISS_FILTER_SINGLE_TAG) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
#endif

        if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L2 Filter Status for filter ID %d\n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }
    }
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclDestroyMacFilter  function \n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclDestroyUserDefFilter                            */
/*                                                                           */
/*     DESCRIPTION      : This function destroys a UserDefined ACL filter    */
/*                        and enters the corresponding configuration mode    */
/*                                                                           */
/*     INPUT            : i4Filterno - UserDefined ACL number                */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
AclDestroyUserDefFilter (tCliHandle CliHandle, INT4 i4FilterNo)
{
    INT4                i4Status;
    INT4                i4ActionOld = 0;
    INT4                i4RedirectStatus = 0;
    UINT4               u4RedirectGrpId = 0;
    UINT4               u4ErrCode;

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclDestroyUserDefFilter function \n");
    if (nmhGetIssAclUserDefinedFilterStatus ((UINT4) i4FilterNo, &i4Status) ==
        SNMP_FAILURE)
    {
        /* Filter does not exist */
        CliPrintf (CliHandle, "\r%% Invalid Filter number \r\n");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to retrieve User Defined Filter Status %d for Filter Id %d \n",
                      i4Status, i4FilterNo);
        return (CLI_FAILURE);
    }
    else
    {
        if (nmhGetIssAclUserDefinedFilterAction
            ((UINT4) i4FilterNo, &i4ActionOld) == SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to retrieve User Defined Filter Action %d for Filter Id %d \n",
                          i4ActionOld, i4FilterNo);
            return (CLI_FAILURE);
        }
        if (i4ActionOld == ISS_REDIRECT_TO)
        {
            nmhGetIssAclUserDefinedFilterRedirectId ((UINT4) i4FilterNo,
                                                     (INT4 *) &u4RedirectGrpId);
            if (u4RedirectGrpId != 0)
            {
                if (nmhGetIssRedirectInterfaceGrpStatus
                    (u4RedirectGrpId, &i4RedirectStatus) == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to retrieve RedirectInterface Status for Group Id %d \n",
                                  u4RedirectGrpId);
                    return (CLI_FAILURE);
                }
                if (i4RedirectStatus != 0)
                {
                    if (nmhSetIssRedirectInterfaceGrpStatus
                        (u4RedirectGrpId, ISS_DESTROY) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to Set RedirectInterface Status for Group Id %d \n",
                                      u4RedirectGrpId);
                        return (CLI_FAILURE);
                    }
                }
            }
        }
        if (nmhTestv2IssAclUserDefinedFilterStatus (&u4ErrCode,
                                                    (UINT4) i4FilterNo,
                                                    ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test User Defined Filter Status for filter ID %d with  error code as %d\n",
                          i4FilterNo, u4ErrCode);
            return (CLI_FAILURE);
        }
        if (nmhSetIssAclUserDefinedFilterStatus
            ((UINT4) i4FilterNo, ISS_DESTROY) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set User Defined Filter Status for filter ID %d\n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclCreateUserDefFilter                             */
/*                                                                           */
/*     DESCRIPTION      : This function creates an User Defined  ACL filter  */
/*                           and enters the corresponding configuration mode */
/*                                                                           */
/*     INPUT            : i4Filterno - UserDefined ACL number                */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
AclCreateUserDefFilter (tCliHandle CliHandle, INT4 i4FilterNo)
{
    INT4                i4Status;
    UINT4               u4ErrCode;
    UINT1               au1IfName[ACL_MAX_NAME_LENGTH];

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclCreateUserDefFilter function \n");
    if (nmhGetIssAclUserDefinedFilterStatus ((UINT4) i4FilterNo, &i4Status) ==
        SNMP_FAILURE)
    {
        if (nmhTestv2IssAclUserDefinedFilterStatus (&u4ErrCode,
                                                    (UINT4) i4FilterNo,
                                                    ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test  User Defined Filter Status for filter ID %d with  error code as %d\n",
                          i4FilterNo, u4ErrCode);
            return (CLI_FAILURE);
        }
        if (nmhSetIssAclUserDefinedFilterStatus
            ((UINT4) i4FilterNo, ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "CLI:Failed to set User Defined Filter Status for filter ID%d\n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }
    }
    CLI_SET_USERDEFFACL (i4FilterNo);
    SNPRINTF ((CHR1 *) au1IfName, ACL_MAX_NAME_LENGTH, "%s%d",
              CLI_USRDEFACL_MODE, i4FilterNo);
    CliChangePath ((CHR1 *) au1IfName);
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclCreateUserDefFilter function \n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclStdIpFilterConfig                               */
/*                                                                           */
/*     DESCRIPTION      : This function configures a standard IP ACL filter  */
/*                         and enters the corresponding configuration mode   */
/*                                                                           */
/*     INPUT            :  u4Action -Permit/Deny                             */
/*                         u4SrcType - any/source network/ source host       */
/*                         u4SrcIp - Source IP Address                       */
/*                         u4SrcMask - Source IP Mask                        */
/*                         u4DestType - any/dest network/dest host           */
/*                         u4DestIp - Dest IP Address                        */
/*                         u4DestMask - Dest IP Mask                         */
/*                         i4Priority - Priority value of filter             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
AclStdIpFilterConfig (tCliHandle CliHandle, INT4 i4Action,
                      UINT4 u4SrcType, UINT4 u4SrcIpAddr,
                      UINT4 u4SrcMask, UINT4 u4DestType,
                      UINT4 u4DestIpAddr, UINT4 u4DestMask, INT4 i4Priority)
{
    INT4                i4FilterNo;
    INT4                i4Status;
    INT4                i4ActionOld = 0;
    INT4                i4RedirectStatus = 0;
    UINT4               u4RedirectGrpId = 0;
    UINT4               u4ErrCode;
    INT4                i4ActiveFlag = 0;
    INT4                i4Direction;
    INT4                i4StatsOld = 0;
    INT1                i4RetVal = SNMP_SUCCESS;
    tIssL3FilterEntry   IssAclL3FilterEntry;
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;
    MEMSET (&IssAclL3FilterEntry, 0, sizeof (tIssL3FilterEntry));
    tSNMP_OCTET_STRING_TYPE InPortList;
    tSNMP_OCTET_STRING_TYPE OutPortList;
    tSNMP_OCTET_STRING_TYPE InPortChannelList;
    tSNMP_OCTET_STRING_TYPE OutPortChannelList;
    UINT1               au1InPortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];
    UINT1               au1OutPortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];
    UINT1               au1InPortList[ISS_PORTLIST_LEN];
    UINT1               au1OutPortList[ISS_PORTLIST_LEN];

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclStdIpFilterConfig function \n");
    MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1OutPortList, 0, ISS_PORTLIST_LEN);
    ISS_MEMSET (&InPortList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    ISS_MEMSET (&OutPortList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    InPortList.i4_Length = ISS_PORTLIST_LEN;
    InPortList.pu1_OctetList = &au1InPortList[0];

    OutPortList.i4_Length = ISS_PORTLIST_LEN;
    OutPortList.pu1_OctetList = &au1OutPortList[0];

    MEMSET (au1InPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    MEMSET (au1OutPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    ISS_MEMSET (&InPortChannelList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    ISS_MEMSET (&OutPortChannelList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    InPortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;
    InPortChannelList.pu1_OctetList = &au1InPortChannelList[0];

    OutPortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;
    OutPortChannelList.pu1_OctetList = &au1OutPortChannelList[0];

    /*Get the IP Access list number from the current mode in CLI */
    i4FilterNo = CLI_GET_STDACLID ();
    if (nmhGetIssExtL3FilterStatus (i4FilterNo, &i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Retrieve L3 Filter Status %d for Filter ID %d\n",
                      i4Status, i4FilterNo);
        return CLI_FAILURE;
    }

    /* Store the contents in temporary variable. In case of any failure, copy this
     * over the existing node to avoid intermediate updates
     */
    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        ISS_MEMCPY (&IssAclL3FilterEntry, pIssExtL3FilterEntry,
                    sizeof (tIssL3FilterEntry));
    }
    else
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    if (i4Status != ISS_ACTIVE)
    {
        AclResetL3Filter (i4FilterNo);
    }
    nmhGetIssAclL3FilterStatsEnabledStatus (i4FilterNo, &i4StatsOld);
    if (i4Status != ISS_NOT_READY)
    {
        if (i4Status == ISS_ACTIVE)
        {
            i4ActiveFlag = 1;
            nmhGetIssExtL3FilterDirection (i4FilterNo, &i4Direction);
            if (i4Direction == ACL_ACCESS_IN)
            {
                nmhGetIssExtL3FilterInPortList (i4FilterNo, &InPortList);
                nmhGetIssExtL3FilterInPortChannelList (i4FilterNo,
                                                       &InPortChannelList);

            }
            else
            {
                nmhGetIssExtL3FilterOutPortChannelList (i4FilterNo,
                                                        &OutPortChannelList);
                nmhGetIssExtL3FilterOutPortList (i4FilterNo, &OutPortList);
            }
        }
        /* Filter has previously been configured. This must be over-written 
         * First destroy the filter and create again 
         */
        if (nmhGetIssExtL3FilterAction (i4FilterNo, &i4ActionOld) ==
            SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssAclL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to retrieve L3 Filter Action %d for Filter Id %d \n",
                          i4ActionOld, i4FilterNo);
            return (CLI_FAILURE);
        }
        if (AclValidateActionWithDirection (i4Direction, i4Action) ==
            CLI_FAILURE)
        {
            ISS_TRC (ALL_FAILURE_TRC,
                     "\nCLI:Failed as specified  action conflicts with ACL direction");
            return (CLI_FAILURE);
        }

        if (i4ActionOld == ISS_REDIRECT_TO)
        {
            nmhGetIssAclL3FilterRedirectId (i4FilterNo,
                                            (INT4 *) &u4RedirectGrpId);
            if (u4RedirectGrpId != 0)
            {
                if (nmhGetIssRedirectInterfaceGrpStatus (u4RedirectGrpId,
                                                         &i4RedirectStatus) ==
                    SNMP_FAILURE)
                {
                    if (pIssExtL3FilterEntry != NULL)
                    {
                        ISS_MEMCPY (pIssExtL3FilterEntry, &IssAclL3FilterEntry,
                                    sizeof (tIssL3FilterEntry));
                    }
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to retrieve RedirectInterface Status %d for Group Id %d \n",
                                  i4RedirectStatus, u4RedirectGrpId);
                    return (CLI_FAILURE);
                }
                if (i4RedirectStatus != 0)
                {
                    if (nmhSetIssRedirectInterfaceGrpStatus
                        (u4RedirectGrpId, ISS_DESTROY) == SNMP_FAILURE)
                    {
                        if (pIssExtL3FilterEntry != NULL)
                        {
                            ISS_MEMCPY (pIssExtL3FilterEntry,
                                        &IssAclL3FilterEntry,
                                        sizeof (tIssL3FilterEntry));
                        }
                        CLI_FATAL_ERROR (CliHandle);
                        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to set RedirectInterface Status for Group Id %d \n",
                                      u4RedirectGrpId);
                        return (CLI_FAILURE);
                    }
                }
            }
        }
        if (nmhTestv2IssExtL3FilterStatus (&u4ErrCode, i4FilterNo, ISS_DESTROY)
            == SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test L3 Filter Status for filter ID %d with  error code as %d\n",
                          i4FilterNo, u4ErrCode);
            return (CLI_FAILURE);
        }

        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssAclL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3Filter Status for filter ID %d\n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }

        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssAclL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3Filter Status for filter ID %d\n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }

    }

    nmhSetIssAclL3FilteAddrType (i4FilterNo, QOS_IPV4);

    if (nmhTestv2IssExtL3FilterPriority (&u4ErrCode, i4FilterNo, i4Priority)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test L3 Filter Priority %d for Filter ID %d                          with  error code as %d\n",
                      i4Priority, i4FilterNo, u4ErrCode);
        return (CLI_FAILURE);
    }

    if (nmhTestv2IssExtL3FilterAction (&u4ErrCode,
                                       i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssAclL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test L3 Filter Action %d for Filter ID %d                              with  error code as %d\n",
                      i4Action, i4FilterNo, u4ErrCode);
        return (CLI_FAILURE);
    }

    /*Test the user input for destination IP and mask */
    if (AclTestIpParams
        (ACL_SRC, i4FilterNo, u4SrcType, u4SrcIpAddr, u4SrcMask) == CLI_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssAclL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        CliPrintf (CliHandle, "%%Invalid Source IP Address. \r\n ");
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test source Ip parameters with u4SrcMask  and u4SrcIpAddr  with u4SrcType  for filter ID %d\n",
                      i4FilterNo);
        return (CLI_FAILURE);
    }

    /*Test the user input for destination IP and mask */
    if (AclTestIpParams
        (ACL_DST, i4FilterNo, u4DestType, u4DestIpAddr,
         u4DestMask) == CLI_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssAclL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        CliPrintf (CliHandle, "%%Invalid Destination IP Address. \r\n ");
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test destination Ip parameters with u4DestMask  and u4DestIpAddr with u4DestType  for filter ID %d\n",
                      i4FilterNo);
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL3FilterAction (i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssAclL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Set L3 Filter Action %d for Filter ID %d\n",
                      i4Action, i4FilterNo);
        return (CLI_FAILURE);
    }

    if (AclSetIpParams
        (ACL_SRC, i4FilterNo, u4SrcType, u4SrcIpAddr, u4SrcMask) == CLI_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssAclL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to set source Ip parameters with u4SrcMask and u4SrcIpAddr with u4SrcType  for filter ID %d\n",
                      u4SrcType, i4FilterNo);
        return (CLI_FAILURE);
    }

    if (AclSetIpParams
        (ACL_DST, i4FilterNo, u4DestType, u4DestIpAddr,
         u4DestMask) == CLI_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssAclL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to set destination Ip parameters with u4DestMask  and u4DestIpAddr  with u4DestType  for filter ID %d\n",
                      u4DestType, i4FilterNo);
        return (CLI_FAILURE);
    }
    if (nmhSetIssExtL3FilterPriority (i4FilterNo, i4Priority) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Set L3 Filter Priority  %d for Filter ID %d\n",
                      i4Priority, i4FilterNo);
        return (CLI_FAILURE);
    }
    if ((i4ActiveFlag == 1) && (i4Status == ISS_ACTIVE))
    {
        i4RetVal = nmhSetIssExtL3FilterDirection (i4FilterNo, i4Direction);
        if (i4RetVal == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to Set L3 Filter Direction %d for Filter ID %d\n",
                          i4Direction, i4FilterNo);
            return (CLI_FAILURE);
        }
        if (i4Direction == ACL_ACCESS_IN)
        {
            i4RetVal =
                nmhSetIssExtL3FilterInPortChannelList (i4FilterNo,
                                                       &InPortChannelList);
            if (i4RetVal == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter InPortChannelList  for Filter ID %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }

            i4RetVal = nmhSetIssExtL3FilterInPortList (i4FilterNo, &InPortList);
            if (i4RetVal == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter InPortList  for Filter ID %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }
        }
        else
        {
            i4RetVal =
                nmhSetIssExtL3FilterOutPortChannelList (i4FilterNo,
                                                        &OutPortChannelList);
            if (i4RetVal == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter OutPortChannelList  for Filter ID %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }
            i4RetVal =
                nmhSetIssExtL3FilterOutPortList (i4FilterNo, &OutPortList);
            if (i4RetVal == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter  OutPortList   for Filter ID%d \n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }
        }
        if (i4StatsOld == ACL_STAT_ENABLE)
        {
            if ((AclIpStatsSetEnabledStatus (CliHandle, i4FilterNo, i4StatsOld))
                == CLI_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_HW_UPDATE_FAILED);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to enable statistics for Filter ID %d \n",
                              i4FilterNo);
                return CLI_FAILURE;
            }
        }
        i4RetVal = nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_ACTIVE);
        if (i4RetVal == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3 Filter Status for Filter ID \n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }
    }
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclStdIpFilterConfig function \n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclValidateActionWithDirection                     */
/*                                                                           */
/*     DESCRIPTION      : This function validates if specified operation is  */
/*                        permissible with ACL direction.                    */
/*                                                                           */
/*     INPUT            :  i4Direction - ACL direction                       */
/*                         i4Action    - Current action specified            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
AclValidateActionWithDirection (INT4 i4Direction, INT4 i4Action)
{
    if ((i4Direction == ACL_ACCESS_OUT) && (i4Action == ISS_SWITCH_COPYTOCPU))
    {
        CLI_SET_ERR (CLI_ACL_INVALID_COPY_ERR);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclExtIp6FilterConfig                               */
/*                                                                           */
/*     DESCRIPTION      : This function configures a standard IP ACL filter  */
/*                         and enters the corresponding configuration mode   */
/*                                                                           */
/*     INPUT            :  u4Action -Permit/Deny                             */
/*                         u4SrcType - any/source network/ source host       */
/*                         u4SrcIp - Source IP Address                       */
/*                         u4SrcMask - Source IP Mask                        */
/*                         u4DestType - any/dest network/dest host           */
/*                         u4DestIp - Dest IP Address                        */
/*                         u4DestMask - Dest IP Mask                         */
/*                         i4Priority - Priority value of filter             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
AclExtIp6FilterConfig (tCliHandle CliHandle, UINT4 u4SrcIpAddr,
                       tIp6Addr SrcIp6Addr, UINT4 u4SrcIpMask,
                       UINT4 u4DestIpAddr, tIp6Addr DstIp6Addr,
                       UINT4 u4DstIpMask, UINT4 u4FlowId, INT4 i4Action,
                       INT4 i4Priority)
{

    INT4                i4FilterNo = 0;
    INT4                i4Status = 0;
    INT4                i4ActionOld = 0;
    INT4                i4RedirectStatus = 0;
    UINT4               u4ErrorCode;
    UINT4               u4RedirectGrpId = 0;
    tSNMP_OCTET_STRING_TYPE SrcIpv6;
    tSNMP_OCTET_STRING_TYPE DstIpv6;
    UINT1               au1SrcIPv6[IP6_ADDR_SIZE];
    UINT1               au1DstIPv6[IP6_ADDR_SIZE];
    INT4                i4ActiveFlag = 0;
    INT4                i4Direction;
    INT1                i4RetVal = SNMP_SUCCESS;
    tSNMP_OCTET_STRING_TYPE InPortList;
    tSNMP_OCTET_STRING_TYPE OutPortList;
    UINT1               au1InPortList[ISS_PORTLIST_LEN];
    UINT1               au1OutPortList[ISS_PORTLIST_LEN];
    tSNMP_OCTET_STRING_TYPE InPortChannelList;
    tSNMP_OCTET_STRING_TYPE OutPortChannelList;
    tIssL3FilterEntry   IssExtL3FilterEntry;
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;
    UINT1               au1InPortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];
    UINT1               au1OutPortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];;
    MEMSET (&IssExtL3FilterEntry, 0, sizeof (tIssL3FilterEntry));
    MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1OutPortList, 0, ISS_PORTLIST_LEN);
    ISS_MEMSET (&InPortList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    ISS_MEMSET (&OutPortList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    InPortList.i4_Length = ISS_PORTLIST_LEN;
    InPortList.pu1_OctetList = &au1InPortList[0];

    OutPortList.i4_Length = ISS_PORTLIST_LEN;
    OutPortList.pu1_OctetList = &au1OutPortList[0];
    MEMSET (au1SrcIPv6, 0, sizeof (au1SrcIPv6));
    MEMSET (au1DstIPv6, 0, sizeof (au1DstIPv6));

    SrcIpv6.pu1_OctetList = au1SrcIPv6;
    DstIpv6.pu1_OctetList = au1DstIPv6;

    MEMSET (au1InPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    MEMSET (au1OutPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    ISS_MEMSET (&InPortChannelList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    ISS_MEMSET (&OutPortChannelList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    InPortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;
    InPortChannelList.pu1_OctetList = &au1InPortChannelList[0];

    OutPortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;
    OutPortChannelList.pu1_OctetList = &au1OutPortChannelList[0];

    i4FilterNo = CLI_GET_EXTACL ();
    if (nmhGetIssAclL3FilterStatus (i4FilterNo, &i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    /* Store the contents in temporary variable. In case of any failure, copy this
     * over the existing node to avoid intermediate updates
     */
    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        ISS_MEMCPY (&IssExtL3FilterEntry, pIssExtL3FilterEntry,
                    sizeof (tIssL3FilterEntry));
    }
    else
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    if (i4Status != ISS_ACTIVE)
    {
        AclResetL3FilterQualifiers (i4FilterNo);
    }
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclExtIp6FilterConfig function \n");
    if (i4Status != ISS_NOT_READY)
    {
        if (i4Status == ISS_ACTIVE)
        {
            i4ActiveFlag = 1;
            nmhGetIssExtL3FilterDirection (i4FilterNo, &i4Direction);

            if (i4Direction == ACL_ACCESS_IN)
            {
                nmhGetIssExtL3FilterInPortList (i4FilterNo, &InPortList);
                nmhGetIssExtL3FilterInPortChannelList (i4FilterNo,
                                                       &InPortChannelList);

            }
            else
            {
                nmhGetIssExtL3FilterOutPortChannelList (i4FilterNo,
                                                        &OutPortChannelList);
                nmhGetIssExtL3FilterOutPortList (i4FilterNo, &OutPortList);
            }
        }
        if (nmhGetIssExtL3FilterAction (i4FilterNo, &i4ActionOld) ==
            SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test L3 Filter Action %d for Filter ID %d\n",
                          i4ActionOld, i4FilterNo);
            return (CLI_FAILURE);
        }

        if (i4ActionOld == ISS_REDIRECT_TO)
        {
            nmhGetIssAclL3FilterRedirectId (i4FilterNo,
                                            (INT4 *) &u4RedirectGrpId);
            if (u4RedirectGrpId != 0)
            {

                if (nmhGetIssRedirectInterfaceGrpStatus (u4RedirectGrpId,
                                                         &i4RedirectStatus) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to retrieve RedirectInterface Status %d for Group Id %d \n",
                                  i4RedirectStatus, u4RedirectGrpId);
                    return (CLI_FAILURE);
                }

                if (i4RedirectStatus != 0)
                {
                    if (nmhSetIssRedirectInterfaceGrpStatus
                        (u4RedirectGrpId, ISS_DESTROY) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to set Redirect Interface Status for Group Id %d \n",
                                      u4RedirectGrpId);
                        return (CLI_FAILURE);
                    }
                }
            }
        }

        /* Filter has previously been configured. This must be over-written 
         * First destroy the filter and create again */
        if (nmhTestv2IssAclL3FilterStatus
            (&u4ErrorCode, i4FilterNo, ISS_DESTROY) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test L3 Filter Status for filter ID %d with  error code as %d\n",
                          i4FilterNo, u4ErrorCode);
            return (CLI_FAILURE);
        }

        if (nmhSetIssAclL3FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3Filter Status for filter ID %d\n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }

        if (nmhSetIssAclL3FilterStatus (i4FilterNo, ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3Filter Status for filter ID %d\n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }

    }
    i4RetVal = nmhSetIssAclL3FilteAddrType (i4FilterNo, QOS_IPV6);
    i4RetVal = nmhSetIssAclL3FilterProtocol (i4FilterNo, ISS_IP_PROTO_DEF);

    if (u4SrcIpAddr != ACL_ANY)
    {
        /*Used diffserv since we did not have mIB for fsqos */
        i4RetVal = nmhSetIssAclL3FilterFlowId (i4FilterNo, ISS_FLOWID_INVALID);

        MEMCPY (SrcIpv6.pu1_OctetList, &SrcIp6Addr, IP6_ADDR_SIZE);
        SrcIpv6.i4_Length = IP6_ADDR_SIZE;

        if (nmhTestv2IssAclL3FilterSrcIpAddr
            (&u4ErrorCode, i4FilterNo, &SrcIpv6) == SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test  L3 Filter SrcIpv6 Address  for Filter ID %d                with error code as %d\n",
                          i4FilterNo, u4ErrorCode);
            return CLI_FAILURE;
        }

        if (nmhSetIssAclL3FilterSrcIpAddr (i4FilterNo, &SrcIpv6) ==
            SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set  L3 Filter SrcIpv6 Address  for Filter ID %d\n",
                          i4FilterNo);
            return CLI_FAILURE;
        }
        if (nmhTestv2IssAclL3FilterSrcIpAddrPrefixLength (&u4ErrorCode,
                                                          i4FilterNo,
                                                          u4SrcIpMask)
            == SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test  L3 Filter Src Ip Mask for Filter ID %d with error                  code as %d\n",
                          i4FilterNo, u4ErrorCode);
            return CLI_FAILURE;
        }
        if (nmhSetIssAclL3FilterSrcIpAddrPrefixLength (i4FilterNo,
                                                       u4SrcIpMask) ==
            SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3 Filter Src Ip Mask for Filter ID %d\n",
                          i4FilterNo);
            return CLI_FAILURE;
        }
    }

    if (u4DestIpAddr != ACL_ANY)
    {
        /*Used diffserv since we did not have mIB for fsqos */

        i4RetVal = nmhSetIssAclL3FilterFlowId (i4FilterNo, ISS_FLOWID_INVALID);
        MEMCPY (DstIpv6.pu1_OctetList, &DstIp6Addr, IP6_ADDR_SIZE);
        DstIpv6.i4_Length = IP6_ADDR_SIZE;

        if (nmhTestv2IssAclL3FilterDstIpAddr
            (&u4ErrorCode, i4FilterNo, &DstIpv6) == SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test  L3 Filter Dst Ipv6 Addr for Filter ID %d with error  code as %d\n",
                          i4FilterNo, u4ErrorCode);
            MEM_FREE ((UINT1 *) DstIpv6.pu1_OctetList);
            return CLI_FAILURE;
        }

        if (nmhSetIssAclL3FilterDstIpAddr (i4FilterNo, &DstIpv6) ==
            SNMP_FAILURE)
        {
            if (u4SrcIpAddr != ACL_ANY)
            {
                MEM_FREE ((UINT1 *) SrcIpv6.pu1_OctetList);
            }
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3 Filter Dst Ipv6 Addr for Filter ID %d\n",
                          i4FilterNo);
            return CLI_FAILURE;
        }
        if (nmhTestv2IssAclL3FilterDstIpAddrPrefixLength (&u4ErrorCode,
                                                          i4FilterNo,
                                                          u4DstIpMask)
            == SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test  L3 Filter Destination Ip Mask  for Filter ID %d with             error code as %d\n",
                          i4FilterNo, u4ErrorCode);
            return CLI_FAILURE;
        }
        if (nmhSetIssAclL3FilterDstIpAddrPrefixLength (i4FilterNo,
                                                       u4DstIpMask) ==
            SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3 Filter Dst Ip Mask for Filter ID %d\n",
                          i4FilterNo);
            return CLI_FAILURE;
        }
    }

    if (u4FlowId != ISS_FLOWID_INVALID)
    {
        MEMSET (SrcIpv6.pu1_OctetList, 0, IP6_ADDR_SIZE);
        SrcIpv6.i4_Length = IP6_ADDR_SIZE;
        if (nmhSetIssAclL3FilterSrcIpAddr (i4FilterNo, &SrcIpv6) ==
            SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set  L3 Filter SrcIpv6 Address  for Filter ID %d\n",
                          i4FilterNo);
            return CLI_FAILURE;
        }
        MEMSET (DstIpv6.pu1_OctetList, 0, IP6_ADDR_SIZE);
        DstIpv6.i4_Length = IP6_ADDR_SIZE;
        if (nmhSetIssAclL3FilterDstIpAddr (i4FilterNo, &DstIpv6) ==
            SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3 Filter Dst Ipv6 Addr  for Filter ID %d\n",
                          i4FilterNo);
            return CLI_FAILURE;
        }
        /*Before need to remove the Ipv6Address configurations */
        if (nmhTestv2IssAclL3FilterFlowId
            (&u4ErrorCode, i4FilterNo, u4FlowId) == SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test  L3 Filter FlowId  %d for Filter ID %d with error                  code as %d\n",
                          u4FlowId, i4FilterNo, u4ErrorCode);
            return CLI_FAILURE;
        }

        if (nmhSetIssAclL3FilterFlowId (i4FilterNo, u4FlowId) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_ACL_SRC_DST_IP_ADDR_CONF_ERR);
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set  L3 Filter FlowId  %d for Filter ID %d\n",
                          u4FlowId, i4FilterNo);
            return CLI_FAILURE;
        }

    }

    /* Set the action (permit/deny) accorring to the user config value */
    if (nmhTestv2IssExtL3FilterAction (&u4ErrorCode,
                                       i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test L3 Filter Action %d for Filter ID %d                              with  error code as %d\n",
                      i4Action, i4FilterNo, u4ErrorCode);
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL3FilterAction (i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Set L3 Filter Action %d for Filter ID %d\n",
                      i4Action, i4FilterNo);
        return (CLI_FAILURE);
    }
    if (nmhTestv2IssExtL3FilterPriority (&u4ErrorCode, i4FilterNo, i4Priority)
        == SNMP_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test L3 Filter Priority %d for Filter ID %d with  error code as %d\n",
                      i4Priority, i4FilterNo, u4ErrorCode);
        return (CLI_FAILURE);
    }
    if (nmhSetIssExtL3FilterPriority (i4FilterNo, i4Priority) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Set L3 Filter Priority  %d for Filter ID %d\n",
                      i4Priority, i4FilterNo);
        return (CLI_FAILURE);
    }
    if ((i4ActiveFlag == 1) && (i4Status == ISS_ACTIVE))
    {
        i4RetVal = nmhSetIssExtL3FilterDirection (i4FilterNo, i4Direction);
        if (i4RetVal == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to Set L3 Filter Direction %d for Filter ID %d\n",
                          i4Direction, i4FilterNo);
            return (CLI_FAILURE);
        }
        if (i4Direction == ACL_ACCESS_IN)
        {
            i4RetVal =
                nmhSetIssExtL3FilterInPortChannelList (i4FilterNo,
                                                       &InPortChannelList);
            if (i4RetVal == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
                if (pIssExtL3FilterEntry != NULL)
                {
                    ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                                sizeof (tIssL3FilterEntry));
                }
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter InPortChannelList  for Filter ID %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }

            i4RetVal = nmhSetIssExtL3FilterInPortList (i4FilterNo, &InPortList);
            if (i4RetVal == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
                if (pIssExtL3FilterEntry != NULL)
                {
                    ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                                sizeof (tIssL3FilterEntry));
                }
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter InPortList   for Filter ID %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }
        }
        else
        {
            i4RetVal =
                nmhSetIssExtL3FilterOutPortChannelList (i4FilterNo,
                                                        &OutPortChannelList);
            if (i4RetVal == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
                if (pIssExtL3FilterEntry != NULL)
                {
                    ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                                sizeof (tIssL3FilterEntry));
                }
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter OutPortChannelList  for Filter ID %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }
            i4RetVal =
                nmhSetIssExtL3FilterOutPortList (i4FilterNo, &OutPortList);
            if (i4RetVal == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
                if (pIssExtL3FilterEntry != NULL)
                {
                    ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                                sizeof (tIssL3FilterEntry));
                }
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter  OutPortList  for Filter ID%d \n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }
        }
        i4RetVal = nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_ACTIVE);
        if (i4RetVal == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3 Filter Status for Filter ID \n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }
    }
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclExtIp6FilterConfig function \n");
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclExtIpFilterConfig                               */
/*                                                                           */
/*     DESCRIPTION      : This function configures a extended IP ACL filter  */
/*                         and enters the corresponding configuration mode   */
/*                                                                           */
/*     INPUT            :  i4Action -Permit/Deny                             */
/*                         i4Protocol - Protocol value                       */
/*                         u4SrcType - any/source network/ source host       */
/*                         u4SrcIp - Source IP Address                       */
/*                         u4SrcMask - Source IP Mask                        */
/*                         u4DestType - any/dest network/dest host           */
/*                         u4DestIp - Dest IP Address                        */
/*                         u4DestMask - Dest IP Mask                         */
/*                         i4Priority - Filter priority                      */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
AclExtIpFilterConfig (tCliHandle CliHandle, INT4 i4Action,
                      INT4 i4Protocol, UINT4 u4SrcType,
                      UINT4 u4SrcIpAddr, UINT4 u4SrcMask,
                      UINT4 u4DestType, UINT4 u4DestIpAddr,
                      UINT4 u4DestMask, INT4 i4Tos, INT4 i4Dscp,
                      INT4 i4Priority)
{
    INT4                i4FilterNo;
    INT4                i4Status;
    INT4                i4ActionOld = 0;
    INT4                i4RedirectStatus = 0;
    UINT4               u4RedirectGrpId = 0;
    UINT4               u4ErrCode;
    UINT4               u4EnabledErrCode = 0;
    INT4                i4ActiveFlag = 0;
    INT4                i4Direction;
    INT4                i4StatsOld = 0;
    INT1                i4RetVal = SNMP_SUCCESS;
    tSNMP_OCTET_STRING_TYPE InPortList;
    tSNMP_OCTET_STRING_TYPE OutPortList;
    UINT1               au1InPortList[ISS_PORTLIST_LEN];
    UINT1               au1OutPortList[ISS_PORTLIST_LEN];
    tSNMP_OCTET_STRING_TYPE InPortChannelList;
    tSNMP_OCTET_STRING_TYPE OutPortChannelList;
    UINT1               au1InPortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];
    UINT1               au1OutPortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];
    tIssL3FilterEntry   IssExtL3FilterEntry;
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;
    MEMSET (&IssExtL3FilterEntry, 0, sizeof (tIssL3FilterEntry));
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclExtIpFilterConfig function \n");
    MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1OutPortList, 0, ISS_PORTLIST_LEN);
    ISS_MEMSET (&InPortList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    ISS_MEMSET (&OutPortList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    InPortList.i4_Length = ISS_PORTLIST_LEN;
    InPortList.pu1_OctetList = &au1InPortList[0];

    OutPortList.i4_Length = ISS_PORTLIST_LEN;
    OutPortList.pu1_OctetList = &au1OutPortList[0];

    MEMSET (au1InPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    MEMSET (au1OutPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    ISS_MEMSET (&InPortChannelList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    ISS_MEMSET (&OutPortChannelList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    InPortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;
    InPortChannelList.pu1_OctetList = &au1InPortChannelList[0];

    OutPortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;
    OutPortChannelList.pu1_OctetList = &au1OutPortChannelList[0];

    /*Get the IP Access list number from the current mode in CLI */
    i4FilterNo = CLI_GET_EXTACL ();
    if (nmhGetIssExtL3FilterStatus (i4FilterNo, &i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    /* Store the contents in temporary variable. In case of any failure, copy this
     * over the existing node to avoid intermediate updates
     */
    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        ISS_MEMCPY (&IssExtL3FilterEntry, pIssExtL3FilterEntry,
                    sizeof (tIssL3FilterEntry));
    }
    else
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    if (i4Status != ISS_ACTIVE)
    {
        AclResetL3Filter (i4FilterNo);
    }
    nmhGetIssAclL3FilterStatsEnabledStatus (i4FilterNo, &i4StatsOld);
    if (i4Status != ISS_NOT_READY)
    {
        if (i4Status == ISS_ACTIVE)
        {
            i4ActiveFlag = 1;
            nmhGetIssExtL3FilterDirection (i4FilterNo, &i4Direction);
            if (i4Direction == ACL_ACCESS_IN)
            {
                nmhGetIssExtL3FilterInPortList (i4FilterNo, &InPortList);
                nmhGetIssExtL3FilterInPortChannelList (i4FilterNo,
                                                       &InPortChannelList);

            }
            else
            {
                nmhGetIssExtL3FilterOutPortChannelList (i4FilterNo,
                                                        &OutPortChannelList);
                nmhGetIssExtL3FilterOutPortList (i4FilterNo, &OutPortList);
            }
        }
        if (nmhGetIssExtL3FilterAction (i4FilterNo, &i4ActionOld) ==
            SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to retrieve  L3 Filter Action %d for Filter ID %d\n",
                          i4ActionOld, i4FilterNo);
            return (CLI_FAILURE);
        }

        if (i4ActionOld == ISS_REDIRECT_TO)
        {
            nmhGetIssAclL3FilterRedirectId (i4FilterNo,
                                            (INT4 *) &u4RedirectGrpId);

            if (u4RedirectGrpId != 0)
            {

                if (nmhGetIssRedirectInterfaceGrpStatus (u4RedirectGrpId,
                                                         &i4RedirectStatus) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to retrieve RedirectInterface Status %d for Group Id %d \n",
                                  i4RedirectStatus, u4RedirectGrpId);
                    return (CLI_FAILURE);
                }

                if (i4RedirectStatus != 0)
                {

                    if (nmhSetIssRedirectInterfaceGrpStatus
                        (u4RedirectGrpId, ISS_DESTROY) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to set Redirect Interface Status for Group Id %d \n",
                                      u4RedirectGrpId);
                        return (CLI_FAILURE);
                    }
                }
            }
        }

        /* Filter has previously been configured. This must be over-written 
         * First destroy the filter and create again */
        if (nmhTestv2IssExtL3FilterStatus (&u4ErrCode, i4FilterNo, ISS_DESTROY)
            == SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test L3 Filter Status for filter ID %d with  error code as %d\n",
                          i4FilterNo, u4ErrCode);
            return (CLI_FAILURE);
        }

        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3Filter Status for filter ID %d\n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }

        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3Filter Status for filter ID %d\n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }

    }

    i4RetVal = nmhSetIssAclL3FilteAddrType (i4FilterNo, QOS_IPV4);

    if (nmhTestv2IssExtL3FilterAction (&u4ErrCode,
                                       i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test L3 Filter Action %d for Filter ID %d  with  error code as %d\n",
                      i4Action, i4FilterNo, u4ErrCode);
        return (CLI_FAILURE);
    }

    if (nmhTestv2IssExtL3FilterProtocol (&u4ErrCode,
                                         i4FilterNo,
                                         i4Protocol) == SNMP_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test L3 Filter Protocol %d for Filter ID %d                                  with  error code as %d\n",
                      i4Protocol, i4FilterNo, u4ErrCode);
        return (CLI_FAILURE);
    }

    /*Test the user input for source IP, mask */
    if (AclTestIpParams
        (ACL_SRC, i4FilterNo, u4SrcType, u4SrcIpAddr, u4SrcMask) == CLI_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        CliPrintf (CliHandle, "%%Invalid Source IP Address. \r\n ");
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test source Ip parameters with u4SrcMask and u4SrcIpAddr with u4SrcType  for filter ID %d\n",
                      i4FilterNo);
        return (CLI_FAILURE);
    }

    /*Test the user input for destination IP, mask */
    if (AclTestIpParams
        (ACL_DST, i4FilterNo, u4DestType, u4DestIpAddr,
         u4DestMask) == CLI_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        CliPrintf (CliHandle, "%%Invalid Destination IP Address. \r\n ");
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test destination Ip parameters with u4DestMask  and u4DestIpAddr with u4DestType  for filter ID %d\n",
                      i4FilterNo);
        return (CLI_FAILURE);
    }

    if ((i4Tos != ISS_TOS_INVALID) && (i4FilterNo > ACL_STD_MAX_VALUE))
    {
        if (nmhTestv2IssExtL3FilterTos (&u4ErrCode, i4FilterNo, i4Tos)
            == SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test  L3 Filter TOS  %d for Filter ID %d                                        with error code as %d\n",
                          i4Tos, i4FilterNo, u4ErrCode);
            return (CLI_FAILURE);
        }
    }

    if ((i4Dscp != ISS_DSCP_INVALID) && (i4FilterNo > ACL_STD_MAX_VALUE))
    {
        if (nmhTestv2IssExtL3FilterDscp (&u4ErrCode, i4FilterNo, i4Dscp)
            == SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test  L3 Filter Dscp  %d for Filter ID %d                                   with error code as %d\n",
                          i4Dscp, i4FilterNo, u4ErrCode);
            return (CLI_FAILURE);
        }
    }

    if (nmhTestv2IssExtL3FilterPriority (&u4ErrCode, i4FilterNo, i4Priority)
        == SNMP_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test  L3 Filter Priority  %d for Filter ID %d                                             with error code as %d\n",
                      i4Priority, i4FilterNo, u4ErrCode);
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL3FilterAction (i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Set L2 Filter Action %d for Filter ID %d\n",
                      i4Action, i4FilterNo);
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL3FilterProtocol (i4FilterNo, i4Protocol) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Set L2 Filter  Protocol  %d for Filter ID %d\n",
                      i4Protocol, i4FilterNo);
        return (CLI_FAILURE);
    }

    if (AclSetIpParams
        (ACL_SRC, i4FilterNo, u4SrcType, u4SrcIpAddr, u4SrcMask) == CLI_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to set source Ip parameters with u4SrcMask and u4SrcIpAddr  with u4SrcType  for filter ID %d\n",
                      i4FilterNo);
        return (CLI_FAILURE);
    }

    if (AclSetIpParams
        (ACL_DST, i4FilterNo, u4DestType, u4DestIpAddr,
         u4DestMask) == CLI_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to set destination Ip parameters with u4DestMask  and u4DestIpAddr  with u4DestType  for filter ID %d\n",
                      i4FilterNo);
        return (CLI_FAILURE);
    }

    if (i4Tos != ISS_TOS_INVALID)
    {
        if (nmhSetIssExtL3FilterTos (i4FilterNo, i4Tos) == SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set  L3 Filter Tos  %d for Filter ID %d\n",
                          i4Tos, i4FilterNo);
            return (CLI_FAILURE);
        }
    }

    if (i4Dscp != ISS_DSCP_INVALID)
    {
        if (nmhSetIssExtL3FilterDscp (i4FilterNo, i4Dscp) == SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set  L3 Filter Dscp  %d for Filter ID %d\n",
                          i4Dscp, i4FilterNo);
            return (CLI_FAILURE);
        }
    }
    if (nmhSetIssExtL3FilterPriority (i4FilterNo, i4Priority) == SNMP_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Set L3 Filter Priority  %d for Filter ID %d\n",
                      i4Priority, i4FilterNo);
        return (CLI_FAILURE);
    }
    if ((i4ActiveFlag == 1) && (i4Status == ISS_ACTIVE))
    {
        i4RetVal = nmhSetIssExtL3FilterDirection (i4FilterNo, i4Direction);
        if (i4RetVal == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to Set L3 Filter Direction %d for Filter ID %d\n",
                          i4Direction, i4FilterNo);
            return (CLI_FAILURE);
        }
        if (i4Direction == ACL_ACCESS_IN)
        {
            i4RetVal =
                nmhSetIssExtL3FilterInPortChannelList (i4FilterNo,
                                                       &InPortChannelList);
            if (i4RetVal == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter InPortChannelList  for Filter ID %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }
            i4RetVal = nmhSetIssExtL3FilterInPortList (i4FilterNo, &InPortList);
            if (i4RetVal == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter OutPortChannelList  for Filter ID %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }
        }
        else
        {
            i4RetVal =
                nmhSetIssExtL3FilterOutPortChannelList (i4FilterNo,
                                                        &OutPortChannelList);
            if (i4RetVal == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter OutPortChannelList  for Filter ID %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }
            i4RetVal =
                nmhSetIssExtL3FilterOutPortList (i4FilterNo, &OutPortList);
            if (i4RetVal == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter  OutPortList  for Filter ID%d \n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }
        }

        if (i4StatsOld == ACL_STAT_ENABLE)
        {
            if ((AclIpStatsSetEnabledStatus (CliHandle, i4FilterNo, i4StatsOld))
                == CLI_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_HW_UPDATE_FAILED);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to enable statistics for Filter ID %d \n",
                              i4FilterNo);
                return CLI_FAILURE;
            }
        }

        i4RetVal = nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_ACTIVE);
        if (i4RetVal == SNMP_FAILURE)
        {

            CLI_GET_ERR (&u4EnabledErrCode);
            if (u4EnabledErrCode != CLI_ACL_OUT_MULTIPLE_PORTS)
            {
                CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
            }
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3 Filter Status for Filter ID \n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }
    }
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclExtIpFilterConfig function \n");
    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclExtPbL3FilterConfig                             */
/*                                                                           */
/*     DESCRIPTION      : This function configures a extended IP ACL filter  */
/*                                                                           */
/*     INPUT            :  i4SVlan  - Service vlan Id                        */
/*                         i4SVlanPrio - Service Vlan Priority               */
/*                         i4CVlan - Cutomer Vlan Id                         */
/*                         i4CVlanPrio - Customer Vlan Priority              */
/*                         i4TagType - Packet Tag type on which the filter   */
/*                                     will be applied                       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
AclExtPbL3FilterConfig (tCliHandle CliHandle, INT4 i4SVlan,
                        INT4 i4SVlanPrio, INT4 i4CVlan,
                        INT4 i4CVlanPrio, INT4 i4TagType)
{

    INT4                i4FilterNo;
    UINT4               u4ErrCode;
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclExtPbL3FilterConfig function \n");
    i4FilterNo = CLI_GET_EXTACL ();

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4FilterNo);

    if ((pIssL3FilterEntry != NULL) &&
        (pIssL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE))
    {
        nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_NOT_IN_SERVICE);
    }

    if (AclPbTestL3Filter (&u4ErrCode, CliHandle, i4FilterNo, i4SVlan,
                           i4SVlanPrio, i4CVlan, i4CVlanPrio) == SNMP_FAILURE)
    {
        ISS_TRC_ARG6 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test L3 Filter CVlanPrio %d of CVlan %d and                                 SVlanPrio %d of SVlan %dfor filter ID %d with  error code as %d\n",
                      i4CVlanPrio, i4CVlan, i4SVlanPrio, i4SVlan, i4FilterNo,
                      u4ErrCode);
        nmhSetIssExtL3FilterStatus (i4FilterNo, ACTIVE);
        return (CLI_FAILURE);
    }
    if (AclPbSetL3Filter
        (CliHandle, i4FilterNo, i4SVlan, i4SVlanPrio, i4CVlan,
         i4CVlanPrio) == SNMP_FAILURE)
    {
        ISS_TRC_ARG5 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to set L3 Filter CVlanPrio %d of CVlan %d and                                 SVlanPrio %d of SVlan %dfor filter ID %d \n",
                      i4CVlanPrio, i4CVlan, i4SVlanPrio, i4SVlan, i4FilterNo);
        nmhSetIssExtL3FilterStatus (i4FilterNo, ACTIVE);
        return (CLI_FAILURE);
    }

    if (i4TagType != ISS_ZERO_ENTRY)
    {
        if (AclPbTestSetL3TagType (&u4ErrCode, CliHandle, i4FilterNo,
                                   i4TagType) == SNMP_FAILURE)
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test L3 Filter TagType %dfor filter ID %d                                    with  error code as %d\n",
                          i4TagType, i4FilterNo, u4ErrCode);
            nmhSetIssExtL3FilterStatus (i4FilterNo, ACTIVE);
            return (CLI_FAILURE);
        }
    }
    if (pIssL3FilterEntry->u1IssL3FilterStatus == ISS_NOT_IN_SERVICE)
    {
        nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_ACTIVE);
    }

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclExtPbL3FilterConfig function \n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclExtPbL2FilterConfig                             */
/*                                                                           */
/*     DESCRIPTION      : This function configures a extended IP ACL filter  */
/*                                                                           */
/*     INPUT            :  i4SVlan  - Service vlan Id                        */
/*                         i4SVlanPrio - Service Vlan Priority               */
/*                         i4CVlan - Cutomer Vlan Id                         */
/*                         i4CVlanPrio - Customer Vlan Priority              */
/*                         i4TagType - Packet Tag type on which the filter   */
/*                                     will be applied                       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
AclExtPbL2FilterConfig (tCliHandle CliHandle, INT4 i4OuterEType,
                        INT4 i4SVlan, INT4 i4SVlanPrio,
                        INT4 i4CVlanPrio, INT4 i4TagType)
{

    INT4                i4FilterNo;
    UINT4               u4ErrCode;
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclExtPbL2FilterConfig function \n");
    i4FilterNo = CLI_GET_MACACL ();

    pIssL2FilterEntry = IssExtGetL2FilterEntry (i4FilterNo);

    if ((pIssL2FilterEntry != NULL) &&
        (pIssL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE))
    {
        nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_NOT_IN_SERVICE);
    }

    if (AclPbTestL2Filter (&u4ErrCode, CliHandle, i4FilterNo, i4OuterEType,
                           i4SVlan, i4SVlanPrio, i4CVlanPrio) == SNMP_FAILURE)
    {
        ISS_TRC_ARG6 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test L2 Filter CVlanPrio %d  and                                                  SVlanPrio %d of SVlan %dand OuterEType %d for filter ID %d with  error code as %d\n",
                      i4CVlanPrio, i4SVlanPrio, i4SVlan, i4OuterEType,
                      i4FilterNo, u4ErrCode);
        return (CLI_FAILURE);
    }

    if (AclPbSetL2Filter (CliHandle, i4FilterNo, i4OuterEType,
                          i4SVlan, i4SVlanPrio, i4CVlanPrio) == SNMP_FAILURE)
    {
        ISS_TRC_ARG5 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test L2 Filter CVlanPrio %d  and                                                  SVlanPrio %d of SVlan %dand OuterEType %d for filter ID %d\n",
                      i4CVlanPrio, i4SVlanPrio, i4SVlan, i4OuterEType,
                      i4FilterNo);
        return (CLI_FAILURE);
    }

    if (i4TagType != 0)
    {
        if (AclPbTestSetL2TagType (&u4ErrCode, CliHandle, i4FilterNo,
                                   i4TagType) == SNMP_FAILURE)
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test L2 Filter TagType %dfor filter ID %d                                    with  error code as %d\n",
                          i4TagType, i4FilterNo, u4ErrCode);
            return (CLI_FAILURE);
        }
    }
    if (pIssL2FilterEntry->u1IssL2FilterStatus == ISS_NOT_IN_SERVICE)
    {
        nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_ACTIVE);
    }
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclExtPbL2FilterConfig function \n");
    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclExtIpFilterTcpUdpConfig                         */
/*                                                                           */
/*     DESCRIPTION      : This function configures a extended IP ACL filter  */
/*                         and enters the corresponding configuration mode   */
/*                                                                           */
/*     INPUT            :  i4Action -Permit/Deny                             */
/*                         i4Protocol - Protocol value                       */
/*                         u4SrcType - any/source network/ source host       */
/*                         u4SrcIp - Source IP Address                       */
/*                         u4SrcMask - Source IP Mask                        */
/*                         u4DestType - any/dest network/dest host           */
/*                         u4DestIp - Dest IP Address                        */
/*                         u4DestMask - Dest IP Mask                         */
/*                         i4Priority - Filter priority                      */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
AclExtIpFilterTcpUdpConfig (tCliHandle CliHandle, INT4 i4Action,
                            INT4 i4Protocol, UINT4 u4SrcType,
                            UINT4 u4SrcIpAddr, UINT4 u4SrcMask,
                            UINT4 u4SrcPortFlag, UINT4 u4SrcMinPort,
                            UINT4 u4SrcMaxPort, UINT4 u4DestType,
                            UINT4 u4DestIpAddr, UINT4 u4DestMask,
                            UINT4 u4DestPortFlag, UINT4 u4DestMinPort,
                            UINT4 u4DestMaxPort, UINT4 u4BitType,
                            INT4 i4Tos, INT4 i4Dscp, INT4 i4Priority)
{
    INT4                i4FilterNo;
    INT4                i4Status;
    INT4                i4ActionOld = 0;
    INT4                i4RedirectStatus = 0;
    UINT4               u4RedirectGrpId = 0;
    UINT4               u4ErrCode;
    INT4                i4ActiveFlag = 0;
    INT4                i4Direction;
    INT4                i4StatsOld = 0;
    INT1                i4RetVal = SNMP_SUCCESS;
    tSNMP_OCTET_STRING_TYPE InPortList;
    tSNMP_OCTET_STRING_TYPE OutPortList;
    UINT1               au1InPortList[ISS_PORTLIST_LEN];
    UINT1               au1OutPortList[ISS_PORTLIST_LEN];
    tSNMP_OCTET_STRING_TYPE InPortChannelList;
    tSNMP_OCTET_STRING_TYPE OutPortChannelList;
    UINT1               au1InPortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];
    UINT1               au1OutPortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];
    tIssL3FilterEntry   IssExtL3FilterEntry;
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;
    MEMSET (&IssExtL3FilterEntry, 0, sizeof (tIssL3FilterEntry));
    ISS_TRC (INIT_SHUT_TRC,
             "\nCLI:Entry AclExtIpFilterTcpUdpConfig  function \n");
    MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1OutPortList, 0, ISS_PORTLIST_LEN);
    ISS_MEMSET (&InPortList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    ISS_MEMSET (&OutPortList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    InPortList.i4_Length = ISS_PORTLIST_LEN;
    InPortList.pu1_OctetList = &au1InPortList[0];

    OutPortList.i4_Length = ISS_PORTLIST_LEN;
    OutPortList.pu1_OctetList = &au1OutPortList[0];

    MEMSET (au1InPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    MEMSET (au1OutPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    ISS_MEMSET (&InPortChannelList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    ISS_MEMSET (&OutPortChannelList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    InPortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;
    InPortChannelList.pu1_OctetList = &au1InPortChannelList[0];

    OutPortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;
    OutPortChannelList.pu1_OctetList = &au1OutPortChannelList[0];

    /*Get the IP Access list number from the current mode in CLI */
    i4FilterNo = CLI_GET_EXTACL ();
    if (nmhGetIssExtL3FilterStatus (i4FilterNo, &i4Status) == SNMP_FAILURE)
    {
        /* Filter does not exist */
        CliPrintf (CliHandle, "\r%% Invalid Filter number \r\n");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Retrieve L3 Filter Status %d for Filter ID %d\n",
                      i4Status, i4FilterNo);
        return (CLI_FAILURE);
    }
    /* Store the contents in temporary variable. In case of any failure, copy this
     * over the existing node to avoid intermediate updates
     */
    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        ISS_MEMCPY (&IssExtL3FilterEntry, pIssExtL3FilterEntry,
                    sizeof (tIssL3FilterEntry));
    }
    else
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (i4Status != ISS_ACTIVE)
    {
        AclResetL3Filter (i4FilterNo);
    }
    nmhGetIssAclL3FilterStatsEnabledStatus (i4FilterNo, &i4StatsOld);
    if (i4Status != ISS_NOT_READY)
    {
        if (i4Status == ISS_ACTIVE)
        {
            i4ActiveFlag = 1;
            nmhGetIssExtL3FilterDirection (i4FilterNo, &i4Direction);
            if (i4Direction == ACL_ACCESS_IN)
            {
                nmhGetIssExtL3FilterInPortList (i4FilterNo, &InPortList);
                nmhGetIssExtL3FilterInPortChannelList (i4FilterNo,
                                                       &InPortChannelList);

            }
            else
            {
                nmhGetIssExtL3FilterOutPortChannelList (i4FilterNo,
                                                        &OutPortChannelList);
                nmhGetIssExtL3FilterOutPortList (i4FilterNo, &OutPortList);
            }
        }
        /* Filter has previously been configured. This must be over-written
         * First destroy the filter and create again */

        if (nmhGetIssExtL3FilterAction (i4FilterNo, &i4ActionOld) ==
            SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to retrieve L3 Filter Action %d for Filter Id %d \n",
                          i4ActionOld, i4FilterNo);
            return (CLI_FAILURE);
        }

        if (i4ActionOld == ISS_REDIRECT_TO)
        {
            nmhGetIssAclL3FilterRedirectId (i4FilterNo,
                                            (INT4 *) &u4RedirectGrpId);

            if (u4RedirectGrpId != 0)
            {

                if (nmhGetIssRedirectInterfaceGrpStatus (u4RedirectGrpId,
                                                         &i4RedirectStatus) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to retrieve Redirect Interface Status %d for Group Id %d \n",
                                  i4RedirectStatus, u4RedirectGrpId);
                    return (CLI_FAILURE);
                }

                if (i4RedirectStatus != 0)
                {

                    if (nmhSetIssRedirectInterfaceGrpStatus
                        (u4RedirectGrpId, ISS_DESTROY) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to set RedirectInterface Status for Group Id %d \n",
                                      u4RedirectGrpId);
                        return (CLI_FAILURE);
                    }
                }
            }
        }
        if (nmhTestv2IssExtL3FilterStatus (&u4ErrCode, i4FilterNo, ISS_DESTROY)
            == SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test L3 Filter Status for filter ID %d with  error code as %d\n",
                          i4FilterNo, u4ErrCode);
            return (CLI_FAILURE);
        }

        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3Filter Status for filter ID %d\n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }
        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3Filter Status for filter ID %d\n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }

    }
    if (nmhTestv2IssExtL3FilterAction (&u4ErrCode,
                                       i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test L3 Filter Action %d for Filter ID %d                              with  error code as %d\n",
                      i4Action, i4FilterNo, u4ErrCode);
        return (CLI_FAILURE);
    }

    if (nmhTestv2IssExtL3FilterProtocol (&u4ErrCode,
                                         i4FilterNo,
                                         i4Protocol) == SNMP_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test L3 Filter Protocol %d for Filter ID %d                                  with  error code as %d\n",
                      i4Protocol, i4FilterNo, u4ErrCode);
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL3FilterProtocol (i4FilterNo, i4Protocol) == SNMP_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Set L2 Filter  Protocol  %d for Filter ID %d\n",
                      i4Protocol, i4FilterNo);
        return (CLI_FAILURE);
    }

    /*Test the user input for source IP, mask */
    if (AclTestIpParams
        (ACL_SRC, i4FilterNo, u4SrcType, u4SrcIpAddr, u4SrcMask) == CLI_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        CliPrintf (CliHandle, "%%Invalid Source IP Address. \r\n ");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test source Ip parameters with u4SrcMask and u4SrcIpAddr  with u4SrcType  for filter ID %d\n",
                      u4SrcType, i4FilterNo);
        return (CLI_FAILURE);
    }

    if ((u4SrcPortFlag == TRUE) && ((i4Protocol == ISS_PROT_TCP)
                                    || (i4Protocol == ISS_PROT_UDP)))
    {
        if (nmhTestv2IssExtL3FilterMinSrcProtPort
            (&u4ErrCode, i4FilterNo, u4SrcMinPort) == SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test L3 Filter  Min Src Port%d for Filter ID %d with  error code as %d\n",
                          u4SrcMinPort, i4FilterNo, u4ErrCode);
            return (CLI_FAILURE);
        }
        if (nmhTestv2IssExtL3FilterMaxSrcProtPort
            (&u4ErrCode, i4FilterNo, u4SrcMaxPort) == SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test L3 Filter Max SrcPort%d for Filter ID %d with  error code as %d\n",
                          u4SrcMaxPort, i4FilterNo, u4ErrCode);
            return (CLI_FAILURE);
        }
    }

    /*Test the user input for destination IP, mask */
    if (AclTestIpParams
        (ACL_DST, i4FilterNo, u4DestType, u4DestIpAddr,
         u4DestMask) == CLI_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        CliPrintf (CliHandle, "%%Invalid Destination IP Address. \r\n ");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test destination Ip parameters with u4DestMask  and u4DestIpAddr with u4DestType%d  for filter ID %d\n",
                      u4DestType, i4FilterNo);
        return (CLI_FAILURE);
    }

    if ((u4DestPortFlag == TRUE) && ((i4Protocol == ISS_PROT_TCP)
                                     || (i4Protocol == ISS_PROT_UDP)))
    {
        if (nmhTestv2IssExtL3FilterMinDstProtPort
            (&u4ErrCode, i4FilterNo, u4DestMinPort) == SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test L3 Filter  Min Dest Port%d for Filter ID %d with  error code as %d\n",
                          u4DestMinPort, i4FilterNo, u4ErrCode);
            return (CLI_FAILURE);
        }
        if (nmhTestv2IssExtL3FilterMaxDstProtPort
            (&u4ErrCode, i4FilterNo, u4DestMaxPort) == SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test L3 Filter  Max Dest Port%d for Filter ID %d with  error code as %d\n",
                          u4DestMaxPort, i4FilterNo, u4ErrCode);
            return (CLI_FAILURE);
        }
    }

    if ((u4BitType == ACL_ACK) && (i4Protocol == ISS_PROT_TCP))
    {
        /* filter TCP ACK bits */
        if (nmhTestv2IssExtL3FilterAckBit (&u4ErrCode, i4FilterNo,
                                           ISS_ACK_ESTABLISH) == SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test L3 filter AckBit for filter ID %d with  error code as %d\n",
                          i4FilterNo, u4ErrCode);
            return (CLI_FAILURE);
        }
    }
    else if ((u4BitType == ACL_RST) && (i4Protocol == ISS_PROT_TCP))
    {
        /* filter TCP RST bits */
        if (nmhTestv2IssExtL3FilterRstBit
            (&u4ErrCode, i4FilterNo, ISS_RST_SET) == SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test L3 filter Rstbit for filter ID %d with  error code as %d\n",
                          i4FilterNo, u4ErrCode);
            return (CLI_FAILURE);
        }
    }

    if ((i4Tos != ISS_TOS_INVALID) && (i4FilterNo > ACL_STD_MAX_VALUE))
    {
        if (nmhTestv2IssExtL3FilterTos (&u4ErrCode, i4FilterNo, i4Tos)
            == SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test  L3 Filter TOS  %d for Filter ID %d                                        with error code as %d\n",
                          i4Tos, i4FilterNo, u4ErrCode);
            return (CLI_FAILURE);
        }
    }
    if ((i4Dscp != ISS_DSCP_INVALID) && (i4FilterNo > ACL_STD_MAX_VALUE))
    {
        if (nmhTestv2IssExtL3FilterDscp (&u4ErrCode, i4FilterNo, i4Dscp)
            == SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test  L3 Filter Dscp  %d for Filter ID %d                                   with error code as %d\n",
                          i4Dscp, i4FilterNo, u4ErrCode);
            return (CLI_FAILURE);
        }
    }
    if (nmhTestv2IssExtL3FilterPriority (&u4ErrCode, i4FilterNo, i4Priority)
        == SNMP_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test  L3 Filter Priority  %d for Filter ID %d                                             with error code as %d\n",
                      i4Priority, i4FilterNo, u4ErrCode);
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL3FilterAction (i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Set L2 Filter Action %d for Filter ID %d\n",
                      i4Action, i4FilterNo);
        return (CLI_FAILURE);
    }

    if (AclSetIpParams
        (ACL_SRC, i4FilterNo, u4SrcType, u4SrcIpAddr, u4SrcMask) == CLI_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to set source Ip parameters with u4SrcMask and u4SrcIpAddr with u4SrcType  for filter ID %d\n",
                      u4SrcType, i4FilterNo);
        return (CLI_FAILURE);
    }

    if ((u4SrcPortFlag == TRUE) && ((i4Protocol == ISS_PROT_TCP)
                                    || (i4Protocol == ISS_PROT_UDP)))
    {
        if (nmhSetIssExtL3FilterMinSrcProtPort (i4FilterNo, u4SrcMinPort)
            == SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test L3 Filter  Min Src Port%d for Filter ID %d \n",
                          u4SrcMinPort, i4FilterNo);
            return (CLI_FAILURE);
        }
        if (nmhSetIssExtL3FilterMaxSrcProtPort (i4FilterNo, u4SrcMaxPort)
            == SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test L3 Filter Max SrcPort%d for Filter ID %d \n",
                          u4SrcMaxPort, i4FilterNo);
            return (CLI_FAILURE);
        }
    }

    if (AclSetIpParams
        (ACL_DST, i4FilterNo, u4DestType, u4DestIpAddr,
         u4DestMask) == CLI_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to set destination Ip parameters with u4DestMask and u4DestIpAddr with u4DestType%d  for filter ID %d\n",
                      u4DestType, i4FilterNo);
        return (CLI_FAILURE);
    }

    if ((u4DestPortFlag == TRUE) && ((i4Protocol == ISS_PROT_TCP)
                                     || (i4Protocol == ISS_PROT_UDP)))
    {
        if (nmhSetIssExtL3FilterMinDstProtPort (i4FilterNo, u4DestMinPort)
            == SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3 Filter  Min Dest Port%d for Filter ID %d \n",
                          u4DestMinPort, i4FilterNo);
            return (CLI_FAILURE);
        }
        if (nmhSetIssExtL3FilterMaxDstProtPort (i4FilterNo, u4DestMaxPort)
            == SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3 Filter  Max Dest Port%d for Filter ID %d \n",
                          u4DestMaxPort, i4FilterNo);
            return (CLI_FAILURE);
        }
    }

    if ((i4Tos != ISS_TOS_INVALID) && (i4FilterNo > ACL_STD_MAX_VALUE))
    {
        if (nmhSetIssExtL3FilterTos (i4FilterNo, i4Tos) == SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test  L3 Filter TOS  %d for Filter ID %d\n",
                          i4Tos, i4FilterNo);
            return (CLI_FAILURE);
        }
    }

    if ((i4Dscp != ISS_DSCP_INVALID) && (i4FilterNo > ACL_STD_MAX_VALUE))
    {
        if (nmhSetIssExtL3FilterDscp (i4FilterNo, i4Dscp) == SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set  L3 Filter Dscp  %d for Filter ID %d\n",
                          i4Dscp, i4FilterNo);
            return (CLI_FAILURE);
        }
    }
    if (nmhSetIssExtL3FilterPriority (i4FilterNo, i4Priority) == SNMP_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Set L3 Filter Priority  %d for Filter ID %d\n",
                      i4Priority, i4FilterNo);
        return (CLI_FAILURE);
    }

    if ((u4BitType == ACL_ACK) && (i4Protocol == ISS_PROT_TCP))
    {
        /* Filter TCP ACK bits */
        if (nmhSetIssExtL3FilterAckBit (i4FilterNo, ISS_ACK_ESTABLISH) ==
            SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3 filter AckBit for filter ID %d\n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }
    }

    else if ((u4BitType == ACL_RST) && (i4Protocol == ISS_PROT_TCP))
    {
        /* Filter TCP RST bits */
        if (nmhSetIssExtL3FilterRstBit (i4FilterNo, ISS_RST_SET) ==
            SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3 filter Rst Bit for filter ID %d\n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }
    }

    if ((i4ActiveFlag == 1) && (i4Status == ISS_ACTIVE))
    {
        i4RetVal = nmhSetIssExtL3FilterDirection (i4FilterNo, i4Direction);
        if (i4RetVal == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to Set L3 Filter Direction %d for Filter ID %d\n",
                          i4Direction, i4FilterNo);
            return (CLI_FAILURE);
        }
        if (i4Direction == ACL_ACCESS_IN)
        {
            i4RetVal =
                nmhSetIssExtL3FilterInPortChannelList (i4FilterNo,
                                                       &InPortChannelList);
            if (i4RetVal == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter InPortChannelList  for Filter ID %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }

            i4RetVal = nmhSetIssExtL3FilterInPortList (i4FilterNo, &InPortList);
            if (i4RetVal == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter InPortList for Filter ID %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }
        }
        else
        {
            i4RetVal =
                nmhSetIssExtL3FilterOutPortChannelList (i4FilterNo,
                                                        &OutPortChannelList);
            if (i4RetVal == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter OutPortChannelList  for Filter ID %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }
            i4RetVal =
                nmhSetIssExtL3FilterOutPortList (i4FilterNo, &OutPortList);
            if (i4RetVal == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter  OutPortList  for Filter ID%d \n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }
        }
        if (i4StatsOld == ACL_STAT_ENABLE)
        {
            if ((AclIpStatsSetEnabledStatus (CliHandle, i4FilterNo, i4StatsOld))
                == CLI_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_HW_UPDATE_FAILED);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to enable statistics for Filter ID %d \n",
                              i4FilterNo);
                return CLI_FAILURE;
            }
        }
        i4RetVal = nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_ACTIVE);
        if (i4RetVal == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3 Filter Status for Filter ID \n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nCLI:Exit AclExtIpFilterTcpUdpConfig function \n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclExtIpFilterIcmpConfig                           */
/*                                                                           */
/*     DESCRIPTION      : This function configures a extended IP ACL filter  */
/*                         and enters the corresponding configuration mode   */
/*                                                                           */
/*     INPUT            :  i4Action -Permit/Deny                             */
/*                         u4SrcType - any/source network/ source host       */
/*                         u4SrcIp - Source IP Address                       */
/*                         u4SrcMask - Source IP Mask                        */
/*                         u4DestType - any/dest network/dest host           */
/*                         u4DestIp - Dest IP Address                        */
/*                         u4DestMask - Dest IP Mask                         */
/*                         u4MesageType, u4MessageCode - ICMP details        */
/*                         i4Priority - Filter priority                      */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
AclExtIpFilterIcmpConfig (tCliHandle CliHandle, INT4 i4Action,
                          UINT4 u4SrcType, UINT4 u4SrcIpAddr,
                          UINT4 u4SrcMask, UINT4 u4DestType,
                          UINT4 u4DestIpAddr, UINT4 u4DestMask,
                          INT4 i4MessageType, INT4 i4MessageCode,
                          INT4 i4Priority)
{
    INT4                i4FilterNo;
    INT4                i4Status;
    INT4                i4ActionOld = 0;
    INT4                i4RedirectStatus = 0;
    UINT4               u4RedirectGrpId = 0;
    UINT4               u4ErrCode;
    INT4                i4ActiveFlag = 0;
    INT4                i4Direction;
    INT4                i4StatsOld = 0;
    INT1                i4RetVal = SNMP_SUCCESS;
    tSNMP_OCTET_STRING_TYPE InPortList;
    tSNMP_OCTET_STRING_TYPE OutPortList;
    UINT1               au1InPortList[ISS_PORTLIST_LEN];
    UINT1               au1OutPortList[ISS_PORTLIST_LEN];
    tSNMP_OCTET_STRING_TYPE InPortChannelList;
    tSNMP_OCTET_STRING_TYPE OutPortChannelList;
    UINT1               au1InPortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];
    UINT1               au1OutPortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];
    tIssL3FilterEntry   IssExtL3FilterEntry;
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;
    MEMSET (&IssExtL3FilterEntry, 0, sizeof (tIssL3FilterEntry));

    ISS_TRC (INIT_SHUT_TRC,
             "\nCLI:Entry AclExtIpFilterIcmpConfig  function \n");
    MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1OutPortList, 0, ISS_PORTLIST_LEN);
    ISS_MEMSET (&InPortList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    ISS_MEMSET (&OutPortList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    InPortList.i4_Length = ISS_PORTLIST_LEN;
    InPortList.pu1_OctetList = &au1InPortList[0];

    OutPortList.i4_Length = ISS_PORTLIST_LEN;
    OutPortList.pu1_OctetList = &au1OutPortList[0];

    MEMSET (au1InPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    MEMSET (au1OutPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    ISS_MEMSET (&InPortChannelList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    ISS_MEMSET (&OutPortChannelList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    InPortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;
    InPortChannelList.pu1_OctetList = &au1InPortChannelList[0];

    OutPortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;
    OutPortChannelList.pu1_OctetList = &au1OutPortChannelList[0];

    /*Get the IP Access list number from the current mode in CLI */
    i4FilterNo = CLI_GET_EXTACL ();

    if (nmhGetIssExtL3FilterStatus (i4FilterNo, &i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    /* Store the contents in temporary variable. In case of any failure, copy this
     * over the existing node to avoid intermediate updates
     */
    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        ISS_MEMCPY (&IssExtL3FilterEntry, pIssExtL3FilterEntry,
                    sizeof (tIssL3FilterEntry));
    }
    else
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (i4Status != ISS_ACTIVE)
    {
        AclResetL3Filter (i4FilterNo);
    }
    nmhGetIssAclL3FilterStatsEnabledStatus (i4FilterNo, &i4StatsOld);
    if (i4Status != ISS_NOT_READY)
    {
        if (i4Status == ISS_ACTIVE)
        {
            i4ActiveFlag = 1;
            nmhGetIssExtL3FilterDirection (i4FilterNo, &i4Direction);
            if (i4Direction == ACL_ACCESS_IN)
            {
                nmhGetIssExtL3FilterInPortList (i4FilterNo, &InPortList);
                nmhGetIssExtL3FilterInPortChannelList (i4FilterNo,
                                                       &InPortChannelList);

            }
            else
            {
                nmhGetIssExtL3FilterOutPortChannelList (i4FilterNo,
                                                        &OutPortChannelList);
                nmhGetIssExtL3FilterOutPortList (i4FilterNo, &OutPortList);
            }
        }
        /* Filter has previously been configured. This must be over-written
         * First destroy the filter and create again */
        if (nmhGetIssExtL3FilterAction (i4FilterNo, &i4ActionOld) ==
            SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to retrieve L3 Filter Action %d for Filter Id %d \n",
                          i4ActionOld, i4FilterNo);
            return (CLI_FAILURE);
        }

        if (i4ActionOld == ISS_REDIRECT_TO)
        {
            nmhGetIssAclL3FilterRedirectId (i4FilterNo,
                                            (INT4 *) &u4RedirectGrpId);

            if (u4RedirectGrpId != 0)
            {

                if (nmhGetIssRedirectInterfaceGrpStatus (u4RedirectGrpId,
                                                         &i4RedirectStatus) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to retrieve Redirect Interface Status %d for Group Id %d \n",
                                  i4RedirectStatus, u4RedirectGrpId);
                    return (CLI_FAILURE);
                }

                if (i4RedirectStatus == ISS_ACTIVE)
                {

                    if (nmhSetIssRedirectInterfaceGrpStatus
                        (u4RedirectGrpId, ISS_DESTROY) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to set RedirectInterface Status for Group Id %d \n",
                                      u4RedirectGrpId);
                        return (CLI_FAILURE);
                    }
                }
            }
        }
        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3Filter Status for filter ID %d\n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }
        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3Filter Status for filter ID %d\n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }

    }

    if (nmhTestv2IssExtL3FilterAction (&u4ErrCode,
                                       i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test L3 Filter Action %d for Filter ID %d                              with  error code as %d\n",
                      i4Action, i4FilterNo, u4ErrCode);
        return (CLI_FAILURE);
    }

    /*Test the user input for source IP, mask */

    if (AclTestIpParams
        (ACL_SRC, i4FilterNo, u4SrcType, u4SrcIpAddr, u4SrcMask) == CLI_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        CliPrintf (CliHandle, "%%Invalid Source IP Address. \r\n ");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test source Ip parameters with u4SrcMask and u4SrcIpAddr with u4SrcType  for filter ID %d\n",
                      u4SrcType, i4FilterNo);
        return (CLI_FAILURE);
    }

    /*Test the user input for destination IP, mask */
    if (AclTestIpParams
        (ACL_DST, i4FilterNo, u4DestType, u4DestIpAddr,
         u4DestMask) == CLI_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test destination Ip parameters with u4DestMask and u4DestIpAddr  with u4DestType%d  for filter ID %d\n",
                      u4DestType, i4FilterNo);
        return (CLI_FAILURE);
    }

    if (nmhTestv2IssExtL3FilterProtocol (&u4ErrCode,
                                         i4FilterNo,
                                         ISS_PROT_ICMP) == SNMP_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test L3 Filter protocol for Filter ID %d with  error code as %d\n",
                      i4FilterNo, u4ErrCode);
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL3FilterProtocol (i4FilterNo, ISS_PROT_ICMP) ==
        SNMP_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to set L3Filter Protocol for filter ID %d\n",
                      i4FilterNo);
        return (CLI_FAILURE);
    }

    if (i4MessageType != ISS_DEFAULT_MSG_TYPE)
    {
        if (nmhTestv2IssExtL3FilterMessageType
            (&u4ErrCode, i4FilterNo, i4MessageType) == SNMP_FAILURE)

        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            /* ICMP Message Type */
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test L3 Filter MessageType %d for filter ID %d with  error code as %d\n",
                          i4MessageType, i4FilterNo, u4ErrCode);
            CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
            return (CLI_FAILURE);
        }
    }

    if (i4MessageCode != ISS_DEFAULT_MSG_CODE)
    {
        if (nmhTestv2IssExtL3FilterMessageCode
            (&u4ErrCode, i4FilterNo, i4MessageCode) == SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            /* ICMP Message Code */
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test L3 Filter Message code %d for filter ID %d with  error code as %d\n",
                          i4MessageCode, i4FilterNo, u4ErrCode);
            CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
            return (CLI_FAILURE);
        }
    }
    if (nmhSetIssExtL3FilterAction (i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to set L3 Filter Action %d for Filter ID %d\n",
                      i4Action, i4FilterNo);
        return (CLI_FAILURE);
    }
    /* Filter Priority */
    if (nmhTestv2IssExtL3FilterPriority (&u4ErrCode, i4FilterNo, i4Priority)
        == SNMP_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test  L3 Filter Priority  %d for Filter ID %d                                             with error code as %d\n",
                      i4Priority, i4FilterNo, u4ErrCode);
        return (CLI_FAILURE);
    }

    if (AclSetIpParams
        (ACL_SRC, i4FilterNo, u4SrcType, u4SrcIpAddr, u4SrcMask) == CLI_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to set source Ip parameters with u4SrcMask  and u4SrcIpAddr with u4SrcType  for filter ID %d\n",
                      u4SrcType, i4FilterNo);
        return (CLI_FAILURE);
    }

    if (AclSetIpParams
        (ACL_DST, i4FilterNo, u4DestType, u4DestIpAddr,
         u4DestMask) == CLI_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to set destination Ip parameters with u4DestMask and u4DestIpAddr with u4DestType  for filter ID %d\n",
                      u4DestType, i4FilterNo);
        return (CLI_FAILURE);
    }

    if (i4MessageType != ISS_DEFAULT_MSG_TYPE)
    {
        if (nmhSetIssExtL3FilterMessageType (i4FilterNo, i4MessageType)
            == SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            /* ICMP Message Type */
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3 Filter MessageType %d for filter ID %d\n",
                          i4MessageType, i4FilterNo);
            return (CLI_FAILURE);
        }
    }

    if (i4MessageCode != ISS_DEFAULT_MSG_CODE)
    {
        if (nmhSetIssExtL3FilterMessageCode (i4FilterNo, i4MessageCode)
            == SNMP_FAILURE)
        {
            if (pIssExtL3FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                            sizeof (tIssL3FilterEntry));
            }
            /* ICMP Message Code */
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3 Filter Message code %d for filter ID %d\n",
                          i4MessageCode, i4FilterNo);
            return (CLI_FAILURE);
        }
    }
    if (nmhSetIssExtL3FilterPriority (i4FilterNo, i4Priority) == SNMP_FAILURE)
    {
        if (pIssExtL3FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL3FilterEntry, &IssExtL3FilterEntry,
                        sizeof (tIssL3FilterEntry));
        }
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to set L3 Filter Priority %d for filter ID %d\n",
                      i4Priority, i4FilterNo);
        return (CLI_FAILURE);
    }
    if ((i4ActiveFlag == 1) && (i4Status == ISS_ACTIVE))
    {
        i4RetVal = nmhSetIssExtL3FilterDirection (i4FilterNo, i4Direction);
        if (i4RetVal == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3 Filter Direction %d for filter ID %d\n",
                          i4Direction, i4FilterNo);
            return (CLI_FAILURE);
        }
        if (i4Direction == ACL_ACCESS_IN)
        {
            i4RetVal =
                nmhSetIssExtL3FilterInPortChannelList (i4FilterNo,
                                                       &InPortChannelList);
            if (i4RetVal == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter InPortChannelList  for Filter ID %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }

            i4RetVal = nmhSetIssExtL3FilterInPortList (i4FilterNo, &InPortList);
            if (i4RetVal == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter InPortList  for filter ID %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }
        }
        else
        {
            i4RetVal =
                nmhSetIssExtL3FilterOutPortChannelList (i4FilterNo,
                                                        &OutPortChannelList);
            if (i4RetVal == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter OutPortChannelList  for Filter ID %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }
            i4RetVal =
                nmhSetIssExtL3FilterOutPortList (i4FilterNo, &OutPortList);
            if (i4RetVal == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter OutPortList  for filter ID %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }
        }

        if (i4StatsOld == ACL_STAT_ENABLE)
        {
            if ((AclIpStatsSetEnabledStatus (CliHandle, i4FilterNo, i4StatsOld))
                == CLI_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_HW_UPDATE_FAILED);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to enable statistics for Filter ID %d \n",
                              i4FilterNo);
                return CLI_FAILURE;
            }
        }

        i4RetVal = nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_ACTIVE);
        if (i4RetVal == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3Filter Status for filter ID %d\n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }
    }
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclExtIpFilterIcmpConfig function \n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclExtIp6FilterIcmp6Config                         */
/*                                                                           */
/*     DESCRIPTION      : This function configures a standard IPv6 ACL filter*/
/*                         and enters the corresponding configuration mode   */
/*                                                                           */
/*     INPUT            :  i4Action - Permit/Deny                            */
/*                         u4SrcType - any/source network/ source host       */
/*                         SrcIp6Addr - Source IPv6 Address                  */
/*                         u4SrcPrefixLen - Source Prefix Len                */
/*                         u4DestType - any/dest network/dest host           */
/*                         DstIp6Addr - Dest IPv6 Address                    */
/*                         u4DestPrefixLen - Dest Prefix Len                 */
/*                         u4MesageType, u4MessageCode - ICMPv6 details      */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
AclExtIp6FilterIcmpConfig (tCliHandle CliHandle, UINT4 u4SrcType,
                           tIp6Addr SrcIp6Addr, UINT4 u4SrcPrefixLen,
                           UINT4 u4DstType, tIp6Addr DstIp6Addr,
                           UINT4 u4DstPrefixLen, INT4 i4MsgType,
                           INT4 i4MsgCode, INT4 i4Dscp, UINT4 u4FlowId,
                           INT4 i4Priority, INT4 i4Action)
{
    INT4                i4FilterNo = 0;
    INT4                i4Status = 0;
    UINT4               u4ErrCode;
    tSNMP_OCTET_STRING_TYPE SrcIpv6;
    tSNMP_OCTET_STRING_TYPE DstIpv6;
    UINT1               au1SrcIPv6[IP6_ADDR_SIZE];
    UINT1               au1DstIPv6[IP6_ADDR_SIZE];
    INT4                i4ActiveFlag = 0;
    INT4                i4Direction;
    INT1                i4RetVal = SNMP_SUCCESS;
    tSNMP_OCTET_STRING_TYPE InPortList;
    tSNMP_OCTET_STRING_TYPE OutPortList;
    UINT1               au1InPortList[ISS_PORTLIST_LEN];
    UINT1               au1OutPortList[ISS_PORTLIST_LEN];
    tSNMP_OCTET_STRING_TYPE InPortChannelList;
    tSNMP_OCTET_STRING_TYPE OutPortChannelList;
    UINT1               au1InPortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];
    UINT1               au1OutPortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];

    ISS_TRC (INIT_SHUT_TRC,
             "\nCLI:Entry AclExtIp6FilterIcmpConfig function \n");
    MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1OutPortList, 0, ISS_PORTLIST_LEN);
    ISS_MEMSET (&InPortList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    ISS_MEMSET (&OutPortList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    InPortList.i4_Length = ISS_PORTLIST_LEN;
    InPortList.pu1_OctetList = &au1InPortList[0];

    OutPortList.i4_Length = ISS_PORTLIST_LEN;
    OutPortList.pu1_OctetList = &au1OutPortList[0];

    MEMSET (au1InPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    MEMSET (au1OutPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    ISS_MEMSET (&InPortChannelList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    ISS_MEMSET (&OutPortChannelList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    InPortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;
    InPortChannelList.pu1_OctetList = &au1InPortChannelList[0];

    OutPortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;
    OutPortChannelList.pu1_OctetList = &au1OutPortChannelList[0];

    MEMSET (au1SrcIPv6, 0, sizeof (au1SrcIPv6));
    MEMSET (au1DstIPv6, 0, sizeof (au1DstIPv6));

    SrcIpv6.pu1_OctetList = au1SrcIPv6;
    DstIpv6.pu1_OctetList = au1DstIPv6;

    i4FilterNo = CLI_GET_EXTACL ();
    nmhGetIssAclL3FilterStatus (i4FilterNo, &i4Status);

    if (i4Status != ISS_NOT_READY)
    {
        if (i4Status == ISS_ACTIVE)
        {
            i4ActiveFlag = 1;
            nmhGetIssExtL3FilterDirection (i4FilterNo, &i4Direction);
            if (i4Direction == ACL_ACCESS_IN)
            {
                nmhGetIssExtL3FilterInPortList (i4FilterNo, &InPortList);
                nmhGetIssExtL3FilterInPortChannelList (i4FilterNo,
                                                       &InPortChannelList);

            }
            else
            {
                nmhGetIssExtL3FilterOutPortChannelList (i4FilterNo,
                                                        &OutPortChannelList);
                nmhGetIssExtL3FilterOutPortList (i4FilterNo, &OutPortList);
            }
        }
        /* Filter has previously been configured. This must be over-written 
         * First destroy the filter and create again */
        if (nmhTestv2IssAclL3FilterStatus (&u4ErrCode, i4FilterNo, ISS_DESTROY)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test L3 Filter Status for filter ID %d with  error code as %d\n",
                          i4FilterNo, u4ErrCode);
            return (CLI_FAILURE);
        }

        if (nmhSetIssAclL3FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3Filter Status for filter ID %d\n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }

        if (nmhSetIssAclL3FilterStatus (i4FilterNo, ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3Filter Status for filter ID %d\n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }

    }

    i4RetVal = nmhSetIssAclL3FilteAddrType (i4FilterNo, QOS_IPV6);

    if (nmhTestv2IssExtL3FilterProtocol (&u4ErrCode, i4FilterNo,
                                         ISS_PROT_ICMP6) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test L3 Filter protocol for Filter ID %d with  error code as %d\n",
                      i4FilterNo, u4ErrCode);
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL3FilterProtocol (i4FilterNo,
                                      ISS_PROT_ICMP6) == SNMP_FAILURE)
    {
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to set L3Filter Protocol for filter ID %d\n",
                      i4FilterNo);
        return (CLI_FAILURE);
    }

    if (i4MsgType != ISS_DEFAULT_MSG_TYPE)
    {
        if (nmhTestv2IssExtL3FilterMessageType
            (&u4ErrCode, i4FilterNo, i4MsgType) == SNMP_FAILURE)
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test L3 Filter MessageType %d for filter ID %d with  error code as %d\n",
                          i4MsgType, i4FilterNo, u4ErrCode);
            return (CLI_FAILURE);
        }
    }

    if (i4MsgCode != ISS_DEFAULT_MSG_CODE)
    {
        if (nmhTestv2IssExtL3FilterMessageCode
            (&u4ErrCode, i4FilterNo, i4MsgCode) == SNMP_FAILURE)
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test L3 Filter Message code %d for filter ID %d with  error code as %d\n",
                          i4MsgCode, i4FilterNo, u4ErrCode);
            return (CLI_FAILURE);
        }
    }

    if (i4Dscp != ISS_DSCP_INVALID)
    {
        if (nmhTestv2IssExtL3FilterDscp (&u4ErrCode, i4FilterNo, i4Dscp)
            == SNMP_FAILURE)
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test  L3 Filter Dscp  %d for Filter ID %d                                   with error code as %d\n",
                          i4Dscp, i4FilterNo, u4ErrCode);
            return CLI_FAILURE;
        }
    }
    if (u4FlowId != ISS_FLOWID_INVALID)
    {
        if (nmhTestv2IssAclL3FilterFlowId (&u4ErrCode, i4FilterNo, u4FlowId)
            == SNMP_FAILURE)
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test  L3 Filter FlowId  %d for Filter ID %d with error                  code as %d\n",
                          u4FlowId, i4FilterNo, u4ErrCode);
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2IssExtL3FilterPriority (&u4ErrCode, i4FilterNo, i4Priority)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test L3 Filter Priority %d for Filter ID %d                              with  error code as %d\n",
                      i4Priority, i4FilterNo, u4ErrCode);
        return CLI_FAILURE;
    }

    /* Set the action (permit/deny) accorring to the user config value */
    if (nmhTestv2IssExtL3FilterAction (&u4ErrCode,
                                       i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test L3 Filter Action %d for Filter ID %d                                      with  error code as %d\n",
                      i4Action, i4FilterNo, u4ErrCode);
        return (CLI_FAILURE);
    }

    if (u4SrcType != ACL_ANY)
    {
        MEMCPY (SrcIpv6.pu1_OctetList, &SrcIp6Addr, IP6_ADDR_SIZE);
        SrcIpv6.i4_Length = IP6_ADDR_SIZE;
        if (nmhTestv2IssAclL3FilterSrcIpAddr
            (&u4ErrCode, i4FilterNo, &SrcIpv6) == SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test  L3 Filter SrcIpv6 Address  for Filter ID %d                with error code as %d\n",
                          i4FilterNo, u4ErrCode);
            return CLI_FAILURE;
        }

        if (nmhSetIssAclL3FilterSrcIpAddr (i4FilterNo, &SrcIpv6) ==
            SNMP_FAILURE)
        {
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set  L3 Filter SrcIpv6 Address for Filter ID %d\n",
                          i4FilterNo);
            return CLI_FAILURE;
        }

        if (u4SrcType == ACL_HOST_IP_MASK)
        {
            if (nmhTestv2IssAclL3FilterSrcIpAddrPrefixLength (&u4ErrCode,
                                                              i4FilterNo,
                                                              u4SrcPrefixLen)
                == SNMP_FAILURE)
            {
                ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to test  L3 Filter Src Prefix Length  %d for Filter ID %d with             error code as %d\n",
                              u4SrcPrefixLen, i4FilterNo, u4ErrCode);
                return CLI_FAILURE;
            }
            if (nmhSetIssAclL3FilterSrcIpAddrPrefixLength (i4FilterNo,
                                                           u4SrcPrefixLen) ==
                SNMP_FAILURE)
            {
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set  L3 Filter Src Prefix Length  %d for Filter ID %d\n",
                              u4SrcPrefixLen, i4FilterNo);
                return CLI_FAILURE;
            }
        }
    }
    else
    {
        ISS_MEMSET (SrcIpv6.pu1_OctetList, 0, IP6_ADDR_SIZE);
        SrcIpv6.i4_Length = IP6_ADDR_SIZE;
        if (nmhSetIssAclL3FilterSrcIpAddr (i4FilterNo, &SrcIpv6) ==
            SNMP_FAILURE)
        {
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set  L3 Filter SrcIpv6 Address  for Filter ID %d\n",
                          i4FilterNo);
            return CLI_FAILURE;
        }

        u4SrcPrefixLen = ISS_DEF_PREFIX_LEN;
        if (nmhSetIssAclL3FilterSrcIpAddrPrefixLength (i4FilterNo,
                                                       u4SrcPrefixLen) ==
            SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set  L3 Filter Src Prefix Length  %d for Filter ID %d\n",
                          u4SrcPrefixLen, i4FilterNo);
            return CLI_FAILURE;
        }
    }

    if (u4DstType != ACL_ANY)
    {
        MEMCPY (DstIpv6.pu1_OctetList, &DstIp6Addr, IP6_ADDR_SIZE);
        DstIpv6.i4_Length = IP6_ADDR_SIZE;

        if (nmhTestv2IssAclL3FilterDstIpAddr
            (&u4ErrCode, i4FilterNo, &DstIpv6) == SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test  L3 Filter Dst Ipv6 Addr for Filter ID %d with error                  code as %d\n",
                          i4FilterNo, u4ErrCode);
            return CLI_FAILURE;
        }

        if (nmhSetIssAclL3FilterDstIpAddr (i4FilterNo, &DstIpv6) ==
            SNMP_FAILURE)
        {
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3 Filter Dst Ipv6 Addr for Filter ID %d\n",
                          i4FilterNo);
            return CLI_FAILURE;
        }
        if (u4DstType == ACL_HOST_IP_MASK)
        {
            if (nmhTestv2IssAclL3FilterDstIpAddrPrefixLength (&u4ErrCode,
                                                              i4FilterNo,
                                                              u4DstPrefixLen)
                == SNMP_FAILURE)
            {
                ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to test  L3 Filter Destination Prefix Length  %d for Filter ID %d with                                             error code as %d\n",
                              u4DstPrefixLen, i4FilterNo, u4ErrCode);
                return CLI_FAILURE;
            }
            if (nmhSetIssAclL3FilterDstIpAddrPrefixLength (i4FilterNo,
                                                           u4DstPrefixLen) ==
                SNMP_FAILURE)
            {
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set  L3 Filter Destination Prefix Length  %d for Filter ID %d\n",
                              u4DstPrefixLen, i4FilterNo);
                return CLI_FAILURE;
            }
        }
    }
    else
    {
        ISS_MEMSET (DstIpv6.pu1_OctetList, 0, IP6_ADDR_SIZE);
        DstIpv6.i4_Length = IP6_ADDR_SIZE;
        if (nmhSetIssAclL3FilterDstIpAddr (i4FilterNo, &DstIpv6) ==
            SNMP_FAILURE)
        {
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3 Filter Dst Ipv6 Addr  for Filter ID %d\n",
                          i4FilterNo);
            return CLI_FAILURE;
        }

        u4DstPrefixLen = ISS_DEF_PREFIX_LEN;
        if (nmhSetIssAclL3FilterDstIpAddrPrefixLength (i4FilterNo,
                                                       u4DstPrefixLen) ==
            SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set  L3 Filter Destination Prefix Length  %d for Filter ID %d\n",
                          u4DstPrefixLen, i4FilterNo);
            return CLI_FAILURE;
        }
    }

    if (i4MsgType != ISS_DEFAULT_MSG_TYPE)
    {
        if (nmhSetIssExtL3FilterMessageType (i4FilterNo, i4MsgType) ==
            SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3 Filter MessageType %d for filter ID %d\n",
                          i4MsgType, i4FilterNo);
            return (CLI_FAILURE);
        }
    }

    if (i4MsgCode != ISS_DEFAULT_MSG_CODE)
    {
        if (nmhSetIssExtL3FilterMessageCode (i4FilterNo, i4MsgCode) ==
            SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3 Filter Message code %d for filter ID %d\n",
                          i4MsgCode, i4FilterNo);
            return (CLI_FAILURE);
        }
    }

    if (i4Dscp != ISS_DSCP_INVALID)
    {
        if (nmhSetIssExtL3FilterDscp (i4FilterNo, i4Dscp) == SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set  L3 Filter Dscp  %d for Filter ID %d\n",
                          i4Dscp, i4FilterNo);
            return SNMP_FAILURE;
        }
    }
    if (u4FlowId != ISS_FLOWID_INVALID)
    {
        if (nmhSetIssAclL3FilterFlowId (i4FilterNo, u4FlowId) == SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set  L3 Filter FlowId  %d for Filter ID %d\n",
                          u4FlowId, i4FilterNo);
            return CLI_FAILURE;
        }
    }

    if (nmhSetIssExtL3FilterPriority (i4FilterNo, i4Priority) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Set L3 Filter Priority  %d for Filter ID %d\n",
                      i4Priority, i4FilterNo);
        return CLI_FAILURE;
    }

    /* Set the action (permit/deny) accorring to the user config value */
    if (nmhSetIssExtL3FilterAction (i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Set L3 Filter Action %d for Filter ID %d\n",
                      i4Action, i4FilterNo);
        return (CLI_FAILURE);
    }
    if ((i4ActiveFlag == 1) && (i4Status == ISS_ACTIVE))
    {
        i4RetVal = nmhSetIssExtL3FilterDirection (i4FilterNo, i4Direction);
        if (i4RetVal == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to Set L3 Filter Direction %d for Filter ID %d\n",
                          i4Direction, i4FilterNo);
            return (CLI_FAILURE);
        }
        if (i4Direction == ACL_ACCESS_IN)
        {
            i4RetVal =
                nmhSetIssExtL3FilterInPortChannelList (i4FilterNo,
                                                       &InPortChannelList);
            if (i4RetVal == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter InPortChannelList  for Filter ID %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }

            i4RetVal = nmhSetIssExtL3FilterInPortList (i4FilterNo, &InPortList);
            if (i4RetVal == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter InPortList  for Filter ID %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }
        }
        else
        {
            i4RetVal =
                nmhSetIssExtL3FilterOutPortChannelList (i4FilterNo,
                                                        &OutPortChannelList);
            if (i4RetVal == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter OutPortChannelList  for Filter ID %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }
            i4RetVal =
                nmhSetIssExtL3FilterOutPortList (i4FilterNo, &OutPortList);
            if (i4RetVal == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter  OutPortList  for Filter ID%d \n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }
        }
        i4RetVal = nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_ACTIVE);
        if (i4RetVal == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3 Filter Status for Filter ID \n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }
    }
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclExtIp6FilterIcmpConfig function \n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclIpAccessGroup                                   */
/*                                                                           */
/*     DESCRIPTION      : This function associated an IP acl to an interface */
/*                                                                           */
/*     INPUT            : i4FilterNo - IP ACL number                         */
/*                        i4Direction - In or out                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclIpAccessGroup (tCliHandle CliHandle, INT4 i4FilterNo,
                  INT4 i4Direction, INT4 i4Stats)
{
    UINT4               u4IfIndex;
    UINT4               u4L3RedirectIndex = 0;
    UINT1               au1PortList[ISS_PORTLIST_LEN];
    UINT1               au1OldPortList[ISS_PORTLIST_LEN];
    UINT1               au1PortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];
    UINT1               au1OldPortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];
    tSNMP_OCTET_STRING_TYPE PortList;
    tSNMP_OCTET_STRING_TYPE OldPortList;
    tSNMP_OCTET_STRING_TYPE PortChannelList;
    tSNMP_OCTET_STRING_TYPE OldPortChannelList;
    UINT4               u4ErrCode;
    UINT4               u4SChannelIndex = 0;
    INT4                i4Status;
    INT4                i4DirectionStatus;
    INT4                i4ActionOld = 0;
    INT4                i4RedirectStatus = 0;
    INT1                i4RetVal = 0;
    INT4                i4StatsOld = ACL_STAT_ENABLE;

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclIpAccessGroup function \n");
    MEMSET (au1PortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1OldPortList, 0, ISS_PORTLIST_LEN);

    PortList.i4_Length = ISS_PORTLIST_LEN;
    PortList.pu1_OctetList = &au1PortList[0];

    OldPortList.i4_Length = ISS_PORTLIST_LEN;
    OldPortList.pu1_OctetList = &au1OldPortList[0];

    PortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;
    PortChannelList.pu1_OctetList = &au1PortChannelList[0];

    OldPortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;
    OldPortChannelList.pu1_OctetList = &au1OldPortChannelList[0];

    /*Get the interface index from the current mode in CLI */
    u4IfIndex = CLI_GET_IFINDEX ();
    if ((nmhGetIssExtL3FilterStatus (i4FilterNo, &i4Status)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid IP Filter \r\n");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Retrieve L3 Filter Status %d for Filter ID %d\n",
                      i4Status, i4FilterNo);
        return (CLI_FAILURE);
    }
    if (nmhGetIssExtL3FilterDirection (i4FilterNo, &i4DirectionStatus)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Retrieve L3 Filter  Direction Status %d for Filter ID %d\n",
                      i4DirectionStatus, i4FilterNo);
        return CLI_FAILURE;
    }
    if (i4Status == ISS_ACTIVE && i4DirectionStatus != i4Direction)
    {
        CliPrintf (CliHandle,
                   "\r%% Filter is already active in alternate direction \r\n");

        return CLI_FAILURE;
    }

    /* If Action is Redirection for Filter then call our API */
    if (nmhGetIssExtL3FilterAction (i4FilterNo, &i4ActionOld) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to retrieve  L3 Filter Action %d for Filter ID %d\n",
                      i4ActionOld, i4FilterNo);
        return (CLI_FAILURE);
    }

    if ((i4Direction == ACL_ACCESS_OUT)
        && (i4ActionOld == ISS_SWITCH_COPYTOCPU
            || i4ActionOld == ISS_DROP_COPYTOCPU))
    {

        CliPrintf (CliHandle,
                   "\r%% Copy to CPU Is Not Applicable For Egress Port\r\n");
        return CLI_FAILURE;
    }

    if ((i4Direction != ACL_ACCESS_IN) && (i4ActionOld == ISS_REDIRECT_TO))
    {
        CliPrintf (CliHandle,
                   "\r%% Redirect Filter Not Applicable For OutPort \r\n");
        return CLI_FAILURE;
    }

    if (i4ActionOld == ISS_REDIRECT_TO)
    {
        ISS_REDIRECT_ID_GET (i4FilterNo, u4L3RedirectIndex);
        if (u4L3RedirectIndex == 0)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:L3 Redirect Index retrieve  zero %d for Filter ID %d\n",
                          u4L3RedirectIndex, i4FilterNo);
            return CLI_FAILURE;
        }
    }

    if ((u4IfIndex >= CFA_MIN_EVB_SBP_INDEX) &&
        (u4IfIndex <= CFA_MAX_EVB_SBP_INDEX))
    {
        if (i4Direction == ACL_ACCESS_IN)
        {
            nmhGetIssAclL3FilterSChannelIfIndex (i4FilterNo,
                                                 (INT4 *) &u4SChannelIndex);
        }
    }

    /* check for physical or port channel interface */
    else if ((u4IfIndex > BRG_MAX_PHY_PORTS)
             && (u4IfIndex <= BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        /* Getting the PortChannel List already present for this filter
         * and that is being copied to PortChannel List */
        if (i4Direction == ACL_ACCESS_IN)
        {
            nmhGetIssExtL3FilterInPortChannelList (i4FilterNo,
                                                   &PortChannelList);
        }
        else
        {
            nmhGetIssExtL3FilterOutPortChannelList (i4FilterNo,
                                                    &PortChannelList);
        }
        MEMCPY (OldPortChannelList.pu1_OctetList, PortChannelList.pu1_OctetList,
                PortChannelList.i4_Length);
        OldPortChannelList.i4_Length = PortChannelList.i4_Length;

        ACL_ADD_PORT_LIST (au1PortChannelList, u4IfIndex);
    }
    else
    {
        if (i4Direction == ACL_ACCESS_IN)
        {
            nmhGetIssExtL3FilterInPortList (i4FilterNo, &PortList);
        }
        else
        {
            nmhGetIssExtL3FilterOutPortList (i4FilterNo, &PortList);
        }
        MEMCPY (OldPortList.pu1_OctetList, PortList.pu1_OctetList,
                PortList.i4_Length);
        OldPortList.i4_Length = PortList.i4_Length;

        ACL_ADD_PORT_LIST (au1PortList, u4IfIndex);
    }

    if (i4Status == ISS_ACTIVE)
    {
        if (nmhTestv2IssExtL3FilterStatus
            (&u4ErrCode, i4FilterNo, ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test L3 Filter Status for filter ID %d with  error code as %d\n",
                          i4FilterNo, u4ErrCode);
            return (CLI_FAILURE);
        }

        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L3Filter Status for filter ID %d\n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }
        if (u4L3RedirectIndex != 0)
        {
            nmhGetIssRedirectInterfaceGrpStatus (u4L3RedirectIndex,
                                                 &i4RedirectStatus);

            if (i4RedirectStatus == ISS_ACTIVE)
            {
                if (nmhTestv2IssRedirectInterfaceGrpStatus (&u4ErrCode,
                                                            u4L3RedirectIndex,
                                                            ISS_NOT_IN_SERVICE)
                    == SNMP_FAILURE)
                {
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to test L3 Filter RedirectIndex %d with  error code as %d\n",
                                  u4L3RedirectIndex, u4ErrCode);
                    return (CLI_FAILURE);
                }

                if (nmhSetIssRedirectInterfaceGrpStatus (u4L3RedirectIndex,
                                                         ISS_NOT_IN_SERVICE) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to test L3 Filter RedirectIndex %d\n",
                                  u4L3RedirectIndex);
                    return (CLI_FAILURE);
                }
            }
        }
    }
    if (nmhTestv2IssExtL3FilterDirection
        (&u4ErrCode, i4FilterNo, i4Direction) == SNMP_FAILURE)
    {
        if (nmhSetIssExtL3FilterStatus (i4FilterNo, i4Status) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to Set L3 Filter Status %d for Filter ID %d\n",
                          i4Status, i4FilterNo);
            return (CLI_FAILURE);
        }
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test L3 Filter Direction%d for Filter ID %dwith  error code as %d\n",
                      i4Direction, i4FilterNo, u4ErrCode);
        return CLI_FAILURE;
    }

    if (nmhSetIssExtL3FilterDirection (i4FilterNo, i4Direction) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Set L3 Filter Direction %d for Filter ID %d\n",
                      i4Direction, i4FilterNo);
        return CLI_FAILURE;
    }
    /* Based on the direction we need to set the OutPortlist or the InPortlist
     * The status of the filter entry is set to NOT_IN_SERVICE and the port-list
     * should be updated.
     */

    if (i4Direction == ACL_ACCESS_IN)
    {

        if ((u4IfIndex >= CFA_MIN_EVB_SBP_INDEX) &&
            (u4IfIndex <= CFA_MAX_EVB_SBP_INDEX))
        {

            /* Call nmhroutines to test & set the s-ch IfIndex */
            if (nmhTestv2IssAclL3FilterSChannelIfIndex
                (&u4ErrCode, i4FilterNo, u4IfIndex) == SNMP_FAILURE)
            {
                if (nmhSetIssExtL3FilterDirection
                    (i4FilterNo, i4DirectionStatus) == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to Set L3 Filter  Direction Status %d for Filter ID %d\n",
                                  i4DirectionStatus, i4FilterNo);
                    return CLI_FAILURE;
                }

                if (nmhSetIssExtL3FilterStatus (i4FilterNo, i4Status) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to Set L3 Filter Status %d for Filter ID %d\n",
                                  i4Status, i4FilterNo);
                    return (CLI_FAILURE);
                }
                return (CLI_FAILURE);

            }
            if (nmhSetIssAclL3FilterSChannelIfIndex (i4FilterNo, u4IfIndex)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to Set the S-Channel IfIndex  for Filter %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);

            }

        }

        else if ((u4IfIndex > BRG_MAX_PHY_PORTS)
                 && (u4IfIndex <= BRG_MAX_PHY_PLUS_LOG_PORTS))
        {
            if (nmhTestv2IssExtL3FilterInPortChannelList
                (&u4ErrCode, i4FilterNo, &PortChannelList) == SNMP_FAILURE)
            {
                if (nmhSetIssExtL3FilterDirection
                    (i4FilterNo, i4DirectionStatus) == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to Set L3 Filter  Direction Status %d for Filter ID %d\n",
                                  i4DirectionStatus, i4FilterNo);
                    return CLI_FAILURE;
                }

                if (nmhSetIssExtL3FilterStatus (i4FilterNo, i4Status) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to Set L3 Filter Status %d for Filter ID %d\n",
                                  i4Status, i4FilterNo);
                    return (CLI_FAILURE);
                }
                return (CLI_FAILURE);
            }
            if (nmhSetIssExtL3FilterInPortChannelList
                (i4FilterNo, &PortChannelList) == SNMP_FAILURE)
            {
                i4RetVal = nmhSetIssExtL3FilterStatus (i4FilterNo, i4Status);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter InPortChannelList  for Filter ID %d\n",
                              i4FilterNo);
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
        }
        else
        {
            if (nmhTestv2IssExtL3FilterInPortList
                (&u4ErrCode, i4FilterNo, &PortList) == SNMP_FAILURE)
            {
                if (nmhSetIssExtL3FilterDirection
                    (i4FilterNo, i4DirectionStatus) == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to Set L3 Filter  Direction Status %d for Filter ID %d\n",
                                  i4DirectionStatus, i4FilterNo);
                    return CLI_FAILURE;
                }

                if (nmhSetIssExtL3FilterStatus (i4FilterNo, i4Status) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to Set L3 Filter Status %d for Filter ID %d\n",
                                  i4Status, i4FilterNo);
                    return (CLI_FAILURE);
                }
                return (CLI_FAILURE);
            }
            if (nmhSetIssExtL3FilterInPortList (i4FilterNo, &PortList) ==
                SNMP_FAILURE)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, i4Status);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter InPortList for Filter ID %d\n",
                              i4FilterNo);
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
        }
    }
    else
    {
        if ((u4IfIndex > BRG_MAX_PHY_PORTS)
            && (u4IfIndex <= BRG_MAX_PHY_PLUS_LOG_PORTS))
        {
            if (nmhTestv2IssExtL3FilterOutPortChannelList
                (&u4ErrCode, i4FilterNo, &PortChannelList) == SNMP_FAILURE)
            {
                if (nmhSetIssExtL3FilterDirection
                    (i4FilterNo, i4DirectionStatus) == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to Set L3 Filter  Direction Status %d for Filter ID %d\n",
                                  i4DirectionStatus, i4FilterNo);
                    return CLI_FAILURE;
                }

                if (nmhSetIssExtL3FilterStatus (i4FilterNo, i4Status) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to Set L3 Filter Status %d for Filter ID %d\n",
                                  i4Status, i4FilterNo);
                    return (CLI_FAILURE);
                }
                return (CLI_FAILURE);
            }
            if (nmhSetIssExtL3FilterOutPortChannelList
                (i4FilterNo, &PortChannelList) == SNMP_FAILURE)
            {
                i4RetVal = nmhSetIssExtL3FilterStatus (i4FilterNo, i4Status);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter OutPortChannelList  for Filter ID %d\n",
                              i4FilterNo);
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
        }
        else
        {
            if (nmhTestv2IssExtL3FilterOutPortList
                (&u4ErrCode, i4FilterNo, &PortList) == SNMP_FAILURE)
            {
                if (nmhSetIssExtL3FilterDirection
                    (i4FilterNo, i4DirectionStatus) == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to Set L3 Filter  Direction Status %d for Filter ID %d\n",
                                  i4DirectionStatus, i4FilterNo);
                    return CLI_FAILURE;
                }

                if (nmhSetIssExtL3FilterStatus (i4FilterNo, i4Status) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to Set L3 Filter Status %d for Filter ID %d\n",
                                  i4Status, i4FilterNo);
                    return (CLI_FAILURE);
                }

                return (CLI_FAILURE);
            }
            if (nmhSetIssExtL3FilterOutPortList (i4FilterNo, &PortList)
                == SNMP_FAILURE)
            {
                i4RetVal = nmhSetIssExtL3FilterStatus (i4FilterNo, i4Status);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter outPortList for Filter ID %d\n",
                              i4FilterNo);
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

        }
    }
    /* Enable ACL Counter if required */
    if ((nmhGetIssAclL3FilterStatsEnabledStatus
         (i4FilterNo, &i4StatsOld)) == SNMP_SUCCESS)
    {
        if ((i4StatsOld == ACL_STAT_DISABLE) && (i4Stats == ACL_STAT_ENABLE))
        {
            if ((AclIpStatsSetEnabledStatus (CliHandle, i4FilterNo, i4Stats))
                == CLI_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_HW_UPDATE_FAILED);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to enable statistics for Filter ID %d \n",
                              i4FilterNo);
                return CLI_FAILURE;
            }
            CliPrintf (CliHandle,
                       "\r\n <Information> Hardware counter will be enabled for all the associated interfaces.\r\n");
        }
    }
    else
    {
        CliPrintf (CliHandle, "\r%% Invalid IP Filter Statistics Status \r\n");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Retrieve L3 Filter Statistics Status %d for Filter ID %d\n",
                      i4StatsOld, i4FilterNo);
    }

    /* Activate the filter entry after configuring all the necessary parameters */

    if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_ACTIVE) == SNMP_FAILURE)
    {
        if (i4Direction == ACL_ACCESS_IN)
        {
            if ((u4IfIndex > BRG_MAX_PHY_PORTS)
                && (u4IfIndex <= BRG_MAX_PHY_PLUS_LOG_PORTS))
            {
                i4RetVal = nmhSetIssExtL3FilterInPortChannelList (i4FilterNo,
                                                                  &OldPortChannelList);
            }
            else
            {
                nmhSetIssExtL3FilterInPortList (i4FilterNo, &OldPortList);
            }
        }
        else
        {
            if ((u4IfIndex > BRG_MAX_PHY_PORTS)
                && (u4IfIndex <= BRG_MAX_PHY_PLUS_LOG_PORTS))
            {
                i4RetVal = nmhSetIssExtL3FilterOutPortChannelList (i4FilterNo,
                                                                   &OldPortChannelList);
            }
            else
            {
                i4RetVal =
                    nmhSetIssExtL3FilterOutPortList (i4FilterNo, &OldPortList);
            }
        }
        i4RetVal = nmhSetIssExtL3FilterStatus (i4FilterNo, i4Status);
        nmhSetIssAclL3FilterStatsEnabledStatus (i4FilterNo, i4StatsOld);
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Set L3 Filter Status for Filter ID %d\n",
                      i4FilterNo);
        return (CLI_FAILURE);

    }

    if (u4L3RedirectIndex != 0)
    {
        if (nmhSetIssRedirectInterfaceGrpStatus (u4L3RedirectIndex,
                                                 ISS_ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to Set L3 Filter RedirectIndex %d\n",
                          u4L3RedirectIndex);
            return (CLI_FAILURE);
        }
    }
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclIpAccessGroup function \n");
    UNUSED_PARAM (i4RetVal);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
    /*                                                                           */
/*     FUNCTION NAME    : AclNoIpAccessGroup                                 */
/*                                                                           */
/*     DESCRIPTION      : This function remove an IP acls from  an interface */
/*                                                                           */
/*     INPUT            : i4FilterNo - IP ACL number                         */
/*                        i4Direction - In or out                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclNoIpAccessGroup (tCliHandle CliHandle, INT4 i4FilterNo,
                    INT4 i4Direction, INT4 i4Stats, UINT4 u4IfIndexArg)
{
    tSNMP_OCTET_STRING_TYPE PortList;
    tSNMP_OCTET_STRING_TYPE PortChannelList;
    UINT1               au1PortList[ISS_PORTLIST_LEN];
    UINT1               au1NullPortList[ISS_PORTLIST_LEN];
    UINT1               au1PortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];
    UINT1               au1NullPortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];
    UINT4               u4IfIndex;
    INT4                i4CurrentFilter = 0;
    UINT1               u1Flag = 1;
    UINT4               u4ErrCode;
    UINT4               u4SChannelIndex = 0;
    UINT4               u4L3RedirectIndex = 0;
    INT4                i4RedirectStatus = 0;
    UINT1               u1IsMemberPort = 1;
    INT4                i4StatsOld = ACL_STAT_DISABLE;
    INT4                i4DirectionOld = ACL_ACCESS_IN;

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclNoIpAccessGroup function \n");
    MEMSET (au1PortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1NullPortList, 0, ISS_PORTLIST_LEN);

    PortList.i4_Length = ISS_PORTLIST_LEN;
    PortList.pu1_OctetList = &au1PortList[0];

    MEMSET (au1PortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    MEMSET (au1NullPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);

    PortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;
    PortChannelList.pu1_OctetList = &au1PortChannelList[0];

    /*Get the interface index from the current mode in CLI */
    u4IfIndex = CLI_GET_IFINDEX ();

    /* To update filter due to deleting port-channel interface */
    if (u4IfIndex == (UINT4) (-1))
    {
        u4IfIndex = u4IfIndexArg;
    }

    if (i4FilterNo == 0)
    {
        /* The boolean u1Flag is used to specify whether the operation 
         * needs to be performed for all filters or for specific filter
         * only.
         */

        u1Flag = 0;

        if (nmhGetFirstIndexIssExtL3FilterTable (&i4FilterNo) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% No IP ACLs have been configured\r\n");
            return (CLI_SUCCESS);
        }
    }

    do
    {
        i4CurrentFilter = i4FilterNo;

        if ((u4IfIndex >= CFA_MIN_EVB_SBP_INDEX) &&
            (u4IfIndex <= CFA_MAX_EVB_SBP_INDEX))
        {
            if (i4Direction == ACL_ACCESS_IN)
            {
                nmhGetIssAclL2FilterSChannelIfIndex (i4FilterNo,
                                                     (INT4 *) &u4SChannelIndex);
            }
            u1IsMemberPort = 0;
        }
        else if ((u4IfIndex > BRG_MAX_PHY_PORTS)
                 && (u4IfIndex <= BRG_MAX_PHY_PLUS_LOG_PORTS))
        {
            if (i4Direction == ACL_ACCESS_IN)
            {
                nmhGetIssExtL3FilterInPortChannelList (i4FilterNo,
                                                       &PortChannelList);
            }
            else
            {
                nmhGetIssExtL3FilterOutPortChannelList (i4FilterNo,
                                                        &PortChannelList);
            }
            if (CliIsMemberPort (PortChannelList.pu1_OctetList,
                                 ISS_PORT_CHANNEL_LIST_SIZE,
                                 u4IfIndex) != CLI_SUCCESS)
            {
                if (u1Flag == 1)
                {
                    CliPrintf (CliHandle,
                               "\r%% This filter has not been configured for this "
                               "port channel interface or for this direction in this interface"
                               "\r\n");
                    return (CLI_SUCCESS);
                }
            }
            else
            {
                u1IsMemberPort = 0;
            }
        }
        else
        {
            if (i4Direction == ACL_ACCESS_IN)
            {
                nmhGetIssExtL3FilterInPortList (i4FilterNo, &PortList);
            }
            else
            {
                nmhGetIssExtL3FilterOutPortList (i4FilterNo, &PortList);
            }

            if (CliIsMemberPort (PortList.pu1_OctetList,
                                 ISS_PORTLIST_LEN, u4IfIndex) != CLI_SUCCESS)
            {
                if (u1Flag == 1)
                {
                    CliPrintf (CliHandle,
                               "\r%% This filter has not been configured for this "
                               "interface or for this direction in this interface"
                               "\r\n");
                    return (CLI_SUCCESS);
                }
            }
            else
            {
                u1IsMemberPort = 0;
            }
        }
        if (u1IsMemberPort == 0)
        {
            if (nmhGetIssExtL3FilterDirection (i4FilterNo, &i4DirectionOld)
                == SNMP_SUCCESS)
            {
                /* Disable ACL Counter alone if hardware-count token is present in CLI */
                if ((i4DirectionOld == i4Direction)
                    && (i4Stats == ACL_STAT_DISABLE))
                {
                    if ((nmhGetIssAclL3FilterStatsEnabledStatus
                         (i4FilterNo, &i4StatsOld)) == SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% Unable to retrieve L3 Filter Statistics Status \r\n");
                        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to Retrieve L3 Filter Statistics Status for Filter ID %d\n",
                                      i4FilterNo);
                        continue;
                    }
                    /* Disable ACL Counter if already enabled */
                    if (i4StatsOld == ACL_STAT_ENABLE)
                    {
                        if ((AclIpStatsSetEnabledStatus
                             (CliHandle, i4FilterNo, i4Stats)) == CLI_FAILURE)
                        {
                            continue;
                        }
                        CliPrintf (CliHandle,
                                   "\r\n <Information> Hardware counter will be disabled for all the associated interfaces.\r\n");
                    }
                    /* Ignore ACL de-association with interface and proceed with next ACL */
                    continue;
                }
            }
            else
            {
                CliPrintf (CliHandle,
                           "\r%% Unable to retrieve L3 Filter Direction \r\n");
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to Retrieve L3 Filter Direction for Filter ID %d\n",
                              i4FilterNo);
                continue;
            }
            if ((u4IfIndex > BRG_MAX_PHY_PORTS)
                && (u4IfIndex <= BRG_MAX_PHY_PLUS_LOG_PORTS))
            {
                ACL_DEL_PORT_LIST (au1PortChannelList, u4IfIndex);
            }
            else
            {
                ACL_DEL_PORT_LIST (au1PortList, u4IfIndex);
            }
            /* If there are no ports left in the port list,
             * then apply the filter on all ports */

            /* Filter exists for this interface */

            if (nmhTestv2IssExtL3FilterStatus
                (&u4ErrCode, i4FilterNo, ISS_NOT_IN_SERVICE) == SNMP_FAILURE)

            {
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to test L3 Filter Status for Filter ID %d with  error code as %d\n",
                              i4FilterNo, u4ErrCode);
                return (CLI_FAILURE);
            }

            if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter Status for Filter ID %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }

            if (u4L3RedirectIndex != 0)
            {
                nmhGetIssRedirectInterfaceGrpStatus (u4L3RedirectIndex,
                                                     &i4RedirectStatus);

                if (i4RedirectStatus == ISS_ACTIVE)
                {
                    if (nmhTestv2IssRedirectInterfaceGrpStatus (&u4ErrCode,
                                                                u4L3RedirectIndex,
                                                                ISS_NOT_IN_SERVICE)
                        == SNMP_FAILURE)
                    {
                        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to test L3 Filter RedirectIndex %d with  error code as %d\n",
                                      u4L3RedirectIndex, u4ErrCode);
                        return (CLI_FAILURE);
                    }

                    if (nmhSetIssRedirectInterfaceGrpStatus (u4L3RedirectIndex,
                                                             ISS_NOT_IN_SERVICE)
                        == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to test L3 Filter RedirectIndex %d\n",
                                      u4L3RedirectIndex);
                        return (CLI_FAILURE);
                    }
                }
            }

            if ((u4IfIndex > BRG_MAX_PHY_PORTS)
                && (u4IfIndex <= BRG_MAX_PHY_PLUS_LOG_PORTS))
            {
                if (i4Direction == ACL_ACCESS_IN)
                {
                    if (nmhTestv2IssExtL3FilterInPortChannelList
                        (&u4ErrCode, i4FilterNo,
                         &PortChannelList) == SNMP_FAILURE)
                    {
                        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to test L3 Filter InPortChannelList for Filter ID %dwith  error code as %d\n",
                                      i4FilterNo, u4ErrCode);
                        return (CLI_FAILURE);
                    }
                    if (nmhSetIssExtL3FilterInPortChannelList
                        (i4FilterNo, &PortChannelList) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to Set L3 Filter InPortChannelList for Filter ID %d\n",
                                      i4FilterNo);
                        return (CLI_FAILURE);
                    }
                }
                else
                {
                    if (nmhTestv2IssExtL3FilterOutPortChannelList
                        (&u4ErrCode, i4FilterNo,
                         &PortChannelList) == SNMP_FAILURE)
                    {
                        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to test L3 Filter OutPortChannelList for Filter ID %dwith  error code as %d\n",
                                      i4FilterNo, u4ErrCode);
                        return (CLI_FAILURE);
                    }
                    if (nmhSetIssExtL3FilterOutPortChannelList
                        (i4FilterNo, &PortChannelList) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to Set L3 Filter OutPortChannelList for Filter ID %d\n",
                                      i4FilterNo);
                        return (CLI_FAILURE);
                    }
                }
                if ((MEMCMP
                     (au1PortChannelList, au1NullPortChannelList,
                      ISS_PORT_CHANNEL_LIST_SIZE)) != 0)
                {
                    if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_ACTIVE) ==
                        SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to set L3Filter Status for filter ID %d\n",
                                      i4FilterNo);
                        return (CLI_FAILURE);
                    }
                }
            }
            else
            {
                if (i4Direction == ACL_ACCESS_IN)
                {
                    if (nmhTestv2IssExtL3FilterInPortList
                        (&u4ErrCode, i4FilterNo, &PortList) == SNMP_FAILURE)
                    {
                        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to test L3 Filter InPortList for Filter ID %dwith  error code as %d\n",
                                      i4FilterNo, u4ErrCode);
                        return (CLI_FAILURE);
                    }
                    if (nmhSetIssExtL3FilterInPortList (i4FilterNo, &PortList)
                        == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to set L3 Filter InPortList for Filter ID %d\n",
                                      i4FilterNo);
                        return (CLI_FAILURE);
                    }
                }
                else
                {
                    if (nmhTestv2IssExtL3FilterOutPortList
                        (&u4ErrCode, i4FilterNo, &PortList) == SNMP_FAILURE)
                    {
                        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to test L3 Filter out PortList for Filter ID %dwith  error code as %d\n",
                                      i4FilterNo, u4ErrCode);
                        return (CLI_FAILURE);
                    }

                    if (nmhSetIssExtL3FilterOutPortList (i4FilterNo, &PortList)
                        == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to set L3 Filter Out PortList for Filter ID %d\n",
                                      i4FilterNo);
                        return (CLI_FAILURE);
                    }
                }
                if ((MEMCMP (au1PortList, au1NullPortList, ISS_PORTLIST_LEN)) !=
                    0)
                {
                    if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_ACTIVE) ==
                        SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to set L3Filter Status for filter ID %d\n",
                                      i4FilterNo);
                        return (CLI_FAILURE);
                    }
                }
            }
            if (u4L3RedirectIndex != 0)
            {
                if (nmhSetIssRedirectInterfaceGrpStatus
                    (u4L3RedirectIndex, ISS_ACTIVE) == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to test L3 Filter RedirectIndex %d\n",
                                  u4L3RedirectIndex);
                    return (CLI_FAILURE);
                }
            }
        }
        u1IsMemberPort = 1;
    }
    while ((nmhGetNextIndexIssExtL3FilterTable
            (i4CurrentFilter, &i4FilterNo) == SNMP_SUCCESS) && (u1Flag == 0));
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclNoIpAccessGroup function \n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclExtMacFilterConfig                              */
/*                                                                           */
/*     DESCRIPTION      : This function to configure MAC acls                */
/*                                                                           */
/*     INPUT            : u4SrcType - ANY/ Source HOST MAC                   */
/*                        SrcMacAddr - Source Mac Address                    */
/*                        u4DestType - ANY/ Dest Source MAC                  */
/*                        i4Protocol - Protocol value                        */
/*                        i4EtherType - Ethertype                            */
/*                        u4VlanId - Vlan Id                                 */
/*                        u4Priority - Priority value                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclExtMacFilterConfig (tCliHandle CliHandle, INT4 i4Action,
                       UINT4 u4SrcType, tMacAddr SrcMacAddr,
                       UINT4 u4DestType, tMacAddr DestMacAddr,
                       INT4 i4Protocol, INT4 i4EtherType,
                       UINT4 u4VlanId, INT4 i4Priority)
{
    UINT4               u4ErrCode = 0;
    INT4                i4FilterNo;
    INT4                i4Status;
    INT4                i4ActionOld = 0;
    INT4                i4RedirectStatus = 0;
    UINT4               u4RedirectGrpId = 0;
    INT4                i4ActiveFlag = 0;
    INT4                i4Direction = 0;
    INT1                i4RetVal = SNMP_SUCCESS;
    INT4                i4StatsOld = ACL_STAT_DISABLE;
    tSNMP_OCTET_STRING_TYPE InPortList;
    tSNMP_OCTET_STRING_TYPE OutPortList;
    UINT1               au1InPortList[ISS_PORTLIST_LEN];
    UINT1               au1OutPortList[ISS_PORTLIST_LEN];
    tSNMP_OCTET_STRING_TYPE InPortChannelList;
    tSNMP_OCTET_STRING_TYPE OutPortChannelList;
    UINT1               au1InPortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];
    UINT1               au1OutPortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];

    tIssL2FilterEntry   IssExtL2FilterEntry;
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;
    MEMSET (&IssExtL2FilterEntry, 0, sizeof (tIssL2FilterEntry));
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclExtMacFilterConfig function \n");
    MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1OutPortList, 0, ISS_PORTLIST_LEN);
    ISS_MEMSET (&InPortList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    ISS_MEMSET (&OutPortList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    InPortList.i4_Length = ISS_PORTLIST_LEN;
    InPortList.pu1_OctetList = &au1InPortList[0];

    OutPortList.i4_Length = ISS_PORTLIST_LEN;
    OutPortList.pu1_OctetList = &au1OutPortList[0];

    MEMSET (au1InPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    MEMSET (au1OutPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    ISS_MEMSET (&InPortChannelList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    ISS_MEMSET (&OutPortChannelList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    InPortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;
    InPortChannelList.pu1_OctetList = &au1InPortChannelList[0];

    OutPortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;
    OutPortChannelList.pu1_OctetList = &au1OutPortChannelList[0];

    /*Get the MAC access list number from the current mode in CLI */
    i4FilterNo = CLI_GET_MACACL ();

    if (nmhGetIssExtL2FilterStatus (i4FilterNo, &i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    /* Store the contents in temporary variable. In case of any failure, copy this
     * over the existing node to avoid intermediate updates
     */
    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4FilterNo);

    if (pIssExtL2FilterEntry != NULL)
    {
        ISS_MEMCPY (&IssExtL2FilterEntry, pIssExtL2FilterEntry,
                    sizeof (tIssL2FilterEntry));
    }
    else
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    if (i4Status != ISS_ACTIVE)
    {
        AclResetL2Filter (i4FilterNo);
    }
    /* Retrieving the old status value */
    nmhGetIssAclL2FilterStatsEnabledStatus (i4FilterNo, &i4StatsOld);
    if (i4Status != ISS_NOT_READY)
    {
        if (i4Status == ISS_ACTIVE)
        {
            i4ActiveFlag = 1;
            nmhGetIssExtL2FilterDirection (i4FilterNo, &i4Direction);
            if (i4Direction == ACL_ACCESS_IN)
            {
                nmhGetIssExtL2FilterInPortList (i4FilterNo, &InPortList);
                nmhGetIssExtL2FilterInPortChannelList (i4FilterNo,
                                                       &InPortChannelList);

            }
            else
            {
                nmhGetIssExtL2FilterOutPortChannelList (i4FilterNo,
                                                        &OutPortChannelList);
                nmhGetIssExtL2FilterOutPortList (i4FilterNo, &OutPortList);
            }
        }
        /* Filter has previously been configured, it must be overwriten
         */
        if (nmhGetIssExtL2FilterAction (i4FilterNo, &i4ActionOld) ==
            SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to retrieve  L2 Filter Action %d for Filter ID %d\n",
                          i4ActionOld, i4FilterNo);
            return (CLI_FAILURE);
        }

        if (i4ActionOld == ISS_REDIRECT_TO)
        {
            nmhGetIssAclL2FilterRedirectId (i4FilterNo,
                                            (INT4 *) &u4RedirectGrpId);
            if (u4RedirectGrpId != 0)
            {

                if (nmhGetIssRedirectInterfaceGrpStatus (u4RedirectGrpId,
                                                         &i4RedirectStatus) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to retrieve RedirectInterface Status %d for Group Id %d \n",
                                  i4RedirectStatus, u4RedirectGrpId);
                    return (CLI_FAILURE);
                }

                if (i4RedirectStatus != 0)
                {

                    if (nmhSetIssRedirectInterfaceGrpStatus
                        (u4RedirectGrpId, ISS_DESTROY) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to set Redirect Interface Status for Group Id %d \n",
                                      u4RedirectGrpId);
                        return (CLI_FAILURE);
                    }
                }
            }
        }
        if (nmhTestv2IssExtL2FilterStatus (&u4ErrCode, i4FilterNo, ISS_DESTROY)
            == SNMP_FAILURE)
        {
            ISSCliCheckAndThrowFatalError (CliHandle);
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test L2 Filter Status for filter ID %d with  error code as %d\n",
                          i4FilterNo, u4ErrCode);
            return (CLI_FAILURE);
        }

        if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L2 Filter Status for filter ID %d\n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }
        if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L2 Filter Status for filter ID %d\n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }

    }
    if (nmhTestv2IssExtL2FilterAction (&u4ErrCode,
                                       i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        if (pIssExtL2FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL2FilterEntry, &IssExtL2FilterEntry,
                        sizeof (tIssL2FilterEntry));
        }
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test L2 Filter Action %d for Filter ID %d  with  error code as %d\n",
                      i4Action, i4FilterNo, u4ErrCode);
        return (CLI_FAILURE);
    }

    if (u4SrcType != ACL_ANY)
    {
        /* Source MAC has been specified */

        if (nmhTestv2IssExtL2FilterSrcMacAddr (&u4ErrCode, i4FilterNo,
                                               SrcMacAddr) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "%%Invalid Source MAC Address.\r\n");
            if (pIssExtL2FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL2FilterEntry, &IssExtL2FilterEntry,
                            sizeof (tIssL2FilterEntry));
            }
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test L2 Filter SrcMacAddr  for Filter ID %d                                   with  error code as %d\n",
                          i4FilterNo, u4ErrCode);
            return (CLI_FAILURE);
        }
    }

    if (u4DestType != ACL_ANY)
    {
        /* Destination MAC has been specified */

        if (nmhTestv2IssExtL2FilterDstMacAddr (&u4ErrCode, i4FilterNo,
                                               DestMacAddr) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "%%Invalid Destination MAC Address.\r\n");
            if (pIssExtL2FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL2FilterEntry, &IssExtL2FilterEntry,
                            sizeof (tIssL2FilterEntry));
            }
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test L2 Filter DestMacAddr for Filter ID %d                                        with  error code as %d\n",
                          i4FilterNo, u4ErrCode);
            return (CLI_FAILURE);
        }
    }

    if (i4Protocol != 0)
    {
        if (nmhTestv2IssExtL2FilterProtocolType (&u4ErrCode, i4FilterNo,
                                                 i4Protocol) == SNMP_FAILURE)
        {
            if (pIssExtL2FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL2FilterEntry, &IssExtL2FilterEntry,
                            sizeof (tIssL2FilterEntry));
            }
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test L2 Filter Protocol type %d for Filter ID %d                               with  error code as %d\n",
                          i4Protocol, i4FilterNo, u4ErrCode);
            return (CLI_FAILURE);
        }
    }

    if (i4EtherType != 0)
    {
        if (nmhTestv2IssExtL2FilterEtherType
            (&u4ErrCode, i4FilterNo, i4EtherType) == SNMP_FAILURE)
        {
            if (pIssExtL2FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL2FilterEntry, &IssExtL2FilterEntry,
                            sizeof (tIssL2FilterEntry));
            }
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test L2 Filter EtherType %d for Filter ID %d                                    with  error code as %d\n",
                          i4EtherType, i4FilterNo, u4ErrCode);
            return (CLI_FAILURE);
        }
    }

    if (u4VlanId != 0)
    {
        if (nmhTestv2IssExtL2FilterVlanId (&u4ErrCode,
                                           i4FilterNo,
                                           u4VlanId) == SNMP_FAILURE)
        {
            if (pIssExtL2FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL2FilterEntry, &IssExtL2FilterEntry,
                            sizeof (tIssL2FilterEntry));
            }
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test L2 Filter Vlan ID %d for Filter ID %d                                         with  error code as %d\n",
                          u4VlanId, i4FilterNo, u4ErrCode);
            return (CLI_FAILURE);
        }
    }
    if (nmhTestv2IssExtL2FilterPriority (&u4ErrCode, i4FilterNo, i4Priority)
        == SNMP_FAILURE)
    {
        if (pIssExtL2FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL2FilterEntry, &IssExtL2FilterEntry,
                        sizeof (tIssL2FilterEntry));
        }
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test  L2 Filter Priority  %d for Filter ID %d,                                             with error code as %d\n",
                      i4Priority, i4FilterNo, u4ErrCode);
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL2FilterAction (i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        if (pIssExtL2FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL2FilterEntry, &IssExtL2FilterEntry,
                        sizeof (tIssL2FilterEntry));
        }
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to set L2 Filter Action %d for Filter ID %d\n",
                      i4Action, i4FilterNo);
        return (CLI_FAILURE);
    }

    if (u4SrcType != ACL_ANY)
    {

        if (nmhSetIssExtL2FilterSrcMacAddr (i4FilterNo, SrcMacAddr)
            == SNMP_FAILURE)
        {
            if (pIssExtL2FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL2FilterEntry, &IssExtL2FilterEntry,
                            sizeof (tIssL2FilterEntry));
            }
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to Set L2 Filter Src Mac Address for Filter ID %d\n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }
    }

    if (u4DestType != ACL_ANY)
    {

        if (nmhSetIssExtL2FilterDstMacAddr (i4FilterNo, DestMacAddr)
            == SNMP_FAILURE)
        {
            if (pIssExtL2FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL2FilterEntry, &IssExtL2FilterEntry,
                            sizeof (tIssL2FilterEntry));
            }
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to Set L2 Filter Dest Mac Address for Filter ID %d\n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }
    }

    if (i4Protocol != 0)
    {
        if (nmhSetIssExtL2FilterProtocolType (i4FilterNo, i4Protocol) ==
            SNMP_FAILURE)
        {
            if (pIssExtL2FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL2FilterEntry, &IssExtL2FilterEntry,
                            sizeof (tIssL2FilterEntry));
            }
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to Set L2 Filter  Protocol  %d for Filter ID %d\n",
                          i4Protocol, i4FilterNo);
            return (CLI_FAILURE);
        }
    }

    if (i4EtherType != 0)
    {
        if (nmhSetIssExtL2FilterEtherType (i4FilterNo, i4EtherType) ==
            SNMP_FAILURE)
        {
            if (pIssExtL2FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL2FilterEntry, &IssExtL2FilterEntry,
                            sizeof (tIssL2FilterEntry));
            }
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to Set L2 Filter  EtherType  %d for Filter ID %d\n",
                          i4EtherType, i4FilterNo);
            return (CLI_FAILURE);
        }
    }

    if (u4VlanId != 0)
    {
        if (nmhSetIssExtL2FilterVlanId (i4FilterNo, u4VlanId) == SNMP_FAILURE)
        {
            if (pIssExtL2FilterEntry != NULL)
            {
                ISS_MEMCPY (pIssExtL2FilterEntry, &IssExtL2FilterEntry,
                            sizeof (tIssL2FilterEntry));
            }
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to Set L2 Filter VlanId  %d for Filter ID %d\n",
                          u4VlanId, i4FilterNo);
            return (CLI_FAILURE);
        }
    }
    if (nmhSetIssExtL2FilterPriority (i4FilterNo, i4Priority) == SNMP_FAILURE)
    {
        if (pIssExtL2FilterEntry != NULL)
        {
            ISS_MEMCPY (pIssExtL2FilterEntry, &IssExtL2FilterEntry,
                        sizeof (tIssL2FilterEntry));
        }
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Set L2 Filter Priority  %d for Filter ID %d\n",
                      i4Priority, i4FilterNo);
        return (CLI_FAILURE);
    }
    if ((i4ActiveFlag == 1) && (i4Status == ISS_ACTIVE))
    {
        i4RetVal = nmhSetIssExtL2FilterDirection (i4FilterNo, i4Direction);
        if (i4RetVal == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to Set L2 Filter Direction %d for Filter ID %d\n",
                          i4Direction, i4FilterNo);
            return (CLI_FAILURE);
        }

        if (i4Direction == ACL_ACCESS_IN)
        {
            i4RetVal =
                nmhSetIssExtL2FilterInPortChannelList (i4FilterNo,
                                                       &InPortChannelList);
            if (i4RetVal == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L2 Filter InPortChannelList  for Filter ID %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }

            i4RetVal = nmhSetIssExtL2FilterInPortList (i4FilterNo, &InPortList);
            if (i4RetVal == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L2 Filter InPortList  for Filter ID %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }
        }
        else
        {
            i4RetVal =
                nmhSetIssExtL2FilterOutPortChannelList (i4FilterNo,
                                                        &OutPortChannelList);
            if (i4RetVal == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L2 Filter OutPortChannelList  for Filter ID %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }
            i4RetVal =
                nmhSetIssExtL2FilterOutPortList (i4FilterNo, &OutPortList);
            if (i4RetVal == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L2 Filter OutPortList  for Filter ID %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }
        }

        if (i4StatsOld == ACL_STAT_ENABLE)
        {
            if ((AclMacStatsSetEnabledStatus
                 (CliHandle, i4FilterNo, i4StatsOld)) == CLI_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_HW_UPDATE_FAILED);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to enable statistics for Filter ID %d \n",
                              i4FilterNo);
                return CLI_FAILURE;
            }

        }

        i4RetVal = nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_ACTIVE);
        if (i4RetVal == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_ACL_UNABLE_TO_SET);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L2 FilterStatus for Filter ID \n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }
    }
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclExtMacFilterConfig  function \n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclMacExtAccessGroup                                */
/*                                                                           */
/*     DESCRIPTION      : This function associated an MAC acl to an interface*/
/*                                                                           */
/*     INPUT            : i4FilterNo - MAC ACL number                        */
/*                        i4Direction - In or out                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclMacExtAccessGroup (tCliHandle CliHandle, INT4 i4FilterNo,
                      INT4 i4Direction, INT4 i4Stats)
{
    UINT4               u4IfIndex;
    UINT4               u4ProtocolType = 0;
    UINT1               au1PortList[ISS_PORTLIST_LEN];
    UINT1               au1OldPortList[ISS_PORTLIST_LEN];
    UINT1               au1PortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];
    UINT1               au1OldPortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];
    tSNMP_OCTET_STRING_TYPE PortList;
    tSNMP_OCTET_STRING_TYPE OldPortList;
    tSNMP_OCTET_STRING_TYPE PortChannelList;
    tSNMP_OCTET_STRING_TYPE OldPortChannelList;
    UINT4               u4ErrCode;
    UINT4               u4L2RedirectIndex = 0;
    UINT4               u4EnabledErrCode = 0;
    UINT4               u4SChannelIndex = 0;
    INT4                i4Status;
    INT4                i4DirectionStatus;
    INT4                i4EtherType = 0;
    INT4                i4ActionOld = 0;
    INT4                i4RedirectStatus = 0;
    INT1                i4RetVal = 0;
    INT4                i4StatsOld = ACL_STAT_ENABLE;

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclMacExtAccessGroup function \n");
    MEMSET (au1PortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1OldPortList, 0, ISS_PORTLIST_LEN);

    PortList.i4_Length = ISS_PORTLIST_LEN;
    PortList.pu1_OctetList = &au1PortList[0];

    OldPortList.i4_Length = ISS_PORTLIST_LEN;
    OldPortList.pu1_OctetList = &au1OldPortList[0];

    MEMSET (au1PortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    MEMSET (au1OldPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);

    PortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;
    PortChannelList.pu1_OctetList = &au1PortChannelList[0];

    OldPortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;
    OldPortChannelList.pu1_OctetList = &au1OldPortChannelList[0];

    /*Get the interface index from the current mode in CLI */
    u4IfIndex = CLI_GET_IFINDEX ();
    if ((nmhGetIssExtL2FilterStatus (i4FilterNo, &i4Status)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid MAC Filter \r\n");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to retrieve L2 Filter Status %d for filter ID %d\n",
                      i4Status, i4FilterNo);
        return (CLI_FAILURE);
    }
    if (nmhGetIssExtL2FilterDirection (i4FilterNo, &i4DirectionStatus)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to retrieve L2 Filter Direction Status %d for filter ID %d\n",
                      i4DirectionStatus, i4FilterNo);
        return CLI_FAILURE;
    }
    if ((i4Status == ISS_ACTIVE) && (i4DirectionStatus != i4Direction))
    {
        CliPrintf (CliHandle,
                   "\r%% Filter is already active in alternate direction \r\n");
        return CLI_FAILURE;
    }

    /* If Action is Redirection for Filter then call our API */
    if (nmhGetIssExtL2FilterAction (i4FilterNo, &i4ActionOld) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to retrieve L2 Filter Action %d for Filter Id %d \n",
                      i4ActionOld, i4FilterNo);
        return (CLI_FAILURE);
    }

    if ((i4Direction != ACL_ACCESS_IN) && (i4ActionOld == ISS_REDIRECT_TO))
    {
        CliPrintf (CliHandle,
                   "\r%% Redirect Filter Not Applicable For OutPort \r\n");
        return CLI_FAILURE;
    }

    if (i4ActionOld == ISS_REDIRECT_TO)
    {
        ISS_REDIRECT_ID_GET (i4FilterNo, u4L2RedirectIndex);
        if (u4L2RedirectIndex == 0)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:L2 Redirect Index retrieve  zero %d for Filter ID %d\n",
                          u4L2RedirectIndex, i4FilterNo);
            return CLI_FAILURE;
        }

    }

    nmhGetIssExtL2FilterProtocolType (i4FilterNo, &u4ProtocolType);
    nmhGetIssExtL2FilterEtherType (i4FilterNo, &i4EtherType);

    if ((u4ProtocolType != 0) && (i4EtherType != 0))
    {
        CliPrintf (CliHandle,
                   "\r%% Invalid Input - Both Protocol and Encaptype not allowed\r\n");
        return (CLI_FAILURE);
    }

    if ((u4IfIndex >= CFA_MIN_EVB_SBP_INDEX) &&
        (u4IfIndex <= CFA_MAX_EVB_SBP_INDEX))
    {
        if (i4Direction == ACL_ACCESS_IN)
        {
            nmhGetIssAclL2FilterSChannelIfIndex (i4FilterNo,
                                                 (INT4 *) &u4SChannelIndex);
        }
    }

    else if ((u4IfIndex > BRG_MAX_PHY_PORTS)
             && (u4IfIndex <= BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        if (i4Direction == ACL_ACCESS_IN)
        {
            nmhGetIssExtL2FilterInPortChannelList (i4FilterNo,
                                                   &PortChannelList);
        }
        else
        {
            nmhGetIssExtL2FilterOutPortChannelList (i4FilterNo,
                                                    &PortChannelList);
        }
        MEMCPY (OldPortChannelList.pu1_OctetList, PortChannelList.pu1_OctetList,
                PortChannelList.i4_Length);
        OldPortChannelList.i4_Length = PortChannelList.i4_Length;

        OSIX_BITLIST_SET_BIT (au1PortChannelList, u4IfIndex,
                              ISS_PORT_CHANNEL_LIST_SIZE);
    }
    else
    {
        if (i4Direction == ACL_ACCESS_IN)
        {
            nmhGetIssExtL2FilterInPortList (i4FilterNo, &PortList);
        }
        else
        {
            nmhGetIssExtL2FilterOutPortList (i4FilterNo, &PortList);
        }
        MEMCPY (OldPortList.pu1_OctetList, PortList.pu1_OctetList,
                PortList.i4_Length);
        OldPortList.i4_Length = PortList.i4_Length;

        OSIX_BITLIST_SET_BIT (au1PortList, u4IfIndex, ISS_PORTLIST_LEN);
    }

    if (nmhTestv2IssExtL2FilterStatus
        (&u4ErrCode, i4FilterNo, ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test L2 Filter Status for filter ID %d with  error code as %d\n",
                      i4FilterNo, u4ErrCode);
        return (CLI_FAILURE);
    }

    if (i4Status == ISS_ACTIVE)
    {
        if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L2 FilterStatus for filter ID %d \n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }
        if (u4L2RedirectIndex != 0)
        {
            nmhGetIssRedirectInterfaceGrpStatus (u4L2RedirectIndex,
                                                 &i4RedirectStatus);

            if (i4RedirectStatus == ISS_ACTIVE)
            {

                if (nmhTestv2IssRedirectInterfaceGrpStatus (&u4ErrCode,
                                                            u4L2RedirectIndex,
                                                            ISS_NOT_IN_SERVICE)
                    == SNMP_FAILURE)
                {
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to test L2 Filter RedirectIndex %d                                            with  error code as %d\n",
                                  u4L2RedirectIndex, u4ErrCode);
                    return (CLI_FAILURE);
                }

                if (nmhSetIssRedirectInterfaceGrpStatus (u4L2RedirectIndex,
                                                         ISS_NOT_IN_SERVICE) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to test L3 Filter RedirectIndex %d\n",
                                  u4L2RedirectIndex);
                    return (CLI_FAILURE);
                }
            }
        }
    }
    if (nmhTestv2IssExtL2FilterDirection
        (&u4ErrCode, i4FilterNo, i4Direction) == SNMP_FAILURE)
    {
        if (nmhSetIssExtL2FilterStatus (i4FilterNo, i4Status) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to Set L2 Filter Status %d for Filter ID %d\n",
                          i4Status, i4FilterNo);
            return (CLI_FAILURE);
        }

        return CLI_FAILURE;
    }

    if (nmhSetIssExtL2FilterDirection (i4FilterNo, i4Direction) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Set L2 Filter Direction%d for Filter ID %d\n",
                      i4Direction, i4FilterNo);
        return SNMP_FAILURE;
    }

    if ((u4IfIndex >= CFA_MIN_EVB_SBP_INDEX) &&
        (u4IfIndex <= CFA_MAX_EVB_SBP_INDEX))
    {
        if (i4Direction == ACL_ACCESS_IN)
        {
            if (nmhTestv2IssAclL2FilterSChannelIfIndex
                (&u4ErrCode, i4FilterNo, u4IfIndex) == SNMP_FAILURE)
            {
                if (nmhSetIssExtL2FilterDirection
                    (i4FilterNo, i4DirectionStatus) == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to Set L2 Filter  Direction Status %d for Filter ID %d\n",
                                  i4DirectionStatus, i4FilterNo);
                    return CLI_FAILURE;
                }

                if (nmhSetIssExtL2FilterStatus (i4FilterNo, i4Status) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to Set L2 Filter Status %d for Filter ID %d\n",
                                  i4Status, i4FilterNo);
                    return (CLI_FAILURE);
                }
                return (CLI_FAILURE);

            }
            if (nmhSetIssAclL2FilterSChannelIfIndex (i4FilterNo, u4IfIndex)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to Set the S-Channel IfIndex  for Filter %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);

            }

        }

    }

    else if ((u4IfIndex > BRG_MAX_PHY_PORTS)
             && (u4IfIndex <= BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        if (i4Direction == ACL_ACCESS_IN)
        {

            if (nmhTestv2IssExtL2FilterInPortChannelList
                (&u4ErrCode, i4FilterNo, &PortChannelList) == SNMP_FAILURE)
            {
                if (nmhSetIssExtL2FilterDirection
                    (i4FilterNo, i4DirectionStatus) == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to Set L2 Filter  Direction Status %d for Filter ID %d\n",
                                  i4DirectionStatus, i4FilterNo);
                    return CLI_FAILURE;
                }

                if (nmhSetIssExtL2FilterStatus (i4FilterNo, i4Status) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to Set L2 Filter Status %d for Filter ID %d\n",
                                  i4Status, i4FilterNo);
                    return (CLI_FAILURE);
                }
                return (CLI_FAILURE);
            }
            if (nmhSetIssExtL2FilterInPortChannelList
                (i4FilterNo, &PortChannelList) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter InPortChannelList  for Filter ID %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }
        }
        else
        {

            if (nmhTestv2IssExtL2FilterOutPortChannelList
                (&u4ErrCode, i4FilterNo, &PortChannelList) == SNMP_FAILURE)
            {
                if (nmhSetIssExtL2FilterDirection
                    (i4FilterNo, i4DirectionStatus) == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to Set L2 Filter  Direction Status %d for Filter ID %d\n",
                                  i4DirectionStatus, i4FilterNo);
                    return CLI_FAILURE;
                }

                if (nmhSetIssExtL2FilterStatus (i4FilterNo, i4Status) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to Set L2 Filter Status %d for Filter ID %d\n",
                                  i4Status, i4FilterNo);
                    return (CLI_FAILURE);
                }
                return (CLI_FAILURE);
            }
            if (nmhSetIssExtL2FilterOutPortChannelList
                (i4FilterNo, &PortChannelList) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter OutPortChannelList  for Filter ID %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }
        }
    }
    else
    {
        if (i4Direction == ACL_ACCESS_IN)
        {
            if (nmhTestv2IssExtL2FilterInPortList
                (&u4ErrCode, i4FilterNo, &PortList) == SNMP_FAILURE)
            {
                if (nmhSetIssExtL2FilterDirection
                    (i4FilterNo, i4DirectionStatus) == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to Set L2 Filter  Direction Status %d for Filter ID %d\n",
                                  i4DirectionStatus, i4FilterNo);
                    return CLI_FAILURE;
                }

                if (nmhSetIssExtL2FilterStatus (i4FilterNo, i4Status) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to Set L2 Filter Status %d for Filter ID %d\n",
                                  i4Status, i4FilterNo);
                    return (CLI_FAILURE);
                }
                return (CLI_FAILURE);
            }
            if (nmhSetIssExtL2FilterInPortList (i4FilterNo, &PortList) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter InPortList  for Filter ID %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }
        }
        else
        {
            if (nmhTestv2IssExtL2FilterOutPortList
                (&u4ErrCode, i4FilterNo, &PortList) == SNMP_FAILURE)
            {
                if (nmhSetIssExtL2FilterDirection
                    (i4FilterNo, i4DirectionStatus) == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to Set L2 Filter  Direction Status %d for Filter ID %d\n",
                                  i4DirectionStatus, i4FilterNo);
                    return CLI_FAILURE;
                }

                if (nmhSetIssExtL2FilterStatus (i4FilterNo, i4Status) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to Set L2 Filter Status %d for Filter ID %d\n",
                                  i4Status, i4FilterNo);
                    return (CLI_FAILURE);
                }
                return (CLI_FAILURE);
            }
            if (nmhSetIssExtL2FilterOutPortList (i4FilterNo, &PortList) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L3 Filter OutPortList  for Filter ID %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }
        }
    }
    /* Enable ACL Counter if required */
    if ((nmhGetIssAclL2FilterStatsEnabledStatus
         (i4FilterNo, &i4StatsOld)) == SNMP_SUCCESS)
    {
        if ((i4StatsOld == ACL_STAT_DISABLE) && (i4Stats == ACL_STAT_ENABLE))
        {
            if ((AclMacStatsSetEnabledStatus (CliHandle, i4FilterNo, i4Stats))
                == CLI_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_HW_UPDATE_FAILED);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to enable statistics for Filter ID %d \n",
                              i4FilterNo);
                return CLI_FAILURE;
            }
            CliPrintf (CliHandle,
                       "\r\n <Information> Hardware counter will be enabled for all the associated interfaces.\r\n");
        }
    }
    else
    {
        CliPrintf (CliHandle, "\r%% Invalid MAC Filter Statistics Status \r\n");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Retrieve L2 Filter Statistics Status %d for Filter ID %d\n",
                      i4StatsOld, i4FilterNo);
    }

    if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_ACTIVE) == SNMP_FAILURE)
    {
        if ((u4IfIndex > BRG_MAX_PHY_PORTS)
            && (u4IfIndex <= BRG_MAX_PHY_PLUS_LOG_PORTS))
        {
            if (i4Direction == ACL_ACCESS_IN)
            {
                nmhSetIssExtL2FilterInPortChannelList (i4FilterNo,
                                                       &OldPortChannelList);
            }
            else
            {
                nmhSetIssExtL2FilterOutPortChannelList (i4FilterNo,
                                                        &OldPortChannelList);
            }
        }
        else
        {
            if (i4Direction == ACL_ACCESS_IN)
            {
                i4RetVal =
                    nmhSetIssExtL2FilterInPortList (i4FilterNo, &OldPortList);
            }
            else
            {
                i4RetVal =
                    nmhSetIssExtL2FilterOutPortList (i4FilterNo, &OldPortList);
            }
        }
        nmhSetIssExtL2FilterStatus (i4FilterNo, i4Status);
        nmhSetIssAclL2FilterStatsEnabledStatus (i4FilterNo, i4StatsOld);

        CLI_GET_ERR (&u4EnabledErrCode);
        if (u4EnabledErrCode != CLI_ACL_OUT_MULTIPLE_PORTS)
        {
            CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
        }

        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to set L2 FilterStatus for Filter ID \n",
                      i4FilterNo);
        return (CLI_FAILURE);
    }

    if (u4L2RedirectIndex != 0)
    {
        if (nmhSetIssRedirectInterfaceGrpStatus (u4L2RedirectIndex, ISS_ACTIVE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set L2 Filter RedirectIndex %d\n",
                          u4L2RedirectIndex);
            return (CLI_FAILURE);
        }
    }
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4RetVal);
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclMacExtAccessGroup function \n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclNoMacExtAccessGroup                              */
/*                                                                           */
/*     DESCRIPTION      : This function remove an IP acls from  an interface */
/*                                                                           */
/*     INPUT            : i4FilterNo - IP ACL number                         */
/*                        i4Direction - In or out                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclNoMacExtAccessGroup (tCliHandle CliHandle, INT4 i4FilterNo,
                        INT4 i4Direction, INT4 i4Stats, UINT4 u4IfIndexArg)
{
    tSNMP_OCTET_STRING_TYPE PortList;
    tSNMP_OCTET_STRING_TYPE PortChannelList;
    UINT1               au1PortList[ISS_PORTLIST_LEN];
    UINT1               au1NullPortList[ISS_PORTLIST_LEN];
    UINT1               au1PortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];
    UINT1               au1NullPortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];
    UINT4               u4IfIndex;
    UINT4               u4SChannelIndex = 0;
    INT4                i4CurrentFilter;
    UINT1               u1Flag = 1;
    UINT1               u1IsMemberPort = 0;
    UINT4               u4ErrCode;
    UINT4               u4L2RedirectIndex = 0;
    INT4                i4RedirectStatus = 0;
    INT4                i4ActionOld = 0;
    INT4                i4StatsOld = ACL_STAT_DISABLE;
    INT4                i4DirectionOld = ACL_ACCESS_IN;

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclNoMacExtAccessGroup function \n");
    MEMSET (au1PortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1NullPortList, 0, ISS_PORTLIST_LEN);

    PortList.i4_Length = ISS_PORTLIST_LEN;
    PortList.pu1_OctetList = &au1PortList[0];

    MEMSET (au1PortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    MEMSET (au1NullPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);

    PortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;
    PortChannelList.pu1_OctetList = &au1PortChannelList[0];

    /*Get the interface index from the current mode in CLI */
    u4IfIndex = CLI_GET_IFINDEX ();

    /* To update filter due to deleting port-channel interface */
    if (u4IfIndex == (UINT4) (-1))
    {
        u4IfIndex = u4IfIndexArg;
    }

    if (i4FilterNo == 0)
    {
        u1Flag = 0;
        /* to delete all IP ACL 's in a given interface */

        if (nmhGetFirstIndexIssExtL2FilterTable (&i4FilterNo) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% No MAC ACLs have been configured\r\n");
            return (CLI_SUCCESS);
        }
    }

    do
    {
        u1IsMemberPort = 0;
        i4CurrentFilter = i4FilterNo;

        if ((u4IfIndex >= CFA_MIN_EVB_SBP_INDEX) &&
            (u4IfIndex <= CFA_MAX_EVB_SBP_INDEX))
        {

            if (i4Direction == ACL_ACCESS_IN)
            {
                nmhGetIssAclL2FilterSChannelIfIndex (i4FilterNo,
                                                     (INT4 *) &u4SChannelIndex);

                /* If Action is Redirection for Filter then call our API */
                if (nmhGetIssExtL2FilterAction (i4FilterNo, &i4ActionOld) ==
                    SNMP_FAILURE)
                {

                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to retrieve L2 Filter Action %d for Filter Id %d \n",
                                  i4ActionOld, i4FilterNo);
                    return (CLI_FAILURE);
                }

                if (i4ActionOld == ISS_REDIRECT_TO)
                {
                    nmhGetIssAclL2FilterRedirectId (i4FilterNo,
                                                    (INT4 *)
                                                    &u4L2RedirectIndex);
                    if (u4L2RedirectIndex == 0)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        return CLI_FAILURE;
                    }
                }

            }

            if (u4SChannelIndex == u4IfIndex)
            {
                u1IsMemberPort = 1;
            }

        }

        else if ((u4IfIndex > BRG_MAX_PHY_PORTS)
                 && (u4IfIndex <= BRG_MAX_PHY_PLUS_LOG_PORTS))
        {
            if (i4Direction == ACL_ACCESS_IN)
            {
                if (nmhGetIssExtL2FilterInPortChannelList
                    (i4FilterNo, &PortChannelList) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%% This filter has not been configured for this interface\r\n");
                    return (CLI_SUCCESS);
                }
                else
                {

                    /* If Action is Redirection for Filter then call our API */
                    if (nmhGetIssExtL2FilterAction (i4FilterNo, &i4ActionOld) ==
                        SNMP_FAILURE)
                    {
                        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to retrieve L2 Filter Action %d for Filter Id %d \n",
                                      i4ActionOld, i4FilterNo);
                        return (CLI_FAILURE);
                    }

                    if (i4ActionOld == ISS_REDIRECT_TO)
                    {
                        nmhGetIssAclL2FilterRedirectId (i4FilterNo,
                                                        (INT4 *)
                                                        &u4L2RedirectIndex);
                        if (u4L2RedirectIndex == 0)
                        {
                            CLI_FATAL_ERROR (CliHandle);
                            return CLI_FAILURE;
                        }
                    }
                }
            }
            else
            {
                nmhGetIssExtL2FilterOutPortChannelList (i4FilterNo,
                                                        &PortChannelList);
            }
            if (CliIsMemberPort (PortChannelList.pu1_OctetList,
                                 ISS_PORT_CHANNEL_LIST_SIZE,
                                 u4IfIndex) == CLI_SUCCESS)
            {
                u1IsMemberPort = 1;
                OSIX_BITLIST_RESET_BIT (au1PortChannelList, u4IfIndex,
                                        ISS_PORT_CHANNEL_LIST_SIZE);
            }
        }
        else
        {
            if (i4Direction == ACL_ACCESS_IN)
            {
                if (nmhGetIssExtL2FilterInPortList (i4FilterNo, &PortList) ==
                    SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%% This filter has not been configured for this interface\r\n");
                    return (CLI_SUCCESS);

                }
                else
                {

                    /* If Action is Redirection for Filter then call our API */
                    if (nmhGetIssExtL2FilterAction (i4FilterNo, &i4ActionOld) ==
                        SNMP_FAILURE)
                    {
                        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to retrieve L2 Filter Action %d for Filter Id %d \n",
                                      i4ActionOld, i4FilterNo);
                        return (CLI_FAILURE);
                    }

                    if (i4ActionOld == ISS_REDIRECT_TO)
                    {
                        nmhGetIssAclL2FilterRedirectId (i4FilterNo,
                                                        (INT4 *)
                                                        &u4L2RedirectIndex);
                        if (u4L2RedirectIndex == 0)
                        {
                            CLI_FATAL_ERROR (CliHandle);
                            return CLI_FAILURE;
                        }
                    }
                }
            }
            else
            {
                nmhGetIssExtL2FilterOutPortList (i4FilterNo, &PortList);
            }
            if (CliIsMemberPort (PortList.pu1_OctetList,
                                 ISS_PORTLIST_LEN, u4IfIndex) == CLI_SUCCESS)
            {
                u1IsMemberPort = 1;
                OSIX_BITLIST_RESET_BIT (au1PortList, u4IfIndex,
                                        ISS_PORTLIST_LEN);
            }
        }

        if (u1IsMemberPort == 1)

        {
            /* If there are no ports left in the port list, then apply the filter
             * on all ports . This happens when portlist is 0 */

            /* Filter exists for this interface */

            if (nmhGetIssExtL2FilterDirection (i4FilterNo, &i4DirectionOld)
                == SNMP_SUCCESS)
            {
                /* Disable ACL Counter alone if hardware-count token is present in CLI */
                if ((i4DirectionOld == i4Direction)
                    && (i4Stats == ACL_STAT_DISABLE))
                {
                    if ((nmhGetIssAclL2FilterStatsEnabledStatus
                         (i4FilterNo, &i4StatsOld)) == SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% Unable to retrieve L2 Filter Statistics Status \r\n");
                        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to Retrieve L2 Filter Statistics Status for Filter ID %d\n",
                                      i4FilterNo);
                        continue;
                    }
                    /* Disable ACL Counter if already enabled */
                    if (i4StatsOld == ACL_STAT_ENABLE)
                    {
                        if ((AclMacStatsSetEnabledStatus
                             (CliHandle, i4FilterNo, i4Stats)) == CLI_FAILURE)
                        {
                            continue;
                        }
                        CliPrintf (CliHandle,
                                   "\r\n <Information> Hardware counter will be disabled for all the associated interfaces.\r\n");
                    }
                    /* Ignore ACL de-association with interface and proceed with next ACL */
                    continue;
                }
            }
            else
            {
                CliPrintf (CliHandle,
                           "\r%% Unable to retrieve L2 Filter Direction \r\n");
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to Retrieve L2 Filter Direction for Filter ID %d\n",
                              i4FilterNo);
                continue;
            }
            if (nmhTestv2IssExtL2FilterStatus
                (&u4ErrCode, i4FilterNo, ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to test L2 Filter Status for filter ID %d with  error code as %d\n",
                              i4FilterNo, u4ErrCode);
                return (CLI_FAILURE);
            }

            if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set L2 FilterStatus for filter ID %d \n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }

            if (u4L2RedirectIndex != 0)
            {
                nmhGetIssRedirectInterfaceGrpStatus (u4L2RedirectIndex,
                                                     &i4RedirectStatus);

                if (i4RedirectStatus == ISS_ACTIVE)
                {
                    if (nmhTestv2IssRedirectInterfaceGrpStatus (&u4ErrCode,
                                                                u4L2RedirectIndex,
                                                                ISS_NOT_IN_SERVICE)
                        == SNMP_FAILURE)
                    {
                        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to test L3 Filter RedirectIndex %d with  error code as%d\n",
                                      u4L2RedirectIndex, u4ErrCode);
                        return (CLI_FAILURE);
                    }

                    if (nmhSetIssRedirectInterfaceGrpStatus (u4L2RedirectIndex,
                                                             ISS_NOT_IN_SERVICE)
                        == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to test L3 Filter RedirectIndex %d\n",
                                      u4L2RedirectIndex);
                        return (CLI_FAILURE);
                    }
                }
            }

            if ((u4IfIndex >= CFA_MIN_EVB_SBP_INDEX) &&
                (u4IfIndex <= CFA_MAX_EVB_SBP_INDEX))
            {
                if (i4Direction == ACL_ACCESS_IN)
                {

                    if (nmhTestv2IssAclL2FilterSChannelIfIndex
                        (&u4ErrCode, i4FilterNo, u4IfIndex) == SNMP_FAILURE)
                    {

                        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to test L2 Filter S-Channel Ifindex for Filter ID %dwith  error code ...as %d\n",
                                      i4FilterNo, u4ErrCode);
                        return (CLI_FAILURE);

                    }

                    if (nmhSetIssAclL2FilterSChannelIfIndex
                        (i4FilterNo, u4IfIndex) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to Set the S-Channel IfIndex  for Filter %d\n",
                                      i4FilterNo);
                        return (CLI_FAILURE);

                    }

                }

            }

            else if ((u4IfIndex > BRG_MAX_PHY_PORTS)
                     && (u4IfIndex <= BRG_MAX_PHY_PLUS_LOG_PORTS))
            {
                if (i4Direction == ACL_ACCESS_IN)
                {
                    if (nmhTestv2IssExtL2FilterInPortChannelList
                        (&u4ErrCode, i4FilterNo,
                         &PortChannelList) == SNMP_FAILURE)
                    {
                        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to test L2 Filter InPortChannelList for Filter ID %dwith  error code as %d\n",
                                      i4FilterNo, u4ErrCode);
                        return (CLI_FAILURE);
                    }
                    if (nmhSetIssExtL2FilterInPortChannelList
                        (i4FilterNo, &PortChannelList) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to Set L2 Filter InPortChannelList for Filter ID %d\n",
                                      i4FilterNo);
                        return (CLI_FAILURE);
                    }
                }
                else
                {
                    if (nmhTestv2IssExtL2FilterOutPortChannelList
                        (&u4ErrCode, i4FilterNo,
                         &PortChannelList) == SNMP_FAILURE)
                    {
                        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to test L2 Filter OutPortChannelList for Filter ID %dwith  error code as %d\n",
                                      i4FilterNo, u4ErrCode);
                        return (CLI_FAILURE);
                    }
                    if (nmhSetIssExtL2FilterOutPortChannelList
                        (i4FilterNo, &PortChannelList) == SNMP_FAILURE)
                    {
                        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to Set L2 Filter OutPortChannelList for Filter ID %d\n",
                                      i4FilterNo);
                        return (CLI_FAILURE);
                    }
                }
                /* Check for Port List not empty */
                if (i4Direction == ACL_ACCESS_IN)
                {
                    nmhGetIssExtL2FilterInPortList (i4FilterNo, &PortList);
                }
                else
                {
                    nmhGetIssExtL2FilterOutPortList (i4FilterNo, &PortList);
                }
                if (((MEMCMP
                      (au1PortChannelList, au1NullPortChannelList,
                       ISS_PORT_CHANNEL_LIST_SIZE)) != 0) ||
                    (MEMCMP (au1PortList, au1NullPortList, ISS_PORTLIST_LEN) !=
                     0))

                {
                    if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_ACTIVE)
                        == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to set L2 FilterStatus for Filter ID \n",
                                      i4FilterNo);
                        return (CLI_FAILURE);
                    }
                }
            }
            else
            {
                if (i4Direction == ACL_ACCESS_IN)
                {
                    if (nmhTestv2IssExtL2FilterInPortList
                        (&u4ErrCode, i4FilterNo, &PortList) == SNMP_FAILURE)
                    {
                        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to test L2 Filter InPortList for Filter ID %dwith  error code as %d\n",
                                      i4FilterNo, u4ErrCode);
                        return (CLI_FAILURE);
                    }

                    if (nmhSetIssExtL2FilterInPortList (i4FilterNo, &PortList)
                        == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to set  L2 Filter InPortList  for Filter ID %d\n",
                                      i4FilterNo);
                        return (CLI_FAILURE);
                    }
                }
                else
                {
                    if (nmhTestv2IssExtL2FilterOutPortList
                        (&u4ErrCode, i4FilterNo, &PortList) == SNMP_FAILURE)
                    {
                        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to set L2 Filter OutPortList for Filter ID %dwith  error code as %d\n",
                                      i4FilterNo, u4ErrCode);
                        return (CLI_FAILURE);
                    }

                    if (nmhSetIssExtL2FilterOutPortList (i4FilterNo, &PortList)
                        == SNMP_FAILURE)
                    {
                        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to set  L2 Filter OutPortList  for Filter ID %d\n",
                                      i4FilterNo);
                        return (CLI_FAILURE);
                    }
                }
                /* Check for port channel list not empty */
                if (i4Direction == ACL_ACCESS_IN)
                {
                    nmhGetIssExtL2FilterInPortChannelList (i4FilterNo,
                                                           &PortChannelList);
                }
                else
                {
                    nmhGetIssExtL2FilterOutPortChannelList (i4FilterNo,
                                                            &PortChannelList);
                }
                if (((MEMCMP (au1PortList, au1NullPortList, ISS_PORTLIST_LEN))
                     != 0)
                    ||
                    (MEMCMP
                     (au1PortChannelList, au1NullPortChannelList,
                      ISS_PORT_CHANNEL_LIST_SIZE) != 0))
                {
                    if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_ACTIVE)
                        == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                      "\nCLI:Failed to set L2 FilterStatus for Filter ID \n",
                                      i4FilterNo);
                        return (CLI_FAILURE);
                    }
                }
            }
            if (u4L2RedirectIndex != 0)
            {
                if (nmhSetIssRedirectInterfaceGrpStatus
                    (u4L2RedirectIndex, ISS_ACTIVE) == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to set L2 Filter RedirectIndex %d\n",
                                  u4L2RedirectIndex);
                    return (CLI_FAILURE);
                }
            }
        }

        else if (u1Flag == 1)
        {
            CliPrintf (CliHandle,
                       "\r%% This filter has not been configured for this interface "
                       "or for this direction in this interface\r\n");
            return (CLI_SUCCESS);
        }
        u1IsMemberPort = 0;
    }
    while ((nmhGetNextIndexIssExtL2FilterTable
            (i4CurrentFilter, &i4FilterNo) == SNMP_SUCCESS) && (u1Flag == 0));
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclNoMacExtAccessGroup function \n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclUserDefinedOperations                           */
/*                                                                           */
/*     DESCRIPTION      : This function does AND of filters                  */
/*                                                                           */
/*                                                                           */
/*     INPUT            :  i4FilterOper - Type of Filter Operation           */
/*                         u4Filter1Id - Filter 1 Id                         */
/*                         u4Filter2Id - Filter 2 Id                         */
/*                         u4Priority  - Priority                            */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
AclUserDefinedOperations (tCliHandle CliHandle, INT4 i4FilterOper, UINT4
                          u4Filter1Id, UINT4 u4Filter2Id, UINT4 u4Priority)
{
    INT4                i4FilteOneType = 0;
    INT4                i4FilterTwoType = 0;
    INT4                i4Action = 0;
    INT4                i4Status = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4FilterNo = 0;

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclUserDefinedOperations function \n");
    u4FilterNo = (UINT4) CLI_GET_STDACLID ();

    if (nmhGetIssAclUserDefinedFilterStatus (u4FilterNo, &i4Status) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Retrieve User Defined Filter Status %d for Filter ID %d\n",
                      i4Status, u4FilterNo);
        return CLI_FAILURE;
    }
    if (i4Status == ISS_ACTIVE)
    {
        if (nmhSetIssAclUserDefinedFilterStatus (u4FilterNo, ISS_NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "CLI:Failed to set User Defined Filter Status for filter ID%d\n",
                          u4FilterNo);
            return CLI_FAILURE;
        }
    }
    if (i4FilterOper == ISS_USERDEFINED_L2L3FILTER_APPLY_AND_OP)
    {
        i4FilteOneType = ISS_L2_FILTER;
        i4FilterTwoType = ISS_L3_FILTER;
        i4Action = ISS_AND;
    }
    if (nmhTestv2IssAclUserDefinedFilterAction (&u4ErrCode,
                                                u4FilterNo,
                                                i4Action) == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test User Defined Filter Action %d for Filter ID %d  with  error code as %d\n",
                      i4Action, u4FilterNo, u4ErrCode);
        return (CLI_FAILURE);
    }
    if (nmhSetIssAclUserDefinedFilterAction (u4FilterNo, i4Action)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Set User Defined Filter Action %d for Filter ID %d\n",
                      i4Action, u4FilterNo);
        return (CLI_FAILURE);
    }
    if (nmhTestv2IssAclUserDefinedFilterIdOneType (&u4ErrCode,
                                                   u4FilterNo,
                                                   i4FilteOneType) ==
        SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test User Defined Filter OneType %d for Filter ID %d with error code as %d\n",
                      i4FilteOneType, u4FilterNo, u4ErrCode);
        return (CLI_FAILURE);
    }
    if (nmhSetIssAclUserDefinedFilterIdOneType (u4FilterNo, i4FilteOneType)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Set User Defined Filter OneType %d for Filter ID %d\n",
                      i4FilteOneType, u4FilterNo);
        return (CLI_FAILURE);
    }
    if (nmhTestv2IssAclUserDefinedFilterIdOne (&u4ErrCode,
                                               u4FilterNo,
                                               u4Filter1Id) == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test User Defined Filter one ID %d for Filter ID %d with error code as %d\n",
                      u4Filter1Id, u4FilterNo, u4ErrCode);
        return (CLI_FAILURE);
    }
    if (nmhSetIssAclUserDefinedFilterIdOne (u4FilterNo, u4Filter1Id)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to set User Defined Filter one ID %d for Filter ID %d\n",
                      u4Filter1Id, u4FilterNo);
        return (CLI_FAILURE);
    }
    if (nmhTestv2IssAclUserDefinedFilterIdTwoType (&u4ErrCode,
                                                   u4FilterNo,
                                                   i4FilterTwoType) ==
        SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test User Defined Filter Two Type %d for Filter ID %d with error code as %d\n",
                      i4FilterTwoType, u4FilterNo, u4ErrCode);
        return (CLI_FAILURE);
    }
    if (nmhSetIssAclUserDefinedFilterIdTwoType (u4FilterNo, i4FilterTwoType)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Set User Defined Filter Two Type %d for Filter ID %d\n",
                      i4FilterTwoType, u4FilterNo);
        return (CLI_FAILURE);
    }
    if (nmhTestv2IssAclUserDefinedFilterIdTwo (&u4ErrCode,
                                               u4FilterNo,
                                               u4Filter2Id) == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test User Defined Filter Two ID %d for Filter ID %d with error code as %d\n",
                      u4Filter2Id, u4FilterNo, u4ErrCode);
        return (CLI_FAILURE);
    }
    if (nmhSetIssAclUserDefinedFilterIdTwo (u4FilterNo, u4Filter2Id)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to set User Defined Filter one ID %d for Filter ID %d\n",
                      u4Filter2Id, u4FilterNo);
        return (CLI_FAILURE);
    }
    if (nmhTestv2IssAclUserDefinedFilterPriority (&u4ErrCode,
                                                  u4FilterNo,
                                                  (INT4) u4Priority) ==
        SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test User Defined Filter Priority %d for Filter ID %d with error code as %d\n",
                      u4Priority, u4FilterNo, u4ErrCode);
        return (CLI_FAILURE);
    }
    if (nmhSetIssAclUserDefinedFilterPriority (u4FilterNo, (INT4) u4Priority)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to set User Defined Filter Priority %d for Filter ID %d\n",
                      u4Priority, u4FilterNo);
        return (CLI_FAILURE);
    }
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclUserDefinedOperations  function \n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclUserDefinedAccessGroup                          */
/*                                                                           */
/*     DESCRIPTION      : This function associated an UDB acl to an interface*/
/*                                                                           */
/*     INPUT            : i4FilterNo - UDB ACL number                        */
/*                        i4Direction - In or out                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclUserDefinedAccessGroup (tCliHandle CliHandle, INT4 i4FilterNo,
                           INT4 i4Direction, INT4 i4Stats)
{
    UINT4               u4IfIndex = 0;
    UINT1               au1PortList[ISS_PORTLIST_LEN];
    UINT1               au1OldPortList[ISS_PORTLIST_LEN];
    tSNMP_OCTET_STRING_TYPE PortList;
    tSNMP_OCTET_STRING_TYPE OldPortList;
    UINT4               u4UdbRedirectIndex = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4Status = 0;
    INT4                i4ActionOld = 0;
    INT4                i4RedirectStatus = 0;
    INT4                i4StatsOld = ACL_STAT_ENABLE;

    ISS_TRC (INIT_SHUT_TRC,
             "\nCLI:Entry AclUserDefinedAccessGroup  function \n");

    MEMSET (au1PortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1OldPortList, 0, ISS_PORTLIST_LEN);
    PortList.i4_Length = ISS_PORTLIST_LEN;
    PortList.pu1_OctetList = &au1PortList[0];
    OldPortList.i4_Length = ISS_PORTLIST_LEN;
    OldPortList.pu1_OctetList = &au1OldPortList[0];
    if (i4Direction != ACL_ACCESS_IN)
    {
        CliPrintf (CliHandle, "\r%% UserDefined Filter is Applicable only for"
                   "Ingress port \r\n");
        return (CLI_FAILURE);
    }
    /*Get the interface index from the current mode in CLI */
    u4IfIndex = (UINT4) CLI_GET_IFINDEX ();

    if ((nmhGetIssAclUserDefinedFilterStatus ((UINT4) i4FilterNo, &i4Status)) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid UDB Filter \r\n");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Retrieve User Defined Filter Status %d for Filter ID %d\n",
                      i4Status, i4FilterNo);
        return (CLI_FAILURE);
    }
    /* If Action is Redirection for Filter then call our API */
    if (nmhGetIssAclUserDefinedFilterAction ((UINT4) i4FilterNo, &i4ActionOld)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to retrieve User Defined Filter Action %d for Filter Id %d \n",
                      i4ActionOld, i4FilterNo);
        return (CLI_FAILURE);
    }
    if (i4ActionOld == ISS_REDIRECT_TO)
    {
        nmhGetIssAclUserDefinedFilterRedirectId ((UINT4) i4FilterNo,
                                                 (INT4 *) &u4UdbRedirectIndex);
        if (u4UdbRedirectIndex == 0)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI: UserDefined Redirect Index retrieve  zero %d for Filter ID %d\n",
                          u4UdbRedirectIndex, i4FilterNo);
            return CLI_FAILURE;
        }
    }
    nmhGetIssAclUserDefinedFilterInPortList ((UINT4) i4FilterNo, &PortList);
    MEMCPY (OldPortList.pu1_OctetList, PortList.pu1_OctetList,
            PortList.i4_Length);
    OldPortList.i4_Length = PortList.i4_Length;
    ACL_ADD_PORT_LIST (au1PortList, u4IfIndex);

    /* Enable ACL Counter if required */
    if ((nmhGetIssAclUserDefinedFilterStatsEnabledStatus
         ((UINT4) i4FilterNo, &i4StatsOld)) == SNMP_SUCCESS)
    {
        if ((i4StatsOld == ACL_STAT_DISABLE) && (i4Stats == ACL_STAT_ENABLE))
        {
            if ((AclUserDefinedStatsSetEnabledStatus
                 (CliHandle, i4FilterNo, i4Stats)) == CLI_FAILURE)
            {
                CLI_SET_ERR (CLI_ACL_HW_UPDATE_FAILED);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to enable statistics for Filter ID %d \n",
                              i4FilterNo);
                return CLI_FAILURE;
            }
        }
    }
    else
    {
        CliPrintf (CliHandle, "\r%% Invalid UDB Filter Statistics Status \r\n");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Retrieve Udb Filter Statistics Status %d for Filter ID %d\n",
                      i4StatsOld, i4FilterNo);
    }

    if (i4Status == ISS_ACTIVE)
    {
        if (nmhTestv2IssAclUserDefinedFilterStatus
            (&u4ErrCode, (UINT4) i4FilterNo,
             ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test User Defined Filter Status for filter ID %d with  error code as %d\n",
                          i4FilterNo, u4ErrCode);
            return (CLI_FAILURE);
        }
        if (nmhSetIssAclUserDefinedFilterStatus
            ((UINT4) i4FilterNo, ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set User Defined Filter Status for filter ID %d\n",
                          i4FilterNo);
            return (CLI_FAILURE);
        }
        if (u4UdbRedirectIndex != 0)
        {
            nmhGetIssRedirectInterfaceGrpStatus (u4UdbRedirectIndex,
                                                 &i4RedirectStatus);
            if (i4RedirectStatus == ISS_ACTIVE)
            {
                if (nmhTestv2IssRedirectInterfaceGrpStatus (&u4ErrCode,
                                                            u4UdbRedirectIndex,
                                                            ISS_NOT_IN_SERVICE)
                    == SNMP_FAILURE)
                {
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to test User Defined  Filter Redirect Index %d with  error code as %d\n",
                                  u4UdbRedirectIndex, u4ErrCode);
                    return (CLI_FAILURE);
                }
                if (nmhSetIssRedirectInterfaceGrpStatus (u4UdbRedirectIndex,
                                                         ISS_NOT_IN_SERVICE) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to set User Defined  Filter Redirect Index %d\n",
                                  u4UdbRedirectIndex);
                    return (CLI_FAILURE);
                }
            }
        }
    }
/* Based on the direction we need to set the OutPortlist or the InPortlist
 *      * The status of the filter entry is set to NOT_IN_SERVICE and the port-list
 *           * should be updated.
 *                */
    if (nmhTestv2IssAclUserDefinedFilterInPortList
        (&u4ErrCode, (UINT4) i4FilterNo, &PortList) == SNMP_FAILURE)
    {
        if (nmhSetIssAclUserDefinedFilterStatus ((UINT4) i4FilterNo, i4Status)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to Set User Defined Filter Status %d for Filter ID %d\n",
                          i4Status, i4FilterNo);
            return (CLI_FAILURE);
        }
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test User Defined Filter InPortList for Filter ID %dwith  error code as %d\n",
                      i4FilterNo, u4ErrCode);
        return (CLI_FAILURE);
    }
    if (nmhSetIssAclUserDefinedFilterInPortList ((UINT4) i4FilterNo, &PortList)
        == SNMP_FAILURE)
    {
        nmhSetIssAclUserDefinedFilterStatus ((UINT4) i4FilterNo, i4Status);
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to set User Defined Filter InPortList for Filter ID %d\n",
                      i4FilterNo);
        return (CLI_FAILURE);
    }
    /* Activate the filter entry after configuring all the necessary parameters */
    if (nmhTestv2IssAclUserDefinedFilterStatus (&u4ErrCode, (UINT4) i4FilterNo,
                                                ISS_ACTIVE) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test User Defined Filter Status for Filter ID %d with  error code as %d\n",
                      i4FilterNo, u4ErrCode);
        return (CLI_FAILURE);
    }
    if (nmhSetIssAclUserDefinedFilterStatus ((UINT4) i4FilterNo, ISS_ACTIVE) ==
        SNMP_FAILURE)
    {
        nmhSetIssAclUserDefinedFilterInPortList ((UINT4) i4FilterNo,
                                                 &OldPortList);
        nmhSetIssAclUserDefinedFilterStatus ((UINT4) i4FilterNo, i4Status);
        CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to set User Defined Filter Status for Filter ID %d\n",
                      i4FilterNo);
        return (CLI_FAILURE);
    }
    if (u4UdbRedirectIndex != 0)
    {
        if (nmhTestv2IssRedirectInterfaceGrpStatus
            (&u4ErrCode, u4UdbRedirectIndex, ISS_ACTIVE) == SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test User Defined  Filter Redirect Index %d with  error code as %d\n",
                          u4UdbRedirectIndex, u4ErrCode);
            return (CLI_FAILURE);
        }
        if (nmhSetIssRedirectInterfaceGrpStatus (u4UdbRedirectIndex,
                                                 ISS_ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set User Defined  Filter Redirect Index %d\n",
                          u4UdbRedirectIndex);
            return (CLI_FAILURE);
        }
    }
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclUserDefinedAccessGroup function \n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclNoUserDefinedAccessGroup                        */
/*                                                                           */
/*     DESCRIPTION      : This function removes an UDB acls from an interface*/
/*                                                                           */
/*     INPUT            : i4FilterNo - UDB ACL number                        */
/*                        i4Direction - In or out                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclNoUserDefinedAccessGroup (tCliHandle CliHandle, INT4 i4FilterNo,
                             INT4 i4Direction, INT4 i4Stats)
{
    tSNMP_OCTET_STRING_TYPE PortList;
    UINT1               au1PortList[ISS_PORTLIST_LEN];
    UINT1               au1PortListAll[ISS_PORTLIST_LEN];
    UINT4               u4IfIndex = 0;
    INT4                i4CurrentFilter = 0;
    INT4                i4StatsOld = ACL_STAT_DISABLE;
    UINT1               u1Flag = 1;
    UINT4               u4ErrCode = 0;
    ISS_TRC (INIT_SHUT_TRC,
             "\nCLI:Entry AclNoUserDefinedAccessGroup function \n");
    MEMSET (au1PortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1PortListAll, 0, ISS_PORTLIST_LEN);
    PortList.i4_Length = ISS_PORTLIST_LEN;
    PortList.pu1_OctetList = &au1PortList[0];
    if (i4Direction != ACL_ACCESS_IN)
    {
        CliPrintf (CliHandle, "\r%% UserDefined Filter is Applicable only for"
                   "Ingress port \r\n");
        return (CLI_FAILURE);
    }
    /*Get the interface index from the current mode in CLI */
    u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
    if (i4FilterNo == 0)
    {
        u1Flag = 0;
        if (nmhGetFirstIndexIssAclUserDefinedFilterTable ((UINT4 *) &i4FilterNo)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n No UDB have been configured\r\n");
            return (CLI_SUCCESS);
        }
    }
    do
    {
        i4CurrentFilter = i4FilterNo;

        /* Disable ACL Counter alone if hardware-count token is present in CLI */
        if (i4Stats == ACL_STAT_DISABLE)
        {
            if ((nmhGetIssAclUserDefinedFilterStatsEnabledStatus
                 ((UINT4) i4FilterNo, &i4StatsOld)) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Invalid UDB Filter Statistics Status \r\n");
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to Retrieve Udb Filter Statistics Status %d for Filter ID %d\n",
                              i4StatsOld, i4FilterNo);
                continue;
            }
            /* Disable ACL Counter if already enabled */
            if (i4StatsOld == ACL_STAT_ENABLE)
            {
                if ((AclUserDefinedStatsSetEnabledStatus
                     (CliHandle, i4FilterNo, i4Stats)) == CLI_FAILURE)
                {
                    continue;
                }
            }
            /* Ignore ACL de-association with interface and proceed with next ACL */
            continue;
        }

        if (i4Direction == ACL_ACCESS_IN)
        {
            nmhGetIssAclUserDefinedFilterInPortList ((UINT4) i4FilterNo,
                                                     &PortList);
        }
        if (CliIsMemberPort (PortList.pu1_OctetList,
                             ISS_PORTLIST_LEN, u4IfIndex) == CLI_SUCCESS)
        {
            ACL_DEL_PORT_LIST (au1PortList, u4IfIndex);
            /* If there are no ports left in the port list, then apply the filter
             * on all ports . This happens when portlist is 0 */
            /* Filter exists for this interface */
            if (nmhTestv2IssAclUserDefinedFilterStatus
                (&u4ErrCode, (UINT4) i4FilterNo,
                 ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to test User Defined Filter Status for Filter ID %d with  error code as %d\n",
                              i4FilterNo, u4ErrCode);
                return (CLI_FAILURE);
            }
            if (nmhSetIssAclUserDefinedFilterStatus
                ((UINT4) i4FilterNo, ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set User Defined Filter Status for Filter ID %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }
            if (nmhTestv2IssAclUserDefinedFilterInPortList
                (&u4ErrCode, (UINT4) i4FilterNo, &PortList) == SNMP_FAILURE)
            {
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to test User Defined Filter InPortList for Filter ID %dwith  error code as %d\n",
                              i4FilterNo, u4ErrCode);
                return (CLI_FAILURE);
            }
            if (nmhSetIssAclUserDefinedFilterInPortList
                ((UINT4) i4FilterNo, &PortList) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set User Defined Filter InPortList for Filter ID %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }
            if (nmhSetIssAclUserDefinedFilterStatus
                ((UINT4) i4FilterNo, ISS_ACTIVE) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set User Defined Filter Status for Filter ID %d\n",
                              i4FilterNo);
                return (CLI_FAILURE);
            }
            return (CLI_SUCCESS);
        }
        else if (u1Flag == 1)
        {
            CliPrintf (CliHandle,
                       "\r%% This filter has not been configured for this interface\r\n");
            return (CLI_SUCCESS);
        }
    }
    while ((nmhGetNextIndexIssAclUserDefinedFilterTable
            ((UINT4) i4CurrentFilter, (UINT4 *) &i4FilterNo) == SNMP_SUCCESS)
           && (u1Flag == 0));
    ISS_TRC (INIT_SHUT_TRC,
             "\nCLI:Exit AclNoUserDefinedAccessGroup function \n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclShowAccessLists                                 */
/*                                                                           */
/*     DESCRIPTION      : This function displays all the acls conifgured in  */
/*                        switch                                             */
/*                                                                           */
/*     INPUT            : CliHandle - CLI HAndler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclShowAccessLists (tCliHandle CliHandle, INT4 i4FilterType, INT4 i4FilterNo)
{
    INT4                i4NextFilter = 0;
    INT4                i4PrevFilter;
    UINT1               u1Quit = CLI_FAILURE;

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclShowAccessLists function \n");
    if (i4FilterNo != 0)
    {
        if (i4FilterType == CLI_IP_ACL)
        {
            if (AclShowL3Filter (CliHandle, i4FilterNo) == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid IP Access List \r\n");
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to show L3 Filter ID %d\n",
                              i4FilterNo);
                return CLI_FAILURE;
            }
        }
        else if (i4FilterType == CLI_MAC_ACL)
        {
            if (AclShowL2Filter (CliHandle, i4FilterNo) == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid Mac Access List \r\n");
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to show L2 Filter ID %d\n",
                              i4FilterNo);
                return CLI_FAILURE;
            }
        }
        else if (i4FilterType == CLI_UDB_ACL)
        {
            if (AclShowUDBFilter (CliHandle, i4FilterNo) == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid User-defined List \r\n");
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to show User defined Filter ID %d\n",
                              i4FilterNo);
                return CLI_FAILURE;
            }
        }
        else
        {
            if (AclShowL3Filter (CliHandle, i4FilterNo) == CLI_SUCCESS)
            {
                u1Quit = CLI_SUCCESS;
            }
            if (AclShowL2Filter (CliHandle, i4FilterNo) == CLI_SUCCESS)
            {
                u1Quit = CLI_SUCCESS;
            }
            if (AclShowUDBFilter (CliHandle, i4FilterNo) == CLI_SUCCESS)
            {
                u1Quit = CLI_SUCCESS;
            }
            if (u1Quit == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid Access List \r\n");
                return CLI_FAILURE;
            }
        }
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\nIP ACCESS LISTS \r\n");
    CliPrintf (CliHandle, "-----------------\r\n");

    if (nmhGetFirstIndexIssExtL3FilterTable (&i4NextFilter) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r No IP Access Lists have been configured \r\n");
    }
    else
    {
        do
        {
            AclShowL3Filter (CliHandle, i4NextFilter);
            i4PrevFilter = i4NextFilter;
            u1Quit = (UINT1) CliPrintf (CliHandle, "\r\n");
        }
        while ((INT4) (nmhGetNextIndexIssExtL3FilterTable
                       (i4PrevFilter, &i4NextFilter) != SNMP_FAILURE)
               && (u1Quit != CLI_FAILURE));

        if (u1Quit == CLI_FAILURE)
        {
            return (u1Quit);
        }
    }

    CliPrintf (CliHandle, "\r\nMAC ACCESS LISTS\r\n");
    CliPrintf (CliHandle, "-----------------\r\n");

    if (nmhGetFirstIndexIssExtL2FilterTable (&i4NextFilter) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n No MAC Access Lists have been configured\r\n\r\n");
    }
    else
    {
        do
        {
            AclShowL2Filter (CliHandle, i4NextFilter);
            i4PrevFilter = i4NextFilter;
            u1Quit = (UINT1) CliPrintf (CliHandle, "\r\n\r\n");
        }
        while ((INT4) (nmhGetNextIndexIssExtL2FilterTable
                       (i4PrevFilter, &i4NextFilter) != SNMP_FAILURE)
               && (u1Quit != CLI_FAILURE));
        if (u1Quit == CLI_FAILURE)
        {
            return (u1Quit);
        }
    }
    CliPrintf (CliHandle, "\r\nUSER DEFINED LISTS\r\n");
    CliPrintf (CliHandle, "----------------------\r\n");
    if (nmhGetFirstIndexIssAclUserDefinedFilterTable ((UINT4 *) &i4NextFilter)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n No User Defined Lists have been configured\r\n\r\n");
    }
    else
    {
        do
        {
            AclShowUDBFilter (CliHandle, i4NextFilter);
            i4PrevFilter = i4NextFilter;
            u1Quit = (UINT1) CliPrintf (CliHandle, "\r\n\r\n");
        }
        while ((INT4) (nmhGetNextIndexIssAclUserDefinedFilterTable
                       ((UINT4) i4PrevFilter,
                        (UINT4 *) &i4NextFilter) != SNMP_FAILURE)
               && (u1Quit != CLI_FAILURE));
        if (u1Quit == CLI_FAILURE)
        {
            return (u1Quit);
        }
    }
    return (u1Quit);
}

INT4
AclShowL3Filter (tCliHandle CliHandle, INT4 i4NextFilter)
{
    INT4                i4FltrTos = 0;
    INT4                i4FltrPrio = 0;
    INT4                i4MsgCode = ISS_DEFAULT_MSG_CODE;
    INT4                i4MsgType = ISS_DEFAULT_MSG_CODE;
    INT4                i4Protocol = 0;
    INT4                i4FltrAckBit = 0;
    INT4                i4FltrRstBit = 0;
    INT4                i4FltrAction = 0;
    INT4                i4RowStatus = 0;
    INT4                i4SVlan = 0;
    INT4                i4SVlanPrio = 0;
    INT4                i4CVlan = 0;
    INT4                i4CVlanPrio = 0;
    INT4                i4TagType = 0;
    INT4                i4RedirectInterfaceGrpType = 0;
    UINT4               u4SrcIpAddr = 0;
    UINT4               u4SrcIpMask = 0;
    UINT4               u4DstIpAddr = 0;
    UINT4               u4DstIpMask = 0;
    UINT4               u4MinSrcProtPort = 0;
    UINT4               u4MaxSrcProtPort = 0;
    UINT4               u4MinDstProtPort = 0;
    UINT4               u4MaxDstProtPort = 0;
    INT4                i4AddrType = 0;
    INT4                i4Stats = ACL_STAT_DISABLE;
    UINT4               u4FlowId = ISS_FLOWID_INVALID;
    UINT4               u4FilterHits = 0;
    CHR1               *pu1String;
    UINT1               au1String[ISS_MAX_LEN];
    UINT1               au1InPortList[ISS_PORTLIST_LEN];
    UINT1               au1OutPortList[ISS_PORTLIST_LEN];
    UINT1               au1InPortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];
    UINT1               au1OutPortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    tSNMP_OCTET_STRING_TYPE InPortList;
    tSNMP_OCTET_STRING_TYPE OutPortList;
    tSNMP_OCTET_STRING_TYPE RedirectsPorts;
    tSNMP_OCTET_STRING_TYPE InPortChannelList;
    tSNMP_OCTET_STRING_TYPE OutPortChannelList;
    INT1               *piIfName;
    INT4                i4Count;
    INT4                i4CommaCount;
    UINT1               u1StdAclFlag;
    INT4                i4FltrDscp = 0;
    UINT4               u4SrcIpPrefixLength;
    UINT4               u4DstIpPrefixLength;
    UINT4               u4NewRedirectGrpIndex = 0;
    UINT1               au1Temp[ISS_ADDR_LEN];
    UINT1               au1Temp1[ISS_ADDR_LEN];
    INT4                i4FilterCreationMode = 0;
    tSNMP_OCTET_STRING_TYPE SrcIpAddrOctet;
    tSNMP_OCTET_STRING_TYPE DstIpAddrOctet;
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;
    UINT4               u4InterfaceIndex = 0;
    tPortList          *pPorts = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4NextFilter);
    SrcIpAddrOctet.pu1_OctetList = &au1Temp[0];
    DstIpAddrOctet.pu1_OctetList = &au1Temp1[0];

    InPortList.i4_Length = ISS_PORTLIST_LEN;
    OutPortList.i4_Length = ISS_PORTLIST_LEN;

    InPortList.pu1_OctetList = &au1InPortList[0];
    OutPortList.pu1_OctetList = &au1OutPortList[0];

    InPortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;
    OutPortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;

    InPortChannelList.pu1_OctetList = &au1InPortChannelList[0];
    OutPortChannelList.pu1_OctetList = &au1OutPortChannelList[0];
    pu1String = (CHR1 *) & au1String[0];
    piIfName = (INT1 *) &au1IfName[0];

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclShowL3Filter function \n");

    MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1OutPortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1InPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    MEMSET (au1OutPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1String, 0, ISS_MAX_LEN);
    MEMSET (au1Temp, 0, ISS_ADDR_LEN);
    MEMSET (au1Temp1, 0, ISS_ADDR_LEN);

    if (nmhValidateIndexInstanceIssExtL3FilterTable (i4NextFilter) ==
        SNMP_FAILURE)
    {
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Validate Index Instance for L3 Filter %d\n",
                      i4NextFilter);
        return CLI_FAILURE;
    }

    u1StdAclFlag = 0;
    /* Standard ACLs can have numbers upto 1000 */
    if (i4NextFilter < 1001)
    {
        u1StdAclFlag = 1;
    }

    if (!u1StdAclFlag)
    {
        nmhGetIssExtL3FilterProtocol (i4NextFilter, &i4Protocol);

        AclPbGetL3Filter (i4NextFilter, &i4SVlan, &i4SVlanPrio,
                          &i4CVlan, &i4CVlanPrio, &i4TagType);
    }

    if (i4Protocol == ISS_PROT_ICMP)
    {
        nmhGetIssExtL3FilterMessageType (i4NextFilter, &i4MsgType);
        nmhGetIssExtL3FilterMessageCode (i4NextFilter, &i4MsgCode);
    }

    nmhGetIssExtL3FilterPriority (i4NextFilter, &i4FltrPrio);
    nmhGetIssAclL3FilteAddrType (i4NextFilter, &i4AddrType);
    nmhGetIssAclL3FilterSrcIpAddr (i4NextFilter, &SrcIpAddrOctet);

    nmhGetIssAclL3FilterSrcIpAddrPrefixLength (i4NextFilter,
                                               &u4SrcIpPrefixLength);
    IssUtlConvertPrefixToMask (u4SrcIpPrefixLength, &u4SrcIpMask);
    nmhGetIssAclL3FilterDstIpAddr (i4NextFilter, &DstIpAddrOctet);
    nmhGetIssAclL3FilterDstIpAddrPrefixLength (i4NextFilter,
                                               &u4DstIpPrefixLength);
    IssUtlConvertPrefixToMask (u4DstIpPrefixLength, &u4DstIpMask);

    nmhGetIssAclL3FilterFlowId (i4NextFilter, &u4FlowId);
    nmhGetIssAclL3FilterInPortList (i4NextFilter, &InPortList);
    nmhGetIssAclL3FilterOutPortList (i4NextFilter, &OutPortList);

    nmhGetIssAclL3FilterInPortChannelList (i4NextFilter, &InPortChannelList);
    nmhGetIssAclL3FilterOutPortChannelList (i4NextFilter, &OutPortChannelList);
    if (i4Protocol == ISS_PROT_TCP)
    {
        nmhGetIssExtL3FilterAckBit (i4NextFilter, &i4FltrAckBit);
        nmhGetIssExtL3FilterRstBit (i4NextFilter, &i4FltrRstBit);
    }

    if (i4NextFilter > ACL_STANDARD_MAX_VALUE)
    {
        /* TOS bits */
        nmhGetIssExtL3FilterTos (i4NextFilter, &i4FltrTos);
        nmhGetIssExtL3FilterDscp (i4NextFilter, &i4FltrDscp);
    }

    /* Source and destination protocol port ranges for TCP/UDP */
    if ((i4Protocol == ISS_PROT_TCP) || (i4Protocol == ISS_PROT_UDP))
    {
        nmhGetIssExtL3FilterMinSrcProtPort (i4NextFilter, &u4MinSrcProtPort);
        nmhGetIssExtL3FilterMaxSrcProtPort (i4NextFilter, &u4MaxSrcProtPort);
        nmhGetIssExtL3FilterMinDstProtPort (i4NextFilter, &u4MinDstProtPort);
        nmhGetIssExtL3FilterMaxDstProtPort (i4NextFilter, &u4MaxDstProtPort);
    }

    nmhGetIssExtL3FilterAction (i4NextFilter, &i4FltrAction);
    nmhGetIssExtL3FilterStatus (i4NextFilter, &i4RowStatus);
    nmhGetIssAclL3FilterCreationMode (i4NextFilter, &i4FilterCreationMode);

    /* Standard ACLs can have numbers upto 1000 */
    if (i4NextFilter < 1001)
    {
        CliPrintf (CliHandle,
                   "\r\nStandard IP Access List %d\r\n", i4NextFilter);
        CliPrintf (CliHandle, "----------------------------\r\n");
    }
    /* Extended ACLs can have numbers in the range 1001-65535 */
    else
    {
        CliPrintf (CliHandle,
                   "\r\nExtended IP Access List %d\r\n", i4NextFilter);
        CliPrintf (CliHandle, "-----------------------------\r\n");
    }

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1String, 0, ISS_MAX_LEN);

    /* protocol type fields are not
     * configurable for Standard ACL's */
    CliPrintf (CliHandle, "%-33s : %d\r\n", " Filter Priority", i4FltrPrio);

    if (!u1StdAclFlag)
    {
        CliPrintf (CliHandle, "%-33s : ", " Filter Protocol Type");
        switch (i4Protocol)
        {
            case ISS_PROT_ICMP:
                CliPrintf (CliHandle, "ICMP\r\n");
                break;
            case ISS_PROT_IGMP:
                CliPrintf (CliHandle, "IGMP\r\n");
                break;
            case ISS_PROT_GGP:
                CliPrintf (CliHandle, "GGP\r\n");
                break;
            case ISS_PROT_IP:
                CliPrintf (CliHandle, "IP\r\n");
                break;
            case ISS_PROT_TCP:
                CliPrintf (CliHandle, "TCP\r\n");
                break;
            case ISS_PROT_EGP:
                CliPrintf (CliHandle, "EGP\r\n");
                break;
            case ISS_PROT_IGP:
                CliPrintf (CliHandle, "IGP\r\n");
                break;
            case ISS_PROT_NVP:
                CliPrintf (CliHandle, "NVP\r\n");
                break;
            case ISS_PROT_UDP:
                CliPrintf (CliHandle, "UDP\r\n");
                break;
            case ISS_PROT_IRTP:
                CliPrintf (CliHandle, "IRTP\r\n");
                break;
            case ISS_PROT_IDPR:
                CliPrintf (CliHandle, "IDPR\r\n");
                break;
            case ISS_PROT_RSVP:
                CliPrintf (CliHandle, "RSVP\r\n");
                break;
            case ISS_PROT_MHRP:
                CliPrintf (CliHandle, "MHRP\r\n");
                break;
            case ISS_PROT_IGRP:
                CliPrintf (CliHandle, "IGRP\r\n");
                break;
            case ISS_PROT_OSPFIGP:
                CliPrintf (CliHandle, "OSPF\r\n");
                break;
            case ISS_PROT_PIM:
                CliPrintf (CliHandle, "PIM\r\n");
                break;
            case ISS_IP_PROTO_DEF:
                CliPrintf (CliHandle, "ANY\r\n");
                break;
            default:
                CliPrintf (CliHandle, "%d\r\n", i4Protocol);
                break;
        }
        if (i4Protocol == ISS_PROT_ICMP)
        {

            switch (i4MsgType)
            {
                case ISS_ECHO_REPLY:
                    CliPrintf (CliHandle,
                               "%-33s : Echo reply\r\n", " ICMP type");
                    break;
                case ISS_DEST_UNREACH:
                    CliPrintf (CliHandle,
                               "%-33s : Destination unreachable\r\n",
                               " ICMP type");
                    break;
                case ISS_SRC_QUENCH:
                    CliPrintf (CliHandle,
                               "%-33s : Source quench\r\n", " ICMP type");
                    break;
                case ISS_REDIRECT:
                    CliPrintf (CliHandle, "%-33s : Redirect\r\n", " ICMP type");
                    break;
                case ISS_ECHO_REQ:
                    CliPrintf (CliHandle,
                               "%-33s : Echo request\r\n", " ICMP type");
                    break;
                case ISS_TIME_EXCEED:
                    CliPrintf (CliHandle,
                               "%-33s : Time exceeded\r\n", " ICMP type");
                    break;
                case ISS_PARAM_PROB:
                    CliPrintf (CliHandle,
                               "%-33s : Parameter problem\r\n", " ICMP type");
                    break;
                case ISS_TIMEST_REQ:
                    CliPrintf (CliHandle,
                               "%-33s : Timestamp request\r\n", " ICMP type");
                    break;
                case ISS_TIMEST_REP:
                    CliPrintf (CliHandle,
                               "%-33s : Timestamp reply\r\n", " ICMP type");
                    break;
                case ISS_INFO_REQ:
                    CliPrintf (CliHandle,
                               "%-33s : Information request\r\n", " ICMP type");
                    break;
                case ISS_INTO_REP:
                    CliPrintf (CliHandle,
                               "%-33s : Information reply\r\n", " ICMP type");
                    break;
                case ISS_ADD_MK_REQ:
                    CliPrintf (CliHandle,
                               "%-33s : Address mask request\r\n",
                               " ICMP type");
                    break;
                case ISS_ADD_MK_REP:
                    CliPrintf (CliHandle,
                               "%-33s : Address mask reply\r\n", " ICMP type");
                    break;
                case ISS_NO_ICMP_TYPE:
                case ISS_DEFAULT_MSG_TYPE:
                    CliPrintf (CliHandle,
                               "%-33s : No ICMP types to be filtered\r\n",
                               " ICMP type");
                    break;
                default:
                    CliPrintf (CliHandle,
                               "%-33s : %d\r\n", " ICMP type", i4MsgType);
                    break;
            }

            switch (i4MsgCode)
            {
                case ISS_NET_UNREACH:
                    CliPrintf (CliHandle,
                               "%-33s : Network unreachable\r\n", " ICMP code");
                    break;
                case ISS_HOST_UNREACH:
                    CliPrintf (CliHandle,
                               "%-33s : Host unreachable\r\n", " ICMP code");
                    break;
                case ISS_PROT_UNREACH:
                    CliPrintf (CliHandle,
                               "%-33s : Protocol unreachable\r\n",
                               " ICMP code");
                    break;
                case ISS_PORT_UNREACH:
                    CliPrintf (CliHandle,
                               "%-33s : Port unreachable\r\n", " ICMP code");
                    break;
                case ISS_FRG_NEED:
                    CliPrintf (CliHandle,
                               "%-33s : Fragment need\r\n", " ICMP code");
                    break;
                case ISS_SRC_RT_FAIL:
                    CliPrintf (CliHandle,
                               "%-33s : Source route failed\r\n", " ICMP code");
                    break;
                case ISS_DST_NET_UNK:
                    CliPrintf (CliHandle,
                               "%-33s : Destination network unknown\r\n",
                               " ICMP code");
                    break;
                case ISS_DST_HOST_UNK:
                    CliPrintf (CliHandle,
                               "%-33s : Destination host unknown\r\n",
                               " ICMP code");
                    break;
                case ISS_SRC_HOST_ISO:
                    CliPrintf (CliHandle,
                               "%-33s : Source host isolated\r\n",
                               " ICMP code");
                    break;
                case ISS_DST_NET_ADMP:
                    CliPrintf (CliHandle,
                               "%-33s : Destination network "
                               "administratively prohibited\r\n", " ICMP code");
                    break;
                case ISS_DST_HOST_ADMP:
                    CliPrintf (CliHandle,
                               "%-33s : Destination host"
                               "administratively prohibited\r\n", " ICMP code");
                    break;
                case ISS_NET_UNRE_TOS:
                    CliPrintf (CliHandle,
                               "%-33s : Network unreachable TOS\r\n",
                               " ICMP code");
                    break;
                case ISS_HOST_UNRE_TOS:
                    CliPrintf (CliHandle,
                               "%-33s : Host unreachable TOS\r\n",
                               " ICMP code");
                    break;
                case ISS_NO_ICMP_CODE:
                case ISS_DEFAULT_MSG_CODE:
                    CliPrintf (CliHandle,
                               "%-33s : No ICMP codes to be filtered\r\n",
                               " ICMP code");
                    break;
                default:
                    CliPrintf (CliHandle,
                               "%-33s : %d\r\n", " ICMP code", i4MsgCode);
                    break;
            }
        }
    }
    /*filter source IP address mask */
    if (i4AddrType == QOS_IPV6)
    {
        CliPrintf (CliHandle, "%-33s : %-17s\r\n", " IP address Type", "IPV6");
    }
    if (i4AddrType == QOS_IPV4)
    {
        CliPrintf (CliHandle, "%-33s : %-17s\r\n", " IP address Type", "IPV4");

    }

    /*filter source IP address mask */
    if (i4AddrType == QOS_IPV6)
    {
        if (IS_ADDR_UNSPECIFIED (pIssExtL3FilterEntry->ipv6SrcIpAddress))
        {

            CliPrintf (CliHandle, "%-33s : %-17s\r\n", " Source IP address",
                       ACL_PRINT_ANY);
        }
        else
        {
            CliPrintf (CliHandle, "%-33s : %-17s\r\n", " Source IP address",
                       Ip6PrintNtop ((tIp6Addr *) &
                                     (pIssExtL3FilterEntry->ipv6SrcIpAddress)));
        }
    }
    else
    {
        MEMSET (pu1String, 0, STRLEN (pu1String));
        PTR_FETCH4 (u4SrcIpAddr, SrcIpAddrOctet.pu1_OctetList);
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SrcIpAddr);
        if (STRCMP (pu1String, "0.0.0.0") == 0)
        {
            MEMSET (pu1String, 0, STRLEN (pu1String));
            MEMCPY (pu1String, "ANY", STRLEN ("ANY"));
        }
        CliPrintf (CliHandle,
                   "%-33s : %-17s\r\n", " Source IP address", pu1String);

    }

    MEMSET (au1String, 0, ISS_MAX_LEN);

    if (i4AddrType == QOS_IPV4)
    {
        /*filter source IP address mask */
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SrcIpMask);
        if (STRCMP (pu1String, "0.0.0.0") == 0)
        {
            MEMSET (pu1String, 0, STRLEN (pu1String));
            MEMCPY (pu1String, "ANY", STRLEN ("ANY"));
        }
        CliPrintf (CliHandle,
                   "%-33s : %-17s\r\n", " Source IP address mask", pu1String);
    }
    MEMSET (au1String, 0, ISS_MAX_LEN);

    if ((i4AddrType == QOS_IPV6) &&
        (IS_ADDR_UNSPECIFIED (pIssExtL3FilterEntry->ipv6SrcIpAddress)))
    {
        CliPrintf (CliHandle,
                   "%-33s : %-17s\r\n", " Source IP Prefix Length",
                   ACL_PRINT_ANY);
    }
    else
    {
        CliPrintf (CliHandle,
                   "%-33s : %-17d\r\n", " Source IP Prefix Length",
                   u4SrcIpPrefixLength);
    }

    /*filter destination IP address */
    if (i4AddrType == QOS_IPV6)
    {
        if (IS_ADDR_UNSPECIFIED (pIssExtL3FilterEntry->ipv6DstIpAddress))
        {

            CliPrintf (CliHandle, "%-33s : %-17s\r\n",
                       " Destination IP address", ACL_PRINT_ANY);
        }
        else
        {
            CliPrintf (CliHandle, "%-33s : %-17s\r\n",
                       " Destination IP address",
                       Ip6PrintNtop ((tIp6Addr *) &
                                     (pIssExtL3FilterEntry->ipv6DstIpAddress)));
        }
    }
    else
    {
        PTR_FETCH4 (u4DstIpAddr, DstIpAddrOctet.pu1_OctetList);
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4DstIpAddr);

        if (STRCMP (pu1String, "0.0.0.0") == 0)
        {
            MEMSET (pu1String, 0, STRLEN (pu1String));
            MEMCPY (pu1String, "ANY", STRLEN ("ANY"));
        }

        CliPrintf (CliHandle,
                   "%-33s : %-17s\r\n", " Destination IP address", pu1String);
    }

    MEMSET (au1String, 0, ISS_MAX_LEN);
    if (i4AddrType == QOS_IPV4)
    {
        /*filter destination IP address */
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4DstIpMask);
        if (STRCMP (pu1String, "0.0.0.0") == 0)
        {
            MEMSET (pu1String, 0, STRLEN (pu1String));
            MEMCPY (pu1String, "ANY", STRLEN ("ANY"));
        }
        CliPrintf (CliHandle,
                   "%-33s : %-17s\r\n", " Destination IP address mask",
                   pu1String);
    }
    MEMSET (au1String, 0, ISS_MAX_LEN);

    if ((i4AddrType == QOS_IPV6) &&
        (IS_ADDR_UNSPECIFIED (pIssExtL3FilterEntry->ipv6DstIpAddress)))
    {
        CliPrintf (CliHandle,
                   "%-33s : %-17s\r\n", " Destination IP Prefix Length",
                   ACL_PRINT_ANY);
    }
    else
    {
        CliPrintf (CliHandle,
                   "%-33s : %-17d\r\n", " Destination IP Prefix Length",
                   u4DstIpPrefixLength);
    }
    if (u4FlowId != ISS_FLOWID_INVALID)
    {
        CliPrintf (CliHandle, "%-33s : %-17d\r\n", " Flow Identifier ",
                   u4FlowId);
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : %-17s\r\n", " Flow Identifier ", "NIL");
    }

    /* in ports */

    CliPrintf (CliHandle, "%-33s : ", " In Port List");

    i4Count = 0;
    i4CommaCount = 0;
    do
    {
        if (CliIsMemberPort (InPortList.pu1_OctetList,
                             ISS_PORTLIST_LEN, i4Count) == CLI_SUCCESS)
        {
            i4CommaCount++;
            CfaCliGetIfName ((UINT4) i4Count, piIfName);
            /* This function will format the display of the port list */
            AclCliPrintPortList (CliHandle, i4CommaCount, (UINT1 *) piIfName);
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        }

        i4Count++;
    }
    while (i4Count <= SYS_DEF_MAX_PHYSICAL_INTERFACES);

    /*If no port list has been configured */
    if (i4CommaCount == 0)
    {
        CliPrintf (CliHandle, "NIL");
    }

    MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
    CliPrintf (CliHandle, "\r\n");

    /* in port channels */

    CliPrintf (CliHandle, "%-33s : ", " In Port Channel List");
    i4Count = 0;
    i4Count = SYS_DEF_MAX_PHYSICAL_INTERFACES + 1;
    i4CommaCount = 0;
    do
    {
        if (CliIsMemberPort (InPortChannelList.pu1_OctetList,
                             ISS_PORT_CHANNEL_LIST_SIZE,
                             i4Count) == CLI_SUCCESS)
        {
            i4CommaCount++;
            CfaCliGetIfName ((UINT4) i4Count, piIfName);
            /* This function will format the display of the port list */
            AclCliPrintPortList (CliHandle, i4CommaCount, (UINT1 *) piIfName);
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        }
        i4Count++;
    }
    while (i4Count <= BRG_MAX_PHY_PLUS_LOG_PORTS);

    /*If no port channel list has been configured */
    if (i4CommaCount == 0)
    {
        CliPrintf (CliHandle, "NIL");
    }

    MEMSET (au1InPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    CliPrintf (CliHandle, "\r\n");
    /* out ports */

    CliPrintf (CliHandle, "%-33s : ", " Out Port List");
    i4Count = 0;
    i4CommaCount = 0;
    do
    {
        if (CliIsMemberPort (OutPortList.pu1_OctetList,
                             ISS_PORTLIST_LEN, i4Count) == CLI_SUCCESS)
        {
            i4CommaCount++;
            CfaCliGetIfName ((UINT4) i4Count, piIfName);
            AclCliPrintPortList (CliHandle, i4CommaCount, (UINT1 *) piIfName);
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        }

        i4Count++;
    }
    while (i4Count <= SYS_DEF_MAX_PHYSICAL_INTERFACES);
    /*If no port list has been configured */
    if (i4CommaCount == 0)
    {
        CliPrintf (CliHandle, "NIL");
    }

    MEMSET (au1OutPortList, 0, ISS_PORTLIST_LEN);
    CliPrintf (CliHandle, "\r\n");

    /* out port channel */

    CliPrintf (CliHandle, "%-33s : ", " Out Port Channel List");
    i4Count = 0;
    i4Count = SYS_DEF_MAX_PHYSICAL_INTERFACES + 1;
    i4CommaCount = 0;
    do
    {
        if (CliIsMemberPort (OutPortChannelList.pu1_OctetList,
                             ISS_PORT_CHANNEL_LIST_SIZE,
                             i4Count) == CLI_SUCCESS)
        {
            i4CommaCount++;
            CfaCliGetIfName ((UINT4) i4Count, piIfName);
            AclCliPrintPortList (CliHandle, i4CommaCount, (UINT1 *) piIfName);
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        }

        i4Count++;
    }
    while (i4Count <= BRG_MAX_PHY_PLUS_LOG_PORTS);
    /*If no port list has been configured */
    if (i4CommaCount == 0)
    {
        CliPrintf (CliHandle, "NIL");
    }

    MEMSET (au1OutPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    CliPrintf (CliHandle, "\r\n");
    if (i4Protocol == ISS_PROT_TCP)
    {
        /* ACK bit */
        if (i4FltrAckBit == ISS_ACK_ESTABLISH)
        {
            CliPrintf (CliHandle, " Filtering ACK bit\r\n");
        }

        /* RST bit */
        if (i4FltrRstBit == ISS_RST_SET)
        {
            CliPrintf (CliHandle, " Filtering RST bit\r\n");
        }

    }
    /* Display TOS if  access list is extended and protocol
     * is not ICMP 
     */
    if ((i4NextFilter > ACL_STANDARD_MAX_VALUE) && (i4FltrTos != -1))
    {

        switch (i4FltrTos)
        {
            case ISS_TOS_NONE:
                CliPrintf (CliHandle, "%-33s : Normal\r\n", " Filter TOS");
                break;
            case ISS_TOS_HI_REL:
                CliPrintf (CliHandle, "%-33s : High reliability\r\n",
                           " Filter TOS");
                break;
            case ISS_TOS_HI_THR:
                CliPrintf (CliHandle, "%-33s : High throughput\r\n",
                           " Filter TOS");
                break;
            case ISS_TOS_HI_REL_HI_THR:
                CliPrintf (CliHandle, "%-33s : High reliability"
                           " and high throughput\r\n", " Filter TOS");
                break;
            case ISS_TOS_LO_DEL:
                CliPrintf (CliHandle, "%-33s : Low delay\r\n", " Filter TOS");
                break;
            case ISS_TOS_LO_DEL_HI_REL:
                CliPrintf (CliHandle, "%-33s : Low delay and high"
                           " reliability\r\n", " Filter TOS");
                break;
            case ISS_TOS_LO_DEL_HI_THR:
                CliPrintf (CliHandle, "%-33s : Low delay and high"
                           " throughput\r\n", " Filter TOS");
                break;
            case ISS_TOS_LO_DEL_HI_THR_HI_REL:
                CliPrintf (CliHandle, "%-33s : Low delay, high"
                           " throughput and high reliability\r\n",
                           " Filter TOS");
                break;
            default:
                CliPrintf (CliHandle, "%-33s : None\r\n", " Filter TOS");
                break;
        }

        if (i4FltrDscp == ISS_DSCP_INVALID)
        {
            CliPrintf (CliHandle, "%-33s : NIL\r\n", " Filter DSCP");
        }
        else
        {
            CliPrintf (CliHandle, "%-33s : %d\r\n", " Filter DSCP", i4FltrDscp);
        }
    }

    /* Source and destination protocol port ranges for TCP/UDP */
    if ((i4Protocol == ISS_PROT_TCP) || (i4Protocol == ISS_PROT_UDP))
    {
        CliPrintf (CliHandle,
                   "%-33s : %d\r\n",
                   " Filter Source Ports From", u4MinSrcProtPort);

        CliPrintf (CliHandle, "%-33s : %d\r\n",
                   " Filter Source Ports Till", u4MaxSrcProtPort);

        CliPrintf (CliHandle, "%-33s : %d\r\n",
                   " Filter Destination Ports From", u4MinDstProtPort);

        CliPrintf (CliHandle, "%-33s : %d\r\n",
                   " Filter Destination Ports Till", u4MaxDstProtPort);
    }

    if (!u1StdAclFlag)
    {
        AclPbShowL3Filter (CliHandle, i4SVlan, i4SVlanPrio, i4CVlan,
                           i4CVlanPrio, i4TagType);
    }
    /* filter action */
    if (i4FltrAction == ISS_ALLOW)
    {
        CliPrintf (CliHandle, "%-33s : Permit\r\n", " Filter Action");
    }
    else if (i4FltrAction == ISS_DROP)
    {
        CliPrintf (CliHandle, "%-33s : Deny\r\n", " Filter Action");
    }
    else if (i4FltrAction == ISS_REDIRECT_TO)
    {
        CliPrintf (CliHandle, "%-33s : Redirect\r\n", " Filter Action");
    }
    else if (i4FltrAction == ISS_SWITCH_COPYTOCPU)
    {
        CliPrintf (CliHandle, "%-33s : Copy-to-cpu\r\n", " Filter Action");
    }
    else if (i4FltrAction == ISS_DROP_COPYTOCPU)
    {
        CliPrintf (CliHandle, "%-33s : Drop Copy-to-cpu\r\n", " Filter Action");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : Unknown\r\n", " Filter Action");
    }

    /* Print Redirect Port List */

    if (i4FltrAction == ISS_REDIRECT_TO)
    {
        nmhGetIssAclL3FilterRedirectId (i4NextFilter,
                                        (INT4 *) &u4NewRedirectGrpIndex);
    }

    i4CommaCount = 0;
    CliPrintf (CliHandle, "%-33s : ", " Redirect Port List");

    if (u4NewRedirectGrpIndex != 0)
    {
        pPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

        if (pPorts == NULL)
        {
            ISS_TRC (ALL_FAILURE_TRC, "\nCLI:pPorts is NULL \n");
            return CLI_FAILURE;
        }

        MEMSET (pPorts, 0, sizeof (tPortList));

        RedirectsPorts.pu1_OctetList = (UINT1 *) pPorts;
        RedirectsPorts.i4_Length = sizeof (tPortList);

        nmhGetIssRedirectInterfaceGrpType (u4NewRedirectGrpIndex,
                                           &i4RedirectInterfaceGrpType);

        nmhGetIssRedirectInterfaceGrpPortList (u4NewRedirectGrpIndex,
                                               &RedirectsPorts);

        MEMCPY (&u4InterfaceIndex, RedirectsPorts.pu1_OctetList,
                sizeof (UINT4));
        CfaCliGetIfName (u4InterfaceIndex, piIfName);
        CliPrintf (CliHandle, "%s ", piIfName);
        MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

        FsUtilReleaseBitList ((UINT1 *) pPorts);
    }

    CliPrintf (CliHandle, "\r\n");

    /* Filter Creation Mode */
    if (i4FilterCreationMode == ISS_ACL_CREATION_INTERNAL)
    {
        CliPrintf (CliHandle, "%-33s : Internal\r\n", " Filter Creation Mode");
    }
    else if (i4FilterCreationMode == ISS_ACL_CREATION_EXTERNAL)
    {
        CliPrintf (CliHandle, "%-33s : External\r\n", " Filter Creation Mode");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : Unknown\r\n", " Filter Creation Mode");
    }

    if (i4RowStatus == ISS_ACTIVE)
    {
        CliPrintf (CliHandle, "%-33s : Active", " Status");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : InActive", " Status");
    }
    CliPrintf (CliHandle, "\r\n");

    /* Filter Statistics */
    if ((nmhGetIssAclL3FilterStatsEnabledStatus (i4NextFilter, &i4Stats)) ==
        SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Retrieve L3 Filter Statistics Status %d for Filter ID %d\n",
                      i4Stats, i4NextFilter);
    }
    if ((nmhGetIssAclL3FilterMatchCount (i4NextFilter, &u4FilterHits)) ==
        SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Retrieve L3 Filter Statistics %u for Filter ID %d\n",
                      u4FilterHits, i4NextFilter);
    }
    if (i4Stats == ACL_STAT_ENABLE)
    {
        CliPrintf (CliHandle, "%-33s : Enabled\r\n", " Counter Status");
        CliPrintf (CliHandle, "%-33s : %u", " Match Count", u4FilterHits);
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : Disabled\r\n", " Counter Status");
        CliPrintf (CliHandle, "%-33s : %u", " Match Count", u4FilterHits);
    }
    CliPrintf (CliHandle, "\r\n\r\n");
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclShowL3Filter  function \n");
    return CLI_SUCCESS;
}

INT4
AclShowL2Filter (tCliHandle CliHandle, INT4 i4NextFilter)
{
    tMacAddr            DstMacAddr;
    tMacAddr            SrcMacAddr;
    INT4                i4FltrPrio;
    INT4                i4Protocol;
    INT4                i4VlanId;
    INT4                i4FltrAction;
    INT4                i4RowStatus;
    INT4                i4Count;
    INT4                i4CommaCount;
    INT4                i4OuterEType;
    INT4                i4EtherType;
    INT4                i4SVlan;
    INT4                i4SVlanPrio;
    INT4                i4CVlanPrio;
    INT4                i4TagType;
    INT4                i4FilterCreationMode = 0;
    INT4                i4RedirectInterfaceGrpType = 0;
    INT4                i4Stats = ACL_STAT_DISABLE;
    CHR1               *pu1String;
    INT1               *piIfName;
    UINT1               au1String[ISS_MAX_LEN];
    UINT1               au1InPortList[ISS_PORTLIST_LEN];
    UINT1               au1OutPortList[ISS_PORTLIST_LEN];
    UINT1               au1InPortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];
    UINT1               au1OutPortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4FilterHits = 0;
    tSNMP_OCTET_STRING_TYPE InPortList;
    tSNMP_OCTET_STRING_TYPE OutPortList;
    tSNMP_OCTET_STRING_TYPE InPortChannelList;
    tSNMP_OCTET_STRING_TYPE OutPortChannelList;
    tSNMP_OCTET_STRING_TYPE RedirectsPorts;
    UINT4               u4NewRedirectGrpIndex = 0;
    UINT4               u4InterfaceIndex = 0;
    tPortList          *pPorts = NULL;

    InPortList.i4_Length = ISS_PORTLIST_LEN;
    OutPortList.i4_Length = ISS_PORTLIST_LEN;

    InPortList.pu1_OctetList = &au1InPortList[0];
    OutPortList.pu1_OctetList = &au1OutPortList[0];

    InPortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;
    OutPortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;

    InPortChannelList.pu1_OctetList = &au1InPortChannelList[0];
    OutPortChannelList.pu1_OctetList = &au1OutPortChannelList[0];

    pu1String = (CHR1 *) & au1String[0];
    piIfName = (INT1 *) &au1IfName[0];

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclShowL2Filter function \n");

    MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1OutPortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1InPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    MEMSET (au1OutPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1String, 0, ISS_MAX_LEN);

    if (nmhValidateIndexInstanceIssExtL2FilterTable (i4NextFilter) ==
        SNMP_FAILURE)
    {
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to validate the Index Instance for L2 filter%d\n",
                      i4NextFilter);
        return CLI_FAILURE;
    }
    nmhGetIssExtL2FilterPriority (i4NextFilter, &i4FltrPrio);
    nmhGetIssExtL2FilterEtherType (i4NextFilter, &i4EtherType);
    nmhGetIssExtL2FilterProtocolType (i4NextFilter, (UINT4 *) &i4Protocol);
    nmhGetIssExtL2FilterVlanId (i4NextFilter, &i4VlanId);
    nmhGetIssExtL2FilterDstMacAddr (i4NextFilter, (tMacAddr *) & DstMacAddr);
    nmhGetIssExtL2FilterSrcMacAddr (i4NextFilter, (tMacAddr *) & SrcMacAddr);
    nmhGetIssExtL2FilterInPortList (i4NextFilter, &InPortList);
    nmhGetIssAclL2FilterOutPortList (i4NextFilter, &OutPortList);
    nmhGetIssExtL2FilterInPortChannelList (i4NextFilter, &InPortChannelList);
    nmhGetIssAclL2FilterOutPortChannelList (i4NextFilter, &OutPortChannelList);

    AclPbGetL2Filter (i4NextFilter, &i4OuterEType, &i4SVlan,
                      &i4SVlanPrio, &i4CVlanPrio, &i4TagType);

    nmhGetIssExtL2FilterAction (i4NextFilter, &i4FltrAction);
    nmhGetIssExtL2FilterStatus (i4NextFilter, &i4RowStatus);
    nmhGetIssAclL2FilterCreationMode (i4NextFilter, &i4FilterCreationMode);

    CliPrintf (CliHandle, "\r\nExtended MAC Access List %d\r\n", i4NextFilter);
    CliPrintf (CliHandle, "-----------------------------\r\n");

    CliPrintf (CliHandle, "%-33s : %d\r\n", " Filter Priority", i4FltrPrio);

    CliPrintf (CliHandle, "%-33s : %d\r\n", " Ether Type", i4EtherType);

    CliPrintf (CliHandle, "%-33s : ", " Protocol Type");
    switch (i4Protocol)
    {
        case AARP:
            CliPrintf (CliHandle, "AARP\r\n");
            break;

        case AMBER:
            CliPrintf (CliHandle, "AMBER\r\n");
            break;

        case DEC_SPANNING:
            CliPrintf (CliHandle, "DEC_SPANNING\r\n");
            break;

        case DIAGNOSTIC:
            CliPrintf (CliHandle, "DIAGNOSTIC\r\n");
            break;

        case DSM:
            CliPrintf (CliHandle, "DSM\r\n");
            break;

        case ETYPE_6000:
            CliPrintf (CliHandle, "ETYPE_6000\r\n");
            break;

        case ETYPE_8042:
            CliPrintf (CliHandle, "ETYPE_8042\r\n");
            break;

        case LAT:
            CliPrintf (CliHandle, "LAT\r\n");
            break;

        case LAVC_SCA:
            CliPrintf (CliHandle, "LAVC_SCA\r\n");
            break;

        case MOP_CONSOLE:
            CliPrintf (CliHandle, "MOP_CONSOLE\r\n");
            break;

        case MSDOS:
            CliPrintf (CliHandle, "MSDOS\r\n");
            break;

        case MUMPS:
            CliPrintf (CliHandle, "MUMPS\r\n");
            break;

        case NET_BIOS:
            CliPrintf (CliHandle, "NET_BIOS\r\n");
            break;

        case VINES_ECHO:
            CliPrintf (CliHandle, "VINES_ECHO\r\n");
            break;

        case VINES_IP:
            CliPrintf (CliHandle, "VINES_IP\r\n");
            break;

        case XNS_ID:
            CliPrintf (CliHandle, "XNS_ID\r\n");
            break;

        default:
            CliPrintf (CliHandle, "%d\r\n", i4Protocol);
            break;
    }

    CliPrintf (CliHandle, "%-33s : %d\r\n", " Vlan Id", i4VlanId);

    CliPrintf (CliHandle, "%-33s : ", " Destination MAC Address");
    PrintMacAddress (DstMacAddr, (UINT1 *) pu1String);
    CliPrintf (CliHandle, "%s", pu1String);
    MEMSET (au1String, 0, ISS_MAX_LEN);
    CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle, "%-33s : ", " Source MAC Address");
    PrintMacAddress (SrcMacAddr, (UINT1 *) pu1String);
    CliPrintf (CliHandle, "%s", pu1String);
    MEMSET (au1String, 0, ISS_MAX_LEN);
    CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle, "%-33s : ", " In Port List");
    i4Count = 0;
    i4CommaCount = 0;
    do
    {
        if (CliIsMemberPort (InPortList.pu1_OctetList,
                             ISS_PORTLIST_LEN, i4Count) == CLI_SUCCESS)
        {
            i4CommaCount++;
            CfaCliGetIfName ((UINT4) i4Count, piIfName);
            /* This function will format the display of the port list */
            AclCliPrintPortList (CliHandle, i4CommaCount, (UINT1 *) piIfName);
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        }

        i4Count++;
    }
    while (i4Count <= SYS_DEF_MAX_PHYSICAL_INTERFACES);

    /*If no port list has been configured */
    if (i4CommaCount == 0)
    {
        CliPrintf (CliHandle, "NIL");
    }

    MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
    CliPrintf (CliHandle, "\r\n");

    /* In port channel */
    CliPrintf (CliHandle, "%-33s : ", " In Port Channel List");
    i4Count = 0;
    i4Count = SYS_DEF_MAX_PHYSICAL_INTERFACES + 1;
    i4CommaCount = 0;
    do
    {
        if (CliIsMemberPort (InPortChannelList.pu1_OctetList,
                             ISS_PORT_CHANNEL_LIST_SIZE,
                             i4Count) == CLI_SUCCESS)
        {
            i4CommaCount++;
            CfaCliGetIfName ((UINT4) i4Count, piIfName);
            /* This function will format the display of the port list */
            AclCliPrintPortList (CliHandle, i4CommaCount, (UINT1 *) piIfName);
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        }

        i4Count++;
    }
    while (i4Count <= BRG_MAX_PHY_PLUS_LOG_PORTS);

    /*If no port list has been configured */
    if (i4CommaCount == 0)
    {
        CliPrintf (CliHandle, "NIL");
    }
    MEMSET (au1InPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    CliPrintf (CliHandle, "\r\n");

    /* out ports */

    CliPrintf (CliHandle, "%-33s : ", " Out Port List");
    i4Count = 0;
    i4CommaCount = 0;
    do
    {
        if (CliIsMemberPort (OutPortList.pu1_OctetList,
                             ISS_PORTLIST_LEN, i4Count) == CLI_SUCCESS)
        {
            i4CommaCount++;
            CfaCliGetIfName ((UINT4) i4Count, piIfName);
            AclCliPrintPortList (CliHandle, i4CommaCount, (UINT1 *) piIfName);
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        }

        i4Count++;
    }
    while (i4Count <= SYS_DEF_MAX_PHYSICAL_INTERFACES);
    /*If no port list has been configured */
    if (i4CommaCount == 0)
    {
        CliPrintf (CliHandle, "NIL");
    }

    MEMSET (au1OutPortList, 0, ISS_PORTLIST_LEN);
    CliPrintf (CliHandle, "\r\n");

    /* out port channel */

    CliPrintf (CliHandle, "%-33s : ", " Out Port Channel List");
    i4Count = 0;
    i4Count = SYS_DEF_MAX_PHYSICAL_INTERFACES + 1;
    i4CommaCount = 0;
    do
    {
        if (CliIsMemberPort (OutPortChannelList.pu1_OctetList,
                             ISS_PORT_CHANNEL_LIST_SIZE,
                             i4Count) == CLI_SUCCESS)
        {
            i4CommaCount++;
            CfaCliGetIfName ((UINT4) i4Count, piIfName);
            AclCliPrintPortList (CliHandle, i4CommaCount, (UINT1 *) piIfName);
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        }

        i4Count++;
    }
    while (i4Count <= BRG_MAX_PHY_PLUS_LOG_PORTS);

    /*If no port list has been configured */
    if (i4CommaCount == 0)
    {
        CliPrintf (CliHandle, "NIL");
    }

    MEMSET (au1OutPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    CliPrintf (CliHandle, "\r\n");

    AclPbShowL2Filter (CliHandle, i4OuterEType, i4SVlan, i4SVlanPrio,
                       i4CVlanPrio, i4TagType);

    if (i4FltrAction == ISS_ALLOW)
    {
        CliPrintf (CliHandle, "%-33s : Permit\r\n", " Filter Action");
    }
    else if (i4FltrAction == ISS_DROP)
    {
        CliPrintf (CliHandle, "%-33s : Deny\r\n", " Filter Action");
    }
    else if (i4FltrAction == ISS_REDIRECT_TO)
    {
        CliPrintf (CliHandle, "%-33s : Redirect\r\n", " Filter Action");
    }
    else if (i4FltrAction == ISS_SWITCH_COPYTOCPU)
    {
        CliPrintf (CliHandle, "%-33s : Copy-to-cpu\r\n", " Filter Action");
    }
    else if (i4FltrAction == ISS_DROP_COPYTOCPU)
    {
        CliPrintf (CliHandle, "%-33s : Drop Copy-to-cpu\r\n", " Filter Action");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : Unknown\r\n", " Filter Action");
    }

    /* Print Redirect Port List */

    if (i4FltrAction == ISS_REDIRECT_TO)
    {
        nmhGetIssAclL2FilterRedirectId (i4NextFilter,
                                        (INT4 *) &u4NewRedirectGrpIndex);
    }

    CliPrintf (CliHandle, "%-33s : ", " Redirect Port List");

    if (u4NewRedirectGrpIndex != 0)
    {

        pPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

        if (pPorts == NULL)
        {
            ISS_TRC (ALL_FAILURE_TRC, "\nCLI:pPorts is NULL \n");
            return CLI_FAILURE;
        }

        MEMSET (pPorts, 0, sizeof (tPortList));

        RedirectsPorts.pu1_OctetList = (UINT1 *) pPorts;
        RedirectsPorts.i4_Length = sizeof (tPortList);

        nmhGetIssRedirectInterfaceGrpType (u4NewRedirectGrpIndex,
                                           &i4RedirectInterfaceGrpType);

        nmhGetIssRedirectInterfaceGrpPortList (u4NewRedirectGrpIndex,
                                               &RedirectsPorts);

        MEMCPY (&u4InterfaceIndex, RedirectsPorts.pu1_OctetList,
                sizeof (UINT4));
        CfaCliGetIfName (u4InterfaceIndex, piIfName);
        CliPrintf (CliHandle, "%s ", piIfName);
        MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

        FsUtilReleaseBitList ((UINT1 *) pPorts);
    }

    CliPrintf (CliHandle, "\r\n");

    /* Filter Creation Mode */
    if (i4FilterCreationMode == ISS_ACL_CREATION_INTERNAL)
    {
        CliPrintf (CliHandle, "%-33s : Internal\r\n", " Filter Creation Mode");
    }
    else if (i4FilterCreationMode == ISS_ACL_CREATION_EXTERNAL)
    {
        CliPrintf (CliHandle, "%-33s : External\r\n", " Filter Creation Mode");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : Unknown\r\n", " Filter Creation Mode");
    }

    if (i4RowStatus == ISS_ACTIVE)
    {
        CliPrintf (CliHandle, "%-33s : Active", " Status");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : InActive", " Status");
    }
    CliPrintf (CliHandle, "\r\n");

    /* Filter Statistics */
    if ((nmhGetIssAclL2FilterStatsEnabledStatus (i4NextFilter, &i4Stats)) ==
        SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Retrieve L2 Filter Statistics Status %d for Filter ID %d\n",
                      i4Stats, i4NextFilter);
    }
    if ((nmhGetIssAclL2FilterMatchCount (i4NextFilter, &u4FilterHits)) ==
        SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Retrieve L2 Filter Statistics %u for Filter ID %d\n",
                      u4FilterHits, i4NextFilter);
    }
    if (i4Stats == ACL_STAT_ENABLE)
    {
        CliPrintf (CliHandle, "%-33s : Enabled\r\n", " Counter Status");
        CliPrintf (CliHandle, "%-33s : %u", " Match Count", u4FilterHits);
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : Disabled\r\n", " Counter Status");
        CliPrintf (CliHandle, "%-33s : %u", " Match Count", u4FilterHits);
    }
    CliPrintf (CliHandle, "\r\n\r\n");
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclShowL2Filter  function \n");
    return CLI_SUCCESS;
}

INT4
AclShowUDBFilter (tCliHandle CliHandle, INT4 i4NextFilter)
{
    INT4                i4PktType = 0;
    INT4                i4Base = 0;
    INT4                i4Priority = 0;
    INT4                i4FilterOneType = 0;
    INT4                i4FilterOneId = 0;
    INT4                i4FilterTwoType = 0;
    INT4                i4FilterTwoId = 0;
    INT4                i4Action = 0;
    INT4                i4RowStatus;
    INT4                i4RedirectInterfaceGrpType = 0;
    INT4                i4SubActionId = 0;
    INT4                i4SubAction = 0;
    INT4                i4Count = 0;
    INT4                i4CommaCount = 0;
    INT4                i4TrafficDistByte = 0;
    INT4                i4RedirectUdbPosition = 0;
    INT4                i4Length = 0;
    INT4                i4ColonCount = 0;
    INT4                i4Stats = ACL_STAT_DISABLE;
    UINT4               u4NewRedirectGrpIndex = 0;
    UINT4               u4InterfaceIndex = 0;
    UINT4               u4FilterHits = 0;
    CHR1               *pu1String;
    INT1               *piIfName;
    UINT1               au1String[ISS_MAX_LEN];
    UINT1               au1InPortList[ISS_PORTLIST_LEN];
    UINT1               au1SetValue[ISS_UDB_MAX_OFFSET];
    UINT1               au1SetMask[ISS_UDB_MAX_OFFSET];
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    tSNMP_OCTET_STRING_TYPE InPortList;
    tSNMP_OCTET_STRING_TYPE SetValue;
    tSNMP_OCTET_STRING_TYPE SetMask;
    tSNMP_OCTET_STRING_TYPE RedirectsPorts;
    tPortList          *pPorts = NULL;
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclShowUDBFilter function \n");
    MEMSET (au1SetValue, 0, ISS_UDB_MAX_OFFSET);
    MEMSET (au1SetMask, 0, ISS_UDB_MAX_OFFSET);
    InPortList.i4_Length = ISS_PORTLIST_LEN;
    SetValue.i4_Length = ISS_PORTLIST_LEN;
    SetMask.i4_Length = ISS_PORTLIST_LEN;
    InPortList.pu1_OctetList = &au1InPortList[0];
    SetValue.pu1_OctetList = &au1SetValue[0];
    SetMask.pu1_OctetList = &au1SetMask[0];
    pu1String = (CHR1 *) & au1String[0];
    piIfName = (INT1 *) &au1IfName[0];
    MEMSET (SetValue.pu1_OctetList, 0, sizeof (SetValue.pu1_OctetList));
    MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1String, 0, ISS_MAX_LEN);
    MEMSET (&RedirectsPorts, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    if (nmhValidateIndexInstanceIssAclUserDefinedFilterTable (i4NextFilter) ==
        SNMP_FAILURE)
    {
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "CLI:Failed to Validate IndexInstancefor User Defined Filter %d\n",
                      i4NextFilter);
        return CLI_FAILURE;
    }
    nmhGetIssAclUserDefinedFilterPktType (i4NextFilter, &i4PktType);
    nmhGetIssAclUserDefinedFilterOffSetBase (i4NextFilter, &i4Base);
    nmhGetIssAclUserDefinedFilterOffSetValue (i4NextFilter, &SetValue);
    nmhGetIssAclUserDefinedFilterOffSetMask (i4NextFilter, &SetMask);
    nmhGetIssAclUserDefinedFilterPriority (i4NextFilter, &i4Priority);
    nmhGetIssAclUserDefinedFilterAction (i4NextFilter, &i4Action);
    nmhGetIssAclUserDefinedFilterInPortList (i4NextFilter, &InPortList);
    nmhGetIssAclUserDefinedFilterIdOneType (i4NextFilter, &i4FilterOneType);
    nmhGetIssAclUserDefinedFilterIdOne (i4NextFilter, (UINT4 *) &i4FilterOneId);
    nmhGetIssAclUserDefinedFilterIdTwoType (i4NextFilter, &i4FilterTwoType);
    nmhGetIssAclUserDefinedFilterIdTwo (i4NextFilter, (UINT4 *) &i4FilterTwoId);
    nmhGetIssAclUserDefinedFilterStatus (i4NextFilter, &i4RowStatus);
    nmhGetIssAclUserDefinedFilterSubAction (i4NextFilter, &i4SubAction);
    nmhGetIssAclUserDefinedFilterSubActionId (i4NextFilter, &i4SubActionId);
    CliPrintf (CliHandle, "\r\n User Defined Access List %d\r\n", i4NextFilter);
    CliPrintf (CliHandle, "-----------------------------\r\n");
    CliPrintf (CliHandle, "%-33s : %d\r\n", " Priority", i4Priority);
    CliPrintf (CliHandle, "%-33s : ", " Packet Type");
    switch (i4PktType)
    {
        case ISS_UDB_PKT_TYPE_USER_DEF:
            CliPrintf (CliHandle, "User-Defined\r\n");
            break;
        case ISS_UDB_PKT_TYPE_IPV4_TCP:
            CliPrintf (CliHandle, "TCP\r\n");
            break;
        case ISS_UDB_PKT_TYPE_IPV4_UDP:
            CliPrintf (CliHandle, "UDP\r\n");
            break;
        case ISS_UDB_PKT_TYPE_MPLS:
            CliPrintf (CliHandle, "MPLS\r\n");
            break;
        case ISS_UDB_PKT_TYPE_IPV4:
            CliPrintf (CliHandle, "IPv4\r\n");
            break;
        case ISS_UDB_PKT_TYPE_IPV6:
            CliPrintf (CliHandle, "IPv6\r\n");
            break;
        case ISS_UDB_PKT_TYPE_FRAG:
            CliPrintf (CliHandle, "FRAG-IP\r\n");
            break;
        default:
            CliPrintf (CliHandle, "%d\r\n", i4PktType);
            break;
    }
    CliPrintf (CliHandle, "%-33s : ", " Offset Base");
    switch (i4Base)
    {
        case ISS_UDB_OFFSET_L2:
            CliPrintf (CliHandle, "L2\r\n");
            break;
        case ISS_UDB_OFFSET_L3:
            CliPrintf (CliHandle, "L3\r\n");
            break;
        case ISS_UDB_OFFSET_L4:
            CliPrintf (CliHandle, "L4\r\n");
            break;
        case ISS_UDB_OFFSET_IPV6_EXT_HDR:
            CliPrintf (CliHandle, "IPV6\r\n");
            break;
        case ISS_UDB_OFFSET_L3_MINUS_2:
            CliPrintf (CliHandle, "Ether-Type\r\n");
            break;
        case ISS_UDB_OFFSET_MPLS_MINUS_2:
            CliPrintf (CliHandle, "Ether-Type\r\n");
            break;
        default:
            CliPrintf (CliHandle, "%d\r\n", i4Base);
            break;
    }
    CliPrintf (CliHandle, "%-33s", " OffSet Position");
    CliPrintf (CliHandle, " : ", "");
    i4ColonCount = 0;
    for (i4Length = 0; i4Length < SetValue.i4_Length; i4Length++)
    {
        if (au1SetValue[i4Length] != 0)
        {
            if (i4ColonCount != 0)
            {
                CliPrintf (CliHandle, ":");
            }
            /*Display in HEX:"%02x:" */
            CliPrintf (CliHandle, "%02d", i4Length);
            i4ColonCount++;
        }
    }
    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, "%-33s", " OffSet Value");
    CliPrintf (CliHandle, " : ", "");
    i4ColonCount = 0;
    for (i4Length = 0; i4Length < SetValue.i4_Length; i4Length++)
    {
        if (au1SetValue[i4Length] != 0)
        {
            if (i4ColonCount != 0)
            {
                CliPrintf (CliHandle, ":");
            }
            /*Display in HEX:"%02x:" */
            CliPrintf (CliHandle, "%02d", au1SetValue[i4Length]);
            i4ColonCount++;
        }
    }
    CliPrintf (CliHandle, "\r\n");
    if (i4Action == ISS_ALLOW)
    {
        CliPrintf (CliHandle, "%-33s : Permit\r\n", " Filter Action");
    }
    else if (i4Action == ISS_DROP)
    {
        CliPrintf (CliHandle, "%-33s : Deny\r\n", " Filter Action");
    }
    else if (i4Action == ISS_AND)
    {
        CliPrintf (CliHandle, "%-33s : AND\r\n", " Filter Action");
    }
    else if (i4Action == ISS_OR)
    {
        CliPrintf (CliHandle, "%-33s : OR\r\n", " Filter Action");
    }
    else if (i4Action == ISS_NOT)
    {
        CliPrintf (CliHandle, "%-33s : NOT\r\n", " Filter Action");
    }
    else if (i4Action == ISS_REDIRECT_TO)
    {
        CliPrintf (CliHandle, "%-33s : Redirect\r\n", " Filter Action");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : Unknown\r\n", " Filter Action");
    }
    CliPrintf (CliHandle, "%-33s : ", " In Port List");
    i4Count = 0;
    i4CommaCount = 0;
    do
    {
        if (CliIsMemberPort (InPortList.pu1_OctetList,
                             ISS_PORTLIST_LEN, i4Count) == CLI_SUCCESS)
        {
            i4CommaCount++;
            CfaCliGetIfName ((UINT4) i4Count, piIfName);
            /* This function will format the display of the port list */
            AclCliPrintPortList (CliHandle, i4CommaCount, (UINT1 *) piIfName);
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        }
        i4Count++;
    }
    while (i4Count <= SYS_DEF_MAX_PHYSICAL_INTERFACES);
    /* Print Redirect Port List */
    if (i4Action == ISS_REDIRECT_TO)
    {
        nmhGetIssAclUserDefinedFilterRedirectId (i4NextFilter,
                                                 (INT4 *)
                                                 &u4NewRedirectGrpIndex);
    }
    /*If no port list has been configured */
    if (i4CommaCount == 0)
    {
        CliPrintf (CliHandle, "NIL");
    }
    MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
    CliPrintf (CliHandle, "\r\n");
    if (i4FilterOneType == ISS_L2_FILTER)
    {
        CliPrintf (CliHandle, "%-33s : L2 Filter\r\n", " Filter One Type");
    }
    else if (i4FilterOneType == ISS_L3_FILTER)
    {
        CliPrintf (CliHandle, "%-33s : L3 Filter\r\n", " Filter One Type");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : None\r\n", " Filter One Type");
    }
    CliPrintf (CliHandle, "%-33s : %d\r\n", " Filter Id", i4FilterOneId);
    if (i4FilterTwoType == ISS_L2_FILTER)
    {
        CliPrintf (CliHandle, "%-33s : L2 Filter\r\n", " Filter Two Type");
    }
    else if (i4FilterTwoType == ISS_L3_FILTER)
    {
        CliPrintf (CliHandle, "%-33s : L3 Filter\r\n", " Filter Two Type");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : None\r\n", " Filter Two Type");
    }
    CliPrintf (CliHandle, "%-33s : %d\r\n", " Filter Id", i4FilterTwoId);
    i4CommaCount = 0;
    CliPrintf (CliHandle, "%-33s : ", " Redirect Port List");
    if (u4NewRedirectGrpIndex != 0)
    {
        pPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));
        if (pPorts == NULL)
        {
            ISS_TRC (ALL_FAILURE_TRC, "\nCLI:pPorts is NULL \n");
            return CLI_FAILURE;
        }
        MEMSET (*pPorts, 0, sizeof (tPortList));
        RedirectsPorts.pu1_OctetList = *pPorts;
        RedirectsPorts.i4_Length = sizeof (tPortList);
        nmhGetIssRedirectInterfaceGrpType (u4NewRedirectGrpIndex,
                                           &i4RedirectInterfaceGrpType);
        nmhGetIssRedirectInterfaceGrpPortList (u4NewRedirectGrpIndex,
                                               &RedirectsPorts);
        nmhGetIssRedirectInterfaceGrpDistByte (u4NewRedirectGrpIndex,
                                               &i4TrafficDistByte);
        nmhGetIssRedirectInterfaceGrpUdbPosition (u4NewRedirectGrpIndex,
                                                  &i4RedirectUdbPosition);
        i4Count = 0;
        i4CommaCount = 0;
        while ((i4Count < ISS_REDIRECT_MAX_PORTS) &&
               (i4RedirectInterfaceGrpType == ISS_REDIRECT_TO_PORTLIST))
        {
            if (CliIsMemberPort (RedirectsPorts.pu1_OctetList,
                                 ISS_REDIRECT_PORT_LIST_SIZE,
                                 i4Count) == CLI_SUCCESS)
            {
                i4CommaCount++;
                CfaCliGetIfName ((UINT4) i4Count, piIfName);
                AclCliPrintPortList (CliHandle, i4CommaCount,
                                     (UINT1 *) piIfName);
                MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
            }
            i4Count++;
        }
        if (i4RedirectInterfaceGrpType != ISS_REDIRECT_TO_PORTLIST)
        {
            MEMCPY (&u4InterfaceIndex, RedirectsPorts.pu1_OctetList,
                    sizeof (UINT4));
            i4CommaCount++;
            CfaCliGetIfName (u4InterfaceIndex, piIfName);
            AclCliPrintPortList (CliHandle, i4CommaCount, (UINT1 *) piIfName);
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        }
        FsUtilReleaseBitList ((UINT1 *) pPorts);
    }
/*If no port list has been configured */
    if (i4CommaCount == 0)
    {
        CliPrintf (CliHandle, "NIL");
    }
    CliPrintf (CliHandle, "\r\n");
    switch (i4TrafficDistByte)
    {
        case ISS_REDIRECT_UDB_FIELD:
            CliPrintf (CliHandle, "%-33s : UDB:%d", " TrafficDistField",
                       i4RedirectUdbPosition);
            break;
        case ISS_TRAFF_DIST_BYTE_SRCIP:
            CliPrintf (CliHandle, "%-33s : SRC IP", " TrafficDistField");
            break;
        case ISS_TRAFF_DIST_BYTE_DSTIP:
            CliPrintf (CliHandle, "%-33s : DST IP", " TrafficDistField");
            break;
        case ISS_TRAFF_DIST_BYTE_SMAC:
            CliPrintf (CliHandle, "%-33s : SRC MAC", " TrafficDistField");
            break;
        case ISS_TRAFF_DIST_BYTE_DMAC:
            CliPrintf (CliHandle, "%-33s : DST MAC", " TrafficDistField");
            break;
        case ISS_TRAFF_DIST_BYTE_STCPPORT:
            CliPrintf (CliHandle, "%-33s : SRC TCP PORT", " TrafficDistField");
            break;
        case ISS_TRAFF_DIST_BYTE_DTCPPORT:
            CliPrintf (CliHandle, "%-33s : DST TCP PORT", " TrafficDistField");
            break;
        case ISS_TRAFF_DIST_BYTE_SUDPPORT:
            CliPrintf (CliHandle, "%-33s : SRC UDP PORT", " TrafficDistField");
            break;
        case ISS_TRAFF_DIST_BYTE_DUDPPORT:
            CliPrintf (CliHandle, "%-33s : DST UDP PORT", " TrafficDistField");
            break;
        case ISS_TRAFF_DIST_BYTE_VLANID:
            CliPrintf (CliHandle, "%-33s : VLAN ID", " TrafficDistField");
            break;
        case ISS_TRAFF_DIST_BYTE_ETHERTYPE:
            CliPrintf (CliHandle, "%-33s : ETHER-TYPE", " TrafficDistField");
            break;
        case ISS_TRAFF_DIST_BYTE_IPPROTOCOL:
            CliPrintf (CliHandle, "%-33s : INNER IP", " TrafficDistField");
            break;
        case ISS_TRAFF_DIST_BYTE_INNER_SRCIP:
            CliPrintf (CliHandle, "%-33s : INNER SRCIP", " TrafficDistField");
            break;
        case ISS_TRAFF_DIST_BYTE_INNER_DESTIP:
            CliPrintf (CliHandle, "%-33s : INNER DSTIP", " TrafficDistField");
            break;
        default:
            CliPrintf (CliHandle, "%-33s : Unknown", " TrafficDistField");
    }
    CliPrintf (CliHandle, "\r\n");
    if (i4SubAction == ISS_NONE)
    {
        CliPrintf (CliHandle, "%-33s : NONE", " Sub Action");
    }
    else if (i4SubAction == ISS_MODIFY_VLAN)
    {
        CliPrintf (CliHandle, "%-33s : MODIFY VLAN", " Sub Action");
    }
    else if (i4SubAction == ISS_NESTED_VLAN)
    {
        CliPrintf (CliHandle, "%-33s : NESTED VLAN", " Sub Action");
    }
    else if (i4SubAction == ISS_STRIP_OUTER_HDR)
    {
        CliPrintf (CliHandle, "%-33s : STRIP OUTER HEADER", " Sub Action");
    }
    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, "%-33s : %d\r\n", " Sub Action Id", i4SubActionId);
    if (i4RowStatus == ISS_ACTIVE)
    {
        CliPrintf (CliHandle, "%-33s : Active", " Status");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : InActive", " Status");
    }
    CliPrintf (CliHandle, "\r\n");

    /* Filter Statistics */
    if ((nmhGetIssAclUserDefinedFilterStatsEnabledStatus
         (i4NextFilter, &i4Stats)) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Retrieve UDB Filter Statistics Status %d for Filter ID %d\n",
                      i4Stats, i4NextFilter);
    }
    if ((nmhGetIssAclUserDefinedFilterMatchCount (i4NextFilter, &u4FilterHits))
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Retrieve UDB Filter Statistics %u for Filter ID %d\n",
                      u4FilterHits, i4NextFilter);
    }
    if (i4Stats == ACL_STAT_ENABLE)
    {
        CliPrintf (CliHandle, "%-33s : Enabled\r\n", " Counter Status");
        CliPrintf (CliHandle, "%-33s : %u", " Match Count", u4FilterHits);
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : Disabled\r\n", " Counter Status");
        CliPrintf (CliHandle, "%-33s : %u", " Match Count", u4FilterHits);
    }
    CliPrintf (CliHandle, "\r\n\r\n");
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclShowUDBFilter function \n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclShowRunningConfig                               */
/*                                                                           */
/*     DESCRIPTION      : This function  displays the  RSTP Configuration    */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
AclShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{

    INT4                i4Index = -1;

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclShowRunningConfig function \n");
    AclShowRunningConfigTables (CliHandle);

    if (u4Module == ISS_ACL_SHOW_RUNNING_CONFIG)
    {
        AclShowRunningConfigInterfaceDetails (CliHandle, i4Index);

    }
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclShowRunningConfig function \n");
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*     FUNCTION NAME    : AclShowRunningConfigTables                         */
/*                                                                           */
/*     DESCRIPTION      : This function displays table  objects in ACL  for  */
/*                        show running configuration.                        */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
AclShowRunningConfigTables (tCliHandle CliHandle)
{

    tMacAddr            DstMacAddr;
    tMacAddr            SrcMacAddr;
    tMacAddr            zeroAddr;
    tMacAddr            ResrvFrmMacAddr;
    INT4                i4NextFilter = 0;
    INT4                i4PrevFilter = 0;
    INT4                i4FltrAction = 0;
    INT4                i4Tos = ISS_TOS_INVALID;
    INT4                i4Dscp = ISS_DSCP_INVALID;
    INT4                i4MsgType = ISS_DEFAULT_MSG_TYPE;
    INT4                i4MsgCode = ISS_DEFAULT_MSG_CODE;
    INT4                i4FltrAckBit;
    INT4                i4FltrRstBit;
    INT4                i4EtherType = 0;
    INT4                i4FltrPrio = 0;
    INT4                i4Protocol = 0;
    INT4                i4SVlan = ACL_DEF_SVLAN;
    INT4                i4SVlanPrio = ACL_DEF_SVLAN_PRIO;
    INT4                i4VlanId = ACL_DEF_CVLAN;
    INT4                i4CVlanPrio = ACL_DEF_CVLAN_PRIO;
    INT4                i4TagType = ISS_FILTER_SINGLE_TAG;
    INT4                i4AddrType = QOS_IPV4;
    INT4                i4RedirectInterfaceGrpType = 0;
    INT4                i4PktType = 0;
    INT4                i4Base = 0;
    INT4                i4Priority = 0;
    INT1                i1OutCome;
    UINT4               u4SrcIpAddr = 0;
    UINT4               u4SrcIpMask;
    UINT4               u4DstIpAddr = 0;
    UINT4               u4DstIpMask;
    UINT4               u4MinSrcProtPort = 0;
    UINT4               u4MaxSrcProtPort = 0;
    UINT4               u4MinDstProtPort = 0;
    UINT4               u4MaxDstProtPort = 0;
    UINT4               u4SrcIpPrefixLength = 0;
    UINT4               u4DstIpPrefixLength = 0;
    UINT4               u4FlowId = ISS_FLOWID_INVALID;
    UINT4               u4RedirectGrpId = 0;
    UINT4               u4NextFilter = 0;
    UINT4               u4PagingStatus = 0;
    UINT4               u4PrevFilter = 0;
    UINT4               u4Switching = 0;
    INT4                i4OuterEType;
    CHR1               *pu1String;
    UINT1               au1SrcAddr[ISS_ADDR_LEN];
    UINT1               au1SrcMask[ISS_ADDR_LEN];
    UINT1               au1DstAddr[ISS_ADDR_LEN];
    UINT1               au1DstMask[ISS_ADDR_LEN];
    UINT4               u4Quit = CLI_SUCCESS;
    CHR1                au1String[ISS_ADDR_LEN];
    UINT1               u1StdAclFlag = 0;
    UINT1               au1SrcTemp[IP6_ADDR_SIZE];
    UINT1               au1DstTemp[IP6_ADDR_SIZE];
    tIp6Addr            SrcIp6Addr;
    tIp6Addr            DstIp6Addr;
    tSNMP_OCTET_STRING_TYPE SrcIpAddrOctet;
    tSNMP_OCTET_STRING_TYPE DstIpAddrOctet;
    tSNMP_OCTET_STRING_TYPE RedirectsPorts;
    tSNMP_OCTET_STRING_TYPE SetValue;
    tSNMP_OCTET_STRING_TYPE SetMask;
    tSNMP_OCTET_STRING_TYPE InPortList;
    INT1               *piIfName;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               au1SetValue[ISS_UDB_MAX_OFFSET];
    UINT1               au1SetMask[ISS_UDB_MAX_OFFSET];
    UINT1               au1InPortList[ISS_PORTLIST_LEN];
    UINT1               au1ResMacStr[MAC_ADDR_LEN];
    piIfName = (INT1 *) &au1IfName[0];
    UINT4               u4InterfaceIndex = 0;
    tPortList          *pPorts = NULL;
    INT4                i4PbFilterFlag = 0;
    INT4                i4DefaultFlag = 0;
    INT4                i4FilterOneType = 0;
    INT4                i4FilterOneId = 0;
    INT4                i4FilterTwoType = 0;
    INT4                i4FilterTwoId = 0;
    INT4                i4TotOffsets = 0;
    INT4                i4Length = 0;
    INT4                i4TrafficDistByte = 0;
    INT4                i4SubActionId = 0;
    INT4                i4SubAction = 0;
    INT4                i4RedirectUdbPosition = 0;
    INT4                i4ReservedFrameCtrlPktType = 0;
    INT4                i4ReservedFrameCtrlAction = 0;
    INT4                i4MacMask = 0;
    INT4                i4ReservedFrameCtrlStatus = 0;
    UINT4               u4NextReservedFrameCtrlId = 0;
    UINT4               u4CurrReservedFrameCtrlId = 0;
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pu1String = &au1String[0];
    SrcIpAddrOctet.pu1_OctetList = au1SrcTemp;
    DstIpAddrOctet.pu1_OctetList = au1DstTemp;

    SetValue.i4_Length = ISS_PORTLIST_LEN;
    SetMask.i4_Length = ISS_PORTLIST_LEN;
    SetValue.pu1_OctetList = &au1SetValue[0];
    SetMask.pu1_OctetList = &au1SetMask[0];
    InPortList.pu1_OctetList = &au1InPortList[0];

    ISS_TRC (INIT_SHUT_TRC,
             "\nCLI:Entry AclShowRunningConfigTables function \n");

    CLI_MEMSET (&SrcIp6Addr, 0, sizeof (tIp6Addr));
    CLI_MEMSET (&DstIp6Addr, 0, sizeof (tIp6Addr));
    CLI_MEMSET (au1String, 0, ISS_ADDR_LEN);
    CLI_MEMSET (zeroAddr, 0, MAC_ADDR_LEN);
    CLI_MEMSET (&RedirectsPorts, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    CLI_MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

    CliRegisterLock (CliHandle, IssLock, IssUnLock);
    ISS_LOCK ();

    /*Ip Access lists */

    i1OutCome = nmhGetFirstIndexIssExtL3FilterTable (&i4NextFilter);

    CliPrintf (CliHandle, "!\r\n");

    while (i1OutCome != SNMP_FAILURE)
    {

        u1StdAclFlag = 0;
        /* L3 ACL show running display only configured through ACL commands */
        pIssL3FilterEntry = IssExtGetL3FilterEntry (i4NextFilter);
        if ((pIssL3FilterEntry != NULL) &&
            (pIssL3FilterEntry->b1IsL3AclConfFromExt == TRUE))
        {
            /* ACL configured through other application, so skip this */
            i4PrevFilter = i4NextFilter;

            i1OutCome =
                nmhGetNextIndexIssExtL3FilterTable (i4PrevFilter,
                                                    &i4NextFilter);

            continue;
        }

        if (i4NextFilter < 1001)
        {
            CliPrintf (CliHandle, "ip access-list standard %d\r\n",
                       i4NextFilter);
            u1StdAclFlag = 1;
        }
        else
        {
            CliPrintf (CliHandle, "\nip  access-list extended %d\r\n",
                       i4NextFilter);
        }

        if (!u1StdAclFlag)
        {
            nmhGetIssExtL3FilterProtocol (i4NextFilter, &i4Protocol);
            nmhGetIssAclL3FilteAddrType (i4NextFilter, &i4AddrType);

            AclPbGetL3Filter (i4NextFilter, &i4SVlan, &i4SVlanPrio,
                              &i4VlanId, &i4CVlanPrio, &i4TagType);
            if ((i4SVlan != ACL_DEF_SVLAN)
                || (i4SVlanPrio != ACL_DEF_SVLAN_PRIO)
                || (i4VlanId != ACL_DEF_CVLAN)
                || (i4CVlanPrio != ACL_DEF_CVLAN_PRIO)
                || (i4TagType != ISS_FILTER_SINGLE_TAG))
            {
                i4PbFilterFlag = 1;
            }
        }

        if (i4Protocol == ISS_PROT_ICMP)
        {
            nmhGetIssExtL3FilterMessageType (i4NextFilter, &i4MsgType);
            nmhGetIssExtL3FilterMessageCode (i4NextFilter, &i4MsgCode);
        }

        /* Source and destination protocol port ranges for TCP/UDP */
        if ((i4Protocol == ISS_PROT_TCP) || (i4Protocol == ISS_PROT_UDP))
        {
            nmhGetIssExtL3FilterMinSrcProtPort (i4NextFilter,
                                                &u4MinSrcProtPort);
            nmhGetIssExtL3FilterMaxSrcProtPort (i4NextFilter,
                                                &u4MaxSrcProtPort);
            nmhGetIssExtL3FilterMinDstProtPort (i4NextFilter,
                                                &u4MinDstProtPort);
            nmhGetIssExtL3FilterMaxDstProtPort (i4NextFilter,
                                                &u4MaxDstProtPort);
        }

        nmhGetIssExtL3FilterPriority (i4NextFilter, &i4FltrPrio);
        nmhGetIssAclL3FilterAction (i4NextFilter, &i4FltrAction);
        nmhGetIssAclL3FilterSrcIpAddr (i4NextFilter, &SrcIpAddrOctet);
        nmhGetIssAclL3FilterDstIpAddr (i4NextFilter, &DstIpAddrOctet);
        nmhGetIssAclL3FilterSrcIpAddrPrefixLength (i4NextFilter,
                                                   &u4SrcIpPrefixLength);
        nmhGetIssAclL3FilterDstIpAddrPrefixLength (i4NextFilter,
                                                   &u4DstIpPrefixLength);
        nmhGetIssAclL3FilterFlowId (i4NextFilter, &u4FlowId);

        if (i4AddrType == QOS_IPV4)
        {
            PTR_FETCH4 (u4SrcIpAddr, SrcIpAddrOctet.pu1_OctetList);
            IssUtlConvertPrefixToMask (u4SrcIpPrefixLength, &u4SrcIpMask);
            PTR_FETCH4 (u4DstIpAddr, DstIpAddrOctet.pu1_OctetList);
            IssUtlConvertPrefixToMask (u4DstIpPrefixLength, &u4DstIpMask);
        }
        if (i4AddrType == QOS_IPV6)
        {
            MEMCPY (&SrcIp6Addr, SrcIpAddrOctet.pu1_OctetList, IP6_ADDR_SIZE);
            MEMCPY (&DstIp6Addr, DstIpAddrOctet.pu1_OctetList, IP6_ADDR_SIZE);
        }

        if (!u1StdAclFlag)
        {
            if (i4NextFilter > ACL_STANDARD_MAX_VALUE)
            {
                nmhGetIssExtL3FilterTos (i4NextFilter, &i4Tos);

                nmhGetIssExtL3FilterDscp (i4NextFilter, &i4Dscp);
            }
        }

        if (i4Protocol == ISS_PROT_TCP)
        {
            nmhGetIssExtL3FilterAckBit (i4NextFilter, &i4FltrAckBit);
            nmhGetIssExtL3FilterRstBit (i4NextFilter, &i4FltrRstBit);
        }
        /*To Mask default rule "permit any any" getting displayed in Shor running-config IP acl */
        if ((i4FltrAction == ISS_ALLOW) && (i4AddrType == QOS_IPV4) && (i4FltrPrio <= 1))    /*Common grouping of Std and Ext ACL */
        {
            if ((u4SrcIpAddr == ISS_ZERO_ENTRY)
                && (u4DstIpAddr == ISS_ZERO_ENTRY))
            {
                if ((!u1StdAclFlag) && (i4Protocol == ISS_IP_PROTO_DEF))    /*For Extended ACL */
                {
                    if ((i4Tos == ISS_TOS_INVALID)
                        && (i4Dscp == ISS_DSCP_INVALID))
                    {
                        if (i4PbFilterFlag == 0)    /*In case of Pb L3 Filter configured */
                        {
                            i4PrevFilter = i4NextFilter;
                            i1OutCome =
                                nmhGetNextIndexIssExtL3FilterTable
                                (i4PrevFilter, &i4NextFilter);
                            CliPrintf (CliHandle, "!\r\n");
                            continue;
                        }
                    }
                }
                else if (u1StdAclFlag == 1)    /*for Standard ACL */
                {
                    i4PrevFilter = i4NextFilter;
                    i1OutCome =
                        nmhGetNextIndexIssExtL3FilterTable (i4PrevFilter,
                                                            &i4NextFilter);
                    CliPrintf (CliHandle, "!\r\n");
                    continue;
                }
            }
        }
        if (i4FltrAction == ISS_ALLOW || i4FltrAction == ISS_REDIRECT_TO)
        {
            CliPrintf (CliHandle, "permit ");
        }
        else if (i4FltrAction == ISS_DROP)
        {
            CliPrintf (CliHandle, "deny ");
        }
        else if ((i4FltrAction == ISS_DROP_COPYTOCPU) ||
                 (i4FltrAction == ISS_SWITCH_COPYTOCPU))
        {
            CliPrintf (CliHandle, "copy-to-cpu ");
        }

        if (!u1StdAclFlag)
        {
            if (i4Protocol == ISS_PROT_OSPFIGP)
            {
                CliPrintf (CliHandle, "ospf ");

            }
            else if (i4Protocol == ISS_PROT_ICMP6)
            {
                if (i4Tos == ISS_TOS_INVALID)
                {
                    CliPrintf (CliHandle, "icmpv6 ");
                }
                else
                {
                    CliPrintf (CliHandle, "%d ", i4Protocol);
                }
            }
            else if (i4Protocol == ISS_PROT_ICMP)
            {
                if ((i4Tos == ISS_TOS_INVALID) && (i4Dscp == ISS_DSCP_INVALID))
                {
                    CliPrintf (CliHandle, " icmp ");
                }
                else
                {
                    CliPrintf (CliHandle, "%d ", i4Protocol);
                }
            }
            else if (i4Protocol == ISS_PROT_PIM)
            {
                CliPrintf (CliHandle, "pim ");
            }
            else if (i4Protocol == ISS_PROT_TCP)
            {
                CliPrintf (CliHandle, "tcp ");
            }
            else if (i4Protocol == ISS_PROT_UDP)
            {
                CliPrintf (CliHandle, "udp ");
            }
            else if (i4AddrType == QOS_IPV6)
            {
                CliPrintf (CliHandle, "ipv6 ");
            }
            else if (i4Protocol == ISS_IP_PROTO_DEF)
            {
                CliPrintf (CliHandle, "ip ");
            }
            else
            {
                CliPrintf (CliHandle, "%d ", i4Protocol);
            }

        }

        if (i4AddrType == QOS_IPV4)
        {
            /*Source Ip Address */
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SrcIpAddr);
            CLI_MEMCPY (au1SrcAddr, pu1String, ISS_ADDR_LEN);

            CLI_MEMSET (au1String, 0, ISS_ADDR_LEN);
            /*Source Ip Mask */
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SrcIpMask);
            CLI_MEMCPY (au1SrcMask, pu1String, ISS_ADDR_LEN);

            CLI_MEMSET (au1String, 0, ISS_ADDR_LEN);
            /*filter destination IP address */
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4DstIpAddr);
            CLI_MEMCPY (au1DstAddr, pu1String, ISS_ADDR_LEN);

            CLI_MEMSET (au1String, 0, ISS_ADDR_LEN);

            /*filter destination IP address */
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4DstIpMask);
            CLI_MEMCPY (au1DstMask, pu1String, ISS_ADDR_LEN);

            if (u4SrcIpAddr == 0)
            {
                CliPrintf (CliHandle, "any ");
            }

            else if ((u4SrcIpAddr != 0) && (CLI_STRCMP (au1SrcMask,
                                                        "255.255.255.255")
                                            == 0))
            {
                CliPrintf (CliHandle, "host %s ", au1SrcAddr);
            }
            else if ((u4SrcIpAddr != 0) && (CLI_STRCMP (au1SrcMask,
                                                        "255.255.255.255")
                                            != 0))
            {
                CliPrintf (CliHandle, "%s %s ", au1SrcAddr, au1SrcMask);
            }
        }
        if ((i4AddrType == QOS_IPV6)
            && ((u4FlowId == ISS_FLOWID_INVALID) || (0 == u4FlowId)))
        {
            if ((CLI_IPV6_EQUAL_ZERO (SrcIp6Addr))
                && (CLI_IPV6_EQUAL_ZERO (DstIp6Addr)))
            {
                CliPrintf (CliHandle, "any any ");
            }
            else if ((!CLI_IPV6_EQUAL_ZERO (SrcIp6Addr))
                     && (CLI_IPV6_EQUAL_ZERO (DstIp6Addr)))
            {
                CliPrintf (CliHandle, "host %s %d any ",
                           Ip6PrintNtop ((tIp6Addr *) & (SrcIp6Addr)),
                           u4SrcIpPrefixLength);
            }
            else if ((!CLI_IPV6_EQUAL_ZERO (DstIp6Addr))
                     && (CLI_IPV6_EQUAL_ZERO (SrcIp6Addr)))
            {
                CliPrintf (CliHandle, "any host %s %d ",
                           Ip6PrintNtop ((tIp6Addr *) & (DstIp6Addr)),
                           u4DstIpPrefixLength);
            }
            else if ((!CLI_IPV6_EQUAL_ZERO (SrcIp6Addr))
                     && (!CLI_IPV6_EQUAL_ZERO (DstIp6Addr)))
            {
                CliPrintf (CliHandle, "host %s %d host %s %d ",
                           Ip6PrintNtop ((tIp6Addr *) & (SrcIp6Addr)),
                           u4SrcIpPrefixLength,
                           Ip6PrintNtop ((tIp6Addr *) & (DstIp6Addr)),
                           u4DstIpPrefixLength);
            }
        }
        if ((!u1StdAclFlag) && ((i4Protocol == ISS_PROT_TCP) ||
                                (i4Protocol == ISS_PROT_UDP)))
        {
            if ((u4MinSrcProtPort != 0) && (u4MaxSrcProtPort != 0))
            {
                if (u4MinSrcProtPort == u4MaxSrcProtPort)
                {
                    CliPrintf (CliHandle, "eq %d ", u4MinSrcProtPort);
                }
                else if (u4MinSrcProtPort == 1 && u4MaxSrcProtPort != 0xffff)
                {
                    CliPrintf (CliHandle, "lt %d ", u4MaxSrcProtPort + 1);
                }
                else if (u4MaxSrcProtPort == 0xffff && u4MinSrcProtPort != 1)
                {
                    CliPrintf (CliHandle, "gt %d ", u4MinSrcProtPort - 1);
                }
                else
                {
                    CliPrintf (CliHandle, "range %d %d ",
                               u4MinSrcProtPort, u4MaxSrcProtPort);
                }
            }
        }
        if (i4AddrType == QOS_IPV4)
        {
            if (u4DstIpAddr == 0)
            {
                CliPrintf (CliHandle, "any ");
            }
            else if ((u4DstIpAddr != 0) && (CLI_STRCMP (au1DstMask,
                                                        "255.255.255.255")
                                            == 0))
            {
                CliPrintf (CliHandle, "host %s ", au1DstAddr);
            }
            else if ((u4DstIpAddr != 0) && (CLI_STRCMP (au1DstMask,
                                                        "255.255.255.255")
                                            != 0))
            {
                CliPrintf (CliHandle, "%s %s ", au1DstAddr, au1DstMask);
            }
        }

        if ((!u1StdAclFlag) && ((i4Protocol == ISS_PROT_TCP) ||
                                (i4Protocol == ISS_PROT_UDP)))
        {
            if ((u4MinDstProtPort != 0) && (u4MaxDstProtPort != 0))
            {
                if (u4MinDstProtPort == u4MaxDstProtPort)
                {
                    CliPrintf (CliHandle, "eq %d ", u4MinDstProtPort);
                }
                else if (u4MinDstProtPort == 1 && u4MaxDstProtPort != 0xffff)
                {
                    CliPrintf (CliHandle, "lt %d ", u4MaxDstProtPort + 1);
                }
                else if (u4MaxDstProtPort == 0xffff && u4MinDstProtPort != 1)
                {
                    CliPrintf (CliHandle, "gt %d ", u4MinDstProtPort - 1);
                }
                else
                {
                    CliPrintf (CliHandle, "range %d %d ",
                               u4MinDstProtPort, u4MaxDstProtPort);
                }
            }
        }

        if (i4Protocol == ISS_PROT_TCP)
        {
            /* ACK bit */
            if (i4FltrAckBit == ISS_ACK_ESTABLISH)
            {
                CliPrintf (CliHandle, "ack ");
            }

            /* RST bit */
            if (i4FltrRstBit == ISS_RST_SET)
            {
                CliPrintf (CliHandle, "rst ");
            }

        }

        if (i4Protocol == ISS_PROT_ICMP || i4Protocol == ISS_PROT_ICMP6)
        {
            if (i4MsgType != ISS_DEFAULT_MSG_TYPE)
            {
                CliPrintf (CliHandle, "message-type %d ", i4MsgType);
            }
            if (i4MsgCode != ISS_DEFAULT_MSG_CODE)
            {
                CliPrintf (CliHandle, "message-code %d ", i4MsgCode);
            }

        }

        if (!u1StdAclFlag)
        {
            if (i4Protocol != ISS_PROT_ICMP)
            {
                if (i4Tos != 8)
                {
                    if (i4Tos == ISS_TOS_HI_REL)
                    {
                        CliPrintf (CliHandle, "tos max-reliability ");
                    }
                    else if (i4Tos == ISS_TOS_HI_THR)
                    {

                        CliPrintf (CliHandle, "tos max-throughput ");
                    }
                    else if (i4Tos == ISS_TOS_LO_DEL)
                    {

                        CliPrintf (CliHandle, "tos min-delay ");
                    }
                    else if (i4Tos == ISS_TOS_NONE)
                    {

                        CliPrintf (CliHandle, "tos normal ");
                    }
                    else
                    {
                        CliPrintf (CliHandle, "tos %d ", i4Tos);
                    }
                }

                if (i4Dscp != ISS_DSCP_INVALID)
                {
                    CliPrintf (CliHandle, "dscp %d ", i4Dscp);

                }
            }

            if ((i4AddrType == QOS_IPV6) && (u4FlowId != ISS_FLOWID_INVALID))
            {
                if (0 != u4FlowId)
                {
                    CliPrintf (CliHandle, "flow-label %d ", u4FlowId);
                }
            }
            if (i4FltrPrio != 0)
            {
                CliPrintf (CliHandle, "priority %d ", i4FltrPrio);

            }

            if (i4SVlan != ACL_DEF_SVLAN)
            {
                CliPrintf (CliHandle, "svlan-id %d ", i4SVlan);
            }
            if (i4SVlanPrio != ACL_DEF_SVLAN_PRIO)
            {
                CliPrintf (CliHandle, "svlan-priority %d ", i4SVlanPrio);
            }
            if (i4VlanId != ACL_DEF_CVLAN)
            {
                CliPrintf (CliHandle, "cvlan-id %d ", i4VlanId);
            }
            if (i4CVlanPrio != ACL_DEF_CVLAN_PRIO)
            {
                CliPrintf (CliHandle, "cvlan-priority %d ", i4CVlanPrio);
            }
            if (i4TagType != ISS_FILTER_SINGLE_TAG)
            {
                CliPrintf (CliHandle, "double-tag ");
            }
        }
        if (i4FltrAction == ISS_REDIRECT_TO)
        {
            CliPrintf (CliHandle, "redirect interface ");
            nmhGetIssAclL3FilterRedirectId (i4NextFilter,
                                            (INT4 *) &u4RedirectGrpId);

            if (u4RedirectGrpId != 0)
            {

                pPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

                if (pPorts == NULL)
                {
                    ISS_TRC (ALL_FAILURE_TRC, "\nCLI:pPorts is NULL \n");
                    return;
                }

                MEMSET (pPorts, 0, sizeof (tPortList));

                RedirectsPorts.pu1_OctetList = (UINT1 *) pPorts;
                RedirectsPorts.i4_Length = sizeof (tPortList);

                nmhGetIssRedirectInterfaceGrpType
                    (u4RedirectGrpId, &i4RedirectInterfaceGrpType);

                nmhGetIssRedirectInterfaceGrpPortList
                    (u4RedirectGrpId, &RedirectsPorts);

                MEMCPY (&u4InterfaceIndex, RedirectsPorts.pu1_OctetList,
                        sizeof (UINT4));
                CfaCliConfGetIfName (u4InterfaceIndex, piIfName);
                CliPrintf (CliHandle, "%s ", piIfName);
                MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

                FsUtilReleaseBitList ((UINT1 *) pPorts);
            }
        }
        if (i4FltrAction == ISS_DROP_COPYTOCPU)
        {
            CliPrintf (CliHandle, "noswitching ");
        }
        if (u1StdAclFlag)
        {
            if (i4FltrPrio != 0)
            {
                CliPrintf (CliHandle, "priority %d ", i4FltrPrio);

            }
        }
        u4Quit = CliPrintf (CliHandle, "\n!\r\n");
        if (u4Quit == CLI_FAILURE)
        {
            ISS_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return;
        }

        i4PrevFilter = i4NextFilter;

        i1OutCome =
            nmhGetNextIndexIssExtL3FilterTable (i4PrevFilter, &i4NextFilter);

    }

    /*MacAccessLists */
    u4RedirectGrpId = 0;
    i4RedirectInterfaceGrpType = 0;
    i4PbFilterFlag = 0;
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (&RedirectsPorts, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    CliPrintf (CliHandle, "\r\n");

    i1OutCome = nmhGetFirstIndexIssExtL2FilterTable (&i4NextFilter);

    while (i1OutCome != SNMP_FAILURE)
    {
        i4DefaultFlag = 0;
        /* L2 ACL show running display only configured through ACL commands */
        pIssL2FilterEntry = IssExtGetL2FilterEntry (i4NextFilter);
        if ((pIssL2FilterEntry != NULL) &&
            (pIssL2FilterEntry->b1IsL2AclConfFromExt == TRUE))
        {
            /* ACL configured through other application, so skip this */
            i4PrevFilter = i4NextFilter;

            i1OutCome =
                nmhGetNextIndexIssExtL2FilterTable (i4PrevFilter,
                                                    &i4NextFilter);

            continue;
        }

        CliPrintf (CliHandle, "mac access-list extended %d\r\n", i4NextFilter);

        nmhGetIssExtL2FilterPriority (i4NextFilter, &i4FltrPrio);
        if (i4FltrPrio == 1)
        {
            i4DefaultFlag++;
        }
        nmhGetIssExtL2FilterEtherType (i4NextFilter, &i4EtherType);
        if (i4EtherType == 0)
        {
            i4DefaultFlag++;
        }
        nmhGetIssExtL2FilterProtocolType (i4NextFilter, (UINT4 *) &i4Protocol);
        if (i4Protocol == 0)
        {
            i4DefaultFlag++;
        }
        nmhGetIssExtL2FilterVlanId (i4NextFilter, &i4VlanId);
        if (i4VlanId == 0)
        {
            i4DefaultFlag++;
        }
        nmhGetIssExtL2FilterDstMacAddr (i4NextFilter,
                                        (tMacAddr *) & DstMacAddr);
        nmhGetIssExtL2FilterSrcMacAddr (i4NextFilter,
                                        (tMacAddr *) & SrcMacAddr);
        AclPbGetL2Filter (i4NextFilter, &i4OuterEType, &i4SVlan,
                          &i4SVlanPrio, &i4CVlanPrio, &i4TagType);
        if ((i4OuterEType != ACL_DEF_ETYPE) || (i4SVlan != ACL_DEF_SVLAN)
            || (i4SVlanPrio != ACL_DEF_SVLAN_PRIO)
            || (i4CVlanPrio != ACL_DEF_CVLAN_PRIO)
            || (i4TagType != ISS_FILTER_SINGLE_TAG))
        {
            i4PbFilterFlag = 1;
        }
        nmhGetIssExtL2FilterAction (i4NextFilter, &i4FltrAction);
        /*To Mask Default rule "permit any any" getting displayed is show running-config of Mac ACL */
        if ((!CLI_MEMCMP (SrcMacAddr, zeroAddr, MAC_ADDR_LEN))
            && !CLI_MEMCMP (DstMacAddr, zeroAddr, MAC_ADDR_LEN)
            && (i4DefaultFlag == 4) && (i4FltrAction == ISS_ALLOW))
        {
            if (!i4PbFilterFlag)
            {
                i4PrevFilter = i4NextFilter;
                i1OutCome =
                    nmhGetNextIndexIssExtL2FilterTable (i4PrevFilter,
                                                        &i4NextFilter);
                CliPrintf (CliHandle, "!\r\n");
                continue;
            }
        }
        if (i4FltrAction == ISS_ALLOW || i4FltrAction == ISS_REDIRECT_TO)
        {
            CliPrintf (CliHandle, "permit ");
        }
        else if (i4FltrAction == ISS_DROP)
        {
            CliPrintf (CliHandle, "deny ");
        }

        else if ((i4FltrAction == ISS_DROP_COPYTOCPU) ||
                 (i4FltrAction == ISS_SWITCH_COPYTOCPU))
        {
            CliPrintf (CliHandle, "copy-to-cpu ");
        }

        CLI_MEMSET (au1String, 0, ISS_ADDR_LEN);

        if (CLI_MEMCMP (SrcMacAddr, zeroAddr, MAC_ADDR_LEN) != 0)
        {
            AclPrintMacAddress (SrcMacAddr, (UINT1 *) pu1String);
            CliPrintf (CliHandle, "host %s", pu1String);
        }

        if (CLI_MEMCMP (SrcMacAddr, zeroAddr, MAC_ADDR_LEN) == 0)
        {
            CliPrintf (CliHandle, "any ");
        }

        CLI_MEMSET (au1String, 0, ISS_ADDR_LEN);

        if (CLI_MEMCMP (DstMacAddr, zeroAddr, MAC_ADDR_LEN) != 0)
        {
            AclPrintMacAddress (DstMacAddr, (UINT1 *) pu1String);
            CliPrintf (CliHandle, "host %s", pu1String);
        }

        if (CLI_MEMCMP (DstMacAddr, zeroAddr, MAC_ADDR_LEN) == 0)
        {
            CliPrintf (CliHandle, "any ");
        }

        switch (i4Protocol)
        {
            case AARP:
                CliPrintf (CliHandle, "aarp ");
                break;

            case AMBER:
                CliPrintf (CliHandle, "amber ");
                break;

            case DEC_SPANNING:
                CliPrintf (CliHandle, "dec-spanning ");
                break;

            case DIAGNOSTIC:
                CliPrintf (CliHandle, "diagnostic ");
                break;

            case DSM:
                CliPrintf (CliHandle, "dsm ");
                break;

            case ETYPE_6000:
                CliPrintf (CliHandle, "etype-6000 ");
                break;
            case ETYPE_8042:
                CliPrintf (CliHandle, "etype-8042 ");
                break;

            case LAT:
                CliPrintf (CliHandle, "lat ");
                break;

            case LAVC_SCA:
                CliPrintf (CliHandle, "lavc-sca ");
                break;

            case MOP_CONSOLE:
                CliPrintf (CliHandle, "mop-console ");
                break;

            case MSDOS:
                CliPrintf (CliHandle, "msdos ");
                break;

            case MUMPS:
                CliPrintf (CliHandle, "mumps ");
                break;

            case NET_BIOS:
                CliPrintf (CliHandle, "net-bios ");
                break;

            case VINES_ECHO:
                CliPrintf (CliHandle, "vines-echo ");
                break;

            case VINES_IP:
                CliPrintf (CliHandle, "vines-ip ");
                break;
            case XNS_ID:
                CliPrintf (CliHandle, "xns-id ");
                break;

            default:
                if (i4Protocol != 0)
                {
                    CliPrintf (CliHandle, "%d ", i4Protocol);
                }

                break;
        }

        if (i4EtherType != 0)
        {
            CliPrintf (CliHandle, "encaptype %d ", i4EtherType);
        }
        if (i4VlanId != 0)
        {
            CliPrintf (CliHandle, "vlan %d ", i4VlanId);
        }
        if (i4FltrPrio > 0)
        {
            CliPrintf (CliHandle, "priority %d ", i4FltrPrio);
        }
        if (i4OuterEType != ACL_DEF_ETYPE)
        {
            CliPrintf (CliHandle, "outerEtherType %d ", i4OuterEType);
        }
        if (i4SVlan != ACL_DEF_SVLAN)
        {
            CliPrintf (CliHandle, "svlan-id %d ", i4SVlan);
        }

        if (i4CVlanPrio != ACL_DEF_CVLAN_PRIO)
        {
            CliPrintf (CliHandle, "cvlan-priority %d ", i4CVlanPrio);
        }
        if (i4SVlanPrio != ACL_DEF_SVLAN_PRIO)
        {
            CliPrintf (CliHandle, "svlan-priority %d ", i4SVlanPrio);
        }

        if (i4TagType != ISS_FILTER_SINGLE_TAG)
        {
            CliPrintf (CliHandle, "double-tag ");
        }

        if (i4FltrAction == ISS_REDIRECT_TO)
        {
            CliPrintf (CliHandle, "redirect interface ");
            nmhGetIssAclL2FilterRedirectId (i4NextFilter,
                                            (INT4 *) &u4RedirectGrpId);

            if (u4RedirectGrpId != 0)
            {
                pPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

                if (pPorts == NULL)
                {
                    ISS_TRC (ALL_FAILURE_TRC, "\nCLI:pPorts is NULL \n");
                    return;
                }

                MEMSET (pPorts, 0, sizeof (tPortList));

                RedirectsPorts.pu1_OctetList = (UINT1 *) pPorts;
                RedirectsPorts.i4_Length = sizeof (tPortList);

                nmhGetIssRedirectInterfaceGrpType
                    (u4RedirectGrpId, &i4RedirectInterfaceGrpType);

                nmhGetIssRedirectInterfaceGrpPortList
                    (u4RedirectGrpId, &RedirectsPorts);

                MEMCPY (&u4InterfaceIndex, RedirectsPorts.pu1_OctetList,
                        sizeof (UINT4));
                CfaCliGetIfName (u4InterfaceIndex, piIfName);
                CliPrintf (CliHandle, "%s ", piIfName);
                MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

                FsUtilReleaseBitList ((UINT1 *) pPorts);
            }
        }
        u4Quit = CliPrintf (CliHandle, "\n!\r\n");

        if (u4Quit == CLI_FAILURE)
        {
            ISS_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return;
        }

        i4PrevFilter = i4NextFilter;

        i1OutCome =
            nmhGetNextIndexIssExtL2FilterTable (i4PrevFilter, &i4NextFilter);

    }
    /*UserDef Filter Start */
    CliPrintf (CliHandle, "\n!\r\n");
    i1OutCome = nmhGetFirstIndexIssAclUserDefinedFilterTable (&u4NextFilter);
    while (i1OutCome != SNMP_FAILURE)
    {

        u1StdAclFlag = 0;

        CliPrintf (CliHandle, "\n user-defined access-list %d\r\n",
                   u4NextFilter);
        nmhGetIssAclUserDefinedFilterPktType (u4NextFilter, &i4PktType);
        nmhGetIssAclUserDefinedFilterOffSetBase (u4NextFilter, &i4Base);
        nmhGetIssAclUserDefinedFilterOffSetValue (u4NextFilter, &SetValue);
        nmhGetIssAclUserDefinedFilterOffSetMask (u4NextFilter, &SetMask);
        nmhGetIssAclUserDefinedFilterPriority (u4NextFilter, &i4Priority);
        nmhGetIssAclUserDefinedFilterAction (u4NextFilter, &i4FltrAction);
        nmhGetIssAclUserDefinedFilterInPortList (u4NextFilter, &InPortList);
        nmhGetIssAclUserDefinedFilterIdOneType (u4NextFilter, &i4FilterOneType);
        nmhGetIssAclUserDefinedFilterIdOne (u4NextFilter,
                                            (UINT4 *) &i4FilterOneId);
        nmhGetIssAclUserDefinedFilterIdTwoType (u4NextFilter, &i4FilterTwoType);
        nmhGetIssAclUserDefinedFilterIdTwo (u4NextFilter,
                                            (UINT4 *) &i4FilterTwoId);
        i4TotOffsets = 0;
        if ((i4FltrAction == ISS_ALLOW) || (i4FltrAction == ISS_REDIRECT_TO))
        {
            CliPrintf (CliHandle, "permit ");
        }
        else if (i4FltrAction == ISS_DROP)
        {
            CliPrintf (CliHandle, "deny ");
        }
        if ((i4FltrAction == ISS_AND) || (i4FltrAction == ISS_OR)
            || (i4FltrAction == ISS_NOT))
        {
            CliPrintf (CliHandle, "userdefined-list");
            if ((i4FilterOneType == ISS_L3_FILTER)
                && ((i4FilterTwoType == ISS_L3_FILTER)))
            {
                if (i4FltrAction == ISS_AND)
                {
                    CliPrintf (CliHandle,
                               " ip-acl1-and-ip-acl2 %d %d ",
                               i4FilterOneId, i4FilterTwoId);
                }
                else
                {
                    CliPrintf (CliHandle,
                               " ip-acl1-or-ip-acl2 %d %d ",
                               i4FilterOneId, i4FilterTwoId);
                }

            }
            if ((i4FilterOneType == ISS_L2_FILTER)
                && ((i4FilterTwoType == ISS_L2_FILTER)))
            {
                if (i4FltrAction == ISS_AND)
                {
                    CliPrintf (CliHandle,
                               " mac-acl1-and-mac-acl2 %d %d ",
                               i4FilterOneId, i4FilterTwoId);
                }
                else
                {
                    CliPrintf (CliHandle,
                               " mac-acl1-or-mac-acl2 %d %d ",
                               i4FilterOneId, i4FilterTwoId);
                }

            }
            if ((i4FilterOneType == ISS_L2_FILTER)
                && ((i4FilterTwoType == ISS_L3_FILTER)))
            {
                if (i4FltrAction == ISS_AND)
                {
                    CliPrintf (CliHandle,
                               " mac-acl1-and-ip-acl2 %d %d ",
                               i4FilterOneId, i4FilterTwoId);
                }

            }
            if ((i4FilterOneType == ISS_L3_FILTER)
                && ((i4FilterTwoType == ISS_L2_FILTER)))
            {
                if (i4FltrAction == ISS_OR)
                {
                    CliPrintf (CliHandle,
                               " ip-acl1-or-mac-acl2 %d %d ",
                               i4FilterOneId, i4FilterTwoId);
                }

            }
            if ((i4FilterOneType == ISS_NOT)
                && ((i4FilterOneType == ISS_L2_FILTER))
                && ((i4FilterOneId != 0)))
            {
                CliPrintf (CliHandle, "not-mac-acl %d ", i4FilterOneId);
            }
            if ((i4FilterTwoType == ISS_NOT)
                && ((i4FilterTwoType == ISS_L2_FILTER))
                && ((i4FilterTwoId != 0)))
            {
                CliPrintf (CliHandle, "not-mac-acl %d ", i4FilterTwoId);
            }
            if ((i4FilterOneType == ISS_NOT)
                && ((i4FilterOneType == ISS_L3_FILTER))
                && ((i4FilterOneId != 0)))
            {
                CliPrintf (CliHandle, "not-ip-acl %d ", i4FilterOneId);
            }
            if ((i4FilterTwoType == ISS_NOT)
                && ((i4FilterTwoType == ISS_L3_FILTER))
                && ((i4FilterTwoId != 0)))
            {
                CliPrintf (CliHandle, "not-ip-acl %d ", i4FilterTwoId);
            }

        }
        else
        {
            if (i4PktType != 0)
            {
                CliPrintf (CliHandle, " usr-defined-packet-type ");
                switch (i4PktType)
                {
                    case ISS_UDB_PKT_TYPE_USER_DEF:
                        CliPrintf (CliHandle, " user-def");
                        break;

                    case ISS_UDB_PKT_TYPE_IPV4_TCP:
                        CliPrintf (CliHandle, "tcp");
                        break;

                    case ISS_UDB_PKT_TYPE_IPV4_UDP:
                        CliPrintf (CliHandle, "udp");
                        break;

                    case ISS_UDB_PKT_TYPE_MPLS:
                        CliPrintf (CliHandle, "mpls");
                        break;
                    case ISS_UDB_PKT_TYPE_IPV4:
                        CliPrintf (CliHandle, "ipv4");
                        break;

                    case ISS_UDB_PKT_TYPE_IPV6:
                        CliPrintf (CliHandle, "ipv6");
                        break;

                    case ISS_UDB_PKT_TYPE_FRAG:
                        CliPrintf (CliHandle, "frag-ip");
                        break;
                    default:
                        break;
                }
                CliPrintf (CliHandle, "%s  ", " offset-base");
                switch (i4Base)
                {
                    case ISS_UDB_OFFSET_L2:
                        CliPrintf (CliHandle, "L2");
                        break;

                    case ISS_UDB_OFFSET_L3:
                        CliPrintf (CliHandle, "L3");
                        break;

                    case ISS_UDB_OFFSET_L4:
                        CliPrintf (CliHandle, "L4");
                        break;

                    case ISS_UDB_OFFSET_IPV6_EXT_HDR:
                        CliPrintf (CliHandle, "IPV6");
                        break;

                    case ISS_UDB_OFFSET_L3_MINUS_2:
                        CliPrintf (CliHandle, "Ether-Type");
                        break;

                    case ISS_UDB_OFFSET_MPLS_MINUS_2:
                        CliPrintf (CliHandle, "Ether-Type");
                        break;

                    default:
                        CliPrintf (CliHandle, "%d\r\n", i4Base);
                        break;
                }
                for (i4Length = 0; i4Length < SetValue.i4_Length; i4Length++)
                {
                    if (au1SetValue[i4Length] != 0)

                    {
                        if ((i4TotOffsets == 0) && (i4TotOffsets < 6))
                        {
                            CliPrintf (CliHandle, " offset1 %d %d ", i4Length,
                                       au1SetValue[i4Length]);
                        }
                        if ((i4TotOffsets == 1) && (i4TotOffsets < 6))
                        {
                            CliPrintf (CliHandle, " offset2 %d %d ", i4Length,
                                       au1SetValue[i4Length]);
                        }
                        if ((i4TotOffsets == 2) && (i4TotOffsets < 6))
                        {
                            CliPrintf (CliHandle, " offset3 %d %d ", i4Length,
                                       au1SetValue[i4Length]);
                        }
                        if ((i4TotOffsets == 3) && (i4TotOffsets < 6))
                        {
                            CliPrintf (CliHandle, " offset4 %d %d ", i4Length,
                                       au1SetValue[i4Length]);
                        }
                        if ((i4TotOffsets == 4) && (i4TotOffsets < 6))
                        {
                            CliPrintf (CliHandle, " offset5 %d %d ", i4Length,
                                       au1SetValue[i4Length]);
                        }
                        if ((i4TotOffsets == 5) && (i4TotOffsets < 6))
                        {
                            CliPrintf (CliHandle, " offset6 %d %d ", i4Length,
                                       au1SetValue[i4Length]);
                        }
                        i4TotOffsets++;
                    }
                }
            }
            u4RedirectGrpId = 0;
            u4InterfaceIndex = 0;
            i4TrafficDistByte = 0;
            i4RedirectInterfaceGrpType = 0;
            i4SubActionId = 0;
            i4SubAction = 0;
            MEMSET (&RedirectsPorts, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

            if (i4FltrAction == ISS_REDIRECT_TO)
            {
                CliPrintf (CliHandle, "redirect ");
                nmhGetIssAclUserDefinedFilterRedirectId (u4NextFilter,
                                                         (INT4 *)
                                                         &u4RedirectGrpId);

                if (u4RedirectGrpId != 0)
                {
                    pPorts = (tPortList *)
                        FsUtilAllocBitList (sizeof (tPortList));

                    if (pPorts == NULL)
                        return;

                    MEMSET (*pPorts, 0, sizeof (tPortList));

                    RedirectsPorts.pu1_OctetList = *pPorts;
                    RedirectsPorts.i4_Length = sizeof (tPortList);

                    nmhGetIssRedirectInterfaceGrpDistByte
                        (u4RedirectGrpId, &i4TrafficDistByte);

                    nmhGetIssRedirectInterfaceGrpUdbPosition
                        (u4RedirectGrpId, &i4RedirectUdbPosition);

                    nmhGetIssRedirectInterfaceGrpType
                        (u4RedirectGrpId, &i4RedirectInterfaceGrpType);

                    nmhGetIssRedirectInterfaceGrpPortList
                        (u4RedirectGrpId, &RedirectsPorts);

                    if ((FsUtilBitListIsAllZeros
                         (RedirectsPorts.pu1_OctetList,
                          (UINT4) RedirectsPorts.i4_Length) == OSIX_FALSE) &&
                        (i4RedirectInterfaceGrpType ==
                         ISS_REDIRECT_TO_PORTLIST))
                    {
                        u4PagingStatus = CLI_SUCCESS;
                        CliConfOctetToIfName (CliHandle, "",
                                              NULL,
                                              &RedirectsPorts, &u4PagingStatus);
                    }
                    if (i4RedirectInterfaceGrpType != ISS_REDIRECT_TO_PORTLIST)
                    {
                        MEMCPY (&u4InterfaceIndex,
                                RedirectsPorts.pu1_OctetList, sizeof (UINT4));

                        CfaCliGetIfName (u4InterfaceIndex, piIfName);

                        CliPrintf (CliHandle, " interface %s", piIfName);
                    }
                    FsUtilReleaseBitList ((UINT1 *) pPorts);
                    switch (i4TrafficDistByte)
                    {
                        case ISS_TRAFF_DIST_BYTE_SRCIP:
                            CliPrintf (CliHandle, " load-balance src-ip ");
                            break;

                        case ISS_TRAFF_DIST_BYTE_DSTIP:
                            CliPrintf (CliHandle, "  load-balance dst-ip ");
                            break;

                        case ISS_TRAFF_DIST_BYTE_SMAC:
                            CliPrintf (CliHandle, "  load-balance src-mac ");
                            break;

                        case ISS_TRAFF_DIST_BYTE_DMAC:
                            CliPrintf (CliHandle, " load-balance dst-mac ");
                            break;

                        case ISS_TRAFF_DIST_BYTE_STCPPORT:
                            CliPrintf (CliHandle,
                                       "  load-balance src-tcpport ");
                            break;

                        case ISS_TRAFF_DIST_BYTE_DTCPPORT:
                            CliPrintf (CliHandle,
                                       "  load-balance dst-tcpport ");
                            break;

                        case ISS_TRAFF_DIST_BYTE_SUDPPORT:
                            CliPrintf (CliHandle,
                                       "  load-balance src-udpport ");
                            break;

                        case ISS_TRAFF_DIST_BYTE_DUDPPORT:
                            CliPrintf (CliHandle,
                                       "  load-balance dst-udpport ");
                            break;

                        case ISS_TRAFF_DIST_BYTE_VLANID:
                            CliPrintf (CliHandle, "  load-balance vlanid ");
                            break;

                        case ISS_REDIRECT_UDB_FIELD:
                            CliPrintf (CliHandle, " load-balance udb  %d",
                                       i4RedirectUdbPosition);
                            break;
                        default:
                            CliPrintf (CliHandle, " ");
                    }
                }
            }

            nmhGetIssAclUserDefinedFilterSubAction (u4NextFilter, &i4SubAction);
            nmhGetIssAclUserDefinedFilterSubActionId (u4NextFilter,
                                                      &i4SubActionId);

            if (i4SubAction == ISS_MODIFY_VLAN)
            {
                CliPrintf (CliHandle, "  sub-action modify-vlan %d",
                           i4SubActionId);
            }
            else if (i4SubAction == ISS_NESTED_VLAN)
            {
                CliPrintf (CliHandle, "  sub-action nested-vlan %d",
                           i4SubActionId);
            }
            if (u4Switching == 1)
            {
                CliPrintf (CliHandle, "noswitching ");
                u4Switching = 0;
            }

        }
        if (i4Priority != 1)
        {
            CliPrintf (CliHandle, "priority %d\n", i4Priority);
        }
        u4Quit = (UINT4) CliPrintf (CliHandle, "\n!\r\n");
        if (u4Quit == CLI_FAILURE)
        {
            ISS_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return;
        }
        u4PrevFilter = u4NextFilter;
        i1OutCome =
            nmhGetNextIndexIssAclUserDefinedFilterTable (u4PrevFilter,
                                                         &u4NextFilter);
    }

    /*Reserve frame Transmission Start */
    i1OutCome =
        nmhGetFirstIndexIssReservedFrameCtrlTable (&u4NextReservedFrameCtrlId);
    while (i1OutCome != SNMP_FAILURE)
    {
        MEMSET (ResrvFrmMacAddr, 0, sizeof (tMacAddr));
        MEMSET (au1ResMacStr, 0, MAC_ADDR_LEN);
        CliPrintf (CliHandle, "\n reserved-frame-action ");
        nmhGetIssReservedFrameCtrlPktType (u4NextReservedFrameCtrlId,
                                           &i4ReservedFrameCtrlPktType);
        nmhGetIssReservedFrameCtrlAction (u4NextReservedFrameCtrlId,
                                          &i4ReservedFrameCtrlAction);
        nmhGetIssReservedFrameCtrlOtherMacAddr (u4NextReservedFrameCtrlId,
                                                &ResrvFrmMacAddr);
        nmhGetIssReservedFrameCtrlOtherMacMask (u4NextReservedFrameCtrlId,
                                                &i4MacMask);
        nmhGetIssReservedFrameCtrlStatus (u4NextReservedFrameCtrlId,
                                          &i4ReservedFrameCtrlStatus);

        switch (i4ReservedFrameCtrlPktType)
        {
            case ISS_RESERVED_FRAME_BPDU:
                CliPrintf (CliHandle, "bpdu ");
                break;
            case ISS_RESERVED_FRAME_LACPDU:
                CliPrintf (CliHandle, "lacpdu ");
                break;
            case ISS_RESERVED_FRAME_EAP:
                CliPrintf (CliHandle, "eap ");
                break;
            case ISS_RESERVED_FRAME_LLDPDU:
                CliPrintf (CliHandle, "lldpdu ");
                break;
            case ISS_RESERVED_FRAME_OTHER:
                CliPrintf (CliHandle, "other ");
                if (i4MacMask != 0xFF)
                {
                    CliPrintf (CliHandle, "range ");
                    PrintMacAddress (ResrvFrmMacAddr, au1ResMacStr);
                    CliPrintf (CliHandle, " %-20s ", au1ResMacStr);
                    CliPrintf (CliHandle, "mask ");
                    CliPrintf (CliHandle, "0x%x ", i4MacMask);
                }
                else
                {
                    PrintMacAddress (ResrvFrmMacAddr, au1ResMacStr);
                    CliPrintf (CliHandle, " %-20s ", au1ResMacStr);
                }

                break;
            case ISS_RESERVED_FRAME_ALL:
                CliPrintf (CliHandle, "all ");
                break;
        }
        if (i4ReservedFrameCtrlAction == RESERVED_FRAME_ACTION_FWD)
        {
            CliPrintf (CliHandle, "forward");
        }
        else
        {
            CliPrintf (CliHandle, "drop");
        }
        u4CurrReservedFrameCtrlId = u4NextReservedFrameCtrlId;
        i1OutCome =
            nmhGetNextIndexIssReservedFrameCtrlTable (u4CurrReservedFrameCtrlId,
                                                      &u4NextReservedFrameCtrlId);
    }
    CliPrintf (CliHandle, "\r\n");
    ISS_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    ISS_TRC (INIT_SHUT_TRC,
             "\nCLI:Exit AclShowRunningConfigTables function \n");
    return;

}

/*****************************************************************************/
/*     FUNCTION NAME    : AclShowRunningConfigInterfaceDetails               */
/*                                                                           */
/*     DESCRIPTION      : This function displays the  Interface objects in ACL*/
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
AclShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4Index)
{
    INT4                i4Direction = 0;
    INT4                i4Count;
    INT4                i4NextFilter;
    INT4                i4PrevFilter;
    INT4                i4RowStatus = 0;
    INT4                i4Stats = ACL_STAT_DISABLE;
    INT1                i4RetVal = 0;
    UINT1               au1InPortList[ISS_PORTLIST_LEN];
    UINT1               au1OutPortList[ISS_PORTLIST_LEN];
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *piIfName;
    tSNMP_OCTET_STRING_TYPE InPortList;
    tSNMP_OCTET_STRING_TYPE OutPortList;
    INT1                i1OutCome;
    tSNMP_OCTET_STRING_TYPE InPortChannelList;
    tSNMP_OCTET_STRING_TYPE OutPortChannelList;
    UINT1               au1InPortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];
    UINT1               au1OutPortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];
    UINT4               u4SChannelIfIndex = 0;
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    piIfName = (INT1 *) &au1IfName[0];

    InPortList.pu1_OctetList = &au1InPortList[0];
    InPortList.i4_Length = ISS_PORTLIST_LEN;

    OutPortList.pu1_OctetList = &au1OutPortList[0];
    OutPortList.i4_Length = ISS_PORTLIST_LEN;

    InPortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;
    InPortChannelList.pu1_OctetList = &au1InPortChannelList[0];

    OutPortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;
    OutPortChannelList.pu1_OctetList = &au1OutPortChannelList[0];

    CliRegisterLock (CliHandle, IssLock, IssUnLock);
    ISS_LOCK ();
    /*Ip Access Lists */
    i1OutCome = nmhGetFirstIndexIssExtL3FilterTable (&i4NextFilter);

    while (i1OutCome != SNMP_FAILURE)
    {

        i4Stats = ACL_STAT_DISABLE;

        /* L3 ACL show running display only configured through ACL commands */
        pIssL3FilterEntry = IssExtGetL3FilterEntry (i4NextFilter);
        if ((pIssL3FilterEntry != NULL) &&
            (pIssL3FilterEntry->b1IsL3AclConfFromExt == TRUE))
        {
            /* ACL configured through other application, so skip this */
            i4PrevFilter = i4NextFilter;

            i1OutCome =
                nmhGetNextIndexIssExtL3FilterTable (i4PrevFilter,
                                                    &i4NextFilter);

            continue;
        }

        MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
        MEMSET (au1OutPortList, 0, ISS_PORTLIST_LEN);

        MEMSET (au1InPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
        MEMSET (au1OutPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);

        i4RetVal = nmhGetIssExtL3FilterStatus (i4NextFilter, &i4RowStatus);

        if (i4RowStatus == ISS_ACTIVE)
        {
            nmhGetIssExtL3FilterInPortList (i4NextFilter, &InPortList);
            nmhGetIssExtL3FilterOutPortList (i4NextFilter, &OutPortList);
            nmhGetIssExtL3FilterInPortChannelList (i4NextFilter,
                                                   &InPortChannelList);
            nmhGetIssExtL3FilterOutPortChannelList (i4NextFilter,
                                                    &OutPortChannelList);
            nmhGetIssAclL3FilterSChannelIfIndex (i4NextFilter,
                                                 (INT4 *) &u4SChannelIfIndex);
            /* Get Ingress S-channelIfIndex from the L3 Filter Table */
            if ((nmhGetIssAclL3FilterStatsEnabledStatus (i4NextFilter,
                                                         &i4Stats)) ==
                SNMP_FAILURE)
            {
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to Retrieve L3 Filter Statistics Status %d for Filter ID %d\n",
                              i4Stats, i4NextFilter);
            }

            if (i4Index == -1)
            {
                i4Count = 0;

                do
                {
                    if (CliIsMemberPort (InPortList.pu1_OctetList,
                                         ISS_PORTLIST_LEN,
                                         i4Count) == CLI_SUCCESS)
                    {
                        MEMSET (piIfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                        CfaCliConfGetIfName ((UINT4) i4Count, piIfName);
                        CliPrintf (CliHandle, "\r\n!\r\n");
                        CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                        CliPrintf (CliHandle, "ip access-group %d in",
                                   i4NextFilter);
                        if (i4Stats == ACL_STAT_ENABLE)
                        {
                            CliPrintf (CliHandle, " hardware-count");
                        }
                        CliPrintf (CliHandle, "\r\n!\r\n");

                    }

                    i4Count++;

                }
                while (i4Count <= SYS_DEF_MAX_PHYSICAL_INTERFACES);

                i4Count = 0;
                i4Count = SYS_DEF_MAX_PHYSICAL_INTERFACES + 1;

                do
                {
                    if (CliIsMemberPort (InPortChannelList.pu1_OctetList,
                                         ISS_PORT_CHANNEL_LIST_SIZE,
                                         i4Count) == CLI_SUCCESS)
                    {
                        MEMSET (piIfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                        CfaCliConfGetIfName ((UINT4) i4Count, piIfName);
                        CliPrintf (CliHandle, "\r\n!\r\n");
                        CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                        CliPrintf (CliHandle, "ip access-group %d in",
                                   i4NextFilter);
                        if (i4Stats == ACL_STAT_ENABLE)
                        {
                            CliPrintf (CliHandle, " hardware-count");
                        }
                        CliPrintf (CliHandle, "\r\n!\r\n");

                    }

                    i4Count++;

                }
                while (i4Count <= BRG_MAX_PHY_PLUS_LOG_PORTS);

                if (u4SChannelIfIndex != 0)
                {
                    MEMSET (piIfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                    CfaCliConfGetIfName (u4SChannelIfIndex, piIfName);
                    CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                    CliPrintf (CliHandle, "ip access-group %d in",
                               i4NextFilter);
                    CliPrintf (CliHandle, "\r\n!\r\n");
                }

                i4Count = 0;
                do
                {

                    if (CliIsMemberPort (OutPortList.pu1_OctetList,
                                         ISS_PORTLIST_LEN,
                                         i4Count) == CLI_SUCCESS)
                    {
                        MEMSET (piIfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                        CfaCliConfGetIfName ((UINT4) i4Count, piIfName);
                        CliPrintf (CliHandle, "\r\n!\r\n");
                        CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                        CliPrintf (CliHandle, "ip access-group %d out",
                                   i4NextFilter);
                        if (i4Stats == ACL_STAT_ENABLE)
                        {
                            CliPrintf (CliHandle, " hardware-count");
                        }
                        CliPrintf (CliHandle, "\r\n!\r\n");

                    }

                    i4Count++;
                }
                while (i4Count <= SYS_DEF_MAX_PHYSICAL_INTERFACES);

                i4Count = 0;
                i4Count = SYS_DEF_MAX_PHYSICAL_INTERFACES + 1;
                do
                {
                    if (CliIsMemberPort (OutPortChannelList.pu1_OctetList,
                                         ISS_PORT_CHANNEL_LIST_SIZE,
                                         i4Count) == CLI_SUCCESS)
                    {
                        MEMSET (piIfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                        CfaCliConfGetIfName ((UINT4) i4Count, piIfName);

                        CliPrintf (CliHandle, "\r\n!\r\n");
                        CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                        CliPrintf (CliHandle, "ip access-group %d out",
                                   i4NextFilter);
                        if (i4Stats == ACL_STAT_ENABLE)
                        {
                            CliPrintf (CliHandle, " hardware-count");
                        }
                        CliPrintf (CliHandle, "\r\n!\r\n");

                    }

                    i4Count++;
                }
                while (i4Count <= BRG_MAX_PHY_PLUS_LOG_PORTS);
            }
            else
            {
                if ((i4Index >= CFA_MIN_EVB_SBP_INDEX) &&
                    (i4Index <= CFA_MAX_EVB_SBP_INDEX))
                {
                    MEMSET (piIfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                    CfaCliConfGetIfName (u4SChannelIfIndex, piIfName);
                    CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                    CliPrintf (CliHandle, "ip access-group %d in",
                               i4NextFilter);
                    CliPrintf (CliHandle, "\r\n!\r\n");
                }

                else if ((i4Index > BRG_MAX_PHY_PORTS)
                         && (i4Index <= BRG_MAX_PHY_PLUS_LOG_PORTS))
                {
                    if (CliIsMemberPort (InPortChannelList.pu1_OctetList,
                                         ISS_PORT_CHANNEL_LIST_SIZE,
                                         i4Index) == CLI_SUCCESS)
                    {
                        MEMSET (piIfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                        CfaCliConfGetIfName ((UINT4) i4Index, piIfName);

                        CliPrintf (CliHandle, "\r\n!\r\n");
                        CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                        CliPrintf (CliHandle, "ip access-group %d in",
                                   i4NextFilter);
                        if (i4Stats == ACL_STAT_ENABLE)
                        {
                            CliPrintf (CliHandle, " hardware-count");
                        }
                        CliPrintf (CliHandle, "\r\n!\r\n");
                    }

                    if (CliIsMemberPort (OutPortChannelList.pu1_OctetList,
                                         ISS_PORT_CHANNEL_LIST_SIZE,
                                         i4Index) == CLI_SUCCESS)
                    {
                        MEMSET (piIfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                        CfaCliConfGetIfName ((UINT4) i4Index, piIfName);

                        CliPrintf (CliHandle, "\r\n!\r\n");
                        CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                        CliPrintf (CliHandle, "ip access-group %d out",
                                   i4NextFilter);
                        if (i4Stats == ACL_STAT_ENABLE)
                        {
                            CliPrintf (CliHandle, " hardware-count");
                        }
                        CliPrintf (CliHandle, "\r\n!\r\n");
                    }
                }
                else
                {
                    if (CliIsMemberPort (InPortList.pu1_OctetList,
                                         ISS_PORTLIST_LEN,
                                         i4Index) == CLI_SUCCESS)
                    {
                        MEMSET (piIfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                        CfaCliConfGetIfName ((UINT4) i4Index, piIfName);

                        CliPrintf (CliHandle, "\r\n!\r\n");
                        CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                        CliPrintf (CliHandle, "ip access-group %d in",
                                   i4NextFilter);
                        if (i4Stats == ACL_STAT_ENABLE)
                        {
                            CliPrintf (CliHandle, " hardware-count");
                        }
                        CliPrintf (CliHandle, "\r\n!\r\n");
                    }

                    if (CliIsMemberPort (OutPortList.pu1_OctetList,
                                         ISS_PORTLIST_LEN,
                                         i4Index) == CLI_SUCCESS)
                    {
                        MEMSET (piIfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                        CfaCliConfGetIfName ((UINT4) i4Index, piIfName);
                        CliPrintf (CliHandle, "\r\n!\r\n");
                        CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                        CliPrintf (CliHandle, "ip access-group %d out",
                                   i4NextFilter);
                        if (i4Stats == ACL_STAT_ENABLE)
                        {
                            CliPrintf (CliHandle, " hardware-count");
                        }
                        CliPrintf (CliHandle, "\r\n!\r\n");
                    }

                }

            }

        }

        i4PrevFilter = i4NextFilter;
        i1OutCome =
            nmhGetNextIndexIssExtL3FilterTable (i4PrevFilter, &i4NextFilter);
    }

    /*MacAccessLists */

    i1OutCome = nmhGetFirstIndexIssExtL2FilterTable (&i4NextFilter);

    while (i1OutCome != SNMP_FAILURE)
    {

        i4Count = 0;
        u4SChannelIfIndex = 0;
        i4Stats = ACL_STAT_DISABLE;

        /* L2 ACL show running display only configured through ACL commands */
        pIssL2FilterEntry = IssExtGetL2FilterEntry (i4NextFilter);
        if ((pIssL2FilterEntry != NULL) &&
            (pIssL2FilterEntry->b1IsL2AclConfFromExt == TRUE))
        {
            /* ACL configured through other application, so skip this */
            i4PrevFilter = i4NextFilter;

            i1OutCome =
                nmhGetNextIndexIssExtL2FilterTable (i4PrevFilter,
                                                    &i4NextFilter);

            continue;
        }

        MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
        MEMSET (au1OutPortList, 0, ISS_PORTLIST_LEN);

        nmhGetIssExtL2FilterStatus (i4NextFilter, &i4RowStatus);

        if (i4RowStatus == ISS_ACTIVE)
        {
            nmhGetIssExtL2FilterDirection (i4NextFilter, &i4Direction);
            nmhGetIssExtL2FilterInPortList (i4NextFilter, &InPortList);
            nmhGetIssExtL2FilterOutPortList (i4NextFilter, &OutPortList);
            nmhGetIssExtL2FilterInPortChannelList (i4NextFilter,
                                                   &InPortChannelList);
            nmhGetIssExtL2FilterOutPortChannelList (i4NextFilter,
                                                    &OutPortChannelList);
            nmhGetIssAclL2FilterSChannelIfIndex (i4NextFilter,
                                                 (INT4 *) &u4SChannelIfIndex);
            if ((nmhGetIssAclL2FilterStatsEnabledStatus
                 (i4NextFilter, &i4Stats)) == SNMP_FAILURE)
            {
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to Retrieve L2 Filter Statistics Status %d for Filter ID %d\n",
                              i4Stats, i4NextFilter);
            }

            if (i4Index == -1)
            {
                i4Count = 0;

                if (i4Direction == ACL_ACCESS_IN)
                {
                    do
                    {
                        if (CliIsMemberPort (InPortList.pu1_OctetList,
                                             ISS_PORTLIST_LEN,
                                             i4Count) == CLI_SUCCESS)
                        {
                            MEMSET (piIfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                            CfaCliConfGetIfName ((UINT4) i4Count, piIfName);

                            CliPrintf (CliHandle, "\r\n!\r\n");
                            CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                            CliPrintf (CliHandle,
                                       "mac access-group %d in", i4NextFilter);
                            if (i4Stats == ACL_STAT_ENABLE)
                            {
                                CliPrintf (CliHandle, " hardware-count");
                            }
                            CliPrintf (CliHandle, "\r\n!\r\n");
                        }

                        i4Count++;
                    }
                    while (i4Count <= SYS_DEF_MAX_PHYSICAL_INTERFACES);

                    i4Count = 0;
                    i4Count = SYS_DEF_MAX_PHYSICAL_INTERFACES + 1;

                    do
                    {
                        if (CliIsMemberPort (InPortChannelList.pu1_OctetList,
                                             ISS_PORT_CHANNEL_LIST_SIZE,
                                             i4Count) == CLI_SUCCESS)
                        {
                            MEMSET (piIfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                            CfaCliConfGetIfName ((UINT4) i4Count, piIfName);

                            CliPrintf (CliHandle, "\r\n!\r\n");
                            CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                            CliPrintf (CliHandle, "ip access-group %d in",
                                       i4NextFilter);
                            if (i4Stats == ACL_STAT_ENABLE)
                            {
                                CliPrintf (CliHandle, " hardware-count");
                            }
                            CliPrintf (CliHandle, "\r\n!\r\n");
                        }
                        i4Count++;
                    }
                    while (i4Count <= BRG_MAX_PHY_PLUS_LOG_PORTS);

                    if (u4SChannelIfIndex != 0)
                    {
                        MEMSET (piIfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                        CfaCliConfGetIfName (u4SChannelIfIndex, piIfName);
                        CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                        CliPrintf (CliHandle,
                                   "mac access-group %d in", i4NextFilter);
                        CliPrintf (CliHandle, "\r\n!\r\n");

                    }
                }
                else
                {
                    do
                    {

                        if (CliIsMemberPort (OutPortList.pu1_OctetList,
                                             ISS_PORTLIST_LEN,
                                             i4Count) == CLI_SUCCESS)
                        {
                            MEMSET (piIfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                            CfaCliConfGetIfName ((UINT4) i4Count, piIfName);

                            CliPrintf (CliHandle, "\r\n!\r\n");
                            CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                            CliPrintf (CliHandle,
                                       "mac access-group %d out", i4NextFilter);
                            if (i4Stats == ACL_STAT_ENABLE)
                            {
                                CliPrintf (CliHandle, " hardware-count");
                            }
                            CliPrintf (CliHandle, "\r\n!\r\n");
                        }

                        i4Count++;
                    }
                    while (i4Count <= SYS_DEF_MAX_PHYSICAL_INTERFACES);

                    i4Count = 0;
                    i4Count = SYS_DEF_MAX_PHYSICAL_INTERFACES + 1;
                    do
                    {
                        if (CliIsMemberPort (OutPortChannelList.pu1_OctetList,
                                             ISS_PORT_CHANNEL_LIST_SIZE,
                                             i4Count) == CLI_SUCCESS)
                        {
                            MEMSET (piIfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                            CfaCliConfGetIfName ((UINT4) i4Count, piIfName);

                            CliPrintf (CliHandle, "\r\n!\r\n");
                            CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                            CliPrintf (CliHandle, "ip access-group %d out",
                                       i4NextFilter);
                            if (i4Stats == ACL_STAT_ENABLE)
                            {
                                CliPrintf (CliHandle, " hardware-count");
                            }
                            CliPrintf (CliHandle, "\r\n!\r\n");
                        }
                        i4Count++;
                    }
                    while (i4Count <= BRG_MAX_PHY_PLUS_LOG_PORTS);
                }
            }
            else
            {

                if (i4Direction == ACL_ACCESS_IN)
                {
                    if ((i4Index >= CFA_MIN_EVB_SBP_INDEX) &&
                        (i4Index <= CFA_MAX_EVB_SBP_INDEX))
                    {
                        MEMSET (piIfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                        CfaCliConfGetIfName (u4SChannelIfIndex, piIfName);
                        CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                        CliPrintf (CliHandle, "mac access-group %d in",
                                   i4NextFilter);
                        CliPrintf (CliHandle, "\r\n!\r\n");
                    }

                    else if ((i4Index > BRG_MAX_PHY_PORTS)
                             && (i4Index <= BRG_MAX_PHY_PLUS_LOG_PORTS))
                    {
                        if (CliIsMemberPort (InPortChannelList.pu1_OctetList,
                                             ISS_PORT_CHANNEL_LIST_SIZE,
                                             i4Index) == CLI_SUCCESS)
                        {
                            MEMSET (piIfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                            CfaCliConfGetIfName ((UINT4) i4Index, piIfName);

                            CliPrintf (CliHandle, "\r\n!\r\n");
                            CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                            CliPrintf (CliHandle, "mac access-group %d in",
                                       i4NextFilter);
                            if (i4Stats == ACL_STAT_ENABLE)
                            {
                                CliPrintf (CliHandle, " hardware-count");
                            }
                            CliPrintf (CliHandle, "\r\n!\r\n");
                        }
                    }
                    else
                    {
                        if (CliIsMemberPort (InPortList.pu1_OctetList,
                                             ISS_PORTLIST_LEN,
                                             i4Index) == CLI_SUCCESS)
                        {
                            MEMSET (piIfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                            CfaCliConfGetIfName ((UINT4) i4Index, piIfName);
                            CliPrintf (CliHandle, "\r\n!\r\n");
                            CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                            CliPrintf (CliHandle, "mac access-group %d in",
                                       i4NextFilter);
                            if (i4Stats == ACL_STAT_ENABLE)
                            {
                                CliPrintf (CliHandle, " hardware-count");
                            }
                            CliPrintf (CliHandle, "\r\n!\r\n");
                        }
                    }
                }
                else
                {
                    if ((i4Index > BRG_MAX_PHY_PORTS)
                        && (i4Index <= BRG_MAX_PHY_PLUS_LOG_PORTS))
                    {
                        if (CliIsMemberPort (OutPortChannelList.pu1_OctetList,
                                             ISS_PORT_CHANNEL_LIST_SIZE,
                                             i4Index) == CLI_SUCCESS)
                        {
                            MEMSET (piIfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                            CfaCliConfGetIfName ((UINT4) i4Index, piIfName);
                            CliPrintf (CliHandle, "\r\n!\r\n");
                            CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                            CliPrintf (CliHandle, "mac access-group %d out",
                                       i4NextFilter);
                            if (i4Stats == ACL_STAT_ENABLE)
                            {
                                CliPrintf (CliHandle, " hardware-count");
                            }
                            CliPrintf (CliHandle, "\r\n!\r\n");
                        }
                    }
                    else
                    {
                        if (CliIsMemberPort (OutPortList.pu1_OctetList,
                                             ISS_PORTLIST_LEN,
                                             i4Index) == CLI_SUCCESS)
                        {
                            MEMSET (piIfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                            CfaCliConfGetIfName ((UINT4) i4Index, piIfName);
                            CliPrintf (CliHandle, "\r\n!\r\n");
                            CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                            CliPrintf (CliHandle, "mac access-group %d out",
                                       i4NextFilter);
                            if (i4Stats == ACL_STAT_ENABLE)
                            {
                                CliPrintf (CliHandle, " hardware-count");
                            }
                            CliPrintf (CliHandle, "\r\n!\r\n");
                        }
                    }
                }
            }
        }

        i4PrevFilter = i4NextFilter;

        i1OutCome =
            nmhGetNextIndexIssExtL2FilterTable (i4PrevFilter, &i4NextFilter);
    }
    /*UDB Access Lists */
    i1OutCome =
        nmhGetFirstIndexIssAclUserDefinedFilterTable ((UINT4 *) &i4NextFilter);
    while (i1OutCome != SNMP_FAILURE)
    {
        i4Stats = ACL_STAT_DISABLE;
        MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
        MEMSET (au1OutPortList, 0, ISS_PORTLIST_LEN);
        nmhGetIssAclUserDefinedFilterStatus (i4NextFilter, &i4RowStatus);
        if (i4RowStatus == ISS_ACTIVE)
        {
            i4Direction = ACL_ACCESS_IN;
            nmhGetIssAclUserDefinedFilterInPortList (i4NextFilter, &InPortList);
            if ((nmhGetIssAclUserDefinedFilterStatsEnabledStatus (i4NextFilter,
                                                                  &i4Stats)) !=
                SNMP_FAILURE)
            {
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to Retrieve UDB Filter Statistics Status %d for Filter ID %d\n",
                              i4Stats, i4NextFilter);
            }
            if (i4Index == -1)
            {
                i4Count = 0;
                if (i4Direction == ACL_ACCESS_IN)
                {
                    do
                    {
                        if (CliIsMemberPort (InPortList.pu1_OctetList,
                                             ISS_PORTLIST_LEN,
                                             i4Count) == CLI_SUCCESS)
                        {
                            MEMSET (piIfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                            CfaCliConfGetIfName ((UINT4) i4Index, piIfName);
                            CliPrintf (CliHandle, "\r\n!\r\n");
                            CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                            CliPrintf (CliHandle,
                                       "user-defined access-group %d in",
                                       i4NextFilter);
                            if (i4Stats == ACL_STAT_ENABLE)
                            {
                                CliPrintf (CliHandle, " hardware-count");
                            }
                            CliPrintf (CliHandle, "\r\n!\r\n");
                        }
                        i4Count++;
                    }
                    while (i4Count <= SYS_DEF_MAX_PHYSICAL_INTERFACES);
                }
            }
            else
            {
                if (CliIsMemberPort (InPortList.pu1_OctetList,
                                     ISS_PORTLIST_LEN, i4Index) == CLI_SUCCESS)
                {
                    if (i4Direction == ACL_ACCESS_IN)
                    {
                        MEMSET (piIfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                        CfaCliConfGetIfName ((UINT4) i4Index, piIfName);
                        CliPrintf (CliHandle, "\r\n!\r\n");
                        CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                        CliPrintf (CliHandle,
                                   "user-defined access-group %d out",
                                   i4NextFilter);
                        if (i4Stats == ACL_STAT_ENABLE)
                        {
                            CliPrintf (CliHandle, " hardware-count");
                        }
                        CliPrintf (CliHandle, "\r\n!\r\n");
                    }
                }
            }
        }
        i4PrevFilter = i4NextFilter;
        i1OutCome =
            nmhGetNextIndexIssAclUserDefinedFilterTable ((UINT4) i4PrevFilter,
                                                         (UINT4 *)
                                                         &i4NextFilter);
    }
    ISS_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    ISS_TRC (INIT_SHUT_TRC,
             "\nCLI:Exit  function  AclShowRunningConfigInterfaceDetails\n");
    UNUSED_PARAM (i4RetVal);
    return;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : StdAclGetCfgPrompt                                 */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Class map prompt in pi1DispStr if  */
/*                        valid.                                             */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/

INT1
StdAclGetCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;
    INT4                i4StdAclIndex;

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry StdAclGetCfgPrompt function \n");
    if (!pi1DispStr)
    {
        return FALSE;
    }

    /* NULL is passed to return "config-std-nacl" as the prompt 
     * for the mode  */
    if (pi1ModeName == NULL)
    {
        STRCPY (pi1DispStr, "config-std-nacl");
        return TRUE;
    }

    u4Len = STRLEN (CLI_STDACL_MODE);

    if (STRNCMP (pi1ModeName, CLI_STDACL_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    i4StdAclIndex = CLI_ATOI (pi1ModeName);

    /* Set the mode information for CLI */
    CLI_SET_STDACLID (i4StdAclIndex);

    STRCPY (pi1DispStr, "(config-std-nacl)#");
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit StdAclGetCfgPrompt function \n");
    return TRUE;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ExtAclGetCfgPrompt                                 */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Class map prompt in pi1DispStr if  */
/*                        valid.                                             */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/

INT1
ExtAclGetCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;
    INT4                i4ExtAclIndex;

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry ExtAclGetCfgPrompt function \n");
    if (!pi1DispStr)
    {
        return FALSE;
    }

    /* NULL is passed to return "config-std-nacl" as the prompt 
     * for the mode  */
    if (pi1ModeName == NULL)
    {
        STRCPY (pi1DispStr, "config-ext-nacl");
        return TRUE;
    }

    u4Len = STRLEN (CLI_EXTACL_MODE);
    if (STRNCMP (pi1ModeName, CLI_EXTACL_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    i4ExtAclIndex = CLI_ATOI (pi1ModeName);

    /* Set the mode information for CLI */
    CLI_SET_EXTACL (i4ExtAclIndex);

    STRCPY (pi1DispStr, "(config-ext-nacl)#");
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit ExtAclGetCfgPrompt function \n");
    return TRUE;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MacAclGetCfgPrompt                                 */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Class map prompt in pi1DispStr if  */
/*                        valid.                                             */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/

INT1
MacAclGetCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;
    INT4                i4MacAclIndex;

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry MacAclGetCfgPrompt function \n");
    if (!pi1DispStr)
    {
        return FALSE;
    }

    /* NULL is passed to return "config-std-nacl" as the prompt 
     * for the mode  */
    if (pi1ModeName == NULL)
    {
        STRCPY (pi1DispStr, "config-ext-macl");
        return TRUE;
    }

    u4Len = STRLEN (CLI_MACACL_MODE);
    if (STRNCMP (pi1ModeName, CLI_MACACL_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    i4MacAclIndex = CLI_ATOI (pi1ModeName);

    /* Set the mode information for CLI */
    CLI_SET_MACACL (i4MacAclIndex);

    STRCPY (pi1DispStr, "(config-ext-macl)#");
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit MacAclGetCfgPrompt function \n");
    return TRUE;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclTestIpParams                                    */
/*                                                                           */
/*     DESCRIPTION      : This function tests the IP address, mask for a     */
/*                        filter                                             */
/*                                                                           */
/*     INPUT            : CliHandle - CLI HAndler                            */
/*                        u1Type - whether Source/Destination Ip address     */
/*                        information must be tested                         */
/*                        i4FilterNo - IP Filter Number                      */
/*                        i4IpType - Whether IPaddress and/or mask is user   */
/*                        input                                              */
/*                        u4IpAddr - Ip Address to be tested                 */
/*                        u4IpMask - Ip Mask to be tested                    */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclTestIpParams (UINT1 u1Type, INT4 i4FilterNo, UINT4 u4IpType,
                 UINT4 u4IpAddr, UINT4 u4IpMask)
{
    UINT4               u4ErrCode;
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclTestIpParams  function \n");
    if (u1Type == ACL_SRC)
    {
        if (u4IpType != ACL_ANY)
        {
            /* Source IP address has been specified */
            if (nmhTestv2IssExtL3FilterSrcIpAddr
                (&u4ErrCode, i4FilterNo, u4IpAddr) == SNMP_FAILURE)
            {
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to test L3 Filter Src IpAddr for Filter ID %d                                         with  error code as %d\n",
                              i4FilterNo, u4ErrCode);
                return (CLI_FAILURE);
            }

            if (u4IpType != ACL_HOST_IP)
            {
                /*Subnet Mask has been specified */
                if (nmhTestv2IssExtL3FilterSrcIpAddrMask (&u4ErrCode,
                                                          i4FilterNo,
                                                          u4IpMask) ==
                    SNMP_FAILURE)
                {
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to test L3 Filter Src IpMask for Filter ID %d                                         with  error code as %d\n",
                                  i4FilterNo, u4ErrCode);
                    return (CLI_FAILURE);
                }
            }
        }
    }
    else if (u1Type == ACL_DST)
    {
        if (u4IpType != ACL_ANY)
        {
            /* Destination IP address has been specified */
            if (nmhTestv2IssExtL3FilterDstIpAddr
                (&u4ErrCode, i4FilterNo, u4IpAddr) == SNMP_FAILURE)
            {
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to test L3 Filter Destination IpAddr for Filter ID %d                                         with  error code as %d\n",
                              i4FilterNo, u4ErrCode);
                return (CLI_FAILURE);
            }

            if (u4IpType != ACL_HOST_IP)
            {
                if (nmhTestv2IssExtL3FilterDstIpAddrMask
                    (&u4ErrCode, i4FilterNo, u4IpMask) == SNMP_FAILURE)
                {
                    ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to test L3 Filter Destination IpMask for Filter ID %d                                         with  error code as %d\n",
                                  i4FilterNo, u4ErrCode);
                    /*Subnet Mask has been specified */
                    return (CLI_FAILURE);
                }
            }
        }
    }
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclTestIpParams function \n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclSetIpParams                                     */
/*                                                                           */
/*     DESCRIPTION      : This function sets the IP address, mask for a      */
/*                        filter                                             */
/*                                                                           */
/*     INPUT            : CliHandle - CLI HAndler                            */
/*                        u1Type - whether Source/Destination Ip address     */
/*                        information must                                   */
/*                        be set                                             */
/*                        i4FilterNo - IP Filter Number                      */
/*                        i4IpType - Whether IPaddress and/or mask is user   */
/*                        input                                              */
/*                        u4IpAddr - Ip Address to be set                    */
/*                        u4IpMask - Ip Maxk to be set                       */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/

INT4
AclSetIpParams (UINT1 u1Type, INT4 i4FilterNo, UINT4 u4IpType,
                UINT4 u4IpAddr, UINT4 u4IpMask)
{
    UINT4               u4IpSubnet;
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclSetIpParams function \n");
    if (u1Type == ACL_SRC)
    {
        if (u4IpType != ACL_ANY)
        {
            /* Source IP address has been specified */
            if (u4IpType != ACL_HOST_IP)
            {
                u4IpSubnet = (u4IpAddr & u4IpMask);

                if (nmhSetIssExtL3FilterSrcIpAddr (i4FilterNo, u4IpSubnet)
                    == SNMP_FAILURE)
                {
                    ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to set L3 Filter Source IP Subnet for filter Id %d\n",
                                  i4FilterNo);
                    return (CLI_FAILURE);
                }
                /*Subnet Mask has been specified */
                if (nmhSetIssExtL3FilterSrcIpAddrMask (i4FilterNo,
                                                       u4IpMask) ==
                    SNMP_FAILURE)
                {
                    ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to set L3 Filter Source IP address mask for filter Id %d\n",
                                  i4FilterNo);
                    return (CLI_FAILURE);
                }
            }
            else
            {
                if (nmhSetIssExtL3FilterSrcIpAddr (i4FilterNo, u4IpAddr)
                    == SNMP_FAILURE)
                {
                    ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to set L3 Filter  Source IP address for filter Id %d\n",
                                  i4FilterNo);
                    return (CLI_FAILURE);
                }
                if (nmhSetIssExtL3FilterSrcIpAddrMask (i4FilterNo,
                                                       (UINT4)
                                                       CLI_INET_ADDR
                                                       (HOST_MASK)) ==
                    SNMP_FAILURE)
                {
                    ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to set L3 Filter Source IP address  mask  for filter Id %d\n",
                                  i4FilterNo);
                    return (CLI_FAILURE);
                }
            }
        }
    }
    else if (u1Type == ACL_DST)
    {
        if (u4IpType != ACL_ANY)
        {
            /* Destination IP address has been specified */
            if (u4IpType != ACL_HOST_IP)
            {
                u4IpSubnet = (u4IpAddr & u4IpMask);

                if (nmhSetIssExtL3FilterDstIpAddr (i4FilterNo, u4IpSubnet)
                    == SNMP_FAILURE)
                {
                    ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to set L3 Filter Destination IP Subnet for filter Id %d\n",
                                  i4FilterNo);
                    return (CLI_FAILURE);
                }
                if (nmhSetIssExtL3FilterDstIpAddrMask (i4FilterNo,
                                                       u4IpMask) ==
                    SNMP_FAILURE)
                {
                    ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to set L3 Filter Destination IP address mask for filter Id %d\n",
                                  i4FilterNo);
                    /*Subnet Mask has been specified */
                    return (CLI_FAILURE);
                }
            }
            else
            {
                if (nmhSetIssExtL3FilterDstIpAddr (i4FilterNo, u4IpAddr)
                    == SNMP_FAILURE)
                {
                    ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to set L3 Filter  Destination IP address for filter Id %d\n",
                                  i4FilterNo);
                    return (CLI_FAILURE);
                }
                if (nmhSetIssExtL3FilterDstIpAddrMask (i4FilterNo,
                                                       (UINT4)
                                                       CLI_INET_ADDR
                                                       (HOST_MASK)) ==
                    SNMP_FAILURE)
                {
                    ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed to set L3 Filter Destination IP address  mask  for filter Id %d\n",
                                  i4FilterNo);
                    return (CLI_FAILURE);
                }
            }

        }
    }
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclSetIpParams function \n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclCliPrintPortList                                */
/*                                                                           */
/*     DESCRIPTION      : This function displays the Port list               */
/*                                                                           */
/*     INPUT            :  piIfName - Port Name                              */
/*                         CliHandle-CLI Handler                             */
/*                         i4CommaCount- position of port in port list       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

VOID
AclCliPrintPortList (tCliHandle CliHandle, INT4 i4CommaCount, UINT1 *piIfName)
{
    INT4                i4Times;
    INT1                i1Loop;

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclCliPrintPortList function \n");
    i4Times = ISS_PORTLIST_LEN * 2;

    if (i4CommaCount == 1)
    {
        CliPrintf (CliHandle, "%s ", piIfName);
        ISS_TRC (INIT_SHUT_TRC,
                 "\nCLI:Exit AclCliPrintPortList function in i4CommaCount \n");
        return;
    }
    for (i1Loop = 1; i1Loop < i4Times; i1Loop++)
    {
        /* Only 4 ports per line will be displayed */
        if (i4CommaCount <= (4 * i1Loop))
        {
            if (i4CommaCount == (4 * (i1Loop - 1)) + 1)
            {
                CliPrintf (CliHandle, " \r\n%-33s", " ");
            }
            CliPrintf (CliHandle, " , %s", piIfName);
            break;
        }
    }
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclCliPrintPortList function \n");
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : AclIPv6GetCfgPrompt                                */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Class map prompt in pi1DispStr if  */
/*                        valid.                                             */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/
INT1
AclIPv6GetCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = 0;
    INT4                i4IPv6AclIndex = 0;

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclIPv6GetCfgPrompt function \n");
    if (pi1DispStr == NULL)
    {
        return FALSE;
    }

    /* NULL is passed to return "config-ipv6-acl" as the prompt
       for the mode  */
    if (pi1ModeName == NULL)
    {
        STRCPY (pi1DispStr, "config-ipv6-acl");
        return TRUE;
    }

    u4Len = STRLEN (CLI_IPV6ACL_MODE);
    if (STRNCMP (pi1ModeName, CLI_IPV6ACL_MODE, u4Len) != 0)
    {
        return FALSE;
    }

    pi1ModeName = pi1ModeName + u4Len;

    i4IPv6AclIndex = CLI_ATOI (pi1ModeName);

    /* Set the mode information for CLI */
    CLI_SET_IPV6ACL (i4IPv6AclIndex);
    STRCPY (pi1DispStr, "(config-ipv6-acl)#");
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclIPv6GetCfgPrompt function \n");
    return TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : UserDefAclGetCfgPrompt                             */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Class map prompt in pi1DispStr if  */
/*                        valid.                                             */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/

INT1
UserDefAclGetCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;
    INT4                i4UserDefAclIndex;

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry UserDefAclGetCfgPrompt function \n");
    if (!pi1DispStr)
    {
        return FALSE;
    }

    /* NULL is passed to return "config-std-nacl" as the prompt 
     * for the mode  */
    if (pi1ModeName == NULL)
    {
        STRCPY (pi1DispStr, "config-userdef-acl");
        return TRUE;
    }

    u4Len = STRLEN (CLI_USRDEFACL_MODE);
    if (STRNCMP (pi1ModeName, CLI_USRDEFACL_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    i4UserDefAclIndex = CLI_ATOI (pi1ModeName);

    /* Set the mode information for CLI */
    CLI_SET_USERDEFFACL (i4UserDefAclIndex);

    STRCPY (pi1DispStr, "(config-userdef-acl)#");
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit UserDefAclGetCfgPrompt function \n");
    return TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclRedirectFilterConfig                 */
/*                                                                           */
/*     DESCRIPTION      : This function displays the destination Port        */
/*               for redirection                     */
/*                                                                           */
/*     INPUT            :  piIfName - Port Name                              */
/*                         CliHandle-CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
AclRedirectFilterConfig (tCliHandle CliHandle, INT4 i4Action, INT4 i4IsPortList,
                         INT1 *pi1IfName0, INT1 *pi1IfListStr0)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    tSNMP_OCTET_STRING_TYPE RedirectPortGroup;
    UINT4               u4NewRedirectGrpIndex = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4IfIndex = 0x0;
    INT4                i4FilterNo = 0;
    UINT1               au1PortArray[ISS_PORTLIST_LEN];

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclRedirectFilterConfig function \n");
    ISS_MEMSET (&RedirectPortGroup, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    ISS_MEMSET (&au1PortArray, 0, ISS_PORTLIST_LEN);

    RedirectPortGroup.pu1_OctetList = &au1PortArray[0];
    if (i4IsPortList == ISS_REDIRECT_TO_PORT)
    {
        if (pi1IfListStr0 != NULL)
        {
            if (CfaCliGetIfIndex (pi1IfName0, pi1IfListStr0, &u4IfIndex) ==
                CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Port Channel does Not Exist\r\n");
                return CLI_FAILURE;
            }

        }
        RedirectPortGroup.pu1_OctetList = (UINT1 *) &u4IfIndex;
        RedirectPortGroup.i4_Length = ISS_PORTLIST_LEN;
    }
    i4FilterNo = CLI_GET_STDACLID ();

    /* Step 1: Get the Corresponding L2/L3/UDB Filter Status and
     * see if the This Filter is already matched to some
     * Redirect Group. If Not Go to Step 2 */

    /* Step 2: Run Through save the lowest free Index
     * in the case match succeeds use that index else use lowest
     * free Index */

    /*Step 3: If the Filter is newly attached then Set the Parameters
     * of the for new Group . This is done by first setting the
     * Row Status as Create And Wait and then moving to ACtive */

    /* Step 4: If the filter is already matches the Redirect Group
     * then See if any of the Parameter has got changed If So
     * delete all the filters using (NOT_IN_SERVICE) and then used
     * newly formed parametrs and set the filters again by moving
     * to Active */

    /* Please Note : Currently Comaprison of Value
     * is not being done. Need to be taken care. */

    nmhGetIssRedirectInterfaceGrpIdNextFree (&u4NewRedirectGrpIndex);
    if (u4NewRedirectGrpIndex == 0)
    {
        CliPrintf (CliHandle,
                   "\r%% No More Free Index For Redirect Group Id  \r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2IssRedirectInterfaceGrpStatus
        (&u4ErrCode, u4NewRedirectGrpIndex,
         ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test Redirect group index%d with  error code as %d\n",
                      u4NewRedirectGrpIndex, u4ErrCode);
        return CLI_FAILURE;
    }

    if (nmhSetIssRedirectInterfaceGrpStatus
        (u4NewRedirectGrpIndex, ISS_CREATE_AND_WAIT) == SNMP_FAILURE)

    {
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to set Redirect group index %d\n",
                      u4NewRedirectGrpIndex);
        return CLI_FAILURE;
    }
    if (nmhTestv2IssRedirectInterfaceGrpFilterId
        (&u4ErrCode, u4NewRedirectGrpIndex, i4FilterNo) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test Redirect group index %d for filter ID %d with  error code  as %d\n",
                      u4NewRedirectGrpIndex, i4FilterNo, u4ErrCode);
        return CLI_FAILURE;
    }

    if (nmhSetIssRedirectInterfaceGrpFilterId
        (u4NewRedirectGrpIndex, i4FilterNo) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to set Redirect group index %d for filter ID %d\n",
                      u4NewRedirectGrpIndex, i4FilterNo);
        return CLI_FAILURE;
    }
    if (nmhTestv2IssRedirectInterfaceGrpFilterType
        (&u4ErrCode, u4NewRedirectGrpIndex, i4Action) == SNMP_FAILURE)

    {
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test Redirect group action %d for Redirect index %d with  error code  as %d\n",
                      i4Action, u4NewRedirectGrpIndex, u4ErrCode);
        return CLI_FAILURE;
    }

    if (nmhSetIssRedirectInterfaceGrpFilterType
        (u4NewRedirectGrpIndex, i4Action) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to set Redirect group action %d for Redirect index %d \n",
                      i4Action, u4ErrCode);
        return CLI_FAILURE;
    }
    if (nmhTestv2IssRedirectInterfaceGrpType
        (&u4ErrCode, u4NewRedirectGrpIndex, i4IsPortList) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test Redirect group  index %d with  error code  as %d\n",
                      u4NewRedirectGrpIndex, u4ErrCode);
        return CLI_FAILURE;
    }

    if (nmhSetIssRedirectInterfaceGrpType
        (u4NewRedirectGrpIndex, i4IsPortList) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to set Redirect group  index %d\n",
                      u4NewRedirectGrpIndex);
        return CLI_FAILURE;
    }
    if (nmhTestv2IssRedirectInterfaceGrpPortList
        (&u4ErrCode, u4NewRedirectGrpIndex, &RedirectPortGroup) == SNMP_FAILURE)

    {
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test Redirect port group for Redirect index %d with  error code  as %d\n",
                      u4NewRedirectGrpIndex, u4ErrCode);
        return CLI_FAILURE;
    }

    if (nmhSetIssRedirectInterfaceGrpPortList
        (u4NewRedirectGrpIndex, &RedirectPortGroup) == SNMP_FAILURE)

    {
        CLI_FATAL_ERROR (CliHandle);
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to set Redirect port group for Redirect index %d \n",
                      u4NewRedirectGrpIndex);
        return CLI_FAILURE;
    }

    if (i4Action == ISS_L2_REDIRECT)
    {
        pIssL2FilterEntry = IssExtGetL2FilterEntry (i4FilterNo);
        pIssL2FilterEntry->RedirectIfGrp.u4RedirectGrpId =
            u4NewRedirectGrpIndex;
    }
    else if (i4Action == ISS_L3_REDIRECT)
    {
        pIssL3FilterEntry = IssExtGetL3FilterEntry (i4FilterNo);
        pIssL3FilterEntry->RedirectIfGrp.u4RedirectGrpId =
            u4NewRedirectGrpIndex;
    }
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclRedirectFilterConfig function \n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclResetL3Filter                                   */
/*                                                                           */
/*     DESCRIPTION      : This function resets the L3 Filter Parameters      */
/*                                                                           */
/*     INPUT            :  i4FilterNo - Filter No                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
AclResetL3Filter (INT4 i4FilterNo)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4FilterNo);
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclResetL3Filter function \n");
    if (pIssAclL3FilterEntry != NULL)
    {
        pIssAclL3FilterEntry->i4IssL3FilterNo = i4FilterNo;
        pIssAclL3FilterEntry->i4IssL3FilterPriority =
            ISS_DEFAULT_FILTER_PRIORITY;
        pIssAclL3FilterEntry->IssL3FilterProtocol = ISS_IP_PROTO_DEF;
        pIssAclL3FilterEntry->u4SVlanId = 0;
        pIssAclL3FilterEntry->i1SVlanPriority = ISS_DEFAULT_VLAN_PRIORITY;
        pIssAclL3FilterEntry->u4CVlanId = 0;
        pIssAclL3FilterEntry->i1CVlanPriority = ISS_DEFAULT_VLAN_PRIORITY;
        pIssAclL3FilterEntry->u1IssL3FilterTagType = ISS_FILTER_SINGLE_TAG;
        pIssAclL3FilterEntry->i4IssL3FilterMessageType = ISS_DEFAULT_MSG_TYPE;
        pIssAclL3FilterEntry->i4IssL3FilterMessageCode = ISS_DEFAULT_MSG_CODE;
        pIssAclL3FilterEntry->u4IssL3FilterDstIpAddr = ISS_ZERO_ENTRY;
        pIssAclL3FilterEntry->u4IssL3FilterSrcIpAddr = ISS_ZERO_ENTRY;
        pIssAclL3FilterEntry->u4IssL3FilterDstIpAddrMask = ISS_DEF_FILTER_MASK;
        pIssAclL3FilterEntry->u4IssL3FilterSrcIpAddrMask = ISS_DEF_FILTER_MASK;
        pIssAclL3FilterEntry->u4IssL3FilterMinDstProtPort = ISS_ZERO_ENTRY;
        pIssAclL3FilterEntry->u4IssL3FilterMaxDstProtPort = ISS_MAX_PORT_VALUE;
        pIssAclL3FilterEntry->u4IssL3FilterMinSrcProtPort = ISS_ZERO_ENTRY;
        pIssAclL3FilterEntry->u4IssL3FilterMaxSrcProtPort = ISS_MAX_PORT_VALUE;
        pIssAclL3FilterEntry->IssL3FilterAckBit = ISS_ACK_ANY;
        pIssAclL3FilterEntry->IssL3FilterRstBit = ISS_RST_ANY;
        pIssAclL3FilterEntry->IssL3FilterTos = ISS_TOS_INVALID;
        pIssAclL3FilterEntry->i4IssL3FilterDscp = ISS_DSCP_INVALID;
        pIssAclL3FilterEntry->IssL3FilterDirection = ISS_DIRECTION_IN;
        pIssAclL3FilterEntry->IssL3FilterAction = ISS_ALLOW;
        pIssAclL3FilterEntry->u4IssL3FilterMatchCount = ISS_ZERO_ENTRY;
        pIssAclL3FilterEntry->u4IssL3FilterMultiFieldClfrDstPrefixLength =
            ISS_ZERO_ENTRY;
        pIssAclL3FilterEntry->u4IssL3FilterMultiFieldClfrSrcPrefixLength =
            ISS_ZERO_ENTRY;
        pIssAclL3FilterEntry->u4IssL3MultiFieldClfrFlowId = ISS_ZERO_ENTRY;
        pIssAclL3FilterEntry->i4IssL3MultiFieldClfrAddrType = QOS_IPV4;
        pIssAclL3FilterEntry->u4RefCount = 0;
        pIssAclL3FilterEntry->u1IssL3FilterStatus = 0;
        pIssAclL3FilterEntry->u1IssL3FilterCreationMode
            = ISS_ACL_CREATION_EXTERNAL;
        pIssAclL3FilterEntry->u4IssL3RedirectId = 0;
    }
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclResetL3Filter function \n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclResetL3FilterQualifiers                 */
/*                                                                           */
/*     DESCRIPTION      : This function resets the optional fields of the    */
/*                           commands related to L3 Filter                         */
/*                                                                           */
/*     INPUT            :  i4FilterNo - Filter No                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
AclResetL3FilterQualifiers (INT4 i4FilterNo)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4FilterNo);
    ISS_TRC (INIT_SHUT_TRC,
             "\nCLI:Entry AclResetL3FilterQualifiers function \n");
    if (pIssAclL3FilterEntry != NULL)
    {
        pIssAclL3FilterEntry->i4IssL3FilterNo = i4FilterNo;
        pIssAclL3FilterEntry->i4IssL3FilterPriority =
            ISS_DEFAULT_FILTER_PRIORITY;
        pIssAclL3FilterEntry->IssL3FilterProtocol = ISS_IP_PROTO_DEF;
        pIssAclL3FilterEntry->u4SVlanId = 0;
        pIssAclL3FilterEntry->i1SVlanPriority = ISS_DEFAULT_VLAN_PRIORITY;
        pIssAclL3FilterEntry->u4CVlanId = 0;
        pIssAclL3FilterEntry->i1CVlanPriority = ISS_DEFAULT_VLAN_PRIORITY;
        pIssAclL3FilterEntry->u1IssL3FilterTagType = ISS_FILTER_SINGLE_TAG;
        pIssAclL3FilterEntry->i4IssL3FilterMessageType = ISS_DEFAULT_MSG_TYPE;
        pIssAclL3FilterEntry->i4IssL3FilterMessageCode = ISS_DEFAULT_MSG_CODE;
        pIssAclL3FilterEntry->u4IssL3FilterDstIpAddr = ISS_ZERO_ENTRY;
        pIssAclL3FilterEntry->u4IssL3FilterSrcIpAddr = ISS_ZERO_ENTRY;
        pIssAclL3FilterEntry->u4IssL3FilterDstIpAddrMask = ISS_DEF_FILTER_MASK;
        pIssAclL3FilterEntry->u4IssL3FilterSrcIpAddrMask = ISS_DEF_FILTER_MASK;
        pIssAclL3FilterEntry->u4IssL3FilterMinDstProtPort = ISS_ZERO_ENTRY;
        pIssAclL3FilterEntry->u4IssL3FilterMaxDstProtPort = ISS_MAX_PORT_VALUE;
        pIssAclL3FilterEntry->u4IssL3FilterMinSrcProtPort = ISS_ZERO_ENTRY;
        pIssAclL3FilterEntry->u4IssL3FilterMaxSrcProtPort = ISS_MAX_PORT_VALUE;
        pIssAclL3FilterEntry->IssL3FilterAckBit = ISS_ACK_ANY;
        pIssAclL3FilterEntry->IssL3FilterRstBit = ISS_ACK_ANY;
        pIssAclL3FilterEntry->IssL3FilterTos = ISS_TOS_INVALID;
        pIssAclL3FilterEntry->i4IssL3FilterDscp = ISS_DSCP_INVALID;
        pIssAclL3FilterEntry->u4IssL3FilterMultiFieldClfrDstPrefixLength =
            ISS_ZERO_ENTRY;
        pIssAclL3FilterEntry->u4IssL3FilterMultiFieldClfrSrcPrefixLength =
            ISS_ZERO_ENTRY;
        pIssAclL3FilterEntry->u4IssL3MultiFieldClfrFlowId = ISS_ZERO_ENTRY;
        pIssAclL3FilterEntry->i4IssL3MultiFieldClfrAddrType = QOS_IPV4;
        pIssAclL3FilterEntry->u1IssL3FilterCreationMode
            = ISS_ACL_CREATION_EXTERNAL;
        pIssAclL3FilterEntry->u4IssL3RedirectId = 0;
        MEMSET (&(pIssAclL3FilterEntry->ipv6SrcIpAddress), 0,
                sizeof (tIp6Addr));
        MEMSET (&(pIssAclL3FilterEntry->ipv6DstIpAddress), 0,
                sizeof (tIp6Addr));
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nCLI:Exit AclResetL3FilterQualifiers function \n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclResetL2Filter                                   */
/*                                                                           */
/*     DESCRIPTION      : This function resets the L2 Filter Parameters      */
/*                                                                           */
/*     INPUT            :  i4FilterNo - Filter No                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
AclResetL2Filter (INT4 i4FilterNo)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4FilterNo);
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclResetL2Filter function \n");
    if (pIssAclL2FilterEntry != NULL)
    {
        pIssAclL2FilterEntry->i4IssL2FilterNo = i4FilterNo;
        pIssAclL2FilterEntry->i4IssL2FilterPriority =
            ISS_DEFAULT_FILTER_PRIORITY;
        pIssAclL2FilterEntry->u4IssL2FilterProtocolType =
            ISS_DEFAULT_PROTOCOL_TYPE;
        pIssAclL2FilterEntry->i1IssL2FilterCVlanPriority =
            ISS_DEFAULT_VLAN_PRIORITY;
        pIssAclL2FilterEntry->i1IssL2FilterSVlanPriority =
            ISS_DEFAULT_VLAN_PRIORITY;
        pIssAclL2FilterEntry->u4IssL2FilterCustomerVlanId = 0;
        pIssAclL2FilterEntry->u4IssL2FilterServiceVlanId = 0;
        pIssAclL2FilterEntry->u2InnerEtherType = 0;
        pIssAclL2FilterEntry->u2OuterEtherType = 0;
        pIssAclL2FilterEntry->u1IssL2FilterTagType = ISS_FILTER_SINGLE_TAG;
        pIssAclL2FilterEntry->u1FilterDirection = ISS_DIRECTION_IN;
        pIssAclL2FilterEntry->IssL2FilterAction = ISS_ALLOW;
        pIssAclL2FilterEntry->u4IssL2FilterMatchCount = 0;
        pIssAclL2FilterEntry->u1IssL2FilterStatus = 0;
        pIssAclL2FilterEntry->u1IssL2FilterCreationMode
            = ISS_ACL_CREATION_EXTERNAL;
        pIssAclL2FilterEntry->u4IssL2RedirectId = 0;
    }
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclResetL2Filter function \n");
    return CLI_SUCCESS;
}

/*****************************************************************************
**                                                                           
**     FUNCTION NAME    : AclIpStatsSetEnabledStatus
**                                                                           
**     DESCRIPTION      : This function set tests and sets the status of IP
**                          ACL Counter.
**                                                                           
**     INPUT            : CliHandle - Handler for CLI
**                        i4FilterNo - IP Filter Number
**                        i4Stats - can be ACL_STAT_ENABLE or ACL_STAT_DISABLE
**                                                                           
**     OUTPUT           : NONE                                               
**                                                                           
**     RETURNS          : CLI_SUCCESS or CLI_FAILURE
**                                                                           
*****************************************************************************/

INT4
AclIpStatsSetEnabledStatus (tCliHandle CliHandle, INT4 i4FilterNo, INT4 i4Stats)
{
    UINT4               u4ErrCode = SNMP_ERR_NO_ERROR;

    ISS_TRC (INIT_SHUT_TRC,
             "\nCLI:Entry AclIpStatsSetEnabledStatus function \n");

    if ((nmhTestv2IssAclL3FilterStatsEnabledStatus
         (&u4ErrCode, i4FilterNo, i4Stats)) == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test L3 Filter Statistics Status %d for Filter ID %d with error code as %d\n",
                      i4Stats, i4FilterNo, u4ErrCode);
        return CLI_FAILURE;
    }

    if ((nmhSetIssAclL3FilterStatsEnabledStatus (i4FilterNo, i4Stats)) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% IP Filter Statistics Status not modified \r\n");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Set L3 Filter Statistics Status %d for Filter ID %d\n",
                      i4Stats, i4FilterNo);
        return CLI_FAILURE;
    }

    ISS_TRC (INIT_SHUT_TRC,
             "\nCLI:Exit AclIpStatsSetEnabledStatus function \n");
    return CLI_SUCCESS;
}

/*****************************************************************************
**                                                                           
**     FUNCTION NAME    : AclMacStatsSetEnabledStatus
**                                                                           
**     DESCRIPTION      : This function set tests and sets the status of MAC
**                          ACL Counter.
**                                                                           
**     INPUT            : CliHandle - Handler for CLI
**                        i4FilterNo - MAC Filter Number
**                        i4Stats - can be ACL_STAT_ENABLE or ACL_STAT_DISABLE
**                                                                           
**     OUTPUT           : NONE                                               
**                                                                           
**     RETURNS          : CLI_SUCCESS or CLI_FAILURE
**                                                                           
*****************************************************************************/

INT4
AclMacStatsSetEnabledStatus (tCliHandle CliHandle, INT4 i4FilterNo,
                             INT4 i4Stats)
{
    UINT4               u4ErrCode = SNMP_ERR_NO_ERROR;

    ISS_TRC (INIT_SHUT_TRC,
             "\nCLI:Entry AclMacStatsSetEnabledStatus function \n");

    if ((nmhTestv2IssAclL2FilterStatsEnabledStatus
         (&u4ErrCode, i4FilterNo, i4Stats)) == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test L2 Filter Statistics Status %d for Filter ID %d with error code as %d\n",
                      i4Stats, i4FilterNo, u4ErrCode);
        return CLI_FAILURE;
    }

    if ((nmhSetIssAclL2FilterStatsEnabledStatus (i4FilterNo, i4Stats)) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% MAC Filter Statistics Status not modified \r\n");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Set L2 Filter Statistics Status %d for Filter ID %d\n",
                      i4Stats, i4FilterNo);
        return CLI_FAILURE;
    }

    ISS_TRC (INIT_SHUT_TRC,
             "\nCLI:Exit AclMacStatsSetEnabledStatus function \n");
    return CLI_SUCCESS;
}

/*****************************************************************************
**                                                                           
**     FUNCTION NAME    : AclUserDefinedStatsSetEnabledStatus
**                                                                           
**     DESCRIPTION      : This function set tests and sets the status of User-
**                          Defined ACL Counter.
**                                                                           
**     INPUT            : CliHandle - Handler for CLI
**                        i4FilterNo - User-Defined Filter Number
**                        i4Stats - can be ACL_STAT_ENABLE or ACL_STAT_DISABLE
**                                                                           
**     OUTPUT           : NONE                                               
**                                                                           
**     RETURNS          : CLI_SUCCESS or CLI_FAILURE
**                                                                           
*****************************************************************************/

INT4
AclUserDefinedStatsSetEnabledStatus (tCliHandle CliHandle, INT4 i4FilterNo,
                                     INT4 i4Stats)
{
    UINT4               u4ErrCode = SNMP_ERR_NO_ERROR;

    ISS_TRC (INIT_SHUT_TRC,
             "\nCLI:Entry AclUserDefinedStatsSetEnabledStatus function \n");

    if ((nmhTestv2IssAclUserDefinedFilterStatsEnabledStatus
         (&u4ErrCode, (UINT4) i4FilterNo, i4Stats)) == SNMP_FAILURE)
    {
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to test Udb Filter Statistics Status %d for Filter ID %d with error code as %d\n",
                      i4Stats, i4FilterNo, u4ErrCode);
        return CLI_FAILURE;
    }

    if ((nmhSetIssAclUserDefinedFilterStatsEnabledStatus
         ((UINT4) i4FilterNo, i4Stats)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% UDB Filter Statistics Status not modified \r\n");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nCLI:Failed to Set Udb Filter Statistics Status %d for Filter ID %d\n",
                      i4Stats, i4FilterNo);
        return CLI_FAILURE;
    }

    ISS_TRC (INIT_SHUT_TRC,
             "\nCLI:Exit AclUserDefinedStatsSetEnabledStatus function \n");
    return CLI_SUCCESS;
}

/*****************************************************************************
**                                                                           
**     FUNCTION NAME    : AclIpStatsClear
**                                                                           
**     DESCRIPTION      : This function clears the match count of IP ACL
**                          Counter.
**                                                                           
**     INPUT            : CliHandle - Handler for CLI
**                        i4FilterNo - IP Filter Number
**                                                                           
**     OUTPUT           : NONE                                               
**                                                                           
**     RETURNS          : CLI_SUCCESS or CLI_FAILURE
**                                                                           
*****************************************************************************/

INT4
AclIpStatsClear (tCliHandle CliHandle, INT4 i4FilterNo)
{
    INT4                i4StatsOld = ACL_STAT_DISABLE;
    INT4                i4Status = ISS_NOT_IN_SERVICE;
    INT4                i4CurrentFilter = 0;
    UINT4               u4ErrCode = SNMP_ERR_NO_ERROR;
    UINT1               u1Flag = 1;

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclIpStatsClear function \n");

    if (i4FilterNo == 0)
    {
        /* The boolean u1Flag is used to specify whether the operation
         * needs to be performed for all filters or for specific filter
         * only.
         */

        u1Flag = 0;

        if (nmhGetFirstIndexIssExtL3FilterTable (&i4FilterNo) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% No IP ACLs have been configured\r\n");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to Retrieve L3 Filter ID %d\n",
                          i4FilterNo);
            return (CLI_SUCCESS);
        }
    }

    do
    {
        i4CurrentFilter = i4FilterNo;
        if ((nmhGetIssExtL3FilterStatus (i4FilterNo, &i4Status)) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Invalid IP Filter \r\n");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to Retrieve L3 Filter Status %d for Filter ID %d\n",
                          i4Status, i4FilterNo);
            continue;
        }
        if ((nmhGetIssAclL3FilterStatsEnabledStatus
             (i4FilterNo, &i4StatsOld)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Invalid IP Filter Statistics Status \r\n");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to Retrieve L3 Filter Statistics Status %d for Filter ID %d\n",
                          i4StatsOld, i4FilterNo);
            continue;
        }

        if (i4StatsOld == ACL_STAT_ENABLE)
        {
            if (i4Status != ISS_ACTIVE)
            {
                CLI_SET_ERR (CLI_ACL_FILTER_NOT_ACTIVE);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:L3 Filter Status is not active for Filter ID %d\n",
                              i4FilterNo);
                continue;
            }
            if ((nmhTestv2IssAclClearL3FilterStats
                 (&u4ErrCode, i4FilterNo, ISS_TRUE)) == SNMP_FAILURE)
            {
                ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to test L3 Filter Clear Statistics Status %d for Filter ID %d with error code as %d\n",
                              ISS_TRUE, i4FilterNo, u4ErrCode);
                continue;
            }
            if ((nmhSetIssAclClearL3FilterStats (i4FilterNo, ISS_TRUE)) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to Set L3 Filter Clear Statistics Status %d for Filter ID %d\n",
                              ISS_TRUE, i4FilterNo);
                continue;
            }
        }
    }
    while ((nmhGetNextIndexIssExtL3FilterTable
            (i4CurrentFilter, &i4FilterNo) == SNMP_SUCCESS) && (u1Flag == 0));

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclIpStatsClear function \n");
    return (CLI_SUCCESS);
}

/*****************************************************************************
**                                                                           
**     FUNCTION NAME    : AclMacStatsClear
**                                                                           
**     DESCRIPTION      : This function clears the match count of MAC ACL
**                          Counter.
**                                                                           
**     INPUT            : CliHandle - Handler for CLI
**                        i4FilterNo - MAC Filter Number
**                                                                           
**     OUTPUT           : NONE                                               
**                                                                           
**     RETURNS          : CLI_SUCCESS or CLI_FAILURE
**                                                                           
*****************************************************************************/

INT4
AclMacStatsClear (tCliHandle CliHandle, INT4 i4FilterNo)
{
    INT4                i4StatsOld = ACL_STAT_DISABLE;
    INT4                i4Status = ISS_NOT_IN_SERVICE;
    INT4                i4CurrentFilter = 0;
    UINT4               u4ErrCode = SNMP_ERR_NO_ERROR;
    UINT1               u1Flag = 1;

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclMacStatsClear function \n");

    if (i4FilterNo == 0)
    {
        /* The boolean u1Flag is used to specify whether the operation
         * needs to be performed for all filters or for specific filter
         * only.
         */

        u1Flag = 0;

        if (nmhGetFirstIndexIssExtL2FilterTable (&i4FilterNo) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% No MAC ACLs have been configured\r\n");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to Retrieve L2 Filter ID %d\n",
                          i4FilterNo);
            return (CLI_SUCCESS);
        }
    }

    do
    {
        i4CurrentFilter = i4FilterNo;
        if ((nmhGetIssExtL2FilterStatus (i4FilterNo, &i4Status)) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Invalid MAC Filter \r\n");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to Retrieve L2 Filter Status %d for Filter ID %d\n",
                          i4Status, i4FilterNo);
            continue;
        }
        if ((nmhGetIssAclL2FilterStatsEnabledStatus
             (i4FilterNo, &i4StatsOld)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Invalid MAC Filter Statistics Status \r\n");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to Retrieve L2 Filter Statistics Status %d for Filter ID %d\n",
                          i4StatsOld, i4FilterNo);
            continue;
        }

        if (i4StatsOld == ACL_STAT_ENABLE)
        {
            if (i4Status != ISS_ACTIVE)
            {
                CLI_SET_ERR (CLI_ACL_FILTER_NOT_ACTIVE);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:L2 Filter Status is not active for Filter ID %d\n",
                              i4FilterNo);
                continue;
            }
            if ((nmhTestv2IssAclClearL2FilterStats
                 (&u4ErrCode, i4FilterNo, ISS_TRUE)) == SNMP_FAILURE)
            {
                ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to test L2 Filter Clear Statistics Status %d for Filter ID %d with error code as %d\n",
                              ISS_TRUE, i4FilterNo, u4ErrCode);
                continue;
            }
            if ((nmhSetIssAclClearL2FilterStats (i4FilterNo, ISS_TRUE)) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to Set L2 Filter Clear Statistics Status %d for Filter ID %d\n",
                              ISS_TRUE, i4FilterNo);
                continue;
            }
        }
    }
    while ((nmhGetNextIndexIssExtL2FilterTable
            (i4CurrentFilter, &i4FilterNo) == SNMP_SUCCESS) && (u1Flag == 0));

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclMacStatsClear function \n");
    return (CLI_SUCCESS);
}

/*****************************************************************************
**                                                                           
**     FUNCTION NAME    : AclUserDefinedStatsClear
**                                                                           
**     DESCRIPTION      : This function clears the match count of User-
**                          Defined ACL Counter.
**                                                                           
**     INPUT            : CliHandle - Handler for CLI
**                        i4FilterNo - User-Defined Filter Number
**                                                                           
**     OUTPUT           : NONE                                               
**                                                                           
**     RETURNS          : CLI_SUCCESS or CLI_FAILURE
**                                                                           
*****************************************************************************/

INT4
AclUserDefinedStatsClear (tCliHandle CliHandle, INT4 i4FilterNo)
{
    INT4                i4StatsOld = ACL_STAT_DISABLE;
    INT4                i4Status = ISS_NOT_IN_SERVICE;
    INT4                i4CurrentFilter = 0;
    UINT4               u4ErrCode = SNMP_ERR_NO_ERROR;
    UINT1               u1Flag = 1;

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclUserDefinedStatsClear function \n");

    if (i4FilterNo == 0)
    {
        /* The boolean u1Flag is used to specify whether the operation
         * needs to be performed for all filters or for specific filter
         * only.
         */

        u1Flag = 0;

        if (nmhGetFirstIndexIssAclUserDefinedFilterTable ((UINT4 *) &i4FilterNo)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% No User Defined ACLs have been configured\r\n");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to Retrieve Udb Filter ID %d\n",
                          i4FilterNo);
            return (CLI_SUCCESS);
        }
    }

    do
    {
        i4CurrentFilter = i4FilterNo;
        if ((nmhGetIssAclUserDefinedFilterStatus
             ((UINT4) i4FilterNo, &i4Status)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Invalid UDB Filter \r\n");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to Retrieve Udb Filter Status %d for Filter ID %d\n",
                          i4Status, i4FilterNo);
            continue;
        }
        if ((nmhGetIssAclUserDefinedFilterStatsEnabledStatus
             ((UINT4) i4FilterNo, &i4StatsOld)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Invalid UDB Filter Statistics Status \r\n");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to Retrieve Udb Filter Statistics Status %d for Filter ID %d\n",
                          i4StatsOld, i4FilterNo);
            continue;
        }

        if (i4StatsOld == ACL_STAT_ENABLE)
        {
            if (i4Status != ISS_ACTIVE)
            {
                CLI_SET_ERR (CLI_ACL_FILTER_NOT_ACTIVE);
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Udb Filter Status is not active for Filter ID %d\n",
                              i4FilterNo);
                continue;
            }
            if ((nmhTestv2IssAclClearUserDefinedFilterStats
                 (&u4ErrCode, (UINT4) i4FilterNo, ISS_TRUE)) == SNMP_FAILURE)
            {
                ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to test Udb Filter Clear Statistics Status %d for Filter ID %d with error code as %d\n",
                              ISS_TRUE, i4FilterNo, u4ErrCode);
                continue;
            }

            if ((nmhSetIssAclClearUserDefinedFilterStats
                 ((UINT4) i4FilterNo, ISS_TRUE)) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to Set Udb Filter Clear Statistics Status %d for Filter ID %d\n",
                              ISS_TRUE, i4FilterNo);
                continue;
            }
        }
    }
    while ((nmhGetNextIndexIssAclUserDefinedFilterTable
            ((UINT4) i4CurrentFilter, (UINT4 *) &i4FilterNo) == SNMP_SUCCESS)
           && (u1Flag == 0));

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclUserDefinedStatsClear function \n");
    return (CLI_SUCCESS);
}

/***************************************************************************
 *                                                                         *
 *     Function Name : AclCliMacToStr                                      *
 *                                                                         *
 *     Description   : This function converts the given mac address        *
 *                     in to a string of form aa:aa:aa:aa:aa:aa            *
 *                                                                         *
 *     Input(s)      : pMacAdrr   : Pointer to the Mac address value array *
 *                     pu1Temp    : Pointer to the converted mac address   *
 *                                  string.                                *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : NULL                                                *
 *                                                                         *
 ***************************************************************************/

VOID
AclCliMacToStr (UINT1 *pMacAddr, UINT1 *pu1Temp)
{

    UINT1               u1Byte;

    if (!(pMacAddr) || !(pu1Temp))
        return;

    for (u1Byte = 0; u1Byte < MAC_LEN; u1Byte++)
    {
        pu1Temp += SPRINTF ((CHR1 *) pu1Temp, "%02x:", *(pMacAddr + u1Byte));
    }
    SPRINTF ((CHR1 *) (pu1Temp - 1), " ");

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclReservFrameFilterConfig                         */
/*                                                                           */
/*     DESCRIPTION      : This function configures a standard IP ACL filter  */
/*                         and enters the corresponding configuration mode   */
/*                                                                           */
/*     INPUT            :  u4Action -Permit/Deny                             */
/*                         u4SrcType - any/source network/ source host       */
/*                         u4SrcIp - Source IP Address                       */
/*                         u4SrcMask - Source IP Mask                        */
/*                         u4DestType - any/dest network/dest host           */
/*                         u4DestIp - Dest IP Address                        */
/*                         u4DestMask - Dest IP Mask                         */
/*                         i4Priority - Priority value of filter             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
AclReservFrameFilterConfig (tCliHandle CliHandle, UINT4 u4Protocol,
                            tMacAddr MacAddr, UINT1 u1MacMask,
                            UINT1 u1ActionStatus, UINT1 u1ResrvFrameStatus)
{
    tIssReservFrmCtrlTable *pIssReservFrmCtrlInfo = NULL;
    tMacAddr            ZeroMacAddr;
    tMacAddr            CheckMacAddr;
    UINT4               u4ErrCode = 0;
    UINT4               u4NewRservFrmTableIndex = 0;
    UINT4               u4IsExistRservFrmTableIndex = 0;

    ISS_TRC (INIT_SHUT_TRC,
             "\nCLI:Enter AclReservFrameFilterConfig function \n");

    MEMSET (ZeroMacAddr, 0, sizeof (tMacAddr));
    MEMSET (CheckMacAddr, 0, sizeof (tMacAddr));

    if (u1ResrvFrameStatus == ISS_RESERV_FRAME_ADD)
    {

        /*checking whether the entry is already exsiting in our table */
        MEMCPY (CheckMacAddr, MacAddr, sizeof (tMacAddr));

        u4IsExistRservFrmTableIndex =
            AclCheckReservFrameEntry (u4Protocol, CheckMacAddr, u1MacMask);
        if (u4IsExistRservFrmTableIndex == 0)
        {
            u4NewRservFrmTableIndex =
                IssReservFramCtrlIdNextFree (u4NewRservFrmTableIndex);

            if (u4NewRservFrmTableIndex == 0)
            {
                CliPrintf (CliHandle,
                           "\r\n %%Maximum Reserve frame entries reached\r\n");
                return CLI_FAILURE;
            }

        }

        /*checking whether the entry is already exist in the control table
           with the same action */
        if (IssCheckResrvFrameCtrlEntry
            (u4Protocol, CheckMacAddr, (UINT4) u1MacMask,
             u1ActionStatus) == SNMP_SUCCESS)
        {
            /*Entry is alerady exist in the table */
            return CLI_SUCCESS;
        }

        pIssReservFrmCtrlInfo =
            GET_RESERVE_FRAME_CTRL_INFO (u4NewRservFrmTableIndex);
        if (u4IsExistRservFrmTableIndex == 0)
        {
            if (nmhTestv2IssReservedFrameCtrlStatus
                (&u4ErrCode, u4NewRservFrmTableIndex,
                 ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to test Reserv Frame Control Table index%d with  error code as %d\n",
                              u4NewRservFrmTableIndex, u4ErrCode);
                return CLI_FAILURE;
            }

            if (nmhSetIssReservedFrameCtrlStatus
                (u4NewRservFrmTableIndex, ISS_CREATE_AND_WAIT) == SNMP_FAILURE)

            {
                CLI_FATAL_ERROR (CliHandle);

                /*Clearing the entry in the Reserve frame table both control and dataplane */
                if (nmhSetIssReservedFrameCtrlStatus
                    (u4NewRservFrmTableIndex, ISS_DESTROY) == SNMP_FAILURE)
                {
                    ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed in clearing the Reserv frame  Control Table index %d\n",
                                  u4NewRservFrmTableIndex);
                }
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set Reserv frame  Control Table index %d\n",
                              u4NewRservFrmTableIndex);
                return CLI_FAILURE;
            }
            /* If the entry is not exist in the table, we will set the operational flag ADD */
            pIssReservFrmCtrlInfo =
                GET_RESERVE_FRAME_CTRL_INFO (u4NewRservFrmTableIndex);
            if (pIssReservFrmCtrlInfo != NULL)
            {
                pIssReservFrmCtrlInfo->u1OperationStatus = ISS_RESERV_FRAME_ADD;
            }
        }
        else
        {
            /*if the entry is already exist in the table,
             *assign the old entry to the new entry and do the configuration*/
            u4NewRservFrmTableIndex = u4IsExistRservFrmTableIndex;

            if (nmhTestv2IssReservedFrameCtrlStatus
                (&u4ErrCode, u4NewRservFrmTableIndex,
                 ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to test Reserv Frame Control Table index%d with  error code as %d\n",
                              u4NewRservFrmTableIndex, u4ErrCode);
                return CLI_FAILURE;
            }

            /*Flushing out the hardware entries */

            if (nmhSetIssReservedFrameCtrlStatus
                (u4NewRservFrmTableIndex, ISS_NOT_IN_SERVICE) == SNMP_FAILURE)

            {
                CLI_FATAL_ERROR (CliHandle);

                /*Clearing the entry in the Reserve frame table both control and dataplane */
                if (nmhSetIssReservedFrameCtrlStatus
                    (u4NewRservFrmTableIndex, ISS_DESTROY) == SNMP_FAILURE)
                {
                    ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed in clearing the Reserv frame  Control Table index %d\n",
                                  u4NewRservFrmTableIndex);
                }

                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set Reserv frame  Control Table index %d\n",
                              u4NewRservFrmTableIndex);
                return CLI_FAILURE;
            }

            /*If the entry is already exist, we will set the operational flag as MODIFICATION of the
               existing entry */
            pIssReservFrmCtrlInfo =
                GET_RESERVE_FRAME_CTRL_INFO (u4NewRservFrmTableIndex);
            if (pIssReservFrmCtrlInfo != NULL)
            {
                pIssReservFrmCtrlInfo->u1OperationStatus =
                    ISS_RESERV_FRAME_MODIFY;
            }
        }
        if (nmhTestv2IssReservedFrameCtrlPktType
            (&u4ErrCode, u4NewRservFrmTableIndex,
             (INT4) u4Protocol) == SNMP_FAILURE)
        {

            /*Clearing the entry in the Reserve frame table both control and dataplane */
            if (nmhSetIssReservedFrameCtrlStatus
                (u4NewRservFrmTableIndex, ISS_DESTROY) == SNMP_FAILURE)
            {
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed in clearing the Reserv frame  Control Table index %d\n",
                              u4NewRservFrmTableIndex);
            }

            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test  Reserv frame  Control Table index %d / control pkt type %d  with  error code  as %d\n",
                          u4NewRservFrmTableIndex, u4Protocol, u4ErrCode);
            return CLI_FAILURE;
        }

        if (nmhSetIssReservedFrameCtrlPktType
            (u4NewRservFrmTableIndex, (INT4) u4Protocol) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);

            /*Clearing the entry in the Reserve frame table both control and dataplane */
            if (nmhSetIssReservedFrameCtrlStatus
                (u4NewRservFrmTableIndex, ISS_DESTROY) == SNMP_FAILURE)
            {
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed in clearing the Reserv frame  Control Table index %d\n",
                              u4NewRservFrmTableIndex);
            }

            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set Reserv frame  Control Table index %d /control pkt type %d with error code  as %d\n",
                          u4NewRservFrmTableIndex, u4Protocol, u4ErrCode);
            return CLI_FAILURE;
        }

        /* If the MAC address was not filled, need not call the test and set routine for "other token" protocols */
        if ((MEMCMP (ZeroMacAddr, MacAddr, sizeof (tMacAddr)) != 0))
        {
            if (nmhTestv2IssReservedFrameCtrlOtherMacAddr
                (&u4ErrCode, u4NewRservFrmTableIndex, MacAddr) == SNMP_FAILURE)
            {

                /*Clearing the entry in the Reserve frame table both control and dataplane */
                if (nmhSetIssReservedFrameCtrlStatus
                    (u4NewRservFrmTableIndex, ISS_DESTROY) == SNMP_FAILURE)
                {
                    ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed in clearing the Reserv frame  Control Table index %d\n",
                                  u4NewRservFrmTableIndex);
                }

                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to test  Reserv frame Mac address  with  error code  as %d\n",
                              u4ErrCode);
                return CLI_FAILURE;
            }

            if (nmhSetIssReservedFrameCtrlOtherMacAddr
                (u4NewRservFrmTableIndex, MacAddr) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);

                /*Clearing the entry in the Reserve frame table both control and dataplane */
                if (nmhSetIssReservedFrameCtrlStatus
                    (u4NewRservFrmTableIndex, ISS_DESTROY) == SNMP_FAILURE)
                {
                    ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed in clearing the Reserv frame  Control Table index %d\n",
                                  u4NewRservFrmTableIndex);
                }

                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set Reserv frame Mac address  with  error code  as %d\n",
                              u4ErrCode);
                return CLI_FAILURE;
            }
            if (nmhTestv2IssReservedFrameCtrlOtherMacMask
                (&u4ErrCode, u4NewRservFrmTableIndex,
                 u1MacMask) == SNMP_FAILURE)
            {

                /*Clearing the entry in the Reserve frame table both control and dataplane */
                if (nmhSetIssReservedFrameCtrlStatus
                    (u4NewRservFrmTableIndex, ISS_DESTROY) == SNMP_FAILURE)
                {
                    ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed in clearing the Reserv frame  Control Table index %d\n",
                                  u4NewRservFrmTableIndex);
                }

                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to test  Reserv frame Mac Mask  with  error code  as %d\n",
                              u4ErrCode);
                return CLI_FAILURE;
            }

            if (nmhSetIssReservedFrameCtrlOtherMacMask
                (u4NewRservFrmTableIndex, u1MacMask) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);

                /*Clearing the entry in the Reserve frame table both control and dataplane */
                if (nmhSetIssReservedFrameCtrlStatus
                    (u4NewRservFrmTableIndex, ISS_DESTROY) == SNMP_FAILURE)
                {
                    ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                  "\nCLI:Failed in clearing the Reserv frame  Control Table index %d\n",
                                  u4NewRservFrmTableIndex);
                }

                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set Reserv frame Mac Mask  with  error code  as %d\n",
                              u4ErrCode);
                return CLI_FAILURE;
            }
        }

        if (nmhTestv2IssReservedFrameCtrlAction
            (&u4ErrCode, u4NewRservFrmTableIndex,
             u1ActionStatus) == SNMP_FAILURE)
        {

            /*Clearing the entry in the Reserve frame table both control and dataplane */
            if (nmhSetIssReservedFrameCtrlStatus
                (u4NewRservFrmTableIndex, ISS_DESTROY) == SNMP_FAILURE)
            {
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed in clearing the Reserv frame  Control Table index %d\n",
                              u4NewRservFrmTableIndex);
            }

            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test  Reserv frame Control Action with error code  as %d\n",
                          u4ErrCode);
            return CLI_FAILURE;
        }

        if (nmhSetIssReservedFrameCtrlAction
            (u4NewRservFrmTableIndex, u1ActionStatus) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);

            /*Clearing the entry in the Reserve frame table both control and dataplane */
            if (nmhSetIssReservedFrameCtrlStatus
                (u4NewRservFrmTableIndex, ISS_DESTROY) == SNMP_FAILURE)
            {
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed in clearing the Reserv frame  Control Table index %d\n",
                              u4NewRservFrmTableIndex);
            }

            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set Reserv frame Control Action with error code  as %d\n",
                          u4ErrCode);
            return CLI_FAILURE;
        }

        if (nmhTestv2IssReservedFrameCtrlStatus
            (&u4ErrCode, u4NewRservFrmTableIndex, ISS_ACTIVE) == SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to test Reserv Frame Control Table index%d with  error code as %d\n",
                          u4NewRservFrmTableIndex, u4ErrCode);
            return CLI_FAILURE;
        }

        if (nmhSetIssReservedFrameCtrlStatus
            (u4NewRservFrmTableIndex, ISS_ACTIVE) == SNMP_FAILURE)

        {
            CLI_FATAL_ERROR (CliHandle);

            /*Clearing the entry in the Reserve frame table both control and dataplane */
            if (nmhSetIssReservedFrameCtrlStatus
                (u4NewRservFrmTableIndex, ISS_DESTROY) == SNMP_FAILURE)
            {
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed in clearing the Reserv frame  Control Table index %d\n",
                              u4NewRservFrmTableIndex);
            }
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nCLI:Failed to set Reserv frame  Control Table index %d\n",
                          u4NewRservFrmTableIndex);
            return CLI_FAILURE;
        }
    }
    else
    {
        /*checking whether the entry is already exsiting in our table */
        MEMCPY (CheckMacAddr, MacAddr, sizeof (tMacAddr));
        u4IsExistRservFrmTableIndex =
            AclCheckReservFrameEntry (u4Protocol, CheckMacAddr, u1MacMask);
        if (u4IsExistRservFrmTableIndex != 0)
        {
            if (nmhTestv2IssReservedFrameCtrlStatus
                (&u4ErrCode, u4IsExistRservFrmTableIndex,
                 ISS_DESTROY) == SNMP_FAILURE)
            {
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to test Reserv Frame Control Table index%d with  error code as %d\n",
                              u4IsExistRservFrmTableIndex, u4ErrCode);
                return CLI_FAILURE;
            }

            if (nmhSetIssReservedFrameCtrlStatus
                (u4IsExistRservFrmTableIndex, ISS_DESTROY) == SNMP_FAILURE)

            {
                CLI_FATAL_ERROR (CliHandle);

                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "\nCLI:Failed to set Reserv frame  Control Table index %d\n",
                              u4NewRservFrmTableIndex);
                return CLI_FAILURE;
            }

        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\n %% The specified Reserved Frame entry is not avaliable \r\n");
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclReservFrameShow                                 */
/*                                                                           */
/*     DESCRIPTION      : This function displays the reserve frame  filter   */
/*                         configurations.                                   */
/*                                                                           */
/*     INPUT            : NONE                                               */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
AclReservFrameShow (tCliHandle CliHandle)
{
    tIssReservFrmCtrlTable *pIssReservFrmCtrlInfo = NULL;
    tMacAddr            ResrvFrmMacAddr;
    UINT4               u4RservFrmTableIndex = 0;
    INT4                i4CtrlPktType = 0;
    INT4                i4ControlAction = 0;
    INT4                i4MacMask = 0;
    UINT1               au1MacString[ISS_ADDR_LEN];
    CliPrintf (CliHandle,
               "\rProtocols\t\tMAC addresses\t\t\t\tMask\t\t\tReserve Frame Action \r\n");
    CliPrintf (CliHandle,
               "\r---------\t\t-------------\t\t\t\t----\t\t\t---------------------\r\n");

    for (u4RservFrmTableIndex = 1;
         u4RservFrmTableIndex <= ISS_MAX_RESERV_FRM_CTRL_ID;
         u4RservFrmTableIndex++)
    {
        MEMSET (au1MacString, 0, ISS_ADDR_LEN);
        MEMSET (ResrvFrmMacAddr, 0, sizeof (tMacAddr));
        pIssReservFrmCtrlInfo =
            GET_RESERVE_FRAME_CTRL_INFO (u4RservFrmTableIndex);
        if (pIssReservFrmCtrlInfo == NULL)
        {
            continue;
        }
        if (nmhGetIssReservedFrameCtrlAction
            (u4RservFrmTableIndex, &i4ControlAction) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhGetIssReservedFrameCtrlPktType
            (u4RservFrmTableIndex, &i4CtrlPktType) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhGetIssReservedFrameCtrlOtherMacMask
            (u4RservFrmTableIndex, &i4MacMask) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhGetIssReservedFrameCtrlOtherMacAddr
            (u4RservFrmTableIndex, &ResrvFrmMacAddr) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        switch (i4CtrlPktType)
        {
            case ISS_RESERVED_FRAME_BPDU:
                if (i4ControlAction == RESERVED_FRAME_ACTION_FWD)
                {
                    CliPrintf (CliHandle, "BPDU\t\t\t");

                    PrintMacAddress (ResrvFrmMacAddr, au1MacString);

                    CliPrintf (CliHandle, "%-20s", au1MacString);

                    CliPrintf (CliHandle, "\t\t\t-");

                    CliPrintf (CliHandle, "\t\t\tForward\r\n");

                }
                else
                {
                    CliPrintf (CliHandle, "BPDU\t\t\t");

                    PrintMacAddress (ResrvFrmMacAddr, au1MacString);

                    CliPrintf (CliHandle, "%-20s", au1MacString);

                    CliPrintf (CliHandle, "\t\t\t-");

                    CliPrintf (CliHandle, "\t\t\tDrop\r\n");

                }

                break;
            case ISS_RESERVED_FRAME_LACPDU:
                if (i4ControlAction == RESERVED_FRAME_ACTION_FWD)
                {

                    CliPrintf (CliHandle, "LACPDU\t\t\t");

                    PrintMacAddress (ResrvFrmMacAddr, au1MacString);

                    CliPrintf (CliHandle, "%-20s", au1MacString);

                    CliPrintf (CliHandle, "\t\t\t-");

                    CliPrintf (CliHandle, "\t\t\tForward\r\n");

                }
                else
                {
                    CliPrintf (CliHandle, "LACPDU\t\t\t");

                    PrintMacAddress (ResrvFrmMacAddr, au1MacString);

                    CliPrintf (CliHandle, "%-20s", au1MacString);

                    CliPrintf (CliHandle, "\t\t\t-");

                    CliPrintf (CliHandle, "\t\t\tDrop\r\n");

                }

                break;
            case ISS_RESERVED_FRAME_EAP:
                if (i4ControlAction == RESERVED_FRAME_ACTION_FWD)
                {
                    CliPrintf (CliHandle, "EAP\t\t\t");

                    PrintMacAddress (ResrvFrmMacAddr, au1MacString);

                    CliPrintf (CliHandle, "%-20s", au1MacString);

                    CliPrintf (CliHandle, "\t\t\t-");

                    CliPrintf (CliHandle, "\t\t\tForward\r\n");

                }
                else
                {

                    CliPrintf (CliHandle, "EAP\t\t\t");

                    PrintMacAddress (ResrvFrmMacAddr, au1MacString);

                    CliPrintf (CliHandle, "%-20s", au1MacString);

                    CliPrintf (CliHandle, "\t\t\t-");

                    CliPrintf (CliHandle, "\t\t\tDrop\r\n");

                }

                break;
            case ISS_RESERVED_FRAME_LLDPDU:
                if (i4ControlAction == RESERVED_FRAME_ACTION_FWD)
                {
                    CliPrintf (CliHandle, "LLDP\t\t\t");

                    PrintMacAddress (ResrvFrmMacAddr, au1MacString);

                    CliPrintf (CliHandle, "%-20s", au1MacString);

                    CliPrintf (CliHandle, "\t\t\t-");

                    CliPrintf (CliHandle, "\t\t\tForward\r\n");
                }
                else
                {
                    CliPrintf (CliHandle, "LLDP\t\t\t");

                    PrintMacAddress (ResrvFrmMacAddr, au1MacString);

                    CliPrintf (CliHandle, "%-20s", au1MacString);

                    CliPrintf (CliHandle, "\t\t\t-");

                    CliPrintf (CliHandle, "\t\t\tDrop\r\n");

                }

                break;
            case ISS_RESERVED_FRAME_OTHER:
                if (i4ControlAction == RESERVED_FRAME_ACTION_FWD)
                {
                    CliPrintf (CliHandle, "Others\t\t\t");

                    PrintMacAddress (ResrvFrmMacAddr, au1MacString);

                    CliPrintf (CliHandle, "%-20s", au1MacString);

                    if (i4MacMask != 0xFF)
                    {
                        CliPrintf (CliHandle, "\t\t\t%x", i4MacMask);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "\t\t\t-");
                    }

                    CliPrintf (CliHandle, "\t\t\tForward\r\n");
                }
                else
                {
                    CliPrintf (CliHandle, "Others\t\t\t");

                    PrintMacAddress (ResrvFrmMacAddr, au1MacString);

                    CliPrintf (CliHandle, "%-20s", au1MacString);

                    if (i4MacMask != 0xFF)
                    {
                        CliPrintf (CliHandle, "\t\t\t%x", i4MacMask);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "\t\t\t-");
                    }
                    CliPrintf (CliHandle, "\t\t\tDrop\r\n");

                }

                break;
            case ISS_RESERVED_FRAME_ALL:
                if (i4ControlAction == RESERVED_FRAME_ACTION_FWD)
                {
                    CliPrintf (CliHandle, "All\t\t\t");

                    PrintMacAddress (ResrvFrmMacAddr, au1MacString);

                    CliPrintf (CliHandle, "%-20s", au1MacString);

                    if (i4MacMask != 0xFF)
                    {
                        CliPrintf (CliHandle, "\t\t\t%x", i4MacMask);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "\t\t\t-");
                    }
                    CliPrintf (CliHandle, "\t\t\tForward\r\n");

                }
                else
                {
                    CliPrintf (CliHandle, "All\t\t\t");

                    PrintMacAddress (ResrvFrmMacAddr, au1MacString);

                    CliPrintf (CliHandle, "%-20s", au1MacString);

                    if (i4MacMask != 0xFF)
                    {
                        CliPrintf (CliHandle, "\t\t\t%x", i4MacMask);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "\t\t\t-");
                    }
                    CliPrintf (CliHandle, "\t\t\tDrop\r\n");

                }

                break;
            default:
                return CLI_FAILURE;
                break;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclCheckReservFrameEntry                 */
/*                                                                           */
/*     DESCRIPTION      : This function displays the reserve frame  filter   */
/*                         configurations.                                   */
/*                                                                           */
/*     INPUT            : NONE                                               */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
UINT4
AclCheckReservFrameEntry (UINT4 u4Protocol, tMacAddr MacAddr, UINT1 u1MacMask)
{
    tIssReservFrmCtrlTable *pIssReservFrmCtrlInfo = NULL;
    UINT4               u4RservFrmTableIndex = 0;
    UINT4               u4EntryNotFound = 0;

/*Filling the MAC address with respect to the protocol */
    switch (u4Protocol)
    {
        case ISS_RESERVED_FRAME_BPDU:
            MEMSET (MacAddr, 0, sizeof (tMacAddr));
            MEMCPY (MacAddr, ISS_BPDU_MAC_ADDRESS, sizeof (tMacAddr));
            break;
        case ISS_RESERVED_FRAME_LACPDU:
            MEMSET (MacAddr, 0, sizeof (tMacAddr));
            MEMCPY (MacAddr, ISS_LACPDU_MAC_ADDRESS, sizeof (tMacAddr));
            break;
        case ISS_RESERVED_FRAME_EAP:
            MEMSET (MacAddr, 0, sizeof (tMacAddr));
            MEMCPY (MacAddr, ISS_EAP_MAC_ADDRESS, sizeof (tMacAddr));
            break;
        case ISS_RESERVED_FRAME_LLDPDU:
            MEMSET (MacAddr, 0, sizeof (tMacAddr));
            MEMCPY (MacAddr, ISS_LLDPDU_MAC_ADDRESS, sizeof (tMacAddr));
            break;
        case ISS_RESERVED_FRAME_ALL:
            MEMSET (MacAddr, 0, sizeof (tMacAddr));
            MEMCPY (MacAddr, ISS_BPDU_MAC_ADDRESS, sizeof (tMacAddr));
            u1MacMask = 0xE0;
            break;
    }

    for (u4RservFrmTableIndex = 1;
         u4RservFrmTableIndex <= ISS_MAX_RESERV_FRM_CTRL_ID;
         u4RservFrmTableIndex++)
    {
        pIssReservFrmCtrlInfo =
            GET_RESERVE_FRAME_CTRL_INFO (u4RservFrmTableIndex);
        if (pIssReservFrmCtrlInfo != NULL)
        {
            /* Checking whether the entry is avalaible */
            if ((pIssReservFrmCtrlInfo->u1RowStatus == ISS_ACTIVE)
                && (pIssReservFrmCtrlInfo->u4ControlPktType == u4Protocol)
                &&
                (MEMCMP
                 (pIssReservFrmCtrlInfo->MacAddr, MacAddr,
                  sizeof (tMacAddr)) == 0)
                && (pIssReservFrmCtrlInfo->u4MacMask == (UINT4) u1MacMask))
            {
                /*If the Entry matches with the existing table,returning the index of that entry */
                return (u4RservFrmTableIndex);
            }
        }
    }
    return (u4EntryNotFound);

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssFetchMacAddressesFrmMask                      */
/*                                                                          */
/*    Description        : This function is used to fetch the MAC addresses */
/*                        from the MASK                                     */
/*                                                                          */
/*    Input(s)           : pIssReservFrmCtrlInfo - Reserved Fram Table      */
/*                    None                                                  */
/*                                                                          */
/*    Output(s)                 : None.                                     */
/*    Global Variables Referred : None.                                     */
/*    Global Variables Modified : None.                                     */
/*    Exceptions or Operating                                               */
/*    System Error Handling     : None.                                     */
/*    Use of Recursion          : None.                                     */
/*    Returns                   : FNP_SUCCESS/FNP_FAILURE                   */
/*                                                                          */
/*****************************************************************************/
PUBLIC INT4
IssFetchMacAddressesFrmMask (tMacAddr ResrvMacAddresses[],
                             tMacAddr MacAddr, UINT4 u4MacMask,
                             INT4 *pi4TotalMacAddresses)
{
    tMacAddr            StartMacAddr;
    tMacAddr            TempMacAddr;
    INT4                i4Index = 0;

    MEMSET (StartMacAddr, 0, sizeof (tMacAddr));
    MEMSET (TempMacAddr, 0, sizeof (tMacAddr));
    switch (u4MacMask)
    {
        case FF_VALUE:
            *pi4TotalMacAddresses = 1;
            break;
        case FE_VALUE:
            *pi4TotalMacAddresses = 2;
            break;
        case FC_VALUE:
            *pi4TotalMacAddresses = 4;
            break;
        case F8_VALUE:
            *pi4TotalMacAddresses = 8;
            break;
        case F0_VALUE:
            *pi4TotalMacAddresses = 16;
            break;
        case E0_VALUE:
            /*
             * when the MASK value is 0xE0,totally 32 MAC addresses needs to be dropped or forwareded
             * based on the action given, here we have mention as 16 MAC addresses,since these 32 MAC
             * addresses are not in sequential manner that is, the range is in 00-0F and 20-2F.
             * Once all the 32 MAC addresses copied, total MAC addresses values will be changed to
             * 32 MAC addressses.
             *
             */
            *pi4TotalMacAddresses = 16;
            break;
        default:
            break;

    }
    MEMCPY (StartMacAddr, MacAddr, sizeof (tMacAddr));

    /*Calculating Starting MAC address */
    StartMacAddr[5] = StartMacAddr[5] & u4MacMask;

    if (u4MacMask == E0_VALUE)
    {
        /* Copying first group (00-0F) MAC addresses.
         * Starting MAC address 20 is copied in next step.*/
        MEMCPY (StartMacAddr, ISS_BPDU_MAC_ADDRESS, sizeof (tMacAddr));
    }

    for (i4Index = 0; i4Index < *pi4TotalMacAddresses; i4Index++)
    {
        MEMSET (&ResrvMacAddresses[i4Index], 0, sizeof (tMacAddr));
        MEMCPY (&ResrvMacAddresses[i4Index], StartMacAddr, sizeof (tMacAddr));
        StartMacAddr[5] = (StartMacAddr[5] + 1);
    }

    if (u4MacMask == E0_VALUE)
    {
        /*
         * All the next group (20-2F) MAC addresses copied.
         * Totally 32 MAC addresses copied.So,here Changing the total MAC addresses
         * as 32.
         */
        *pi4TotalMacAddresses = 32;

        /* Copying next group (20-2F) MAC addresses.
         * Starting MAC address 20 is copied in next step.*/
        MEMCPY (StartMacAddr, gL2CacheGvrpAddrGroup, sizeof (tMacAddr));

        for (; i4Index < *pi4TotalMacAddresses; i4Index++)
        {
            MEMSET (&ResrvMacAddresses[i4Index], 0, sizeof (tMacAddr));
            MEMCPY (&ResrvMacAddresses[i4Index], StartMacAddr,
                    sizeof (tMacAddr));
            StartMacAddr[5] = (StartMacAddr[5] + 1);
        }
    }

    return CLI_SUCCESS;

}

#endif
