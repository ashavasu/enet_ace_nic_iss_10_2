/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: issexport.c,v 1.9 2015/07/17 09:50:26 siva Exp $
 *
 * Description: This file contains the the function ported from other Modules
 * (like RM Module).
 *****************************************************************************/

#include "issexinc.h"
#include "aclcli.h"
#include "fsisselw.h"
#include "fsissalw.h"

/*****************************************************************************/
/* Function Name      : AclRmRegisterProtocols                                 */
/*                                                                           */
/* Description        : This function Register ACL module with RM Module     */
/*                                                                           */
/* Input(s)           : pRmRegParams - Pointer to the structure of RM          */
/*                       register parameters              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                     */
/*****************************************************************************/
PUBLIC INT4
AclRmRegisterProtocols (tRmRegParams * pRmRegParams)
{
    INT4                i4RetVal = RM_SUCCESS;
    ISS_TRC (INIT_SHUT_TRC, "\nPORT:Entry AclRmRegisterProtocols function \n");
#ifdef L2RED_WANTED
    i4RetVal = RmRegisterProtocols (pRmRegParams);
#else
    UNUSED_PARAM (pRmRegParams);
#endif
    ISS_TRC (INIT_SHUT_TRC, "\nPORT:Exit AclRmRegisterProtocols function \n");
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : AclRmDeRegisterProtocols                         */
/*                                                                           */
/* Description        : This function De-Register ACL module with RM Module  */
/*                                                                           */
/* Input(s)           : None                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                     */
/*****************************************************************************/
PUBLIC INT4
AclRmDeRegisterProtocols ()
{
    INT4                i4RetVal = RM_SUCCESS;
    ISS_TRC (INIT_SHUT_TRC,
             "\nPORT:Entry AclRmDeRegisterProtocols  function \n");
#ifdef L2RED_WANTED
    i4RetVal = RmDeRegisterProtocols (RM_ACL_APP_ID);
#endif
    ISS_TRC (INIT_SHUT_TRC,
             "\nPORT:Exit AclRmDeRegisterProtocols  function \n");
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : AclPortRmReleaseMemoryForMsg                         */
/*                                                                           */
/* Description        : This function Release RM Message memory postedf to   */
/*            ACL module                         */
/*                                                                           */
/* Input(s)           : pData - Pointer to the RM Message               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                     */
/*****************************************************************************/
PUBLIC INT4
AclPortRmReleaseMemoryForMsg (UINT1 *pData)
{
    INT4                i4RetVal = RM_SUCCESS;
    ISS_TRC (INIT_SHUT_TRC,
             "\nPORT:Entry AclPortRmReleaseMemoryForMsg  function \n");
#ifdef L2RED_WANTED
    i4RetVal = RmReleaseMemoryForMsg (pData);
#else
    UNUSED_PARAM (pData);
#endif
    ISS_TRC (INIT_SHUT_TRC,
             "\nPORT:Exit AclPortRmReleaseMemoryForMsg  function \n");
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : AclPortRmApiHandleProtocolEvent                         */
/*                                                                           */
/* Description        : This function posts the event to the RM module         */
/*            form ACL module                         */
/*                                                                           */
/* Input(s)           : pEvt - Pointer to the strucutre for RM protocol evt  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                     */
/*****************************************************************************/
PUBLIC INT4
AclPortRmApiHandleProtocolEvent (tRmProtoEvt * pEvt)
{
    INT4                i4RetVal = RM_SUCCESS;
    ISS_TRC (INIT_SHUT_TRC,
             "\nPORT:Entry AclPortRmApiHandleProtocolEvent function \n");
#ifdef L2RED_WANTED
    i4RetVal = RmApiHandleProtocolEvent (pEvt);
#else
    UNUSED_PARAM (pEvt);
#endif
    ISS_TRC (INIT_SHUT_TRC,
             "\nPORT:Exit AclPortRmApiHandleProtocolEvent function \n");
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : AclPortRmApiSendProtoAckToRM                         */
/*                                                                           */
/* Description        : This function posts the acknowledgement to the         */
/*            RM module                         */
/*                                                                           */
/* Input(s)           : pProtoAck-Pointer to the struct for RM protocol Ack  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                     */
/*****************************************************************************/
PUBLIC INT4
AclPortRmApiSendProtoAckToRM (tRmProtoAck * pProtoAck)
{
    INT4                i4RetVal = RM_SUCCESS;
    ISS_TRC (INIT_SHUT_TRC,
             "\nPORT:Entry AclPortRmApiSendProtoAckToRM function \n");
#ifdef L2RED_WANTED
    i4RetVal = RmApiSendProtoAckToRM (pProtoAck);
#else
    UNUSED_PARAM (pProtoAck);
#endif
    ISS_TRC (INIT_SHUT_TRC,
             "\nPORT:Exit AclPortRmApiSendProtoAckToRM function \n");
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : AclPortRmEnqMsgToRm                               */
/*                                                                           */
/* Description        : This function posts the RM message to the RM Module  */
/*                                                                           */
/* Input(s)           : pRmMsg - Pointer to the RM Message             */
/*            u2DataLen - Length of the data                  */
/*            u4SrcEntId - Source Application ID.             */
/*            u4DestEntId - Destination Application ID.          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                     */
/*****************************************************************************/
UINT4
AclPortRmEnqMsgToRm (tRmMsg * pRmMsg, UINT2 u2DataLen, UINT4 u4SrcEntId,
                     UINT4 u4DestEntId)
{
    UINT4               u4RetVal = RM_SUCCESS;
    ISS_TRC (INIT_SHUT_TRC, "\nPORT:Entry AclPortRmEnqMsgToRm function \n");
#ifdef L2RED_WANTED
    u4RetVal =
        RmEnqMsgToRmFromAppl (pRmMsg, u2DataLen, u4SrcEntId, u4DestEntId);
#else
    UNUSED_PARAM (pRmMsg);
    UNUSED_PARAM (u2DataLen);
    UNUSED_PARAM (u4SrcEntId);
    UNUSED_PARAM (u4DestEntId);
#endif
    ISS_TRC (INIT_SHUT_TRC, "\nPORT:Exit AclPortRmEnqMsgToRm function \n");
    return u4RetVal;
}

/*****************************************************************************/
/* Function Name      : AclPortRmGetNodeState                     */
/*                                                                           */
/* Description        : This function gets the RM node state.                 */
/*                                                                           */
/* Input(s)           : None                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                     */
/*****************************************************************************/
PUBLIC INT4
AclPortRmGetNodeState (VOID)
{
    ISS_TRC (INIT_SHUT_TRC, "\nPORT:Entry AclPortRmGetNodeState  function \n");
#ifdef L2RED_WANTED
    return (RmGetNodeState ());
#else
    return RM_ACTIVE;
#endif
    ISS_TRC (INIT_SHUT_TRC, "\nPORT:Exit AclPortRmGetNodeState  function \n");
}

/*****************************************************************************/
/* Function Name      : AclPortRmGetStandbyNodeCount                  */
/*                                                                           */
/* Description        : This function gets the RM Standby Count.             */
/*                                                                           */
/* Input(s)           : None                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                     */
/*****************************************************************************/
PUBLIC UINT1
AclPortRmGetStandbyNodeCount (VOID)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nPORT:Entry AclPortRmGetStandbyNodeCount  function \n");
#ifdef L2RED_WANTED
    return (RmGetStandbyNodeCount ());
#else
    return ISS_ZERO_ENTRY;
#endif
    ISS_TRC (INIT_SHUT_TRC,
             "\nPORT:Exit AclPortRmGetStandbyNodeCount  function \n");
}

/*****************************************************************************/
/* Function Name      : AclPortRmSetBulkUpdatesStatus                 */
/*                                                                           */
/* Description        : This function sets the RM Bul Update Status in RM    */
/*                                                                           */
/* Input(s)           : None                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
PUBLIC VOID
AclPortRmSetBulkUpdatesStatus (VOID)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nPORT:Entry AclPortRmSetBulkUpdatesStatus   function \n");
#ifdef L2RED_WANTED
    RmSetBulkUpdatesStatus (RM_ACL_APP_ID);
    return;
#else
    return;
#endif
    ISS_TRC (INIT_SHUT_TRC,
             "\nPORT:Exit AclPortRmSetBulkUpdatesStatus  function \n");
}

/*****************************************************************************
**
**     FUNCTION NAME    : AclUpdateOverPortChannel
**
**     DESCRIPTION      : This function removes any L2/L3 ACL that is
**                        applied on a given port-channel interface.
**
**     INPUT            : u4IfIndex - Interface Index
**
**     OUTPUT           : NONE
**
**     RETURNS          : CFA_SUCCESS or CFA_FAILURE
**
*****************************************************************************/
INT4
AclUpdateOverPortChannel (UINT4 u4IfIndex)
{
    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Entry AclUpdateOverPortChannel function \n");
    
    /* Validate interface index is a port-channel */    
    if ((u4IfIndex <= BRG_MAX_PHY_PORTS)
        || (u4IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclUpdateOverPortChannel function \n");
        return CFA_SUCCESS;
    }
    
    /* Remove port-channel from any L2 ACL */
    if (AclNoMacExtAccessGroup
        (-1, 0, ACL_ACCESS_IN, ACL_STAT_ENABLE, u4IfIndex) == CLI_FAILURE)
    {
        ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclUpdateOverPortChannel function \n");
        return CFA_FAILURE;
    }

    if (AclNoMacExtAccessGroup
        (-1, 0, ACL_ACCESS_OUT, ACL_STAT_ENABLE, u4IfIndex) == CLI_FAILURE)
    {
        ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclUpdateOverPortChannel function \n");
        return CFA_FAILURE;
    }

    /* Remove port-channel from any L3 ACL */
    if (AclNoIpAccessGroup
        (-1, 0, ACL_ACCESS_IN, ACL_STAT_ENABLE, u4IfIndex) == CLI_FAILURE)
    {
        ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclUpdateOverPortChannel function \n");
        return CFA_FAILURE;
    }

    if (AclNoIpAccessGroup
        (-1, 0, ACL_ACCESS_OUT, ACL_STAT_ENABLE, u4IfIndex) == CLI_FAILURE)
    {
        ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclUpdateOverPortChannel function \n");
        return CFA_FAILURE;
    }

    ISS_TRC (INIT_SHUT_TRC, "\nCLI:Exit AclUpdateOverPortChannel function \n");
    return CFA_SUCCESS;
}

/*****************************************************************************
 **
 **     FUNCTION NAME    : AclIpFilterEnabledForPort
 **
 **     DESCRIPTION      : This function checks whether
 **                       IP any ACL is associated with this port
 **
 **
 **     INPUT            : u4IfIndex - Interface Index
 **
 **     OUTPUT           : NONE
 **
 **     RETURNS          :  CLI_SUCCESS / CLI_FAILURE
 **
 *****************************************************************************/
INT4
AclIpFilterEnabledForPort (UINT4 u4IfIndex)
{
       tSNMP_OCTET_STRING_TYPE PortList;
       INT4 i4FilterNo = 0;
       UINT1               au1PortList[ISS_PORTLIST_LEN];
       INT4                i4CurrentFilter = 0;
       MEMSET (&PortList, 0x0, sizeof (PortList));
       MEMSET (au1PortList, 0, ISS_PORTLIST_LEN);
       PortList.pu1_OctetList = &au1PortList[0];
       PortList.i4_Length = ISS_PORTLIST_LEN;
       if (nmhGetFirstIndexIssExtL3FilterTable (&i4FilterNo) == SNMP_FAILURE)
       {
               return (CLI_FAILURE);
       }

       do
       {
	       i4CurrentFilter = i4FilterNo;
	       /* for in direction     */
	       nmhGetIssExtL3FilterInPortList (i4FilterNo, &PortList);
	       if (CliIsMemberPort (PortList.pu1_OctetList,
				       ISS_PORTLIST_LEN, u4IfIndex) == CLI_SUCCESS)
	       {
                       return (CLI_SUCCESS);
               }
               /* for out direction    */
               nmhGetIssExtL3FilterOutPortList (i4FilterNo, &PortList);
               if (CliIsMemberPort (PortList.pu1_OctetList,
                                       ISS_PORTLIST_LEN, u4IfIndex) == CLI_SUCCESS)
               {
                       return (CLI_SUCCESS);
               }
       }
       while (nmhGetNextIndexIssExtL3FilterTable
                       (i4CurrentFilter, &i4FilterNo) == SNMP_SUCCESS);
       return (CLI_FAILURE);
}



/*****************************************************************************
 **
 **     FUNCTION NAME    : AclMacFilterEnabledForPort
 **
 **     DESCRIPTION      : This function checks whether
 **                       any MAC ACL is associated with this port
 **
 **
 **     INPUT            : u4IfIndex - Interface Index
 **
 **     OUTPUT           : NONE
 **
 **     RETURNS          :  CLI_SUCCESS / CLI_FAILURE
 **
 *****************************************************************************/
INT4
AclMacFilterEnabledForPort (UINT4 u4IfIndex)
{
       tSNMP_OCTET_STRING_TYPE PortList;
       INT4 i4FilterNo = 0;
       UINT1               au1PortList[ISS_PORTLIST_LEN];
       INT4                i4CurrentFilter = 0;
       MEMSET (&PortList, 0x0, sizeof (PortList));
       MEMSET (au1PortList, 0, ISS_PORTLIST_LEN);
       PortList.pu1_OctetList = &au1PortList[0];
       PortList.i4_Length = ISS_PORTLIST_LEN;

       if (nmhGetFirstIndexIssExtL2FilterTable (&i4FilterNo) == SNMP_FAILURE)
       {
               return (CLI_FAILURE);
       }

       do
       {

               i4CurrentFilter = i4FilterNo;
               nmhGetIssExtL2FilterInPortList (i4FilterNo, &PortList);
               if (CliIsMemberPort (PortList.pu1_OctetList,
                                       ISS_PORTLIST_LEN, u4IfIndex) == CLI_SUCCESS)
               {
                       return (CLI_SUCCESS);
               }

               nmhGetIssExtL2FilterOutPortList (i4FilterNo, &PortList);
               if (CliIsMemberPort (PortList.pu1_OctetList,
                                       ISS_PORTLIST_LEN, u4IfIndex) == CLI_SUCCESS)
               {
                       return (CLI_SUCCESS);
               }
       }
       while (nmhGetNextIndexIssExtL2FilterTable
                       (i4CurrentFilter, &i4FilterNo) == SNMP_SUCCESS);
       return (CLI_FAILURE);
}



/*****************************************************************************
 **
 **     FUNCTION NAME    : AclUserDefinedFilterEnabledForPort
 **
 **     DESCRIPTION      : This function checks whether
 **                       any UserDefined ACL is associated with this port
 **
 **
 **     INPUT            : u4IfIndex - Interface Index
 **
 **     OUTPUT           : NONE
 **
 **     RETURNS          :  CLI_SUCCESS / CLI_FAILURE
 **
 *****************************************************************************/
INT4
AclUserDefinedFilterEnabledForPort (UINT4 u4IfIndex)
{
       tSNMP_OCTET_STRING_TYPE PortList;
       INT4 i4FilterNo = 0;
       UINT1               au1PortList[ISS_PORTLIST_LEN];
       INT4                i4CurrentFilter = 0;
       MEMSET (&PortList, 0x0, sizeof (PortList));
       MEMSET (au1PortList, 0, ISS_PORTLIST_LEN);
       PortList.pu1_OctetList = &au1PortList[0];
       PortList.i4_Length = ISS_PORTLIST_LEN;

       if (nmhGetFirstIndexIssAclUserDefinedFilterTable ((UINT4 *) &i4FilterNo) == SNMP_FAILURE)
       {
               return (CLI_FAILURE);
       }
       do
       {
               i4CurrentFilter = i4FilterNo;
               nmhGetIssAclUserDefinedFilterInPortList ((UINT4) i4FilterNo,
                               &PortList);
               if (CliIsMemberPort (PortList.pu1_OctetList,
                                       ISS_PORTLIST_LEN, u4IfIndex) == CLI_SUCCESS)
               {
                       return (CLI_SUCCESS);
               }
       }
       while (nmhGetNextIndexIssAclUserDefinedFilterTable
                       ((UINT4) i4CurrentFilter, (UINT4 *) &i4FilterNo) == SNMP_SUCCESS);
       return (CLI_FAILURE);
}


/*****************************************************************************
 **
 **     FUNCTION NAME    : AclEnabledForPort
 **
 **     DESCRIPTION      : This function checks whether
 **                       any ACL is associated with this port
 **
 **
 **     INPUT            : u4IfIndex - Interface Index
 **
 **     OUTPUT           : NONE
 **
 **     RETURNS          : CLI_SUCCESS / CLI_FAILURE
 **
 *****************************************************************************/

INT4
AclEnabledForPort (UINT4 u4IfIndex)
{

       if (AclIpFilterEnabledForPort (u4IfIndex) == CLI_SUCCESS)
       {
               return (CLI_SUCCESS);
       }

       if (AclMacFilterEnabledForPort (u4IfIndex) == CLI_SUCCESS)
       {
               return (CLI_SUCCESS);
       }

       if (AclUserDefinedFilterEnabledForPort (u4IfIndex) == CLI_SUCCESS)
       {
               return (CLI_SUCCESS);
       }

       return (CLI_FAILURE);
}

