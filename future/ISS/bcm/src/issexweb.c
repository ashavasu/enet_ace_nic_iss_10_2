/* $Id: issexweb.c,v 1.37 2014/09/25 12:15:09 siva Exp $ */
/************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved    */
/* Licensee Aricent Inc., 2004-2005       */
/*                                                          */
/*  FILE NAME             : issexweb.c                       */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                 */
/*  SUBSYSTEM NAME        : ISS                             */
/*  MODULE NAME           :                              */
/*  LANGUAGE              : C                               */
/*  TARGET ENVIRONMENT    :                                 */
/*  DATE OF FIRST RELEASE :                                 */
/*  AUTHOR                : Aricent Inc.                 */
/*                                                          */
/************************************************************/

#ifdef WEBNM_WANTED
#include "issexinc.h"
#include "issexweb.h"
#include "fsissecli.h"
#include "fsissacli.h"
#include "fsissewr.h"
#include "fsissawr.h"
/*********************************************************************
*  Function Name : IssProcessCustomPages
*  Description   : This function processes the Pages specific to
*                  target
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
INT4
IssProcessCustomPages (tHttp * pHttp)
{
    UINT4               u4Count;

    ISS_TRC (INIT_SHUT_TRC, "\nWEB:Entry IssProcessCustomPages function \n");
    for (u4Count = 0; asIssTargetpages[u4Count].au1Page[0] != '\0'; u4Count++)
    {
        if (STRCMP (pHttp->ai1HtmlName, asIssTargetpages[u4Count].au1Page) == 0)
        {
            asIssTargetpages[u4Count].pfunctPtr (pHttp);
            return ISS_SUCCESS;
        }
    }
    ISS_TRC (INIT_SHUT_TRC, "\nWEB:Exit IssProcessCustomPages function \n");
    return ISS_FAILURE;
}

/************************************************************************ 
*  Function Name   : IssRedirectMacFilterPage 
*  Description     : This function will redirect Mac Filter page 
*  Input           : pHttp - Pointer to http entry where the html file 
*                    name needs to be changed(copied) 
*  Output          : None 
*  Returns         : None 
************************************************************************/
VOID
IssRedirectMacFilterPage (tHttp * pHttp)
{
    STRCPY (pHttp->ai1HtmlName, "bcm_mac_filterconf.html");
}

/************************************************************************ 
*  Function Name   : IssRedirectRateCtrlPage
*  Description     : This function will redirect Mac Filter page
*  Input           : pHttp - Pointer to http entry where the html file
*                    name needs to be changed(copied)
*  Output          : None
*  Returns         : None
************************************************************************/
VOID
IssRedirectRateCtrlPage (tHttp * pHttp)
{
    STRCPY (pHttp->ai1HtmlName, "bcm_port_ratectrl.html");
}

/************************************************************************ 
*  Function Name   : IssRedirectDiffSrvPage 
*  Description     : This function will redirect Diffsrv page 
*  Input           : pHttp - Pointer to http entry where the html file 
*                    name needs to be changed(copied) 
*  Output          : None 
*  Returns         : None 
************************************************************************/
VOID
IssRedirectDiffSrvPage (tHttp * pHttp)
{
    STRCPY (pHttp->ai1HtmlName, "bcm_dfs_globalconf.html");
}

/*********************************************************************
*  Function Name : IssBcmProcessIPFilterConfPage 
*  Description   : This function processes the request coming for the IP 
*                  Filter Configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
VOID
IssBcmProcessIPFilterConfPage (tHttp * pHttp)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nWEB:Entry IssBcmProcessIPFilterConfPage function \n");
    if (pHttp->i4Method == ISS_GET)
    {
        IssBcmProcessIPFilterConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssBcmProcessIPFilterConfPageSet (pHttp);
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nWEB:Exit IssBcmProcessIPFilterConfPage function \n");
}

/*********************************************************************
*  Function Name : IssBcmProcessIPStdFilterConfPage 
*  Description   : This function processes the request coming for the IP 
*                  standard Filter Configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
VOID
IssBcmProcessIPStdFilterConfPage (tHttp * pHttp)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nWEB:Entry IssBcmProcessIPStdFilterConfPage function \n");
    if (pHttp->i4Method == ISS_GET)
    {
        IssBcmProcessIPStdFilterConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssBcmProcessIPStdFilterConfPageSet (pHttp);
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nWEB:Exit IssBcmProcessIPStdFilterConfPage function \n");
}

/*********************************************************************
*  Function Name : IssBcmProcessMACFilterConfPage 
*  Description   : This function processes the request coming for the MAC 
*                  Filter Configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
VOID
IssBcmProcessMACFilterConfPage (tHttp * pHttp)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nWEB:Entry IssBcmProcessMACFilterConfPage function \n");
    if (pHttp->i4Method == ISS_GET)
    {
        IssBcmProcessMACFilterConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssBcmProcessMACFilterConfPageSet (pHttp);
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nWEB:Exit IssBcmProcessMACFilterConfPage function \n");
}

/*********************************************************************
*  Function Name : IssBcmProcessVlanPvidSettingsPage 
*  Description   : This function processes the request coming for the Vlan Pvid
                   Configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
VOID
IssBcmProcessVlanPvidSettingsPage (tHttp * pHttp)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nWEB:Entry IssBcmProcessVlanPvidSettingsPage function \n");
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessPost (pHttp);
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nWEB:Exit IssBcmProcessVlanPvidSettingsPage function \n");
}

/************************************************************************ 
*  Function Name   : IssRedirectVlanSubnetEnablePage 
*  Description     : This function will redirect Diffsrv page 
*  Input           : pHttp - Pointer to http entry where the html file 
*                    name needs to be changed(copied) 
*  Output          : None 
*  Returns         : None 
************************************************************************/
VOID
IssRedirectVlanSubnetEnablePage (tHttp * pHttp)
{
    STRCPY (pHttp->ai1HtmlName, "bcm_vlan_pvidsetting.html");
}

/************************************************************************ 
*  Function Name   : IssRedirectVlanSubnetPerPortEnablePage 
*  Description     : This function will redirect Diffsrv page 
*  Input           : pHttp - Pointer to http entry where the html file 
*                    name needs to be changed(copied) 
*  Output          : None 
*  Returns         : None 
************************************************************************/
VOID
IssRedirectVlanSubnetPerPortEnablePage (tHttp * pHttp)
{
    STRCPY (pHttp->ai1HtmlName, "bcm_vlan_subnetipmap.html");
}

/*********************************************************************
*  Function Name : IssBcmProcessVlanSubnetPerPortSettingsPage 
*  Description   : This function processes the request coming for the Vlan Pvid
                   Configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
VOID
IssBcmProcessVlanSubnetPerPortSettingsPage (tHttp * pHttp)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nWEB:Entry IssBcmProcessVlanSubnetPerPortSettingsPage function \n");
    if (pHttp->i4Method == ISS_GET)
    {
        IssBcmProcessVlanSubnetPerPortSettingsPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssBcmProcessVlanSubnetPerPortSettingsPagePost (pHttp);
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nWEB:Exit IssBcmProcessVlanSubnetPerPortSettingsPage function \n");
}

/*********************************************************************
*  Function Name : IssBcmProcessVlanSubnetPerPortSettingsPageGet 
*  Description   : This function processes the get request coming for the  
*                  Subnet Mac Map VLAN page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssBcmProcessVlanSubnetPerPortSettingsPageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    INT4                i4OutCome;
    tSNMP_MULTI_DATA_TYPE *pu1DataString = NULL;
    UINT4               u4SubnetMacMapAddr = 0;
    INT4                i4NextPort = 0;
    INT4                i4VlanPortSubnetMapVid = 0;
    INT4                i4VlanPortSubnetMapARPOption = 0;
    UINT4               u4CId;

    ISS_TRC (INIT_SHUT_TRC,
             "\nWEB:Entry IssBcmProcessVlanSubnetPerPortSettingsPageGet function \n");
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    WebnmRegisterLock (pHttp, VlanLock, VlanUnLock);
    VLAN_LOCK ();

    i4OutCome = WebnmApiGetContextId (pHttp, &u4CId);
    if (i4OutCome != ENM_SUCCESS)
    {
        VLAN_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Context Selection Failed");
        ISS_TRC (ALL_FAILURE_TRC, "\nWEB:Failed to retrieve Web ContextId  \n");
        return;
    }
    i4NextPort = 0;
    i4OutCome = nmhGetFirstIndexDot1qFutureVlanPortSubnetMapTable
        (&i4NextPort, &u4SubnetMacMapAddr);

    if (i4OutCome == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to retrieve SubnetMac Map Addr  for the port %d \n",
                      i4NextPort);
        return;
    }
    if ((pu1DataString = WebnmAllocMultiData ()) == NULL)
    {
        VLAN_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Insufficient memory!!!");
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:Failed to  Allocate a Multidata from multidata pool \n");
        return;
    }

    u4Temp = pHttp->i4Write;
    do
    {
        pHttp->i4Write = u4Temp;

        pu1DataString->u4_ULongValue = u4SubnetMacMapAddr;
        WebnmSendToSocket (pHttp, "fsMIdot1qFutureVlanPortSubnetMapAddr_KEY",
                           pu1DataString, ENM_IPADDRESS);

        STRCPY (pHttp->au1KeyString, "fsMIdot1qFutureVlanPortSubnetMapVid_KEY");

        WebnmSendString (pHttp, pHttp->au1KeyString);

        nmhGetDot1qFutureVlanPortSubnetMapVid (i4NextPort, u4SubnetMacMapAddr,
                                               &i4VlanPortSubnetMapVid);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4VlanPortSubnetMapVid);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        STRCPY (pHttp->au1KeyString,
                "fsMIdot1qFutureVlanPortSubnetMapARPOption_KEY");

        WebnmSendString (pHttp, pHttp->au1KeyString);

        nmhGetDot1qFutureVlanPortSubnetMapARPOption (i4NextPort,
                                                     u4SubnetMacMapAddr,
                                                     &i4VlanPortSubnetMapARPOption);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                 i4VlanPortSubnetMapARPOption);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    while (nmhGetNextIndexDot1qFutureVlanPortSubnetMapTable
           (i4NextPort, &i4NextPort,
            u4SubnetMacMapAddr, &u4SubnetMacMapAddr) == SNMP_SUCCESS);

    VLAN_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    ISS_TRC (INIT_SHUT_TRC,
             "\nWEB:Exit IssBcmProcessVlanSubnetPerPortSettingsPageGet function \n");
}

/*********************************************************************
*  Function Name : IssBcmProcessVlanSubnetPerPortSettingsPagePost
*  Description   : This function processes the get request coming for the  
*                  Subnet Mac Map VLAN page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssBcmProcessVlanSubnetPerPortSettingsPagePost (tHttp * pHttp)
{
    UINT4               u4ErrCode = 0;
    INT4                i4RowStatus;
    INT4                i4OutCome;
    INT4                i4SubnetARPOption = 0;
    INT4                i4SubnetVlanId = 0;
    UINT4               u4CId;
    UINT2               u2LocalPort;
    UINT4               u4SubnetAddr = 0;
    ISS_TRC (INIT_SHUT_TRC,
             "\nWEB:Entry IssBcmProcessVlanSubnetPerPortSettingsPagePost  function \n");
    WebnmRegisterLock (pHttp, VlanLock, VlanUnLock);
    VLAN_LOCK ();
    i4OutCome = WebnmApiGetContextId (pHttp, &u4CId);
    if (i4OutCome != ENM_SUCCESS)
    {
        VLAN_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Context Selection Failed");
        ISS_TRC (ALL_FAILURE_TRC, "\nWEB:Failed to retrieve Web ContextId  \n");
        return;
    }
    u2LocalPort = 0;
    STRCPY (pHttp->au1KeyString, "fsMIdot1qFutureVlanPortSubnetMapAddr");
    HttpGetValuebyName (pHttp->au1KeyString, pHttp->au1Value,
                        pHttp->au1PostQuery);
    u4SubnetAddr = OSIX_HTONL (INET_ADDR (pHttp->au1Value));

    STRCPY (pHttp->au1KeyString, "fsMIdot1qFutureVlanPortSubnetMapVid");
    HttpGetValuebyName (pHttp->au1KeyString, pHttp->au1Value,
                        pHttp->au1PostQuery);
    i4SubnetVlanId = (INT4) ATOI ((CONST INT1 *) pHttp->au1Value);

    i4SubnetARPOption = 2;

    STRCPY (pHttp->au1KeyString, "ACTION");
    HttpGetValuebyName (pHttp->au1KeyString,
                        pHttp->au1Value, pHttp->au1PostQuery);
    if (nmhGetDot1qFutureVlanPortSubnetMapRowStatus
        ((INT4) u2LocalPort, u4SubnetAddr, &i4RowStatus) == SNMP_SUCCESS)
    {
        if (STRCMP (pHttp->au1Value, "Add") == 0)
        {
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Row Already Exist");
            ISS_TRC (ALL_FAILURE_TRC,
                     "\nWEB:Failed to Add a row-Row Already Exist \n");
            return;
        }
        if (STRCMP (pHttp->au1Value, "Apply") == 0)
        {
            i4RowStatus = ISS_NOT_IN_SERVICE;
        }
        else if (STRCMP (pHttp->au1Value, "Delete") == 0)
        {
            i4RowStatus = ISS_DESTROY;
        }
    }
    else
    {
        if (STRCMP (pHttp->au1Value, "Add") == 0)
        {
            i4RowStatus = ISS_CREATE_AND_WAIT;
        }
        else if (STRCMP (pHttp->au1Value, "Delete") == 0)
        {
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Entry Does Not Exist");
            ISS_TRC (ALL_FAILURE_TRC,
                     "\nWEB:Failed to Delete a row - Entry Does Not Exist  \n");
            return;
        }
    }

    if (nmhTestv2Dot1qFutureVlanPortSubnetMapRowStatus (&u4ErrCode,
                                                        (INT4) u2LocalPort,
                                                        u4SubnetAddr,
                                                        i4RowStatus) ==
        SNMP_FAILURE)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();

        IssSendError (pHttp, (CONST INT1 *) "Entry does not exist");
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test  Vlan Port SubnetMap RowStatus %d for SubnetAddr  of LocalPort %d              with error code as %d \n",
                      i4RowStatus, u2LocalPort, u4ErrCode);
        return;
    }

    if (i4RowStatus == ISS_CREATE_AND_WAIT)
    {

        if (nmhTestv2Dot1qFutureVlanPortSubnetMapVid (&u4ErrCode,
                                                      (INT4) u2LocalPort,
                                                      u4SubnetAddr,
                                                      i4SubnetVlanId) ==
            SNMP_FAILURE)
        {
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            printf ("The subnet vlan id is %d", i4SubnetVlanId);
            IssSendError (pHttp, (CONST INT1 *) "Vlan Id is not valid");
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test  Vlan Port SubnetMap VlanId %d for SubnetAddr  of LocalPort %d              with error code as %d \n",
                          i4SubnetVlanId, u2LocalPort, u4ErrCode);
            return;
        }

        if (nmhTestv2Dot1qFutureVlanPortSubnetMapARPOption (&u4ErrCode,
                                                            (INT4) u2LocalPort,
                                                            u4SubnetAddr,
                                                            i4SubnetARPOption)
            == SNMP_FAILURE)
        {
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "ARP Option could not be set");
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test  Vlan Port SubnetMap ARPOption %d for SubnetAddr  of LocalPort %d              with error code as %d \n",
                          i4SubnetARPOption, u2LocalPort, u4ErrCode);
            return;
        }

        if (nmhSetDot1qFutureVlanPortSubnetMapRowStatus ((INT4) u2LocalPort,
                                                         u4SubnetAddr,
                                                         i4RowStatus) ==
            SNMP_FAILURE)
        {
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to create Entry");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set  Vlan Port SubnetMap RowStatus %d for SubnetAddr  of LocalPort %d\n",
                          i4RowStatus, u2LocalPort);
            return;
        }

        i4RowStatus = ISS_ACTIVE;

        if (nmhSetDot1qFutureVlanPortSubnetMapVid
            ((INT4) u2LocalPort, u4SubnetAddr, i4SubnetVlanId) == SNMP_FAILURE)
        {
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Vlan ID is not valid");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set  Vlan Port SubnetMap VlanId %d for SubnetAddr of LocalPort %d\n",
                          i4SubnetVlanId, u2LocalPort);
            return;
        }

        if (nmhSetDot1qFutureVlanPortSubnetMapARPOption ((INT4) u2LocalPort,
                                                         u4SubnetAddr,
                                                         i4SubnetARPOption) ==
            SNMP_FAILURE)
        {
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "ARP Option not Valid");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set Vlan Port SubnetMap ARPOption %d for SubnetAddr  of LocalPort %d\n",
                          i4SubnetARPOption, u2LocalPort);
            return;
        }

        if (nmhSetDot1qFutureVlanPortSubnetMapRowStatus ((INT4) u2LocalPort,
                                                         u4SubnetAddr,
                                                         i4RowStatus) ==
            SNMP_FAILURE)
        {
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to  Create Entry");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set  Vlan Port SubnetMap RowStatus %d for SubnetAddr  of LocalPort %d\n",
                          i4RowStatus, u2LocalPort);
            return;
        }

    }

    else if (i4RowStatus == ISS_DESTROY)
    {
        if (nmhSetDot1qFutureVlanPortSubnetMapRowStatus ((INT4) u2LocalPort,
                                                         u4SubnetAddr,
                                                         i4RowStatus) ==
            SNMP_FAILURE)
        {
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to Delete The entry");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set Vlan Port SubnetMap after DESTROY RowStatus %d for SubnetAddr of LocalPort %d\n",
                          i4RowStatus, u2LocalPort);
            return;
        }

    }

    else
    {

        if (nmhTestv2Dot1qFutureVlanPortSubnetMapVid (&u4ErrCode,
                                                      (INT4) u2LocalPort,
                                                      u4SubnetAddr,
                                                      i4SubnetVlanId) ==
            SNMP_FAILURE)
        {
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to Edit The Entry");
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test  Vlan Port SubnetMap VlanId %d for SubnetAddr  of LocalPort %d              with error code as %d \n",
                          i4SubnetVlanId, u2LocalPort, u4ErrCode);
            return;
        }

        if (nmhTestv2Dot1qFutureVlanPortSubnetMapARPOption (&u4ErrCode,
                                                            (INT4) u2LocalPort,
                                                            u4SubnetAddr,
                                                            i4SubnetARPOption)
            == SNMP_FAILURE)
        {
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "ARP Option not Valid");
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test  Vlan Port SubnetMap ARPOption %d for SubnetAddr  of LocalPort %d              with error code as %d \n",
                          i4SubnetARPOption, u2LocalPort, u4ErrCode);
            return;
        }

        if (nmhSetDot1qFutureVlanPortSubnetMapRowStatus ((INT4) u2LocalPort,
                                                         u4SubnetAddr,
                                                         i4RowStatus) ==
            SNMP_FAILURE)
        {
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to Edit the Entry");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set  Vlan Port SubnetMap RowStatus %d for SubnetAddr  of LocalPort %d\n",
                          i4RowStatus, u2LocalPort);
            return;
        }
        if (nmhSetDot1qFutureVlanPortSubnetMapVid
            ((INT4) u2LocalPort, u4SubnetAddr, i4SubnetVlanId) == SNMP_FAILURE)
        {
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Vlan Id in not valid");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set  Vlan Port SubnetMap VlanId %d for SubnetAddr  of LocalPort %d\n",
                          i4SubnetVlanId, u2LocalPort);
            return;
        }

        if (nmhSetDot1qFutureVlanPortSubnetMapARPOption ((INT4) u2LocalPort,
                                                         u4SubnetAddr,
                                                         i4SubnetARPOption) ==
            SNMP_FAILURE)
        {
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "ARP option not valid");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set Vlan Port SubnetMap ARPOption %d for SubnetAddr  of LocalPort %d\n",
                          i4SubnetARPOption, u2LocalPort);
            return;
        }

        if (nmhSetDot1qFutureVlanPortSubnetMapRowStatus ((INT4) u2LocalPort,
                                                         u4SubnetAddr,
                                                         ISS_ACTIVE) ==
            SNMP_FAILURE)
        {
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Unable to edit the Entry");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set  Vlan Port SubnetMap RowStatus for SubnetAddr of LocalPort %d\n",
                          u2LocalPort);
            return;
        }

    }

    VlanReleaseContext ();
    VLAN_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    IssBcmProcessVlanSubnetPerPortSettingsPageGet (pHttp);
    ISS_TRC (INIT_SHUT_TRC,
             "\nWEB:Failed to IssBcmProcessVlanSubnetPerPortSettingsPagePost function \n");
}

/*********************************************************************
*  Function Name : IssBcmProcessIPFilterConfPageSet
*  Description   : This function processes the set request for the IP
*                 filter configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssBcmProcessIPFilterConfPageSet (tHttp * pHttp)
{
    INT4                i4AckBit = 0, i4RstBit = 0, i4TOS = 0, i4Status = 0;
    UINT4               u4SrcIpAddr = 0, u4SrcAddrMask = 0, u4DestIpAddr =
        0, u4DestAddrMask = 0;
    UINT4               u4SrcPortFrom = 0, u4SrcPortTo = 0, u4DstPortFrom = 0;
    UINT4               u4DstPortTo = 0, u4Priority = 0, u4Code = 0;
    UINT4               u4ErrorCode = 0, u4Protocol = 0, u4Type = 0;
    INT4                i4Action = 0, i4FilterNo = 0, i4GroupId = 0;
    INT4                i4Dscp = 0, i4Direction = 0;
    INT4                i4AddressType = 0;
    INT4                i4Stats = ACL_STAT_DISABLE;
    INT4                i4StatsOld = ACL_STAT_DISABLE;
    INT4                i4ClearStats = ISS_FALSE;
    UINT4               u4DstPrefix = 0;
    UINT4               u4SrcPrefix = 0;
    UINT4               u4FlowId = 0;
#ifdef QOSX_WANTED
    INT4                i4Storage = 0;
#endif
    tIp6Addr            SrcIp6Addr;
    tIp6Addr            DstIp6Addr;

    tSNMP_OCTET_STRING_TYPE SrcIpv6;
    tSNMP_OCTET_STRING_TYPE DstIpv6;

    tSNMP_OCTET_STRING_TYPE InPortList, OutPortList, RedirectPortList,
        InPortChannelList, OutPortChannelList;
    UINT1               u1InPortList[ISS_PORTLIST_LEN],
        u1OutPortList[ISS_PORTLIST_LEN], u1RedirectPortList[ISS_PORTLIST_LEN];
    UINT1               u1InPortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];
    UINT1               u1OutPortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];
    UINT1               u1Apply = 0, u1RowStatus = ISS_CREATE_AND_WAIT;

    tPortList           InTempPortList;
    tPortList           OutTempPortList;
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    /* Filter type can be L3_REDIRECT in IP Filter Page Set */
    INT4                i4GrpFilterType = ISS_L3_REDIRECT;
    /* Currently only Redirect to port is supported in BCM */
    INT4                i4GrpType = ISS_REDIRECT_TO_PORT;

    MEMSET (&SrcIp6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&DstIp6Addr, 0, sizeof (tIp6Addr));

    HttpGetValuebyName ((UINT1 *) "FltNo", pHttp->au1Value,
                        pHttp->au1PostQuery);
    i4FilterNo = ATOI (pHttp->au1Value);

    MEMSET (InTempPortList, 0, VLAN_PORT_LIST_SIZE);
    MEMSET (OutTempPortList, 0, VLAN_PORT_LIST_SIZE);

    ISS_TRC (INIT_SHUT_TRC,
             "\nWEB:Entry IssBcmProcessIPFilterConfPageSet function \n");
    /* Check whether the request is for ADD/DELETE */
    HttpGetValuebyName (ISS_ACTION, pHttp->au1Value, pHttp->au1PostQuery);

    ISS_LOCK ();
    /* check for delete */

    if (STRCMP (pHttp->au1Value, ISS_DELETE) == 0)
    {
        if (nmhTestv2IssExtL3FilterStatus
            (&u4ErrorCode, i4FilterNo, ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *) "IP filter not available.");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test L3 Filter Status for filter ID %d  with error code as %d \n",
                          i4FilterNo, u4ErrorCode);
            return;
        }

        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *) "IP filter not available.");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set L3 Filter Status for filter ID %d \n",
                          i4FilterNo);
            return;
        }

        ISS_UNLOCK ();

        IssBcmProcessIPFilterConfPageGet (pHttp);
        return;
    }

    if (STRCMP (pHttp->au1Value, ISS_APPLY) == 0)
    {
        u1Apply = 1;
        u1RowStatus = ISS_NOT_IN_SERVICE;
    }

    HttpGetValuebyName ((UINT1 *) "FltOption", pHttp->au1Value,
                        pHttp->au1PostQuery);
    i4Action = ATOI (pHttp->au1Value);

    HttpGetValuebyName ((UINT1 *) "SrcIp", pHttp->au1Value,
                        pHttp->au1PostQuery);
    if (STRLEN (pHttp->au1Value) != 0)
    {
        u4SrcIpAddr = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
    }

    HttpGetValuebyName ((UINT1 *) "SrcMask", pHttp->au1Value,
                        pHttp->au1PostQuery);
    if (STRLEN (pHttp->au1Value) != 0)
    {
        u4SrcAddrMask = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
    }

    HttpGetValuebyName ((UINT1 *) "DestIp", pHttp->au1Value,
                        pHttp->au1PostQuery);
    if (STRLEN (pHttp->au1Value) != 0)
    {
        u4DestIpAddr = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
    }

    HttpGetValuebyName ((UINT1 *) "DestMask", pHttp->au1Value,
                        pHttp->au1PostQuery);
    if (STRLEN (pHttp->au1Value) != 0)
    {
        u4DestAddrMask = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
    }

    HttpGetValuebyName ((UINT1 *) "InPrtList", pHttp->au1Value,
                        pHttp->au1PostQuery);
    MEMSET (u1InPortList, 0, ISS_PORTLIST_LEN);
    issDecodeSpecialChar (pHttp->au1Value);
    if ((STRLEN (pHttp->au1Value) != 0) && (STRCMP (pHttp->au1Value, "0") != 0))
    {
        if (IssPortListStringParser (pHttp->au1Value, u1InPortList) ==
            ENM_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *)
                          "Check the allowed in port list");
            ISS_TRC (ALL_FAILURE_TRC,
                     "\nWEB:Parse string failed for InPrtList \n");
            return;
        }
    }
    InPortList.i4_Length = ISS_PORTLIST_LEN;
    InPortList.pu1_OctetList = MEM_CALLOC (InPortList.i4_Length, 1, UINT1);
    if (InPortList.pu1_OctetList == NULL)
    {
        ISS_UNLOCK ();
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:Failed to Allocate memory for In PortList \n");
        return;
    }
    MEMCPY (InPortList.pu1_OctetList, u1InPortList, InPortList.i4_Length);

    HttpGetValuebyName ((UINT1 *) "OutPrtList", pHttp->au1Value,
                        pHttp->au1PostQuery);
    MEMSET (u1OutPortList, 0, ISS_PORTLIST_LEN);
    issDecodeSpecialChar (pHttp->au1Value);

    if ((STRLEN (pHttp->au1Value) != 0) && (STRCMP (pHttp->au1Value, "0") != 0))
    {
        if (IssPortListStringParser (pHttp->au1Value, u1OutPortList) ==
            ENM_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            IssSendError (pHttp, (CONST INT1 *)
                          "Check the allowed out port list");
            ISS_TRC (ALL_FAILURE_TRC,
                     "\nWEB: Parse string failed for In PortList\n");
            return;
        }
    }
    OutPortList.i4_Length = ISS_PORTLIST_LEN;
    OutPortList.pu1_OctetList = MEM_CALLOC (OutPortList.i4_Length, 1, UINT1);
    if (OutPortList.pu1_OctetList == NULL)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:Memory allocation for OutPortList gets null\n");
        return;
    }
    MEMCPY (OutPortList.pu1_OctetList, u1OutPortList, OutPortList.i4_Length);

    HttpGetValuebyName ((UINT1 *) "RedirectPrtList", pHttp->au1Value,
                        pHttp->au1PostQuery);
    MEMSET (u1RedirectPortList, 0, ISS_PORTLIST_LEN);
    issDecodeSpecialChar (pHttp->au1Value);

    if ((STRLEN (pHttp->au1Value) != 0) && (STRCMP (pHttp->au1Value, "0") != 0))
    {
        if (IssPortListStringParser (pHttp->au1Value, u1RedirectPortList) ==
            ENM_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            IssSendError (pHttp, (CONST INT1 *)
                          "Check the allowed Redirect port list");
            ISS_TRC (ALL_FAILURE_TRC,
                     "\nWEB:Parse string failed for Redirect PortList\n");
            return;
        }
    }
    MEMSET (&RedirectPortList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RedirectPortList.i4_Length = ISS_PORTLIST_LEN;
    RedirectPortList.pu1_OctetList =
        MEM_CALLOC (RedirectPortList.i4_Length, 1, UINT1);
    if (RedirectPortList.pu1_OctetList == NULL)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:Memory allocation for  RedirectPortList OctetList gets Null \n");
        return;
    }
    MEMCPY (RedirectPortList.pu1_OctetList, u1RedirectPortList,
            RedirectPortList.i4_Length);

    /* Determine the type of ACL from the filter number and process the SET
     * request
     */

    HttpGetValuebyName ((UINT1 *) "Protocol", pHttp->au1Value,
                        pHttp->au1PostQuery);
    u4Protocol = ATOI (pHttp->au1Value);

    if (u4Protocol == 4)
    {
        /*We need not install the L4 protocol vbalue for IP */
        u4Protocol = ISS_PROT_ANY;
    }
    if (u4Protocol == ISS_PROT_OTHER)
    {
        HttpGetValuebyName ((UINT1 *) "Other", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Protocol = ATOI (pHttp->au1Value);
        if (u4Protocol == 0)
        {
            u4Protocol = ISS_PROT_OTHER;
        }
    }
    /* For Processing ICMP options. */
    if (u4Protocol == 1)
    {
        HttpGetValuebyName ((UINT1 *) "MsgCode", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Code = ATOI (pHttp->au1Value);

        HttpGetValuebyName ((UINT1 *) "MsgType", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Type = ATOI (pHttp->au1Value);

        HttpGetValuebyName ((UINT1 *) "Priority", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Priority = ATOI (pHttp->au1Value);
    }
    else
    {

        HttpGetValuebyName ((UINT1 *) "Priority", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Priority = ATOI (pHttp->au1Value);

        HttpGetValuebyName ((UINT1 *) "Dscp", pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4Dscp = ATOI (pHttp->au1Value);

        HttpGetValuebyName ((UINT1 *) "Tos", pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4TOS = ATOI (pHttp->au1Value);

        /* Configure either Dscp or TOS */
        if ((i4TOS > 0) && (i4Dscp > 0))
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to configure both Dscp and TOS, "
                          "configure one at a time");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to configure TOS %d and Dscp %d \n",
                          i4TOS, i4Dscp);
            return;
        }
        else if (i4Dscp > 0)
        {
            i4TOS = ISS_TOS_INVALID;
        }
        else
        {
            i4Dscp = ISS_DSCP_INVALID;
        }
    }

    /* For Processing TCP options. */
    if (u4Protocol == 6)
    {
        HttpGetValuebyName ((UINT1 *) "AckBit", pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4AckBit = ATOI (pHttp->au1Value);

        HttpGetValuebyName ((UINT1 *) "RstBit", pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4RstBit = ATOI (pHttp->au1Value);

    }

    /* For Processing TCP/UDP options. */
    if (u4Protocol == 17 || u4Protocol == 6)
    {
        HttpGetValuebyName ((UINT1 *) "SrcPortMin", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4SrcPortFrom = ATOI (pHttp->au1Value);

        HttpGetValuebyName ((UINT1 *) "SrcPortMax", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4SrcPortTo = ATOI (pHttp->au1Value);

        HttpGetValuebyName ((UINT1 *) "DstPortMin", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4DstPortFrom = ATOI (pHttp->au1Value);

        HttpGetValuebyName ((UINT1 *) "DstPortMax", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4DstPortTo = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "DIRECTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Direction = ATOI (pHttp->au1Value);

    HttpGetValuebyName ((UINT1 *) "InPrtChannelList", pHttp->au1Value,
                        pHttp->au1PostQuery);
    MEMSET (u1InPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    issDecodeSpecialChar (pHttp->au1Value);
    if ((STRLEN (pHttp->au1Value) != 0) && (STRCMP (pHttp->au1Value, "0") != 0))
    {
        if (IssPortListStringParser (pHttp->au1Value, u1InPortChannelList) ==
            ENM_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            IssSendError (pHttp, (CONST INT1 *)
                          "Check the allowed in portchannel list");
            ISS_TRC (ALL_FAILURE_TRC,
                     "\nWEB:Parse string failed for InPrtchannelList \n");
            return;
        }
    }
    InPortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;
    InPortChannelList.pu1_OctetList =
        MEM_CALLOC (InPortChannelList.i4_Length, 1, UINT1);
    if (InPortChannelList.pu1_OctetList == NULL)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:Failed to Allocate memory for In PortChannelList \n");
        return;
    }
    MEMCPY (InPortChannelList.pu1_OctetList, u1InPortChannelList,
            InPortChannelList.i4_Length);

    HttpGetValuebyName ((UINT1 *) "OutPrtChannelList", pHttp->au1Value,
                        pHttp->au1PostQuery);
    MEMSET (u1OutPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    issDecodeSpecialChar (pHttp->au1Value);
    if ((STRLEN (pHttp->au1Value) != 0) && (STRCMP (pHttp->au1Value, "0") != 0))
    {
        if (IssPortListStringParser (pHttp->au1Value, u1OutPortChannelList) ==
            ENM_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            IssSendError (pHttp, (CONST INT1 *)
                          "Check the allowed Out portchannel list");
            ISS_TRC (ALL_FAILURE_TRC,
                     "\nWEB:Parse string failed for OutPrtchannelList \n");
            return;
        }
    }
    OutPortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;
    OutPortChannelList.pu1_OctetList =
        MEM_CALLOC (OutPortChannelList.i4_Length, 1, UINT1);
    if (OutPortChannelList.pu1_OctetList == NULL)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:Failed to Allocate memory for Out PortChannelList \n");
        return;
    }
    MEMCPY (OutPortChannelList.pu1_OctetList, u1OutPortChannelList,
            OutPortChannelList.i4_Length);

    STRCPY (pHttp->au1Name, "COUNTER");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Stats = ATOI (pHttp->au1Value);

    if (u1Apply == 1)
    {
        STRCPY (pHttp->au1Name, "CLEAR_COUNT");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4ClearStats = ATOI (pHttp->au1Value);

        if (i4ClearStats == ISS_TRUE)
        {
            if (nmhTestv2IssAclClearL3FilterStats (&u4ErrorCode, i4FilterNo,
                                                   i4ClearStats) ==
                SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                MEM_FREE (RedirectPortList.pu1_OctetList);
                MEM_FREE (InPortChannelList.pu1_OctetList);
                MEM_FREE (OutPortChannelList.pu1_OctetList);
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to clear IP filter statistics.");
                ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                              "\nWEB:Failed to test L3 Filter Clear Statistics %d for Filter ID %d with error code as %d\n",
                              i4ClearStats, i4FilterNo, u4ErrorCode);
                return;
            }
            if (nmhSetIssAclClearL3FilterStats (i4FilterNo, i4ClearStats)
                == SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                MEM_FREE (RedirectPortList.pu1_OctetList);
                MEM_FREE (InPortChannelList.pu1_OctetList);
                MEM_FREE (OutPortChannelList.pu1_OctetList);
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to clear IP filter statistics.");
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nWEB:Failed to clear L3 Filter Statistics %d for Filter ID %d \n",
                              i4ClearStats, i4FilterNo);
                return;
            }
        }
    }

    if (nmhTestv2IssExtL3FilterStatus (&u4ErrorCode, i4FilterNo,
                                       u1RowStatus) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);
        IssSendError (pHttp, (CONST INT1 *) "Invalid IP filter number.");
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to set  L3 Filter RowStatus %d for Filter ID %d with error code as %d\n",
                      u1RowStatus, i4FilterNo, u4ErrorCode);
        return;
    }

    if ((nmhGetIssExtL3FilterStatus (i4FilterNo, &i4Status)) == SNMP_FAILURE)
    {
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
    }
    if (i4Status == ISS_NOT_READY)
    {
        if ((InPortList.pu1_OctetList != NULL)
            && (nmhSetIssExtL3FilterInPortList (i4FilterNo, &InPortList) ==
                SNMP_FAILURE))
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *) "Unable to set In Port List.");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set InPortList for Filter ID %d\n",
                          i4FilterNo);
            return;
        }

        if ((OutPortList.pu1_OctetList != NULL)
            && (nmhSetIssExtL3FilterOutPortList (i4FilterNo, &OutPortList) ==
                SNMP_FAILURE))
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *) "Unable to set Out Port List.");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set OutPortList for Filter ID%d \n",
                          i4FilterNo);
            return;
        }
        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_ACTIVE) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            IssSendError (pHttp, (CONST INT1 *) "Since Both In Port & Out Port"
                          " List are empty, filter status will be Inactive");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set L3FilterStatus for Filter ID \n",
                          i4FilterNo);
            return;
        }

    }
    if ((u1RowStatus == ISS_CREATE_AND_WAIT)
        || (u1RowStatus == ISS_NOT_IN_SERVICE))
    {
        if (nmhSetIssExtL3FilterStatus (i4FilterNo, u1RowStatus) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            IssSendError (pHttp, (CONST INT1 *)
                          "Setting Invalid IP filter number.");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set L3Filter RowStatus %d for Filter ID \n",
                          u1RowStatus, i4FilterNo);
            return;
        }
    }

    HttpGetValuebyName ((UINT1 *) "AddressType", pHttp->au1Value,
                        pHttp->au1PostQuery);
    i4AddressType = ATOI (pHttp->au1Value);
    if (i4AddressType != 0)
    {
        if (nmhTestv2IssAclL3FilteAddrType
            (&u4ErrorCode, i4FilterNo, i4AddressType) == SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unable to set In Address Type Test Failed .");
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test  L3 Filter AddressType %d for Filter ID %d           with error code as %d\n",
                          i4AddressType, i4FilterNo, u4ErrorCode);
            return;
        }
        if (nmhSetIssAclL3FilteAddrType (i4FilterNo, i4AddressType) ==
            SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set In Address Type  .");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set  L3 Filter AddressType %d for Filter ID %d \n",
                          i4AddressType, i4FilterNo);
            return;
        }
    }

    if (i4AddressType == 2)
    {
        HttpGetValuebyName ((UINT1 *) "SrcIp", pHttp->au1Value,
                            pHttp->au1PostQuery);
        issDecodeSpecialChar (pHttp->au1Value);
        INET_ATON6 (pHttp->au1Value, &SrcIp6Addr);
        SrcIpv6.pu1_OctetList = MEM_MALLOC (sizeof (tIp6Addr), UINT1);
        MEMCPY (SrcIpv6.pu1_OctetList, &SrcIp6Addr, IP6_ADDR_SIZE);
        SrcIpv6.i4_Length = IP6_ADDR_SIZE;

        if ((nmhTestv2IssAclL3FilterSrcIpAddr
             (&u4ErrorCode, i4FilterNo, &SrcIpv6)) == SNMP_FAILURE)
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test  L3 Filter SrcIpv6 Address %s for Filter ID %d with error code as %d\n",
                          Ip6PrintAddr (&SrcIp6Addr), i4FilterNo, u4ErrorCode);
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set the filter with this "
                          "source IPV6 Address.");
            return;
        }

        if ((nmhSetIssAclL3FilterSrcIpAddr
             (i4FilterNo, &SrcIpv6) == SNMP_FAILURE))
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set  L3 Filter SrcIpv6 Address %s for Filter ID %d\n",
                          Ip6PrintAddr (&SrcIp6Addr), i4FilterNo);
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set the l3 Source IPV6 Address.");
            return;
        }

        HttpGetValuebyName ((UINT1 *) "DestIp", pHttp->au1Value,
                            pHttp->au1PostQuery);
        issDecodeSpecialChar (pHttp->au1Value);
        INET_ATON6 (pHttp->au1Value, &DstIp6Addr);
        DstIpv6.pu1_OctetList = MEM_MALLOC (sizeof (tIp6Addr), UINT1);
        MEMCPY (DstIpv6.pu1_OctetList, &DstIp6Addr, IP6_ADDR_SIZE);
        DstIpv6.i4_Length = IP6_ADDR_SIZE;

        if ((nmhTestv2IssAclL3FilterDstIpAddr
             (&u4ErrorCode, i4FilterNo, &DstIpv6) == SNMP_FAILURE))
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to Test L3 Filter DstIpv6 Address %s for Filter ID %d                     with error code as %d\n",
                          Ip6PrintAddr (&DstIp6Addr), i4FilterNo, u4ErrorCode);
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set the filter with "
                          "this Destination IPV6 Address.");
            return;
        }

        if ((nmhSetIssAclL3FilterDstIpAddr
             (i4FilterNo, &DstIpv6) == SNMP_FAILURE))
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set L3 Filter DstIpv6 Address %s for Filter ID %d\n",
                          Ip6PrintAddr (&DstIp6Addr), i4FilterNo);
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set the l3 Source Address Mask.");
            return;
        }

    }

    if ((i4Action != 0) && (nmhTestv2IssExtL3FilterAction
                            (&u4ErrorCode, i4FilterNo,
                             i4Action)) == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the l3 filter action.");
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test  L3 Filter Action %d for Filter ID %d                              with error code as %d\n",
                      i4Action, i4FilterNo, u4ErrorCode);
        return;
    }

    if (i4AddressType == 1)
    {
        if ((u4SrcIpAddr > 0) && (nmhTestv2IssExtL3FilterSrcIpAddr
                                  (&u4ErrorCode, i4FilterNo, u4SrcIpAddr))
            == SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set the filter with this "
                          "source IP Address.");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test  L3 Filter SrcIpAddr  for Filter ID %d                                   with error code as %d\n",
                          i4FilterNo, u4ErrorCode);
            return;
        }

        if ((u4SrcAddrMask > 0) && (nmhTestv2IssExtL3FilterSrcIpAddrMask
                                    (&u4ErrorCode, i4FilterNo, u4SrcAddrMask)
                                    == SNMP_FAILURE))
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set the filter with this "
                          "source Address Mask.");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test  L3 Filter SrcAddrMask for Filter ID %d                                        with error code as %d\n",
                          i4FilterNo, u4ErrorCode);
            return;
        }

        if ((u4DestIpAddr > 0) && (nmhTestv2IssExtL3FilterDstIpAddr
                                   (&u4ErrorCode, i4FilterNo, u4DestIpAddr)
                                   == SNMP_FAILURE))
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set the filter with this "
                          "Destination IP Address.");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test  L3 Filter DstIpAddr  for Filter ID %d                                        with error code as %d\n",
                          i4FilterNo, u4ErrorCode);
            return;
        }

        if ((u4DestAddrMask > 0) && (nmhTestv2IssExtL3FilterDstIpAddrMask
                                     (&u4ErrorCode, i4FilterNo, u4DestAddrMask)
                                     == SNMP_FAILURE))
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set the filter with this "
                          "Destination Address Mask.");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test  L3 Filter DestAddrMask  for Filter ID %d                                             with error code as %d\n",
                          i4FilterNo, u4ErrorCode);
            return;
        }
    }

    if ((InPortList.pu1_OctetList != NULL) && (nmhTestv2IssExtL3FilterInPortList
                                               (&u4ErrorCode, i4FilterNo,
                                                &InPortList) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *) "Unable to set this in port list.");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test  L3 Filter InPortList for Filter ID %d                                                  with error code as %d\n",
                      i4FilterNo, u4ErrorCode);
        return;
    }

    if ((OutPortList.pu1_OctetList != NULL)
        &&
        (nmhTestv2IssExtL3FilterOutPortList
         (&u4ErrorCode, i4FilterNo, &OutPortList) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set this Out port list.");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test  L3 Filter OutPortList for Filter ID %d                                                       with error code as %d\n",
                      i4FilterNo, u4ErrorCode);
        return;
    }

    if ((u4Protocol != 0) && (nmhTestv2IssExtL3FilterProtocol
                              (&u4ErrorCode, i4FilterNo, u4Protocol))
        == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the l3 filter protocol.");
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test  L3 Protocol %d for Filter ID %d                                                            with error code as %d\n",
                      u4Protocol, i4FilterNo, u4ErrorCode);
        return;
    }
    if (u4Protocol == 1)
    {
        if (nmhTestv2IssExtL3FilterMessageType
            (&u4ErrorCode, i4FilterNo, u4Type) == SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set ICMP MessageType.");
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test  L3 Filter Message Type %d for Filter ID %d                                      with error code as %d\n",
                          u4Type, i4FilterNo, u4ErrorCode);
            return;
        }

        if (nmhTestv2IssExtL3FilterMessageCode
            (&u4ErrorCode, i4FilterNo, u4Code) == SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set ICMP MessageCode.");
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test  L3 Filter Message code %d for Filter ID %d                                           with error code as %d\n",
                          u4Code, i4FilterNo, u4ErrorCode);
            return;
        }
    }
    else
    {
        if (i4Dscp != ISS_DSCP_INVALID)
        {
            if (nmhTestv2IssExtL3FilterDscp (&u4ErrorCode, i4FilterNo, i4Dscp)
                == SNMP_FAILURE)
            {
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                MEM_FREE (RedirectPortList.pu1_OctetList);
                MEM_FREE (InPortChannelList.pu1_OctetList);
                MEM_FREE (OutPortChannelList.pu1_OctetList);
                ISS_UNLOCK ();
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to set Filter Dscp.");
                ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                              "\nWEB:Failed to test  L3 Filter Dscp  %d for Filter ID %d                                   with error code as %d\n",
                              i4Dscp, i4FilterNo, u4ErrorCode);
                return;
            }
        }

        if (i4TOS != ISS_TOS_INVALID)
        {
            if (nmhTestv2IssExtL3FilterTos (&u4ErrorCode, i4FilterNo, i4TOS)
                == SNMP_FAILURE)
            {
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                MEM_FREE (RedirectPortList.pu1_OctetList);
                MEM_FREE (InPortChannelList.pu1_OctetList);
                MEM_FREE (OutPortChannelList.pu1_OctetList);
                ISS_UNLOCK ();
                IssSendError (pHttp, (CONST INT1 *) "Unable to set TCP TOS.");
                ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                              "\nWEB:Failed to test  L3 Filter TOS  %d for Filter ID %d                                        with error code as %d\n",
                              i4TOS, i4FilterNo, u4ErrorCode);
                return;
            }
        }
    }

    if ((u4Priority != 0) && (nmhTestv2IssExtL3FilterPriority
                              (&u4ErrorCode, i4FilterNo, u4Priority)
                              == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *) "Unable to set filter priority.");
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test  L3 Filter Priority  %d for Filter ID %d                                             with error code as %d\n",
                      u4Priority, i4FilterNo, u4ErrorCode);
        return;
    }

    if (u4Protocol == 6)
    {
        if (nmhTestv2IssExtL3FilterAckBit (&u4ErrorCode, i4FilterNo, i4AckBit)
            == SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *) "Unable to set TCP Ack Bit.");
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test  L3 Filter AckBit  %d for Filter ID %d                                                  with error code as %d\n",
                          i4AckBit, i4FilterNo, u4ErrorCode);
            return;
        }

        if (nmhTestv2IssExtL3FilterRstBit (&u4ErrorCode, i4FilterNo, i4RstBit)
            == SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *) "Unable to set TCP Rst Bit.");
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test  L3 Filter RstBit  %d for Filter ID %d                             with error code as %d\n",
                          i4RstBit, i4FilterNo, u4ErrorCode);
            return;
        }
    }
    if (u4Protocol == 17 || u4Protocol == 6)
    {
        if ((u4SrcPortFrom != 0) || (u4SrcPortTo != 0))
        {
            if (nmhTestv2IssExtL3FilterMinSrcProtPort (&u4ErrorCode, i4FilterNo,
                                                       u4SrcPortFrom) ==
                SNMP_FAILURE)
            {
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                MEM_FREE (RedirectPortList.pu1_OctetList);
                MEM_FREE (InPortChannelList.pu1_OctetList);
                MEM_FREE (OutPortChannelList.pu1_OctetList);
                ISS_UNLOCK ();
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to set TCP/UDP Src Port Start.");
                ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                              "\nWEB:Failed to test  L3 Filter Minimum SrcPort %d for Filter ID %d                                  with error code as %d\n",
                              u4SrcPortFrom, i4FilterNo, u4ErrorCode);
                return;
            }

            if (nmhTestv2IssExtL3FilterMaxSrcProtPort (&u4ErrorCode, i4FilterNo,
                                                       u4SrcPortTo) ==
                SNMP_FAILURE)
            {
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                MEM_FREE (RedirectPortList.pu1_OctetList);
                MEM_FREE (InPortChannelList.pu1_OctetList);
                MEM_FREE (OutPortChannelList.pu1_OctetList);
                ISS_UNLOCK ();
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to set TCP/UDP Src Port End.");
                ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                              "\nWEB:Failed to test  L3 Filter Maximum SrcPort %d for Filter ID %d                                       with error code as %d\n",
                              u4SrcPortTo, i4FilterNo, u4ErrorCode);
                return;
            }
        }

        if ((u4DstPortFrom != 0) || (u4DstPortTo != 0))
        {
            if (nmhTestv2IssExtL3FilterMinDstProtPort (&u4ErrorCode, i4FilterNo,
                                                       u4DstPortFrom) ==
                SNMP_FAILURE)
            {
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                MEM_FREE (RedirectPortList.pu1_OctetList);
                MEM_FREE (InPortChannelList.pu1_OctetList);
                MEM_FREE (OutPortChannelList.pu1_OctetList);
                ISS_UNLOCK ();
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to set TCP/UDP Dst Port Start.");
                ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                              "\nWEB:Failed to test  L3 Filter Minimum Dst Port %d for Filter ID %d                                       with error code as %d\n",
                              u4DstPortFrom, i4FilterNo, u4ErrorCode);
                return;
            }

            if (nmhTestv2IssExtL3FilterMaxDstProtPort (&u4ErrorCode, i4FilterNo,
                                                       u4DstPortTo) ==
                SNMP_FAILURE)
            {
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                MEM_FREE (RedirectPortList.pu1_OctetList);
                MEM_FREE (InPortChannelList.pu1_OctetList);
                MEM_FREE (OutPortChannelList.pu1_OctetList);
                ISS_UNLOCK ();
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to set TCP/UDP Dst Port End.");
                ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                              "\nWEB:Failed to test  L3 Filter Maximum Dst Port %d for Filter ID %d                                            with error code as %d\n",
                              u4DstPortTo, i4FilterNo, u4ErrorCode);
                return;
            }
        }
    }

    if (nmhTestv2IssExtL3FilterDirection (&u4ErrorCode, i4FilterNo, i4Direction)
        == SNMP_FAILURE)
    {
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Direction.");
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test  L3 Filter Direction %d for Filter ID %d                                  with error code as %d\n",
                      i4Direction, i4FilterNo, u4ErrorCode);
        return;
    }

    if ((InPortChannelList.pu1_OctetList != NULL)
        && nmhTestv2IssAclL3FilterInPortChannelList (&u4ErrorCode, i4FilterNo,
                                                     &InPortChannelList) ==
        SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *) "Unable to set this in port list.");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test  L3 Filter InPortChannelList for  Filter ID %d with error code as %d\n",
                      i4FilterNo, u4ErrorCode);
        return;
    }
    if ((OutPortChannelList.pu1_OctetList != NULL)
        && nmhTestv2IssAclL3FilterOutPortChannelList (&u4ErrorCode, i4FilterNo,
                                                      &OutPortChannelList) ==
        SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *) "Unable to set this in port list.");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test  L3 Filter OutPortChannelList for  Filter ID %d with error code as %d\n",
                      i4FilterNo, u4ErrorCode);
        return;
    }
    if ((i4Action != 0) && (nmhSetIssExtL3FilterAction (i4FilterNo, i4Action)
                            == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the l3 filter action.");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to set L3 Filter Action %d for Filter ID %d\n",
                      i4Action, i4FilterNo);
        return;
    }

    if (i4AddressType == 1)
    {
        if ((u4SrcIpAddr > 0) && (nmhSetIssExtL3FilterSrcIpAddr
                                  (i4FilterNo, u4SrcIpAddr) == SNMP_FAILURE))
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set the l3 Source Ip Address.");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set  L3 Filter SrcIpAddr  for Filter ID %d\n",
                          i4FilterNo);
            return;
        }

        if ((u4SrcAddrMask > 0) && (nmhSetIssExtL3FilterSrcIpAddrMask
                                    (i4FilterNo,
                                     u4SrcAddrMask) == SNMP_FAILURE))
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set the l3 Source Address Mask.");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set  L3 Filter SrcAddrMask  for Filter ID %d\n",
                          i4FilterNo);
            return;
        }

        if ((u4DestIpAddr > 0) && (nmhSetIssExtL3FilterDstIpAddr
                                   (i4FilterNo, u4DestIpAddr) == SNMP_FAILURE))
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set the l3 Destination Ip Address.");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set  L3 Filter Dst IpAddr  for Filter ID %d\n",
                          i4FilterNo);
            return;
        }

        if ((u4DestAddrMask > 0) && (nmhSetIssExtL3FilterDstIpAddrMask
                                     (i4FilterNo,
                                      u4DestAddrMask) == SNMP_FAILURE))
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set the l3 Destination Address Mask.");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set  L3 Filter Dst AddrMask  for Filter ID %d\n",
                          i4FilterNo);
            return;
        }
    }
    if ((InPortList.pu1_OctetList != NULL) && (nmhSetIssExtL3FilterInPortList
                                               (i4FilterNo, &InPortList)
                                               == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *) "Unable to set In Port List.");
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to set L3Filter InPortList for Filter ID%d \n",
                      i4FilterNo);
        return;
    }

    if ((OutPortList.pu1_OctetList != NULL) && (nmhSetIssExtL3FilterOutPortList
                                                (i4FilterNo, &OutPortList)
                                                == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *) "Unable to set Out Port List.");
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to set L3Filter OutPortList for Filter ID%d\n",
                      i4FilterNo);
        return;
    }

    if ((u4Protocol != 0) && (nmhSetIssExtL3FilterProtocol
                              (i4FilterNo, u4Protocol) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *) "Unable to set filter priority.");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to set L3Filter Protocol%d for Filter ID%d\n",
                      u4Protocol, i4FilterNo);
        return;
    }
    if (u4Protocol == 1)
    {
        if (nmhSetIssExtL3FilterMessageType (i4FilterNo, u4Type) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set ICMP MessageType.");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set L3Filter ICMP MessageType%d for Filter ID%d\n",
                          u4Type, i4FilterNo);
            return;
        }

        if (nmhSetIssExtL3FilterMessageCode (i4FilterNo, u4Code) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set ICMP MessageCode.");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set L3Filter ICMP Message Code%d for Filter ID%d\n",
                          u4Code, i4FilterNo);
            return;
        }
    }
    else
    {
        if (i4Dscp != ISS_DSCP_INVALID)
        {
            if (nmhSetIssExtL3FilterDscp (i4FilterNo, i4Dscp) == SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                MEM_FREE (RedirectPortList.pu1_OctetList);
                MEM_FREE (InPortChannelList.pu1_OctetList);
                MEM_FREE (OutPortChannelList.pu1_OctetList);
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to set Filter Dscp.");
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nWEB:Failed to set L3Filter Dscp%d for Filter ID%d\n",
                              i4Dscp, i4FilterNo);
                return;
            }
        }

        if (i4TOS != ISS_TOS_INVALID)
        {
            if (nmhSetIssExtL3FilterTos (i4FilterNo, i4TOS) == SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                MEM_FREE (RedirectPortList.pu1_OctetList);
                MEM_FREE (InPortChannelList.pu1_OctetList);
                MEM_FREE (OutPortChannelList.pu1_OctetList);
                IssSendError (pHttp, (CONST INT1 *) "Unable to set TCP TOS.");
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nWEB:Failed to set L3Filter TOS%d for Filter ID%d\n",
                              i4TOS, i4FilterNo);
                return;
            }
        }
    }

    if ((u4Priority != 0) && (nmhSetIssExtL3FilterPriority
                              (i4FilterNo, u4Priority) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *) "Unable to set filter priority.");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to set L3Filter Priority%d for Filter ID%d\n",
                      u4Priority, i4FilterNo);
        return;
    }

    if (u4Protocol == 6)
    {
        if (nmhSetIssExtL3FilterAckBit (i4FilterNo, i4AckBit) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set TCP Ack Bit.");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set L3Filter AckBit%d for Filter ID%d\n",
                          i4AckBit, i4FilterNo);
            return;
        }
        if (nmhSetIssExtL3FilterRstBit (i4FilterNo, i4RstBit) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set TCP Rst Bit.");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set L3Filter RstBit%d for Filter ID%d\n",
                          i4RstBit, i4FilterNo);
            return;
        }
    }
    if (u4Protocol == 17 || u4Protocol == 6)
    {
        if ((u4SrcPortFrom != 0) || (u4SrcPortTo != 0))
        {
            if (nmhSetIssExtL3FilterMinSrcProtPort (i4FilterNo, u4SrcPortFrom)
                == SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                MEM_FREE (RedirectPortList.pu1_OctetList);
                MEM_FREE (InPortChannelList.pu1_OctetList);
                MEM_FREE (OutPortChannelList.pu1_OctetList);
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to set TCP/UDP Src Port Start.");
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nWEB:Failed to set L3Filter Minimum Src port%d for Filter ID%d\n",
                              u4SrcPortFrom, i4FilterNo);
                return;
            }

            if (nmhSetIssExtL3FilterMaxSrcProtPort (i4FilterNo, u4SrcPortTo)
                == SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                MEM_FREE (RedirectPortList.pu1_OctetList);
                MEM_FREE (InPortChannelList.pu1_OctetList);
                MEM_FREE (OutPortChannelList.pu1_OctetList);
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to set TCP/UDP Src Port End.");
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nWEB:Failed to set L3Filter Maximum Src port%d for Filter ID%d\n",
                              u4SrcPortTo, i4FilterNo);
                return;
            }
        }

        if ((u4DstPortFrom != 0) || (u4DstPortTo != 0))
        {
            if (nmhSetIssExtL3FilterMinDstProtPort (i4FilterNo, u4DstPortFrom)
                == SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                MEM_FREE (RedirectPortList.pu1_OctetList);
                MEM_FREE (InPortChannelList.pu1_OctetList);
                MEM_FREE (OutPortChannelList.pu1_OctetList);
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to set TCP/UDP Dst Port Start.");
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nWEB:Failed to set L3Filter Minimum Dst port%d for Filter ID%d\n",
                              u4DstPortFrom, i4FilterNo);
                return;
            }

            if (nmhSetIssExtL3FilterMaxDstProtPort (i4FilterNo, u4DstPortTo)
                == SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                MEM_FREE (RedirectPortList.pu1_OctetList);
                MEM_FREE (InPortChannelList.pu1_OctetList);
                MEM_FREE (OutPortChannelList.pu1_OctetList);
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to set TCP/UDP Dst Port End.");
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nWEB:Failed to set L3Filter Maximum Dst port%d for Filter ID%d\n",
                              u4DstPortTo, i4FilterNo);
                return;
            }
        }
    }

    HttpGetValuebyName ((UINT1 *) "DstPrefix", pHttp->au1Value,
                        pHttp->au1PostQuery);
    u4DstPrefix = ATOI (pHttp->au1Value);
    if (nmhTestv2IssAclL3FilterDstIpAddrPrefixLength
        (&u4ErrorCode, i4FilterNo, u4DstPrefix) == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set In Dst Prefix Length Test Failed.");
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test  L3 Filter DstIpAddr Prefix Length %d for Filter ID %dwith error code as %d\n",
                      u4DstPrefix, i4FilterNo, u4ErrorCode);
        return;
    }

    if ((u4DstPrefix != 0)
        &&
        (nmhSetIssAclL3FilterDstIpAddrPrefixLength (i4FilterNo, u4DstPrefix)
         == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set In Dst Prefix Length .");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to set L3Filter DstIpAddr Prefix Length%d for Filter ID%d\n",
                      u4DstPrefix, i4FilterNo);
        return;
    }

    HttpGetValuebyName ((UINT1 *) "SrcPrefix", pHttp->au1Value,
                        pHttp->au1PostQuery);
    u4SrcPrefix = ATOI (pHttp->au1Value);
    if (nmhTestv2IssAclL3FilterSrcIpAddrPrefixLength
        (&u4ErrorCode, i4FilterNo, u4SrcPrefix) == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set In Src Prefix Length Test Failed .");
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test  L3 Filter Src IpAddr Prefix Length %d for Filter ID %dwith      error code as %d\n",
                      u4SrcPrefix, i4FilterNo, u4ErrorCode);
        return;
    }
    if ((u4SrcPrefix != 0) && (nmhSetIssAclL3FilterSrcIpAddrPrefixLength
                               (i4FilterNo, u4SrcPrefix) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set In Src Prefix Length.");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to set L3Filter Src IpAddr Prefix Length%d for Filter ID%d\n",
                      u4SrcPrefix, i4FilterNo);
        return;
    }

    if (i4AddressType == 2)
    {
        HttpGetValuebyName ((UINT1 *) "FlowId", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4FlowId = ATOI (pHttp->au1Value);

        if (nmhTestv2IssAclL3FilterFlowId
            (&u4ErrorCode, i4FilterNo, u4FlowId) == SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set In Flow Id  Test Failed .");
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test  L3 Filter Flow Id %d for Filter ID %dwith  error code as %d\n",
                          u4FlowId, i4FilterNo, u4ErrorCode);
            return;
        }

        if (nmhSetIssAclL3FilterFlowId (i4FilterNo, u4FlowId) == SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *) "Unable to set In Flow Id.");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set L3Filter Flow Id%d for Filter ID%d\n",
                          u4FlowId, i4FilterNo);
            return;
        }
    }

    if (nmhSetIssExtL3FilterDirection (i4FilterNo, i4Direction) == SNMP_FAILURE)
    {
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set Direction.");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to set L3Filter Direction%d for Filter ID%d\n",
                      i4Direction, i4FilterNo);
        return;
    }

    if ((InPortChannelList.pu1_OctetList != NULL)
        &&
        (nmhSetIssAclL3FilterInPortChannelList (i4FilterNo, &InPortChannelList)
         == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to set In Port channelList.");
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to set L3Filter InPortChannelList for Filter ID%d \n",
                      i4FilterNo);
        return;
    }
    if ((OutPortChannelList.pu1_OctetList != NULL)
        &&
        (nmhSetIssAclL3FilterOutPortChannelList
         (i4FilterNo, &OutPortChannelList) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to set In Port channelList.");
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to set L3Filter OutPortChannelList for Filter ID%d \n",
                      i4FilterNo);
        return;
    }
#ifdef QOSX_WANTED
    HttpGetValuebyName ((UINT1 *) "Storage", pHttp->au1Value,
                        pHttp->au1PostQuery);
    i4Storage = ATOI (pHttp->au1Value);
    if (nmhTestv2DiffServMultiFieldClfrStorage
        (&u4ErrorCode, i4FilterNo, i4Storage) == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *) "Unable to set In Storage.");
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test DiffServ MultiField Storage %d for Filter ID %dwith  error code as %d\n",
                      i4Storage, i4FilterNo, u4ErrorCode);
        return;
    }

    if (nmhSetDiffServMultiFieldClfrStorage ((UINT4) i4FilterNo, i4Storage) ==
        SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *) "Unable to set In Storage.");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to set DiffServ MultiField Storage %d for Filter ID %d\n",
                      i4Storage, i4FilterNo);
        return;
    }
#endif

    if (i4Action == ISS_REDIRECT_TO)
    {
        nmhGetIssRedirectInterfaceGrpIdNextFree ((UINT4 *) &i4GroupId);
        if (i4GroupId == 0)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            IssSendError (pHttp, (INT1 *) "Check the Group Id Value");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to Retrieve Redirect Interface for Group ID%d \n",
                          i4GroupId);
            return;
        }
        if (nmhTestv2IssRedirectInterfaceGrpStatus (&u4ErrorCode, i4GroupId,
                                                    ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            IssSendError (pHttp,
                          (INT1 *) "Unable to create the redirect row status");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test  Redirect Interfacestatus for Group ID %d with error code as %d\n",
                          i4GroupId, u4ErrorCode);
            return;
        }

        if (nmhSetIssRedirectInterfaceGrpStatus (i4GroupId, ISS_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            IssSendError (pHttp,
                          (INT1 *) "Unable to create the redirect row status");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set Redirect Interface status for Group Id%d \n",
                          i4GroupId);
            return;
        }

        if (nmhTestv2IssRedirectInterfaceGrpFilterId
            (&u4ErrorCode, i4GroupId, i4FilterNo) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            IssSendError (pHttp, (INT1 *) "Check the Filter Id value");
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test Redirect Interface filter GrpId %d for Filter ID %dwith  error code as %     d\n",
                          u4FlowId, i4FilterNo, u4ErrorCode);
            return;
        }

        if (nmhSetIssRedirectInterfaceGrpFilterId
            (i4GroupId, i4FilterNo) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            IssSendError (pHttp, (INT1 *) "Unable to set the Filter Id value");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set Redirect Interface filter GrpId %d for Filter ID%d\n",
                          i4GroupId, i4FilterNo);
            return;
        }
        if (nmhTestv2IssRedirectInterfaceGrpFilterType
            (&u4ErrorCode, i4GroupId, i4GrpFilterType) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            IssSendError (pHttp, (INT1 *) "Check the Filter Type value");
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test Redirect Interface GrpFilterType %d for Filter ID %d  with  error code as %d\n",
                          i4GrpFilterType, i4FilterNo, u4ErrorCode);
            return;
        }

        if (nmhSetIssRedirectInterfaceGrpFilterType
            (i4GroupId, i4GrpFilterType) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            IssSendError (pHttp,
                          (INT1 *) "Unable to set the Filter Type value");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set Redirect Interface GrpFilterType %d for Filter ID%d\n",
                          i4GrpFilterType, i4FilterNo);
            return;
        }
        if (nmhTestv2IssRedirectInterfaceGrpType
            (&u4ErrorCode, i4GroupId, i4GrpType) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            IssSendError (pHttp, (INT1 *) "Check the Interface Grp type value");
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test Redirect Interface GrpFilterType %d for Filter ID %d           with  error code as %d\n",
                          i4GrpFilterType, i4FilterNo, u4ErrorCode);
            return;
        }

        if (nmhSetIssRedirectInterfaceGrpType (i4GroupId, i4GrpType) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            IssSendError (pHttp,
                          (INT1 *)
                          "Unable to set the Interface Grp type value");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set Redirect Interface GrpType %d for Filter ID%d\n",
                          i4GrpType, i4FilterNo);
            return;
        }
        if (nmhTestv2IssRedirectInterfaceGrpPortList
            (&u4ErrorCode, i4GroupId, &RedirectPortList) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            IssSendError (pHttp, (INT1 *) "Check the Port List value");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test Redirect Interface Group PortList  for Filter ID %d                with  error code as %d\n",
                          i4FilterNo, u4ErrorCode);
            return;
        }

        if ((RedirectPortList.pu1_OctetList != NULL)
            &&
            (nmhSetIssRedirectInterfaceGrpPortList
             (i4GroupId, &RedirectPortList) == SNMP_FAILURE))
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssRedirectInterfaceGrpStatus (i4GroupId, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set Redirect Port List.");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set Redirect Interface Group PortList  for Filter ID%d\n",
                          i4FilterNo);
            return;
        }
        if (i4GrpFilterType == ISS_L3_REDIRECT)
        {
            pIssL3FilterEntry = IssExtGetL3FilterEntry (i4FilterNo);
            pIssL3FilterEntry->RedirectIfGrp.u4RedirectGrpId = i4GroupId;
        }
    }
    if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_ACTIVE) == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the Filter Status as Active,"
                      "Check In/Out Port Lists.");
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to set L3 Filter Status for Filter ID%d\n",
                      i4FilterNo);
        return;
    }
    if (i4Action == ISS_REDIRECT_TO)
    {
        nmhGetIssAclL3FilterRedirectId (i4FilterNo, &i4GroupId);
        if (nmhSetIssRedirectInterfaceGrpStatus (i4GroupId, ISS_ACTIVE) ==
            SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssRedirectInterfaceGrpStatus (i4GroupId, ISS_DESTROY);
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set the Redirect Grp status.");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set Redirect Interface Group Status for Filter ID%d\n",
                          i4FilterNo);
            return;
        }
    }
    /* Enable ACL Counter if required */
    if ((nmhGetIssAclL3FilterStatsEnabledStatus
         (i4FilterNo, &i4StatsOld)) == SNMP_SUCCESS)
    {
        if (i4StatsOld != i4Stats)
        {
            if ((nmhTestv2IssAclL3FilterStatsEnabledStatus
                 (&u4ErrorCode, i4FilterNo, i4Stats)) == SNMP_FAILURE)
            {
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                MEM_FREE (RedirectPortList.pu1_OctetList);
                MEM_FREE (InPortChannelList.pu1_OctetList);
                MEM_FREE (OutPortChannelList.pu1_OctetList);
                ISS_UNLOCK ();
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to set Filter Statistics Status.");
                ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                              "\nWEB:Failed to test L3 Filter Statistics Status %d for Filter ID %d with error code as %d\n",
                              i4Stats, i4FilterNo, u4ErrorCode);
                return;
            }

            if ((nmhSetIssAclL3FilterStatsEnabledStatus (i4FilterNo, i4Stats))
                == SNMP_FAILURE)
            {
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                MEM_FREE (RedirectPortList.pu1_OctetList);
                MEM_FREE (InPortChannelList.pu1_OctetList);
                MEM_FREE (OutPortChannelList.pu1_OctetList);
                ISS_UNLOCK ();
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to set Filter Statistics Status.");
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nWEB:Failed to Set L3 Filter Statistics Status %d for Filter ID %d\n",
                              i4Stats, i4FilterNo);
                return;
            }
        }
    }
    else
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to Retrieve L3 Filter Statistics Status %d for Filter ID %d\n",
                      i4StatsOld, i4FilterNo);
    }
    ISS_UNLOCK ();

    MEM_FREE (InPortList.pu1_OctetList);
    MEM_FREE (OutPortList.pu1_OctetList);
    MEM_FREE (RedirectPortList.pu1_OctetList);
    MEM_FREE (InPortChannelList.pu1_OctetList);
    MEM_FREE (OutPortChannelList.pu1_OctetList);
    IssBcmProcessIPFilterConfPageGet (pHttp);
    ISS_TRC (INIT_SHUT_TRC,
             "\nWEB:Exit IssBcmProcessIPFilterConfPageSet function \n");
}

/*********************************************************************
*  Function Name : IssBcmProcessIPFilterConfPageGet
*  Description   : This function processes the get request for the IP
*                  filter configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssBcmProcessIPFilterConfPageGet (tHttp * pHttp)
{
    INT4                i4FilterNo, i4CurrentFilterNo, i4RetVal, i4OutCome,
        i4GroupId = 0;
    INT4                i4Protocol;
    UINT4               u4RetVal, u4Temp;
    tSNMP_OCTET_STRING_TYPE *InPortList = NULL, *OutPortList =
        NULL, *RedirectPortList = NULL, *InPortChannelList =
        NULL, *OutPortChannelList = NULL;
    UINT1              *pu1String;
    INT4                i4AddressType = 0;
    UINT4               u4DstPrefix = 0;
    UINT4               u4SrcPrefix = 0;
    UINT4               u4FlowId = 0;
#ifdef QOSX_WANTED
    INT4                i4Storage = 0;
#endif
    UINT4               au4SrcTemp[4];
    UINT4               au4DstTemp[4];

    UINT1               au1String[ISS_MAX_LEN];
    tIp6Addr            SrcIp6Addr;
    tIp6Addr            DstIp6Addr;
    tSNMP_OCTET_STRING_TYPE SrcIpAddrOctet;
    tSNMP_OCTET_STRING_TYPE DstIpAddrOctet;

    pu1String = &au1String[0];
    SrcIpAddrOctet.pu1_OctetList = (UINT1 *) au4SrcTemp;
    DstIpAddrOctet.pu1_OctetList = (UINT1 *) au4DstTemp;

    MEMSET (&SrcIp6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&DstIp6Addr, 0, sizeof (tIp6Addr));
    MEMSET (au1String, 0, ISS_ADDR_LEN);

    tPortList           InTempPortList, InTempPortChannelList,
        OutTempPortChannelList;
    tPortList           OutTempPortList, RedirectTempPortList;
    tSNMP_MULTI_DATA_TYPE *pu1DataString = NULL;
    MEMSET (InTempPortList, 0, VLAN_PORT_LIST_SIZE);
    MEMSET (OutTempPortList, 0, VLAN_PORT_LIST_SIZE);
    MEMSET (RedirectTempPortList, 0, VLAN_PORT_LIST_SIZE);
    MEMSET (InTempPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    MEMSET (OutTempPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);

    ISS_TRC (INIT_SHUT_TRC,
             "\nWEB:Entry  IssBcmProcessIPFilterConfPageGet function \n");
    if ((pu1DataString = WebnmAllocMultiData ()) == NULL)
    {
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:Failed to Allocate a Multidata from multidata pool \n");
        return;
    }

#ifdef QOSX_WANTED
    STRCPY (pHttp->ai1HtmlName, "bcm_ip_stdqosfilter.html");

    if (HttpReadhtml (pHttp) == ENM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:Failed to copy the html file content \n");
        return;
    }
#endif

    pHttp->i4Write = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = pHttp->i4Write;

    WebnmRegisterLock (pHttp, IssLock, IssUnLock);

    ISS_LOCK ();

    i4OutCome = (INT4) (nmhGetFirstIndexIssExtL3FilterTable (&i4FilterNo));
    if (i4OutCome == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to retrievere First Index L3 Filtertable for filter ID %d\n",
                      i4FilterNo);
        return;
    }

    InPortList = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PORTLIST_LEN);

    if (InPortList == NULL)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:Failed to Allocate memory for  InPortList\n");
        return;
    }

    OutPortList = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PORTLIST_LEN);
    if (OutPortList == NULL)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (InPortList);
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:Failed to Allocate memory for OutPortList \n");
        return;
    }

    RedirectPortList = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PORTLIST_LEN);
    if (RedirectPortList == NULL)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (InPortList);
        free_octetstring (OutPortList);
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:Failed to  Allocate memory for RedirectPortList\n");
        return;
    }

    InPortChannelList = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PORTLIST_LEN);

    if (InPortChannelList == NULL)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (InPortList);
        free_octetstring (OutPortList);
        free_octetstring (RedirectPortList);
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:Failed to Allocate memory for  InPortChannelList\n");
        return;
    }
    OutPortChannelList = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PORTLIST_LEN);

    if (OutPortChannelList == NULL)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (InPortList);
        free_octetstring (OutPortList);
        free_octetstring (RedirectPortList);
        free_octetstring (InPortChannelList);
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:Failed to Allocate memory for  OutPortChannelList\n");
        return;
    }
    do
    {
        pHttp->i4Write = u4Temp;

        /* Display only extended lists */
        if (i4FilterNo >= ACL_EXTENDED_START)
        {
            STRCPY (pHttp->au1KeyString, "FILTER_NUMBER");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4FilterNo);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, pHttp->au1KeyString);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "ACTION");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssExtL3FilterAction (i4FilterNo, &i4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "ADDRESS_TYPE");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssAclL3FilteAddrType (i4FilterNo, &i4AddressType);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4AddressType);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            if (i4AddressType == 2)
            {

                STRCPY (pHttp->au1KeyString, "SOURCE_IP_ADDRESS");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssAclL3FilterSrcIpAddr (i4FilterNo, &SrcIpAddrOctet);
                MEMCPY (&SrcIp6Addr, SrcIpAddrOctet.pu1_OctetList,
                        IP6_ADDR_SIZE);
                STRCPY (pu1String,
                        Ip6PrintNtop ((tIp6Addr *) (VOID *) SrcIpAddrOctet.
                                      pu1_OctetList));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "SOURCE_MASK");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "None");
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "DESTINATION_IP_ADDRESS");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssAclL3FilterDstIpAddr (i4FilterNo, &DstIpAddrOctet);
                MEMCPY (&DstIp6Addr, DstIpAddrOctet.pu1_OctetList,
                        IP6_ADDR_SIZE);
                STRCPY (pu1String,
                        Ip6PrintNtop ((tIp6Addr *) (VOID *) DstIpAddrOctet.
                                      pu1_OctetList));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "DESTINATION_MASK");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "None");
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            }
            if (i4AddressType == 1)
            {
                STRCPY (pHttp->au1KeyString, "SOURCE_IP_ADDRESS");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterSrcIpAddr (i4FilterNo, &u4RetVal);
                WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "SOURCE_MASK");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterSrcIpAddrMask (i4FilterNo, &u4RetVal);
                WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "DESTINATION_IP_ADDRESS");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterDstIpAddr (i4FilterNo, &u4RetVal);
                WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "DESTINATION_MASK");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterDstIpAddrMask (i4FilterNo, &u4RetVal);
                WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            }
            MEMSET (InPortList->pu1_OctetList, 0, ISS_PORTLIST_LEN);
            nmhGetIssL3FilterInPortList (i4FilterNo,
                                         pu1DataString->pOctetStrValue);
            MEMSET (InTempPortList, 0, VLAN_PORT_LIST_SIZE);
            IssConvertToIfPortList (pHttp, pu1DataString->pOctetStrValue,
                                    InTempPortList);
            MEMCPY (pu1DataString->pOctetStrValue->pu1_OctetList,
                    InTempPortList, pu1DataString->pOctetStrValue->i4_Length);
            WebnmSendToSocket (pHttp, "IN_PORTS_LIST", pu1DataString,
                               ENM_PORT_LIST);

            MEMSET (OutPortList->pu1_OctetList, 0, ISS_PORTLIST_LEN);
            nmhGetIssL3FilterOutPortList (i4FilterNo,
                                          pu1DataString->pOctetStrValue);
            MEMSET (OutTempPortList, 0, VLAN_PORT_LIST_SIZE);
            IssConvertToIfPortList (pHttp, pu1DataString->pOctetStrValue,
                                    OutTempPortList);
            MEMCPY (pu1DataString->pOctetStrValue->pu1_OctetList,
                    OutTempPortList, pu1DataString->pOctetStrValue->i4_Length);
            WebnmSendToSocket (pHttp, "OUT_PORTS_LIST", pu1DataString,
                               ENM_PORT_LIST);

            nmhGetIssAclL3FilterRedirectId (i4FilterNo, &i4GroupId);
            MEMSET (RedirectPortList->pu1_OctetList, 0, ISS_PORTLIST_LEN);
            nmhGetIssRedirectInterfaceGrpPortList (i4GroupId,
                                                   pu1DataString->
                                                   pOctetStrValue);
            MEMSET (RedirectTempPortList, 0, VLAN_PORT_LIST_SIZE);
            IssConvertToIfPortList (pHttp, pu1DataString->pOctetStrValue,
                                    RedirectTempPortList);
            MEMCPY (pu1DataString->pOctetStrValue->pu1_OctetList,
                    RedirectTempPortList,
                    pu1DataString->pOctetStrValue->i4_Length);
            WebnmSendToSocket (pHttp, "REDIRECT_PORTS_LIST", pu1DataString,
                               ENM_PORT_LIST);

            STRCPY (pHttp->au1KeyString, "PROTOCOL");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssExtL3FilterProtocol (i4FilterNo, &i4Protocol);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Protocol);

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "OTHER");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Protocol);

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            if (i4Protocol == 1)
            {
                STRCPY (pHttp->au1KeyString, "CODE");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterMessageCode (i4FilterNo, &i4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "TYPE");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterMessageType (i4FilterNo, &i4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "PRIORITY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterPriority (i4FilterNo, &i4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            }
            else
            {
                STRCPY (pHttp->au1KeyString, "CODE");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                STRCPY (pHttp->au1DataString, "");
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "TYPE");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                STRCPY (pHttp->au1DataString, "");
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "PRIORITY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterPriority (i4FilterNo, &i4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "DSCP");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterDscp (i4FilterNo, &i4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "TOS");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterTos (i4FilterNo, &i4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            }

            if (i4Protocol == 6)
            {
                STRCPY (pHttp->au1KeyString, "ACK_BIT");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterAckBit (i4FilterNo, &i4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "RST_BIT");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterRstBit (i4FilterNo, &i4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }

            if (i4Protocol == 6 || i4Protocol == 17)
            {
                STRCPY (pHttp->au1KeyString, "SOURCE_PORT_START");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterMinSrcProtPort (i4FilterNo, &u4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "SOURCE_PORT_END");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterMaxSrcProtPort (i4FilterNo, &u4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "DESTINATION_PORT_START");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterMinDstProtPort (i4FilterNo, &u4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "DESTINATION_PORT_END");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterMaxDstProtPort (i4FilterNo, &u4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            }

            STRCPY (pHttp->au1KeyString, "DST_PREFIX");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssAclL3FilterDstIpAddrPrefixLength (i4FilterNo,
                                                       &u4DstPrefix);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4DstPrefix);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "SRC_PREFIX");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssAclL3FilterSrcIpAddrPrefixLength (i4FilterNo,
                                                       &u4SrcPrefix);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4SrcPrefix);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "FLOW_ID");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssAclL3FilterFlowId (i4FilterNo, &u4FlowId);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4FlowId);

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "DIRECTION");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssExtL3FilterDirection (i4FilterNo, &i4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            MEMSET (InPortChannelList->pu1_OctetList, 0,
                    ISS_PORT_CHANNEL_LIST_SIZE);
            nmhGetIssAclL3FilterInPortChannelList (i4FilterNo,
                                                   pu1DataString->
                                                   pOctetStrValue);
            MEMSET (InTempPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
            IssConvertToIfPortList (pHttp, pu1DataString->pOctetStrValue,
                                    InTempPortChannelList);
            MEMCPY (pu1DataString->pOctetStrValue->pu1_OctetList,
                    InTempPortChannelList,
                    pu1DataString->pOctetStrValue->i4_Length);
            WebnmSendToSocket (pHttp, "IN_PORTCHANNEL_LIST", pu1DataString,
                               ENM_PORT_LIST);

            MEMSET (OutPortChannelList->pu1_OctetList, 0,
                    ISS_PORT_CHANNEL_LIST_SIZE);
            nmhGetIssAclL3FilterOutPortChannelList (i4FilterNo,
                                                    pu1DataString->
                                                    pOctetStrValue);
            MEMSET (OutTempPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
            IssConvertToIfPortList (pHttp, pu1DataString->pOctetStrValue,
                                    OutTempPortChannelList);
            MEMCPY (pu1DataString->pOctetStrValue->pu1_OctetList,
                    OutTempPortChannelList,
                    pu1DataString->pOctetStrValue->i4_Length);
            WebnmSendToSocket (pHttp, "OUT_PORTCHANNEL_LIST", pu1DataString,
                               ENM_PORT_LIST);

            STRCPY (pHttp->au1KeyString, "COUNTER");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssAclL3FilterStatsEnabledStatus (i4FilterNo, &i4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "MATCH_COUNT");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssAclL3FilterMatchCount (i4FilterNo, &u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "CLEAR_COUNT");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssAclClearL3FilterStats (i4FilterNo, &i4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "MFC_STORAGE");
            WebnmSendString (pHttp, pHttp->au1KeyString);
#ifdef QOSX_WANTED
            nmhGetDiffServMultiFieldClfrStorage (i4FilterNo, &i4Storage);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", i4Storage);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
#endif
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        }
        i4CurrentFilterNo = i4FilterNo;
    }
    while (nmhGetNextIndexIssExtL3FilterTable
           (i4CurrentFilterNo, &i4FilterNo) == SNMP_SUCCESS);

    ISS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    free_octetstring (InPortList);
    free_octetstring (OutPortList);
    free_octetstring (RedirectPortList);
    free_octetstring (InPortChannelList);
    free_octetstring (OutPortChannelList);

    if (i4FilterNo >= ACL_EXTENDED_START)
    {
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nWEB:Exit  IssBcmProcessIPFilterConfPageGet function  \n");
}

/*********************************************************************
*  Function Name : IssBcmProcessIPStdFilterConfPageGet 
*  Description   : This function processes the get request for the IP
*                  standard filter configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssBcmProcessIPStdFilterConfPageGet (tHttp * pHttp)
{
    INT4                i4FilterNo, i4CurrentFilterNo, i4RetVal, i4OutCome,
        i4GroupId = 0;
    UINT4               u4RetVal, u4Temp;
    tSNMP_OCTET_STRING_TYPE *InPortList = NULL, *OutPortList =
        NULL, *RedirectPortList = NULL;
    tSNMP_MULTI_DATA_TYPE *pu1DataString = NULL;
    tPortList           InTempPortList;
    tPortList           OutTempPortList, RedirectTempPortList;
    UINT1              *pu1String;

    MEMSET (InTempPortList, 0, VLAN_PORT_LIST_SIZE);
    MEMSET (OutTempPortList, 0, VLAN_PORT_LIST_SIZE);
    MEMSET (RedirectTempPortList, 0, VLAN_PORT_LIST_SIZE);

    ISS_TRC (INIT_SHUT_TRC,
             "\nWEB:Entry IssBcmProcessIPStdFilterConfPageGet function \n");

    if ((pu1DataString = WebnmAllocMultiData ()) == NULL)
    {
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:Failed to  Allocate a Multidata from multidata pool \n");
        return;
    }

    pHttp->i4Write = 0;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = pHttp->i4Write;
    WebnmRegisterLock (pHttp, IssLock, IssUnLock);

    ISS_LOCK ();

    i4OutCome = (INT4) (nmhGetFirstIndexIssExtL3FilterTable (&i4FilterNo));
    if ((i4OutCome == SNMP_FAILURE) || (i4FilterNo >= ACL_EXTENDED_START))
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to retrieve L3 Filter Table for Filter Id %d \n",
                      i4FilterNo);
        return;
    }

    InPortList = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PORTLIST_LEN);

    if (InPortList == NULL)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:Failed to Allocate memory for  InPortList\n");
        return;
    }

    OutPortList = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PORTLIST_LEN);

    if (OutPortList == NULL)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (InPortList);
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:Failed to  Allocate memory for OutPortList\n");
        return;
    }

    RedirectPortList = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PORTLIST_LEN);

    if (RedirectPortList == NULL)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (InPortList);
        free_octetstring (OutPortList);
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:Failed to Allocate memory for  RedirectPortList\n");
        return;
    }
    do
    {

        /* Display only if the ACL number is in the range 1 to 1000 */

        if (i4FilterNo < ACL_EXTENDED_START)
        {
            pHttp->i4Write = u4Temp;
            STRCPY (pHttp->au1KeyString, "FILTER_NUMBER");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4FilterNo);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, pHttp->au1KeyString);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "ACTION");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssExtL3FilterAction (i4FilterNo, &i4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "SOURCE_IP_ADDRESS");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssExtL3FilterSrcIpAddr (i4FilterNo, &u4RetVal);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "SOURCE_MASK");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssExtL3FilterSrcIpAddrMask (i4FilterNo, &u4RetVal);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "DESTINATION_IP_ADDRESS");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssExtL3FilterDstIpAddr (i4FilterNo, &u4RetVal);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "DESTINATION_MASK");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssExtL3FilterDstIpAddrMask (i4FilterNo, &u4RetVal);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetIssL3FilterInPortList (i4FilterNo,
                                         pu1DataString->pOctetStrValue);
            MEMSET (InTempPortList, 0, VLAN_PORT_LIST_SIZE);
            IssConvertToIfPortList (pHttp, pu1DataString->pOctetStrValue,
                                    InTempPortList);
            MEMCPY (pu1DataString->pOctetStrValue->pu1_OctetList,
                    InTempPortList, pu1DataString->pOctetStrValue->i4_Length);
            WebnmSendToSocket (pHttp, "IN_PORTS_LIST", pu1DataString,
                               ENM_PORT_LIST);

            nmhGetIssL3FilterOutPortList (i4FilterNo,
                                          pu1DataString->pOctetStrValue);
            MEMSET (OutTempPortList, 0, VLAN_PORT_LIST_SIZE);
            IssConvertToIfPortList (pHttp, pu1DataString->pOctetStrValue,
                                    OutTempPortList);
            MEMCPY (pu1DataString->pOctetStrValue->pu1_OctetList,
                    OutTempPortList, pu1DataString->pOctetStrValue->i4_Length);
            WebnmSendToSocket (pHttp, "OUT_PORTS_LIST", pu1DataString,
                               ENM_PORT_LIST);

            nmhGetIssAclL3FilterRedirectId (i4FilterNo, &i4GroupId);
            MEMSET (RedirectPortList->pu1_OctetList, 0, ISS_PORTLIST_LEN);
            nmhGetIssRedirectInterfaceGrpPortList (i4GroupId,
                                                   pu1DataString->
                                                   pOctetStrValue);
            MEMSET (RedirectTempPortList, 0, VLAN_PORT_LIST_SIZE);
            IssConvertToIfPortList (pHttp, pu1DataString->pOctetStrValue,
                                    RedirectTempPortList);
            MEMCPY (pu1DataString->pOctetStrValue->pu1_OctetList,
                    RedirectTempPortList,
                    pu1DataString->pOctetStrValue->i4_Length);
            WebnmSendToSocket (pHttp, "REDIRECT_PORTS_LIST", pu1DataString,
                               ENM_PORT_LIST);

            STRCPY (pHttp->au1KeyString, "DIRECTION");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssExtL3FilterDirection (i4FilterNo, &i4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "COUNTER");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssAclL3FilterStatsEnabledStatus (i4FilterNo, &i4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "MATCH_COUNT");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssAclL3FilterMatchCount (i4FilterNo, &u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "CLEAR_COUNT");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssAclClearL3FilterStats (i4FilterNo, &i4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        }

        i4CurrentFilterNo = i4FilterNo;
    }
    while (nmhGetNextIndexIssExtL3FilterTable
           (i4CurrentFilterNo, &i4FilterNo) == SNMP_SUCCESS);

    ISS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    free_octetstring (InPortList);
    free_octetstring (OutPortList);
    free_octetstring (RedirectPortList);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    ISS_TRC (INIT_SHUT_TRC,
             "\nWEB:Exit IssBcmProcessIPStdFilterConfPageGet function \n");
}

/*********************************************************************
*  Function Name : IssBcmProcessIPStdFilterConfPageSet 
*  Description   : This function processes the set request for the IP
*                 standard filter configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None 
*********************************************************************/
VOID
IssBcmProcessIPStdFilterConfPageSet (tHttp * pHttp)
{
    UINT4               u4SrcIpAddr = 0, u4SrcAddrMask = 0, u4DestIpAddr =
        0, u4DestAddrMask = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4Action = 0, i4FilterNo = 0, i4Status = 0, i4GroupId =
        0, i4Direction = 0;
    INT4                i4Stats = ACL_STAT_DISABLE;
    INT4                i4StatsOld = ACL_STAT_DISABLE;
    INT4                i4ClearStats = ISS_FALSE;
    tSNMP_OCTET_STRING_TYPE InPortList, OutPortList, RedirectPortList;
    UINT1               u1InPortList[ISS_PORTLIST_LEN],
        u1OutPortList[ISS_PORTLIST_LEN], u1RedirectPortList[ISS_PORTLIST_LEN];
    UINT1               u1Apply = 0, u1RowStatus = ISS_CREATE_AND_WAIT;
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    /* Filter type can be L3_REDIRECT in IP Filter Page Set */
    INT4                i4GrpFilterType = ISS_L3_REDIRECT;
    /* Currently only Redirect to port is supported in BCM */
    INT4                i4GrpType = ISS_REDIRECT_TO_PORT;

    ISS_TRC (INIT_SHUT_TRC,
             "\nWEB:Entry IssBcmProcessIPStdFilterConfPageSet function \n");

    STRCPY (pHttp->au1Name, "FILTER_NUMBER");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4FilterNo = ATOI (pHttp->au1Value);

    /* Check whether the request is for ADD/DELETE */
    STRCPY (pHttp->au1Name, "DELETE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    ISS_LOCK ();
    /* check for delete */
    if (STRCMP (pHttp->au1Value, "DELETE") == 0)
    {
        if (nmhTestv2IssExtL3FilterStatus
            (&u4ErrorCode, i4FilterNo, ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *)
                          "ACL with this number not available.");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test L3 FilterStatus for Filter ID %d with  error code as %d\n",
                          i4FilterNo, u4ErrorCode);
            return;
        }

        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *)
                          "ACL with this number not available.");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to Set L3 FilterStatus for Filter ID %d\n",
                          i4FilterNo);
            return;
        }
        ISS_UNLOCK ();
        IssBcmProcessIPStdFilterConfPageGet (pHttp);
        return;
    }

    if (STRCMP (pHttp->au1Value, "Apply") == 0)
    {
        u1Apply = 1;
        u1RowStatus = ISS_NOT_IN_SERVICE;
    }
    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Action = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "SOURCE_ADDRESS");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
        u4SrcIpAddr = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));

    STRCPY (pHttp->au1Name, "SOURCE_MASK");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
        u4SrcAddrMask = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));

    STRCPY (pHttp->au1Name, "DESTINATION_ADDRESS");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
        u4DestIpAddr = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));

    STRCPY (pHttp->au1Name, "DESTINATION_MASK");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
        u4DestAddrMask = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
    STRCPY (pHttp->au1Name, "IN_PORTS_LIST");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    MEMSET (u1InPortList, 0, ISS_PORTLIST_LEN);
    issDecodeSpecialChar (pHttp->au1Value);
    if ((STRLEN (pHttp->au1Value) != 0) && (STRCMP (pHttp->au1Value, "0") != 0))
    {
        if (IssPortListStringParser (pHttp->au1Value, u1InPortList) ==
            ENM_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *)
                          "Check the allowed in port list");
            ISS_TRC (ALL_FAILURE_TRC,
                     "\nWEB:Parse string failed for In PortList\n");
            return;
        }
    }

    InPortList.i4_Length = ISS_PORTLIST_LEN;
    InPortList.pu1_OctetList = MEM_CALLOC (InPortList.i4_Length, 1, UINT1);
    if (InPortList.pu1_OctetList == NULL)
    {
        ISS_UNLOCK ();
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:Failed to Allocate memory for InPortList\n");
        return;
    }
    MEMCPY (InPortList.pu1_OctetList, u1InPortList, InPortList.i4_Length);

    STRCPY (pHttp->au1Name, "OUT_PORTS_LIST");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    MEMSET (u1OutPortList, 0, ISS_PORTLIST_LEN);
    issDecodeSpecialChar (pHttp->au1Value);

    if ((STRLEN (pHttp->au1Value) != 0) && (STRCMP (pHttp->au1Value, "0") != 0))
    {
        if (IssPortListStringParser (pHttp->au1Value, u1OutPortList) ==
            ENM_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            IssSendError (pHttp, (CONST INT1 *)
                          "Check the allowed Out port list");
            ISS_TRC (ALL_FAILURE_TRC,
                     "\nWEB:Parse string failed for Out PortList\n");
            return;
        }
    }
    OutPortList.i4_Length = ISS_PORTLIST_LEN;
    OutPortList.pu1_OctetList = MEM_CALLOC (OutPortList.i4_Length, 1, UINT1);
    if (OutPortList.pu1_OctetList == NULL)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:Failed to Allocate memory for Out PortList\n");
        return;
    }
    MEMCPY (OutPortList.pu1_OctetList, u1OutPortList, OutPortList.i4_Length);

    STRCPY (pHttp->au1Name, "REDIRECT_PORTS_LIST");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    MEMSET (u1RedirectPortList, 0, ISS_PORTLIST_LEN);
    issDecodeSpecialChar (pHttp->au1Value);

    if ((STRLEN (pHttp->au1Value) != 0) && (STRCMP (pHttp->au1Value, "0") != 0))
    {
        if (IssPortListStringParser (pHttp->au1Value, u1RedirectPortList) ==
            ENM_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            IssSendError (pHttp, (CONST INT1 *)
                          "Check the allowed Redirect port list");
            ISS_TRC (ALL_FAILURE_TRC,
                     "\nWEB:Parse string failed for Redirect PortList\n");
            return;
        }
    }
    MEMSET (&RedirectPortList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RedirectPortList.i4_Length = ISS_PORTLIST_LEN;
    RedirectPortList.pu1_OctetList =
        MEM_CALLOC (RedirectPortList.i4_Length, 1, UINT1);
    if (RedirectPortList.pu1_OctetList == NULL)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:  failed to allocate memory for Redirect PortList \n");
        return;
    }
    MEMCPY (RedirectPortList.pu1_OctetList, u1RedirectPortList,
            RedirectPortList.i4_Length);

    STRCPY (pHttp->au1Name, "DIRECTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Direction = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "COUNTER");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Stats = ATOI (pHttp->au1Value);

    if (u1Apply == 1)
    {
        STRCPY (pHttp->au1Name, "CLEAR_COUNT");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4ClearStats = ATOI (pHttp->au1Value);

        if (i4ClearStats == ISS_TRUE)
        {
            if (nmhTestv2IssAclClearL3FilterStats (&u4ErrorCode, i4FilterNo,
                                                   i4ClearStats) ==
                SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                MEM_FREE (RedirectPortList.pu1_OctetList);
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to clear IP filter statistics.");
                ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                              "\nWEB:Failed to test L3 Filter Clear Statistics %d for Filter ID %d with error code as %d\n",
                              i4ClearStats, i4FilterNo, u4ErrorCode);
                return;
            }
            if (nmhSetIssAclClearL3FilterStats (i4FilterNo, i4ClearStats)
                == SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                MEM_FREE (RedirectPortList.pu1_OctetList);
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to clear IP filter statistics.");
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nWEB:Failed to clear L3 Filter Statistics %d for Filter ID %d \n",
                              i4ClearStats, i4FilterNo);
                return;
            }
        }
    }

    if (nmhTestv2IssExtL3FilterStatus (&u4ErrorCode, i4FilterNo,
                                       u1RowStatus) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        IssSendError (pHttp, (CONST INT1 *) "Invalid ACL number.");
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test L3 FilterStatus %d for Filter ID %d with  error code as %d\n",
                      u1RowStatus, i4FilterNo, u4ErrorCode);
        return;
    }
    if ((nmhGetIssExtL3FilterStatus (i4FilterNo, &i4Status)) == SNMP_FAILURE)
    {
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
    }
    if (i4Status == ISS_NOT_READY)
    {
        if ((InPortList.pu1_OctetList != NULL)
            && (nmhSetIssExtL3FilterInPortList (i4FilterNo, &InPortList) ==
                SNMP_FAILURE))
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *) "Unable to set In Port List.");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test L3 Filter InPortList  for Filter ID %d\n",
                          i4FilterNo);
            return;
        }
        if ((OutPortList.pu1_OctetList != NULL)
            && (nmhSetIssExtL3FilterOutPortList (i4FilterNo, &OutPortList) ==
                SNMP_FAILURE))
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *) "Unable to set Out Port List.");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test L3 Filter OutPortList for Filter ID %d\n",
                          i4FilterNo);
            return;
        }

        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_ACTIVE) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            IssSendError (pHttp, (CONST INT1 *) "Since Both In Port & Out Port"
                          " List are empty, filter status will be Inactive");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test L3 Filter status for Filter ID %d\n",
                          i4FilterNo);
            return;
        }
    }

    if (nmhSetIssExtL3FilterStatus (i4FilterNo, u1RowStatus) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        IssSendError (pHttp, (CONST INT1 *) "Entry already exists for this IP"
                      " filter number");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test L3 Filter RowStatus %d for Filter ID %d\n",
                      u1RowStatus, i4FilterNo);
        return;
    }

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Action = ATOI (pHttp->au1Value);

    if ((i4Action != 0) && (nmhTestv2IssExtL3FilterAction
                            (&u4ErrorCode, i4FilterNo,
                             i4Action)) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the standard ACL filter action.");
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test L3 Filter Actions %d for Filter ID %d with  error code as %d\n",
                      i4Action, i4FilterNo, u4ErrorCode);
        return;
    }

    if ((u4SrcIpAddr > 0) && (nmhTestv2IssExtL3FilterSrcIpAddr
                              (&u4ErrorCode, i4FilterNo, u4SrcIpAddr))
        == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set ACL with this source IP address.");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test L3 Filter SrcIp Addr for Filter ID %d with  error code as %d\n",
                      i4FilterNo, u4ErrorCode);
        return;
    }

    if ((u4SrcAddrMask > 0) && (nmhTestv2IssExtL3FilterSrcIpAddrMask
                                (&u4ErrorCode, i4FilterNo, u4SrcAddrMask)
                                == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set ACL with this source IP address Mask.");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test L3 Filter Src IP AddrMask  for Filter ID %d with  error code as %d\n     ",
                      i4FilterNo, u4ErrorCode);
        return;
    }
    if ((u4DestIpAddr > 0) && (nmhTestv2IssExtL3FilterDstIpAddr
                               (&u4ErrorCode, i4FilterNo, u4DestIpAddr)
                               == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set ACL with this destination IP Address.");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test L3 Filter Destination IpAddr  for Filter ID %d with  error code as %d\n",
                      i4FilterNo, u4ErrorCode);
        return;
    }

    if ((u4DestAddrMask > 0) && (nmhTestv2IssExtL3FilterDstIpAddrMask
                                 (&u4ErrorCode, i4FilterNo, u4DestAddrMask)
                                 == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set ACL with this destination IP address mask");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test L3 Filter Destination IpAddr Mask  for Filter ID %d with  error code as %d\n",
                      i4FilterNo, u4ErrorCode);
        return;
    }

    if ((InPortList.pu1_OctetList != NULL) && (nmhTestv2IssExtL3FilterInPortList
                                               (&u4ErrorCode, i4FilterNo,
                                                &InPortList) == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        IssSendError (pHttp, (CONST INT1 *) "Unable to set this in port list.");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test L3 Filter InPortList for Filter ID %d with       error code as %d\n",
                      i4FilterNo, u4ErrorCode);
        return;
    }

    if ((OutPortList.pu1_OctetList != NULL)
        &&
        (nmhTestv2IssExtL3FilterOutPortList
         (&u4ErrorCode, i4FilterNo, &OutPortList) == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set this Out port list.");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test L3 Filter OutPortList for Filter ID %d with error code as %d\n",
                      i4FilterNo, u4ErrorCode);
        return;
    }

    if (nmhTestv2IssExtL3FilterDirection (&u4ErrorCode, i4FilterNo, i4Direction)
        == SNMP_FAILURE)
    {
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Direction.");
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test L3 Filter Direction %d for Filter ID %d with error code      as %d\n",
                      i4Direction, i4FilterNo, u4ErrorCode);
        return;
    }
    if ((i4Action != 0) && (nmhSetIssExtL3FilterAction (i4FilterNo, i4Action)
                            == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the Standard ACL  filter action.");
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to Set L3 Filter Action%d for Filter ID %d\n",
                      i4Action, i4FilterNo);
        return;
    }

    if ((u4SrcIpAddr > 0) && (nmhSetIssExtL3FilterSrcIpAddr
                              (i4FilterNo, u4SrcIpAddr) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the ACL source IP address.");
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to Set L3 Filter Source IpAddr  for Filter ID %d\n",
                      i4FilterNo);
        return;
    }
    if ((u4SrcAddrMask > 0) && (nmhSetIssExtL3FilterSrcIpAddrMask
                                (i4FilterNo, u4SrcAddrMask) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set ACL source address mask.");
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to Set L3 Filter Source IpAddr Mask  for Filter ID %d\n",
                      i4FilterNo);
        return;
    }

    if ((u4DestIpAddr > 0) && (nmhSetIssExtL3FilterDstIpAddr
                               (i4FilterNo, u4DestIpAddr) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set ACL destination IP address.");
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to Set L3 Filter Destination IpAddr for Filter ID %d\n",
                      i4FilterNo);
        return;
    }
    if ((u4DestAddrMask > 0) && (nmhSetIssExtL3FilterDstIpAddrMask
                                 (i4FilterNo, u4DestAddrMask) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the destination IP address mask.");
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to Set L3 Filter Destination IpAddr Mask for Filter ID %d\n",
                      i4FilterNo);
        return;
    }

    if ((InPortList.pu1_OctetList != NULL) && (nmhSetIssExtL3FilterInPortList
                                               (i4FilterNo, &InPortList)
                                               == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *) "Unable to set In Port List.");
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to Set L3 Filter In PortList for Filter ID %d\n",
                      i4FilterNo);
        return;
    }
    if ((OutPortList.pu1_OctetList != NULL) && (nmhSetIssExtL3FilterOutPortList
                                                (i4FilterNo, &OutPortList)
                                                == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *) "Unable to set Out Port List.");
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to Set L3 Filter In Out Port list for Filter ID %d\n",
                      i4FilterNo);
        return;
    }
    if (nmhSetIssExtL3FilterDirection (i4FilterNo, i4Direction) == SNMP_FAILURE)
    {
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        IssSendError (pHttp, (CONST INT1 *) "Unable to set Direction.");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to Set L3 Filter Direction%d for Filter ID %d\n",
                      i4Direction, i4FilterNo);
        return;
    }
    if (i4Action == ISS_REDIRECT_TO)
    {
        nmhGetIssRedirectInterfaceGrpIdNextFree ((UINT4 *) &i4GroupId);
        if (i4GroupId == 0)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            IssSendError (pHttp, (INT1 *) "Check the Group Id Value");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to Retrieve Redirect Interface for Group ID %d\n",
                          i4GroupId);
            return;
        }
        if (nmhTestv2IssRedirectInterfaceGrpStatus (&u4ErrorCode, i4GroupId,
                                                    ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            IssSendError (pHttp,
                          (INT1 *) "Unable to create the redirect row status");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test Redirect Interface Group Status for GroupId  %d with  error code as %d\n",
                          i4GroupId, u4ErrorCode);
            return;
        }

        if (nmhSetIssRedirectInterfaceGrpStatus (i4GroupId, ISS_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            IssSendError (pHttp,
                          (INT1 *) "Unable to create the redirect row status");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set Redirect Interface GrpStatus for GroupId  %d\n",
                          i4GroupId);
            return;
        }

        if (nmhTestv2IssRedirectInterfaceGrpFilterId
            (&u4ErrorCode, i4GroupId, i4FilterNo) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            IssSendError (pHttp, (INT1 *) "Check the Filter Id value");
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test Redirect Interface GrpFilter ID %d for Filter ID %d                with  error code as %d\n",
                          i4GroupId, i4FilterNo, u4ErrorCode);
            return;
        }

        if (nmhSetIssRedirectInterfaceGrpFilterId
            (i4GroupId, i4FilterNo) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            IssSendError (pHttp, (INT1 *) "Unable to set the Filter Id value");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set Redirect Interface GrpFilter ID %d for Filter ID %d\n",
                          i4GroupId, i4FilterNo);
            return;
        }
        if (nmhTestv2IssRedirectInterfaceGrpFilterType
            (&u4ErrorCode, i4GroupId, i4GrpFilterType) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            IssSendError (pHttp, (INT1 *) "Check the Filter Type value");
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test Redirect Interface GrpFiltertype %d for Group ID %d                     with  error code as %d\n",
                          i4GrpFilterType, i4GroupId, u4ErrorCode);
            return;
        }

        if (nmhSetIssRedirectInterfaceGrpFilterType
            (i4GroupId, i4GrpFilterType) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            IssSendError (pHttp,
                          (INT1 *) "Unable to set the Filter Type value");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set Redirect Interface GrpFiltertype %d for Group ID %d\n",
                          i4GrpFilterType, i4GroupId);
            return;
        }
        if (nmhTestv2IssRedirectInterfaceGrpType
            (&u4ErrorCode, i4GroupId, i4GrpType) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            IssSendError (pHttp, (INT1 *) "Check the Interface Grp type value");
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test Redirect Interface Grptype %d for Group ID %d                          with  error code as %d\n",
                          i4GrpType, i4GroupId, u4ErrorCode);
            return;
        }

        if (nmhSetIssRedirectInterfaceGrpType (i4GroupId, i4GrpType) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            IssSendError (pHttp,
                          (INT1 *)
                          "Unable to set the Interface Grp type value");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set Redirect Interface Grptype %d for Group ID %d\n",
                          i4GrpType, i4GroupId);
            return;
        }
        if (nmhTestv2IssRedirectInterfaceGrpPortList
            (&u4ErrorCode, i4GroupId, &RedirectPortList) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            IssSendError (pHttp, (INT1 *) "Check the Port List value");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test Redirect Interface Port List  for Group ID %d                               with  error code as %d\n",
                          i4GroupId, u4ErrorCode);
            return;
        }

        if ((RedirectPortList.pu1_OctetList != NULL)
            &&
            (nmhSetIssRedirectInterfaceGrpPortList
             (i4GroupId, &RedirectPortList) == SNMP_FAILURE))
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssRedirectInterfaceGrpStatus (i4GroupId, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set Redirect Port List.");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set Redirect Interface Group Portlist for Group ID%d\n",
                          i4GroupId);
            return;
        }
        if (i4GrpFilterType == ISS_L3_REDIRECT)
        {
            pIssL3FilterEntry = IssExtGetL3FilterEntry (i4FilterNo);
            pIssL3FilterEntry->RedirectIfGrp.u4RedirectGrpId = i4GroupId;
        }
    }
    if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_ACTIVE) == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to set Filter Status as Active,"
                      "Check In/Out Port List");
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to set L3 Filter Status for Group ID%d\n",
                      i4GroupId);
        return;
    }

    if (i4Action == ISS_REDIRECT_TO)
    {
        nmhGetIssAclL3FilterRedirectId (i4FilterNo, &i4GroupId);
        if (nmhSetIssRedirectInterfaceGrpStatus (i4GroupId, ISS_ACTIVE) ==
            SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssRedirectInterfaceGrpStatus (i4GroupId, ISS_DESTROY);
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set the Redirect Grp status.");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set Redirect Interface status for Group ID%d\n",
                          i4GroupId);
            return;
        }
    }
    /* Enable ACL Counter if required */
    if ((nmhGetIssAclL3FilterStatsEnabledStatus
         (i4FilterNo, &i4StatsOld)) == SNMP_SUCCESS)
    {
        if (i4StatsOld != i4Stats)
        {
            if ((nmhTestv2IssAclL3FilterStatsEnabledStatus
                 (&u4ErrorCode, i4FilterNo, i4Stats)) == SNMP_FAILURE)
            {
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                MEM_FREE (RedirectPortList.pu1_OctetList);
                ISS_UNLOCK ();
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to set Filter Statistics Status.");
                ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                              "\nWEB:Failed to test L3 Filter Statistics Status %d for Filter ID %d with error code as %d\n",
                              i4Stats, i4FilterNo, u4ErrorCode);
                return;
            }

            if ((nmhSetIssAclL3FilterStatsEnabledStatus (i4FilterNo, i4Stats))
                == SNMP_FAILURE)
            {
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                MEM_FREE (RedirectPortList.pu1_OctetList);
                ISS_UNLOCK ();
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to set Filter Statistics Status.");
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nWEB:Failed to Set L3 Filter Statistics Status %d for Filter ID %d\n",
                              i4Stats, i4FilterNo);
                return;
            }
        }
    }
    else
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to Retrieve L3 Filter Statistics Status %d for Filter ID %d\n",
                      i4StatsOld, i4FilterNo);
    }
    ISS_UNLOCK ();
    MEM_FREE (InPortList.pu1_OctetList);
    MEM_FREE (OutPortList.pu1_OctetList);
    MEM_FREE (RedirectPortList.pu1_OctetList);
    IssBcmProcessIPStdFilterConfPageGet (pHttp);
    ISS_TRC (INIT_SHUT_TRC,
             "\nWEB:Exit IssBcmProcessIPStdFilterConfPageSet  function\n");
}

/*********************************************************************
*  Function Name : IssBcmProcessMACFilterConfPageGet
*  Description   : This function processes the get request for the MAC
*                  filter configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssBcmProcessMACFilterConfPageGet (tHttp * pHttp)
{
    INT4                i4CurrentFilterNo, i4FilterNo, i4RetVal, i4OutCome,
        i4GroupId = 0;
    UINT4               u4RetVal, u4Temp;
    UINT1               au1String[20];
    tMacAddr            SrcMacAddr, DestMacAddr;
    tSNMP_OCTET_STRING_TYPE *InPortList = NULL, *OutPortList =
        NULL, *RedirectPortList = NULL, *InPortChannelList =
        NULL, *OutPortChannelList = NULL;

    tPortList           InTempPortList, InTempPortChannelList,
        OutTempPortChannelList;
    tPortList           OutTempPortList, RedirectTempPortList;

    tSNMP_MULTI_DATA_TYPE *pu1DataString = NULL;
    MEMSET (InTempPortList, 0, VLAN_PORT_LIST_SIZE);
    MEMSET (OutTempPortList, 0, VLAN_PORT_LIST_SIZE);
    MEMSET (RedirectTempPortList, 0, VLAN_PORT_LIST_SIZE);
    MEMSET (InTempPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    MEMSET (OutTempPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);

    ISS_TRC (INIT_SHUT_TRC,
             "\nWEB:Entry IssBcmProcessMACFilterConfPageGet function \n");
    if ((pu1DataString = WebnmAllocMultiData ()) == NULL)
    {
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:Failed to Allocate a Multidata from multidata pool \n");
        return;
    }

    pHttp->i4Write = 0;

    IssPrintAvailableVlans (pHttp);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = pHttp->i4Write;

    WebnmRegisterLock (pHttp, IssLock, IssUnLock);

    ISS_LOCK ();
    i4OutCome = (INT4) (nmhGetFirstIndexIssExtL2FilterTable (&i4FilterNo));
    if (i4OutCome == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to Retrieve L2 FilterTable for Filter ID %d\n",
                      i4FilterNo);
        return;
    }

    InPortList = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PORTLIST_LEN);

    if (InPortList == NULL)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:Failed to Allocate memory for InPortList\n");
        return;
    }

    OutPortList = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PORTLIST_LEN);

    if (OutPortList == NULL)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:Failed to Allocate memory for OutPortList\n");
        return;
    }
    RedirectPortList = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PORTLIST_LEN);

    if (RedirectPortList == NULL)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:Failed to Allocate memory for RedirectPortList\n");
        return;
    }
    InPortChannelList = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PORTLIST_LEN);

    if (InPortChannelList == NULL)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (InPortList);
        free_octetstring (OutPortList);
        free_octetstring (RedirectPortList);
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:Failed to Allocate memory for  InPortChannelList\n");
        return;
    }
    OutPortChannelList = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PORTLIST_LEN);

    if (OutPortChannelList == NULL)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (InPortList);
        free_octetstring (OutPortList);
        free_octetstring (RedirectPortList);
        free_octetstring (InPortChannelList);
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:Failed to Allocate memory for  OutPortChannelList\n");
        return;
    }
    do
    {
        pHttp->i4Write = u4Temp;
        STRCPY (pHttp->au1KeyString, "FILTER_NUMBER");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4FilterNo);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "SOURCE_MAC");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssExtL2FilterSrcMacAddr (i4FilterNo, &SrcMacAddr);
        IssPrintMacAddress (SrcMacAddr, au1String);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "DESTINATION_MAC");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssExtL2FilterDstMacAddr (i4FilterNo, &DestMacAddr);
        IssPrintMacAddress (DestMacAddr, au1String);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "ACTION_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssExtL2FilterAction (i4FilterNo, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "PRIORITY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssExtL2FilterPriority (i4FilterNo, &i4RetVal);
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        IssPrintAvailableVlans (pHttp);
        STRCPY (pHttp->au1KeyString, "VLAN_ID");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmRegisterLock (pHttp, IssLock, IssUnLock);
        ISS_LOCK ();
        nmhGetIssExtL2FilterVlanId (i4FilterNo, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        MEMSET (InPortList->pu1_OctetList, 0, ISS_PORTLIST_LEN);
        nmhGetIssExtL2FilterInPortList (i4FilterNo,
                                        pu1DataString->pOctetStrValue);

        MEMSET (InTempPortList, 0, VLAN_PORT_LIST_SIZE);
        IssConvertToIfPortList (pHttp, pu1DataString->pOctetStrValue,
                                InTempPortList);

        MEMCPY (pu1DataString->pOctetStrValue->pu1_OctetList, InTempPortList,
                pu1DataString->pOctetStrValue->i4_Length);
        WebnmSendToSocket (pHttp, "IN_PORT_LIST", pu1DataString, ENM_PORT_LIST);

        MEMSET (OutPortList->pu1_OctetList, 0, ISS_PORTLIST_LEN);
        nmhGetIssExtL2FilterOutPortList (i4FilterNo,
                                         pu1DataString->pOctetStrValue);
        MEMSET (OutTempPortList, 0, VLAN_PORT_LIST_SIZE);
        IssConvertToIfPortList (pHttp, pu1DataString->pOctetStrValue,
                                OutTempPortList);
        MEMCPY (pu1DataString->pOctetStrValue->pu1_OctetList,
                OutTempPortList, pu1DataString->pOctetStrValue->i4_Length);
        WebnmSendToSocket (pHttp, "OUT_PORT_LIST", pu1DataString,
                           ENM_PORT_LIST);

        nmhGetIssAclL2FilterRedirectId (i4FilterNo, &i4GroupId);
        MEMSET (RedirectPortList->pu1_OctetList, 0, ISS_PORTLIST_LEN);
        nmhGetIssRedirectInterfaceGrpPortList (i4GroupId,
                                               pu1DataString->pOctetStrValue);
        MEMSET (RedirectTempPortList, 0, VLAN_PORT_LIST_SIZE);
        IssConvertToIfPortList (pHttp, pu1DataString->pOctetStrValue,
                                RedirectTempPortList);
        MEMCPY (pu1DataString->pOctetStrValue->pu1_OctetList,
                RedirectTempPortList, pu1DataString->pOctetStrValue->i4_Length);
        WebnmSendToSocket (pHttp, "REDIRECT_PORT_LIST", pu1DataString,
                           ENM_PORT_LIST);

        STRCPY (pHttp->au1KeyString, "ENCAPTYPE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssExtL2FilterEtherType (i4FilterNo, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "PROTOCOL");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssExtL2FilterProtocolType (i4FilterNo, &u4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "OTHER");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "DIRECTION");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssExtL2FilterDirection (i4FilterNo, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        MEMSET (InPortChannelList->pu1_OctetList, 0,
                ISS_PORT_CHANNEL_LIST_SIZE);
        nmhGetIssAclL2FilterInPortChannelList (i4FilterNo,
                                               pu1DataString->pOctetStrValue);
        MEMSET (InTempPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
        IssConvertToIfPortList (pHttp, pu1DataString->pOctetStrValue,
                                InTempPortChannelList);
        MEMCPY (pu1DataString->pOctetStrValue->pu1_OctetList,
                InTempPortChannelList,
                pu1DataString->pOctetStrValue->i4_Length);
        WebnmSendToSocket (pHttp, "IN_PORTCHANNEL_LIST", pu1DataString,
                           ENM_PORT_LIST);

        MEMSET (OutPortChannelList->pu1_OctetList, 0,
                ISS_PORT_CHANNEL_LIST_SIZE);
        nmhGetIssAclL2FilterOutPortChannelList (i4FilterNo,
                                                pu1DataString->pOctetStrValue);
        MEMSET (OutTempPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
        IssConvertToIfPortList (pHttp, pu1DataString->pOctetStrValue,
                                OutTempPortChannelList);
        MEMCPY (pu1DataString->pOctetStrValue->pu1_OctetList,
                OutTempPortChannelList,
                pu1DataString->pOctetStrValue->i4_Length);
        WebnmSendToSocket (pHttp, "OUT_PORTCHANNEL_LIST", pu1DataString,
                           ENM_PORT_LIST);

        STRCPY (pHttp->au1KeyString, "COUNTER");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssAclL2FilterStatsEnabledStatus (i4FilterNo, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "MATCH_COUNT");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssAclL2FilterMatchCount (i4FilterNo, &u4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "CLEAR_COUNT");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssAclClearL2FilterStats (i4FilterNo, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

        i4CurrentFilterNo = i4FilterNo;
    }
    while (nmhGetNextIndexIssExtL2FilterTable
           (i4CurrentFilterNo, &i4FilterNo) == SNMP_SUCCESS);

    ISS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    free_octetstring (InPortList);
    free_octetstring (OutPortList);
    free_octetstring (RedirectPortList);
    free_octetstring (InPortChannelList);
    free_octetstring (OutPortChannelList);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    ISS_TRC (INIT_SHUT_TRC,
             "\nWEB:Exit IssBcmProcessMACFilterConfPageGet function\n");
}

/*********************************************************************
*  Function Name : IssBcmProcessMACFilterConfPageSet
*  Description   : This function processes the set request for the MAC
*                  filter configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssBcmProcessMACFilterConfPageSet (tHttp * pHttp)
{
    INT4                i4Action, i4FilterNo, i4Priority, i4EtherType,
        i4GroupId;
    INT4                i4VlanId, i4Direction = 0;
    INT4                i4Status;
    INT4                i4Stats = ACL_STAT_DISABLE;
    INT4                i4StatsOld = ACL_STAT_DISABLE;
    INT4                i4ClearStats = ISS_FALSE;
    UINT4               u4ErrorCode, u4Protocol;
    tMacAddr            SrcMacAddr, DestMacAddr;
    tSNMP_OCTET_STRING_TYPE InPortList, OutPortList, RedirectPortList,
        InPortChannelList, OutPortChannelList;;
    UINT1               u1PortList[ISS_PORTLIST_LEN], u1Apply =
        0, u1OutPortList[ISS_PORTLIST_LEN];
    UINT1               u1RedirectPortList[ISS_PORTLIST_LEN];
    UINT1               u1InPortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];
    UINT1               u1OutPortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];
    UINT1               u1RowStatus = ISS_CREATE_AND_WAIT;
    UINT1               au1ZeroMac[ISS_MAC_LEN];
    UINT1               au1MemberPorts[VLAN_IFPORT_LIST_SIZE];
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;
    /* Filter type can be L2_REDIRECT in MAC Filter Page Set */
    INT4                i4GrpFilterType = ISS_L2_REDIRECT;
    /* Currently only Redirect to port is supported in BCM */
    INT4                i4GrpType = ISS_REDIRECT_TO_PORT;

    ISS_TRC (INIT_SHUT_TRC,
             "\nWEB:Entry IssBcmProcessMACFilterConfPageSet function \n");
    MEMSET (au1MemberPorts, 0, VLAN_IFPORT_LIST_SIZE);
    MEMSET (au1ZeroMac, 0, ISS_MAC_LEN);

    STRCPY (pHttp->au1Name, "FILTER_NUMBER");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4FilterNo = ATOI (pHttp->au1Value);

    /* Check whether the request is for ADD/DELETE */
    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    ISS_LOCK ();
    /* check for delete */
    if (STRCMP (pHttp->au1Value, "DELETE") == 0)
    {
        if (nmhTestv2IssExtL2FilterStatus
            (&u4ErrorCode, i4FilterNo, ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *) "MAC filter not available.");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test L2FilterStatus for filter ID %dwith  error code as %d\n",
                          i4FilterNo, u4ErrorCode);
            return;
        }

        if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *) "MAC filter not available.");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test L2 Filter Status for filter ID %d\n",
                          i4FilterNo);
            return;
        }
        ISS_UNLOCK ();
        IssBcmProcessMACFilterConfPageGet (pHttp);
        return;
    }

    if (STRCMP (pHttp->au1Value, "Apply") == 0)
    {
        u1Apply = 1;
        u1RowStatus = ISS_NOT_IN_SERVICE;
    }

    STRCPY (pHttp->au1Name, "SOURCE_MAC");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);
    StrToMac (pHttp->au1Value, SrcMacAddr);

    STRCPY (pHttp->au1Name, "DESTINATION_MAC");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);
    StrToMac (pHttp->au1Value, DestMacAddr);

    STRCPY (pHttp->au1Name, "ACTION_KEY");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Action = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "PRIORITY");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Priority = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "VLAN_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4VlanId = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "IN_PORT_LIST");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    MEMSET (u1PortList, 0, ISS_PORTLIST_LEN);
    issDecodeSpecialChar (pHttp->au1Value);

    if ((STRLEN (pHttp->au1Value) != 0) && (STRCMP (pHttp->au1Value, "0") != 0))
    {
        if (IssPortListStringParser (pHttp->au1Value, u1PortList) ==
            ENM_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *) "Check the allowed port list");
            ISS_TRC (ALL_FAILURE_TRC,
                     "\nWEB:Parse string failed for  PortList\n");
            return;
        }
    }
    InPortList.i4_Length = ISS_PORTLIST_LEN;

    InPortList.pu1_OctetList = MEM_CALLOC (InPortList.i4_Length, 1, UINT1);
    if (InPortList.pu1_OctetList == NULL)
    {
        ISS_UNLOCK ();
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:Failed to Allocate memory for InPortList \n");
        return;
    }
    MEMCPY (InPortList.pu1_OctetList, u1PortList, InPortList.i4_Length);

    STRCPY (pHttp->au1Name, "OUT_PORT_LIST");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    MEMSET (u1OutPortList, 0, ISS_PORTLIST_LEN);
    issDecodeSpecialChar (pHttp->au1Value);

    if ((STRLEN (pHttp->au1Value) != 0) && (STRCMP (pHttp->au1Value, "0") != 0))
    {
        if (IssPortListStringParser (pHttp->au1Value, u1OutPortList) ==
            ENM_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            IssSendError (pHttp, (CONST INT1 *)
                          "Check the allowed Out port list");
            ISS_TRC (ALL_FAILURE_TRC,
                     "\nWEB:Parse string failed for OutPortList \n");
            return;
        }
    }
    OutPortList.i4_Length = ISS_PORTLIST_LEN;
    OutPortList.pu1_OctetList = MEM_CALLOC (OutPortList.i4_Length, 1, UINT1);
    if (OutPortList.pu1_OctetList == NULL)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:Failed to Allocate memory for OutPortList \n");
        return;
    }
    MEMCPY (OutPortList.pu1_OctetList, u1OutPortList, OutPortList.i4_Length);

    STRCPY (pHttp->au1Name, "REDIRECT_PORT_LIST");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    MEMSET (u1RedirectPortList, 0, ISS_PORTLIST_LEN);
    issDecodeSpecialChar (pHttp->au1Value);

    if ((STRLEN (pHttp->au1Value) != 0) && (STRCMP (pHttp->au1Value, "0") != 0))
    {
        if (IssPortListStringParser (pHttp->au1Value, u1RedirectPortList) ==
            ENM_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            IssSendError (pHttp, (CONST INT1 *)
                          "Check the allowed Redirect port list");
            ISS_TRC (ALL_FAILURE_TRC,
                     "\nWEB:Parse string failed for Redirect PortList \n");
            return;
        }
    }
    MEMSET (&RedirectPortList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RedirectPortList.i4_Length = ISS_PORTLIST_LEN;
    RedirectPortList.pu1_OctetList =
        MEM_CALLOC (RedirectPortList.i4_Length, 1, UINT1);
    if (RedirectPortList.pu1_OctetList == NULL)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:Failed to Allocate memory for Redirect PortList \n");
        return;
    }
    MEMCPY (RedirectPortList.pu1_OctetList, u1RedirectPortList,
            RedirectPortList.i4_Length);

    STRCPY (pHttp->au1Name, "ENCAPTYPE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4EtherType = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "PROTOCOL");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4Protocol = ATOI (pHttp->au1Value);
    if (u4Protocol == OTHER)
    {
        STRCPY (pHttp->au1Name, "OTHER");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Protocol = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "DIRECTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Direction = ATOI (pHttp->au1Value);

    HttpGetValuebyName ((UINT1 *) "InPrtChannelList", pHttp->au1Value,
                        pHttp->au1PostQuery);
    MEMSET (u1InPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    issDecodeSpecialChar (pHttp->au1Value);
    if ((STRLEN (pHttp->au1Value) != 0) && (STRCMP (pHttp->au1Value, "0") != 0))
    {
        if (IssPortListStringParser (pHttp->au1Value, u1InPortChannelList) ==
            ENM_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            IssSendError (pHttp, (CONST INT1 *)
                          "Check the allowed in portchannel list");
            ISS_TRC (ALL_FAILURE_TRC,
                     "\nWEB:Parse string failed for InPrtchannelList \n");
            return;
        }
    }
    InPortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;
    InPortChannelList.pu1_OctetList =
        MEM_CALLOC (InPortChannelList.i4_Length, 1, UINT1);
    if (InPortChannelList.pu1_OctetList == NULL)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:Failed to Allocate memory for In PortChannelList \n");
        return;
    }
    MEMCPY (InPortChannelList.pu1_OctetList, u1InPortChannelList,
            InPortChannelList.i4_Length);

    HttpGetValuebyName ((UINT1 *) "OutPrtChannelList", pHttp->au1Value,
                        pHttp->au1PostQuery);
    MEMSET (u1OutPortChannelList, 0, ISS_PORT_CHANNEL_LIST_SIZE);
    issDecodeSpecialChar (pHttp->au1Value);
    if ((STRLEN (pHttp->au1Value) != 0) && (STRCMP (pHttp->au1Value, "0") != 0))
    {
        if (IssPortListStringParser (pHttp->au1Value, u1OutPortChannelList) ==
            ENM_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            IssSendError (pHttp, (CONST INT1 *)
                          "Check the allowed in portchannel list");
            ISS_TRC (ALL_FAILURE_TRC,
                     "\nWEB:Parse string failed for InPrtchannelList \n");
            return;
        }
    }
    OutPortChannelList.i4_Length = ISS_PORT_CHANNEL_LIST_SIZE;
    OutPortChannelList.pu1_OctetList =
        MEM_CALLOC (OutPortChannelList.i4_Length, 1, UINT1);
    if (OutPortChannelList.pu1_OctetList == NULL)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:Failed to Allocate memory for In PortChannelList \n");
        return;
    }
    MEMCPY (OutPortChannelList.pu1_OctetList, u1OutPortChannelList,
            OutPortChannelList.i4_Length);

    STRCPY (pHttp->au1Name, "COUNTER");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Stats = ATOI (pHttp->au1Value);

    if (u1Apply == 1)
    {
        STRCPY (pHttp->au1Name, "CLEAR_COUNT");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4ClearStats = ATOI (pHttp->au1Value);

        if (i4ClearStats == ISS_TRUE)
        {
            if (nmhTestv2IssAclClearL2FilterStats (&u4ErrorCode, i4FilterNo,
                                                   i4ClearStats) ==
                SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                MEM_FREE (RedirectPortList.pu1_OctetList);
                MEM_FREE (InPortChannelList.pu1_OctetList);
                MEM_FREE (OutPortChannelList.pu1_OctetList);
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to clear MAC filter statistics.");
                ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                              "\nWEB:Failed to test L2 Filter Clear Statistics %d for Filter ID %d with error code as %d\n",
                              i4ClearStats, i4FilterNo, u4ErrorCode);
                return;
            }
            if (nmhSetIssAclClearL2FilterStats (i4FilterNo, i4ClearStats)
                == SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                MEM_FREE (RedirectPortList.pu1_OctetList);
                MEM_FREE (InPortChannelList.pu1_OctetList);
                MEM_FREE (OutPortChannelList.pu1_OctetList);
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to clear MAC filter statistics.");
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nWEB:Failed to clear L2 Filter Statistics %d for Filter ID %d \n",
                              i4ClearStats, i4FilterNo);
                return;
            }
        }
    }

    if (nmhTestv2IssExtL2FilterStatus (&u4ErrorCode, i4FilterNo,
                                       u1RowStatus) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);
        IssSendError (pHttp, (CONST INT1 *) "Invalid MAC filter number.");
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test L2 Filter RowStatus %d for Filter ID %d                                    with  error code as %d\n",
                      u1RowStatus, i4FilterNo, u4ErrorCode);
        return;
    }

    if (nmhTestv2IssExtL2FilterAction (&u4ErrorCode, i4FilterNo, i4Action)
        == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the l2 filter action.");
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test L2 Filter Action %d for Filter ID %d                              with  error code as %d\n",
                      i4Action, i4FilterNo, u4ErrorCode);
        return;
    }

    if (MEMCMP (au1ZeroMac, SrcMacAddr, ISS_MAC_LEN))
    {
        if (nmhTestv2IssExtL2FilterSrcMacAddr (&u4ErrorCode, i4FilterNo,
                                               SrcMacAddr) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);
            IssSendError (pHttp, (CONST INT1 *) "Unable to set the filter with "
                          "this source MAC Address.");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test L2 Filter SrcMacAddr  for Filter ID %d                                   with  error code as %d\n",
                          i4FilterNo, u4ErrorCode);
            return;
        }
    }

    if (MEMCMP (au1ZeroMac, DestMacAddr, ISS_MAC_LEN))
    {
        if (nmhTestv2IssExtL2FilterDstMacAddr (&u4ErrorCode, i4FilterNo,
                                               DestMacAddr) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);

            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set the filter with this"
                          " destination MAC Address.");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test L2 Filter DestMacAddr  for Filter ID %d                                        with  error code as %d\n",
                          i4FilterNo, u4ErrorCode);
            return;
        }
    }

    if (nmhTestv2IssExtL2FilterPriority (&u4ErrorCode, i4FilterNo, i4Priority)
        == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);

        IssSendError (pHttp, (CONST INT1 *) "Unable to set filter priority");
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test L2 Filter Priority %d for Filter ID %d                          with  error code as %d\n",
                      i4Priority, i4FilterNo, u4ErrorCode);
        return;
    }

    if ((u4Protocol != 0) && (nmhTestv2IssExtL2FilterProtocolType
                              (&u4ErrorCode, i4FilterNo, u4Protocol)
                              == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);

        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set MAC filter protocol type!");
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test L2 Filter Protocol %d for Filter ID %d                               with  error code as %d\n",
                      u4Protocol, i4FilterNo, u4ErrorCode);
        return;
    }

    if ((i4EtherType != 0) && (nmhTestv2IssExtL2FilterEtherType
                               (&u4ErrorCode, i4FilterNo, i4EtherType)
                               == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);

        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set MAC the filter ether type");
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test L2 Filter EtherType %d for Filter ID %d                                    with  error code as %d\n",
                      i4EtherType, i4FilterNo, u4ErrorCode);
        return;
    }

    if ((i4VlanId != 0) && (nmhTestv2IssExtL2FilterVlanId
                            (&u4ErrorCode, i4FilterNo,
                             i4VlanId) == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);

        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set MAC filter VLAN ID.");
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test L2 Filter Vlan ID %d for Filter ID %d                                         with  error code as %d\n",
                      i4VlanId, i4FilterNo, u4ErrorCode);
        return;
    }

    if ((InPortList.pu1_OctetList != NULL) && (nmhTestv2IssExtL2FilterInPortList
                                               (&u4ErrorCode, i4FilterNo,
                                                &InPortList)) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);

        IssSendError (pHttp, (CONST INT1 *) "Unable to set this in port list.");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test L2 Filter InPortList  for Filter ID %d                                              with  error code as %d\n",
                      i4FilterNo, u4ErrorCode);
        return;
    }

    if ((nmhGetIssExtL2FilterStatus (i4FilterNo, &i4Status)) == SNMP_FAILURE)
    {
        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
    }
    if (i4Status == ISS_NOT_READY)
    {
        if ((InPortList.pu1_OctetList != NULL)
            && (nmhSetIssExtL2FilterInPortList (i4FilterNo, &InPortList) ==
                SNMP_FAILURE))
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);

            if (u1Apply != 1)
            {
                nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *) "Unable to set In Port List.");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to Set L2 Filter InPortList for Filter ID %d\n",
                          i4FilterNo);
            return;
        }
        if ((OutPortList.pu1_OctetList != NULL)
            && (nmhSetIssExtL2FilterOutPortList (i4FilterNo, &OutPortList) ==
                SNMP_FAILURE))
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);

            if (u1Apply != 1)
            {
                nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *) "Unable to set Out Port List.");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to Set L2 Filter OutPortList  for Filter ID %d\n",
                          i4FilterNo);
            return;
        }
        if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_ACTIVE) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);

            IssSendError (pHttp, (CONST INT1 *) "Since In Port List is Empty"
                          " filter status will be Inactive");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to Set L2 Filter Status for Filter ID %d\n",
                          i4FilterNo);
            return;
        }
    }
    if (nmhSetIssExtL2FilterStatus (i4FilterNo, u1RowStatus) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);

        IssSendError (pHttp, (CONST INT1 *) "Entry already exists for this MAC"
                      " filter number");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to Set L2 Filter RowStatus %d for Filter ID %d\n",
                      u1RowStatus, i4FilterNo);
        return;
    }

    if (nmhSetIssExtL2FilterAction (i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);

        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *)
                      "Unable to set the l2 filter action.");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to Set L2 Filter Action %d for Filter ID %d\n",
                      i4Action, i4FilterNo);
        return;
    }

    if (MEMCMP (au1ZeroMac, SrcMacAddr, ISS_MAC_LEN))
    {
        if (nmhSetIssExtL2FilterSrcMacAddr (i4FilterNo, SrcMacAddr)
            == SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);

            if (u1Apply != 1)
            {
                nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set the source MAC Address.");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to Set L2 Filter Src Mac Address for Filter ID %d\n",
                          i4FilterNo);
            return;
        }
    }

    if (MEMCMP (au1ZeroMac, DestMacAddr, ISS_MAC_LEN))
    {
        if (nmhSetIssExtL2FilterDstMacAddr
            (i4FilterNo, DestMacAddr) == SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);

            if (u1Apply != 1)
            {
                nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set the Destination MAC Address.");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to Set L2 Filter Dest Mac Address for Filter ID %d\n",
                          i4FilterNo);
            return;
        }
    }

    if (nmhSetIssExtL2FilterPriority (i4FilterNo, i4Priority) == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);

        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *) "Unable to set filter priority.");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to Set L2 Filter Priority  %d for Filter ID %d\n",
                      i4Priority, i4FilterNo);
        return;
    }

    if ((u4Protocol != 0) && (nmhSetIssExtL2FilterProtocolType
                              (i4FilterNo, u4Protocol) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);

        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to set filter protocol type.");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to Set L2 Filter  Protocol  %d for Filter ID %d\n",
                      u4Protocol, i4FilterNo);
        return;
    }

    if ((i4EtherType != 0) && (nmhSetIssExtL2FilterEtherType
                               (i4FilterNo, i4EtherType) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);

        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *) "Unable to set filter ether type.");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to Set L2 Filter  EtherType  %d for Filter ID %d\n",
                      i4EtherType, i4FilterNo);
        return;
    }

    if ((i4VlanId != 0) && (nmhSetIssExtL2FilterVlanId
                            (i4FilterNo, i4VlanId) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);

        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *) "Unable to set filter VLAN ID.");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to Set L2 Filter VlanId  %d for Filter ID %d\n",
                      i4VlanId, i4FilterNo);
        return;
    }
    if ((InPortList.pu1_OctetList != NULL) && (nmhSetIssExtL2FilterInPortList
                                               (i4FilterNo, &InPortList)
                                               == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);

        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *) "Unable to set In Port List.");
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to Set L2 Filter InPortList  for Filter ID %d\n",
                      i4FilterNo);
        return;
    }

    if ((OutPortList.pu1_OctetList != NULL)
        &&
        (nmhTestv2IssExtL2FilterOutPortList
         (&u4ErrorCode, i4FilterNo, &OutPortList)) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);

        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to set this out port list.");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test L2 Filter OutPortList  for Filter ID %d                                                   with  error code as %d\n",
                      i4FilterNo, u4ErrorCode);
        return;
    }
    if ((OutPortList.pu1_OctetList != NULL) && (nmhSetIssExtL2FilterOutPortList
                                                (i4FilterNo, &OutPortList)
                                                == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);

        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *) "Unable to set Out Port List.");
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to set L2 Filter OutPortList  for Filter ID %d\n",
                      i4FilterNo);
        return;
    }

    if (nmhTestv2IssAclL2FilterDirection (&u4ErrorCode, i4FilterNo, i4Direction)
        == SNMP_FAILURE)
    {
        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);

        IssSendError (pHttp, (CONST INT1 *) "Invalid Direction.");
        ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test L2 Filter Direction %d for Filter ID %d                           with  error code as %d\n",
                      i4Direction, i4FilterNo, u4ErrorCode);
        return;
    }
    if (nmhSetIssAclL2FilterDirection (i4FilterNo, i4Direction) == SNMP_FAILURE)
    {
        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);

        IssSendError (pHttp, (CONST INT1 *) "Unable to set Direction.");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to Set L2 Filter Direction %d for Filter ID %d\n",
                      i4Direction, i4FilterNo);
        return;
    }
    if ((InPortChannelList.pu1_OctetList != NULL)
        && nmhTestv2IssAclL2FilterInPortChannelList (&u4ErrorCode, i4FilterNo,
                                                     &InPortChannelList) ==
        SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *) "Unable to set this in port list.");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test  L2 Filter InPortChannelList for  Filter ID %d with error code as %d\n",
                      i4FilterNo, u4ErrorCode);
        return;
    }

    if ((InPortChannelList.pu1_OctetList != NULL)
        &&
        (nmhSetIssAclL2FilterInPortChannelList (i4FilterNo, &InPortChannelList)
         == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to set In Port channelList.");
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to set L2Filter InPortChannelList for Filter ID%d \n",
                      i4FilterNo);
        return;
    }

    if ((OutPortChannelList.pu1_OctetList != NULL)
        && nmhTestv2IssAclL2FilterOutPortChannelList (&u4ErrorCode, i4FilterNo,
                                                      &OutPortChannelList) ==
        SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *) "Unable to set this in port list.");
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to test  L2 Filter OutPortChannelList for  Filter ID %d with error code as %d\n",
                      i4FilterNo, u4ErrorCode);
        return;
    }
    if ((OutPortChannelList.pu1_OctetList != NULL)
        &&
        (nmhSetIssAclL2FilterOutPortChannelList
         (i4FilterNo, &OutPortChannelList) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to set In Port channelList.");
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to set L2Filter OutPortChannelList for Filter ID%d \n",
                      i4FilterNo);
        return;
    }

    if (i4Action == ISS_REDIRECT_TO)
    {
        nmhGetIssRedirectInterfaceGrpIdNextFree ((UINT4 *) &i4GroupId);
        if (i4GroupId == 0)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);

            IssSendError (pHttp, (INT1 *) "Check the Group Id Value");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to Retrieve Redirect Interface for Group ID %d\n",
                          i4GroupId);
            return;
        }
        if (nmhTestv2IssRedirectInterfaceGrpStatus (&u4ErrorCode, i4GroupId,
                                                    ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);

            IssSendError (pHttp,
                          (INT1 *) "Unable to create the redirect row status");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test Redirect Interface GrpStatus  for Group ID %d with  error code as %d\n",
                          i4GroupId, u4ErrorCode);
            return;
        }

        if (nmhSetIssRedirectInterfaceGrpStatus (i4GroupId, ISS_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);

            IssSendError (pHttp,
                          (INT1 *) "Unable to create the redirect row status");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set Redirect Interface GrpStatus  for Group ID %d\n",
                          i4GroupId);
            return;
        }

        if (nmhTestv2IssRedirectInterfaceGrpFilterId
            (&u4ErrorCode, i4GroupId, i4FilterNo) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);

            IssSendError (pHttp, (INT1 *) "Check the Filter Id value");
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test RedirectInterface GroupId %d for Filter ID %d                                with  error code as %d\n",
                          i4GroupId, i4FilterNo, u4ErrorCode);
            return;
        }

        if (nmhSetIssRedirectInterfaceGrpFilterId
            (i4GroupId, i4FilterNo) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);

            IssSendError (pHttp, (INT1 *) "Unable to set the Filter Id value");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set RedirectInterface GroupId %d for Filter ID %d\n",
                          i4GroupId, i4FilterNo);
            return;
        }
        if (nmhTestv2IssRedirectInterfaceGrpFilterType
            (&u4ErrorCode, i4GroupId, i4GrpFilterType) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);

            IssSendError (pHttp, (INT1 *) "Check the Filter Type value");
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test RedirectInterface Group FilterType %d for Group ID %d             with  error code as %d\n",
                          i4GrpFilterType, i4GroupId, u4ErrorCode);
            return;
        }

        if (nmhSetIssRedirectInterfaceGrpFilterType
            (i4GroupId, i4GrpFilterType) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);

            IssSendError (pHttp,
                          (INT1 *) "Unable to set the Filter Type value");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to Set RedirectInterface Group Filter Type %d for Filter ID %d\n",
                          i4GrpFilterType, i4GroupId);
            return;
        }
        if (nmhTestv2IssRedirectInterfaceGrpType
            (&u4ErrorCode, i4GroupId, i4GrpType) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);

            IssSendError (pHttp, (INT1 *) "Check the Interface Grp type value");
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test RedirectInterface Group GrpType %d for Group ID %d                  with  error code as %d\n",
                          i4GrpType, i4GroupId, u4ErrorCode);
            return;
        }

        if (nmhSetIssRedirectInterfaceGrpType (i4GroupId, i4GrpType) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);

            IssSendError (pHttp,
                          (INT1 *)
                          "Unable to set the Interface Grp type value");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to Set RedirectInterface Group Type %d for Group ID %d\n",
                          i4GrpType, i4GroupId);
            return;
        }
        if (nmhTestv2IssRedirectInterfaceGrpPortList
            (&u4ErrorCode, i4GroupId, &RedirectPortList) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);

            IssSendError (pHttp, (INT1 *) "Check the Port List value");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test RedirectInterface PortList for Group ID %d                       with  error code as %d\n",
                          i4GroupId, u4ErrorCode);
            return;
        }

        if ((RedirectPortList.pu1_OctetList != NULL)
            &&
            (nmhSetIssRedirectInterfaceGrpPortList
             (i4GroupId, &RedirectPortList) == SNMP_FAILURE))
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);

            if (u1Apply != 1)
            {
                nmhSetIssRedirectInterfaceGrpStatus (i4GroupId, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set Redirect Port List.");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set RedirectInterface PortList for Group ID %d\n",
                          i4GroupId);
            return;
        }
        if (i4GrpFilterType == ISS_L2_REDIRECT)
        {
            pIssL2FilterEntry = IssExtGetL2FilterEntry (i4FilterNo);
            pIssL2FilterEntry->RedirectIfGrp.u4RedirectGrpId = i4GroupId;
        }
    }
    if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_ACTIVE) == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        MEM_FREE (RedirectPortList.pu1_OctetList);
        MEM_FREE (InPortChannelList.pu1_OctetList);
        MEM_FREE (OutPortChannelList.pu1_OctetList);

        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *) "Unable to set the filter status.");
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to set L2 FilterStatus  for Filter ID %d\n",
                      i4FilterNo);
        return;
    }
    if (i4Action == ISS_REDIRECT_TO)
    {
        nmhGetIssAclL2FilterRedirectId (i4FilterNo, &i4GroupId);
        if (nmhSetIssRedirectInterfaceGrpStatus (i4GroupId, ISS_ACTIVE) ==
            SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            MEM_FREE (RedirectPortList.pu1_OctetList);
            MEM_FREE (InPortChannelList.pu1_OctetList);
            MEM_FREE (OutPortChannelList.pu1_OctetList);

            if (u1Apply != 1)
            {
                nmhSetIssRedirectInterfaceGrpStatus (i4GroupId, ISS_DESTROY);
                nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *)
                          "Unable to set the Redirect Grp status.");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set RedirectInterface Grp Status for group ID %d\n",
                          i4GroupId);
            return;
        }
    }
    /* Enable ACL Counter if required */
    if ((nmhGetIssAclL2FilterStatsEnabledStatus
         (i4FilterNo, &i4StatsOld)) == SNMP_SUCCESS)
    {
        if (i4StatsOld != i4Stats)
        {
            if ((nmhTestv2IssAclL2FilterStatsEnabledStatus
                 (&u4ErrorCode, i4FilterNo, i4Stats)) == SNMP_FAILURE)
            {
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                MEM_FREE (RedirectPortList.pu1_OctetList);
                MEM_FREE (InPortChannelList.pu1_OctetList);
                MEM_FREE (OutPortChannelList.pu1_OctetList);
                ISS_UNLOCK ();
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to set Filter Statistics Status.");
                ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                              "\nWEB:Failed to test L2 Filter Statistics Status %d for Filter ID %d with error code as %d\n",
                              i4Stats, i4FilterNo, u4ErrorCode);
                return;
            }

            if ((nmhSetIssAclL2FilterStatsEnabledStatus (i4FilterNo, i4Stats))
                == SNMP_FAILURE)
            {
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                MEM_FREE (RedirectPortList.pu1_OctetList);
                MEM_FREE (InPortChannelList.pu1_OctetList);
                MEM_FREE (OutPortChannelList.pu1_OctetList);
                ISS_UNLOCK ();
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to set Filter Statistics Status.");
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nWEB:Failed to Set L2 Filter Statistics Status %d for Filter ID %d\n",
                              i4Stats, i4FilterNo);
                return;
            }
        }
    }
    else
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to Retrieve L2 Filter Statistics Status %d for Filter ID %d\n",
                      i4StatsOld, i4FilterNo);
    }
    ISS_UNLOCK ();
    MEM_FREE (InPortList.pu1_OctetList);
    MEM_FREE (OutPortList.pu1_OctetList);
    MEM_FREE (RedirectPortList.pu1_OctetList);
    MEM_FREE (InPortChannelList.pu1_OctetList);
    MEM_FREE (OutPortChannelList.pu1_OctetList);

    IssBcmProcessMACFilterConfPageGet (pHttp);
    ISS_TRC (INIT_SHUT_TRC,
             "\nWEB:Exit IssBcmProcessMACFilterConfPageSet function \n");
}

#ifdef DIFFSRV_WANTED

/*********************************************************************
 *  Function Name : IssBcmProcessDfsPolicyMapPage
 *  Description   : This function processes the request coming for the 
 *                  DiffSrv Policy Map page.
 *                     
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssBcmProcessDfsPolicyMapPage (tHttp * pHttp)
{
    ISS_TRC (INIT_SHUT_TRC,
             "\nWEB:Entry IssBcmProcessDfsPolicyMapPage function \n");
    if (pHttp->i4Method == ISS_GET)
    {
        IssBcmProcessDfsPolicyMapPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssBcmProcessDfsPolicyMapPageSet (pHttp);
    }
    ISS_TRC (INIT_SHUT_TRC,
             "\nWEB:Failed to IssBcmProcessDfsPolicyMapPage function \n");
    return;
}

/***************************************************************************
 *  Function Name : IssBcmProcessDfsPolicyMapPageGet
 *  Description   : This function processes the Get request coming for the 
 *                  Diff Server Policy Map Page.
 *                     
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *****************************************************************************/
VOID
IssBcmProcessDfsPolicyMapPageGet (tHttp * pHttp)
{
    tDiffServWebClfrData PolicyMapData;
    INT4                i4ClfrId;
    INT4                i4OutCome = 0;
    INT4                i4PolicyMapId, i4NextPolicyMapId;
    INT4                i4OutProfActionId = 0;
    INT4                i4MeterId;
    INT4                i4InProfActionType = 0;
    INT4                i4OutProfActionType = 0;
    UINT4               u4TrafficRate;
    UINT4               u4Temp = 0;

    ISS_TRC (INIT_SHUT_TRC,
             "\nWEB:Entry IssBcmProcessDfsPolicyMapPageGet function \n");
    pHttp->i4Write = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    WebnmRegisterLock (pHttp, DfsLock, DfsUnLock);

    DFS_LOCK ();

    i4OutCome = (INT4) nmhGetFirstIndexFsDiffServClfrTable (&i4PolicyMapId);

    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        DFS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nWEB:Failed to retrieve  DiffServ Table for Policy Map Id %d \n",
                      i4PolicyMapId);
        return;
    }
    u4Temp = pHttp->i4Write;
    do
    {
        pHttp->i4Write = u4Temp;
        STRCPY (pHttp->au1KeyString, "POLICY_MAPID");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4PolicyMapId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        i4OutCome = nmhGetFsDiffServClfrMFClfrId (i4PolicyMapId, &i4ClfrId);
        STRCPY (pHttp->au1KeyString, "CLFR_ID");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4OutCome != SNMP_FAILURE)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4ClfrId);
        }
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));

        i4OutCome = nmhGetFsDiffServClfrOutProActionId (i4PolicyMapId,
                                                        &i4OutProfActionId);
        i4OutCome &= nmhGetFsDiffServOutProfileActionMID (i4OutProfActionId,
                                                          &(i4MeterId));
        u4TrafficRate = 0;
        i4OutCome &= nmhGetFsDiffServMeterRefreshCount (i4MeterId,
                                                        &u4TrafficRate);
        STRCPY (pHttp->au1KeyString, "TRAFFIC_RATE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if ((i4OutProfActionId != 0) && (i4MeterId != 0) &&
            (i4OutCome != SNMP_FAILURE))
        {
            if (u4TrafficRate > 0)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4TrafficRate);
            }
            else
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, " ");
            }
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, " ");
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        if (DsWebnmGetPolicyMapEntry (i4PolicyMapId, &PolicyMapData)
            == SNMP_FAILURE)
        {
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            i4NextPolicyMapId = i4PolicyMapId;
            continue;
        }

        STRCPY (pHttp->au1KeyString, "INPROF_ACT_TYPE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (STRCMP (PolicyMapData.au1DsClfrInProfActionType, "Policed-Dscp") ==
            0)
        {
            i4InProfActionType = 1;
        }
        else if (STRCMP
                 (PolicyMapData.au1DsClfrInProfActionType,
                  "Policed-Precedence") == 0)
        {
            i4InProfActionType = 2;
        }
        else if (STRCMP
                 (PolicyMapData.au1DsClfrInProfActionType,
                  "Policed-Priority") == 0)
        {
            i4InProfActionType = 3;
        }
        else
        {
            i4InProfActionType = 0;
        }
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4InProfActionType);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "INPROF_ACT_VALUE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                 PolicyMapData.au1DsClfrInProfActionValue);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "OUTPROF_ACT_TYPE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (STRCMP (PolicyMapData.au1DsClfrOutProfActionType, "Drop") == 0)
        {
            i4OutProfActionType = 1;
        }
        else if (STRCMP
                 (PolicyMapData.au1DsClfrOutProfActionType,
                  "Policed-Dscp") == 0)
        {
            i4OutProfActionType = 2;
        }
        else
        {
            i4OutProfActionType = 0;
        }
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4OutProfActionType);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "OUTPROF_ACT_VALUE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                 PolicyMapData.au1DsClfrOutProfActionValue);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        i4NextPolicyMapId = i4PolicyMapId;
    }
    while (nmhGetNextIndexFsDiffServClfrTable (i4NextPolicyMapId,
                                               &i4PolicyMapId) == SNMP_SUCCESS);

    DFS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    ISS_TRC (INIT_SHUT_TRC,
             "\nWEB:Exit IssBcmProcessDfsPolicyMapPageGet function  \n");
}

/*********************************************************************
 *  Function Name : IssBcmProcessDfsPolicyMapPageSet
 *  Description   : This function processes the request coming for the 
 *                  Diff Server Policy Map Page.
 *                     
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ***********************************************************************/
VOID
IssBcmProcessDfsPolicyMapPageSet (tHttp * pHttp)
{
    tDiffServWebSetClfrEntry ClfrEntry;
    UINT1               au1ErrStr[255];
    UINT1               u1Apply = 0;
    INT4                i4PolicyMapId;
    INT4                i4ClassMapId;
    INT4                i4InProfActionId;
    INT4                i4OutProfActionId;
    INT4                i4MeterId;
    INT4                i4FirstClassMapId;
    INT4                i4NextClassMapId;
    INT4                i4MFCId;
    INT4                i4Status;
    INT4                i4DsStatus;
    UINT4               u4ErrorCode;
    UINT4               u4TrafficRate;
    UINT4               u4OutProfActType;
    UINT4               u4InDscp = 0;
    UINT4               u4InPrec = 0;
    UINT4               u4OutDscp = 0;
    UINT4               u4CosValue = 0;
    UINT4               u4InProfActType = 0;
    UINT1               u1RowStatus = ISS_CREATE_AND_WAIT;

    ISS_TRC (INIT_SHUT_TRC,
             "\nWEB:Entry IssBcmProcessDfsPolicyMapPageSet function \n");
    /* Get the Policy-Map ID */
    STRCPY (pHttp->au1Name, "POLICY_MAPID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PolicyMapId = ATOI (pHttp->au1Value);

    DFS_LOCK ();

    nmhGetFsDsStatus (&i4DsStatus);
    if (i4DsStatus == 2)
    {
        DFS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *) "DiffServer not enabled.");
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nWEB:Failed to  retrieve DsStatus---DiffServer not enabled \n");
        return;
    }
    /* Check whether the request is for ADD/DELETE */
    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    /* check for delete */
    if (STRCMP (pHttp->au1Value, "DELETE") == 0)
    {
        if (nmhGetFsDiffServClfrStatus
            (i4PolicyMapId, &i4Status) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *)
                          "Policy Map entry not available.");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to retrieve the  DiffServ Status %d for PolicyMapId %d \n",
                          i4Status, i4PolicyMapId);
            return;
        }

        if ((i4Status == ISS_ACTIVE) &&
            (nmhSetFsDiffServClfrStatus (i4PolicyMapId,
                                         ISS_NOT_IN_SERVICE) == SNMP_FAILURE))
        {
            DFS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *)
                          "Cannot modify Policy Map entry status.");
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set DiffServ Status  for PolicyMapId %d \n",
                          i4PolicyMapId);
            return;
        }

        nmhGetFsDiffServClfrInProActionId (i4PolicyMapId, &i4InProfActionId);
        if (i4InProfActionId != 0)
        {
            if (nmhGetFsDiffServInProfileActionStatus
                (i4InProfActionId, &i4Status) != SNMP_FAILURE)
            {

                if (nmhSetFsDiffServInProfileActionStatus
                    (i4InProfActionId, ISS_DESTROY) == SNMP_FAILURE)
                {
                    DFS_UNLOCK ();
                    IssSendError (pHttp, (CONST INT1 *)
                                  "In-Profile Id does not exists");
                    ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                  "\nWEB:Failed to set InProfile Action Status  for ActionId %d \n",
                                  i4InProfActionId);
                    return;
                }
            }
        }

        nmhGetFsDiffServClfrOutProActionId (i4PolicyMapId, &i4OutProfActionId);
        nmhGetFsDiffServOutProfileActionMID (i4OutProfActionId, &i4MeterId);
        if (i4OutProfActionId != 0)
        {
            if (nmhGetFsDiffServOutProfileActionStatus
                (i4OutProfActionId, &i4Status) != SNMP_FAILURE)
            {
                if (nmhSetFsDiffServOutProfileActionStatus
                    (i4OutProfActionId, ISS_DESTROY) == SNMP_FAILURE)
                {
                    DFS_UNLOCK ();
                    IssSendError (pHttp, (CONST INT1 *)
                                  "Out-Profile Id does not exists");
                    ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                  "\nWEB:Failed to set outProfile Action Status  for ActionId %d \n",
                                  i4OutProfActionId);
                    return;
                }
            }
            if (i4MeterId != 0)
            {
                if (nmhGetFsDiffServMeterStatus
                    (i4MeterId, &i4Status) != SNMP_FAILURE)
                {
                    if (nmhSetFsDiffServMeterStatus
                        (i4MeterId, ISS_DESTROY) == SNMP_FAILURE)
                    {
                        DFS_UNLOCK ();
                        IssSendError (pHttp, (CONST INT1 *)
                                      "Meter Id does not exists");
                        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                      "\nWEB:Failed to set DiffServMeterStatus for MeterId %d \n",
                                      i4MeterId);
                        return;
                    }
                }
            }
        }
        if (nmhGetFsDiffServClfrStatus
            (i4PolicyMapId, &i4Status) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to retrieve the  DiffServ Status %d for PolicyMapId %d \n",
                          i4Status, i4PolicyMapId);
            return;
        }

        if (nmhSetFsDiffServClfrStatus (i4PolicyMapId,
                                        ISS_DESTROY) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *)
                          "Cannot delete Policy Map entry.");
            DFS_UNLOCK ();
            IssBcmProcessDfsPolicyMapPageGet (pHttp);
            ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set the  DiffServ Status for PolicyMapId%d\n",
                          i4PolicyMapId);
            return;
        }

        STRCPY (pHttp->au1Name, "ACTION");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        if (STRCMP (pHttp->au1Value, "apply") == 0)
        {
            u1Apply = 1;
            u1RowStatus = ISS_NOT_IN_SERVICE;
        }

        STRCPY (pHttp->au1Name, "CLFR_ID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4ClassMapId = ATOI (pHttp->au1Value);

        STRCPY (pHttp->au1Name, "ACTION");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        if (STRCMP (pHttp->au1Value, "Add") == 0)
        {
            /* Checking with Policy-Map ID */
            STRCPY (pHttp->au1Name, "POLICY_MAPID");
            if (nmhTestv2FsDiffServClfrStatus
                (&u4ErrorCode, i4PolicyMapId, u1RowStatus) == SNMP_FAILURE)
            {
                DFS_UNLOCK ();
                IssSendError (pHttp, (CONST INT1 *) "Invalid Policy Map ID.");
                ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                              "\nWEB:Failed to test DiffServ RowStatus%d for PolicyMapIdID %d                            with  error code as %d\n",
                              u1RowStatus, i4PolicyMapId, u4ErrorCode);
                return;
            }
            /* Check if class map exists */
            if (nmhGetFsDiffServMultiFieldClfrStatus
                (i4ClassMapId, &i4Status) == SNMP_FAILURE)
            {
                DFS_UNLOCK ();
                IssSendError (pHttp, (CONST INT1 *) "Invalid Class Map ID");
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nWEB:Failed to retrieve the  DiffServ Status %d for ClassMapId %d \n",
                              i4Status, i4ClassMapId);
                return;
            }

            /* Check if the class-map already allocated to another policy-map.
             * We can't allocate the same class-map to different policy-map, because
             * Class-Map id == Datapath id */
            if (nmhGetFirstIndexFsDiffServClfrTable (&i4FirstClassMapId)
                != SNMP_FAILURE)
            {
                /* For the first time */
                i4NextClassMapId = i4FirstClassMapId;
                do
                {
                    nmhGetFsDiffServClfrMFClfrId (i4NextClassMapId, &i4MFCId);
                    if (i4MFCId == i4ClassMapId)
                    {
                        DFS_UNLOCK ();
                        IssSendError (pHttp,
                                      "Class map already allocated to another policy-map");
                        return;
                    }

                    i4FirstClassMapId = i4NextClassMapId;
                }
                while (nmhGetNextIndexFsDiffServClfrTable
                       (i4FirstClassMapId, &i4NextClassMapId) != SNMP_FAILURE);
            }
        }

        STRCPY (pHttp->au1Name, "TRAFFIC_RATE");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4TrafficRate = ATOI (pHttp->au1Value);

        STRCPY (pHttp->au1Name, "INPROF_ACT_TYPE");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4InProfActType = ATOI (pHttp->au1Value);

        if (u4InProfActType == 1)
        {
            STRCPY (pHttp->au1Name, "INPROF_ACT_VALUE");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            u4InDscp = ATOI (pHttp->au1Value);
        }
        else if (u4InProfActType == 2)
        {
            STRCPY (pHttp->au1Name, "INPROF_ACT_VALUE");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            u4InPrec = ATOI (pHttp->au1Value);
        }
        else if (u4InProfActType == 3)
        {
            STRCPY (pHttp->au1Name, "INPROF_ACT_VALUE");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            u4CosValue = ATOI (pHttp->au1Value);
        }

        STRCPY (pHttp->au1Name, "OUTPROF_ACT_TYPE");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4OutProfActType = ATOI (pHttp->au1Value);

        if (u4OutProfActType == 2)
        {
            STRCPY (pHttp->au1Name, "OUTPROF_ACT_VALUE");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            u4OutDscp = ATOI (pHttp->au1Value);
        }

        MEMSET (&ClfrEntry, 0, sizeof (tDiffServWebSetClfrEntry));
        ClfrEntry.i4DsClfrId = i4PolicyMapId;
        ClfrEntry.i4DsClfrMFClfrId = i4ClassMapId;
        ClfrEntry.i4DsClfrInProActionId = i4PolicyMapId;
        ClfrEntry.u4InProfActType = u4InProfActType;
        ClfrEntry.u4InDscp = u4InDscp;
        ClfrEntry.u4InPrec = u4InPrec;
        ClfrEntry.u4CosValue = u4CosValue;
        ClfrEntry.i4DsClfrOutProActionId = i4PolicyMapId;
        ClfrEntry.i4MeterId = i4PolicyMapId;
        ClfrEntry.u4OutProfActType = u4OutProfActType;
        ClfrEntry.u4OutDscp = u4OutDscp;
        ClfrEntry.u4TrafficRate = u4TrafficRate;

        if (DsWebnmSetPolicyMapEntry (&ClfrEntry, u1RowStatus, au1ErrStr) ==
            SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp, (CONST INT1 *) au1ErrStr);
            IssBcmProcessDfsPolicyMapPageGet (pHttp);
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set Web PolicyMapEntry%d, with RowStatus %d \n",
                          ClfrEntry, u1RowStatus);
            return;
        }

        DFS_UNLOCK ();
        IssBcmProcessDfsPolicyMapPageGet (pHttp);
        ISS_TRC (INIT_SHUT_TRC,
                 "\nWEB:Exit IssBcmProcessDfsPolicyMapPageSet function \n");
    }

/*********************************************************************
 *  function name : IssBcmProcessSchdAlgoPage
 *  description   : This function processes the request coming for the 
 *                  diffsrv Cosq Schedule Algorithm configuration page.
 *                     
 *  input(s)      : phttp - pointer to the global http data structure.
 *  output(s)     : none.
 *  return values : none
 *********************************************************************/

    VOID                IssBcmProcessSchdAlgoPage (tHttp * pHttp)
    {
        ISS_TRC (INIT_SHUT_TRC,
                 "\nWEB:Entry IssBcmProcessSchdAlgoPage function \n");
        if (pHttp->i4Method == ISS_GET)
        {
            IssBcmProcessSchdAlgoPageGet (pHttp);
        }
        else if (pHttp->i4Method == ISS_SET)
        {
            IssBcmProcessSchdAlgoPageSet (pHttp);
        }
        ISS_TRC (INIT_SHUT_TRC,
                 "\nWEB:Failed to IssBcmProcessSchdAlgoPage function \n");
        return;
    }

/*********************************************************************
 *  function name : IssBcmProcessSchdAlgoPageGet
 *  description   : This function processes the get request coming for the 
 *                  diffsrv Cosq Schedule Algorithm configuration page.
 *                  Get   
 *  input(s)      : phttp - pointer to the global http data structure.
 *  output(s)     : none.
 *  return values : none
 *********************************************************************/

    VOID                IssBcmProcessSchdAlgoPageGet (tHttp * pHttp)
    {
        INT4                i4NextIndex = 0;
        INT4                i4PreIndex = 0;
        INT4                i4CoSqAlg = 0;
        INT4                i4OutCome = 0;
        UINT4               u4Temp = 0;

        ISS_TRC (INIT_SHUT_TRC,
                 "\nWEB:Entry IssBcmProcessSchdAlgoPageGet function \n");
        WebnmRegisterLock (pHttp, DfsLock, DfsUnLock);
        DFS_LOCK ();

        i4OutCome = nmhGetFirstIndexFsDiffServCoSqAlgorithmTable (&i4NextIndex);
        if (i4OutCome == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            ISS_TRC (ALL_FAILURE_TRC,
                     "\nWEB:Failed to retrieve Diff Serv Cosq Algorithm Table \n");
            return;
        }

        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4Temp = pHttp->i4Write;

        do
        {
            pHttp->i4Write = u4Temp;

            STRCPY (pHttp->au1KeyString, "PORT_NUM_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextIndex);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsDiffServCoSqAlgorithm (i4NextIndex, &i4CoSqAlg);
            STRCPY (pHttp->au1KeyString, "COSQ_ALGO_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4CoSqAlg);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

            i4PreIndex = i4NextIndex;
        }
        while (nmhGetNextIndexFsDiffServCoSqAlgorithmTable
               (i4PreIndex, &i4NextIndex) == SNMP_SUCCESS);
        DFS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        ISS_TRC (INIT_SHUT_TRC,
                 "\nWEB:Exit IssBcmProcessSchdAlgoPageGet function  \n");
        return;

    }

/*********************************************************************
 *  function name : IssBcmProcessSchdAlgoPageSet
 *  description   : This function processes the set request coming for the 
 *                  diffsrv Cosq Schedule Algorithm configuration page.
 *                  Set   
 *  input(s)      : phttp - pointer to the global http data structure.
 *  output(s)     : none.
 *  return values : none
 *********************************************************************/

    VOID                IssBcmProcessSchdAlgoPageSet (tHttp * pHttp)
    {
        INT4                i4PortNum = 0;
        INT4                i4CoSqAlg = 0;
        UINT4               u4ErrCode = 0;

        ISS_TRC (INIT_SHUT_TRC,
                 "\nWEB:Entry IssBcmProcessSchdAlgoPageSet function \n");
        STRCPY (pHttp->au1Name, "PORT_NUM");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4PortNum = ATOI (pHttp->au1Value);

        STRCPY (pHttp->au1Name, "COSQ_ALGO");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4CoSqAlg = ATOI (pHttp->au1Value);

        WebnmRegisterLock (pHttp, DfsLock, DfsUnLock);
        DFS_LOCK ();

        if (nmhTestv2FsDiffServCoSqAlgorithm
            (&u4ErrCode, i4PortNum, i4CoSqAlg) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set the CoSq Algorithm");
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test Diff Serv CoSq Algorithm%d for port number %d                            with  error code as %d\n",
                          i4CoSqAlg, i4PortNum, u4ErrorCode);
            return;
        }

        if (nmhSetFsDiffServCoSqAlgorithm (i4PortNum, i4CoSqAlg) ==
            SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set the CoSq Algorithm");
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set Diff Serv CoSq Algorithm%d for port number %d\n",
                          i4CoSqAlg, i4PortNum);
            return;
        }

        DFS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssBcmProcessSchdAlgoPageGet (pHttp);
        ISS_TRC (INIT_SHUT_TRC,
                 "\nWEB:Exit IssBcmProcessSchdAlgoPageSet function \n");
    }

/*********************************************************************
 *  function name : IssBcmProcessDfsCosqWeightBWConfPage
 *  description   : This function processes the request coming for the 
 *                  diffsrv Cosq Weight and Bandwidth configuration page.
 *                     
 *  input(s)      : phttp - pointer to the global http data structure.
 *  output(s)     : none.
 *  return values : none
 *********************************************************************/

    VOID                IssBcmProcessDfsCosqWeightBWConfPage (tHttp * pHttp)
    {
        ISS_TRC (INIT_SHUT_TRC,
                 "\nWEB:Entry IssBcmProcessDfsCosqWeightBWConfPage function \n");
        if (pHttp->i4Method == ISS_GET)
        {
            IssBcmProcessDfsCosqWeightBWConfPageGet (pHttp);
        }
        else if (pHttp->i4Method == ISS_SET)
        {
            IssBcmProcessDfsCosqWeightBWConfPageSet (pHttp);
        }
        ISS_TRC (INIT_SHUT_TRC,
                 "\nWEB:Failed to IssBcmProcessDfsCosqWeightBWConfPage function \n");
        return;
    }

/*********************************************************************
 *  function name : IssBcmProcessDfsCosqWeightBWConfPageGet
 *  description   : This function processes the get request coming for the 
 *                  diffsrv Cosq Weight and Bandwidth configuration page.
 *                  Get   
 *  input(s)      : phttp - pointer to the global http data structure.
 *  output(s)     : none.
 *  return values : none
 *********************************************************************/

    VOID                IssBcmProcessDfsCosqWeightBWConfPageGet (tHttp * pHttp)
    {
        INT4                i4PortNum = 0;
        INT4                i4CosqId = 0;
        UINT4               u4CosqMaxBw = 0;
        UINT4               u4CosqMinBw = 0;
        UINT4               u4CosqWt = 0;
        UINT4               i4CosqFlag = 0;
        UINT4               u4Temp = 0;

        ISS_TRC (INIT_SHUT_TRC,
                 "\nWEB:Entry IssBcmProcessDfsCosqWeightBWConfPageGet  function \n");
        IssPrintAvailableL2Ports (pHttp);

        WebnmRegisterLock (pHttp, DfsLock, DfsUnLock);
        DFS_LOCK ();

        STRCPY (pHttp->au1Name, "PORT_NUM");
        if (HttpGetValuebyName
            (pHttp->au1Name, pHttp->au1Value,
             pHttp->au1PostQuery) == ENM_FAILURE)
        {
            nmhGetFirstIndexFsDiffServCoSqWeightBwTable (&i4PortNum, &i4CosqId);
        }
        else
        {
            i4PortNum = ATOI (pHttp->au1Value);
        }

        STRCPY (pHttp->au1KeyString, "PORT_NUM_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4PortNum);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));

        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4Temp = pHttp->i4Write;

        for (; i4CosqId <= 7; i4CosqId++)
        {
            pHttp->i4Write = u4Temp;

            STRCPY (pHttp->au1KeyString, "COSQ_ID_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4CosqId);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsDiffServCoSqWeight (i4PortNum, i4CosqId, &u4CosqWt);
            STRCPY (pHttp->au1KeyString, "COSQ_WEIGHT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4CosqWt);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsDiffServCoSqBwMin (i4PortNum, i4CosqId, &u4CosqMinBw);
            STRCPY (pHttp->au1KeyString, "COSQ_MIN_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4CosqMinBw);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsDiffServCoSqBwMax (i4PortNum, i4CosqId, &u4CosqMaxBw);
            STRCPY (pHttp->au1KeyString, "COSQ_MAX_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4CosqMaxBw);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetFsDiffServCoSqBwFlags (i4PortNum, i4CosqId, &i4CosqFlag);
            STRCPY (pHttp->au1KeyString, "COSQ_FLAG_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4CosqFlag);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        }
        DFS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        ISS_TRC (INIT_SHUT_TRC,
                 "\nWEB:Exit IssBcmProcessDfsCosqWeightBWConfPageGet  function \n");
        return;

    }

/*********************************************************************
 *  function name : IssBcmProcessDfsCosqWeightBWConfPageSet
 *  description   : This function processes the request coming for the 
 *                  diffsrv Cosq Weight and Bandwidth configuration page.
 *                  Set   
 *  input(s)      : phttp - pointer to the global http data structure.
 *  output(s)     : none.
 *  return values : none
 *********************************************************************/

    VOID                IssBcmProcessDfsCosqWeightBWConfPageSet (tHttp * pHttp)
    {
        INT4                i4PortNum = 0;
        INT4                i4CosqId = 0;
        UINT4               u4CosqMinBw = 0;
        UINT4               u4CosqWt = 0;
        UINT4               u4ErrCode = 0;

        ISS_TRC (INIT_SHUT_TRC,
                 "\nWEB:Entry IssBcmProcessDfsCosqWeightBWConfPageSet function \n");
        STRCPY (pHttp->au1Name, "PORT_NUM");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4PortNum = ATOI (pHttp->au1Value);

        STRCPY (pHttp->au1Name, "COSQ_ID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4CosqId = ATOI (pHttp->au1Value);

        STRCPY (pHttp->au1Name, "COSQ_WEIGHT");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4CosqWt = ATOI (pHttp->au1Value);

        STRCPY (pHttp->au1Name, "COSQ_MIN");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4CosqMinBw = ATOI (pHttp->au1Value);

        WebnmRegisterLock (pHttp, DfsLock, DfsUnLock);
        DFS_LOCK ();

        if (nmhTestv2FsDiffServCoSqWeight
            (&u4ErrCode, i4PortNum, i4CosqId, u4CosqWt) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set the CoSq Weight");
            ISS_TRC_ARG4 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to test Diff Serv CoSq Weight%d for Cosq ID%d with port number %d                                 with  error code as %d\n",
                          u4CosqWt, i4CosqId, i4PortNum, u4ErrorCode);
            return;
        }

        if (nmhSetFsDiffServCoSqWeight (i4PortNum, i4CosqId, u4CosqWt) ==
            SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set the CoSq Weight");
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "\nWEB:Failed to set Diff Serv CoSq Weight%d for Cosq ID%d with port number \n",
                          u4CosqWt, i4CosqId, i4PortNum);
            return;
        }

        if (u4CosqMinBw != 0)
        {
            if (nmhTestv2FsDiffServCoSqBwMin
                (&u4ErrCode, i4PortNum, i4CosqId, u4CosqMinBw) == SNMP_FAILURE)
            {
                DFS_UNLOCK ();
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to set the CoSq Minimum Bandwidth");
                ISS_TRC_ARG4 (ALL_FAILURE_TRC,
                              "\nWEB:Failed to test Diff Serv CoSq Minimum Bandwidth%d for Cosq ID%d with port number %d with  error code as %d\n",
                              u4CosqMinBw, i4CosqId, i4PortNum, u4ErrorCode);
                return;
            }

            if (nmhSetFsDiffServCoSqBwMin
                (i4PortNum, i4CosqId, u4CosqMinBw) == SNMP_FAILURE)
            {
                DFS_UNLOCK ();
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to set the CoSq Minimum Bandwidth");
                ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                              "\nWEB:Failed to set Diff Serv CoSq Minimum Bandwidth%d for Cosq ID%d with port number %d\n",
                              u4CosqMinBw, i4CosqId, i4PortNum);
                return;
            }
        }

        DFS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssBcmProcessDfsCosqWeightBWConfPageGet (pHttp);
        ISS_TRC (INIT_SHUT_TRC,
                 "\nWEB:Exit IssBcmProcessDfsCosqWeightBWConfPageSet function  \n");
    }
#endif
#endif
