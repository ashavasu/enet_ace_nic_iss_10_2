/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved               
*                                                                    
* $Id: issexcmd.def,v 1.13 2016/03/19 13:03:05 siva Exp $                                                         
*                                                                    
*********************************************************************/

DEFINE GROUP: ISS_EXT_INT_CMDS 

#if defined (NPAPI_WANTED)

COMMAND : storm-control { broadcast |multicast | dlf } level <integer(1-262143)>
ACTION  : {
	    UINT4 u4Type =0;
	    
	    if ($1 != NULL)
	    {
	      u4Type = ISS_RATE_BCAST_LIMIT;
	    }
	    else if ($2 != NULL)
	    {
	      u4Type = ISS_RATE_MCAST_LIMIT;
	    }
            else if ($3 != NULL) 
	    {
	      u4Type = ISS_RATE_DLF_LIMIT ;
	    }

	    cli_process_iss_ext_cmd ( CliHandle,  CLI_ISS_STORM_CONTROL, NULL, 
                                                    u4Type, $5);
	 }

SYNTAX  : storm-control { broadcast |multicast | dlf } level <rate-value(1-262143)>
PRVID   : 15
HELP    : Sets storm control rate for broadcast , multicast and DLF packets
CXT_HELP : storm-control Storm control related configuration |
           broadcast Set storm control for Broadcast Packets |
           multicast Set storm control for Multicast Packets |
           dlf Set storm control for dlf Packets |
           level Storm control level |
           (1-262143) Storm control rate value |
           <CR> Configures storm control for specified Packet type on the Interface
           

COMMAND : no storm-control { broadcast |multicast | dlf } level 
ACTION  : {	   
             UINT4 u4Type =0;
	    
      	    if ($2 != NULL)
             {
                 u4Type = ISS_RATE_BCAST_LIMIT;
             }
             else if ($3 != NULL)
             {
                 u4Type = ISS_RATE_MCAST_LIMIT;
             }
             else if ($4 != NULL) 
             {
                 u4Type = ISS_RATE_DLF_LIMIT;
             }
             cli_process_iss_ext_cmd ( CliHandle,  CLI_ISS_NO_STORM_CONTROL,                                             NULL, u4Type);
         }
 
SYNTAX  : no storm-control { broadcast |multicast | dlf } level 
PRVID   : 15
HELP    : Sets storm control rate for broadcast , multicast and DLF packets to the default value
CXT_HELP : no Disables Storm control |
           storm-control Storm control related configuration |
           broadcast Disables storm control for Broadcast Packets |
           multicast Disables storm control for Multicast Packets |
           dlf Disables storm control for dlf Packets |
           level Storm control level |
           <rate-value> Storm control value |
           <CR> Disables storm control for specified Packet type on the Interface

COMMAND : rate-limit output ([rate-value <integer(0-80000000)>] [burst-value <integer(0-80000000)>])
ACTION  : cli_process_iss_ext_cmd(CliHandle, CLI_ISS_PORT_RATE_LIMIT, NULL, $3, $5);
SYNTAX  : rate-limit output ([rate-value <integer(0-80000000)>] [burst-value <integer(0-80000000)>])
PRVID   : 15
HELP    : Enables the rate limiting and burst size rate limiting by configuring the egress packet rate of an interface
CXT_HELP : rate-limit Rate limiting and Burst size related configuration |
           output Indicates egress packet rate on an Interface |
	   rate-value Rate limit related configuration |
           (0-80000000) Rate limit value for an Interface in kbps |
	   burst-value Burst size related configuration |
           (0-80000000) Burst size value for an Interface in kbits |
           <CR> Configures rate limit and Burst size value for an Interface

COMMAND : no rate-limit output [rate-limit] [burst-limit]
ACTION  : cli_process_iss_ext_cmd(CliHandle, CLI_ISS_NO_PORT_RATE_LIMIT, NULL, $3, $4);
SYNTAX  : no rate-limit output [rate-limit] [burst-limit]
PRVID   : 15
HELP    : Disables the rate limiting and burst size rate limit on an egress port.
CXT_HELP : no Disables the rate limiting and burst size rate limit on an egress port |
           rate-limit Rate limiting and Burst size related configuration |
           output Indicates egress packet rate on an Interface |
           rate-limit Rate limit related configuration |
           burst-limit Burst size related configuration |
           <CR> Disables the rate limiting and burst size rate limit on an Interface
 
COMMAND : storm-control {enable|disable}
ACTION  : cli_process_iss_ext_cmd(CliHandle,CLI_ISS_PORT_RATE_DEFAULT,NULL,$1,$2);
SYNTAX  : storm-control {enable|disable}
PRVID   : 15
HELP    : enables or disables storm-control feature for a port
CXT_HELP : storm-control Configures storm-control related information |
           enable Enables storm-control for port |
           disable Disables storm-control for port |
           <CR> Enable or Disable storm-control 


#endif

END GROUP   
