/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: issexglob.h,v 1.5 2015/09/03 11:40:22 siva Exp $
*
* Description: Global Data
*********************************************************************/
#ifndef _ISSEXGLOB_H        
#define _ISSEXGLOB_H        

extern UINT4                  gu4IssDebugFlags;

/*MAC addresses for reserved Feame Control PKT*/
#define ISS_BPDU_MAC_ADDRESS gBpduControlMacAddr
#define ISS_LLDPDU_MAC_ADDRESS gLldpduControlMacAddr
#define ISS_LACPDU_MAC_ADDRESS gLacppduControlMacAddr
#define ISS_EAP_MAC_ADDRESS gEapControlMacAddr
#define ISS_GVRP_MAC_ADDRESS gEapControlMacAddr


#ifdef _ISSEXSYS_C
tIssExtGlobalInfo                gIssExGlobalInfo;
tIssRedirectIntfGrpTable         *gpIssRedirectIntfInfo;

tMacAddr gBpduControlMacAddr =
        { 0x01, 0x80, 0xc2, 0x00, 0x00, 0x00 };

tMacAddr gLldpduControlMacAddr =
        { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x0E };

tMacAddr gLacppduControlMacAddr =
        { 0x01, 0x80, 0xc2, 0x00, 0x00, 0x02 };

tMacAddr gEapControlMacAddr =
        { 0x01, 0x80, 0xc2, 0x00, 0x00, 0x03 };

tMacAddr gL2CacheGvrpAddrGroup =
        { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x20 };


#else
extern tIssExtGlobalInfo         gIssExGlobalInfo;
extern tIssRedirectIntfGrpTable         *gpIssRedirectIntfInfo;
/*This is already defined in issglob.h in ISS/common/system */ 
extern UINT4                  gu4IssCidrSubnetMask[ISS_MAX_CIDR + 1];

extern tMacAddr gBpduControlMacAddr;
extern tMacAddr gLldpduControlMacAddr;
extern tMacAddr  gLacppduControlMacAddr;
extern tMacAddr gEapControlMacAddr;
extern tMacAddr gL2CacheGvrpAddrGroup;
#endif

#endif
