/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: issexmacr.h,v 1.15 2016/03/19 13:03:05 siva Exp $
 *
 * Description: This file include all the macros required 
 *              for the operation of the ACL module
 ****************************************************************************/

#ifndef _ISSEXMACR_H
#define _ISSEXMACR_H

#define ISS_MAX_RATE_VALUE              65535
#define ISS_DEF_RATE_VALUE              500
#define ISS_MIN_RATE_VALUE              1
#define ISS_STORM_CONTROL_ENABLE        1
#define ISS_STORM_CONTROL_DISABLE       0
#define ISS_RATE_ZEROVAL                0

/* Macros for validattion */
#define ISS_MIN_PROTOCOL_ID             1536
#define ISS_MAX_PROTOCOL_ID             65535
#define ISS_MIN_FILTER_ID               0
#define ISS_MAX_FILTER_ID               65535
#define ISS_MAX_FILTER_PRIORITY         255
#define ISS_DEFAULT_FILTER_PRIORITY     1
#define ISS_DEFAULT_PROTOCOL_TYPE       0
#define ISS_DEF_FILTER_MASK             0
#define ISS_MIN_DSCP_VALUE              0
#define ISS_MAX_DSCP_VALUE              63
/* ISS l2  ACL */
#define ISS_DEFAULT_VLAN_PRIORITY       -1
#define ISS_SERVICE_VLAN_ETHER_TYPE     0x88a8
#define ISS_CUSTOMER_VLAN_ETHER_TYPE    0x8100


/* Macros for ACL Queue */
#define ISS_SYS_ACL_QUEUE  "ACLQ"
#define ISS_QUEUE_SIZE   30
#define ISS_ACL_RM_MESSAGE  1

/* Macros for Hardware Audit Np Sync */
#define ACL_NPSYNC_BLK() \
  (gIssExGlobalInfo.AclRedGlobalInfo.u1NpSyncBlockCount)
#define ISS_MAX_HW_NP_BUF_ENTRY 1

#define ISS_MAX_SHADOW_TEMP_SIZE  1 /* To allocate additional shodow 
    memory to assist L2/L3 hardware audit process */

#define ISS_ACL_NP_SYNC_UPDATE 1
#define ISS_ACL_NP_SYNC_FAILURE 2
#define ISS_ACL_NP_SYNC_SUCCESS 3
#define ISS_MIN_REDIRECT_GRP_ID   0
#define ISS_MAX_REDIRECT_GRP_ID   50

/* Macros of Redundancy Bulk Update */

#define ACL_RED_BULK_REQUEST_MSG 1
#define ACL_DYN_L2_HW_INFO  2
#define ACL_DYN_L3_HW_INFO  3
#define ACL_RED_BULK_UPDT_TAIL_MSG 4

#define ACL_RED_BULK_REQ_SIZE  3
#define ACL_DYN_L2_HW_INFO_SIZE  (3 + 4 + (4 * ISS_FILTER_SHADOW_MEM_SIZE))
                                        /* 3 bytes for Type of Length +
                                           4 Bytes for Filter Number + 
                                           (4 * ISS_FILTER_SHADOW_MEM_SIZE)
                                    bytes for Hardware Handle info
                                         */
#define ACL_DYN_L3_HW_INFO_SIZE  (3 + 4 + (4 * ISS_FILTER_SHADOW_MEM_SIZE))
                                        /* 3 bytes for Type of Length +
                                           4 Bytes for Filter Number + 
                                           (4 * ISS_FILTER_SHADOW_MEM_SIZE)
                                    bytes for Hardware Handle info
                                         */
#define ACL_RED_BULK_UPDT_TAIL_SIZE 3
#define ACL_DB_MAX_BUF_SIZE  1500

#define ISS_FILTER_SHADOW_POOL_SIZE ISS_MAX_L2_FILTERS + ISS_MAX_L3_FILTERS + \
        ISS_MAX_SHADOW_TEMP_SIZE + ISS_MAX_UDB_FILTERS  

/* MEM Pool Id Definitions */
#define ISS_RATEENTRY_POOL_ID            gIssExGlobalInfo.IssRateCtrlPoolId
#define ISS_L2FILTERENTRY_POOL_ID        gIssExGlobalInfo.IssL2FilterPoolId
#define ISS_L3FILTERENTRY_POOL_ID        gIssExGlobalInfo.IssL3FilterPoolId

#define ISS_FILTER_SHADOW_POOL_ID     gIssExGlobalInfo.IssFilterShadowPoolId
#define ISS_ACL_MSG_QUEUE_POOL_ID     gIssExGlobalInfo.AclQueuePoolId
#define ISS_ACL_MSG_QUE_ID      gIssExGlobalInfo.AclQueId
#define ISS_UDB_FILTER_TABLE_POOL_ID     gIssExGlobalInfo.IssUdbTablePoolId
#define ISS_UDB_FILTERENTRY_POOL_ID      gIssExGlobalInfo.IssUdbFilterPoolId

/*Reserve frame MEMPOOL Id declaration*/
#define ISS_RESERV_FRAME_CTRL_ENTRY_POOL_ID gIssExGlobalInfo.ISSResrvFrmePoolId

#define ISS_RESERV_FRAME_POOL_SIZE ISS_MAX_RESERV_FRM_CTRL_ID
/* Filter List */
#define ISS_L2FILTER_LIST               gIssExGlobalInfo.IssL2FilterListHead
#define ISS_L2FILTER_LIST_COUNT   gIssExGlobalInfo.IssL2FilterListHead.u4_Count
#define ISS_L2FILTER_LIST_HEAD          (&ISS_L2FILTER_LIST)->Tail
#define ISS_IS_L2FILTER_ID_VALID(ISSId) \
        (((ISSId <= ISS_MIN_FILTER_ID) || \
        (ISSId > ISS_MAX_FILTER_ID)) ? ISS_FALSE : ISS_TRUE)

#define ISS_L3FILTER_LIST               gIssExGlobalInfo.IssL3FilterListHead
#define ISS_L3FILTER_LIST_COUNT   gIssExGlobalInfo.IssL3FilterListHead.u4_Count
#define ISS_L3FILTER_LIST_HEAD          (&ISS_L3FILTER_LIST)->Tail
/*Adding Sorted List based on filter priority to be in sync with h/w list*/
#define ISS_L3FILTER_SORTED_LIST        gIssExGlobalInfo.IssL3FilterSortedListHead
#define ISS_L3FILTER_SORTED_LIST_HEAD   (&ISS_L3FILTER_SORTED_LIST)->Tail
#define ISS_L2FILTER_SORTED_LIST        gIssExGlobalInfo.IssL2FilterSortedListHead
#define ISS_L2FILTER_SORTED_LIST_HEAD   (&ISS_L2FILTER_SORTED_LIST)->Tail

#define ISS_IS_L3FILTER_ID_VALID(ISSId) \
        (((ISSId <= ISS_MIN_FILTER_ID) || \
        (ISSId > ISS_MAX_FILTER_ID)) ? ISS_FALSE : ISS_TRUE)

#define ISS_L2FILTERENTRY_MEMBLK_COUNT  ISS_MAX_L2_FILTERS
#define ISS_L3FILTERENTRY_MEMBLK_COUNT  ISS_MAX_L3_FILTERS
#define ISS_UDB_FILTER_TABLE_MEMBLK_COUNT 500
#define ISS_UDB_FILTERENTRY_MEMBLK_COUNT  500

#define ISS_RATEENTRY_MEMBLK_COUNT      (BRG_MAX_PHY_PLUS_LOG_PORTS + 1)

#define ISS_RATEENTRY_MEMBLK_SIZE       sizeof(tIssRateCtrlEntry)

#define ISS_L2FILTERENTRY_MEMBLK_SIZE   sizeof(tIssL2FilterEntry)

#define  ISS_CREATE_RATEENTRY_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,ISS_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)

#define  ISS_CREATE_L2FILTERENTRY_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,ISS_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)

#define  ISS_CREATE_L3FILTERENTRY_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,ISS_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)

#define  ISS_CREATE_ACL_MSG_QUEUE_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,ISS_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)

#define  ISS_CREATE_FILTER_SHADOW_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,ISS_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)

#define  ISS_CREATE_RESERV_FRAME_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,ISS_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)

#define  ISS_DELETE_RATEENTRY_MEM_POOL(IssPoolId)\
         MemDeleteMemPool((tMemPoolId) IssPoolId)

#define  ISS_DELETE_L2FILTERENTRY_MEM_POOL(IssPoolId)\
         MemDeleteMemPool((tMemPoolId) IssPoolId)

#define  ISS_DELETE_L3FILTERENTRY_MEM_POOL(IssPoolId)\
         MemDeleteMemPool((tMemPoolId) IssPoolId)

#define  ISS_DELETE_UDB_FILTER_TABLE_MEM_POOL(IssPoolId)\
         MemDeleteMemPool((tMemPoolId) IssPoolId)

#define  ISS_CREATE_UDB_FILTER_TABLE_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,ISS_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)


#define  ISS_CREATE_UDB_FILTER_ENTRY_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,ISS_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)

#define  ISS_DELETE_RESERVE_FRAME_MEM_POOL(IssPoolId)\
         MemDeleteMemPool((tMemPoolId) IssPoolId)

#define  ISS_DELETE_ACL_MSG_QUEUE_MEM_POOL(IssPoolId)\
         MemDeleteMemPool((tMemPoolId) IssPoolId)

#define  ISS_DELETE_FILTER_SHADOW_MEM_POOL(IssPoolId)\
         MemDeleteMemPool((tMemPoolId) IssPoolId)

#define  ISS_RATEENTRY_ALLOC_MEM_BLOCK(pu1Block)\
         (pu1Block = \
          (tIssRateCtrlEntry *) (MemAllocMemBlk (ISS_RATEENTRY_POOL_ID)))

#define  ISS_RESERVE_FRAME_ALLOC_MEM_BLOCK(pu1Block)\
  (pu1Block = \
   (tIssReservFrmCtrlTable *) (MemAllocMemBlk (ISS_RESERV_FRAME_CTRL_ENTRY_POOL_ID)))

#define  ISS_L2FILTERENTRY_ALLOC_MEM_BLOCK(pu1Block)\
 (pu1Block = \
  (tIssL2FilterEntry *)(MemAllocMemBlk (ISS_L2FILTERENTRY_POOL_ID)))

#define  ISS_L3FILTERENTRY_ALLOC_MEM_BLOCK(pu1Block)\
         (pu1Block = \
          (tIssL3FilterEntry *)(MemAllocMemBlk (ISS_L3FILTERENTRY_POOL_ID)))

#define ISS_UDB_FILTER_TABLE_MEMBLK_SIZE sizeof (tIssUserDefinedFilterTable)

#define ISS_UDB_FILTER_ENTRY_MEMBLK_SIZE   sizeof (tIssUDBFilterEntry)


#define  ISS_ACL_MSG_QUEUE_ALLOC_MEM_BLOCK(pu1Block) \
         (pu1Block = \
          (tAclQMsg *)(MemAllocMemBlk (ISS_ACL_MSG_QUEUE_POOL_ID)))

#define  ISS_FILTER_SHADOW_ALLOC_MEM_BLOCK(pu1Block) \
         (pu1Block = \
          (tIssFilterShadowTable *)(MemAllocMemBlk (ISS_FILTER_SHADOW_POOL_ID)))

#define  ISS_RATEENTRY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_RATEENTRY_POOL_ID, (UINT1 *)pu1Msg)

#define  ISS_L2FILTERENTRY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_L2FILTERENTRY_POOL_ID, (UINT1 *)pu1Msg)

#define  ISS_L3FILTERENTRY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_L3FILTERENTRY_POOL_ID, (UINT1 *)pu1Msg)

#define  ISS_ACL_MSG_QUEUE_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_ACL_MSG_QUEUE_POOL_ID, (UINT1 *)pu1Msg)

#define  ISS_RESERVE_FRAME_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_RESERV_FRAME_CTRL_ENTRY_POOL_ID, (UINT1 *)pu1Msg)

#define ISS_L2_FILTER_ID_SCAN(u4L2FilterId) \
    for (u4L2FilterId = 1; \
         u4L2FilterId <= ISS_MAX_FILTER_ID; u4L2FilterId++)

#define ISS_L3_FILTER_ID_SCAN(u4L3FilterId) \
    for (u4L3FilterId = 1; \
         u4L3FilterId <= ISS_MAX_FILTER_ID; u4L3FilterId++)
#define  ISS_FILTER_SHADOW_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_FILTER_SHADOW_POOL_ID, (UINT1 *)pu1Msg)
#define ISS_UDB_FILTER_ID_SCAN(u4UdbFilterId) \
    for (u4UdbFilterId = 1; \
         u4UdbFilterId <= ISS_MAX_FILTER_ID; u4UdbFilterId++)

#define ISS_REDIRECT_ID_GET(i4FilterNo, u4RedirectId) \
{\
    UINT4 u4Index; \
    for (u4Index = 0;u4Index < ISS_MAX_REDIRECT_GRP_ID; u4Index++) \
    {\
        if ((gpIssRedirectIntfInfo[u4Index].u4AclId ==(UINT4) i4FilterNo)\
            && (gpIssRedirectIntfInfo[u4Index].u1AclIdType == ISS_L2_REDIRECT))\
                    u4RedirectId = u4Index + 1; \
        else if ((gpIssRedirectIntfInfo[u4Index].u4AclId ==(UINT4) i4FilterNo)\
                 && (gpIssRedirectIntfInfo[u4Index].u1AclIdType == ISS_L3_REDIRECT))\
                    u4RedirectId = u4Index + 1; \
    }\
}


#define ISS_UDB_FILTER_TABLE_LIST        gIssExGlobalInfo.IssUdbFilterTableHead
#define ISS_UDB_FILTER_TABLE_LIST_COUNT   gIssExGlobalInfo.IssUdbFilterTableHead.u4_Count
#define ISS_IS_REDIRECT_GRP_ID_VALID(ISSId) \
        (((ISSId <= ISS_MIN_REDIRECT_GRP_ID) || \
        (ISSId > ISS_MAX_REDIRECT_GRP_ID)) ? ISS_FALSE : ISS_TRUE)

 #define ISS_IS_UDB_FILTER_ID_VALID(ISSId) \
        (((ISSId == ISS_MIN_FILTER_ID) || \
        (ISSId > ISS_MAX_FILTER_ID)) ? ISS_FALSE : ISS_TRUE)
#define  ISS_UDB_FILTER_TABLE_ALLOC_MEM_BLOCK(pu1Block) \
         (pu1Block = \
          (tIssUserDefinedFilterTable *)(MemAllocMemBlk (ISS_UDB_FILTER_TABLE_POOL_ID)))
#define  ISS_UDB_FILTERENTRY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_UDB_FILTERENTRY_POOL_ID, (UINT1 *)pu1Msg)
#define  ISS_UDB_FILTER_ENTRY_ALLOC_MEM_BLOCK(pu1Block) \
         (pu1Block = \
          (tIssUDBFilterEntry *)(MemAllocMemBlk (ISS_UDB_FILTERENTRY_POOL_ID)))

#define  ISS_UDB_FILTERTABLE_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_UDB_FILTER_TABLE_POOL_ID, (UINT1 *)pu1Msg)

/************************************************
* Reserved Frame control ID validation
*************************************************/
#define ISS_IS_RESERV_FRM_CTRL_ID_VALID(ISSId) \
        (((ISSId <= ISS_MIN_RESERV_FRM_CTRL_ID) || \
        (ISSId > ISS_MAX_RESERV_FRM_CTRL_ID)) ? ISS_FALSE : ISS_TRUE)

/**********************************************
* Macro for Reserve frame control Index 
***********************************************/
#define GET_RESERVE_FRAME_CTRL_INFO(ControlIndex) \
 (gIssExGlobalInfo.apISSResrvFrmeEntry[ControlIndex - 1])

/**************************************************
 *  *Traffic Separtion and Protection Control MACROS *
 *   **************************************************/
#define  ACL_MIN_L2_SYSTEM_PROTOCOLS                    (1)
#define  ACL_MIN_L3_SYSTEM_PROTOCOLS                    (1)
#define ACL_OFFSET(x,y)  ((FS_ULONG)(&(((x *)0)->y)))

#define GET_L3FILTER_PTR_FROM_SORT_LST(x)  ((tIssL3FilterEntry *)(((FS_ULONG)x) \
                                                                                     - ACL_OFFSET(tIssL3FilterEntry, IssSortedNextNode)))

#define GET_L2FILTER_PTR_FROM_SORT_LST(x)  ((tIssL2FilterEntry *)(((FS_ULONG)x) \
                                                                                     - ACL_OFFSET(tIssL2FilterEntry, IssSortedNextNode)))

#endif  /* _ISSEXMACR_H */
