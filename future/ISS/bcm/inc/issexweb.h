/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: issexweb.h,v 1.14 2014/09/25 12:15:10 siva Exp $
 * 
 * Description: This file contains the proto type 
 *              declaration of the web functions.
 *****************************************************************************/

#ifndef _BCMISSWEB_H
#define _BCMISSWEB_H

#include "webiss.h"
#include "isshttp.h"
#include "aclcli.h"
#include "iss.h"
#include "fsisselw.h"
#include "diffsrv.h"

#define   ACL_EXTENDED_START 1001

/* Prototypes for specific pages processing */
INT4 IssProcessCustomPages (tHttp * pHttp);
VOID IssRedirectMacFilterPage (tHttp *pHttp);
VOID IssRedirectVlanSubnetEnablePage (tHttp *pHttp);
VOID IssRedirectDiffSrvPage (tHttp *pHttp);
VOID IssBcmProcessIPFilterConfPage (tHttp * pHttp);
VOID IssBcmProcessIPStdFilterConfPage (tHttp * pHttp);
VOID IssBcmProcessMACFilterConfPage (tHttp * pHttp);
VOID IssBcmProcessVlanPvidSettingsPage (tHttp * pHttp);
VOID IssBcmProcessVlanSubnetPerPortSettingsPage (tHttp * pHttp);
VOID IssBcmProcessVlanSubnetPerPortSettingsPageGet (tHttp * pHttp);
VOID IssBcmProcessVlanSubnetPerPortSettingsPagePost (tHttp * pHttp);
VOID IssBcmProcessIPFilterConfPageSet (tHttp * pHttp);
VOID IssBcmProcessIPFilterConfPageGet (tHttp * pHttp);
VOID IssBcmProcessIPStdFilterConfPageGet (tHttp * pHttp);
VOID IssBcmProcessIPStdFilterConfPageSet (tHttp * pHttp);
VOID IssBcmProcessMACFilterConfPageGet (tHttp * pHttp);
VOID IssBcmProcessMACFilterConfPageSet (tHttp * pHttp);
VOID IssBcmProcessDfsPolicyMapPage (tHttp * pHttp);
VOID IssBcmProcessDfsPolicyMapPageGet (tHttp * pHttp);
VOID IssBcmProcessDfsPolicyMapPageSet (tHttp * pHttp);
VOID IssBcmProcessSchdAlgoPage (tHttp * pHttp);
VOID IssBcmProcessSchdAlgoPageGet (tHttp * pHttp);
VOID IssBcmProcessSchdAlgoPageSet (tHttp * pHttp);
VOID IssBcmProcessDfsCosqWeightBWConfPage (tHttp * pHttp);
VOID IssBcmProcessDfsCosqWeightBWConfPageGet (tHttp * pHttp);
VOID IssBcmProcessDfsCosqWeightBWConfPageSet (tHttp * pHttp); 

extern INT1 nmhGetDot1qFutureVlanPortSubnetBasedClassification ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhSetDot1qFutureVlanPortSubnetBasedClassification ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhTestv2Dot1qFutureVlanPortSubnetBasedClassification ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhGetDot1qFutureVlanSubnetBasedOnAllPorts ARG_LIST((INT4 *));
extern INT1 nmhSetDot1qFutureVlanSubnetBasedOnAllPorts ARG_LIST((INT4 ));
extern INT1 nmhTestv2Dot1qFutureVlanSubnetBasedOnAllPorts ARG_LIST((UINT4 *  ,INT4 ));
/* Proto Validate Index Instance for Dot1qFutureVlanPortSubnetMapTable. */
/* Proto Type for Low Level GET FIRST fn for Dot1qFutureVlanPortSubnetMapTable  */
extern INT1 nmhGetFirstIndexDot1qFutureVlanPortSubnetMapTable ARG_LIST((INT4 * , UINT4 *));
extern INT1 nmhGetNextIndexDot1qFutureVlanPortSubnetMapTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));
extern INT1 nmhGetDot1qFutureVlanPortSubnetMapVid ARG_LIST((INT4  , UINT4 ,INT4 *));
extern INT1 nmhGetDot1qFutureVlanPortSubnetMapARPOption ARG_LIST((INT4  , UINT4 ,INT4 *));
extern INT1 nmhGetDot1qFutureVlanPortSubnetMapRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));
extern INT1 nmhSetDot1qFutureVlanPortSubnetMapVid ARG_LIST((INT4  , UINT4  ,INT4 ));
extern INT1 nmhSetDot1qFutureVlanPortSubnetMapARPOption ARG_LIST((INT4  , UINT4  ,INT4 ));
extern INT1 nmhSetDot1qFutureVlanPortSubnetMapRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2Dot1qFutureVlanPortSubnetMapVid ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2Dot1qFutureVlanPortSubnetMapARPOption ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2Dot1qFutureVlanPortSubnetMapRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* nmh routines used for scheduling */
extern INT4 nmhGetFirstIndexFsDiffServCoSqAlgorithmTable (INT4 *);
extern INT4 nmhGetNextIndexFsDiffServCoSqAlgorithmTable (INT4, INT4 *);
extern INT4 nmhTestv2FsDiffServCoSqAlgorithm (UINT4 *, INT4, INT4);
extern INT4 nmhSetFsDiffServCoSqAlgorithm (INT4, INT4);
extern INT4 nmhGetFsDiffServCoSqAlgorithm (INT4, INT4 *);
extern INT4 nmhGetFirstIndexFsDiffServCoSqWeightBwTable (INT4 *, INT4 *);
extern INT4 nmhGetFsDiffServCoSqWeight (INT4, INT4, UINT4 *);
extern INT4 nmhGetFsDiffServCoSqBwMin (INT4, INT4, UINT4 *); 
extern INT4 nmhGetFsDiffServCoSqBwMax (INT4, INT4, UINT4 *);
extern INT4 nmhGetFsDiffServCoSqBwFlags (INT4, INT4, INT4 *); 
extern INT4 nmhTestv2FsDiffServCoSqWeight (UINT4 *, INT4, INT4, UINT4);
extern INT4 nmhTestv2FsDiffServCoSqBwMin (UINT4 *, INT4, INT4, UINT4);
extern INT4 nmhTestv2FsDiffServCoSqBwMax (UINT4 *, INT4, INT4, UINT4);
extern INT4 nmhTestv2FsDiffServCoSqBwFlags (UINT4 *, INT4, INT4, INT4);
extern INT4 nmhSetFsDiffServCoSqWeight (INT4, INT4, UINT4);
extern INT4 nmhSetFsDiffServCoSqBwMin (INT4, INT4, UINT4);
extern INT4 nmhSetFsDiffServCoSqBwMax(INT4, INT4, UINT4);
extern INT4 nmhSetFsDiffServCoSqBwFlags(INT4, INT4, INT4);
extern INT1 nmhGetIssL3FilterInPortList ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetIssL3FilterOutPortList ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhTestv2DiffServMultiFieldClfrAddrType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1
nmhTestv2DiffServMultiFieldClfrDstPrefixLength ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1
nmhTestv2DiffServMultiFieldClfrSrcPrefixLength ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1
nmhTestv2DiffServMultiFieldClfrFlowId ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1
nmhGetDiffServMultiFieldClfrAddrType ARG_LIST((UINT4 ,INT4 *));

extern INT1
nmhGetDiffServMultiFieldClfrDstPrefixLength ARG_LIST((UINT4 ,UINT4 *));

extern INT1
nmhGetDiffServMultiFieldClfrSrcPrefixLength ARG_LIST((UINT4 ,UINT4 *));


extern INT1
nmhGetDiffServMultiFieldClfrFlowId ARG_LIST((UINT4 ,UINT4 *));

extern INT1
nmhSetDiffServMultiFieldClfrAddrType ARG_LIST((UINT4  ,INT4 ));


extern INT1
nmhSetDiffServMultiFieldClfrDstPrefixLength ARG_LIST((UINT4  ,UINT4 ));

/*Redirect */
extern INT1 nmhGetFirstIndexIssRedirectInterfaceGrpTable ARG_LIST((UINT4 *)); 
extern INT1 nmhGetNextIndexIssRedirectInterfaceGrpTable ARG_LIST((UINT4 , UINT4 *)); 
extern INT1 nmhGetIssRedirectInterfaceGrpFilterType ARG_LIST((UINT4 ,INT4 *)); 
extern INT1 nmhGetIssRedirectInterfaceGrpFilterId ARG_LIST((UINT4 ,UINT4 *)); 
extern INT1 nmhGetIssRedirectInterfaceGrpDistByte ARG_LIST((UINT4 ,INT4 *)); 
extern INT1 nmhGetIssRedirectInterfaceGrpPortList ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * )); 
extern INT1 nmhGetIssRedirectInterfaceGrpType ARG_LIST((UINT4 ,INT4 *)); 
extern INT1 nmhGetIssRedirectInterfaceGrpStatus ARG_LIST((UINT4 ,INT4 *)); 
extern INT1 nmhSetIssRedirectInterfaceGrpFilterType ARG_LIST((UINT4  ,INT4 )); 
extern INT1 nmhSetIssRedirectInterfaceGrpFilterId ARG_LIST((UINT4  ,UINT4 )); 
extern INT1 nmhSetIssRedirectInterfaceGrpDistByte ARG_LIST((UINT4  ,INT4 )); 
extern INT1 nmhSetIssRedirectInterfaceGrpPortList ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *)); 
extern INT1 nmhSetIssRedirectInterfaceGrpType ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhSetIssRedirectInterfaceGrpStatus ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhTestv2IssRedirectInterfaceGrpFilterType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhTestv2IssRedirectInterfaceGrpFilterId ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));
extern INT1 nmhTestv2IssRedirectInterfaceGrpDistByte ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhTestv2IssRedirectInterfaceGrpPortList ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2IssRedirectInterfaceGrpType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhTestv2IssRedirectInterfaceGrpStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhGetIssRedirectInterfaceGrpIdNextFree ARG_LIST((UINT4 *));
extern tIssL2FilterEntry *IssExtGetL2FilterEntry (INT4 i4IssL2FilterNo);
extern tIssL3FilterEntry *IssExtGetL3FilterEntry (INT4 i4IssL3FilterNo);

extern INT1 nmhGetIssAclL2FilterRedirectId ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetIssAclL3FilterRedirectId ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhTestv2IssAclL2FilterDirection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhSetIssAclL2FilterDirection ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetDiffServMultiFieldClfrSrcPrefixLength ARG_LIST((UINT4  ,UINT4 ));

extern INT1
nmhTestv2DiffServMultiFieldClfrStorage ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1
nmhSetDiffServMultiFieldClfrStorage ARG_LIST((UINT4  ,INT4 ));

extern INT1
nmhGetDiffServMultiFieldClfrStorage ARG_LIST((UINT4 ,INT4 *));

extern INT1
nmhSetDiffServMultiFieldClfrFlowId ARG_LIST((UINT4  ,UINT4 ));
VOID
IssRedirectVlanSubnetPerPortEnablePage (tHttp * );
    

extern INT4
DsWebnmGetPolicyMapEntry (INT4 i4PolicyMapId, tDiffServWebClfrData * pClfrData);
extern INT4
DsWebnmSetPolicyMapEntry (tDiffServWebSetClfrEntry * pClfrEntry,
                          UINT1 u1RowStatus, UINT1 *pu1ErrString);
extern UINT1 nmhTestv2IssAclL3FilteAddrType(UINT4 * , 
         INT4 , INT4 );

extern UINT1 nmhSetIssAclL3FilteAddrType(INT4 ,INT4); 
extern UINT1 nmhTestv2IssAclL3FilterSrcIpAddr (UINT4 * , INT4 ,tSNMP_OCTET_STRING_TYPE *);
extern INT1  nmhSetIssAclL3FilterInPortChannelList (INT4, tSNMP_OCTET_STRING_TYPE *);
extern UINT1 nmhTestv2IssAclL3FilterInPortChannelList (UINT4 * , INT4 ,tSNMP_OCTET_STRING_TYPE *);
extern INT1  nmhSetIssAclL3FilterOutPortChannelList (INT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1  nmhTestv2IssAclL3FilterOutPortChannelList (UINT4 *, INT4 ,tSNMP_OCTET_STRING_TYPE *);
extern UINT1 nmhSetIssAclL2FilterInPortChannelList (UINT4 * , INT4 ,tSNMP_OCTET_STRING_TYPE *);
extern UINT1 nmhTestv2IssAclL2FilterInPortChannelList (UINT4 * , INT4 ,tSNMP_OCTET_STRING_TYPE *);
extern INT1  nmhSetIssAclL2FilterOutPortChannelList (INT4, tSNMP_OCTET_STRING_TYPE *);
extern UINT1 nmhTestv2IssAclL2FilterOutPortChannelList (UINT4 * , INT4 ,tSNMP_OCTET_STRING_TYPE *);
extern UINT1 nmhSetIssAclL3FilterSrcIpAddr (INT4, tSNMP_OCTET_STRING_TYPE *);
extern UINT1 nmhTestv2IssAclL3FilterDstIpAddr (UINT4 *e, INT4, tSNMP_OCTET_STRING_TYPE *);
extern UINT1 nmhSetIssAclL3FilterDstIpAddr (INT4, tSNMP_OCTET_STRING_TYPE *);
extern UINT1 nmhTestv2IssAclL3FilterDstIpAddrPrefixLength  (UINT4*, INT4, UINT4);
extern UINT1 nmhSetIssAclL3FilterDstIpAddrPrefixLength (INT4, UINT4);
extern UINT1 nmhTestv2IssAclL3FilterSrcIpAddrPrefixLength (UINT4 *, INT4, UINT4);
extern UINT1 nmhSetIssAclL3FilterSrcIpAddrPrefixLength (INT4, UINT4);
extern UINT1 nmhTestv2IssAclL3FilterFlowId (UINT4 *, INT4, UINT4);
extern UINT1 nmhSetIssAclL3FilterFlowId (INT4 , UINT4);
extern UINT1 nmhGetIssAclL3FilteAddrType (INT4, INT4 *);
extern UINT1 nmhGetIssAclL3FilterSrcIpAddr (INT4, tSNMP_OCTET_STRING_TYPE *);
extern UINT1 nmhGetIssAclL3FilterDstIpAddr (INT4, tSNMP_OCTET_STRING_TYPE *);
extern UINT1 nmhGetIssAclL3FilterDstIpAddrPrefixLength (INT4, UINT4 *);
extern UINT1 nmhGetIssAclL3FilterSrcIpAddrPrefixLength (INT4, UINT4 *);
extern UINT1 nmhGetIssAclL3FilterFlowId (INT4, UINT4 *);
extern INT1  nmhGetIssL3FilterOutPortList (INT4, tSNMP_OCTET_STRING_TYPE*);
extern INT1  nmhGetIssL3FilterInPortList (INT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1  nmhGetIssAclL3FilterInPortChannelList (INT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1  nmhGetIssAclL3FilterOutPortChannelList (INT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1  nmhGetIssAclL2FilterInPortChannelList (INT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1  nmhGetIssAclL2FilterOutPortChannelList (INT4, tSNMP_OCTET_STRING_TYPE *);
extern tSNMP_OCTET_STRING_TYPE * allocmem_octetstring (INT4 i4Size);
extern VOID free_octetstring (tSNMP_OCTET_STRING_TYPE * pOctetStr);

/* L2 ACL Counters */
extern INT1 nmhGetIssAclL2FilterStatsEnabledStatus ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetIssAclClearL2FilterStats ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetIssAclL2FilterMatchCount ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhSetIssAclL2FilterStatsEnabledStatus ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetIssAclClearL2FilterStats ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhTestv2IssAclL2FilterStatsEnabledStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2IssAclClearL2FilterStats ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* L3 ACL Counters */
extern INT1 nmhGetIssAclL3FilterMatchCount ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetIssAclL3FilterStatsEnabledStatus ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetIssAclClearL3FilterStats ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhSetIssAclL3FilterStatsEnabledStatus ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetIssAclClearL3FilterStats ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhTestv2IssAclL3FilterStatsEnabledStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2IssAclClearL3FilterStats ARG_LIST((UINT4 *  ,INT4  ,INT4 ));


tSpecificPage       asIssTargetpages[] = {
   
    {"bcm_ip_stdqosfilter.html", IssBcmProcessIPFilterConfPage},
    
    {"bcm_ip_filterconf.html", IssBcmProcessIPFilterConfPage},

    {"bcm_ip_stdfilterconf.html", IssBcmProcessIPStdFilterConfPage},
    {"bcm_mac_filterconf.html", IssBcmProcessMACFilterConfPage},
    {"bcm_vlan_pvidsetting.html", IssBcmProcessVlanPvidSettingsPage},
    {"bcm_vlan_subnetipmap.html", IssBcmProcessVlanSubnetPerPortSettingsPage},
#ifdef DIFFSRV_WANTED
    {"bcm_dfs_policymapconf.html", IssBcmProcessDfsPolicyMapPage},
 {"bcm_dfs_cosqschdalgorithm.html", IssBcmProcessSchdAlgoPage},
 {"bcm_dfs_cosqweightbandwd.html", IssBcmProcessDfsCosqWeightBWConfPage},
#endif
    {"", NULL}
};

#endif /* _ISSWEB_H */
