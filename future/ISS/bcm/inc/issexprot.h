/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: issexprot.h,v 1.25 2015/08/06 05:36:35 siva Exp $
 *
 * Description: This file include all the prototypes required 
 *              for the operation of the ACL module
 ****************************************************************************/

#ifndef _ISSEXPROT_H
#define _ISSEXPROT_H

INT4 IssExInit (VOID);
VOID IssSetDefaultRateCtrlValues (UINT2 u2PortIndex,
                                  tIssRateCtrlEntry * pIssRateCtrlEntry);
INT4 IssExCreatePortRateCtrl (UINT2 u2PortIndex, tIssTableName IssTableFlag);
INT4 IssExDeletePortRateCtrl (UINT2 u2PortIndex);

tIssL2FilterEntry *IssExtGetL2FilterEntry (INT4 i4IssL2FilterNo);
tIssL3FilterEntry *IssExtGetL3FilterEntry (INT4 i4IssL3FilterNo);
tIssUserDefinedFilterTable * IssExtGetUdbFilterTableEntry (UINT4 u4UdbFilter);
INT4 IssExtQualifyL2FilterData (tIssL2FilterEntry ** ppIssL2FilterEntry);

INT4 IssExtQualifyL3FilterData (tIssL3FilterEntry ** ppIssL3FilterEntry);

/* Red */

VOID AclProcessQMsgEvent(VOID);

INT4 AclRedInitGlobalInfo (VOID);
INT4 AclRedDeInitGlobalInfo (VOID);
INT4 AclRmRegisterProtocols(tRmRegParams *pRmRegParams);
INT4 AclRmDeRegisterProtocols (VOID);
INT4 AclPortRmReleaseMemoryForMsg (UINT1 *pData);
INT4 AclPortRmApiHandleProtocolEvent (tRmProtoEvt * pEvt);
INT4 AclPortRmApiSendProtoAckToRM (tRmProtoAck * pProtoAck);
UINT4 AclPortRmEnqMsgToRm (tRmMsg * pRmMsg, UINT2 u2DataLen, UINT4 u4SrcEntId,
                      UINT4 u4DestEntId);
INT4 AclPortRmGetNodeState(VOID);
UINT1 AclPortRmGetStandbyNodeCount (VOID);
VOID AclPortRmSetBulkUpdatesStatus (VOID);
INT4 AclRedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen);
INT4 AclQueEnqMsg(tAclQMsg * pMsg);
VOID AclRedHandleRmEvents (tAclQMsg * pMsg);
VOID AclRedHandleGoActive (VOID);
VOID AclRedHandleGoStandby (VOID);
VOID AclRedHandleStandbyToActive (VOID);
VOID AclRedHandleActiveToStandby (VOID);
VOID AclRedHandleIdleToStandby (VOID);
VOID AclRedHandleIdleToActive (VOID);
VOID AclRedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen);
VOID AclRedHwAuditIncBlkCounter (VOID);
VOID AclRedHwAuditDecBlkCounter (VOID);
VOID AclRedInitNpSyncBufferTable (VOID);
VOID AclRedUpdateNpSyncBufferTable (unNpSync *pNpSync,UINT4 u4NpApiId,
                                        UINT4 u4EventId);
VOID AclUpdateHwFilterEntry(UINT4 u4NpApiId, unNpSync unNpData);
VOID AclProcessNpSyncMsg (tRmMsg  *pMsg, UINT2 *pu2OffSet);
VOID AclRedInitHardwareAudit (VOID);
VOID AclRedHandleL2FilterHwAudit(INT4 i4IssL2FilterNo, tIssFilterShadowTable IssFilterShadowTable,
                                                        INT4 i4Value);
VOID AclRedHandleL3FilterHwAudit(INT4 i4IssL3FilterNo, tIssFilterShadowTable IssFilterShadowTable,
                                                         INT4 i4Value);
VOID AclRedHandleRateLimitHwAudit(UINT4 u4IfIndex, UINT1 u1PacketType,INT4 i4RateLimitVal);
VOID AclRedHandlePortEgressRateHwAudit(UINT4 u4IfIndex, INT4 i4PktRate, INT4 i4BurstRate);

VOID AclRedSendBulkRequest (VOID);
VOID AclRedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen);
VOID AclRedHandleBulkRequest (VOID);
VOID AclRedSyncL2FilterHwHandle (VOID);
VOID AclRedSyncL3FilterHwHandle (VOID);
VOID AclRedSendBulkUpdTailMsg (VOID);
VOID AclRedProcessBulkTailMsg (VOID);
VOID AclProcessL2DynSyncMsg (tRmMsg  *pMsg, UINT2 *pu2OffSet);
VOID AclProcessL3DynSyncMsg (tRmMsg  *pMsg, UINT2 *pu2OffSet);
INT4 IssExPrgAclsToNpWithPriority (VOID);
/* End Red */

VOID RegisterFSISSM (VOID);
extern UINT1 * Ip6PrintNtop (tIp6Addr * pAddr);
extern VOID Ip6AddrCopy  PROTO ((tIp6Addr * pDst, tIp6Addr * pSrc));
INT4 AclCreateUserDefFilter PROTO ((tCliHandle CliHandle, INT4));
INT4 AclDestroyUserDefFilter PROTO ((tCliHandle CliHandle, INT4));
INT4 AclUserDefinedAccessGroup PROTO ((tCliHandle CliHandle, INT4, INT4, INT4));
INT4 AclNoUserDefinedAccessGroup PROTO ((tCliHandle CliHandle, INT4, INT4, INT4));
INT4 AclUserDefinedStatsSetEnabledStatus PROTO ((tCliHandle, INT4, INT4));
INT4 AclUserDefinedStatsClear PROTO ((tCliHandle, INT4));

INT4 AclReservFrameFilterConfig PROTO ((tCliHandle, UINT4, tMacAddr, UINT1, UINT1, UINT1));
INT4 AclReservFrameShow PROTO ((tCliHandle));
UINT4 AclCheckReservFrameEntry PROTO((UINT4, tMacAddr, UINT1));

INT4 IssReservFramCtrlIdNextFree PROTO((UINT4));
INT4 IssClearResrvFrameCtrlEntry PROTO((tIssReservFrmCtrlTable *, UINT4));
INT4 IssCheckResrvFrameCtrlEntry PROTO((UINT4, tMacAddr, UINT4, UINT1));
INT4 IssResrvFrmIsAllProtoActionDone PROTO((tIssReservFrmCtrlTable  *));
INT4 IssResrvFrmIsAnyEntryExist PROTO((tIssReservFrmCtrlTable  *));
INT4 IssResrvFrmIsOtherProtoMacMatched PROTO((tIssReservFrmCtrlTable  *));

INT4 AclUserDefinedOperations (tCliHandle CliHandle, INT4 , UINT4, UINT4 ,
                          UINT4 );
INT4 AclShowUDBFilter PROTO ((tCliHandle CliHandle, INT4 ));

INT4  AclCreateIPFilter PROTO ((tCliHandle, UINT4, INT4 ));

INT4  AclDestroyIPFilter PROTO ((tCliHandle,  INT4 ));

INT4 AclCreateMacFilter PROTO ((tCliHandle , INT4 ));

INT4 AclDestroyMacFilter PROTO ((tCliHandle , INT4 ));

INT4 AclMacAccessGroup PROTO ((tCliHandle CliHandle, INT4 i4FilterNo ));

INT4 AclNoMacAccessGroup PROTO ((tCliHandle CliHandle, INT4 i4FilterNo));

 INT4 AclMacExtAccessGroup PROTO ((tCliHandle, INT4, INT4, INT4));
 INT4 AclNoMacExtAccessGroup PROTO ((tCliHandle, INT4, INT4, INT4, UINT4));
 INT4 AclMacStatsSetEnabledStatus PROTO ((tCliHandle, INT4, INT4));
 INT4 AclMacStatsClear PROTO ((tCliHandle, INT4));

INT4 AclShowAccessLists PROTO ((tCliHandle CliHandle,INT4 i4FilterType,INT4 i4FilterNo));
INT4 AclShowL2Filter PROTO ((tCliHandle CliHandle,INT4 i4NextFilter));
INT4 AclShowL3Filter PROTO ((tCliHandle CliHandle,INT4 i4NextFilter));

INT4 AclShowRunningConfig(tCliHandle,UINT4);
VOID AclShowRunningConfigTables(tCliHandle);
VOID AclShowRunningConfigInterfaceDetails(tCliHandle,INT4);

INT4 AclTestIpParams PROTO ((UINT1 ,INT4 i4FilterNo,UINT4 u4SrcType,UINT4 u4SrcIpAddr, UINT4 u4SrcMask ));
                                           
INT4 AclSetIpParams PROTO ((UINT1 ,INT4 i4FilterNo,UINT4 u4SrcType,UINT4 u4SrcIpAddr, UINT4 u4SrcMask ));

VOID AclCliPrintPortList(tCliHandle CliHandle,
                         INT4 i4CommaCount, UINT1 * piIfName);
INT4 AclExtPbL3FilterConfig PROTO ((tCliHandle, INT4, INT4, INT4, INT4, INT4));
INT4 AclExtPbL2FilterConfig PROTO ((tCliHandle, INT4, INT4, INT4, INT4, INT4));

INT4 AclStdIpFilterConfig PROTO 
((tCliHandle, INT4,UINT4, UINT4 ,UINT4 ,UINT4, UINT4, UINT4, INT4));

INT4 AclExtIpFilterConfig PROTO 
((tCliHandle, INT4, INT4, UINT4, UINT4, UINT4, UINT4, UINT4, UINT4, INT4, INT4,
INT4));

INT4 AclValidateActionWithDirection PROTO ((INT4 i4Direction, INT4 i4Action));

INT4
AclExtIp6FilterConfig PROTO ((tCliHandle CliHandle, UINT4 u4SrcIpAddr,
                       tIp6Addr SrcIp6Addr, UINT4 u4SrcIpMask,
                       UINT4 u4DestIpAddr, tIp6Addr DstIp6Addr,
         UINT4 u4DestIpMask, UINT4 u4FlowId, INT4 i4Action, INT4 i4Priority));

INT4 AclExtIpFilterTcpUdpConfig PROTO ((tCliHandle, INT4 i4Action, INT4 i4Protocol,
  UINT4 u4SrcType , UINT4 u4SrcIpAddr,  UINT4 u4SrcIpMask, 
  UINT4 u4SrcFlag, UINT4 u4SrcMinPort,  UINT4 u4SrcMaxRangePort,
  UINT4 u4DestType, UINT4  u4DestIpAddr,UINT4 u4DestIpMask,
  UINT4 u4DestFlag, UINT4 u4DestMinPort ,UINT4  u4DestMaxRangePort, 
  UINT4 u4BitType , INT4 i4Tos, INT4 i4Dscp, INT4 i4Priority ));

 INT4 AclExtIpFilterIcmpConfig PROTO
 ((tCliHandle, INT4 i4Action, UINT4,UINT4 , UINT4, UINT4, UINT4, UINT4 , INT4, INT4, INT4));

 INT4 AclIpAccessGroup PROTO ((tCliHandle, INT4, INT4, INT4));
 INT4 AclNoIpAccessGroup PROTO ((tCliHandle, INT4, INT4, INT4, UINT4));
 INT4 AclIpStatsSetEnabledStatus PROTO ((tCliHandle, INT4, INT4));
 INT4 AclIpStatsClear PROTO ((tCliHandle, INT4));
 
 INT4 AclExtMacFilterConfig PROTO ((tCliHandle CliHandle, INT4 i4Action,
                       UINT4 u4SrcType, tMacAddr SrcMacAddr,
                       UINT4 u4DestType ,tMacAddr DestMacAddr,
                       INT4 i4Protocol, INT4 i4Encap,
                       UINT4 u4VlanId, INT4 u4Priority));

INT4 IssExtSnmpLowValidateIndexPortRateTable (INT4 i4PortIndex);

INT4 IssExtSnmpLowGetNextValidL3FilterTableIndex (INT4 i4IssL3FilterNo,
                                                  INT4 *pi4NextL3FilterNo);


INT4 IssExtSnmpLowGetFirstValidL2FilterTableIndex (INT4 *pi4FirstL2FilterIndex);

INT4 IssExtSnmpLowGetNextValidL2FilterTableIndex (INT4 i4IssL2FilterNo,
                                                  INT4 *pi4NextL2FilterNo);

INT4 IssExtSnmpLowGetFirstValidL3FilterTableIndex (INT4 *pi4FirstL3FilterIndex);

INT4 IssExtValidateRateCtrlEntry (INT4 i4RateCtrlIndex);
/******** Function Prototypes *************/

INT4 IssAclInstallL2Filter (tIssL2FilterEntry *pIssL2Filter);
INT4 IssAclInstallL3Filter  (tIssL3FilterEntry *pIssL3Filter);
INT4 IssACLInstallFilter (UINT4 u4FilterType,
                          tIssL2FilterEntry *pIssL2Filter,
                          tIssL3FilterEntry *pIssL3Filter);
INT4 IssAclQosProcessL2Proto (VOID);
INT4 IssAclQosProcessL3Proto (VOID);
INT4 IssAclQosProcessL2L3Proto (VOID);
INT4 IssAclQosDeleteL2L3Filter (VOID);
INT4 IssAclUninstallFilter (UINT4  u4FilterType,
                            tIssL2FilterEntry *pIssL2Filter,
                            tIssL3FilterEntry *pIssL3Filter);
INT4 IssAclInstallUserDefFilter (tIssUserDefinedFilterTable *);
VOID IssExDeleteAclPriorityFilterTable PROTO ((INT4 i4Priority,tTMO_SLL_NODE *pFilterNode,UINT1 u1FilterType));
INT4 IssExGetIssCommitSupportImmediate PROTO ((VOID));
INT4 IssGetTriggerCommit PROTO ((VOID));
INT4 IssAclUnInstallUserDefFilter (tIssUserDefinedFilterTable *);
INT4 IssAclUnInstallL2Filter (tIssL2FilterEntry *pIssL2Filter);
INT4 IssAclUnInstallL3Filter (tIssL3FilterEntry *pIssL3Filter);
INT4 IssAclUpdateL2FilterEntry (tAclFilterInfo *, UINT4);
INT4 IssAclUpdateL3FilterEntry (tAclFilterInfo *, UINT4);
VOID IssAclGetValidL2FilterId (UINT4 *);
VOID IssAclGetValidL3FilterId (UINT4 *);
INT4 IssAclUpdateUDBFilterEntry (tAclFilterInfo *, UINT4, UINT4, UINT4);
VOID IssAclGetValidUDBFilterId (UINT4 *);
INT4 IssExtSnmpLowGetFirstValidUDBFilterTableIndex (INT4 *pu4FirstUdbFilterIndex);
INT4 IssSnmpLowGetNextValidUdbFilterTableIndex (UINT4 u4IssUdbFilterNo,
                            UINT4 *pu4NextUdbFilterNo);

VOID IssAclUpdateHwByL3Priority(tIssL3FilterEntry * pIssL3FilterEntry);
VOID IssAclAddTosortL3FilterLst (tIssL3FilterEntry * pIssL3FilterEntry);
VOID IssAclUpdateHwByL2Priority(tIssL2FilterEntry * pIssL2FilterEntry);
VOID IssAclAddTosortL2FilterLst (tIssL2FilterEntry * pIssL2FilterEntry);

INT4 AclExtIp6FilterIcmpConfig (tCliHandle CliHandle, UINT4 u4SrcType,
                       tIp6Addr SrcIp6Addr, UINT4 u4SrcPrefixLen,
                       UINT4 u4DstType, tIp6Addr DstIp6Addr,
                       UINT4 u4DstPrefixLen, INT4 i4MsgType, 
         INT4 i4MsgCode, INT4 i4Dscp, UINT4 u4FlowId, 
                       INT4 i4Priority, INT4 i4Action);

INT4 AclRedirectFilterConfig PROTO ((tCliHandle CliHandle, INT4 i4Action,
                          INT4 i4IsPortList,
                          INT1 *pi1IfName0, INT1 *pi1IfListStr0));

INT4  AclResetL3Filter PROTO ((INT4 i4FilterNo));
INT4  AclResetL3FilterQualifiers PROTO ((INT4 i4FilterNo));
INT4  AclResetL2Filter PROTO ((INT4 i4FilterNo));

VOID IssInterfaceGrpRedirectIdNextFree PROTO ((UINT4 *pu4RetValIssRedirectInterfaceGrpIdNextFree));

INT4 AclUpdateOverPortChannel PROTO ((UINT4 u4IfIndex));
INT4 AclEnabledForPort (UINT4 u4IfIndex);
INT4 AclIpFilterEnabledForPort (UINT4 u4IfIndex);
INT4 AclMacFilterEnabledForPort (UINT4 u4IfIndex);
INT4 AclUserDefinedFilterEnabledForPort  (UINT4 u4IfIndex);

#endif /* _ISSEXPROT_H */

