/********************************************************************
 *      Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *   
 *      $Id: isssys.c,v 1.13 2014/06/18 11:05:04 siva Exp $
 *     
 *      Description: Routines for snmp agent main module
 ********************************************************************/

#include "issinc.h"
#include "vcm.h"
#include "diffsrv.h"
#include "snp.h"

#ifndef NPAPI_WANTED
#define FNP_SUCCESS      1
#define FNP_FAILURE      0
#endif
extern tNVRAM_DATA  sNvRamData;
extern tNVRAM_AGG_MAC sNvRamAggMac[LA_DEV_MAX_TRUNK_GROUP];
extern UINT1        gau1SystemDefaultDevName[CFA_MAX_PORT_NAME_LENGTH];
extern UINT4        gu4SystemDefaultSubnetMask;
extern tIssSysGroupInfo gIssSysGroupInfo;
UINT1               gu1IssLoginAuthMode;
INT1                gi1SnmpEngineID[SNMP_MAX_STR_ENGINEID_LEN + 1] =
    "80.00.08.1c.04.46.53";
/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssReadSystemInfoFromNvRam                       */
/*                                                                          */
/*    Description        : This function is invoked to read the System info */
/*                         from the NVRAM.                                  */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
INT4
IssReadSystemInfoFromNvRam ()
{
    if (NvRamRead (&sNvRamData) != FNP_SUCCESS)
    {
        return ISS_FAILURE;
    }
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetPimModeFromNvRam                           */
/*                                                                          */
/*    Description        : This function is used to get the Pim Mode        */
/*                         from the NVRAM.                                  */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PIM Mode i.e. ISS_SM/ISS_DM                      */
/****************************************************************************/
UINT4
IssGetPimModeFromNvRam (VOID)
{
    return (sNvRamData.u4PimMode);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetTimeStampMethodFromNvRam                   */
/*                                                                          */
/*    Description        : This function is used to get the configured      */
/*                         Time Stamp Method from NVRAM.                    */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Time Stamping Method.                            */
/****************************************************************************/
UINT2
IssGetTimeStampMethodFromNvRam (VOID)
{
    return (sNvRamData.u2TimeStampMethod);
}

/* SNOOP_WR */
/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetSnoopFwdModeFromNvRam                      */
/*                                                                          */
/*    Description        : This function is used to get the SNOOP Forward Mode*/
/*                         from the NVRAM.                                  */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : IGS Forward Mode i.e. ISS_IGS_IP/ISS_IGS_MAC     */
/****************************************************************************/
UINT4
IssGetSnoopFwdModeFromNvRam (VOID)
{
    return (sNvRamData.u4SnoopFwdMode);
}

/* SNOOP_WR */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetIpAddrFromNvRam                            */
/*                                                                          */
/*    Description        : This function is invoked to get the IP address   */
/*                         of the default interface from the NVRAM.         */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : pIpAddr - IP Address of the default interface    */
/****************************************************************************/
UINT4
IssGetIpAddrFromNvRam ()
{
    return sNvRamData.u4LocalIpAddr;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetInterfaceFromNvRam                         */
/*                                                                          */
/*    Description        : This function is invoked to get the default      */
/*                         interface name from the NVRAM.                   */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Interface Name                                   */
/****************************************************************************/
INT1               *
IssGetInterfaceFromNvRam ()
{
    return ((INT1 *) sNvRamData.ai1InterfaceName);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetRmIfNameFromNvRam                          */
/*                                                                          */
/*    Description        : This function is invoked to get the backplane    */
/*                         interface name from the NVRAM.                   */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Interface Name                                   */
/****************************************************************************/
INT1               *
IssGetRmIfNameFromNvRam ()
{
    return ((INT1 *) sNvRamData.ai1RmIfName);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetSubnetMaskFromNvRam                        */
/*                                                                          */
/*    Description        : This function is invoked to get the Subnet Mask  */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Local Subnet Mask                                */
/****************************************************************************/
UINT4
IssGetSubnetMaskFromNvRam ()
{
    return sNvRamData.u4LocalSubnetMask;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetMgmtPortFromNvRam                          */
/*                                                                          */
/*    Description        : This function is invoked to get MgmtPort flag    */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : TRUE/FALSE                                       */
/****************************************************************************/
UINT4
IssGetMgmtPortFromNvRam ()
{
    return sNvRamData.u4MgmtPort;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetDefaultVlanIdFromNvRam                     */
/*                                                                          */
/*    Description        : This function is used to get the Default Vlan Id */
/*                         from the NVRAM.                                  */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Default Vlan Id                                  */
/****************************************************************************/
UINT2
IssGetDefaultVlanIdFromNvRam ()
{
    return (sNvRamData.u2DefaultVlan);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetNpapiModeFromNvRam                          */
/*                                                                          */
/*    Description        : This function is used to get the NPAPI Mode of   */
/*                         Processing from the NVRAM.                       */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Default Vlan Id                                  */
/****************************************************************************/
UINT1              *
IssGetNpapiModeFromNvRam ()
{
    return ((UINT1 *) sNvRamData.au1NpapiMode);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetDefaultVlanIdToNvRam                       */
/*                                                                          */
/*    Description        : This function is used to set the default VLAN    */
/*                         Identifier  to  NVRAM.                           */
/*                                                                          */
/*    Input(s)           : u4DefaultVlanId - VLAN Identifier                */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/

VOID
IssSetDefaultVlanIdToNvRam (UINT2 u2DefaultVland)
{
    sNvRamData.u2DefaultVlan = u2DefaultVland;
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        printf ("FAILED to write default vlan id to NvRam\n");
        return;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetNpapiModeToNvRam                           */
/*                                                                          */
/*    Description        : This function is used to set the default VLAN    */
/*                         Identifier  to  NVRAM.                           */
/*                                                                          */
/*    Input(s)           : pu1NpapiMode - Pointer to NPAPI Mode             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetNpapiModeToNvRam (UINT1 *pu1NpapiMode)
{
    ISS_STRCPY (sNvRamData.au1NpapiMode, pu1NpapiMode);
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssInitialiseNvRamDefVal                         */
/*                                                                          */
/*    Description        : This function initialises NvRam with the default */
/*                         values.                                          */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : VOID                                             */
/****************************************************************************/
VOID
IssInitialiseNvRamDefVal ()
{
    tMacAddr            DefaultSwitchMac =
        { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05 };

    sNvRamData.u4LocalIpAddr = ISS_DEFAULT_IP_ADDRESS;
    sNvRamData.u4IpAddrAllocProto = CFA_PROTO_DHCP;
    sNvRamData.u4LocalSubnetMask = ISS_DEFAULT_IP_MASK;
    sNvRamData.u4CfgMode = ISS_CFG_MANUAL;
    sNvRamData.u4RestoreOption = ISS_CONFIG_NO_RESTORE;
    sNvRamData.i1ResFlag = ISS_DEFAULT_RESTORE_FLAG;
    sNvRamData.i1SaveFlag = ISS_DEFAULT_SAVE_FLAG;
    sNvRamData.u4RestoreIpAddr = ISS_DEFAULT_RES_IP_ADDRESS;

    STRCPY (sNvRamData.ai1InterfaceName, ISS_DEFAULT_INTERFACE_NAME);
    STRCPY (sNvRamData.ai1RmIfName, ISS_DEFAULT_RM_INTERFACE_NAME);
    sNvRamData.u4PimMode = ISS_DEFAULT_PIM_MODE;
    sNvRamData.u4BridgeMode = ISS_DEFAULT_BRIDGE_MODE;
    sNvRamData.u4SnoopFwdMode = ISS_DEFAULT_SNOOP_FWD_MODE;
    MEMCPY (sNvRamData.au1SwitchMac, DefaultSwitchMac, 6);
    MEMCPY (sNvRamData.ai1SnmpEngineID, gi1SnmpEngineID,
            STRLEN (gi1SnmpEngineID));

#ifdef MBSM_WANTED
    sNvRamData.u4MgmtPort = TRUE;
#else
    sNvRamData.u4MgmtPort = FALSE;
#endif
    sNvRamData.u4CliSerialConsole = ISS_DEFAULT_CLI_SERIAL_CONSOLE;

    /* Write to NvRam here */
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        printf ("FAILED to write to NvRam\n");
        return;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSysGetSwitchName                              */
/*                                                                          */
/*    Description        : This function is invoked to get the Switch name  */
/*                         from the ISS Group Information                   */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : Pointer to the switch name string                */
/****************************************************************************/
UINT1              *
IssSysGetSwitchName (VOID)
{
    return (gIssSysGroupInfo.au1IssSwitchName);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetAggregatorMac                              */
/*                                                                          */
/*    Description        : This function is invoked to get the Aggregator   */
/*                         MAC address from the ISS Group Information.      */
/*                                                                          */
/*    Input(s)           : u2AggIndex - Aggregator Index.                   */
/*                                                                          */
/*    Output(s)          : AggrMac.                                         */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
INT4
IssGetAggregatorMac (UINT2 u2AggIndex, tMacAddr AggrMac)
{
    UINT4               u4MacIndex;

    u4MacIndex = u2AggIndex - SYS_DEF_MAX_PHYSICAL_INTERFACES - 1;
    MEMCPY (AggrMac, sNvRamAggMac[u4MacIndex].au1AggregatorMac,
            CFA_ENET_ADDR_LEN);
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetConfigModeFromNvRam                        */
/*                                                                          */
/*    Description        : This function is used to get the Configuration   */
/*                         mode from the NVRAM.                             */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Config Mode  i.e. ISS_CFG_MANUAL/DYNAMIC         */
/****************************************************************************/
UINT4
IssGetConfigModeFromNvRam ()
{
    return (sNvRamData.u4CfgMode);
}

VOID
CustomStartup (int argc, char *argv[])
{
    UNUSED_PARAM (argc);
    UNUSED_PARAM (argv);
    StackCustomStartup ();
}

INT4
IssCreatePort (UINT2 u2PortIndex, tIssTableName IssTableFlag)
{
    UNUSED_PARAM (u2PortIndex);
    UNUSED_PARAM (IssTableFlag);
    return ISS_SUCCESS;
}

INT4
IssDeletePort (UINT2 u2PortIndex, tIssTableName IssTableFlag)
{
    UNUSED_PARAM (u2PortIndex);
    UNUSED_PARAM (IssTableFlag);
    return ISS_SUCCESS;
}

INT4
IssLock (VOID)
{
    return ISS_SUCCESS;
}

INT4
IssUnLock (VOID)
{
    return ISS_SUCCESS;
}

/* SNOOP_WR */
/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetSnoopFwdModeToNvRam                        */
/*                                                                          */
/*    Description        : This function is used to set the Snoop Forward   */
/*                         Mode to the NVRAM.                               */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetSnoopFwdModeToNvRam (UINT4 u4SnoopFwdMode)
{
    sNvRamData.u4SnoopFwdMode = u4SnoopFwdMode;
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        printf ("FAILED to write Snoop Fwd mode" "to NvRam\n");
        return;
    }
}

/* SNOOP_WR */

/*****************************************************************************/
/* Function Name      : EoidGetEnterpriseOid                                 */
/*                                                                           */
/* Description        : This function retrieves the Enterprise OID_1.        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : gu4Eoid                                              */
/*****************************************************************************/
unsigned char      *
EoidGetEnterpriseOid ()
{
    return ("2076");
}

/*****************************************************************************/
/* Function Name      : EoidGetSecondEnterpriseOid                           */
/*                                                                           */
/* Description        : This function retrieves the Enterprise OID_2.        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : gu4Eoid                                              */
/*****************************************************************************/
unsigned char      *
EoidGetSecondEnterpriseOid ()
{
    return ("29601");
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetRestoreOptionFromNvRam                     */
/*                                                                          */
/*    Description        : This function is used to get the Restore Option  */
/*                         from the NVRAM.                                  */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : RestoreOpion i.e. LOCAL/REMOTE                   */
/****************************************************************************/
UINT4
IssGetRestoreOptionFromNvRam ()
{
    return (sNvRamData.u4RestoreOption);
}

INT4
IssInit ()
{
    lrInitComplete (OSIX_SUCCESS);
    return OSIX_SUCCESS;
}

INT4
IssIntfShowRunningConfig (INT4 i4Handle, UINT4 u4Port, UINT1 u1Flag)
{
    UNUSED_PARAM (i4Handle);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u1Flag);
    return OSIX_SUCCESS;
}

INT4
AclShowRunningConfigInterfaceDetails (INT4 i4Handle, UINT4 u4Port)
{
    UNUSED_PARAM (i4Handle);
    UNUSED_PARAM (u4Port);
    return OSIX_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetSnmpEngineIDToNvRam                        */
/*                                                                          */
/*    Description        : This function is invoked to set the SNMP         */
/*                         EngineID to the NVRAM.                           */
/*                                                                          */
/*    Input(s)           : pi1SnmpEngineID - pointer to Snmp EngineID       */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetSnmpEngineIDToNvRam (INT1 *pi1SnmpEngineID)
{

    UNUSED_PARAM (pi1SnmpEngineID);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetSnmpEngineID                               */
/*                                                                          */
/*    Description        : This function is invoked to get Snmp EngineID.  */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : pointer to Snmp EngineID                         */
/****************************************************************************/
INT1               *
IssGetSnmpEngineID ()
{
    return ((INT1 *) gi1SnmpEngineID);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetBridgeModeFromNvRam                        */
/*                                                                          */
/*    Description        : This function is used to get the Bridge Mode     */
/*                         from the NVRAM.                                  */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Bridge Mode i.e. ISS_CUSTOMER_BRIDGE_MODE /      */
/*                         ISS_PROVIDER_BRIDGE_MODE /                       */
/*                         ISS_PROVIDER_EDGE_BRIDGE_MODE /                  */
/*                         ISS_PROVIDER_CORE_BRIDGE_MODE                    */
/****************************************************************************/
UINT4
IssGetBridgeModeFromNvRam (VOID)
{
    return (sNvRamData.u4BridgeMode);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetBridgeModeToNvRam                          */
/*                                                                          */
/*    Description        : This function is used to set the Bridge Mode     */
/*                         to the NVRAM.                                    */
/*                                                                          */
/*    Input(s)           : u4BridgeMode - CB/PB/PEB/PCB.                    */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetBridgeModeToNvRam (UINT4 u4BridgeMode)
{
    UNUSED_PARAM (u4BridgeMode);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetRestoreOptionToNvRam                       */
/*                                                                          */
/*    Description        : This function is used to get the Restore Option  */
/*                         from the NVRAM.                                  */
/*                                                                          */
/*    Input(s)           : RestoreOpion i.e. LOCAL/REMOTE                   */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetRestoreOptionToNvRam (UINT4 u4RestoreOption)
{
    UNUSED_PARAM (u4RestoreOption);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetRestoreFlagFromNvRam                       */
/*                                                                          */
/*    Description        : This function is invoked to get Restore Flag.    */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Restore Flag                                     */
/****************************************************************************/
INT1
IssGetRestoreFlagFromNvRam ()
{
    return 0;
}

VOID
cli_process_iss_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4Command);
    return;
}

INT1
nmhGetIssPortCtrlMode (INT4 i4IssPortCtrlIndex, INT4 *pi4RetValIssPortCtrlMode)
{
    UNUSED_PARAM (i4IssPortCtrlIndex);
    UNUSED_PARAM (pi4RetValIssPortCtrlMode);
    return 0;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetStackPortCountFromNvRam                    */
/*                                                                          */
/*    Description        : This function is used to get the configured      */
/*                         port count from the NVRAM.                       */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Number of valid Front Pannel Ports               */
/****************************************************************************/
UINT2
IssGetStackPortCountFromNvRam ()
{
    return (sNvRamData.u2StackPortCount);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetColdStandbyFromNvRam                       */
/*                                                                          */
/*    Description        : This function is invoked to get Switch Id        */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : ColdStandby                                      */
/****************************************************************************/
UINT4
IssGetColdStandbyFromNvRam ()
{
    return (sNvRamData.u4ColdStandby);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetVRMac                                      */
/*                                                                          */
/*    Description        : This function is invoked to get the Virtual      */
/*                         Router MAC address.                              */
/*                                                                          */
/*    Input(s)           : u4ContextId - Virtual Router Id.                 */
/*                                                                          */
/*    Output(s)          : VRMac        - Virtual Router Mac address.       */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
INT4
IssGetVRMac (UINT4 u4ContextId, tMacAddr VRMac)
{
    UNUSED_PARAM (u4ContextId);
    ISS_MEMCPY (VRMac, gIssSysGroupInfo.BaseMacAddr, MAC_ADDR_LEN);
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssMirrorStatusCheck                             */
/*                                                                          */
/*    Description        : This function checks if mirroring can be enabled */
/*                         on a given port.                                 */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS if mirroring can be enabled         */
/*                         ISS_FAILURE if mirroring cannot be enabled      */
/****************************************************************************/
INT4
IssMirrorStatusCheck (UINT2 u2Port)
{
    UNUSED_PARAM (u2Port);
    return ISS_SUCCESS;
}
