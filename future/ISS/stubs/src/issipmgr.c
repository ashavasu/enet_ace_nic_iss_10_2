#include "issinc.h"

INT4
IssApplyFilter (UINT2 u2InPort, UINT2 InVlan, UINT4 u4SrcIp,
                UINT1 u1L3Proto, UINT2 u2L4DstPort, BOOL1 bOOBFlag)
{
    UNUSED_PARAM (u2InPort);
    UNUSED_PARAM (InVlan);
    UNUSED_PARAM (u4SrcIp);
    UNUSED_PARAM (u1L3Proto);
    UNUSED_PARAM (u2L4DstPort);
    UNUSED_PARAM (bOOBFlag);
    return ISS_SUCCESS;
}

INT4
IssIpAuthMgrInit ()
{
    return ISS_SUCCESS;
}

int
NmhIoctl (unsigned long p)
{
    UNUSED_PARAM (p);
    return ISS_SUCCESS;
}
