#include "issinc.h"

/* Global Variables */

#define ISS_NVRAM_FILE        "issnvram.txt"
extern tIssSysGroupInfo gIssSysGroupInfo;
extern UINT4        gu4IssCidrSubnetMask[ISS_MAX_CIDR + 1];
extern tNVRAM_DATA  sNvRamData;
extern UINT4        gu4MgmtPort;
extern UINT1        gau1SystemDefaultDevName[CFA_MAX_PORT_NAME_LENGTH];
extern UINT4        gu4SystemDefaultSubnetMask;
extern INT1         gi1SnmpEngineID[SNMP_MAX_STR_ENGINEID_LEN + 1];

/* Stubs defined for compilation of CFA without IP */
VOID
StackCustomStartup ()
{
    /* Initialising the various data-structures */
    MEMSET (&gIssSysGroupInfo, 0, sizeof (tIssSysGroupInfo));

    /* Read nvram file */
    if (IssReadSystemInfoFromNvRam () != ISS_SUCCESS)
    {
        /* Condition - No NvRam configuration is present */
        /* or part of/all information present is wrong */
        /* so write with existing (and correct) fields */
        /* and default values with non-existant fields */
        if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
        {
            printf ("FAILED to write to NvRam\n");
        }
    }

    /* Set the IssSysGroupInfo and gSystemSize Data-Structures. */
    IssInitSystemParams ();
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssInitSystemParams                              */
/*                                                                          */
/*    Description        : This function initialize the ip address and      */
/*                         device name for the default interface            */
/*                         from the nvram.                                  */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssInitSystemParams ()
{
    gIssSysGroupInfo.u4IssDefaultIpSubnetMask = IssGetSubnetMaskFromNvRam ();

    /* Read default device name from the nvram */
    STRCPY ((INT1 *) &(gIssSysGroupInfo.au1IssDefaultInterface),
            IssGetInterfaceFromNvRam ());

    STRCPY ((INT1 *) &(gIssSysGroupInfo.au1IssDefaultRmIfName),
            IssGetRmIfNameFromNvRam ());

    gIssSysGroupInfo.u4IssDefaultIpAddress = IssGetIpAddrFromNvRam ();

    gu4MgmtPort = IssGetMgmtPortFromNvRam ();

    /* Set the Hardware version name */
    if (STRLEN (IssGetHardwareVersion ()) > ISS_STR_LEN)
    {
        MEMCPY (gIssSysGroupInfo.au1IssHardwareVersion,
                IssGetHardwareVersion (), ISS_STR_LEN);
    }
    else
    {
        MEMCPY (gIssSysGroupInfo.au1IssHardwareVersion,
                IssGetHardwareVersion (), STRLEN (IssGetHardwareVersion ()));
    }

    /* Set the Firmware version name */
    if (STRLEN (IssGetFirmwareVersion ()) > ISS_STR_LEN)
    {
        MEMCPY (gIssSysGroupInfo.au1IssFirmwareVersion,
                IssGetFirmwareVersion (), ISS_STR_LEN);
    }
    else
    {
        MEMCPY (gIssSysGroupInfo.au1IssFirmwareVersion,
                IssGetFirmwareVersion (), STRLEN (IssGetFirmwareVersion ()));
    }

    /* Set the Configuration Mode From NvRam */
    gIssSysGroupInfo.IssCfgMode = IssGetConfigModeFromNvRam ();

    gIssSysGroupInfo.u1IssDefaultIpAddrAllocProto = (UINT1)
        IssGetIpAddrAllocProtoFromNvRam ();

    STRCPY (gIssSysGroupInfo.au1IssRestoreFileVersion,
            ISS_RESTORE_FILE_VERSION);

    /* Set the PIM config Mode */
    gIssSysGroupInfo.IssPimMode = IssGetPimModeFromNvRam ();

    /* Set the Bridge Mode */
    gIssSysGroupInfo.IssBridgeMode = IssGetBridgeModeFromNvRam ();
    gIssSysGroupInfo.IssSnoopFwdMode = IssGetSnoopFwdModeFromNvRam ();

    MEMCPY (gIssSysGroupInfo.BaseMacAddr, sNvRamData.au1SwitchMac, 6);

    STRCPY (gIssSysGroupInfo.au1IssSwitchName, IssGetSwitchName ());

    STRCPY (gIssSysGroupInfo.au1IssLocation, IssGetSysLocation ());
    STRCPY (gIssSysGroupInfo.au1IssContact, IssGetSysContact ());

    /*Read NVRAM and get the MAC Addresses for aggregators */
    NvRamReadAggregatorMac ();

}

/************************************************************************/
/*  Function Name   : IssNotifyConfiguration                            */
/*  Description     : This function is called from the low level        */
/*                     set routines, to send configuration update event */
/*                     triggers to MSR.                                 */
/*                                                                      */
/*  Input           : SnmpNotifyInfo - Snmp Notification Strucutre      */
/*                    pu1Fmt      - The data type of indices and data.  */
/*                    variable set of inputs depending the on the       */
/*                    indices and the data.                             */
/*  Output          : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

VOID
IssNotifyConfiguration (tSnmpNotifyInfo SnmpNotifyInfo, CHR1 * pu1Fmt, ...)
{

    UNUSED_PARAM (SnmpNotifyInfo);
    UNUSED_PARAM (pu1Fmt);
    return;
}

/************************************************************************/
/*  Function Name   : IssMsrNotifyConfiguration                         */
/*  Description     : This function is called from the low level        */
/*                     set routines, to send configuration update event */
/*                     triggers to MSR.                                 */
/*                                                                      */
/*  Input           : SnmpNotifyInfo - Snmp Notification Strucutre      */
/*                    pu1Fmt      - The data type of indices and data.  */
/*                    variable set of inputs depending the on the       */
/*                    indices and the data.                             */
/*  Output          : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

VOID
IssMsrNotifyConfiguration (tSnmpNotifyInfo SnmpNotifyInfo, CHR1 * pu1Fmt, ...)
{

    UNUSED_PARAM (SnmpNotifyInfo);
    UNUSED_PARAM (pu1Fmt);
    return;
}

/****************************************************************************
 *  Function Name   : MsrNotifyPortDeletion                                 *
 *  Description     : In the incremental save on mode, this function        *
 *                notifies the MSR process that a Port has been deleted.    *
 *  Input           : u4InterfaceId  - The Port identifier that has been    *
                                deleted                                     *
 *  Output          : None                                                  *
 *  Returns         : MSR_SUCCESS / MSR_FAILURE                             *
****************************************************************************/
INT1
MsrNotifyPortDeletion (UINT4 u4InterfaceId)
{
    UNUSED_PARAM (u4InterfaceId);
    return MSR_SUCCESS;
}
