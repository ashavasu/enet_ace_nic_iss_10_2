
/*****************************************************************************/
/* Copyright (C) 2013 Aricent Inc . All Rights Reserved                                  */
/* $Id: isscfa.c,v 1.9 2013/09/14 10:04:13 siva Exp $      */
/*****************************************************************************/
/*    FILE  NAME            : isscfa.c                                          */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : ISS System                                     */
/*    MODULE NAME           :                                                */
/*    ISSNGUAGE             : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*                            saved.                                         */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
#include "issinc.h"
#include "isscfa.h"
tOsixTaskId         gCfaTaskId;
tOsixQId            gCfaPktMuxQId;

INT1
CfaGetIfConfigPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UNUSED_PARAM (pi1ModeName);
    UNUSED_PARAM (pi1DispStr);
    return TRUE;
}

INT1
CfaGetIfRouterPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UNUSED_PARAM (pi1ModeName);
    UNUSED_PARAM (pi1DispStr);
    return TRUE;
}

INT1
CfaGetIfOpenflowPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UNUSED_PARAM (pi1ModeName);
    UNUSED_PARAM (pi1DispStr);
    return TRUE;
}

INT1
CfaGetIvrPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UNUSED_PARAM (pi1ModeName);
    UNUSED_PARAM (pi1DispStr);
    return TRUE;
}

INT1
CfaGetLinuxIvrPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UNUSED_PARAM (pi1ModeName);
    UNUSED_PARAM (pi1DispStr);
    return TRUE;
}

INT1
CfaGetTnlPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UNUSED_PARAM (pi1ModeName);
    UNUSED_PARAM (pi1DispStr);
    return TRUE;
}

INT1
CfaGetPortChannelPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UNUSED_PARAM (pi1ModeName);
    UNUSED_PARAM (pi1DispStr);
    return TRUE;
}

INT4
CfaCliGetIfIndex (INT1 *pi1IfName, INT1 *pi1IfNum, UINT4 *pu4IfIndex)
{
    UNUSED_PARAM (pi1IfName);
    UNUSED_PARAM (pi1IfNum);
    UNUSED_PARAM (pu4IfIndex);
    return ISS_SUCCESS;
}

INT4
CfaCliGetOobIfIndex (UINT4 *pu4IfIndex)
{
    UNUSED_PARAM (pu4IfIndex);
    return ISS_SUCCESS;
}

UINT4
CfaIsMgmtPort (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return TRUE;
}

INT4
CfaHandlePktFromIp (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                    UINT4 u4NextHopIpAddr, UINT1 *au1MediaAddr,
                    UINT2 u2Protocol, UINT1 u1PktType, UINT1 u1EncapType)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4NextHopIpAddr);
    UNUSED_PARAM (au1MediaAddr);
    UNUSED_PARAM (u2Protocol);
    UNUSED_PARAM (u1PktType);
    UNUSED_PARAM (u1EncapType);

    return ISS_SUCCESS;
}

UINT4
CfaIsLinuxVlanIntf (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return TRUE;
}

UINT4
CfaIsLoopBackIntf (UINT4 u4IfIndex)
{
    /* change this when loop back supported is added in ISS */
    UNUSED_PARAM (u4IfIndex);
    return FALSE;
}

INT4
CfaIpIfGetSrcAddressOnInterface (UINT4 u4CfaIfIndex,
                                 UINT4 u4Dest, UINT4 *pu4Src)
{
    UNUSED_PARAM (u4CfaIfIndex);
    UNUSED_PARAM (u4Dest);
    UNUSED_PARAM (pu4Src);
    return ISS_SUCCESS;
}

INT4
CfaGetIfIpPort (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return ISS_SUCCESS;
}

INT4
CfaIpIfGetNextSecondaryAddress (UINT4 u4CfaIfIndex, UINT4 u4IpAddress,
                                UINT4 *pu4NextIpAddress, UINT4 *pu4NetMask)
{
    UNUSED_PARAM (u4CfaIfIndex);
    UNUSED_PARAM (u4IpAddress);
    UNUSED_PARAM (pu4NextIpAddress);
    UNUSED_PARAM (pu4NetMask);
    return ISS_SUCCESS;
}

INT4
CfaNotifyIpUp (VOID)
{
    return ISS_SUCCESS;
}

VOID
CfaHandleIpForwardingStatusUpdate (UINT1 u1IpForwardingStatus)
{
    UNUSED_PARAM (u1IpForwardingStatus);
}

INT4
CfaIpIfGetNextIpAddr (UINT4 u4IpAddress, UINT4 *pu4NextIpAddress)
{
    UNUSED_PARAM (u4IpAddress);
    UNUSED_PARAM (pu4NextIpAddress);
    return ISS_SUCCESS;
}

INT4
CfaIpIfGetIpAddressInfo (UINT4 u4IpAddress, UINT4 *pu4NetMask,
                         UINT4 *pu4BcastAddr, UINT4 *pu4CfaIfIndex)
{
    UNUSED_PARAM (u4IpAddress);
    UNUSED_PARAM (pu4NetMask);
    UNUSED_PARAM (pu4BcastAddr);
    UNUSED_PARAM (pu4CfaIfIndex);
    return ISS_SUCCESS;
}

UINT4
CfaGetDefaultRouterIfIndex (VOID)
{
    return ISS_SUCCESS;
}

INT4
CfaCliConfGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pi1IfName);
    return ISS_SUCCESS;
}

INT4
CfaInterfaceStatusChangeIndication (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1OperStatus);
    return ISS_SUCCESS;
}

INT4
CfaSendEventToCfaTask (UINT4 u4Event)
{
    UNUSED_PARAM (u4Event);
    return ISS_SUCCESS;

}

INT4
CfaNotifyStartPktProcessEvent (VOID)
{
    return OSIX_SUCCESS;
}

INT4
CfaNotifyStartBootupEvent (VOID)
{
    return OSIX_SUCCESS;
}

UINT4
CfaGetInterfaceNameFromIndex (UINT4 u4Index, UINT1 *pu1Alias)
{
    UNUSED_PARAM (u4Index);
    UNUSED_PARAM (pu1Alias);
    return ISS_SUCCESS;
}

INT4
CfaIpIfGetIfInfo (UINT4 u4IfIndex, tIpConfigInfo * pIpInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pIpInfo);
    return ISS_SUCCESS;
}

INT4
CfaCliGetIfList (INT1 *pi1IfName, INT1 *pi1IfListStr, UINT1 *pu1IfListArray,
                 UINT4 u4IfListArrayLen)
{
    UNUSED_PARAM (pi1IfName);
    UNUSED_PARAM (pi1IfListStr);
    UNUSED_PARAM (pu1IfListArray);
    UNUSED_PARAM (u4IfListArrayLen);
    return ISS_SUCCESS;
}

VOID
CfaNotifyProtoToApp (UINT1 u1ModeId, tNotifyProtoToApp NotifyProtoToApp)
{
    UNUSED_PARAM (u1ModeId);
    UNUSED_PARAM (NotifyProtoToApp);
}

INT4
CfaCliGetIndexFromType (UINT4 u4IfType, INT1 *pi1IfNum, UINT4 *pu4IfIndex)
{
    UNUSED_PARAM (u4IfType);
    UNUSED_PARAM (pi1IfNum);
    UNUSED_PARAM (pu4IfIndex);
    return ISS_SUCCESS;
}

INT4
CfaIfmBringupAllInterfaces (INT1 *pi1Dummy)
{
    UNUSED_PARAM (pi1Dummy);
    return ISS_SUCCESS;
}

INT4
CfaCliGetPoIndex (INT1 *pi1IfName, INT1 *pi1IfNum, UINT4 *pu4IfIndex)
{
    UNUSED_PARAM (pi1IfName);
    UNUSED_PARAM (pi1IfNum);
    UNUSED_PARAM (pu4IfIndex);
    return ISS_SUCCESS;
}

INT4
CfaCliGetVirtualIndex (INT1 *pi1IfName, INT1 *pi1IfNum, UINT4 *pu4IfIndex)
{
    UNUSED_PARAM (pi1IfName);
    UNUSED_PARAM (pi1IfNum);
    UNUSED_PARAM (pu4IfIndex);
    return ISS_SUCCESS;
}

VOID
CfaSetPauseMode (INT4 i4IssPortCtrlIndex, INT4 i4PortCtrlMode)
{
    UNUSED_PARAM (i4IssPortCtrlIndex);
    UNUSED_PARAM (i4PortCtrlMode);
    return;
}

INT4
CfaGetNextActivePort (UINT4 u4Port, UINT4 *pu4NextPort)
{
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (pu4NextPort);
    return ISS_SUCCESS;
}

UINT4
CfaGetInterfaceIndexFromName (UINT1 *pu1Alias, UINT4 *pu4Index)
{
    UNUSED_PARAM (pu1Alias);
    UNUSED_PARAM (pu4Index);
    return ISS_SUCCESS;
}

INT4
CfaIfGetIpAllocMethod (UINT4 u4IfIndex, UINT1 *pu1IpAllocMethod)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1IpAllocMethod);
    return CFA_SUCCESS;
}

UINT1
CfaIsTunnelInterface (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return ISS_SUCCESS;
}

INT1
nmhGetIfMainType (INT4 i4IfMainIndex, INT4 *pi4RetValIfMainType)
{
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (pi4RetValIfMainType);
    return ISS_SUCCESS;
}

INT1
nmhGetIfMainMtu (INT4 i4IfMainIndex, INT4 *pi4RetValIfMainMtu)
{
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (pi4RetValIfMainMtu);
    return ISS_SUCCESS;
}

INT1
nmhGetIfMainEncapType (INT4 i4IfMainIndex, INT4 *pi4RetValIfMainEncapType)
{
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (pi4RetValIfMainEncapType);
    return ISS_SUCCESS;
}

INT1
CfaGetLoopbackPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UNUSED_PARAM (pi1ModeName);
    UNUSED_PARAM (pi1DispStr);
    return ISS_SUCCESS;
}
