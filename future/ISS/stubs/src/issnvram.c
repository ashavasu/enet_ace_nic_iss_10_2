/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: issnvram.c,v 1.7 2012/05/10 13:18:17 siva Exp $
 *
 * Description: This file has the routines which are customer
 *              specific
 *
 *******************************************************************/

/* SOURCE FILE  :
 *
 *  ---------------------------------------------------------------------------
 * |  Copyright (C) Future Software, 2001-2001                                 |
 * |  Licensee Future Communications Software, 2001-2001                       |
 * |                                                                           |
 * |  FILE NAME                   :  issnvram.c                                |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR            :  Future Software Ltd                       |
 * |                                                                           |
 * |  SUBSYSTEM NAME              :  ISS                                       |
 * |                                                                           |
 * |  MODULE NAME                 :  ISS                                       |
 * |                                                                           |
 * |  LANGUAGE                    :  C                                         |
 * |                                                                           |
 * |  TARGET ENVIRONMENT          :  LINUX                                     |
 * |                                                                           |
 * |  AUTHOR                      :  ISS Team                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION                 :  This file has the routines for the Iss    |
 * |                                 Nvram related calls.                      |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 * 
 */

#include "issinc.h"

#define ISS_NVRAM_FILE        "issnvram.txt"

tIssSysGroupInfo    gIssSysGroupInfo;

/* table for mapping CIDR numbers to IP subnet masks - from RFC 1878*/
UINT4               gu4IssCidrSubnetMask[ISS_MAX_CIDR + 1] = {
    0x00000000, 0x80000000, 0xC0000000, 0xE0000000, 0xF0000000, 0xF8000000,
    0xFC000000, 0xFE000000, 0xFF000000, 0xFF800000, 0xFFC00000, 0xFFE00000,
    0xFFF00000, 0xFFF80000, 0xFFFC0000, 0xFFFE0000, 0xFFFF0000, 0xFFFF8000,
    0xFFFFC000, 0xFFFFE000, 0xFFFFF000, 0xFFFFF800, 0xFFFFFC00, 0xFFFFFE00,
    0xFFFFFF00, 0xFFFFFF80, 0xFFFFFFC0, 0xFFFFFFE0, 0xFFFFFFF0, 0xFFFFFFF8,
    0xFFFFFFFC, 0xFFFFFFFE, 0xFFFFFFFF
};

tNVRAM_DATA         sNvRamData;
tNVRAM_AGG_MAC      sNvRamAggMac[LA_DEV_MAX_TRUNK_GROUP];

extern INT1         gi1SnmpEngineID[SNMP_MAX_STR_ENGINEID_LEN + 1];

#ifndef NPAPI_WANTED
#define FNP_SUCCESS      1
#define FNP_FAILURE      0
#endif

/****************************************************************************
*                   NVRAM RELATED PORTABLE FUNCIONS                         *
*****************************************************************************/

/****************************************************************************/
/*                                                                          */
/*    Function Name      : NvRamRead                                        */
/*                                                                          */
/*    Description        : This function is is a portable routine, to       */
/*                         read the critical configuration for the switch   */
/*                         from NvRam.                                      */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : tNVRAM_DATA  *pNvRamData                         */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/*                                                                          */
/*    Note               : The return type should be as mentioned above for */
/*                         ported routines also.                            */
/****************************************************************************/
int
NvRamRead (tNVRAM_DATA * pNvRamData)
{
    UINT1               u1Index;
    UINT1               au1Buffer[MAX_COLUMN_LENGTH];
    UINT1              *pTemp = NULL;
    UINT1               au1TempMac[18];
    UINT1               u1ErrorCount = 0;
    tMacAddr            DefaultSwitchMac
        = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05 };

    /* First check whether the file exist or not */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "IP_CONFIG_MODE",
                       au1Buffer) == NVRAM_FILE_NOT_FOUND)
    {
        PRINTF ("\r\n Writing new ISS_NVRAM_FILE with default values \r\n");
        return FNP_SUCCESS;
    }

    /* Read the IP Config Mode */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "IP_CONFIG_MODE",
                       au1Buffer) == NVRAM_FAILURE)
    {
        PRINTF ("\r\n [ERROR]: Failed to read ip config mode \r\n");
        u1ErrorCount++;
        pNvRamData->u4CfgMode = ISS_CFG_MANUAL;
    }
    else
    {
        pNvRamData->u4CfgMode = (UINT4) ATOI (au1Buffer);
    }

    /* Check for presence of IP_ADDR_ALLOC_PROTO */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE,
                       (UINT1 *) "IP_ADDR_ALLOC_PROTO", au1Buffer)
        == NVRAM_FAILURE)
    {
        PRINTF ("\r\n [ERROR]: Failed to read boot protocol \r\n");
        u1ErrorCount++;
        pNvRamData->u4IpAddrAllocProto = CFA_PROTO_DHCP;
    }
    else
    {
        pNvRamData->u4IpAddrAllocProto = (UINT4) ATOI (au1Buffer);
    }

    /* Read default IP Address */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "IP_ADDRESS",
                       au1Buffer) == NVRAM_FAILURE)
    {
        PRINTF ("\r\n [ERROR]: Failed to read ip address \r\n");
        u1ErrorCount++;
        pNvRamData->u4LocalIpAddr = ISS_DEFAULT_IP_ADDRESS;
    }
    else
    {
        pNvRamData->u4LocalIpAddr = OSIX_NTOHL (INET_ADDR (au1Buffer));

        /* Allow IP address which are Class A, B or C */
        if (!(((ISS_IS_ADDR_CLASS_A (pNvRamData->u4LocalIpAddr)) ||
               (ISS_IS_ADDR_CLASS_B (pNvRamData->u4LocalIpAddr)) ||
               (ISS_IS_ADDR_CLASS_C (pNvRamData->u4LocalIpAddr))) &&
              (ISS_IS_ADDR_VALID (pNvRamData->u4LocalIpAddr))))
        {
            PRINTF ("\r\n [ERROR]: Invalid ip address \r\n");
            u1ErrorCount++;
            pNvRamData->u4LocalIpAddr = ISS_DEFAULT_IP_ADDRESS;
        }
    }

    /* Read the IP Mask */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "IP_MASK",
                       au1Buffer) == NVRAM_FAILURE)
    {
        PRINTF ("\r\n [ERROR]: Failed to read Ip mask \r\n");
        u1ErrorCount++;
        pNvRamData->u4LocalSubnetMask = ISS_DEFAULT_IP_MASK;
    }
    else
    {
        pNvRamData->u4LocalSubnetMask = OSIX_NTOHL (INET_ADDR (au1Buffer));

        /* inet_addr routine returns 0xffffffff for all invalid values of
         * au1Buffer. However if au1Buffer = "255.255.255.255" , which is a
         * valid, value it again returns 0xffffffff. This is taken care of by
         * the following check */
        if (pNvRamData->u4LocalSubnetMask == 0xffffffff)
        {
            if (STRNCMP ("255.255.255.255", au1Buffer,
                         STRLEN ("255.255.255.255")))
            {
                PRINTF ("\r\n [ERROR]: Incorrect Ip mask\r\n");
                u1ErrorCount++;
                pNvRamData->u4LocalSubnetMask = ISS_DEFAULT_IP_MASK;
            }
        }

        /* The valid subnet address is from 0 to 32 */
        for (u1Index = 0; u1Index <= ISS_MAX_CIDR; ++u1Index)
        {
            if (gu4IssCidrSubnetMask[u1Index] == pNvRamData->u4LocalSubnetMask)
            {
                break;
            }
        }
        if (u1Index > ISS_MAX_CIDR)
        {
            PRINTF ("\r\n [ERROR]: Invalid Subnet Mask\r\n");
            u1ErrorCount++;
            pNvRamData->u4LocalSubnetMask = ISS_DEFAULT_IP_MASK;
        }
    }

    /* Read the Default Interface */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "INTERFACE",
                       au1Buffer) == NVRAM_FAILURE)
    {
        PRINTF ("\r\n [ERROR]: Failed to read default interface\r\n");
        u1ErrorCount++;
        MEMCPY ((UINT1 *) pNvRamData->ai1InterfaceName,
                ISS_DEFAULT_INTERFACE_NAME,
                STRLEN (ISS_DEFAULT_INTERFACE_NAME));
        pNvRamData->ai1InterfaceName[STRLEN (ISS_DEFAULT_INTERFACE_NAME)]
            = '\0';
    }
    else
    {
        if (STRLEN (au1Buffer) >= INTERFACE_STR_SIZE)
        {
            PRINTF ("\r\n [ERROR]: default interface exceeds size\r\n");
            u1ErrorCount++;
            MEMCPY ((UINT1 *) pNvRamData->ai1InterfaceName,
                    ISS_DEFAULT_INTERFACE_NAME,
                    STRLEN (ISS_DEFAULT_INTERFACE_NAME));
            pNvRamData->ai1InterfaceName[STRLEN (ISS_DEFAULT_INTERFACE_NAME)]
                = '\0';
        }
        else
        {
            MEMCPY ((UINT1 *) pNvRamData->ai1InterfaceName, au1Buffer,
                    STRLEN (au1Buffer));
            pNvRamData->ai1InterfaceName[STRLEN (au1Buffer)] = '\0';
        }
    }

    /* Check for presence of OOB port */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "MGMT_PORT",
                       au1Buffer) == NVRAM_FAILURE)
    {
        PRINTF ("\r\n [ERROR]: Failed to read MgmtPort Flag\r\n");
        u1ErrorCount++;
#ifdef MBSM_WANTED
        pNvRamData->u4MgmtPort = TRUE;
#else
        pNvRamData->u4MgmtPort = FALSE;
#endif
    }
    else
    {
        if (STRNCMP (au1Buffer, "YES", STRLEN (au1Buffer)) == 0)
        {
            pNvRamData->u4MgmtPort = TRUE;    /* MgmtPort is present */

        }
        else
        {
            pNvRamData->u4MgmtPort = FALSE;    /* MgmtPort is not present */
        }
    }

    /* Validate the Default Interface Name if OOB is not Present */
    if (pNvRamData->u4MgmtPort == FALSE)
    {
        if (STRNCMP (pNvRamData->ai1InterfaceName, ISS_ALIAS_PREFIX,
                     STRLEN (ISS_ALIAS_PREFIX)) != 0)
        {
            PRINTF ("\r\n [ERROR]: Mismatch between Alias prefix & "
                    "Default interface\r\n");
            u1ErrorCount++;
        }
    }

#ifdef RM_WANTED
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "RM_INTERFACE",
                       au1Buffer) == NVRAM_FAILURE)
    {
        PRINTF ("\r\n [ERROR]: Failed to read RM Interface\r\n");
        u1ErrorCount++;
        MEMCPY ((UINT1 *) pNvRamData->ai1RmIfName,
                ISS_DEFAULT_RM_INTERFACE_NAME,
                STRLEN (ISS_DEFAULT_RM_INTERFACE_NAME));
        pNvRamData->ai1RmIfName[STRLEN (ISS_DEFAULT_RM_INTERFACE_NAME)] = '\0';
    }
    else
    {
        if ((STRLEN (au1Buffer)) >= INTERFACE_STR_SIZE)
        {
            PRINTF ("\r\n [ERROR]: RM interface exceeds size\r\n");
            u1ErrorCount++;
            MEMCPY ((UINT1 *) pNvRamData->ai1RmIfName,
                    ISS_DEFAULT_RM_INTERFACE_NAME,
                    STRLEN (ISS_DEFAULT_RM_INTERFACE_NAME));
            pNvRamData->ai1RmIfName[STRLEN (ISS_DEFAULT_RM_INTERFACE_NAME)]
                = '\0';
        }
        else
        {
            MEMCPY ((UINT1 *) pNvRamData->ai1RmIfName, au1Buffer,
                    STRLEN (au1Buffer));
            pNvRamData->ai1RmIfName[STRLEN (au1Buffer)] = '\0';
        }
    }
#else
    MEMCPY ((UINT1 *) pNvRamData->ai1RmIfName,
            ISS_DEFAULT_RM_INTERFACE_NAME,
            STRLEN (ISS_DEFAULT_RM_INTERFACE_NAME));
    pNvRamData->ai1RmIfName[STRLEN (ISS_DEFAULT_RM_INTERFACE_NAME)] = '\0';
#endif

    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "PIM_MODE",
                       au1Buffer) == NVRAM_FAILURE)
    {
        PRINTF ("\r\n [ERROR]: Failed to read Pim mode\r\n");
        u1ErrorCount++;
        pNvRamData->u4PimMode = ISS_DEFAULT_PIM_MODE;
    }
    else
    {
        pNvRamData->u4PimMode = (UINT4) ATOI (au1Buffer);
        /* PIM Modes
         * PIM_DM_MODE = 1
         * PIM_SM_MODE = 2
         */
        if ((pNvRamData->u4PimMode != 1) && (pNvRamData->u4PimMode != 2))
        {
            PRINTF ("\r\n [ERROR]: Invalid Pim Mode\r\n");
            u1ErrorCount++;
            pNvRamData->u4PimMode = ISS_DEFAULT_PIM_MODE;
        }
    }

    /* Read the Bridge Mode */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "BRIDGE_MODE",
                       au1Buffer) == NVRAM_FAILURE)
    {
        PRINTF ("\r\n [ERROR]: Failed to Bridge Mode\r\n");
        u1ErrorCount++;
        pNvRamData->u4BridgeMode = ISS_DEFAULT_BRIDGE_MODE;
    }
    else
    {
        pNvRamData->u4BridgeMode = (UINT4) ATOI (au1Buffer);

        /* Bridge Modes
         * VLAN_CUSTOMER_BRIDGE_MODE = 1
         * VLAN_PROVIDER_BRIDGE_MODE = 2
         * VLAN_PROVIDER_EDGE_BRIDGE_MODE = 3
         * VLAN_PROVIDER_CORE_BRIDGE_MODE = 4
         */
        if ((pNvRamData->u4BridgeMode < ISS_CUSTOMER_BRIDGE_MODE) &&
            (pNvRamData->u4BridgeMode > ISS_PROVIDER_CORE_BRIDGE_MODE))
        {
            PRINTF ("\r\n [ERROR]: Invalid Bridge Mode\r\n");
            u1ErrorCount++;
            pNvRamData->u4PimMode = ISS_DEFAULT_BRIDGE_MODE;
        }

    }

    /* Read the IGS Forward mode */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE,
                       (UINT1 *) "SNOOP_FORWARD_MODE", au1Buffer)
        == NVRAM_FAILURE)
    {
        PRINTF ("\r\n [ERROR]: Failed to read IGS Forward mode\r\n");
        u1ErrorCount++;
        pNvRamData->u4SnoopFwdMode = ISS_DEFAULT_SNOOP_FWD_MODE;
    }
    else
    {
        pNvRamData->u4SnoopFwdMode = (UINT4) ATOI (au1Buffer);
        /* IGS Forward Modes
         * ISS_IGS_IP = 1
         * ISS_IGS_MAC = 2
         */
        if ((pNvRamData->u4SnoopFwdMode != 1)
            && (pNvRamData->u4SnoopFwdMode != 2))
        {
            PRINTF ("\r\n [ERROR]: Invalid IGS Forward Mode\r\n");
            u1ErrorCount++;
            pNvRamData->u4SnoopFwdMode = ISS_DEFAULT_SNOOP_FWD_MODE;
        }
    }

    /* Read the save flag */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "SAVE_FLAG",
                       au1Buffer) == NVRAM_FAILURE)
    {
        PRINTF ("\r\n [ERROR]: Failed to read Save Flag\r\n");
        u1ErrorCount++;
        pNvRamData->i1SaveFlag = ISS_DEFAULT_SAVE_FLAG;
    }
    else
    {
        pNvRamData->i1SaveFlag = (INT1) ATOI (au1Buffer);
    }

    /* Read the restore flag */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "RES_FLAG",
                       au1Buffer) == NVRAM_FAILURE)
    {
        PRINTF ("\r\n [ERROR]: Failed to read Restore Flag\r\n");
        u1ErrorCount++;
        pNvRamData->i1ResFlag = ISS_DEFAULT_RESTORE_FLAG;
    }
    else
    {
        pNvRamData->i1ResFlag = (INT1) ATOI (au1Buffer);
    }

    /* Read the restore option */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "RES_OPTION",
                       au1Buffer) == NVRAM_FAILURE)
    {
        PRINTF ("\r\n [ERROR]: Failed to read Restore Option\r\n");
        u1ErrorCount++;
        pNvRamData->u4RestoreOption = ISS_CONFIG_NO_RESTORE;
    }
    else
    {
        pNvRamData->u4RestoreOption = (UINT4) ATOI (au1Buffer);

        /* Currently Only Local Restore is supported */
        pNvRamData->u4RestoreIpAddr = ISS_DEFAULT_RES_IP_ADDRESS;

        /* Allow IP address which are Class A, B or C */
        if (!(((ISS_IS_ADDR_CLASS_A (pNvRamData->u4RestoreIpAddr)) ||
               (ISS_IS_ADDR_CLASS_B (pNvRamData->u4RestoreIpAddr)) ||
               (ISS_IS_ADDR_CLASS_C (pNvRamData->u4RestoreIpAddr))) &&
              (ISS_IS_ADDR_VALID (pNvRamData->u4LocalIpAddr))))
        {
            PRINTF ("\r\n [ERROR]: Invalid Restore Ip Address\r\n");
            u1ErrorCount++;
        }
    }

    /* Read the CLI serial console presence option */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "CONSOLE_CLI",
                       au1Buffer) == NVRAM_FAILURE)
    {
        PRINTF ("\r\n [ERROR]: Failed to read CLI Serial Console Option\r\n");
        u1ErrorCount++;
        pNvRamData->u4CliSerialConsole = ISS_DEFAULT_CLI_SERIAL_CONSOLE;
    }
    else
    {
        /* CLI console prompt required in serial console */
        pNvRamData->u4CliSerialConsole = (UINT4) ATOI (au1Buffer);
    }

    /* Read the switch mac address */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "SWITCH_MAC",
                       au1Buffer) == NVRAM_FAILURE)
    {
        PRINTF ("\r\n [ERROR]: Failed to read Switch Mac Address\r\n");
        u1ErrorCount++;
        MEMCPY (pNvRamData->au1SwitchMac, DefaultSwitchMac, 6);
    }
    else
    {
        MEMSET (au1TempMac, 0, 18);
        MEMSET (pNvRamData->au1SwitchMac, 0, 6);
        MEMCPY (au1TempMac, au1Buffer, 17);
        au1TempMac[17] = '\0';

        if ((pTemp = (UINT1 *) STRSTR (au1Buffer, ".")) != NULL)
        {
            PRINTF ("\r\n [ERROR]: Failed to read Switch Mac Address\r\n");
            u1ErrorCount++;
            MEMCPY (pNvRamData->au1SwitchMac, DefaultSwitchMac, 6);
        }
        else
        {
            StrToMac (au1TempMac, pNvRamData->au1SwitchMac);
        }
    }

    /* Read the SNMP Engine ID */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "SNMP_ENGINE_ID",
                       au1Buffer) == NVRAM_FAILURE)
    {
        PRINTF ("\r\n [ERROR]: Failed to read Snmp Engine ID \r\n");
        u1ErrorCount++;
        MEMCPY (pNvRamData->ai1SnmpEngineID, gi1SnmpEngineID,
                STRLEN (gi1SnmpEngineID));
    }
    else
    {
        MEMCPY ((UINT1 *) pNvRamData->ai1SnmpEngineID, au1Buffer,
                STRLEN ((const char *) au1Buffer));

        pNvRamData->ai1SnmpEngineID[STRLEN (au1Buffer)] = '\0';
    }

/*  Read the Default Vlan Id */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "DEFAULT_VLAN",
                       au1Buffer) == NVRAM_FAILURE)
    {
        PRINTF ("\r\n [ERROR]: Failed to get the default vlan\r\n");
        u1ErrorCount++;
        pNvRamData->u2DefaultVlan = ISS_DEFAULT_VLAN_ID;
    }
    else
    {
        pNvRamData->u2DefaultVlan = (UINT4) ISS_ATOI (au1Buffer);

        if ((pNvRamData->u2DefaultVlan < VLAN_DEV_MIN_VLAN_ID) ||
            (pNvRamData->u2DefaultVlan > VLAN_DEV_MAX_VLAN_ID))
        {
            PRINTF
                ("\r\n [ERROR]: Default vlan Id not in the allowed range\r\n");
            u1ErrorCount++;
            pNvRamData->u2DefaultVlan = ISS_DEFAULT_VLAN_ID;
        }
    }

    /* Read the NPAPI Mode of Processing */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "NPAPI_MODE",
                       au1Buffer) == NVRAM_FAILURE)
    {
        PRINTF ("\r\n [ERROR]: Failed to get the NPAPI MODE\r\n");
        u1ErrorCount++;
        ISS_MEMCPY ((UINT1 *) pNvRamData->au1NpapiMode,
                    ISS_DEFAULT_NPAPI_MODE, STRLEN (ISS_DEFAULT_NPAPI_MODE));
        pNvRamData->au1NpapiMode[STRLEN (ISS_DEFAULT_NPAPI_MODE)] = '\0';
    }
    else
    {
        if ((STRCMP (au1Buffer, "Asynchronous") != 0) &&
            (STRCMP (au1Buffer, "Synchronous") != 0))
        {
            PRINTF ("\r\n [ERROR]: NPAPI Mode Incorrect\r\n");
            u1ErrorCount++;
            ISS_MEMCPY ((UINT1 *) pNvRamData->au1NpapiMode,
                        ISS_DEFAULT_NPAPI_MODE,
                        STRLEN (ISS_DEFAULT_NPAPI_MODE));
            pNvRamData->au1NpapiMode[STRLEN (ISS_DEFAULT_NPAPI_MODE)] = '\0';
        }
        else
        {
            ISS_MEMCPY ((UINT1 *) pNvRamData->au1NpapiMode,
                        au1Buffer, STRLEN (au1Buffer));
            pNvRamData->au1NpapiMode[STRLEN (au1Buffer)] = '\0';
        }
    }

    if (u1ErrorCount > 0)
    {
        return FNP_FAILURE;
    }
    else
    {
        return FNP_SUCCESS;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : NvRamWrite                                       */
/*                                                                          */
/*    Description        : This function is a portable routine, to       */
/*                         write the critical configuration for the switch  */
/*                         to the NvRam.                                    */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/*                                                                          */
/*    Note               : The return type should be as mentioned above for */
/*                         ported routines also.                            */
/****************************************************************************/
int
NvRamWrite (tNVRAM_DATA * pNvRamData)
{
    FILE               *fp = NULL;
    tUtlInAddr          IpAddr;
    UINT1               u1MacLen;

    fp = FOPEN (ISS_NVRAM_FILE, "w");
    if (fp == NULL)
    {
        return FNP_FAILURE;
    }
    fprintf (fp, "IP_CONFIG_MODE    =%d\n", pNvRamData->u4CfgMode);
    fprintf (fp, "IP_ADDR_ALLOC_PROTO =%d\n", pNvRamData->u4IpAddrAllocProto);
    IpAddr.u4Addr = OSIX_NTOHL (pNvRamData->u4LocalIpAddr);
    fprintf (fp, "IP_ADDRESS        =%s\n", UtlInetNtoa (IpAddr));
    IpAddr.u4Addr = OSIX_NTOHL (pNvRamData->u4LocalSubnetMask);
    fprintf (fp, "IP_MASK           =%s\n", UtlInetNtoa (IpAddr));
    fprintf (fp, "INTERFACE         =%s\n", pNvRamData->ai1InterfaceName);
    if (pNvRamData->u4MgmtPort == TRUE)
    {
        fprintf (fp, "MGMT_PORT         =%s\n", "YES");
    }
    else
    {
        fprintf (fp, "MGMT_PORT         =%s\n", "NO");
    }

    fprintf (fp, "RM_INTERFACE      =%s\n", pNvRamData->ai1RmIfName);
    fprintf (fp, "PIM_MODE          =%d\n", pNvRamData->u4PimMode);
    fprintf (fp, "BRIDGE_MODE          =%d\n", pNvRamData->u4BridgeMode);
    fprintf (fp, "SNOOP_FORWARD_MODE  =%d\n", pNvRamData->u4SnoopFwdMode);
    fprintf (fp, "SAVE_FLAG         =%d\n", pNvRamData->i1SaveFlag);
    fprintf (fp, "RES_FLAG          =%d\n", pNvRamData->i1ResFlag);
    fprintf (fp, "RES_OPTION        =%d\n", pNvRamData->u4RestoreOption);
    fprintf (fp, "RES_FILE_NAME     =%s\n", pNvRamData->ai1RestoreFileName);
    fprintf (fp, "CONSOLE_CLI       =%d\n", pNvRamData->u4CliSerialConsole);
    fprintf (fp, "SWITCH_MAC        =");
    for (u1MacLen = 0; u1MacLen < ISS_MAC_LEN; u1MacLen++)
    {
        fprintf (fp, "%02x", pNvRamData->au1SwitchMac[u1MacLen]);
        if (u1MacLen < (ISS_MAC_LEN - 1))
            fprintf (fp, ":");
    }
    fprintf (fp, "\n");
    fprintf (fp, "SNMP_ENGINE_ID    =%s\n", pNvRamData->ai1SnmpEngineID);
    fprintf (fp, "DEFAULT_VLAN =%d\n", pNvRamData->u2DefaultVlan);
    fprintf (fp, "NPAPI_MODE =%s\n", pNvRamData->au1NpapiMode);

    fflush (fp);
    fclose (fp);
#if defined(SWC)
    system ("etc flush");
#endif
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : GetNvRamValue                                    */
/*                                                                          */
/*    Description        : This function searches pu1SearchString in the    */
/*                         given file and stores the result in pu1StrValue. */
/*                         Each and every line of the file should be in the */
/*                         following format:                                */
/*                         IP_ADDRESS        =     12.0.0.1                 */
/*                         i.e pu1SearchString = pu1StrValue                */
/*                         White space character(s) should follow           */
/*                         pu1SearchString (atleast one). No matter whether */
/*                         pu1StrValue is preceded by white space character */
/*                         or not. Size of pu1StrValue is expected to be    */
/*                         greater than or equal to MAX_COLUMN_LENGTH. It   */
/*                         will store first match value in case of duplicate*/
/*                         pu1SearchString.                                 */
/*                                                                          */
/*    Input(s)           : File name, Search string                         */
/*                                                                          */
/*    Output(s)          : pu1StrValue                                      */
/*                                                                          */
/*    Returns            : NVRAM_SUCCESS/NVRAM_FAILURE/NVRAM_FILE_NOT_FOUND */
/*                                                                          */
/****************************************************************************/

UINT4
GetNvRamValue (UINT1 *pu1FileName, UINT1 *pu1SearchStr, UINT1 *pu1StrValue)
{
    FILE               *pFile;
    CHR1                au1Buf[MAX_COLUMN_LENGTH],
        au1ReadStr[MAX_COLUMN_LENGTH];
    UINT1               au1StrValue[MAX_COLUMN_LENGTH],
        au1TmpValue[MAX_COLUMN_LENGTH];
    UINT4               u4Flag = 0;
    UINT1              *pu1Temp;

    pFile = FOPEN ((CONST CHR1 *) pu1FileName, "r");
    if (pFile == NULL)
    {
        PRINTF ("\r\n [ERROR]: Failed to open NVRAM \r\n");
        return NVRAM_FILE_NOT_FOUND;
    }

    MEMSET (pu1StrValue, 0, MAX_COLUMN_LENGTH);

    while (!feof (pFile))
    {

        fgets (au1Buf, MAX_COLUMN_LENGTH, pFile);
        if ((STRSTR (au1Buf, pu1SearchStr)) != NULL)
        {
            MEMSET (au1ReadStr, 0, MAX_COLUMN_LENGTH);
            MEMSET (au1StrValue, 0, MAX_COLUMN_LENGTH);
            MEMSET (au1TmpValue, 0, MAX_COLUMN_LENGTH);

            SSCANF (au1Buf, "%120s%120s%120s", au1ReadStr, au1StrValue,
                    au1TmpValue);
            if (STRCMP (au1ReadStr, pu1SearchStr) != 0)
            {
                continue;
            }
            if (STRLEN (au1TmpValue) != 0)
            {
                STRCPY (pu1StrValue, au1TmpValue);
            }
            else
            {
                pu1Temp = au1StrValue;
                pu1Temp++;
                STRCPY (pu1StrValue, pu1Temp);
            }

            u4Flag = 1;
            break;
        }
    }

    fclose (pFile);

    if (u4Flag == 1)
    {
        return NVRAM_SUCCESS;
    }
    else
    {
        return NVRAM_FAILURE;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : NvRamReadAggregatorMac                           */
/*                                                                          */
/*    Description        : This is a portable routine                       */
/*                         --> To derive the Mac address for each aggregator*/
/*                             in the switch based on the 'base mac address'*/
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
NvRamReadAggregatorMac (VOID)
{
    UINT4               u4MacIndex;
    UINT4               u4AggIndex = SYS_DEF_MAX_PHYSICAL_INTERFACES;
    tMacAddr            au1BaseMac;
    UINT1               u1OldMac;

    MEMSET (au1BaseMac, 0, CFA_ENET_ADDR_LEN);

    CfaGetSysMacAddress (au1BaseMac);

    for (u4MacIndex = 0; u4MacIndex < LA_DEV_MAX_TRUNK_GROUP;
         u4MacIndex++, u4AggIndex++)
    {
        MEMCPY (sNvRamAggMac[u4MacIndex].au1AggregatorMac, au1BaseMac,
                CFA_ENET_ADDR_LEN);

        u1OldMac = sNvRamAggMac[u4MacIndex].au1AggregatorMac[5];
        sNvRamAggMac[u4MacIndex].au1AggregatorMac[5] += u4AggIndex;

        /* If 6th octet New value is less than old value, the other octets
         * needs to be updated.
         */
        if (sNvRamAggMac[u4MacIndex].au1AggregatorMac[5] < u1OldMac)
        {
            sNvRamAggMac[u4MacIndex].au1AggregatorMac[4] += 1;
            if (sNvRamAggMac[u4MacIndex].au1AggregatorMac[4] == 0x00)
            {
                sNvRamAggMac[u4MacIndex].au1AggregatorMac[3] += 1;
                if (sNvRamAggMac[u4MacIndex].au1AggregatorMac[3] == 0x00)
                {
                    sNvRamAggMac[u4MacIndex].au1AggregatorMac[2] += 1;
                    if (sNvRamAggMac[u4MacIndex].au1AggregatorMac[2] == 0x00)
                    {
                        sNvRamAggMac[u4MacIndex].au1AggregatorMac[1] += 1;
                    }
                }
            }
        }

        /* If AggIndex exceeds 256, sNvRamAggMac[u4MacIndex].au1AggregatorMac[5]
         * value >= old-value.The other octets will not get updated.So we need
         * to check it.
         */
        if ((u4AggIndex / 256) != 0)
        {
            u1OldMac = sNvRamAggMac[u4MacIndex].au1AggregatorMac[4];

            /* If 5th octet New value is less than old value, the other octets
             * needs to be updated.
             */
            sNvRamAggMac[u4MacIndex].au1AggregatorMac[4] += (u4AggIndex / 256);

            if (sNvRamAggMac[u4MacIndex].au1AggregatorMac[4] <= u1OldMac)
            {
                sNvRamAggMac[u4MacIndex].au1AggregatorMac[3] += 1;
                if (sNvRamAggMac[u4MacIndex].au1AggregatorMac[3] == 0x00)
                {
                    sNvRamAggMac[u4MacIndex].au1AggregatorMac[2] += 1;
                    if (sNvRamAggMac[u4MacIndex].au1AggregatorMac[2] == 0x00)
                    {
                        sNvRamAggMac[u4MacIndex].au1AggregatorMac[1] += 1;
                    }
                }

            }
        }
    }
}

#ifndef NPAPI_WANTED
/****************************************************************************/
/*                                                                          */
/*    Function Name      : NvRamMacRead                                     */
/*                                                                          */
/*    Description        : This function is a portable routine, to          */
/*                         Read the MAC address from the NVRAM              */
/*                                                                          */
/*    Input(s)           : u2IfIndex,                                       */
/*                         pu1HwAddr                                        */
/*                                                                          */
/*    Output(s)          : pu1HwAddr                                        */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/ISS_FAILURE                          */
/*                                                                          */
/****************************************************************************/
int
NvRamMacRead (unsigned int u2IfIndex, char *pu1HwAddr)
{
    if (u2IfIndex == 0)
    {
        return FNP_FAILURE;
    }

    pu1HwAddr[0] = 0x01;
    pu1HwAddr[1] = 0x02;
    pu1HwAddr[2] = 0x03;
    pu1HwAddr[3] = 0x04;
    pu1HwAddr[4] = 0x05;
    pu1HwAddr[5] = 0x06;

    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : NvRamMacWriteBulk                                */
/*                                                                          */
/*    Description        : This is a portable routine                       */
/*                         --> To derive the Mac address for each interface */
/*                             in the switch based on the 'base mac address'*/
/*                         --> To write all the interface MAC addresses     */
/*                             that are derived, to Nvram,so that each time */
/*                             the switch comes up with this MAC address    */
/*                                                                          */
/*    Input(s)           : pu1BaseMac - Starting H/W address for the switch.*/
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/****************************************************************************/
int
NvRamMacWriteBulk (unsigned char *pu1BaseMac)
{
    MEMCPY (sNvRamData.au1SwitchMac, pu1BaseMac, sizeof (tMacAddr));

    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        PRINTF ("FAILED to write to NvRam\n");
        return FNP_FAILURE;
    }

    return FNP_SUCCESS;
}

#endif

const char         *
IssGetHardwareVersion ()
{
    return ISS_HW_VERSION;
}

const char         *
IssGetFirmwareVersion ()
{
    return ISS_CUST_FW_VERSION;
}

const char         *
IssGetSwitchName ()
{
    return "ISS";
}

const char         *
IssGetSysLocation ()
{
    return "FutureSoft";
}

const char         *
IssGetSysContact ()
{
    return "support@future.futsoft.com";
}

/* Newly added stubs */
UINT4
IssGetIpAddrAllocProtoFromNvRam ()
{
    return (0);
}

const char         *
IssGetCustomCliPrompt ()
{
    return "iss";
}

INT1
IssGetCliDisplayBanner (CHR1 * pu1BannerMsg)
{
    STRCPY (pu1BannerMsg, "FS Intelligent Switch Solution");
    return ISS_SUCCESS;
}
