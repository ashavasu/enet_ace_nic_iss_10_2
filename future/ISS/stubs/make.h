#!/bin/csh
# (C) 2002 Future Software Pvt. Ltd.
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Future Software Pvt. Ltd.                     |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX ( Slackware 1.2.1 )                     |
# |                                                                          |
# |   DATE                   : 07 March 2002                                 |
# |                                                                          |
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################


TOTAL_OPNS = $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################
ISS_DIR = ${BASE_DIR}/ISS/stubs
ISS_BASE_DIR = ${ISS_DIR}
ISS_SRC_DIR  = ${ISS_BASE_DIR}/src
ISS_INC_DIR  = ${ISS_BASE_DIR}/inc
ISS_OBJ_DIR  = ${ISS_BASE_DIR}/obj
COMMON_INC_DIR  = ${BASE_DIR}/inc
SNMP_INC_DIR  = ${BASE_DIR}/inc/snmp
# FS_DELTA - Define CFA_BASE_DIR
CFA_BASE_DIR = ${BASE_DIR}/cfa2
CFA_INCD    = ${CFA_BASE_DIR}/inc

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  = -I${ISS_INC_DIR} -I${CFA_INCD} -I${COMMON_INC_DIR} -I${SNMP_INC_DIR}
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################
