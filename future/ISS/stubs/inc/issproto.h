/********************************************************************
 *     Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *   
 *      $Id: issproto.h,v 1.6 2015/04/07 06:14:43 siva Exp $
 *     
 *      Description: Routines for snmp agent main module
 ********************************************************************/

INT1
nmhGetFsIpifDrtBcastFwdingEnable (INT4 i4FsIpifIndex,
                                  INT4 *pi4RetValFsIpifDrtBcastFwdingEnable);

INT1
nmhTestv2FsIpPmtuEntryAge (UINT4 *pu4ErrorCode, INT4 i4TestValFsIpPmtuEntryAge);

INT1
nmhSetFsIpPmtuEntryAge (INT4 i4SetValFsIpPmtuEntryAge);

INT1
nmhGetIssPortHOLBlockPrevention (INT4 i4IssPortCtrlIndex,
                                 INT4 *pi4RetValIssPortHOLBlockPrevention);

INT4
IssHwGetPortSpeed (UINT4 u4Port, INT4 *pi4PortSpeed);

INT4
IssApplyFilter PROTO ((UINT2 u2InPort, UINT2 InVlan, UINT4 u4SrcIp, 
                       UINT1 u1L3Proto, UINT2 u2L4DstPort,BOOL1 bOOBFlag));

#ifdef OS_RTLINUX

INT4
IssHwInitFilter (VOID);

VOID
FsNpProcVlanAdd (INT4 i4UnitId, UINT2 u2VlanId);

VOID
FsNpProcVlanDelete (INT4 i4UnitId, UINT2 u2VlanId);

UINT4
FsNpIpv4DeleteIpInterface (UINT4 u4VrId, UINT4 u4IfIndex, UINT2 u2VlanId);

UINT4
FsNpIpv4CreateIpInterface (UINT4 u4VrId,
                           UINT4 u4CfaIfIndex,
                           UINT4 u4IpAddr, UINT4 u4IpSubNetMask,
                           UINT2 u2VlanId, UINT1 *au1MacAddr);

#endif /* OS_RTLINUX */

INT4
CfaIfmBringupAllInterfaces (VOID);

VOID
CfaNpFillInterfaceName (VOID);

VOID
StackCustomStartup (VOID);

VOID IssInitSystemParams PROTO (( VOID ));


/* Reading the System Information */
INT4 IssReadSystemInfoFromNvRam PROTO ((VOID));
VOID IssInitialiseNvRamDefVal PROTO ((VOID));

/* Function for getting Nvram Data */
INT1 *IssGetInterfaceFromNvRam PROTO ((VOID));
UINT4 IssGetSubnetMaskFromNvRam PROTO ((VOID));
UINT4 IssGetVlanLearningModeFromNvRam PROTO ((VOID));
UINT4 IssGetRestoreOptionFromNvRam PROTO((VOID));
VOID  IssSetVlanLearningModeToNvRam PROTO((UINT4));

unsigned char * EoidGetEnterpriseOid PROTO((VOID));
unsigned char * EoidGetSecondEnterpriseOid PROTO ((VOID));

INT4 IssIntfShowRunningConfig PROTO((INT4, UINT4, UINT1*));
INT4 AclShowRunningConfigInterfaceDetails PROTO ((INT4 i4Handle, 
                                                  UINT4 u4Port));

INT1 *IssGetRmIfNameFromNvRam PROTO ((VOID));
UINT4 IssGetMgmtPortFromNvRam PROTO ((VOID));
const char  * IssGetHardwareVersion PROTO ((VOID));
const char  * IssGetFirmwareVersion PROTO ((VOID));
const char  * IssGetSwitchName PROTO ((VOID));
const char  * IssGetSysLocation PROTO ((VOID));
const char  * IssGetSysContact PROTO ((VOID));
int NvRamMacRead (unsigned int u2IfIndex, char *pu1HwAddr);
int NvRamMacWriteBulk (unsigned char *pu1BaseMac);
INT1 IssGetRestoreFlagFromNvRam PROTO ((VOID));
VOID cli_process_iss_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);
INT1 nmhGetIssPortCtrlMode (INT4 i4IssPortCtrlIndex, INT4 *pi4RetValIssPortCtrlMode);

