/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : issnvram.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : ISS System                                     */
/*    MODULE NAME           :                                                */
/*    ISSNGUAGE             : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : ISS Team                                       */
/*    DESCRIPTION           : This file contains nvram related declarations. */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#ifndef _ISSNVRAM_H
#define _ISSNVRAM_H

#define   ISS_MEMCPY              MEMCPY
#define   ISS_STRCPY              STRCPY
#define ISS_TRC_FLAG              0x00000000
#define ISS_MOD_NAME              ((const char*)"ISS")



#define ISS_TRC(TraceType, Str)                                              \
	        MOD_TRC(ISS_TRC_FLAG, TraceType, ISS_MOD_NAME, (const char *)Str)



#define NVRAM_SUCCESS                 0
#define NVRAM_FAILURE                 1
#define NVRAM_FILE_NOT_FOUND          2
#define STR_LEN                       15
#define MAX_COLUMN_LENGTH             120
#define INTERFACE_STR_SIZE            CFA_MAX_PORT_NAME_LENGTH
#define RESTORE_FILE_NAME_LEN         ISS_CONFIG_FILE_NAME_LEN
#define ISS_NPAPI_MODE_NAME_LEN        15

/* This is the default interface alias name */
#ifdef MBSM_WANTED
#define ISS_DEFAULT_INTERFACE_NAME    ((const char *) "eth0")
#else
#define ISS_DEFAULT_INTERFACE_NAME    ((const char *) "Slot0/1")
#endif
#if defined (MBSM_WANTED) && defined (RM_WANTED)
#define ISS_DEFAULT_RM_INTERFACE_NAME    ((const char *) "eth1")
#else
#define ISS_DEFAULT_RM_INTERFACE_NAME    ((const char *) "NONE")
#endif

#define ISS_DEFAULT_IP_ADDRESS        0x0a000001
#define ISS_DEFAULT_RES_IP_ADDRESS    0x0a000002
#define ISS_DEFAULT_RESTORE_FLAG      1
#define ISS_DEFAULT_SAVE_FLAG         1
#define ISS_DEFAULT_RES_FILE_NAME     "iss.conf"
#define ISS_DEFAULT_IP_MASK           0xff000000
#define ISS_DEFAULT_PIM_MODE          2 
#define ISS_DEFAULT_SNOOP_FWD_MODE      ISS_SNOOP_MAC
#define ISS_ATOI                         ATOI
#define ISS_DEFAULT_CLI_SERIAL_CONSOLE  1
/*By Default, ISS comes as Customer Bridge Mode*/
#define ISS_DEFAULT_BRIDGE_MODE       1 
#define ISS_DEFAULT_VLAN_ID           1
#define ISS_DEFAULT_NPAPI_MODE        "Synchronous"

typedef struct {
      unsigned int u4LocalIpAddr;
      unsigned int u4LocalSubnetMask;
      unsigned int u4CfgMode;
      unsigned int u4RestoreOption;
      tIPvXAddr    RestoreIpAddr;
      unsigned int u4PimMode;
      unsigned int u4SnoopFwdMode;
      unsigned int u4MgmtPort; /* Flag set to TRUE if MgmtPort exists */
      unsigned int u4CliSerialConsole; /* TRUE - Serial console enabled
                                        * FALSE - Serial console disabled
                                        */
      unsigned int u4IpAddrAllocProto;
      unsigned int u4BridgeMode;  /*Indicates the bridge comes as
                                   * CB/PB/PEB/PCB*/
      unsigned int u4ColdStandby; /* Indicates the state of cold stand */
      unsigned short int u2DefaultVlan;
      unsigned short u2StackPortCount; /* Number of Stack Ports */
       
      unsigned short int u2SwitchId;
      unsigned short int u2UserPreference;
      unsigned short u2TimeStampMethod;/*Time Stamping method to be used */
      char ai1InterfaceName[INTERFACE_STR_SIZE];
      char ai1RmIfName[INTERFACE_STR_SIZE]; 
      char ai1RestoreFileName[RESTORE_FILE_NAME_LEN];
      char ai1SnmpEngineID[SNMP_MAX_STR_ENGINEID_LEN + 1];
      char au1NpapiMode[ISS_NPAPI_MODE_NAME_LEN+1];
      char i1SaveFlag;   /* If Set to 1 MIB is saved */
      char i1ResFlag;    /* If Set to 1 MIB is restored*/
      tMacAddr au1SwitchMac; /* Base MAC address */
}tNVRAM_DATA;

typedef struct {
    tMacAddr  au1AggregatorMac; /* Aggregator MAC address */
}tNVRAM_AGG_MAC;


int NvRamRead PROTO ((tNVRAM_DATA *));
int NvRamWrite PROTO ((tNVRAM_DATA *));
VOID NvRamReadAggregatorMac PROTO ((VOID));
UINT4 GetNvRamValue PROTO ((UINT1 *, UINT1 *, UINT1 *));

#endif /*_ISSNVRAM_H */
