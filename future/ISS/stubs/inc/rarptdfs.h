/* Definitions added for CFA compilation without IP */


/* Declaration of RARP config table */
typedef struct
{
    UINT1  u1ClientStatus;  /* Status of the RARP client by which it can be */
    UINT1  u1ServerStatus;  /* Status of the RARP server */
    UINT1  u1MaxRetries;    /* The maximum retransmissions of RARP Request */
    UINT1  u1MaxEntries;
    UINT4  u4TimeoutInt;    /* Timeout interval for retransmission of RARP */
} tRarpConfigTable;

tRarpConfigTable   RarpConfigTable;
