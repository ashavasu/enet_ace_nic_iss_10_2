extern INT4 CfaIfmCreateAndInitIfEntry PROTO((UINT2 u2IfIndex, UINT1 u1IfType,
                                       UINT1 *au1PortName));
extern UINT4            gu4IsIvrEnabled;


extern INT4 CfaSetIfActiveStatus (UINT4 u4IfIndex, INT4 i4Status);
extern INT4 CfaGetIfBridgedIfaceStatus PROTO ((UINT4 u4IfIndex, UINT1 *pu1Status));
