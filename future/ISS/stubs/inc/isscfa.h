
/*****************************************************************************/
/* Copyright (C) 2013 Aricent Inc . All Rights Reserved                                  */
/* $Id: isscfa.h,v 1.6 2013/09/14 10:04:11 siva Exp $      */
/*****************************************************************************/
/*    FILE  NAME            : isscfa.h                                          */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : ISS System                                     */
/*    MODULE NAME           :                                                */
/*    ISSNGUAGE             : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*                            saved.                                         */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
#ifndef _ISSCFA_H
#define _ISSCFA_H

INT1 CfaGetIfConfigPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT1 CfaGetIfRouterPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT1 CfaGetIfOpenflowPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT1 CfaGetIvrPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT1 CfaGetLinuxIvrPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT1 CfaGetTnlPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT1 CfaGetPortChannelPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT4 CfaCliGetIfIndex (INT1 *pi1IfName, INT1 *pi1IfNum, UINT4 *pu4IfIndex);

INT4 CfaCliGetOobIfIndex (UINT4 *pu4IfIndex);

UINT4 CfaIsMgmtPort (UINT4 u4IfIndex);

INT4 CfaHandlePktFromIp (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                    UINT4 u4NextHopIpAddr, UINT1 *au1MediaAddr,
                    UINT2 u2Protocol, UINT1 u1PktType, UINT1 u1EncapType);
UINT4 CfaIsLinuxVlanIntf (UINT4 u4IfIndex);

UINT4 CfaIsLoopBackIntf (UINT4 u4IfIndex);

INT4 CfaIpIfGetSrcAddressOnInterface (UINT4 u4CfaIfIndex, UINT4 u4Dest, UINT4 *pu4Src);

INT4 CfaGetIfIpPort (UINT4 u4IfIndex);

INT4 CfaIpIfGetNextSecondaryAddress (UINT4 u4CfaIfIndex, UINT4 u4IpAddress,
                                UINT4 *pu4NextIpAddress, UINT4 *pu4NetMask);

INT4 CfaNotifyIpUp (VOID);

VOID CfaHandleIpForwardingStatusUpdate (UINT1 u1IpForwardingStatus);

INT4 CfaIpIfGetNextIpAddr (UINT4 u4IpAddress, UINT4 *pu4NextIpAddress);

INT4 CfaIpIfGetIpAddressInfo (UINT4 u4IpAddress, UINT4 *pu4NetMask,
                         UINT4 *pu4BcastAddr, UINT4 *pu4CfaIfIndex);

UINT4 CfaGetDefaultRouterIfIndex (VOID);

INT4 CfaCliConfGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName);

INT4 CfaInterfaceStatusChangeIndication (UINT4 u4IfIndex, UINT1 u1OperStatus);

INT4 CfaSendEventToCfaTask (UINT4 u4Event);

UINT4 CfaGetInterfaceNameFromIndex (UINT4 u4Index, UINT1 *pu1Alias);

INT4 CfaIpIfGetIfInfo (UINT4 u4IfIndex, tIpConfigInfo * pIpInfo);

INT4 CfaCliGetIfList (INT1 *pi1IfName, INT1 *pi1IfListStr, UINT1 *pu1IfListArray,
                 UINT4 u4IfListArrayLen);

VOID CfaNotifyProtoToApp (UINT1 u1ModeId, tNotifyProtoToApp NotifyProtoToApp);

INT4 CfaCliGetIndexFromType (UINT4 u4IfType, INT1 *pi1IfNum, UINT4 *pu4IfIndex);

INT4 CfaIfmBringupAllInterfaces (VOID);

INT4 CfaCliGetPoIndex (INT1 *pi1IfName, INT1 *pi1IfNum, UINT4 *pu4IfIndex);

INT4 CfaCliGetVirtualIndex (INT1 *pi1IfName, INT1 *pi1IfNum, UINT4 *pu4IfIndex);

VOID CfaSetPauseMode (INT4 i4IssPortCtrlIndex, INT4 i4PortCtrlMode);

INT4 CfaGetNextActivePort (UINT4 u4Port, UINT4 *pu4NextPort);

UINT4 CfaGetInterfaceIndexFromName (UINT1 *pu1Alias, UINT4 *pu4Index);

INT4 CfaIfGetIpAllocMethod (UINT4 u4IfIndex, UINT1 *pu1IpAllocMethod);

UINT1 CfaIsTunnelInterface (UINT4 u4IfIndex);

INT1 nmhGetIfMainType (INT4 i4IfMainIndex, INT4 *pi4RetValIfMainType);

INT1 nmhGetIfMainMtu (INT4 i4IfMainIndex, INT4 *pi4RetValIfMainMtu);

INT1 nmhGetIfMainEncapType (INT4 i4IfMainIndex, INT4 *pi4RetValIfMainEncapType);

INT1 CfaGetLoopbackPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);
#endif
