
/* $Id: issexglob.h,v 1.1 2013/09/28 11:44:20 siva Exp $            */
#ifndef _ISSEXGLOB_H        
#define _ISSEXGLOB_H        

#ifdef _ISSEXSYS_C
tIssExtGlobalInfo                gIssExGlobalInfo;
tIssRedirectIntfGrpTable         *gpIssRedirectIntfInfo;
#else
extern tIssExtGlobalInfo         gIssExGlobalInfo;
extern tIssRedirectIntfGrpTable         *gpIssRedirectIntfInfo;
/* This is already defined in issglob.h in ISS/common/system */ 
#endif

extern tIssPriorityFilterTable  *gaPriorityTable[ISS_MAX_FILTER_PRIORITY];

#endif
