/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissmdb.h,v 1.1 2013/09/28 11:44:20 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSISSMDB_H
#define _FSISSMDB_H

UINT1 IssMetroL2FilterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 IssMetroL3FilterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 fsissm [] ={1,3,6,1,4,1,2076,81,8,4};
tSNMP_OID_TYPE fsissmOID = {10, fsissm};


UINT4 IssMetroL2FilterOuterEtherType [ ] ={1,3,6,1,4,1,2076,81,8,4,1,1,1,1};
UINT4 IssMetroL2FilterSVlanId [ ] ={1,3,6,1,4,1,2076,81,8,4,1,1,1,2};
UINT4 IssMetroL2FilterSVlanPriority [ ] ={1,3,6,1,4,1,2076,81,8,4,1,1,1,3};
UINT4 IssMetroL2FilterCVlanPriority [ ] ={1,3,6,1,4,1,2076,81,8,4,1,1,1,4};
UINT4 IssMetroL2FilterPacketTagType [ ] ={1,3,6,1,4,1,2076,81,8,4,1,1,1,5};
UINT4 IssMetroL3FilterSVlanId [ ] ={1,3,6,1,4,1,2076,81,8,4,2,1,1,1};
UINT4 IssMetroL3FilterSVlanPriority [ ] ={1,3,6,1,4,1,2076,81,8,4,2,1,1,2};
UINT4 IssMetroL3FilterCVlanId [ ] ={1,3,6,1,4,1,2076,81,8,4,2,1,1,3};
UINT4 IssMetroL3FilterCVlanPriority [ ] ={1,3,6,1,4,1,2076,81,8,4,2,1,1,4};
UINT4 IssMetroL3FilterPacketTagType [ ] ={1,3,6,1,4,1,2076,81,8,4,2,1,1,5};


tMbDbEntry fsissmMibEntry[]= {

{{14,IssMetroL2FilterOuterEtherType}, GetNextIndexIssMetroL2FilterTable, IssMetroL2FilterOuterEtherTypeGet, IssMetroL2FilterOuterEtherTypeSet, IssMetroL2FilterOuterEtherTypeTest, IssMetroL2FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssMetroL2FilterTableINDEX, 1, 0, 0, "0"},

{{14,IssMetroL2FilterSVlanId}, GetNextIndexIssMetroL2FilterTable, IssMetroL2FilterSVlanIdGet, IssMetroL2FilterSVlanIdSet, IssMetroL2FilterSVlanIdTest, IssMetroL2FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssMetroL2FilterTableINDEX, 1, 0, 0, "0"},

{{14,IssMetroL2FilterSVlanPriority}, GetNextIndexIssMetroL2FilterTable, IssMetroL2FilterSVlanPriorityGet, IssMetroL2FilterSVlanPrioritySet, IssMetroL2FilterSVlanPriorityTest, IssMetroL2FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssMetroL2FilterTableINDEX, 1, 0, 0, "-1"},

{{14,IssMetroL2FilterCVlanPriority}, GetNextIndexIssMetroL2FilterTable, IssMetroL2FilterCVlanPriorityGet, IssMetroL2FilterCVlanPrioritySet, IssMetroL2FilterCVlanPriorityTest, IssMetroL2FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssMetroL2FilterTableINDEX, 1, 0, 0, "-1"},

{{14,IssMetroL2FilterPacketTagType}, GetNextIndexIssMetroL2FilterTable, IssMetroL2FilterPacketTagTypeGet, IssMetroL2FilterPacketTagTypeSet, IssMetroL2FilterPacketTagTypeTest, IssMetroL2FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssMetroL2FilterTableINDEX, 1, 0, 0, "1"},

{{14,IssMetroL3FilterSVlanId}, GetNextIndexIssMetroL3FilterTable, IssMetroL3FilterSVlanIdGet, IssMetroL3FilterSVlanIdSet, IssMetroL3FilterSVlanIdTest, IssMetroL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssMetroL3FilterTableINDEX, 1, 0, 0, "0"},

{{14,IssMetroL3FilterSVlanPriority}, GetNextIndexIssMetroL3FilterTable, IssMetroL3FilterSVlanPriorityGet, IssMetroL3FilterSVlanPrioritySet, IssMetroL3FilterSVlanPriorityTest, IssMetroL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssMetroL3FilterTableINDEX, 1, 0, 0, "-1"},

{{14,IssMetroL3FilterCVlanId}, GetNextIndexIssMetroL3FilterTable, IssMetroL3FilterCVlanIdGet, IssMetroL3FilterCVlanIdSet, IssMetroL3FilterCVlanIdTest, IssMetroL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssMetroL3FilterTableINDEX, 1, 0, 0, "0"},

{{14,IssMetroL3FilterCVlanPriority}, GetNextIndexIssMetroL3FilterTable, IssMetroL3FilterCVlanPriorityGet, IssMetroL3FilterCVlanPrioritySet, IssMetroL3FilterCVlanPriorityTest, IssMetroL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssMetroL3FilterTableINDEX, 1, 0, 0, "-1"},

{{14,IssMetroL3FilterPacketTagType}, GetNextIndexIssMetroL3FilterTable, IssMetroL3FilterPacketTagTypeGet, IssMetroL3FilterPacketTagTypeSet, IssMetroL3FilterPacketTagTypeTest, IssMetroL3FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssMetroL3FilterTableINDEX, 1, 0, 0, "1"},
};
tMibData fsissmEntry = { 10, fsissmMibEntry };
#endif /* _FSISSMDB_H */

