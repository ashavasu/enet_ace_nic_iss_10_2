/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: issextdfs.h,v 1.1 2013/09/28 11:44:20 siva Exp $
 *
 * Description: This file include all the structures definition required 
 *              for the operation of the ACL module
 ****************************************************************************/

#ifndef _ISSEXTDFS_H
#define _ISSEXTDFS_H

#include "qosxtd.h"

typedef tTMO_SLL            tIssExSll;

typedef struct IssRateCtrlEntry {

   UINT4                u4IssRateCtrlDLFLimitValue;
   UINT4                u4IssRateCtrlBCASTLimitValue;
   UINT4                u4IssRateCtrlMCASTLimitValue;
   INT4                 i4IssRateCtrlPortLimitRate;
   INT4                 i4IssRateCtrlPortBurstRate;

}tIssRateCtrlEntry;

typedef struct AclRedGlobalIndo {
   tMemPoolId  HwAuditTablePoolId;
    /* Np Sync Buffer Table Pool Id. */
   tTMO_SLL  HwAuditTable;
    /* SLL header of Hardware Audit 
      Np-Sync Buffer Table. */
   UINT1  u1NodeStatus;
    /* Node Status - Idle/Standby/Active */
   UINT1  u1NumOfStandbyNodesUp;
    /* Number of Standby nodes which are 
       booted up.If this value is 0, then dynamic
       messages are not sent by active node*/
   UINT1  u1NpSyncBlockCount;
    /* Counter to Block the NP Sync Message 
       from Active to Standby Node. */
   BOOL1                bBulkReqRcvd;
    /* Boolean to check the Bulk Request received
      Status from the Standby node */
}tAclRedGlobalInfo;

/* Data Structure for NP-Sync Buffer Table */
typedef tNpSyncBufferEntry tAclRedNpSyncEntry;

/* Iss Extension Global Info */
typedef struct { 

   tMemPoolId              IssRateCtrlPoolId;
   tMemPoolId              IssL2FilterPoolId;
   tMemPoolId              IssL3FilterPoolId;
   tMemPoolId              IssUdbTablePoolId;
   tMemPoolId              IssUdbFilterPoolId;
   tMemPoolId              IssFilterShadowPoolId;
   tMemPoolId              AclQueuePoolId; 
   tMemPoolId              IssPriorityFilterPoolId;
   tMemPoolId              IssRedirectIntfGrpPoolId;
   tMemPoolId              IssRedirectPortMaskPoolId;
   tMemPoolId              IssRedirectIfPortGroupPoolId;
   tIssRateCtrlEntry      *apIssRateCtrlEntry[ISS_RATEENTRY_MEMBLK_COUNT];
   tIssExSll               IssL2FilterListHead;
   tIssExSll               IssL3FilterListHead;
   tIssExSll               IssUdbFilterTableHead;
   UINT4                    u4PriorityTableSet;
   tAclRedGlobalInfo    AclRedGlobalInfo;
   tOsixQId     AclQueId;
}tIssExtGlobalInfo;

typedef struct _IssRedirectIntfGrpTable {
   tPortList      PortList;             /* List of ports belonging to this group */
   UINT4          u4RedirectIntfGrpId;  /*  Redirect Interface Group Id; */
   UINT4          u4AclId;              /* Acl Id mapped to this Group */
   UINT2          u2EgressVlanId;       /* Egress VLAN Id  */
   UINT2          u2TrafficDistByte;    /*Virtual trunk traffic distribution byte . */
   UINT1          u1AclIdType;          /* L2_FILTER or L3_FILTER or USERDEFINED_FILTER */
   UINT1          u1PortListType;       /*ISS_REDIRECT_TO_PORT if redirect interface is single port
                                          ISS_REDIRECT_TO_PORTLIST if redirect interface is port list*/ 
   UINT1          u1RowStatus;
   UINT1          u1UdbPosition;    /*Position of UserDefined Byte*/ 
   UINT1          u1PriorityFlag;
   UINT1          u1Pad[3];
}tIssRedirectIntfGrpTable;

typedef struct _IssRedirectPortMaskAndIndex
{
   UINT4 u4PortIndex;
   UINT1 u1PortMaskMsb;
   UINT1 u1PortMaskLsb;
   UINT1 u1PortValueMsb;
   UINT1 u1PortValueLsb;

}tIssRedirectPortMaskAndIndex;


typedef struct _IssRedirectIfPortGrp
{
    UINT4 u4IfIndex;              /* CFA If Index */
    UINT1 u1OperStatus;           /* UP/DOWN */
    UINT1 u1Pad[3];

}tIssRedirectIfPortGroup;

/* Validity Value */
typedef enum {

   ISS_OK = 1,
   ISS_NOTOK

}tValid;
typedef struct AclRmMsg {
   tRmMsg  *pData;
   /* Pointer to received message info from RM */
   UINT2  u2DataLen;
   /* Length of the message received from RM */
   UINT1  u1Event;
   /* Event received from RM */
   UINT1  u1Reserved;
   /* Padding bytes */
}tAclRmMsg;

typedef struct AclQMsg {
   UINT4  u4MsgType;
   /* Message Type of the Queue  */
   tAclRmMsg  RmMsg;
   /*Structure to store the RM Message and 
       Events from the RM module */
}tAclQMsg;
#endif
