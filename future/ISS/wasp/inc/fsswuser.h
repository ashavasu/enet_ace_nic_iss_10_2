/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsswuser.h,v 1.1 2016/12/17 09:54:12 siva Exp $
*
* Description: AP Firware upgrade dependent structure definitions
*********************************************************************/
#ifndef SW_UPGRADE_USER
#define SW_UPGRADE_USER

#ifdef QCAX_WANTED
#include <mtd/mtd-user.h>

typedef struct {
unsigned char BF_MAGIC;
unsigned char actPart;
} bootFlagInfo;

typedef enum {
     FLASH_PARTITION_NO_PRIMARY        = 1,
     FLASH_PARTITION_NO_SECONDARY      = 2
}flash_partition_number_et;

typedef enum {
APIRET_FAIL = -1,
APIRET_SUCCESS
} inf_return_et;

extern inf_return_et  upgd_sw_image (
        flash_partition_number_et   standby_partition_no,
        unsigned char*   upgd_pnames );

extern inf_return_et mtd_dev_open(unsigned char dev_num, unsigned char* mtd_fd);
extern inf_return_et mtd_dev_close(unsigned char mtd_fd);
extern inf_return_et mtd_dev_bflag_rw(unsigned char option,unsigned char dev_to, bootFlagInfo *bflag_data);
extern inf_return_et mtd_dev_erase(unsigned char dev_to, struct erase_info_user *erase);
extern inf_return_et read_active_boot_partition(flash_partition_number_et *part_num);
extern inf_return_et switch_active_partition(flash_partition_number_et *part_num);
#endif
#endif
