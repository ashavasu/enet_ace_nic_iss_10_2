/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsswkern.h,v 1.1 2016/12/17 09:54:12 siva Exp $
*
* Description: AP Firware upgrade dependent MACROS
*********************************************************************/
#ifndef SW_UPGRADE_KERNEL
#define SW_UPGRADE_KERNEL
 
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#define NAND_PAGE_SIZE 0x800
#define AXM_MULTI_IMAGE_SIZE 0x3800000
#define AXM_MULTI_PRIMARY_PART   0
#define AXM_MULTI_SECONDARY_PART 1

#define MTD_DEV_NAME_SIZE 20
#define BOOT_FLAG_PARTITION 3
#define BOOTFLAG_READ  1
#define BOOTFLAG_WRITE 2
#endif

