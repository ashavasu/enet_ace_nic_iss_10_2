/*******************************************************************************
 * Copyright (C) 2014 Aricent Inc . All Rights Reserved
 *
 * $Id: fsswupgr.c,v 1.1 2016/12/17 09:54:13 siva Exp $
 *
 * Description: The upgrade related functions are present in this file
 ******************************************************************************/
#ifndef SWUPG_C
#define SWUPG_C
#include "fsswkern.h"
#include "fsswuser.h"
#include "lr.h"

#ifdef QCAX_WANTED
/******************************************************************************/
/*      Function Name   :      mtd_dev_open                                   */
/*                                                                            */
/*      Description     :      This function opens MTD                        */
/*                                                                            */
/*      Input(s)        :      dev_num - Device Number                        */
/*                                                                            */
/*      Output(s)       :      mtd_fd - File Descriptor                       */
/*                                                                            */
/*      Returns         :      APIRET_SUCCESS/APIRET_FAILURE                  */
/******************************************************************************/

inf_return_et mtd_dev_open(unsigned char dev_num, unsigned char* mtd_fd)
{
    unsigned char dev_name[MTD_DEV_NAME_SIZE];

    sprintf(dev_name, "/dev/mtd%d", dev_num);
    printf("Opening device:%s\n",dev_name);

    /* opening the MTD device file */
    *mtd_fd = open(dev_name, O_RDWR);

    /* error check for open system call */
    if (*mtd_fd < 0 ) {
        printf("%s, MTD dev open failed, dev=%s ret=%d\n",
                                __func__,dev_name, *mtd_fd);
        return APIRET_FAIL;
    }

   return APIRET_SUCCESS;
}


/******************************************************************************/
/*      Function Name   :      mtd_dev_close                                  */
/*                                                                            */
/*      Description     :      This function close the MTD                    */
/*                                                                            */
/*      Input(s)        :      mtd_fd - File Descriptor                       */
/*                                                                            */
/*      Output(s)       :      None                                           */
/*                                                                            */
/*      Returns         :      APIRET_SUCCESS/APIRET_FAILURE                  */
/******************************************************************************/


inf_return_et mtd_dev_close(unsigned char mtd_fd)
{
    int ret = 0;

    /* closing MTD device file */
    ret = close(mtd_fd);
    if (ret) {
        printf("%s, MTD dev close failed, ret=%d\n", __func__, ret);
        return APIRET_FAIL;
    }

    return APIRET_SUCCESS;
}


/******************************************************************************/
/*      Function Name   :      mtd_dev_bflag_rw                               */
/*                                                                            */
/*      Description     :      This function wirte the boot  flag            */
/*                                                                            */
/*      Input(s)        :      option - Option                               */
/*                             dev_to - Dev To                                */
/*                                                                            */
/*      Output(s)       :      bflag_data - BootFlag data                     */
/*                                                                            */
/*      Returns         :      APIRET_SUCCESS/APIRET_FAILURE                  */
/******************************************************************************/


inf_return_et mtd_dev_bflag_rw(unsigned char option,unsigned char dev_to, bootFlagInfo *bflag_data)
{
        int read_size = 0, write_size = 0;

        if (BOOTFLAG_WRITE == option) {
                lseek(dev_to, 0x0, SEEK_SET);
                write_size = write(dev_to, bflag_data, sizeof(bootFlagInfo));
                if (write_size == 0) {
                        printf("%s,ERR: MTD Write Failed\n",__func__);
                        mtd_dev_close(dev_to);
                        return APIRET_FAIL;
                }
        } else if (BOOTFLAG_READ == option) {
                lseek(dev_to, 0x0, SEEK_SET);
                read_size = read(dev_to, bflag_data, sizeof(bootFlagInfo));
                if (read_size == 0) {
                        printf("%s,ERR: MTD Read Failed\n",__func__);
                        mtd_dev_close(dev_to);
                        return APIRET_FAIL;
                }
        }
        return APIRET_SUCCESS;
}

/************************************************************************
   * * FUNCTION NAME: bsp_bf_mtd_dev_erase
   * * DESCRIPTION  : This API does memory erase
   *
   * * INPUT (s)    :
   * * OUTPUT (s)   :
   * * GLOBAL VARIABLE REFERRED :
   * * GLOBAL VARIABLE MODIFIED :
   * * RETURN       : CPE_IRET_SUCCESS/CPE_IRET_FAILURE
   * ARICENT
*******************************************************************************/
inf_return_et mtd_dev_erase(unsigned char dev_to, struct erase_info_user *erase)
{
        unsigned char ret = 0;

        ret = ioctl(dev_to, MEMERASE, erase);
        if (ret < 0) {
                printf("%s,ERR: MTD ERASE IOCTL Failed\n",__func__);
                mtd_dev_close(dev_to);
                return APIRET_FAIL;
        }
        return APIRET_SUCCESS;
}

/************************************************************************
   * * FUNCTION NAME: upgd_sw_image
   * * DESCRIPTION  : This API does software upgrade of multi-image
   *
   * * INPUT (s)    : partition number [1 - primary, 2-secondary],
			Multi-File Absolute Path Name [/tmp/multi.img]
   * * OUTPUT (s)   :
   * * GLOBAL VARIABLE REFERRED :
   * * GLOBAL VARIABLE MODIFIED :
   * * RETURN       : CPE_IRET_SUCCESS/CPE_IRET_FAILURE
   * ARICENT
*******************************************************************************/

inf_return_et  upgd_sw_image (
        flash_partition_number_et   standby_partition_no,
        unsigned char  *multi_file_path_name)
{
  	unsigned char mtd_part_number = 0;
  	int ret;
  	unsigned char image_fd,dev_fd;
 	unsigned char buf[NAND_PAGE_SIZE];
  	unsigned long image_len;
  	unsigned int num_pages,count;
  	struct erase_info_user erase;
  	int read_size,write_size;

  	if (standby_partition_no != FLASH_PARTITION_NO_PRIMARY && 
		standby_partition_no != FLASH_PARTITION_NO_SECONDARY)
  	{
        	printf ("%s,Invalid partition number:%d\n",
			__func__,standby_partition_no);
        	goto error;
  	}

	if (standby_partition_no == FLASH_PARTITION_NO_PRIMARY) {
		mtd_part_number = AXM_MULTI_PRIMARY_PART;
	}else {
		mtd_part_number = AXM_MULTI_SECONDARY_PART;
	}

	ret = access (multi_file_path_name, F_OK);
        if (ret != 0) {
                printf ("%s,Fail to access image at %s\n",__func__,
			multi_file_path_name);
                goto error;
        }

	image_fd = open(multi_file_path_name,O_RDWR);
	if (image_fd < 0)
	{
		printf ("%s,Fail to open image file path: %s\n",__func__,
			multi_file_path_name);
		goto error;
	}

	ret = mtd_dev_open (mtd_part_number, &dev_fd);
	if(ret){
            printf ("%s,MTD:%d Device Open failed\n",__func__,mtd_part_number);
	    close (image_fd);
	    goto error;
        }


	lseek (dev_fd,0,SEEK_SET);

	image_len = lseek (image_fd,0,SEEK_END);
	lseek (image_fd,0,SEEK_SET);

	 if (image_len % NAND_PAGE_SIZE == 0)
		num_pages = (image_len / NAND_PAGE_SIZE);
	 else
		num_pages = (image_len / NAND_PAGE_SIZE) + 1;

	memset(&erase, 0, sizeof(erase));
	erase.start = 0x0;
	erase.length = AXM_MULTI_IMAGE_SIZE; // AXM Multi Image size

	ret = ioctl (dev_fd, MEMERASE,&erase);
	if (ret < 0){
		printf ("%s,Failed to erase partition /dev/mtd%d\n",
			__func__,mtd_part_number);
		mtd_dev_close(dev_fd);
		close (image_fd);
		goto error;
	}

	printf ("\n%s Boot Partition:%d Erased..\n",
		(standby_partition_no==1)?"Primary":"Secondary",mtd_part_number);
	
	for (count = 0; count < num_pages; count ++)
	{	
		/* Read image to Buffer */
		read_size = read (image_fd,buf,NAND_PAGE_SIZE);
		if (read_size < 0) {
			printf ("%s,Failed to read image file %s\n",__func__,
			multi_file_path_name);
			mtd_dev_close(dev_fd);
			close (image_fd);
			goto error;
		}

		/* Write the Buffer to Partition */
		write_size = write (dev_fd,buf,NAND_PAGE_SIZE);
		if (write_size < 0){
			printf ("%s,Failed to write the page %d in NAND\n",
			__func__,count);
			mtd_dev_close(dev_fd);
			close (image_fd);
		    goto error;
		}
  	}

	ret = close (image_fd);
	if (ret < 0)
	{
		printf ("%s,Fail to close the image file: %s\n",__func__,
			multi_file_path_name);
		mtd_dev_close(dev_fd);
		goto error;
	}
	ret = mtd_dev_close(dev_fd);
	if (ret < 0)
	{
		printf ("%s,Fail to close %s partition:%d file\n",__func__,
			(standby_partition_no==1)?"Primary":"Secondary",mtd_part_number);
		goto error;
	}
	printf ("\nProgramming image %s in %s Partition Done..\n\n",multi_file_path_name,
		(standby_partition_no==1)?"Primary":"Secondary");
	return APIRET_SUCCESS;
error:
	return APIRET_FAIL;

}

/******************************************************************************/
/*      Function Name   :      read_active_boot_partition                     */
/*                                                                            */
/*      Description     :      This function read the boot partition          */
/*                                                                            */
/*      Input(s)        :      None                                           */
/*                                                                            */
/*      Output(s)       :      part_num - Partition Number                    */
/*                                                                            */
/*      Returns         :      APIRET_SUCCESS/APIRET_FAILURE                  */
/******************************************************************************/

inf_return_et read_active_boot_partition(flash_partition_number_et *part_num)
{
        unsigned char dev_to, dev_num, rc;
        bootFlagInfo mtd_bfrd;

        dev_num = BOOT_FLAG_PARTITION;

        rc=mtd_dev_open(dev_num, &dev_to);
        if(rc){
	    printf ("%s,MTD device open failed\n",__func__);
            return APIRET_FAIL;
	}

        if( 0>mtd_dev_bflag_rw(1,dev_to,&mtd_bfrd) ){
                printf("%s,Fail to read active boot partition\n",__func__);
            	return APIRET_FAIL;

        }
        *part_num = ((mtd_bfrd.actPart==0xbb)?2:1);

        mtd_dev_close(dev_to);

        return APIRET_SUCCESS;
}

/******************************************************************************/
/*      Function Name   :      read_active_boot_partition                     */
/*                                                                            */
/*      Description     :      This function switch the active partition      */
/*                                                                            */
/*      Input(s)        :      None                                           */
/*                                                                            */
/*      Output(s)       :      part_num - Partition Number                    */
/*                                                                            */
/*      Returns         :      APIRET_SUCCESS/APIRET_FAILURE                  */
/******************************************************************************/

inf_return_et switch_active_partition(flash_partition_number_et *part_num)
{
        unsigned char dev_to, dev_num,rc;
        bootFlagInfo mtd_bfsp;
        dev_num = BOOT_FLAG_PARTITION;

        struct erase_info_user erase;
        erase.start = 0x0;
        erase.length = 65536;

        rc=mtd_dev_open(dev_num, &dev_to);
        if(rc){
            printf ("%s,MTD Device Open failed\n",__func__);
            return APIRET_FAIL;
        }

        if( 0>mtd_dev_bflag_rw(1,dev_to,&mtd_bfsp) ){
                printf("%s,ERR: MTD Read Failed\n",__func__);
                return APIRET_FAIL;
        }
        if (mtd_dev_erase(dev_to,&erase) < 0) {
                printf("%s,ERR: MTD Erase Failed\n",__func__);
                return APIRET_FAIL;
        }

        mtd_bfsp.actPart=((mtd_bfsp.actPart==0xaa)?0xbb:0xaa);
        *part_num=((mtd_bfsp.actPart==0xaa)?1:2);

        if( 0>mtd_dev_bflag_rw(2,dev_to,&mtd_bfsp) ){
                printf("%s,ERR: MTD Write Failed\n",__func__);
                return APIRET_FAIL;
        }
        mtd_dev_close(dev_to);

        return APIRET_SUCCESS;
}
#endif

/*****************************************************************************/
/* Function Name      : IssCustFirmwareUpgrade                              */
/*                                                                           */
/* Description        : This function performs firmware upgradation          */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pu1DstImageName - destination image name             */
/*                      pu1CustFilePath - destination file path              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
/*****************************************************************************/

INT4
IssCustFirmwareUpgrade(CONST UINT1 *pu1SrcImagePath,UINT1 *pu1SrcImage)
{
#ifdef QCAX_WANTED

    if ( strncasecmp( pu1SrcImage ,"fallback",8) == 0)
    {
        if (upgd_sw_image (2,pu1SrcImagePath) < 0)
        {
            printf ("\n[ERROR] Fallback SW upgrade Failed !!!\n");
            return  0;
        }
    }
    else
    {
        if (upgd_sw_image (1,pu1SrcImagePath) < 0)
        {
            printf ("\n[ERROR] Normal SW upgrade Failed !!!\n");
            return 0;
        }
    }
    printf ("\n SW Upgrade SUCCESS for %s",pu1SrcImage);
#else
    UNUSED_PARAM (pu1SrcImagePath);
    UNUSED_PARAM (pu1SrcImage);
#endif
    return 1;
}

#endif
