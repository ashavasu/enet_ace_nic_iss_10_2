/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: issexmbsm.c,v 1.1 2013/09/28 11:44:21 siva Exp $
 *
 * Description: 
 *
 *******************************************************************/

#ifndef _ISSEXMBSM_C
#define _ISSEXMBSM_C

#include "issexinc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : IssMbsmL2FilterCardInsert                                  */
/*                                                                           */
/* Description  : This function updates the L2 Filter HW configuration when  */
/*                a card is inserted into the MBSM System.                   */
/*                                                                           */
/* Input        : pPortInfo  - Pointer to Port Related Info.                 */
/*              : pSlotInfo  - Pointer to Slot Related Info.                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
IssMbsmL2FilterCardInsert (tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo)
{

    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;
    tIssL2FilterEntry   tempIssL2FilterEntry;
    tIssPortList        zeroAddr;
    INT4                i4RetVal;

    if (pPortInfo->u4PortCount == 0)
    {
        return MBSM_SUCCESS;
    }

    ISS_MEMSET (zeroAddr, 0, ISS_PORT_LIST_SIZE);

    /* Scan Global L2 Filter List */
    TMO_SLL_Scan (&ISS_L2FILTER_LIST, pIssL2FilterEntry, tIssL2FilterEntry *)
    {

        if (pIssL2FilterEntry->u1IssL2FilterStatus != ISS_ACTIVE)
        {
            continue;
        }

        ISS_MEMCPY (&tempIssL2FilterEntry, pIssL2FilterEntry,
                    sizeof (tIssL2FilterEntry));

        if (!ISS_MEMCMP (pIssL2FilterEntry->IssL2FilterInPortList, zeroAddr,
                         ISS_PORT_LIST_SIZE))
        {

            /* This is a glabal filter applicable on all the ports */
            ISS_MEMCPY (&tempIssL2FilterEntry.IssL2FilterInPortList,
                        MBSM_PORT_INFO_PORTLIST (pPortInfo),
                        ISS_PORT_LIST_SIZE);

            /* Call the hardware API to Add the filter */
            i4RetVal =
                IssMbsmHwUpdateL2Filter (&tempIssL2FilterEntry,
                                         ISS_L2FILTER_ADD, pSlotInfo);

            if (i4RetVal != FNP_SUCCESS)
            {
                return MBSM_FAILURE;
            }

        }
        else
        {

            if (MBSM_SLOT_INFO_ISPRECONF (pSlotInfo) == MBSM_FALSE)
            {
                continue;
            }

            ISS_AND_PORT_LIST (tempIssL2FilterEntry.IssL2FilterInPortList,
                               MBSM_PORT_INFO_PORTLIST (pPortInfo));

            if (ISS_MEMCMP
                (&tempIssL2FilterEntry.IssL2FilterInPortList, zeroAddr,
                 ISS_PORT_LIST_SIZE))
            {
                /* Call the hardware API to Add the filter */
                i4RetVal =
                    IssMbsmHwUpdateL2Filter (&tempIssL2FilterEntry,
                                             ISS_L2FILTER_ADD, pSlotInfo);

                if (i4RetVal != FNP_SUCCESS)
                {
                    return MBSM_FAILURE;
                }
            }
        }

    }

    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IssMbsmL2FilterCardRemove                                  */
/*                                                                           */
/* Description  : This function updates the L2 Filter HW configuration when  */
/*                a card is removed into the MBSM System.                    */
/*                                                                           */
/* Input        : pPortInfo  - Pointer to Port Related Info.                 */
/*              : pSlotInfo  - Pointer to Slot Related Info.                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
IssMbsmL2FilterCardRemove (tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo)
{
    /* Filter deletion for L2 filters is not needed when Card Removal event
     * is received. Since we are maintaining a global L2 filter list for every 
     * L2 filter handle (via BCMX), no need to clean-up the filter list when a 
     * line card is removed. Just ignore the card removal event for this case.
     */

    UNUSED_PARAM (pPortInfo);
    UNUSED_PARAM (pSlotInfo);
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IssMbsmL3FilterCardInsert                                  */
/*                                                                           */
/* Description  : This function updates the L3 Filter HW configuration when  */
/*                a card is inserted into the MBSM System.                   */
/*                                                                           */
/* Input        : pPortInfo  - Pointer to Port Related Info.                 */
/*              : pSlotInfo  - Pointer to Slot Related Info.                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
IssMbsmL3FilterCardInsert (tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo)
{

    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    tIssL3FilterEntry   tempIssL3FilterEntry;
    tIssPortList        zeroAddr;
    INT4                i4RetVal;

    if (pPortInfo->u4PortCount == 0)
    {
        return MBSM_SUCCESS;
    }

    ISS_MEMSET (zeroAddr, 0, ISS_PORT_LIST_SIZE);

    /* Scan Global L3 Filter List */
    TMO_SLL_Scan (&ISS_L3FILTER_LIST, pIssL3FilterEntry, tIssL3FilterEntry *)
    {

        if (pIssL3FilterEntry->u1IssL3FilterStatus != ISS_ACTIVE)
        {
            continue;
        }

        ISS_MEMCPY (&tempIssL3FilterEntry, pIssL3FilterEntry,
                    sizeof (tIssL3FilterEntry));

        if (((!ISS_MEMCMP (pIssL3FilterEntry->IssL3FilterInPortList, zeroAddr,
                           ISS_PORT_LIST_SIZE)) &&
             (pIssL3FilterEntry->IssL3FilterDirection == ISS_DIRECTION_IN)) ||
            ((!ISS_MEMCMP (pIssL3FilterEntry->IssL3FilterOutPortList, zeroAddr,
                           ISS_PORT_LIST_SIZE)) &&
             (pIssL3FilterEntry->IssL3FilterDirection == ISS_DIRECTION_OUT)))
        {

            /* This is a glabal filter applicable on all the ports */
            if (pIssL3FilterEntry->IssL3FilterDirection == ISS_DIRECTION_IN)
            {
                ISS_MEMCPY (&tempIssL3FilterEntry.IssL3FilterInPortList,
                            MBSM_PORT_INFO_PORTLIST (pPortInfo),
                            ISS_PORT_LIST_SIZE);
            }
            else
            {

                ISS_MEMCPY (&tempIssL3FilterEntry.IssL3FilterOutPortList,
                            MBSM_PORT_INFO_PORTLIST (pPortInfo),
                            ISS_PORT_LIST_SIZE);
            }

            /* Call the hardware API to Add the filter */
            i4RetVal =
                IssMbsmHwUpdateL3Filter (&tempIssL3FilterEntry,
                                         ISS_L3FILTER_ADD, pSlotInfo);

            if (i4RetVal != FNP_SUCCESS)
            {
                return MBSM_FAILURE;
            }

        }
        else
        {

            if (MBSM_SLOT_INFO_ISPRECONF (pSlotInfo) == MBSM_FALSE)
            {
                continue;
            }

            /* This is not a glabal filter */
            if (pIssL3FilterEntry->IssL3FilterDirection == ISS_DIRECTION_IN)
            {
                ISS_AND_PORT_LIST (tempIssL3FilterEntry.
                                   IssL3FilterInPortList,
                                   MBSM_PORT_INFO_PORTLIST (pPortInfo));
            }
            else
            {
                ISS_AND_PORT_LIST (tempIssL3FilterEntry.
                                   IssL3FilterOutPortList,
                                   MBSM_PORT_INFO_PORTLIST (pPortInfo));

            }

            if (ISS_MEMCMP
                (&tempIssL3FilterEntry.IssL3FilterInPortList, zeroAddr,
                 ISS_PORT_LIST_SIZE))
            {

                /* Call the hardware API to Add the filter */
                i4RetVal =
                    IssMbsmHwUpdateL3Filter (&tempIssL3FilterEntry,
                                             ISS_L3FILTER_ADD, pSlotInfo);

                if (i4RetVal != FNP_SUCCESS)
                {
                    return MBSM_FAILURE;
                }
            }
        }

    }

    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IssMbsmL3FilterCardRemove                                  */
/*                                                                           */
/* Description  : This function updates the L3 Filter HW configuration when  */
/*                a card is removed into the MBSM System.                    */
/*                                                                           */
/* Input        : pPortInfo  - Pointer to Port Related Info.                 */
/*              : pSlotInfo  - Pointer to Slot Related Info.                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
IssMbsmL3FilterCardRemove (tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo)
{
    /* Filter deletion for L3 filters is not needed when Card Removal event
     * is received. Since we are maintaining a global L3 filter list for every 
     * L3 filter handle (via BCMX), no need to clean-up the filter list when a 
     * line card is removed. Just ignore the card removal event for this case.
     */

    UNUSED_PARAM (pPortInfo);
    UNUSED_PARAM (pSlotInfo);
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IssMbsmCardInsertRateCtrlTable                             */
/*                                                                           */
/* Description  : This function updates the port related HW configuration    */
/*                for Rate Control Table when                                */
/*                a card is inserted into the MBSM System.                   */
/*                                                                           */
/* Input        : pPortInfo  - Pointer to Port Related Info.                 */
/*              : pSlotInfo  - Pointer to Slot Related Info.                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
IssMbsmCardInsertRateCtrlTable (tMbsmPortInfo * pPortInfo,
                                tMbsmSlotInfo * pSlotInfo)
{

    tIssRateCtrlEntry  *pIssRateCtrlEntry = NULL;
    INT4                i4RateCtrlIndex = 0;
    UINT4               u4StartPortIndex = 0;
    UINT4               u4EndPortIndex = 0;
    INT4                i4RetVal;
    UINT1               u1IsSetInPortList = OSIX_FALSE;
    UINT1               u1IsSetInPortListStatus = OSIX_FALSE;

    if ((pSlotInfo->i1IsPreConfigured == MBSM_FALSE) ||
        (pPortInfo->u4PortCount == 0))
    {
        return MBSM_SUCCESS;
    }

    u4StartPortIndex = MBSM_PORT_INFO_STARTINDEX (pPortInfo);
    u4EndPortIndex = MBSM_PORT_INFO_STARTINDEX (pPortInfo) +
        MBSM_PORT_INFO_PORTCOUNT (pPortInfo);
    i4RateCtrlIndex = u4StartPortIndex;

    while (u4StartPortIndex != u4EndPortIndex)
    {
        OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST (pPortInfo),
                                 i4RateCtrlIndex, sizeof (tPortList),
                                 u1IsSetInPortList);
        OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST_STATUS (pPortInfo),
                                 i4RateCtrlIndex, sizeof (tPortList),
                                 u1IsSetInPortListStatus);
        if ((u1IsSetInPortList == OSIX_TRUE)
            && (OSIX_FALSE == u1IsSetInPortListStatus))
        {

            pIssRateCtrlEntry = gIssExGlobalInfo.
                apIssRateCtrlEntry[i4RateCtrlIndex];

            if (pIssRateCtrlEntry != NULL)
            {

                if (pIssRateCtrlEntry->u4IssRateCtrlDLFLimitValue != 0)
                {
                    /* Port specific NP calls will call only BCMX call with IfIndex 
                     * as an argument. The BCMX call will call bcm call only for that
                     * specific LPort. Hence no need to call NP call in npx.c file
                     */

                    i4RetVal =
                        IssHwSetRateLimitingValue ((UINT4) i4RateCtrlIndex,
                                                   ISS_RATE_DLF,
                                                   pIssRateCtrlEntry->
                                                   u4IssRateCtrlDLFLimitValue);

                    if (i4RetVal != FNP_SUCCESS)
                    {
                        return MBSM_FAILURE;
                    }
                }

                if (pIssRateCtrlEntry->u4IssRateCtrlBCASTLimitValue != 0)
                {
                    /* Port specific NP calls will call only BCMX call with IfIndex 
                     * as an argument. The BCMX call will call bcm call only for that
                     * specific LPort. Hence no need to call NP call in npx.c file
                     */

                    i4RetVal =
                        IssHwSetRateLimitingValue ((UINT4) i4RateCtrlIndex,
                                                   ISS_RATE_BCAST,
                                                   pIssRateCtrlEntry->
                                                   u4IssRateCtrlBCASTLimitValue);

                    if (i4RetVal != FNP_SUCCESS)
                    {
                        return MBSM_FAILURE;
                    }

                }

                if (pIssRateCtrlEntry->u4IssRateCtrlMCASTLimitValue != 0)
                {
                    /* Port specific NP calls will call only BCMX call with IfIndex 
                     * as an argument. The BCMX call will call bcm call only for that
                     * specific LPort. Hence no need to call NP call in npx.c file
                     */

                    i4RetVal =
                        IssHwSetRateLimitingValue ((UINT4) i4RateCtrlIndex,
                                                   ISS_RATE_MCAST,
                                                   pIssRateCtrlEntry->
                                                   u4IssRateCtrlMCASTLimitValue);

                    if (i4RetVal != FNP_SUCCESS)
                    {
                        return MBSM_FAILURE;
                    }

                }

            }

        }
        u4StartPortIndex++;
        i4RateCtrlIndex++;
    }

    return MBSM_SUCCESS;
}

#endif /* _ISSEXMBSM_C */
