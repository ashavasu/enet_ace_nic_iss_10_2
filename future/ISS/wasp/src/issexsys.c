/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: issexsys.c,v 1.4 2016/06/14 12:31:21 siva Exp $
 *
 * Description: This file contains the ACL module initialisation routines
 *****************************************************************************/

#ifndef _ISSEXSYS_C
#define _ISSEXSYS_C

#include "issexinc.h"
#include "fsissewr.h"
#include "fsissawr.h"
#include "fsissalw.h"
#include "fsissmwr.h"
extern double       UtilPow (double, double);
/* Port list bit mask list */
UINT1               gau1IssPortBitMask[ISS_REDIRECT_PORTS_PER_BYTE] =
    { 0x01, 0x80, 0x40, 0x20, 0x10,
    0x08, 0x04, 0x02
};

tIssPriorityFilterTable *gaPriorityTable[ISS_MAX_FILTER_PRIORITY];
#ifdef NPAPI_WANTED
static UINT1        gIssNullPortList[] = { 0 };
#endif
/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssExInit                                        */
/*                                                                          */
/*    Description        : This function initialises the memory pools used  */
/*                         by the ISS Extension Module.                     */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/ISS_FAILURE                          */
/****************************************************************************/
INT4
IssExInit ()
{
    UINT4               u4Priority = 0;

    ISS_CREATE_PRIORITY_FILTER_TABLE_MEM_POOL
        (ISS_PRIORITY_FILTER_TABLE_MEMBLK_SIZE,
         ISS_PRIORITY_FILTER_TABLE_MEMBLK_COUNT,
         &(ISS_PRIORITY_FILTER_POOL_ID));

    for (u4Priority = 0; u4Priority < ISS_MAX_FILTER_PRIORITY; u4Priority++)
    {
        ISS_PRIORITY_FILTER_ALLOC_MEM_BLOCK (gaPriorityTable[u4Priority]);

        TMO_DLL_Init (&(gaPriorityTable[u4Priority]->PriFilterList));
    }

    gIssExGlobalInfo.u4PriorityTableSet = ISS_FALSE;

    /* Allocate memory pool for Rate ctrl table */
    if (ISS_CREATE_RATEENTRY_MEM_POOL (ISS_RATEENTRY_MEMBLK_SIZE,
                                       ISS_RATEENTRY_MEMBLK_COUNT,
                                       &(ISS_RATEENTRY_POOL_ID)) != MEM_SUCCESS)
    {
        ISS_DELETE_PRIORITY_FILTER_TABLE_MEM_POOL (ISS_PRIORITY_FILTER_POOL_ID);
        ISS_TRC (INIT_SHUT_TRC, "Rate Entry Memory Pool Creation FAILED\n");
        return ISS_FAILURE;
    }

    /* Call the Hardware routine to initialise filters in hardware */

    /* Allocate memory pool for L2 Filter table */
    if (ISS_CREATE_L2FILTERENTRY_MEM_POOL (ISS_L2FILTERENTRY_MEMBLK_SIZE,
                                           ISS_L2FILTERENTRY_MEMBLK_COUNT,
                                           &(ISS_L2FILTERENTRY_POOL_ID))
        != MEM_SUCCESS)
    {
        ISS_DELETE_RATEENTRY_MEM_POOL (ISS_RATEENTRY_POOL_ID);
        ISS_DELETE_PRIORITY_FILTER_TABLE_MEM_POOL (ISS_PRIORITY_FILTER_POOL_ID);
        ISS_TRC (INIT_SHUT_TRC,
                 "L2 Filter Entry Memory Pool Creation FAILED\n");
        return ISS_FAILURE;
    }

    /* Initialise the SLL lists for L2 Filter Table */
    ISS_SLL_INIT (&(ISS_L2FILTER_LIST));

    /* Allocate memory pool for L3 Filter table */
    if (ISS_CREATE_L3FILTERENTRY_MEM_POOL (ISS_L3FILTERENTRY_MEMBLK_SIZE,
                                           ISS_L3FILTERENTRY_MEMBLK_COUNT,
                                           &(ISS_L3FILTERENTRY_POOL_ID))
        != MEM_SUCCESS)
    {
        ISS_DELETE_RATEENTRY_MEM_POOL (ISS_RATEENTRY_POOL_ID);
        ISS_DELETE_PRIORITY_FILTER_TABLE_MEM_POOL (ISS_PRIORITY_FILTER_POOL_ID);
        ISS_DELETE_L2FILTERENTRY_MEM_POOL (ISS_L2FILTERENTRY_POOL_ID);
        ISS_TRC (INIT_SHUT_TRC,
                 "L3 Filter Entry Memory Pool Creation FAILED\n");
        return ISS_FAILURE;
    }

    if (ISS_CREATE_ACL_MSG_QUEUE_MEM_POOL (sizeof (tAclQMsg),
                                           ISS_QUEUE_SIZE,
                                           &(ISS_ACL_MSG_QUEUE_POOL_ID))
        != MEM_SUCCESS)
    {
        ISS_TRC (INIT_SHUT_TRC,
                 "ACL Message queue  Memory Pool Creation FAILED\n");

        ISS_DELETE_RATEENTRY_MEM_POOL (ISS_RATEENTRY_POOL_ID);
        ISS_DELETE_PRIORITY_FILTER_TABLE_MEM_POOL (ISS_PRIORITY_FILTER_POOL_ID);
        ISS_DELETE_L2FILTERENTRY_MEM_POOL (ISS_L2FILTERENTRY_POOL_ID);
        return ISS_FAILURE;
    }

    if (ISS_CREATE_FILTER_SHADOW_MEM_POOL (sizeof (tIssFilterShadowTable),
                                           ISS_FILTER_SHADOW_POOL_SIZE,
                                           &(ISS_FILTER_SHADOW_POOL_ID))
        != MEM_SUCCESS)
    {
        ISS_TRC (INIT_SHUT_TRC,
                 "Filter Shadow Table Memory Pool Creation FAILED\n");
        ISS_DELETE_RATEENTRY_MEM_POOL (ISS_RATEENTRY_POOL_ID);
        ISS_DELETE_L2FILTERENTRY_MEM_POOL (ISS_L2FILTERENTRY_POOL_ID);
        ISS_DELETE_PRIORITY_FILTER_TABLE_MEM_POOL (ISS_PRIORITY_FILTER_POOL_ID);
        ISS_DELETE_ACL_MSG_QUEUE_MEM_POOL (ISS_ACL_MSG_QUEUE_POOL_ID);
        return ISS_FAILURE;
    }

    if (OsixQueCrt ((UINT1 *) ISS_SYS_ACL_QUEUE, OSIX_MAX_Q_MSG_LEN,
                    ISS_QUEUE_SIZE, &(ISS_ACL_MSG_QUE_ID)) != OSIX_SUCCESS)
    {
        ISS_TRC (INIT_SHUT_TRC, "ACL queue creation FAILED\n");
        ISS_DELETE_RATEENTRY_MEM_POOL (ISS_RATEENTRY_POOL_ID);
        ISS_DELETE_PRIORITY_FILTER_TABLE_MEM_POOL (ISS_PRIORITY_FILTER_POOL_ID);
        ISS_DELETE_L2FILTERENTRY_MEM_POOL (ISS_L2FILTERENTRY_POOL_ID);
        ISS_DELETE_ACL_MSG_QUEUE_MEM_POOL (ISS_ACL_MSG_QUEUE_POOL_ID);
        ISS_DELETE_FILTER_SHADOW_MEM_POOL (ISS_FILTER_SHADOW_POOL_ID);

        return ISS_FAILURE;
    }

    /* Initialise the SLL lists for L3 Filter Table */
    ISS_SLL_INIT (&(ISS_L3FILTER_LIST));
    ISS_CREATE_REDIRECT_INTF_GRP_MEM_POOL (ISS_REDIRECT_INTF_GRP_MEMBLK_SIZE,
                                           ISS_REDIRECT_INTF_GRP_MEMBLK_COUNT,
                                           &(ISS_REDIRECT_INTF_GRP_POOL_ID));

    ISS_REDIRECT_INTF_GRP_ALLOC_MEM_BLOCK (gpIssRedirectIntfInfo);

    if (gpIssRedirectIntfInfo != NULL)
    {
        ISS_MEMSET (gpIssRedirectIntfInfo, 0,
                    (sizeof (tIssRedirectIntfGrpTable) *
                     ISS_MAX_REDIRECT_GRP_ID));
    }
    else
    {
        ISS_DELETE_RATEENTRY_MEM_POOL (ISS_RATEENTRY_POOL_ID);
        ISS_DELETE_PRIORITY_FILTER_TABLE_MEM_POOL (ISS_PRIORITY_FILTER_POOL_ID);
        ISS_DELETE_L2FILTERENTRY_MEM_POOL (ISS_L2FILTERENTRY_POOL_ID);
        ISS_DELETE_L3FILTERENTRY_MEM_POOL (ISS_L3FILTERENTRY_POOL_ID);
        return ISS_FAILURE;
    }

    /* Allocate memory pool for UDB Filter table and Entry */

    if (ISS_CREATE_UDB_FILTER_TABLE_MEM_POOL (ISS_UDB_FILTER_TABLE_MEMBLK_SIZE,
                                              ISS_UDB_FILTER_TABLE_MEMBLK_COUNT,
                                              &(ISS_UDB_FILTER_TABLE_POOL_ID))
        != MEM_SUCCESS)
    {
        ISS_DELETE_RATEENTRY_MEM_POOL (ISS_RATEENTRY_POOL_ID);
        ISS_DELETE_PRIORITY_FILTER_TABLE_MEM_POOL (ISS_PRIORITY_FILTER_POOL_ID);
        ISS_DELETE_REDIRECT_INTF_GRP_MEM_POOL (ISS_REDIRECT_INTF_GRP_POOL_ID);
        ISS_DELETE_L2FILTERENTRY_MEM_POOL (ISS_L2FILTERENTRY_POOL_ID);
        ISS_DELETE_L3FILTERENTRY_MEM_POOL (ISS_L3FILTERENTRY_POOL_ID);
        ISS_TRC (INIT_SHUT_TRC,
                 "UDB Filter Table Memory Pool Creation FAILED\n");
        return ISS_FAILURE;
    }

    if (ISS_CREATE_UDB_FILTER_ENTRY_MEM_POOL (ISS_UDB_FILTER_ENTRY_MEMBLK_SIZE,
                                              ISS_UDB_FILTERENTRY_MEMBLK_COUNT,
                                              &(ISS_UDB_FILTERENTRY_POOL_ID))
        != MEM_SUCCESS)
    {
        ISS_DELETE_RATEENTRY_MEM_POOL (ISS_RATEENTRY_POOL_ID);
        ISS_DELETE_PRIORITY_FILTER_TABLE_MEM_POOL (ISS_PRIORITY_FILTER_POOL_ID);
        ISS_DELETE_REDIRECT_INTF_GRP_MEM_POOL (ISS_REDIRECT_INTF_GRP_POOL_ID);
        ISS_DELETE_L2FILTERENTRY_MEM_POOL (ISS_L2FILTERENTRY_POOL_ID);
        ISS_DELETE_L3FILTERENTRY_MEM_POOL (ISS_L3FILTERENTRY_POOL_ID);
        ISS_DELETE_UDB_FILTER_TABLE_MEM_POOL (ISS_UDB_FILTER_TABLE_POOL_ID);
        ISS_TRC (INIT_SHUT_TRC,
                 "UDB Filter Entry Memory Pool Creation FAILED\n");
        return ISS_FAILURE;
    }

    /* Initialise the SLL lists for UDB Filter Table */
    ISS_SLL_INIT (&(ISS_UDB_FILTER_TABLE_LIST));
    if (ISS_CREATE_REDIR_PORT_MSK_MEM_POOL (ISS_REDIRECT_PORTMASK_MEMBLK_SIZE,
                                            ISS_REDIR_PORTMASK_MEMBLK_CNT,
                                            &(ISS_REDIR_PORT_MSK_POOL_ID))
        != MEM_SUCCESS)
    {
        ISS_DELETE_RATEENTRY_MEM_POOL (ISS_RATEENTRY_POOL_ID);
        ISS_DELETE_PRIORITY_FILTER_TABLE_MEM_POOL (ISS_PRIORITY_FILTER_POOL_ID);
        ISS_DELETE_REDIRECT_INTF_GRP_MEM_POOL (ISS_REDIRECT_INTF_GRP_POOL_ID);
        ISS_DELETE_L2FILTERENTRY_MEM_POOL (ISS_L2FILTERENTRY_POOL_ID);
        ISS_DELETE_L3FILTERENTRY_MEM_POOL (ISS_L3FILTERENTRY_POOL_ID);
        ISS_DELETE_UDB_FILTER_TABLE_MEM_POOL (ISS_UDB_FILTER_TABLE_POOL_ID);
        ISS_DELETE_UDB_FILTER_TABLE_MEM_POOL (ISS_UDB_FILTERENTRY_POOL_ID);
        ISS_TRC (INIT_SHUT_TRC,
                 "Redirect Port Mask Memory Pool Creation FAILED\n");
        return ISS_FAILURE;
    }

    if (ISS_CREATE_REDIR_IFPORT_GRP_MEM_POOL (ISS_REDIRECT_IFPORT_MEMBLK_SIZE,
                                              ISS_REDIR_IFPORT_MEMBLK_CNT,
                                              &(ISS_REDIR_IFPORT_GRP_POOL_ID))
        != MEM_SUCCESS)
    {
        ISS_DELETE_RATEENTRY_MEM_POOL (ISS_RATEENTRY_POOL_ID);
        ISS_DELETE_PRIORITY_FILTER_TABLE_MEM_POOL (ISS_PRIORITY_FILTER_POOL_ID);
        ISS_DELETE_REDIRECT_INTF_GRP_MEM_POOL (ISS_REDIRECT_INTF_GRP_POOL_ID);
        ISS_DELETE_L2FILTERENTRY_MEM_POOL (ISS_L2FILTERENTRY_POOL_ID);
        ISS_DELETE_L3FILTERENTRY_MEM_POOL (ISS_L3FILTERENTRY_POOL_ID);
        ISS_DELETE_UDB_FILTER_TABLE_MEM_POOL (ISS_UDB_FILTER_TABLE_POOL_ID);
        ISS_DELETE_UDB_FILTER_TABLE_MEM_POOL (ISS_UDB_FILTERENTRY_POOL_ID);
        ISS_DELETE_REDIR_PORT_MSK_MEM_POOL (ISS_REDIR_PORT_MSK_POOL_ID);
        ISS_TRC (INIT_SHUT_TRC,
                 "Redirect Interface Port Memory Pool Creation FAILED\n");
        return ISS_FAILURE;
    }
#ifdef SNMP_2_WANTED
    RegisterFSISSE ();
    RegisterFSISSM ();
    RegisterFSISSA ();
#endif
    AclRedInitGlobalInfo ();

    return ISS_SUCCESS;
}

/****************************************************************************/
/*    Function Name      : AclShutdown                                      */
/*                                                                          */
/*    Description        : This function shutdown the ISS Extension Module. */
/*                   And will be used only by RM Modue for HA support.*/
/*                    in case of communication lost scenario        */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : OSIX_SUCCESS/OSIX_FAILURE                          */
/****************************************************************************/
PUBLIC INT4
AclStart ()
{
    /* Allocate memory pool for Rate ctrl table */
    if (ISS_CREATE_RATEENTRY_MEM_POOL (ISS_RATEENTRY_MEMBLK_SIZE,
                                       ISS_RATEENTRY_MEMBLK_COUNT,
                                       &(ISS_RATEENTRY_POOL_ID)) != MEM_SUCCESS)
    {
        ISS_TRC (INIT_SHUT_TRC, "Rate Entry Memory Pool Creation FAILED\n");
        return OSIX_FAILURE;
    }

    /* Call the Hardware routine to initialise filters in hardware */

    /* Allocate memory pool for L2 Filter table */
    if (ISS_CREATE_L2FILTERENTRY_MEM_POOL (ISS_L2FILTERENTRY_MEMBLK_SIZE,
                                           ISS_L2FILTERENTRY_MEMBLK_COUNT,
                                           &(ISS_L2FILTERENTRY_POOL_ID))
        != MEM_SUCCESS)
    {
        ISS_TRC (INIT_SHUT_TRC,
                 "L2 Filter Entry Memory Pool Creation FAILED\n");
        return OSIX_FAILURE;
    }

    /* Initialise the SLL lists for L2 Filter Table */
    ISS_SLL_INIT (&(ISS_L2FILTER_LIST));

    /* Allocate memory pool for L3 Filter table */
    if (ISS_CREATE_L3FILTERENTRY_MEM_POOL (ISS_L3FILTERENTRY_MEMBLK_SIZE,
                                           ISS_L3FILTERENTRY_MEMBLK_COUNT,
                                           &(ISS_L3FILTERENTRY_POOL_ID))
        != MEM_SUCCESS)
    {
        ISS_TRC (INIT_SHUT_TRC,
                 "L3 Filter Entry Memory Pool Creation FAILED\n");
        return OSIX_FAILURE;
    }

    /* Initialise the SLL lists for L3 Filter Table */
    ISS_SLL_INIT (&(ISS_L3FILTER_LIST));

    if (ISS_CREATE_FILTER_SHADOW_MEM_POOL (sizeof (tIssFilterShadowTable),
                                           ISS_FILTER_SHADOW_POOL_SIZE,
                                           &(ISS_FILTER_SHADOW_POOL_ID))
        != MEM_SUCCESS)
    {
        ISS_TRC (INIT_SHUT_TRC,
                 "Filter Shadow Table Memory Pool Creation FAILED\n");
        return OSIX_FAILURE;
    }

    AclRedInitGlobalInfo ();

    return OSIX_SUCCESS;
}

/****************************************************************************/
/*    Function Name      : AclShutdown                                      */
/*                                                                          */
/*    Description        : This function shutdown the ISS Extension Module. */
/*                   And will be used only by RM Modue for HA support.*/
/*                    in case of communication lost scenario        */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : OSIX_SUCCESS/OSIX_FAILURE                          */
/****************************************************************************/
PUBLIC INT4
AclShutdown ()
{

    tTMO_SLL_NODE      *pCurrNode = NULL;
    tTMO_SLL_NODE      *pNextNode = NULL;
    tIssRateCtrlEntry  *pIssRateCtrlEntry = NULL;
    tIssSllNode        *pSllNode = NULL;
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    UINT2               u2PortIndex = 0;
    INT4                i4RetStatus = 0;

    /* Release all Rate Control Entries */
    for (u2PortIndex = 1; u2PortIndex <= ISS_MAX_PORTS; u2PortIndex++)
    {
        pIssRateCtrlEntry = gIssExGlobalInfo.apIssRateCtrlEntry[u2PortIndex];
        /* Release the RateCtrl Mem Block to the Pool */
        if (pIssRateCtrlEntry != NULL)
        {
            if (ISS_RATEENTRY_FREE_MEM_BLOCK (pIssRateCtrlEntry) != MEM_SUCCESS)
            {
                ISS_TRC (INIT_SHUT_TRC,
                         "ISS_RATEENTRY_FREE_MEM_BLOCK() FAILED\n");
            }
        }
        gIssExGlobalInfo.apIssRateCtrlEntry[u2PortIndex] = NULL;
    }

    /* Delete Rate Control Memory Pool */
    ISS_DELETE_RATEENTRY_MEM_POOL (ISS_RATEENTRY_POOL_ID);
    ISS_RATEENTRY_POOL_ID = 0;

    pSllNode = ISS_SLL_FIRST (&ISS_L2FILTER_LIST);

    while (pSllNode != NULL)
    {
        pIssL2FilterEntry = (tIssL2FilterEntry *) pSllNode;

        /* Delete the Entry from the Software */
        if (ISS_FILTER_SHADOW_FREE_MEM_BLOCK
            (pIssL2FilterEntry->pIssFilterShadowInfo) != MEM_SUCCESS)
        {
            ISS_TRC (INIT_SHUT_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                     "Filter Sahdow Entry Free Failure\n");
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L2FILTER_LIST, pSllNode);
    }

    if ((TMO_SLL_Count (&(ISS_L2FILTER_LIST))) > 0)
    {
        UTL_SLL_OFFSET_SCAN (&(ISS_L2FILTER_LIST),
                             pCurrNode, pNextNode, tTMO_SLL_NODE *)
        {
            TMO_SLL_Delete (&(ISS_L2FILTER_LIST), pCurrNode);

            i4RetStatus =
                MemReleaseMemBlock (ISS_L2FILTERENTRY_POOL_ID,
                                    (UINT1 *) pCurrNode);

            if (i4RetStatus != MEM_SUCCESS)
            {
                ISS_TRC (BUFFER_TRC | INIT_SHUT_TRC | ALL_FAILURE_TRC,
                         "ReleaseMemBlok L2 filter Entry Failed\n");
            }

        }                        /* End of Scan */

    }
    ISS_DELETE_L2FILTERENTRY_MEM_POOL (ISS_L2FILTERENTRY_POOL_ID);
    ISS_L2FILTERENTRY_POOL_ID = 0;

    pSllNode = ISS_SLL_FIRST (&ISS_L3FILTER_LIST);

    while (pSllNode != NULL)
    {
        pIssL3FilterEntry = (tIssL3FilterEntry *) pSllNode;

        /* Delete the Entry from the Software */
        if (ISS_FILTER_SHADOW_FREE_MEM_BLOCK
            (pIssL3FilterEntry->pIssFilterShadowInfo) != MEM_SUCCESS)
        {
            ISS_TRC (BUFFER_TRC | INIT_SHUT_TRC | ALL_FAILURE_TRC,
                     "Filter Shadow Entry Free Failure\n");
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L3FILTER_LIST, pSllNode);
    }
    if ((TMO_SLL_Count (&(ISS_L3FILTER_LIST))) > 0)
    {
        UTL_SLL_OFFSET_SCAN (&(ISS_L3FILTER_LIST),
                             pCurrNode, pNextNode, tTMO_SLL_NODE *)
        {
            TMO_SLL_Delete (&(ISS_L3FILTER_LIST), pCurrNode);

            i4RetStatus =
                MemReleaseMemBlock (ISS_L3FILTERENTRY_POOL_ID,
                                    (UINT1 *) pCurrNode);

            if (i4RetStatus != MEM_SUCCESS)
            {
                ISS_TRC (BUFFER_TRC | INIT_SHUT_TRC | ALL_FAILURE_TRC,
                         "ReleaseMemBlock L3 filter Entry Failed\n");
            }

        }                        /* End of Scan */

    }
    ISS_DELETE_L3FILTERENTRY_MEM_POOL (ISS_L3FILTERENTRY_POOL_ID);
    ISS_L3FILTERENTRY_POOL_ID = 0;

    ISS_DELETE_FILTER_SHADOW_MEM_POOL (ISS_FILTER_SHADOW_POOL_ID);
    ISS_FILTER_SHADOW_POOL_ID = 0;

    AclRedDeInitGlobalInfo ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssSetDefaultRateCtrlValues                          */
/*                                                                           */
/* Description        : This function is called from IssCreatePort() to      */
/*                      initialise the Rate table entry with default values  */
/*                                                                           */
/* Input(s)           : tIssRateCtrlEntry *                                  */
/*                      u2PortIndex - port index                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IssSetDefaultRateCtrlValues (UINT2 u2PortIndex,
                             tIssRateCtrlEntry * pIssRateCtrlEntry)
{
    /* Setting the default values for the Rate Entry */
    pIssRateCtrlEntry->u4IssRateCtrlDLFLimitValue = 0;
    pIssRateCtrlEntry->u4IssRateCtrlBCASTLimitValue = 0;
    pIssRateCtrlEntry->u4IssRateCtrlMCASTLimitValue = 0;
    pIssRateCtrlEntry->i4IssRateCtrlPortLimitRate = 0;
    pIssRateCtrlEntry->i4IssRateCtrlPortBurstRate = 0;

#ifdef NPAPI_WANTED
    AclRedHwAuditIncBlkCounter ();
    IssHwSetRateLimitingValue ((UINT4) u2PortIndex,
                               ISS_RATE_DLF,
                               (INT4) pIssRateCtrlEntry->
                               u4IssRateCtrlDLFLimitValue);

    IssHwSetRateLimitingValue ((UINT4) u2PortIndex,
                               ISS_RATE_BCAST,
                               (INT4) pIssRateCtrlEntry->
                               u4IssRateCtrlBCASTLimitValue);

    IssHwSetRateLimitingValue ((UINT4) u2PortIndex,
                               ISS_RATE_MCAST,
                               (INT4) pIssRateCtrlEntry->
                               u4IssRateCtrlMCASTLimitValue);

    IssHwSetPortEgressPktRate ((UINT4) u2PortIndex, pIssRateCtrlEntry->
                               i4IssRateCtrlPortLimitRate, pIssRateCtrlEntry->
                               i4IssRateCtrlPortBurstRate);
    AclRedHwAuditDecBlkCounter ();
#else
    UNUSED_PARAM (u2PortIndex);
#endif
}

/*****************************************************************************/
/* Function Name      : AclProcessQMsgEvent                     */
/*                                                                           */
/* Description        : This function process the ACL Queue message posted in*/
/*            ACl Queue                          */
/*                                                                           */
/* Input(s)           : None                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
VOID
AclProcessQMsgEvent (VOID)
{
    tAclQMsg           *pAclQMsg = NULL;

    while (OsixQueRecv (ISS_ACL_MSG_QUE_ID, (UINT1 *) &pAclQMsg,
                        OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
    {
        switch (pAclQMsg->u4MsgType)
        {
            case ISS_ACL_RM_MESSAGE:
                AclRedHandleRmEvents (pAclQMsg);
                break;
            default:
                ISS_TRC (ALL_FAILURE_TRC,
                         "Wrong Message Type Received in the Message Queue\n");
                break;
        }
        ISS_ACL_MSG_QUEUE_FREE_MEM_BLOCK (pAclQMsg);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : IssExCreatePortRateCtrl                              */
/*                                                                           */
/* Description        : This function allocates memory blocks for the port   */
/*                      based rate control info and sets the default values  */
/*                      for the entries.                                     */
/*                                                                           */
/* Input(s)           : u2PortIndex                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS - On success                             */
/*                      ISS_FAILURE - On failure                             */
/*****************************************************************************/
INT4
IssExCreatePortRateCtrl (UINT2 u2PortIndex, tIssTableName IssTableFlag)
{
    tIssRateCtrlEntry  *pIssRateCtrlEntry = NULL;

    if ((IssTableFlag == ISS_ALL_TABLES)
        || (IssTableFlag == ISS_RATECTRL_TABLE))
    {
        /* Check if an entry for Rate ctrl index exists */
        if (gIssExGlobalInfo.apIssRateCtrlEntry[u2PortIndex] != NULL)
        {
            ISS_TRC (INIT_SHUT_TRC,
                     "Port with same Index EXISTS Another Rate Ctrl Entry with"
                     " same Index cannot be created\n");
            return ISS_SUCCESS;
        }

        /* Allocate a block for Rate Ctrl entry */
        if (ISS_RATEENTRY_ALLOC_MEM_BLOCK (pIssRateCtrlEntry) == NULL)
        {
            ISS_TRC (INIT_SHUT_TRC, "ISS_RATEENTRY_ALLOC_MEM_BLOCK FAILED\n");
            return ISS_FAILURE;
        }
        /* Memset the Rate Ctrl entry to zero */
        ISS_MEMSET (pIssRateCtrlEntry, 0, sizeof (tIssRateCtrlEntry));
        gIssExGlobalInfo.apIssRateCtrlEntry[u2PortIndex] = pIssRateCtrlEntry;
        IssSetDefaultRateCtrlValues (u2PortIndex, pIssRateCtrlEntry);
    }

    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssExDeletePortRateCtrl                              */
/*                                                                           */
/* Description        : This function releases memory blocks of the port     */
/*                      based rate control info                              */
/*                                                                           */
/* Input(s)           : u2PortIndex                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS - On success                             */
/*                      ISS_FAILURE - On failure                             */
/*****************************************************************************/
INT4
IssExDeletePortRateCtrl (UINT2 u2PortIndex)
{
    tIssRateCtrlEntry  *pIssRateCtrlEntry = NULL;

    pIssRateCtrlEntry = gIssExGlobalInfo.apIssRateCtrlEntry[u2PortIndex];

    /* Release the RateCtrl Mem Block to the Pool */
    if (pIssRateCtrlEntry != NULL)
    {
        if (ISS_RATEENTRY_FREE_MEM_BLOCK (pIssRateCtrlEntry) != MEM_SUCCESS)
        {
            ISS_TRC (INIT_SHUT_TRC, "ISS_RATEENTRY_FREE_MEM_BLOCK() FAILED\n");
        }
    }

    gIssExGlobalInfo.apIssRateCtrlEntry[u2PortIndex] = NULL;

    return ISS_SUCCESS;
}

/****************************************************************************
* Function    :  IssGetL3FilterAddrType
* Input       :  INT4 i4IssL3FilterNo
* Output      :  None
* Returns     :  tIssL3FilterEntry * 
*****************************************************************************/
UINT1
IssGetL3FilterAddrType (UINT4 u4IssL3FilterNo, INT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry ((INT4) u4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement = (INT4) pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  IssSetL3FilterAddrType
 Input       :  The Indices
               i4IssL3FilterNo

                The Object
               
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IssSetL3FilterAddrType (INT4 i4IssL3FilterNo, INT4 i4AddrType)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == i4AddrType)
        {
            return SNMP_SUCCESS;
        }
        pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType = i4AddrType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/**********************/
/****************************************************************************
* Function    :  IssGetL3FilterDstPrefixLength
* Input       :  INT4 i4IssL3FilterNo
* Output      :  None
* Returns     :  tIssL3FilterEntry *
*****************************************************************************/
UINT1
IssGetL3FilterDstPrefixLength (INT4 i4IssL3FilterNo, INT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement =
            (INT4) pIssL3FilterEntry->
            u4IssL3FilterMultiFieldClfrDstPrefixLength;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  IssSetL3FilterDstPrefixLength
 Input       :  The Indices
               i4IssL3FilterNo

                The Object

 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IssSetL3FilterDstPrefixLength (INT4 i4IssL3FilterNo, INT4 i4DstPrefixLength)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->u4IssL3FilterMultiFieldClfrDstPrefixLength ==
            (UINT4) i4DstPrefixLength)
        {
            return SNMP_SUCCESS;
        }
        pIssL3FilterEntry->u4IssL3FilterMultiFieldClfrDstPrefixLength =
            i4DstPrefixLength;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
* Function    :  IssGetL3FilterSrcPrefixLength
* Input       :  INT4 i4IssL3FilterNo
* Output      :  None
* Returns     :  tIssL3FilterEntry *
*****************************************************************************/
UINT1
IssGetL3FilterSrcPrefixLength (INT4 i4IssL3FilterNo, INT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement =
            (INT4) pIssL3FilterEntry->
            u4IssL3FilterMultiFieldClfrSrcPrefixLength;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  IssSetL3FilterSrcPrefixLength
 Input       :  The Indices
               i4IssL3FilterNo

                The Object

 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IssSetL3FilterSrcPrefixLength (INT4 i4IssL3FilterNo, INT4 i4SrcPrefixLength)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->u4IssL3FilterMultiFieldClfrSrcPrefixLength ==
            (UINT4) i4SrcPrefixLength)
        {
            return SNMP_SUCCESS;
        }
        pIssL3FilterEntry->u4IssL3FilterMultiFieldClfrSrcPrefixLength =
            (UINT4) i4SrcPrefixLength;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
* Function    :  IssGetL3FilterControlFlowId
* Input       :  INT4 i4IssL3FilterNo
* Output      :  None
* Returns     :  tIssL3FilterEntry *
*****************************************************************************/
UINT1
IssGetL3FilterControlFlowId (INT4 i4IssL3FilterNo, UINT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement = (INT4) pIssL3FilterEntry->u4IssL3MultiFieldClfrFlowId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  IssSetL3FilterControlFlowId
 Input       :  The Indices
               i4IssL3FilterNo

                The Object

 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IssSetL3FilterControlFlowId (INT4 i4IssL3FilterNo, UINT4 u4FlowId)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->u4IssL3MultiFieldClfrFlowId == u4FlowId)
        {
            return SNMP_SUCCESS;
        }
        pIssL3FilterEntry->u4IssL3MultiFieldClfrFlowId = u4FlowId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
* Function    :  IssGetL3FilterStorageType
* Input       :  INT4 i4IssL3FilterNo
* Output      :  None
* Returns     :  tIssL3FilterEntry *
*****************************************************************************/
UINT1
IssGetL3FilterStorageType (INT4 i4IssL3FilterNo, UINT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement = (INT4) pIssL3FilterEntry->i4StorageType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  IssSetL3FilterStorageType
 Input       :  The Indices
               i4IssL3FilterNo

                The Object

 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IssSetL3FilterStorageType (INT4 i4IssL3FilterNo, INT4 i4StorageType)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->i4StorageType == i4StorageType)
        {
            return SNMP_SUCCESS;
        }
        pIssL3FilterEntry->i4StorageType = i4StorageType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/*****************************************************************************/
/* Function Name      : IssUtlConvertPrefixToMask                             */
/*                                                                           */
/* Description        : This function converts the give prefix to Mssk       */
/*                                                                           */
/* Input(s)           : u4Prefix,: Prefix                                    */
/*                                                                           */
/* Output(s)           : u4Mask : Mask                                       */
/*                                                                           */
/* Return Value(s)    :                              */
/*****************************************************************************/
VOID
IssUtlConvertPrefixToMask (UINT4 u4Prefix, UINT4 *pu4Mask)
{
    INT4                i4Count = 0;

    *pu4Mask = 0;

    for (i4Count = 0; i4Count < (INT4) u4Prefix; i4Count++)
    {
        *pu4Mask = *pu4Mask >> 1;
        *pu4Mask |= 0x80000000;
    }
}

/*****************************************************************************/
/* Function Name      : IssUtlConvertMaskToPrefix                             */
/*                                                                           */
/* Description        : This function converts the give prefix to Mssk       */
/*                                                                           */
/* Input(s)           : u4Prefix,: Prefix                                    */
/*                                                                           */
/* Output(s)           : u4Mask : Mask                                       */
/*                                                                           */
/* Return Value(s)    :                              */
/*****************************************************************************/
VOID
IssUtlConvertMaskToPrefix (UINT4 u4Mask, UINT4 *pu4Prefix)
{
    UINT4               u4Count = 0;

    *pu4Prefix = 0;

    while (u4Mask)
    {
        u4Count++;
        u4Mask = u4Mask << 1;
    }

    *pu4Prefix = u4Count;
}

/**************************************************************************/
/* Function Name       : IssConvertLocalPortListToArray                  */
/*                                                                        */
/* Description         : This function converts Local port list to array  */
/*                       of physical ports ports.                         */
/*                                                                        */
/* Input(s)            : LocalPortList - Local port list                  */
/*                       pIssRedirectIfPortGroup -  hold list of ports    */
/*                       status                                           */
/*                                                                        */
/*                                                                        */
/* Output(s)           : pu4IfPortArray, u1NumPorts,u1ActivePortExist    */
/*                                                                        */
/* Global Variables                                                       */
/* Referred            : None                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Modified            : None                                             */
/*                                                                        */
/* Exceptions or OS                                                       */
/* Error Handling      : None                                             */
/*                                                                        */
/* Use of Recursion    : None                                             */
/*                                                                        */
/* Returns             : None                                             */
/*                                                                        */
/**************************************************************************/
VOID
IssConvertRedirectPortListToArray (tPortList LocalPortList,
                                   tIssRedirectIfPortGroup *
                                   pIssRedirectIfPortGroup,
                                   UINT4 *pu4IfPortArray, UINT4 *pu4NumPorts,
                                   UINT1 *pu1ActivePortExist)
{
    UINT2               u2Port = 0;
    UINT2               u2BytIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1PortFlag = 0;
    UINT4               u4Counter = 0;

    *pu1ActivePortExist = OSIX_TRUE;
    for (u2BytIndex = 0; u2BytIndex < ISS_REDIRECT_PORT_LIST_SIZE; u2BytIndex++)
    {
        u1PortFlag = LocalPortList[u2BytIndex];
        for (u2BitIndex = 0; ((u2BitIndex < ISS_REDIRECT_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {

            if ((u1PortFlag & ISS_REDIRECT_BIT8) != 0)
            {
                u2Port = (UINT2) ((u2BytIndex * ISS_REDIRECT_PORTS_PER_BYTE) +
                                  u2BitIndex + 1);
                pu4IfPortArray[u4Counter] = u2Port;
                pIssRedirectIfPortGroup[u4Counter].u4IfIndex = u2Port;
                CfaGetIfOperStatus (pIssRedirectIfPortGroup[u4Counter].
                                    u4IfIndex,
                                    (UINT1 *)
                                    &(pIssRedirectIfPortGroup[u4Counter].
                                      u1OperStatus));
                if (pIssRedirectIfPortGroup[u4Counter].u1OperStatus ==
                    CFA_IF_DOWN)
                {
                    *pu1ActivePortExist = OSIX_FALSE;
                }
                u4Counter = (UINT4) (u4Counter + 1);
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
        /* update number of ports */
        *pu4NumPorts = u4Counter;
    }
}

/**************************************************************************/
/* Function Name       : IssConvertLocalPortListToArray                  */
/*                                                                        */
/* Description         : This function converts Local port list to array  */
/*                       of physical ports ports.                         */
/*                                                                        */
/* Input(s)            : u4NumPort- No Of Ports in VT                     */
/*                       pIssRedirectPortMaskAndIndex - Redirect Mask     */
/*                       and Index                                        */
/*                       pu1mask - Mask Value                             */
/*                       pu4NoOfRules - No of Rules                       */
/*                       pu1ActivePortExist-If port is down its set to    */
/*                       OSIX_FALSE                                       */
/*                       pIssRedirectIfPortGroup- Holds active ports      */
/*                                                                        */
/* Output(s)           : pu4IfPortArray, u1NumPorts                       */
/*                                                                        */
/* Global Variables                                                       */
/* Referred            : None                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Modified            : None                                             */
/*                                                                        */
/* Exceptions or OS                                                       */
/* Error Handling      : None                                             */
/*                                                                        */
/* Use of Recursion    : None                                             */
/*                                                                        */
/* Returns             : None                                             */
/*                                                                        */
/**************************************************************************/
UINT1
IssCalculateTrafficLogicForPort (UINT4 u4NumPort, tIssRedirectPortMaskAndIndex
                                 * pIssRedirectPortMaskAndIndex, UINT1 *pu1mask,
                                 UINT4 *pu4NoOfRules, UINT1 u1ActivePortExist,
                                 tIssRedirectIfPortGroup *
                                 pIssRedirectIfPortGroup)
{

    UINT1               u1Index = 0x0;
    UINT1               u1MsbMask = 0;
    UINT1               u1LsbMask = 0;
    UINT1               u1IndexHigh = 0;
    UINT1               u1IndexLow = 0;
    UINT2               u2TempVal = 0x0;
    UINT4               u4CurrIndex = 0;
    UINT4               u4RuleCount = 0;
    UINT4               u4CntNumPorts = 0;
    UINT1               u1AllPortsStatus = 0;

    for (u4CntNumPorts = 0; u4CntNumPorts < u4NumPort; u4CntNumPorts++)
    {
        if (pIssRedirectIfPortGroup[u4CntNumPorts].u1OperStatus == CFA_IF_DOWN)
        {
            u1AllPortsStatus++;

        }
    }
    if (u1AllPortsStatus == u4NumPort)
    {
        return ISS_FAILURE;
    }
    for (u1Index = 0; u1Index < ISS_REDIRECT_PORTS_PER_BYTE; u1Index++)
    {
        u2TempVal = (UINT2) POW (2, u1Index);
        if (u2TempVal >= u4NumPort)
        {
            break;
        }
    }
    if (u1Index > ISS_REDIRECT_PORTS_PER_BYTE)
    {
        return ISS_FAILURE;
    }

    *pu1mask = u2TempVal - 1;
/*
 * Case 1 : No. of ports in the virtual trunk is < 8 and is not equal to a 
 * POWer-of 2.
 * Bits selected = 4 bits
 * The distribution logic willl be based on the distribution parameter 
 * as follows .
 *  
 *  0th Byte (LSB byte)   .    2 LSB bits would be selected
 *  2nd Byte (MSB byte )   -- 2  LSB bits would be selected
 *   
 *   Note - 16 PCL rules would be programmed to the TCAM. Traffic would 
 *   be evenly distributed to all the active ports in the virtual trunk.
 *
 */

    if ((u4NumPort < ISS_REDIRECT_PORTS_PER_BYTE) &&
        ((u4NumPort & (u4NumPort - 1)) != 0))
    {
        u1LsbMask = POW (2, 2);
        u1MsbMask = POW (2, 2);
    }

/*
 * Case 2 : No. of ports in the virtual trunk is  equal to a POWer-of 2 
 * i.e N = 2 POW x , where x is the number of bits selected
 * The distribution logic wil be based on the distribution parameter as 
 * follows .
 *  
 *  If 'x' is an even number, then bits would be selected as follows -
 *  0th Byte (LSB byte)   .    x/2 LSB bits would be selected
 *  2nd Byte (MSB byte )   -- x/2  LSB bits would be selected
 *   
 *   If 'x' is an odd number, then bits would be selected as follows -
 *   0th Byte (LSB byte)   .    (x div 2) + 1 LSB bits would be selected
 *   2nd Byte (MSB byte )   -- (x div 2)  LSB bits would be selected
 *   Note - 'N'  PCL rules would be programmed to the TCAM as 'x' 
 *   bits are sufficient to distribute traffic. Traffic would be evenly 
 *   distributed to all the active ports in the virtual trunk.
 *
 *
 *
 * */

    if ((u4NumPort & (u4NumPort - 1)) == 0)
    {
        if ((u1Index % 2) == 0)
        {
            u1LsbMask = POW (2, (u1Index / 2));
            u1MsbMask = POW (2, (u1Index / 2));
        }
        else
        {
            u1LsbMask = POW (2, ((u1Index / 2) + 1));
            u1MsbMask = POW (2, (u1Index / 2));
        }
    }

/*
 *
 * Case 3 : No. of ports in the virtual trunk is > 8 and is not equal to a 
 * POWer-of 2 : N < 2 pow x, where x is the number of bits selected -
 * The distribution logic willl be based on the distribution parameter 
 * as follows .
 *  
 *  If 'x' is an even number, then bits would be selected as follows -
 *  0th Byte (LSB byte)   .    x/2 LSB bits would be selected
 *  2nd Byte (MSB byte )   -- x/2  LSB bits would be selected
 *  If 'x' is an odd number, then bits would be selected as follows -
 *  0th Byte (LSB byte)   .    (x div 2) + 1 LSB bits would be selected
 *  2nd Byte (MSB byte )   -- (x div 2)  LSB bits would be selected
 *   
 *   Note - (2 POW x) PCL rules would be programmed to the TCAM. 
 *   Traffic would be evenly distributed to all the active ports in the virtual trunk.
 *
 **/
    if ((u4NumPort > ISS_REDIRECT_PORTS_PER_BYTE) &&
        ((u4NumPort & (u4NumPort - 1)) != 0))
    {
        if ((u1Index % 2) == 0)
        {
            u1LsbMask = POW (2, (u1Index / 2));
            u1MsbMask = POW (2, (u1Index / 2));
        }
        else
        {
            u1LsbMask = POW (2, (u1Index / 2 + 1));
            u1MsbMask = POW (2, (u1Index / 2));
        }
    }

    for (u1IndexHigh = 0; u1IndexHigh < u1MsbMask; u1IndexHigh++)
    {
        for (u1IndexLow = 0; u1IndexLow < u1LsbMask; u1IndexLow++)
        {
            pIssRedirectPortMaskAndIndex->u1PortValueMsb = u1IndexHigh;
            pIssRedirectPortMaskAndIndex->u1PortValueLsb = u1IndexLow;
            pIssRedirectPortMaskAndIndex->u4PortIndex = u4RuleCount % u4NumPort;
            pIssRedirectPortMaskAndIndex->u1PortMaskMsb = u1MsbMask - 1;
            pIssRedirectPortMaskAndIndex->u1PortMaskLsb = u1LsbMask - 1;
            pIssRedirectPortMaskAndIndex++;
            u4RuleCount++;
        }
    }
    *pu4NoOfRules = u4RuleCount;
    if (u1ActivePortExist == OSIX_TRUE)
    {
        /* No Active ports exist, Keep the default logic */
        return ISS_SUCCESS;
    }
    /* Move to starting of the address */
    pIssRedirectPortMaskAndIndex = pIssRedirectPortMaskAndIndex - (u4RuleCount);

    /* Recalculare Hashing logic according to the active ports
     * in the system.
     *
     *  Ex: P1, P2, P3 are in VT.If P2 is down recalculate hash logic
     *  as give below .
     *
     *  Initial Hashing             Active Ports               New Logic
     *  ==============              ============               =========
     *       P1                        P1, P3                     P1
     *       P2                                                   P1
     *       P3                                                   P3
     *       P1                                                   P1
     *       P2                                                   P3
     *       P3                                                   P3
     *       P1                                                   P1
     *       P2                                                   P1
     *       P3                                                   P3
     *       P1                                                   P1
     *       P2                                                   P3
     *       P3                                                   P3
     *       P1                                                   P1
     *       P2                                                   P1
     *       P3                                                   P3
     *       P1                                                   P1
     *
     *  Replace P2 with New Active ports in round robin method.
     *  When the port comes back the same logic should be applied.
     *
     * */
    for (u1Index = 0; u1Index < *pu4NoOfRules; u1Index++)
    {
        if (pIssRedirectIfPortGroup[pIssRedirectPortMaskAndIndex->u4PortIndex].
            u1OperStatus == CFA_IF_DOWN)
        {
            IssGetNextActiveOutIndex (pIssRedirectIfPortGroup, u4NumPort,
                                      &u4CurrIndex);
            pIssRedirectPortMaskAndIndex->u4PortIndex = u4CurrIndex;
            u4CurrIndex++;
        }
        pIssRedirectPortMaskAndIndex++;
    }

    return ISS_SUCCESS;
}

/**************************************************************************/
/* Function Name       : IssGetNextActiveHashIndex                        */
/*                                                                        */
/* Description         : This function is used to get the next active     */
/*                       index.                                           */
/*                                                                        */
/* Input(s)            : pIssRedirectIfPortGroup- Active Port Information */
/*                       u4NumPorts     - Number of Ports in Port Channel */
/*                       pu4CurrIndex   - Current Index of the active port*/
/*                                                                        */
/* Output(s)           : pu4CurrIndex   - Newly Calculated Index          */
/*                                                                        */
/* Global Variables                                                       */
/* Referred            : None                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Modified            : None                                             */
/*                                                                        */
/* Exceptions or OS                                                       */
/* Error Handling      : None                                             */
/*                                                                        */
/* Use of Recursion    : None                                             */
/*                                                                        */
/* Returns             : None                                             */
/*                                                                        */
/**************************************************************************/

VOID
IssGetNextActiveOutIndex (tIssRedirectIfPortGroup * pIssRedirectIfPortGroup,
                          UINT4 u4NumPorts, UINT4 *pu4CurrIndex)
{
    while (1)
    {
        if (*pu4CurrIndex > u4NumPorts)
        {
            *pu4CurrIndex = 0;
        }

        if (pIssRedirectIfPortGroup[*pu4CurrIndex].u1OperStatus == CFA_IF_UP)
        {
            break;
        }
        *pu4CurrIndex = *pu4CurrIndex + 1;
    }
    return;
}

/**************************************************************************/
/* Function Name       : IssUpdatePortLinkStatus                          */
/*                                                                        */
/* Description         : This function Updates the port link status for   */
/*                       recalculation of hashing logic                   */
/*                                                                        */
/* Input(s)            : LocalPortList - Local port list                  */
/*                                                                        */
/* Output(s)           : pu4IfPortArray, u1NumPorts                       */
/*                                                                        */
/* Global Variables                                                       */
/* Referred            : None                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Modified            : None                                             */
/*                                                                        */
/* Exceptions or OS                                                       */
/* Error Handling      : None                                             */
/*                                                                        */
/* Use of Recursion    : None                                             */
/*                                                                        */
/* Returns             : None                                             */
/*                                                                        */
/**************************************************************************/
VOID
IssUpdatePortLinkStatus (UINT4 u4IfIndex, UINT1 u1IfType, UINT1 u1OperStatus)
{
#ifdef NPAPI_WANTED
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;
    tIssRedirectPortMaskAndIndex IssRedirectPortMaskAndIndex[256];
    tIssRedirectIfPortGroup IssRedirectIfPortGroup[256];
    UINT4               au4PortList[ISS_REDIRECT_MAX_PORTS];
    UINT4               u4Index = 0;
    UINT4               u4NumPorts = 0x0;
    UINT4               u4NoOfRules = 0;
    UINT4               au4ActivePorts[ISS_REDIRECT_MAX_PORTS];
    UINT2               u2TrafficDistByte = 0x0;
    UINT1               u1ReDirectPortIndex = 0x0;
    UINT1               u1Index = 0x0;
    UINT1               u1Mask = 0x0;
    UINT1               u1IsMember = ISS_FALSE;
    UINT1               u1ActivePortExist;
    INT4                i4RetVal = FNP_FAILURE;
    INT4                i4PortStatus = ISS_FALSE;

    ISS_MEMSET (au4ActivePorts, 0, sizeof (au4ActivePorts));
    ISS_MEMSET (&IssRedirectIfPortGroup, 0, sizeof (IssRedirectIfPortGroup));
    ISS_MEMSET (au4PortList, 0, sizeof (au4PortList));
    if ((u1IfType == CFA_ENET) || (u1IfType == CFA_LAGG))
    {
        /*Get the ACL Id and ACL type and re-install the filter ie 
         * call the existing NPAPI. Call nmhSetIssRedirectInterfaceGrpStatus 
         * with the ACTIVE */
        for (u4Index = 1; u4Index <= ISS_MAX_REDIRECT_GRP_ID; u4Index++)
        {
            if ((gpIssRedirectIntfInfo[u4Index - 1].u1RowStatus == ISS_ACTIVE)
                && (gpIssRedirectIntfInfo[u4Index - 1].u1PortListType ==
                    ISS_REDIRECT_TO_PORTLIST))
            {
                /*Never the u4Index with 0 value will have rowstatus active */
                if (IssIsMemberPort
                    (gpIssRedirectIntfInfo[u4Index - 1].PortList,
                     ISS_REDIRECT_PORT_LIST_SIZE, u4IfIndex) == ISS_SUCCESS)

                {

                    u1IsMember = ISS_TRUE;
                    if (gpIssRedirectIntfInfo[u4Index - 1].u4AclId != 0x0)
                    {

                        IssConvertRedirectPortListToArray
                            (gpIssRedirectIntfInfo
                             [u4Index - 1].PortList,
                             (tIssRedirectIfPortGroup *) &
                             IssRedirectIfPortGroup, au4PortList, &u4NumPorts,
                             &u1ActivePortExist);

                        IssCalculateTrafficLogicForPort
                            (u4NumPorts, (tIssRedirectPortMaskAndIndex *)
                             & IssRedirectPortMaskAndIndex, &u1Mask,
                             &u4NoOfRules, u1ActivePortExist,
                             (tIssRedirectIfPortGroup *) &
                             IssRedirectIfPortGroup);

                        /* If the Port List Type is Port List
                         * Extract the PortList into Port If Indexes */
                        u2TrafficDistByte =
                            gpIssRedirectIntfInfo[u4Index - 1].
                            u2TrafficDistByte;

                        switch (gpIssRedirectIntfInfo[u4Index - 1].u1AclIdType)
                        {

                            case ISS_L2_REDIRECT:
                                pIssL2FilterEntry =
                                    IssExtGetL2FilterEntry
                                    (gpIssRedirectIntfInfo[u4Index - 1].
                                     u4AclId);

                                if (pIssL2FilterEntry == NULL)
                                {
                                    return;
                                }

                                IssHwUpdateL2Filter (pIssL2FilterEntry,
                                                     ISS_L2FILTER_DELETE);

                                for (u1Index = 0; u1Index < u4NoOfRules;
                                     u1Index++)
                                {
                                    /* For the Redirect Filter Installation 
                                     *  of Port / Trunk fill the parameters
                                     *  of (Interface Type and Interface 
                                     *  IfIndex) */

                                    pIssL2FilterEntry->RedirectIfGrp.
                                        u2TunnelIfIndex = 0;

                                    pIssL2FilterEntry->RedirectIfGrp.
                                        u2TrafficDistByte = u2TrafficDistByte;

                                    pIssL2FilterEntry->RedirectIfGrp.
                                        u1TrafficDistMaskLsb =
                                        IssRedirectPortMaskAndIndex[u1Index].
                                        u1PortMaskLsb;

                                    pIssL2FilterEntry->RedirectIfGrp.
                                        u1TrafficDistMaskMsb =
                                        IssRedirectPortMaskAndIndex[u1Index].
                                        u1PortMaskMsb;

                                    pIssL2FilterEntry->RedirectIfGrp.
                                        u1TrafficDistValueLsb =
                                        IssRedirectPortMaskAndIndex[u1Index].
                                        u1PortValueLsb;

                                    pIssL2FilterEntry->RedirectIfGrp.
                                        u1TrafficDistValueMsb =
                                        IssRedirectPortMaskAndIndex[u1Index].
                                        u1PortValueMsb;

                                    u1ReDirectPortIndex =
                                        IssRedirectPortMaskAndIndex[u1Index].
                                        u4PortIndex;

                                    pIssL2FilterEntry->RedirectIfGrp.
                                        u4EgressIfIndex =
                                        au4PortList[u1ReDirectPortIndex %
                                                    u4NumPorts];

                                    /* Currently MPLS Tunnel Redirection is Not
                                     * being done . Only Ethernet Port /Trunk
                                     *  is being taken care of */

                                    if (pIssL2FilterEntry->RedirectIfGrp.
                                        u4EgressIfIndex <=
                                        SYS_DEF_MAX_PHYSICAL_INTERFACES)
                                    {
                                        pIssL2FilterEntry->RedirectIfGrp.
                                            u1EgressIfType =
                                            ISS_REDIRECT_TO_ETHERNET;

                                    }
                                    else
                                    {
                                        pIssL2FilterEntry->RedirectIfGrp.
                                            u1EgressIfType =
                                            ISS_REDIRECT_TO_TRUNK;
                                    }
                                    if (MEMCMP
                                        (pIssL2FilterEntry->
                                         IssL2FilterInPortList,
                                         gIssNullPortList,
                                         sizeof (pIssL2FilterEntry->
                                                 IssL2FilterInPortList)) != 0)
                                    {

                                        if (ISS_IS_NP_PROGRAMMING_ALLOWED () ==
                                            ISS_TRUE)
                                        {

                                            i4RetVal =
                                                IssHwUpdateL2Filter
                                                (pIssL2FilterEntry,
                                                 ISS_L2FILTER_ADD);
                                            if (i4RetVal != FNP_SUCCESS)
                                            {
                                                return;
                                            }
                                        }
                                    }

                                }

                                /* Fill the Redirect Group Id also
                                 * as it will be used in Kepping Tack
                                 * of Same Filter is mapped to this 
                                 * Redirect Group . USeful from CLI 
                                 * Perpective. From SNMP Perspective
                                 * Manager needs to take of ACL-
                                 * Redirect Group Mapping */

                                break;
                            case ISS_L3_REDIRECT:
                                pIssL3FilterEntry =
                                    IssExtGetL3FilterEntry
                                    (gpIssRedirectIntfInfo[u4Index - 1].
                                     u4AclId);

                                if (pIssL3FilterEntry == NULL)
                                {
                                    return;
                                }
                                IssHwUpdateL3Filter (pIssL3FilterEntry,
                                                     ISS_L3FILTER_DELETE);
                                for (u1Index = 0; u1Index < u4NoOfRules;
                                     u1Index++)
                                {

                                    /*  For the Redirect Filter Installation 
                                     *  of Port / Trunk fill the parameters
                                     *  of (Interface Type and Interface IfIndex) */

                                    pIssL3FilterEntry->RedirectIfGrp.
                                        u2TunnelIfIndex = 0;

                                    pIssL3FilterEntry->RedirectIfGrp.
                                        u2TrafficDistByte = u2TrafficDistByte;

                                    pIssL3FilterEntry->RedirectIfGrp.
                                        u1TrafficDistMaskLsb =
                                        IssRedirectPortMaskAndIndex[u1Index].
                                        u1PortMaskLsb;

                                    pIssL3FilterEntry->RedirectIfGrp.
                                        u1TrafficDistMaskMsb =
                                        IssRedirectPortMaskAndIndex[u1Index].
                                        u1PortMaskMsb;

                                    pIssL3FilterEntry->RedirectIfGrp.
                                        u1TrafficDistValueLsb =
                                        IssRedirectPortMaskAndIndex[u1Index].
                                        u1PortValueLsb;

                                    pIssL3FilterEntry->RedirectIfGrp.
                                        u1TrafficDistValueMsb =
                                        IssRedirectPortMaskAndIndex[u1Index].
                                        u1PortValueMsb;

                                    u1ReDirectPortIndex =
                                        IssRedirectPortMaskAndIndex[u1Index].
                                        u4PortIndex;;

                                    pIssL3FilterEntry->RedirectIfGrp.
                                        u4EgressIfIndex =
                                        au4PortList[u1ReDirectPortIndex %
                                                    u4NumPorts];

                                    /* Currently MPLS Tunnel Redirection is Not
                                     * being done . Only Ethernet Port /Trunk
                                     *  is being taken care of */

                                    if (pIssL3FilterEntry->RedirectIfGrp.
                                        u4EgressIfIndex <=
                                        SYS_DEF_MAX_PHYSICAL_INTERFACES)
                                    {
                                        pIssL3FilterEntry->RedirectIfGrp.
                                            u1EgressIfType =
                                            ISS_REDIRECT_TO_ETHERNET;

                                    }
                                    else
                                    {
                                        pIssL3FilterEntry->RedirectIfGrp.
                                            u1EgressIfType =
                                            ISS_REDIRECT_TO_TRUNK;
                                    }

                                    if (MEMCMP
                                        (pIssL3FilterEntry->
                                         IssL3FilterInPortList,
                                         gIssNullPortList,
                                         sizeof (pIssL3FilterEntry->
                                                 IssL3FilterInPortList)) != 0)
                                    {
                                        if (ISS_IS_NP_PROGRAMMING_ALLOWED () ==
                                            ISS_TRUE)
                                        {
                                            i4RetVal = IssHwUpdateL3Filter
                                                (pIssL3FilterEntry,
                                                 ISS_L3FILTER_ADD);
                                            if (i4RetVal != FNP_SUCCESS)
                                            {
                                                return;
                                            }
                                        }
                                    }

                                }

                                /* Fill the Redirect Group Id also
                                 * as it will be used in Kepping Tack
                                 * of Same Filter is mapped to this 
                                 * Redirect Group . USeful from CLI 
                                 * Perpective. From SNMP Perspective
                                 * Manager needs to take of ACL-
                                 * Redirect Group Mapping */

                                pIssL3FilterEntry->RedirectIfGrp.
                                    u4RedirectGrpId = u4Index;
                                break;
                            case ISS_UDB_REDIRECT:
                                pIssAclUdbFilterTableEntry =
                                    IssExtGetUdbFilterTableEntry
                                    (gpIssRedirectIntfInfo[u4Index - 1].
                                     u4AclId);

                                if (pIssAclUdbFilterTableEntry == NULL)
                                {
                                    return;
                                }
                                IssHwUpdateUserDefinedFilter
                                    (pIssAclUdbFilterTableEntry->
                                     pAccessFilterEntry, NULL, NULL,
                                     ISS_USERDEFINED_ACCESSFILTER_DELETE);
                                for (u1Index = 0; u1Index <= u1Mask; u1Index++)
                                {

                                    /* For the Redirect Filter Installation 
                                     *  of Port / Trunk fill the parameters
                                     *  of (Interface Type and Interface IfIndex) */

                                    pIssAclUdbFilterTableEntry->
                                        pAccessFilterEntry->RedirectIfGrp.
                                        u2TunnelIfIndex = 0;

                                    pIssAclUdbFilterTableEntry->
                                        pAccessFilterEntry->RedirectIfGrp.
                                        u2TrafficDistByte = u2TrafficDistByte;

                                    pIssAclUdbFilterTableEntry->
                                        pAccessFilterEntry->RedirectIfGrp.
                                        u1TrafficDistMaskLsb =
                                        IssRedirectPortMaskAndIndex[u1Index].
                                        u1PortMaskLsb;

                                    pIssAclUdbFilterTableEntry->
                                        pAccessFilterEntry->
                                        RedirectIfGrp.u1TrafficDistMaskMsb =
                                        IssRedirectPortMaskAndIndex[u1Index].
                                        u1PortMaskMsb;

                                    pIssAclUdbFilterTableEntry->
                                        pAccessFilterEntry->
                                        RedirectIfGrp.u1TrafficDistValueLsb
                                        = IssRedirectPortMaskAndIndex[u1Index].
                                        u1PortValueLsb;

                                    pIssAclUdbFilterTableEntry->
                                        pAccessFilterEntry->
                                        RedirectIfGrp.u1TrafficDistValueMsb
                                        = IssRedirectPortMaskAndIndex[u1Index].
                                        u1PortValueMsb;

                                    u1ReDirectPortIndex =
                                        IssRedirectPortMaskAndIndex[u1Index].
                                        u4PortIndex;

                                    pIssAclUdbFilterTableEntry->
                                        pAccessFilterEntry->RedirectIfGrp.
                                        u4EgressIfIndex =
                                        au4PortList[u1ReDirectPortIndex %
                                                    u4NumPorts];

                                    /* Currently MPLS Tunnel Redirection is Not
                                     * being done . Only Ethernet Port /Trunk
                                     *  is being taken care of */

                                    if (pIssAclUdbFilterTableEntry->
                                        pAccessFilterEntry->RedirectIfGrp.
                                        u4EgressIfIndex <=
                                        SYS_DEF_MAX_PHYSICAL_INTERFACES)
                                    {
                                        pIssAclUdbFilterTableEntry->
                                            pAccessFilterEntry->RedirectIfGrp.
                                            u1EgressIfType =
                                            ISS_REDIRECT_TO_ETHERNET;

                                    }
                                    else
                                    {
                                        pIssAclUdbFilterTableEntry->
                                            pAccessFilterEntry->RedirectIfGrp.
                                            u1EgressIfType =
                                            ISS_REDIRECT_TO_TRUNK;
                                    }

                                    pIssAclUdbFilterTableEntry->
                                        pAccessFilterEntry->RedirectIfGrp.
                                        u4RedirectGrpId = u4Index;

                                    if (MEMCMP
                                        (pIssAclUdbFilterTableEntry->
                                         pAccessFilterEntry->
                                         IssUdbFilterInPortList,
                                         gIssNullPortList,
                                         sizeof (pIssAclUdbFilterTableEntry->
                                                 pAccessFilterEntry->
                                                 IssUdbFilterInPortList)) != 0)
                                    {
                                        if (ISS_IS_NP_PROGRAMMING_ALLOWED () ==
                                            ISS_TRUE)
                                        {
                                            i4RetVal =
                                                IssHwUpdateUserDefinedFilter
                                                (pIssAclUdbFilterTableEntry->
                                                 pAccessFilterEntry, NULL, NULL,
                                                 ISS_USERDEFINED_ACCESSFILTER_ADD);
                                            if (i4RetVal != FNP_SUCCESS)
                                            {
                                                return;
                                            }
                                        }

                                    }

                                }
                                break;
                            default:
                                return;
                        }
                    }
                }

            }

            if (((gpIssRedirectIntfInfo[u4Index - 1].u1RowStatus == ISS_ACTIVE)
                 && (gpIssRedirectIntfInfo[u4Index - 1].u1PortListType ==
                     ISS_REDIRECT_TO_PORT)))
            {

                if (gpIssRedirectIntfInfo[u4Index - 1].u4AclId != 0x0)
                {
                    /* PortList it self Contains Port Number
                     * So Update the Array with Port IfIndex */
                    ISS_MEMSET (au4PortList, 0, sizeof (au4PortList));
                    ISS_MEMCPY
                        (&au4PortList[u4NumPorts],
                         gpIssRedirectIntfInfo
                         [u4Index - 1].PortList, sizeof (UINT4));
                    if (u1OperStatus == CFA_IF_DOWN)
                    {
                        i4PortStatus = ISS_TRUE;
                        u4NoOfRules = 0;

                    }
                    else
                    {
                        u4NoOfRules = 1;
                    }
                    if (u4IfIndex != au4PortList[u4NumPorts])
                    {
                        continue;
                    }
                    u4NumPorts = 1;

                    u1Mask = 0x0;

                    /* If the Port List Type is Port List
                     * Extract the PortList into Port If Indexes */
                    u2TrafficDistByte =
                        gpIssRedirectIntfInfo[u4Index - 1].u2TrafficDistByte;

                    switch (gpIssRedirectIntfInfo[u4Index - 1].u1AclIdType)
                    {

                        case ISS_L2_REDIRECT:
                            pIssL2FilterEntry =
                                IssExtGetL2FilterEntry
                                (gpIssRedirectIntfInfo[u4Index - 1].u4AclId);

                            if (pIssL2FilterEntry == NULL)
                            {
                                return;
                            }

                            IssHwUpdateL2Filter (pIssL2FilterEntry,
                                                 ISS_L2FILTER_DELETE);

                            for (u1Index = 0; u1Index < u4NoOfRules; u1Index++)
                            {
                                /* For the Redirect Filter Installation 
                                 *  of Port / Trunk fill the parameters
                                 *  of (Interface Type and Interface 
                                 *  IfIndex) */

                                pIssL2FilterEntry->RedirectIfGrp.
                                    u2TunnelIfIndex = 0;

                                pIssL2FilterEntry->RedirectIfGrp.
                                    u2TrafficDistByte = u2TrafficDistByte;

                                pIssL2FilterEntry->RedirectIfGrp.
                                    u1TrafficDistMaskLsb =
                                    IssRedirectPortMaskAndIndex[u1Index].
                                    u1PortMaskLsb;

                                pIssL2FilterEntry->RedirectIfGrp.
                                    u1TrafficDistMaskMsb =
                                    IssRedirectPortMaskAndIndex[u1Index].
                                    u1PortMaskMsb;

                                pIssL2FilterEntry->RedirectIfGrp.
                                    u1TrafficDistValueLsb =
                                    IssRedirectPortMaskAndIndex[u1Index].
                                    u1PortValueLsb;

                                pIssL2FilterEntry->RedirectIfGrp.
                                    u1TrafficDistValueMsb =
                                    IssRedirectPortMaskAndIndex[u1Index].
                                    u1PortValueMsb;

                                u1ReDirectPortIndex =
                                    IssRedirectPortMaskAndIndex[u1Index].
                                    u4PortIndex;

                                pIssL2FilterEntry->RedirectIfGrp.
                                    u4EgressIfIndex =
                                    au4PortList[u1ReDirectPortIndex %
                                                u4NumPorts];

                                /* Currently MPLS Tunnel Redirection is Not
                                 * being done . Only Ethernet Port /Trunk
                                 *  is being taken care of */

                                if (pIssL2FilterEntry->RedirectIfGrp.
                                    u4EgressIfIndex <=
                                    SYS_DEF_MAX_PHYSICAL_INTERFACES)
                                {
                                    pIssL2FilterEntry->RedirectIfGrp.
                                        u1EgressIfType =
                                        ISS_REDIRECT_TO_ETHERNET;

                                }
                                else
                                {
                                    pIssL2FilterEntry->RedirectIfGrp.
                                        u1EgressIfType = ISS_REDIRECT_TO_TRUNK;
                                }
                                if (MEMCMP
                                    (pIssL2FilterEntry->
                                     IssL2FilterInPortList,
                                     gIssNullPortList,
                                     sizeof (pIssL2FilterEntry->
                                             IssL2FilterInPortList)) != 0)
                                {
                                    if (ISS_IS_NP_PROGRAMMING_ALLOWED () ==
                                        ISS_TRUE)
                                    {
                                        if (i4PortStatus != ISS_TRUE)
                                        {

                                            i4RetVal =
                                                IssHwUpdateL2Filter
                                                (pIssL2FilterEntry,
                                                 ISS_L2FILTER_ADD);
                                            if (i4RetVal != FNP_SUCCESS)
                                            {
                                                return;
                                            }
                                        }
                                    }
                                }

                            }

                            /* Fill the Redirect Group Id also
                             * as it will be used in Kepping Tack
                             * of Same Filter is mapped to this 
                             * Redirect Group . USeful from CLI 
                             * Perpective. From SNMP Perspective
                             * Manager needs to take of ACL-
                             * Redirect Group Mapping */

                            break;
                        case ISS_L3_REDIRECT:
                            pIssL3FilterEntry =
                                IssExtGetL3FilterEntry
                                (gpIssRedirectIntfInfo[u4Index - 1].u4AclId);

                            if (pIssL3FilterEntry == NULL)
                            {
                                return;
                            }
                            IssHwUpdateL3Filter (pIssL3FilterEntry,
                                                 ISS_L3FILTER_DELETE);
                            for (u1Index = 0; u1Index < u4NoOfRules; u1Index++)
                            {

                                /*  For the Redirect Filter Installation 
                                 *  of Port / Trunk fill the parameters
                                 *  of (Interface Type and Interface IfIndex) */

                                pIssL3FilterEntry->RedirectIfGrp.
                                    u2TunnelIfIndex = 0;

                                pIssL3FilterEntry->RedirectIfGrp.
                                    u2TrafficDistByte = u2TrafficDistByte;

                                pIssL3FilterEntry->RedirectIfGrp.
                                    u1TrafficDistMaskLsb =
                                    IssRedirectPortMaskAndIndex[u1Index].
                                    u1PortMaskLsb;

                                pIssL3FilterEntry->RedirectIfGrp.
                                    u1TrafficDistMaskMsb =
                                    IssRedirectPortMaskAndIndex[u1Index].
                                    u1PortMaskMsb;

                                pIssL3FilterEntry->RedirectIfGrp.
                                    u1TrafficDistValueLsb =
                                    IssRedirectPortMaskAndIndex[u1Index].
                                    u1PortValueLsb;

                                pIssL3FilterEntry->RedirectIfGrp.
                                    u1TrafficDistValueMsb =
                                    IssRedirectPortMaskAndIndex[u1Index].
                                    u1PortValueMsb;

                                u1ReDirectPortIndex =
                                    IssRedirectPortMaskAndIndex[u1Index].
                                    u4PortIndex;;

                                pIssL3FilterEntry->RedirectIfGrp.
                                    u4EgressIfIndex =
                                    au4PortList[u1ReDirectPortIndex %
                                                u4NumPorts];

                                /* Currently MPLS Tunnel Redirection is Not
                                 * being done . Only Ethernet Port /Trunk
                                 *  is being taken care of */

                                if (pIssL3FilterEntry->RedirectIfGrp.
                                    u4EgressIfIndex <=
                                    SYS_DEF_MAX_PHYSICAL_INTERFACES)
                                {
                                    pIssL3FilterEntry->RedirectIfGrp.
                                        u1EgressIfType =
                                        ISS_REDIRECT_TO_ETHERNET;

                                }
                                else
                                {
                                    pIssL3FilterEntry->RedirectIfGrp.
                                        u1EgressIfType = ISS_REDIRECT_TO_TRUNK;
                                }

                                if (MEMCMP
                                    (pIssL3FilterEntry->
                                     IssL3FilterInPortList,
                                     gIssNullPortList,
                                     sizeof (pIssL3FilterEntry->
                                             IssL3FilterInPortList)) != 0)
                                {
                                    if (ISS_IS_NP_PROGRAMMING_ALLOWED () ==
                                        ISS_TRUE)
                                    {
                                        if (i4PortStatus != ISS_TRUE)
                                        {

                                            i4RetVal = IssHwUpdateL3Filter
                                                (pIssL3FilterEntry,
                                                 ISS_L3FILTER_ADD);
                                            if (i4RetVal != FNP_SUCCESS)
                                            {
                                                return;
                                            }
                                        }
                                    }
                                }

                            }

                            /* Fill the Redirect Group Id also
                             * as it will be used in Kepping Tack
                             * of Same Filter is mapped to this 
                             * Redirect Group . USeful from CLI 
                             * Perpective. From SNMP Perspective
                             * Manager needs to take of ACL-
                             * Redirect Group Mapping */

                            pIssL3FilterEntry->RedirectIfGrp.
                                u4RedirectGrpId = u4Index;
                            break;
                        case ISS_UDB_REDIRECT:
                            pIssAclUdbFilterTableEntry =
                                IssExtGetUdbFilterTableEntry
                                (gpIssRedirectIntfInfo[u4Index - 1].u4AclId);

                            if (pIssAclUdbFilterTableEntry == NULL)
                            {
                                return;
                            }
                            IssHwUpdateUserDefinedFilter
                                (pIssAclUdbFilterTableEntry->
                                 pAccessFilterEntry, NULL, NULL,
                                 ISS_USERDEFINED_ACCESSFILTER_DELETE);
                            for (u1Index = 0; u1Index <= u1Mask; u1Index++)
                            {

                                /* For the Redirect Filter Installation 
                                 *  of Port / Trunk fill the parameters
                                 *  of (Interface Type and Interface IfIndex) */

                                pIssAclUdbFilterTableEntry->
                                    pAccessFilterEntry->RedirectIfGrp.
                                    u2TunnelIfIndex = 0;

                                pIssAclUdbFilterTableEntry->
                                    pAccessFilterEntry->RedirectIfGrp.
                                    u2TrafficDistByte = u2TrafficDistByte;

                                pIssAclUdbFilterTableEntry->
                                    pAccessFilterEntry->RedirectIfGrp.
                                    u1TrafficDistMaskLsb =
                                    IssRedirectPortMaskAndIndex[u1Index].
                                    u1PortMaskLsb;

                                pIssAclUdbFilterTableEntry->
                                    pAccessFilterEntry->
                                    RedirectIfGrp.u1TrafficDistMaskMsb =
                                    IssRedirectPortMaskAndIndex[u1Index].
                                    u1PortMaskMsb;

                                pIssAclUdbFilterTableEntry->
                                    pAccessFilterEntry->
                                    RedirectIfGrp.u1TrafficDistValueLsb
                                    = IssRedirectPortMaskAndIndex[u1Index].
                                    u1PortValueLsb;

                                pIssAclUdbFilterTableEntry->
                                    pAccessFilterEntry->
                                    RedirectIfGrp.u1TrafficDistValueMsb
                                    = IssRedirectPortMaskAndIndex[u1Index].
                                    u1PortValueMsb;

                                u1ReDirectPortIndex =
                                    IssRedirectPortMaskAndIndex[u1Index].
                                    u4PortIndex;

                                pIssAclUdbFilterTableEntry->
                                    pAccessFilterEntry->RedirectIfGrp.
                                    u4EgressIfIndex =
                                    au4PortList[u1ReDirectPortIndex %
                                                u4NumPorts];

                                /* Currently MPLS Tunnel Redirection is Not
                                 * being done . Only Ethernet Port /Trunk
                                 *  is being taken care of */

                                if (pIssAclUdbFilterTableEntry->
                                    pAccessFilterEntry->RedirectIfGrp.
                                    u4EgressIfIndex <=
                                    SYS_DEF_MAX_PHYSICAL_INTERFACES)
                                {
                                    pIssAclUdbFilterTableEntry->
                                        pAccessFilterEntry->RedirectIfGrp.
                                        u1EgressIfType =
                                        ISS_REDIRECT_TO_ETHERNET;

                                }
                                else
                                {
                                    pIssAclUdbFilterTableEntry->
                                        pAccessFilterEntry->RedirectIfGrp.
                                        u1EgressIfType = ISS_REDIRECT_TO_TRUNK;
                                }

                                pIssAclUdbFilterTableEntry->
                                    pAccessFilterEntry->RedirectIfGrp.
                                    u4RedirectGrpId = u4Index;

                                if (MEMCMP
                                    (pIssAclUdbFilterTableEntry->
                                     pAccessFilterEntry->
                                     IssUdbFilterInPortList,
                                     gIssNullPortList,
                                     sizeof (pIssAclUdbFilterTableEntry->
                                             pAccessFilterEntry->
                                             IssUdbFilterInPortList)) != 0)
                                {
                                    if (ISS_IS_NP_PROGRAMMING_ALLOWED () ==
                                        ISS_TRUE)
                                    {
                                        if (i4PortStatus != ISS_TRUE)
                                        {

                                            i4RetVal =
                                                IssHwUpdateUserDefinedFilter
                                                (pIssAclUdbFilterTableEntry->
                                                 pAccessFilterEntry, NULL,
                                                 NULL,
                                                 ISS_USERDEFINED_ACCESSFILTER_ADD);
                                            if (i4RetVal != FNP_SUCCESS)
                                            {
                                                return;
                                            }
                                        }
                                    }

                                }

                            }
                            break;
                        default:
                            return;
                    }
                }
            }
        }
        gIssExGlobalInfo.u4PriorityTableSet = ISS_TRUE;
        if (u1IsMember == ISS_FALSE)
        {
            /*Thie means that port is not member of any interface group, so nothing to do */
            return;
        }
    }
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1IfType);
    UNUSED_PARAM (u1OperStatus);
#endif
    return;
}

/****************************************************************************
 *                                                                          *
 *     Function Name : IssIsMemberPort                                      *
 *                                                                          *
 *     Description   : The function is used to check if a the bit for the   *
 *                     given port number is set or not.                     *
 *                                                                          *
 *     Input(s)      : pu1PortArray    : Pointer to the port list array.    *
 *                     u4PortArrayLen  : Length of the array.               *
 *                     u4Port          : Port number to check.              *
 *                                                                          *
 *     Output(s)     : NULL                                                 *
 *                                                                          *
 *     Returns       : ISS_SUCCESS, if u4Port bit is set                   *
 *                     else ISS_FAILURE.                                   *
 *                                                                          *
 ****************************************************************************/

INT4
IssIsMemberPort (UINT1 *pu1PortArray, UINT4 u4PortArrayLen, UINT4 u4Port)
{
    UINT4               u4PortBytePos = 0;
    UINT4               u4PortBitPos = 0;
    INT4                i4RetVal = ISS_FAILURE;

    u4PortBytePos = (UINT4) (u4Port / ISS_REDIRECT_PORTS_PER_BYTE);
    u4PortBitPos = (UINT4) (u4Port % ISS_REDIRECT_PORTS_PER_BYTE);

    if (u4PortBitPos == 0)
    {
        u4PortBytePos -= 1;
    }
    if ((pu1PortArray) && (u4PortBytePos < u4PortArrayLen) &&
        ((pu1PortArray[u4PortBytePos] & gau1IssPortBitMask[u4PortBitPos]) != 0))
    {
        i4RetVal = ISS_SUCCESS;
    }
    return i4RetVal;
}

/****************************************************************************
 *                                                                          *
 *     Function Name : IssInterfaceGrpRedirectIdNextFree                    *
 *                                                                          *
 *     Description   : The function is used to get the next free index for  *
 *                      the redirect interface group.                       *
 *                                                                          *
 *     Input(s)      : pu4RetValIssRedirectInterfaceGrpIdNextFree           *
 *                                                                          *
 *     Output(s)     : NULL                                                 *
 *                                                                          *
 *     Returns       :  NONE                                                *
 *                                                                          *
 *                                                                          *
 ****************************************************************************/

VOID
IssInterfaceGrpRedirectIdNextFree (UINT4
                                   *pu4RetValIssRedirectInterfaceGrpIdNextFree)
{
    UINT4               u4Index = 0;

    /* Calculate the Redirect Group Index to be Used */

    for (u4Index = 0; u4Index < ISS_MAX_REDIRECT_GRP_ID; u4Index++)
    {
        if (gpIssRedirectIntfInfo[u4Index].u1RowStatus == 0)

        {
            *pu4RetValIssRedirectInterfaceGrpIdNextFree = u4Index + 1;
            break;
        }
    }
    return;
}

/****************************************************************************
 *                                                                          *
 *     Function Name : IssExAddAclToPriorityTable                           *
 *                                                                          *
 *     Description   : The function is used to add filter in priority table *
 *                                                                          *
 *                                                                          *
 *     Input(s)      : i4Priority,pFilterNode,u1FilterType                  *
 *                                                                          *
 *     Output(s)     : NULL                                                 *
 *                                                                          *
 *     Returns       :  NONE                                                *
 *                                                                          *
 *                                                                          *
 ****************************************************************************/

VOID
IssExAddAclToPriorityTable (INT4 i4Priority, tTMO_SLL_NODE * pFilterNode,
                            UINT1 u1FilterType)
{
    gIssExGlobalInfo.u4PriorityTableSet = ISS_TRUE;
    i4Priority = i4Priority - 1;

    switch (u1FilterType)
    {
        case ISS_L2FILTER:
        case ISS_L2_REDIRECT:

            ((tIssL2FilterEntry *) pFilterNode)->
                PriorityNode.u1FilterType = u1FilterType;
            TMO_DLL_Add (&(gaPriorityTable[i4Priority]->PriFilterList),
                         &((tIssL2FilterEntry *) pFilterNode)->PriorityNode.
                         PriNode);
            break;

        case ISS_L3FILTER:
        case ISS_L3_REDIRECT:

            ((tIssL3FilterEntry *) pFilterNode)->
                PriorityNode.u1FilterType = u1FilterType;
            TMO_DLL_Add (&(gaPriorityTable[i4Priority]->PriFilterList),
                         &((tIssL3FilterEntry *) pFilterNode)->PriorityNode.
                         PriNode);
            break;

        case ISS_UDBFILTER:
        case ISS_UDB_REDIRECT:

            ((tIssUserDefinedFilterTable *) pFilterNode)->
                PriorityNode.u1FilterType = u1FilterType;
            TMO_DLL_Add (&(gaPriorityTable[i4Priority]->PriFilterList),
                         &((tIssUserDefinedFilterTable *) pFilterNode)->
                         PriorityNode.PriNode);
            break;
    }

    return;
}

/****************************************************************************
 *                                                                          *
 *     Function Name : IssExDeleteAclPriorityFilterTable                    *
 *                                                                          *
 *     Description   : The function is used to delete filter from           *
 *                      priority table                                      *
 *                                                                          *
 *                                                                          *
 *     Input(s)      : i4Priority,pFilterNode,u1FilterType                  *
 *                                                                          *
 *     Output(s)     : NULL                                                 *
 *                                                                          *
 *     Returns       :  NONE                                                *
 *                                                                          *
 *                                                                          *
 ****************************************************************************/
VOID
IssExDeleteAclPriorityFilterTable (INT4 i4Priority, tTMO_SLL_NODE * pFilterNode,
                                   UINT1 u1FilterType)
{
    gIssExGlobalInfo.u4PriorityTableSet = ISS_TRUE;
    i4Priority = i4Priority - 1;

    switch (u1FilterType)
    {
        case ISS_L2FILTER:
        case ISS_L2_REDIRECT:

            TMO_DLL_Delete (&(gaPriorityTable[i4Priority]->
                              PriFilterList),
                            &((tIssL2FilterEntry *)
                              pFilterNode)->PriorityNode.PriNode);
            break;

        case ISS_L3FILTER:
        case ISS_L3_REDIRECT:

            TMO_DLL_Delete (&(gaPriorityTable[i4Priority]->
                              PriFilterList),
                            &((tIssL3FilterEntry *) pFilterNode)->
                            PriorityNode.PriNode);
            break;
        case ISS_UDBFILTER:
        case ISS_UDB_REDIRECT:

            TMO_DLL_Delete (&(gaPriorityTable[i4Priority]->PriFilterList),
                            &((tIssUserDefinedFilterTable *) pFilterNode)->
                            PriorityNode.PriNode);
            break;
    }

    return;
}

/****************************************************************************
 *                                                                          *
 *     Function Name : IssExGetPriorityFilterTableCount                     *
 *                                                                          *
 *     Description   : The function is used to give no of filters from      *
 *                      priority table                                      *
 *                                                                          *
 *                                                                          *
 *     Input(s)      : i4Priority,pFilterNode,u1FilterType                  *
 *                                                                          *
 *     Output(s)     : NULL                                                 *
 *                                                                          *
 *     Returns       :  NONE                                                *
 *                                                                          *
 *                                                                          *
 ****************************************************************************/
VOID
IssExGetPriorityFilterTableCount (UINT4 *pu4PriFilterCount)
{
    INT4                i4Priority = 0;
    UINT4               u4FilterCount = 0;
    UINT4               u4PriEntries = 0;
    while (i4Priority != ISS_MAX_FILTER_PRIORITY)
    {
        u4FilterCount =
            TMO_DLL_Count (&gaPriorityTable[i4Priority]->PriFilterList);
        if (u4FilterCount > 0)
        {
            u4PriEntries = u4PriEntries + u4FilterCount;
        }
        i4Priority++;
    }
    *pu4PriFilterCount = u4PriEntries;
    return;
}

/****************************************************************************
 *                                                                          *
 *     Function Name : IssExGetIssCommitSupportImmediate                    *
 *                                                                          *
 *     Description   : This function specifies if Immediate is configured   *
 *                                                                          *
 *                                                                          *
 *     Input(s)      : NULL                                                 *
 *                                                                          *
 *     Output(s)     : NULL                                                 *
 *                                                                          *
 *     Returns       : NONE                                                 *
 *                                                                          *
 *                                                                          *
 ****************************************************************************/
INT4
IssExGetIssCommitSupportImmediate (VOID)
{
    if (gIssSysGroupInfo.i4ProvisionMode == ISS_IMMEDIATE)
    {
        return ISS_TRUE;
    }
    return ISS_FALSE;

}

/****************************************************************************
 *                                                                          *
 *     Function Name : IssGetTriggerCommit                                  *
 *                                                                          *
 *     Description   : This function specifies if trigger commit is true    *
 *                                                                          *
 *                                                                          *
 *     Input(s)      : NULL                                                 *
 *                                                                          *
 *     Output(s)     : NULL                                                 *
 *                                                                          *
 *     Returns       : NONE                                                 *
 *                                                                          *
 *                                                                          *
 ****************************************************************************/
INT4
IssGetTriggerCommit (VOID)
{

    if (gIssSysGroupInfo.i4TriggerCommit == ISS_COMMIT_ACTION_TRUE)
    {

        return ISS_TRUE;

    }
    return ISS_FALSE;
}

/****************************************************************************
 *                                                                          *
 *     Function Name : IssExPrgAclsToNpWithPriority                         *
 *                                                                          *
 *     Description   : This function programs the h/w based on priority     *
 *                     table.                                               *
 *                                                                          *
 *                                                                          *
 *     Input(s)      : NULL                                                 *
 *                                                                          *
 *     Output(s)     : NULL                                                 *
 *                                                                          *
 *     Returns       :  NONE                                                *
 *                                                                          *
 *                                                                          *
 ****************************************************************************/

INT4
IssExPrgAclsToNpWithPriority (VOID)
{

    INT4                i4Priority = 0;
    UINT4               u4RedirectIndex = 0;
    UINT4               u4FilterCount = 0;
    UINT4               u4CheckCount = 0;
    tIssPriorityFilterEntry *pPriCurrentNode = NULL;
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    IssExGetPriorityFilterTableCount (&u4FilterCount);
    if (u4FilterCount > 0 && gIssExGlobalInfo.u4PriorityTableSet == ISS_TRUE)
    {
        while (i4Priority != ISS_MAX_FILTER_PRIORITY)
        {
            TMO_DLL_Scan (&(gaPriorityTable[i4Priority]->
                            PriFilterList), pPriCurrentNode,
                          tIssPriorityFilterEntry *)
            {

                switch (pPriCurrentNode->u1FilterType)
                {
                    case ISS_L2FILTER:
                        pIssL2FilterEntry = (VOID *) (UINT1 *) pPriCurrentNode -
                            FSAP_OFFSETOF (tIssL2FilterEntry,
                                           PriorityNode.PriNode);

                        pIssL2FilterEntry->u1PriorityFlag = ISS_TRUE;
                        nmhSetIssAclL2FilterStatus (pIssL2FilterEntry->
                                                    i4IssL2FilterNo,
                                                    ISS_NOT_IN_SERVICE);
                        break;
                    case ISS_L2_REDIRECT:

                        pIssL2FilterEntry = (VOID *) (UINT1 *) pPriCurrentNode -
                            FSAP_OFFSETOF (tIssL2FilterEntry,
                                           PriorityNode.PriNode);
                        pIssL2FilterEntry->RedirectIfGrp.
                            u1PriorityFlag = ISS_TRUE;

                        nmhGetIssAclL2FilterRedirectId (pIssL2FilterEntry->
                                                        i4IssL2FilterNo,
                                                        (INT4 *)
                                                        &u4RedirectIndex);
                        if (u4RedirectIndex != 0)
                        {
                            nmhSetIssRedirectInterfaceGrpStatus
                                (u4RedirectIndex, ISS_NOT_IN_SERVICE);
                        }
                        break;

                    case ISS_L3FILTER:

                        pIssL3FilterEntry = (VOID *) (UINT1 *) pPriCurrentNode -
                            FSAP_OFFSETOF (tIssL3FilterEntry,
                                           PriorityNode.PriNode);
                        pIssL3FilterEntry->u1PriorityFlag = ISS_TRUE;
                        nmhSetIssAclL3FilterStatus (pIssL3FilterEntry->
                                                    i4IssL3FilterNo,
                                                    ISS_NOT_IN_SERVICE);
                        break;
                    case ISS_L3_REDIRECT:

                        pIssL3FilterEntry = (VOID *) (UINT1 *) pPriCurrentNode -
                            FSAP_OFFSETOF (tIssL3FilterEntry,
                                           PriorityNode.PriNode);
                        pIssL3FilterEntry->RedirectIfGrp.
                            u1PriorityFlag = ISS_TRUE;

                        nmhGetIssAclL3FilterRedirectId (pIssL3FilterEntry->
                                                        i4IssL3FilterNo,
                                                        (INT4 *)
                                                        &u4RedirectIndex);
                        if (u4RedirectIndex != 0)
                        {
                            nmhSetIssRedirectInterfaceGrpStatus
                                (u4RedirectIndex, ISS_NOT_IN_SERVICE);
                        }
                        break;
                    case ISS_UDBFILTER:

                        pIssAclUdbFilterTableEntry =
                            (VOID *) (UINT1 *) pPriCurrentNode -
                            FSAP_OFFSETOF (tIssUserDefinedFilterTable,
                                           PriorityNode.PriNode);
                        pIssAclUdbFilterTableEntry->u1PriorityFlag = ISS_TRUE;
                        nmhSetIssAclUserDefinedFilterStatus
                            (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                             u4UDBFilterId, ISS_NOT_IN_SERVICE);
                        break;

                    case ISS_UDB_REDIRECT:

                        pIssAclUdbFilterTableEntry =
                            (VOID *) (UINT1 *) pPriCurrentNode -
                            FSAP_OFFSETOF (tIssUserDefinedFilterTable,
                                           PriorityNode.PriNode);
                        pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                            RedirectIfGrp.u1PriorityFlag = ISS_TRUE;

                        nmhGetIssAclUserDefinedFilterRedirectId
                            (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                             u4UDBFilterId, (INT4 *) &u4RedirectIndex);
                        if (u4RedirectIndex != 0)
                        {
                            nmhSetIssRedirectInterfaceGrpStatus
                                (u4RedirectIndex, ISS_NOT_IN_SERVICE);
                        }
                        break;

                }

                if (gaPriorityTable[i4Priority]->PriFilterList.u4_Count ==
                    u4CheckCount)
                {
                    u4CheckCount = 0;
                    break;

                }
                u4CheckCount++;
            }
            i4Priority++;
        }
        u4CheckCount = 0;
        i4Priority = 0;

        while (i4Priority != ISS_MAX_FILTER_PRIORITY)
        {
            pPriCurrentNode = NULL;
            TMO_DLL_Scan (&(gaPriorityTable[i4Priority]->
                            PriFilterList), pPriCurrentNode,
                          tIssPriorityFilterEntry *)
            {

                switch (pPriCurrentNode->u1FilterType)
                {
                    case ISS_L2FILTER:

                        pIssL2FilterEntry = (VOID *) (UINT1 *) pPriCurrentNode -
                            FSAP_OFFSETOF (tIssL2FilterEntry,
                                           PriorityNode.PriNode);
                        pIssL2FilterEntry->u1PriorityFlag = ISS_TRUE;
                        nmhSetIssAclL2FilterStatus (pIssL2FilterEntry->
                                                    i4IssL2FilterNo,
                                                    ISS_ACTIVE);
                        break;
                    case ISS_L2_REDIRECT:

                        pIssL2FilterEntry = (VOID *) (UINT1 *) pPriCurrentNode -
                            FSAP_OFFSETOF (tIssL2FilterEntry,
                                           PriorityNode.PriNode);
                        pIssL2FilterEntry->RedirectIfGrp.
                            u1PriorityFlag = ISS_TRUE;

                        nmhGetIssAclL2FilterRedirectId (pIssL2FilterEntry->
                                                        i4IssL2FilterNo,
                                                        (INT4 *)
                                                        &u4RedirectIndex);
                        if (u4RedirectIndex != 0)
                        {
                            nmhSetIssRedirectInterfaceGrpStatus
                                (u4RedirectIndex, ISS_ACTIVE);
                        }
                        break;

                    case ISS_L3FILTER:
                        pIssL3FilterEntry = (VOID *) (UINT1 *) pPriCurrentNode -
                            FSAP_OFFSETOF (tIssL3FilterEntry,
                                           PriorityNode.PriNode);

                        pIssL3FilterEntry->u1PriorityFlag = ISS_TRUE;
                        nmhSetIssAclL3FilterStatus (pIssL3FilterEntry->
                                                    i4IssL3FilterNo,
                                                    ISS_ACTIVE);
                        break;
                    case ISS_L3_REDIRECT:

                        pIssL3FilterEntry = (VOID *) (UINT1 *) pPriCurrentNode -
                            FSAP_OFFSETOF (tIssL3FilterEntry,
                                           PriorityNode.PriNode);
                        pIssL3FilterEntry->RedirectIfGrp.
                            u1PriorityFlag = ISS_TRUE;

                        nmhGetIssAclL3FilterRedirectId (pIssL3FilterEntry->
                                                        i4IssL3FilterNo,
                                                        (INT4 *)
                                                        &u4RedirectIndex);
                        if (u4RedirectIndex != 0)
                        {
                            nmhSetIssRedirectInterfaceGrpStatus
                                (u4RedirectIndex, ISS_ACTIVE);
                        }
                        break;
                    case ISS_UDBFILTER:
                        pIssAclUdbFilterTableEntry =
                            (VOID *) (UINT1 *) pPriCurrentNode -
                            FSAP_OFFSETOF (tIssUserDefinedFilterTable,
                                           PriorityNode.PriNode);

                        pIssAclUdbFilterTableEntry->u1PriorityFlag = ISS_TRUE;
                        nmhSetIssAclUserDefinedFilterStatus
                            (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                             u4UDBFilterId, ISS_ACTIVE);
                        break;

                    case ISS_UDB_REDIRECT:
                        pIssAclUdbFilterTableEntry =
                            (VOID *) (UINT1 *) pPriCurrentNode -
                            FSAP_OFFSETOF (tIssUserDefinedFilterTable,
                                           PriorityNode.PriNode);

                        pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                            RedirectIfGrp.u1PriorityFlag = ISS_TRUE;

                        nmhGetIssAclUserDefinedFilterRedirectId
                            (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                             u4UDBFilterId, (INT4 *) &u4RedirectIndex);
                        if (u4RedirectIndex != 0)
                        {
                            nmhSetIssRedirectInterfaceGrpStatus
                                (u4RedirectIndex, ISS_ACTIVE);
                        }
                        break;

                }
                if (gaPriorityTable[i4Priority]->PriFilterList.u4_Count ==
                    u4CheckCount)
                {
                    u4CheckCount = 0;
                    break;
                }
                u4CheckCount++;

            }
            i4Priority++;
        }
        gIssExGlobalInfo.u4PriorityTableSet = ISS_FALSE;
    }

    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssACLCreateFilter                                   */
/*                                                                           */
/* Description        : This function is called from the other modules       */
/*                      to create L2, L3 and user defined filter.            */
/*                                                                           */
/* Input(s)           : pAclFilterInfo - Pointer to ACL filter information   */
/*                                                                           */
/* Output(s)          : pu4L2FilterId  - Pointer to L2 filter ID             */
/*                      pu4L3FilterId  - Pointer to L3 filter ID             */
/*                      pu4UDBFilterId - Pointer to User defined filter ID   */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
IssACLCreateFilter (tAclFilterInfo * pAclFilterInfo, UINT4 *pu4L2FilterId,
                    UINT4 *pu4L3FilterId, UINT4 *pu4UDBFilterId)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    UINT4               u4L2FilterId = ISS_ZERO_ENTRY;
    UINT4               u4L3FilterId = ISS_ZERO_ENTRY;
    UINT4               u4UDBFilterId = ISS_ZERO_ENTRY;

    *pu4L2FilterId = ISS_ZERO_ENTRY;
    *pu4L3FilterId = ISS_ZERO_ENTRY;
    *pu4UDBFilterId = ISS_ZERO_ENTRY;

    ISS_LOCK ();

    if (pAclFilterInfo->u1FilterType == ISS_L3FILTER)
    {
        /* Get a valid L3 filter ID by scanning through the 
         * L3 filter table */
        IssAclGetValidL3FilterId (&u4L3FilterId);

        /* Update the L3 filter Entry from the pAclFilterInfo and install 
         * the L3 filter entry */
        if (IssAclUpdateL3FilterEntry (pAclFilterInfo, u4L3FilterId)
            == ISS_FAILURE)
        {
            ISS_UNLOCK ();
            return ISS_FAILURE;
        }

        *pu4L3FilterId = u4L3FilterId;
    }

    if (pAclFilterInfo->u1FilterType == ISS_UDBFILTER)
    {
        /* Get a valid L2 filter ID by scanning through the 
         * L2 filter table */
        IssAclGetValidL2FilterId (&u4L2FilterId);

        /* Update the L2 filter Entry from the pAclFilterInfo and install 
         * the L2 filter entry */
        if (IssAclUpdateL2FilterEntry (pAclFilterInfo, u4L2FilterId)
            == ISS_FAILURE)
        {
            ISS_UNLOCK ();
            return ISS_FAILURE;
        }

        /* Get a valid L3 filter ID by scanning through the 
         * L3 filter table */
        IssAclGetValidL3FilterId (&u4L3FilterId);

        /* Update the L3 filter Entry from the pAclFilterInfo and install 
         * the L3 filter entry */
        if (IssAclUpdateL3FilterEntry (pAclFilterInfo, u4L3FilterId)
            == ISS_FAILURE)
        {
            /*Uninstall the already created L2Filter entry as installation of 
             * L3 filter entry fails */
            if ((pIssL2FilterEntry = IssExtGetL2FilterEntry (u4L2FilterId))
                != NULL)
            {
                IssAclUnInstallL2Filter (pIssL2FilterEntry);
            }
            ISS_UNLOCK ();
            return ISS_FAILURE;
        }

        /* Get a valid User-defined filter ID by scanning through the 
         * user-defined filter table */
        IssAclGetValidUDBFilterId (&u4UDBFilterId);

        /* Update the user-defined filter Entry from the pAclFilterInfo and 
         * install the user-defined filter entry */
        if (IssAclUpdateUDBFilterEntry (pAclFilterInfo, u4L2FilterId,
                                        u4L3FilterId, u4UDBFilterId)
            == ISS_FAILURE)
        {
            /*Uninstall the already created L2 & L3 Filter entry as 
             * installation of user-defined filter entry fails */
            if ((pIssL2FilterEntry = IssExtGetL2FilterEntry (u4L2FilterId))
                != NULL)
            {
                IssAclUnInstallL2Filter (pIssL2FilterEntry);
            }
            if ((pIssL3FilterEntry = IssExtGetL3FilterEntry (u4L3FilterId))
                != NULL)
            {
                IssAclUnInstallL3Filter (pIssL3FilterEntry);
            }
            ISS_UNLOCK ();
            return ISS_FAILURE;
        }

        *pu4L2FilterId = u4L2FilterId;
        *pu4L3FilterId = u4L3FilterId;
        *pu4UDBFilterId = u4UDBFilterId;
    }
    ISS_UNLOCK ();
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssACLDeleteFilter                                   */
/*                                                                           */
/* Description        : This function is called from the other modules       */
/*                      to delete L2, L3 and user defined filter.            */
/*                                                                           */
/* Input(s)           : u1FilterType - Filter Type (L2/L3/User-defined)      */
/*                      u4L2FilterId - L2 filter ID                          */
/*                      u4L3FilterId - L3 filter ID                          */
/*                      u4UserDefFilterId - User defined filter ID           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
IssACLDeleteFilter (UINT1 u1FilterType, UINT4 u4L2FilterId,
                    UINT4 u4L3FilterId, UINT4 u4UserDefFilterId)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    tIssUserDefinedFilterTable *pIssUserDefFilterEntry = NULL;

    ISS_LOCK ();

    if (u1FilterType == ISS_L3FILTER)
    {
        /* Delete the L3 filter entry if the entry exists */
        if ((pIssL3FilterEntry = IssExtGetL3FilterEntry (u4L3FilterId)) != NULL)
        {
            IssAclUnInstallL3Filter (pIssL3FilterEntry);
        }
    }

    if (u1FilterType == ISS_UDBFILTER)
    {
        /* Delete the L2 filter entry if the entry exists */
        if ((pIssL2FilterEntry = IssExtGetL2FilterEntry (u4L2FilterId)) != NULL)
        {
            IssAclUnInstallL2Filter (pIssL2FilterEntry);
        }

        /* Delete the L3 filter entry if the entry exists */
        if ((pIssL3FilterEntry = IssExtGetL3FilterEntry (u4L3FilterId)) != NULL)
        {
            IssAclUnInstallL3Filter (pIssL3FilterEntry);
        }

        /* Delete the user defined filter entry if the entry exists */
        if ((pIssUserDefFilterEntry =
             IssExtGetUdbFilterTableEntry ((INT4) u4UserDefFilterId)) != NULL)
        {
            IssAclUnInstallUserDefFilter (pIssUserDefFilterEntry);
        }
    }

    ISS_UNLOCK ();

    return;
}

/*****************************************************************************/
/* Function Name      : IssACLApiModifyFilterEntry                           */
/*                                                                           */
/* Description        : This function is called from the DCBx Application    */
/*                      priority module to configure the priority for        */
/*                      a protocol.                                          */
/*                                                                           */
/* Input(s)           :                                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
IssACLApiModifyFilterEntry (tIssAclHwFilterInfo * pAppPriAclInfo,
                            UINT1 u1FilterType, UINT1 u1Status)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    tMacAddr            NullMacAddress = { 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00
    };
    tIssPortList        PortList;
    tAclFilterInfo      AclFilterInfo;
    tIssL2FilterEntry   IssL2FilterEntry;
    tIssL3FilterEntry   IssL3FilterEntry;
    UINT4               u4L2FilterId = 0;
    UINT4               u4L3FilterId = 0;
    UINT1               u1MatchFound = FALSE;
    INT1                i1Result = OSIX_FALSE;

    MEMSET (&IssL2FilterEntry, 0, sizeof (tIssL2FilterEntry));
    MEMSET (&IssL3FilterEntry, 0, sizeof (tIssL3FilterEntry));
    MEMSET (&AclFilterInfo, 0, sizeof (tAclFilterInfo));
    MEMSET (&PortList, 0, sizeof (tIssPortList));

    ISS_TRC (MGMT_TRC, "ACL Debug Traces *******....... !!! \n");

    if (u1FilterType == ISS_L2_FILTER)
    {
        /* Scan the entire L2 Filter List and find the exact match 
         * for the incoming protocol id and port no */

        /* If Match is found , then return the appropriate Filter Id */
        /* If Match is not found, get a valid filter Id and create a L2 Filter
         * with the entries present in pAppPriAclInfo */

        ISS_TRC_ARG1 (MGMT_TRC, "There are %u entries in ISS_L2FILTER_LIST \n",
                      TMO_SLL_Count (&(ISS_L2FILTER_LIST)));

        TMO_SLL_Scan (&(ISS_L2FILTER_LIST), pIssL2FilterEntry,
                      tIssL2FilterEntry *)
        {

            /* If the other entries except the incoming entries
             * are configured then continue to the next entry */
            if ((pIssL2FilterEntry->i4IssL2FilterEncapType) != 0)
            {
                continue;
            }

            if ((pIssL2FilterEntry->u4IssL2FilterCustomerVlanId) != 0)
            {
                continue;
            }

            if ((pIssL2FilterEntry->u4IssL2FilterServiceVlanId) != 0)
            {
                continue;
            }

            if ((MEMCMP (pIssL2FilterEntry->IssL2FilterDstMacAddr,
                         NullMacAddress, MAC_ADDR_LEN)) != 0)
            {
                continue;
            }

            if ((MEMCMP (pIssL2FilterEntry->IssL2FilterSrcMacAddr,
                         NullMacAddress, MAC_ADDR_LEN)) != 0)
            {
                continue;
            }

            if (MEMCMP (pIssL2FilterEntry->IssL2FilterOutPortList, PortList,
                        sizeof (tIssPortList)) != 0)
            {
                continue;
            }

            if ((pIssL2FilterEntry->u2InnerEtherType) != 0)
            {
                continue;
            }

            if ((pIssL2FilterEntry->u2OuterEtherType) != 0)
            {
                continue;
            }

            /* If the incoming protocol Id is not equal to the L2entry's 
             * protocol Id then continue to the next entry */
            if (pIssL2FilterEntry->u4IssL2FilterProtocolType != 0)
            {
                if (pAppPriAclInfo->u4Protocol ==
                    pIssL2FilterEntry->u4IssL2FilterProtocolType)
                {
                    u1MatchFound = TRUE;
                    break;
                }
                ISS_TRC_ARG3 (MGMT_TRC, "In %s :Incoming protocol Id %u "
                              "matches with L2 Filter List's Protocol Id %u\n",
                              __FUNCTION__, pAppPriAclInfo->u4Protocol,
                              pIssL2FilterEntry->u4IssL2FilterProtocolType);
            }
        }

        /* If the incoming AppPriEntry matches with an already created L2 Filter
         * then return the appropriate Filter Id.
         * If no match is found create a filter in ACL module with the incoming 
         * entries */

        if (u1MatchFound == TRUE)
        {
            pAppPriAclInfo->u4FilterId = pIssL2FilterEntry->i4IssL2FilterNo;
            pAppPriAclInfo->u1FilterAction = ISS_FILTER_UPDATE;
            pIssL2FilterEntry->u1FilterDirection = ISS_DIRECTION_IN;
            pIssL2FilterEntry->u1IssL2FilterStatus = ISS_NOT_READY;
            pIssL2FilterEntry->u1IssL2FilterTagType = pAppPriAclInfo->u1TagType;

            if (u1Status == TRUE)
            {
                /* if Filter is to be updated */

                /* Check if the Port is member of PortList on which the Filter is 
                 * created*/
                OSIX_BITLIST_IS_BIT_SET (pIssL2FilterEntry->
                                         IssL2FilterInPortList,
                                         pAppPriAclInfo->u4PortNo,
                                         sizeof (tIssPortList), i1Result);
                if (i1Result == OSIX_FALSE)
                {
                    /* Set Port in FilterEntry's InPortList */
                    OSIX_BITLIST_SET_BIT (pIssL2FilterEntry->
                                          IssL2FilterInPortList,
                                          pAppPriAclInfo->u4PortNo,
                                          sizeof (tIssPortList));
                }

                /*Install the Filter */
                if (IssAclInstallL2Filter (pIssL2FilterEntry) == ISS_FAILURE)
                {
                    ISS_TRC_ARG1 (MGMT_TRC, "In %s: IssAclInstallL2Filter "
                                  "returned Failure\r\n", __FUNCTION__);
                    return ISS_FAILURE;
                }
            }
            else
            {
                /* If Filter is to be deleted */
                OSIX_BITLIST_IS_BIT_SET (pIssL2FilterEntry->
                                         IssL2FilterInPortList,
                                         pAppPriAclInfo->u4PortNo,
                                         sizeof (tIssPortList), i1Result);
                if (i1Result == OSIX_FALSE)
                {
                    /*If Filter is not present, return */
                    return ISS_SUCCESS;
                }

                OSIX_BITLIST_RESET_BIT (pIssL2FilterEntry->
                                        IssL2FilterInPortList,
                                        pAppPriAclInfo->u4PortNo,
                                        sizeof (tIssPortList));

                /* If no other port is using this filter, un-install this filter */
                if (MEMCMP (pIssL2FilterEntry->IssL2FilterOutPortList, PortList,
                            sizeof (tIssPortList)) == 0)
                {
                    pAppPriAclInfo->u1FilterAction = ISS_FILTER_DELETE;
                    if (IssAclUnInstallL2Filter (pIssL2FilterEntry) ==
                        ISS_FAILURE)
                    {
                        ISS_TRC_ARG1 (MGMT_TRC,
                                      "In %s: IssAclUnInstallL2Filter "
                                      "returned Failure\r\n", __FUNCTION__);
                        return ISS_FAILURE;
                    }
                }
                pAppPriAclInfo->u1FilterAction = ISS_FILTER_UPDATE;
            }

            return ISS_SUCCESS;
        }                        /* if MatchFound */
        else
        {
            if (u1Status == FALSE)
            {
                ISS_TRC_ARG2 (MGMT_TRC,
                              "iIn %s : L2Filter not found for protocol\r\n",
                              __FUNCTION__, pAppPriAclInfo->u4Protocol);
                return ISS_SUCCESS;
            }
            /* Create a new Filter entry */

            /* Get a valid L2 filter ID by scanning through the
             * L2 filter table */
            IssAclGetValidL2FilterId (&u4L2FilterId);
            pAppPriAclInfo->u4FilterId = u4L2FilterId;
            pAppPriAclInfo->u1FilterAction = ISS_FILTER_CREATE;
            IssL2FilterEntry.i4IssL2FilterNo = u4L2FilterId;
            IssL2FilterEntry.u4IssL2FilterProtocolType =
                pAppPriAclInfo->u4Protocol;
            IssL2FilterEntry.u4RefCount = ISS_ZERO_ENTRY;
            IssL2FilterEntry.IssL2FilterAction = ISS_ALLOW;
            IssL2FilterEntry.u1FilterDirection = ISS_DIRECTION_IN;
            IssL2FilterEntry.u1IssL2FilterStatus = ISS_NOT_READY;
            IssL2FilterEntry.u1IssL2FilterTagType = pAppPriAclInfo->u1TagType;

            OSIX_BITLIST_SET_BIT (IssL2FilterEntry.IssL2FilterInPortList,
                                  pAppPriAclInfo->u4PortNo,
                                  sizeof (tIssPortList));

            IssL2FilterEntry.i4IssL2FilterPriority =
                ISS_DEFAULT_FILTER_PRIORITY;

            if (IssAclInstallL2Filter (&IssL2FilterEntry) == ISS_FAILURE)
            {
                ISS_TRC_ARG1 (MGMT_TRC, "In %s : IssAclInstallL2Filter "
                              "returned Failure\r\n", __FUNCTION__);
                return ISS_FAILURE;
            }

        }
    }

    else if (u1FilterType == ISS_L3_FILTER)
    {
        /* Scan the entire L3 Filter List and find the exact match 
         * for the incoming protocol id and port no */

        /* If Match is found , then return the appropriate Filter Id */
        /* If Match is not found, get a valid filter Id and create a L3 Filter
         * with the entries present in pAppPriAclInfo */

        ISS_TRC_ARG2 (MGMT_TRC, "In %s :There are %u entries in "
                      "ISS_L3FILTER_LIST \n", __FUNCTION__,
                      TMO_SLL_Count (&(ISS_L3FILTER_LIST)));

        TMO_SLL_Scan (&(ISS_L3FILTER_LIST),
                      pIssL3FilterEntry, tIssL3FilterEntry *)
        {

            /* If the other entries except the incoming entries
             * are configured then continue to the next entry */

            if ((pIssL3FilterEntry->u4IssL3FilterDstIpAddr) != 0)
            {
                continue;
            }

            if ((pIssL3FilterEntry->u4IssL3FilterSrcIpAddr) != 0)
            {
                continue;
            }

            if ((pIssL3FilterEntry->u4IssL3FilterDstIpAddrMask) != 0)
            {
                continue;
            }

            if ((pIssL3FilterEntry->u4IssL3FilterSrcIpAddrMask) != 0)
            {
                continue;
            }

            if ((pIssL3FilterEntry->u4IssL3FilterMinSrcProtPort) != 0)
            {
                continue;
            }

            if ((pIssL3FilterEntry->u4IssL3FilterMaxSrcProtPort) != 0)
            {
                continue;
            }
            if (MEMCMP (pIssL3FilterEntry->IssL3FilterOutPortList, PortList,
                        sizeof (tIssPortList)) != 0)
            {
                continue;
            }

            /* If the incoming protocol Id is not equal to the L2entry's 
             * protocol Id then continue to the next entry */
            if ((pAppPriAclInfo->u4Protocol ==
                 pIssL3FilterEntry->IssL3FilterProtocol) &&
                (pAppPriAclInfo->u4PortNo ==
                 pIssL3FilterEntry->u4IssL3FilterMinDstProtPort) &&
                (pAppPriAclInfo->u4PortNo ==
                 pIssL3FilterEntry->u4IssL3FilterMaxDstProtPort))
            {

                u1MatchFound = TRUE;
                break;
            }
        }

        /* If the incoming AppPriEntry matches with an already created L3 Filter
         * then return the appropriate Filter Id.
         * If no match is found create a filter in ACL module with the incoming 
         * entries */
        if (u1MatchFound == TRUE)
        {
            pAppPriAclInfo->u4FilterId = pIssL3FilterEntry->i4IssL3FilterNo;
            pAppPriAclInfo->u1FilterAction = ISS_FILTER_UPDATE;
            pIssL3FilterEntry->IssL3FilterProtocol = pAppPriAclInfo->u4Protocol;
            pIssL3FilterEntry->i4IssL3FilterPriority = 1;
            pIssL3FilterEntry->u4IssL3FilterMinDstProtPort =
                pAppPriAclInfo->u4PortNo;
            pIssL3FilterEntry->u4IssL3FilterMaxDstProtPort =
                pAppPriAclInfo->u4PortNo;
            pIssL3FilterEntry->u1IssL3FilterStatus = NOT_READY;
            pIssL3FilterEntry->u1IssL3FilterCreationMode =
                ISS_ACL_CREATION_INTERNAL;
            pIssL3FilterEntry->i4IssL3FilterPriority =
                ISS_DEFAULT_FILTER_PRIORITY;

            /* check If Filter entry is to be updated */
            if (u1Status == TRUE)
            {
                /* If Port is already in FilerEntry's InPortList,update the Filterentry
                 * else set the port in InPortList and update the FilterEntry */
                OSIX_BITLIST_IS_BIT_SET (pIssL3FilterEntry->
                                         IssL3FilterInPortList,
                                         pAppPriAclInfo->u4L3PortNo,
                                         sizeof (tIssPortList), i1Result);
                if (i1Result == OSIX_FALSE)
                {
                    OSIX_BITLIST_SET_BIT (pIssL3FilterEntry->
                                          IssL3FilterInPortList,
                                          pAppPriAclInfo->u4L3PortNo,
                                          sizeof (tIssPortList));
                    pAppPriAclInfo->u1FilterAction = ISS_FILTER_UPDATE;
                }

                if (IssAclInstallL3Filter (pIssL3FilterEntry) == ISS_FAILURE)
                {
                    ISS_TRC_ARG1 (MGMT_TRC, "In %s : IssAclInstallL3Filter "
                                  "returned Failure\r\n", __FUNCTION__);
                    return ISS_FAILURE;
                }
            }                    /* If Filter is to be deleted */
            else
            {
                /* If Port is part of InPortList of FilterEntry, remove it from the 
                 * InPortList.*/
                OSIX_BITLIST_IS_BIT_SET (pIssL3FilterEntry->
                                         IssL3FilterInPortList,
                                         pAppPriAclInfo->u4L3PortNo,
                                         sizeof (tIssPortList), i1Result);
                if (i1Result == OSIX_FALSE)
                {
                    return ISS_SUCCESS;
                }
                OSIX_BITLIST_RESET_BIT (pIssL3FilterEntry->
                                        IssL3FilterInPortList,
                                        pAppPriAclInfo->u4L3PortNo,
                                        sizeof (tIssPortList));

                /*If no other port is using this filter entry, delete the filter */
                if (MEMCMP (pIssL3FilterEntry->IssL3FilterInPortList, PortList,
                            sizeof (tIssPortList)) == 0)
                {
                    pAppPriAclInfo->u1FilterAction = ISS_FILTER_DELETE;

                    if (IssAclUnInstallL3Filter (pIssL3FilterEntry) ==
                        ISS_FAILURE)
                    {
                        ISS_TRC_ARG1 (MGMT_TRC,
                                      "In %s : IssAclUnInstallL3Filter "
                                      "returned Failure\r\n", __FUNCTION__);
                        return ISS_FAILURE;
                    }
                }
                pAppPriAclInfo->u1FilterAction = ISS_FILTER_UPDATE;
            }

            return ISS_SUCCESS;
        }                        /* If Match not found */
        else
        {
            if (u1Status == FALSE)
            {
                ISS_TRC_ARG2 (MGMT_TRC, "In %s : L3Filter "
                              "not found for protocol\r\n", __FUNCTION__,
                              pAppPriAclInfo->u4Protocol);
                return ISS_SUCCESS;
            }

            /* Get Free FilterId */
            IssAclGetValidL3FilterId (&u4L3FilterId);

            /* Create a new Filter entry */
            pAppPriAclInfo->u4FilterId = u4L3FilterId;
            pAppPriAclInfo->u1FilterAction = ISS_FILTER_CREATE;
            IssL3FilterEntry.i4IssL3FilterNo = u4L3FilterId;
            IssL3FilterEntry.IssL3FilterProtocol = pAppPriAclInfo->u4Protocol;
            IssL3FilterEntry.u4IssL3FilterMinDstProtPort =
                pAppPriAclInfo->u4PortNo;
            IssL3FilterEntry.u4IssL3FilterMaxDstProtPort =
                pAppPriAclInfo->u4PortNo;
            IssL3FilterEntry.i4IssL3FilterPriority =
                ISS_DEFAULT_FILTER_PRIORITY;
            IssL3FilterEntry.IssL3FilterAckBit = ISS_ACK_ANY;
            IssL3FilterEntry.IssL3FilterRstBit = ISS_ACK_ANY;
            IssL3FilterEntry.IssL3FilterTos = ISS_TOS_INVALID;
            IssL3FilterEntry.i4IssL3FilterDscp = ISS_DSCP_INVALID;
            IssL3FilterEntry.IssL3FilterDirection = ISS_DIRECTION_IN;
            IssL3FilterEntry.u4IssL3FilterMultiFieldClfrDstPrefixLength =
                ISS_ZERO_ENTRY;
            IssL3FilterEntry.u4IssL3FilterMultiFieldClfrSrcPrefixLength =
                ISS_ZERO_ENTRY;
            IssL3FilterEntry.u4IssL3MultiFieldClfrFlowId = ISS_ZERO_ENTRY;
            IssL3FilterEntry.i4IssL3MultiFieldClfrAddrType = QOS_IPV4;
            IssL3FilterEntry.i4StorageType = ISS_ZERO_ENTRY;
            IssL3FilterEntry.i1CVlanPriority = ISS_DEFAULT_VLAN_PRIORITY;
            IssL3FilterEntry.i1SVlanPriority = ISS_DEFAULT_VLAN_PRIORITY;
            IssL3FilterEntry.u1IssL3FilterTagType = ISS_FILTER_SINGLE_TAG;
            IssL3FilterEntry.u1IssL3FilterStatus = NOT_READY;
            IssL3FilterEntry.u1IssL3FilterCreationMode =
                ISS_ACL_CREATION_INTERNAL;
            IssL3FilterEntry.IssL3FilterAction = ISS_ALLOW;

            OSIX_BITLIST_SET_BIT (IssL3FilterEntry.IssL3FilterInPortList,
                                  pAppPriAclInfo->u4L3PortNo,
                                  sizeof (tIssPortList));

            if (IssAclInstallL3Filter (&IssL3FilterEntry) == ISS_FAILURE)
            {
                ISS_TRC_ARG1 (MGMT_TRC, "In %s : IssAclInstallL3Filter "
                              "returned Failure\r\n", __FUNCTION__);

                return ISS_FAILURE;
            }

        }
    }

    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssACLApiGetFilterEntry                           */
/*                                                                           */
/* Description        : This function is called from the DCBx Application    */
/*                      priority module to configure the priority for        */
/*                      a protocol.                                          */
/*                                                                           */
/* Input(s)           :                                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
IssACLApiGetFilterEntry (tIssAclHwFilterInfo * pAppPriAclInfo,
        UINT1 u1FilterType, UINT1 u1Status)

{
    UNUSED_PARAM (pAppPriAclInfo);
    UNUSED_PARAM (u1FilterType);
    UNUSED_PARAM (u1Status);
    return;
}

/*****************************************************************************/
/* Function Name      : IssACLApiValidateL3AclConfigFlag                     */
/*                                                                           */
/* Description        : This function is called from msrval for validating   */
/*                      L3 Acl Config Flag (which is set based on            */
/*                      configuration) done through ACL or other protocol    */
/*                      a protocol.                                          */
/*                                                                           */
/* Input(s)           : i4L3AclFilterId - filter id                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/

VOID
IssACLApiValidateL3AclConfigFlag (INT4 i4L3AclFilterId, BOOL1 *pb1IsL3AclConfFromExt)
{
    UNUSED_PARAM (i4L3AclFilterId);
    UNUSED_PARAM (pb1IsL3AclConfFromExt);
    return;
}

/*****************************************************************************/
/* Function Name      : IssACLApiValidateL2AclConfigFlag                     */
/*                                                                           */
/* Description        : This function is called from msrval for validating   */
/*                      L2 Acl Config Flag (which is set based on            */
/*                      configuration) done through ACL or other protocol    */
/*                      a protocol.                                          */
/*                                                                           */
/* Input(s)           : i4L2AclFilterId - filter id                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/

VOID
IssACLApiValidateL2AclConfigFlag (INT4 i4L2AclFilterId, BOOL1 *pb1IsL2AclConfFromExt)
{
    UNUSED_PARAM (i4L2AclFilterId);
    UNUSED_PARAM (pb1IsL2AclConfFromExt);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclValidateMemberPort                            */
/*                                                                           */
/*     DESCRIPTION      : This function checks wheather the filter is        */
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  /*                   attached with the physical port                    *//*                                                                           *//*     INPUT            : u4IfIndex - Physical port number                   *//*                                                                           *//*     OUTPUT           : NONE                                               *//*                                                                           *//*     RETURNS          : NONE                                               *//*                                                                           *//*****************************************************************************/

INT4
AclValidateMemberPort (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return (CLI_SUCCESS);
}

#endif /* _ISSEXSYS_C */
