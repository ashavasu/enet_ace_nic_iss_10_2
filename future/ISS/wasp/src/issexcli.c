/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: issexcli.c,v 1.1 2013/09/28 11:44:21 siva Exp $
 *
 * Description: This file implements the CLI routines for the ACL commands
 ****************************************************************************/

#ifndef  __ISSEXCLI_C__
#define  __ISSEXCLI_C__

#include "lr.h"
#include "fssnmp.h"
#include "iss.h"
#include "msr.h"
#include "issexinc.h"
#include "fsisselw.h"
#include "isscli.h"

/***************************************************************/
/*  Function Name   : cli_process_iss_ext_cmd                  */
/*  Description     : This function servers as the handler for */
/*                    extended system CLI commands             */
/*  Input(s)        :                                          */
/*                    CliHandle - CLI Handle                   */
/*                    u4Command - Command given by user        */
/*                    Variable set of inputs depending on      */
/*                    the user command                         */
/*  Output(s)       : Error message - on failure               */
/*  Returns         : None                                     */
/***************************************************************/

VOID
cli_process_iss_ext_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[CLI_ISS_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4Inst;
    UINT4               u4IfIndex;
    INT4                i4RetStatus = CLI_SUCCESS;
    INT4                i4RateLimit = ISS_CLI_RATE_LIMIT_INVALID;
    INT4                i4BurstSize = ISS_CLI_RATE_LIMIT_INVALID;

    va_start (ap, u4Command);

    /* Third arguement is always interface name/index */
    i4Inst = va_arg (ap, INT4);

    /* Walk through the rest of the arguements and store in args array.  */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == CLI_ISS_MAX_ARGS)
            break;
    }
    va_end (ap);

    CLI_SET_ERR (0);

    CliRegisterLock (CliHandle, IssLock, IssUnLock);

    ISS_LOCK ();

    switch (u4Command)
    {
        case CLI_ISS_STORM_CONTROL:
            u4IfIndex = CLI_GET_IFINDEX ();
            CliSetIssStormControl (CliHandle, u4IfIndex,
                                   CLI_PTR_TO_U4 (args[0]), *args[1]);
            break;

        case CLI_ISS_NO_STORM_CONTROL:
            u4IfIndex = CLI_GET_IFINDEX ();
            CliSetIssStormControl (CliHandle, u4IfIndex,
                                   CLI_PTR_TO_U4 (args[0]), 0);
            break;

        case CLI_ISS_PORT_RATE_LIMIT:
            u4IfIndex = CLI_GET_IFINDEX ();
            if ((args[0]) != NULL)
            {
                i4RateLimit = *(INT4 *) args[0];

            }
            if ((args[1]) != NULL)
            {
                i4BurstSize = *(INT4 *) args[1];
            }

            i4RetStatus = CliSetIssConfPortRateLimit (CliHandle, u4IfIndex,
                                                      i4RateLimit, i4BurstSize);
            break;

        case CLI_ISS_NO_PORT_RATE_LIMIT:
            u4IfIndex = CLI_GET_IFINDEX ();
            if ((args[0]) != NULL)
            {
                i4RateLimit = ISS_ZERO_ENTRY;
            }
            if ((args[1]) != NULL)
            {
                i4BurstSize = ISS_ZERO_ENTRY;
            }

            i4RetStatus = CliSetIssConfPortRateLimit (CliHandle, u4IfIndex,
                                                      i4RateLimit, i4BurstSize);
            break;

    }
    if ((CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_ISS_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", IssCliErrString[u4ErrCode]);
        }

        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetStatus);

    CliUnRegisterLock (CliHandle);

    ISS_UNLOCK ();
}

/******************************************************************/
/*  Function Name   : CliSetIssStormControl                       */
/*  Description     : This function is used to set the            */
/*                    strom control limit                         */
/*  Input(s)        :                                             */
/*                    CliHandle   - CLI Handle                    */
/*                    u4IfIndex   - Port on which Storm control   */
/*                                  is applied.                   */
/*                    u4Mode      - Mode (Bcast/Mcast/DLF)        */
/*                    u4Rate      - Rate value                    */
/*  Output(s)       : None                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                     */
/******************************************************************/

INT4
CliSetIssStormControl (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4Mode,
                       UINT4 u4Rate)
{
    UINT4               u4ErrorCode;
    INT4                i4RetVal = CLI_SUCCESS;

    switch (u4Mode)
    {
        case ISS_RATE_DLF_LIMIT:
            if (nmhTestv2IssExtRateCtrlDLFLimitValue (&u4ErrorCode,
                                                      u4IfIndex, u4Rate)
                == SNMP_FAILURE)
            {
                i4RetVal = CLI_FAILURE;
                break;
            }

            if (nmhSetIssExtRateCtrlDLFLimitValue (u4IfIndex, u4Rate)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                i4RetVal = CLI_FAILURE;
                break;
            }

            break;

        case ISS_RATE_BCAST_LIMIT:
            if (nmhTestv2IssExtRateCtrlBCASTLimitValue (&u4ErrorCode,
                                                        u4IfIndex, u4Rate)
                == SNMP_FAILURE)
            {
                i4RetVal = CLI_FAILURE;
                break;
            }

            if (nmhSetIssExtRateCtrlBCASTLimitValue (u4IfIndex, u4Rate)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                i4RetVal = CLI_FAILURE;
                break;
            }
            break;

        case ISS_RATE_MCAST_LIMIT:
            if (nmhTestv2IssExtRateCtrlMCASTLimitValue (&u4ErrorCode,
                                                        u4IfIndex, u4Rate)
                == SNMP_FAILURE)
            {
                i4RetVal = CLI_FAILURE;
                break;
            }

            if (nmhSetIssExtRateCtrlMCASTLimitValue (u4IfIndex, u4Rate)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                i4RetVal = CLI_FAILURE;
                break;
            }
            break;

        default:
            i4RetVal = CLI_FAILURE;
            break;
    }

    return i4RetVal;
}

/***************************************************************/
/*  Function Name   : CliSetIssConfPortRateLimit               */
/*  Description     : This function is used to configure rate  */
/*                    limit for a port                         */
/*  Input(s)        : CliHandle        -  CLI Handle           */
/*                    u4IfIndex        -  Interface Index      */
/*                    i4PortRateLimit  -  Port rate limit      */
/*                    i4PortBurstSize  -  Burst size limit     */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
INT4
CliSetIssConfPortRateLimit (tCliHandle CliHandle, UINT4 u4IfIndex,
                            INT4 i4PortRateLimit, INT4 i4PortBurstSize)
{
    UINT4               u4ErrorCode;

    if (i4PortRateLimit != ISS_CLI_RATE_LIMIT_INVALID)
    {
        if (nmhTestv2IssExtRateCtrlPortRateLimit (&u4ErrorCode, u4IfIndex,
                                                  i4PortRateLimit) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetIssExtRateCtrlPortRateLimit (u4IfIndex, i4PortRateLimit) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (i4PortBurstSize != ISS_CLI_RATE_LIMIT_INVALID)
    {
        if (nmhTestv2IssExtRateCtrlPortBurstSize (&u4ErrorCode, u4IfIndex,
                                                  i4PortBurstSize) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetIssExtRateCtrlPortBurstSize (u4IfIndex, i4PortBurstSize) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }

    return CLI_SUCCESS;
}

#endif /* __ISSEXCLI_C__ */
