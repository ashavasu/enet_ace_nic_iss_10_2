/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissalw.c,v 1.1 2013/09/28 11:44:21 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsissalw.h"
# include  "issexinc.h"
# include  "fsisselw.h"
# include  "isscli.h"
# include  "aclcli.h"
# include  "diffsrv.h"
# include  "qosxtd.h"
# include "ipv6.h"

#define QOS_IPV4_LEN                    4
#define QOS_IPV6_LEN                    16
#define QOS_MF_PERFIX_IPV4_MIN          0
#define QOS_MF_PERFIX_IPV4_MAX          32
#define QOS_MF_PERFIX_IPV6_MIN          0
#define QOS_MF_PERFIX_IPV6_MAX          128
#define QOS_FLOW_ID_MIN                 0
#define QOS_FLOW_ID_MAX                 1048575

static UINT1        gNullPortList[ISS_PORT_LIST_SIZE] = { 0 };
extern INT1 nmhGetIssAclTrafficSeperationCtrl ARG_LIST ((INT4 *));

/* LOW LEVEL Routines for Table : IssAclRateCtrlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssAclRateCtrlTable
 Input       :  The Indices
                IssAclRateCtrlIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssAclRateCtrlTable (INT4 i4IssAclRateCtrlIndex)
{
    if (IssExtSnmpLowValidateIndexPortRateTable (i4IssAclRateCtrlIndex) ==
        ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssAclRateCtrlTable
 Input       :  The Indices
                IssAclRateCtrlIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssAclRateCtrlTable (INT4 *pi4IssAclRateCtrlIndex)
{
    return (nmhGetNextIndexIssAclRateCtrlTable (0, pi4IssAclRateCtrlIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIssAclRateCtrlTable
 Input       :  The Indices
                IssAclRateCtrlIndex
                nextIssAclRateCtrlIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIssAclRateCtrlTable (INT4 i4IssAclRateCtrlIndex,
                                    INT4 *pi4NextIssAclRateCtrlIndex)
{
    INT4                i4Count = 0;

    if ((i4IssAclRateCtrlIndex < 0) || (i4IssAclRateCtrlIndex >= ISS_MAX_PORTS))
    {
        return SNMP_FAILURE;
    }

    for (i4Count = i4IssAclRateCtrlIndex + 1; i4Count <= ISS_MAX_PORTS;
         i4Count++)
    {
        if ((gIssExGlobalInfo.apIssRateCtrlEntry[i4Count]) != NULL)
        {
            *pi4NextIssAclRateCtrlIndex = i4Count;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIssAclRateCtrlDLFLimitValue
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                retValIssAclRateCtrlDLFLimitValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclRateCtrlDLFLimitValue (INT4 i4IssAclRateCtrlIndex,
                                   INT4 *pi4RetValIssAclRateCtrlDLFLimitValue)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry;

    if (IssExtValidateRateCtrlEntry (i4IssAclRateCtrlIndex) != ISS_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    *pi4RetValIssAclRateCtrlDLFLimitValue =
        (INT4) pIssAclRateCtrlEntry->u4IssRateCtrlDLFLimitValue;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIssAclRateCtrlBCASTLimitValue
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                retValIssAclRateCtrlBCASTLimitValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclRateCtrlBCASTLimitValue (INT4 i4IssAclRateCtrlIndex,
                                     INT4
                                     *pi4RetValIssAclRateCtrlBCASTLimitValue)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry;

    if (IssExtValidateRateCtrlEntry (i4IssAclRateCtrlIndex) != ISS_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    *pi4RetValIssAclRateCtrlBCASTLimitValue =
        (INT4) pIssAclRateCtrlEntry->u4IssRateCtrlBCASTLimitValue;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIssAclRateCtrlMCASTLimitValue
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                retValIssAclRateCtrlMCASTLimitValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclRateCtrlMCASTLimitValue (INT4 i4IssAclRateCtrlIndex,
                                     INT4
                                     *pi4RetValIssAclRateCtrlMCASTLimitValue)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry;

    if (IssExtValidateRateCtrlEntry (i4IssAclRateCtrlIndex) != ISS_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    *pi4RetValIssAclRateCtrlMCASTLimitValue =
        (INT4) pIssAclRateCtrlEntry->u4IssRateCtrlMCASTLimitValue;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIssAclRateCtrlPortRateLimit
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                retValIssAclRateCtrlPortRateLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclRateCtrlPortRateLimit (INT4 i4IssAclRateCtrlIndex,
                                   INT4 *pi4RetValIssAclRateCtrlPortRateLimit)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry = NULL;

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    if (pIssAclRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIssAclRateCtrlPortRateLimit =
        pIssAclRateCtrlEntry->i4IssRateCtrlPortLimitRate;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIssAclRateCtrlPortBurstSize
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                retValIssAclRateCtrlPortBurstSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclRateCtrlPortBurstSize (INT4 i4IssAclRateCtrlIndex,
                                   INT4 *pi4RetValIssAclRateCtrlPortBurstSize)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry = NULL;

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    if (pIssAclRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIssAclRateCtrlPortBurstSize =
        pIssAclRateCtrlEntry->i4IssRateCtrlPortBurstRate;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIssAclRateCtrlDLFLimitValue
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                setValIssAclRateCtrlDLFLimitValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclRateCtrlDLFLimitValue (INT4 i4IssAclRateCtrlIndex,
                                   INT4 i4SetValIssAclRateCtrlDLFLimitValue)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    if (pIssAclRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclRateCtrlEntry->u4IssRateCtrlDLFLimitValue == (UINT4)
        i4SetValIssAclRateCtrlDLFLimitValue)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    i4RetVal = IssHwSetRateLimitingValue ((UINT4) i4IssAclRateCtrlIndex,
                                          ISS_RATE_DLF,
                                          i4SetValIssAclRateCtrlDLFLimitValue);

    if (i4RetVal != FNP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif

    pIssAclRateCtrlEntry->u4IssRateCtrlDLFLimitValue = (UINT4)
        i4SetValIssAclRateCtrlDLFLimitValue;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssAclRateCtrlBCASTLimitValue
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                setValIssAclRateCtrlBCASTLimitValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclRateCtrlBCASTLimitValue (INT4 i4IssAclRateCtrlIndex,
                                     INT4 i4SetValIssAclRateCtrlBCASTLimitValue)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    if (pIssAclRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclRateCtrlEntry->u4IssRateCtrlBCASTLimitValue == (UINT4)
        i4SetValIssAclRateCtrlBCASTLimitValue)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    i4RetVal = IssHwSetRateLimitingValue ((UINT4) i4IssAclRateCtrlIndex,
                                          ISS_RATE_BCAST,
                                          i4SetValIssAclRateCtrlBCASTLimitValue);

    if (i4RetVal != FNP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif

    pIssAclRateCtrlEntry->u4IssRateCtrlBCASTLimitValue = (UINT4)
        i4SetValIssAclRateCtrlBCASTLimitValue;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssAclRateCtrlMCASTLimitValue
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                setValIssAclRateCtrlMCASTLimitValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclRateCtrlMCASTLimitValue (INT4 i4IssAclRateCtrlIndex,
                                     INT4 i4SetValIssAclRateCtrlMCASTLimitValue)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    if (pIssAclRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclRateCtrlEntry->u4IssRateCtrlMCASTLimitValue == (UINT4)
        i4SetValIssAclRateCtrlMCASTLimitValue)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    i4RetVal = IssHwSetRateLimitingValue ((UINT4) i4IssAclRateCtrlIndex,
                                          ISS_RATE_MCAST,
                                          i4SetValIssAclRateCtrlMCASTLimitValue);

    if (i4RetVal != FNP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif

    pIssAclRateCtrlEntry->u4IssRateCtrlMCASTLimitValue = (UINT4)
        i4SetValIssAclRateCtrlMCASTLimitValue;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssAclRateCtrlPortRateLimit
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                setValIssAclRateCtrlPortRateLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclRateCtrlPortRateLimit (INT4 i4IssAclRateCtrlIndex,
                                   INT4 i4SetValIssAclRateCtrlPortRateLimit)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif
    INT4                i4SetValIssAclRateCtrlPortBurstSize = 0;

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    if (pIssAclRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((INT4) (pIssAclRateCtrlEntry->i4IssRateCtrlPortLimitRate) ==
        i4SetValIssAclRateCtrlPortRateLimit)
    {
        return SNMP_SUCCESS;
    }

    i4SetValIssAclRateCtrlPortBurstSize =
        pIssAclRateCtrlEntry->i4IssRateCtrlPortBurstRate;

#ifdef NPAPI_WANTED
    i4RetVal = IssHwSetPortEgressPktRate
        ((UINT4) i4IssAclRateCtrlIndex, i4SetValIssAclRateCtrlPortRateLimit,
         i4SetValIssAclRateCtrlPortBurstSize);

    if (i4RetVal != FNP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif
    pIssAclRateCtrlEntry->i4IssRateCtrlPortLimitRate =
        i4SetValIssAclRateCtrlPortRateLimit;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssAclRateCtrlPortBurstSize
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                setValIssAclRateCtrlPortBurstSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclRateCtrlPortBurstSize (INT4 i4IssAclRateCtrlIndex,
                                   INT4 i4SetValIssAclRateCtrlPortBurstSize)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif
    INT4                i4SetValIssAclRateCtrlPortRateLimit = 0;

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    if (pIssAclRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((INT4) (pIssAclRateCtrlEntry->i4IssRateCtrlPortBurstRate) ==
        i4SetValIssAclRateCtrlPortBurstSize)
    {
        return SNMP_SUCCESS;
    }

    i4SetValIssAclRateCtrlPortRateLimit =
        pIssAclRateCtrlEntry->i4IssRateCtrlPortLimitRate;

#ifdef NPAPI_WANTED
    i4RetVal = IssHwSetPortEgressPktRate
        ((UINT4) i4IssAclRateCtrlIndex, i4SetValIssAclRateCtrlPortRateLimit,
         i4SetValIssAclRateCtrlPortBurstSize);

    if (i4RetVal != FNP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif

    pIssAclRateCtrlEntry->i4IssRateCtrlPortBurstRate =
        i4SetValIssAclRateCtrlPortBurstSize;
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IssAclRateCtrlDLFLimitValue
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                testValIssAclRateCtrlDLFLimitValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclRateCtrlDLFLimitValue (UINT4 *pu4ErrorCode,
                                      INT4 i4IssAclRateCtrlIndex,
                                      INT4 i4TestValIssAclRateCtrlDLFLimitValue)
{
    if (IssExtValidateRateCtrlEntry (i4IssAclRateCtrlIndex) != ISS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclRateCtrlDLFLimitValue < 0)
        || (i4TestValIssAclRateCtrlDLFLimitValue > RATE_LIMIT_MAX_VALUE))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclRateCtrlBCASTLimitValue
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                testValIssAclRateCtrlBCASTLimitValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclRateCtrlBCASTLimitValue (UINT4 *pu4ErrorCode,
                                        INT4 i4IssAclRateCtrlIndex,
                                        INT4
                                        i4TestValIssAclRateCtrlBCASTLimitValue)
{
    if (IssExtValidateRateCtrlEntry (i4IssAclRateCtrlIndex) != ISS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclRateCtrlBCASTLimitValue < 0)
        || (i4TestValIssAclRateCtrlBCASTLimitValue > RATE_LIMIT_MAX_VALUE))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclRateCtrlMCASTLimitValue
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                testValIssAclRateCtrlMCASTLimitValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclRateCtrlMCASTLimitValue (UINT4 *pu4ErrorCode,
                                        INT4 i4IssAclRateCtrlIndex,
                                        INT4
                                        i4TestValIssAclRateCtrlMCASTLimitValue)
{
    if (IssExtValidateRateCtrlEntry (i4IssAclRateCtrlIndex) != ISS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclRateCtrlMCASTLimitValue < 0)
        || (i4TestValIssAclRateCtrlMCASTLimitValue > RATE_LIMIT_MAX_VALUE))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclRateCtrlPortRateLimit
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                testValIssAclRateCtrlPortRateLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclRateCtrlPortRateLimit (UINT4 *pu4ErrorCode,
                                      INT4 i4IssAclRateCtrlIndex,
                                      INT4 i4TestValIssAclRateCtrlPortRateLimit)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry;

    if (IssExtValidateRateCtrlEntry (i4IssAclRateCtrlIndex) != ISS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    if (pIssAclRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclRateCtrlPortRateLimit < 0) ||
        (i4TestValIssAclRateCtrlPortRateLimit > 80000000))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclRateCtrlPortBurstSize
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                testValIssAclRateCtrlPortBurstSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclRateCtrlPortBurstSize (UINT4 *pu4ErrorCode,
                                      INT4 i4IssAclRateCtrlIndex,
                                      INT4 i4TestValIssAclRateCtrlPortBurstSize)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry;

    if (IssExtValidateRateCtrlEntry (i4IssAclRateCtrlIndex) != ISS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    if (pIssAclRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclRateCtrlPortBurstSize < 0) ||
        (i4TestValIssAclRateCtrlPortBurstSize > 80000000))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IssAclRateCtrlTable
 Input       :  The Indices
                IssAclRateCtrlIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IssAclRateCtrlTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IssAclL2FilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssAclL2FilterTable
 Input       :  The Indices
                IssAclL2FilterNo
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssAclL2FilterTable (INT4 i4IssAclL2FilterNo)
{
    tIssSllNode        *pSllNode = NULL;
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) != ISS_TRUE)
    {
        return SNMP_FAILURE;
    }
    pSllNode = ISS_SLL_FIRST (&ISS_L2FILTER_LIST);
    while (pSllNode != NULL)
    {
        pIssAclL2FilterEntry = (tIssL2FilterEntry *) pSllNode;
        if (i4IssAclL2FilterNo == pIssAclL2FilterEntry->i4IssL2FilterNo)
        {
            return SNMP_SUCCESS;
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L2FILTER_LIST, pSllNode);
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssAclL2FilterTable
 Input       :  The Indices
                IssAclL2FilterNo
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssAclL2FilterTable (INT4 *pi4IssAclL2FilterNo)
{
    if ((IssExtSnmpLowGetFirstValidL2FilterTableIndex (pi4IssAclL2FilterNo)) ==
        ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhGetNextIndexIssAclL2FilterTable
 Input       :  The Indices
                IssAclL2FilterNo
                nextIssAclL2FilterNo
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIssAclL2FilterTable (INT4 i4IssAclL2FilterNo,
                                    INT4 *pi4NextIssAclL2FilterNo)
{
    if (i4IssAclL2FilterNo < 0)
    {
        return SNMP_FAILURE;
    }

    if ((IssExtSnmpLowGetNextValidL2FilterTableIndex
         (i4IssAclL2FilterNo, pi4NextIssAclL2FilterNo)) == ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterPriority
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterPriority (INT4 i4IssAclL2FilterNo,
                              INT4 *pi4RetValIssAclL2FilterPriority)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        *pi4RetValIssAclL2FilterPriority =
            pIssAclL2FilterEntry->i4IssL2FilterPriority;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterEtherType
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterEtherType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterEtherType (INT4 i4IssAclL2FilterNo,
                               INT4 *pi4RetValIssAclL2FilterEtherType)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        *pi4RetValIssAclL2FilterEtherType =
            pIssAclL2FilterEntry->u2InnerEtherType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterProtocolType
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterProtocolType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterProtocolType (INT4 i4IssAclL2FilterNo,
                                  UINT4 *pu4RetValIssAclL2FilterProtocolType)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        *pu4RetValIssAclL2FilterProtocolType =
            pIssAclL2FilterEntry->u4IssL2FilterProtocolType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterDstMacAddr
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterDstMacAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterDstMacAddr (INT4 i4IssAclL2FilterNo,
                                tMacAddr * pRetValIssAclL2FilterDstMacAddr)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        ISS_MEMCPY ((UINT1 *) pRetValIssAclL2FilterDstMacAddr,
                    pIssAclL2FilterEntry->IssL2FilterDstMacAddr,
                    ISS_ETHERNET_ADDR_SIZE);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterSrcMacAddr
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterSrcMacAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterSrcMacAddr (INT4 i4IssAclL2FilterNo,
                                tMacAddr * pRetValIssAclL2FilterSrcMacAddr)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        ISS_MEMCPY ((UINT1 *) pRetValIssAclL2FilterSrcMacAddr,
                    pIssAclL2FilterEntry->IssL2FilterSrcMacAddr,
                    ISS_ETHERNET_ADDR_SIZE);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterVlanId
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterVlanId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterVlanId (INT4 i4IssAclL2FilterNo,
                            INT4 *pi4RetValIssAclL2FilterVlanId)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        *pi4RetValIssAclL2FilterVlanId =
            (INT4) pIssAclL2FilterEntry->u4IssL2FilterCustomerVlanId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterInPortList
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterInPortList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterInPortList (INT4 i4IssAclL2FilterNo,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValIssAclL2FilterInPortList)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        ISS_MEMSET (pRetValIssAclL2FilterInPortList->pu1_OctetList, 0,
                    ISS_PORT_LIST_SIZE);
        ISS_ADD_PORT_LIST (pRetValIssAclL2FilterInPortList->pu1_OctetList,
                           pIssAclL2FilterEntry->IssL2FilterInPortList);
        pRetValIssAclL2FilterInPortList->i4_Length = ISS_PORT_LIST_SIZE;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterAction
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterAction (INT4 i4IssAclL2FilterNo,
                            INT4 *pi4RetValIssAclL2FilterAction)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        *pi4RetValIssAclL2FilterAction =
            pIssAclL2FilterEntry->IssL2FilterAction;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterMatchCount
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterMatchCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterMatchCount (INT4 i4IssAclL2FilterNo,
                                UINT4 *pu4RetValIssAclL2FilterMatchCount)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        *pu4RetValIssAclL2FilterMatchCount =
            pIssAclL2FilterEntry->u4IssL2FilterMatchCount;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterStatus
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterStatus (INT4 i4IssAclL2FilterNo,
                            INT4 *pi4RetValIssAclL2FilterStatus)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        *pi4RetValIssAclL2FilterStatus =
            (INT4) pIssAclL2FilterEntry->u1IssL2FilterStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterOutPortList
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterOutPortList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterOutPortList (INT4 i4IssAclL2FilterNo,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValIssAclL2FilterOutPortList)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        ISS_MEMSET (pRetValIssAclL2FilterOutPortList->pu1_OctetList, 0,
                    ISS_PORT_LIST_SIZE);
        ISS_ADD_PORT_LIST (pRetValIssAclL2FilterOutPortList->pu1_OctetList,
                           pIssAclL2FilterEntry->IssL2FilterOutPortList);
        pRetValIssAclL2FilterOutPortList->i4_Length = ISS_PORT_LIST_SIZE;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterDirection
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterDirection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterDirection (INT4 i4IssAclL2FilterNo,
                               INT4 *pi4RetValIssAclL2FilterDirection)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        *pi4RetValIssAclL2FilterDirection =
            (INT4) pIssAclL2FilterEntry->u1FilterDirection;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterSubAction
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterSubAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterSubAction (INT4
                               i4IssAclL2FilterNo,
                               INT4 *pi4RetValIssAclL2FilterSubAction)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        *pi4RetValIssAclL2FilterSubAction =
            (INT4) pIssAclL2FilterEntry->u1IssL2SubAction;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterSubActionId
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterSubActionId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterSubActionId (INT4
                                 i4IssAclL2FilterNo,
                                 INT4 *pi4RetValIssAclL2FilterSubActionId)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        *pi4RetValIssAclL2FilterSubActionId =
            (INT4) pIssAclL2FilterEntry->u2IssL2SubActionId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterRedirectId
 Input       :  The Indices
                IssAclL2FilterNo

                The Object
                retValIssAclL2FilterRedirectId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterRedirectId (INT4 i4IssAclL2FilterNo,
                                INT4 *pi4RetValIssAclL2FilterRedirectId)
{
    UINT4               u4Index = 0;
    for (u4Index = 0; u4Index < ISS_MAX_REDIRECT_GRP_ID; u4Index++)
    {
        if ((gpIssRedirectIntfInfo[u4Index].u4AclId ==
             (UINT4) i4IssAclL2FilterNo)
            && (gpIssRedirectIntfInfo[u4Index].u1AclIdType == ISS_L2_REDIRECT))
        {
            *pi4RetValIssAclL2FilterRedirectId = u4Index + 1;
            break;
        }
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2NextFilterNo
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2NextFilterNo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2NextFilterNo (INT4 i4IssAclL2FilterNo,
                            INT4 *pi4RetValIssAclL2NextFilterNo)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        *pi4RetValIssAclL2NextFilterNo =
            pIssAclL2FilterEntry->i4IssL2NextFilterNo;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssAclL2NextFilterType
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2NextFilterType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2NextFilterType (INT4 i4IssAclL2FilterNo,
                              INT4 *pi4RetValIssAclL2NextFilterType)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        *pi4RetValIssAclL2NextFilterType =
            pIssAclL2FilterEntry->i4IssL2NextFilterType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterCreationMode
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterCreationMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterCreationMode (INT4 i4IssAclL2FilterNo,
                                  INT4 *pi4RetValIssAclL2FilterCreationMode)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        *pi4RetValIssAclL2FilterCreationMode =
            (INT4) pIssAclL2FilterEntry->u1IssL2FilterCreationMode;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterPriority
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterPriority (INT4 i4IssAclL2FilterNo,
                              INT4 i4SetValIssAclL2FilterPriority)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        pIssAclL2FilterEntry->i4IssL2FilterPriority =
            i4SetValIssAclL2FilterPriority;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterEtherType
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterEtherType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterEtherType (INT4 i4IssAclL2FilterNo,
                               INT4 i4SetValIssAclL2FilterEtherType)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        pIssAclL2FilterEntry->u2InnerEtherType = (UINT2)
            i4SetValIssAclL2FilterEtherType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterProtocolType
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterProtocolType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterProtocolType (INT4 i4IssAclL2FilterNo,
                                  UINT4 u4SetValIssAclL2FilterProtocolType)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        pIssAclL2FilterEntry->u4IssL2FilterProtocolType =
            u4SetValIssAclL2FilterProtocolType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterDstMacAddr
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterDstMacAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterDstMacAddr (INT4 i4IssAclL2FilterNo,
                                tMacAddr SetValIssAclL2FilterDstMacAddr)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        ISS_MEMSET (pIssAclL2FilterEntry->IssL2FilterDstMacAddr, 0,
                    ISS_ETHERNET_ADDR_SIZE);
        ISS_MEMCPY (pIssAclL2FilterEntry->IssL2FilterDstMacAddr,
                    SetValIssAclL2FilterDstMacAddr, ISS_ETHERNET_ADDR_SIZE);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterSrcMacAddr
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterSrcMacAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterSrcMacAddr (INT4 i4IssAclL2FilterNo,
                                tMacAddr SetValIssAclL2FilterSrcMacAddr)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        ISS_MEMSET (pIssAclL2FilterEntry->IssL2FilterSrcMacAddr, 0,
                    ISS_ETHERNET_ADDR_SIZE);
        ISS_MEMCPY (pIssAclL2FilterEntry->IssL2FilterSrcMacAddr,
                    SetValIssAclL2FilterSrcMacAddr, ISS_ETHERNET_ADDR_SIZE);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterVlanId
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterVlanId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterVlanId (INT4 i4IssAclL2FilterNo,
                            INT4 i4SetValIssAclL2FilterVlanId)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        pIssAclL2FilterEntry->u4IssL2FilterCustomerVlanId =
            (UINT4) i4SetValIssAclL2FilterVlanId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterInPortList
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterInPortList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterInPortList (INT4 i4IssAclL2FilterNo,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValIssAclL2FilterInPortList)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        ISS_MEMSET (pIssAclL2FilterEntry->IssL2FilterInPortList, 0,
                    ISS_PORT_LIST_SIZE);
        ISS_MEMCPY (pIssAclL2FilterEntry->IssL2FilterInPortList,
                    pSetValIssAclL2FilterInPortList->pu1_OctetList,
                    pSetValIssAclL2FilterInPortList->i4_Length);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterAction
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterAction (INT4 i4IssAclL2FilterNo,
                            INT4 i4SetValIssAclL2FilterAction)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        pIssAclL2FilterEntry->IssL2FilterAction = i4SetValIssAclL2FilterAction;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterStatus
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterStatus (INT4 i4IssAclL2FilterNo,
                            INT4 i4SetValIssAclL2FilterStatus)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;
    INT4                i4CommitFlag = 0;
    INT4                i4CommitAction = 0;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        if (pIssAclL2FilterEntry->u1IssL2FilterStatus ==
            (UINT1) i4SetValIssAclL2FilterStatus)
        {
            if (pIssAclL2FilterEntry->u1PriorityFlag == ISS_FALSE)
            {
                return SNMP_SUCCESS;
            }
        }

    }
    else
    {
        /* This Filter Entry is not there in the Database */
        if ((i4SetValIssAclL2FilterStatus != ISS_CREATE_AND_WAIT) &&
            (i4SetValIssAclL2FilterStatus != ISS_CREATE_AND_GO))
        {
            return SNMP_FAILURE;
        }
    }

    switch (i4SetValIssAclL2FilterStatus)
    {

        case ISS_CREATE_AND_WAIT:
        case ISS_CREATE_AND_GO:

            if (ISS_L2FILTERENTRY_ALLOC_MEM_BLOCK (pIssAclL2FilterEntry) ==
                NULL)
            {
                ISS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "No Free L2 Filter Entry"
                         "or h/w init failed at startup\n");
                return SNMP_FAILURE;
            }

            ISS_MEMSET (pIssAclL2FilterEntry, 0, sizeof (tIssL2FilterEntry));

            if (ISS_FILTER_SHADOW_ALLOC_MEM_BLOCK
                (pIssAclL2FilterEntry->pIssFilterShadowInfo) == NULL)
            {
                ISS_L2FILTERENTRY_FREE_MEM_BLOCK (pIssAclL2FilterEntry);
                ISS_TRC (BUFFER_TRC | ALL_FAILURE_TRC,
                         "No Free Filter Shadow Entry\n");
                return SNMP_FAILURE;
            }

            ISS_MEMSET (pIssAclL2FilterEntry->pIssFilterShadowInfo,
                        ISS_ZERO_ENTRY, sizeof (tIssFilterShadowTable));

            /* Setting all the default values for the objects */
            pIssAclL2FilterEntry->i4IssL2FilterNo = i4IssAclL2FilterNo;
            pIssAclL2FilterEntry->i4IssL2FilterPriority =
                ISS_DEFAULT_FILTER_PRIORITY;

            pIssAclL2FilterEntry->u4IssL2FilterProtocolType =
                ISS_DEFAULT_PROTOCOL_TYPE;

            pIssAclL2FilterEntry->i1IssL2FilterCVlanPriority =
                ISS_DEFAULT_VLAN_PRIORITY;
            pIssAclL2FilterEntry->i1IssL2FilterSVlanPriority =
                ISS_DEFAULT_VLAN_PRIORITY;

            pIssAclL2FilterEntry->u4IssL2FilterCustomerVlanId = 0;
            pIssAclL2FilterEntry->u4IssL2FilterServiceVlanId = 0;

            pIssAclL2FilterEntry->u2InnerEtherType = 0;
            pIssAclL2FilterEntry->u2OuterEtherType = 0;
            pIssAclL2FilterEntry->u1IssL2FilterTagType = ISS_FILTER_SINGLE_TAG;

            pIssAclL2FilterEntry->u1FilterDirection = ISS_DIRECTION_IN;

            pIssAclL2FilterEntry->IssL2FilterAction = ISS_ALLOW;

            pIssAclL2FilterEntry->u4IssL2FilterMatchCount = 0;
            pIssAclL2FilterEntry->u1PriorityFlag = ISS_FALSE;
            pIssAclL2FilterEntry->RedirectIfGrp.u1PriorityFlag = ISS_FALSE;

            pIssAclL2FilterEntry->u1IssL2FilterCreationMode
                = ISS_ACL_CREATION_EXTERNAL;

            /* Adding the Entry to the Filter List */
            ISS_SLL_ADD (&(ISS_L2FILTER_LIST),
                         &(pIssAclL2FilterEntry->IssNextNode));

            /* Setting the RowStatus to NOT_READY if the
             * filter if i4SetValIssAclL2FilterStatus is ISS_CREATE_AND_WAIT*/

            pIssAclL2FilterEntry->u1IssL2FilterStatus = ISS_NOT_READY;

            if (i4SetValIssAclL2FilterStatus == ISS_CREATE_AND_GO)
            {
#ifdef NPAPI_WANTED
                {
                    i4RetVal =
                        IssHwUpdateL2Filter (pIssAclL2FilterEntry,
                                             ISS_L2FILTER_ADD);

                    if (i4RetVal != FNP_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                }
#endif

                pIssAclL2FilterEntry->u1IssL2FilterStatus = ISS_ACTIVE;
            }

            break;

        case ISS_NOT_IN_SERVICE:
            if (pIssAclL2FilterEntry->IssL2FilterAction == ISS_REDIRECT_TO)
            {
                pIssAclL2FilterEntry->u1IssL2FilterStatus = ISS_NOT_IN_SERVICE;
                return (SNMP_SUCCESS);
            }
            IssExDeleteAclPriorityFilterTable (pIssAclL2FilterEntry->
                                               i4IssL2FilterPriority,
                                               &(pIssAclL2FilterEntry->
                                                 IssNextNode), ISS_L2FILTER);

            i4CommitFlag = IssExGetIssCommitSupportImmediate ();
            i4CommitAction = IssGetTriggerCommit ();
#ifdef QOSX_WANTED
            /* This Entry will be deleted from the HW using Delete
             * Classification in the qosxtd Module */
            if (pIssAclL2FilterEntry->IssL2FilterAction == ISS_FORQOS)
            {
                /*This will be deleted in UnmapClass */
                pIssAclL2FilterEntry->u1IssL2FilterStatus = ISS_NOT_IN_SERVICE;

                return (SNMP_SUCCESS);
            }
#endif

            if ((i4CommitFlag == ISS_TRUE) || (i4CommitAction == ISS_TRUE))
            {
#ifdef NPAPI_WANTED
                if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                {
                    /* Call the hardware API to update the filter */
                    i4RetVal =
                        IssHwUpdateL2Filter (pIssAclL2FilterEntry,
                                             ISS_L2FILTER_DELETE);

                    if (i4RetVal != FNP_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                    pIssAclL2FilterEntry->u1IssL2HwStatus = ISS_NOTOK;
                }
#endif
            }

            pIssAclL2FilterEntry->u1IssL2FilterStatus = ISS_NOT_IN_SERVICE;

            break;

        case ISS_ACTIVE:
            if (pIssAclL2FilterEntry->IssL2FilterAction == ISS_REDIRECT_TO)
            {
                pIssAclL2FilterEntry->u1IssL2FilterStatus = ISS_ACTIVE;
                break;
            }
            if (pIssAclL2FilterEntry->u1PriorityFlag == ISS_FALSE)
            {
                IssExAddAclToPriorityTable (pIssAclL2FilterEntry->
                                            i4IssL2FilterPriority,
                                            &(pIssAclL2FilterEntry->
                                              IssNextNode), ISS_L2FILTER);
            }

            i4CommitFlag = IssExGetIssCommitSupportImmediate ();
            i4CommitAction = IssGetTriggerCommit ();
#ifdef QOSX_WANTED
            /* This Entry will be Added into the HW using Add Policy or 
             * Classification in the qosxtd Module */
            if (pIssAclL2FilterEntry->IssL2FilterAction == ISS_FORQOS)
            {
                pIssAclL2FilterEntry->u1IssL2FilterStatus = ISS_ACTIVE;
                return (SNMP_SUCCESS);
            }
#endif
            if ((i4CommitFlag == ISS_TRUE) || (i4CommitAction == ISS_TRUE))
            {
#ifdef NPAPI_WANTED
                if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                {
                    /* Call the hardware API to add the L2 filter */
                    i4RetVal =
                        IssHwUpdateL2Filter (pIssAclL2FilterEntry,
                                             ISS_L2FILTER_ADD);

                    if (i4RetVal != FNP_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                    pIssAclL2FilterEntry->u1IssL2HwStatus = ISS_OK;
                }
#endif
            }
            pIssAclL2FilterEntry->u1IssL2FilterStatus = ISS_ACTIVE;

            break;

        case ISS_DESTROY:

            if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
            {
                if ((pIssAclL2FilterEntry->IssL2FilterAction != ISS_REDIRECT_TO)
                    && (pIssAclL2FilterEntry->IssL2FilterAction != ISS_FORQOS))
                {

                    if (pIssAclL2FilterEntry->u1PriorityFlag == ISS_FALSE)
                    {
                        IssExDeleteAclPriorityFilterTable
                            (pIssAclL2FilterEntry->i4IssL2FilterPriority,
                             &(pIssAclL2FilterEntry->IssNextNode),
                             ISS_L2FILTER);
                    }

                    i4CommitFlag = IssExGetIssCommitSupportImmediate ();
                    i4CommitAction = IssGetTriggerCommit ();

                    if ((i4CommitFlag == ISS_TRUE) ||
                        (i4CommitAction == ISS_TRUE))
                    {
#ifdef NPAPI_WANTED
                        if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                        {
                            /* Call the hardware API to delete the filter */
                            i4RetVal =
                                IssHwUpdateL2Filter (pIssAclL2FilterEntry,
                                                     ISS_L2FILTER_DELETE);
                            if (i4RetVal != FNP_SUCCESS)
                            {
                                return SNMP_FAILURE;
                            }
                            pIssAclL2FilterEntry->u1IssL2HwStatus = ISS_NOTOK;
                        }
#endif
                    }
                }
            }
            /* Deleting the node from the list */
            ISS_SLL_DELETE (&(ISS_L2FILTER_LIST),
                            &(pIssAclL2FilterEntry->IssNextNode));
            /* Delete the Entry from the Software */
            if (ISS_FILTER_SHADOW_FREE_MEM_BLOCK
                (pIssAclL2FilterEntry->pIssFilterShadowInfo) != MEM_SUCCESS)
            {
                ISS_TRC (BUFFER_TRC | ALL_FAILURE_TRC,
                         "Filter Shadow Entry Free Failure\n");
                return SNMP_FAILURE;
            }

            if (ISS_L2FILTERENTRY_FREE_MEM_BLOCK (pIssAclL2FilterEntry)
                != MEM_SUCCESS)
            {
                ISS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "L2 Filter Entry Free Failure\n");
                return SNMP_FAILURE;
            }
            break;

        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterOutPortList
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterOutPortList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterOutPortList (INT4 i4IssAclL2FilterNo,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValIssAclL2FilterOutPortList)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;
    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        ISS_MEMSET (pIssAclL2FilterEntry->IssL2FilterOutPortList, 0,
                    ISS_PORT_LIST_SIZE);
        ISS_MEMCPY (pIssAclL2FilterEntry->IssL2FilterOutPortList,
                    pSetValIssAclL2FilterOutPortList->pu1_OctetList,
                    pSetValIssAclL2FilterOutPortList->i4_Length);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterDirection
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterDirection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterDirection (INT4 i4IssAclL2FilterNo,
                               INT4 i4SetValIssAclL2FilterDirection)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        pIssAclL2FilterEntry->u1FilterDirection =
            (UINT1) i4SetValIssAclL2FilterDirection;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterSubAction
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterSubAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterSubAction (INT4
                               i4IssAclL2FilterNo,
                               INT4 i4SetValIssAclL2FilterSubAction)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {

        if (pIssAclL2FilterEntry->u1IssL2SubAction ==
            (UINT1) i4SetValIssAclL2FilterSubAction)
        {
            return SNMP_SUCCESS;
        }

        pIssAclL2FilterEntry->u1IssL2SubAction =
            (UINT1) i4SetValIssAclL2FilterSubAction;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterSubActionId
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterSubActionId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterSubActionId (INT4
                                 i4IssAclL2FilterNo,
                                 INT4 i4SetValIssAclL2FilterSubActionId)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        if (pIssAclL2FilterEntry->u2IssL2SubActionId ==
            (UINT2) i4SetValIssAclL2FilterSubActionId)
        {
            return SNMP_SUCCESS;
        }

        pIssAclL2FilterEntry->u2IssL2SubActionId =
            (UINT2) i4SetValIssAclL2FilterSubActionId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2NextFilterNo
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2NextFilterNo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2NextFilterNo (INT4 i4IssAclL2FilterNo,
                            INT4 i4SetValIssAclL2NextFilterNo)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        if (pIssAclL2FilterEntry->i4IssL2NextFilterNo ==
            i4SetValIssAclL2NextFilterNo)
        {
            return SNMP_SUCCESS;
        }

        pIssAclL2FilterEntry->i4IssL2NextFilterNo =
            i4SetValIssAclL2NextFilterNo;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIssAclL2NextFilterType
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2NextFilterType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2NextFilterType (INT4 i4IssAclL2FilterNo,
                              INT4 i4SetValIssAclL2NextFilterType)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        if (pIssAclL2FilterEntry->i4IssL2NextFilterType ==
            i4SetValIssAclL2NextFilterType)
        {
            return SNMP_SUCCESS;
        }

        pIssAclL2FilterEntry->i4IssL2NextFilterType =
            i4SetValIssAclL2NextFilterType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterPriority
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterPriority (UINT4 *pu4ErrorCode, INT4 i4IssAclL2FilterNo,
                                 INT4 i4TestValIssAclL2FilterPriority)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL &&
        pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL2FilterPriority <= ISS_MAX_FILTER_PRIORITY) &&
        (i4TestValIssAclL2FilterPriority >= ISS_DEFAULT_FILTER_PRIORITY))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterEtherType
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterEtherType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterEtherType (UINT4 *pu4ErrorCode, INT4 i4IssAclL2FilterNo,
                                  INT4 i4TestValIssAclL2FilterEtherType)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL2FilterEtherType < 0) ||
        (i4TestValIssAclL2FilterEtherType > 65535))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterProtocolType
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterProtocolType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterProtocolType (UINT4 *pu4ErrorCode,
                                     INT4 i4IssAclL2FilterNo,
                                     UINT4 u4TestValIssAclL2FilterProtocolType)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (((u4TestValIssAclL2FilterProtocolType < ISS_MIN_PROTOCOL_ID)
         || (u4TestValIssAclL2FilterProtocolType > ISS_MAX_PROTOCOL_ID))
        && (u4TestValIssAclL2FilterProtocolType != ISS_ZERO_ENTRY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterDstMacAddr
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterDstMacAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterDstMacAddr (UINT4 *pu4ErrorCode, INT4 i4IssAclL2FilterNo,
                                   tMacAddr TestValIssAclL2FilterDstMacAddr)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    UNUSED_PARAM (TestValIssAclL2FilterDstMacAddr);

    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterSrcMacAddr
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterSrcMacAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterSrcMacAddr (UINT4 *pu4ErrorCode, INT4 i4IssAclL2FilterNo,
                                   tMacAddr TestValIssAclL2FilterSrcMacAddr)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;
    UNUSED_PARAM (TestValIssAclL2FilterSrcMacAddr);

    UNUSED_PARAM (i4IssAclL2FilterNo);

    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterVlanId
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterVlanId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterVlanId (UINT4 *pu4ErrorCode, INT4 i4IssAclL2FilterNo,
                               INT4 i4TestValIssAclL2FilterVlanId)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL2FilterVlanId < 0) ||
        (i4TestValIssAclL2FilterVlanId > 4094))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterInPortList
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterInPortList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterInPortList (UINT4 *pu4ErrorCode, INT4 i4IssAclL2FilterNo,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValIssAclL2FilterInPortList)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pTestValIssAclL2FilterInPortList->i4_Length <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    /* This function checks if any port greater than the maximum number of
     * ports in the system is in the port list.*/

    if (IssIsPortListValid
        (pTestValIssAclL2FilterInPortList->pu1_OctetList,
         pTestValIssAclL2FilterInPortList->i4_Length) == ISS_FALSE)
    {
        if (pTestValIssAclL2FilterInPortList->i4_Length > ISS_PORT_LIST_SIZE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        }
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterAction
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterAction (UINT4 *pu4ErrorCode, INT4 i4IssAclL2FilterNo,
                               INT4 i4TestValIssAclL2FilterAction)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL2FilterAction == ISS_ALLOW)
        || (i4TestValIssAclL2FilterAction == ISS_DROP)
        || (i4TestValIssAclL2FilterAction == ISS_FORQOS)
        || (i4TestValIssAclL2FilterAction == ISS_REDIRECT_TO)
        || (i4TestValIssAclL2FilterAction == ISS_SWITCH_COPYTOCPU)
        || (i4TestValIssAclL2FilterAction == ISS_DROP_COPYTOCPU))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterStatus
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterStatus (UINT4 *pu4ErrorCode, INT4 i4IssAclL2FilterNo,
                               INT4 i4TestValIssAclL2FilterStatus)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;
    INT4                i4RetStatus = ISS_ZERO_ENTRY;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL2FilterStatus < ISS_ACTIVE)
        || (i4TestValIssAclL2FilterStatus > ISS_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry == NULL &&
        (i4TestValIssAclL2FilterStatus != ISS_CREATE_AND_WAIT
         && i4TestValIssAclL2FilterStatus != ISS_CREATE_AND_GO))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pIssAclL2FilterEntry != NULL &&
        (i4TestValIssAclL2FilterStatus == ISS_CREATE_AND_WAIT
         || i4TestValIssAclL2FilterStatus == ISS_CREATE_AND_GO))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pIssAclL2FilterEntry != NULL &&
        (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_NOT_READY &&
         i4TestValIssAclL2FilterStatus == ISS_NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pIssAclL2FilterEntry != NULL &&
        (pIssAclL2FilterEntry->u1IssL2FilterStatus != ISS_ACTIVE &&
         i4TestValIssAclL2FilterStatus == ISS_NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (nmhGetIssAclTrafficSeperationCtrl (&i4RetStatus) == SNMP_SUCCESS)
    {
        if (i4RetStatus == ACL_TRAFFICSEPRTN_CTRL_SYSTEM_DEFAULT)
        {
            if ((pIssAclL2FilterEntry != NULL) &&
                (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE &&
                 i4TestValIssAclL2FilterStatus == ISS_DESTROY) &&
                (pIssAclL2FilterEntry->u1IssL2FilterCreationMode ==
                 ISS_ACL_CREATION_INTERNAL))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
    }

#if defined (DIFFSRV_WANTED)
    if ((i4TestValIssAclL2FilterStatus == ISS_DESTROY)
        || (i4TestValIssAclL2FilterStatus == ISS_NOT_IN_SERVICE))
    {
        ISS_UNLOCK ();

        if (DsCheckMFClfrTable (i4IssAclL2FilterNo, DS_MAC_FILTER) ==
            DS_FAILURE)
        {
            CLI_SET_ERR (CLI_ACL_FILTER_NO_CHANGE);
            ISS_LOCK ();
            return SNMP_FAILURE;
        }

        ISS_LOCK ();
    }
#endif

#ifdef QOSX_WANTED
    /* Check the RefCount in IssAclL2Filter node -how many times it is used 
     * in the qosxtd Module */
    if ((i4TestValIssAclL2FilterStatus == ISS_DESTROY)
        || (i4TestValIssAclL2FilterStatus == ISS_NOT_IN_SERVICE))
    {
        if (pIssAclL2FilterEntry->u4RefCount > 0)
        {
            CLI_SET_ERR (CLI_ACL_FILTER_NO_CHANGE);
            return (SNMP_FAILURE);
        }
    }
#endif

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterOutPortList
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterOutPortList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterOutPortList (UINT4 *pu4ErrorCode,
                                    INT4 i4IssAclL2FilterNo,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValIssAclL2FilterOutPortList)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pTestValIssAclL2FilterOutPortList->i4_Length <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    /* This function checks if any port greater than the maximum number of
     * ports in the system is in the port list.*/

    if (IssIsPortListValid (pTestValIssAclL2FilterOutPortList->pu1_OctetList,
                            pTestValIssAclL2FilterOutPortList->i4_Length) ==
        ISS_FALSE)
    {
        if (pTestValIssAclL2FilterOutPortList->i4_Length > ISS_PORT_LIST_SIZE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        }
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterSubAction
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterSubAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterSubAction (UINT4
                                  *pu4ErrorCode,
                                  INT4
                                  i4IssAclL2FilterNo,
                                  INT4 i4TestValIssAclL2FilterSubAction)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL2FilterSubAction != ISS_NONE) &&
        (i4TestValIssAclL2FilterSubAction != ISS_MODIFY_VLAN) &&
        (i4TestValIssAclL2FilterSubAction != ISS_NESTED_VLAN) &&
        (i4TestValIssAclL2FilterSubAction != ISS_STRIP_OUTER_HDR))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterSubActionId
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterSubActionId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterSubActionId (UINT4
                                    *pu4ErrorCode,
                                    INT4
                                    i4IssAclL2FilterNo,
                                    INT4 i4TestValIssAclL2FilterSubActionId)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL2FilterSubActionId < 0) ||
        (i4TestValIssAclL2FilterSubActionId > 4094))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2NextFilterNo
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2NextFilterNo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2NextFilterNo (UINT4 *pu4ErrorCode, INT4 i4IssAclL2FilterNo,
                               INT4 i4TestValIssAclL2NextFilterNo)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL2NextFilterNo < 0) ||
        (i4TestValIssAclL2NextFilterNo > 65535))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2NextFilterType
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2NextFilterType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2NextFilterType (UINT4 *pu4ErrorCode, INT4 i4IssAclL2FilterNo,
                                 INT4 i4TestValIssAclL2NextFilterType)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL2NextFilterType != ISS_L2FILTER) &&
        (i4TestValIssAclL2NextFilterType != ISS_L3FILTER) &&
        (i4TestValIssAclL2NextFilterType != ISS_UDBFILTER) &&
        (i4TestValIssAclL2NextFilterType != ISS_NOFILTER))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterDirection
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterDirection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterDirection (UINT4 *pu4ErrorCode, INT4 i4IssAclL2FilterNo,
                                  INT4 i4TestValIssAclL2FilterDirection)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValIssAclL2FilterDirection != ISS_DIRECTION_IN &&
        i4TestValIssAclL2FilterDirection != ISS_DIRECTION_OUT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IssAclL2FilterTable
 Input       :  The Indices
                IssAclL2FilterNo
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IssAclL2FilterTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IssAclL3FilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssAclL3FilterTable
 Input       :  The Indices
                IssAclL3FilterNo
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssAclL3FilterTable (INT4 i4IssAclL3FilterNo)
{
    tIssSllNode        *pSllNode = NULL;
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pSllNode = ISS_SLL_FIRST (&ISS_L3FILTER_LIST);

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) != ISS_TRUE)
    {
        return SNMP_FAILURE;
    }
    while (pSllNode != NULL)
    {
        pIssAclL3FilterEntry = (tIssL3FilterEntry *) pSllNode;
        if (pIssAclL3FilterEntry->i4IssL3FilterNo == i4IssAclL3FilterNo)
        {
            return SNMP_SUCCESS;
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L3FILTER_LIST, pSllNode);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssAclL3FilterTable
 Input       :  The Indices
                IssAclL3FilterNo
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssAclL3FilterTable (INT4 *pi4IssAclL3FilterNo)
{
    if ((IssExtSnmpLowGetFirstValidL3FilterTableIndex (pi4IssAclL3FilterNo)) ==
        ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexIssAclL3FilterTable
 Input       :  The Indices
                IssAclL3FilterNo
                nextIssAclL3FilterNo
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIssAclL3FilterTable (INT4 i4IssAclL3FilterNo,
                                    INT4 *pi4NextIssAclL3FilterNo)
{

    if (i4IssAclL3FilterNo < 0)
    {
        return SNMP_FAILURE;
    }

    if ((IssExtSnmpLowGetNextValidL3FilterTableIndex
         (i4IssAclL3FilterNo, pi4NextIssAclL3FilterNo)) == ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterPriority
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterPriority (INT4 i4IssAclL3FilterNo,
                              INT4 *pi4RetValIssAclL3FilterPriority)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterPriority =
            pIssAclL3FilterEntry->i4IssL3FilterPriority;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterProtocol
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterProtocol (INT4 i4IssAclL3FilterNo,
                              INT4 *pi4RetValIssAclL3FilterProtocol)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterProtocol =
            pIssAclL3FilterEntry->IssL3FilterProtocol;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterMessageType
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterMessageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterMessageType (INT4 i4IssAclL3FilterNo,
                                 INT4 *pi4RetValIssAclL3FilterMessageType)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterMessageType =
            pIssAclL3FilterEntry->i4IssL3FilterMessageType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterMessageCode
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterMessageCode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterMessageCode (INT4 i4IssAclL3FilterNo,
                                 INT4 *pi4RetValIssAclL3FilterMessageCode)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterMessageCode =
            pIssAclL3FilterEntry->i4IssL3FilterMessageCode;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilteAddrType
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilteAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilteAddrType (INT4 i4IssAclL3FilterNo,
                             INT4 *pi4RetValIssAclL3FilteAddrType)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValIssAclL3FilteAddrType =
        pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterDstIpAddr
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterDstIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterDstIpAddr (INT4 i4IssAclL3FilterNo,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValIssAclL3FilterDstIpAddr)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV4)
        {
            PTR_ASSIGN4 (pRetValIssAclL3FilterDstIpAddr->pu1_OctetList,
                         pIssL3FilterEntry->u4IssL3FilterDstIpAddr);
            pRetValIssAclL3FilterDstIpAddr->i4_Length = QOS_IPV4_LEN;
            return SNMP_SUCCESS;
        }
        else if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV6)
        {
            Ip6AddrCopy ((tIp6Addr *) (VOID *)
                         pRetValIssAclL3FilterDstIpAddr->pu1_OctetList,
                         &pIssL3FilterEntry->ipv6DstIpAddress);

            pRetValIssAclL3FilterDstIpAddr->i4_Length = QOS_IPV6_LEN;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterSrcIpAddr
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterSrcIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterSrcIpAddr (INT4 i4IssAclL3FilterNo,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValIssAclL3FilterSrcIpAddr)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV4)
        {
            PTR_ASSIGN4 (pRetValIssAclL3FilterSrcIpAddr->pu1_OctetList,
                         pIssL3FilterEntry->u4IssL3FilterSrcIpAddr);
            pRetValIssAclL3FilterSrcIpAddr->i4_Length = QOS_IPV4_LEN;
            return SNMP_SUCCESS;
        }
        else if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV6)
        {
            Ip6AddrCopy ((tIp6Addr *) (VOID *) pRetValIssAclL3FilterSrcIpAddr->
                         pu1_OctetList, &pIssL3FilterEntry->ipv6SrcIpAddress);

            pRetValIssAclL3FilterSrcIpAddr->i4_Length = QOS_IPV6_LEN;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterDstIpAddrPrefixLength
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterDstIpAddrPrefixLength
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterDstIpAddrPrefixLength (INT4 i4IssAclL3FilterNo,
                                           UINT4
                                           *pu4RetValIssAclL3FilterDstIpAddrPrefixLength)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pu4RetValIssAclL3FilterDstIpAddrPrefixLength =
            pIssL3FilterEntry->u4IssL3FilterMultiFieldClfrDstPrefixLength;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterSrcIpAddrPrefixLength
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterSrcIpAddrPrefixLength
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterSrcIpAddrPrefixLength (INT4 i4IssAclL3FilterNo,
                                           UINT4
                                           *pu4RetValIssAclL3FilterSrcIpAddrPrefixLength)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pu4RetValIssAclL3FilterSrcIpAddrPrefixLength =
            pIssL3FilterEntry->u4IssL3FilterMultiFieldClfrSrcPrefixLength;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterMinDstProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterMinDstProtPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterMinDstProtPort (INT4 i4IssAclL3FilterNo,
                                    UINT4
                                    *pu4RetValIssAclL3FilterMinDstProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pu4RetValIssAclL3FilterMinDstProtPort =
            pIssAclL3FilterEntry->u4IssL3FilterMinDstProtPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterMaxDstProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterMaxDstProtPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterMaxDstProtPort (INT4 i4IssAclL3FilterNo,
                                    UINT4
                                    *pu4RetValIssAclL3FilterMaxDstProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pu4RetValIssAclL3FilterMaxDstProtPort =
            pIssAclL3FilterEntry->u4IssL3FilterMaxDstProtPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterMinSrcProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterMinSrcProtPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterMinSrcProtPort (INT4 i4IssAclL3FilterNo,
                                    UINT4
                                    *pu4RetValIssAclL3FilterMinSrcProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pu4RetValIssAclL3FilterMinSrcProtPort =
            pIssAclL3FilterEntry->u4IssL3FilterMinSrcProtPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterMaxSrcProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterMaxSrcProtPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterMaxSrcProtPort (INT4 i4IssAclL3FilterNo,
                                    UINT4
                                    *pu4RetValIssAclL3FilterMaxSrcProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pu4RetValIssAclL3FilterMaxSrcProtPort =
            pIssAclL3FilterEntry->u4IssL3FilterMaxSrcProtPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterInPortList
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterInPortList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterInPortList (INT4 i4IssAclL3FilterNo,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValIssAclL3FilterInPortList)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        ISS_MEMSET (pRetValIssAclL3FilterInPortList->pu1_OctetList, 0,
                    ISS_PORT_LIST_SIZE);
        ISS_ADD_PORT_LIST (pRetValIssAclL3FilterInPortList->pu1_OctetList,
                           pIssAclL3FilterEntry->IssL3FilterInPortList);
        pRetValIssAclL3FilterInPortList->i4_Length = ISS_PORT_LIST_SIZE;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterOutPortList
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterOutPortList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterOutPortList (INT4 i4IssAclL3FilterNo,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValIssAclL3FilterOutPortList)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        ISS_MEMSET (pRetValIssAclL3FilterOutPortList->pu1_OctetList, 0,
                    ISS_PORT_LIST_SIZE);
        ISS_ADD_PORT_LIST (pRetValIssAclL3FilterOutPortList->pu1_OctetList,
                           pIssAclL3FilterEntry->IssL3FilterOutPortList);
        pRetValIssAclL3FilterOutPortList->i4_Length = ISS_PORT_LIST_SIZE;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterAckBit
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterAckBit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterAckBit (INT4 i4IssAclL3FilterNo,
                            INT4 *pi4RetValIssAclL3FilterAckBit)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterAckBit =
            pIssAclL3FilterEntry->IssL3FilterAckBit;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterRstBit
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterRstBit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterRstBit (INT4 i4IssAclL3FilterNo,
                            INT4 *pi4RetValIssAclL3FilterRstBit)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterRstBit =
            pIssAclL3FilterEntry->IssL3FilterRstBit;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterTos
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterTos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterTos (INT4 i4IssAclL3FilterNo,
                         INT4 *pi4RetValIssAclL3FilterTos)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterTos = pIssAclL3FilterEntry->IssL3FilterTos;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterDscp
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterDscp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterDscp (INT4 i4IssAclL3FilterNo,
                          INT4 *pi4RetValIssAclL3FilterDscp)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterDscp = pIssAclL3FilterEntry->i4IssL3FilterDscp;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterDirection
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterDirection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterDirection (INT4 i4IssAclL3FilterNo,
                               INT4 *pi4RetValIssAclL3FilterDirection)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterDirection =
            pIssAclL3FilterEntry->IssL3FilterDirection;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterAction
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterAction (INT4 i4IssAclL3FilterNo,
                            INT4 *pi4RetValIssAclL3FilterAction)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterAction =
            pIssAclL3FilterEntry->IssL3FilterAction;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterMatchCount
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterMatchCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterMatchCount (INT4 i4IssAclL3FilterNo,
                                UINT4 *pu4RetValIssAclL3FilterMatchCount)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pu4RetValIssAclL3FilterMatchCount =
            pIssAclL3FilterEntry->u4IssL3FilterMatchCount;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterFlowId
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterFlowId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterFlowId (INT4 i4IssAclL3FilterNo,
                            UINT4 *pu4RetValIssAclL3FilterFlowId)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);
    if (pIssL3FilterEntry != NULL)
    {
        *pu4RetValIssAclL3FilterFlowId =
            pIssL3FilterEntry->u4IssL3MultiFieldClfrFlowId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterStatus
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterStatus (INT4 i4IssAclL3FilterNo,
                            INT4 *pi4RetValIssAclL3FilterStatus)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterStatus =
            (INT4) pIssAclL3FilterEntry->u1IssL3FilterStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterSubAction
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterSubAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterSubAction (INT4 i4IssAclL3FilterNo,
                               INT4 *pi4RetValIssAclL3FilterSubAction)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterSubAction =
            (INT4) pIssAclL3FilterEntry->u1IssL3SubAction;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterSubActionId
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterSubActionId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterSubActionId (INT4
                                 i4IssAclL3FilterNo,
                                 INT4 *pi4RetValIssAclL3FilterSubActionId)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterSubActionId =
            (INT4) pIssAclL3FilterEntry->u2IssL3SubActionId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterRedirectId
 Input       :  The Indices
                IssAclL3FilterNo

                The Object
                retValIssAclL3FilterRedirectId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterRedirectId (INT4 i4IssAclL3FilterNo,
                                INT4 *pi4RetValIssAclL3FilterRedirectId)
{

    UINT4               u4Index = 0;

    for (u4Index = 0; u4Index < ISS_MAX_REDIRECT_GRP_ID; u4Index++)
    {
        if ((gpIssRedirectIntfInfo[u4Index].u4AclId ==
             (UINT4) i4IssAclL3FilterNo)
            && (gpIssRedirectIntfInfo[u4Index].u1AclIdType == ISS_L3_REDIRECT))
        {
            *pi4RetValIssAclL3FilterRedirectId = u4Index + 1;
            break;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterCreationMode
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterCreationMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterCreationMode (INT4 i4IssAclL3FilterNo,
                                  INT4 *pi4RetValIssAclL3FilterCreationMode)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterCreationMode =
            (INT4) pIssAclL3FilterEntry->u1IssL3FilterCreationMode;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterPriority
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterPriority (INT4 i4IssAclL3FilterNo,
                              INT4 i4SetValIssAclL3FilterPriority)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        pIssAclL3FilterEntry->i4IssL3FilterPriority =
            i4SetValIssAclL3FilterPriority;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterProtocol
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterProtocol
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterProtocol (INT4 i4IssAclL3FilterNo,
                              INT4 i4SetValIssAclL3FilterProtocol)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        pIssAclL3FilterEntry->IssL3FilterProtocol =
            i4SetValIssAclL3FilterProtocol;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterMessageType
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterMessageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterMessageType (INT4 i4IssAclL3FilterNo,
                                 INT4 i4SetValIssAclL3FilterMessageType)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        pIssAclL3FilterEntry->i4IssL3FilterMessageType =
            i4SetValIssAclL3FilterMessageType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterMessageCode
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterMessageCode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterMessageCode (INT4 i4IssAclL3FilterNo,
                                 INT4 i4SetValIssAclL3FilterMessageCode)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        pIssAclL3FilterEntry->i4IssL3FilterMessageCode =
            i4SetValIssAclL3FilterMessageCode;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilteAddrType
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilteAddrType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilteAddrType (INT4 i4IssAclL3FilterNo,
                             INT4 i4SetValIssAclL3FilteAddrType)
{
    if (IssSetL3FilterAddrType (i4IssAclL3FilterNo,
                                i4SetValIssAclL3FilteAddrType) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterDstIpAddr
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterDstIpAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterDstIpAddr (INT4 i4IssAclL3FilterNo,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValIssAclL3FilterDstIpAddr)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    INT4                i4RetStatus = SNMP_FAILURE;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);
    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV6)
        {
            Ip6AddrCopy (&pIssL3FilterEntry->ipv6DstIpAddress,
                         (tIp6Addr *) (VOID *) pSetValIssAclL3FilterDstIpAddr->
                         pu1_OctetList);
            i4RetStatus = SNMP_SUCCESS;
        }
        if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV4)
        {
            PTR_FETCH4 (pIssL3FilterEntry->u4IssL3FilterDstIpAddr,
                        pSetValIssAclL3FilterDstIpAddr->pu1_OctetList);

            i4RetStatus = SNMP_SUCCESS;
        }
    }
    if (i4RetStatus == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterSrcIpAddr
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterSrcIpAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterSrcIpAddr (INT4 i4IssAclL3FilterNo,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValIssAclL3FilterSrcIpAddr)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    INT4                i4RetStatus = SNMP_FAILURE;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV6)
        {
            Ip6AddrCopy (&pIssL3FilterEntry->ipv6SrcIpAddress,
                         (tIp6Addr *) (VOID *) pSetValIssAclL3FilterSrcIpAddr->
                         pu1_OctetList);
            i4RetStatus = SNMP_SUCCESS;
        }
        if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV4)
        {
            PTR_FETCH4 (pIssL3FilterEntry->u4IssL3FilterSrcIpAddr,
                        pSetValIssAclL3FilterSrcIpAddr->pu1_OctetList);

            i4RetStatus = SNMP_SUCCESS;
        }
    }

    if (i4RetStatus == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterDstIpAddrPrefixLength
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterDstIpAddrPrefixLength
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterDstIpAddrPrefixLength (INT4 i4IssAclL3FilterNo,
                                           UINT4
                                           u4SetValIssAclL3FilterDstIpAddrPrefixLength)
{
    if (IssSetL3FilterDstPrefixLength (i4IssAclL3FilterNo,
                                       u4SetValIssAclL3FilterDstIpAddrPrefixLength)
        != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterSrcIpAddrPrefixLength
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterSrcIpAddrPrefixLength
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterSrcIpAddrPrefixLength (INT4 i4IssAclL3FilterNo,
                                           UINT4
                                           u4SetValIssAclL3FilterSrcIpAddrPrefixLength)
{
    if (IssSetL3FilterSrcPrefixLength (i4IssAclL3FilterNo,
                                       u4SetValIssAclL3FilterSrcIpAddrPrefixLength)
        != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterMinDstProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterMinDstProtPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterMinDstProtPort (INT4 i4IssAclL3FilterNo,
                                    UINT4 u4SetValIssAclL3FilterMinDstProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        pIssAclL3FilterEntry->u4IssL3FilterMinDstProtPort =
            u4SetValIssAclL3FilterMinDstProtPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterMaxDstProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterMaxDstProtPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterMaxDstProtPort (INT4 i4IssAclL3FilterNo,
                                    UINT4 u4SetValIssAclL3FilterMaxDstProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        pIssAclL3FilterEntry->u4IssL3FilterMaxDstProtPort =
            u4SetValIssAclL3FilterMaxDstProtPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterMinSrcProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterMinSrcProtPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterMinSrcProtPort (INT4 i4IssAclL3FilterNo,
                                    UINT4 u4SetValIssAclL3FilterMinSrcProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        pIssAclL3FilterEntry->u4IssL3FilterMinSrcProtPort =
            u4SetValIssAclL3FilterMinSrcProtPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterMaxSrcProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterMaxSrcProtPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterMaxSrcProtPort (INT4 i4IssAclL3FilterNo,
                                    UINT4 u4SetValIssAclL3FilterMaxSrcProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        pIssAclL3FilterEntry->u4IssL3FilterMaxSrcProtPort =
            u4SetValIssAclL3FilterMaxSrcProtPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterInPortList
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterInPortList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterInPortList (INT4 i4IssAclL3FilterNo,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValIssAclL3FilterInPortList)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        ISS_MEMSET (pIssAclL3FilterEntry->IssL3FilterInPortList, 0,
                    ISS_PORT_LIST_SIZE);
        ISS_MEMCPY (pIssAclL3FilterEntry->IssL3FilterInPortList,
                    pSetValIssAclL3FilterInPortList->pu1_OctetList,
                    pSetValIssAclL3FilterInPortList->i4_Length);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterOutPortList
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterOutPortList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterOutPortList (INT4 i4IssAclL3FilterNo,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValIssAclL3FilterOutPortList)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        ISS_MEMSET (pIssAclL3FilterEntry->IssL3FilterOutPortList, 0,
                    ISS_PORT_LIST_SIZE);
        ISS_MEMCPY (pIssAclL3FilterEntry->IssL3FilterOutPortList,
                    pSetValIssAclL3FilterOutPortList->pu1_OctetList,
                    pSetValIssAclL3FilterOutPortList->i4_Length);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterAckBit
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterAckBit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterAckBit (INT4 i4IssAclL3FilterNo,
                            INT4 i4SetValIssAclL3FilterAckBit)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        pIssAclL3FilterEntry->IssL3FilterAckBit = i4SetValIssAclL3FilterAckBit;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterRstBit
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterRstBit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterRstBit (INT4 i4IssAclL3FilterNo,
                            INT4 i4SetValIssAclL3FilterRstBit)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        pIssAclL3FilterEntry->IssL3FilterRstBit = i4SetValIssAclL3FilterRstBit;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterTos
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterTos
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterTos (INT4 i4IssAclL3FilterNo,
                         INT4 i4SetValIssAclL3FilterTos)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        pIssAclL3FilterEntry->IssL3FilterTos = i4SetValIssAclL3FilterTos;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterDscp
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterDscp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterDscp (INT4 i4IssAclL3FilterNo,
                          INT4 i4SetValIssAclL3FilterDscp)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        pIssAclL3FilterEntry->i4IssL3FilterDscp = i4SetValIssAclL3FilterDscp;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterDirection
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterDirection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterDirection (INT4 i4IssAclL3FilterNo,
                               INT4 i4SetValIssAclL3FilterDirection)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        pIssAclL3FilterEntry->IssL3FilterDirection =
            i4SetValIssAclL3FilterDirection;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterAction
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterAction (INT4 i4IssAclL3FilterNo,
                            INT4 i4SetValIssAclL3FilterAction)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        pIssAclL3FilterEntry->IssL3FilterAction = i4SetValIssAclL3FilterAction;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterFlowId
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterFlowId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterFlowId (INT4 i4IssAclL3FilterNo,
                            UINT4 u4SetValIssAclL3FilterFlowId)
{
    if (IssSetL3FilterControlFlowId (i4IssAclL3FilterNo,
                                     u4SetValIssAclL3FilterFlowId) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterStatus
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterStatus (INT4 i4IssAclL3FilterNo,
                            INT4 i4SetValIssAclL3FilterStatus)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif
    INT4                i4CommitFlag = 0;
    INT4                i4CommitAction = 0;
    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus ==
            (UINT1) i4SetValIssAclL3FilterStatus)
        {
            if (pIssAclL3FilterEntry->u1PriorityFlag == ISS_FALSE)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    else
    {
        /* This Filter Entry is not there in the Database */
        if ((i4SetValIssAclL3FilterStatus != ISS_CREATE_AND_WAIT) &&
            (i4SetValIssAclL3FilterStatus != ISS_CREATE_AND_GO))
        {
            return SNMP_FAILURE;
        }
    }

    switch (i4SetValIssAclL3FilterStatus)
    {

        case ISS_CREATE_AND_WAIT:
        case ISS_CREATE_AND_GO:

            if (ISS_L3FILTERENTRY_ALLOC_MEM_BLOCK (pIssAclL3FilterEntry) ==
                NULL)
            {
                ISS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "No Free L3 Filter Entry"
                         "or h/w init failed at startup\n");
                return SNMP_FAILURE;
            }

            ISS_MEMSET (pIssAclL3FilterEntry, 0, sizeof (tIssL3FilterEntry));

            if (ISS_FILTER_SHADOW_ALLOC_MEM_BLOCK
                (pIssAclL3FilterEntry->pIssFilterShadowInfo) == NULL)
            {
                ISS_L3FILTERENTRY_FREE_MEM_BLOCK (pIssAclL3FilterEntry);
                ISS_TRC (BUFFER_TRC | ALL_FAILURE_TRC,
                         "No Free Filter Shadow Entry\n");
                return SNMP_FAILURE;
            }

            ISS_MEMSET (pIssAclL3FilterEntry->pIssFilterShadowInfo,
                        ISS_ZERO_ENTRY, sizeof (tIssFilterShadowTable));

            /* Setting all the default values for the objects */
            pIssAclL3FilterEntry->i4IssL3FilterNo = i4IssAclL3FilterNo;

            pIssAclL3FilterEntry->i4IssL3FilterPriority =
                ISS_DEFAULT_FILTER_PRIORITY;

            pIssAclL3FilterEntry->IssL3FilterProtocol = ISS_PROTO_ANY;
            pIssAclL3FilterEntry->u4SVlanId = 0;
            pIssAclL3FilterEntry->i1SVlanPriority = ISS_DEFAULT_VLAN_PRIORITY;
            pIssAclL3FilterEntry->u4CVlanId = 0;
            pIssAclL3FilterEntry->i1CVlanPriority = ISS_DEFAULT_VLAN_PRIORITY;
            pIssAclL3FilterEntry->u1IssL3FilterTagType = ISS_FILTER_SINGLE_TAG;
            pIssAclL3FilterEntry->i4IssL3FilterMessageType = ISS_ANY;
            pIssAclL3FilterEntry->i4IssL3FilterMessageCode = ISS_ANY;
            pIssAclL3FilterEntry->u4IssL3FilterDstIpAddr = ISS_ZERO_ENTRY;
            pIssAclL3FilterEntry->u4IssL3FilterSrcIpAddr = ISS_ZERO_ENTRY;
            pIssAclL3FilterEntry->u4IssL3FilterDstIpAddrMask =
                ISS_DEF_FILTER_MASK;
            pIssAclL3FilterEntry->u4IssL3FilterSrcIpAddrMask =
                ISS_DEF_FILTER_MASK;
            pIssAclL3FilterEntry->u4IssL3FilterMinDstProtPort = ISS_ZERO_ENTRY;
            pIssAclL3FilterEntry->u4IssL3FilterMaxDstProtPort =
                ISS_MAX_PORT_VALUE;
            pIssAclL3FilterEntry->u4IssL3FilterMinSrcProtPort = ISS_ZERO_ENTRY;
            pIssAclL3FilterEntry->u4IssL3FilterMaxSrcProtPort =
                ISS_MAX_PORT_VALUE;
            pIssAclL3FilterEntry->IssL3FilterAckBit = ISS_ACK_ANY;
            pIssAclL3FilterEntry->IssL3FilterRstBit = ISS_ACK_ANY;
            pIssAclL3FilterEntry->IssL3FilterTos = ISS_TOS_INVALID;
            pIssAclL3FilterEntry->i4IssL3FilterDscp = ISS_DSCP_INVALID;
            pIssAclL3FilterEntry->IssL3FilterDirection = ISS_DIRECTION_IN;
            pIssAclL3FilterEntry->IssL3FilterAction = ISS_ALLOW;
            pIssAclL3FilterEntry->u4IssL3FilterMatchCount = ISS_ZERO_ENTRY;
            pIssAclL3FilterEntry->u4IssL3FilterMultiFieldClfrDstPrefixLength =
                ISS_ZERO_ENTRY;
            pIssAclL3FilterEntry->u4IssL3FilterMultiFieldClfrSrcPrefixLength =
                ISS_ZERO_ENTRY;
            pIssAclL3FilterEntry->u4IssL3MultiFieldClfrFlowId = ISS_ZERO_ENTRY;
            pIssAclL3FilterEntry->i4IssL3MultiFieldClfrAddrType = QOS_IPV4;
            pIssAclL3FilterEntry->u4RefCount = 0;
            pIssAclL3FilterEntry->u1IssL3FilterStatus = 0;
            pIssAclL3FilterEntry->u1PriorityFlag = ISS_FALSE;
            pIssAclL3FilterEntry->RedirectIfGrp.u1PriorityFlag = ISS_FALSE;
            pIssAclL3FilterEntry->u1IssL3FilterCreationMode
                = ISS_ACL_CREATION_EXTERNAL;

            /* Adding the Entry to the Filter List */
            ISS_SLL_ADD (&(ISS_L3FILTER_LIST),
                         &(pIssAclL3FilterEntry->IssNextNode));

            /* Setting the RowStatus to NOT_READY as all 
             * mandatory objects are not set taken. */
            pIssAclL3FilterEntry->u1IssL3FilterStatus = ISS_NOT_READY;

            if (i4SetValIssAclL3FilterStatus == ISS_CREATE_AND_GO)
            {
#ifdef NPAPI_WANTED
                {
                    /* Call the hardware API to add the L3 filter */
                    i4RetVal =
                        IssHwUpdateL3Filter (pIssAclL3FilterEntry,
                                             ISS_L3FILTER_ADD);

                    if (i4RetVal != FNP_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                }
#endif
                pIssAclL3FilterEntry->u1IssL3FilterStatus = ISS_ACTIVE;

            }

            break;

        case ISS_NOT_IN_SERVICE:

#ifdef QOSX_WANTED
            /* This Entry will be deleted from the HW using Delete
             * Classification in the qosxtd Module */
            if (pIssAclL3FilterEntry->IssL3FilterAction == ISS_FORQOS)
            {
                /*This will be deleted in UnmapClass */
                pIssAclL3FilterEntry->u1IssL3FilterStatus = ISS_NOT_IN_SERVICE;
                return (SNMP_SUCCESS);
            }
#endif
            if (pIssAclL3FilterEntry->IssL3FilterAction == ISS_REDIRECT_TO)
            {
                pIssAclL3FilterEntry->u1IssL3FilterStatus = ISS_NOT_IN_SERVICE;
                return (SNMP_SUCCESS);
            }
            IssExDeleteAclPriorityFilterTable (pIssAclL3FilterEntry->
                                               i4IssL3FilterPriority,
                                               &(pIssAclL3FilterEntry->
                                                 IssNextNode), ISS_L3FILTER);

            i4CommitFlag = IssExGetIssCommitSupportImmediate ();
            i4CommitAction = IssGetTriggerCommit ();

            if ((i4CommitFlag == ISS_TRUE) || (i4CommitAction == ISS_TRUE))
            {

#ifdef NPAPI_WANTED
                if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                {
                    /* Call the hardware API to update the filter */
                    i4RetVal =
                        IssHwUpdateL3Filter (pIssAclL3FilterEntry,
                                             ISS_L3FILTER_DELETE);
                    if (i4RetVal != FNP_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                    pIssAclL3FilterEntry->u1IssL3HwStatus = ISS_NOTOK;
                }
#endif
            }

            pIssAclL3FilterEntry->u1IssL3FilterStatus = ISS_NOT_IN_SERVICE;

            return SNMP_SUCCESS;

        case ISS_ACTIVE:

#ifdef QOSX_WANTED
            /* This Entry will be Added into the HW using Add Policy or 
             * Classification in the qosxtd Module */
            if (pIssAclL3FilterEntry->IssL3FilterAction == ISS_FORQOS)
            {
                pIssAclL3FilterEntry->u1IssL3FilterStatus = ISS_ACTIVE;
                return (SNMP_SUCCESS);
            }
#endif
            if (pIssAclL3FilterEntry->IssL3FilterAction == ISS_REDIRECT_TO)
            {
                pIssAclL3FilterEntry->u1IssL3FilterStatus = ISS_ACTIVE;
                break;
            }
            if (pIssAclL3FilterEntry->u1PriorityFlag == ISS_FALSE)
            {
                IssExAddAclToPriorityTable (pIssAclL3FilterEntry->
                                            i4IssL3FilterPriority,
                                            &(pIssAclL3FilterEntry->
                                              IssNextNode), ISS_L3FILTER);
            }

            i4CommitFlag = IssExGetIssCommitSupportImmediate ();
            i4CommitAction = IssGetTriggerCommit ();

            if ((i4CommitFlag == ISS_TRUE) || (i4CommitAction == ISS_TRUE))
            {

#ifdef NPAPI_WANTED
                if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                {
                    /* Call the hardware API to add the L3 filter */
                    i4RetVal =
                        IssHwUpdateL3Filter (pIssAclL3FilterEntry,
                                             ISS_L3FILTER_ADD);

                    if (i4RetVal != FNP_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                    pIssAclL3FilterEntry->u1IssL3HwStatus = ISS_OK;
                }
#endif
            }

            pIssAclL3FilterEntry->u1IssL3FilterStatus = ISS_ACTIVE;

            break;

        case ISS_DESTROY:

            if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
            {
                if ((pIssAclL3FilterEntry->IssL3FilterAction != ISS_FORQOS) ||
                    (pIssAclL3FilterEntry->IssL3FilterAction !=
                     ISS_REDIRECT_TO))
                {

                    if (pIssAclL3FilterEntry->u1PriorityFlag == ISS_FALSE)
                    {
                        IssExDeleteAclPriorityFilterTable
                            (pIssAclL3FilterEntry->i4IssL3FilterPriority,
                             &(pIssAclL3FilterEntry->IssNextNode),
                             ISS_L3FILTER);
                    }

                    i4CommitFlag = IssExGetIssCommitSupportImmediate ();
                    i4CommitAction = IssGetTriggerCommit ();

                    if ((i4CommitFlag == ISS_TRUE) ||
                        (i4CommitAction == ISS_TRUE))
                    {

#ifdef NPAPI_WANTED
                        if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                        {
                            /* Call the hardware API to delete the filter */
                            i4RetVal =
                                IssHwUpdateL3Filter (pIssAclL3FilterEntry,
                                                     ISS_L3FILTER_DELETE);
                            if (i4RetVal != FNP_SUCCESS)
                            {
                                return SNMP_FAILURE;
                            }
                            pIssAclL3FilterEntry->u1IssL3HwStatus = ISS_NOTOK;
                        }
#endif
                    }
                }
            }
            /* Deleting the node from the list */
            ISS_SLL_DELETE (&(ISS_L3FILTER_LIST),
                            &(pIssAclL3FilterEntry->IssNextNode));
            /* Delete the Entry from the Software */
            if (ISS_FILTER_SHADOW_FREE_MEM_BLOCK
                (pIssAclL3FilterEntry->pIssFilterShadowInfo) != MEM_SUCCESS)
            {
                ISS_TRC (BUFFER_TRC | ALL_FAILURE_TRC,
                         "Filter Shadow Entry Free Failure\n");
                return SNMP_FAILURE;
            }
            if (ISS_L3FILTERENTRY_FREE_MEM_BLOCK (pIssAclL3FilterEntry)
                != MEM_SUCCESS)
            {
                ISS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "L3 Filter Entry Free Failure\n");
                return SNMP_FAILURE;
            }

            break;

        default:
            return SNMP_FAILURE;
    }
#ifdef QOSX_WANTED
    QosUpdateDiffServMFCNextFree (i4IssAclL3FilterNo,
                                  i4SetValIssAclL3FilterStatus);
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterSubAction
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterSubAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterSubAction (INT4
                               i4IssAclL3FilterNo,
                               INT4 i4SetValIssAclL3FilterSubAction)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3SubAction ==
            (UINT1) i4SetValIssAclL3FilterSubAction)
        {
            return SNMP_SUCCESS;
        }
        pIssAclL3FilterEntry->u1IssL3SubAction =
            (UINT1) i4SetValIssAclL3FilterSubAction;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterSubActionId
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterSubActionId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterSubActionId (INT4
                                 i4IssAclL3FilterNo,
                                 INT4 i4SetValIssAclL3FilterSubActionId)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u2IssL3SubActionId ==
            (UINT2) i4SetValIssAclL3FilterSubActionId)
        {
            return SNMP_SUCCESS;
        }

        pIssAclL3FilterEntry->u2IssL3SubActionId = (UINT2)
            i4SetValIssAclL3FilterSubActionId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterPriority
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterPriority (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                                 INT4 i4TestValIssAclL3FilterPriority)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL3FilterPriority <= ISS_MAX_FILTER_PRIORITY) &&
        (i4TestValIssAclL3FilterPriority >= ISS_DEFAULT_FILTER_PRIORITY))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterProtocol
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterProtocol
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterProtocol (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                                 INT4 i4TestValIssAclL3FilterProtocol)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL3FilterProtocol < 0)
        || (i4TestValIssAclL3FilterProtocol > 255))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterMessageType
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterMessageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterMessageType (UINT4 *pu4ErrorCode,
                                    INT4 i4IssAclL3FilterNo,
                                    INT4 i4TestValIssAclL3FilterMessageType)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL3FilterMessageType < 0)
        || (i4TestValIssAclL3FilterMessageType > 255))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterMessageCode
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterMessageCode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterMessageCode (UINT4 *pu4ErrorCode,
                                    INT4 i4IssAclL3FilterNo,
                                    INT4 i4TestValIssAclL3FilterMessageCode)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL3FilterMessageCode < 0)
        || (i4TestValIssAclL3FilterMessageCode > 255))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilteAddrType
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilteAddrType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilteAddrType (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                                INT4 i4TestValIssAclL3FilteAddrType)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (pIssL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValIssAclL3FilteAddrType == QOS_IPV4) ||
        (i4TestValIssAclL3FilteAddrType == QOS_IPV6))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterDstIpAddr
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterDstIpAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterDstIpAddr (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValIssAclL3FilterDstIpAddr)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pIssL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV4)
    {
        if (pTestValIssAclL3FilterDstIpAddr->i4_Length != QOS_IPV4_LEN)
        {
            return SNMP_FAILURE;
        }

        PTR_FETCH4 (pIssL3FilterEntry->u4IssL3FilterDstIpAddr,
                    pTestValIssAclL3FilterDstIpAddr->pu1_OctetList);

        /* Allow IP address which are Class A, B, C or D */
        if (!
            ((ISS_IS_ADDR_CLASS_A (pIssL3FilterEntry->u4IssL3FilterDstIpAddr))
             ||
             (ISS_IS_ADDR_CLASS_B (pIssL3FilterEntry->u4IssL3FilterDstIpAddr))
             ||
             (ISS_IS_ADDR_CLASS_C (pIssL3FilterEntry->u4IssL3FilterDstIpAddr))
             ||
             (ISS_IS_ADDR_CLASS_D (pIssL3FilterEntry->u4IssL3FilterDstIpAddr))))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV6)
    {
        if (pTestValIssAclL3FilterDstIpAddr->i4_Length != QOS_IPV6_LEN)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterSrcIpAddr
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterSrcIpAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterSrcIpAddr (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValIssAclL3FilterSrcIpAddr)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV4)
    {
        if (pTestValIssAclL3FilterSrcIpAddr->i4_Length != QOS_IPV4_LEN)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }

        PTR_FETCH4 (pIssL3FilterEntry->u4IssL3FilterSrcIpAddr,
                    pTestValIssAclL3FilterSrcIpAddr->pu1_OctetList);

        /* Allow IP address which are Class A, B, C or D */
        if (!
            ((ISS_IS_ADDR_CLASS_A (pIssL3FilterEntry->u4IssL3FilterSrcIpAddr))
             ||
             (ISS_IS_ADDR_CLASS_B (pIssL3FilterEntry->u4IssL3FilterSrcIpAddr))
             ||
             (ISS_IS_ADDR_CLASS_C (pIssL3FilterEntry->u4IssL3FilterSrcIpAddr))
             ||
             (ISS_IS_ADDR_CLASS_D (pIssL3FilterEntry->u4IssL3FilterSrcIpAddr))))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

    }
    if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV6)
    {
        if (pTestValIssAclL3FilterSrcIpAddr->i4_Length != QOS_IPV6_LEN)
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterDstIpAddrPrefixLength
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterDstIpAddrPrefixLength
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterDstIpAddrPrefixLength (UINT4 *pu4ErrorCode,
                                              INT4 i4IssAclL3FilterNo,
                                              UINT4
                                              u4TestValIssAclL3FilterDstIpAddrPrefixLength)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV4)
    {
        if (u4TestValIssAclL3FilterDstIpAddrPrefixLength >
            QOS_MF_PERFIX_IPV4_MAX)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    else if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV6)
    {
        if ((u4TestValIssAclL3FilterDstIpAddrPrefixLength >
             QOS_MF_PERFIX_IPV6_MAX))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterSrcIpAddrPrefixLength
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterSrcIpAddrPrefixLength
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterSrcIpAddrPrefixLength (UINT4 *pu4ErrorCode,
                                              INT4 i4IssAclL3FilterNo,
                                              UINT4
                                              u4TestValIssAclL3FilterSrcIpAddrPrefixLength)
{

    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV4)
    {
        if (u4TestValIssAclL3FilterSrcIpAddrPrefixLength >
            QOS_MF_PERFIX_IPV4_MAX)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    else if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV6)
    {
        if ((u4TestValIssAclL3FilterSrcIpAddrPrefixLength >
             QOS_MF_PERFIX_IPV6_MAX))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterMinDstProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterMinDstProtPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterMinDstProtPort (UINT4 *pu4ErrorCode,
                                       INT4 i4IssAclL3FilterNo,
                                       UINT4
                                       u4TestValIssAclL3FilterMinDstProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pIssAclL3FilterEntry->u4IssL3FilterMaxDstProtPort != 0) &&
        (pIssAclL3FilterEntry->u4IssL3FilterMaxDstProtPort <
         u4TestValIssAclL3FilterMinDstProtPort))
    {
        return SNMP_FAILURE;
    }

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4TestValIssAclL3FilterMinDstProtPort <= 65535)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterMaxDstProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterMaxDstProtPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterMaxDstProtPort (UINT4 *pu4ErrorCode,
                                       INT4 i4IssAclL3FilterNo,
                                       UINT4
                                       u4TestValIssAclL3FilterMaxDstProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry->u4IssL3FilterMinDstProtPort >
        u4TestValIssAclL3FilterMaxDstProtPort)
    {
        return SNMP_FAILURE;
    }

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4TestValIssAclL3FilterMaxDstProtPort <= 65535)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterMinSrcProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterMinSrcProtPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterMinSrcProtPort (UINT4 *pu4ErrorCode,
                                       INT4 i4IssAclL3FilterNo,
                                       UINT4
                                       u4TestValIssAclL3FilterMinSrcProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pIssAclL3FilterEntry->u4IssL3FilterMaxSrcProtPort != 0) &&
        (pIssAclL3FilterEntry->u4IssL3FilterMaxSrcProtPort <
         u4TestValIssAclL3FilterMinSrcProtPort))
    {
        return SNMP_FAILURE;
    }

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4TestValIssAclL3FilterMinSrcProtPort <= 65535)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterMaxSrcProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterMaxSrcProtPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterMaxSrcProtPort (UINT4 *pu4ErrorCode,
                                       INT4 i4IssAclL3FilterNo,
                                       UINT4
                                       u4TestValIssAclL3FilterMaxSrcProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry->u4IssL3FilterMinSrcProtPort >
        u4TestValIssAclL3FilterMaxSrcProtPort)
    {
        return SNMP_FAILURE;
    }

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4TestValIssAclL3FilterMaxSrcProtPort <= 65535)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterInPortList
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterInPortList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterInPortList (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValIssAclL3FilterInPortList)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pTestValIssAclL3FilterInPortList->i4_Length <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (IssIsPortListValid
        (pTestValIssAclL3FilterInPortList->pu1_OctetList,
         pTestValIssAclL3FilterInPortList->i4_Length) == ISS_FALSE)
    {
        if (pTestValIssAclL3FilterInPortList->i4_Length > ISS_PORT_LIST_SIZE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        }
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterOutPortList
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterOutPortList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterOutPortList (UINT4 *pu4ErrorCode,
                                    INT4 i4IssAclL3FilterNo,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValIssAclL3FilterOutPortList)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pTestValIssAclL3FilterOutPortList->i4_Length <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (IssIsPortListValid
        (pTestValIssAclL3FilterOutPortList->pu1_OctetList,
         pTestValIssAclL3FilterOutPortList->i4_Length) == ISS_FALSE)
    {
        if (pTestValIssAclL3FilterOutPortList->i4_Length > ISS_PORT_LIST_SIZE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        }
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterAckBit
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterAckBit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterAckBit (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                               INT4 i4TestValIssAclL3FilterAckBit)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL3FilterAckBit >= ISS_ACK_ESTABLISH)
        && (i4TestValIssAclL3FilterAckBit <= ISS_ACK_ANY))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterRstBit
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterRstBit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterRstBit (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                               INT4 i4TestValIssAclL3FilterRstBit)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL3FilterRstBit >= ISS_RST_SET)
        && (i4TestValIssAclL3FilterRstBit <= ISS_RST_ANY))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterTos
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterTos
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterTos (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                            INT4 i4TestValIssAclL3FilterTos)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry->i4IssL3FilterDscp != ISS_DSCP_INVALID)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL3FilterTos >= ISS_TOS_NONE)
        && (i4TestValIssAclL3FilterTos < ISS_TOS_INVALID))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterDscp
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterDscp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterDscp (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                             INT4 i4TestValIssAclL3FilterDscp)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry->IssL3FilterTos != ISS_TOS_INVALID)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (((i4TestValIssAclL3FilterDscp >= ISS_MIN_DSCP_VALUE) &&
         (i4TestValIssAclL3FilterDscp <= ISS_MAX_DSCP_VALUE)) ||
        (i4TestValIssAclL3FilterDscp == -1))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterDirection
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterDirection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterDirection (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                                  INT4 i4TestValIssAclL3FilterDirection)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL3FilterDirection == ISS_DIRECTION_IN)
        || (i4TestValIssAclL3FilterDirection == ISS_DIRECTION_OUT))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterAction
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterAction (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                               INT4 i4TestValIssAclL3FilterAction)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL3FilterAction == ISS_ALLOW)
        || (i4TestValIssAclL3FilterAction == ISS_DROP)
        || (i4TestValIssAclL3FilterAction == ISS_FORQOS)
        || (i4TestValIssAclL3FilterAction == ISS_REDIRECT_TO)
        || (i4TestValIssAclL3FilterAction == ISS_SWITCH_COPYTOCPU)
        || (i4TestValIssAclL3FilterAction == ISS_DROP_COPYTOCPU))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterFlowId
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterFlowId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterFlowId (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                               UINT4 u4TestValIssAclL3FilterFlowId)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType != QOS_IPV6)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pIssL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((u4TestValIssAclL3FilterFlowId == QOS_FLOW_ID_MIN) ||
        ((u4TestValIssAclL3FilterFlowId >= 1) &&
         (u4TestValIssAclL3FilterFlowId <= QOS_FLOW_ID_MAX)))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterStatus
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterStatus (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                               INT4 i4TestValIssAclL3FilterStatus)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
    INT4                i4RetStatus = ISS_ZERO_ENTRY;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL3FilterStatus < ISS_ACTIVE)
        || (i4TestValIssAclL3FilterStatus > ISS_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL &&
        (i4TestValIssAclL3FilterStatus != ISS_CREATE_AND_WAIT
         && i4TestValIssAclL3FilterStatus != ISS_CREATE_AND_GO))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry != NULL &&
        (i4TestValIssAclL3FilterStatus == ISS_CREATE_AND_WAIT
         || i4TestValIssAclL3FilterStatus == ISS_CREATE_AND_GO))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry != NULL &&
        pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_NOT_READY &&
        i4TestValIssAclL3FilterStatus == ISS_NOT_IN_SERVICE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry != NULL &&
        pIssAclL3FilterEntry->u1IssL3FilterStatus != ISS_ACTIVE &&
        i4TestValIssAclL3FilterStatus == ISS_NOT_IN_SERVICE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (nmhGetIssAclTrafficSeperationCtrl (&i4RetStatus) == SNMP_SUCCESS)
    {
        if (i4RetStatus == ACL_TRAFFICSEPRTN_CTRL_SYSTEM_DEFAULT)
        {
            if ((pIssAclL3FilterEntry != NULL) &&
                (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE &&
                 i4TestValIssAclL3FilterStatus == ISS_DESTROY) &&
                (pIssAclL3FilterEntry->u1IssL3FilterCreationMode ==
                 ISS_ACL_CREATION_INTERNAL))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
    }
#if defined (DIFFSRV_WANTED)
    if ((i4TestValIssAclL3FilterStatus == ISS_DESTROY)
        || (i4TestValIssAclL3FilterStatus == ISS_NOT_IN_SERVICE))
    {
        ISS_UNLOCK ();

        if (DsCheckMFClfrTable (i4IssAclL3FilterNo, DS_IP_FILTER) == DS_FAILURE)
        {
            ISS_LOCK ();
            CLI_SET_ERR (CLI_ACL_FILTER_NO_CHANGE);
            return SNMP_FAILURE;
        }

        ISS_LOCK ();
    }
#endif

#ifdef QOSX_WANTED
    /* Check the RefCount in IssAclL3Filter node -how many times it is used 
     * in the qosxtd Module */
    if ((i4TestValIssAclL3FilterStatus == ISS_DESTROY)
        || (i4TestValIssAclL3FilterStatus == ISS_NOT_IN_SERVICE))
    {
        if (pIssAclL3FilterEntry->u4RefCount > 0)
        {
            CLI_SET_ERR (CLI_ACL_FILTER_NO_CHANGE);
            return (SNMP_FAILURE);
        }
    }
#endif

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterSubAction
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterSubAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterSubAction (UINT4
                                  *pu4ErrorCode,
                                  INT4
                                  i4IssAclL3FilterNo,
                                  INT4 i4TestValIssAclL3FilterSubAction)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL3FilterSubAction != ISS_NONE) &&
        (i4TestValIssAclL3FilterSubAction != ISS_MODIFY_VLAN) &&
        (i4TestValIssAclL3FilterSubAction != ISS_NESTED_VLAN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterSubActionId
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterSubActionId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterSubActionId (UINT4
                                    *pu4ErrorCode,
                                    INT4
                                    i4IssAclL3FilterNo,
                                    INT4 i4TestValIssAclL3FilterSubActionId)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL3FilterSubActionId < 0) ||
        (i4TestValIssAclL3FilterSubActionId > 4094))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IssAclL3FilterTable
 Input       :  The Indices
                IssAclL3FilterNo
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IssAclL3FilterTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IssAclUserDefinedFilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssAclUserDefinedFilterTable
 Input       :  The Indices
                IssAclUserDefinedFilterId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssAclUserDefinedFilterTable (UINT4
                                                      u4IssAclUserDefinedFilterId)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        return SNMP_FAILURE;
    }

    pIssAclUdbFilterTableEntry =
        IssExtGetUdbFilterTableEntry (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssAclUserDefinedFilterTable
 Input       :  The Indices
                IssAclUserDefinedFilterId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssAclUserDefinedFilterTable (UINT4
                                              *pu4IssAclUserDefinedFilterId)
{
    if (IssExtSnmpLowGetFirstValidUDBFilterTableIndex
        ((INT4 *) pu4IssAclUserDefinedFilterId) == ISS_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIssAclUserDefinedFilterTable
 Input       :  The Indices
                IssAclUserDefinedFilterId
                nextIssAclUserDefinedFilterId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIssAclUserDefinedFilterTable (UINT4
                                             u4IssAclUserDefinedFilterId,
                                             UINT4
                                             *pu4NextIssAclUserDefinedFilterId)
{
    if (u4IssAclUserDefinedFilterId == 0)
    {
        return SNMP_FAILURE;
    }

    if ((IssSnmpLowGetNextValidUdbFilterTableIndex
         (u4IssAclUserDefinedFilterId,
          pu4NextIssAclUserDefinedFilterId)) != ISS_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetIssAclUserDefinedFilterPktType
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                retValIssAclUserDefinedFilterPktType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclUserDefinedFilterPktType (UINT4
                                      u4IssAclUserDefinedFilterId,
                                      INT4
                                      *pi4RetValIssAclUserDefinedFilterPktType)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry != NULL)
    {
        *pi4RetValIssAclUserDefinedFilterPktType =
            pIssAclUdbFilterTableEntry->pAccessFilterEntry->
            u1AccessFilterPktType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssAclUserDefinedFilterOffSetBase
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                retValIssAclUserDefinedFilterOffSetBase
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclUserDefinedFilterOffSetBase (UINT4 u4IssAclUserDefinedFilterId,
                                         INT4
                                         *pi4RetValIssAclUserDefinedFilterOffSetBase)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry != NULL)
    {
        *pi4RetValIssAclUserDefinedFilterOffSetBase =
            pIssAclUdbFilterTableEntry->pAccessFilterEntry->
            u2AccessFilterOffset;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclUserDefinedFilterOffSetValue
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                retValIssAclUserDefinedFilterOffSetValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclUserDefinedFilterOffSetValue (UINT4 u4IssAclUserDefinedFilterId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pRetValIssAclUserDefinedFilterOffSetValue)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry != NULL)
    {

        ISS_MEMCPY (pRetValIssAclUserDefinedFilterOffSetValue->pu1_OctetList,
                    pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                    au1AccessFilterOffsetValue,
                    sizeof (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                            au1AccessFilterOffsetValue));

        pRetValIssAclUserDefinedFilterOffSetValue->i4_Length =
            ISS_UDB_MAX_OFFSET;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclUserDefinedFilterOffSetMask
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                retValIssAclUserDefinedFilterOffSetMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclUserDefinedFilterOffSetMask (UINT4 u4IssAclUserDefinedFilterId,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pRetValIssAclUserDefinedFilterOffSetMask)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry != NULL)
    {

        ISS_MEMCPY (pRetValIssAclUserDefinedFilterOffSetMask->pu1_OctetList,
                    (pIssAclUdbFilterTableEntry->
                     pAccessFilterEntry->au1AccessFilterOffsetMask),
                    sizeof (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                            au1AccessFilterOffsetMask));

        pRetValIssAclUserDefinedFilterOffSetMask->i4_Length =
            ISS_UDB_MAX_OFFSET;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclUserDefinedFilterPriority
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                retValIssAclUserDefinedFilterPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclUserDefinedFilterPriority (UINT4
                                       u4IssAclUserDefinedFilterId,
                                       INT4
                                       *pi4RetValIssAclUserDefinedFilterPriority)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry != NULL)
    {
        *pi4RetValIssAclUserDefinedFilterPriority =
            pIssAclUdbFilterTableEntry->
            pAccessFilterEntry->u1AccessFilterPriority;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssAclUserDefinedFilterAction
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                retValIssAclUserDefinedFilterAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclUserDefinedFilterAction (UINT4
                                     u4IssAclUserDefinedFilterId,
                                     INT4
                                     *pi4RetValIssAclUserDefinedFilterAction)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->pAccessFilterEntry != NULL))
    {
        *pi4RetValIssAclUserDefinedFilterAction =
            pIssAclUdbFilterTableEntry->pAccessFilterEntry->
            IssAccessFilterAction;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclUserDefinedFilterInPortList
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                retValIssAclUserDefinedFilterInPortList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclUserDefinedFilterInPortList (UINT4
                                         u4IssAclUserDefinedFilterId,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pRetValIssAclUserDefinedFilterInPortList)
{

    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry != NULL)
    {

        if (pIssAclUdbFilterTableEntry->pAccessFilterEntry != NULL)
        {
            ISS_MEMSET (pRetValIssAclUserDefinedFilterInPortList->pu1_OctetList,
                        0, ISS_PORT_LIST_SIZE);

            MEMCPY (pRetValIssAclUserDefinedFilterInPortList->pu1_OctetList,
                    pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                    IssUdbFilterInPortList, ISS_PORT_LIST_SIZE);

            pRetValIssAclUserDefinedFilterInPortList->i4_Length =
                ISS_PORT_LIST_SIZE;

            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssAclUserDefinedFilterIdOneType
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                retValIssAclUserDefinedFilterIdOneType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclUserDefinedFilterIdOneType (UINT4
                                        u4IssAclUserDefinedFilterId,
                                        INT4
                                        *pi4RetValIssAclUserDefinedFilterIdOneType)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->pAccessFilterEntry != NULL))
    {
        *pi4RetValIssAclUserDefinedFilterIdOneType =
            pIssAclUdbFilterTableEntry->u1AclOneType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssAclUserDefinedFilterIdOne
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                retValIssAclUserDefinedFilterIdOne
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclUserDefinedFilterIdOne (UINT4
                                    u4IssAclUserDefinedFilterId,
                                    UINT4
                                    *pu4RetValIssAclUserDefinedFilterIdOne)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->pAccessFilterEntry != NULL))
    {
        *pu4RetValIssAclUserDefinedFilterIdOne =
            pIssAclUdbFilterTableEntry->u4BaseAclOne;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclUserDefinedFilterIdTwoType
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                retValIssAclUserDefinedFilterIdTwoType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclUserDefinedFilterIdTwoType (UINT4
                                        u4IssAclUserDefinedFilterId,
                                        INT4
                                        *pi4RetValIssAclUserDefinedFilterIdTwoType)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->pAccessFilterEntry != NULL))
    {
        *pi4RetValIssAclUserDefinedFilterIdTwoType =
            pIssAclUdbFilterTableEntry->u1AclTwoType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclUserDefinedFilterIdTwo
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                retValIssAclUserDefinedFilterIdTwo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclUserDefinedFilterIdTwo (UINT4
                                    u4IssAclUserDefinedFilterId,
                                    UINT4
                                    *pu4RetValIssAclUserDefinedFilterIdTwo)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->pAccessFilterEntry != NULL))
    {
        *pu4RetValIssAclUserDefinedFilterIdTwo =
            pIssAclUdbFilterTableEntry->u4BaseAclTwo;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssAclUserDefinedFilterSubAction
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                retValIssAclUserDefinedFilterSubAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclUserDefinedFilterSubAction (UINT4
                                        u4IssAclUserDefinedFilterId,
                                        INT4
                                        *pi4RetValIssAclUserDefinedFilterSubAction)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->pAccessFilterEntry != NULL))
    {
        *pi4RetValIssAclUserDefinedFilterSubAction =
            (INT4) pIssAclUdbFilterTableEntry->pAccessFilterEntry->
            u1IssUdbSubAction;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclUserDefinedFilterSubActionId
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                retValIssAclUserDefinedFilterSubActionId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclUserDefinedFilterSubActionId (UINT4
                                          u4IssAclUserDefinedFilterId,
                                          INT4
                                          *pi4RetValIssAclUserDefinedFilterSubActionId)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->pAccessFilterEntry != NULL))
    {
        *pi4RetValIssAclUserDefinedFilterSubActionId =
            (INT4) pIssAclUdbFilterTableEntry->pAccessFilterEntry->
            u2IssUdbSubActionId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclUserDefinedFilterRedirectId
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object
                retValIssAclUserDefinedFilterRedirectId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclUserDefinedFilterRedirectId (UINT4 u4IssAclUserDefinedFilterId,
                                         INT4
                                         *pi4RetValIssAclUserDefinedFilterRedirectId)
{
    UINT4               u4Index = 0;

    for (u4Index = 0; u4Index < ISS_MAX_REDIRECT_GRP_ID; u4Index++)
    {
        if ((gpIssRedirectIntfInfo[u4Index].u4AclId ==
             (UINT4) u4IssAclUserDefinedFilterId)
            && (gpIssRedirectIntfInfo[u4Index].u1AclIdType == ISS_UDB_REDIRECT))
        {
            *pi4RetValIssAclUserDefinedFilterRedirectId = u4Index + 1;
            break;
        }
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIssAclUserDefinedFilterStatus
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                retValIssAclUserDefinedFilterStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclUserDefinedFilterStatus (UINT4
                                     u4IssAclUserDefinedFilterId,
                                     INT4
                                     *pi4RetValIssAclUserDefinedFilterStatus)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->pAccessFilterEntry != NULL))
    {
        *pi4RetValIssAclUserDefinedFilterStatus =
            pIssAclUdbFilterTableEntry->u1RowStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIssAclUserDefinedFilterPktType
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                setValIssAclUserDefinedFilterPktType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclUserDefinedFilterPktType (UINT4
                                      u4IssAclUserDefinedFilterId,
                                      INT4
                                      i4SetValIssAclUserDefinedFilterPktType)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->pAccessFilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->pAccessFilterEntry->u1AccessFilterPktType
        == i4SetValIssAclUserDefinedFilterPktType)
    {
        return SNMP_SUCCESS;
    }

    pIssAclUdbFilterTableEntry->pAccessFilterEntry->u1AccessFilterPktType
        = (UINT1) i4SetValIssAclUserDefinedFilterPktType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssAclUserDefinedFilterOffSetBase
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                setValIssAclUserDefinedFilterOffSetBase
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclUserDefinedFilterOffSetBase (UINT4
                                         u4IssAclUserDefinedFilterId,
                                         INT4
                                         i4SetValIssAclUserDefinedFilterOffSetBase)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->pAccessFilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->pAccessFilterEntry->u2AccessFilterOffset
        == i4SetValIssAclUserDefinedFilterOffSetBase)
    {
        return SNMP_SUCCESS;
    }

    pIssAclUdbFilterTableEntry->pAccessFilterEntry->u2AccessFilterOffset
        = (UINT2) i4SetValIssAclUserDefinedFilterOffSetBase;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssAclUserDefinedFilterOffSetValue
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                setValIssAclUserDefinedFilterOffSetValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclUserDefinedFilterOffSetValue (UINT4
                                          u4IssAclUserDefinedFilterId,
                                          tSNMP_OCTET_STRING_TYPE
                                          *
                                          pSetValIssAclUserDefinedFilterOffSetValue)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->pAccessFilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    ISS_MEMCPY ((pIssAclUdbFilterTableEntry->
                 pAccessFilterEntry->au1AccessFilterOffsetValue),
                pSetValIssAclUserDefinedFilterOffSetValue->pu1_OctetList,
                pSetValIssAclUserDefinedFilterOffSetValue->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssAclUserDefinedFilterOffSetMask
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                setValIssAclUserDefinedFilterOffSetMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclUserDefinedFilterOffSetMask (UINT4
                                         u4IssAclUserDefinedFilterId,
                                         tSNMP_OCTET_STRING_TYPE
                                         *
                                         pSetValIssAclUserDefinedFilterOffSetMask)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->pAccessFilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    ISS_MEMCPY ((pIssAclUdbFilterTableEntry->
                 pAccessFilterEntry->au1AccessFilterOffsetMask),
                pSetValIssAclUserDefinedFilterOffSetMask->pu1_OctetList,
                pSetValIssAclUserDefinedFilterOffSetMask->i4_Length);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssAclUserDefinedFilterPriority
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                setValIssAclUserDefinedFilterPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclUserDefinedFilterPriority (UINT4
                                       u4IssAclUserDefinedFilterId,
                                       INT4
                                       i4SetValIssAclUserDefinedFilterPriority)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->pAccessFilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->pAccessFilterEntry->u1AccessFilterPriority
        == i4SetValIssAclUserDefinedFilterPriority)
    {
        return SNMP_SUCCESS;
    }

    pIssAclUdbFilterTableEntry->pAccessFilterEntry->u1AccessFilterPriority
        = (UINT1) i4SetValIssAclUserDefinedFilterPriority;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssAclUserDefinedFilterAction
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                setValIssAclUserDefinedFilterAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclUserDefinedFilterAction (UINT4
                                     u4IssAclUserDefinedFilterId,
                                     INT4 i4SetValIssAclUserDefinedFilterAction)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->pAccessFilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->pAccessFilterEntry->IssAccessFilterAction
        == (tIssFilterAction) i4SetValIssAclUserDefinedFilterAction)
    {
        return SNMP_SUCCESS;
    }

    pIssAclUdbFilterTableEntry->pAccessFilterEntry->IssAccessFilterAction
        = i4SetValIssAclUserDefinedFilterAction;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssAclUserDefinedFilterInPortList
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                setValIssAclUserDefinedFilterInPortList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclUserDefinedFilterInPortList (UINT4
                                         u4IssAclUserDefinedFilterId,
                                         tSNMP_OCTET_STRING_TYPE
                                         *
                                         pSetValIssAclUserDefinedFilterInPortList)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry != NULL)
    {
        if (pIssAclUdbFilterTableEntry->pAccessFilterEntry != NULL)
        {
            ISS_MEMSET (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                        IssUdbFilterInPortList, 0, ISS_PORT_LIST_SIZE);
            ISS_MEMCPY (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                        IssUdbFilterInPortList,
                        pSetValIssAclUserDefinedFilterInPortList->pu1_OctetList,
                        pSetValIssAclUserDefinedFilterInPortList->i4_Length);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIssAclUserDefinedFilterIdOneType
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                setValIssAclUserDefinedFilterIdOneType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclUserDefinedFilterIdOneType (UINT4
                                        u4IssAclUserDefinedFilterId,
                                        INT4
                                        i4SetValIssAclUserDefinedFilterIdOneType)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->u1AclOneType
        == i4SetValIssAclUserDefinedFilterIdOneType)
    {
        return SNMP_SUCCESS;
    }

    pIssAclUdbFilterTableEntry->u1AclOneType
        = (UINT1) i4SetValIssAclUserDefinedFilterIdOneType;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssAclUserDefinedFilterIdOne
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                setValIssAclUserDefinedFilterIdOne
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclUserDefinedFilterIdOne (UINT4
                                    u4IssAclUserDefinedFilterId,
                                    UINT4 u4SetValIssAclUserDefinedFilterIdOne)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->u4BaseAclOne
        == u4SetValIssAclUserDefinedFilterIdOne)
    {
        return SNMP_SUCCESS;
    }

    pIssAclUdbFilterTableEntry->u4BaseAclOne
        = u4SetValIssAclUserDefinedFilterIdOne;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssAclUserDefinedFilterIdTwoType
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                setValIssAclUserDefinedFilterIdTwoType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclUserDefinedFilterIdTwoType (UINT4
                                        u4IssAclUserDefinedFilterId,
                                        INT4
                                        i4SetValIssAclUserDefinedFilterIdTwoType)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->u1AclTwoType
        == i4SetValIssAclUserDefinedFilterIdTwoType)
    {
        return SNMP_SUCCESS;
    }

    pIssAclUdbFilterTableEntry->u1AclTwoType
        = (UINT1) i4SetValIssAclUserDefinedFilterIdTwoType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssAclUserDefinedFilterIdTwo
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                setValIssAclUserDefinedFilterIdTwo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclUserDefinedFilterIdTwo (UINT4
                                    u4IssAclUserDefinedFilterId,
                                    UINT4 u4SetValIssAclUserDefinedFilterIdTwo)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->u4BaseAclTwo
        == u4SetValIssAclUserDefinedFilterIdTwo)
    {
        return SNMP_SUCCESS;
    }

    pIssAclUdbFilterTableEntry->u4BaseAclTwo
        = u4SetValIssAclUserDefinedFilterIdTwo;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssAclUserDefinedFilterSubAction
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                setValIssAclUserDefinedFilterSubAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclUserDefinedFilterSubAction (UINT4
                                        u4IssAclUserDefinedFilterId,
                                        INT4
                                        i4SetValIssAclUserDefinedFilterSubAction)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pIssAclUdbFilterTableEntry->pAccessFilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->pAccessFilterEntry->u1IssUdbSubAction
        == i4SetValIssAclUserDefinedFilterSubAction)
    {
        return SNMP_SUCCESS;
    }

    pIssAclUdbFilterTableEntry->pAccessFilterEntry->u1IssUdbSubAction
        = (UINT1) i4SetValIssAclUserDefinedFilterSubAction;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssAclUserDefinedFilterSubActionId
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                setValIssAclUserDefinedFilterSubActionId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclUserDefinedFilterSubActionId (UINT4
                                          u4IssAclUserDefinedFilterId,
                                          INT4
                                          i4SetValIssAclUserDefinedFilterSubActionId)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pIssAclUdbFilterTableEntry->pAccessFilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->pAccessFilterEntry->u2IssUdbSubActionId
        == i4SetValIssAclUserDefinedFilterSubActionId)
    {
        return SNMP_SUCCESS;
    }

    pIssAclUdbFilterTableEntry->pAccessFilterEntry->u2IssUdbSubActionId
        = (UINT2) i4SetValIssAclUserDefinedFilterSubActionId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssAclUserDefinedFilterStatus
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                setValIssAclUserDefinedFilterStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclUserDefinedFilterStatus (UINT4
                                     u4IssAclUserDefinedFilterId,
                                     INT4 i4SetValIssAclUserDefinedFilterStatus)
{
#ifdef NPAPI_WANTED
    INT4                i4RetVal = 0x0;
    tIssL2FilterEntry  *pIssAclL2FilterEntryOne = NULL;
    tIssL3FilterEntry  *pIssAclL3FilterEntryOne = NULL;
    tIssL2FilterEntry  *pIssAclL2FilterEntryTwo = NULL;
    tIssL3FilterEntry  *pIssAclL3FilterEntryTwo = NULL;
    tIssL2FilterEntry   IssL2FilterEntry;
    tIssL3FilterEntry   IssL3FilterEntry;
    INT1                u1Action;
#endif
    INT4                i4CommitFlag = 0;
    INT4                i4CommitAction = 0;
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry != NULL)
    {
        if (pIssAclUdbFilterTableEntry->u1RowStatus ==
            (UINT1) i4SetValIssAclUserDefinedFilterStatus)
        {
            if (pIssAclUdbFilterTableEntry->u1PriorityFlag == ISS_FALSE)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    else
    {
        /* This Filter Entry is not there in the Database */
        if ((i4SetValIssAclUserDefinedFilterStatus != ISS_CREATE_AND_WAIT) &&
            (i4SetValIssAclUserDefinedFilterStatus != ISS_CREATE_AND_GO))
        {
            return SNMP_FAILURE;
        }
    }

    switch (i4SetValIssAclUserDefinedFilterStatus)
    {

        case ISS_CREATE_AND_WAIT:
        case ISS_CREATE_AND_GO:

            if (ISS_UDB_FILTER_TABLE_ALLOC_MEM_BLOCK
                (pIssAclUdbFilterTableEntry) == NULL)
            {
                ISS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "No Free UDB Filter Table Entry"
                         "or h/w init failed at startup\n");
                return SNMP_FAILURE;
            }
            ISS_MEMSET (pIssAclUdbFilterTableEntry, 0,
                        sizeof (tIssUserDefinedFilterTable));
            if (ISS_UDB_FILTER_ENTRY_ALLOC_MEM_BLOCK
                (pIssAclUdbFilterTableEntry->pAccessFilterEntry) == NULL)
            {
                ISS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "No Free UDB Filter Entry"
                         "or h/w init failed at startup\n");
                ISS_UDB_FILTERTABLE_FREE_MEM_BLOCK (pIssAclUdbFilterTableEntry);
                return SNMP_FAILURE;
            }

            ISS_MEMSET (pIssAclUdbFilterTableEntry->pAccessFilterEntry, 0,
                        sizeof (tIssUDBFilterEntry));

            ISS_MEMSET (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                        au1AccessFilterOffsetValue, 0, ISS_UDB_MAX_OFFSET);

            ISS_MEMSET (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                        au1AccessFilterOffsetMask, 0, ISS_UDB_MAX_OFFSET);

            /* Setting the RowStatus to NOT_READY if the
             * filter if i4SetValIssAclL2FilterStatus is ISS_CREATE_AND_WAIT*/
            pIssAclUdbFilterTableEntry->pAccessFilterEntry->u4UDBFilterId
                = u4IssAclUserDefinedFilterId;
            pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                u1AccessFilterPktType = ISS_UDB_PKT_TYPE_USER_DEF;
            pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                u1AccessFilterPriority = ISS_DEFAULT_FILTER_PRIORITY;
            pIssAclUdbFilterTableEntry->u1AclOneType = 0;
            pIssAclUdbFilterTableEntry->u1AclTwoType = 0;
            pIssAclUdbFilterTableEntry->u1PriorityFlag = ISS_FALSE;
            pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                RedirectIfGrp.u1PriorityFlag = ISS_FALSE;
            /* Adding the Entry to the Filter List */
            ISS_SLL_ADD (&(ISS_UDB_FILTER_TABLE_LIST),
                         &(pIssAclUdbFilterTableEntry->IssNextNode));

            pIssAclUdbFilterTableEntry->u1RowStatus = ISS_NOT_IN_SERVICE;
            break;

        case ISS_NOT_IN_SERVICE:

            if (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                IssAccessFilterAction == ISS_REDIRECT_TO)
            {
                pIssAclUdbFilterTableEntry->u1RowStatus = ISS_NOT_IN_SERVICE;
                return (SNMP_SUCCESS);
            }

            IssExDeleteAclPriorityFilterTable (pIssAclUdbFilterTableEntry->
                                               pAccessFilterEntry->
                                               u1AccessFilterPriority,
                                               &(pIssAclUdbFilterTableEntry->
                                                 IssNextNode),
                                               ISS_UDB_REDIRECT);

            i4CommitFlag = IssExGetIssCommitSupportImmediate ();
            i4CommitAction = IssGetTriggerCommit ();

            if ((i4CommitFlag == ISS_TRUE) || (i4CommitAction == ISS_TRUE))
            {
#ifdef NPAPI_WANTED
                if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                {
                    if ((pIssAclUdbFilterTableEntry->
                         pAccessFilterEntry->IssAccessFilterAction
                         == ISS_ALLOW) ||
                        (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                         IssAccessFilterAction == ISS_DROP))
                    {
                        IssHwUpdateUserDefinedFilter
                            (pIssAclUdbFilterTableEntry->pAccessFilterEntry,
                             NULL, NULL, ISS_USERDEFINED_ACCESSFILTER_DELETE);
                    }
                    else if (pIssAclUdbFilterTableEntry->
                             pAccessFilterEntry->IssAccessFilterAction ==
                             ISS_NOT)
                    {

                        if (pIssAclUdbFilterTableEntry->u1AclOneType ==
                            ISS_L2FILTER)
                        {
                            IssHwUpdateUserDefinedFilter
                                (pIssAclUdbFilterTableEntry->pAccessFilterEntry,
                                 NULL, NULL, ISS_USERDEFINED_L2L2FILTER_DELETE);
                        }
                        else if (pIssAclUdbFilterTableEntry->u1AclOneType ==
                                 ISS_L3_FILTER)
                        {
                            IssHwUpdateUserDefinedFilter
                                (pIssAclUdbFilterTableEntry->pAccessFilterEntry,
                                 NULL, NULL, ISS_USERDEFINED_L3L3FILTER_DELETE);
                        }
                    }
                    else if ((pIssAclUdbFilterTableEntry->
                              pAccessFilterEntry->IssAccessFilterAction ==
                              ISS_AND) || (pIssAclUdbFilterTableEntry->
                                           pAccessFilterEntry->
                                           IssAccessFilterAction == ISS_OR))
                    {

                        if ((pIssAclUdbFilterTableEntry->u1AclOneType ==
                             ISS_L2FILTER) &&
                            (pIssAclUdbFilterTableEntry->u1AclTwoType ==
                             ISS_L2FILTER))
                        {
                            IssHwUpdateUserDefinedFilter
                                (pIssAclUdbFilterTableEntry->pAccessFilterEntry,
                                 NULL, NULL, ISS_USERDEFINED_L2L2FILTER_DELETE);
                        }
                        else if ((pIssAclUdbFilterTableEntry->u1AclOneType ==
                                  ISS_L3FILTER) &&
                                 (pIssAclUdbFilterTableEntry->u1AclTwoType ==
                                  ISS_L3FILTER))
                        {
                            IssHwUpdateUserDefinedFilter
                                (pIssAclUdbFilterTableEntry->pAccessFilterEntry,
                                 NULL, NULL, ISS_USERDEFINED_L3L3FILTER_DELETE);
                        }
                        else if (pIssAclUdbFilterTableEntry->u1AclOneType !=
                                 pIssAclUdbFilterTableEntry->u1AclTwoType)

                        {
                            IssHwUpdateUserDefinedFilter
                                (pIssAclUdbFilterTableEntry->pAccessFilterEntry,
                                 NULL, NULL, ISS_USERDEFINED_L2L3FILTER_DELETE);

                        }

                    }
                }
#endif
            }
            pIssAclUdbFilterTableEntry->u1RowStatus = ISS_NOT_IN_SERVICE;

            break;

        case ISS_ACTIVE:

            if (pIssAclUdbFilterTableEntry->u1PriorityFlag == ISS_FALSE)
            {
                IssExAddAclToPriorityTable (pIssAclUdbFilterTableEntry->
                                            pAccessFilterEntry->
                                            u1AccessFilterPriority,
                                            &(pIssAclUdbFilterTableEntry->
                                              IssNextNode), ISS_UDB_REDIRECT);
            }

            i4CommitFlag = IssExGetIssCommitSupportImmediate ();
            i4CommitAction = IssGetTriggerCommit ();

            if ((i4CommitFlag == ISS_TRUE) || (i4CommitAction == ISS_TRUE))
            {

#ifdef NPAPI_WANTED
                if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                {
                    if ((pIssAclUdbFilterTableEntry->
                         pAccessFilterEntry->IssAccessFilterAction
                         == ISS_ALLOW) ||
                        (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                         IssAccessFilterAction == ISS_DROP))
                    {
                        i4RetVal =
                            IssHwUpdateUserDefinedFilter
                            (pIssAclUdbFilterTableEntry->pAccessFilterEntry,
                             NULL, NULL, ISS_USERDEFINED_ACCESSFILTER_ADD);

                        if (i4RetVal != FNP_SUCCESS)
                        {
                            return SNMP_FAILURE;
                        }
                    }
                    else if (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                             IssAccessFilterAction != ISS_REDIRECT_TO)
                    {
                        if (pIssAclUdbFilterTableEntry->u1AclOneType ==
                            ISS_L2FILTER)
                        {
                            pIssAclL2FilterEntryOne =
                                IssExtGetL2FilterEntry
                                (pIssAclUdbFilterTableEntry->u4BaseAclOne);

                            if (pIssAclL2FilterEntryOne == NULL)
                            {
                                return SNMP_FAILURE;
                            }

                        }
                        else if (pIssAclUdbFilterTableEntry->u1AclOneType ==
                                 ISS_L3FILTER)
                        {
                            pIssAclL3FilterEntryOne =
                                IssExtGetL3FilterEntry
                                (pIssAclUdbFilterTableEntry->u4BaseAclOne);

                            if (pIssAclL3FilterEntryOne == NULL)
                            {
                                return ISS_FAILURE;
                            }

                        }

                        if (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                            IssAccessFilterAction != ISS_NOT)
                        {
                            if (pIssAclUdbFilterTableEntry->u1AclTwoType
                                == ISS_L2FILTER)
                            {
                                pIssAclL2FilterEntryTwo =
                                    IssExtGetL2FilterEntry
                                    (pIssAclUdbFilterTableEntry->u4BaseAclTwo);

                                if (pIssAclL2FilterEntryTwo == NULL)
                                {
                                    return SNMP_FAILURE;
                                }

                            }
                            else if (pIssAclUdbFilterTableEntry->u1AclTwoType
                                     == ISS_L3FILTER)
                            {
                                pIssAclL3FilterEntryTwo =
                                    IssExtGetL3FilterEntry
                                    (pIssAclUdbFilterTableEntry->u4BaseAclTwo);

                                if (pIssAclL3FilterEntryTwo == NULL)
                                {
                                    return SNMP_FAILURE;
                                }
                            }
                        }

                        if (((pIssAclUdbFilterTableEntry->u1AclOneType
                              == pIssAclUdbFilterTableEntry->u1AclTwoType) &&
                             (pIssAclUdbFilterTableEntry->u1AclOneType ==
                              ISS_L2FILTER)) ||
                            ((pIssAclUdbFilterTableEntry->u1AclOneType ==
                              ISS_L2FILTER)
                             && (pIssAclUdbFilterTableEntry->
                                 pAccessFilterEntry->IssAccessFilterAction ==
                                 ISS_NOT)))
                        {

                            i4RetVal = IssProcessAndOrNotFilterParamForL2
                                (pIssAclL2FilterEntryOne,
                                 pIssAclL2FilterEntryTwo,
                                 pIssAclUdbFilterTableEntry->
                                 pAccessFilterEntry->IssAccessFilterAction,
                                 (tIssL2FilterEntry *)
                                 & IssL2FilterEntry, (UINT1 *) &u1Action);

                            if (i4RetVal == ISS_FAILURE)
                            {
                                return SNMP_FAILURE;
                            }
                        }
                        else if (((pIssAclUdbFilterTableEntry->u1AclOneType
                                   == pIssAclUdbFilterTableEntry->u1AclTwoType)
                                  && (pIssAclUdbFilterTableEntry->
                                      u1AclOneType == ISS_L3FILTER))
                                 ||
                                 ((pIssAclUdbFilterTableEntry->u1AclOneType ==
                                   ISS_L3FILTER)
                                  && (pIssAclUdbFilterTableEntry->
                                      pAccessFilterEntry->
                                      IssAccessFilterAction == ISS_NOT)))
                        {

                            i4RetVal = IssProcessAndOrNotFilterParamForL3
                                (pIssAclL3FilterEntryOne,
                                 pIssAclL3FilterEntryTwo,
                                 pIssAclUdbFilterTableEntry->
                                 pAccessFilterEntry->IssAccessFilterAction,
                                 (tIssL3FilterEntry *)
                                 & IssL3FilterEntry, (UINT1 *) &u1Action);

                            if (i4RetVal == ISS_FAILURE)
                            {
                                return SNMP_FAILURE;
                            }
                        }
                        else if ((pIssAclUdbFilterTableEntry->u1AclOneType !=
                                  pIssAclUdbFilterTableEntry->u1AclTwoType) &&
                                 (pIssAclUdbFilterTableEntry->
                                  pAccessFilterEntry->IssAccessFilterAction ==
                                  ISS_AND))
                        {

                            if (pIssAclL2FilterEntryOne->IssL2FilterAction !=
                                pIssAclL3FilterEntryTwo->IssL3FilterAction)
                            {
                                return SNMP_FAILURE;
                            }

                            if ((pIssAclL2FilterEntryOne->IssL2FilterAction ==
                                 ISS_REDIRECT_TO) ||
                                (pIssAclL3FilterEntryTwo->IssL3FilterAction ==
                                 ISS_REDIRECT_TO))
                            {
                                return SNMP_FAILURE;
                            }

                            u1Action = ISS_USERDEFINED_L2L3FILTER_APPLY_AND_OP;
                        }
                        else if ((pIssAclUdbFilterTableEntry->u1AclOneType !=
                                  pIssAclUdbFilterTableEntry->u1AclTwoType) &&
                                 (pIssAclUdbFilterTableEntry->
                                  pAccessFilterEntry->IssAccessFilterAction ==
                                  ISS_OR))
                        {
                            if (pIssAclL2FilterEntryOne->IssL2FilterAction ==
                                pIssAclL3FilterEntryTwo->IssL3FilterAction)
                            {
                                return SNMP_FAILURE;
                            }

                            u1Action = ISS_USERDEFINED_L2L3FILTER_APPLY_OR_OP;
                        }

                        switch (u1Action)
                        {
                                /*While Applying AND/ OR / NOT operations L2 filter
                                 * the exisiting filter in Hardware is deleted and 
                                 * a new rule is added*/
                            case ISS_USERDEFINED_L2FILTER_APPLY_AND_OP:
#ifdef NPAPI_WANTED
                                if (ISS_IS_NP_PROGRAMMING_ALLOWED () ==
                                    ISS_TRUE)
                                {
                                    i4RetVal =
                                        IssHwUpdateL2Filter
                                        (pIssAclL2FilterEntryOne,
                                         ISS_L2FILTER_DELETE);
                                    if (i4RetVal != FNP_SUCCESS)
                                    {
                                        return SNMP_FAILURE;
                                    }
                                    i4RetVal =
                                        IssHwUpdateL2Filter
                                        (pIssAclL2FilterEntryTwo,
                                         ISS_L2FILTER_DELETE);
                                    if (i4RetVal != FNP_SUCCESS)
                                    {
                                        return SNMP_FAILURE;
                                    }
                                    pIssAclL2FilterEntryOne->u1IssL2HwStatus =
                                        ISS_NOTOK;
                                    pIssAclL2FilterEntryTwo->u1IssL2HwStatus =
                                        ISS_NOTOK;
                                    IssL2FilterEntry.i4IssL2FilterNo =
                                        pIssAclUdbFilterTableEntry->
                                        pAccessFilterEntry->u4UDBFilterId;

                                    ISS_MEMCPY (IssL2FilterEntry.
                                                IssL2FilterInPortList,
                                                pIssAclUdbFilterTableEntry->
                                                pAccessFilterEntry->
                                                IssUdbFilterInPortList,
                                                sizeof
                                                (pIssAclUdbFilterTableEntry->
                                                 pAccessFilterEntry->
                                                 IssUdbFilterInPortList));

                                    i4RetVal =
                                        IssHwUpdateUserDefinedFilter (NULL,
                                                                      &IssL2FilterEntry,
                                                                      NULL,
                                                                      (UINT4)
                                                                      u1Action);
                                    if (i4RetVal != FNP_SUCCESS)
                                    {
                                        return SNMP_FAILURE;
                                    }
                                }
#endif
                                break;
                            case ISS_USERDEFINED_L2FILTER_APPLY_NOT_OP:
#ifdef NPAPI_WANTED
                                if (ISS_IS_NP_PROGRAMMING_ALLOWED () ==
                                    ISS_TRUE)
                                {
                                    i4RetVal =
                                        IssHwUpdateL2Filter
                                        (pIssAclL2FilterEntryOne,
                                         ISS_L2FILTER_DELETE);
                                    if (i4RetVal != FNP_SUCCESS)
                                    {
                                        return SNMP_FAILURE;
                                    }
                                    pIssAclL2FilterEntryOne->u1IssL2HwStatus =
                                        ISS_NOTOK;
                                    IssL2FilterEntry.i4IssL2FilterNo =
                                        pIssAclUdbFilterTableEntry->
                                        pAccessFilterEntry->u4UDBFilterId;

                                    ISS_MEMCPY (IssL2FilterEntry.
                                                IssL2FilterInPortList,
                                                pIssAclUdbFilterTableEntry->
                                                pAccessFilterEntry->
                                                IssUdbFilterInPortList,
                                                sizeof
                                                (pIssAclUdbFilterTableEntry->
                                                 pAccessFilterEntry->
                                                 IssUdbFilterInPortList));

                                    i4RetVal =
                                        IssHwUpdateUserDefinedFilter (NULL,
                                                                      &IssL2FilterEntry,
                                                                      NULL,
                                                                      (UINT4)
                                                                      u1Action);
                                    if (i4RetVal != FNP_SUCCESS)
                                    {
                                        return SNMP_FAILURE;
                                    }
                                }
#endif
                                break;

                            case ISS_USERDEFINED_L2FILTER_APPLY_OR_OP:

#ifdef NPAPI_WANTED
                                if (ISS_IS_NP_PROGRAMMING_ALLOWED () ==
                                    ISS_TRUE)
                                {
                                    i4RetVal = IssHwUpdateL2Filter
                                        (pIssAclL2FilterEntryTwo,
                                         ISS_L2FILTER_DELETE);
                                    if (i4RetVal != FNP_SUCCESS)
                                    {
                                        return SNMP_FAILURE;
                                    }
                                    pIssAclL2FilterEntryTwo->u1IssL2HwStatus =
                                        ISS_NOTOK;
                                }
#endif

                                IssL2FilterEntry.i4IssL2FilterNo =
                                    pIssAclUdbFilterTableEntry->
                                    pAccessFilterEntry->u4UDBFilterId;

                                ISS_MEMCPY (IssL2FilterEntry.
                                            IssL2FilterInPortList,
                                            pIssAclUdbFilterTableEntry->
                                            pAccessFilterEntry->
                                            IssUdbFilterInPortList,
                                            sizeof (pIssAclUdbFilterTableEntry->
                                                    pAccessFilterEntry->
                                                    IssUdbFilterInPortList));

#ifdef NPAPI_WANTED
                                if (ISS_IS_NP_PROGRAMMING_ALLOWED () ==
                                    ISS_TRUE)
                                {
                                    i4RetVal =
                                        IssHwUpdateUserDefinedFilter (NULL,
                                                                      &IssL2FilterEntry,
                                                                      NULL,
                                                                      (UINT4)
                                                                      u1Action);
                                    if (i4RetVal != FNP_SUCCESS)
                                    {
                                        return SNMP_FAILURE;
                                    }
                                }
#endif

                                break;
                            case ISS_USERDEFINED_L3FILTER_APPLY_AND_OP:
#ifdef NPAPI_WANTED
                                if (ISS_IS_NP_PROGRAMMING_ALLOWED () ==
                                    ISS_TRUE)
                                {
                                    i4RetVal =
                                        IssHwUpdateL3Filter
                                        (pIssAclL3FilterEntryOne,
                                         ISS_L3FILTER_DELETE);
                                    if (i4RetVal != FNP_SUCCESS)
                                    {
                                        return SNMP_FAILURE;
                                    }
                                    i4RetVal =
                                        IssHwUpdateL3Filter
                                        (pIssAclL3FilterEntryTwo,
                                         ISS_L3FILTER_DELETE);
                                    if (i4RetVal != FNP_SUCCESS)
                                    {
                                        return SNMP_FAILURE;
                                    }
                                    pIssAclL3FilterEntryOne->u1IssL3HwStatus =
                                        ISS_NOTOK;
                                    pIssAclL3FilterEntryTwo->u1IssL3HwStatus =
                                        ISS_NOTOK;
                                    IssL3FilterEntry.i4IssL3FilterNo =
                                        pIssAclUdbFilterTableEntry->
                                        pAccessFilterEntry->u4UDBFilterId;

                                    ISS_MEMCPY (IssL3FilterEntry.
                                                IssL3FilterInPortList,
                                                pIssAclUdbFilterTableEntry->
                                                pAccessFilterEntry->
                                                IssUdbFilterInPortList,
                                                sizeof
                                                (pIssAclUdbFilterTableEntry->
                                                 pAccessFilterEntry->
                                                 IssUdbFilterInPortList));

                                    i4RetVal =
                                        IssHwUpdateUserDefinedFilter (NULL,
                                                                      NULL,
                                                                      &IssL3FilterEntry,
                                                                      (UINT4)
                                                                      u1Action);
                                    if (i4RetVal != FNP_SUCCESS)
                                    {
                                        return SNMP_FAILURE;
                                    }
                                }
#endif
                                break;
                            case ISS_USERDEFINED_L3FILTER_APPLY_NOT_OP:
#ifdef NPAPI_WANTED
                                if (ISS_IS_NP_PROGRAMMING_ALLOWED () ==
                                    ISS_TRUE)
                                {
                                    i4RetVal =
                                        IssHwUpdateL3Filter
                                        (pIssAclL3FilterEntryOne,
                                         ISS_L3FILTER_DELETE);
                                    if (i4RetVal != FNP_SUCCESS)
                                    {
                                        return SNMP_FAILURE;
                                    }
                                    pIssAclL3FilterEntryOne->u1IssL3HwStatus =
                                        ISS_NOTOK;
                                    IssL3FilterEntry.i4IssL3FilterNo =
                                        pIssAclUdbFilterTableEntry->
                                        pAccessFilterEntry->u4UDBFilterId;

                                    ISS_MEMCPY (IssL3FilterEntry.
                                                IssL3FilterInPortList,
                                                pIssAclUdbFilterTableEntry->
                                                pAccessFilterEntry->
                                                IssUdbFilterInPortList,
                                                sizeof
                                                (pIssAclUdbFilterTableEntry->
                                                 pAccessFilterEntry->
                                                 IssUdbFilterInPortList));

                                    i4RetVal =
                                        IssHwUpdateUserDefinedFilter (NULL,
                                                                      NULL,
                                                                      &IssL3FilterEntry,
                                                                      (UINT4)
                                                                      u1Action);
                                    if (i4RetVal != FNP_SUCCESS)
                                    {
                                        return SNMP_FAILURE;
                                    }
                                }
#endif
                                break;

                            case ISS_USERDEFINED_L3FILTER_APPLY_OR_OP:

#ifdef NPAPI_WANTED
                                if (ISS_IS_NP_PROGRAMMING_ALLOWED () ==
                                    ISS_TRUE)
                                {
                                    i4RetVal = IssHwUpdateL3Filter
                                        (pIssAclL3FilterEntryTwo,
                                         ISS_L3FILTER_DELETE);
                                    if (i4RetVal != FNP_SUCCESS)
                                    {
                                        return SNMP_FAILURE;
                                    }
                                    pIssAclL3FilterEntryTwo->u1IssL3HwStatus =
                                        ISS_NOTOK;
                                }
#endif

                                IssL3FilterEntry.i4IssL3FilterNo =
                                    pIssAclUdbFilterTableEntry->
                                    pAccessFilterEntry->u4UDBFilterId;

                                ISS_MEMCPY (IssL3FilterEntry.
                                            IssL3FilterInPortList,
                                            pIssAclUdbFilterTableEntry->
                                            pAccessFilterEntry->
                                            IssUdbFilterInPortList,
                                            sizeof (pIssAclUdbFilterTableEntry->
                                                    pAccessFilterEntry->
                                                    IssUdbFilterInPortList));

#ifdef NPAPI_WANTED
                                if (ISS_IS_NP_PROGRAMMING_ALLOWED () ==
                                    ISS_TRUE)
                                {
                                    i4RetVal =
                                        IssHwUpdateUserDefinedFilter (NULL,
                                                                      NULL,
                                                                      &IssL3FilterEntry,
                                                                      (UINT4)
                                                                      u1Action);
                                    if (i4RetVal != FNP_SUCCESS)
                                    {
                                        return SNMP_FAILURE;
                                    }
                                }
#endif

                                break;
                            case ISS_USERDEFINED_L2L3FILTER_APPLY_AND_OP:

#ifdef NPAPI_WANTED

                                if (ISS_IS_NP_PROGRAMMING_ALLOWED () ==
                                    ISS_TRUE)
                                {

                                    i4RetVal =
                                        IssHwUpdateL2Filter
                                        (pIssAclL2FilterEntryOne,
                                         ISS_L2FILTER_DELETE);
                                    if (i4RetVal != FNP_SUCCESS)
                                    {
                                        return SNMP_FAILURE;
                                    }

                                    pIssAclL2FilterEntryOne->u1IssL2HwStatus =
                                        ISS_NOTOK;

                                    i4RetVal =
                                        IssHwUpdateL3Filter
                                        (pIssAclL3FilterEntryTwo,
                                         ISS_L3FILTER_DELETE);
                                    if (i4RetVal != FNP_SUCCESS)
                                    {
                                        return SNMP_FAILURE;
                                    }
                                    pIssAclL3FilterEntryTwo->u1IssL3HwStatus =
                                        ISS_NOTOK;
                                }
                                if (ISS_IS_NP_PROGRAMMING_ALLOWED () ==
                                    ISS_TRUE)
                                {

                                    i4RetVal =
                                        IssHwUpdateUserDefinedFilter
                                        (pIssAclUdbFilterTableEntry->
                                         pAccessFilterEntry,
                                         pIssAclL2FilterEntryOne,
                                         pIssAclL3FilterEntryTwo,
                                         (UINT4) u1Action);
                                    if (i4RetVal != FNP_SUCCESS)
                                    {
                                        return SNMP_FAILURE;
                                    }
                                }
#endif
                                break;
                            case ISS_USERDEFINED_L2L3FILTER_APPLY_OR_OP:

#ifdef NPAPI_WANTED

                                if (ISS_IS_NP_PROGRAMMING_ALLOWED () ==
                                    ISS_TRUE)
                                {

                                    i4RetVal =
                                        IssHwUpdateL2Filter
                                        (pIssAclL2FilterEntryTwo,
                                         ISS_L2FILTER_DELETE);
                                    if (i4RetVal != FNP_SUCCESS)
                                    {
                                        return SNMP_FAILURE;
                                    }

                                    pIssAclL2FilterEntryTwo->u1IssL2HwStatus =
                                        ISS_NOTOK;

                                    i4RetVal =
                                        IssHwUpdateL3Filter
                                        (pIssAclL3FilterEntryOne,
                                         ISS_L3FILTER_DELETE);
                                    if (i4RetVal != FNP_SUCCESS)
                                    {
                                        return SNMP_FAILURE;
                                    }
                                    pIssAclL3FilterEntryOne->u1IssL3HwStatus =
                                        ISS_NOTOK;
                                }
                                if (ISS_IS_NP_PROGRAMMING_ALLOWED () ==
                                    ISS_TRUE)
                                {

                                    i4RetVal =
                                        IssHwUpdateUserDefinedFilter
                                        (pIssAclUdbFilterTableEntry->
                                         pAccessFilterEntry,
                                         pIssAclL2FilterEntryTwo,
                                         pIssAclL3FilterEntryOne,
                                         (UINT4) u1Action);
                                    if (i4RetVal != FNP_SUCCESS)
                                    {
                                        return SNMP_FAILURE;
                                    }
                                }
#endif
                                break;

                            default:
                                return SNMP_FAILURE;

                        }

                    }

                }

#endif
            }
            pIssAclUdbFilterTableEntry->u1RowStatus = ISS_ACTIVE;
            break;

        case ISS_DESTROY:

            if (pIssAclUdbFilterTableEntry->u1PriorityFlag == ISS_FALSE)
            {
                IssExDeleteAclPriorityFilterTable (pIssAclUdbFilterTableEntry->
                                                   pAccessFilterEntry->
                                                   u1AccessFilterPriority,
                                                   &
                                                   (pIssAclUdbFilterTableEntry->
                                                    IssNextNode),
                                                   ISS_UDB_REDIRECT);
            }

            i4CommitFlag = IssExGetIssCommitSupportImmediate ();
            i4CommitAction = IssGetTriggerCommit ();

            if ((i4CommitFlag == ISS_TRUE) || (i4CommitAction == ISS_TRUE))
            {
#ifdef NPAPI_WANTED
                if (pIssAclUdbFilterTableEntry->u1RowStatus == ISS_ACTIVE)
                {
                    if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                    {
                        if ((pIssAclUdbFilterTableEntry->
                             pAccessFilterEntry->IssAccessFilterAction
                             == ISS_ALLOW) ||
                            (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                             IssAccessFilterAction == ISS_DROP))
                        {
                            IssHwUpdateUserDefinedFilter
                                (pIssAclUdbFilterTableEntry->pAccessFilterEntry,
                                 NULL, NULL,
                                 ISS_USERDEFINED_ACCESSFILTER_DELETE);
                        }
                        else if (pIssAclUdbFilterTableEntry->
                                 pAccessFilterEntry->IssAccessFilterAction ==
                                 ISS_NOT)
                        {

                            if (pIssAclUdbFilterTableEntry->u1AclOneType ==
                                ISS_L2FILTER)
                            {
                                IssHwUpdateUserDefinedFilter
                                    (pIssAclUdbFilterTableEntry->
                                     pAccessFilterEntry, NULL, NULL,
                                     ISS_USERDEFINED_L2L2FILTER_DELETE);
                            }
                            else if (pIssAclUdbFilterTableEntry->u1AclOneType ==
                                     ISS_L3_FILTER)
                            {
                                IssHwUpdateUserDefinedFilter
                                    (pIssAclUdbFilterTableEntry->
                                     pAccessFilterEntry, NULL, NULL,
                                     ISS_USERDEFINED_L3L3FILTER_DELETE);
                            }
                        }
                        else if ((pIssAclUdbFilterTableEntry->
                                  pAccessFilterEntry->IssAccessFilterAction ==
                                  ISS_AND) || (pIssAclUdbFilterTableEntry->
                                               pAccessFilterEntry->
                                               IssAccessFilterAction == ISS_OR))
                        {

                            if ((pIssAclUdbFilterTableEntry->u1AclOneType ==
                                 ISS_L2FILTER) &&
                                (pIssAclUdbFilterTableEntry->u1AclTwoType ==
                                 ISS_L2FILTER))
                            {
                                IssHwUpdateUserDefinedFilter
                                    (pIssAclUdbFilterTableEntry->
                                     pAccessFilterEntry, NULL, NULL,
                                     ISS_USERDEFINED_L2L2FILTER_DELETE);
                            }
                            else if ((pIssAclUdbFilterTableEntry->
                                      u1AclOneType == ISS_L3FILTER)
                                     && (pIssAclUdbFilterTableEntry->
                                         u1AclTwoType == ISS_L3FILTER))
                            {
                                IssHwUpdateUserDefinedFilter
                                    (pIssAclUdbFilterTableEntry->
                                     pAccessFilterEntry, NULL, NULL,
                                     ISS_USERDEFINED_L3L3FILTER_DELETE);
                            }
                            else if (pIssAclUdbFilterTableEntry->u1AclOneType !=
                                     pIssAclUdbFilterTableEntry->u1AclTwoType)

                            {
                                IssHwUpdateUserDefinedFilter
                                    (pIssAclUdbFilterTableEntry->
                                     pAccessFilterEntry, NULL, NULL,
                                     ISS_USERDEFINED_L2L3FILTER_DELETE);

                            }

                        }
                    }
                }
#endif
            }
            if (pIssAclUdbFilterTableEntry->u1PriorityFlag == ISS_FALSE)
            {
                /* Deleting the node from the list */
                ISS_SLL_DELETE (&(ISS_UDB_FILTER_TABLE_LIST),
                                &(pIssAclUdbFilterTableEntry->IssNextNode));

                /* Delete the Entry from the Software */
                if (ISS_UDB_FILTERENTRY_FREE_MEM_BLOCK
                    (pIssAclUdbFilterTableEntry->pAccessFilterEntry) !=
                    MEM_SUCCESS)
                {
                    ISS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                             "UDB Filter Entry Free Failure\n");
                    return SNMP_FAILURE;
                }

                /* Delete the Entry from the Software */
                if (ISS_UDB_FILTERTABLE_FREE_MEM_BLOCK
                    (pIssAclUdbFilterTableEntry) != MEM_SUCCESS)
                {
                    ISS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                             "UDB Filter Table Free Failure\n");
                    return SNMP_FAILURE;
                }
            }
            break;

        default:

            return SNMP_FAILURE;

    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IssAclUserDefinedFilterPktType
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                testValIssAclUserDefinedFilterPktType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclUserDefinedFilterPktType (UINT4
                                         *pu4ErrorCode,
                                         UINT4
                                         u4IssAclUserDefinedFilterId,
                                         INT4
                                         i4TestValIssAclUserDefinedFilterPktType)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->u1RowStatus == ISS_ACTIVE))
    {
        return SNMP_FAILURE;
    }

    if (i4TestValIssAclUserDefinedFilterPktType > ISS_UDB_PKT_TYPE_IPV6_UDP)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssAclUserDefinedFilterOffSetBase
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                testValIssAclUserDefinedFilterOffSetBase
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclUserDefinedFilterOffSetBase (UINT4
                                            *pu4ErrorCode,
                                            UINT4
                                            u4IssAclUserDefinedFilterId,
                                            INT4
                                            i4TestValIssAclUserDefinedFilterOffSetBase)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->u1RowStatus == ISS_ACTIVE))
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclUserDefinedFilterOffSetBase < 0) ||
        (i4TestValIssAclUserDefinedFilterOffSetBase >
         ISS_UDB_OFFSET_MPLS_MINUS_2))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclUserDefinedFilterOffSetValue
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                testValIssAclUserDefinedFilterOffSetValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclUserDefinedFilterOffSetValue (UINT4
                                             *pu4ErrorCode,
                                             UINT4
                                             u4IssAclUserDefinedFilterId,
                                             tSNMP_OCTET_STRING_TYPE
                                             *
                                             pTestValIssAclUserDefinedFilterOffSetValue)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->u1RowStatus == ISS_ACTIVE))
    {
        return SNMP_FAILURE;
    }
    if ((pTestValIssAclUserDefinedFilterOffSetValue->i4_Length <= 0) ||
        (pTestValIssAclUserDefinedFilterOffSetValue->i4_Length >
         ISS_UDB_MAX_OFFSET))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssAclUserDefinedFilterOffSetMask
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                testValIssAclUserDefinedFilterOffSetMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclUserDefinedFilterOffSetMask (UINT4
                                            *pu4ErrorCode,
                                            UINT4
                                            u4IssAclUserDefinedFilterId,
                                            tSNMP_OCTET_STRING_TYPE
                                            *
                                            pTestValIssAclUserDefinedFilterOffSetMask)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);
    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->u1RowStatus == ISS_ACTIVE))
    {
        return SNMP_FAILURE;
    }
    if ((pTestValIssAclUserDefinedFilterOffSetMask->i4_Length <= 0) ||
        (pTestValIssAclUserDefinedFilterOffSetMask->i4_Length >
         ISS_UDB_MAX_OFFSET))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssAclUserDefinedFilterPriority
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                testValIssAclUserDefinedFilterPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclUserDefinedFilterPriority (UINT4
                                          *pu4ErrorCode,
                                          UINT4
                                          u4IssAclUserDefinedFilterId,
                                          INT4
                                          i4TestValIssAclUserDefinedFilterPriority)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->u1RowStatus == ISS_ACTIVE))
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclUserDefinedFilterPriority <= 0) ||
        (i4TestValIssAclUserDefinedFilterPriority > 255))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssAclUserDefinedFilterAction
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                testValIssAclUserDefinedFilterAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclUserDefinedFilterAction (UINT4
                                        *pu4ErrorCode,
                                        UINT4
                                        u4IssAclUserDefinedFilterId,
                                        INT4
                                        i4TestValIssAclUserDefinedFilterAction)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->u1RowStatus == ISS_ACTIVE))
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclUserDefinedFilterAction != ISS_ALLOW) &&
        (i4TestValIssAclUserDefinedFilterAction != ISS_DROP) &&
        (i4TestValIssAclUserDefinedFilterAction != ISS_REDIRECT_TO) &&
        (i4TestValIssAclUserDefinedFilterAction != ISS_AND) &&
        (i4TestValIssAclUserDefinedFilterAction != ISS_OR) &&
        (i4TestValIssAclUserDefinedFilterAction != ISS_NOT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssAclUserDefinedFilterInPortList
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                testValIssAclUserDefinedFilterInPortList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclUserDefinedFilterInPortList (UINT4
                                            *pu4ErrorCode,
                                            UINT4
                                            u4IssAclUserDefinedFilterId,
                                            tSNMP_OCTET_STRING_TYPE
                                            *
                                            pTestValIssAclUserDefinedFilterInPortList)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclUdbFilterTableEntry->u1RowStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pTestValIssAclUserDefinedFilterInPortList->i4_Length <= 0) ||
        (pTestValIssAclUserDefinedFilterInPortList->i4_Length >
         ISS_PORT_LIST_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    /* This function checks if any port greater than the maximum number of
     * ports in the system is in the port list.*/

    if (IssIsPortListValid
        (pTestValIssAclUserDefinedFilterInPortList->pu1_OctetList,
         pTestValIssAclUserDefinedFilterInPortList->i4_Length) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssAclUserDefinedFilterIdOneType
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                testValIssAclUserDefinedFilterIdOneType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclUserDefinedFilterIdOneType (UINT4
                                           *pu4ErrorCode,
                                           UINT4
                                           u4IssAclUserDefinedFilterId,
                                           INT4
                                           i4TestValIssAclUserDefinedFilterIdOneType)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->u1RowStatus == ISS_ACTIVE))
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclUserDefinedFilterIdOneType != ISS_L2_FILTER) &&
        (i4TestValIssAclUserDefinedFilterIdOneType != ISS_L3_FILTER))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssAclUserDefinedFilterIdOne
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                testValIssAclUserDefinedFilterIdOne
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclUserDefinedFilterIdOne (UINT4
                                       *pu4ErrorCode,
                                       UINT4
                                       u4IssAclUserDefinedFilterId,
                                       UINT4
                                       u4TestValIssAclUserDefinedFilterIdOne)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->u1RowStatus == ISS_ACTIVE))
    {
        return SNMP_FAILURE;
    }

    if (u4TestValIssAclUserDefinedFilterIdOne == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssAclUserDefinedFilterSubAction
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                testValIssAclUserDefinedFilterSubAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclUserDefinedFilterSubAction (UINT4
                                           *pu4ErrorCode,
                                           UINT4
                                           u4IssAclUserDefinedFilterId,
                                           INT4
                                           i4TestValIssAclUserDefinedFilterSubAction)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry != NULL &&
        pIssAclUdbFilterTableEntry->u1RowStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclUserDefinedFilterSubAction != ISS_NONE) &&
        (i4TestValIssAclUserDefinedFilterSubAction != ISS_MODIFY_VLAN) &&
        (i4TestValIssAclUserDefinedFilterSubAction != ISS_NESTED_VLAN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssAclUserDefinedFilterSubActionId
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                testValIssAclUserDefinedFilterSubActionId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclUserDefinedFilterSubActionId (UINT4
                                             *pu4ErrorCode,
                                             UINT4
                                             u4IssAclUserDefinedFilterId,
                                             INT4
                                             i4TestValIssAclUserDefinedFilterSubActionId)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if (pIssAclUdbFilterTableEntry != NULL &&
        pIssAclUdbFilterTableEntry->u1RowStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclUserDefinedFilterSubActionId < 0) ||
        (i4TestValIssAclUserDefinedFilterSubActionId > 4094))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssAclUserDefinedFilterIdTwoType
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                testValIssAclUserDefinedFilterIdTwoType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclUserDefinedFilterIdTwoType (UINT4
                                           *pu4ErrorCode,
                                           UINT4
                                           u4IssAclUserDefinedFilterId,
                                           INT4
                                           i4TestValIssAclUserDefinedFilterIdTwoType)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->u1RowStatus == ISS_ACTIVE))
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclUserDefinedFilterIdTwoType != ISS_L2_FILTER) &&
        (i4TestValIssAclUserDefinedFilterIdTwoType != ISS_L3_FILTER))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclUserDefinedFilterIdTwo
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                testValIssAclUserDefinedFilterIdTwo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclUserDefinedFilterIdTwo (UINT4
                                       *pu4ErrorCode,
                                       UINT4
                                       u4IssAclUserDefinedFilterId,
                                       UINT4
                                       u4TestValIssAclUserDefinedFilterIdTwo)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;

    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->u1RowStatus == ISS_ACTIVE))
    {
        return SNMP_FAILURE;
    }

    if (u4TestValIssAclUserDefinedFilterIdTwo == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssAclUserDefinedFilterStatus
 Input       :  The Indices
                IssAclUserDefinedFilterId

                The Object 
                testValIssAclUserDefinedFilterStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclUserDefinedFilterStatus (UINT4
                                        *pu4ErrorCode,
                                        UINT4
                                        u4IssAclUserDefinedFilterId,
                                        INT4
                                        i4TestValIssAclUserDefinedFilterStatus)
{
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;
    UINT4               u4RedirectIntfGrp = 0x0;

    if (ISS_IS_UDB_FILTER_ID_VALID (u4IssAclUserDefinedFilterId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclUserDefinedFilterStatus < ISS_ACTIVE)
        || (i4TestValIssAclUserDefinedFilterStatus > ISS_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
        (u4IssAclUserDefinedFilterId);

    if ((pIssAclUdbFilterTableEntry == NULL) &&
        ((i4TestValIssAclUserDefinedFilterStatus != ISS_CREATE_AND_WAIT)
         && (i4TestValIssAclUserDefinedFilterStatus != ISS_CREATE_AND_GO)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        ((i4TestValIssAclUserDefinedFilterStatus == ISS_CREATE_AND_WAIT)
         || (i4TestValIssAclUserDefinedFilterStatus == ISS_CREATE_AND_GO)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->u1RowStatus != ISS_ACTIVE) &&
        (i4TestValIssAclUserDefinedFilterStatus == ISS_NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (i4TestValIssAclUserDefinedFilterStatus == ISS_ACTIVE))

    {
        /* Before Making Row Status Following 
         * Things Has to Be Taken Care in Case of UDB Filter */

        if (pIssAclUdbFilterTableEntry->pAccessFilterEntry == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }

        if (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
            IssAccessFilterAction == 0x0)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }

    if ((pIssAclUdbFilterTableEntry != NULL) &&
        (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
         IssAccessFilterAction == ISS_REDIRECT_TO) &&
        (i4TestValIssAclUserDefinedFilterStatus == ISS_DESTROY))
    {
        u4RedirectIntfGrp =
            pIssAclUdbFilterTableEntry->pAccessFilterEntry->RedirectIfGrp.
            u4RedirectGrpId;

        if (u4RedirectIntfGrp != 0x0)
        {
            if (gpIssRedirectIntfInfo[u4RedirectIntfGrp - 1].u1RowStatus != 0)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IssAclUserDefinedFilterTable
 Input       :  The Indices
                IssAclUserDefinedFilterId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IssAclUserDefinedFilterTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IssRedirectInterfaceGrpTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssRedirectInterfaceGrpTable
 Input       :  The Indices
                IssRedirectInterfaceGrpId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssRedirectInterfaceGrpTable (UINT4
                                                      u4IssRedirectInterfaceGrpId)
{
    if ((u4IssRedirectInterfaceGrpId == 0) || (u4IssRedirectInterfaceGrpId > 1))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssRedirectInterfaceGrpTable
 Input       :  The Indices
                IssRedirectInterfaceGrpId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssRedirectInterfaceGrpTable (UINT4
                                              *pu4IssRedirectInterfaceGrpId)
{

    UINT4               u4FirstIndex = 0x0;

    if (nmhGetNextIndexIssRedirectInterfaceGrpTable (0, &u4FirstIndex) ==
        SNMP_FAILURE)
    {

        return SNMP_FAILURE;

    }

    *pu4IssRedirectInterfaceGrpId = u4FirstIndex;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIssRedirectInterfaceGrpTable
 Input       :  The Indices
                IssRedirectInterfaceGrpId
                nextIssRedirectInterfaceGrpId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIssRedirectInterfaceGrpTable (UINT4
                                             u4IssRedirectInterfaceGrpId,
                                             UINT4
                                             *pu4NextIssRedirectInterfaceGrpId)
{
    UINT4               u4TempId = 0x0;

    if ((u4IssRedirectInterfaceGrpId == 50))
    {
        return SNMP_FAILURE;
    }
    for (u4TempId = u4IssRedirectInterfaceGrpId + 1;
         u4TempId <= ISS_MAX_REDIRECT_GRP_ID; u4TempId++)
    {
        if (gpIssRedirectIntfInfo[u4TempId - 1].u1RowStatus == 0x0)
        {
            continue;
        }
        else
        {
            *pu4NextIssRedirectInterfaceGrpId = u4TempId;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIssRedirectInterfaceGrpFilterType
 Input       :  The Indices
                IssRedirectInterfaceGrpId

                The Object 
                retValIssRedirectInterfaceGrpFilterType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssRedirectInterfaceGrpFilterType (UINT4
                                         u4IssRedirectInterfaceGrpId,
                                         INT4
                                         *pi4RetValIssRedirectInterfaceGrpFilterType)
{
    *pi4RetValIssRedirectInterfaceGrpFilterType =
        gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1AclIdType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIssRedirectInterfaceGrpFilterId
 Input       :  The Indices
                IssRedirectInterfaceGrpId

                The Object 
                retValIssRedirectInterfaceGrpFilterId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssRedirectInterfaceGrpFilterId (UINT4
                                       u4IssRedirectInterfaceGrpId,
                                       UINT4
                                       *pu4RetValIssRedirectInterfaceGrpFilterId)
{
    *pu4RetValIssRedirectInterfaceGrpFilterId =
        gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u4AclId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIssRedirectInterfaceGrpDistByte
 Input       :  The Indices
                IssRedirectInterfaceGrpId

                The Object 
                retValIssRedirectInterfaceGrpDistByte
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssRedirectInterfaceGrpDistByte (UINT4
                                       u4IssRedirectInterfaceGrpId,
                                       INT4
                                       *pi4RetValIssRedirectInterfaceGrpDistByte)
{
    *pi4RetValIssRedirectInterfaceGrpDistByte =
        gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId -
                              1].u2TrafficDistByte;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIssRedirectInterfaceGrpPortList
 Input       :  The Indices
                IssRedirectInterfaceGrpId

                The Object 
                retValIssRedirectInterfaceGrpPortList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssRedirectInterfaceGrpPortList (UINT4
                                       u4IssRedirectInterfaceGrpId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pRetValIssRedirectInterfaceGrpPortList)
{
    MEMCPY (pRetValIssRedirectInterfaceGrpPortList->pu1_OctetList,
            gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].PortList,
            sizeof (tPortList));

    pRetValIssRedirectInterfaceGrpPortList->i4_Length = sizeof (tPortList);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIssRedirectInterfaceGrpType
 Input       :  The Indices
                IssRedirectInterfaceGrpId

                The Object 
                retValIssRedirectInterfaceGrpType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssRedirectInterfaceGrpType (UINT4
                                   u4IssRedirectInterfaceGrpId,
                                   INT4 *pi4RetValIssRedirectInterfaceGrpType)
{
    *pi4RetValIssRedirectInterfaceGrpType =
        gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1PortListType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIssRedirectInterfaceGrpUdbPosition
 Input       :  The Indices
                IssRedirectInterfaceGrpId

                The Object 
                retValIssRedirectInterfaceGrpUdbPosition
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssRedirectInterfaceGrpUdbPosition (UINT4 u4IssRedirectInterfaceGrpId,
                                          INT4
                                          *pi4RetValIssRedirectInterfaceGrpUdbPosition)
{
    *pi4RetValIssRedirectInterfaceGrpUdbPosition = (INT4)
        gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1UdbPosition;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIssRedirectInterfaceGrpStatus
 Input       :  The Indices
                IssRedirectInterfaceGrpId

                The Object 
                retValIssRedirectInterfaceGrpStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssRedirectInterfaceGrpStatus (UINT4
                                     u4IssRedirectInterfaceGrpId,
                                     INT4
                                     *pi4RetValIssRedirectInterfaceGrpStatus)
{
    *pi4RetValIssRedirectInterfaceGrpStatus =
        gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1RowStatus;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIssRedirectInterfaceGrpFilterType
 Input       :  The Indices
                IssRedirectInterfaceGrpId

                The Object 
                setValIssRedirectInterfaceGrpFilterType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssRedirectInterfaceGrpFilterType (UINT4
                                         u4IssRedirectInterfaceGrpId,
                                         INT4
                                         i4SetValIssRedirectInterfaceGrpFilterType)
{

    gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1AclIdType
        = (UINT1) i4SetValIssRedirectInterfaceGrpFilterType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssRedirectInterfaceGrpFilterId
 Input       :  The Indices
                IssRedirectInterfaceGrpId

                The Object 
                setValIssRedirectInterfaceGrpFilterId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssRedirectInterfaceGrpFilterId (UINT4
                                       u4IssRedirectInterfaceGrpId,
                                       UINT4
                                       u4SetValIssRedirectInterfaceGrpFilterId)
{
    gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u4AclId
        = u4SetValIssRedirectInterfaceGrpFilterId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssRedirectInterfaceGrpDistByte
 Input       :  The Indices
                IssRedirectInterfaceGrpId

                The Object 
                setValIssRedirectInterfaceGrpDistByte
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssRedirectInterfaceGrpDistByte (UINT4
                                       u4IssRedirectInterfaceGrpId,
                                       INT4
                                       i4SetValIssRedirectInterfaceGrpDistByte)
{
    gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u2TrafficDistByte
        = (UINT2) i4SetValIssRedirectInterfaceGrpDistByte;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssRedirectInterfaceGrpPortList
 Input       :  The Indices
                IssRedirectInterfaceGrpId

                The Object 
                setValIssRedirectInterfaceGrpPortList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssRedirectInterfaceGrpPortList (UINT4
                                       u4IssRedirectInterfaceGrpId,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pSetValIssRedirectInterfaceGrpPortList)
{
    ISS_MEMCPY (gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].
                PortList, pSetValIssRedirectInterfaceGrpPortList->pu1_OctetList,
                pSetValIssRedirectInterfaceGrpPortList->i4_Length);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssRedirectInterfaceGrpType
 Input       :  The Indices
                IssRedirectInterfaceGrpId

                The Object 
                setValIssRedirectInterfaceGrpType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssRedirectInterfaceGrpType (UINT4
                                   u4IssRedirectInterfaceGrpId,
                                   INT4 i4SetValIssRedirectInterfaceGrpType)
{
    gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1PortListType
        = (UINT1) i4SetValIssRedirectInterfaceGrpType;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssRedirectInterfaceGrpUdbPosition
 Input       :  The Indices
                IssRedirectInterfaceGrpId

                The Object 
                setValIssRedirectInterfaceGrpUdbPosition
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssRedirectInterfaceGrpUdbPosition (UINT4 u4IssRedirectInterfaceGrpId,
                                          INT4
                                          i4SetValIssRedirectInterfaceGrpUdbPosition)
{
    gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1UdbPosition
        = (UINT1) i4SetValIssRedirectInterfaceGrpUdbPosition;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssRedirectInterfaceGrpStatus
 Input       :  The Indices
                IssRedirectInterfaceGrpId

                The Object 
                setValIssRedirectInterfaceGrpStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssRedirectInterfaceGrpStatus (UINT4
                                     u4IssRedirectInterfaceGrpId,
                                     INT4 i4SetValIssRedirectInterfaceGrpStatus)
{

    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;
    tIssRedirectPortMaskAndIndex *pIssRedirectPortMaskAndIndex = NULL;
    tIssRedirectIfPortGroup *pIssRedirectIfPortGroup = NULL;
    UINT4               au4PortList[ISS_REDIRECT_MAX_PORTS];
    UINT4               u4NoOfRules = 0;
    UINT4               u4AclId = 0x0;
    UINT4               u4NumPorts = 0x0;
    UINT2               u2TrafficDistByte = 0x0;
    UINT1               u1ActivePortExist;
    UINT1               u1ReDirectPortIndex = 0x0;
    UINT1               u1Index = 0x0;
    UINT1               u1Mask = 0x0;
    UINT1               u1AclIdType = 0x0;
    UINT1               u1CurrRowStatus = 0;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif
    INT4                i4CommitAction = 0;
    INT4                i4CommitFlag = 0;

    ISS_MEMSET (gNullPortList, 0, ISS_PORT_LIST_SIZE);
    ISS_MEMSET (au4PortList, 0, sizeof (au4PortList));

    u1CurrRowStatus =
        gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1RowStatus;

    if (u1CurrRowStatus == i4SetValIssRedirectInterfaceGrpStatus)
    {
        if (gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].
            u1PriorityFlag == ISS_FALSE)
        {
            return SNMP_SUCCESS;
        }
    }

    u4AclId = gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u4AclId;

    u1AclIdType = gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].
        u1AclIdType;

    switch (i4SetValIssRedirectInterfaceGrpStatus)
    {
        case ISS_CREATE_AND_WAIT:
        case ISS_CREATE_AND_GO:

            ISS_MEMSET (&gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1],
                        0, sizeof (tIssRedirectIntfGrpTable));

            gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].
                u4RedirectIntfGrpId = u4IssRedirectInterfaceGrpId;

            gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1RowStatus
                = ISS_NOT_READY;
            gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId -
                                  1].u1PriorityFlag = ISS_FALSE;
            break;
        case ISS_NOT_IN_SERVICE:

            /* Delete only the Data Plane Information */
#ifdef NPAPI_WANTED
            switch (u1AclIdType)
            {

                case ISS_L2_REDIRECT:
                    pIssL2FilterEntry = IssExtGetL2FilterEntry (u4AclId);

                    if (pIssL2FilterEntry == NULL)
                    {
                        return SNMP_FAILURE;
                    }
                    if (pIssL2FilterEntry->RedirectIfGrp.u1PriorityFlag ==
                        ISS_FALSE)
                    {
                        IssExDeleteAclPriorityFilterTable (pIssL2FilterEntry->
                                                           i4IssL2FilterPriority,
                                                           &(pIssL2FilterEntry->
                                                             IssNextNode),
                                                           ISS_L2_REDIRECT);
                    }

                    i4CommitFlag = IssExGetIssCommitSupportImmediate ();
                    i4CommitAction = IssGetTriggerCommit ();

                    if ((i4CommitFlag == ISS_TRUE) ||
                        (i4CommitAction == ISS_TRUE))
                    {

#ifdef NPAPI_WANTED
                        if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                        {
                            i4RetVal = IssHwUpdateL2Filter (pIssL2FilterEntry,
                                                            ISS_L2FILTER_DELETE);
                            if (i4RetVal != FNP_SUCCESS)
                            {
                                return SNMP_FAILURE;
                            }
                        }
#endif
                    }

                    break;

                case ISS_L3_REDIRECT:
                    pIssL3FilterEntry = IssExtGetL3FilterEntry (u4AclId);

                    if (pIssL3FilterEntry == NULL)
                    {
                        return SNMP_FAILURE;
                    }
                    if (pIssL3FilterEntry->RedirectIfGrp.u1PriorityFlag ==
                        ISS_FALSE)
                    {
                        IssExDeleteAclPriorityFilterTable (pIssL3FilterEntry->
                                                           i4IssL3FilterPriority,
                                                           &(pIssL3FilterEntry->
                                                             IssNextNode),
                                                           ISS_L3_REDIRECT);
                    }

                    i4CommitFlag = IssExGetIssCommitSupportImmediate ();
                    i4CommitAction = IssGetTriggerCommit ();

                    if ((i4CommitFlag == ISS_TRUE) ||
                        (i4CommitAction == ISS_TRUE))

                    {
#ifdef NPAPI_WANTED
                        if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                        {
                            i4RetVal = IssHwUpdateL3Filter (pIssL3FilterEntry,
                                                            ISS_L3FILTER_DELETE);
                            if (i4RetVal != FNP_SUCCESS)
                            {
                                return SNMP_FAILURE;
                            }
                        }
#endif
                    }
                    break;
                case ISS_UDB_REDIRECT:

                    pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
                        (u4AclId);

                    if (pIssAclUdbFilterTableEntry == NULL)
                    {
                        return SNMP_FAILURE;
                    }

                    if (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                        RedirectIfGrp.u1PriorityFlag == ISS_FALSE)
                    {
                        IssExDeleteAclPriorityFilterTable
                            (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                             u1AccessFilterPriority,
                             &(pIssAclUdbFilterTableEntry->IssNextNode),
                             ISS_UDB_REDIRECT);
                    }

                    i4CommitFlag = IssExGetIssCommitSupportImmediate ();
                    i4CommitAction = IssGetTriggerCommit ();

                    if ((i4CommitFlag == ISS_TRUE) ||
                        (i4CommitAction == ISS_TRUE))

                    {
                        IssHwUpdateUserDefinedFilter
                            (pIssAclUdbFilterTableEntry->pAccessFilterEntry,
                             NULL, NULL, ISS_USERDEFINED_ACCESSFILTER_DELETE);
                    }
                    break;

            }

#endif

            gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1RowStatus
                = ISS_NOT_IN_SERVICE;

            break;

        case ISS_ACTIVE:

            if (u4AclId != 0x0)
            {
                if (ISS_REDIR_PORT_MSK_ALLOC_MEM_BLOCK
                    (pIssRedirectPortMaskAndIndex) == NULL)
                {
                    return SNMP_FAILURE;
                }

                ISS_MEMSET (pIssRedirectPortMaskAndIndex, 0,
                            (sizeof (tIssRedirectPortMaskAndIndex) *
                             ISS_MAX_REDIRECT_GRP_SIZE));

                if (ISS_REDIR_IFPORT_GRP_ALLOC_MEM_BLOCK
                    (pIssRedirectIfPortGroup) == NULL)
                {
                    ISS_REDIR_PORT_MSK_FREE_MEM_BLOCK
                        (pIssRedirectPortMaskAndIndex);
                    return SNMP_FAILURE;
                }

                ISS_MEMSET (pIssRedirectIfPortGroup, 0,
                            (sizeof (tIssRedirectIfPortGroup) *
                             ISS_MAX_REDIRECT_GRP_SIZE));

                /* If the Port List Type is Port List
                 * Extract the PortList into Port If Indexes */

                if (gpIssRedirectIntfInfo
                    [u4IssRedirectInterfaceGrpId - 1].u1PortListType
                    != ISS_REDIRECT_TO_PORT)
                {
                    IssConvertRedirectPortListToArray
                        (gpIssRedirectIntfInfo
                         [u4IssRedirectInterfaceGrpId - 1].PortList,
                         (tIssRedirectIfPortGroup *)
                         pIssRedirectIfPortGroup,
                         au4PortList, &u4NumPorts, &u1ActivePortExist);

                    if (u4NumPorts <= 1)
                    {
                        ISS_REDIR_PORT_MSK_FREE_MEM_BLOCK
                            (pIssRedirectPortMaskAndIndex);
                        ISS_REDIR_IFPORT_GRP_FREE_MEM_BLOCK
                            (pIssRedirectIfPortGroup);
                        return SNMP_FAILURE;
                    }
                }
                else
                {
                    /* PortList it self Contains Port Number
                     * So Update the Array with Port IfIndex */
                    ISS_MEMCPY
                        (&au4PortList[u4NumPorts],
                         gpIssRedirectIntfInfo
                         [u4IssRedirectInterfaceGrpId - 1].PortList,
                         sizeof (UINT4));

                    u4NumPorts = 1;
                    u4NoOfRules = 1;
                    u1Mask = 0x0;
                }

                if (u4NumPorts != 1)
                {
                    IssCalculateTrafficLogicForPort
                        (u4NumPorts,
                         (tIssRedirectPortMaskAndIndex *)
                         pIssRedirectPortMaskAndIndex, &u1Mask, &u4NoOfRules,
                         u1ActivePortExist,
                         (tIssRedirectIfPortGroup *) pIssRedirectIfPortGroup);
                }

                u2TrafficDistByte =
                    gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].
                    u2TrafficDistByte;
                switch (u1AclIdType)
                {

                    case ISS_L2_REDIRECT:

                        pIssL2FilterEntry = IssExtGetL2FilterEntry (u4AclId);

                        if (pIssL2FilterEntry == NULL)
                        {
                            ISS_REDIR_PORT_MSK_FREE_MEM_BLOCK
                                (pIssRedirectPortMaskAndIndex);
                            ISS_REDIR_IFPORT_GRP_FREE_MEM_BLOCK
                                (pIssRedirectIfPortGroup);
                            return SNMP_FAILURE;
                        }
                        for (u1Index = 0; u1Index < u4NoOfRules; u1Index++)
                        {

                            /* For the Redirect Filter Installation 
                             *  of Port / Trunk fill the parameters
                             *  of (Interface Type and Interface IfIndex) */

                            pIssL2FilterEntry->RedirectIfGrp.u2TunnelIfIndex
                                = 0;

                            pIssL2FilterEntry->RedirectIfGrp.u2TrafficDistByte
                                = u2TrafficDistByte;

                            pIssL2FilterEntry->RedirectIfGrp.
                                u1TrafficDistMaskLsb =
                                pIssRedirectPortMaskAndIndex[u1Index].
                                u1PortMaskLsb;

                            pIssL2FilterEntry->RedirectIfGrp.
                                u1TrafficDistMaskMsb =
                                pIssRedirectPortMaskAndIndex[u1Index].
                                u1PortMaskMsb;

                            pIssL2FilterEntry->RedirectIfGrp.
                                u1TrafficDistValueLsb =
                                pIssRedirectPortMaskAndIndex[u1Index].
                                u1PortValueLsb;

                            pIssL2FilterEntry->RedirectIfGrp.
                                u1TrafficDistValueMsb =
                                pIssRedirectPortMaskAndIndex[u1Index].
                                u1PortValueMsb;

                            u1ReDirectPortIndex = (UINT1)
                                pIssRedirectPortMaskAndIndex[u1Index].
                                u4PortIndex;

                            pIssL2FilterEntry->RedirectIfGrp.
                                u4EgressIfIndex =
                                au4PortList[u1ReDirectPortIndex % u4NumPorts];

                            /* Currently MPLS Tunnel Redirection is Not
                             * being done . Only Ethernet Port /Trunk
                             *  is being taken care of */

                            if (pIssL2FilterEntry->RedirectIfGrp.
                                u4EgressIfIndex <=
                                SYS_DEF_MAX_PHYSICAL_INTERFACES)
                            {
                                pIssL2FilterEntry->RedirectIfGrp.
                                    u1EgressIfType = ISS_REDIRECT_TO_ETHERNET;

                            }
                            else
                            {
                                pIssL2FilterEntry->RedirectIfGrp.
                                    u1EgressIfType = ISS_REDIRECT_TO_TRUNK;
                            }

                            pIssL2FilterEntry->RedirectIfGrp.
                                u4RedirectGrpId = u4IssRedirectInterfaceGrpId;
                            if (MEMCMP
                                (pIssL2FilterEntry->IssL2FilterInPortList,
                                 gNullPortList, ISS_PORT_LIST_SIZE) != 0)
                            {

                                if (pIssL2FilterEntry->RedirectIfGrp.
                                    u1PriorityFlag == ISS_FALSE)
                                {

                                    IssExAddAclToPriorityTable
                                        (pIssL2FilterEntry->
                                         i4IssL2FilterPriority,
                                         &(pIssL2FilterEntry->IssNextNode),
                                         ISS_L2_REDIRECT);
                                }

                                i4CommitFlag =
                                    IssExGetIssCommitSupportImmediate ();
                                i4CommitAction = IssGetTriggerCommit ();

                                if ((i4CommitFlag == ISS_TRUE) ||
                                    (i4CommitAction == ISS_TRUE))
                                {

#ifdef NPAPI_WANTED
                                    if (ISS_IS_NP_PROGRAMMING_ALLOWED () ==
                                        ISS_TRUE)
                                    {
                                        i4RetVal =
                                            IssHwUpdateL2Filter
                                            (pIssL2FilterEntry,
                                             ISS_L2FILTER_ADD);
                                        if (i4RetVal != FNP_SUCCESS)
                                        {
                                            ISS_REDIR_PORT_MSK_FREE_MEM_BLOCK
                                                (pIssRedirectPortMaskAndIndex);
                                            ISS_REDIR_IFPORT_GRP_FREE_MEM_BLOCK
                                                (pIssRedirectIfPortGroup);
                                            return SNMP_FAILURE;
                                        }
                                    }
#endif
                                }
                            }

                        }

                        /* Fill the Redirect Group Id also
                         * as it will be used in Kepping Tack
                         * of Same Filter is mapped to this 
                         * Redirect Group . USeful from CLI 
                         * Perpective. From SNMP Perspective
                         * Manager needs to take of ACL-
                         * Redirect Group Mapping */
                        gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].
                            u1RowStatus = ISS_ACTIVE;

                        break;
                    case ISS_L3_REDIRECT:
                        pIssL3FilterEntry = IssExtGetL3FilterEntry (u4AclId);

                        if (pIssL3FilterEntry == NULL)
                        {
                            ISS_REDIR_PORT_MSK_FREE_MEM_BLOCK
                                (pIssRedirectPortMaskAndIndex);
                            ISS_REDIR_IFPORT_GRP_FREE_MEM_BLOCK
                                (pIssRedirectIfPortGroup);
                            return SNMP_FAILURE;
                        }

                        for (u1Index = 0; u1Index < u4NoOfRules; u1Index++)
                        {

                            /* For the Redirect Filter Installation 
                             *  of Port / Trunk fill the parameters
                             *  of (Interface Type and Interface IfIndex) */

                            pIssL3FilterEntry->RedirectIfGrp.u2TunnelIfIndex
                                = 0;

                            pIssL3FilterEntry->RedirectIfGrp.u2TrafficDistByte
                                = u2TrafficDistByte;

                            pIssL3FilterEntry->RedirectIfGrp.
                                u1TrafficDistMaskLsb =
                                pIssRedirectPortMaskAndIndex[u1Index].
                                u1PortMaskLsb;

                            pIssL3FilterEntry->RedirectIfGrp.
                                u1TrafficDistMaskMsb =
                                pIssRedirectPortMaskAndIndex[u1Index].
                                u1PortMaskMsb;

                            pIssL3FilterEntry->RedirectIfGrp.
                                u1TrafficDistValueLsb =
                                pIssRedirectPortMaskAndIndex[u1Index].
                                u1PortValueLsb;

                            pIssL3FilterEntry->RedirectIfGrp.
                                u1TrafficDistValueMsb =
                                pIssRedirectPortMaskAndIndex[u1Index].
                                u1PortValueMsb;

                            u1ReDirectPortIndex = (UINT1)
                                pIssRedirectPortMaskAndIndex[u1Index].
                                u4PortIndex;

                            pIssL3FilterEntry->RedirectIfGrp.
                                u4EgressIfIndex =
                                au4PortList[u1ReDirectPortIndex % u4NumPorts];

                            /* Currently MPLS Tunnel Redirection is Not
                             * being done . Only Ethernet Port /Trunk
                             *  is being taken care of */

                            if (pIssL3FilterEntry->RedirectIfGrp.
                                u4EgressIfIndex <=
                                SYS_DEF_MAX_PHYSICAL_INTERFACES)
                            {
                                pIssL3FilterEntry->RedirectIfGrp.
                                    u1EgressIfType = ISS_REDIRECT_TO_ETHERNET;

                            }
                            else
                            {
                                pIssL3FilterEntry->RedirectIfGrp.
                                    u1EgressIfType = ISS_REDIRECT_TO_TRUNK;
                            }

                            pIssL3FilterEntry->RedirectIfGrp.
                                u4RedirectGrpId = u4IssRedirectInterfaceGrpId;

                            if (MEMCMP
                                (pIssL3FilterEntry->IssL3FilterInPortList,
                                 gNullPortList,
                                 sizeof (pIssL3FilterEntry->
                                         IssL3FilterInPortList)) != 0)
                            {
                                if (pIssL3FilterEntry->RedirectIfGrp.
                                    u1PriorityFlag == ISS_FALSE)
                                {

                                    IssExAddAclToPriorityTable
                                        (pIssL3FilterEntry->
                                         i4IssL3FilterPriority,
                                         &(pIssL3FilterEntry->IssNextNode),
                                         ISS_L3_REDIRECT);
                                }
                                i4CommitFlag =
                                    IssExGetIssCommitSupportImmediate ();
                                i4CommitAction = IssGetTriggerCommit ();

                                if ((i4CommitFlag == ISS_TRUE) ||
                                    (i4CommitAction == ISS_TRUE))
                                {
#ifdef NPAPI_WANTED
                                    if (ISS_IS_NP_PROGRAMMING_ALLOWED () ==
                                        ISS_TRUE)
                                    {
                                        i4RetVal = IssHwUpdateL3Filter
                                            (pIssL3FilterEntry,
                                             ISS_L3FILTER_ADD);
                                        if (i4RetVal != FNP_SUCCESS)
                                        {
                                            ISS_REDIR_PORT_MSK_FREE_MEM_BLOCK
                                                (pIssRedirectPortMaskAndIndex);
                                            ISS_REDIR_IFPORT_GRP_FREE_MEM_BLOCK
                                                (pIssRedirectIfPortGroup);
                                            return SNMP_FAILURE;
                                        }
                                    }
#endif
                                }
                            }

                        }

                        /* Fill the Redirect Group Id also
                         * as it will be used in Kepping Tack
                         * of Same Filter is mapped to this 
                         * Redirect Group . USeful from CLI 
                         * Perpective. From SNMP Perspective
                         * Manager needs to take of ACL-
                         * Redirect Group Mapping */

                        gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].
                            u1RowStatus = ISS_ACTIVE;
                        break;
                    case ISS_UDB_REDIRECT:
                        pIssAclUdbFilterTableEntry =
                            IssExtGetUdbFilterTableEntry (u4AclId);

                        if (pIssAclUdbFilterTableEntry == NULL)
                        {
                            ISS_REDIR_PORT_MSK_FREE_MEM_BLOCK
                                (pIssRedirectPortMaskAndIndex);
                            ISS_REDIR_IFPORT_GRP_FREE_MEM_BLOCK
                                (pIssRedirectIfPortGroup);
                            return SNMP_FAILURE;
                        }
                        for (u1Index = 0; u1Index < u4NoOfRules; u1Index++)
                        {

                            /* For the Redirect Filter Installation 
                             *  of Port / Trunk fill the parameters
                             *  of (Interface Type and Interface IfIndex) */

                            pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                                RedirectIfGrp.u2TunnelIfIndex = 0;

                            pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                                RedirectIfGrp.u1UdbPosition =
                                gpIssRedirectIntfInfo
                                [u4IssRedirectInterfaceGrpId - 1].u1UdbPosition;

                            pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                                RedirectIfGrp.u2TrafficDistByte =
                                u2TrafficDistByte;

                            pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                                RedirectIfGrp.u1TrafficDistMaskLsb =
                                pIssRedirectPortMaskAndIndex[u1Index].
                                u1PortMaskLsb;

                            pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                                RedirectIfGrp.u1TrafficDistMaskMsb =
                                pIssRedirectPortMaskAndIndex[u1Index].
                                u1PortMaskMsb;

                            pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                                RedirectIfGrp.u1TrafficDistValueLsb
                                = pIssRedirectPortMaskAndIndex[u1Index].
                                u1PortValueLsb;

                            pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                                RedirectIfGrp.u1TrafficDistValueMsb
                                = pIssRedirectPortMaskAndIndex[u1Index].
                                u1PortValueMsb;

                            u1ReDirectPortIndex = (UINT1)
                                pIssRedirectPortMaskAndIndex[u1Index].
                                u4PortIndex;

                            pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                                RedirectIfGrp.u4EgressIfIndex =
                                au4PortList[u1ReDirectPortIndex % u4NumPorts];

                            /* Currently MPLS Tunnel Redirection is Not
                             * being done . Only Ethernet Port /Trunk
                             *  is being taken care of */

                            if (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                                RedirectIfGrp.u4EgressIfIndex <=
                                SYS_DEF_MAX_PHYSICAL_INTERFACES)
                            {
                                pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                                    RedirectIfGrp.u1EgressIfType =
                                    ISS_REDIRECT_TO_ETHERNET;

                            }
                            else
                            {
                                pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                                    RedirectIfGrp.u1EgressIfType =
                                    ISS_REDIRECT_TO_TRUNK;
                            }

                            pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                                RedirectIfGrp.u4RedirectGrpId =
                                u4IssRedirectInterfaceGrpId;

                            if (MEMCMP
                                (pIssAclUdbFilterTableEntry->
                                 pAccessFilterEntry->IssUdbFilterInPortList,
                                 gNullPortList,
                                 sizeof (pIssAclUdbFilterTableEntry->
                                         pAccessFilterEntry->
                                         IssUdbFilterInPortList)) != 0)
                            {
                                if (pIssAclUdbFilterTableEntry->
                                    pAccessFilterEntry->RedirectIfGrp.
                                    u1PriorityFlag == ISS_FALSE)
                                {

                                    IssExAddAclToPriorityTable
                                        (pIssAclUdbFilterTableEntry->
                                         pAccessFilterEntry->
                                         u1AccessFilterPriority,
                                         &(pIssAclUdbFilterTableEntry->
                                           IssNextNode), ISS_UDB_REDIRECT);
                                }

                                i4CommitFlag =
                                    IssExGetIssCommitSupportImmediate ();
                                i4CommitAction = IssGetTriggerCommit ();

                                if ((i4CommitFlag == ISS_TRUE) ||
                                    (i4CommitAction == ISS_TRUE))
                                {
#ifdef NPAPI_WANTED
                                    if (ISS_IS_NP_PROGRAMMING_ALLOWED () ==
                                        ISS_TRUE)
                                    {
                                        i4RetVal =
                                            IssHwUpdateUserDefinedFilter
                                            (pIssAclUdbFilterTableEntry->
                                             pAccessFilterEntry, NULL, NULL,
                                             ISS_USERDEFINED_ACCESSFILTER_ADD);
                                        if (i4RetVal != FNP_SUCCESS)
                                        {
                                            ISS_REDIR_PORT_MSK_FREE_MEM_BLOCK
                                                (pIssRedirectPortMaskAndIndex);
                                            ISS_REDIR_IFPORT_GRP_FREE_MEM_BLOCK
                                                (pIssRedirectIfPortGroup);
                                            return SNMP_FAILURE;
                                        }
                                    }

#endif
                                }
                            }

                        }

                        /* Fill the Redirect Group Id also
                         * as it will be used in Kepping Tack
                         * of Same Filter is mapped to this 
                         * Redirect Group . USeful from CLI 
                         * Perpective. From SNMP Perspective
                         * Manager needs to take of ACL-
                         * Redirect Group Mapping */

                        gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].
                            u1RowStatus = ISS_ACTIVE;
                        break;
                    default:
                        ISS_REDIR_PORT_MSK_FREE_MEM_BLOCK
                            (pIssRedirectPortMaskAndIndex);
                        ISS_REDIR_IFPORT_GRP_FREE_MEM_BLOCK
                            (pIssRedirectIfPortGroup);
                        return SNMP_FAILURE;
                }
                ISS_REDIR_PORT_MSK_FREE_MEM_BLOCK
                    (pIssRedirectPortMaskAndIndex);
                ISS_REDIR_IFPORT_GRP_FREE_MEM_BLOCK (pIssRedirectIfPortGroup);
            }
            break;

        case ISS_DESTROY:
#ifdef NPAPI_WANTED
            if ((u1AclIdType == ISS_L2_REDIRECT) &&
                (u1CurrRowStatus == ISS_ACTIVE))
            {
                pIssL2FilterEntry = IssExtGetL2FilterEntry (u4AclId);

                if (pIssL2FilterEntry == NULL)
                {
                    return SNMP_FAILURE;
                }
                if (pIssL2FilterEntry->RedirectIfGrp.u1PriorityFlag ==
                    ISS_FALSE)
                {
                    IssExDeleteAclPriorityFilterTable (pIssL2FilterEntry->
                                                       i4IssL2FilterPriority,
                                                       &(pIssL2FilterEntry->
                                                         IssNextNode),
                                                       ISS_L2_REDIRECT);
                }

                i4CommitFlag = IssExGetIssCommitSupportImmediate ();
                i4CommitAction = IssGetTriggerCommit ();

                if ((i4CommitFlag == ISS_TRUE) || (i4CommitAction == ISS_TRUE))
                {

                    if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                    {
                        i4RetVal =
                            IssHwUpdateL2Filter (pIssL2FilterEntry,
                                                 ISS_L2FILTER_DELETE);
                        if (i4RetVal != FNP_SUCCESS)
                        {
                            return SNMP_FAILURE;
                        }
                    }
                }
            }
            else if ((u1AclIdType == ISS_L3_REDIRECT) &&
                     (u1CurrRowStatus == ISS_ACTIVE))
            {
                pIssL3FilterEntry = IssExtGetL3FilterEntry (u4AclId);

                if (pIssL3FilterEntry == NULL)
                {
                    return SNMP_FAILURE;
                }
#ifdef NPAPI_WANTED
                if (pIssL3FilterEntry->RedirectIfGrp.u1PriorityFlag ==
                    ISS_FALSE)
                {
                    IssExDeleteAclPriorityFilterTable (pIssL3FilterEntry->
                                                       i4IssL3FilterPriority,
                                                       &(pIssL3FilterEntry->
                                                         IssNextNode),
                                                       ISS_L3_REDIRECT);
                }

                i4CommitFlag = IssExGetIssCommitSupportImmediate ();
                i4CommitAction = IssGetTriggerCommit ();

                if ((i4CommitFlag == ISS_TRUE) || (i4CommitAction == ISS_TRUE))

                {

                    if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                    {
                        i4RetVal =
                            IssHwUpdateL3Filter (pIssL3FilterEntry,
                                                 ISS_L3FILTER_DELETE);
                        if (i4RetVal != FNP_SUCCESS)
                        {
                            return SNMP_FAILURE;
                        }
                    }
                }
#endif
            }
            else if ((u1AclIdType == ISS_UDB_REDIRECT) &&
                     (u1CurrRowStatus == ISS_ACTIVE))
            {

                pIssAclUdbFilterTableEntry = IssExtGetUdbFilterTableEntry
                    (u4AclId);

                if (pIssAclUdbFilterTableEntry == NULL)
                {
                    return SNMP_FAILURE;
                }
                if (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                    RedirectIfGrp.u1PriorityFlag == ISS_FALSE)
                {
                    IssExDeleteAclPriorityFilterTable
                        (pIssAclUdbFilterTableEntry->pAccessFilterEntry->
                         u1AccessFilterPriority,
                         &(pIssAclUdbFilterTableEntry->IssNextNode),
                         ISS_UDB_REDIRECT);
                }

                i4CommitFlag = IssExGetIssCommitSupportImmediate ();
                i4CommitAction = IssGetTriggerCommit ();

                if ((i4CommitFlag == ISS_TRUE) || (i4CommitAction == ISS_TRUE))

                {

                    IssHwUpdateUserDefinedFilter (pIssAclUdbFilterTableEntry->
                                                  pAccessFilterEntry, NULL,
                                                  NULL,
                                                  ISS_USERDEFINED_ACCESSFILTER_DELETE);
                }

            }
#endif
            if (gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].
                u1PriorityFlag == ISS_FALSE)
            {
                ISS_MEMSET (&gpIssRedirectIntfInfo
                            [u4IssRedirectInterfaceGrpId - 1], 0,
                            sizeof (tIssRedirectIntfGrpTable));
            }

            break;

        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IssRedirectInterfaceGrpFilterType
 Input       :  The Indices
                IssRedirectInterfaceGrpId

                The Object 
                testValIssRedirectInterfaceGrpFilterType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssRedirectInterfaceGrpFilterType (UINT4
                                            *pu4ErrorCode,
                                            UINT4
                                            u4IssRedirectInterfaceGrpId,
                                            INT4
                                            i4TestValIssRedirectInterfaceGrpFilterType)
{

    if (ISS_IS_REDIRECT_GRP_ID_VALID (u4IssRedirectInterfaceGrpId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1RowStatus
         == ISS_ACTIVE) ||
        ((gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1RowStatus) ==
         0))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssRedirectInterfaceGrpFilterType != ISS_L2_REDIRECT) &&
        (i4TestValIssRedirectInterfaceGrpFilterType != ISS_L3_REDIRECT) &&
        (i4TestValIssRedirectInterfaceGrpFilterType != ISS_UDB_REDIRECT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssRedirectInterfaceGrpFilterId
 Input       :  The Indices
                IssRedirectInterfaceGrpId

                The Object 
                testValIssRedirectInterfaceGrpFilterId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssRedirectInterfaceGrpFilterId (UINT4
                                          *pu4ErrorCode,
                                          UINT4
                                          u4IssRedirectInterfaceGrpId,
                                          UINT4
                                          u4TestValIssRedirectInterfaceGrpFilterId)
{
    if (ISS_IS_REDIRECT_GRP_ID_VALID (u4IssRedirectInterfaceGrpId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1RowStatus
        == ISS_ACTIVE ||
        ((gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1RowStatus) ==
         0))

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4TestValIssRedirectInterfaceGrpFilterId == 0)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssRedirectInterfaceGrpDistByte
 Input       :  The Indices
                IssRedirectInterfaceGrpId

                The Object 
                testValIssRedirectInterfaceGrpDistByte
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssRedirectInterfaceGrpDistByte (UINT4
                                          *pu4ErrorCode,
                                          UINT4
                                          u4IssRedirectInterfaceGrpId,
                                          INT4
                                          i4TestValIssRedirectInterfaceGrpDistByte)
{
    i4TestValIssRedirectInterfaceGrpDistByte = 0;
    if (ISS_IS_REDIRECT_GRP_ID_VALID (u4IssRedirectInterfaceGrpId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1RowStatus
         == ISS_ACTIVE) ||
        ((gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1RowStatus) ==
         0))

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssRedirectInterfaceGrpPortList
 Input       :  The Indices
                IssRedirectInterfaceGrpId

                The Object 
                testValIssRedirectInterfaceGrpPortList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssRedirectInterfaceGrpPortList (UINT4
                                          *pu4ErrorCode,
                                          UINT4
                                          u4IssRedirectInterfaceGrpId,
                                          tSNMP_OCTET_STRING_TYPE
                                          *
                                          pTestValIssRedirectInterfaceGrpPortList)
{
    pTestValIssRedirectInterfaceGrpPortList = NULL;

    if (ISS_IS_REDIRECT_GRP_ID_VALID (u4IssRedirectInterfaceGrpId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1RowStatus
         == ISS_ACTIVE) ||
        ((gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1RowStatus) ==
         0))

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Currently Port or Trunk will be be taking care as Port List 
     *  is availbalr for it */

    /* This function checks if any port greater than the maximum number of
     * ports in the system is in the port list.*/
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssRedirectInterfaceGrpType
 Input       :  The Indices
                IssRedirectInterfaceGrpId

                The Object 
                testValIssRedirectInterfaceGrpType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssRedirectInterfaceGrpType (UINT4
                                      *pu4ErrorCode,
                                      UINT4
                                      u4IssRedirectInterfaceGrpId,
                                      INT4 i4TestValIssRedirectInterfaceGrpType)
{
    if (ISS_IS_REDIRECT_GRP_ID_VALID (u4IssRedirectInterfaceGrpId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1RowStatus
         == ISS_ACTIVE) ||
        ((gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1RowStatus) ==
         0))

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssRedirectInterfaceGrpType != ISS_REDIRECT_TO_PORT) &&
        (i4TestValIssRedirectInterfaceGrpType != ISS_REDIRECT_TO_PORTLIST))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;

    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssRedirectInterfaceGrpUdbPosition
 Input       :  The Indices
                IssRedirectInterfaceGrpId

                The Object 
                testValIssRedirectInterfaceGrpUdbPosition
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssRedirectInterfaceGrpUdbPosition (UINT4 *pu4ErrorCode,
                                             UINT4 u4IssRedirectInterfaceGrpId,
                                             INT4
                                             i4TestValIssRedirectInterfaceGrpUdbPosition)
{
    if (ISS_IS_REDIRECT_GRP_ID_VALID (u4IssRedirectInterfaceGrpId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1RowStatus
         == ISS_ACTIVE) ||
        ((gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1RowStatus) ==
         0))

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssRedirectInterfaceGrpUdbPosition < 0) ||
        (i4TestValIssRedirectInterfaceGrpUdbPosition > ISS_UDB_MAX_OFFSET))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;

    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssRedirectInterfaceGrpStatus
 Input       :  The Indices
                IssRedirectInterfaceGrpId

                The Object 
                testValIssRedirectInterfaceGrpStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssRedirectInterfaceGrpStatus (UINT4
                                        *pu4ErrorCode,
                                        UINT4
                                        u4IssRedirectInterfaceGrpId,
                                        INT4
                                        i4TestValIssRedirectInterfaceGrpStatus)
{
    if (ISS_IS_REDIRECT_GRP_ID_VALID (u4IssRedirectInterfaceGrpId) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].u1RowStatus
         != ISS_ACTIVE) &&
        (i4TestValIssRedirectInterfaceGrpStatus == ISS_NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].
         u1RowStatus == 0x0) &&
        ((i4TestValIssRedirectInterfaceGrpStatus != ISS_CREATE_AND_WAIT)
         && (i4TestValIssRedirectInterfaceGrpStatus != ISS_CREATE_AND_GO)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValIssRedirectInterfaceGrpStatus == ISS_ACTIVE)
    {
        if (gpIssRedirectIntfInfo[u4IssRedirectInterfaceGrpId - 1].
            u4AclId == 0x0)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IssRedirectInterfaceGrpTable
 Input       :  The Indices
                IssRedirectInterfaceGrpId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IssRedirectInterfaceGrpTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIssRedirectInterfaceGrpIdNextFree
 Input       :  The Indices

                The Object
                retValIssRedirectInterfaceGrpIdNextFree
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssRedirectInterfaceGrpIdNextFree (UINT4
                                         *pu4RetValIssRedirectInterfaceGrpIdNextFree)
{

    IssInterfaceGrpRedirectIdNextFree
        (pu4RetValIssRedirectInterfaceGrpIdNextFree);
    return SNMP_SUCCESS;
}
