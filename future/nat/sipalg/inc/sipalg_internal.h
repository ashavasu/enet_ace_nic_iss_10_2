/********************************************************************
 * Copyright (C) Aricent Communications Sotware,2007
 *
 * $Id: sipalg_internal.h,v 1.3 2011/10/25 09:50:06 siva Exp $
 *
 * Description:This file contains the translation function's prototypes for
 *              SIP Msg.
 *******************************************************************/

/***********************************************************************
 ** FUNCTION:
 **             Init function prototypes for all structures
 **
 ***********************************************************************
 **
 ** FILENAME:
 ** sipalg_inc.h
 **
 ** DESCRIPTION: This file contains the prototypes for functions
 **     used by SIP ALG which interact with NAT and Microstack 
 **
 ** DATE        NAME           REFERENCE       REASON
 ** ----        ----           ---------       --------
 ** 26/07/2007  ALG Team               -        Initial Creation
 **
 **  Copyright (C) 2006 Aricent Inc . All Rights Reserved
 **********************************************************************/

#ifndef __SIPALG_INTERNAL_H_
#define __SIPALG_INTERNAL_H_

#include "sipalg_defs.h"
#include "sipalg.h"
#include "natinc.h"

extern tSipSignalPort          gSipSignalPort;
extern INT4                    gi4NatSipAlgPartialEntryTimer;
/* Global array having interface specific information */
extern sipAlgInterfaceInfo    dGlbInterfaceInfo[MAX_WAN_LINKS];

/* global interface index */
extern SIP_U16bit dGlbInterfaceIndex; 

/* Global structure for the sipalg port information */

extern sipAlgSipPortInfo   dGlbSipAlgPortInfo;

/****************************************************************************
* Function Name : sipAlgPerformTranslation
* Description     : This function is invoked to modify the IP address and Port
*                  for all the headers present in SIP message. In case mapping 
*                  is available in NAT hash table, the entry is returned. 
*                  Otherwise a new binding is created in NAT. This function
*                  is also used to translated media entry. 
* Input/Output (s) :pNatInfo - The SIP header IP and Port will be passed 
*                        here, which is translated
*                    protocol - UDP or TCP
*                    isMediaElement - Indicates if the SIP request is for media
*                        or for signaling.
*                    pMediaKey - Media key
*                    ifIndex - The WAN Interface index
* Returns    : SUCCESS/FAILURE
****************************************************************************/
SipBool 
sipAlgPerformTranslation (SipNatInfo *pNatInfo, en_Protocol protocol, 
    en_SipBoolean isMediaElement,SipAlgMediaKey *pMediaKey, SIP_U32bit ifIndex);

/****************************************************************************
* Function Name : sipAlgUpdateMediaInfoInNatHash
* Description     : This function is invoked so as to create an entry for the 
*                  IP address and port mentioned in the media line. Both 
*                  for the incoming and outgoing direction
* Input/Output (s) :pNatInfo - The SIP media IP and Port will be passed 
*                        here, which is translated
*                    pMediaKey - The session key is added in the list 
*                        maintained in NAT hash element
* Returns    : SUCCESS/FAILURE
****************************************************************************/
SipBool 
sipAlgUpdateMediaInfoInNatHash (SipNatInfo *pNatInfo, 
    SipAlgMediaKey *pSessionKey);

/****************************************************************************
* Function Name : sipAlgDeleteMediaInfoFromNATHash
* Description     : This function is invoked on receiving a BYE message or a 
*                  failure response for INVITE. In either case, clear the 
*                  entries that are present
* Input/Output (s) : pKey - The IP and Port entry key 
*                     isTransKey - Whether the NAT hash entry for media is
*                     mapped IP/Port or not
* Returns    : VOID
****************************************************************************/
void 
sipAlgDeleteMediaInfoFromNATHash (SipAlgNatHashKey *pKey, en_SipBoolean isTransKey, en_SipBoolean isDeleteDynamicEntry);

/****************************************************************************
* Function Name : sipAlgUpdateMediaKeyInNatHash
* Description     : When the INVITE request gets a response, the To tag is 
*                  added in the dialog parameter. So a new entry is created in
*                  Medai hash table. This dialg parameter is then added in
*                  Nat hash table for the corresponding IP/Port entry of the 
*                  media. Both for incoming and outgoing direction 
* Input/Output (s) :pKey - This will map to the IP address and Port of the
*                        media for which dialog key needs to be added
*                    pMediaKey - This dialog key will be appended in the list
* Returns    : SUCCESS/FAILURE
****************************************************************************/
SipBool 
sipAlgUpdateMediaKeyInNatHash (SipAlgNatHashKey  *pKey, 
    SipAlgMediaKey *pMediaKey);

/****************************************************************************
* Function Name : sipAlgUpdateRegContactInfo
* Description     : This is the function which will set the SIP ALG timer  
*                  for the contact information present in NAT hash table.
*                 
* Input/Output (s) :pNatInfo - The contact's translated IP will be passed 
*                        here, which is mapped back to the private address
*                    expiryValue - SIP ALG timer will be started for this
*                        duration of seconds 
* Returns    : SUCCESS/FAILURE
****************************************************************************/
SipBool  
sipAlgUpdateRegContactInfo (SipNatInfo *pNatInfo,SIP_U32bit expiryValue, SIP_U32bit ifIndex,
                            sipAlgError *pAlgError, SIP_S8bit * pAORString);

/****************************************************************************
* Function Name : sip_initNatElement
* Description     : Initialize the Nat Hash table for a new entry.
*                 
* Input/Output (s) :  ppNatHashElement - The element to be initialed
*                      pKey - Key for NAT hash table    
*                      isMediaElement - Whether the element created is for
*                      media or a contact            
* Returns    : SUCCESS/FAILURE
****************************************************************************/
SipBool 
sipAlgInitNatHashElement (SipAlgNatHashElement **ppNatHashElement, 
    SipAlgNatHashKey *pKey, en_SipBoolean isMediaElement,\
    en_SipBoolean dIsTimerReqd);


/****************************************************************************
* Function Name : sipAlgTimerExpCbkFunc
* Description     : On SIP ALG timer expiry this function is invoked. Based on
*                  this either close the pinhole or bindings
*                 
* Input/Output (s) :  pSipAlgTimer - This object is returned by NAT timer
* Returns    : Nothing
****************************************************************************/
void sipAlgTimerExpCbkFunc (tNatSipAlgTimer *pSipAlgTimer);

/****************************************************************************
* Function Name : sipAlgHandleCreatedDynEntry
* Description     : Once a dynamic entry is created in NAT corresponding to a
*                  partial entry, this callback function is invoked by NAT.
*                  The same function is invoked even in case of dynamic entry
*                  being created because of pinhole or because of NAT partial
*                  entry
*                 
* Input/Output (s) : natEntry - This object is returned by NAT
* Returns    : VOID
****************************************************************************/
void sipAlgHandleCreatedDynEntry (tNatEntryInfo natEntry);

/****************************************************************************
* Function Name : sipAlgNatNotificationCbkFunc
* Description     :  This is the callback function which will be registered from 
*    the SIP ALG to the NAT module. When the NAT Idle timer expires for any 
*     binding and pinhole, it will be notified to the SIP ALG through this 
*    function only. SIP ALG will clear of the details maintained in its scope 
*    using the information provided by the NAT module. Register the NAT 
*     Notification Function from SIP ALG to NAT module. This function is 
*    invoked to clear the NAT hash entry (NAT idle timer is expired)
* Input/Output (s) : pNatEntry - Passed by NAT
* Returns    : VOID
****************************************************************************/


/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:     sipAlgUpdateMediaInfo */
/*  */
/*  DESCRIPTION:  */
/*                   This is the function which will update the media */
/*                   information in the ALG Hash Table as well as NAT Hash */
/*                   Table maintained in ALG. It will also invoke the NAT API  */
/*                   to create the bindings and the pinholes for it. */
/* //////////////////////////////////////////////////////////////////////////// */
SipBool  sipAlgUpdateMediaInfo(
                    SipAlgMediaKey *pDialogParams,
                    SipNatInfo **ppNatInfo,
                    en_SipAValueInSdp a_line_attribute);

/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:     sipAlgUpdateMediaTables */
/*  */
/*  DESCRIPTION:  */
/*                   This is the function which will update the media */
/*                   information in the ALG Hash Table. */
/* //////////////////////////////////////////////////////////////////////////// */
 SipBool  sipAlgUpdateMediaTables (
                    SipAlgMediaKey *pDialogParams,
                    SipNatInfo *pNatInfo,
                    en_SipAValueInSdp    a_line_attribute);

/* /////////////////////////////////////////////////////////////////// */
/*         sipAlgAddMediaElementInMediaList     */
/*  */
/* ///////////////////////////////////////////////////////////////// */
SipBool        sipAlgAddMediaElementInMediaHash(SipNatInfo *pNatInfo,\
        SipMediaHashElement *pMediaHashElement,\
        SipAlgMediaKey    *pKey,en_SipBoolean dIsHashAdd);

/* /////////////////////////////////////////////////////////////////// */
/*             sipAlgDeleteMediaElement */
/*  */
/* ///////////////////////////////////////////////////////////////// */
SipBool sipAlgDeleteMediaElement(\
        SipAlgMediaKey *pKey,
        SipAlgIpPort    *pIpPort);

/* ///////////////////////////////////////////////////////////////////////// */
/*     Function Name : sipAlgCreatePartialEntryInNat  */
/*  */
/* Description    : */
/*     This is the explicit  function which is internally used by the SIP ALG to */
/* create the partial Bindings and the pinholes information on NAT. */
/* This will be used only by the SDP parameters in INVITE request and */
/* responses. */
/* ///////////////////////////////////////////////////////////////////////// */

SipBool sipAlgCreatePartialEntryInNat(\
        SipNatInfo **ppNatInfo,
        en_SipAValueInSdp dirAttr,SipAlgMediaKey *pMediaKey);

/* ///////////////////////////////////////////////////////////////////////// */
/*     Function Name : sipAlgDeleteNatBindingsNPinholes */
/*  */
/* Description    : */
/*     This is the explicit  function which is internally used by the SIP ALG to */
/* delete the Bindings and the pinholes. This will be used only by the SIP */
/* signalling request message. ie. BYE / CANCEL request. */
/* ///////////////////////////////////////////////////////////////////////// */
SipBool    sipAlgDeleteNatBindingsNPinholes(\
        SipAlgMediaKey *pDialogParams);


/****************************************************************************
* Function Name : sipAlgFreeDestList
* Description     : This is the function registered at the time of list init
*    Once list element is deleted, this function gets called (callback)
*                 
* Input/Output (s) :  pDestElement - The element in the list
* Returns    : Nothing
****************************************************************************/
void (sipAlgFreeDestList)(SIP_Pvoid pDestElement);

/****************************************************************************
* Function Name : sipAlgFreeCallList
* Description     : This is the function registered at the time of list init
*    Once list element is deleted, this function gets called (callback)
*                 
* Input/Output (s) :  pCallElement - The element in the list
* Returns    : Nothing
****************************************************************************/
void (sipAlgFreeCallList)(SIP_Pvoid pCallElement);

void (sipAlgFreeMediaInfoList)(SIP_Pvoid abc);
 
/****************************************************************************
* Function Name : sipAlgFreeIpPortList
* Description     : This is the function registered at the time of list init
*    Once list element is deleted, this function gets called (callback)
*                 
* Input/Output (s) :  pIpPort - The element in the list
* Returns    : Nothing
****************************************************************************/
void (sipAlgFreeIpPortList)(SIP_Pvoid pIpPort);

/****************************************************************************
* Function Name : sipAlgInitMediaHashElement
* Description     : Initialize the Media Hash table for a new entry.
*                 
* Input/Output (s) :  ppNatHashElement - The element to be initialed
*                      pKey - Key for NAT hash table    
* Returns    : SUCCESS/FAILURE
****************************************************************************/
SipBool 
sipAlgInitMediaHashElement (SipMediaHashElement **ppMediaHashElement);

/****************************************************************************
* Function Name : sipAlgDeleteDynamicEntry
* Description     : This function is invoked to clear the NAT hash entry 
*                  (NAT idle timer is expired)
* Input/Output (s) : pNatHashElement- This element has the corresponding dyn
*                    entry.
*                    pKey - Key for the hash element
*                    pNatEntry - Passed by NAT
* Returns    : VOID
****************************************************************************/
void 
sipAlgDeleteDynamicEntry(SipAlgNatHashElement *pNatHashElement, 
    SipAlgNatHashKey *pKey, tNatEntryInfo *pNatEntry);

/****************************************************************************
* Function Name : sipAlgHandleDeletedDynEntry
* Description     : This function is invoked to clear the entry becasue
*                  NAT idle timer is expired.
* Input/Output (s) : natEntry- This is passed by NAT
* Returns    : SUCCESS/FAILURE
****************************************************************************/
void 
sipAlgHandleDeletedDynEntry (tNatEntryInfo natEntry);

/* function for sip listen port change notification */


/* function for global nat disable notification */


/****************************************************************************
* Function Name : sipAlgAddPortMapping
* Description     : This function is invoked at the time of initialization
*    if B2B server is running along with CAS. That time, one binding 
*    will be created for the IP/Port of B2B for which there will
*     be no timer running. This binding will be used for all singaling
*     entries.
* Input/Output (s) : b2bIP - This is the VLAN IP from where B2B send pkt
*                    ifIndex - The WAN interface index 
* Returns    : SUCCESS/FAILURE
****************************************************************************/
SipBool sipAlgAddPortMapping(SIP_U32bit b2bIP, SIP_U32bit ifIndex,\
                         SIP_U32bit natIP,en_SipBoolean isNatEnabled);

/* //////////////////////////////////////////////////////////////////////////// */
/* FUNCTION:     sipAlgDeInitOnInterfaceChange */
/*  */
/*  DESCRIPTION: Function is to de-initialize all the data structure used by */
/*  the SIP ALG for the given interface. Basically it clears the entries */
/*  from NAT and media hash for that interface. No deletion of the hash table */
/*  is done. It is used when the WAN IP is changed.  */
/* //////////////////////////////////////////////////////////////////////////// */

void sipAlgDeInitOnInterfaceChange(tLinkInfo linkInfo);

SipBool sipAlgDeleteMediaOnInterfaceChange(SipAlgMediaKey *pMediaKey, \
                      SipList *pMediaIpPortList);

SipBool
sipAlgDeletePortMappingNPinhole (SipAlgNatHashElement *pNatHashElement);

/* SIPALG - 2.1 */
/****************************************************************************
* Function Name : sipAlgPerformIPTranslation
* Description     : This function is invoked to modify the IP address 
*                  for all the headers present in SIP message in case there is
*                  no port present. Basically for the handling of session level
*                  c line and translation of o-line in SDP body. In case mapping 
*                  is available in NAT hash table, the entry is returned. 
*                  Otherwise a new entries are created in NAT hash table. 
*                  None of the entries are entered in actual NAT/Firewall. These
*                  values are maintained only in SIP ALG. This function will
*                  be invoked only when NAT is enabled.
* Input/Output (s) :pNatInfo - The SIP header IP and Port (zero) will be passed 
*                        here, which is translated. This varible when passed 
*                        should be filled with both private IP and mapped IP.
*                    pMediaKey - The media key
*                    pVersionId - Version Id from O-line 
*                    isRequest - Whether the SIP message is a request or resp
* Returns    : SUCCESS/FAILURE
****************************************************************************/
SipBool sipAlgPerformIPTranslation (SipNatInfo *pNatInfo, 
    SipAlgMediaKey *pMediaKey, SIP_S8bit *pVersionId,
    en_SipBoolean isRequest);

/****************************************************************************
* Function Name : sipAlgInitIPHashElement
* Description     : Initialize the IP Hash table for a new entry.
*                 
* Input/Output (s) :  ppIPHashElement - The element to be initialed
*                      pKey - Key for IP hash table    
* Returns    : SUCCESS/FAILURE
****************************************************************************/
SipBool 
sipAlgInitIPHashElement (SipAlgIPHashElement **ppIPHashElement,\
    SipAlgIPHashKey *pKey);

/****************************************************************************
* Function Name : sipAlgDeleteContactsOnDeRegister
* Description     :Update the REGHash table by setting isExpired to TRUE  
*                  corresponding to the all contacts for  this AOR
* Input/Output (s) :  pAORString - "user@host"
*                     wildcard   - 
* Returns    : SUCCESS/FAILURE
****************************************************************************/

SipBool
sipAlgDeleteContactsOnDeRegister(SIP_S8bit * pAORString);

/****************************************************************************
* Function Name : sipAlgAddContactToDeRegHash
* Description     :Hash fetch Dereg Hash (key = pAORString) and Get the ip /port for which the 
*      expiry has happend. Now do hash fetch to NAT hash with this ip/port & reset
*      the alg timer for this =1 secs so that it expitres soon.  
* Input/Output (s) :  pAORString - "user@host"
*                     ipaddr - ip address of contact
*                     port   - port of contact
*                     ifindex - interface index
* Returns    : SUCCESS/FAILURE
****************************************************************************/
SipBool
sipAlgAddContactToDeRegHash 
(SIP_S8bit * pAORString, SIP_U32bit ipaddr, \
SIP_U16bit port, SIP_U32bit ifindex);

/****************************************************************************
* Function Name : sipAlgDeleteDeRegOnParExp
* Description     :Hash fetch Dereg Hash (key = pAORString) and delete the entry 
* which matches with the provided dPvtIP and dPvtPort. This is called when partial
* expiry has happened for this pIP and port.
* Input/Output (s) :  pAORString - "user@host"
*                  Pvt ip and port for which partial timer has expired.
* Returns    : SUCCESS/FAILURE
****************************************************************************/
SipBool
sipAlgDeleteDeRegOnParExp ( SIP_S8bit * pAORString,\
                  SIP_U32bit dPvtIP, SIP_U16bit dPvtPort);

/****************************************************************************
* Function Name : sipAlgUpdateExpiredContacts
* Description     : isexpired field is set to true fo rthe contacts which are expired 
* Input/Output (s) :  pAORString - "user@host"
*                     isWildcard - is it wild card or not in contact- 
*                                  (expires all contacts)
*                     ipaddr - ip address of contact
*                     port   - port of contact
*                     ifindex - interface index
* Returns    : SUCCESS/FAILURE
****************************************************************************/
void 
sipAlgUpdateExpiredContacts 
(SIP_S8bit *pAORString, en_SipBoolean isWildcard,\
 SIP_U32bit ipaddr, SIP_U16bit port);

/****************************************************************************
* Function Name : sipAlgFreeDeRegInfoList
* Description     : This is the function registered at the time of list init
*    Once list element is deleted, this function gets called (callback)
*                 
* Input/Output (s) :  pDeRegElement - The element in the list
* Returns    : Nothing
***************************************************************************/
void                (sipAlgFreeDeRegInfoList) (SIP_Pvoid pDeRegElement);


/****************************************************************************
* Function Name : sipAlgInitDeRegHashElement
* Description     : Initialize the DeReg Hash table for a new entry.
*                 
* Input/Output (s) :  ppNatHashElement - The element to be initialed
*                      pKey - Key for NAT hash table    
* Returns    : SUCCESS/FAILURE
****************************************************************************/
SipBool
sipAlgInitDeRegHashElement (SipAlgDeRegHashElement** ppDeRegHashElement);

VOID
PrintSipParitalEntry (tCliHandle CliHandle,
                      tNatPartialLinkNode * pNatPartialLinkNode);

#endif
