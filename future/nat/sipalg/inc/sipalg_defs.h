/********************************************************************
 * Copyright (C) Aricent Communications Sotware,2007
 *
 * $Id: sipalg_defs.h,v 1.3 2014/01/25 13:52:25 siva Exp $
 *
 *******************************************************************/

/***********************************************************************
 ** FUNCTION:
 **             Init function prototypes for all structures
 **
 ***********************************************************************
 **
 ** FILENAME:
 ** sipalg_defs.h
 **
 ** DESCRIPTION: This file contains the data structure typedefs
 **     used by SIP ALG
 **
 ** DATE        NAME           REFERENCE       REASON
 ** ----        ----           ---------       --------
 ** 26/07/2007  ALG Team               -        Initial Creation
 **
 **  Copyright (C) 2006 Aricent Inc . All Rights Reserved
 **********************************************************************/

#ifndef __SIPALG_DEFS_H_
#define __SIPALG_DEFS_H_

#include "natinc.h"

#define MAX_WAN_LINKS    64

/* This should match to tHeaderInfo for the direction */
typedef enum SipAlgPktInOut    
{
  SipPktIn = NAT_INBOUND,
  SipPktOut = NAT_OUTBOUND
} SipAlgPktInOut ;

/* lakshmi added - NAT API Enhancements -START */
/* Make use of NAT parity */
/*Modified to pass parity to NAT */
typedef enum
{
    en_parityAny=0,
    en_parityEven=1,
    en_parityOdd=2,

}en_ParityOfPort;
/*NAT API Enhancements - END */

/* rasheed-fix : enum for getusertype */
typedef enum
{
  en_fail=0,
  en_lan=1,
  en_wan=2,
  en_ravpn=3,
  en_sitesitevpn=4,
}en_IpAddrType;

/* rasheed - for sipserver status */
typedef enum
{
  en_sipEnable = 1,
  en_sipDisable=2
}en_sipStatus;

typedef enum
{
  sipAlgNoError=0,
  sipAlgHashEntryNotFound=1 
} sipAlgError;

typedef struct sipAlgInterfaceInfo
{
    /* Interface index */
    SIP_U32bit ifIndex;
    /* Boolean indicating whether there are signaling entries 
       already created or not */
    en_SipBoolean isSignalingEntryCreated;
    /* The ip associated with this interface */
    SIP_U32bit dWanIP;
    /* interface name */
    SIP_U8bit dInterfaceName[10] ;
    SIP_U8bit dpad[2];
}sipAlgInterfaceInfo;

/* sipalg port info */
typedef struct sipAlgSipPortInfo
{
  SIP_U32bit dIPAddress;
  SIP_U32bit ifIndex;
  SIP_U16bit dSipAlgPort;
  SIP_U8bit dpad[2];  
} sipAlgSipPortInfo;
/* -------------------------------------------------------------------- */
/*  SipNatInfo : */
/*              This is the data structure defined by the ALG to store the */
/*              NAT information. It groups all the information which are */
/*              commonly used by both media hash table as well as NAT */
/*              Hash Table maintained in the ALG.It contains private IP and  */
/*             port, Translated IP and port and Outside IP and port. */
/* -------------------------------------------------------------------- */
typedef struct SipNatInfo
{
    /*  Private IP */
    SIP_U32bit        privateIP;

    /*  Mapped or translated IP */
    SIP_U32bit        mappedIP;

    /*  Outside IP */
    SIP_U32bit        outsideIP;

    /*  Mapped or translated port */
    SIP_U16bit        mappedPort;

    /*  Private port */
    SIP_U16bit        privatePort;

    /*  Outside port */
    SIP_U16bit        outsidePort;

    SIP_U8bit        pad[2];

    /*  Tells the direction of the packet */
    SipAlgPktInOut    direction;

    /*  The WAN interface index */
    SIP_U32bit         ifIndex;

    /*lakshmi added - NAT API Enhancements - START */
    en_ParityOfPort parity;
    /* NAT API Enhancements - END */
}SipNatInfo;

/* -------------------------------------------------------------------- */
/* SipAlgNatHashKey: */
/*         This is the data structure used as a key for the Nat */
/*         Hash Table     maintained in the ALG. It contains IP address and port  */
/*         as a key. */
/* -------------------------------------------------------------------- */
typedef struct SipAlgNatHashKey
{
    /*  If the message is from public end, then the IP is translated IP. */
    /*  Otherwise it is private IP. */
    SIP_U32bit    ipAddress;

    /*  If the message is from public end, then port is translated port.  */
    /*  Otherwise it is private port. */
    SIP_U16bit    port;

    /* SIPALG - 2.1 */
    /* 
        This is introduced to handle scenarios where we need the 
        translation of IP address (but there is no port available)
        such as translation of o-line, trans of session/media level
        c line where when port is zero. That time the hash key will
        be based on Call ID
        isMediaPortZero = True indicates that Call ID will be used.
    */
    SIP_U8bit    pad[2];

    en_SipBoolean isMediaPortZero;

    /*  Call ID of the dialog */
    SIP_S8bit    *pCallID;

    SIP_S8bit    *pVersionId;

    /*  This indicates the message type */
    en_SipBoolean isRequest;

    /*  Padding */
    /*SIP_U8bit        u1Pad[2];

     Based on direction the key used in hash comparison will differ. 
       For an outgoing packet, the hash key will be combination of IP
       address, port and interface index. But for an incoming packet
       the hash key will be ip address and port */
    SipAlgPktInOut    direction;
    /* WAN interface index */
    SIP_U32bit    ifIndex;

} SipAlgNatHashKey;

typedef struct SipAlgIpPort
{
  SIP_U32bit    ipAddress;
  SIP_U16bit    port;
  SIP_U8bit    pad[2];
}SipAlgIpPort ;

/* -------------------------------------------------------------------- */
/* SipAlgIpPortPair: */
/*         This is the data structure that will be stored in NAT hash as */
/*         the list of destination IP/port. In case the packet flows from  */
/*         inside to outside, the dest IP will be outside network. In case */
/*         the packet flows from outside to inside, the destination  will  */
/*         be private IP/port */
/* -------------------------------------------------------------------- */
typedef struct SipAlgIpPortPair
{
    /*  Private or Outside IP based on direction */
    SIP_U32bit    destIP;
    /*  Translated IP */
    SIP_U32bit    mappedIP;
    /*  Private or outside port based on direction */
    SIP_U16bit    destPort;
    /*  Translated port */
    SIP_U16bit    mappedPort;

}SipAlgIpPortPair;

/* -------------------------------------------------------------------- */
/* tNatSipAlgTimer: */
/*         This is the data structure maintained in hash table, so that on */
/*         expiry of application timer, appropriate action can be taken place. */
/* -------------------------------------------------------------------- */
typedef struct tNatSipAlgTimer 
{
    /*  Timer node passed to timer component */
    tTmrAppTimer    TimerNode;

    /*  This will be the key, which can be used to delete the  */
    /*  SipAlgNatHashElement after ALG timer expires      */
    SipAlgNatHashKey *pHashKey;

  /*used for removing the DeReg has entries of this AOR for */
  /*the IP/port wich has got partial timer expired*/
    SIP_S8bit *pDeRegAOR;

    SIP_U8bit        u1TimerStatus;
    /*  Padding */
    SIP_U8bit        u1Pad[3];
}tNatSipAlgTimer;


/* -------------------------------------------------------------------- */
/* SipAlgNatHashElement: */
/*         This is the data structure which the Nat hash table maintains    */
/* -------------------------------------------------------------------- */
typedef struct SipAlgNatHashElement
{
    
    /*  If the message is from public end, then the IP is translated IP. */
    /*  Otherwise it is private IP. */
    SIP_U32bit        originalIP;

    /*  If the message is from public end, then port is translated port.  */
    /*  Otherwise it is private port. */
    SIP_U16bit        originalPort;

    /*  Protocol - UDP or TCP */
    SIP_U16bit        protocol;

    /*  List of destination IP and Port */
    SipList            *pDestInfoList;

    /*  Sip Alg timer information */
    tNatSipAlgTimer        *pSipAlgTimerInfo;

    /*  Is the entry for REGISTER message or for media */
    en_SipBoolean         isRegTimerStarted;

    /*  List of ongoing calls - for media information */
    SipList         *pCallInfoList;

    /*  This will indicate whether the element is for media or signaling */
    en_SipBoolean    isMediaElement;

    /*  This will indicate whether the binding is created or pinhole */
    en_SipBoolean    isBindingCreated;

    /*  The status indicating whether NAT is enabled or not */
    en_SipBoolean     isNatEnabled;    

        /* Flag to check whether this is a b2b signalling entry */
         en_SipBoolean   isB2bSingallingEntry;    

    /*  The WAN interface index */
    SIP_U32bit         ifIndex;

    /*lakshmi  added - NAT API Enhancements - START*/
    /*Added to store parity which can be used to specify the parity
    while deleting the binding also*/
    en_ParityOfPort parity; 
    /* NAT API Enhancements - END */
    
    /*  The status indicating whether NAT Partial element is deleted or not */
 en_SipBoolean  isNatPartialEntryDeleted; 


} SipAlgNatHashElement;


/* Make use of NAT port */
typedef enum
{
    en_any = 0,
    en_tcp=1,
    en_udp=2
        
}en_Protocol;


/* 'a' value in SDP parameter */
typedef enum     
{
    /* SDP a line is inactive */
    sendrecv = 0,
    /* SDP a line is send only */
    sendonly,
    /* SDP a line is recv only */
    recvonly,
    /* SDP a line is send and recv */
    inactive,

} en_SipAValueInSdp;      

/* lakshmi  added - NAT API Enhancements - START*/
/*Added as part of NAT API Enhancements to support multiple signaling
 * entries */
typedef enum
{
    /* binding for signaling port */
    en_signaling = 0,
    /* binding is for media */
    en_media

} en_BindingType;
/* NAT API Enhancements - END */


/* -------------------------------------------------------------------- */
/* SipAlgMediaKey: */
/*         This is the data structure used as a key for the Media Hash Table  */
/*         maintained in the ALG. It contains From Tag, To Tag and Call-ID  */
/*         as a key. */
/* -------------------------------------------------------------------- */
typedef struct SipAlgMediaKey
{
    /* From Tag in SIP Message */
    SIP_S8bit    *pFromTag;

    /* To Tag in SIP Message */
    SIP_S8bit    *pToTag;

    /* CAll-ID in SIP Message */
    SIP_S8bit    *pCallID;

} SipAlgMediaKey;

/* ---------------------------------------------------------- */
/*  SipAlgMediaElement: */
/*  This is the data structure that contains the IP address  */
/*  and port which needs ALG operation in message body */
/* ---------------------------------------------------------- */
typedef struct SipAlgMediaElement
{
      /*  IP address  */
      SIP_U32bit    ipAddress;
    /*  Port  */
    SIP_U16bit    port;
    /*  RefCount */
    SIP_U16bit    refCount;
    /*  Interface index */
    SIP_U32bit    ifIndex;
       /* Based on direction the key used in hash comparison will differ. 
       For an outgoing packet, the hash key will be combination of IP
       address, port and interface index. But for an incoming packet
       the hash key will be ip address and port */
    SipAlgPktInOut    direction;
    
} SipAlgMediaElement;


/* -------------------------------------------------------------------- */
/*  SipMediaHashElement : */
/*              This is the data structure which defines the data attribute */
/*              needed by the Media Hash element.This contains the list of the  */
/*              media level information .It is used by ALG in the Media hash  */
/*             table. */
/* -------------------------------------------------------------------- */
typedef struct SipMediaHashElement
{
    /* List of  SipAlgMediaElement  */
    SipList  *pMediaInfoList;
    
}SipMediaHashElement;


/* SIPALG - 2.1 */
/* -------------------------------------------------------------------- */
/* SipAlgIPHashKey: */
/*         This is the data structure used as a key for the IP */
/*         Hash Table maintained in the ALG. Hash table contains the private */
/*         IP and mapped IP. But the key for the hash table is call ID. */
/* -------------------------------------------------------------------- */
typedef struct SipAlgIPHashKey
{
    /*  Call ID of the dialog */
    SIP_S8bit    *pCallID;

    SIP_S8bit    *pVersionId;

    en_SipBoolean    isRequest;

} SipAlgIPHashKey;

/* -------------------------------------------------------------------- */
/* SipAlgIPHashElement: */
/*         This is the data structure which the IP hash table maintains    */
/* -------------------------------------------------------------------- */
typedef struct SipAlgIPHashElement
{
    /*  Contains the private IP */
    SIP_U32bit        privateIP;

    /*  Contains the mapped IP */
    SIP_U32bit        mappedIP;

    /*  Sip Alg timer information  */
    tNatSipAlgTimer        *pSipAlgTimerInfo;
}SipAlgIPHashElement;

/* De-Register Hash Key */
typedef struct SipAlgDeRegHashKey
{
    /*  AOR of the user */
    SIP_S8bit    *pAOR;

} SipAlgDeRegHashKey;

typedef struct SipAlgDeRegHashElement
{
/* List of  SipAlgDeRegElement  */
    SipList  *pDeRegInfoList;
}SipAlgDeRegHashElement;

/* De-Register element */
typedef struct SipAlgDeRegElement
{
    /*  If the message is from public end, then the IP is translated IP. */
    /*  Otherwise it is private IP. */
    SIP_U32bit    ipAddress;

    /*  If the message is from public end, then port is translated port.  */
    /*  Otherwise it is private port. */
    SIP_U16bit    port;
    SIP_U8bit    pad[2];
    
    /* direction is always out- so not needed*/
/*    SipAlgPktInOut    direction;*/

    /* WAN interface index */
    SIP_U32bit    ifIndex;

    /* de-registration flag */
    en_SipBoolean isExpired;

} SipAlgDeRegElement;

/*Structure to store signalling entry's destination info*/
typedef struct sipAlgSignalDestInfo
{
    en_SipBoolean     isAddPortMappingCalled;
    SIP_U32bit        dOutIPAddress;
    SIP_U16bit        dOutPort;
    SIP_U8bit    pad[2];
}sipAlgSignalDestInfo;

#endif /* __SIPALG_DEFS_H_ */

