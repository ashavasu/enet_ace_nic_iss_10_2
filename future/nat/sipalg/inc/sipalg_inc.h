/********************************************************************
 * Copyright (C) Aricent Communications Sotware,2007
 *
 * $Id: sipalg_inc.h,v 1.3 2014/01/25 13:52:25 siva Exp $
 *
 *******************************************************************/

/***********************************************************************
 ** FUNCTION:
 **             Init function prototypes for all structures
 **
 ***********************************************************************
 **
 ** FILENAME:
 ** sipalg_inc.h
 **
 ** DESCRIPTION: This file contains all the include files
 **  used by SIP ALG
 **
 ** DATE        NAME           REFERENCE       REASON
 ** ----        ----           ---------       --------
 ** 26/07/2007  ALG Team      -  Initial Creation
 **
 **  Copyright (C) 2006 Aricent Inc . All Rights Reserved
 **********************************************************************/

#ifndef __SIPALG_INC_H_
#define __SIPALG_INC_H_

/* stack headers */
#include "stack_headers.h"
#include "sipalg_internal.h"
/* ALG specific headers */
#include "sipalg_hash.h"
#include "sipalg_defs.h"
#include "sipalg_parser.h"
#include "sipalg_utils.h"
#include "sipalg_portlayer.h"
/* ALG specific headers defined in nat module */
#include "natsip.h"
#include "nat.h"
/* client library header file */
#include "sipalg.h" 

/*#include "csinc.h"*/
#endif
