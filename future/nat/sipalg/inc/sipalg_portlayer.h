/********************************************************************
 * Copyright (C) Aricent Communications Sotware,2007
 *
 * $Id: sipalg_portlayer.h,v 1.2 2011/06/08 05:03:43 siva Exp $
 *
 * Description:This file contains the prototypes used for portlayer 
 *      function used by SIP ALG 
 *******************************************************************/

#ifndef __SIPALG_PORTLAYER_H_
#define __SIPALG_PORTLAYER_H_

#include "fsapsys.h"
#include "utlmacro.h" 

#define  AF_UNSPEC     0
#define  AF_UNIX       1

typedef enum  t_en_inetFamily
{
    inetInvalid              = AF_UNSPEC,
#ifndef SIP_FSAP 
    inetAFUNIX               = AF_UNIX,
#endif
    inetAFINET               = AF_INET,
    inetAFINET6              = AF_INET6 

} en_inetFamily;

typedef size_t  SIP_size;

/******************************************************************************
 ** FUNCTION: sipAlgatoi
 **
 ** DESCRIPTION:
 ** This function internally calls atoi() of Portlayer class.
 *****************************************************************************/
SIP_S32bit sipAlgatoi(SIP_S8bit *pStr );

/******************************************************************************
 ** FUNCTION: sipAlgstrtokr
 **
 ** DESCRIPTION:
 ** This function internally calls strtok_r of Portlayer class.
 *****************************************************************************/
SIP_S8bit* sipAlgstrtokr(SIP_S8bit *str, \
                       SIP_S8bit *separator,\
                 SIP_S8bit **remaining);

/******************************************************************************
 ** FUNCTION: sipAlginetStrToNet
 **
 ** DESCRIPTION:
 ** This function internally calls inet_pton() of Portlayer class.
 *****************************************************************************/
SipBool  sipAlginetStrToNet(
  en_inetFamily           dFamily,
  SIP_S8bit                 *pStr, 
  void                       *pAddress,
  SipError                *pEcode);

/******************************************************************************
 ** FUNCTION: Sdf_fn_inetNetToStr
 **
 ** DESCRIPTION:
 ** This function internally calls inetNetToStr() of Portlayer class.
 *****************************************************************************/
SipBool sipAlgInetNetToStr (
  en_inetFamily           dFamily,
  void                       *pAddress,
  SIP_S8bit                 *pStr, 
  SIP_size                 size,
  SipError                *pEcode);

/******************************************************************************
 ** FUNCTION: sipAlgstrtok
 **
 ** DESCRIPTION:
 ** This function internally calls strtok of Portlayer class.
 *****************************************************************************/
SIP_S8bit* sipAlgstrtok(
  SIP_S8bit    *str, \
  const SIP_S8bit   *delim);

/******************************************************************************
 ** FUNCTION: sipAlgstrncat
 **
 ** DESCRIPTION:
 ** This function internally calls strncat of Portlayer class.
 *****************************************************************************/
SIP_S8bit* sipAlgstrncat(
  SIP_S8bit   *dest, 
  const SIP_S8bit  *src,
  SIP_size    n);

#endif
