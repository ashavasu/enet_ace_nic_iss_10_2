/********************************************************************
 * Copyright (C) Aricent Communications Sotware,2007
 *
 * $Id: sipalg_hash.h,v 1.4 2012/12/21 12:00:42 siva Exp $
 *
 *******************************************************************/

/***********************************************************************
 ** FUNCTION:
 **             Init function prototypes for all structures
 **
 ***********************************************************************
 **
 ** FILENAME:
 ** sipalg_hash.h
 **
 ** DESCRIPTION: This file contains the prototypes for hashing functions
 **     used by SIP ALG
 **
 ** DATE        NAME           REFERENCE       REASON
 ** ----        ----           ---------       --------
 ** 26/07/2007  ALG Team               -        Initial Creation
 **
 **  Copyright (C) 2006 Aricent Inc . All Rights Reserved
 **********************************************************************/

#ifndef __SIPALG_HASH_H_
#define __SIPALG_HASH_H_

/*This is the Data structure used by the Session Hash Table
  maintained in the SIP ALG. */
extern SipHash            dGlbMediaHash;

/* This is the Data structure used by the Register Contact Information
   Hash Table maintained in the SIP ALG. */
extern SipHash            dGlbNatHash;

    /* SIPALG - 2.1 */
/* This is the Data structure used by the SIP ALG where the
   IP address and mapped IP is maintaiend. (Used for translation
   of O-line) */
extern SipHash            dGlbIPHash;

/* De-Register Hash */
extern SipHash          dGlbDeRegHash;

/* global variable for sipserver status */
extern en_sipStatus  dGlbSipStatus; 

/* Assuming 50 simultaneous calls, each UA having 2 contacts(50 * 2), 
   2 m-lines in one direction 
   50 *2 (2 mline in one direction + 50 * 2( 2 mlines in other direction)*/
#define     SIP_ALG_NUMBER_OF_HASH_BUCKETS        307

/*  Maximum number of simultaneous session to be allowed in the hash table. */
#define     SIP_ALG_MAX_NUMCALLS                2000
#define     SIP_ALG_MAX_100CHARS               100

/* //////////////////////////////////////////////////////////////////////////// */
/* FUNCTION:      sipAlgFreeNatHashKey */
/*  */
/*  DESCRIPTION: Function to free the key for the hash table (call-id) */
/* //////////////////////////////////////////////////////////////////////////// */
void sipAlgFreeNatHashKey (void *pKey);

/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:      sipAlgRemoveEntryFromNatHash */
/*   */
/*  DESCRIPTION: Function to free the data that is stored in the hash. */
/*  */
/* //////////////////////////////////////////////////////////////////////////// */
void sipAlgRemoveEntryFromNatHash (void *pElement);

/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:      sipAlgCompareNatHashKeys */
/*  */
/*  DESCRIPTION: Comparison function for Register Contact Info hash keys. */
/*  If both keys matches , then this function return Zero as the success */
/*  value otherwise it return -1 as the failure value */
/*  */
/* //////////////////////////////////////////////////////////////////////////// */
SIP_U8bit sipAlgCompareNatHashKeys (void *pKey1, void *pKey2);

/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:      sipAlgNatHashFunc */
/*  */
/*  DESCRIPTION: Function to calculate the hash value (key) as output. */
/*  */
/* //////////////////////////////////////////////////////////////////////////// */
SIP_U32bit sipAlgNatHashFunc(void* pData);

/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:      sipAlgFreeMediaHashKey */
/*  */
/*  DESCRIPTION: Function to free the key for the hash table (call-id) */
/* //////////////////////////////////////////////////////////////////////////// */
void sipAlgFreeMediaHashKey (void *pKey);

/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:      sipAlgRemoveEntryFromMediaHash */
/*  */
/*  DESCRIPTION: Function to free the data that is stored in the hash. */
/*  */
/* //////////////////////////////////////////////////////////////////////////// */
void sipAlgRemoveEntryFromMediaHash (void *pData);

/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:      sipAlgCompareMediaHashKeys */
/*  */
/*  DESCRIPTION: Comparison function for session hash keys. */
/*  If both keys matches , then this function return Zero as the success */
/*  value otherwise it return -1 as the failure value */
/*  */
/* //////////////////////////////////////////////////////////////////////////// */
SIP_U8bit sipAlgCompareMediaHashKeys (void *pKey1, void *pKey2);

/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:      sipAlgMediaHashFunc */
/*  */
/*  DESCRIPTION: Function to calculate the hash value given a key as input. */
/*  */
/* //////////////////////////////////////////////////////////////////////////// */
SIP_U32bit sipAlgMediaHashFunc(void* pData);


/* //////////////////////////////////////////////////////////////////////////// */
/* FUNCTION:     sipAlgInitialization  */
/*  */
/*  DESCRIPTION: Function is to initialize all the data structure used by */
/*  the SIP ALG. It initialize the RegContact Info Hash Table  which is used  */
/*  for maintaining the private used registered contacts with the SSE. . It */
/*  initializes the Session Information Hash Table which maintains  the NAT */
/*  addresss ,Bindings and the pinholes. It also initializes the Timer which */
/*  is maintained by the SIP ALG for the Register Contact Information as */
/*  well as SDP information. */
/* //////////////////////////////////////////////////////////////////////////// */
SipBool    sipAlgInitialization(void);

/* //////////////////////////////////////////////////////////////////////////// */
/* FUNCTION:     sipAlgDeInit */
/*  */
/*  DESCRIPTION: Function is to de-initialize all the data structure used by */
/*  the SIP ALG. It de-initializes the NAT Hash Table which is used for */
/*  all the binding and pinhole information for any IP/port combination.     */
/*  It de-initializes the Media Hash Table which maintains the NAT */
/*  addresss, Bindings and the pinholes corresponding to a dialog. */
/*  When "sip disable" is invoked, that is when the hash de-init done. */
/* //////////////////////////////////////////////////////////////////////////// */


/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:     sipAlgInit  */
/*  */
/*  DESCRIPTION: Function is to reset the global variables used in interface */
/*  info strcuture. On "sip disable" the hash entries are cleared. On "sip enable" */
/*  initialize the interface info structure so that again on a new "Via", the  */
/*  entries for signaling will be created. */
/*  sipAlgInitialization function will be invoked only once in CAS. But this  */
/*  function will be invoked multiple times (based on "sip enable" command) */
/* //////////////////////////////////////////////////////////////////////////// */


    /* SIPALG - 2.1 */
/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:      sipAlgIPHashFunc */
/*  */
/*  DESCRIPTION: Function to calculate the hash value given a key as input. */
/*  */
/* //////////////////////////////////////////////////////////////////////////// */
SIP_U32bit    sipAlgIPHashFunc(void* pData);

/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:      sipAlgCompareIPHashKeys */
/*  */
/*  DESCRIPTION: Comparison function for session hash keys. */
/*  If both keys matches , then this function return 0 as the success */
/*  value otherwise it return 1 as the failure value */
/*  */
/* //////////////////////////////////////////////////////////////////////////// */
SIP_U8bit    sipAlgCompareIPHashKeys (void *pKey1, void *pKey2);

/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:      sipAlgRemoveEntryFromIPHash */
/*  */
/*  DESCRIPTION: Function to free the data that is stored in the hash. */
/*  */
/* //////////////////////////////////////////////////////////////////////////// */
void sipAlgRemoveEntryFromIPHash (void *pData);

/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:      sipAlgFreeIPHashKey */
/*  */
/*  DESCRIPTION: Function to free the key for the hash table (call-id) */
/* //////////////////////////////////////////////////////////////////////////// */
void sipAlgFreeIPHashKey (void *pKey);

/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:      sipAlgDeRegHashFunc */
/*  */
/*  DESCRIPTION: Function to calculate the hash value given a key as input. */
/*  */
/* //////////////////////////////////////////////////////////////////////////// */
SIP_U32bit
sipAlgDeRegHashFunc (void *pData);

/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:      sipAlgCompareDeRegHashKeys */
/*  */
/*  DESCRIPTION: Comparison function for session hash keys. */
/*  If both keys matches , then this function return 0 as the success */
/*  value otherwise it return 1 as the failure value */
/*  */
/* //////////////////////////////////////////////////////////////////////////// */
SIP_U8bit
sipAlgCompareDeRegHashKeys (void *pKey1, void *pKey2);

/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:      sipAlgRemoveEntryFromDeRegHash */
/*  */
/*  DESCRIPTION: Function to free the data that is stored in the hash. */
/*  */
/* //////////////////////////////////////////////////////////////////////////// */
void
sipAlgRemoveEntryFromDeRegHash (void *pData);


/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:      sipAlgFreeMediaHashKey */
/*  */
/*  DESCRIPTION: Function to free the key for the hash table (call-id) */
/* //////////////////////////////////////////////////////////////////////////// */
void
sipAlgFreeDeRegHashKey (void *pKey);



#endif /* __SIPALG_HASH_H_ */
