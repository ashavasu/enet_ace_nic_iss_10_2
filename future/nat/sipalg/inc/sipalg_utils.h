/********************************************************************
 * Copyright (C) Aricent Communications Sotware,2007
 *
 * $Id: sipalg_utils.h,v 1.2 2014/01/31 13:10:37 siva Exp $
 *
 * Description:This file contains the prototypes for sipalg
 *             util functions 
 *******************************************************************/
#ifndef __SIPALG_UTILS_H_
#define __SIPALG_UTILS_H_

#include "natinc.h"


/* IP Address Type */
typedef enum     
{
    en_invalid,
    en_privateIp,
    en_publicIp,
    en_natIP
    
}en_IpType;      

#ifdef SDF_TIMESTAMP

/******************************************************************************
** FUNCTION: Sdf_fn_writeToFile
** DESCRIPTION: This function is used to write the Time Stamp values to file 
**
** PARAMETERS: None
**
** Returns   : None
*******************************************************************************/

VOID
Sdf_fn_writeToFile ();

/******************************************************************************
** FUNCTION: Sdf_fn_printTimeStamp
** DESCRIPTION: This function is used to write the Time Stamp values to tslog
**              structure 
**
** PARAMETERS: None
**
** Returns   : 1
*******************************************************************************/
INT4
Sdf_fn_printTimeStamp (UINT1 *pPrintString);


#endif
/****************************************************************************
** FUNCTION: sipAlgIsValidNumericAddress
** DESCRIPTION: This function gets is used to find whether the given ip is 
**              valid numeric or not 
**
** PARAMETERS:
**          pNumStr(IN)                - Pointer to the string 
**
** Returns   : SUCCESS/FAILURE
******************************************************************************/
SipBool sipAlgIsValidNumericAddress(SIP_U8bit *pNumStr);


/****************************************************************************
** FUNCTION: sipAlgIsNumericAddress
** DESCRIPTION: This function gets is used to find whether the given string is 
**              numeric or not (numeric or hostname)
**
** PARAMETERS:
**          pNumStr(IN)                - Pointer to the string 
**
** Returns   : SUCCESS/FAILURE
******************************************************************************/
SipBool sipAlgIsNumericAddress(SIP_U8bit *pNumStr);

/*********************************************************************************
** FUNCTION: sipAlgGetDialogParamFromSipMsg
** DESCRIPTION: This function is used to get the dialog params from the SIP-MSG 
**
** PARAMETERS:
**          pSipMsg(IN)                - Pointer to SIP message 
**            pDialogKey(OUT)         - pointer to dialog params 
**
** Returns   : SUCCESS/FAILURE
**********************************************************************************/
SipBool sipAlgGetDialogParamFromSipMsg(SipMessage *pSipMsg,SipAlgMediaKey *pDialogKey);

/******************************************************************************
** FUNCTION: sipAlgFreeDialogParam
** DESCRIPTION: This function is used to free the dialog params structure
**              and its members 
**
** PARAMETERS:
**        ppDialogKey(OUT)         - double pointer to dialog params 
**
** Returns   : void
******************************************************************************/
void sipAlgFreeDialogParam(SipAlgMediaKey **ppDialogKey);

/*********************************************************************************
** FUNCTION: sipAlgIsRegisterRequest
** DESCRIPTION: This function is used to check whether this is a REGISTER req. 
**
** PARAMETERS:
**          pSipMsg(IN)                - Pointer to SIP message 
**
** Returns   : SUCCESS/FAILURE
**********************************************************************************/

SipBool sipAlgIsRegisterRequest(SipMessage *pSipMsg);

/*********************************************************************************
** FUNCTION: sipAlgGetMethodFromSipReqest
** DESCRIPTION: This function is used to get the method name from the SIP request 
**
** PARAMETERS:
**          pSipMsg(IN)                - Pointer to SIP message 
**          ppMethod(OUT)           - Method name to be returned
**
** Returns   : SUCCESS/FAILURE
**********************************************************************************/
SipBool sipAlgGetMethodFromSipReqest(SipMessage *pSipMsg,\
                                     SIP_S8bit **ppMethod);

/*********************************************************************************
** FUNCTION: sipAlgGetRespCodeFromSipInviteResponse
** DESCRIPTION: This function gets the response code from the SIP-Response for INVITE 
**
** PARAMETERS:
**          pSipMsg(IN)                - Pointer to SIP message 
**          pRespCode(OUT)          - The response code to be returned
**
** Returns   : SUCCESS/FAILURE
**********************************************************************************/

SipBool sipAlgGetRespCodeFromSipInviteResponse(SipMessage *pSipMsg,\
                                            SIP_U16bit *pRespCode );

/****************************************************************************
* FUNCTION: sipAlgIsWANIPAddress
* DESCRIPTION: This function identifies whether the given IP is a WAN IP 
*       or not.
*
* PARAMETERS:
*             dIPAdress (IN)      - IP Address 
*
* Returns   : SUCCESS/FAILURE
****************************************************************************/
en_SipBoolean
    sipAlgIsWANIPAddress (SIP_U32bit dIPAdress);

/****************************************************************************
* FUNCTION: sipAlgGetWANIPAddress
* DESCRIPTION: This function provides the WAN IP of the system.
*
* PARAMETERS: ifIndex - WAN interface index
*              natIP - Out param     
*
* Returns   : IP address on success, and 0 on failure.
****************************************************************************/
SIP_U32bit sipAlgGetWANIPAddress(SIP_U32bit ifIndex, SIP_U32bit *pNatIP);

/****************************************************************************
* FUNCTION: sipAlgGetUserType
* DESCRIPTION: This function is used to find whether the destinatioin IP
*              is private/public/vpn .
*
* PARAMETERS:  dDestIP - input param 
*
* Returns   : en_IpAddrType.
****************************************************************************/
en_IpAddrType
sipAlgGetUserType (SIP_U32bit dDestIP);

/****************************************************************************
* FUNCTION: sipAlgNatHashPrint
* DESCRIPTION: This function prints all elements in NAT hash 
*        with their values
*
* PARAMETERS: NONE
*
* Returns   : NONE
****************************************************************************/
void sipAlgNatHashPrint(SipHash *pNatHash);

/****************************************************************************
* FUNCTION: sipAlgMediaHashPrint
* DESCRIPTION: This function prints all elements in Media hash 
*        with their values
*
* PARAMETERS: NONE
*
* Returns   : NONE
****************************************************************************/
void sipAlgMediaHashPrint(SipHash *pMediaHash);

/****************************************************************************
* FUNCTION: sipAlgDeRegHashPrint
* DESCRIPTION: This function prints all elements in DeReg hash 
*        with their values
*
* PARAMETERS: DeRegHash
*
* Returns   : NONE
****************************************************************************/
void
sipAlgDeRegHashPrint (SipHash * pDeRegHash);

/****************************************************************************
* FUNCTION: sipAlgGetIfIndex
* DESCRIPTION: This function provides the interface index for the given WAN IP.
*
* PARAMETERS: wanIP - The wan IP for which ifIndex is needed.
*              ifIndex - Out param
*
* Returns   : SUCCESS/FAILURE 
****************************************************************************/
SipBool sipAlgGetIfIndex(SIP_U32bit wanIP, SIP_U32bit *pIfIndex);

/******************************************************************************
 ** FUNCTION: sipAlgGetMethodAndRespCodeFromSipResponse
 ** DESCRIPTION: This function gets the method and response code from the SIP-Response
 **
 ** PARAMETERS:
 **            pSipMsg(IN)                     - Pointer to SIP message
 **          ppMethod(OUT)           - Method name to be returned
 **          pRespCode(OUT)          - The response code to be returned
 **
 ** Returns   : SUCCESS/FAILURE
 *******************************************************************************/
 SipBool sipAlgGetMethodAndRespCodeFromSipResponse(SipMessage *pSipMsg,\
                      SIP_S8bit **ppMethod, SIP_U16bit *pRespCode);

#endif 
