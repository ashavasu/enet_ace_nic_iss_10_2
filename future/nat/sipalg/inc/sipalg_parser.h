/********************************************************************
 * Copyright (C) Aricent Communications Sotware,2007
 *
 * $Id: sipalg_parser.h,v 1.2 2014/01/25 13:52:25 siva Exp $
 *
 * Description:This file contains the translation function's prototypes for
 *              SIP Msg.
 *******************************************************************/

/***********************************************************************
** FUNCTION:
**   SIP Message Translation functionality
************************************************************************
**
** FILENAME:
** sipalg_parser.h
**
** DESCRIPTION:
**  This file contains the translation function's prototypes for 
**  SIP Msg.
**
** DATE    NAME    REFERENCE   REASON
** ----    ----    ---------   --------
** 16/07/2007 Rashed Ahammad  -   Initial     
**
**
**  Copyright (C) 2007 Aricent Inc . All Rights Reserved
***********************************************************************/

#ifndef __SIPALG_PARSER_H_
#define __SIPALG_PARSER_H_

#include "natinc.h"


#define MAX_MESSAGE_LEN 1024
#define SIP_MAX_NODE_SIZE 100
#define SIP_MAX_NODENAME_SIZE 16
#define SIP_MAX_IPADDR_SIZE 20

#define SIP_ALG_INET_ADDRSTRLEN  16
#define SIP_ALG_IP_PORT_LEN   22
#define SIP_ALG_PORT_LEN   6
#define SIP_DELTA    20

/*********************************************************************************
** FUNCTION: sipAlgTranslateSipMsg
** DESCRIPTION: This function is the interface to NAT module to perform the 
**              SIP msg translation for both incoming and outgoing packet.
**              This function is responsible for translating the entire msg 
**
** PARAMETERS:
**    pSipBuf(IN)          - Pointer to SIP buffer 
**   direction(IN)         - specifies whether this is OUT/IN Msg 
**   pNatHeaderInfo(IN)       - pointer to tHeaderInfo which has NAT info 
**   ppTranslatedSipBuf(OUT) - Translated SIP Buffer 
**
** Returns   : SUCCESS/FAILURE
**********************************************************************************/

SIP_U8bit sipAlgTranslateSipMsg (SIP_U8bit *pSipBuf, \
                               SipAlgPktInOut direction,\
                               tHeaderInfo *pNatHeaderInfo,\
                               SIP_U8bit **ppTranslatedSipBuf,\
/* memory leak fix start */
        SIP_U32bit dSipBufLen);
/* memory leak fix end */

/*********************************************************************************
** FUNCTION: sipAlgTranslateIncomingHeaders
** DESCRIPTION: This function translates the incoming msg headers
**
** PARAMETERS:
**    pSipMsg(IN)          - Pointer to SIP Msg 
**   pNatHeaderInfo(IN)       - pointer to tHeaderInfo which has NAT info 
**
**Returns : SUCCESS/FAILURE 
**********************************************************************************/

SipBool sipAlgTranslateIncomingHeaders (SipMessage *pSipMsg,\
                                        tHeaderInfo *pNatHeaderInfo);

/*********************************************************************************
** FUNCTION: sipAlgTranslateOutgoinggHeaders
** DESCRIPTION: This function translates the outgoing msg headers
**
** PARAMETERS:
**    pSipMsg(IN)          - Pointer to SIP Msg 
**   pNatHeaderInfo(IN)       - pointer to tHeaderInfo which has NAT info 
**
**Returns : SUCCESS/FAILURE 
**********************************************************************************/
SipBool sipAlgTranslateOutgoingHeaders (SipMessage *pSipMsg,\
                                        tHeaderInfo *pNatHeaderInfo);

/*********************************************************************************
** FUNCTION: sipAlgTranslateMedia
** DESCRIPTION: This function translates the media in sdp if the sdp body is 
**              present in the message
**
** PARAMETERS:
**    pSipMsg(IN)          - Pointer to SIP Msg 
**   direction(IN)         - specifies whether this is OUT/IN Msg
**   pNatHeaderInfo(IN)       - pointer to tHeaderInfo which has NAT info 
**
** Returns : SUCCESS/FAILURE 
**********************************************************************************/

SipBool sipAlgTranslateMedia (SipMessage *pSipMsg,\
                              SipAlgPktInOut direction,\
                              tHeaderInfo *pNatHeaderInfo);

/*********************************************************************************
** FUNCTION: sipAlgTranslateReqUri
** DESCRIPTION: This function translates the Req-URI 
**
** PARAMETERS:
**    pSipMsg(IN)          - Pointer to SIP Msg 
**   direction(IN)         - specifies whether this is OUT/IN Msg
**   pNatHeaderInfo(IN)       - pointer to tHeaderInfo which has NAT info 
**
** Returns : SUCCESS/FAILURE 
**********************************************************************************/

SipBool sipAlgTranslateReqUri(SipMessage *pSipMsg,\
                              SipAlgPktInOut direction,\
                              tHeaderInfo *pNatHeaderInfo);

/*********************************************************************************
** FUNCTION: sipAlgTranslateViaHdr
** DESCRIPTION: This function translates the Via Hdr 
**
** PARAMETERS:
**    pSipMsg(IN)          - Pointer to SIP Msg 
**   direction(IN)         - specifies whether this is OUT/IN Msg
**   pNatHeaderInfo(IN)       - pointer to tHeaderInfo which has NAT info 
**
** Returns : SUCCESS/FAILURE 
**********************************************************************************/

SipBool sipAlgTranslateViaHdr(SipMessage *pSipMsg,\
                              SipAlgPktInOut direction,\
                              tHeaderInfo *pNatHeaderInfo);

/*********************************************************************************
** FUNCTION: sipAlgTranslateContactHdr
** DESCRIPTION: This function translates the Contact Header 
**
** PARAMETERS:
**    pSipMsg(IN)          - Pointer to SIP Msg 
**   direction(IN)         - specifies whether this is OUT/IN Msg
**   pNatHeaderInfo(IN)       - pointer to tHeaderInfo which has NAT info 
**
** Returns : SUCCESS/FAILURE 
**********************************************************************************/

SipBool sipAlgTranslateContactHdr(SipMessage *pSipMsg,\
                                  SipAlgPktInOut direction,\
                                  tHeaderInfo *pNatHeaderInfo, \
                                  en_SipBoolean dIsRespToReg,en_SipBoolean dIsReq);

/*********************************************************************************
** FUNCTION: sipAlgTranslateRouteHeader
** DESCRIPTION: This function translates the Route Header 
**
** PARAMETERS:
**    pSipMsg(IN)          - Pointer to SIP Msg 
**   direction(IN)         - specifies whether this is OUT/IN Msg
**   pNatHeaderInfo(IN)       - pointer to tHeaderInfo which has NAT info 
**
** Returns : SUCCESS/FAILURE 
**********************************************************************************/

SipBool sipAlgTranslateRouteHdr(SipMessage *pSipMsg,\
                                SipAlgPktInOut direction,\
                                tHeaderInfo *pNatHeaderInfo);

/*********************************************************************************
** FUNCTION: sipAlgTranslateRecordRouteHeader
** DESCRIPTION: This function translates the Record-Route Header 
**
** PARAMETERS:
**    pSipMsg(IN)          - Pointer to SIP Msg 
**   direction(IN)         - specifies whether this is OUT/IN Msg
**   pNatHeaderInfo(IN)       - pointer to tHeaderInfo which has NAT info 
**
** Returns : SUCCESS/FAILURE 
**********************************************************************************/

SipBool sipAlgTranslateRecordRouteHdr(SipMessage *pSipMsg,\
                                      SipAlgPktInOut direction,\
                                      tHeaderInfo *pNatHeaderInfo);

/****************************************************************************
* Function Name : SipAlgModifyXmlMessageBody 
* Description   : This function modifies the xml message body  
*           by replacing priv IP and port with Trans 
*                 Ip and port. 
*                 
* Input/Output (s): 1.pSipMessage - Sip message whose xml body will be modified
*                   2.pErr -Error code
* Input           : pHeaderInfo - 
* 
* Returns        : SUCCESS/FAILURE
*****************************************************************************/

SIP_S32bit
SipAlgModifyXmlMessageBody (SipMessage *pSipMessage,
 tHeaderInfo *pHeaderInfo,SipError *pErr);

/*****************************************************************************
 ** FUNCTION: SipAlgGetMessageBody 
 **
 ** DESCRIPTION:  This function gets the message body and its size.
 **
 *****************************************************************************/

SipBool SipAlgGetMessageBody(
   SipMessage     *pSipMsg,
   SIP_S8bit      **pXmlMsgBody,
   SIP_U32bit  *pLength,
   SipError   *pError);

/*****************************************************************************
 ** FUNCTION: SipAlgSetMessageBody 
 **
 ** DESCRIPTION:  This function sets the message body and its size.
 **
 *****************************************************************************/

SipBool SipAlgSetMessageBody(SipMessage *pSipMsg,SIP_S8bit *pXmlMsgBody, 
 SIP_U32bit *pLength, SipError *pErr);

SipBool SipAlgHandleXpidfMsg(SIP_U8bit *pModifyXmlMsg,SIP_U8bit 
 *pSipNodeBeg,SIP_U8bit *pSipNodeEnd,tHeaderInfo *pHeaderInfo);

SipBool sipAlgHandlePidfMsg(SIP_U8bit *pModifyXmlMsg,SIP_U8bit 
 *pSipNodeBeg,SIP_U8bit *pSipNodeEnd,tHeaderInfo *pHeaderInfo);

SipBool sipAlgGetTranslatedIpPort(SIP_U8bit *pPrivIp,SIP_U8bit *pPrivIpPort,
 tHeaderInfo *pHeaderInfo,SIP_U8bit *pTransIp,SIP_U8bit *pTransIpPort);

#endif 
