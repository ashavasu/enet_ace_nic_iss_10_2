/********************************************************************
 * Copyright (C) Aricent Communications Sotware,2007
 *
 * $Id: sipnat.c,v 1.4 2013/06/14 12:14:52 siva Exp $
 *
 *******************************************************************/

#include "natinc.h"
#include "sipalg_inc.h"

tEventNotificationInfo *NatSipAlgUtilAllocBuf (UINT4 u4Len);
VOID                NatSipAlgUtilFreeBuf (tEventNotificationInfo *
                                          pEventNotificationInfo);
extern INT1 nmhGetNatEnable ARG_LIST ((INT4 *));
/******************************************************************************
 *                                                                            *
 * Function     : NatSipAlgAddPortMapping                                     *
 *                                                                            *
 * Description  : This funtion is used to add port mapping to NAT. This is an *
 *                asynchronous function                                       *
 *                                                                            *
 * Input        : pNatEntryInfo - Nat Entry information                       *
 *                u4Cookie      - Unique Identifier                           *
 *                                                                            *
 * Output       : None                                                        *
 *                                                                            *
 * Return       : OSIX_SUCCESS/OSIX_FAILURE                                   *
 *                                                                            *
 ******************************************************************************/

PUBLIC VOID
NatSipAlgAddPortMapping (tNatEntryInfo * pNatEntryInfo, UINT4 u4Cookie,
                         tNatAsyncResponse * pNatAsyncResp)
{
    tDynamicEntry      *pDynamicEntry = NULL;
    eParity             eBaseParity = PARITY_ANY;
    eBindType           eBindingType = BIND_TYPE_MEDIA;
    UINT4               u4ErrorCode = NATIPC_NO_ERROR;
    UINT4               u4DelPortErrorCode;
    UINT4               u4ExtIp = NATIPC_VALUE_ZERO;
    UINT4               u4Direction = NATIPC_VALUE_ZERO;
    UINT4               u4LocalIP = NATIPC_VALUE_ZERO;
    UINT4               u4IntfNum = NATIPC_VALUE_ZERO;
    INT4                i4Protocol = NAT_PROTO_ANY;
    INT4                i4TransPort = NATIPC_PORT_NUM_ZERO;
    INT4                i4CurrTransPort = NATIPC_PORT_NUM_ZERO;
    INT4                i4RowStatus = NATIPC_VALUE_ZERO;    /* not used */
    INT4                i4NatStatus = 0;    /* not used */
    UINT2               u2Count = NATIPC_VALUE_ZERO;
    UINT2               u2ExtPort = NATIPC_PORT_NUM_ZERO;
    UINT2               u2GlobalFreePort = NATIPC_PORT_NUM_ZERO;
    UINT2               u2NumbOfBindings = NATIPC_VALUE_ZERO;
    UINT2               u2LocalListenBasePort = NATIPC_PORT_NUM_ZERO;
    UINT2               u2LocPort = NATIPC_PORT_NUM_ZERO;
    UINT2               u2GlobalBasePort = NATIPC_PORT_NUM_ZERO;
    INT2                i2PortsAllocFlag = NAT_SIP_FAILURE;
    UINT1               au1Desc[12];    /* not used */
    UINT1               u1TimerFlag = NAT_NOT_ON_HOLD;

    pNatAsyncResp->u4Cookie = u4Cookie;
    pNatAsyncResp->u4Status = NAT_SIP_SUCCESS;
    pNatAsyncResp->u4ErrorCode = NATIPC_NO_ERROR;

    /* Sanity check */
    if (pNatEntryInfo == NULL)
    {
        NAT_TRC (NAT_TRC_ON, "\n NatAddPortMappig():"
                 "Input argument pNatEntryInfo is a NULL pointer \r\n");

        pNatAsyncResp->u4Status = NAT_SIP_FAILURE;
        pNatAsyncResp->u4ErrorCode = NATIPC_INVALID_PARAMETERS;
        /* caller should free this memory */
        return;
    }
    u4ExtIp = pNatEntryInfo->u4NATIP;
    u2ExtPort = pNatEntryInfo->u2NATPort;
    u4LocalIP = pNatEntryInfo->u4SrcIP;
    u2LocalListenBasePort = pNatEntryInfo->u2SrcPort;
    /*
     * We Need This Information if there is corresponding Dynamic Entry.
     * For SIGNAL NAT Should Not Delete The Dynamic Entry.
     */
    eBindingType = pNatEntryInfo->u1BindingType;
    /* Since Media traffic can be initiated either from LAN or WAN side,
     * We need to add Partial entries for INBOUND and OUTBOUD direction.
     * Though the media traffic can be traslated without OUT_PARTIAL,
     * the reason of having two way partials is to avoid the allocation of
     * different traslated ports for same Local Ip/Port & Out IP/Port tuple
     * in both INBOUND and OUTBOUND directions.
     */
    if (eBindingType == BIND_TYPE_MEDIA)
    {
        u4Direction = NAT_ANY;
    }
    else if (pNatEntryInfo->u4Direction == NAT_INBOUND)
    {
        u4Direction = NAT_INBOUND;
    }
    else if (pNatEntryInfo->u4Direction == NAT_OUTBOUND)
    {
        u4Direction = NAT_OUTBOUND;
    }

    u1TimerFlag = NAT_NOT_ON_HOLD;    /* This is unused by SIP. still needed for
                                     * NAT to identify the entries as SIP related */

    if (pNatEntryInfo->u2Protocol == NAT_SIP_TCP)
    {
        i4Protocol = NAT_TCP;
    }
    else if (pNatEntryInfo->u2Protocol == NAT_SIP_UDP)
    {
        i4Protocol = NAT_UDP;
    }
    else if (pNatEntryInfo->u2Protocol == NAT_SIP_ANY)
    {
        i4Protocol = NAT_PROTO_ANY;
    }

    i4TransPort = u2ExtPort;    /* this has to be 0 as mentioned in ICD */
    if (i4TransPort != 0)
    {
        pNatAsyncResp->u4Status = NAT_SIP_FAILURE;
        pNatAsyncResp->u4ErrorCode = NATIPC_INVALID_PARAMETERS;
        /* caller should free this memory */
        return;
    }

    if (SecUtilIpIfGetIfIndexFromIpAddress (u4ExtIp, &u4IntfNum) ==
        OSIX_FAILURE)
    {
        pNatAsyncResp->u4Status = NAT_SIP_FAILURE;
        pNatAsyncResp->u4ErrorCode = NATIPC_NAT_INTERNAL_ERROR;
        /* caller should free this memory */
        return;
    }

    u2NumbOfBindings = pNatEntryInfo->u2NumBindings;
    eBaseParity = pNatEntryInfo->u1Parity;
    u2LocPort = u2LocalListenBasePort;

    nmhGetNatEnable (&i4NatStatus);
    if ((i4NatStatus == NAT_DISABLE) ||
        (NatCheckIfNatEnable (u4IntfNum) == NAT_DISABLE))
    {
        u2GlobalBasePort = u2LocPort;
        u4ExtIp = u4LocalIP;
    }
    else
    {
        for (u2Count = 0; u2Count < u2NumbOfBindings; u2Count++, u2LocPort++)
        {
            u2GlobalFreePort = NATIPC_PORT_NUM_ZERO;
            u2GlobalFreePort = NatApiNaptSearchDynamicList (u4LocalIP,
                                                            u2LocPort,
                                                            u4ExtIp,
                                                            (UINT1) i4Protocol,
                                                            &pDynamicEntry);
            if (pDynamicEntry != NULL)
            {
                /* Sending Event to Application with 7 tuple */
                NatSipAlgSendDynEntryEvent (pDynamicEntry->
                                            u4LocIpAddr,
                                            pDynamicEntry->
                                            u2LocPort,
                                            pDynamicEntry->
                                            u4TranslatedLocIpAddr,
                                            pDynamicEntry->
                                            u2TranslatedLocPort,
                                            pDynamicEntry->
                                            u4OutIpAddr,
                                            pDynamicEntry->
                                            u2OutPort,
                                            pDynamicEntry->
                                            u1PktType,
                                            IPC_NAT_DYN_ENTRY_ADD_EVENT);
                if (eBindingType == BIND_TYPE_SIGNAL)
                {
                    pDynamicEntry->u1RetainOnTmrExpiry = TRUE;
                }
                else
                {
                    pDynamicEntry->u1RetainOnTmrExpiry = FALSE;
                }
                /* There exists a dynamic entry, so the Translated ports would
                 * have been reserved */
                if (u2GlobalFreePort != NATIPC_PORT_NUM_ZERO)
                {
                    break;
                }
            }

        }
        if (u2GlobalFreePort != NATIPC_PORT_NUM_ZERO && pDynamicEntry != NULL)
        {
            /* getting the base translated port that would have been used as
             * a base port when N contiguous translated ports were reserved
             * when the media traffic had gone from LAN UA --> WAN UA */
            u2GlobalBasePort = (UINT2) (u2GlobalFreePort - u2Count);
        }
        else
        {
            /*
             * If NAPT is Enabled We must allocate Global Free Port.
             */
            if (NatCheckIfNaptEnable (u4IntfNum) == NAT_ENABLE)
            {
                /* N Translated ports have to be allocated */

                if (NatGetFreeGlobalPortGroup
                    (&u2GlobalBasePort, u2NumbOfBindings, eBaseParity,
                     eBindingType) != NAT_SUCCESS)
                {
                    pNatAsyncResp->u4Status = NAT_SIP_FAILURE;
                    pNatAsyncResp->u4ErrorCode = NATIPC_NAT_INTERNAL_ERROR;
                    /* caller should free this memory */
                    return;
                }
                i2PortsAllocFlag = NAT_SIP_SUCCESS;
            }
        }
    }                            /* end: NAT  enabled on the port */

    /*
     * If NAPT is Enabled We must Use Global Free Port.
     * else we must use Local IP as Translated IP.
     */
    if (NatCheckIfNaptEnable (u4IntfNum) == NAT_ENABLE)
    {
        i4TransPort = u2GlobalBasePort;
    }
    else
    {
        i4TransPort = u2LocalListenBasePort;
        u2GlobalBasePort = u2LocalListenBasePort;
    }
    u2LocPort = u2LocalListenBasePort;

    for (u2Count = 1; u2Count <= u2NumbOfBindings; u2Count++,
         u2LocPort++, i4TransPort++)
    {
        i4CurrTransPort = i4TransPort;
        if (NatApiAddPortMappingEntry
            (u4IntfNum, u4LocalIP, (INT4) u2LocPort, i4Protocol, u4ExtIp,
             &i4TransPort, i4RowStatus, (UINT2) u4Direction, 0, u1TimerFlag,
             au1Desc, &u4ErrorCode, pDynamicEntry) == NAT_FAILURE)
        {
            i4TransPort = i4CurrTransPort;    /* As NatApiAddPortMappingEntry
                                             * might overwrite i4TransPort */
            /* Nothing to be done for the failed binding */
            for (; u2Count > 1; u2Count--)
            {
                /* point to the last allocation of Local & NAT Port */
                /* clean all the bindings which were created */
                u2LocPort--;
                i4TransPort--;
                if (NatApiDelPortMappingEntry (u4IntfNum, u4LocalIP,
                                               u4ExtIp, i4TransPort,
                                               u2LocPort, (UINT2) i4Protocol,
                                               (UINT2) u4Direction,
                                               &u4DelPortErrorCode) ==
                    NAT_FAILURE)
                {
                    NAT_TRC (NAT_TRC_ON,
                             "\n NatSipAlgAddPortMapping():"
                             "Error in deleting the added port mapping\r\n");

                }
            }

            /*
             * Ports Will be allocated only when NAPT is Enabled.
             * If i2PortsAllocFlag == NAT_SIP_SUCCESS Then NAPT is Enabled.
             * Release all the allocated NAT ports.
             */
            if (i2PortsAllocFlag == NAT_SIP_SUCCESS)
            {
                /* i4TransPort would have the smallest NAT port */
                for (u2Count = 1; u2Count <= u2NumbOfBindings;
                     u2Count++, i4TransPort++)
                {
                    if (NatReleaseGlobalPort ((UINT2) i4TransPort) ==
                        NAT_FAILURE)
                    {
                        NAT_TRC (NAT_TRC_ON,
                                 "\n NatSipAlgAddPortMapping(): "
                                 "Error in releasing the global ports\r\n");
                    }
                }
            }

            if (u4ErrorCode == NATIPC_BINDING_ALREADY_EXISTS ||
                u4ErrorCode == NATIPC_INTERNAL_ERROR)
            {
                pNatAsyncResp->u4RetVal1 = i4TransPort;
                pNatAsyncResp->u4ErrorCode = u4ErrorCode;
                pNatAsyncResp->u4Status = NAT_SIP_FAILURE;

            }

            /* if this api is used as synchronous */
            return;
        }
    }

    pNatAsyncResp->u4RetVal1 = u2GlobalBasePort;

    /* caller should free this memory */
    return;
}

/******************************************************************************
 *                                                                            *
 * Function     : NatSipAlgDeletePortMapping                                        *
 *                                                                            *
 * Description  : This funtion is used to delete the port mapping in NAT. This*
 *                is an asynchronous function                                 *
 *                                                                            *
 * Input        : pNatEntryInfo - Nat Entry information                       *
 *                u4Cookie      - Unique identifier                           *
 *                                                                            *
 * Output       : None                                                        *
 *                                                                            *
 * Return       : OSIX_SUCCESS/OSIX_FAILURE                                   *
 *                                                                            *
 ******************************************************************************/

PUBLIC VOID
NatSipAlgDeletePortMapping (tNatEntryInfo * pNatEntryInfo, UINT4 u4Cookie,
                            tNatAsyncResponse * pNatAsyncResp)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4LocalIP = 0;
    UINT4               u4TransIP = 0;
    UINT4               u4IntfNum = 0;
    INT4                i4Protocol = 0;
    INT4                i4NatStatus = 0;
    UINT2               u2TransPort = 0;
    UINT2               u2LocalPort = 0;
    UINT2               u2Direction;
    UINT2               u2NumBindings = 0;    /* Num Of PinHoles Recvd Frm SIP */
    UINT2               u2Loop = 0;
    UINT4               u4PortExists;

    u4TransIP = pNatEntryInfo->u4NATIP;
    u4LocalIP = pNatEntryInfo->u4SrcIP;
    u2TransPort = pNatEntryInfo->u2NATPort;
    u2LocalPort = pNatEntryInfo->u2SrcPort;
    u2NumBindings = pNatEntryInfo->u2NumBindings;

    if (pNatEntryInfo->u2Protocol == NAT_SIP_TCP)
    {
        i4Protocol = NAT_TCP;
    }
    else if (pNatEntryInfo->u2Protocol == NAT_SIP_UDP)
    {
        i4Protocol = NAT_UDP;
    }
    else if (pNatEntryInfo->u2Protocol == NAT_SIP_ANY)
    {
        i4Protocol = NAT_PROTO_ANY;
    }
    /* Since Media traffic can be initiated either from LAN or WAN side,
     * Partial entries were added for INBOUND and OUTBOUD direction.
     * So if it is media binding, need to delete INBOUND and OUTBOUND partials.
     */
    if (pNatEntryInfo->u1BindingType == BIND_TYPE_MEDIA)
    {
        u2Direction = NAT_ANY;
    }
    else
    {
        u2Direction = (UINT2) pNatEntryInfo->u4Direction;
    }

    pNatAsyncResp->u4Cookie = u4Cookie;
    pNatAsyncResp->u4Status = NAT_SIP_SUCCESS;
    pNatAsyncResp->u4ErrorCode = 0;
    pNatAsyncResp->u4RetVal1 = 0;
    pNatAsyncResp->u4RetVal2 = 0;

    if (SecUtilIpIfGetIfIndexFromIpAddress (u4TransIP, &u4IntfNum) ==
        OSIX_FAILURE)
    {
        pNatAsyncResp->u4Status = NAT_SIP_FAILURE;
        pNatAsyncResp->u4ErrorCode = NATIPC_NAT_INTERNAL_ERROR;
        NAT_TRC (NAT_TRC_ON, "NatSipAlgDeletePortMapping() : "
                 "Exiting function \r\n");
        return;
    }

    nmhGetNatEnable (&i4NatStatus);
    if ((i4NatStatus == NAT_DISABLE) ||
        (NatCheckIfNatEnable (u4IntfNum) == NAT_DISABLE))
    {
        u2TransPort = u2LocalPort;
        u4TransIP = u4LocalIP;
    }
    for (u2Loop = 0; u2Loop < u2NumBindings; u2Loop++)
    {
        if (NatApiDelPortMappingEntry (u4IntfNum, u4LocalIP, u4TransIP,
                                       u2TransPort, (INT4) u2LocalPort,
                                       (UINT1) i4Protocol, u2Direction,
                                       &u4ErrorCode) == NAT_FAILURE)
        {
            pNatAsyncResp->u4Status = NAT_SIP_FAILURE;

            if (u4ErrorCode == NATIPC_BINDING_NOT_EXISTS)
            {
                pNatAsyncResp->u4ErrorCode = NATIPC_BINDING_NOT_EXISTS;
            }
            else
            {
                pNatAsyncResp->u4ErrorCode = NATIPC_INTERNAL_ERROR;
            }
        }
        /*
         * Make sure that translated port is not present in the
         * Global Hash Node, if it is not present release corresponding
         * translated port to free pool list
         */
        /*
         * u2TransPort != u2LocalPort this check is needed, to hadle when nat
         * is disabled.
         */
        if (u2TransPort != u2LocalPort)
        {
            u4PortExists = NatGetHashPortExistsStatus (u4TransIP, u2TransPort,
                                                       NAT_GLOBAL);
            if (u4PortExists == FALSE)
            {
                /*
                 * If NAPT is Enabled We must Release Global Free Port.
                 */
                if (NatCheckIfNaptEnable (u4IntfNum) == NAT_ENABLE)
                {
                    NatReleaseGlobalPort (u2TransPort);
                }
            }
        }
        u2LocalPort = (UINT2) (u2LocalPort + 1);
        u2TransPort = (UINT2) (u2TransPort + 1);
    }

    return;
}

/******************************************************************************
 *                                                                            *
 * Function     : NatSipAlgOpenPinhole                                          *
 *                                                                            *
 * Description  : This funtion is used to open a pinhole in firewall for ip   *
 *                and port combination                                        *
 *                                                                            *
 * Input        : pPinholeInfo - Pin Hole information                         *
 *                u4Cookie     - Unique Identifier                            *
 *                                                                            *
 * Output       : None                                                        *
 *                                                                            *
 * Return       : tNatAsyncResponse pointer                                   *
 *                                                                            *
 ******************************************************************************/

PUBLIC VOID
NatSipAlgOpenPinhole (tPinholeInfo * pPinholeInfo, UINT4 u4Cookie,
                      tNatAsyncResponse * pNatAsyncResp)
{
    eParity             eBaseParity = PARITY_ANY;
    UINT4               u4ErrorCode = NATIPC_NO_ERROR;
    UINT4               u4CloseErrorCode = NATIPC_NO_ERROR;
    UINT4               u4LocalIP = NATIPC_VALUE_ZERO;
    UINT4               u4DestIP = NATIPC_VALUE_ZERO;
    UINT2               u2Count = NATIPC_VALUE_ZERO;
    UINT2               u2NumbOfPinholes = NATIPC_VALUE_ZERO;
    UINT2               u2Direction = NAT_OUTBOUND;
    UINT2               u2Protocol = NAT_PROTO_ANY;
    UINT2               u2DestPort = NATIPC_PORT_NUM_ZERO;
    UINT2               u2LocalPort = NATIPC_PORT_NUM_ZERO;
    UINT2               u2BaseLocalPort = NATIPC_PORT_NUM_ZERO;
    UINT2               u2BaseDestPort = NATIPC_PORT_NUM_ZERO;
    UINT1               u1TimerFlag = NAT_NOT_ON_HOLD;

    u4LocalIP = pPinholeInfo->u4SrcIP;
    u2BaseLocalPort = pPinholeInfo->u2SrcPort;

    if (pPinholeInfo->u2Protocol == NAT_SIP_TCP)
    {
        u2Protocol = NAT_TCP;
    }
    else if (pPinholeInfo->u2Protocol == NAT_SIP_UDP)
    {
        u2Protocol = NAT_UDP;
    }
    else if (pPinholeInfo->u2Protocol == NAT_SIP_ANY)
    {
        u2Protocol = NAT_PROTO_ANY;
    }

    if (pPinholeInfo->u2Direction == NAT_INBOUND)
    {
        u2Direction = NAT_INBOUND;
    }
    else if (pPinholeInfo->u2Direction == NAT_OUTBOUND)
    {
        u2Direction = NAT_OUTBOUND;
    }

    u4DestIP = pPinholeInfo->u4DestIP;
    u2BaseDestPort = pPinholeInfo->u2DestPort;

    u1TimerFlag = NAT_NOT_ON_HOLD;    /* This is unused by SIP. still needed for
                                     * NAT to identify the entries as SIP related */

    pNatAsyncResp->u4Cookie = u4Cookie;
    pNatAsyncResp->u4Status = NAT_SIP_SUCCESS;
    pNatAsyncResp->u4ErrorCode = NATIPC_NO_ERROR;

    u2LocalPort = u2BaseLocalPort;    /* Open pinhole dictates only the 
                                       OUT Port, IN Port would be 0 */
    u2DestPort = u2BaseDestPort;
    u2NumbOfPinholes = pPinholeInfo->u2NumPinholes;
    for (u2Count = 1; u2Count <= u2NumbOfPinholes; u2Count++, u2DestPort++)
    {

        if (NatApiHandleOpenPinhole
            (0, u4LocalIP, u2LocalPort, u2Protocol, u2Direction, u4DestIP,
             u2DestPort, u1TimerFlag, &u4ErrorCode) == NAT_FAILURE)
        {
            /* On failure, the u2DestPort and u4count would have the final values */
            /* clean all the pinholes which were created */
            for (; u2Count > 1; u2Count--)
            {
                u2DestPort--;    /* Get the previous OUT port */
                if (NatApiHandleClosePinhole (0, u4LocalIP, u2LocalPort,
                                              u2Protocol, u2Direction, u4DestIP,
                                              u2DestPort, u1TimerFlag,
                                              &u4CloseErrorCode) == NAT_FAILURE)
                {
                    NAT_TRC (NAT_TRC_ON,
                             "\n NatSipAlgOpenPinhole()"
                             " :Error in closing the opened pinhole\r\n");
                }
            }

            if (u4ErrorCode == NATIPC_PINHOLE_ALREADY_EXISTS ||
                u4ErrorCode == NATIPC_INTERNAL_ERROR)
            {
                pNatAsyncResp->u4ErrorCode = u4ErrorCode;
                pNatAsyncResp->u4Status = NAT_SIP_FAILURE;
            }
            /* if this api is used as synchronous */
            return;
        }
    }

    eBaseParity = pPinholeInfo->u1Parity;
    if (NatAddWanUaPortHashEntry (u4DestIP, u2BaseDestPort,
                                  u2NumbOfPinholes, eBaseParity,
                                  u2Protocol) == NAT_FAILURE)
    {
        /* On failure, the u2DestPort and u4count would have the final values */
        /* clean all the pinholes which were created */
        u2DestPort--;            /* Get the previous OUT port, as that is the last successful pinhole opened */

        for (u2Count = 1; u2Count <= u2NumbOfPinholes; u2Count++, u2DestPort--)
        {
            if (NatApiHandleClosePinhole (0, u4LocalIP, u2LocalPort,
                                          u2Protocol, u2Direction, u4DestIP,
                                          u2DestPort, u1TimerFlag,
                                          &u4CloseErrorCode) == NAT_FAILURE)
            {
                NAT_TRC (NAT_TRC_ON, "NatSipAlgOpenPinhole() : "
                         "Error in closing the opened pinhole\r\n");

            }
        }
        pNatAsyncResp->u4ErrorCode = NATIPC_INTERNAL_ERROR;
        pNatAsyncResp->u4Status = NAT_SIP_FAILURE;

    }

    return;
}

/******************************************************************************
 *                                                                            *
 * Function     : NatSipAlgClosePinhole                                         *
 *                                                                            *
 * Description  : This funtion is used to close a pinhole in firewall for ip  *
 *                and port combination                                        *
 *                                                                            *
 * Input        : pPinholeInfo - Pin Hole information                         *
 *                u4Cookie     - Unique Identifier                            *
 *                                                                            *
 * Output       : None                                                        *
 *                                                                            *
 * Return       : OSIX_SUCCESS/OSIX_FAILURE                                   *
 *                                                                            *
 ******************************************************************************/

PUBLIC VOID
NatSipAlgClosePinhole (tPinholeInfo * pPinholeInfo, UINT4 u4Cookie,
                       tNatAsyncResponse * pNatAsyncResp)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4LocalIP = 0;
    UINT4               u4DestIP = 0;
    UINT2               u2Direction = 0;
    UINT2               u2Protocol = 0;
    UINT2               u2DestPort = 0;
    UINT2               u2LocalPort = 0;
    UINT2               u2NumPinholes = 0;    /* Num Of PinHoles Recvd Frm SIP */
    UINT2               u2Loop = 0;
    UINT1               u1TimerFlag = 0;

    u4LocalIP = pPinholeInfo->u4SrcIP;
    u2LocalPort = pPinholeInfo->u2SrcPort;

    if (pPinholeInfo->u2Protocol == NAT_SIP_TCP)
    {
        u2Protocol = NAT_TCP;
    }
    else if (pPinholeInfo->u2Protocol == NAT_SIP_UDP)
    {
        u2Protocol = NAT_UDP;
    }
    else if (pPinholeInfo->u2Protocol == NAT_SIP_ANY)
    {
        u2Protocol = NAT_PROTO_ANY;
    }

    if (pPinholeInfo->u2Direction == NAT_INBOUND)
    {
        u2Direction = NAT_INBOUND;
    }
    else if (pPinholeInfo->u2Direction == NAT_OUTBOUND)
    {
        u2Direction = NAT_OUTBOUND;
    }

    u4DestIP = pPinholeInfo->u4DestIP;
    u2DestPort = pPinholeInfo->u2DestPort;
    u2NumPinholes = pPinholeInfo->u2NumPinholes;

    u1TimerFlag = NAT_NOT_ON_HOLD;    /* This is unused by SIP. still needed for
                                     * NAT to identify the entries as SIP related */
    pNatAsyncResp->u4Cookie = u4Cookie;
    pNatAsyncResp->u4Status = NAT_SIP_SUCCESS;
    pNatAsyncResp->u4ErrorCode = 0;
    pNatAsyncResp->u4RetVal1 = 0;
    pNatAsyncResp->u4RetVal2 = 0;

    if (NatDelWanUaPortHashEntry (u4DestIP, u2DestPort, u2NumPinholes,
                                  u2Protocol) == NAT_FAILURE)
    {
        NAT_TRC (NAT_TRC_ON, "NatSipAlgClosePinhole() : "
                 "Failed to delete corresponding node"
                 " in WanUaPortHashEntry\r\n");
    }
    for (u2Loop = 0; u2Loop < u2NumPinholes; u2Loop++)
    {
        if (NatApiHandleClosePinhole (0, u4LocalIP, u2LocalPort,
                                      u2Protocol, u2Direction, u4DestIP,
                                      u2DestPort, u1TimerFlag, &u4ErrorCode)
            == NAT_FAILURE)
        {
            pNatAsyncResp->u4Status = NAT_SIP_FAILURE;
            if (u4ErrorCode == NATIPC_PINHOLE_NOT_EXISTS)
            {
                pNatAsyncResp->u4ErrorCode = NATIPC_PINHOLE_NOT_EXISTS;
            }
            else
            {
                pNatAsyncResp->u4ErrorCode = NATIPC_INTERNAL_ERROR;
            }
        }
        u2DestPort = (UINT2) (u2DestPort + 1);
    }

    return;
}

PUBLIC VOID
NatSipAlgDeleteDynamicEntry (tNatEntryInfo * pNatEntryInfo, UINT4 u4Cookie,
                             tNatAsyncResponse * pNatAsyncResp)
{
    UINT4               u4LocalIP = 0;
    UINT4               u4TransIP = 0;
    UINT4               u4IntfNum = 0;
    INT4                i4Protocol = 0;
    UINT2               u2TransPort = 0;
    UINT2               u2LocalPort = 0;
    UINT4               u4RemoteIP = 0;
    UINT2               u2RemotePort = 0;

    u4TransIP = pNatEntryInfo->u4NATIP;
    u4LocalIP = pNatEntryInfo->u4SrcIP;
    u2TransPort = pNatEntryInfo->u2NATPort;
    u2LocalPort = pNatEntryInfo->u2SrcPort;
    u4RemoteIP = pNatEntryInfo->u4DestIP;
    u2RemotePort = pNatEntryInfo->u2DestPort;
    i4Protocol = (INT4) pNatEntryInfo->u2Protocol;

    pNatAsyncResp->u4Cookie = u4Cookie;
    pNatAsyncResp->u4Status = NAT_SIP_SUCCESS;
    pNatAsyncResp->u4ErrorCode = 0;
    pNatAsyncResp->u4RetVal1 = 0;
    pNatAsyncResp->u4RetVal2 = 0;

    if (SecUtilIpIfGetIfIndexFromIpAddress (u4TransIP, &u4IntfNum) ==
        NAT_FAILURE)
    {
        pNatAsyncResp->u4Status = NAT_SIP_FAILURE;
        pNatAsyncResp->u4ErrorCode = NATIPC_NAT_INTERNAL_ERROR;

        return;
    }

    if (NatDeleteSpecifiedEntry (u4IntfNum, u4LocalIP, u4TransIP,
                                 u2LocalPort, u2TransPort) == NAT_FAILURE)
    {
        pNatAsyncResp->u4Status = NAT_SIP_FAILURE;
        pNatAsyncResp->u4ErrorCode = NATIPC_INTERNAL_ERROR;
    }
#ifdef FIREWALL_WANTED
    FwlAppDeleteStateTableEntry (u4LocalIP, u2LocalPort, u4RemoteIP,
                                 u2RemotePort, (UINT2) i4Protocol);
#endif /*  FIREWALL_WANTED */

    return;
}

PUBLIC VOID
NatSipAlgEventNotificationHandler (tEventNotificationInfo *
                                   pEventNotificationInfo)
{
    UNUSED_PARAM (pEventNotificationInfo);
    /* Handler Function Body SHould be defined Here Only */
    /* pEventNotificationInfo should be copied to a local structure variable of
       tEventNotificationInfo type since after return from the function buffer 
       will be freed  */
}

/****************************************************************************
*                                                                           *
* Function     : NatSipAlgSendPortMapEvent                                         *
*                                                                           *
* Description  : This function will send port mapping event to SIP          *
*                                                                           *
* Input        : u4LocalIp   - Local IP Address                             *
*                u2LocalPort - Local Port                                   *
*                u4TransIp   - Translated IP Address                        *
*                u2TransPort - Translated Port                              *
*                u2Protocol  - Protocol identifier                          *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
NatSipAlgSendPortMapEvent (UINT4 u4LocalIp, UINT2 u2LocalPort, UINT4 u4TransIp,
                           UINT2 u2TransPort, UINT2 u2Protocol)
{
    tEventNotificationInfo EventNotificationInfo;
    tNatEntryInfo      *pNatEntryInfo = NULL;

    MEMSET (&EventNotificationInfo, 0, sizeof (tEventNotificationInfo));
    /* Extrating the data pointers */
    pNatEntryInfo = &(EventNotificationInfo.EventInfo.natEntryInfo);

    /* Filling the data */
    EventNotificationInfo.u2EventType = IPC_PORT_MAPPING_EXPIRY_EVENT;
    pNatEntryInfo->u4SrcIP = OSIX_HTONL (u4LocalIp);
    pNatEntryInfo->u2SrcPort = u2LocalPort;
    pNatEntryInfo->u4NATIP = OSIX_HTONL (u4TransIp);
    pNatEntryInfo->u2NATPort = u2TransPort;
    pNatEntryInfo->u2Protocol = u2Protocol;

    NatSipAlgEventNotificationHandler (&EventNotificationInfo);
    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : NatSipAlgSendLinkStatusEvent                               *
*                                                                           *
* Description  : This function will link status event to SIP                *
*                                                                           *
* Input        : u4IpAddr  - Interface ip address                           *
*                u1NwType  - Network type (WAN/LAN)                         *
*                pu1IfName - Interface Name                                 *
*                u4Status  - Interface status                               *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
NatSipAlgSendLinkStatusEvent (UINT4 u4IpAddr, UINT1 u1NwType,
                              UINT1 *pu1IfName, UINT4 u4Status)
{
    tEventNotificationInfo EventNotificationInfo;
    tLinkInfo          *pLinkInfo = NULL;

    MEMSET (&EventNotificationInfo, 0, sizeof (tEventNotificationInfo));

    /* Extrating the data pointers */
    pLinkInfo = &(EventNotificationInfo.EventInfo.linkInfo);

    /* Filling the data */
    EventNotificationInfo.u2EventType = IPC_LINK_STATUS_EVENT;
    pLinkInfo->u4IpAddr = OSIX_HTONL (u4IpAddr);

    /* Filling the network type */
    if (u1NwType == CFA_NETWORK_TYPE_LAN)
    {
        pLinkInfo->u2LinkType = CFA_NETWORK_TYPE_LAN;
    }
    else if (u1NwType == CFA_NETWORK_TYPE_WAN)
    {
        pLinkInfo->u2LinkType = CFA_NETWORK_TYPE_WAN;
    }

    /* Filling the network status */
    if (u4Status == ACTIVE)
    {
        pLinkInfo->u2Status = NAT_SIP_LINK_STATUS_UP;
    }
    else if (u4Status == NOT_IN_SERVICE)
    {
        pLinkInfo->u2Status = NAT_SIP_LINK_STATUS_DOWN;
    }
    else if (u4Status == CREATE_AND_WAIT)
    {
        pLinkInfo->u2Status = NAT_SIP_LINK_STATUS_NOT_CONFIGURED;
    }

    MEMSET (&(pLinkInfo->au1LinkPort[0]), 0, 20);
    MEMCPY (&(pLinkInfo->au1LinkPort[0]), pu1IfName, STRLEN (pu1IfName));

    NatSipAlgEventNotificationHandler (&EventNotificationInfo);
    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : NatSipAlgSendLinkStatusAdd                                 *
*                                                                           *
* Description  : This function will send Link add event to SIP              *
*                                                                           *
* Input        : u4IpAddr  - Interface ip address                           *
*                u1NwType  - Network type (WAN/LAN)                         *
*                pu1IfName - Interface Name                                 *
*                u4Status  - Interface status                               *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
NatSipAlgSendLinkStatusAdd (UINT4 u4IpAddr, UINT1 u1NwType, UINT1 *pu1IfName)
{
    tEventNotificationInfo EventNotificationInfo;
    tLinkInfo          *pLinkInfo = NULL;

    MEMSET (&EventNotificationInfo, 0, sizeof (tEventNotificationInfo));
    /* Extrating the data pointers */
    pLinkInfo = &(EventNotificationInfo.EventInfo.linkInfo);

    /* Filling the data */
    EventNotificationInfo.u2EventType = IPC_LINK_STATUS_ADDED;
    pLinkInfo->u4IpAddr = OSIX_HTONL (u4IpAddr);

    /* Filling the network type */
    if (u1NwType == CFA_NETWORK_TYPE_LAN)
    {
        pLinkInfo->u2LinkType = CFA_NETWORK_TYPE_LAN;
    }
    else if (u1NwType == CFA_NETWORK_TYPE_WAN)
    {
        pLinkInfo->u2LinkType = CFA_NETWORK_TYPE_WAN;
    }

    /* Filling the network status */
    pLinkInfo->u2Status = NAT_SIP_LINK_STATUS_UP;

    MEMSET (&(pLinkInfo->au1LinkPort[0]), 0, 20);
    MEMCPY (&(pLinkInfo->au1LinkPort[0]), pu1IfName, STRLEN (pu1IfName));

    NatSipAlgEventNotificationHandler (&EventNotificationInfo);
    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : NatSipAlgSendLinkStatusDelete                                     *
*                                                                           *
* Description  : This function will send link delete event to SIP           *
*                                                                           *
* Input        : u4IpAddr  - Interface ip address                           *
*                u1NwType  - Network type (WAN/LAN)                         *
*                pu1IfName - Interface Name                                 *
*                u4Status  - Interface status                               *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
NatSipAlgSendLinkStatusDelete (UINT4 u4IpAddr, UINT1 u1NwType, UINT1 *pu1IfName)
{
    tEventNotificationInfo EventNotificationInfo;
    tLinkInfo          *pLinkInfo = NULL;

    MEMSET (&EventNotificationInfo, 0, sizeof (tEventNotificationInfo));

    /* Extrating the data pointers */
    pLinkInfo = &(EventNotificationInfo.EventInfo.linkInfo);

    /* Filling the data */
    EventNotificationInfo.u2EventType = IPC_LINK_STATUS_DELETED;
    pLinkInfo->u4IpAddr = OSIX_HTONL (u4IpAddr);

    /* Filling the network type */
    if (u1NwType == CFA_NETWORK_TYPE_LAN)
    {
        pLinkInfo->u2LinkType = CFA_NETWORK_TYPE_LAN;
    }
    else if (u1NwType == CFA_NETWORK_TYPE_WAN)
    {
        pLinkInfo->u2LinkType = CFA_NETWORK_TYPE_WAN;
    }

    /* Filling the network status */
    pLinkInfo->u2Status = NAT_SIP_LINK_STATUS_DOWN;

    MEMSET (&(pLinkInfo->au1LinkPort[0]), 0, 20);
    MEMCPY (&(pLinkInfo->au1LinkPort[0]), pu1IfName, STRLEN (pu1IfName));

    NatSipAlgEventNotificationHandler (&EventNotificationInfo);

    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : NatSipAlgSendPinHoleExpiryEvent                                   *
*                                                                           *
* Description  : This function will send Pin Hole Expiry event to SIP       *
*                                                                           *
* Input        : u4LocalIp   - Local IP Address                             *
*                u2LocalPort - Local Port                                   *
*                u4TransIp   - Translated IP Address                        *
*                u2TransPort - Translated Port                              *
*                u2Protocol  - Protocol identifier                          *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
NatSipAlgSendPinHoleExpiryEvent (UINT4 u4SrcIp, UINT2 u2SrcPort, UINT4 u4DestIp,
                                 UINT2 u2DestPort, UINT2 u2Protocol,
                                 UINT2 u2Direction)
{
    tEventNotificationInfo EventNotificationInfo;
    tPinholeInfo       *pPinHoleInfo = NULL;

    MEMSET (&EventNotificationInfo, 0, sizeof (tEventNotificationInfo));

    /* Extrating the data pointers */
    pPinHoleInfo = &(EventNotificationInfo.EventInfo.pinholeInfo);

    /* Filling the data */
    EventNotificationInfo.u2EventType = IPC_PINHOLE_EXPIRY_EVENT;
    pPinHoleInfo->u4SrcIP = OSIX_HTONL (u4SrcIp);
    pPinHoleInfo->u2SrcPort = u2SrcPort;
    pPinHoleInfo->u4DestIP = OSIX_HTONL (u4DestIp);
    pPinHoleInfo->u2DestPort = u2DestPort;
    pPinHoleInfo->u2Protocol = u2Protocol;
    if (u2Direction == NAT_INBOUND)
    {
        pPinHoleInfo->u2Direction = NAT_INBOUND;
    }
    else if (u2Direction == NAT_OUTBOUND)
    {
        pPinHoleInfo->u2Direction = NAT_OUTBOUND;
    }

    NatSipAlgEventNotificationHandler (&EventNotificationInfo);
    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : NatSipAlgSendDynEntryEvent                              *
*                                                                           *
* Description  : This function will send port mapping event to SIP          *
*                                                                           *
* Input        : u4LocalIp   - Local IP Address                             *
*                u2LocalPort - Local Port                                   *
*                u4DestIp    - Remote IP                                    *
*                u2DestPort  - Remote Port                                  *
*                u4TransIp   - Translated IP Address                        *
*                u2TransPort - Translated Port                              *
*                u2Protocol  - Protocol identifier                          *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
NatSipAlgSendDynEntryEvent (UINT4 u4LocalIp, UINT2 u2LocalPort,
                            UINT4 u4TransIp, UINT2 u2TransPort,
                            UINT4 u4DestIp, UINT2 u2DestPort,
                            UINT2 u2Protocol, UINT2 u2EventType)
{
    tEventNotificationInfo EventNotificationInfo;
    tNatEntryInfo      *pNatEntryInfo = NULL;

    MEMSET (&EventNotificationInfo, 0, sizeof (tEventNotificationInfo));

    /* Extrating the data pointers */
    pNatEntryInfo = &(EventNotificationInfo.EventInfo.natEntryInfo);

    /* Filling the data */
    EventNotificationInfo.u2EventType = u2EventType;
    pNatEntryInfo->u4SrcIP = OSIX_HTONL (u4LocalIp);
    pNatEntryInfo->u2SrcPort = u2LocalPort;
    pNatEntryInfo->u4NATIP = OSIX_HTONL (u4TransIp);
    pNatEntryInfo->u2NATPort = u2TransPort;
    pNatEntryInfo->u4DestIP = OSIX_HTONL (u4DestIp);
    pNatEntryInfo->u2DestPort = u2DestPort;
    pNatEntryInfo->u2Protocol = u2Protocol;

    NatSipAlgEventNotificationHandler (&EventNotificationInfo);

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : NatSipUpdateInterfaceStatus
 *
 *    Description          : This function is for updating the status of
 *                           the interface to Nat.
 *
 *    Input(s)            : u4IfIndex - Interface Index
 *                          u4IpAddr - Interface IP
 *                          u1NwType - Interface Type (LAN or WAN)
 *                          pu1IfName - Interface Name
 *                          u4Status - Interface Row Status
 *
 *    Output(s)            : None.
 *
 *
 *    Returns              : None.
 *
 *****************************************************************************/
VOID
NatSipUpdateInterfaceStatus (UINT2 u4IfIndex, UINT4 u4IpAddr, UINT1 u1NwType,
                             UINT1 *pu1IfName, UINT4 u4Status)
{
    UNUSED_PARAM (u4IfIndex);

    if (u4Status == CREATE_AND_GO)
    {
        NatSipAlgSendLinkStatusAdd (u4IpAddr, u1NwType, pu1IfName);
    }
    else if (u4Status == DESTROY)
    {
        NatSipAlgSendLinkStatusDelete (u4IpAddr, u1NwType, pu1IfName);
    }
    else
    {
        NatSipAlgSendLinkStatusEvent (u4IpAddr, u1NwType, pu1IfName, u4Status);
    }
}
