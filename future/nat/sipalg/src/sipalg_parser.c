/********************************************************************
 * Copyright (C) Aricent Communications Sotware,2007
 *
 * $Id: sipalg_parser.c,v 1.7 2013/12/18 12:50:13 siva Exp $
 *
 *******************************************************************/

/***********************************************************************
** FUNCTION:
**            SIP Message Translation functionality
************************************************************************
**
** FILENAME:
** sipalg_parser.c
**
** DESCRIPTION:
**        This file contains the translation functions for SIP Msg.
**        This includes APIs and functions for translation of the 
**      signalling headers, sdp(media) and the xml body.
**
** DATE             NAME             REFERENCE         REASON
** ----             ----             ---------         --------
** 16/07/2007    Rashed Ahammad        -            Initial                    
**
**
**     Copyright (C) 2007 Aricent Inc . All Rights Reserved
***********************************************************************/

#include "natinc.h"
#include "sipalg_inc.h"
/***************************************************************************
** FUNCTION: sipAlgTranslateSipMsg
** DESCRIPTION: This function is the interface to NAT module to perform the 
**              SIP msg translation for both incoming and outgoing packet.
**              This function is responsible for translating the entire msg 
**
** PARAMETERS:
**      pSipBuf(IN)                - Pointer to SIP buffer 
**        direction(IN)            - specifies whether this is OUT/IN Msg 
**        pNatHeaderInfo(IN)       - pointer to tHeaderInfo which has NAT info 
**        ppTranslatedSipBuf(OUT) - Translated SIP Buffer 
**
** Returns   : SUCCESS/FAILURE
***************************************************************************/

SIP_U8bit
sipAlgTranslateSipMsg (SIP_U8bit * pSipBuf, SipAlgPktInOut direction,
                       tHeaderInfo * pNatHeaderInfo,
                       SIP_U8bit ** ppTranslatedSipBuf, SIP_U32bit dSipBufLen)
{
    /* This pointer is used to avoid strict-aliasing warning */
    SIP_Pvoid           pTemp = SIP_NULL;
    SipMessage         *pParsedMsg = SIP_NULL;
    SipOptions          dOpt;
    SIP_S8bit          *pNextMsg = SIP_NULL;
    SipError            dError;
    SipBool             dRetVal;
    SIP_U32bit          dLength = 0;
    SipEventContext     dEventContext;
    SIP_S8bit          *pReqMethod = SIP_NULL;
    SIP_S8bit          *pRespMethod = SIP_NULL;
    SIP_U16bit          dRespCode = 0;
    en_SipBoolean       isTranslateMediaInRequest = SipFalse;
    en_SipBoolean       isTranslateMediaInResponse = SipFalse;

    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgTranslateSipMsg");

    /* first decode the sip msg then depending on the
       direction invoke the corresponding header,msgbody functions
       finally, form the buffer again from the sip msg */
    /* The option SIP_OPT_CLEN is set, with this option
       the content-length header will be updated incase 
       of modifications in sdp */
#ifdef SIPALG_UT_FLAG
/* memory leak fix start */
    SIP_S8bit           pBuf[dSipBufLen];
    sip_snprintf (pBuf, dSipBufLen, "%s", pSipBuf);
    sip_printf ("\n\nthe received sip msg at sipalg is:\n\n%s\n\n", pBuf);
/* memory leak fix end */
#endif
    dOpt.dOption = SIP_OPT_CLEN | SIP_OPT_NOTIMER;
    /* call the stack API to decode the message */
#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {
        Sdf_fn_printTimeStamp
            ("before calling sip_decodeMessage in sipAlgTranslateSipMsg");
    }
#endif

/* memory leak fix start */
    dRetVal = sip_decodeMessage ((SIP_S8bit *) pSipBuf, &pParsedMsg, &dOpt,
                                 dSipBufLen, &pNextMsg, &dEventContext,
                                 &dError);
/* memory leak fix end */
#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {
        Sdf_fn_printTimeStamp
            ("after calling sip_decodeMessage in sipAlgTranslateSipMsg");
    }
#endif
    if ((dRetVal == SipFail) || (pParsedMsg == SIP_NULL))
    {
        sip_error (SIP_Major,
                   " In sipAlgTranslateSipMsg: sip_decodeMessage failed\n");
        return NAT_SUCCESS;
    }

    if (direction == SipPktOut)
    {
        if (sipAlgTranslateOutgoingHeaders (pParsedMsg, pNatHeaderInfo) ==
            SipFail)
        {
            sip_error (SIP_Major,
                       " In sipAlgTranslateSipMsg:sipAlgTranslateOutgoingHeaders failed\n");
            sip_freeSipMessage (pParsedMsg);
            return NAT_FAILURE;
        }
    }
    else if (direction == SipPktIn)
    {
        if (sipAlgTranslateIncomingHeaders (pParsedMsg, pNatHeaderInfo) ==
            SipFail)
        {
            sip_error (SIP_Major,
                       " In sipAlgTranslateSipMsg: sipAlgTranslateIncomingHeaders failed\n");
            sip_freeSipMessage (pParsedMsg);
            return NAT_FAILURE;
        }
    }
    /* Get the method name from sip request message */
    if (sipAlgGetMethodFromSipReqest (pParsedMsg, &pReqMethod) == SipFail)
    {
        sip_error (SIP_Major,
                   "In sipAlgTranslateSipMsg:sipAlgGetMethodFromSipReqest failed\n");
        sip_freeSipMessage (pParsedMsg);
        return NAT_FAILURE;
    }
    /* Get the method name and response code from sip response message */
    if (sipAlgGetMethodAndRespCodeFromSipResponse
        (pParsedMsg, &pRespMethod, &dRespCode) == SipFail)
    {
        sip_error (SIP_Major, "In sipAlgTranslateSipMsg:\
                   sipAlgGetRespCodeFromSipInviteResponse failed\n");
        sip_freeSipMessage (pParsedMsg);
        return NAT_FAILURE;
    }

    /* First we need to check whether we need to translate SDP or not.
       For this, we need to see whether the SIP message is
       INVITE/ACK/UPDATE/PRACK request or 
       1xx/2xx response to INVITE or
       2xx response to UPDATE/PRACK */

    if ((pReqMethod != SIP_NULL) &&
        ((sip_strcasecmp (pReqMethod, "INVITE") == 0) ||
         (sip_strcasecmp (pReqMethod, "ACK") == 0) ||
         (sip_strcasecmp (pReqMethod, "UPDATE") == 0) ||
         (sip_strcasecmp (pReqMethod, "PRACK") == 0)))
    {
        isTranslateMediaInRequest = SipTrue;
    }
    if ((pRespMethod != SIP_NULL) && (    /* if is is 1xx/2xx for INVITE or 2xx for UPDATE/PRACK */
                                         ((sip_strcasecmp
                                           (pRespMethod, "INVITE") == 0)
                                          && ((dRespCode >= 100)
                                              && (dRespCode <= 299)))
                                         ||
                                         ((sip_strcasecmp
                                           (pRespMethod, "UPDATE") == 0)
                                          && ((dRespCode >= 200)
                                              && (dRespCode <= 299)))
                                         ||
                                         ((sip_strcasecmp (pRespMethod, "PRACK")
                                           == 0) && ((dRespCode >= 200)
                                                     && (dRespCode <= 299)))))
    {
        isTranslateMediaInResponse = SipTrue;
    }
    if ((isTranslateMediaInRequest == SipTrue)
        || (isTranslateMediaInResponse == SipTrue))
    {
        if (sipAlgTranslateMedia (pParsedMsg, direction, pNatHeaderInfo) ==
            SipFail)
        {
            sip_error (SIP_Major,
                       " In sipAlgTranslateSipMsg: sipAlgTranslateMedia failed\n");
            sip_freeSipMessage (pParsedMsg);
            return NAT_FAILURE;
        }
    }

    /* 
       In order to handle the forking scenarios, if a failure resp is received
       the entries are not deleted. These entriesi

       Issue: In case of forking suppose B2B forks and for which first a 4xx 
       response is received and then 2xx response. On receiving a 4xx reponse, 
       the SDP entries of the caller are removed (Close the pinhole and remove 
       NAT binding). On receiving a 2xx response only pinholes will be created. 
       This will not allow the RTP to flow

       So now on receiving a failure response don't take any action. 
       In case it is a normal call for which a failure response is given, 
       (INVITE - 4xx - ACK). Even in this case the entries will not be deleted. 
       But these entries will get deleted after SIP ALG timer expiry.
     */
    /* On receiving Bye, we need to clear the dynamic entries for media. 
       But since if Bye is generated from WAN, before it reaches LAN side, 
       RTP can still flow and create one more dynamic entry.(Generic behavior
       of NAT, for any outgoing packet, create dynamic entry). Hence clear the 
       Dynamic entries on receiving 200 Ok for Bye. */
    if ((pRespMethod != SIP_NULL) && (sip_strcasecmp (pRespMethod, "BYE")
                                      == 0) && (dRespCode == 200))
    {
        SipAlgMediaKey     *pDialogKey = SIP_NULL;
#ifndef SECURITY_KERNEL_WANTED
#ifdef SDF_TIMESTAMP
        if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
        {
            Sdf_fn_writeToFile ();
        }
#endif
#endif

        /* allocate memory for dialogparams */
        pDialogKey = (SipAlgMediaKey *) sip_memget
            (0, sizeof (SipAlgMediaKey), &dError);
        if (pDialogKey == SIP_NULL)
        {
            sip_error (SIP_Minor,
                       " In sipAlgTranslateMedia:Memory allocation failed\n");
            pTemp = pDialogKey;
            sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
            pDialogKey = SIP_NULL;
            sip_freeSipMessage (pParsedMsg);
            return NAT_FAILURE;
        }
        /* Get the dialog params from sip msg */
        if (sipAlgGetDialogParamFromSipMsg (pParsedMsg, pDialogKey) == SipFail)
        {
            sip_error (SIP_Major,
                       " In sipAlgTranslateSipMsg:sipAlgGetDialogParamFromSipMsg failed\n");
            pTemp = pDialogKey;
            sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
            pDialogKey = SIP_NULL;
            sip_freeSipMessage (pParsedMsg);
            return NAT_FAILURE;
        }
        /* invoke the function to delete bindings and pinholes */
        sipAlgDeleteNatBindingsNPinholes (pDialogKey);
        /* Free the dialog param related memebers */
        sipAlgFreeDialogParam (&pDialogKey);
    }

    if ((gu4NatEnable == NAT_DISABLE) ||
        (NatCheckIfNatEnable (pNatHeaderInfo->u4IfNum) == NAT_DISABLE))
    {
        /* In case there is no NAT on that interace or there is no NAT on
           any interface, then don't change the SIP packet. Then 
           ppTranslatedSipBuf will be NULL and no translation will be done */
        sip_freeSipMessage (pParsedMsg);
        SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgTranslateSipMsg ");
        return NAT_SUCCESS;
    }
    /* now form the buffer from the translted sip msg structure */
    /* first allocate memory to the out buffer */
    *ppTranslatedSipBuf = (SIP_U8bit *) sip_memget
        (0, SIP_MAX_MSG_SIZE, &dError);

    if (*ppTranslatedSipBuf == SIP_NULL)
    {
        sip_error (SIP_Major,
                   " In sipAlgTranslateSipMsg:Failed to allocate memory \n");
        sip_freeSipMessage (pParsedMsg);
        return NAT_FAILURE;        /* no memory avalble */
    }
    dOpt.dOption = SIP_OPT_CLEN | SIP_OPT_FULLFORM;
#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {
        Sdf_fn_printTimeStamp
            ("before calling sip_formMessage in sipAlgTranslateSipMsg");
    }
#endif

    if (sip_formMessage
        (pParsedMsg, &dOpt, (SIP_S8bit *) * ppTranslatedSipBuf, &dLength,
         &dError) == SipFail)
    {
        sip_error (SIP_Major,
                   " In sipAlgTranslateSipMsg:sip_formMessage failed\n");
        sip_memfree (0, (SIP_Pvoid *) ppTranslatedSipBuf, SIP_NULL);
        sip_freeSipMessage (pParsedMsg);
        return NAT_FAILURE;
    }
#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {
        Sdf_fn_printTimeStamp
            ("after calling sip_formMessage in sipAlgTranslateSipMsg");
    }
#endif

    sip_freeSipMessage (pParsedMsg);
#ifdef SIPALG_UT_FLAG
    PRINTF ("\n\nthe trans sip msg at sipalg is:\n\n%s\n\n",
            *ppTranslatedSipBuf);
    PRINTF
        ("XXXXXXXXXXXXXXXXXXX NAT HASH MEDIA HASH TABLESXXXXXXXXXXXXXXXXXX\n");
    sipAlgNatHashPrint (&dGlbNatHash);
    /* SIPALG - 2.1 */
    sipAlgIPHashPrint (&dGlbIPHash);
    sipAlgMediaHashPrint (&dGlbMediaHash);
    PRINTF ("XXXXXXXXXXXXXXXXXXXX THE END XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
#endif
    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgTranslateSipMsg ");
    return NAT_SUCCESS;
}

/****************************************************************************
** FUNCTION: sipAlgTranslateIncomingHeaders
** DESCRIPTION: This function translates the incoming msg headers
**
** PARAMETERS:
**      pSipMsg(IN)                - Pointer to SIP Msg 
**        pNatHeaderInfo(IN)       - pointer to tHeaderInfo which has NAT info 
**
**Returns : SUCCESS/FAILURE 
*****************************************************************************/

SipBool
sipAlgTranslateIncomingHeaders (SipMessage * pSipMsg,
                                tHeaderInfo * pNatHeaderInfo)
{
    SipError            dError;

    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgTranslateIncomingHeaders");

    if (pSipMsg->dType == SipMessageRequest)
    {
        /* if the incoming message is a request then translation needs to be 
           performed only on Request URI and Route header if present */
        if (sipAlgTranslateReqUri (pSipMsg, SipPktIn, pNatHeaderInfo) ==
            SipFail)
        {
            sip_error (SIP_Minor, " In sipAlgTranslateIncomingHeaders:\
                       sipAlgTranslateReqUri failed\n");
            return SipFail;
        }
        if (sipAlgTranslateRouteHdr (pSipMsg, SipPktIn, pNatHeaderInfo) ==
            SipFail)
        {
            sip_error (SIP_Minor, " In sipAlgTranslateIncomingHeaders:\
                       sipAlgTranslateRouteHdr failed\n");
            return SipFail;
        }
        /* To support intra-site calls, even the contacts needs to be
           translated back. [UA - B2B - ALG - SSE - ALG (translate contact in 
           request) - B2B - UA] */
        if (sipAlgTranslateContactHdr
            (pSipMsg, SipPktIn, pNatHeaderInfo, SipFalse, SipTrue) == SipFail)
        {
            sip_error (SIP_Minor, "In sipAlgTranslateIncomingHeaders: \
                       sipAlgTranslateContactHdr failed\n");
            return SipFail;
        }

    }
    else if (pSipMsg->dType == SipMessageResponse)
    {
        SipHeader           header;
        SIP_S8bit          *pTemp = SIP_NULL;

        /* if the incoming message is a response then translation needs to be 
           performed only on Via, Record-Route if present and contact if it is
           a response to REGISTER */

        if (sipAlgTranslateViaHdr (pSipMsg, SipPktIn, pNatHeaderInfo) ==
            SipFail)
        {
            sip_error (SIP_Minor, " In sipAlgTranslateIncomingHeaders:\
                       sipAlgTranslateViaHdr failed\n");
            return SipFail;
        }

        if (sipAlgTranslateRecordRouteHdr (pSipMsg, SipPktIn, pNatHeaderInfo) ==
            SipFail)
        {
            sip_error (SIP_Minor, " In sipAlgTranslateIncomingHeaders:\
                       sipAlgTranslateRecordRouteHdr failed\n");
            return SipFail;
        }

        /* check whether this is a response to REGISTER, */
        if (sip_getHeader (pSipMsg, SipHdrTypeCseq, &header,
                           &dError) == SipFail)
        {
            sip_error (SIP_Minor, "In sipAlgTranslateIncomingHeaders:\
                       Getting Cseq header failed\n");
            return SipFail;
        }

        if (sip_getMethodFromCseqHdr (&header, &pTemp, &dError) == SipFail)
        {
            sip_error (SIP_Minor, " In sipAlgTranslateIncomingHeaders:\
                       Getting method from CSeq header failed\n");
            sip_freeSipHeader (&header);
            return SipFail;
        }
        sip_freeSipHeader (&header);
        if (sip_strcasecmp (pTemp, "REGISTER") == 0)
        {
            /* The expiry value will be adjusted in REGISTER msg - SipTrue */
            if (sipAlgTranslateContactHdr
                (pSipMsg, SipPktIn, pNatHeaderInfo, SipTrue,
                 SipFalse) == SipFail)
            {
                sip_error (SIP_Minor, "In sipAlgTranslateIncomingHeaders: \
                           sipAlgTranslateContactHdr failed\n");
                return SipFail;
            }
        }
        else
        {
            /* To support intra-site calls, even the contacts needs to be
               translated back. (UA - B2B - ALG - SSE - ALG [translate resp now] 
               - B2B - UA) */
            if (sipAlgTranslateContactHdr
                (pSipMsg, SipPktIn, pNatHeaderInfo, SipFalse,
                 SipFalse) == SipFail)
            {
                sip_error (SIP_Minor, "In sipAlgTranslateIncomingHeaders: \
                           sipAlgTranslateContactHdr failed\n");
                return SipFail;
            }
        }
    }
    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgTranslateIncomingHeaders");
    return SipSuccess;
}

/****************************************************************************
** FUNCTION: sipAlgTranslateOutgoingHeaders
** DESCRIPTION: This function translates the outgoing msg headers
**
** PARAMETERS:
**      pSipMsg(IN)                - Pointer to SIP Msg 
**        pNatHeaderInfo(IN)       - pointer to tHeaderInfo which has NAT info 
**
**Returns : SUCCESS/FAILURE 
*****************************************************************************/

SipBool
sipAlgTranslateOutgoingHeaders (SipMessage * pSipMsg,
                                tHeaderInfo * pNatHeaderInfo)
{

    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgTranslateOutgoingHeaders ");

    /* the below block is for the outgoing message */
    if (pSipMsg->dType == SipMessageRequest)
    {
        /* for intra site */
        if (sipAlgTranslateReqUri (pSipMsg, SipPktOut, pNatHeaderInfo) ==
            SipFail)
        {
            sip_error (SIP_Minor, " In sipAlgTranslateOutgoingHeaders:\
                         sipAlgTranslateReqUri failed\n");
            return SipFail;
        }
        /* if the outgoing message is a request then translation needs to be 
           performed viahdr, contact and if present, the record-route header */
        if (sipAlgTranslateViaHdr (pSipMsg, SipPktOut, pNatHeaderInfo) ==
            SipFail)
        {
            sip_error (SIP_Minor, "In sipAlgTranslateOutgoingHeaders:\
                           sipAlgTranslateViaHdr failed\n");
            return SipFail;
        }
        if (sipAlgTranslateContactHdr
            (pSipMsg, SipPktOut, pNatHeaderInfo, SipFalse, SipTrue) == SipFail)
        {
            sip_error (SIP_Minor, "In sipAlgTranslateOutgoingHeaders:\
                     sipAlgTranslateContactHdr failed\n");
            return SipFail;
        }
        if (sipAlgTranslateRecordRouteHdr (pSipMsg, SipPktOut, pNatHeaderInfo)
            == SipFail)
        {
            sip_error (SIP_Minor, " In sipAlgTranslateOutgoingHeaders:\
                 sipAlgTranslateRecordRouteHdr failed\n");
            return SipFail;
        }
    }
    else if (pSipMsg->dType == SipMessageResponse)
    {
        /* if the outgoing message is a response then translation needs to be 
           performed only on contact hdr */
        if (sipAlgTranslateContactHdr
            (pSipMsg, SipPktOut, pNatHeaderInfo, SipFalse, SipFalse) == SipFail)
        {
            sip_error (SIP_Minor, "In sipAlgTranslateOutgoingHeaders:\
                        sipAlgTranslateContactHdr failed\n");
            return SipFail;
        }
    }
    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgTranslateOutgoingHeaders");
    return SipSuccess;
}

/****************************************************************************
** FUNCTION: sipAlgTranslateMedia
** DESCRIPTION: This function translates the media in sdp if the sdp body is 
**              present in the message
**
** PARAMETERS:
**      pSipMsg(IN)                - Pointer to SIP Msg 
**        direction(IN)            - specifies whether this is OUT/IN Msg
**        pNatHeaderInfo(IN)       - pointer to tHeaderInfo which has NAT info 
**
** Returns : SUCCESS/FAILURE 
*****************************************************************************/

SipBool
sipAlgTranslateMedia (SipMessage * pSipMsg, SipAlgPktInOut direction,
                      tHeaderInfo * pNatHeaderInfo)
{
    /* This pointer is used to avoid strict-aliasing warning */
    SIP_Pvoid           pTemp = SIP_NULL;
    SipError            dError;
    SIP_U32bit          i = 0, dContentLengthHdrIndex = 0;
    SipMsgBody         *pMsgBody = SIP_NULL;
    en_SipBoolean       sdpFound = SipFalse;
    SIP_U32bit          dCount = 0, dMediaCount = 0;
    SdpMessage         *pSdpMsg = SIP_NULL;
    SdpConnection      *pSessionConnection = SIP_NULL;
    SIP_U8bit          *pTransConnectionIP = SIP_NULL;
/* SIPALG - 2.1 */
    SIP_U32bit          retVal;
    SIP_U32bit          natIP = 0;
    SIP_U32bit          dSessionLevelCLineIP = 0;
    en_SipBoolean       isNatEnabled = SipTrue;
    en_SipBoolean       isCurrentMediaPortZero = SipFalse;
    en_SipBoolean       isAllMediaPortZero = SipTrue;
    SdpOrigin          *pOrigin = SIP_NULL;
    SIP_U8bit          *pOriginAddr = SIP_NULL;
    SIP_U32bit          dOriginAddr = 0;
    SipAlgMediaKey     *pSessionDialogKey = SIP_NULL;
    SIP_U32bit          dSessionTransIP = 0;
    SIP_U32bit          dOlineTransIP = 0;
    SipNatInfo         *pSessionInfo = SIP_NULL;
    SIP_S8bit          *pTransOlineIP = SIP_NULL;
    SIP_S8bit          *pVersionId = SIP_NULL;
    en_SipMessageType   dMessageType;
    en_SipBoolean       isRequest;
    /* For handling intra-site call */
    en_SipBoolean       dCLineIsWanIp = SipFalse;
    /* This flag will indicate if session level C line is valid */
    en_SipBoolean       dSessLevelCLineExistAndValid = SipTrue;
    SIP_S8bit          *pTempSessionIP = SIP_NULL;
    SIP_S8bit          *pSessionConnAddrType = SIP_NULL;

    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgTranslateMedia");

    /* This block of code is to find whether SDP msg body is present
       in the SIP message */
#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {
        Sdf_fn_printTimeStamp
            ("SipAlgTranslateMedia :Pre - GetMessageBodyCount()");
    }
#endif

    if (sip_getMsgBodyCount (pSipMsg, &dCount, &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateMedia:sip_getMsgBodyCount failed\n");
#ifdef SDF_TIMESTAMP
        if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
        {
            Sdf_fn_printTimeStamp
                ("SipAlgTranslateMedia :GetMessageBodyCount() - fail");
        }
#endif
        return SipFail;
    }

#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {
        Sdf_fn_printTimeStamp
            ("SipAlgTranslateMedia : Post - GetMsgBodyCount()");
    }
#endif

    if (dCount == 0)
    {
        SIPDEBUG ((SIP_S8bit *) "1. Exiting sipAlgTranslateMedia");
        return SipSuccess;
        /* No message body present in the sip message */
    }
    /* iterate thru all the msg bodies and get t he sdp body */
    for (i = 0; i < dCount; i++)
    {
#ifdef SDF_TIMESTAMP
        if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
        {
            Sdf_fn_printTimeStamp
                ("SipAlgTranslateMedia:Pre - getMsgBodyAtIndex");
        }
#endif

        if (sip_getMsgBodyAtIndex (pSipMsg, &pTemp, i, &dError) == SipFail)
        {
            sip_error (SIP_Minor,
                       " In sipAlgTranslateMedia:sip_getMsgBodyAtIndex failed\n");
#ifdef SDF_TIMESTAMP
            if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
            {
                Sdf_fn_printTimeStamp
                    ("SipAlgTranslateMedia: getMsgBodyAtIndex - fail");
            }
#endif
            return SipFail;
        }
#ifdef SDF_TIMESTAMP
        if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
        {

            Sdf_fn_printTimeStamp
                ("SipAlgTranslateMedia:Post - getMsgBodyAtIndex");
        }
#endif
        pMsgBody = pTemp;
        if (pMsgBody == NULL)
        {
            sip_error (SIP_Critical, "SipAlgGetMessageBody :pMsgBody is NULL");
            return SipFail;
        }
        if (pMsgBody->dType == SipSdpBody)
        {
            sdpFound = SipTrue;
            dContentLengthHdrIndex = i;
            break;
        }
        sip_freeSipMsgBody (pMsgBody);
    }
    /* if sdp is not present, return */
    if (sdpFound == SipFalse)
    {
        SIPDEBUG ((SIP_S8bit *) "2. Exiting sipAlgTranslateMedia");
        return SipSuccess;
    }

    /* get the SdpMessage struct */
#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {

        Sdf_fn_printTimeStamp ("SipAlgTranslateMedia:Pre - getSdpFromMsgBody");
    }
#endif
    pTemp = pSdpMsg;
    if (sip_getSdpFromMsgBody (pMsgBody, &pTemp, &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateMedia:sip_g etSdpFromMsgBody failed\n");
        sip_freeSipMsgBody (pMsgBody);
#ifdef SDF_TIMESTAMP
        if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
        {

            Sdf_fn_printTimeStamp
                ("SipAlgTranslateMedia: getSdpFromMsgBody - fail");
        }
#endif
        return SipFail;
    }
    pSdpMsg = pTemp;
#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {
        Sdf_fn_printTimeStamp ("SipAlgTranslateMedia:Post - getSdpFromMsgBody");
    }
#endif

    if (pSdpMsg == SIP_NULL)
    {
        sip_error (SIP_Minor, " In sipAlgTranslateMedia:pSdpMsg is null\n");
        sip_freeSipMsgBody (pMsgBody);
        return SipFail;
    }
    /* get the media count from SDP */
#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {

        Sdf_fn_printTimeStamp ("SipAlgTranslateMedia:Pre - getMediaCount - ");
    }
#endif
    if (sdp_getMediaCount (pSdpMsg, &dMediaCount, &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateMedia:sdp_getMediaCount failed\n");
        sip_freeSdpMessage (pSdpMsg);
        sip_freeSipMsgBody (pMsgBody);
#ifdef SDF_TIMESTAMP
        if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
        {

            Sdf_fn_printTimeStamp ("SipAlgTranslateMedia:getMediaCount - fail");
        }
#endif
        return SipFail;
    }
#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {

        Sdf_fn_printTimeStamp ("SipAlgTranslateMedia:Post - getMediaCount ");
    }
#endif

    /* get the connection line from the sdp-msg body */
#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {

        Sdf_fn_printTimeStamp ("SipAlgTranslateMedia:Pre - getConnection ");
    }
#endif

    pTemp = pSessionConnection;
    if (sdp_getConnection (pSdpMsg, &pTemp, &dError) == SipFail)
    {
#ifdef SDF_TIMESTAMP
        if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
        {

            Sdf_fn_printTimeStamp ("SipAlgTranslateMedia:getConnection - fail");
        }
#endif
        dSessLevelCLineExistAndValid = SipFalse;
    }
    pSessionConnection = pTemp;
#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {

        Sdf_fn_printTimeStamp ("SipAlgTranslateMedia:Post - getConnection ");
    }
#endif

    /* If session level C line is not present then the C line 
       should be present in all the media lines. If not return
       failure 
     */
    if (dSessLevelCLineExistAndValid == SipFalse)
    {
        SdpMedia           *pTempSdpMedia = SIP_NULL;
        SIP_U32bit          dCount1 = 0, u4Index = 0;
        for (u4Index = 0; u4Index < dMediaCount; u4Index++)
        {
            /* Get the media at the given index */
#ifdef SDF_TIMESTAMP
            if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
            {

                Sdf_fn_printTimeStamp
                    ("SipAlgTranslateMedia:Pre - getMediaAtIndex ");
            }
#endif
            pTemp = pTempSdpMedia;

            if (sdp_getMediaAtIndex (pSdpMsg, &pTemp, u4Index,
                                     &dError) == SipFail)
            {
                sip_error (SIP_Minor,
                           "In sipAlgTranslateMedia:sdp_getMediaAtIndex failed\n");
                sip_freeSdpMessage (pSdpMsg);
                sip_freeSipMsgBody (pMsgBody);
#ifdef SDF_TIMESTAMP
                if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
                {

                    Sdf_fn_printTimeStamp
                        ("SipAlgTranslateMedia: getMediaAtIndex - fail ");
                }
#endif
                return SipFail;
            }
            pTempSdpMedia = pTemp;
            /* Check if there is any connection line present in this 
               media */
#ifdef SDF_TIMESTAMP
            if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
            {

                Sdf_fn_printTimeStamp
                    ("SipAlgTranslateMedia:Post - getMediaAtIndex ");
            }
#endif

            if (sdp_getConnectionCountFromMedia (pTempSdpMedia, &dCount1,
                                                 &dError) == SipFail)
            {
                sip_error (SIP_Minor, " In sipAlgTranslateMedia:\
                  sdp_getConnectionCountFromMedia failed\n");
                sip_freeSdpMedia (pTempSdpMedia);
                sip_freeSdpMessage (pSdpMsg);
                sip_freeSipMsgBody (pMsgBody);
#ifdef SDF_TIMESTAMP
                if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
                {

                    Sdf_fn_printTimeStamp
                        ("SipAlgTranslateMedia:GetConnectionCountFromMedia - fail");
                }
#endif
                return SipFail;
            }
#ifdef SDF_TIMESTAMP
            if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
            {

                Sdf_fn_printTimeStamp
                    ("SipAlgTranslateMedia:Post - GetConnectionCountFromMedia ");
            }
#endif

            sip_freeSdpMedia (pTempSdpMedia);
            if (dCount1 == 0)
            {
                sip_error (SIP_Minor,
                           "Both Session level and media level C-line does not exist");
                sip_freeSdpMessage (pSdpMsg);
                sip_freeSipMsgBody (pMsgBody);
                return SipFail;
            }
        }
    }
    if (dSessLevelCLineExistAndValid == SipTrue)
    {
        /* now get the session level C line ip address and address type */
        pTempSessionIP = sip_strdup (pSessionConnection->pAddr, APP_MEM_ID);
        pSessionConnAddrType =
            sip_strdup (pSessionConnection->pAddrType, APP_MEM_ID);
        if (pSessionConnAddrType == NULL)
        {
            sip_error (SIP_Minor,
                       "Both Session level and media level C-line does not exist");
            sip_freeSdpMessage (pSdpMsg);
            sip_freeSipMsgBody (pMsgBody);
            sip_memfree (0, (SIP_Pvoid) & pTempSessionIP, SIP_NULL);
            return SipFail;

        }

        /* invoke the function to see whether session level C line
           is zero, or invalid and also check whether the address type 
           is IPV4 */
        if ((sipAlgIsValidNumericAddress ((SIP_U8bit *) pTempSessionIP) ==
             SipFail) || (sip_strcasecmp (pSessionConnAddrType, "IP4") != 0))
        {
            dSessLevelCLineExistAndValid = SipFalse;
        }
        /* SIPALG - 2.1 */
        if (dSessLevelCLineExistAndValid == SipTrue)
        {
            /* convert the ip from string to net format */
            if (sipAlginetStrToNet (inetAFINET, pTempSessionIP,
                                    (void *) &dSessionLevelCLineIP, &dError) ==
                SipFail)
            {
                sip_error (SIP_Minor,
                           " In sipAlgTranslateMedia:sipAlginetStrToNet failed\n");
                sip_memfree (0, (SIP_Pvoid) & pSessionConnAddrType, SIP_NULL);
                sip_memfree (0, (SIP_Pvoid) & pTempSessionIP, SIP_NULL);
                sip_freeSdpConnection (pSessionConnection);
                sip_freeSdpMessage (pSdpMsg);
                sip_freeSipMsgBody (pMsgBody);
                return SipFail;
            }
        }

        if (dMediaCount == 0)
        {
            /* 
               In case media line is not there, t hen dCLineIsWanIp is
               not set. So for intra-site session level C-line handling,
               this is needed.
             */
#ifdef SDF_TIMESTAMP
            if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
            {

                Sdf_fn_printTimeStamp
                    ("sipAlgTranslateMedia :Pre - isWanIpAddress()");
            }
#endif

            dCLineIsWanIp = sipAlgIsWANIPAddress (dSessionLevelCLineIP);
#ifdef SDF_TIMESTAMP
            if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
            {

                Sdf_fn_printTimeStamp
                    ("sipAlgTranslateMedia :Post - isWanIpAddress()");
            }
#endif
        }
        /* free pSessionConnAddrType here */
        sip_memfree (0, (SIP_Pvoid) & pSessionConnAddrType, SIP_NULL);
        /* free pTempSessionIP */
        sip_memfree (0, (SIP_Pvoid) & pTempSessionIP, SIP_NULL);
    }

    /* SIPALG - 2.1 */
    /*
       In case there exists only session level C-line with
       no media lines, translate session level c-line. Also
       translate the o-line present in SDP body.
       The key for translation will be callID as there is no port. 
       Now to get the call ID, fetch the dialog params. 
     */
    pSessionDialogKey = (SipAlgMediaKey *) sip_memget
        (0, sizeof (SipAlgMediaKey), &dError);

    if (pSessionDialogKey == SIP_NULL)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateMedia:Memory allocation failed\n");
        /* In case the session level c-line is not valid and
           sdp_getConnection failed */
        if (pSessionConnection != SIP_NULL)
            sip_freeSdpConnection (pSessionConnection);
        sip_freeSdpMessage (pSdpMsg);
        sip_freeSipMsgBody (pMsgBody);
        return SipFail;
    }

    /*  invoke the function to get the dialog param */
    if (sipAlgGetDialogParamFromSipMsg (pSipMsg, pSessionDialogKey) == SipFail)
    {
        sip_error (SIP_Minor, "In sipAlgTranslateMedia:\
                      sipAlgGetDialogParamFromSipMsg failed\n");
        pTemp = pSessionDialogKey;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pSessionDialogKey = SIP_NULL;
        if (pSessionConnection != SIP_NULL)
            sip_freeSdpConnection (pSessionConnection);
        sip_freeSdpMessage (pSdpMsg);
        sip_freeSipMsgBody (pMsgBody);
        return SipFail;
    }

    /* Get the WAN IP of the current interface */
    /* There is no need to get the WAN IP if the pkt is
     * incoming as we'll have the WAN IP already */
    if (direction == SipPktOut)
    {
        retVal = sipAlgGetWANIPAddress (pNatHeaderInfo->u4IfNum, &natIP);
        if (retVal == 0)        /* NAT_DISABLE (2) and NAT_ENABLE (1) */
        {
            sip_error (SIP_Minor, "Could not get WAN IP.");
            /*Fix for Memory leak - lakshmi */
            sipAlgFreeDialogParam (&pSessionDialogKey);
            /* sip_memfree(0, (SIP_Pvoid*)&pSessionDialogKey,SIP_NULL); */
            if (pSessionConnection != SIP_NULL)
                sip_freeSdpConnection (pSessionConnection);
            sip_freeSdpMessage (pSdpMsg);
            sip_freeSipMsgBody (pMsgBody);
            return SipFail;
        }
        else if (retVal == NAT_DISABLE)
        {
            isNatEnabled = SipFalse;
        }
    }
#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {

        Sdf_fn_printTimeStamp ("SipAlgTranslateMedia:Pre - GetOrigin ");
    }
#endif

    pTemp = pOrigin;
    if (sdp_getOrigin (pSdpMsg, &pTemp, &dError) == SipFail)
    {
        sip_error (SIP_Minor, "In sipAlgTranslateMedia:sdp_getOrigin failed\n");
        /*Fix for Memory leak - lakshmi */
        sipAlgFreeDialogParam (&pSessionDialogKey);
        /* sip_memfree(0, (SIP_Pvoid*)&pSessionDialogKey,SIP_NULL); */
        if (pSessionConnection != SIP_NULL)
            sip_freeSdpConnection (pSessionConnection);
        sip_freeSdpMessage (pSdpMsg);
        sip_freeSipMsgBody (pMsgBody);
#ifdef SDF_TIMESTAMP
        if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
        {

            Sdf_fn_printTimeStamp ("SipAlgTranslateMedia:GetOrigin - fail ");
        }
#endif
        return SipFail;
    }
    pOrigin = pTemp;
#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {

        Sdf_fn_printTimeStamp ("SipAlgTranslateMedia:Post - GetOrigin ");
    }

    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {

        Sdf_fn_printTimeStamp
            ("SipAlgTranslateMedia:Pre - GetVersionFromOrigin ");
    }
#endif

    pTemp = pVersionId;
    if (sdp_getVersionFromOrigin (pOrigin, &pTemp, &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   "In sipAlgTranslateMedia:sdp_getSessionIdFromOrigin failed\n");
        sip_freeSdpOrigin (pOrigin);
        /*Fix for Memory leak - lakshmi */
        sipAlgFreeDialogParam (&pSessionDialogKey);
        /* sip_memfree(0, (SIP_Pvoid*)&pSessionDialogKey,SIP_NULL); */
        if (pSessionConnection != SIP_NULL)
            sip_freeSdpConnection (pSessionConnection);
        sip_freeSdpMessage (pSdpMsg);
        sip_freeSipMsgBody (pMsgBody);
#ifdef SDF_TIMESTAMP
        if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
        {

            Sdf_fn_printTimeStamp
                ("SipAlgTranslateMedia:GetVersionFromOrigin - fail ");
        }
#endif
        return SipFail;
    }
    pVersionId = pTemp;
#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {

        Sdf_fn_printTimeStamp
            ("SipAlgTranslateMedia:Post - GetVersionFromOrigin ");
    }

    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {

        Sdf_fn_printTimeStamp ("SipAlgTranslateMedia:Pre - GetMessageType ");
    }
#endif

    if (sip_getMessageType (pSipMsg, &dMessageType, &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   "In sipAlgGetMethodFromSipReqest:sip_getMessageType failed\n");
        sip_freeSdpOrigin (pOrigin);
        /*Fix for Memory leak - lakshmi */
        sipAlgFreeDialogParam (&pSessionDialogKey);
        /* sip_memfree(0, (SIP_Pvoid*)&pSessionDialogKey,SIP_NULL); */
        if (pSessionConnection != SIP_NULL)
            sip_freeSdpConnection (pSessionConnection);
        sip_freeSdpMessage (pSdpMsg);
        sip_freeSipMsgBody (pMsgBody);
#ifdef SDF_TIMESTAMP
        if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
        {

            Sdf_fn_printTimeStamp
                ("SipAlgTranslateMedia: GetMessageType - fail ");
        }
#endif

        return SipFail;
    }
#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {

        Sdf_fn_printTimeStamp ("SipAlgTranslateMedia:Post - GetMessageType ");
    }
#endif
    if (dMessageType == SipMessageRequest)
        isRequest = SipTrue;
    else
        isRequest = SipFalse;

    /* now iterate thru all the media lines */
    for (i = 0; i < dMediaCount; i++)
    {
        SdpMedia           *pSdpMedia = SIP_NULL;
        SIP_U32bit          connectionLines = 0;
        SIP_S8bit          *pTempIP = SIP_NULL, *pTempTransIP = SIP_NULL;
        SIP_U16bit          dPort = 0, dTransPort = 0;
        en_SipAValueInSdp   dAttribute = 0;
        SdpConnection      *pConnection = SIP_NULL;
        SipNatInfo         *pMediaInfo = SIP_NULL;
        SIP_U32bit          dTempOrigIP = 0, dTransIP = 0;
        SipAlgMediaKey     *pDialogKey = SIP_NULL;
        en_SipBoolean       dIsWanIP = SipFalse;
        SIP_S8bit          *pConnAddrType = SIP_NULL;
        SIP_S8bit          *pMediaProtocol = SIP_NULL;
        en_SipBoolean       dIsSessionLevelCLineUsed = SipFalse;

        /* SIPALG - 2.1 */
        /* Reset this value every time */
        isCurrentMediaPortZero = SipFalse;

        /* get the media at the given index */
#ifdef SDF_TIMESTAMP
        if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
        {

            Sdf_fn_printTimeStamp
                ("SipAlgTranslateMedia:Pre - GetMediaAtIndex ");
        }
#endif
        pTemp = pSdpMedia;
        if (sdp_getMediaAtIndex (pSdpMsg, &pTemp, i, &dError) == SipFail)
        {
            sip_error (SIP_Minor,
                       " In sipAlgTranslateMedia:sdp_getMediaAtIndex failed\n");
            /* SIPALG - 2.1 */
            /*Fix for Memory leak - lakshmi */
            sipAlgFreeDialogParam (&pSessionDialogKey);
            /* sip_memfree(0, (SIP_Pvoid*)&pSessionDialogKey,SIP_NULL); */
            /*lakshmi - commented - fix */
/*       sip_memfree(0, (SIP_Pvoid*)&pVersionId,SIP_NULL); */
            sip_freeSdpOrigin (pOrigin);
            sip_freeSdpConnection (pSessionConnection);
            sip_freeSdpMessage (pSdpMsg);
            sip_freeSipMsgBody (pMsgBody);
#ifdef SDF_TIMESTAMP
            if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
            {

                Sdf_fn_printTimeStamp
                    ("SipAlgTranslateMedia:GetMediaAtIndex - fail");
            }
#endif
            pTemp = pTransConnectionIP;
            sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
            pTransConnectionIP = SIP_NULL;
            return SipFail;
        }
        pSdpMedia = pTemp;

#ifdef SDF_TIMESTAMP
        if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
        {

            Sdf_fn_printTimeStamp
                ("SipAlgTranslateMedia:Post - GetMediaAtIndex ");
        }
#endif

        /* check if there is any connection line present in this 
           media */
        if (sdp_getConnectionCountFromMedia (pSdpMedia, &connectionLines,
                                             &dError) == SipFail)
        {
            sip_error (SIP_Minor, " In sipAlgTranslateMedia:\
                  sdp_getConnectionCountFromMedia failed\n");
            /* SIPALG - 2.1 */
            /*Fix for Memory leak - lakshmi */
            sipAlgFreeDialogParam (&pSessionDialogKey);
            /*  sip_memfree(0, (SIP_Pvoid*)&pSessionD ialogKey,SIP_NULL); */
            /*lakshmi - commented - fix */
/*       sip_memfree(0, (SIP_Pvoid*)&pVersionId,SIP_NULL); */
            sip_freeSdpOrigin (pOrigin);
            sip_freeSdpMedia (pSdpMedia);
            sip_freeSdpConnection (pSessionConnection);
            sip_freeSdpMessage (pSdpMsg);
            sip_freeSipMsgBody (pMsgBody);
#ifdef SDF_TIMESTAMP
            if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
            {

                Sdf_fn_printTimeStamp
                    ("SipAlgTranslateMedia:GetMediaAtIndex - fail ");
            }
#endif

            pTemp = pTransConnectionIP;
            sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
            pTransConnectionIP = SIP_NULL;
            return SipFail;
        }
#ifdef SDF_TIMESTAMP
        if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
        {

            Sdf_fn_printTimeStamp
                ("SipAlgTranslateMedia:Post - GetConnectionCountFromMedia ");
        }
#endif

        /* get the connection line from media if present */
        if (connectionLines != 0)
        {
#ifdef SDF_TIMESTAMP
            if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
            {

                Sdf_fn_printTimeStamp
                    ("SipAlgTranslateMedia:Pre - GetConnectionAtIndexFromMedia ");
            }
#endif

            pTemp = pConnection;
            if (sdp_getConnectionAtIndexFromMedia (pSdpMedia, &pTemp, 0,
                                                   &dError) == SipFail)
            {
                sip_error (SIP_Minor, "In sipAlgTranslateMedia:\
                         sdp_getConnectionAtIndexFromMedia failed\n");
                /* SIPALG - 2.1 */
                /*Fix for Memory leak - lakshmi */
                sipAlgFreeDialogParam (&pSessionDialogKey);
                /* sip_memfree(0, (SIP_Pvoid*)&pSessionDialogKey,SIP_NULL); */
                /*lakshmi - commented - fix */
/*       sip_memfree(0, (SIP_Pvoid*)&pVersionId,SIP_NULL); */
                sip_freeSdpOrigin (pOrigin);
                sip_freeSdpMedia (pSdpMedia);
                sip_freeSdpConnection (pSessionConnection);
                sip_freeSdpMessage (pSdpMsg);
                sip_freeSipMsgBody (pMsgBody);
#ifdef SDF_TIMESTAMP
                if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
                {

                    Sdf_fn_printTimeStamp
                        ("SipAlgTranslateMedia:GetConnectionAtIndexFromMedia - fail");
                }
#endif
                pTemp = pTransConnectionIP;
                sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
                pTransConnectionIP = SIP_NULL;
                return SipFail;
            }
            pConnection = pTemp;
        }
        else if (connectionLines == 0)
        {
            dIsSessionLevelCLineUsed = SipTrue;
        }
        if ((dIsSessionLevelCLineUsed == SipTrue) &&
            (dSessLevelCLineExistAndValid == SipFalse))
        {
            /* there is no C line present in this media and 
               session level C line is invalid..so translation
               is not reqd for this media */
            sip_freeSdpConnection (pConnection);
            sip_freeSdpMedia (pSdpMedia);
            continue;

        }
#ifdef SDF_TIMESTAMP
        if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
        {

            Sdf_fn_printTimeStamp
                ("SipAlgTranslateMedia:Post - GetConnectionAtIndexFromMedia ");
        }
#endif
        /* get the port from media */
#ifdef SDF_TIMESTAMP
        if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
        {

            Sdf_fn_printTimeStamp
                ("SipAlgTranslateMedia:Pre - GetPortFromMedia ");
        }
#endif

        if (sdp_getPortFromMedia (pSdpMedia, &dPort, &dError) == SipFail)
        {
            sip_error (SIP_Minor,
                       " In sipAlgTranslateMedia:sdp_getPortFromMedia failed\n");
            /* SIPALG - 2.1 */
            /*Fix for Memory leak - lakshmi */
            sipAlgFreeDialogParam (&pSessionDialogKey);
            /*  sip_memfree(0, (SIP_Pvoid*)&pSessionDialogKey,SIP_NULL); */
            /*lakshmi - commented - fix */
/*       sip_memfree(0, (SIP_Pvoid*)&pVersionId,SIP_NULL); */
            sip_freeSdpOrigin (pOrigin);
            sip_freeSdpConnection (pConnection);
            sip_freeSdpMedia (pSdpMedia);
            sip_freeSdpConnection (pSessionConnection);
            sip_freeSdpMessage (pSdpMsg);
            sip_freeSipMsgBody (pMsgBody);
#ifdef SDF_TIMESTAMP
            if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
            {

                Sdf_fn_printTimeStamp
                    ("SipAlgTranslateMedia:GetPortFromMedia - fail");
            }
#endif
            pTemp = pTransConnectionIP;
            sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
            pTransConnectionIP = SIP_NULL;
            return SipFail;
        }
#ifdef SDF_TIMESTAMP
        if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
        {

            Sdf_fn_printTimeStamp
                ("SipAlgTranslateMedia:Post - GetPortFromMedia ");
        }
#endif
        /* If the media line port is zero, 
           no trans. reqd. for this..continue with
           other media lines */
        if (dPort == 0)
        {
            /* SIPALG - 2.1 */
            isCurrentMediaPortZero = SipTrue;
            /* 
               This indicates that the media port is zero and there is
               no media level C-line. So no need to proceed further
               in media handling. Session level C-line will be translated 
               later
             */
            if (dIsSessionLevelCLineUsed == SipTrue)
            {
                /* lakshmi */
#ifdef SDF_TIMESTAMP
                if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
                {

                    Sdf_fn_printTimeStamp
                        ("sipAlgTranslateMedia :Pre - isWanIpAddress()");
                }
#endif
                dCLineIsWanIp = sipAlgIsWANIPAddress (dSessionLevelCLineIP);
#ifdef SDF_TIMESTAMP
                if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
                {

                    Sdf_fn_printTimeStamp
                        ("sipAlgTranslateMedia :Post - isWanIpAddress()");
                }
#endif

                sip_freeSdpConnection (pConnection);
                sip_freeSdpMedia (pSdpMedia);
                continue;
            }
        }
        else
        {
            /* 
               This indicates that the session level C-line is used for a port
               which is not zero. So the entry of  session level C-line will 
               not be stored in IP hash table. The entry will be stored in 
               NAT hash table. The entry for session level C-line will be in 
               IP hash only when all the media ports are zero or there is no 
               media port. 
             */
            if (dIsSessionLevelCLineUsed == SipTrue)
                isAllMediaPortZero = SipFalse;
        }

        /*
           In order to support fax, in case there is m-line 
           irrespective of the protocol, the translation will
           be done. Hence, removing the RTP check.If needed, can be taken from the      previous CVS version.
         */

        /* allocate memory to natinfo structure */
        pMediaInfo = (SipNatInfo *) sip_memget
            (0, sizeof (SipNatInfo), &dError);

        if (pMediaInfo == SIP_NULL)
        {
            sip_error (SIP_Minor,
                       " In sipAlgTranslateMedia:Memory allocation failed\n");
            /* SIPALG - 2.1 */
            /*Fix for Memory leak - lakshmi */
            sipAlgFreeDialogParam (&pSessionDialogKey);
            /* sip_memfree(0, (SIP_Pvoid*)&pSessionDialogKey,SIP_NULL); */
            /*lakshmi - commented - fix */
/*       sip_memfree(0, (SIP_Pvoid*)&pVersionId,SIP_NULL); */
            sip_freeSdpOrigin (pOrigin);
            sip_freeSdpConnection (pConnection);
            sip_freeSdpMedia (pSdpMedia);
            sip_freeSdpConnection (pSessionConnection);
            sip_freeSdpMessage (pSdpMsg);
            sip_freeSipMsgBody (pMsgBody);
            pTemp = pTransConnectionIP;
            sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
            pTransConnectionIP = SIP_NULL;
            return SipFail;
        }

        if (dIsSessionLevelCLineUsed == SipFalse)
        {
            pTempIP = sip_strdup (pConnection->pAddr, APP_MEM_ID);
            pConnAddrType = sip_strdup (pConnection->pAddrType, APP_MEM_ID);
            if (pConnAddrType == NULL)
            {
                sip_error (SIP_Minor,
                           " In sipAlgTranslateMedia:Memory allocation failed\n");
                sipAlgFreeDialogParam (&pSessionDialogKey);
                sip_freeSdpOrigin (pOrigin);
                sip_freeSdpConnection (pConnection);
                sip_freeSdpMedia (pSdpMedia);
                sip_freeSdpConnection (pSessionConnection);
                sip_freeSdpMessage (pSdpMsg);
                sip_freeSipMsgBody (pMsgBody);
                pTemp = pTransConnectionIP;
                sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
                pTransConnectionIP = SIP_NULL;
                pTemp = pMediaInfo;
                sip_memfree (0, &pTemp, SIP_NULL);
                pMediaInfo = SIP_NULL;
                sip_memfree (0, (SIP_Pvoid) & pTempIP, SIP_NULL);
                return SipFail;
            }

            /* invoke the function to see whether  
               C line for this media is zero, or invalid */

            /* now check whether the address type is IPV4
               if it is not IP4 address then continue */

            if ((sipAlgIsValidNumericAddress ((SIP_U8bit *) pTempIP) == SipFail)
                || (sip_strcasecmp (pConnAddrType, "IP4") != 0))
            {
                sip_memfree (0, (SIP_Pvoid) & pConnAddrType, SIP_NULL);
                sip_memfree (0, (SIP_Pvoid) & pTempIP, SIP_NULL);
                pTemp = pMediaInfo;
                sip_memfree (0, &pTemp, SIP_NULL);
                pMediaInfo = SIP_NULL;
                sip_freeSdpConnection (pConnection);
                sip_freeSdpMedia (pSdpMedia);
                continue;
            }
            /* free pConnAddrType here */
            sip_memfree (0, (SIP_Pvoid) & pConnAddrType, SIP_NULL);

        }
        else if (dSessLevelCLineExistAndValid == SipTrue)
        {
            pTempIP = sip_strdup (pSessionConnection->pAddr, APP_MEM_ID);
        }

        /* convert the ip from string to net form at */
        if (sipAlginetStrToNet
            (inetAFINET, pTempIP, (void *) &dTempOrigIP, &dError) == SipFail)
        {
            sip_error (SIP_Minor,
                       " In sipAlgTranslateMedia:sipAlginetStrToNet failed\n");
            /* SIPALG - 2.1 */
            /*Fix for Memory leak - lakshmi */
            sipAlgFreeDialogParam (&pSessionDialogKey);
            /*  sip_memfree(0, (SIP_Pvoid*)&pSessionDialogKey,SIP_NULL); */
            /*lakshmi - commented - fix */
/*       sip_memfree(0, (SIP_Pvoid*)&pVersionId,SIP_NULL); */
            sip_freeSdpOrigin (pOrigin);
            sip_memfree (0, (SIP_Pvoid) & pTempIP, SIP_NULL);
            pTemp = pMediaInfo;
            sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
            pMediaInfo = SIP_NULL;
            sip_freeSdpConnection (pConnection);
            sip_freeSdpMedia (pSdpMedia);
            sip_freeSdpConnection (pSessionConnection);
            sip_freeSdpMessage (pSdpMsg);
            sip_freeSipMsgBody (pMsgBody);
            pTemp = pTransConnectionIP;
            sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
            pTransConnectionIP = SIP_NULL;
            return SipFail;
        }
        /* free pTempIP */
        sip_memfree (0, (SIP_Pvoid) & pTempIP, SIP_NULL);
#ifdef SDF_TIMESTAMP
        if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
        {

            Sdf_fn_printTimeStamp
                ("sipAlgTranslateMedia :Pre - isWanIpAddress()");
        }
#endif
        /* check whether the ip is wan-ip */
        dIsWanIP = sipAlgIsWANIPAddress (dTempOrigIP);
#ifdef SDF_TIMESTAMP
        if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
        {

            Sdf_fn_printTimeStamp
                ("sipAlgTranslateMedia :Post - isWanIpAddress()");
        }
#endif

        /* fix for intra-site call handling */
        /* if it is a session level C line then find out whether */
        /* it is mapped ip */
        if (dSessLevelCLineExistAndValid == SipTrue)
        {
            /* SIPALG - 2.1 */
            /* No need to call sipAlgIsWANIPAddress function again */
            dCLineIsWanIp = dIsWanIP;
        }
        if (direction == SipPktOut)
        {
            pMediaInfo->privateIP = dTempOrigIP;
            pMediaInfo->privatePort = dPort;
            pMediaInfo->outsidePort = 0;
            pMediaInfo->outsideIP = 0;
            pMediaInfo->mappedIP = 0;
            pMediaInfo->mappedPort = 0;
        }
        else if (direction == SipPktIn)
        {
            if (dIsWanIP == SipTrue)
            {
                pMediaInfo->mappedIP = dTempOrigIP;
                pMediaInfo->mappedPort = dPort;
                pMediaInfo->privateIP = 0;
                pMediaInfo->privatePort = 0;
                pMediaInfo->outsidePort = 0;
                pMediaInfo->outsideIP = 0;

            }
            else if (dIsWanIP == SipFalse)
            {
                pMediaInfo->outsideIP = dTempOrigIP;
                pMediaInfo->outsidePort = dPort;
                pMediaInfo->mappedIP = 0;
                pMediaInfo->mappedPort = 0;
                pMediaInfo->privateIP = 0;
                pMediaInfo->privatePort = 0;

            }
        }
        pMediaInfo->direction = direction;
        pMediaInfo->ifIndex = pNatHeaderInfo->u4IfNum;

        /* lakshmi  added - NAT API Enhancements - START */
        /* get the protocol type from media
           if it is not RTP/AVP then continue with other
           media lines */
        pTemp = pMediaProtocol;
        if (sdp_getProtoFromMedia (pSdpMedia, &pTemp, &dError) == SipFail)
        {
            sip_error (SIP_Minor,
                       " In sipAlgTranslateMedia:sdp_getProtoFromMedia failed\n");
            sip_freeSdpConnection (pConnection);
            sip_freeSdpMedia (pSdpMedia);
            sip_freeSdpConnection (pSessionConnection);
            sip_freeSdpMessage (pSdpMsg);
            sip_freeSipMsgBody (pMsgBody);
            pTemp = pTransConnectionIP;
            sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
            pTransConnectionIP = SIP_NULL;
            pTemp = pMediaInfo;
            sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
            pMediaInfo = SIP_NULL;
            sipAlgFreeDialogParam (&pSessionDialogKey);
            return SipFail;
        }
        pMediaProtocol = pTemp;
        if (sip_strcasecmp (pMediaProtocol, "RTP/AVP") != 0)
        {
            pMediaInfo->parity = en_parityAny;
        }
        else
        {
            pMediaInfo->parity = en_parityEven;
        }
        /* NAT API Enhancements - END */

        /* allocate memory for dialogparams */
        pDialogKey = (SipAlgMediaKey *) sip_memget
            (0, sizeof (SipAlgMediaKey), &dError);

        if (pDialogKey == SIP_NULL)
        {
            sip_error (SIP_Minor,
                       " In sipAlgTranslateMedia:Memory allocation failed\n");
            /* SIPALG - 2.1 */
            /*Fix for Memory leak - lakshmi */
            sipAlgFreeDialogParam (&pSessionDialogKey);
            /*  sip_memfree(0, (SIP_Pvoid*)&pSessionDialogKey,SIP_NULL); */
            /*lakshmi - commented - fix */
/*       sip_memfree(0, (SIP_Pvoid*)&pVersionId,SIP_NULL); */
            sip_freeSdpOrigin (pOrigin);
            pTemp = pMediaInfo;
            sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
            pMediaInfo = SIP_NULL;
            sip_freeSdpConnection (pConnection);
            sip_freeSdpMedia (pSdpMedia);
            sip_freeSdpConnection (pSessionConnection);
            sip_freeSdpMessage (pSdpMsg);
            sip_freeSipMsgBody (pMsgBody);
            pTemp = pTransConnectionIP;
            sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
            pTransConnectionIP = SIP_NULL;
            return SipFail;
        }

        /*  invoke the function to get the dialog param */
        if (sipAlgGetDialogParamFromSipMsg (pSipMsg, pDialogKey) == SipFail)
        {
            sip_error (SIP_Minor, "In sipAlgTranslateMedia:\
                      sipAlgGetDialogParamFromSipMsg failed\n");
            /* SIPALG - 2.1 */
            /*Fix for Memory leak - lakshmi */
            sipAlgFreeDialogParam (&pSessionDialogKey);
            /*   sip_memfree(0, (SIP_Pvoid*)&pSessionDialogKey,SIP_NULL); */
            /*lakshmi - commented - fix */
/*       sip_memfree(0, (SIP_Pvoid*)&pVersionId,SIP_NULL); */
            sip_freeSdpOrigin (pOrigin);
            pTemp = pMediaInfo;
            sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
            pMediaInfo = SIP_NULL;
            pTemp = pDialogKey;
            sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
            pDialogKey = SIP_NULL;
            sip_freeSdpConnection (pConnection);
            sip_freeSdpMedia (pSdpMedia);
            sip_freeSdpConnection (pSessionConnection);
            sip_freeSdpMessage (pSdpMsg);
            sip_freeSipMsgBody (pMsgBody);
            pTemp = pTransConnectionIP;
            sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
            pTransConnectionIP = SIP_NULL;
            return SipFail;
        }
        /* SIPALG - 2.1 */
        if ((isCurrentMediaPortZero == SipTrue) && (isNatEnabled == SipTrue))
        {
            SIPDEBUG ((SIP_S8bit *)
                      "Translating media c-line when port is zero");
            /* 
               In case media port is zero but contains the valid IP,
               in order to provide topology hiding, that IP will be
               translated 
             */
            if (direction == SipPktOut)
            {
                /* In case direction is out, pass the mapped IP as well */
                pMediaInfo->mappedIP = OSIX_HTONL (natIP);
            }
            if (SipFail ==
                sipAlgPerformIPTranslation (pMediaInfo, pDialogKey, pVersionId,
                                            isRequest))
            {
                sip_error (SIP_Minor, "In sipAlgTranslateMedia:\
                      sipAlgPerformIPTranslation failed\n");
                /*lakshmi - commented - fix */
                /* sip_memfree(0, (SIP_Pvoid*)&pVersionId,SIP_NULL); */
                sip_freeSdpOrigin (pOrigin);
                /*Fix for Memory leak - lakshmi */
                sipAlgFreeDialogParam (&pSessionDialogKey);
                sipAlgFreeDialogParam (&pDialogKey);
                /* sip_memfree(0, (SIP_Pvoid*)&pSessionDialogKey,SIP_NULL); */
                pTemp = pMediaInfo;
                sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
                pMediaInfo = SIP_NULL;
                /* sip_memfree(0, (SIP_Pvoid*)&pDialogKey,SIP_NULL); */
                sip_freeSdpConnection (pConnection);
                sip_freeSdpMedia (pSdpMedia);
                sip_freeSdpConnection (pSessionConnection);
                sip_freeSdpMessage (pSdpMsg);
                sip_freeSipMsgBody (pMsgBody);
                pTemp = pTransConnectionIP;
                sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
                pTransConnectionIP = SIP_NULL;
                return SipFail;
            }
        }
        else
        {
            /* invoke the function to add media info in hash tables */
            if (sipAlgUpdateMediaInfo (pDialogKey,
                                       &pMediaInfo, dAttribute) == SipFail)
            {
                sip_error (SIP_Minor,
                           " In sipAlgTranslateMedia:sipAlgUpdateMediaInfo failed\n");
                /*lakshmi - commented - fix */
/*               sip_memfree(0, (SIP_Pvoid*)&pVersionId,SIP_NULL); */
                sip_freeSdpOrigin (pOrigin);
                /*Fix for Memory leak - lakshmi */
                sipAlgFreeDialogParam (&pSessionDialogKey);
                /*  sip_memfree(0, (SIP_Pvoid*)&pSessionDialogKey,SIP_NULL); */
                pTemp = pMediaInfo;
                sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
                pMediaInfo = SIP_NULL;
                sipAlgFreeDialogParam (&pDialogKey);
                sip_freeSdpConnection (pConnection);
                sip_freeSdpMedia (pSdpMedia);
                sip_freeSdpConnection (pSessionConnection);
                sip_freeSdpMessage (pSdpMsg);
                sip_freeSipMsgBody (pMsgBody);
                pTemp = pTransConnectionIP;
                sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
                pTransConnectionIP = SIP_NULL;
                return SipFail;
            }
        }
        /* free the dialog params here */
        sipAlgFreeDialogParam (&pDialogKey);
        /* If this is a inbound message and the
           type of ip is public then we dont need to 
           set into sip-msg ..so return success from here */
        if ((direction == SipPktIn) && (dIsWanIP == SipFalse))
        {
            pTemp = pMediaInfo;
            sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
            pMediaInfo = SIP_NULL;
            if (pConnection != SIP_NULL)
                sip_freeSdpConnection (pConnection);
            sip_freeSdpMedia (pSdpMedia);
            continue;
        }
        /* now we need to set the ip and port in sip-msg as per 
           the direction and ip type */
        if (direction == SipPktOut)
        {
            dTransIP = pMediaInfo->mappedIP;
            dTransPort = pMediaInfo->mappedPort;
        }
        else if (direction == SipPktIn)
        {
            if (dIsWanIP == SipTrue)
            {
                dTransIP = pMediaInfo->privateIP;
                dTransPort = pMediaInfo->privatePort;
            }
        }

        if (isCurrentMediaPortZero == SipFalse)
        {
            /* set port in media */
            if (sdp_setPortInMedia (pSdpMedia, dTransPort, &dError) == SipFail)
            {
                sip_error (SIP_Minor,
                           " In sipAlgTranslateMedia:sdp_setPortInMedia failed\n");
                /* SIPALG - 2.1 */
                /*Fix for Memory leak - lakshmi */
                sipAlgFreeDialogParam (&pSessionDialogKey);
                /* sip_memfree(0, (SIP_Pvoid*)&pSessionDialogKey,SIP_NULL); */
                /*lakshmi - commented - fix */
                /*           sip_memfree(0, (SIP_Pvoid*)&pVersionId,SIP_NULL); */
                sip_freeSdpOrigin (pOrigin);
                pTemp = pMediaInfo;
                sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
                pMediaInfo = SIP_NULL;
                sip_freeSdpConnection (pConnection);
                sip_freeSdpMedia (pSdpMedia);
                sip_freeSdpConnection (pSessionConnection);
                sip_freeSdpMessage (pSdpMsg);
                sip_freeSipMsgBody (pMsgBody);
                pTemp = pTransConnectionIP;
                sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
                pTransConnectionIP = SIP_NULL;
                return SipFail;
            }
        }
        pTempTransIP = (SIP_S8bit *) sip_memget
            (0, SIP_ALG_INET_ADDRSTRLEN, &dError);
        if (pTempTransIP == SIP_NULL)
        {
            pTemp = pTransConnectionIP;
            sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
            pTransConnectionIP = SIP_NULL;
            sipAlgFreeDialogParam (&pSessionDialogKey);
            pTemp = pMediaInfo;
            sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
            pMediaInfo = SIP_NULL;
            return SipFail;
        }
        sip_memset ((SIP_Pvoid) pTempTransIP, 0, SIP_ALG_INET_ADDRSTRLEN);

        /* now convert the pMediaInfo->mappedIP to string */
        if (sipAlgInetNetToStr (inetAFINET, (void *) &dTransIP,
                                pTempTransIP, SIP_ALG_INET_ADDRSTRLEN,
                                &dError) == SipFail)
        {
            sip_error (SIP_Minor,
                       " In sipAlgTranslateMedia:sipAlgInetNetToStr failed\n");
            /* SIPALG - 2.1 */
            /*Fix for Memory leak - lakshmi */
            sipAlgFreeDialogParam (&pSessionDialogKey);
            /*   sip_memfree(0, (SIP_Pvoid*)&pSessionDialogKey,SIP_NULL); */
            /*lakshmi - commented - fix */
/*       sip_memfree(0, (SIP_Pvoid*)&pVersionId,SIP_NULL); */
            sip_freeSdpOrigin (pOrigin);
            pTemp = pMediaInfo;
            sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
            pMediaInfo = SIP_NULL;
            sip_freeSdpConnection (pConnection);
            sip_freeSdpMedia (pSdpMedia);
            sip_freeSdpConnection (pSessionConnection);
            sip_freeSdpMessage (pSdpMsg);
            sip_freeSipMsgBody (pMsgBody);
            pTemp = pTransConnectionIP;
            sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
            pTransConnectionIP = SIP_NULL;
            return SipFail;
        }

        if (dIsSessionLevelCLineUsed == SipFalse)
        {
            pTemp = pConnection->pAddr;

            /* Macro sdp_setAddrInConnection has been changed to 
             * sdp_setAddrParamInConnection to avoid the 
             * "warning:dereferencing type-punned pointer will 
             * break strict-aliasing rules"
             */
            if (sdp_setAddrParamInConnection (sip_strdup
                                              (pTempTransIP, APP_MEM_ID),
                                              pTemp,
                                              SipParamString,
                                              &dError) == SipFail)
            {
                sip_error (SIP_Minor,
                           " In sipAlgTranslateMedia:sdp_setAddrInConnection failed\n");
                /* SIPALG - 2.1 */
                /*Fix for Memory leak - lakshmi */
                sipAlgFreeDialogParam (&pSessionDialogKey);
                /*  sip_memfree(0, (SIP_Pvoid*)&pSessionDialogKey,SIP_NULL); */
                /*lakshmi - commented - fix */
                /* sip_memfree(0, (SIP_Pvoid*)&pVersionId,SIP_NULL); */
                sip_freeSdpOrigin (pOrigin);
                pTemp = pMediaInfo;
                sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
                pMediaInfo = SIP_NULL;
                sip_freeSdpConnection (pConnection);
                sip_freeSdpMedia (pSdpMedia);
                sip_freeSdpConnection (pSessionConnection);
                sip_freeSdpMessage (pSdpMsg);
                sip_freeSipMsgBody (pMsgBody);
                pTemp = pTransConnectionIP;
                sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
                pTransConnectionIP = SIP_NULL;
                return SipFail;
            }
            pConnection->pAddr = pTemp;
        }
        /*
           The session level C-line is stored in dSessionLevelCLineIP and is
           later translated below. 
         */
        if ((dSessLevelCLineExistAndValid == SipTrue)
            && (pTransConnectionIP == SIP_NULL))
        {
            pTransConnectionIP =
                (SIP_U8bit *) sip_strdup (pTempTransIP, APP_MEM_ID);
        }
        /* Fix UT: Free all the memory allocated within this block */
        sip_memfree (0, (SIP_Pvoid) & pTempTransIP, SIP_NULL);
        pTemp = pMediaInfo;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pMediaInfo = SIP_NULL;
        if (pConnection != SIP_NULL)
            sip_freeSdpConnection (pConnection);
        sip_freeSdpMedia (pSdpMedia);
    }
    /* fix for intra-site call */
    /* if it is incoming packet and C line is wan ip then it is intra-site call */
    /* we need to replace mapped ip in C line with private ip */
    if ((dSessLevelCLineExistAndValid == SipTrue) && (isNatEnabled == SipTrue))
    {

        if ((direction == SipPktOut) || ((direction == SipPktIn) &&
                                         (dCLineIsWanIp == SipTrue)))
        {
            /* SIPALG - 2.1 */

            SIPDEBUG ((SIP_S8bit *) "Translating session C-line in the media");
            if (isAllMediaPortZero == SipTrue)
            {
                /* 
                   If all the media ports are zero which use session level
                   C-line, or there are no media ports, then only the entry
                   is created in IP hash table
                 */

                /* allocate memory to natinfo structure */
                pSessionInfo = (SipNatInfo *) sip_memget
                    (0, sizeof (SipNatInfo), &dError);

                if (pSessionInfo == SIP_NULL)
                {
                    sip_error (SIP_Minor,
                               " In sipAlgTranslateMedia:Memory allocation failed\n");
                    /*Fix for Memory leak - lakshmi */
                    sipAlgFreeDialogParam (&pSessionDialogKey);
                    /*         sip_memfree(0, (SIP_Pvoid*)&pSessionDialogKey,SIP_NULL); */
                    /*lakshmi - commented - fix */
                    /*     sip_memfree(0, (SIP_Pvoid*)&pVersionId,SIP_NULL); */
                    sip_freeSdpOrigin (pOrigin);

                    sip_freeSdpConnection (pSessionConnection);
                    sip_freeSdpMessage (pSdpMsg);
                    sip_freeSipMsgBody (pMsgBody);
                    pTemp = pTransConnectionIP;
                    sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
                    pTransConnectionIP = SIP_NULL;
                    return SipFail;
                }
                if (direction == SipPktOut)
                {
                    pSessionInfo->privateIP = dSessionLevelCLineIP;
                    pSessionInfo->privatePort = 0;
                    pSessionInfo->mappedIP = OSIX_HTONL (natIP);
                    pSessionInfo->mappedPort = 0;
                }
                else
                {
                    pSessionInfo->mappedIP = dSessionLevelCLineIP;
                    pSessionInfo->mappedPort = 0;
                }
                pSessionInfo->direction = direction;
                pSessionInfo->ifIndex = pNatHeaderInfo->u4IfNum;

                if (SipFail ==
                    sipAlgPerformIPTranslation (pSessionInfo, pSessionDialogKey,
                                                pVersionId, isRequest))
                {
                    sip_error (SIP_Minor,
                               " In sipAlgTranslateMedia: sipAlgPerformIPTranslation failed\n");
                    /*lakshmi - commented - fix */
                    /*  sip_memfree(0, (SIP_Pvoid*)&pVersionId,SIP_NULL); */
                    sip_freeSdpOrigin (pOrigin);
                    pTemp = pSessionInfo;
                    sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
                    pSessionInfo = SIP_NULL;
                    /*Fix for Memory leak - lakshmi */
                    sipAlgFreeDialogParam (&pSessionDialogKey);
                    /* sip_memfree(0, (SIP_Pvoid*)&pSessionDialogKey,SIP_NULL); */
                    sip_freeSdpConnection (pSessionConnection);
                    sip_freeSdpMessage (pSdpMsg);
                    sip_freeSipMsgBody (pMsgBody);
                    pTemp = pTransConnectionIP;
                    sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
                    pTransConnectionIP = SIP_NULL;
                    return SipFail;
                }

                if (pSessionInfo->direction == SipPktOut)
                {
                    dSessionTransIP = pSessionInfo->mappedIP;
                }
                else
                {
                    dSessionTransIP = pSessionInfo->privateIP;
                }
                pTemp = pSessionInfo;
                sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
                pSessionInfo = SIP_NULL;
                /*Memory leak fix by lakshmi */
                if (pTransConnectionIP != SIP_NULL)
                {
                    pTemp = pTransConnectionIP;
                    sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
                    pTransConnectionIP = SIP_NULL;
                }

                pTransConnectionIP = (SIP_U8bit *) sip_memget
                    (0, SIP_ALG_INET_ADDRSTRLEN, &dError);
                if (pTransConnectionIP == NULL)
                {
                    sipAlgFreeDialogParam (&pSessionDialogKey);
                    return SipFail;
                }
                sip_memset ((SIP_Pvoid) pTransConnectionIP, 0,
                            SIP_ALG_INET_ADDRSTRLEN);

                /* now convert the pMediaInfo->mappedIP to string */
                if (sipAlgInetNetToStr (inetAFINET, (void *) &dSessionTransIP,
                                        (SIP_S8bit *) pTransConnectionIP,
                                        SIP_ALG_INET_ADDRSTRLEN,
                                        &dError) == SipFail)
                {
                    sip_error (SIP_Minor,
                               " In sipAlgTranslateMedia:sipAlgInetNetToStr failed\n");
                    /*Fix for Memory leak - lakshmi */
                    sipAlgFreeDialogParam (&pSessionDialogKey);
                    /*  sip_memfree(0, (SIP_Pvoid*)&pSessionDialogKey,SIP_NULL); */
                    /*lakshmi - commented - fix */
/*       sip_memfree(0, (SIP_Pvoid*)&pVersionId,SIP_NULL); */
                    sip_freeSdpOrigin (pOrigin);
                    sip_freeSdpConnection (pSessionConnection);
                    sip_freeSdpMessage (pSdpMsg);
                    sip_freeSipMsgBody (pMsgBody);
                    return SipFail;
                }
            }
            if (pTransConnectionIP != SIP_NULL)
            {
                pTemp = ((SdpConnection *) pSessionConnection)->pAddr;
                if (sdp_setAddrParamInConnection (pTransConnectionIP,
                                                  pTemp, SipParamString,
                                                  &dError) == SipFail)
                {
                    sip_error (SIP_Minor,
                               " In sipAlgTranslateMedia:sdp_setAddrInConnection failed\n");
                    /*Fix for Memory leak - lakshmi */
                    sipAlgFreeDialogParam (&pSessionDialogKey);
                    /* sip_memfree(0, (SIP_Pvoid*)&pSessionDialogKey,SIP_NULL); */
                    /*lakshmi - commented - fix */
/*       sip_memfree(0, (SIP_Pvoid*)&pVersionId,SIP_NULL); */
                    sip_freeSdpOrigin (pOrigin);
                    sip_freeSdpConnection (pSessionConnection);
                    sip_freeSdpMessage (pSdpMsg);
                    sip_freeSipMsgBody (pMsgBody);
                    return SipFail;
                }
                ((SdpConnection *) pSessionConnection)->pAddr = pTemp;
            }
            else
            {
                pTemp = pTransConnectionIP;
                sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
                pTransConnectionIP = SIP_NULL;
            }
        }
        else
        {
            pTemp = pTransConnectionIP;
            sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
            pTransConnectionIP = SIP_NULL;
        }
    }
    else
    {
        pTemp = pTransConnectionIP;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pTransConnectionIP = SIP_NULL;
    }
    /* SIPALG 2.1 */
    /* 
       In case NAT is disabled, then there is no need to 
       perform any translation of the o-line present in 
       the media
     */
    if (isNatEnabled == SipFalse)
    {
        sipAlgFreeDialogParam (&pSessionDialogKey);
        pTemp = pTransConnectionIP;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pTransConnectionIP = SIP_NULL;
        return SipSuccess;
    }

    SIPDEBUG ((SIP_S8bit *) "Translating O-line in the media");
    pTemp = pOriginAddr;
    if (sdp_getAddrFromOrigin (pOrigin, &pTemp, &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   "In sipAlgTranslateMedia:sdp_getAddrFromOrigin failed\n");
        /*lakshmi - commented - fix */
/*       sip_memfree(0, (SIP_Pvoid*)&pVersionId,SIP_NULL); */
        sip_freeSdpOrigin (pOrigin);
        /*Fix for Memory leak - lakshmi */
        sipAlgFreeDialogParam (&pSessionDialogKey);
        /*      sip_memfree(0, (SIP_Pvoid*)&pSessionDialogKey,SIP_NULL); */
        sip_freeSdpConnection (pSessionConnection);
        sip_freeSdpMessage (pSdpMsg);
        sip_freeSipMsgBody (pMsgBody);
        pTemp = pTransConnectionIP;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pTransConnectionIP = SIP_NULL;
        return SipSuccess;
    }
    pOriginAddr = pTemp;
    if (sipAlgIsNumericAddress (pOriginAddr) == SipFail)
    {
        /* Might be a domain name. In that case no need to translate */
        /*sip_memfree (0, (SIP_Pvoid *) & pOriginAddr, SIP_NULL); *//*cert mem leak fix */
        /*lakshmi - commented - fix */
/*       sip_memfree(0, (SIP_Pvoid*)&pVersionId,SIP_NULL); */
        sip_freeSdpOrigin (pOrigin);
        /*Fix for Memory leak - lakshmi */
        sipAlgFreeDialogParam (&pSessionDialogKey);
        /*      sip_memfree(0, (SIP_Pvoid*)&pSessionDialogKey,SIP_NULL); */
        sip_freeSdpConnection (pSessionConnection);
        sip_freeSdpMessage (pSdpMsg);
        sip_freeSipMsgBody (pMsgBody);
        pTemp = pTransConnectionIP;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pTransConnectionIP = SIP_NULL;
        return SipSuccess;
    }

    if (sipAlgIsValidNumericAddress (pOriginAddr) == SipFail)
    {
        /* Might be a domain name. In that case no need to translate */
        /*sip_memfree (0, (SIP_Pvoid *) & pOriginAddr, SIP_NULL); *//*cert mem leak fix */
        /*lakshmi - commented - fix */
/*       sip_memfree(0, (SIP_Pvoid*)&pVersionId,SIP_NULL); */
        sip_freeSdpOrigin (pOrigin);
        /*Fix for Memory leak - lakshmi */
        sipAlgFreeDialogParam (&pSessionDialogKey);
        /*      sip_memfree(0, (SIP_Pvoid*)&pSessionDialogKey,SIP_NULL); */
        sip_freeSdpConnection (pSessionConnection);
        sip_freeSdpMessage (pSdpMsg);
        sip_freeSipMsgBody (pMsgBody);
        pTemp = pTransConnectionIP;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pTransConnectionIP = SIP_NULL;
        return SipSuccess;
    }
    /* convert the IP which in string format to U32 */
    if (sipAlginetStrToNet
        (inetAFINET, (SIP_S8bit *) pOriginAddr, (void *) &dOriginAddr,
         &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateReqUri:sipAlginetStrToNet failed\n");
        /*sip_memfree (0, (SIP_Pvoid *) & pOriginAddr, SIP_NULL); *//*cert mem leak fix */
        /*lakshmi - commented - fix */
/*       sip_memfree(0, (SIP_Pvoid*)&pVersionId,SIP_NULL); */
        sip_freeSdpOrigin (pOrigin);
        /*Fix for Memory leak - lakshmi */
        sipAlgFreeDialogParam (&pSessionDialogKey);
        /*      sip_memfree(0, (SIP_Pvoid*)&pSessionDialogKey,SIP_NULL); */
        sip_freeSdpConnection (pSessionConnection);
        sip_freeSdpMessage (pSdpMsg);
        sip_freeSipMsgBody (pMsgBody);
        /* Only once the IP is validated, this was invoked. Now if
           sipAlginetStrToNet fails, drop the packet */
        pTemp = pTransConnectionIP;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pTransConnectionIP = SIP_NULL;
        return SipFail;
    }

    /* allocate memory to natinfo structure */
    pSessionInfo = (SipNatInfo *) sip_memget (0, sizeof (SipNatInfo), &dError);

    if (pSessionInfo == SIP_NULL)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateMedia:Memory allocation failed\n");
        /*lakshmi - commented - fix */
/*       sip_memfree(0, (SIP_Pvoid*)&pVersionId,SIP_NULL); */
        sip_freeSdpOrigin (pOrigin);
        /*Fix for Memory leak - lakshmi */
        sipAlgFreeDialogParam (&pSessionDialogKey);
        /*      sip_memfree(0, (SIP_Pvoid*)&pSessionDialogKey,SIP_NULL); */
        sip_freeSdpConnection (pSessionConnection);
        sip_freeSdpMessage (pSdpMsg);
        sip_freeSipMsgBody (pMsgBody);
        pTemp = pTransConnectionIP;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pTransConnectionIP = SIP_NULL;
        return SipFail;
    }

    pSessionInfo->privateIP = dOriginAddr;
    pSessionInfo->privatePort = 0;
    pSessionInfo->mappedIP = OSIX_HTONL (natIP);
    pSessionInfo->mappedPort = 0;
    pSessionInfo->direction = direction;
    pSessionInfo->ifIndex = pNatHeaderInfo->u4IfNum;

    if (SipFail ==
        sipAlgPerformIPTranslation (pSessionInfo, pSessionDialogKey, pVersionId,
                                    isRequest))
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateMedia: sipAlgPerformIPTranslation failed- 2\n");
        pTemp = pSessionInfo;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pSessionInfo = SIP_NULL;
        /*lakshmi - commented - fix */
/*       sip_memfree(0, (SIP_Pvoid*)&pVersionId,SIP_NULL); */
        sip_freeSdpOrigin (pOrigin);
        /*Fix for Memory leak - lakshmi */
        sipAlgFreeDialogParam (&pSessionDialogKey);
        /*      sip_memfree(0, (SIP_Pvoid*)&pSessionDialogKey,SIP_NULL); */
        sip_freeSdpConnection (pSessionConnection);
        sip_freeSdpMessage (pSdpMsg);
        sip_freeSipMsgBody (pMsgBody);
        pTemp = pTransConnectionIP;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pTransConnectionIP = SIP_NULL;
        return SipFail;
    }

    if (pSessionInfo->direction == SipPktOut)
    {
        dOlineTransIP = pSessionInfo->mappedIP;
    }
    else
    {
        dOlineTransIP = pSessionInfo->privateIP;
    }
    pTemp = pSessionInfo;
    sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
    pSessionInfo = SIP_NULL;
    /*Fix for Memory leak - lakshmi */
    sipAlgFreeDialogParam (&pSessionDialogKey);
    /* sip_memfree(0, (SIP_Pvoid*)&pSessionDialogKey,SIP_NULL); */

    pTransOlineIP = (SIP_S8bit *) sip_memget
        (0, SIP_ALG_INET_ADDRSTRLEN, &dError);

    if (pTransOlineIP == SIP_NULL)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateMedia:Memory allocation failed\n");
        /*lakshmi - commented - fix */
/*       sip_memfree(0, (SIP_Pvoid*)&pVersionId,SIP_NULL); */
        sip_freeSdpOrigin (pOrigin);
        sip_freeSdpConnection (pSessionConnection);
        sip_freeSdpMessage (pSdpMsg);
        sip_freeSipMsgBody (pMsgBody);
        pTemp = pTransConnectionIP;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pTransConnectionIP = SIP_NULL;
        return SipFail;
    }
    sip_memset ((SIP_Pvoid) pTransOlineIP, 0, SIP_ALG_INET_ADDRSTRLEN);

    /* now convert the pMediaInfo->mappedIP to string */
    if (sipAlgInetNetToStr (inetAFINET, (void *) &dOlineTransIP,
                            pTransOlineIP, SIP_ALG_INET_ADDRSTRLEN,
                            &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateMedia:sipAlgInetNetToStr failed\n");
        pTemp = pTransOlineIP;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pTransOlineIP = SIP_NULL;
        /*lakshmi - commented - fix */
/*       sip_memfree(0, (SIP_Pvoid*)&pVersionId,SIP_NULL); */
        sip_freeSdpOrigin (pOrigin);
        sip_freeSdpConnection (pSessionConnection);
        sip_freeSdpMessage (pSdpMsg);
        sip_freeSipMsgBody (pMsgBody);
        pTemp = pTransConnectionIP;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pTransConnectionIP = SIP_NULL;
        return SipFail;
    }

    pTemp = ((SdpOrigin *) pOrigin)->pAddr;
    if (sdp_setAddrParamInOrigin (pTemp, pTransOlineIP, &dError, SipParamString)
        == SipFail)
    {
        sip_error (SIP_Minor,
                   "In sipAlgTranslateMedia: sdp_setAddrInOrigin failed\n");
        pTemp = pTransOlineIP;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pTransOlineIP = SIP_NULL;
        /*lakshmi - commented - fix */
/*       sip_memfree(0, (SIP_Pvoid*)&pVersionId,SIP_NULL); */
        sip_freeSdpOrigin (pOrigin);
        sip_freeSdpConnection (pSessionConnection);
        sip_freeSdpMessage (pSdpMsg);
        sip_freeSipMsgBody (pMsgBody);
        pTemp = pTransConnectionIP;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pTransConnectionIP = SIP_NULL;
        return SipFail;
    }
    ((SdpOrigin *) pOrigin)->pAddr = pTemp;

    pTemp = ((SdpMessage *) pSdpMsg)->pOrigin;
    if (sdp_setOriginField (pTemp, pOrigin,
                            &dError, SipSdpParamOrigin) == SipFail)
    {
        sip_error (SIP_Minor, "In sipAlgTranslateMedia:sdp_setOrigin failed\n");
        /*lakshmi - commented - fix */
/*       sip_memfree(0, (SIP_Pvoid*)&pVersionId,SIP_NULL); */
        sip_freeSdpOrigin (pOrigin);
        sip_freeSdpConnection (pSessionConnection);
        sip_freeSdpMessage (pSdpMsg);
        sip_freeSipMsgBody (pMsgBody);
        pTemp = pTransConnectionIP;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pTransConnectionIP = SIP_NULL;
        return SipFail;

    }
    ((SdpMessage *) pSdpMsg)->pOrigin = pTemp;

    sip_freeSdpOrigin (pOrigin);
    sip_freeSdpConnection (pSessionConnection);
    sip_freeSdpMessage (pSdpMsg);
    sip_freeSipMsgBody (pMsgBody);

    SIPDEBUG ((SIP_S8bit *) "5. Exiting sipAlgTranslateMedia");
    return SipSuccess;
}

/***************************************************************************
** FUNCTION: sipAlgTranslateReqUri
** DESCRIPTION: This function translates the Req-URI 
**
** PARAMETERS:
**      pSipMsg(IN)                - Pointer to SIP Msg 
**        direction(IN)            - specifies whether this is OUT/IN Msg
**        pNatHeaderInfo(IN)       - pointer to tHeaderInfo which has NAT info 
**
** Returns : SUCCESS/FAILURE 
***************************************************************************/

SipBool
sipAlgTranslateReqUri (SipMessage * pSipMsg, SipAlgPktInOut direction,
                       tHeaderInfo * pNatHeaderInfo)
{
    /* This pointer is used to avoid strict-aliasing warning */
    SIP_Pvoid           pTemp = SIP_NULL;
    SipReqLine         *pReqline = SIP_NULL;
    SipAddrSpec        *pAddrSpec = SIP_NULL;
    SipUrl             *pSipUrl = SIP_NULL;
    SipError            dError;
    SipNatInfo         *pReqUriInfo = SIP_NULL;
    SIP_S8bit          *pTempOrigHost = SIP_NULL, *pTempTransIP = SIP_NULL;
    SIP_U16bit          dPort = 0, dTransPort = 0;
    SIP_U32bit          dTempOrigIP = 0, dTransIP = 0;
    en_Protocol         dProtocol = en_udp;
    en_SipBoolean       dIsWanIp = SipFalse;
    en_IpAddrType       dIPAddrType = en_fail;

    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgTranslateReqUri ");

    /* Get the Request-line from SIP Msg */
    if (sip_getReqLineFromSipReqMsg (pSipMsg, &pReqline, &dError) == SipFail)
    {
        sip_error (SIP_Minor, "In sipAlgTranslateReqUri:\
                   sip_getReqLineFromSipReqMsg failed\n");
        return SipFail;
    }
    /* Getting addr-spec from Req-line */
    if (sip_getAddrSpecFromReqLine (pReqline, &pAddrSpec, &dError) == SipFail)
    {
        sip_error (SIP_Minor, "In sipAlgTranslateReqUri:\
                   sip_getAddrSpecFromReqLine failed\n");
        sip_freeSipReqLine (pReqline);
        return SipFail;
    }
    /* Getting url from addr-spec */
    if (sip_getUrlFromAddrSpec (pAddrSpec, &pSipUrl, &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateReqUri:sip_getUrlFromAddrSpec failed\n");
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipReqLine (pReqline);
        return SipFail;
    }
    /* Getting host from url */
    if (sip_getHostFromUrl (pSipUrl, &pTempOrigHost, &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateReqUri:sip_getHostFromUrl failed\n");
        sip_freeSipUrl (pSipUrl);
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipReqLine (pReqline);
        return SipFail;
    }
    /* invoke the function to see whether psipUrl->pHost
       a domain or ip */
    if (sipAlgIsNumericAddress ((SIP_U8bit *) pTempOrigHost) == SipFail)
    {
        sip_freeSipUrl (pSipUrl);
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipReqLine (pReqline);
        return SipSuccess;
    }
    if (sipAlgIsValidNumericAddress ((SIP_U8bit *) pTempOrigHost) == SipFail)
    {
        sip_freeSipUrl (pSipUrl);
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipReqLine (pReqline);
        return SipFail;
    }

    /* Getting port from url */
    if (sip_getPortFromUrl (pSipUrl, &dPort, &dError) == SipFail)
    {
        if (dError != E_NO_EXIST)
        {
            sip_error (SIP_Minor,
                       " In sipAlgTranslateReqUri:sip_getPortFromUrl failed\n");
            sip_freeSipUrl (pSipUrl);
            sip_freeSipAddrSpec (pAddrSpec);
            sip_freeSipReqLine (pReqline);
            return SipFail;
        }
        dPort = 5060;
    }

    /* allocate memory to natinfo structure */
    pReqUriInfo = (SipNatInfo *) sip_memget (0, sizeof (SipNatInfo), &dError);

    if (pReqUriInfo == SIP_NULL)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateReqUri:Memory allocation failed\n");
        sip_freeSipUrl (pSipUrl);
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipReqLine (pReqline);
        return SipFail;
    }

    /* convert the IP which in string format to U32 */
    if (sipAlginetStrToNet (inetAFINET, pTempOrigHost, (void *) &dTempOrigIP,
                            &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateReqUri:sipAlginetStrToNet failed\n");
        pTemp = pReqUriInfo;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pReqUriInfo = SIP_NULL;
        sip_freeSipUrl (pSipUrl);
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipReqLine (pReqline);
        return SipFail;
    }
    /* get the user type ie the ip-address type */
    dIPAddrType = sipAlgGetUserType (dTempOrigIP);
    if (dIPAddrType == en_fail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateReqUri:sipAlgGetUserType failed\n");
        pTemp = pReqUriInfo;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pReqUriInfo = SIP_NULL;
        sip_freeSipUrl (pSipUrl);
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipReqLine (pReqline);
        return SipFail;
    }
    else if (dIPAddrType == en_wan)
    {
        /* check whether this is wan-ip or not */
        dIsWanIp = sipAlgIsWANIPAddress (dTempOrigIP);
        if (((dIsWanIp == SipTrue) && (SipPktOut == direction))
            || (dIsWanIp == SipFalse))
        {
            pTemp = pReqUriInfo;
            sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
            pReqUriInfo = SIP_NULL;
            sip_freeSipUrl (pSipUrl);
            sip_freeSipAddrSpec (pAddrSpec);
            sip_freeSipReqLine (pReqline);
            return SipSuccess;
        }
    }
    if (direction == SipPktIn)
    {
        pReqUriInfo->mappedIP = dTempOrigIP;
        pReqUriInfo->mappedPort = dPort;
    }
    else
    {
        pReqUriInfo->privateIP = dTempOrigIP;
        pReqUriInfo->privatePort = dPort;
    }
    pReqUriInfo->outsideIP = pNatHeaderInfo->u4OutIpAddr;
    pReqUriInfo->outsidePort = pNatHeaderInfo->u2OutPort;
    pReqUriInfo->direction = direction;
    pReqUriInfo->ifIndex = pNatHeaderInfo->u4IfNum;

    /* lakshmi   added - NAT API Enhancements - START */
    pReqUriInfo->parity = en_parityAny;
    /* NAT API Enhancements - END */
    /* since this is Requri, the protocol value should be 
       passed as per Headerinfo to sipAlgPerformTranslation */
    if (pNatHeaderInfo->u1PktType == NAT_TCP)
    {
        dProtocol = en_tcp;
    }
    else if (pNatHeaderInfo->u1PktType == NAT_UDP)
    {
        dProtocol = en_udp;
    }
#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {
        Sdf_fn_printTimeStamp ("sipAlgTranslateReqUri:Pre - PerformTrans() 1");
    }
#endif

    if (sipAlgPerformTranslation (pReqUriInfo, dProtocol,
                                  SipFalse, SIP_NULL,
                                  pNatHeaderInfo->u4IfNum) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateReqUri:sipAlgPerformTranslation failed\n");
        pTemp = pReqUriInfo;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pReqUriInfo = SIP_NULL;
        sip_freeSipUrl (pSipUrl);
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipReqLine (pReqline);
#ifdef SDF_TIMESTAMP
        if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
        {
            Sdf_fn_printTimeStamp
                ("sipAlgTranslateReqUri:PerformTrans() - fail");
        }
#endif
        return SipFail;
    }
#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {
        Sdf_fn_printTimeStamp ("sipAlgTranslateReqUri:Post - PerformTrans()");
    }
#endif
    if (direction == SipPktOut)
    {
        dTransIP = pReqUriInfo->mappedIP;
        dTransPort = pReqUriInfo->mappedPort;
    }
    else
    {
        dTransIP = pReqUriInfo->privateIP;
        dTransPort = pReqUriInfo->privatePort;
    }

    /* here we have the translated ip and port; update this 
       in the sip header */
    if (sip_setPortInUrl (pSipUrl, dTransPort, &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateReqUri:sip_setPortInUrl failed\n");
        pTemp = pReqUriInfo;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pReqUriInfo = SIP_NULL;
        sip_freeSipUrl (pSipUrl);
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipReqLine (pReqline);
        return SipFail;
    }

    pTempTransIP = (SIP_S8bit *) sip_memget
        (0, SIP_ALG_INET_ADDRSTRLEN, &dError);
    if (pTempTransIP == SIP_NULL)
    {

        pTemp = pReqUriInfo;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pReqUriInfo = SIP_NULL;
        return SipFail;
    }
    sip_memset ((SIP_Pvoid) pTempTransIP, 0, SIP_ALG_INET_ADDRSTRLEN);

    /* now convert the pMediaInfo->mappedIP to string */
    if (sipAlgInetNetToStr (inetAFINET, (void *) &(dTransIP),
                            pTempTransIP, SIP_ALG_INET_ADDRSTRLEN,
                            &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateReqUri:sipAlgInetNetToStr failed\n");
        pTemp = pTempTransIP;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pTempTransIP = SIP_NULL;
        pTemp = pReqUriInfo;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pReqUriInfo = SIP_NULL;
        sip_freeSipUrl (pSipUrl);
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipReqLine (pReqline);
        return SipFail;
    }
    /* set the translated ip */
    if (sip_setHostInUrl (pSipUrl, pTempTransIP, &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateReqUri:sip_setHostInUrl failed\n");
        pTemp = pReqUriInfo;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pReqUriInfo = SIP_NULL;
        sip_freeSipUrl (pSipUrl);
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipReqLine (pReqline);
        return SipFail;
    }
    /* free all the local refeences */
    pTemp = pReqUriInfo;
    sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
    pReqUriInfo = SIP_NULL;
    sip_freeSipUrl (pSipUrl);
    sip_freeSipAddrSpec (pAddrSpec);
    sip_freeSipReqLine (pReqline);

    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgTranslateReqUri");
    return SipSuccess;
}

/***************************************************************************
** FUNCTION: sipAlgTranslateRouteHeader
** DESCRIPTION: This function translates the Route Header 
**
** PARAMETERS:
**      pSipMsg(IN)                - Pointer to SIP Msg 
**        direction(IN)            - specifies whether this is OUT/IN Msg
**        pNatHeaderInfo(IN)       - pointer to tHeaderInfo which has NAT info 
**
** Returns : SUCCESS/FAILURE 
****************************************************************************/

SipBool
sipAlgTranslateRouteHdr (SipMessage * pSipMsg, SipAlgPktInOut direction,
                         tHeaderInfo * pNatHeaderInfo)
{
    /* This pointer is used to avoid strict-aliasing warning */
    SIP_Pvoid           pTemp = SIP_NULL;
    SipAddrSpec        *pAddrSpec = SIP_NULL;
    SipUrl             *pSipUrl = SIP_NULL;
    SipHeader           dSipHeader;
    SipNatInfo         *pRouteInfo = SIP_NULL;
    SIP_U16bit          dPort = 0;
    SIP_U32bit          dTempOrigIP = 0;
    en_Protocol         dProtocol = 0;
    SIP_S8bit          *pTempOrigHost = SIP_NULL, *pTempTransIP = SIP_NULL;
    SipError            dError;
    SIP_U32bit          size = 0;

    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgTranslateRouteHdr");

    if (sip_getHeaderCount (pSipMsg, SipHdrTypeRoute, &size,
                            &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateRouteHdr:sip_getHeaderCount failed\n");
        return SipFail;
    }

    if (size == 0)
    {
        return SipSuccess;
    }
    /* Get the topmost Route Header from the sip msg struct 
       Translation is reqd only for the top most Route */
    if (sip_getHeaderAtIndex (pSipMsg, SipHdrTypeRoute,
                              &dSipHeader, 0, &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateRouteHdr:sip_getHeaderAtIndex failed\n");
        return SipFail;
    }
    /* Get the addr spec */
    if (sip_getAddrSpecFromRouteHdr (&dSipHeader,
                                     &pAddrSpec, &dError) == SipFail)
    {
        sip_error (SIP_Minor, " In sipAlgTranslateRouteHdr:\
                  sip_getAddrSpecFromRouteHdr failed\n");
        sip_freeSipHeader (&dSipHeader);
        return SipFail;
    }
    /* Get the url from addr spec */
    if (sip_getUrlFromAddrSpec (pAddrSpec, &pSipUrl, &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateRouteHdr:sip_getUrlFromAddrSpec failed\n");
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipHeader (&dSipHeader);
        return SipFail;
    }
    /* Get the host from url */
    if (sip_getHostFromUrl (pSipUrl, &pTempOrigHost, &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateRouteHdr:sip_getHostFromUrl failed\n");
        sip_freeSipUrl (pSipUrl);
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipHeader (&dSipHeader);
        return SipFail;
    }
    /* invoke the function to see whether psipUrl->pHost
       a domain or ip */
    if (sipAlgIsNumericAddress ((SIP_U8bit *) pTempOrigHost) == SipFail)
    {
        sip_freeSipUrl (pSipUrl);
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipHeader (&dSipHeader);
        return SipSuccess;
    }
    if (sipAlgIsValidNumericAddress ((SIP_U8bit *) pTempOrigHost) == SipFail)
    {
        sip_freeSipUrl (pSipUrl);
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipHeader (&dSipHeader);
        return SipFail;
    }

    if (sip_getPortFromUrl (pSipUrl, &dPort, &dError) == SipFail)
    {
        if (dError != E_NO_EXIST)
        {
            sip_error (SIP_Minor,
                       " In sipAlgTranslateRouteHdr:sip_getPortFromUrl failed\n");
            sip_freeSipUrl (pSipUrl);
            sip_freeSipAddrSpec (pAddrSpec);
            sip_freeSipHeader (&dSipHeader);
            return SipFail;
        }
        dPort = 5060;
    }
    /* allocate memory to natinfo structure */
    pRouteInfo = (SipNatInfo *) sip_memget (0, sizeof (SipNatInfo), &dError);

    if (pRouteInfo == SIP_NULL)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateRouteHdr:Memory allocation failed\n");
        sip_freeSipUrl (pSipUrl);
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipHeader (&dSipHeader);
        return SipFail;
    }
    /* Get the ip address in U32 format from string */
    if (sipAlginetStrToNet (inetAFINET, pTempOrigHost, (void *) &dTempOrigIP,
                            &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateRouteHdr:sipAlginetStrToNet failed\n");
        pTemp = pRouteInfo;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pRouteInfo = SIP_NULL;
        sip_freeSipUrl (pSipUrl);
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipHeader (&dSipHeader);
        return SipFail;
    }
    /* since this function will be called only for the incoming
       request, we dont need to check for the direction.. the 
       ip and port extracted from the Route is added in the mapped 
       ip and port */
    pRouteInfo->mappedIP = dTempOrigIP;
    pRouteInfo->mappedPort = dPort;
    pRouteInfo->outsideIP = pNatHeaderInfo->u4OutIpAddr;
    pRouteInfo->outsidePort = pNatHeaderInfo->u2OutPort;
    pRouteInfo->direction = direction;
    pRouteInfo->ifIndex = pNatHeaderInfo->u4IfNum;
/* lakshmi   added - NAT API Enhancements - START */
    pRouteInfo->parity = en_parityAny;
/* NAT API Enhancements - END */

    /* since this is Route Hdr, the protocol value should be 
       passed as per Headerinfo to sipAlgPerformTranslation */
    if (pNatHeaderInfo->u1PktType == NAT_TCP)
    {
        dProtocol = en_tcp;
    }
    else if (pNatHeaderInfo->u1PktType == NAT_UDP)
    {
        dProtocol = en_udp;
    }
#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {
        Sdf_fn_printTimeStamp
            ("sipAlgTranslateRouteHeader:Pre - PerformTrans() 2");
    }
#endif

    if (sipAlgPerformTranslation (pRouteInfo, dProtocol,
                                  SipFalse, SIP_NULL,
                                  pNatHeaderInfo->u4IfNum) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateRouteHdr:sipAlgPerformTranslation failed\n");
        pTemp = pRouteInfo;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pRouteInfo = SIP_NULL;
        sip_freeSipUrl (pSipUrl);
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipHeader (&dSipHeader);
#ifdef SDF_TIMESTAMP
        if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
        {
            Sdf_fn_printTimeStamp
                ("sipAlgTranslateRouteHeader:Fail - PerformTrans() 2");
        }
#endif
        return SipFail;
    }
#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {
        Sdf_fn_printTimeStamp
            ("sipAlgTranslateRouteHeader:Post - PerformTrans() 2");
    }
#endif

    /* here we have the translated ip and port */
    if (sip_setPortInUrl (pSipUrl, pRouteInfo->privatePort, &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateRouteHdr:sip_setPortInUrl failed\n");
        pTemp = pRouteInfo;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pRouteInfo = SIP_NULL;
        sip_freeSipUrl (pSipUrl);
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipHeader (&dSipHeader);
        return SipFail;
    }

    pTempTransIP = (SIP_S8bit *) sip_memget
        (0, SIP_ALG_INET_ADDRSTRLEN, &dError);
    if (pTempTransIP == SIP_NULL)
    {

        pTemp = pRouteInfo;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pRouteInfo = SIP_NULL;
        return SipFail;
    }
    sip_memset ((SIP_Pvoid) pTempTransIP, 0, SIP_ALG_INET_ADDRSTRLEN);

    /* now convert the pMediaInfo->mappedIP to string */
    if (sipAlgInetNetToStr (inetAFINET, (void *) &(pRouteInfo->privateIP),
                            pTempTransIP, SIP_ALG_INET_ADDRSTRLEN,
                            &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateRouteHdr:sipAlgInetNetToStr failed\n");
        sip_memfree (0, (SIP_Pvoid) & pTempTransIP, SIP_NULL);
        pTemp = pRouteInfo;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pRouteInfo = SIP_NULL;
        sip_freeSipUrl (pSipUrl);
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipHeader (&dSipHeader);
        return SipFail;
    }

    if (sip_setHostInUrl (pSipUrl, pTempTransIP, &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateRouteHdr:sip_setHostInUrl failed\n");
        pTemp = pRouteInfo;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pRouteInfo = SIP_NULL;
        sip_freeSipUrl (pSipUrl);
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipHeader (&dSipHeader);
        return SipFail;
    }
    pTemp = pRouteInfo;
    sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
    pRouteInfo = SIP_NULL;
    sip_freeSipUrl (pSipUrl);
    sip_freeSipAddrSpec (pAddrSpec);
    sip_freeSipHeader (&dSipHeader);

    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgTranslateRouteHdr");
    return SipSuccess;
}

/***************************************************************************
** FUNCTION: sipAlgTranslateRecordRouteHeader
** DESCRIPTION: This function translates the Record-Route Header 
**
** PARAMETERS:
**         pSipMsg(IN)                - Pointer to SIP Msg 
**        direction(IN)            - specifies whether this is OUT/IN Msg
**        pNatHeaderInfo(IN)       - pointer to tHeaderInfo which has NAT info 
**
** Returns : SUCCESS/FAILURE 
****************************************************************************/
SipBool
sipAlgTranslateRecordRouteHdr (SipMessage * pSipMsg, SipAlgPktInOut direction,
                               tHeaderInfo * pNatHeaderInfo)
{
    /* This pointer is used to avoid strict-aliasing warning */
    SIP_Pvoid           pTemp = SIP_NULL;
    SipError            dError;
    SIP_U32bit          size = 0;
    SipAddrSpec        *pAddrSpec = SIP_NULL;
    SipUrl             *pSipUrl = SIP_NULL;
    SipHeader           dSipHeader;
    SipNatInfo         *pRecordRouteInfo = SIP_NULL;
    en_Protocol         dProtocol = 0;
    SIP_U16bit          dPort = 0, dTransPort = 0;
    SIP_U32bit          dTempOrigIP = 0, dTransIP = 0;
    SIP_S8bit          *pTempOrigHost = SIP_NULL, *pTempTransIP = SIP_NULL;
    en_SipBoolean       dIsWanIp = SipFalse;

    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgTranslateRecordRouteHdr ");

    /* Get the count of record route header */
    if (sip_getHeaderCount (pSipMsg, SipHdrTypeRecordRoute, &size,
                            &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateRecordRouteHdr:sip_getHeaderCount failed\n");
        return SipFail;
    }
    /* if size is zero then there is no record route header present..return */
    if (size == 0)
    {
        SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgTranslateRecordRouteHdr ");
        return SipSuccess;
    }
    /* only the top most record route header needs translation.
       get the header from sip msg */
    if (sip_getHeaderAtIndex (pSipMsg, SipHdrTypeRecordRoute,
                              &dSipHeader, 0, &dError) == SipFail)
    {
        sip_error (SIP_Minor, " In sipAlgTranslateRecordRouteHdr:\
                   sip_getHeaderAtIndex failed\n");
        return SipFail;
    }
    /* get addr spec from record route header */
    if (sip_getAddrSpecFromRecordRouteHdr (&dSipHeader,
                                           &pAddrSpec, &dError) == SipFail)
    {
        sip_error (SIP_Minor, " In sipAlgTranslateRecordRouteHdr:\
                   sip_getAddrSpecFromRecordRouteHdr failed\n");
        sip_freeSipHeader (&dSipHeader);
        return SipFail;
    }
    /* Get the url from addr spec */
    if (sip_getUrlFromAddrSpec (pAddrSpec, &pSipUrl, &dError) == SipFail)
    {
        sip_error (SIP_Minor, "In sipAlgTranslateRecordRouteHdr:\
                   sip_getUrlFromAddrSpec failed\n");
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipHeader (&dSipHeader);
        return SipFail;
    }
    /* get the host from url */
    if (sip_getHostFromUrl (pSipUrl, &pTempOrigHost, &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateRecordRouteHdr:sip_getHostFromUrl failed\n");
        sip_freeSipUrl (pSipUrl);
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipHeader (&dSipHeader);
        return SipFail;
    }
    /* invoke the function to see whether psipUrl->pHost
       a domain or ip */
    if (sipAlgIsNumericAddress ((SIP_U8bit *) pTempOrigHost) == SipFail)
    {
        sip_freeSipUrl (pSipUrl);
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipHeader (&dSipHeader);
        SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgTranslateRecordRouteHdr");
        return SipSuccess;
    }
    if (sipAlgIsValidNumericAddress ((SIP_U8bit *) pTempOrigHost) == SipFail)
    {
        sip_freeSipUrl (pSipUrl);
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipHeader (&dSipHeader);
        SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgTranslateRecordRouteHdr");
        return SipFail;
    }

    /* get the port from url */
    if (sip_getPortFromUrl (pSipUrl, &dPort, &dError) == SipFail)
    {
        if (dError != E_NO_EXIST)
        {
            sip_error (SIP_Minor,
                       " In sipAlgTranslateRecordRouteHdr:sip_getPortFromUrl failed\n");
            sip_freeSipUrl (pSipUrl);
            sip_freeSipAddrSpec (pAddrSpec);
            sip_freeSipHeader (&dSipHeader);
            return SipFail;
        }
        dPort = 5060;
    }

    /* allocate memory to natinfo structure */
    pRecordRouteInfo = (SipNatInfo *) sip_memget
        (0, sizeof (SipNatInfo), &dError);

    if (pRecordRouteInfo == SIP_NULL)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateRecordRouteHdr:Memory allocation failed\n");
        sip_freeSipUrl (pSipUrl);
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipHeader (&dSipHeader);
        return SipFail;
    }
    /* Get the ip address in U32 format from string */
    if (sipAlginetStrToNet (inetAFINET, pTempOrigHost, (void *) &dTempOrigIP,
                            &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateRecordRouteHdr:sipAlginetStrToNet failed\n");
        pTemp = pRecordRouteInfo;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pRecordRouteInfo = SIP_NULL;
        sip_freeSipUrl (pSipUrl);
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipHeader (&dSipHeader);
        return SipFail;
    }
    /* check whether this is wan-ip */
#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {
        Sdf_fn_printTimeStamp
            ("sipAlgTranslateRecordRouteHeader:Pre-isWanIpAddress()");
    }
#endif

    dIsWanIp = sipAlgIsWANIPAddress (dTempOrigIP);
#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {
        Sdf_fn_printTimeStamp
            ("sipAlgTranslateRecordRouteHeader:Post-isWanIpAddress()");
    }
#endif

    if (((SipPktOut == direction) && (dIsWanIp == SipTrue)) ||
        ((SipPktIn == direction) && (dIsWanIp == SipFalse)))
    {
        /*Fix for memory leak - lakshmi */
        pTemp = pRecordRouteInfo;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pRecordRouteInfo = SIP_NULL;
        sip_freeSipUrl (pSipUrl);
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipHeader (&dSipHeader);
        SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgTranslateRecordRouteHdr");
        return SipSuccess;
    }

    if (direction == SipPktOut)
    {
        pRecordRouteInfo->privateIP = dTempOrigIP;
        pRecordRouteInfo->privatePort = dPort;
    }
    else if (direction == SipPktIn)
    {
        pRecordRouteInfo->mappedIP = dTempOrigIP;
        pRecordRouteInfo->mappedPort = dPort;
    }

    pRecordRouteInfo->outsideIP = pNatHeaderInfo->u4OutIpAddr;
    pRecordRouteInfo->outsidePort = pNatHeaderInfo->u2OutPort;
    pRecordRouteInfo->direction = direction;
    pRecordRouteInfo->ifIndex = pNatHeaderInfo->u4IfNum;
    /* NAT API Enhancements */
    pRecordRouteInfo->parity = en_parityAny;

    /* since this is Route Hdr, the protocol value should be 
       passed as per Headerinfo to sipAlgPerformTranslation */
    if (pNatHeaderInfo->u1PktType == NAT_TCP)
    {
        dProtocol = en_tcp;
    }
    else if (pNatHeaderInfo->u1PktType == NAT_UDP)
    {
        dProtocol = en_udp;
    }
#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {
        Sdf_fn_printTimeStamp
            ("sipAlgTranslateRecordRouteHeader:Pre - PerformTrans() 3");
    }
#endif
    if (sipAlgPerformTranslation (pRecordRouteInfo, dProtocol,
                                  SipFalse, SIP_NULL,
                                  pNatHeaderInfo->u4IfNum) == SipFail)
    {
        sip_error (SIP_Minor, "In sipAlgTranslateRecordRouteHdr:\
                   sipAlgPerformTranslation failed\n");
        pTemp = pRecordRouteInfo;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pRecordRouteInfo = SIP_NULL;
        sip_freeSipUrl (pSipUrl);
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipHeader (&dSipHeader);
#ifdef SDF_TIMESTAMP
        if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
        {
            Sdf_fn_printTimeStamp
                ("sipAlgTranslateRecordRouteHeader:Fail - PerformTrans() 3");
        }
#endif
        return SipFail;
    }
#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {
        Sdf_fn_printTimeStamp
            ("sipAlgTranslateRecordRouteHeader:Post - PerformTrans() 3");
    }
#endif
    /* now we need to set the ip and port in sip-msg as per 
       the direction and ip type */
    if (direction == SipPktOut)
    {
        dTransIP = pRecordRouteInfo->mappedIP;
        dTransPort = pRecordRouteInfo->mappedPort;
    }
    else if (direction == SipPktIn)
    {
        if (dIsWanIp == SipTrue)
        {
            dTransIP = pRecordRouteInfo->privateIP;
            dTransPort = pRecordRouteInfo->privatePort;
        }
    }

    /* here we have the translated ip and port */
    if (sip_setPortInUrl (pSipUrl, dTransPort, &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateRecordRouteHdr:sip_setPortInUrl failed\n");
        pTemp = pRecordRouteInfo;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pRecordRouteInfo = SIP_NULL;
        sip_freeSipUrl (pSipUrl);
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipHeader (&dSipHeader);
        return SipFail;
    }
    pTempTransIP = (SIP_S8bit *) sip_memget
        (0, SIP_ALG_INET_ADDRSTRLEN, &dError);
    if (pTempTransIP == SIP_NULL)
    {

        pTemp = pRecordRouteInfo;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pRecordRouteInfo = SIP_NULL;
        return SipFail;
    }
    sip_memset ((SIP_Pvoid) pTempTransIP, 0, SIP_ALG_INET_ADDRSTRLEN);

    /* now convert the pMediaInfo->mappedIP to string */
    if (sipAlgInetNetToStr (inetAFINET, (void *) &(dTransIP),
                            pTempTransIP, SIP_ALG_INET_ADDRSTRLEN,
                            &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateRecordRouteHdr:sipAlgInetNetToStr failed\n");
        sip_memfree (0, (SIP_Pvoid) & pTempTransIP, SIP_NULL);
        pTemp = pRecordRouteInfo;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pRecordRouteInfo = SIP_NULL;
        sip_freeSipUrl (pSipUrl);
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipHeader (&dSipHeader);
        return SipFail;
    }

    if (sip_setHostInUrl (pSipUrl, pTempTransIP, &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateRecordRouteHdr:sip_setHostInUrl failed\n");
        pTemp = pRecordRouteInfo;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pRecordRouteInfo = SIP_NULL;
        sip_freeSipUrl (pSipUrl);
        sip_freeSipAddrSpec (pAddrSpec);
        sip_freeSipHeader (&dSipHeader);
        return SipFail;
    }
    pTemp = pRecordRouteInfo;
    sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
    pRecordRouteInfo = SIP_NULL;
    sip_freeSipUrl (pSipUrl);
    sip_freeSipAddrSpec (pAddrSpec);
    sip_freeSipHeader (&dSipHeader);

    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgTranslateRecordRouteHdr");
    return SipSuccess;
}

/*****************************************************************************
** FUNCTION: sipAlgTranslateViaHdr
** DESCRIPTION: This function translates the Via Hdr 
**
** PARAMETERS:
**         pSipMsg(IN)                - Pointer to SIP Msg 
**        direction(IN)            - specifies whether this is OUT/IN Msg
**        pNatHeaderInfo(IN)       - pointer to tHeaderInfo which has NAT info 
**
** Returns : SUCCESS/FAILURE 
*****************************************************************************/

SipBool
sipAlgTranslateViaHdr (SipMessage * pSipMsg, SipAlgPktInOut direction,
                       tHeaderInfo * pNatHeaderInfo)
{
    /* This pointer is used to avoid strict-aliasing warning */
    SIP_Pvoid           pTemp = SIP_NULL;
    SIP_S8bit          *pTempViaSentBy = SIP_NULL, *pTempViaSentProto =
        SIP_NULL;
    SIP_S8bit          *pViaSentBy = SIP_NULL;
    SIP_S8bit          *pTempPort = SIP_NULL;
    SIP_S8bit          *pTempTransSentBy = SIP_NULL, *pTempArray = SIP_NULL;
    SIP_S8bit           dTempPort[SIP_ALG_PORT_LEN];
    SipError            dError;
    SipHeader           dViaHeader;
    SipNatInfo         *pViaInfo = SIP_NULL;
    en_Protocol         dProtocol = 0;
    SIP_U32bit          dTempOrigIP = 0, dTransIP = 0;
    SIP_U16bit          dTransPort = 0;
    SIP_S8bit          *pTempIP = SIP_NULL;
    en_SipBoolean       dIsWanIp = SipFalse;
    SIP_U32bit          natIP = 0;
    en_SipBoolean       isNatEnabled = SipTrue;
    SIP_U32bit          retVal;
    SIP_U16bit          dInterfaceIndex = 0;
    /*RECEIVE and RPORT related changes STARTS */
    SIP_S8bit          *pReceivedParamHost = SIP_NULL;
    SIP_U32bit          dViaParamCount = 0, dIndex = 0;
    SipParam           *pViaParam = SIP_NULL;
    SIP_S8bit          *pRport = SIP_NULL;
    SIP_S8bit          *pTransRPort = SIP_NULL;
    SIP_S8bit          *pTransRecdIp = SIP_NULL;
    /*RECEIVED and RPORT related changes ends */
    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgTranslateViaHdr");

    /* first get the  via-header from the sip-msg */
    if (sip_getHeaderAtIndex (pSipMsg, SipHdrTypeVia,
                              &dViaHeader, 0, &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateViaHdr:sip_getHeaderAtIndex failed\n");
        return SipFail;
    }
    /* get the sentby from viahdr..this contains the ip and port */
    if (sip_getSentByFromViaHdr (&dViaHeader, &pTempViaSentBy,
                                 &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateViaHdr:sip_getSentByFromViaHdr failed\n");
        sip_freeSipHeader (&dViaHeader);
        return SipFail;
    }
    if (pTempViaSentBy != SIP_NULL)
    {
        pTempArray = sip_strdup (pTempViaSentBy, APP_MEM_ID);
    }

    if (pTempArray == NULL)
    {
        sip_error (SIP_Critical,
                   " In sipAlgTranslateViaHdr:pTempArray is NULL\n");
        sip_freeSipHeader (&dViaHeader);
        return SipFail;
    }
    /* Here dTempArray contains either the domain name or ip:port
       if it is ip:port, tokenize it and store ip and port */

    pViaSentBy = STRCHR (pTempArray, ':');
    if (pViaSentBy != NULL)
    {
        pTempPort = pViaSentBy + 1;
        *pViaSentBy = '\0';
    }
    pViaSentBy = pTempArray;

    /* Invoke the function to see whether pViaSentBy
       a domain or ip */
    if (sipAlgIsNumericAddress ((SIP_U8bit *) pViaSentBy) == SipFail)
    {
        sip_memfree (0, (SIP_Pvoid) & pTempArray, SIP_NULL);
        sip_freeSipHeader (&dViaHeader);
        return SipSuccess;
    }
    if (sipAlgIsValidNumericAddress ((SIP_U8bit *) pViaSentBy) == SipFail)
    {
        sip_memfree (0, (SIP_Pvoid) & pTempArray, SIP_NULL);
        sip_freeSipHeader (&dViaHeader);
        return SipSuccess;
    }

    /* now get the transport type from viahdr..this needs
       be passed to the sipAlgPerformTranslation */
    if (sip_getSentProtocolFromViaHdr (&dViaHeader,
                                       &pTempViaSentProto, &dError) == SipFail)
    {
        sip_error (SIP_Minor, "InsipAlgTranslateViaHdr:\
                   sip_getSentProtocolFromViaHdr failed\n");
        sip_memfree (0, (SIP_Pvoid) & pTempArray, SIP_NULL);
        sip_freeSipHeader (&dViaHeader);
        return SipFail;
    }
    if (sip_strstr (pTempViaSentProto, "TCP") != SIP_NULL)
    {
        dProtocol = en_tcp;
    }
    else if (sip_strstr (pTempViaSentProto, "UDP") != SIP_NULL)
    {
        dProtocol = en_udp;
    }
    /* allocate memory to natinfo structure */
    pViaInfo = (SipNatInfo *) sip_memget (0, sizeof (SipNatInfo), &dError);

    if (pViaInfo == SIP_NULL)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateViaHdr:Memory allocation failed\n");
        sip_memfree (0, (SIP_Pvoid) & pTempArray, SIP_NULL);
        sip_freeSipHeader (&dViaHeader);
        return SipFail;
    }

    /* Get the ip address in U32 format from string */
    if (sipAlginetStrToNet (inetAFINET, pViaSentBy, (void *) &dTempOrigIP,
                            &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateViaHdr:sipAlginetStrToNet failed\n");
        sip_memfree (0, (SIP_Pvoid) & pTempArray, SIP_NULL);
        pTemp = pViaInfo;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pViaInfo = SIP_NULL;
        sip_freeSipHeader (&dViaHeader);
        return SipFail;
    }
    /* check whether this is wan-ip */
#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {
        Sdf_fn_printTimeStamp ("sipAlgTranslateviaHeader:Pre-isWanIpAddress()");
    }
#endif

    dIsWanIp = sipAlgIsWANIPAddress (dTempOrigIP);
#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {
        Sdf_fn_printTimeStamp
            ("sipAlgTranslateviaHeader:Post-isWanIpAddress()");
    }
#endif

    if (((SipPktOut == direction) && (dIsWanIp == SipTrue)) ||
        ((SipPktIn == direction) && (dIsWanIp == SipFalse)))
    {
#ifdef SDF_TIMESTAMP
        if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
        {
            Sdf_fn_printTimeStamp
                ("sipAlgTranslateviaHeader:Translation not required()");
        }
#endif
#ifdef SIPALG_UT_FLAG
        PRINTF ("****trans is not reqd....\n");
#endif
        sip_memfree (0, (SIP_Pvoid) & pTempArray, SIP_NULL);
        pTemp = pViaInfo;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pViaInfo = SIP_NULL;
        sip_freeSipHeader (&dViaHeader);
        SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgTranslateViaHdr");
        return SipSuccess;
    }

    if (direction == SipPktOut)
    {
        pViaInfo->privateIP = dTempOrigIP;
        pViaInfo->privatePort = (SIP_U16bit) sipAlgatoi (pTempPort);
    }
    else if (direction == SipPktIn)
    {
        pViaInfo->mappedIP = dTempOrigIP;
        pViaInfo->mappedPort = (SIP_U16bit) sipAlgatoi (pTempPort);
    }

    pViaInfo->outsideIP = pNatHeaderInfo->u4OutIpAddr;
    pViaInfo->outsidePort = pNatHeaderInfo->u2OutPort;
    pViaInfo->direction = direction;
    pViaInfo->ifIndex = pNatHeaderInfo->u4IfNum;
    /* NAT API Enhancements - START */
    pViaInfo->parity = en_parityAny;
    /* NAT API Enhancements - END */

    if (dGlbSipStatus == en_sipEnable)
    {
        /* In case there is a B2B server along with CAS, then one binding
           will be created for the IP/Port of B2B for which there will
           be no timer running. This binding will be used for all singaling
           entries. This part of code should get executed only once
           for a given interface */

#ifdef SDF_TIMESTAMP
        if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
        {
            Sdf_fn_printTimeStamp
                ("sipAlgTranslateViaHeader:Pre - GetWanIpAddr()");
        }
#endif
        retVal = sipAlgGetWANIPAddress (pNatHeaderInfo->u4IfNum, &natIP);
#ifdef SDF_TIMESTAMP
        if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
        {
            Sdf_fn_printTimeStamp
                ("sipAlgTranslateViaHeader:Post - GetWanIpAddr()");
        }
#endif
        if (retVal == 0)        /*  NAT_DISABLE (2) and NAT_ENABLE (1) */
        {
            sip_error (SIP_Minor, "Could not get WAN IP.");
            sip_memfree (0, (SIP_Pvoid) & pTempArray, SIP_NULL);
            pTemp = pViaInfo;
            sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
            pViaInfo = SIP_NULL;
            sip_freeSipHeader (&dViaHeader);
#ifdef SDF_TIMESTAMP
            if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
            {
                Sdf_fn_printTimeStamp
                    ("sipAlgTranslateviaHeader:Translation not required()");
            }
#endif
            return SipFail;
        }
        else if (retVal == NAT_DISABLE)
        {
            /*Here NAT is disabled so NAT IP should be same as Src IP */
            isNatEnabled = SipFalse;
        }

        if (dGlbInterfaceInfo[dInterfaceIndex].isSignalingEntryCreated ==
            SipFalse)
        {
            SIPDEBUG ((SIP_S8bit *)
                      "This part should get executed only once for a "
                      "given interface");
            if (sipAlgAddPortMapping
                (dTempOrigIP, pNatHeaderInfo->u4IfNum, natIP,
                 isNatEnabled) == SipFail)
            {
                sip_error (SIP_Minor, "sipAlgAddPortMapping failed");
                /* free pTempArray */
                sip_memfree (0, (SIP_Pvoid) & pTempArray, SIP_NULL);
                pTemp = pViaInfo;
                sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
                pViaInfo = SIP_NULL;
                sip_freeSipHeader (&dViaHeader);
#ifdef SDF_TIMESTAMP
                if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
                {
                    Sdf_fn_printTimeStamp
                        ("sipAlgTranslateviaHeader:AddPortMapping:Translation not required()");
                }
#endif
                return SipFail;
            }
            else
            {
                dGlbInterfaceInfo[dInterfaceIndex].isSignalingEntryCreated =
                    SipTrue;
            }
        }
    }                            /* if sipserver is enabled */
#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {
        Sdf_fn_printTimeStamp
            ("sipAlgTranslateViaHeader:Pre - PerformTrans() 4");
    }
#endif

    /*}  if sipserver is enabled */
    if (sipAlgPerformTranslation (pViaInfo, dProtocol,
                                  SipFalse, SIP_NULL,
                                  pNatHeaderInfo->u4IfNum) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateViaHdr:sipAlgPerformTranslation failed\n");
        pTemp = pViaInfo;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pViaInfo = SIP_NULL;
        sip_freeSipHeader (&dViaHeader);
#ifdef SDF_TIMESTAMP
        if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
        {
            Sdf_fn_printTimeStamp
                ("sipAlgTranslateviaHeader:Translation not required()");
        }

        if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
        {
            Sdf_fn_printTimeStamp
                ("sipAlgTranslateViaHeader:Fail - PerformTrans() 4");
        }
#endif
        /*free pTempArray */
        sip_memfree (0, (SIP_Pvoid) & pTempArray, SIP_NULL);
        return SipFail;
    }
#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {
        Sdf_fn_printTimeStamp
            ("sipAlgTranslateViaHeader:Post - PerformTrans() 4");
    }
#endif

    /* now we need to set the ip and port in sip-msg as per 
       the direction and ip type */
    if (direction == SipPktOut)
    {
        dTransIP = pViaInfo->mappedIP;
        dTransPort = pViaInfo->mappedPort;
    }
    else if (direction == SipPktIn)
    {
        dTransIP = pViaInfo->privateIP;
        dTransPort = pViaInfo->privatePort;
    }

    pTempIP = (SIP_S8bit *) sip_memget (0, SIP_ALG_INET_ADDRSTRLEN, &dError);
    if (pTempIP == SIP_NULL)
    {
        pTemp = pViaInfo;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pViaInfo = SIP_NULL;
        sip_memfree (0, (SIP_Pvoid) & pTempArray, SIP_NULL);
        return SipFail;
    }
    sip_memset ((SIP_Pvoid) pTempIP, 0, SIP_ALG_INET_ADDRSTRLEN);

    /* here we have the translated ip and port; update this 
       in the sip header */
    /* now convert the pMediaInfo->mappedIP to string */
    if (sipAlgInetNetToStr (inetAFINET, (void *) &dTransIP, pTempIP,
                            SIP_ALG_INET_ADDRSTRLEN, &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateViaHdr:sipAlgInetNetToStr failed\n");
        sip_memfree (0, (SIP_Pvoid) & pTempIP, SIP_NULL);
        pTemp = pViaInfo;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pViaInfo = SIP_NULL;
        sip_freeSipHeader (&dViaHeader);
#ifdef SDF_TIMESTAMP
        if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
        {
            Sdf_fn_printTimeStamp
                ("sipAlgTranslateviaHeader:Translation not required()");
        }
#endif
        sip_memfree (0, (SIP_Pvoid) & pTempArray, SIP_NULL);
        return SipFail;
    }

    /* now form the translated ip and port in ip:port form */
    pTempTransSentBy =
        (SIP_S8bit *) sip_memget (0, SIP_ALG_IP_PORT_LEN, &dError);
    if (pTempTransSentBy == SIP_NULL)
    {
        pTemp = pViaInfo;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pViaInfo = SIP_NULL;
        sip_memfree (0, (SIP_Pvoid) & pTempArray, SIP_NULL);
        return SipFail;
    }
    sip_memset (pTempTransSentBy, 0, SIP_ALG_IP_PORT_LEN);
    sip_strcpy (pTempTransSentBy, pTempIP);
    sip_strcat (pTempTransSentBy, (SIP_S8bit *) ":");

    sip_memset (dTempPort, 0, SIP_ALG_PORT_LEN);
    sip_snprintf ((SIP_S8bit *) dTempPort, SIP_ALG_PORT_LEN, "%u", dTransPort);
    dTempPort[SIP_ALG_PORT_LEN - 1] = '\0';
    sip_strcat (pTempTransSentBy, dTempPort);
    /* free pTempIP */
    if (sip_setSentByInViaHdr (&dViaHeader, pTempTransSentBy,
                               &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateViaHdr:sip_setSentByInViaHdr failed\n");
        pTemp = pViaInfo;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
        pViaInfo = SIP_NULL;
        sip_freeSipHeader (&dViaHeader);
        sip_memfree (0, (SIP_Pvoid) & pTempArray, SIP_NULL);
        sip_memfree (0, (SIP_Pvoid) & pTempIP, SIP_NULL);
        return SipFail;
    }

    /*RECEIVE and RPORT related changes STARTS */
    if (sip_getViaParamCountFromViaHdr (&dViaHeader, &dViaParamCount,
                                        &dError) == SipSuccess)
    {
        for (dIndex = 0; dIndex < dViaParamCount; dIndex++)
        {
            SIP_S8bit          *pParamName = SIP_NULL;
            SIP_S8bit          *pParamValue = SIP_NULL;

            if (sip_getViaParamAtIndexFromViaHdr
                (&dViaHeader, &pViaParam, dIndex, &dError) == SipSuccess)
            {
                if (sip_getNameFromSipParam (pViaParam, &pParamName,
                                             &dError) == SipSuccess)
                {
                    if (sip_strcmp (pParamName, "received") == 0)
                    {
                        if (sip_getValueAtIndexFromSipParam (pViaParam,
                                                             &pParamValue, 0,
                                                             &dError) ==
                            SipSuccess)
                        {
                            pReceivedParamHost = pParamValue;
                            /*if received param is same as via header's
                               sent by param then change the received param
                               with translated ip */
                            if (sip_strcmp (pViaSentBy, pReceivedParamHost) ==
                                0)
                            {
                                pTransRecdIp = (SIP_S8bit *)
                                    sip_memget (0, SIP_ALG_INET_ADDRSTRLEN,
                                                &dError);
                                if (pTransRecdIp == SIP_NULL)
                                {
                                    sip_memfree (0, (SIP_Pvoid) & pTempArray,
                                                 SIP_NULL);
                                    pTemp = pViaInfo;
                                    sip_memfree (0, (SIP_Pvoid *) & pTemp,
                                                 SIP_NULL);
                                    pViaInfo = SIP_NULL;
                                    return SipFail;
                                }

                                MEMSET (pTransRecdIp, 0,
                                        SIP_ALG_INET_ADDRSTRLEN);
                                sip_strcpy (pTransRecdIp, pTempIP);
                                (void) sip_setValueAtIndexInSipParam (pViaParam,
                                                                      pTransRecdIp,
                                                                      0,
                                                                      &dError);
                                /* MEMLEAK FIX */
                                /*pViaParam = SIP_NULL; */
                                /* MEMLEAK FIX */
                            }
                        }
                    }
                    else if (sip_strcmp (pParamName, "rport") == 0)
                    {
                        if (sip_getValueAtIndexFromSipParam (pViaParam,
                                                             &pParamValue, 0,
                                                             &dError) ==
                            SipSuccess)
                        {
                            pRport = pParamValue;
                            /*if rport param value is same as via header's
                               sent by port then change the rport param
                               value with translated port value */
                            if (pTempPort == SIP_NULL)
                            {
                                pTemp = pViaInfo;
                                sip_memfree (0, (SIP_Pvoid *) & pTemp,
                                             SIP_NULL);
                                pViaInfo = SIP_NULL;
                                sip_memfree (0, (SIP_Pvoid) & pTempArray,
                                             SIP_NULL);
                                return SipFail;
                            }
                            if (sip_strcmp (pTempPort, pRport) == 0)
                            {
                                pTransRPort = (SIP_S8bit *)
                                    sip_memget (0, SIP_ALG_PORT_LEN, &dError);
                                if (pTransRPort == NULL)
                                {
                                    pTemp = pViaInfo;
                                    sip_memfree (0, (SIP_Pvoid *) & pTemp,
                                                 SIP_NULL);
                                    pViaInfo = SIP_NULL;
                                    sip_memfree (0, (SIP_Pvoid) & pTempArray,
                                                 SIP_NULL);
                                    return SipFail;
                                }
                                sip_snprintf ((SIP_S8bit *) pTransRPort,
                                              SIP_ALG_PORT_LEN, "%u",
                                              dTransPort);
                                pTransRPort[SIP_ALG_PORT_LEN - 1] = '\0';

                                (void) sip_setValueAtIndexInSipParam (pViaParam,
                                                                      pTransRPort,
                                                                      0,
                                                                      &dError);
                                /* MEMLEAK FIX */
                                /*pViaParam = SIP_NULL; */
                                /* MEMLEAK FIX */
                            }
                        }
                    }
                }
                /* MEMLEAK FIX */
                sip_freeSipParam (pViaParam);
                /* MEMLEAK FIX */
            }
        }
    }
/*RECEIVE and RPORT related changes ENDS*/
    sip_freeSipHeader (&dViaHeader);
    pTemp = pViaInfo;
    sip_memfree (0, (SIP_Pvoid *) & pTemp, SIP_NULL);
    pViaInfo = SIP_NULL;
/* free pTempArray */
    sip_memfree (0, (SIP_Pvoid) & pTempArray, SIP_NULL);
    sip_memfree (0, (SIP_Pvoid) & pTempIP, SIP_NULL);

    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgTranslateViaHdr ");
    return SipSuccess;
}

/*****************************************************************************
** FUNCTION: sipAlgTranslateContactHdr
** DESCRIPTION: This function translates the Contact Header 
**
** PARAMETERS:
**      pSipMsg(IN)                - Pointer to SIP Msg 
**        direction(IN)            - specifies whether this is OUT/IN Msg
**        pNatHeaderInfo(IN)       - pointer to tHeaderInfo which has NAT info 
**
** Returns : SUCCESS/FAILURE 
*****************************************************************************/

SipBool
sipAlgTranslateContactHdr (SipMessage * pSipMsg, SipAlgPktInOut direction,
                           tHeaderInfo * pNatHeaderInfo,
                           en_SipBoolean dIsRespToReg, en_SipBoolean dIsReq)
{
    SipError            dError;
    SIP_U32bit          dSize = 0;
    SIP_U32bit          dUserLength = 0;
    SIP_U32bit          dHostLength = 0;
    SipUrl             *pSipUrl1;
    SIP_U16bit          i = 0;
    SipHeader           dExpHeader;
    SipHeader           dToHeader;
    SIP_U32bit          dExpiresHdrValue = 0;
    SIP_S8bit          *pReqMethod = SIP_NULL;
    en_SipBoolean       dIsRegExpired = SipFalse;
    en_SipBoolean       dIsRegExpiresHdrPresent = SipFalse;
    SipAddrSpec        *pAddrSpec1;

    SIP_U32bit          CodeNum = 0;    /*non 2xx DeReg */
    SIP_S8bit          *pUser = SIP_NULL;
    SIP_S8bit          *pHost = SIP_NULL;
    SIP_S8bit          *pAORString = SIP_NULL;
    SIP_S8bit          *pTempString = SIP_NULL;

    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgTranslateContactHdr");

    if (sip_getHeaderCount (pSipMsg, SipHdrTypeContactAny, &dSize,
                            &dError) == SipFail)
    {
        sip_error (SIP_Minor,
                   " In sipAlgTranslateContactHdr:sip_getHeaderCount failed\n");
        return SipFail;
    }

    /* Get the method name from sip request message */
    if (dIsReq == SipTrue)
    {
        if (sipAlgGetMethodFromSipReqest (pSipMsg, &pReqMethod) == SipFail)
        {
            sip_error (SIP_Major,
                       "In sipAlgTranslateContactHdr:sipAlgGetMethodFromSipReqest failed\n");
        }
    }

    /*Non 2XX resp should not delete the expired contacts from DeRegHash */
    if ((dGlbSipStatus == en_sipDisable) && dIsRespToReg == SipTrue)
    {
        SipStatusLine      *pLine = SIP_NULL;

        /* now get the response code from the status line */
        if (sip_getStatusLineFromSipRespMsg (pSipMsg, &pLine,
                                             &dError) == SipFail)
        {
            sip_error (SIP_Major, "In sipAlgTranslateContactHdr:\
                       sip_getStatusLineFromSipRespMsg failed\n");
            return SipFail;
        }
        if (sip_getStatusCodeNumFromStatusLine (pLine, &CodeNum,
                                                &dError) == SipFail)
        {
            sip_freeSipStatusLine (pLine);
            sip_error (SIP_Major, "In sipAlgTranslateContactHdr:\
                       sip_getStatusCodeNumFromStatusLine failed\n");
            return SipFail;
        }
        sip_freeSipStatusLine (pLine);
    }

    /*On Register request, we need to get the AOR from To header. */
    /*For Each contact Hdr, we will invoke function 'UpdateDeRegistrationHash' */
    if (((pReqMethod != SIP_NULL)
         && (sip_strcasecmp (pReqMethod, "REGISTER") == 0))
        || (dIsRespToReg == SipTrue))
    {
        if (sip_getHeader (pSipMsg, SipHdrTypeTo, &dToHeader, &dError) ==
            SipFail)
        {
            sip_error (SIP_Minor,
                       "In sipAlgGetDialogParamFromSipMsg:sip_getHeader failed\n");
            sip_freeSipHeader (&dToHeader);
            return SipFail;

        }
        if (sip_getAddrSpecFromToHdr (&dToHeader,
                                      &pAddrSpec1, &dError) == SipFail)
        {
            sip_error (SIP_Minor, "In sipAlgTranslateContactHdr:\
                       sip_getAddrSpecFromContactHdr failed\n");
            sip_freeSipHeader (&dToHeader);
            return SipFail;
        }
        if (sip_getUrlFromAddrSpec (pAddrSpec1, &pSipUrl1, &dError) == SipFail)
        {
            sip_error (SIP_Minor,
                       " In sipAlgTranslateContactHdr:sip_getUrlFromAddrSpec failed\n");
            sip_freeSipAddrSpec (pAddrSpec1);
            sip_freeSipHeader (&dToHeader);
            return SipFail;
        }
        if (sip_getUserFromUrl (pSipUrl1, &pUser, &dError) == SipFail)
        {
            sip_error (SIP_Minor,
                       " In sipAlgTranslateContactHdr:sip_getHostFromUrl failed\n");
            sip_freeSipUrl (pSipUrl1);
            sip_freeSipAddrSpec (pAddrSpec1);
            sip_freeSipHeader (&dToHeader);
            return SipFail;
        }
        if (sip_getHostFromUrl (pSipUrl1, &pHost, &dError) == SipFail)
        {
            sip_error (SIP_Minor,
                       " In sipAlgTranslateContactHdr:sip_getHostFromUrl failed\n");
            sip_freeSipUrl (pSipUrl1);
            sip_freeSipAddrSpec (pAddrSpec1);
            sip_freeSipHeader (&dToHeader);
            return SipFail;
        }

        dUserLength = sip_strlen (pUser);
        dHostLength = sip_strlen (pHost);

        /*  concatenate user@host */
        pAORString = (SIP_S8bit *) sip_memget
            (0, dUserLength + dHostLength + 2, &dError);
        if (pAORString == SIP_NULL)
        {
            return SipFail;
        }
        MEMSET (pAORString, 0, dUserLength + dHostLength + 2);
        pTempString = sipAlgstrncat (pAORString, pUser, dUserLength);
        if (SIP_NULL != pTempString)
        {
            pAORString = pTempString;
            pTempString = NULL;
        }
        else
        {
            sip_memfree (0, (SIP_Pvoid) & pAORString, SIP_NULL);
            return SipFail;
        }
        pTempString = sipAlgstrncat (pAORString, "@", sip_strlen ("@"));
        if (SIP_NULL != pTempString)
        {
            pAORString = pTempString;
            pTempString = NULL;
        }
        else
        {
            sip_memfree (0, (SIP_Pvoid) & pAORString, SIP_NULL);
            return SipFail;
        }
        pTempString = sipAlgstrncat (pAORString, pHost, dHostLength);
        if (SIP_NULL != pTempString)
        {
            pAORString = pTempString;
            pTempString = NULL;
        }
        else
        {
            sip_memfree (0, (SIP_Pvoid) & pAORString, SIP_NULL);
            return SipFail;
        }

        /* MEMLEAK FIX */
        sip_freeSipUrl (pSipUrl1);
        sip_freeSipAddrSpec (pAddrSpec1);
        sip_freeSipHeader (&dToHeader);
        /* MEMLEAK FIX */
    }
    /*if size is zero and its 200ok to De reg, remove */
    /*the corresponding contacts for that AOR */
    /*See the isExpired flag for all contacts of this AOR and delete the contacts */
    /* from the Reg contact info hash. Update the Reg hash table to handle de-reg */
    if ((dGlbSipStatus == en_sipDisable) && dIsRespToReg == SipTrue
        && (200 == CodeNum))
    {
        if (sipAlgDeleteContactsOnDeRegister (pAORString) == SipFail)
        {

            (void) sip_trace (SIP_Detailed, SIP_Init,
                              (SIP_S8bit *)
                              "SIP ALG TRACE: sipAlgDeleteContactsOnDeRegister failed");
            /*sip_freeSipUrl (pSipUrl); */
            /*sip_freeSipAddrSpec (pAddrSpec); */
            sip_memfree (0, (SIP_Pvoid) & pAORString, SIP_NULL);
            return SipFail;
        }
    }

    /* here we will get the expires header value from sip-msg,
       this needs to done only in the case of 200 OK to register */

    if (((pReqMethod != SIP_NULL)
         && (sip_strcasecmp (pReqMethod, "REGISTER") == 0))
        || (dIsRespToReg == SipTrue))
    {
        if (sip_getHeader (pSipMsg, SipHdrTypeExpires, &dExpHeader,
                           &dError) == SipSuccess)
        {
            if (sip_getSecondsFromExpiresHdr (&dExpHeader, &dExpiresHdrValue,
                                              &dError) == SipFail)
            {
                sip_error (SIP_Minor, "In sipAlgTranslateContactHdr:\
                           sip_getSecondsFromExpiresHdr failed\n");
                sip_freeSipHeader (&dExpHeader);
                sip_memfree (0, (SIP_Pvoid) & pAORString, SIP_NULL);
                return SipFail;
            }
            /* If the expireshdr value is zero then set the flag to true */
            if (dExpiresHdrValue == 0)
                dIsRegExpired = SipTrue;

            dIsRegExpiresHdrPresent = SipTrue;

            sip_freeSipHeader (&dExpHeader);
        }
    }

    if (dSize != 0)
    {
        for (i = 0; i < dSize; i++)
        {
            SIP_S8bit          *pTempOrigHost = SIP_NULL;
            SipHeader           dSipHdr;
            SipAddrSpec        *pAddrSpec = SIP_NULL;
            SipUrl             *pSipUrl = SIP_NULL;
            SipNatInfo         *pContactInfo = SIP_NULL;
            SIP_U16bit          dPort = 0, dTransPort = 0;
            SIP_U32bit          dTempOrigIP = 0, dTransIP = 0;
            SIP_S8bit          *pTempTransIP = SIP_NULL;
            en_SipBoolean       dIsWanIp = SipFalse;
            SIP_U32bit          dContactType = 0;
            en_AddrType         addrType;
            en_IpAddrType       dIPAddrType = en_fail;

            SIP_U32bit          dCount = 0, j = 0, dExpiresValue = 0;
            en_SipBoolean       expiresFound = SipFalse;
            SipContactParam    *pContactParam = SIP_NULL;
            sipAlgError         dAlgError = sipAlgNoError;

            if (sip_getHeaderAtIndex (pSipMsg, SipHdrTypeContactAny,
                                      &dSipHdr, i, &dError) == SipFail)
            {
                sip_error (SIP_Minor,
                           " In sipAlgTranslateContactHdr:sip_getHeaderAtIndex failed\n");
                sip_memfree (0, (SIP_Pvoid) & pAORString, SIP_NULL);
                return SipFail;
            }
            if (sip_getTypeFromContactHdr (&dSipHdr, &dContactType,
                                           &dError) == SipFail)
            {
                sip_error (SIP_Minor, "In sipAlgTranslateContactHdr:\
                           sip_getTypeFromContactHdr failed\n");
                sip_freeSipHeader (&dSipHdr);
                sip_memfree (0, (SIP_Pvoid) & pAORString, SIP_NULL);
                return SipFail;
            }
            if (SipContactWildCard == dContactType)
            {
                /* added to handle de-reg */
                if ((dGlbSipStatus == en_sipDisable)
                    && dIsRegExpired == SipTrue)
                {
                    /*Update the REGHash table by setting isExpired to TRUE */
                    /*corresponding to the all contacts for  this AOR */
                    sipAlgUpdateExpiredContacts (pAORString, SipTrue, 0, 0);
                }
                sip_freeSipHeader (&dSipHdr);
                break;
            }

            if (sip_getAddrSpecFromContactHdr (&dSipHdr,
                                               &pAddrSpec, &dError) == SipFail)
            {
                sip_error (SIP_Minor, "In sipAlgTranslateContactHdr:\
                           sip_getAddrSpecFromContactHdr failed\n");
                sip_freeSipAddrSpec (pAddrSpec);
                sip_freeSipHeader (&dSipHdr);
                sip_memfree (0, (SIP_Pvoid) & pAORString, SIP_NULL);
                return SipFail;
            }
            if (sip_getAddrTypeFromAddrSpec (pAddrSpec, &addrType,
                                             &dError) == SipFail)
            {
                sip_error (SIP_Minor,
                           " In sipAlgTranslateContactHdr:sip_getAddrTypeFromAddrSpec failed\n");
                sip_freeSipAddrSpec (pAddrSpec);
                sip_freeSipHeader (&dSipHdr);
                sip_memfree (0, (SIP_Pvoid) & pAORString, SIP_NULL);
                return SipFail;

            }
            if ((addrType != SipAddrSipUri) && (addrType != SipAddrSipSUri))
            {
                sip_freeSipAddrSpec (pAddrSpec);
                sip_freeSipHeader (&dSipHdr);
                continue;
            }
            if (sip_getUrlFromAddrSpec (pAddrSpec, &pSipUrl,
                                        &dError) == SipFail)
            {
                sip_error (SIP_Minor,
                           " In sipAlgTranslateContactHdr:sip_getUrlFromAddrSpec failed\n");
                sip_freeSipAddrSpec (pAddrSpec);
                sip_freeSipHeader (&dSipHdr);
                sip_memfree (0, (SIP_Pvoid) & pAORString, SIP_NULL);
                return SipFail;
            }
            if (sip_getHostFromUrl (pSipUrl, &pTempOrigHost,
                                    &dError) == SipFail)
            {
                sip_error (SIP_Minor,
                           " In sipAlgTranslateContactHdr:sip_getHostFromUrl failed\n");
                sip_freeSipUrl (pSipUrl);
                sip_freeSipAddrSpec (pAddrSpec);
                sip_freeSipHeader (&dSipHdr);
                sip_memfree (0, (SIP_Pvoid) & pAORString, SIP_NULL);
                return SipFail;
            }
            /* invoke the function to see whether psipUrl->pHost
               a domain or ip */
            if (sipAlgIsNumericAddress ((SIP_U8bit *) pTempOrigHost) == SipFail)
            {
                sip_freeSipUrl (pSipUrl);
                sip_freeSipAddrSpec (pAddrSpec);
                sip_freeSipHeader (&dSipHdr);
                continue;
            }
            if (sipAlgIsValidNumericAddress ((SIP_U8bit *) pTempOrigHost) ==
                SipFail)
            {
                sip_freeSipUrl (pSipUrl);
                sip_freeSipAddrSpec (pAddrSpec);
                sip_freeSipHeader (&dSipHdr);
                continue;
            }

            if (sip_getPortFromUrl (pSipUrl, &dPort, &dError) == SipFail)
            {
                if (dError != E_NO_EXIST)
                {
                    sip_error (SIP_Minor,
                               " In sipAlgTranslateContactHdr:sip_getPortFromUrl failed\n");
                    sip_freeSipUrl (pSipUrl);
                    sip_freeSipAddrSpec (pAddrSpec);
                    sip_freeSipHeader (&dSipHdr);
                    sip_memfree (0, (SIP_Pvoid) & pAORString, SIP_NULL);
                    return SipFail;
                }
                dPort = 5060;
            }
            /* allocate memory to natinfo structure */
            pContactInfo = (SipNatInfo *) sip_memget
                (0, sizeof (SipNatInfo), &dError);

            if (pContactInfo == SIP_NULL)
            {
                sip_error (SIP_Minor,
                           " In sipAlgTranslateContactHdr:Memory allocation failed\n");
                sip_freeSipUrl (pSipUrl);
                sip_freeSipAddrSpec (pAddrSpec);
                sip_freeSipHeader (&dSipHdr);
                sip_memfree (0, (SIP_Pvoid) & pAORString, SIP_NULL);
                return SipFail;
            }
            /* Get the ip address in U32 format from string */
            if (sipAlginetStrToNet
                (inetAFINET, pTempOrigHost, (void *) &dTempOrigIP,
                 &dError) == SipFail)
            {
                sip_error (SIP_Minor,
                           " In sipAlgTranslateContactHdr:sipAlginetStrToNet failed\n");
                sip_memfree (0, (SIP_Pvoid) & pContactInfo, SIP_NULL);
                sip_freeSipUrl (pSipUrl);
                sip_freeSipAddrSpec (pAddrSpec);
                sip_freeSipHeader (&dSipHdr);
                sip_memfree (0, (SIP_Pvoid) & pAORString, SIP_NULL);
                return SipFail;
            }
            /* get the user type ie the ip-address type */
            dIPAddrType = sipAlgGetUserType (dTempOrigIP);
            if (dIPAddrType == en_fail)
            {
                sip_error (SIP_Minor,
                           " In sipAlgTranslateContactHdr:sipAlgGetUserType failed\n");
                sip_memfree (0, (SIP_Pvoid) & pContactInfo, SIP_NULL);
                sip_freeSipUrl (pSipUrl);
                sip_freeSipAddrSpec (pAddrSpec);
                sip_freeSipHeader (&dSipHdr);
                sip_memfree (0, (SIP_Pvoid) & pAORString, SIP_NULL);
                return SipFail;
            }
            else if ((dIPAddrType == en_lan) && (SipPktIn == direction))
            {                    /* if the ip in private in the case of incoming message */
                sip_memfree (0, (SIP_Pvoid) & pContactInfo, SIP_NULL);
                sip_freeSipUrl (pSipUrl);
                sip_freeSipAddrSpec (pAddrSpec);
                sip_freeSipHeader (&dSipHdr);
                continue;
            }
            else if (dIPAddrType == en_wan)
            {
                /* check whether this is wan-ip or not */
#ifdef SDF_TIMESTAMP
                if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
                {
                    Sdf_fn_printTimeStamp
                        ("sipAlgTranslateContactHeader:Pre-isWanIpAddress()");
                }
#endif
                dIsWanIp = sipAlgIsWANIPAddress (dTempOrigIP);
#ifdef SDF_TIMESTAMP
                if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
                {
                    Sdf_fn_printTimeStamp
                        ("sipAlgTranslateContactHeader:Post-isWanIpAddress()");
                }
#endif
                if (((dIsWanIp == SipTrue) && (SipPktOut == direction))
                    || ((dIsWanIp == SipFalse) && (SipPktIn == direction)))
                {
                    sip_memfree (0, (SIP_Pvoid) & pContactInfo, SIP_NULL);
                    sip_freeSipUrl (pSipUrl);
                    sip_freeSipAddrSpec (pAddrSpec);
                    sip_freeSipHeader (&dSipHdr);
                    /*when the ua registers with multiple contacts (it can be 
                       combination of both priv and public contacts),the public 
                       contact should not get translated and the loop here should
                       continue instead of returning success */
                    continue;
                }
            }

            if (direction == SipPktOut)
            {
                pContactInfo->privateIP = dTempOrigIP;
                pContactInfo->privatePort = dPort;
            }
            else if (direction == SipPktIn)
            {
                pContactInfo->mappedIP = dTempOrigIP;
                pContactInfo->mappedPort = dPort;
            }

            pContactInfo->outsideIP = pNatHeaderInfo->u4OutIpAddr;
            pContactInfo->outsidePort = pNatHeaderInfo->u2OutPort;
            pContactInfo->direction = direction;
            pContactInfo->parity = en_parityAny;
            pContactInfo->ifIndex = pNatHeaderInfo->u4IfNum;
            /* For 200 OK response of REGISTER message */

            if (sip_getContactParamsCountFromContactHdr (&dSipHdr,
                                                         &dCount,
                                                         &dError) == SipFail)
            {
                sip_error (SIP_Minor, " In sipAlgTranslateContactHdr:\
                           sip_getContactParamsCountFromContactHdr failed\n");
                sip_freeSipUrl (pSipUrl);
                sip_freeSipAddrSpec (pAddrSpec);
                sip_freeSipHeader (&dSipHdr);
                sip_memfree (0, (SIP_Pvoid) & pAORString, SIP_NULL);
                sip_memfree (0, (SIP_Pvoid) & pContactInfo, SIP_NULL);
                return SipFail;
            }
            if (dCount != 0)
            {
                for (j = 0; j < dCount; j++)
                {
                    if (sip_getContactParamAtIndexFromContactHdr (&dSipHdr,
                                                                  &pContactParam,
                                                                  j,
                                                                  &dError)
                        == SipFail)
                    {
                        sip_error (SIP_Minor, " In sipAlgTranslateContactHdr:\
                                   sip_getContactParamAtIndexFromContactHdr failed\n");
                        sip_freeSipUrl (pSipUrl);
                        sip_freeSipAddrSpec (pAddrSpec);
                        sip_freeSipHeader (&dSipHdr);
                        sip_memfree (0, (SIP_Pvoid) & pAORString, SIP_NULL);
                        sip_memfree (0, (SIP_Pvoid) & pContactInfo, SIP_NULL);
                        return SipFail;
                    }
                    if (pContactParam->dType == SipCParamExpires)
                    {
                        expiresFound = SipTrue;
                        break;
                    }
                    sip_freeSipContactParam (pContactParam);
                }
            }
            if (expiresFound == SipTrue)
            {
                dExpiresValue = pContactParam->u.pExpire->dIntVar1;
                sip_freeSipContactParam (pContactParam);
            }
            else
                dExpiresValue = dExpiresHdrValue;

            /* when no expires in REG req - implies its registration not deregistration */
            /*For this contact the expires is present & its 0, so update the reghash that */
            /* it has expired . expires may be = 0 incase neither expires hdr nor expires 
               param is present */
            /*And expired contacts will be removed once we get 200ok for this AOR */
            /*It should be done only for REG req not for the Resp to reg */
            if ((dGlbSipStatus == en_sipDisable)
                && (dIsRespToReg == SipFalse)
                && (pReqMethod != SIP_NULL)
                && (sip_strcasecmp (pReqMethod, "REGISTER") == 0)
                && ((expiresFound == SipTrue)
                    || (dIsRegExpiresHdrPresent == SipTrue))
                && (dExpiresValue == 0) && (SipContactWildCard != dContactType))
            {
                if (pAORString)
                    sipAlgUpdateExpiredContacts (pAORString, SipFalse,
                                                 dTempOrigIP, dPort);
                else
                {
                    SIPDEBUG ((SIP_S8bit *) "In sipAlgTranslateContactHdr"
                              "calling sipAlgUpdateExpiredContacts with no AOR");
                }
            }

            /* if resp to reg call updateregcontactinfo */
            if ((dIsRespToReg == SipTrue) && (200 == CodeNum))
            {
                if (sipAlgUpdateRegContactInfo (pContactInfo, dExpiresValue,
                                                pNatHeaderInfo->u4IfNum,
                                                &dAlgError,
                                                pAORString) == SipFail)
                {
                    sip_error (SIP_Minor, " In sipAlgTranslateContactHdr:\
                               sipAlgUpdateRegContactInfo failed\n");
                    sip_freeSipUrl (pSipUrl);
                    sip_freeSipAddrSpec (pAddrSpec);
                    sip_freeSipHeader (&dSipHdr);
                    sip_memfree (0, (SIP_Pvoid) & pContactInfo, SIP_NULL);
                    sip_memfree (0, (SIP_Pvoid) & pAORString, SIP_NULL);
                    return SipFail;
                }
                if (dAlgError == sipAlgHashEntryNotFound)
                {
                    /* if the entry is not found..continue with other contacts */
                    sip_freeSipUrl (pSipUrl);
                    sip_freeSipAddrSpec (pAddrSpec);
                    sip_memfree (0, (SIP_Pvoid) & pContactInfo, SIP_NULL);
                    sip_freeSipHeader (&dSipHdr);
                    continue;
                }
            }
            else
            {
                /* this block is for contact other than 
                   present in 200 OK to REGISTER */

                /* get transport from contact header */
                en_Protocol         dProtocol = 0;
                SIP_U32bit          dUrlParamCount = 0;
                en_SipBoolean       dTransportParamFound = SipFalse;

                if (sip_getUrlParamCountFromUrl (pSipUrl, &dUrlParamCount,
                                                 &dError) == SipFail)
                {
                    sip_error (SIP_Minor, "In sipAlgTranslateContactHdr:\
                               sip_getUrlParamCountFromUrl failed\n");
                    sip_memfree (0, (SIP_Pvoid) & pContactInfo, SIP_NULL);
                    return SipFail;
                }

                if (dUrlParamCount != 0)
                {
                    SIP_U32bit          dIndex;
                    SipParam           *pUrlParam = SIP_NULL;

                    for (dIndex = 0; dIndex < dUrlParamCount; dIndex++)
                    {
                        SIP_S8bit          *pName = SIP_NULL;

                        (void) sip_getUrlParamAtIndexFromUrl (pSipUrl,
                                                              &pUrlParam,
                                                              dIndex, &dError);
                        (void) sip_getNameFromSipParam (pUrlParam, &pName,
                                                        &dError);

                        if (sip_strcmp (pName, "transport") == 0)
                        {
                            SIP_S8bit          *pTempTranspType = SIP_NULL;

                            (void) sip_getValueAtIndexFromSipParam (pUrlParam,
                                                                    &pTempTranspType,
                                                                    0, &dError);

                            if (sip_strcasecmp (pTempTranspType, "TCP") == 0)
                            {
                                dProtocol = en_tcp;
                            }
                            else if (sip_strcasecmp (pTempTranspType, "UDP") ==
                                     0)
                            {
                                dProtocol = en_udp;
                            }
                            dTransportParamFound = SipTrue;
                            break;
                        }
                        sip_freeSipParam (pUrlParam);
                    }
                    if (dTransportParamFound == SipTrue)
                    {
                        sip_freeSipParam (pUrlParam);
                    }
                }
                else if ((dUrlParamCount == 0)
                         || (dTransportParamFound == SipFalse))
                {
                    /*there is no transport URL param, so take the
                       protocol value from the tHeaderInfo */
                    if (pNatHeaderInfo->u1PktType == NAT_TCP)
                    {
                        dProtocol = en_tcp;
                    }
                    else if (pNatHeaderInfo->u1PktType == NAT_UDP)
                    {
                        dProtocol = en_udp;
                    }
                }
#ifdef SDF_TIMESTAMP
                if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
                {
                    Sdf_fn_printTimeStamp
                        ("sipAlgTranslateContatHeader:Pre - PerformTrans() 5");
                }
#endif

                if (sipAlgPerformTranslation (pContactInfo,
                                              dProtocol, SipFalse, SIP_NULL,
                                              pNatHeaderInfo->u4IfNum) ==
                    SipFail)
                {
                    sip_error (SIP_Minor, "In sipAlgTranslateContactHdr:\
                               sipAlgPerformTranslation failed\n");
                    sip_freeSipUrl (pSipUrl);
                    sip_freeSipAddrSpec (pAddrSpec);
                    sip_freeSipHeader (&dSipHdr);
#ifdef SDF_TIMESTAMP
                    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
                    {
                        Sdf_fn_printTimeStamp
                            ("sipAlgTranslateContatHeader:Fail - PerformTrans() 5");
                    }
#endif
                    sip_memfree (0, (SIP_Pvoid) & pContactInfo, SIP_NULL);
                    sip_memfree (0, (SIP_Pvoid) & pAORString, SIP_NULL);
                    return SipFail;
                }
            }
#ifdef SDF_TIMESTAMP
            if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
            {
                Sdf_fn_printTimeStamp
                    ("sipAlgTranslateContatHeader:Post - PerformTrans() 5");
            }
#endif
            /* now we need to set the ip and port in sip-msg as per 
               the direction and ip type */
            if (direction == SipPktOut)
            {
                dTransIP = pContactInfo->mappedIP;
                dTransPort = pContactInfo->mappedPort;
            }
            else if (direction == SipPktIn)
            {
                if (dIsWanIp == SipTrue)
                {
                    dTransIP = pContactInfo->privateIP;
                    dTransPort = pContactInfo->privatePort;
                }
            }

            /* here we have the translated ip and port */
            if (sip_setPortInUrl (pSipUrl, dTransPort, &dError) == SipFail)
            {
                sip_error (SIP_Minor,
                           " In sipAlgTranslateContactHdr:sip_setPortInUrl failed\n");
                sip_freeSipUrl (pSipUrl);
                sip_freeSipAddrSpec (pAddrSpec);
                sip_freeSipHeader (&dSipHdr);
                sip_memfree (0, (SIP_Pvoid) & pAORString, SIP_NULL);
                sip_memfree (0, (SIP_Pvoid) & pContactInfo, SIP_NULL);
                return SipFail;
            }
            pTempTransIP = (SIP_S8bit *) sip_memget
                (0, SIP_ALG_INET_ADDRSTRLEN, &dError);
            if (pTempTransIP == SIP_NULL)
            {
                sip_memfree (0, (SIP_Pvoid) & pContactInfo, SIP_NULL);
                sip_memfree (0, (SIP_Pvoid) & pAORString, SIP_NULL);
                return SipFail;
            }
            sip_memset ((SIP_Pvoid) pTempTransIP, 0, SIP_ALG_INET_ADDRSTRLEN);

            /* now convert the pMediaInfo->mappedIP to string */
            if (sipAlgInetNetToStr (inetAFINET, (void *) &dTransIP,
                                    pTempTransIP, SIP_ALG_INET_ADDRSTRLEN,
                                    &dError) == SipFail)
            {
                sip_error (SIP_Minor,
                           " In sipAlgTranslateContactHdr:sipAlgInetNetToStr failed\n");
                sip_memfree (0, (SIP_Pvoid) & pTempTransIP, SIP_NULL);
                sip_memfree (0, (SIP_Pvoid) & pContactInfo, SIP_NULL);
                sip_freeSipUrl (pSipUrl);
                sip_freeSipAddrSpec (pAddrSpec);
                sip_freeSipHeader (&dSipHdr);
                sip_memfree (0, (SIP_Pvoid) & pAORString, SIP_NULL);
                return SipFail;
            }

            if (sip_setHostInUrl (pSipUrl, pTempTransIP, &dError) == SipFail)
            {
                sip_error (SIP_Minor,
                           " In sipAlgTranslateContactHdr:sip_setHostInUrl failed\n");
                sip_memfree (0, (SIP_Pvoid) & pContactInfo, SIP_NULL);
                sip_freeSipUrl (pSipUrl);
                sip_freeSipAddrSpec (pAddrSpec);
                sip_freeSipHeader (&dSipHdr);
                sip_memfree (0, (SIP_Pvoid) & pAORString, SIP_NULL);
                return SipFail;
            }
            /*add contact to dereg only in case we get 200resp to reg with the non zero expires */
            /* pvt ip port we get after calling sipalgperformtranslation for 200ok of reg which */
            /*is for a pvt user and public user doesnot register with us */
            if ((dGlbSipStatus == en_sipDisable) && (dIsRespToReg == SipTrue)
                && (dExpiresValue != 0) && (200 == CodeNum))
            {
                if (SipFail == sipAlgAddContactToDeRegHash (pAORString,
                                                            dTransIP,
                                                            dTransPort,
                                                            pNatHeaderInfo->
                                                            u4IfNum))
                {
                    sip_error (SIP_Minor,
                               " In sipAlgTranslateContactHdr:sipAlgAddContactToDeRegHash failed\n");
                    sip_memfree (0, (SIP_Pvoid) & pContactInfo, SIP_NULL);
                    sip_freeSipUrl (pSipUrl);
                    sip_freeSipAddrSpec (pAddrSpec);
                    sip_freeSipHeader (&dSipHdr);
                    sip_memfree (0, (SIP_Pvoid) & pAORString, SIP_NULL);
                    return SipFail;

                }
            }

            sip_memfree (0, (SIP_Pvoid) & pContactInfo, SIP_NULL);
            sip_freeSipUrl (pSipUrl);
            sip_freeSipAddrSpec (pAddrSpec);
            sip_freeSipHeader (&dSipHdr);
        }                        /*for each contact */
    }                            /*if at least on contact */
    sip_memfree (0, (SIP_Pvoid) & pAORString, SIP_NULL);
    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgTranslateContactHdr");
    return SipSuccess;
}

/****************************************************************************
* Function Name : SipAlgModifyXmlMessageBody 
* Description   : This function modifies the xml message body  
*              by replacing priv IP and port with Trans 
*                 Ip and port. 
*                 
* Input/Output (s): 1.pSipMessage - Sip message whose xml body will be modified
*                   2.pErr -Error code
* Input           : pHeaderInfo - 
* 
* Returns        : SUCCESS/FAILURE
*****************************************************************************/

SIP_S32bit
SipAlgModifyXmlMessageBody (SipMessage * pSipMessage,
                            tHeaderInfo * pHeaderInfo, SipError * pErr)
{

    SIP_U8bit           nodeName[SIP_MAX_NODENAME_SIZE];
    SIP_U8bit           endnodeName[SIP_MAX_NODENAME_SIZE];
    SIP_U16bit          xmlMsgLen = 0;
    SIP_S8bit          *ptempXmlMsg = SIP_NULL;
    SIP_S8bit          *pStartPosn = SIP_NULL;
    SIP_S8bit          *pEndPosn = SIP_NULL;
    SIP_S8bit          *pSipNodeBeg = SIP_NULL;
    SIP_U16bit          currLen;
    SIP_U32bit          modifyXmlMsgLen;
    SIP_S8bit          *pModifyXmlMsg = SIP_NULL;
    SIP_S8bit          *pFinalModifyXmlMsg = SIP_NULL;
    SIP_S8bit          *pSipNodeEnd = SIP_NULL;
    SipBool             retVal;
    SipHeader           dContentLengthHdr;
    SipHeader          *pContentLengthHdr = &dContentLengthHdr;
    SipHeader           dContentTypeHdr;
    SipHeader          *pContentTypeHdr = &dContentTypeHdr;
    /* SipError  *pErr; */
/*code rework change start*/
    SIP_U32bit          dContentLength;
    SIP_S8bit          *pMediaType;
    SIP_S8bit          *pXmlMsgBody = SIP_NULL;
    SIP_U32bit          dXmlLength = 0;
    SIP_U32bit         *pXmlLength = &dXmlLength;
/*code rework change end*/
    SIP_U16bit          dStartPresent = 0;

#ifdef SIP_DEBUG
    SIPDEBUG ((SIP_S8bit *) "SIP_DEBUG - Inside "
              "SipAlgModifyXmlMessageBody  function");
#endif

    if ((pSipMessage == NULL) || (pHeaderInfo == NULL) || (pErr == NULL))
    {
#ifdef SIP_DEBUG
        SIPDEBUG ((SIP_S8bit *) "SIP_DEBUG - Invalid Arguments");
#endif
        return SipFail;
    }

    /*Sip Message related parsing start */
    /*  
       SipBool sip_getHeader (SipMessage *pMsg, en_HeaderType
       dType, SipHeader *pHdr, SipError *pErr);
     */

    retVal = sip_getHeader (pSipMessage, SipHdrTypeContentLength,
                            &dContentLengthHdr, pErr);

    if (retVal == SipFail)
    {
#ifdef SIPALG_UT_FLAG
        sip_printf ("Failed with error code %d", *pErr);
#endif
        sip_error (SIP_Minor, "SIP-ERROR:SipAlgModifyXmlMessageBody: "
                   "sip_getHeader failed\n");
/*code rwk start*/
        /*  sip_freeSipHeader(pContentLengthHdr); */
/*code rwk end*/
        return SipFail;
    }

    /*  
       SipBool sip_getLengthFromContentLengthHdr(SipHeader
       *pHeader, SIP_U32bit *pLength, SipError *pErr);
     */

    retVal = sip_getLengthFromContentLengthHdr (pContentLengthHdr,
                                                &dContentLength, pErr);

    if (retVal == SipFail)
    {
#ifdef SIPALG_UT_FLAG
        sip_printf ("Failed with error code %d", *pErr);
#endif
        sip_error (SIP_Minor, "SIP-ERROR:SipAlgModifyXmlMessageBody: "
                   "sip_getLengthFromContentLengthHdr failed\n");
        sip_freeSipHeader (pContentLengthHdr);
        return SipFail;
    }

    pModifyXmlMsg = (SIP_S8bit *) sip_memget (0, (sizeof (SIP_S8bit) *
                                                  (dContentLength + SIP_DELTA)),
                                              pErr);

    if (pModifyXmlMsg == SIP_NULL)
    {
#ifdef SIPALG_UT_FLAG
        sip_printf ("Failed with error code %d", *pErr);
#endif
        sip_error (SIP_Minor, "SIP-ERROR:SipAlgModifyXmlMessageBody: "
                   "sip_memget failed\n");
        sip_freeSipHeader (pContentLengthHdr);
        return SipFail;
    }
    sip_memset ((SIP_Pvoid) pModifyXmlMsg, 0, sizeof (pModifyXmlMsg));

    /*  
       SipBool sip_getMediaTypeFromContentTypeHdr (SipHeader
       *pHeader, SIP_S8bit **ppMediaType, SipError *pErr);
     */

    retVal = sip_getHeader (pSipMessage, SipHdrTypeContentType,
                            pContentTypeHdr, pErr);
    if (retVal == SipFail)
    {
        /* *pErr=E_INV_HEADER;
           sip_printf("Failed with error code %d",*pErr);
           sip_error(SIP_Minor,"SIP-ERROR:SipAlgModifyXmlMessageBody: "\
           "sip_getHeader failed\n"); */
        (void) sip_trace (SIP_Detailed, SIP_Init,
                          (SIP_S8bit *)
                          "SIP ALG TRACE: Content Type header is not present");
        sip_freeSipHeader (pContentLengthHdr);
        /* sip_freeSipHeader(pContentTypeHdr); */
        sip_memfree (0, (SIP_Pvoid) & pModifyXmlMsg, NULL);
        return SipSuccess;
    }

    retVal = sip_getMediaTypeFromContentTypeHdr (pContentTypeHdr,
                                                 &pMediaType, pErr);

    if (retVal == SipFail)
    {
        /* *pErr=E_INV_HEADER; */
#ifdef SIPALG_UT_FLAG
        sip_printf ("Failed with error code %d", *pErr);
#endif
        sip_error (SIP_Minor, "SIP-ERROR:SipAlgModifyXmlMessageBody: "
                   "sip_getMediaTypeFromContentTypeHdr failed\n");
        sip_freeSipHeader (pContentLengthHdr);
        sip_freeSipHeader (pContentTypeHdr);
        sip_memfree (0, (SIP_Pvoid) & pModifyXmlMsg, NULL);
        return SipFail;
    }
    /*Sip Message related parsing end */

    /*"application/pidf+xml" */
    /* identifying the node to be parsed */

    if (sip_strcasecmp (pMediaType, "application/pidf+xml") == 0 ||
        sip_strcasecmp (pMediaType, "application/cpim-pidf+xml") == 0)
    {

        sip_strcpy ((SIP_S8bit *) nodeName, "contact");
        sip_strcpy ((SIP_S8bit *) endnodeName, "contact");

    }
    else if (sip_strcasecmp (pMediaType, "application/xpidf+xml") == 0)
    {
        sip_strcpy ((SIP_S8bit *) nodeName, "address");
        sip_strcpy ((SIP_S8bit *) endnodeName, "address");
    }
    else
    {
#ifdef SIPALG_UT_FLAG
        sip_printf ("unknown Content-Type : Translation not done\n");
#endif
        sip_freeSipHeader (pContentLengthHdr);
        sip_freeSipHeader (pContentTypeHdr);
        sip_memfree (0, (SIP_Pvoid) & pModifyXmlMsg, NULL);
        return SipSuccess;
    }

    /*getting xml msg body start */

    retVal = SipAlgGetMessageBody (pSipMessage, &pXmlMsgBody, pXmlLength, pErr);

    if (retVal != SipSuccess)
    {
#ifdef SIPALG_UT_FLAG
        sip_printf ("Failed with error code %d", *pErr);
#endif
        sip_freeSipHeader (pContentLengthHdr);
        sip_freeSipHeader (pContentTypeHdr);
        sip_memfree (0, (SIP_Pvoid) & pXmlMsgBody, NULL);
        sip_memfree (0, (SIP_Pvoid) & pModifyXmlMsg, NULL);
        return SipFail;
    }
    xmlMsgLen = (SIP_U16bit) * pXmlLength;

    /*getting xml msg body end */
    /*parsing logic start */
    {
        ptempXmlMsg = (SIP_S8bit *) sip_memget (0, (sizeof (SIP_S8bit) *
                                                    (xmlMsgLen + 1)), pErr);
        if (ptempXmlMsg == SIP_NULL)
        {
#ifdef SIPALG_UT_FLAG
            sip_printf ("Failed with error code %d", *pErr);
#endif
            sip_freeSipHeader (pContentLengthHdr);
            sip_freeSipHeader (pContentTypeHdr);
            sip_memfree (0, (SIP_Pvoid) & pXmlMsgBody, NULL);
            sip_memfree (0, (SIP_Pvoid) & pModifyXmlMsg, NULL);
            return SipFail;
        }
        sip_memset ((SIP_Pvoid) ptempXmlMsg, 0, (xmlMsgLen + 1));

        sip_strcpy (ptempXmlMsg, /*pSipMessage */ pXmlMsgBody);
        pStartPosn = ptempXmlMsg;
        pEndPosn = ptempXmlMsg + xmlMsgLen;

        /*finding node (contact/address) start posn */

        while (pStartPosn <= pEndPosn)
        {
            pSipNodeBeg =
                sip_strstr (pStartPosn, (SIP_S8bit *) nodeName /*"contact" */ );
            if (pSipNodeBeg == SIP_NULL)
            {
#ifdef SIPALG_UT_FLAG
                sip_printf ("No more nodes for Translation\n");
#endif
                currLen = (SIP_U16bit) (pEndPosn - pStartPosn);
                sipAlgstrncat (pModifyXmlMsg, pStartPosn, currLen);
                break;
            }
            currLen = (SIP_U16bit) (pSipNodeBeg - pStartPosn - 1);
            sipAlgstrncat (pModifyXmlMsg, pStartPosn, (currLen));

            /*finding (contact/address) end node posn */
            /*changed end tag to contact and then a check is done for 
               to find end tag */

/**********XPIDF logic start*******************************************/

            if (sip_strcasecmp (pMediaType, "application/xpidf+xml") == 0)
            {

                if (*(pSipNodeBeg - 1) == '<')
                {
                    /* dStartPresent=1; */
                    dStartPresent = (SIP_U16bit) (dStartPresent + 1);
                }
                if (dStartPresent == 0 && (*(pSipNodeBeg - 1) == '/'))
                {
#ifdef SIPALG_UT_FLAG
                    sip_printf
                        ("Cannot find start address tag..translation not done\n");
#endif
                    sip_memfree (0, (SIP_Pvoid) & ptempXmlMsg, NULL);
                    sip_freeSipHeader (pContentLengthHdr);
                    sip_freeSipHeader (pContentTypeHdr);
                    sip_memfree (0, (SIP_Pvoid) & pXmlMsgBody, NULL);
                    sip_memfree (0, (SIP_Pvoid) & pModifyXmlMsg, NULL);
                    return SipFail;
                }
                if (((*(pSipNodeBeg - 1)) == '/') && (dStartPresent == 1))
                {
                    dStartPresent = 0;
                    pStartPosn = pSipNodeBeg + 7;
                    sipAlgstrncat (pModifyXmlMsg, (pSipNodeBeg - 1), 7);
                    continue;
                }

                if (dStartPresent == 2)
                {                /* this will be the case when we encounter 2 start address tag */

#ifdef SIPALG_UT_FLAG
                    sip_printf
                        ("Cannot find end address tag..translation not done\n");
#endif
                    sip_memfree (0, (SIP_Pvoid) & ptempXmlMsg, NULL);
                    sip_freeSipHeader (pContentLengthHdr);
                    sip_freeSipHeader (pContentTypeHdr);
                    sip_memfree (0, (SIP_Pvoid) & pXmlMsgBody, NULL);
                    sip_memfree (0, (SIP_Pvoid) & pModifyXmlMsg, NULL);
                    return SipFail;
                }

                pSipNodeEnd = sip_strstr ((pSipNodeBeg + 1), ">");
                if (SipAlgHandleXpidfMsg
                    ((SIP_U8bit *) pModifyXmlMsg, (SIP_U8bit *) pSipNodeBeg,
                     (SIP_U8bit *) pSipNodeEnd, pHeaderInfo) == SipFail)
                {
                    sip_memfree (0, (SIP_Pvoid) & ptempXmlMsg, NULL);
                    sip_freeSipHeader (pContentLengthHdr);
                    sip_freeSipHeader (pContentTypeHdr);
                    sip_memfree (0, (SIP_Pvoid) & pXmlMsgBody, NULL);
                    sip_memfree (0, (SIP_Pvoid) & pModifyXmlMsg, NULL);
                    return SipFail;
                }

            }

            /***XPIDF logic End***/

            else
            {
                /*  Parsing Logic for pidf/cpim-pidf */
                if (*(pSipNodeBeg - 1) != '<')
                {
                    sip_error (SIP_Minor, "SIP_DEBUG - Cannot "
                               "find start node..translation not done");
#ifdef SIPALG_UT_FLAG
                    sip_printf ("Failed with error code %d", *pErr);
#endif
                    sip_memfree (0, (SIP_Pvoid) & ptempXmlMsg, NULL);
                    sip_freeSipHeader (pContentLengthHdr);
                    sip_freeSipHeader (pContentTypeHdr);
                    sip_memfree (0, (SIP_Pvoid) & pXmlMsgBody, NULL);
                    sip_memfree (0, (SIP_Pvoid) & pModifyXmlMsg, NULL);
                    return SipFail;
                }

                pSipNodeEnd =
                    sip_strstr ((pSipNodeBeg + 1),
                                (SIP_S8bit *) endnodeName /*"contact" */ );
                if (pSipNodeEnd == SIP_NULL)
                {
                    sip_error (SIP_Minor, "SIP_DEBUG - Cannot "
                               "find end node..translation not done");
#ifdef SIPALG_UT_FLAG
                    sip_printf ("Failed with error code %d", *pErr);
#endif
                    sip_memfree (0, (SIP_Pvoid) & ptempXmlMsg, NULL);
                    sip_freeSipHeader (pContentLengthHdr);
                    sip_freeSipHeader (pContentTypeHdr);
                    sip_memfree (0, (SIP_Pvoid) & pXmlMsgBody, NULL);
                    sip_memfree (0, (SIP_Pvoid) & pModifyXmlMsg, NULL);
                    return SipFail;
                }

                /*Here we find end tag is present or not */

                if (*(pSipNodeEnd - 1) == '/')
                    pSipNodeEnd = pSipNodeEnd - 1;
                else
                {
                    sip_error (SIP_Minor, "SIP_DEBUG - Cannot find "
                               "end node..translation not done");
                    sip_memfree (0, (SIP_Pvoid) & ptempXmlMsg, NULL);
                    sip_freeSipHeader (pContentLengthHdr);
                    sip_freeSipHeader (pContentTypeHdr);
                    sip_memfree (0, (SIP_Pvoid) & pXmlMsgBody, NULL);
                    sip_memfree (0, (SIP_Pvoid) & pModifyXmlMsg, NULL);
                    return SipFail;
                }
                /*end tag check over */

                /*going to the end (>) of the node ( </contact> or 
                   </address>) */

                pSipNodeEnd = pSipNodeEnd + 8;
                if (sipAlgHandlePidfMsg
                    ((SIP_U8bit *) pModifyXmlMsg, (SIP_U8bit *) pSipNodeBeg,
                     (SIP_U8bit *) pSipNodeEnd, pHeaderInfo) == SipFail)
                {
                    sip_error (SIP_Minor, "SIP_ERROR: - syntax error"
                               "in node ..translation not done");
                    sip_memfree (0, (SIP_Pvoid) & ptempXmlMsg, NULL);
                    sip_freeSipHeader (pContentLengthHdr);
                    sip_freeSipHeader (pContentTypeHdr);
                    sip_memfree (0, (SIP_Pvoid) & pXmlMsgBody, NULL);
                    sip_memfree (0, (SIP_Pvoid) & pModifyXmlMsg, NULL);
                    return SipFail;
                }

            }
            pStartPosn = pSipNodeEnd + 1;
        }
        modifyXmlMsgLen = sip_strlen (pModifyXmlMsg);

        pFinalModifyXmlMsg = (SIP_S8bit *)
            sip_memget (0, (modifyXmlMsgLen + 1), pErr);
        if (pFinalModifyXmlMsg == SIP_NULL)
        {
            sip_memfree (0, (SIP_Pvoid) & pXmlMsgBody, NULL);
            sip_memfree (0, (SIP_Pvoid) & pModifyXmlMsg, NULL);
            return SipFail;
        }
        sip_memset ((SIP_Pvoid) pFinalModifyXmlMsg, 0, (modifyXmlMsgLen + 1));
        sip_strncpy (pFinalModifyXmlMsg, pModifyXmlMsg, modifyXmlMsgLen);
        pFinalModifyXmlMsg[modifyXmlMsgLen] = '\0';

#ifndef SECURITY_KERNEL_WANTED
        puts (pFinalModifyXmlMsg);
#endif
#ifdef SIPALG_UT_FLAG
        PRINTF ("length of pFinalModifyXmlMsg=%d\n",
                sip_strlen (pFinalModifyXmlMsg));
#endif
    }

    /*parsing logic end */

/*code rework start*/

/*
    SipBool sip_setLengthInContentLengthHdr (SipHeader
        *pHeader, SIP_U32bit dLength, SipError *pErr)
*/

    if (sip_setLengthInContentLengthHdr (pContentLengthHdr,
                                         modifyXmlMsgLen, pErr) == SipFail)
    {
#ifdef SIPALG_UT_FLAG
        sip_printf ("sip_setLengthInContentLengthHdr failed"
                    "with error code %d", *pErr);
#endif
        sip_memfree (0, (SIP_Pvoid) & ptempXmlMsg, NULL);
        sip_memfree (0, (SIP_Pvoid) & pModifyXmlMsg, NULL);
        sip_memfree (0, (SIP_Pvoid) & pFinalModifyXmlMsg, NULL);
        sip_freeSipHeader (pContentLengthHdr);
        sip_freeSipHeader (pContentTypeHdr);
        sip_memfree (0, (SIP_Pvoid) & pXmlMsgBody, NULL);
        return SipFail;
    }

/*code rework end*/

    /*put xml msg body back to the sip msg */

    /*
       SipBool SipAlgSetMessageBody(SipMessage *pSipMsg,
       SIP_S8bit *pXmlMsgBody,SIP_U32bit pLength,
       Sdf_st_error   *pError)
     */

    if (SipAlgSetMessageBody (pSipMessage, pFinalModifyXmlMsg,
                              &modifyXmlMsgLen, pErr) == SipFail)
    {
#ifdef SIPALG_UT_FLAG
        sip_printf ("SipAlgSetMessageBody failed with error code %d", *pErr);
#endif
        sip_memfree (0, (SIP_Pvoid) & ptempXmlMsg, NULL);
        sip_memfree (0, (SIP_Pvoid) & pModifyXmlMsg, NULL);
        sip_memfree (0, (SIP_Pvoid) & pFinalModifyXmlMsg, NULL);
        sip_freeSipHeader (pContentLengthHdr);
        sip_freeSipHeader (pContentTypeHdr);
        sip_memfree (0, (SIP_Pvoid) & pXmlMsgBody, NULL);
        return SipFail;
    }

    /*put xml msg body end */
    sip_memfree (0, (SIP_Pvoid) & ptempXmlMsg, NULL);
    sip_memfree (0, (SIP_Pvoid) & pModifyXmlMsg, NULL);
/*     sip_memfree(0,(SIP_Pvoid *)&pFinalModifyXmlMsg, NULL); */
    sip_freeSipHeader (pContentLengthHdr);
    sip_freeSipHeader (pContentTypeHdr);
    sip_memfree (0, (SIP_Pvoid) & pXmlMsgBody, NULL);

#ifdef SIP_DEBUG
    SIPDEBUG ((SIP_S8bit *) "SIP_DEBUG - Exiting"
              "SipAlgModifyXmlMessageBody function");
#endif
    return SipSuccess;

}

/*****************************************************************************
 ** FUNCTION: SipAlgGetMessageBody 
 **
 ** DESCRIPTION:  This function gets the message body and its size.
 **
 *****************************************************************************/

SipBool
SipAlgGetMessageBody (SipMessage * pSipMsg,
                      SIP_S8bit ** pXmlMsgBody,
                      SIP_U32bit * pLength, SipError * pError)
{
    /* This pointer is used to avoid strict-aliasing warning */
    SIP_Pvoid           pTemp = SIP_NULL;
    SIP_U32bit          dCount = 0;
    SIPDEBUG ((SIP_S8bit *) "Inside SipAlgGetMessageBody");
    sip_getMsgBodyCount (pSipMsg, &dCount, pError);
    if (dCount == 1)
    {
        SipMsgBody         *pMsgBody = SIP_NULL;

/*klocwork change start*/
        /* sip_getMsgBodyAtIndex(pSipMsg,&pMsgBody,0,pError); */
        pTemp = pMsgBody;
        if (sip_getMsgBodyAtIndex (pSipMsg, &pTemp, 0, pError) == SipFail)
        {
            sip_error (SIP_Minor,
                       "SipAlgGetMessageBody :sip_getMsgBodyAtIndex Failed");
            return SipFail;
        }
        pMsgBody = pTemp;
/*klocwork change end*/

        if (pMsgBody == NULL)
        {
            sip_error (SIP_Critical, "SipAlgGetMessageBody :pMsgBody is NULL");
            return SipFail;
        }

        if (pMsgBody->dType == SipUnknownBody)
        {
            SipUnknownMessage  *pUnKnown = SIP_NULL;

/*klocwork change start*/
            /* sip_getUnknownFromMsgBody(pMsgBody,&pUnKnown,pError); */
            pTemp = pUnKnown;
            if (sip_getUnknownFromMsgBody (pMsgBody, &pTemp, pError) == SipFail)
            {
                sip_error (SIP_Minor,
                           "SipAlgGetMessageBody :sip_getUnknownFromMsgBody Failed");
                return SipFail;
            }
            pUnKnown = pTemp;
/*klocwork change end*/

            *pXmlMsgBody = (SIP_S8bit *) sip_memget (0,
                                                     (pUnKnown->dLength + 1),
                                                     pError);

            if (*pXmlMsgBody == SIP_NULL)
                return SipFail;

            sip_strncpy (*pXmlMsgBody, pUnKnown->pBuffer, pUnKnown->dLength);

            *pLength = pUnKnown->dLength;

            (*pXmlMsgBody)[*pLength] = '\0';

            sip_freeSipUnknownMessage (pUnKnown);
        }
        sip_freeSipMsgBody (pMsgBody);
    }
    SIPDEBUG ((SIP_S8bit *) "Exiting SipAlgGetMessageBody");
    return SipSuccess;
}

/*****************************************************************************
 ** FUNCTION: SipAlgSetMessageBody 
 **
 ** DESCRIPTION:  This function sets the message body and its size.
 **
 *****************************************************************************/

SipBool
SipAlgSetMessageBody (SipMessage * pSipMsg, SIP_S8bit * pXmlMsgBody,
                      SIP_U32bit * pLength, SipError * pErr)
{
    /* This pointer is used to avoid strict-aliasing warning */
    SIP_Pvoid           pTemp = SIP_NULL;
    SipUnknownMessage  *pUnknownMessage = SIP_NULL;
    SipMsgBody         *pMsgBody = SIP_NULL;

    SIPDEBUG ((SIP_S8bit *) "Inside SipAlgSetMessageBody");

    if (sip_initSipMsgBody (&pMsgBody, SipUnknownBody, pErr) == SipFail)
    {
        sip_error (SIP_Minor, "SipAlgSetMessageBody :Failed to"
                   "initialize a SipMessage Body structure");
        return SipFail;
    }

    if (sip_initSipUnknownMessage (&pUnknownMessage, pErr) == SipFail)
    {
        sip_freeSipMsgBody (pMsgBody);
        sip_error (SIP_Minor, "Failed to initialize a "
                   "UnknownMessagebody structure");
        return SipFail;
    }

    if (sip_setBufferInUnknownMessage (pUnknownMessage,
                                       pXmlMsgBody, *pLength, pErr) == SipFail)
    {
        sip_freeSipMsgBody (pMsgBody);
        sip_freeSipUnknownMessage (pUnknownMessage);
        sip_error (SIP_Minor, "Failed to Set Value in"
                   "UnknownMessagebody Structure");
        return SipFail;
    }

    pTemp = pMsgBody->u.pUnknownMessage;
    if (sip_setParam ((void *) pUnknownMessage,
                      (void **) (&(pTemp)), SipUnknownWhole, pErr) == SipFail)
        if (sip_setUnknownParamInMsgBody (pTemp, pUnknownMessage,
                                          pErr, SipUnknownWhole) == SipFail)
        {
            sip_freeSipMsgBody (pMsgBody);
            sip_freeSipUnknownMessage (pUnknownMessage);
            sip_error (SIP_Minor, "Failed to set Unknown message"
                       "in a Sip Message Body ");
            return SipFail;
        }

    sip_listDeleteAll (&(pSipMsg->slMessageBody), pErr);
    sip_listAppend (&(pSipMsg->slMessageBody), (SIP_Pvoid) pMsgBody, pErr);

    SIPDEBUG ((SIP_S8bit *) "Exit SipAlgSetMessageBody function");

    sip_freeSipUnknownMessage (pUnknownMessage);

    return SipSuccess;
}

/*****************************************************************************
 ** FUNCTION: SipAlgHandleXpidfMsg 
 **
 ** DESCRIPTION:  This function translates the Xpidf message body.
 **
 *****************************************************************************/
SipBool
SipAlgHandleXpidfMsg (SIP_U8bit * pModifyXmlMsg, SIP_U8bit
                      * pSipNodeBeg, SIP_U8bit * pSipNodeEnd,
                      tHeaderInfo * pHeaderInfo)
{
    SIP_U8bit          *pAddrUriPosn = SIP_NULL;
    SIP_U8bit          *pFirstquotePosn = SIP_NULL;
    SIP_U8bit          *pLastquotePosn = SIP_NULL;
    SIP_U8bit          *pSemiClnPosn = SIP_NULL;
    SIP_U8bit          *pEndPosn = SIP_NULL;
    SIP_U8bit          *pSipPosn = SIP_NULL;
    SIP_U8bit          *pAtratePosn = SIP_NULL;
    SIP_U8bit          *pFirstClnPosn = SIP_NULL;
    SIP_U8bit          *pSecondClnPosn = SIP_NULL;
    SIP_U8bit           IpAddr[20] = { 0 };
    SIP_U8bit          *pIpAddr = SIP_NULL;
    SIP_U8bit           TransIpAddr[20] = { 0 };
    SIP_U8bit          *pTransIpAddr = SIP_NULL;
    SIP_U8bit           IpPort[20] = { 0 };
    SIP_U8bit          *pIpPort = SIP_NULL;
    SIP_U8bit           TransIpPort[20] = { 0 };
    SIP_U8bit          *pTransIpPort = SIP_NULL;
    SipBool             retVal;

    SIP_U32bit          dNumIpAddrValue = 0;
    SIP_U32bit         *pNumIpAddrValue = SIP_NULL;
    SIP_U16bit          NumIpPortValue = 0;
    SIP_U16bit         *pNumIpPortValue = SIP_NULL;
    SIP_U8bit           TransIpAddrValue[SIP_MAX_IPADDR_SIZE] = { 0 };
    SIP_U8bit          *pTransIpAddrValue = SIP_NULL;
    SIP_U8bit           TransIpPortValue[SIP_MAX_IPADDR_SIZE] = { 0 };
    SIP_U8bit          *pTransIpPortValue = SIP_NULL;

    pIpAddr = IpAddr;
    pIpPort = IpPort;
    pTransIpAddr = TransIpAddr;
    pTransIpPort = TransIpPort;

    pNumIpAddrValue = &dNumIpAddrValue;
    pNumIpPortValue = &NumIpPortValue;
    pTransIpAddrValue = TransIpAddrValue;
    pTransIpPortValue = TransIpPortValue;

#ifdef SIPALG_UT_FLAG
    sip_strcpy (pTransIpAddr, "40.0.0.10");
    sip_strcpy (pTransIpPort, "4444");
#endif

#ifdef SIP_DEBUG
    SIPDEBUG ((SIP_S8bit *) "SIP_DEBUG - Entering SipAlgHandleXpidfMsg"
              "function");
#endif

    pAddrUriPosn =
        (SIP_U8bit *) sip_strstr ((SIP_S8bit *) (pSipNodeBeg + 1), "uri=");
    if (pAddrUriPosn == SIP_NULL)
    {
        return SipFail;
    }
    pFirstquotePosn =
        (SIP_U8bit *) sip_strstr ((SIP_S8bit *) pAddrUriPosn, "\"");
    pLastquotePosn =
        (SIP_U8bit *) sip_strstr ((SIP_S8bit *) (pFirstquotePosn + 1), "\"");

    if (pFirstquotePosn == SIP_NULL || pFirstquotePosn >= pSipNodeEnd)
    {
        sip_error (SIP_Minor, "Cannot " "find end node..translation not done");
        return SipFail;

    }

    if (pLastquotePosn == SIP_NULL || pLastquotePosn >= pSipNodeEnd)
    {
        sip_error (SIP_Minor, "Cannot " "find end node..translation not done");
        return SipFail;
    }

    /*check for semicolon within two quotes */

    pSemiClnPosn =
        (SIP_U8bit *) sip_strstr ((SIP_S8bit *) pFirstquotePosn, ";");

    if ((pSemiClnPosn != SIP_NULL) && (pSemiClnPosn < pLastquotePosn))
        pEndPosn = pSemiClnPosn;
    else
        pEndPosn = pLastquotePosn;

    pSipPosn = (SIP_U8bit *) sip_strstr ((SIP_S8bit *) pFirstquotePosn, "sip");
    pAtratePosn = (SIP_U8bit *) sip_strstr ((SIP_S8bit *) pFirstquotePosn, "@");
    pFirstClnPosn =
        (SIP_U8bit *) sip_strstr ((SIP_S8bit *) pFirstquotePosn, ":");
    if (pFirstClnPosn != SIP_NULL)
        pSecondClnPosn =
            (SIP_U8bit *) sip_strstr ((SIP_S8bit *) (pFirstClnPosn + 1), ":");

    if (pFirstClnPosn != SIP_NULL && pSecondClnPosn != SIP_NULL)
    {
        if (pSecondClnPosn < pEndPosn)
        {
            if (pAtratePosn != SIP_NULL)
            {
                if ((pAtratePosn > pFirstClnPosn)
                    && (pAtratePosn < pSecondClnPosn))
                {
                    /* sip:alice@10.0.0.1:5060 */
                    if (pSipPosn != SIP_NULL && pFirstClnPosn > pSipPosn)
                    {
                        sip_strncpy ((SIP_S8bit *) pIpAddr,
                                     (SIP_S8bit *) (pAtratePosn + 1),
                                     (pSecondClnPosn - pAtratePosn - 1));
                        sip_strncpy ((SIP_S8bit *) pIpPort,
                                     (SIP_S8bit *) (pSecondClnPosn + 1),
                                     (pEndPosn - pSecondClnPosn - 1));

                        retVal = sipAlgIsNumericAddress (pIpAddr);

                        if (retVal == SipSuccess)
                        {
/*get translated ip start*/
                            if (sipAlgIsValidNumericAddress (pIpAddr) ==
                                SipSuccess)
                            {
                                if (sipAlgGetTranslatedIpPort
                                    (pIpAddr, pIpPort, pHeaderInfo,
                                     pTransIpAddrValue,
                                     pTransIpPortValue) == SipFail)
                                {
                                    sip_error (SIP_Minor,
                                               "In SipAlgHandleXpidfMsg:\
                                      sipAlgGetTranslatedIpPort failed\n");
                                    return SipFail;
                                }

/* get translated ip end*/
                                sip_strcat ((SIP_S8bit *) pModifyXmlMsg, "<");
                                sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                               (const SIP_S8bit *) pSipNodeBeg,
                                               (pAtratePosn - pSipNodeBeg + 1));
                                sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                            (SIP_S8bit *) pTransIpAddrValue);
                                sip_strcat ((SIP_S8bit *) pModifyXmlMsg, ":");
                                sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                            (SIP_S8bit *) pTransIpPortValue);
                                sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                               (const SIP_S8bit *) pEndPosn,
                                               (pSipNodeEnd - pEndPosn + 1));

                                return SipSuccess;
                            }
                        }
                    }
                }
            }
            else
            {
                /* case for sip:10.0.0.1:4040 */
                if (pSipPosn != SIP_NULL && pFirstClnPosn > pSipPosn)
                {
                    sip_strncpy ((SIP_S8bit *) pIpAddr,
                                 (SIP_S8bit *) (pFirstClnPosn + 1),
                                 (pSecondClnPosn - pFirstClnPosn - 1));
                    sip_strncpy ((SIP_S8bit *) pIpPort,
                                 (SIP_S8bit *) (pSecondClnPosn + 1),
                                 (pEndPosn - pSecondClnPosn - 1));

                    retVal = sipAlgIsNumericAddress (pIpAddr);
                    if (retVal == SipSuccess)
                    {
                        if (sipAlgIsValidNumericAddress (pIpAddr) == SipSuccess)
                        {
                            /*todo get translated ip */
                            /*pTransIpAddr and pTransIpPort */
                            if (sipAlgGetTranslatedIpPort
                                (pIpAddr, pIpPort, pHeaderInfo,
                                 pTransIpAddrValue,
                                 pTransIpPortValue) == SipFail)
                            {
                                sip_error (SIP_Minor, "In SipAlgHandleXpidfMsg:\
                                sipAlgGetTranslatedIpPort failed\n");
                                return SipFail;
                            }
                            /*todo get translated ip */

                            sip_strcat ((SIP_S8bit *) pModifyXmlMsg, "<");
                            sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                           (SIP_S8bit *) pSipNodeBeg,
                                           (pFirstClnPosn - pSipNodeBeg + 1));
                            sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                        (SIP_S8bit *) pTransIpAddrValue);
                            sip_strcat ((SIP_S8bit *) pModifyXmlMsg, ":");
                            sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                        (SIP_S8bit *) pTransIpPortValue);
                            sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                           (SIP_S8bit *) pEndPosn,
                                           (pSipNodeEnd - pEndPosn + 1));
                            return SipSuccess;
                        }
                    }
                }
            }
        }

    }

    if (pFirstClnPosn != SIP_NULL && pSecondClnPosn == SIP_NULL)
    {
        if (pFirstClnPosn < pEndPosn)
        {
            if (pAtratePosn != SIP_NULL)
            {
                if (pFirstClnPosn < pAtratePosn)
                {
                    /* for case sip:alice@10.0.0.1 */
                    /* sip:alice@wonderland.com */
                    if (pSipPosn != SIP_NULL && pSipPosn < pFirstClnPosn)
                    {
                        sip_strncpy ((SIP_S8bit *) pIpAddr,
                                     (SIP_S8bit *) (pAtratePosn + 1),
                                     (pEndPosn - pAtratePosn - 1));

                        /* To check numeric ip or not */
                        retVal = sipAlgIsNumericAddress (pIpAddr);

                        /* here port will not be present so consider default port 5060 */
                        sip_strcpy ((SIP_S8bit *) pIpPort, "5060");

                        if (retVal == SipSuccess)
                        {
                            if (sipAlgIsValidNumericAddress (pIpAddr) ==
                                SipSuccess)
                            {
                                /*todo to get transIP and Port */
                                if (sipAlgGetTranslatedIpPort
                                    (pIpAddr, pIpPort, pHeaderInfo,
                                     pTransIpAddrValue,
                                     pTransIpPortValue) == SipFail)
                                {
                                    sip_error (SIP_Minor,
                                               "In SipAlgHandleXpidfMsg:\
                                    sipAlgGetTranslatedIpPort failed\n");
                                    return SipFail;
                                }
                                /*pTransIpAddr and pTransIpPort */
                                sip_strcat ((SIP_S8bit *) pModifyXmlMsg, "<");
                                sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                               (const SIP_S8bit *) pSipNodeBeg,
                                               (pAtratePosn - pSipNodeBeg + 1));
                                sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                            (SIP_S8bit *) pTransIpAddrValue);
                                sip_strcat ((SIP_S8bit *) pModifyXmlMsg, ":");
                                sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                            (SIP_S8bit *) pTransIpPortValue);
                                sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                               (SIP_S8bit *) pEndPosn,
                                               (pSipNodeEnd - pEndPosn + 1));
                                return SipSuccess;
                            }
                        }
                    }

                }

                if (pFirstClnPosn > pAtratePosn)
                {
                    /* for the case alice@10.0.0.1:4040 */
                    if (pSipPosn == SIP_NULL)
                    {
                        sip_strncpy ((SIP_S8bit *) pIpAddr,
                                     (SIP_S8bit *) (pAtratePosn + 1),
                                     (pFirstClnPosn - pAtratePosn - 1));
                        retVal = sipAlgIsNumericAddress (pIpAddr);
                        sip_strncpy ((SIP_S8bit *) pIpPort,
                                     (SIP_S8bit *) (pFirstClnPosn + 1),
                                     (pEndPosn - pFirstClnPosn - 1));

                        /*todo to get pTransIpAddr and pTransIpPort */
                        if (retVal == SipSuccess)
                        {
                            if (sipAlgIsValidNumericAddress (pIpAddr) ==
                                SipSuccess)
                            {
                                if (sipAlgGetTranslatedIpPort (pIpAddr, pIpPort,
                                                               pHeaderInfo,
                                                               pTransIpAddrValue,
                                                               pTransIpPortValue)
                                    == SipFail)
                                {
                                    sip_error (SIP_Minor,
                                               "In SipAlgHandleXpidfMsg:\
                                    sipAlgGetTranslatedIpPort failed\n");
                                    return SipFail;
                                }
                                sip_strcat ((SIP_S8bit *) pModifyXmlMsg, "<");
                                sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                               (const SIP_S8bit *) pSipNodeBeg,
                                               (pAtratePosn - pSipNodeBeg + 1));
                                sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                            (SIP_S8bit *) pTransIpAddrValue);
                                sip_strcat ((SIP_S8bit *) pModifyXmlMsg, ":");
                                sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                            (SIP_S8bit *) pTransIpPortValue);
                                sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                               (const SIP_S8bit *) pEndPosn,
                                               (pSipNodeEnd - pEndPosn + 1));
                                return SipSuccess;
                            }
                        }
                    }
                }
            }
            else
            {
                /* for cases  */
                /* sip:10.0.0.1  */
                /* 10.0.0.1:4040 */
                /* tel:10.0.0.1 */

                /* 1. sip:10.0.0.1 */
                if (pSipPosn != SIP_NULL && pSipPosn < pFirstClnPosn)
                {
                    sip_strncpy ((SIP_S8bit *) pIpAddr,
                                 (SIP_S8bit *) (pFirstClnPosn + 1),
                                 (pEndPosn - pFirstClnPosn - 1));
                    retVal = sipAlgIsNumericAddress (pIpAddr);

                    /* here port will not be present so consider default port 5060 */
                    sip_strcpy ((SIP_S8bit *) pIpPort, "5060");

                    if (retVal == SipSuccess)
                    {
                        if (sipAlgIsValidNumericAddress (pIpAddr) == SipSuccess)
                        {

                            /* To get pTransIpAddr and pTransIpPort */

                            if (sipAlgGetTranslatedIpPort (pIpAddr, pIpPort,
                                                           pHeaderInfo,
                                                           pTransIpAddrValue,
                                                           pTransIpPortValue) ==
                                SipFail)
                            {
                                sip_error (SIP_Minor, "In SipAlgHandleXpidfMsg:\
                                    sipAlgGetTranslatedIpPort failed\n");
                                return SipFail;
                            }

                            sip_strcat ((SIP_S8bit *) pModifyXmlMsg, "<");
                            sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                           (SIP_S8bit *) pSipNodeBeg,
                                           (pFirstClnPosn - pSipNodeBeg + 1));
                            sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                        (SIP_S8bit *) pTransIpAddrValue);
                            sip_strcat ((SIP_S8bit *) pModifyXmlMsg, ":");
                            sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                        (SIP_S8bit *) pTransIpPortValue);
                            sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                           (const SIP_S8bit *) pEndPosn,
                                           (pSipNodeEnd - pEndPosn + 1));
                            return SipSuccess;
                        }
                    }
                }

                if (pSipPosn == SIP_NULL
                    && (sip_isdigit (*(pFirstClnPosn - 1)) != 0))
                {
                    /* This is the case 10.0.0.1:4040 */
                    sip_strncpy ((SIP_S8bit *) pIpAddr,
                                 (SIP_S8bit *) (pFirstquotePosn + 1),
                                 (pFirstClnPosn - pFirstquotePosn - 1));
                    retVal = sipAlgIsNumericAddress (pIpAddr);
                    sip_strncpy ((SIP_S8bit *) pIpPort,
                                 (SIP_S8bit *) (pFirstClnPosn + 1),
                                 (pEndPosn - pFirstClnPosn - 1));
                    if (retVal == SipSuccess)
                    {
                        if (sipAlgIsValidNumericAddress (pIpAddr) == SipSuccess)
                        {
                            /*To get pTransIpAddr and pTransIpPort */
                            if (sipAlgGetTranslatedIpPort (pIpAddr, pIpPort,
                                                           pHeaderInfo,
                                                           pTransIpAddrValue,
                                                           pTransIpPortValue) ==
                                SipFail)
                            {
                                sip_error (SIP_Minor, "In SipAlgHandleXpidfMsg:\
                                    sipAlgGetTranslatedIpPort failed\n");
                                return SipFail;
                            }

                            sip_strcat ((SIP_S8bit *) pModifyXmlMsg, "<");
                            sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                           (const SIP_S8bit *) pSipNodeBeg,
                                           (pFirstquotePosn - pSipNodeBeg + 1));
                            sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                        (SIP_S8bit *) pTransIpAddrValue);
                            sip_strcat ((SIP_S8bit *) pModifyXmlMsg, ":");
                            sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                        (SIP_S8bit *) pTransIpPortValue);
                            sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                           (const SIP_S8bit *) pEndPosn,
                                           (pSipNodeEnd - pEndPosn + 1));
                            return SipSuccess;
                        }
                    }
                }

            }

        }

    }

    if (pFirstClnPosn == SIP_NULL)
    {
        if (pAtratePosn != SIP_NULL && (pAtratePosn < pEndPosn))
        {
            /* for the cases alice@wonderland.com and */
            /*  alice@10.0.0.1 */
            sip_strncpy ((SIP_S8bit *) pIpAddr,
                         (SIP_S8bit *) (pAtratePosn + 1),
                         (pEndPosn - pAtratePosn - 1));
            /* todo check ip is numeric or not */

            retVal = sipAlgIsNumericAddress (pIpAddr);

            /* port will not be present so default port 5060 is used */
            sip_strcpy ((SIP_S8bit *) pIpPort, "5060");

            if (retVal == SipSuccess)
            {
                if (sipAlgIsValidNumericAddress (pIpAddr) == SipSuccess)
                {
                    /*To get pTransIpAddr and pTransIpPort */
                    if (sipAlgGetTranslatedIpPort
                        (pIpAddr, pIpPort, pHeaderInfo, pTransIpAddrValue,
                         pTransIpPortValue) == SipFail)
                    {
                        sip_error (SIP_Minor, "In SipAlgHandleXpidfMsg:\
                               sipAlgGetTranslatedIpPort failed\n");
                        return SipFail;
                    }

                    sip_strcat ((SIP_S8bit *) pModifyXmlMsg, "<");
                    sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                   (const SIP_S8bit *) pSipNodeBeg,
                                   (pAtratePosn - pSipNodeBeg + 1));
                    sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                (SIP_S8bit *) pTransIpAddrValue);
                    sip_strcat ((SIP_S8bit *) pModifyXmlMsg, ":");
                    sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                (SIP_S8bit *) pTransIpPortValue);
                    sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                   (const SIP_S8bit *) pEndPosn,
                                   (pSipNodeEnd - pEndPosn + 1));
                    return SipSuccess;
                }
            }

        }
        if (pAtratePosn == SIP_NULL)
        {
            /* this is the case 10.0.0.1 */
            sip_strncpy ((SIP_S8bit *) pIpAddr,
                         (SIP_S8bit *) (pFirstquotePosn + 1),
                         (pEndPosn - pFirstquotePosn - 1));
            retVal = sipAlgIsNumericAddress (pIpAddr);
            /* port is not present so consider default port */
            sip_strcpy ((SIP_S8bit *) pIpPort, "5060");

            if (retVal == SipSuccess)
            {
                if (sipAlgIsValidNumericAddress (pIpAddr) == SipSuccess)
                {
                    /*To get pTransIpAddr and pTransIpPort */
                    if (sipAlgGetTranslatedIpPort
                        (pIpAddr, pIpPort, pHeaderInfo, pTransIpAddrValue,
                         pTransIpPortValue) == SipFail)
                    {
                        sip_error (SIP_Minor, "In SipAlgHandleXpidfMsg:\
                               sipAlgGetTranslatedIpPort failed\n");
                        return SipFail;
                    }

                    sip_strcat ((SIP_S8bit *) pModifyXmlMsg, "<");
                    sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                   (const SIP_S8bit *) pSipNodeBeg,
                                   (pFirstquotePosn - pSipNodeBeg + 1));
                    sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                (SIP_S8bit *) pTransIpAddrValue);
                    sip_strcat ((SIP_S8bit *) pModifyXmlMsg, ":");
                    sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                (SIP_S8bit *) pTransIpPortValue);
                    sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                   (const SIP_S8bit *) pEndPosn,
                                   (pSipNodeEnd - pEndPosn + 1));
                    return SipSuccess;
                }
            }
        }
    }

    /*If it comes here means that translation wont be done */
    sip_strcat ((SIP_S8bit *) pModifyXmlMsg, "<");
    sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                   (const SIP_S8bit *) pSipNodeBeg,
                   (pSipNodeEnd - pSipNodeBeg + 1));

#ifdef SIP_DEBUG
    SIPDEBUG ((SIP_S8bit *) "SIP_DEBUG - Exit SipAlgHandleXpidfMsg" "function");
#endif
    return SipSuccess;

}

/*****************************************************************************
 ** FUNCTION: SipAlgHandlePidfMsg 
 **
 ** DESCRIPTION:  This function translates the pidf/cpim-pidf message body.
 **
 *****************************************************************************/
SipBool
sipAlgHandlePidfMsg (SIP_U8bit * pModifyXmlMsg, SIP_U8bit * pSipNodeBeg,
                     SIP_U8bit * pSipNodeEnd, tHeaderInfo * pHeaderInfo)
{
    /* SIP_U8bit *pAddrUriPosn=SIP_NULL; */
    SIP_U8bit          *pStartPosn = SIP_NULL;
    /* SIP_U8bit *pLastquotePosn=SIP_NULL; */
    /* SIP_U8bit *pSemiClnPosn=SIP_NULL; */
    SIP_U8bit          *pEndPosn = SIP_NULL;
    SIP_U8bit          *pSipPosn = SIP_NULL;
    SIP_U8bit          *pAtratePosn = SIP_NULL;
    SIP_U8bit          *pFirstClnPosn = SIP_NULL;
    SIP_U8bit          *pSecondClnPosn = SIP_NULL;
    SIP_U8bit           IpAddr[20] = { 0 };
    SIP_U8bit          *pIpAddr = SIP_NULL;
    SIP_U8bit           TransIpAddr[20] = { 0 };
    SIP_U8bit          *pTransIpAddr = SIP_NULL;
    SIP_U8bit           IpPort[20] = { 0 };
    SIP_U8bit          *pIpPort = SIP_NULL;
    SIP_U8bit           TransIpPort[20] = { 0 };
    SIP_U8bit          *pTransIpPort = SIP_NULL;
    SipBool             retVal;

    SIP_U32bit          dNumIpAddrValue = 0;
    SIP_U32bit         *pNumIpAddrValue = SIP_NULL;
    SIP_U16bit          NumIpPortValue = 0;
    SIP_U16bit         *pNumIpPortValue = SIP_NULL;
    SIP_U8bit           TransIpAddrValue[SIP_MAX_IPADDR_SIZE] = { 0 };
    SIP_U8bit          *pTransIpAddrValue = SIP_NULL;
    SIP_U8bit           TransIpPortValue[SIP_MAX_IPADDR_SIZE] = { 0 };
    SIP_U8bit          *pTransIpPortValue = SIP_NULL;

    pIpAddr = IpAddr;
    pIpPort = IpPort;
    pTransIpAddr = TransIpAddr;
    pTransIpPort = TransIpPort;

    pNumIpAddrValue = &dNumIpAddrValue;
    pNumIpPortValue = &NumIpPortValue;
    pTransIpAddrValue = TransIpAddrValue;
    pTransIpPortValue = TransIpPortValue;

#ifdef SIPALG_UT_FLAG
    sip_strcpy (pTransIpAddr, "40.0.0.10");
    sip_strcpy (pTransIpPort, "4444");
#endif

    pStartPosn = (SIP_U8bit *) sip_strstr ((SIP_S8bit *) pSipNodeBeg, ">");
    pEndPosn = (SIP_U8bit *) sip_strstr (((SIP_S8bit *) pStartPosn + 1), "<");

    if (pStartPosn == SIP_NULL || pStartPosn >= pSipNodeEnd)
    {
        sip_error (SIP_Minor, "SIP_DEBUG - Cannot "
                   "find end node..translation not done");
        return SipFail;

    }

    if (pEndPosn == SIP_NULL || pEndPosn >= pSipNodeEnd)
    {
        sip_error (SIP_Minor, "SIP_DEBUG - Cannot "
                   "find end node..translation not done");
        return SipFail;
    }

    pSipPosn = (SIP_U8bit *) sip_strstr ((SIP_S8bit *) pStartPosn, "sip");
    pAtratePosn = (SIP_U8bit *) sip_strstr ((SIP_S8bit *) pStartPosn, "@");
    pFirstClnPosn = (SIP_U8bit *) sip_strstr ((SIP_S8bit *) pStartPosn, ":");
    if (pFirstClnPosn != SIP_NULL)
        pSecondClnPosn =
            (SIP_U8bit *) sip_strstr ((SIP_S8bit *) (pFirstClnPosn + 1), ":");

    if (pFirstClnPosn != SIP_NULL && pSecondClnPosn != SIP_NULL)
    {
        if (pSecondClnPosn < pEndPosn)
        {
            if (pAtratePosn != SIP_NULL)
            {
                if ((pAtratePosn > pFirstClnPosn)
                    && (pAtratePosn < pSecondClnPosn))
                {
                    /* sip:alice@10.0.0.1:5060 */
                    if (pSipPosn != SIP_NULL && pFirstClnPosn > pSipPosn)
                    {
                        sip_strncpy ((SIP_S8bit *) pIpAddr,
                                     (SIP_S8bit *) (pAtratePosn + 1),
                                     (pSecondClnPosn - pAtratePosn - 1));
                        sip_strncpy ((SIP_S8bit *) pIpPort,
                                     (SIP_S8bit *) (pSecondClnPosn + 1),
                                     (pEndPosn - pSecondClnPosn - 1));

                        retVal = sipAlgIsNumericAddress (pIpAddr);
                        if (retVal == SipSuccess)
                        {
                            if (sipAlgIsValidNumericAddress (pIpAddr) ==
                                SipSuccess)
                            {
                                /*todo get translated ip */
                                /*pTransIpAddr and pTransIpPort */
                                if (sipAlgGetTranslatedIpPort (pIpAddr, pIpPort,
                                                               pHeaderInfo,
                                                               pTransIpAddrValue,
                                                               pTransIpPortValue)
                                    == SipFail)
                                {
                                    sip_error (SIP_Minor,
                                               "In SipAlgHandlePidfMsg:\
                                      sipAlgGetTranslatedIpPort failed\n");
                                    return SipFail;
                                }
                                /*todo get translated ip */
                                sip_strcat ((SIP_S8bit *) pModifyXmlMsg, "<");
                                sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                               (const SIP_S8bit *) pSipNodeBeg,
                                               (pAtratePosn - pSipNodeBeg + 1));
                                sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                            (SIP_S8bit *) pTransIpAddrValue);
                                sip_strcat ((SIP_S8bit *) pModifyXmlMsg, ":");
                                sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                            (SIP_S8bit *) pTransIpPortValue);
                                sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                               (const SIP_S8bit *) pEndPosn,
                                               (pSipNodeEnd - pEndPosn + 1));
                                return SipSuccess;
                            }
                        }

                    }
                }
            }
            else
            {
                /* case for sip:10.0.0.1:4040 */
                if (pSipPosn != SIP_NULL && pFirstClnPosn > pSipPosn)
                {
                    sip_strncpy ((SIP_S8bit *) pIpAddr,
                                 (SIP_S8bit *) (pFirstClnPosn + 1),
                                 (pSecondClnPosn - pFirstClnPosn - 1));
                    sip_strncpy ((SIP_S8bit *) pIpPort,
                                 (SIP_S8bit *) (pSecondClnPosn + 1),
                                 (pEndPosn - pSecondClnPosn - 1));

                    retVal = sipAlgIsNumericAddress (pIpAddr);
                    if (retVal == SipSuccess)
                    {
                        if (sipAlgIsValidNumericAddress (pIpAddr) == SipSuccess)
                        {
                            /*todo get translated ip */
                            /*pTransIpAddr and pTransIpPort */
                            if (sipAlgGetTranslatedIpPort (pIpAddr, pIpPort,
                                                           pHeaderInfo,
                                                           pTransIpAddrValue,
                                                           pTransIpPortValue) ==
                                SipFail)
                            {
                                sip_error (SIP_Minor, "In SipAlgHandlePidfMsg:\
                                    sipAlgGetTranslatedIpPort failed\n");
                                return SipFail;
                            }
                            /*todo get translated ip */

                            sip_strcat ((SIP_S8bit *) pModifyXmlMsg, "<");
                            sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                           (const SIP_S8bit *) pSipNodeBeg,
                                           (pFirstClnPosn - pSipNodeBeg + 1));
                            sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                        (SIP_S8bit *) pTransIpAddrValue);
                            sip_strcat ((SIP_S8bit *) pModifyXmlMsg, ":");
                            sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                        (SIP_S8bit *) pTransIpPortValue);
                            sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                           (const SIP_S8bit *) pEndPosn,
                                           (pSipNodeEnd - pEndPosn + 1));
                            return SipSuccess;
                        }
                    }

                }
            }
        }

    }

    if (pFirstClnPosn != SIP_NULL && pSecondClnPosn == SIP_NULL)
    {
        if (pFirstClnPosn < pEndPosn)
        {
            if (pAtratePosn != SIP_NULL)
            {
                if (pFirstClnPosn < pAtratePosn)
                {
                    /* for case sip:alice@10.0.0.1 */
                    /* sip:alice@wonderland.com */
                    if (pSipPosn != SIP_NULL && pSipPosn < pFirstClnPosn)
                    {
                        sip_strncpy ((SIP_S8bit *) pIpAddr,
                                     (SIP_S8bit *) (pAtratePosn + 1),
                                     (pEndPosn - pAtratePosn - 1));

                        /* To check numeric ip or not */
                        retVal = sipAlgIsNumericAddress (pIpAddr);

                        /* here port will not be present so consider default port 5060 */
                        sip_strcpy ((SIP_S8bit *) pIpPort, "5060");

                        if (retVal == SipSuccess)
                        {
                            if (sipAlgIsValidNumericAddress (pIpAddr) ==
                                SipSuccess)
                            {

                                /*todo to get transIP and Port */
                                if (sipAlgGetTranslatedIpPort (pIpAddr, pIpPort,
                                                               pHeaderInfo,
                                                               pTransIpAddrValue,
                                                               pTransIpPortValue)
                                    == SipFail)
                                {
                                    sip_error (SIP_Minor,
                                               "In SipAlgHandlePidfMsg:\
                                        sipAlgGetTranslatedIpPort failed\n");
                                    return SipFail;
                                }

                                /*pTransIpAddr and pTransIpPort */
                                sip_strcat ((SIP_S8bit *) pModifyXmlMsg, "<");
                                sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                               (const SIP_S8bit *) pSipNodeBeg,
                                               (pAtratePosn - pSipNodeBeg + 1));
                                sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                            (SIP_S8bit *) pTransIpAddrValue);
                                sip_strcat ((SIP_S8bit *) pModifyXmlMsg, ":");
                                sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                            (SIP_S8bit *) pTransIpPortValue);
                                sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                               (const SIP_S8bit *) pEndPosn,
                                               (pSipNodeEnd - pEndPosn + 1));
                                return SipSuccess;
                            }
                        }
                    }

                }

                if (pFirstClnPosn > pAtratePosn)
                {
                    /* for the case alice@10.0.0.1:4040 */
                    if (pSipPosn == SIP_NULL)
                    {
                        sip_strncpy ((SIP_S8bit *) pIpAddr,
                                     (SIP_S8bit *) (pAtratePosn + 1),
                                     (pFirstClnPosn - pAtratePosn - 1));
                        retVal = sipAlgIsNumericAddress (pIpAddr);
                        sip_strncpy ((SIP_S8bit *) pIpPort,
                                     (SIP_S8bit *) (pFirstClnPosn + 1),
                                     (pEndPosn - pFirstClnPosn - 1));

                        if (retVal == SipSuccess)
                        {
                            if (sipAlgIsValidNumericAddress (pIpAddr) ==
                                SipSuccess)
                            {
                                /*todo to get pTransIpAddr and pTransIpPort */
                                if (sipAlgGetTranslatedIpPort (pIpAddr, pIpPort,
                                                               pHeaderInfo,
                                                               pTransIpAddrValue,
                                                               pTransIpPortValue)
                                    == SipFail)
                                {
                                    sip_error (SIP_Minor,
                                               "In SipAlgHandlePidfMsg:\
                                        sipAlgGetTranslatedIpPort failed\n");
                                    return SipFail;
                                }
                                sip_strcat ((SIP_S8bit *) pModifyXmlMsg, "<");
                                sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                               (const SIP_S8bit *) pSipNodeBeg,
                                               (pAtratePosn - pSipNodeBeg + 1));
                                sip_strcpy ((SIP_S8bit *) pTransIpAddr,
                                            "40.0.0.10");
                                sip_strcpy ((SIP_S8bit *) pTransIpPort, "4444");
                                sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                            (SIP_S8bit *) pTransIpAddrValue);
                                sip_strcat ((SIP_S8bit *) pModifyXmlMsg, ":");
                                sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                            (SIP_S8bit *) pTransIpPortValue);
                                sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                               (const SIP_S8bit *) pEndPosn,
                                               (pSipNodeEnd - pEndPosn + 1));
                                return SipSuccess;
                            }
                        }
                    }
                }
            }
            else
            {
                /* for cases  */
                /* sip:10.0.0.1  */
                /* 10.0.0.1:4040 */
                /* tel:10.0.0.1 */

                /* 1. sip:10.0.0.1 */
                if (pSipPosn != SIP_NULL && pSipPosn < pFirstClnPosn)
                {
                    sip_strncpy ((SIP_S8bit *) pIpAddr,
                                 (SIP_S8bit *) (pFirstClnPosn + 1),
                                 (pEndPosn - pFirstClnPosn - 1));
                    retVal = sipAlgIsNumericAddress (pIpAddr);

                    /* here port will not be present so consider default port 5060 */
                    sip_strcpy ((SIP_S8bit *) pIpPort, "5060");

                    if (retVal == SipSuccess)
                    {
                        if (sipAlgIsValidNumericAddress (pIpAddr) == SipSuccess)
                        {
                            /*todo to get pTransIpAddr and pTransIpPort */
                            if (sipAlgGetTranslatedIpPort (pIpAddr, pIpPort,
                                                           pHeaderInfo,
                                                           pTransIpAddrValue,
                                                           pTransIpPortValue) ==
                                SipFail)
                            {
                                sip_error (SIP_Minor, "In SipAlgHandlePidfMsg:\
                                    sipAlgGetTranslatedIpPort failed\n");
                                return SipFail;
                            }

                            sip_strcat ((SIP_S8bit *) pModifyXmlMsg, "<");
                            sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                           (const SIP_S8bit *) pSipNodeBeg,
                                           (pFirstClnPosn - pSipNodeBeg + 1));
                            sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                        (SIP_S8bit *) pTransIpAddrValue);
                            sip_strcat ((SIP_S8bit *) pModifyXmlMsg, ":");
                            sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                        (SIP_S8bit *) pTransIpPortValue);
                            sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                           (const SIP_S8bit *) pEndPosn,
                                           (pSipNodeEnd - pEndPosn + 1));
                            return SipSuccess;
                        }
                    }
                }

                if (pSipPosn == SIP_NULL
                    && (sip_isdigit (*(pFirstClnPosn - 1)) != 0))
                {
                    /* This is the case 10.0.0.1:4040 */
                    sip_strncpy ((SIP_S8bit *) pIpAddr,
                                 (SIP_S8bit *) (pStartPosn + 1),
                                 (pFirstClnPosn - pStartPosn - 1));
                    retVal = sipAlgIsNumericAddress (pIpAddr);
                    sip_strncpy ((SIP_S8bit *) pIpPort,
                                 (SIP_S8bit *) (pFirstClnPosn + 1),
                                 (pEndPosn - pFirstClnPosn - 1));
                    if (retVal == SipSuccess)
                    {
                        if (sipAlgIsValidNumericAddress (pIpAddr) == SipSuccess)
                        {
                            /*todo to get pTransIpAddr and pTransIpPort */
                            if (sipAlgGetTranslatedIpPort (pIpAddr, pIpPort,
                                                           pHeaderInfo,
                                                           pTransIpAddrValue,
                                                           pTransIpPortValue) ==
                                SipFail)
                            {
                                sip_error (SIP_Minor, "In SipAlgHandlePidfMsg:\
                                    sipAlgGetTranslatedIpPort failed\n");
                                return SipFail;
                            }
                            sip_strcat ((SIP_S8bit *) pModifyXmlMsg, "<");
                            sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                           (const SIP_S8bit *) pSipNodeBeg,
                                           (pStartPosn - pSipNodeBeg + 1));
                            sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                        (SIP_S8bit *) pTransIpAddrValue);
                            sip_strcat ((SIP_S8bit *) pModifyXmlMsg, ":");
                            sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                        (SIP_S8bit *) pTransIpPortValue);
                            sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                           (const SIP_S8bit *) pEndPosn,
                                           (pSipNodeEnd - pEndPosn + 1));
                            return SipSuccess;
                        }
                    }
                }

            }

        }

    }

    if (pFirstClnPosn == SIP_NULL)
    {
        if (pAtratePosn != SIP_NULL && (pAtratePosn < pEndPosn))
        {
            /* for the cases alice@wonderland.com and */
            /*  alice@10.0.0.1 */
            sip_strncpy ((SIP_S8bit *) pIpAddr, (SIP_S8bit *) (pAtratePosn + 1),
                         (pEndPosn - pAtratePosn - 1));
            /* todo check ip is numeric or not */

            retVal = sipAlgIsNumericAddress (pIpAddr);

            /* port will not be present so default port 5060 is used */
            sip_strcpy ((SIP_S8bit *) pIpPort, "5060");

            if (retVal == SipSuccess)
            {
                if (sipAlgIsValidNumericAddress (pIpAddr) == SipSuccess)
                {
                    /*todo to get pTransIpAddr and pTransIpPort */
                    if (sipAlgGetTranslatedIpPort
                        (pIpAddr, pIpPort, pHeaderInfo, pTransIpAddrValue,
                         pTransIpPortValue) == SipFail)
                    {
                        sip_error (SIP_Minor, "In SipAlgHandlePidfMsg:\
                               sipAlgGetTranslatedIpPort failed\n");
                        return SipFail;
                    }
                    sip_strcat ((SIP_S8bit *) pModifyXmlMsg, "<");
                    sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                   (const SIP_S8bit *) pSipNodeBeg,
                                   (pAtratePosn - pSipNodeBeg + 1));
                    sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                (SIP_S8bit *) pTransIpAddrValue);
                    sip_strcat ((SIP_S8bit *) pModifyXmlMsg, ":");
                    sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                (SIP_S8bit *) pTransIpPortValue);
                    sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                   (const SIP_S8bit *) pEndPosn,
                                   (pSipNodeEnd - pEndPosn + 1));
                    return SipSuccess;
                }
            }

        }
        if (pAtratePosn == SIP_NULL)
        {
            /* this is the case 10.0.0.1 */
            sip_strncpy ((SIP_S8bit *) pIpAddr, (SIP_S8bit *) (pStartPosn + 1),
                         (pEndPosn - pStartPosn - 1));
            retVal = sipAlgIsNumericAddress (pIpAddr);
            /* port is not present so consider default port */
            sip_strcpy ((SIP_S8bit *) pIpPort, "5060");

            if (retVal == SipSuccess)
            {
                if (sipAlgIsValidNumericAddress (pIpAddr) == SipSuccess)
                {
                    /*todo to get pTransIpAddr and pTransIpPort */
                    if (sipAlgGetTranslatedIpPort
                        (pIpAddr, pIpPort, pHeaderInfo, pTransIpAddrValue,
                         pTransIpPortValue) == SipFail)
                    {
                        sip_error (SIP_Minor, "In SipAlgHandlePidfMsg:\
                               sipAlgGetTranslatedIpPort failed\n");
                        return SipFail;
                    }
                    sip_strcat ((SIP_S8bit *) pModifyXmlMsg, "<");
                    sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                   (const SIP_S8bit *) pSipNodeBeg,
                                   (pStartPosn - pSipNodeBeg + 1));
                    sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                (SIP_S8bit *) pTransIpAddrValue);
                    sip_strcat ((SIP_S8bit *) pModifyXmlMsg, ":");
                    sip_strcat ((SIP_S8bit *) pModifyXmlMsg,
                                (SIP_S8bit *) pTransIpPortValue);
                    sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                                   (const SIP_S8bit *) pEndPosn,
                                   (pSipNodeEnd - pEndPosn + 1));
                    return SipSuccess;
                }
            }
        }
    }

    /*If it comes here means that translation wont be done */
    sip_strcat ((SIP_S8bit *) pModifyXmlMsg, "<");
    sipAlgstrncat ((SIP_S8bit *) pModifyXmlMsg,
                   (const SIP_S8bit *) pSipNodeBeg,
                   (pSipNodeEnd - pSipNodeBeg + 1));

    return SipSuccess;

}

/*****************************************************************************
 ** FUNCTION: SipAlgHandlePidfMsg 
 **
 ** DESCRIPTION:  This function gives the translated IP and Port.
 **
 *****************************************************************************/

SipBool
sipAlgGetTranslatedIpPort (SIP_U8bit * pPrivIp, SIP_U8bit * pPrivIpPort,
                           tHeaderInfo * pHeaderInfo, SIP_U8bit * pTransIp,
                           SIP_U8bit * pTransIpPort)
{
    SIP_U32bit          dNumIpAddrValue = 0;
    SIP_U32bit         *pNumIpAddrValue = &dNumIpAddrValue;
    SIP_U16bit          NumIpPortValue = 0;
    SIP_U16bit         *pNumIpPortValue = &NumIpPortValue;
    SIP_U8bit           TransIpAddrValue[SIP_MAX_IPADDR_SIZE];
    SIP_U8bit          *pTransIpAddrValue = TransIpAddrValue;
    SIP_U8bit           TransIpPortValue[SIP_MAX_IPADDR_SIZE];
    SIP_U8bit          *pTransIpPortValue = TransIpPortValue;
    SIP_U32bit          numOfBytes = 0;
    SipError            dErr;

    SipNatInfo          NatInfo;
    SipNatInfo         *pNatInfo = SIP_NULL;
    en_Protocol         dProtocol;
    pNatInfo = &NatInfo;
/*get translated ip start*/
    /*pTransIpAddr and pTransIpPort */

    if (sipAlginetStrToNet (AF_INET, (SIP_S8bit *) pPrivIp,
                            (void *) pNumIpAddrValue, &dErr) == SipFail)
    {
        sip_error (SIP_Minor, "In sipAlgGetTranslatedIpPort:\
                            sipAlginetStrToNet\n");
        return SipFail;
    }

    *pNumIpPortValue = (SIP_U16bit) sipAlgatoi ((SIP_S8bit *) pPrivIpPort);
    pNatInfo->privateIP = *((SIP_U32bit *) pNumIpAddrValue);
    pNatInfo->privatePort = *pNumIpPortValue;
    pNatInfo->outsideIP = pHeaderInfo->u4OutIpAddr;
    pNatInfo->outsidePort = pHeaderInfo->u2OutPort;
    pNatInfo->direction = pHeaderInfo->u4Direction;
    dProtocol = en_udp;

#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {
        Sdf_fn_printTimeStamp
            ("sipAlgGetTranslatedIpPort:Pre - PerformTrans() 6");
    }
#endif
    if (sipAlgPerformTranslation
        (pNatInfo, dProtocol, SipFalse, SIP_NULL,
         pHeaderInfo->u4IfNum) == SipFail)
    {
        sip_error (SIP_Minor, "In sipAlgGetTranslatedIpPort:\
                    sipAlgPerformTranslation failed\n");
#ifdef SDF_TIMESTAMP
        if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
        {
            Sdf_fn_printTimeStamp
                ("sipAlgGetTransltedIpPort:Fail - PerformTrans() 6");
        }
#endif
        return SipFail;
    }
#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {
        Sdf_fn_printTimeStamp
            ("sipAlgGetTransltedIpPort:Post - PerformTrans() 6");
    }
#endif

    *pNumIpAddrValue = pNatInfo->mappedIP;
    *pNumIpPortValue = pNatInfo->mappedPort;
    /* pTransIpPortValue=itoa(pNumIpPortValue); */
    /* itoa not supported in ansi c */
    numOfBytes =
        sip_sprintf ((SIP_S8bit *) pTransIpPortValue, "%hd", *pNumIpPortValue);
    pTransIpPortValue[numOfBytes + 1] = '\0';

    if (sipAlgInetNetToStr (AF_INET,
                            (void *) pNumIpAddrValue,
                            (SIP_S8bit *) pTransIpAddrValue,
                            SIP_ALG_INET_ADDRSTRLEN, &dErr) == SipFail)
    {
        sip_error (SIP_Minor, "In sipAlgGetTranslatedIpPort:\
                            sipAlgInetNetToStr failed\n");
        return SipFail;
    }
    sip_strcpy ((SIP_S8bit *) pTransIp, (SIP_S8bit *) pTransIpAddrValue);
    sip_strcpy ((SIP_S8bit *) pTransIpPort, (SIP_S8bit *) pTransIpPortValue);

/* get translated ip end*/

    return SipSuccess;
}
