/********************************************************************
 * Copyright (C) Aricent Communications Sotware,2007
 *
 * $Id: sipalg_portlayer.c,v 1.4 2011/06/24 12:08:25 siva Exp $
 *
 *******************************************************************/

/***********************************************************************
** FUNCTION:
**            SIPALG portlayer functions
************************************************************************
**
** FILENAME:
** sipalg_portlayer.c
**
** DESCRIPTION:
**        This file contains the translation functions for SIP Msg.
**        This includes APIs and functions for.
**
** DATE             NAME             REFERENCE         REASON
** ----             ----             ---------         --------
** 16/07/2007    Rashed Ahammad        -            Initial                    
**
**
**     Copyright (C) 2007 Aricent Inc . All Rights Reserved
***********************************************************************/
#include "lr.h"
#include "sipalg_inc.h"

#ifndef SECURITY_KERNEL_WANTED
#include <arpa/inet.h>
#else
#define INET_NTOP(af, s, d, n)\
        UtlInetNtop ((int)af, (const void *)(s), (char *)d, n)
#define INET_PTON(af, s, d)\
        UtlInetPton (af, (const char *)(s), (void *)(d))
#endif

/******************************************************************************
 ** FUNCTION: sipAlgatoi
 **
 ** DESCRIPTION:
 ** This function internally calls atoi() of Portlayer class.
 *****************************************************************************/

SIP_S32bit
sipAlgatoi (SIP_S8bit * pStr)
{
    if (pStr == SIP_NULL)
        return 0;
    else
        return ATOI (pStr);
}

/******************************************************************************
 ** FUNCTION: sipAlgstrtokr
 **
 ** DESCRIPTION:
 ** This function internally calls strtok_r of Portlayer class.
 *****************************************************************************/

SIP_S8bit          *
sipAlgstrtokr (SIP_S8bit * str, SIP_S8bit * separator, SIP_S8bit ** remaining)
{
#ifndef SECURITY_KERNEL_WANTED
    SIP_S8bit          *pToken =
        (SIP_S8bit *) STRTOK_R (str, separator, remaining);
    if (pToken == NULL)
        return SIP_NULL;
    else
        return pToken;
#else
    UNUSED_PARAM (str);
    UNUSED_PARAM (separator);
    UNUSED_PARAM (remaining);
    return SIP_NULL;
#endif
}

/******************************************************************************
 ** FUNCTION: sipAlgstrtok
 **
 ** DESCRIPTION:
 ** This function internally calls strtok of Portlayer class.
 *****************************************************************************/

SIP_S8bit          *
sipAlgstrtok (SIP_S8bit * str, const SIP_S8bit * delim)
{
#ifndef SECURITY_KERNEL_WANTED
    return STRTOK (str, delim);
#else
    UNUSED_PARAM (str);
    UNUSED_PARAM (delim);
    return SIP_NULL;
#endif
}

/******************************************************************************
 ** FUNCTION: sipAlgstrncat
 **
 ** DESCRIPTION:
 ** This function internally calls strncat of Portlayer class.
 *****************************************************************************/

SIP_S8bit          *
sipAlgstrncat (SIP_S8bit * dest, const SIP_S8bit * src, SIP_size n)
{
    return STRNCAT (dest, src, n);
}

/******************************************************************************
 ** FUNCTION: sipAlgInetNetToStr
 **
 ** DESCRIPTION:
 ** This function internally calls inet_ntop() of Portlayer class.
 *****************************************************************************/

SipBool
sipAlgInetNetToStr (en_inetFamily dFamily,
                    void *pAddress,
                    SIP_S8bit * pStr, SIP_size size, SipError * pEcode)
{
    if (pEcode != SIP_NULL)
        *pEcode = E_NO_ERROR;
#ifndef SECURITY_KERNEL_WANTED
    if (INET_NTOP (dFamily, pAddress, pStr, size) != SIP_NULL)
#else
    if (INET_NTOP (dFamily, pAddress, pStr, size) != OSIX_FAILURE)
#endif
    {
        return SipSuccess;
    }
    else
        return SipFail;
}

/******************************************************************************
 ** FUNCTION: sipAlginetStrToNet
 **
 ** DESCRIPTION:
 ** This function internally calls inet_pton() of Portlayer class.
 *****************************************************************************/

SipBool
sipAlginetStrToNet (en_inetFamily dFamily,
                    SIP_S8bit * pStr, void *pAddress, SipError * pEcode)
{
    SIP_U16bit          tmpRetval = INET_PTON (dFamily, pStr, pAddress);
    if (pEcode != SIP_NULL)
        *pEcode = E_NO_ERROR;
#ifndef SECURITY_KERNEL_WANTED
    if (tmpRetval > 0)
#else
    if (tmpRetval == OSIX_SUCCESS)
#endif
        return SipSuccess;
    else
        return SipFail;
}
