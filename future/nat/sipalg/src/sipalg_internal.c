/********************************************************************
 * Copyright (C) Aricent Communications Sotware,2007
 *
 * $Id: sipalg_internal.c,v 1.7 2012/12/21 12:00:42 siva Exp $
 *
 *******************************************************************/

#include "natinc.h"
#include "sipalg_inc.h"
#include "sipalg_internal.h"

#define TRANSACTION_TIMER   212

/* Error Code of NAT APIs */
SIP_S8bit          *pSipAlgNATAPIErrorCode[15] = {
    "NATIPC_NO_ERROR",
    "NATIPC_NOT_INITIALIZED",
    "NATIPC_COMMUNICATON_ERROR",
    "NATIPC_INVALID_PARAMETERS",
    "NATIPC_NAT_INTERNAL_ERROR",
    "NATIPC_INTERNAL_ERROR",
    "NATIPC_BINDING_ALREADY_EXISTS",
    "NATIPC_BINDING_NOT_EXISTS",
    "NATIPC_PINHOLE_ALREADY_EXISTS",
    "NATIPC_PINHOLE_NOT_EXISTS",
    "NATIPC_REQUEST_TIMEOUT",
    "NATIPC_LINK_DOWN",
    "NATIPC_LINK_NOT_CONFIGURED",
    "NATIPC_EXACT_MATCH",
    "NATIPC_ROUTE_NOT_FOUND"
};

/* Global array having interface specific information */
sipAlgInterfaceInfo dGlbInterfaceInfo[MAX_WAN_LINKS];

SIP_U16bit          dGlbInterfaceIndex = 0;

/* Global structure for the sipalg port information */

sipAlgSipPortInfo   dGlbSipAlgPortInfo;

/*Global structure to store Signalling entry's destination information*/

sipAlgSignalDestInfo dGlbSignallingDestInfo;

/* SIPALG - 2.1 */
/****************************************************************************
* Function Name : sipAlgPerformIPTranslation
* Description     : This function is invoked to modify the IP address 
*                  for all the headers present in SIP message in case there is
*                  no port present. Basically for the handling of session level
*                  c line and translation of o-line in SDP body. In case mapping 
*                  is available in NAT hash table, the entry is returned. 
*                  Otherwise a new entries are created in NAT hash table. 
*                  None of the entries are entered in actual NAT/Firewall. These
*                  values are maintained only in SIP ALG. This function will
*                  be invoked only when NAT is enabled.
* Input/Output (s) :pNatInfo - The SIP header IP and Port (zero) will be passed 
*                        here, which is translated. This varible when passed 
*                        should be filled with both private IP and mapped IP.
*                    pMediaKey - The media key
*                    pVersionId - Version Id present in O-line 
*                    isRequest - Whether the message is a request or response
* Returns    : SUCCESS/FAILURE
****************************************************************************/
SipBool
sipAlgPerformIPTranslation (SipNatInfo * pNatInfo,
                            SipAlgMediaKey * pMediaKey, SIP_S8bit * pVersionId,
                            en_SipBoolean isRequest)
{
    SipError            err;
    SipAlgIPHashKey    *pKey = SIP_NULL;
    SipAlgIPHashElement *pIPHashElement = SIP_NULL;
    SIP_U32bit          dRetVal;

    SIP_Pvoid           pKeyTemp = NULL;

    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgPerformIPTranslation");

    /* Allocate memory for NAT hash key */
    pKey = (SipAlgIPHashKey *)
        sip_memget (DECODE_MEM_ID, sizeof (SipAlgIPHashKey), &err);

    if (pKey == SIP_NULL)
    {
        return SipFail;
    }
    pKey->pCallID = sip_strdup (pMediaKey->pCallID, 0);
    pKey->pVersionId = sip_strdup (pVersionId, 0);
    pKey->isRequest = isRequest;

#ifdef SIPALG_UT_FLAG
    PRINTF ("\n\nCall ID: %s, Version ID: %s, isRequest: %d\n",
            pMediaKey->pCallID, pVersionId, isRequest);
#endif

    /* Fetch the entry from the IP hash table */
    pIPHashElement = (SipAlgIPHashElement *)
        sip_hashFetch (&dGlbIPHash, (SIP_Pvoid) pKey);

    if (pIPHashElement != SIP_NULL)
    {
        if (pNatInfo->direction == SipPktOut)
        {
            pNatInfo->mappedIP = pIPHashElement->mappedIP;
        }
        else
        {
            pNatInfo->privateIP = pIPHashElement->privateIP;
        }
        sip_hashRelease (&dGlbIPHash, (SIP_Pvoid) pKey);
        /*Memory leak fix by lakshmi */
        sip_memfree (DECODE_MEM_ID, (SIP_Pvoid) & (pKey->pCallID), &err);
        sip_memfree (DECODE_MEM_ID, (SIP_Pvoid) & (pKey->pVersionId), &err);
        pKeyTemp = pKey;
        sip_memfree (DECODE_MEM_ID, &pKeyTemp, &err);
    }
    else
    {
        /* Only outgoing IP will be translated here */
        if (pNatInfo->direction == SipPktIn)
        {
            /*Memory leak fix by lakshmi */
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid) & (pKey->pCallID), &err);
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid) & (pKey->pVersionId), &err);
            pKeyTemp = pKey;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pKeyTemp, &err);
            return SipSuccess;
        }

        /* As there is no entry in the IP hash table, create a 
           new entry */
        if (sipAlgInitIPHashElement (&pIPHashElement, pKey) == SipFail)
        {
            sip_error (SIP_Minor, "Could not initialize IP hash element.");
            /*Memory leak fix by lakshmi */
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid) & (pKey->pCallID), &err);
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid) & (pKey->pVersionId), &err);
            pKeyTemp = pKey;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pKeyTemp, &err);
            return SipFail;
        }

        /* Store the private IP address and port in NAT table */
        pIPHashElement->privateIP = pNatInfo->privateIP;
        pIPHashElement->mappedIP = pNatInfo->mappedIP;

        /* Add the pNatHashElement in the NAT hash table. */
        if (sip_hashAdd (&dGlbIPHash,
                         ((SIP_Pvoid) pIPHashElement),
                         ((SIP_Pvoid) pKey)) != SipSuccess)
        {
            sip_error (SIP_Minor, "Could not add hash element in "
                       " NAT hash table");
            /*Memory leak fix by lakshmi */
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid) & (pKey->pCallID), &err);
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid) & (pKey->pVersionId), &err);
            pKeyTemp = pKey;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pKeyTemp, &err);
            sipAlgRemoveEntryFromIPHash ((SIP_Pvoid) pIPHashElement);
            return SipFail;
        }

        /* Both for signaling and Media start the timer. */
        /*dRetVal = NatSipAlgStartTimer (pIPHashElement->pSipAlgTimerInfo,
           TRANSACTION_TIMER); */
        dRetVal = NatSipAlgStartTimer (pIPHashElement->pSipAlgTimerInfo,
                                       gi4NatSipAlgPartialEntryTimer);
        if (dRetVal != NAT_SUCCESS)
        {
            sip_error (SIP_Minor, "Could not start SIP ALG timer "
                       " for contact.");
            sip_hashRemove (&dGlbIPHash, pKey);
            return SipFail;
        }

        (void) sip_trace (SIP_Detailed, SIP_Init,
                          (SIP_S8bit *)
                          "SIP ALG TRACE: Added entry in NAT hash");
    }
    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgPerformIPTranslation");
    return SipSuccess;
}

/****************************************************************************
* Function Name : sipAlgPerformTranslation
* Description     : This function is invoked to modify the IP address and Port
*                  for all the headers present in SIP message. In case mapping 
*                  is available in NAT hash table, the entry is returned. 
*                  Otherwise a new binding is created in NAT. The same function
*                  is used to map the media element as well.
* Input/Output (s) :pNatInfo - The SIP header IP and Port will be passed 
*                        here, which is translated
*                    protocol - UDP or TCP
*                    isMediaElement - Indicates if the SIP request is for media
*                        or for signaling.
*                    pMediaKey - The media key
*                    ifIndex - WAN interface index
* Returns    : SUCCESS/FAILURE
****************************************************************************/
SipBool
sipAlgPerformTranslation (SipNatInfo * pNatInfo, en_Protocol protocol,
                          en_SipBoolean isMediaElement,
                          SipAlgMediaKey * pMediaKey, SIP_U32bit ifIndex)
{
#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {
        Sdf_fn_printTimeStamp ("SipAlgPerformTranslation:Function Starts");
    }
#endif
    tNatAsyncResponse  *pNatResp = SIP_NULL;
    SipAlgNatHashKey   *pKey = SIP_NULL;
    SipAlgNatHashKey   *pTransKey = SIP_NULL;
    SipAlgNatHashElement *pNatHashElement = SIP_NULL;
    SipAlgNatHashElement *pTempNatHashElement = SIP_NULL;
    SipAlgIpPortPair   *pDestIPPort = SIP_NULL;
    SipAlgIpPortPair   *pTempDestIPPort = SIP_NULL;
    SIP_Pvoid           pIPPort = SIP_NULL;
    tNatEntryInfo      *pNatEntryInfo = SIP_NULL;
    SipError            err;
    SIP_U32bit          dRetVal;
    SIP_U32bit          retVal;
    SIP_U32bit          listIter = 0;
    SipAlgMediaKey     *pPartialKey = SIP_NULL;
    SIP_U32bit          listSizeDest = 0;
    SIP_U32bit          natIP = 0;
    en_SipBoolean       dIsWanIp = SipFalse;
    en_SipBoolean       isNatEnabled = SipTrue;
    SipAlgMediaKey     *pPartialKey1 = SIP_NULL;    /*media hash */
    SipAlgMediaKey     *pMediaKeyTemp = SIP_NULL;    /*media hash */
    /* This pointer is used to avoid strict-aliasing warning */
    SIP_Pvoid           pTemp = SIP_NULL;    /*media hash */
    SipAlgMediaKey     *pMediaKeyTemp1 = SIP_NULL;    /*media hash */

    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgPerformTranslation");

    /* Allocate memory for NAT hash key */
    pKey = (SipAlgNatHashKey *)
        sip_memget (DECODE_MEM_ID, sizeof (SipAlgNatHashKey), &err);
    if (pKey == SIP_NULL)
    {
        return SipFail;
    }
    if (pNatInfo->direction == SipPktOut)
    {
        /* Get the private IP and port from pNatInfo and form the Key to 
           identify whether the entries are present in the NAT Hash Table. */
        pKey->ipAddress = pNatInfo->privateIP;
        pKey->port = pNatInfo->privatePort;
        /* Store the direction in hash key. Direction is used while 
           performing a hash comparison */
        pKey->direction = SipPktOut;
        pKey->ifIndex = ifIndex;
    }
    else if (pNatInfo->direction == SipPktIn)
    {
        /* Store the direction in hash key. Direction is used while 
           performing a hash comparison. Interface index is not set
           as it will not be used in hash comparison for incoming pkt */
        pKey->direction = SipPktIn;

        /* Get the translated IP and port from pNatInfo and form the Key to 
           identify whether the entries are present in the NAT Hash Table. */
        if (isMediaElement == SipFalse)
        {
            pKey->ipAddress = pNatInfo->mappedIP;
            pKey->port = pNatInfo->mappedPort;
        }
        else
        {
            /* For intra-site call, key to hash fetch must be mapped port */
            dIsWanIp = sipAlgIsWANIPAddress (pNatInfo->mappedIP);
            if (dIsWanIp == SipTrue)
            {
                pKey->ipAddress = pNatInfo->mappedIP;
                pKey->port = pNatInfo->mappedPort;
            }
            else
            {
                pKey->ipAddress = pNatInfo->outsideIP;
                pKey->port = pNatInfo->outsidePort;
            }
        }
    }

    /* Fetch the entry from the hash table */
    pNatHashElement = (SipAlgNatHashElement *)
        sip_hashFetch (&dGlbNatHash, (SIP_Pvoid) pKey);

    /* There already exists an entry for this in the NAT hash table */

    /* In case of B2B setup, signaling entries will always be found */
    if (pNatHashElement != SIP_NULL)
    {
        if (pNatInfo->direction == SipPktOut)
        {
            (void) sip_listSizeOf (pNatHashElement->pDestInfoList,
                                   &listSizeDest, &err);
            if (listSizeDest != 0)
            {
                (void) sip_listGetAt (pNatHashElement->pDestInfoList,
                                      listIter, (SIP_Pvoid *) & pIPPort, &err);
                pDestIPPort = (SipAlgIpPortPair *) pIPPort;

                pNatInfo->mappedIP = pDestIPPort->mappedIP;
                pNatInfo->mappedPort = pDestIPPort->mappedPort;
            }
        }
        else
        {
            /* Get the private IP address and port for the corresponding 
               translated IP and port and update pNatInfo for incoming 
               message */
            pNatInfo->privateIP = pNatHashElement->originalIP;
            pNatInfo->privatePort = pNatHashElement->originalPort;
        }

        /* Both for signaling and Media restart the timer. As there
           is a SIP signaling going on, restart the timer except for
           contact. */
        if (pNatHashElement->isRegTimerStarted == SipFalse)
        {

            /* Media Hash stale entry removal : 
               We found the NAT enty corresponding to this IP/PORT. 

               Fetch the callInfoList for the IP/PORT from NAT hash. 

               Now  compare the current dialog param with the dialog params in callinfolist.
               If  the match is found in callinfo list, then it is reINVITE with same src IP/PORT.
               So we need to restart the timer in NAT .
               else  its not a reINVITE (and its for meida not for signalling), rather it is a 
               fresh  INVITE with the same src IP/PORT -
               So we need to delete the media elements from the media hash for the dialog
               params present in the callinfolist.
               After succesfull deletion we need to update the callinfolist with this new dialog param.
               Then restart the timer of existing NAT hash element (for which we updated the callinfolist) 
               Media Hash stale entry removal : */

            /*media hash stale entry removal */
            en_SipBoolean       bIsReInvite = SipFalse;
            listIter = 0;
            /*do Not compare dialog params for signalling  */
            if (pNatHashElement->isMediaElement)
            {
                (void) sip_listSizeOf (pNatHashElement->pCallInfoList,
                                       &listSizeDest, &err);
                while (listIter != listSizeDest)
                {
                    pTemp = pMediaKeyTemp;
                    (void) sip_listGetAt (pNatHashElement->pCallInfoList,
                                          listIter,
                                          (SIP_Pvoid *) & pTemp, &err);
                    pMediaKeyTemp = pTemp;
                    /* compare the media keys */
                    if (sipAlgCompareMediaHashKeys (pMediaKey, pMediaKeyTemp) ==
                        0)
                    {
                        bIsReInvite = SipTrue;
                        break;
                        /*This is reInvite so we need to just restart the timer in NAT */
                    }
                    listIter++;
                }
            }

            /*for fresh INVITE and for media */
            if (bIsReInvite == SipFalse && pNatHashElement->isMediaElement)
            {
                /*So we need to delete the all the media elements from the media hash for the
                   dialog params present in the callinfolist. */
                SipAlgIpPort        ipPortKey;
                ipPortKey.ipAddress = pNatHashElement->originalIP;
                ipPortKey.port = pNatHashElement->originalPort;
                listIter = 0;
                while (listIter != listSizeDest)
                {
                    pTemp = pMediaKeyTemp1;
                    (void) sip_listGetAt (pNatHashElement->pCallInfoList,
                                          0, (SIP_Pvoid *) & pTemp, &err);
                    pMediaKeyTemp1 = pTemp;

                    if (sipAlgDeleteMediaElement (pMediaKeyTemp1, &ipPortKey) ==
                        SipFail)
                    {
                        sip_error (SIP_Minor, "Could not delete entry "
                                   "from media hash");
                        sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
                        return SipFail;
                    }
                    /*Delete the node from InfoList */
                    sip_listDeleteAt (pNatHashElement->pCallInfoList, 0, &err);

                    listIter++;
                }

                /*Add the current node to callinfolist in NAT */
                pPartialKey1 = (SipAlgMediaKey *) sip_memget (0,
                                                              sizeof
                                                              (SipAlgMediaKey),
                                                              &err);
                if (pPartialKey1 == SIP_NULL)
                {
                    sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
                    pTemp = pKey;
                    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                    pKey = NULL;
                    return SipFail;
                }
                pPartialKey1->pFromTag = sip_strdup (pMediaKey->pFromTag, 0);
                pPartialKey1->pCallID = sip_strdup (pMediaKey->pCallID, 0);
                if (pMediaKey->pToTag != SIP_NULL)
                    pPartialKey1->pToTag = sip_strdup (pMediaKey->pToTag, 0);
                else
                    pPartialKey1->pToTag = SIP_NULL;

                /* Add the media key along with the element */
                if (sip_listAppend (pNatHashElement->pCallInfoList,
                                    (SIP_Pvoid) pPartialKey1, &err) == SipFail)
                {
                    sip_error (SIP_Minor, "Could not append dialog key in NAT "
                               "hash for media information.");

                    sip_memfree (DECODE_MEM_ID,
                                 (SIP_Pvoid) & (pPartialKey1->pFromTag), &err);
                    sip_memfree (DECODE_MEM_ID,
                                 (SIP_Pvoid) & (pPartialKey1->pCallID), &err);
                    if (pPartialKey1->pToTag != SIP_NULL)
                        sip_memfree (DECODE_MEM_ID,
                                     (SIP_Pvoid) &
                                     (pPartialKey1->pToTag), &err);
                    pTemp = pPartialKey1;
                    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                    pPartialKey1 = NULL;
                    sipAlgRemoveEntryFromNatHash ((SIP_Pvoid) pNatHashElement);
                    pTemp = pKey;
                    /* MEMLEAK FIX */
                    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                    pKey = NULL;
                    /* MEMLEAK FIX */
                    return SipFail;
                }
            }
            /*Now just restart the timers .... as below */
            /*media hash stale entry removal :END */

            if (pNatInfo->direction == SipPktOut)
            {
                SIPDEBUG ((SIP_S8bit *) "Restarting Timer");
                if (pNatHashElement->pSipAlgTimerInfo != SIP_NULL)
                {
                    dRetVal =
                        NatSipAlgReStartTimer (pNatHashElement->
                                               pSipAlgTimerInfo,
                                               gi4NatSipAlgPartialEntryTimer);
                    if (dRetVal != NAT_SUCCESS)
                    {
                        sip_error (SIP_Minor, "Could not start SIP ALG timer "
                                   "for contact.");
                        sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
                        /* sip_memfree(DECODE_MEM_ID,(SIP_Pvoid *)&pDestIPPort,&err); */
                        pTemp = pKey;
                        sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp,
                                     &err);
                        pKey = NULL;
                        /* sipAlgRemoveEntryFromNatHash((SIP_Pvoid *)pNatHashElement); */
#ifdef SDF_TIMESTAMP
                        if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
                        {
                            Sdf_fn_printTimeStamp
                                ("SipAlgPerformTranslation:Function Returning Fail");
                        }
#endif
                        return SipFail;
                    }
                }
            }
            else if (pNatInfo->direction == SipPktIn)
            {
                SipAlgNatHashKey   *pHashKey = SIP_NULL;
                SipAlgNatHashElement *pHashElement = SIP_NULL;

                pHashKey = (SipAlgNatHashKey *)
                    sip_memget (DECODE_MEM_ID, sizeof (SipAlgNatHashKey), &err);
                if (pHashKey == SIP_NULL)
                {
                    sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
                    pTemp = pKey;
                    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                    pKey = NULL;

                    pTemp = pPartialKey1;
                    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                    pPartialKey1 = NULL;
                    return SipFail;
                }

                pHashKey->ipAddress = pNatInfo->privateIP;
                pHashKey->port = pNatInfo->privatePort;

                /* Store the direction in hash key. Direction is used while 
                   performing a hash comparison. Interface index is not set
                   as it will not be used in hash comparison for incoming pkt */
                pHashKey->direction = pNatInfo->direction;

                pHashElement = (SipAlgNatHashElement *)
                    sip_hashFetch (&dGlbNatHash, (SIP_Pvoid) pHashKey);
                if (pHashElement != SIP_NULL)
                {
                    if (pHashElement->pSipAlgTimerInfo != SIP_NULL)
                    {
                        SIPDEBUG ((SIP_S8bit *) "Restarting Timer");
                        if (NatSipAlgReStartTimer
                            (pHashElement->pSipAlgTimerInfo,
                             gi4NatSipAlgPartialEntryTimer) != NAT_SUCCESS)
                        {
                            sip_hashRelease (&dGlbNatHash,
                                             (SIP_Pvoid) pHashKey);
                            pTemp = pHashKey;
                            sip_memfree (DECODE_MEM_ID,
                                         (SIP_Pvoid *) & pTemp, &err);
                            pHashKey = NULL;
#ifdef SDF_TIMESTAMP
                            if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
                            {
                                Sdf_fn_printTimeStamp
                                    ("SipAlgPerformTranslation:Function Returning Fail");
                            }
#endif
                            /* MEMLEAK FIX */
                            sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
                            pTemp = pKey;
                            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp,
                                         &err);
                            pKey = NULL;
                            /* MEMLEAK FIX */
                            return SipFail;
                        }
                    }
                    sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pHashKey);
                }
                pTemp = pHashKey;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pHashKey = NULL;
            }
        }
        /* decrement the refcount of natHashElement with hashRelease */
        sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
        (void) sip_trace (SIP_Detailed, SIP_Init,
                          (SIP_S8bit *) "SIP ALG TRACE: Translated IP/Port");

        SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgPerformTranslation");
        pTemp = pKey;
        sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
        pKey = NULL;
#ifdef SDF_TIMESTAMP
        if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
        {
            Sdf_fn_printTimeStamp
                ("SipAlgPerformTranslation:Function Returning Success");
        }
#endif
        return SipSuccess;
    }
    else
    {
        if (dGlbSipStatus == en_sipEnable)
        {
            if ((isMediaElement == SipFalse)
                && (pNatInfo->direction == SipPktOut))
            {
                /* This indicates that this entry is for signaling and having a 
                   different IP/Port than that of B2B. This can occur when there
                   is a public contact in REGISTER message. In this case
                   the entries will not be translated
                 */
                pNatInfo->mappedIP = pNatInfo->privateIP;
                pNatInfo->mappedPort = pNatInfo->privatePort;

                SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgPerformTranslation");
#ifdef SDF_TIMESTAMP
                if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
                {

                    Sdf_fn_printTimeStamp
                        ("SipAlgPerformTranslation:Function Returning Success");
                }
#endif
                pTemp = pKey;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pKey = NULL;
                return SipSuccess;
            }
        }
        if (pNatInfo->direction == SipPktOut)
        {
            /* Allocate memory for NAT Entry Info */
            pNatEntryInfo = (tNatEntryInfo *)
                sip_memget (DECODE_MEM_ID, sizeof (tNatEntryInfo), &err);
            if (pNatEntryInfo == SIP_NULL)
            {
                sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
                pTemp = pKey;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pKey = NULL;
                return SipFail;
            }
            sip_memset ((SIP_Pvoid) (pNatEntryInfo), 0, sizeof (tNatEntryInfo));

            /* There is no entry in NAT hash table for the given IP/Port.
               If the entry already exists in NAT, that will be returned */
            pNatEntryInfo->u4SrcIP = OSIX_NTOHL (pNatInfo->privateIP);
            pNatEntryInfo->u2SrcPort = pNatInfo->privatePort;
            retVal = sipAlgGetWANIPAddress (ifIndex, &natIP);
            if (retVal == 0)    /* NAT_DISABLE (2) and NAT_ENABLE (1) */
            {
                sip_error (SIP_Minor, "Could not get WAN IP.");
                pTemp = pKey;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pKey = NULL;
                pTemp = pNatEntryInfo;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pNatEntryInfo = NULL;
                return SipFail;
#ifdef SDF_TIMESTAMP
                if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
                {

                    Sdf_fn_printTimeStamp
                        ("SipAlgPerformTranslation:Function Returning Fail");
                }
#endif
            }
            else if (retVal == NAT_DISABLE)
            {
                isNatEnabled = SipFalse;
            }
            pNatEntryInfo->u4NATIP = natIP;

            pNatEntryInfo->u2NATPort = 0;
            /* A fix, so that for signaling entries only one binding is
               created. */
            /* NAT API --rasheed 
               if (isMediaElement == SipFalse)
               pNatEntryInfo->u2NATPort = (UINT2) NAT_SIP_TCPUDP_PORT; */
            /* For the outgoing packet the pinhole will be created
               in IN direction, so that the packet can come in */
            pNatEntryInfo->u4Direction = SipPktIn;
            /* Outside IP/Port are kept as unknown */
            pNatEntryInfo->u4DestIP = 0;
            pNatEntryInfo->u2DestPort = 0;

            pNatEntryInfo->u2Protocol = (UINT2) protocol;
            pNatEntryInfo->u1TimerFlag = NAT_SIP_ACTIVE;

            pNatEntryInfo->au1Padding[0] = 0;
            pNatEntryInfo->au1Padding[1] = 0;
            pNatEntryInfo->au1Padding[2] = 0;
            if (isMediaElement == SipTrue)
            {
                pNatEntryInfo->u1BindingType = en_media;
                pNatEntryInfo->u2NumBindings = 2;
            }
            else
            {
                pNatEntryInfo->u1BindingType = en_signaling;
                pNatEntryInfo->u2NumBindings = 1;
            }
            pNatEntryInfo->u1Parity = (UINT1) pNatInfo->parity;
            /* Invoke the API to create binding. In case there already exists 
               a binding in NAT, the translated IP and Port will be returned.
               Cookie is passed with value 0 - Sync API */
            CFA_UNLOCK ();
#ifdef SDF_TIMESTAMP
            if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
            {

                Sdf_fn_printTimeStamp
                    ("sipAlgPerformTrans:Pre-AricentNATAddPortMapping_ipc");
            }
#endif
            /* If AddPortMapping is called for signalling then set the 
               dGlbSignallingDestInfo.isAddPortMappingCalled to true, 
               to take action of buffering destination info in the 
               dynamic Entry creation callback and processing the buffered 
               information after AddPortMapping returns success */
            if (isMediaElement == SipFalse)
            {
                dGlbSignallingDestInfo.isAddPortMappingCalled = SipTrue;
            }
            else
            {
                dGlbSignallingDestInfo.isAddPortMappingCalled = SipFalse;
            }
            pNatResp = (tNatAsyncResponse *)
                sip_memget (DECODE_MEM_ID, sizeof (tNatAsyncResponse), &err);
            if (pNatResp == SIP_NULL)
            {
                sip_error (SIP_Minor, "Could not create NAT binding.");
                pTemp = pKey;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pKey = NULL;
                pTemp = pNatEntryInfo;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pNatEntryInfo = NULL;
                dGlbSignallingDestInfo.isAddPortMappingCalled = SipFalse;
#ifdef SDF_TIMESTAMP
                if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
                {

                    Sdf_fn_printTimeStamp
                        ("SipAlgPerformTranslation:Function Returning Fail");
                }
#endif
                return SipFail;
            }
            NatSipAlgAddPortMapping (pNatEntryInfo, 0, pNatResp);
#ifdef SDF_TIMESTAMP
            if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
            {

                Sdf_fn_printTimeStamp
                    ("sipAlgPerformTrans:Post-AricentNATAddPortMapping_ipc");
            }
#endif
            CFA_LOCK ();

            if (pNatResp->u4Status != NAT_SIP_SUCCESS)
            {
                sip_error (SIP_Minor, "Could not create NAT binding:");
                sip_error (SIP_Minor,
                           pSipAlgNATAPIErrorCode[pNatResp->u4ErrorCode]);
                /* free(pNatResp); */
                pTemp = pNatResp;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pNatResp = NULL;
                pTemp = pKey;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pKey = NULL;
                pTemp = pNatEntryInfo;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pNatEntryInfo = NULL;
                dGlbSignallingDestInfo.isAddPortMappingCalled = SipFalse;
#ifdef SDF_TIMESTAMP
                if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
                {

                    Sdf_fn_printTimeStamp
                        ("SipAlgPerformTranslation:Function Returning Fail");
                }
#endif
                return SipFail;
            }
            /* NAT is disabled on this interface. Don't store any entry in
               SIP ALG hash. */
            /* Get the translated IP address and port for the corresponding 
               private IP and port and update pNatInfo for outgoing message */
            pNatInfo->mappedIP = OSIX_HTONL (pNatEntryInfo->u4NATIP);
            /* NAT API--rasheed 
               if (isMediaElement == SipFalse)
               {
               pNatInfo->mappedPort = (UINT2) NAT_SIP_TCPUDP_PORT;
               }
               else
               {
               pNatInfo->mappedPort = pNatResp->u4RetVal1;
               } */
            pNatInfo->mappedPort = (SIP_U16bit) pNatResp->u4RetVal1;
            /*free(pNatResp); */
            pTemp = pNatResp;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
            pNatResp = NULL;
            pTemp = pNatEntryInfo;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
            pNatEntryInfo = NULL;
            (void) sip_trace (SIP_Detailed, SIP_Init,
                              (SIP_S8bit *)
                              "SIP ALG TRACE: Translated IP/Port");
        }
        else if ((pNatInfo->direction == SipPktIn) &&
                 (isMediaElement == SipTrue))
        {
            /* This block is for the creation for pinhole for the
               incoming sdp */
            SIP_U32bit          dCookie = 0;
            tPinholeInfo        dPinhole;

            dPinhole.u4SrcIP = 0;
            dPinhole.u2SrcPort = 0;
            dPinhole.u4DestIP = OSIX_NTOHL (pNatInfo->outsideIP);
            dPinhole.u2DestPort = pNatInfo->outsidePort;
            dPinhole.u2Protocol = NAT_SIP_UDP;
            dPinhole.u2Direction = NAT_OUTBOUND;
            dPinhole.u1TimerFlag = NAT_SIP_ACTIVE;
            /* lakshmi added - for NAT API Enhancements */
            /*dPinhole.au1Padding[0] = 0;
               dPinhole.au1Padding[1] = 0;
               dPinhole.au1Padding[2] = 0; */
            if (isMediaElement == SipTrue)
            {
                dPinhole.u2NumPinholes = 2;
                dPinhole.u1Parity = (UINT1) pNatInfo->parity;
            }
            else
            {
                dPinhole.u2NumPinholes = 1;
                dPinhole.u1Parity = en_any;
            }
            /* NAT API Enhancements - END */
            /* Cookie is passed with value 0 - Sync API */

            if ((dPinhole.u4DestIP == 0) && (dPinhole.u2DestPort == 0))
            {
                sip_error (SIP_Minor,
                           "NAT entry not exists for Translated IP & Port\n");
                /* MEMLEAK FIX */
                pTemp = pKey;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pKey = NULL;
                /* MEMLEAK FIX */
                return SipSuccess;
            }

            CFA_UNLOCK ();
#ifdef SDF_TIMESTAMP
            if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
            {

                Sdf_fn_printTimeStamp
                    ("sipAlgPerformTrans:Pre- AricentNATOpenPinhole_ipc");
            }
#endif
            pNatResp = (tNatAsyncResponse *)
                sip_memget (DECODE_MEM_ID, sizeof (tNatAsyncResponse), &err);
            if (pNatResp == SIP_NULL)
            {
                sip_error (SIP_Minor, "could not open explicit pinhole\n");
                pTemp = pKey;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pKey = NULL;
#ifdef SDF_TIMESTAMP
                if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
                {

                    Sdf_fn_printTimeStamp
                        ("SipAlgPerformTranslation:Function Returning Fail");
                }
#endif
                return SipFail;
            }

            NatSipAlgOpenPinhole ((tPinholeInfo *) & dPinhole, dCookie,
                                  pNatResp);
#ifdef SDF_TIMESTAMP
            if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
            {

                Sdf_fn_printTimeStamp
                    ("sipAlgPerformTrans:Post- AricentNATOpenPinhole_ipc");
            }
#endif

            CFA_LOCK ();
            if (pNatResp->u4Status != NAT_SIP_SUCCESS)
            {
                sip_error (SIP_Minor, "could not open explicit pinhole\n");
                sip_error (SIP_Minor,
                           pSipAlgNATAPIErrorCode[pNatResp->u4ErrorCode]);
                /* free(pNatResp); */
                pTemp = pNatResp;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pNatResp = NULL;
                pTemp = pKey;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pKey = NULL;
#ifdef SDF_TIMESTAMP
                if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
                {

                    Sdf_fn_printTimeStamp
                        ("SipAlgPerformTranslation:Function Returning Fail");
                }
#endif
                return SipFail;
            }
            /* free(pNatResp); */
            pTemp = pNatResp;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
            pNatResp = NULL;
        }
        else
        {
            /* if signalling packet is incoming and the partial entry 
               corresponding to this has got deleted as part of sipalg timer
               expiry callback, then drop the packet, instead of adding junk
               entry to NatHashTable */
            sip_error (SIP_Minor, "Signalling packets has been received from "
                       " public entity after the deletion of partial table "
                       " entry, hence dropping the packet");
            pTemp = pKey;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
            pKey = NULL;
#ifdef SDF_TIMESTAMP
            if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
            {

                Sdf_fn_printTimeStamp
                    ("SipAlgPerformTranslation:Function Returning Fail");
            }
#endif
            return SipFail;

        }

        /* As there is no entry in the NAT hash table, create a 
           new entry */
        if (sipAlgInitNatHashElement (&pNatHashElement, pKey,
                                      isMediaElement, SipTrue) == SipFail)
        {
            sip_error (SIP_Minor, "Could not initialize NAT hash element.");
            pTemp = pKey;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
            pKey = NULL;
#ifdef SDF_TIMESTAMP
            if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
            {

                Sdf_fn_printTimeStamp
                    ("SipAlgPerformTranslation:Function Returning Fail");
            }
#endif
            return SipFail;
        }

        if (pNatInfo->direction == SipPktOut)
        {
            /* Store the private IP address and port in NAT table */
            pNatHashElement->originalIP = pNatInfo->privateIP;
            pNatHashElement->originalPort = pNatInfo->privatePort;

            /* Update the protocol for NAT entry */
            pNatHashElement->protocol = (SIP_U16bit) protocol;
            /* Store the NAT Status */
            pNatHashElement->isNatEnabled = isNatEnabled;

            /* For the outgoin packet, store the translated
               IP and port so that it can be reused in next
               translation before SIP ALG timer expires. */
            pDestIPPort = (SipAlgIpPortPair *)
                sip_memget (DECODE_MEM_ID, sizeof (SipAlgIpPortPair), &err);
            if (pDestIPPort == SIP_NULL)
            {
                sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
                pTemp = pKey;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pKey = NULL;
                sipAlgRemoveEntryFromNatHash ((SIP_Pvoid) pNatHashElement);
                return SipFail;
            }
            /* Store the translated IP address and port and outside 
               IP address and port in NAT table. Outside IP/Port
               will always be unknown. */
            /*pDestIPPort->destIP = 0;
               pDestIPPort->destPort = 0; */
            /* Commenting the above code. For signalling entries we need to
               store the destination information also in nat hash. We get 
               this destination information from the dynamic entry created
               callback, which gets buffered in 
               dGlbSignallingDestInfo.dOutIPAddress and dOutPort */

            if ((dGlbSignallingDestInfo.isAddPortMappingCalled == SipTrue)
                && (isMediaElement == SipFalse))
            {
                pDestIPPort->destIP = dGlbSignallingDestInfo.dOutIPAddress;
                pDestIPPort->destPort = dGlbSignallingDestInfo.dOutPort;
                dGlbSignallingDestInfo.isAddPortMappingCalled = SipFalse;
            }
            else
            {
                pDestIPPort->destIP = 0;
                pDestIPPort->destPort = 0;
            }
            pDestIPPort->mappedIP = pNatInfo->mappedIP;
            pDestIPPort->mappedPort = pNatInfo->mappedPort;

            /* Add the destination IP/Port entry */
            if (sip_listAppend (pNatHashElement->pDestInfoList,
                                (SIP_Pvoid) pDestIPPort, &err) == SipFail)
            {
                sip_error (SIP_Minor, "Could not append dest info in"
                           " NAT hash table");
                pTemp = pKey;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pKey = NULL;
                /* As append is failed, dest list will be null. So freeing
                   it here */
                pTemp = pDestIPPort;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pDestIPPort = NULL;
                sipAlgRemoveEntryFromNatHash ((SIP_Pvoid) pNatHashElement);
#ifdef SDF_TIMESTAMP
                if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
                {

                    Sdf_fn_printTimeStamp
                        ("SipAlgPerformTranslation:Function Returning Fail");
                }
#endif
                return SipFail;
            }
            /* This is set for outgoing media. For signaling the value 
               will always be SipTrue. */
            pNatHashElement->isBindingCreated = SipTrue;
        }
        else if ((pNatInfo->direction == SipPktIn) &&
                 (isMediaElement == SipTrue))
        {
            /* Store the outside IP address and port in NAT table */
            pNatHashElement->originalIP = pNatInfo->outsideIP;
            pNatHashElement->originalPort = pNatInfo->outsidePort;
            /* Update the protocol for NAT entry */
            pNatHashElement->protocol = (SIP_U16bit) protocol;
            pNatHashElement->isBindingCreated = SipFalse;
        }

        if (isMediaElement == SipTrue)
        {
            pPartialKey = (SipAlgMediaKey *) sip_memget (0,
                                                         sizeof
                                                         (SipAlgMediaKey),
                                                         &err);
            if (pPartialKey == SIP_NULL)
            {
                pTemp = pKey;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pKey = NULL;
                sipAlgRemoveEntryFromNatHash ((SIP_Pvoid) pNatHashElement);
                return SipFail;
            }
            pPartialKey->pFromTag = sip_strdup (pMediaKey->pFromTag, 0);
            pPartialKey->pCallID = sip_strdup (pMediaKey->pCallID, 0);
            if (pMediaKey->pToTag != SIP_NULL)
                pPartialKey->pToTag = sip_strdup (pMediaKey->pToTag, 0);
            else
                pPartialKey->pToTag = SIP_NULL;

            /* Add the media key along with the element */
            if (sip_listAppend (pNatHashElement->pCallInfoList,
                                (SIP_Pvoid) pPartialKey, &err) == SipFail)
            {
                sip_error (SIP_Minor, "Could not append dialog key in NAT "
                           "hash for media information.");

                sip_memfree (DECODE_MEM_ID,
                             (SIP_Pvoid) & (pPartialKey->pFromTag), &err);
                sip_memfree (DECODE_MEM_ID,
                             (SIP_Pvoid) & (pPartialKey->pCallID), &err);
                if (pPartialKey->pToTag != SIP_NULL)
                    sip_memfree (DECODE_MEM_ID,
                                 (SIP_Pvoid) & (pPartialKey->pToTag), &err);
                pTemp = pPartialKey;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pPartialKey = NULL;
                sipAlgRemoveEntryFromNatHash ((SIP_Pvoid) pNatHashElement);
#ifdef SDF_TIMESTAMP
                if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
                {

                    Sdf_fn_printTimeStamp
                        ("SipAlgPerformTranslation:Function Returning Fail");
                }
#endif
                /* MEMLEAK FIX */
                pTemp = pKey;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pKey = NULL;
                /* MEMLEAK FIX */
                return SipFail;
            }
        }
        /* The interface index is also stored in NAT hash element. While
           deleting the entries from hash table on WAN IP change, the 
           interface index can be used to delete the specific entry */
        pNatHashElement->ifIndex = pNatInfo->ifIndex;

        /*lakshmi added - NAT API Enhancements - START */
        pNatHashElement->parity = pNatInfo->parity;
        /* NAT API Enhancements - END */

        /* Add the pNatHashElement in the NAT hash table. */
        if (sip_hashAdd (&dGlbNatHash,
                         ((SIP_Pvoid) pNatHashElement),
                         ((SIP_Pvoid) pKey)) != SipSuccess)
        {
            sip_error (SIP_Minor, "Could not add hash element in "
                       " NAT hash table");
            pTemp = pKey;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
            pKey = NULL;
            /* sip_memfree(DECODE_MEM_ID,(SIP_Pvoid *)&pDestIPPort,&err); */
            sipAlgRemoveEntryFromNatHash ((SIP_Pvoid) pNatHashElement);
#ifdef SDF_TIMESTAMP
            if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
            {

                Sdf_fn_printTimeStamp
                    ("SipAlgPerformTranslation:Function Returning Fail");
            }
#endif
            return SipFail;
        }

        /* For Intra-site call handling. Whether it is media element or 
           signalling element we need to store an extra entry in nat hash 
           with translated ip, port as its key for any outgoing element */
        if (pNatInfo->direction == SipPktOut)
        {
            /* Allocate memory for NAT hash key */
            pTransKey = (SipAlgNatHashKey *)
                sip_memget (DECODE_MEM_ID, sizeof (SipAlgNatHashKey), &err);
            if (pTransKey == SIP_NULL)
            {
                sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
                pTemp = pKey;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pKey = NULL;
                return SipFail;
            }
            /* Get the translated IP and port from pNatInfo and form the 
               Key. This will be used to map any response or request 
               coming from WAN side. */
            if (isNatEnabled == SipTrue)
            {
                pTransKey->ipAddress = pNatInfo->mappedIP;
                pTransKey->port = pNatInfo->mappedPort;
            }
            else
            {
                pTransKey->ipAddress = pNatInfo->privateIP;
                pTransKey->port = pNatInfo->privatePort;
            }
            /* Interface index is not set as it will not be used in 
               hash comparison for incoming pkt. This hash entry is to
               allow incoming packets */
            pTransKey->direction = SipPktIn;

            if (sipAlgInitNatHashElement (&pTempNatHashElement, pTransKey,
                                          isMediaElement, SipFalse) == SipFail)
            {
                sip_error (SIP_Minor, "Could not initialize NAT hash element.");
                pTemp = pTransKey;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pTransKey = NULL;
#ifdef SDF_TIMESTAMP
                if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
                {

                    Sdf_fn_printTimeStamp
                        ("SipAlgPerformTranslation:Function Returning Fail");
                }
#endif
                return SipFail;
            }
            /* Store the NAT status */
            pTempNatHashElement->isNatEnabled = isNatEnabled;

            /* Store the private IP address and port in NAT table */
            pTempNatHashElement->originalIP = pNatInfo->privateIP;
            pTempNatHashElement->originalPort = pNatInfo->privatePort;

            /* Update the protocol for NAT entry */
            pTempNatHashElement->protocol = (SIP_U16bit) protocol;

            /* Both for signaling and media, the translated NAT hash entry
               will have isBindingCreated as SipTrue */
            pTempNatHashElement->isBindingCreated = SipTrue;

            /* There is no timer for mapped entries. By setting this value,
               it is made sure that it never gets restarted */
            pTempNatHashElement->isRegTimerStarted = SipTrue;

            /* For the outgoin packet, store the translated
               IP and port so that it can be reused in next
               translation before SIP ALG timer expires. */
            pTempDestIPPort = (SipAlgIpPortPair *)
                sip_memget (DECODE_MEM_ID, sizeof (SipAlgIpPortPair), &err);
            if (pTempDestIPPort == SIP_NULL)
            {
                sipAlgRemoveEntryFromNatHash ((SIP_Pvoid) pTempNatHashElement);
                pTemp = pTransKey;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pTransKey = NULL;
                return SipFail;
            }

            /* Store the translated IP address and port and outside 
               IP address and port in NAT table. Outside IP/Port
               will always be unknown. */
            pTempDestIPPort->destIP = 0;
            pTempDestIPPort->destPort = 0;
            pTempDestIPPort->mappedIP = pNatInfo->mappedIP;
            pTempDestIPPort->mappedPort = pNatInfo->mappedPort;

            /* Add the destination IP/Port entry */
            if (sip_listAppend (pTempNatHashElement->pDestInfoList,
                                (SIP_Pvoid) pTempDestIPPort, &err) == SipFail)
            {
                sip_error (SIP_Minor, "Could not append dest info in"
                           " NAT hash table");
                pTemp = pTransKey;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pTransKey = NULL;
                /* As append is failed, dest list will be null. So freeing
                   it here */
                pTemp = pTempDestIPPort;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pTempDestIPPort = NULL;
                sipAlgRemoveEntryFromNatHash ((SIP_Pvoid) pTempNatHashElement);
#ifdef SDF_TIMESTAMP
                if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
                {

                    Sdf_fn_printTimeStamp
                        ("SipAlgPerformTranslation:Function Returning Fail");
                }
#endif
                return SipFail;
            }

            /* The interface index is also stored in NAT hash element. While
               deleting the entries from hash table on WAN IP change, the 
               interface index can be used to delete the specific entry */
            pTempNatHashElement->ifIndex = pNatInfo->ifIndex;

            /*lakshmi added - NAT API Enhancements - START */
            pTempNatHashElement->parity = pNatInfo->parity;
            /* NAT API Enhancements - END */

            /* Add the pNatHashElement in the hash table using a 
               different key */
            if (sip_hashAdd (&dGlbNatHash,
                             ((SIP_Pvoid) pTempNatHashElement),
                             ((SIP_Pvoid) pTransKey)) != SipSuccess)
            {
                sip_error (SIP_Minor, "Could not add hash element in "
                           "NAT hash table");
                /*  sip_memfree(DECODE_MEM_ID,(SIP_Pvoid *)&pDestIPPort,&err); */
                pTemp = pTransKey;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pTransKey = NULL;
                sipAlgRemoveEntryFromNatHash ((SIP_Pvoid) pTempNatHashElement);
#ifdef SDF_TIMESTAMP
                if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
                {

                    Sdf_fn_printTimeStamp
                        ("SipAlgPerformTranslation:Function Returning Fail");
                }
#endif
                return SipFail;
            }
        }

        /* Both for signaling and Media start the timer. */
        SIPDEBUG ((SIP_S8bit *) "Started Timer");
        dRetVal = NatSipAlgStartTimer (pNatHashElement->pSipAlgTimerInfo,
                                       gi4NatSipAlgPartialEntryTimer);
        if (dRetVal != NAT_SUCCESS)
        {
            sip_error (SIP_Minor, "Could not start SIP ALG timer "
                       " for contact.");
            /* sip_memfree(DECODE_MEM_ID,(SIP_Pvoid *)&pDestIPPort,&err); */
            /* sipAlgRemoveEntryFromNatHash((SIP_Pvoid *)pNatHashElement); */
            /* As hash add is success, call hash remove to free the entry */
            sip_hashRemove (&dGlbNatHash, pKey);
            if (isMediaElement == SipFalse)
                sip_hashRemove (&dGlbNatHash, pTransKey);
#ifdef SDF_TIMESTAMP
            if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
            {

                Sdf_fn_printTimeStamp
                    ("SipAlgPerformTranslation:Function Returning Fail");
            }
#endif

            return SipFail;
        }
        (void) sip_trace (SIP_Detailed, SIP_Init,
                          (SIP_S8bit *)
                          "SIP ALG TRACE: Added entry in NAT hash");
    }
    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgPerformTranslation");
#ifdef SDF_TIMESTAMP
    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
    {
        Sdf_fn_printTimeStamp
            ("SipAlgPerformTranslation:Function Returning Success");
    }
#endif
    return SipSuccess;
}

/****************************************************************************
* Function Name : sipAlgNatNotificationCbkFunc
* Description     :  This is the callback function which will be registered from 
*    the SIP ALG to the NAT module. When the NAT Idle timer expires for any 
*     binding and pinhole, it will be notified to the SIP ALG through this 
*    function only. SIP ALG will clear off the details maintained in its scope 
*    using the information provided by the NAT module.
*   This function is invoked when a dynamic entry is created 
* Input/Output (s) : pEventNotificationInfo - Passed by NAT madule
* Returns    : VOID
****************************************************************************/
void
sipAlgNatNotificationCbkFunc (tEventNotificationInfo * pEventNotificationInfo)
{
    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgNatNotificationCbkFunc");
    /* Switch on the event type and take appropriate action */
    switch (pEventNotificationInfo->u2EventType)
    {
        case IPC_NAT_DYN_ENTRY_EXP_EVENT:
        {
            sipAlgHandleDeletedDynEntry
                (pEventNotificationInfo->EventInfo.natEntryInfo);
            break;
        }
        case IPC_NAT_DYN_ENTRY_ADD_EVENT:
        {
            sipAlgHandleCreatedDynEntry
                (pEventNotificationInfo->EventInfo.natEntryInfo);
            break;
        }
        case IPC_LINK_STATUS_EVENT:
        {
            sipAlgDeInitOnInterfaceChange (pEventNotificationInfo->EventInfo.
                                           linkInfo);
            break;
        }

    }
    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgNatNotificationCbkFunc");
}

/****************************************************************************
* Function Name : sipAlgHandleDeletedDynEntry
* Description     : This function is invoked to clear the entry becasue
*                  NAT idle timer is expired.
* Input/Output (s) : natEntry- This is passed by NAT
* Returns    : SUCCESS/FAILURE
****************************************************************************/
void
sipAlgHandleDeletedDynEntry (tNatEntryInfo natEntry)
{
    /* This pointer is used to avoid strict-aliasing warning */
    SIP_Pvoid           pTemp = SIP_NULL;
    SipAlgNatHashKey   *pKey = SIP_NULL;
    SipAlgNatHashElement *pNatHashElement = SIP_NULL;
    SipError            err;
    SIP_U32bit          tempDest = 0;
    SIP_U32bit          ifIndex = 0;
    SIP_U16bit          tempPort = 0;

    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgHandleDeletedDynEntry");
#ifdef SIPALG_UT_FLAG
    PRINTF ("\n\nThe dynamic entry deletion for the ip and "
            " port: %x,%d,%x,%u,%x,%u\n\n", natEntry.u4SrcIP,
            natEntry.u2SrcPort, natEntry.u4DestIP, natEntry.u2DestPort,
            natEntry.u4NATIP, natEntry.u2NATPort);
#endif
    /* In case of symmetric media (sending and listening port are same).
       Consider the scenario - m1 in outgoing invite and m2 in incoming answer.
       For m1 one entry is created in NAT hash and same for m2. When a dynamic
       entry is created, both the entries in NAT hash were updated. 
       On NAT idle timer, both the entries will be deleted. */

    /* Allocate memory for NAT hash key */
    pKey = (SipAlgNatHashKey *)
        sip_memget (DECODE_MEM_ID, sizeof (SipAlgNatHashKey), &err);
    if (pKey == SIP_NULL)
    {
        return;
    }
    /* Get the outside IP and port from pNatEntryInfo and form the Key to 
       identify whether the entries are present in the NAT Hash Table. */
    pKey->ipAddress = natEntry.u4DestIP;
    pKey->port = natEntry.u2DestPort;
    pKey->direction = SipPktIn;

    /* Fetch the entry from the hash table */
    pNatHashElement = (SipAlgNatHashElement *)
        sip_hashFetch (&dGlbNatHash, (SIP_Pvoid) pKey);

    if (pNatHashElement != SIP_NULL)
    {
        /* NatHashElement stores entries with respect to original
           IP and Port. When a entry is found with Dest IP and Port,
           in the list of dest in pNatHashElement, the entries will be 
           natted IP and Src IP. (see sipAlgDeleteDynamicEntry) */
        tempDest = natEntry.u4DestIP;
        tempPort = natEntry.u2DestPort;
        natEntry.u4DestIP = natEntry.u4SrcIP;
        natEntry.u2DestPort = natEntry.u2SrcPort;

        /* hashRelease will be done in sipAlgDeleteDynamicEntry */
        sipAlgDeleteDynamicEntry (pNatHashElement, pKey, &natEntry);

        /* Get the private IP and port from pNatEntryInfo and form the Key to 
           identify whether the entries are present in the NAT Hash Table. */
        pKey->ipAddress = natEntry.u4SrcIP;
        pKey->port = natEntry.u2SrcPort;
        pKey->direction = SipPktOut;

        if (sipAlgGetIfIndex (natEntry.u4NATIP, &ifIndex) == SipSuccess)
        {
            pKey->ifIndex = ifIndex;
            /* Keep the original value back as it was modified */
            natEntry.u4DestIP = tempDest;
            natEntry.u2DestPort = tempPort;

            /* Fetch the entry from the hash table */
            pNatHashElement = (SipAlgNatHashElement *)
                sip_hashFetch (&dGlbNatHash, (SIP_Pvoid) pKey);
            if (pNatHashElement != SIP_NULL)
            {
                sipAlgDeleteDynamicEntry (pNatHashElement, pKey, &natEntry);
            }
            /* else - asymmetric media flow */
        }
    }
    else
    {
        /* Outside IP/Port are not matching. Might be the case of 
           asymmetric media. */

        /* Get the private IP and port from pNatEntryInfo and form the Key to 
           identify whether the entries are present in the NAT Hash Table. */
        pKey->ipAddress = natEntry.u4SrcIP;
        pKey->port = natEntry.u2SrcPort;
        pKey->direction = SipPktOut;

        if (sipAlgGetIfIndex (natEntry.u4NATIP, &ifIndex) == SipSuccess)
        {
            pKey->ifIndex = ifIndex;
            /* Fetch the entry from the hash table */
            pNatHashElement = (SipAlgNatHashElement *)
                sip_hashFetch (&dGlbNatHash, (SIP_Pvoid) pKey);
            if (pNatHashElement != SIP_NULL)
            {
                sipAlgDeleteDynamicEntry (pNatHashElement, pKey, &natEntry);
            }
            /* else - Not a media related info */
        }
    }
#ifdef SIPALG_UT_FLAG
    PRINTF ("\n\n***The NAT hash and media Hash values at the end of "
            "dynamic entry deletion\n\n");
    sipAlgNatHashPrint (&dGlbNatHash);
    /* SIPALG - 2.1 */
    sipAlgIPHashPrint (&dGlbIPHash);
    sipAlgMediaHashPrint (&dGlbMediaHash);
#endif
    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgHandleDeletedDynEntry");
    pTemp = pKey;
    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
    pKey = NULL;
    return;
}

/****************************************************************************
* Function Name : sipAlgDeleteDynamicEntry
* Description     : This function is invoked to clear the NAT hash entry 
*                  (NAT idle timer is expired)
* Input/Output (s) : pNatHashElement- This element has the corresponding dyn
*                    entry.
*                    pKey - Key for the hash element
*                    pNatEntry - Passed by NAT
* Returns    : VOID
****************************************************************************/
void
sipAlgDeleteDynamicEntry (SipAlgNatHashElement * pNatHashElement,
                          SipAlgNatHashKey * pKey, tNatEntryInfo * pNatEntry)
{
    en_SipBoolean       entryFound = SipFalse;
    SipError            err;
    SIP_U32bit          listSizeDest = 0, dDestInfoActualSize = 0;
    SIP_U32bit          listSizeCall = 0;
    SIP_U32bit          listIter = 0;
    SIP_U32bit          deleteAtindex = 0;
    SipAlgIpPortPair   *pDestIPPort = SIP_NULL;
    SipAlgMediaKey     *pMediaKey = SIP_NULL;
    SipAlgNatHashKey    transKey;
    SIP_Pvoid           pIPPort = SIP_NULL;

    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgDeleteDynamicEntry");
    /* No action on the deletion of dynamic entry for the 
       signaling related entries */
    if (pNatHashElement->isMediaElement == SipFalse)
    {
        (void) sip_trace (SIP_Detailed, SIP_Init,
                          (SIP_S8bit *)
                          "SIP ALG TRACE: Dyn entry for contact is deleted");
        sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
        return;
    }
    /* This is the SIP ALG expiry for media */
    (void) sip_listSizeOf (pNatHashElement->pDestInfoList, &listSizeDest, &err);

    dDestInfoActualSize = listSizeDest;

    listIter = 0;
    while (listIter != listSizeDest)
    {
        (void) sip_listGetAt (pNatHashElement->pDestInfoList,
                              listIter, (SIP_Pvoid *) & pIPPort, &err);
        pDestIPPort = (SipAlgIpPortPair *) pIPPort;

        if ((pNatEntry->u4NATIP
             == pDestIPPort->mappedIP) &&
            (pNatEntry->u2NATPort
             == pDestIPPort->mappedPort) &&
            (pNatEntry->u4DestIP
             == pDestIPPort->destIP) &&
            (pNatEntry->u2DestPort == pDestIPPort->destPort))
        {
            entryFound = SipTrue;
            deleteAtindex = listIter;
        }
        /* The dynamic entry is found. There is a possibility of 
           multiple dynamic entries created because of single partial 
           entry. */
        if (entryFound == SipTrue)
        {
            /* Store the mapped ip and port in transkey so that
               in case it is a binding, the mapped nat hash entry
               can be removed */
            transKey.ipAddress = pDestIPPort->mappedIP;
            transKey.port = pDestIPPort->mappedPort;
            transKey.direction = SipPktIn;
            break;
        }
        listIter++;
    }

    if (entryFound == SipTrue)
    {

        if ((pNatHashElement->isBindingCreated == SipTrue)
            && (dDestInfoActualSize == 1))
        {
            /* In case of binding, if the destinfo list size is 1 then
               the destinfo should not be deleted;
               only the destip and port should be set to zero, since
               on sipalg timer expiry the NAT hash element can be removed */
            (void) sip_listGetAt (pNatHashElement->pDestInfoList,
                                  deleteAtindex, (SIP_Pvoid *) & pIPPort, &err);

            pDestIPPort = (SipAlgIpPortPair *) pIPPort;
            pDestIPPort->destIP = 0;
            pDestIPPort->destPort = 0;
        }
        else
        {
            /* For pinhole, we can delete the dest info list */
            sip_listDeleteAt (pNatHashElement->pDestInfoList, deleteAtindex,
                              &err);
        }

        /* SIP ALG timer already epxpired for this media info. */
        if (pNatHashElement->pSipAlgTimerInfo == SIP_NULL)
        {
            /* first delete the entries from media hash */
            (void) sip_listSizeOf (pNatHashElement->pCallInfoList,
                                   &listSizeCall, &err);

            listIter = 0;
            while (listIter != listSizeCall)
            {
                SipAlgIpPort        ipPortKey;
                /* This pointer is used to avoid strict-aliasing warning */
                SIP_Pvoid           pTemp = SIP_NULL;

                pTemp = pMediaKey;
                (void) sip_listGetAt (pNatHashElement->pCallInfoList,
                                      listIter, (SIP_Pvoid *) & pTemp, &err);
                /* Delete the element from media hash for the
                   dialog key for which dest IP matches. */
                ipPortKey.ipAddress = pNatHashElement->originalIP;
                ipPortKey.port = pNatHashElement->originalPort;
                if (sipAlgDeleteMediaElement (pMediaKey, &ipPortKey) == SipFail)
                {
                    sip_error (SIP_Minor, "Could not delete entry "
                               "from media hash");
                    sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
                    return;
                }
                listIter++;
            }
            /* Get the size of the destinfo..if its zero, it indicates
               that the existing dynamic entry is deleted */
            (void) sip_listSizeOf (pNatHashElement->pDestInfoList,
                                   &listSizeDest, &err);
            sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);

            /* There was only one dynamic entry created and 
               corresponding to one call. So clear this hash element */
            if ((pNatHashElement->isBindingCreated == SipFalse)
                && (listSizeDest == 0))
            {
                if (SipFail == sip_hashRemove (&dGlbNatHash, pKey))
                {
                    sip_error (SIP_Minor, "Could not remove the media hash "
                               "element.");
                    return;
                }
            }
            /* if it is a binding, remove the mapped nat hash entry */
            if ((pNatHashElement->isBindingCreated == SipTrue)
                && (listSizeDest == 1))
            {
                if (SipFail == sip_hashRemove (&dGlbNatHash, pKey))
                {
                    sip_error (SIP_Minor, "Could not remove the media hash "
                               "element.");
                    return;
                }

                if (SipFail == sip_hashRemove (&dGlbNatHash, &transKey))
                {
                    sip_error (SIP_Minor, "Could not remove"
                               " the hash element.");
                    return;
                }
            }
        }
        else
            sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
    }
    else
    {
        /* If dest info is zero, this will occur, when there is an early
           media and dest info is not updated in answers' SDP nat hash entry;
           if dest info is present and there is no matching ie entryFound
           false  */
        sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
    }

    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgDeleteDynamicEntry");
}

/****************************************************************************
* Function Name : sipAlgHandleCreatedDynEntry
* Description     : Once a dynamic entry is created in NAT corresponding to a
*                  partial entry, this callback function is invoked by NAT.
*                  The same function is invoked even in case of dynamic entry
*                  being created because of pinhole (to allow outgoing media) 
*                  or because of NAT partial entry (to allow incoming media)
*                 
* Input/Output (s) : natEntry - This object is returned by NAT
* Returns    : VOID
****************************************************************************/
void
sipAlgHandleCreatedDynEntry (tNatEntryInfo natEntry)
{
    /* This pointer is used to avoid strict-aliasing warning */
    SIP_Pvoid           pTemp = SIP_NULL;
    SipAlgNatHashKey   *pKey = SIP_NULL;
    SipAlgNatHashElement *pNatHashElement = SIP_NULL;
    SipAlgIpPortPair   *pDestIPPort1 = SIP_NULL;
    SipAlgIpPortPair   *pDestIPPort2 = SIP_NULL;
    SipAlgIpPortPair   *pDestIPPort = SIP_NULL;
    SIP_Pvoid           pIPPort = SIP_NULL;
    SIP_U32bit          listSizeDest = 0;
    SIP_U32bit          ifIndex = 0;
    SipError            err;

    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgHandleCreatedDynEntry");
#ifdef SIPALG_UT_FLAG
    PRINTF ("\n\nThe dynamic entry creation for the ip and "
            "port: %x,%u,%x,%u,%x,%u\n\n", natEntry.u4SrcIP,
            natEntry.u2SrcPort, natEntry.u4DestIP, natEntry.u2DestPort,
            natEntry.u4NATIP, natEntry.u2NATPort);
#endif

    /* Allocate memory for NAT hash key */
    pKey = (SipAlgNatHashKey *)
        sip_memget (DECODE_MEM_ID, sizeof (SipAlgNatHashKey), &err);
    if (pKey == SIP_NULL)
    {
        return;
    }

    /* In case there was a pinhole already created to allow the packets 
       to go from private end to outside network, now after creating dynamic 
       entry, the binding should be updated. */

    /* Get the outside IP and port from pNatEntry and form the Key to 
       identify whether the entries are present in the NAT Hash Table. */
    pKey->ipAddress = natEntry.u4DestIP;
    pKey->port = natEntry.u2DestPort;
    pKey->direction = SipPktIn;

    /* Fetch the entry from the hash table */
    pNatHashElement = (SipAlgNatHashElement *)
        sip_hashFetch (&dGlbNatHash, (SIP_Pvoid) pKey);

    if (pNatHashElement != SIP_NULL)
    {
        /* No action on the creation of dynamic entry for signaling entries */
        if (pNatHashElement->isMediaElement == SipFalse)
        {
            sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
            pTemp = pKey;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
            pKey = NULL;
            return;
        }
        pDestIPPort1 = (SipAlgIpPortPair *)
            sip_memget (DECODE_MEM_ID, sizeof (SipAlgIpPortPair), &err);
        if (pDestIPPort1 == SIP_NULL)
        {
            sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
            pTemp = pKey;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
            pKey = NULL;
            return;
        }
        /*Update the private IP/Port and translated IP/port entry in the list */
        pDestIPPort1->destIP = natEntry.u4SrcIP;
        pDestIPPort1->destPort = natEntry.u2SrcPort;
        pDestIPPort1->mappedIP = natEntry.u4NATIP;
        pDestIPPort1->mappedPort = natEntry.u2NATPort;

        /* Add the destination IP/Port entry in NAT hash */
        if (sip_listAppend (pNatHashElement->pDestInfoList,
                            (SIP_Pvoid) pDestIPPort1, &err) == SipFail)
        {
            sip_error (SIP_Minor, "Could not append dest info in"
                       " NAT hash table");

            sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
            pTemp = pKey;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
            pKey = NULL;
            pTemp = pDestIPPort1;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
            pDestIPPort1 = NULL;
            return;
        }
        sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
    }
    /* In case there was already a binding created and now because of RTP
       flow from outside to inside network, a dynamic entry is created
       then update the entry for this binding.

       Get the private IP and port from pNatEntryInfo and form the Key to 
       identify whether the entries are present in the NAT Hash Table. */
    pKey->ipAddress = natEntry.u4SrcIP;
    pKey->port = natEntry.u2SrcPort;
    pKey->direction = SipPktOut;

    if (sipAlgGetIfIndex (natEntry.u4NATIP, &ifIndex) == SipSuccess)
    {
        pKey->ifIndex = ifIndex;
        /* Fetch the entry from the hash table */
        pNatHashElement = (SipAlgNatHashElement *)
            sip_hashFetch (&dGlbNatHash, (SIP_Pvoid) pKey);

        if (pNatHashElement != SIP_NULL)
        {
            /*No action on the creation of dynamic entry for signaling entries */
            if (pNatHashElement->isMediaElement == SipFalse)
            {
                sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
                pTemp = pKey;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pKey = NULL;
                return;
            }
            (void) sip_listSizeOf (pNatHashElement->pDestInfoList,
                                   &listSizeDest, &err);
            /* There will be already one entry for signaling
               which would have stored mapped IP/Port */
            if (listSizeDest == 1)
            {
                (void) sip_listGetAt (pNatHashElement->pDestInfoList,
                                      0, (SIP_Pvoid *) & pIPPort, &err);
                pDestIPPort = (SipAlgIpPortPair *) pIPPort;
                if (pDestIPPort->destIP == 0)
                {
                    /* Delete the existing entry from the list
                       Add the new entry which will have the same
                       mapped IP but different dest IP. */
                    sip_listDeleteAt (pNatHashElement->pDestInfoList, 0, &err);
                }
            }
            pDestIPPort2 = (SipAlgIpPortPair *)
                sip_memget (DECODE_MEM_ID, sizeof (SipAlgIpPortPair), &err);
            if (pDestIPPort2 == SIP_NULL)
            {
                sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
                pTemp = pKey;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pKey = NULL;
                return;
            }
            pNatHashElement->originalIP = natEntry.u4SrcIP;
            pNatHashElement->originalPort = natEntry.u2SrcPort;

            /* Update the outside IP/port entry in the list. The translated
               IP address and port will also be kept along with this. */
            pDestIPPort2->destIP = natEntry.u4DestIP;
            pDestIPPort2->destPort = natEntry.u2DestPort;
            pDestIPPort2->mappedIP = natEntry.u4NATIP;
            pDestIPPort2->mappedPort = natEntry.u2NATPort;

            /* Add the destination IP/Port entry in NAT hash table. */
            if (sip_listAppend (pNatHashElement->pDestInfoList,
                                (SIP_Pvoid) pDestIPPort2, &err) == SipFail)
            {
                sip_error (SIP_Minor, "Could not append dest info in"
                           " NAT hash table");
                sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
                pTemp = pKey;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pKey = NULL;
                pTemp = pDestIPPort2;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pDestIPPort2 = NULL;
                return;
            }
            sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
        }
        /* Upon calling addPortMapping for signalling we get this dynamic
           entry created callback if dynamic entry already exists and
           partial entry is being formed using that. Creation of Hash element
           corresponding to partial entry created will happen only if 
           addPortMaping returns success, so here hashfetch returns fails, 
           hence buffering the destination information here. Once Hash element
           is formed, this buffered info will be processed there */
        else
        {
            if (dGlbSignallingDestInfo.isAddPortMappingCalled == SipTrue)
            {
                dGlbSignallingDestInfo.dOutIPAddress = natEntry.u4DestIP;
                dGlbSignallingDestInfo.dOutPort = natEntry.u2DestPort;
            }
        }
    }
    /* In case of symmetric media (sending and listening port are same).
       Consider the scenario - m1 in outgoing invite and m2 in incoming answer.
       For m1 one entry is created in NAT hash and same for m2. When a dynamic
       entryis created, outside IP/port will match. So update that entry.
       But at the same time, we have to update the entry corresponding to 
       private IP/port if it is there. So both the rows are updated.
       The deletion will be handled on NAT Idle timer expiry */
#ifdef SIPALG_UT_FLAG
    PRINTF ("\n\n*******The NAT hash and media Hash values at the end "
            "of dynamic entry creation\n\n");
    sipAlgNatHashPrint (&dGlbNatHash);
    /* SIPALG - 2.1 */
    sipAlgIPHashPrint (&dGlbIPHash);
    sipAlgMediaHashPrint (&dGlbMediaHash);
#endif

    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgHandleCreatedDynEntry");
    pTemp = pKey;
    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
    pKey = NULL;
    return;
}

/****************************************************************************
* Function Name : sipAlgTimerExpCbkFunc
* Description     : On SIP ALG timer expiry this function is invoked. Based on
*                  this either close the pinhole or bindings
*                 
* Input/Output (s) :  pSipAlgTimer - This object is returned by NAT timer
* Returns    : Nothing
****************************************************************************/
void
sipAlgTimerExpCbkFunc (tNatSipAlgTimer * pSipAlgTimer)
{
    /* This pointer is used to avoid strict-aliasing warning */
    SIP_Pvoid           pTemp = SIP_NULL;
    tNatAsyncResponse  *pNatResp = NULL;
    SipAlgNatHashElement *pNatHashElement = SIP_NULL;
    tNatEntryInfo      *pNatEntryInfo = SIP_NULL;
    tPinholeInfo       *pPinhole = SIP_NULL;
    SIP_U32bit          listSizeCall = 0;
    SIP_U32bit          listIterCall = 0;
    SIP_U32bit          listIter = 0;
    SIP_Pvoid           pIPPort = SIP_NULL;
    SIP_U32bit          listSizeDest = 0;
    SipAlgIpPortPair   *pDestIPPort = SIP_NULL;
    SipAlgMediaKey     *pMediaKey = SIP_NULL;
    SipError            err;
    en_SipBoolean       dIsDynEntryExist = SipTrue;
/*klocwork change*/
    SIP_U16bit          dDestIPPortFlag = 0;

    /* SIPALG - 2.1 */
    SipAlgIPHashElement *pIPHashElement;
    SipAlgIPHashKey    *pIPKey = SIP_NULL;

    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgTimerExpCbkFunc");

    /* In case the timer expired for IP hash table (used for O-line)
       clear the entries in IP hash table and return */
    if (pSipAlgTimer->pHashKey->isMediaPortZero == SipTrue)
    {
        if (pSipAlgTimer->pHashKey->pCallID != SIP_NULL)
        {

            /* Allocate memory for NAT hash key */
            pIPKey = (SipAlgIPHashKey *)
                sip_memget (DECODE_MEM_ID, sizeof (SipAlgIPHashKey), &err);
            if (pIPKey == SIP_NULL)
            {
                return;
            }
            pIPKey->pCallID = sip_strdup (pSipAlgTimer->pHashKey->pCallID, 0);
            pIPKey->pVersionId =
                sip_strdup (pSipAlgTimer->pHashKey->pVersionId, 0);
            pIPKey->isRequest = pSipAlgTimer->pHashKey->isRequest;
            /* Fetch the entry from the IP hash table */
            pIPHashElement = (SipAlgIPHashElement *)
                sip_hashFetch (&dGlbIPHash, (SIP_Pvoid) pIPKey);

            if (pIPHashElement != SIP_NULL)
            {
                sip_hashRelease (&dGlbIPHash, (SIP_Pvoid) pIPKey);

                if (SipFail == sip_hashRemove (&dGlbIPHash, pIPKey))
                {
                    sip_error (SIP_Minor,
                               "Could not remove the IP hash element.");
                    sip_memfree (0,
                                 (SIP_Pvoid) & (pSipAlgTimer->
                                                pHashKey->pCallID), &err);
                    sip_memfree (0,
                                 (SIP_Pvoid) & (pSipAlgTimer->
                                                pHashKey->pVersionId), &err);
                    pTemp = pSipAlgTimer->pHashKey;
                    sip_memfree (0, (SIP_Pvoid *) & (pTemp), &err);
                    pSipAlgTimer->pHashKey = NULL;
                    pTemp = pSipAlgTimer;
                    sip_memfree (0, (SIP_Pvoid *) & (pTemp), &err);
                    pSipAlgTimer = NULL;
                    if (pNatHashElement != NULL)
                    {
                        pNatHashElement->pSipAlgTimerInfo = SIP_NULL;
                    }
                    sip_memfree (0, (SIP_Pvoid) & (pIPKey->pCallID), &err);
                    sip_memfree (0, (SIP_Pvoid) & (pIPKey->pVersionId), &err);
                    pTemp = pIPKey;
                    sip_memfree (0, (SIP_Pvoid *) & (pTemp), &err);
                    pIPKey = NULL;
                    return;
                }
                pIPHashElement->pSipAlgTimerInfo = SIP_NULL;
                /* Fix for memory leak - lakshmi */
                pTemp = pIPHashElement;
                sip_memfree (0, (SIP_Pvoid *) & (pTemp), &err);
                pIPHashElement = NULL;
            }
#ifdef SIPALG_UT_FLAG
            /* SIPALG - 2.1 */
            sipAlgIPHashPrint (&dGlbIPHash);
#endif
            sip_memfree (0, (SIP_Pvoid) & (pIPKey->pCallID), &err);
            sip_memfree (0, (SIP_Pvoid) & (pIPKey->pVersionId), &err);
            pTemp = pIPKey;
            sip_memfree (0, (SIP_Pvoid *) & (pTemp), &err);
            pIPKey = NULL;
        }
        return;
    }
    /* Fetch the entry from the NAT hash table */
    pNatHashElement = (SipAlgNatHashElement *)
        sip_hashFetch (&dGlbNatHash, (SIP_Pvoid) pSipAlgTimer->pHashKey);

    if (pNatHashElement != SIP_NULL)
    {
#ifdef SIPALG_UT_FLAG
        PRINTF ("\n\n NAT HASH Entries at the beginning of "
                "timer exp for port: %d \n\n", pNatHashElement->originalPort);
        sipAlgNatHashPrint (&dGlbNatHash);
        /* SIPALG - 2.1 */
        sipAlgIPHashPrint (&dGlbIPHash);

        PRINTF ("\n\n Media HASH Entries at the beginning of"
                "timer exp for port: %d \n\n", pNatHashElement->originalPort);
        sipAlgMediaHashPrint (&dGlbMediaHash);
#endif

        if (pNatHashElement->isBindingCreated == SipTrue)
        {
            /* Allocate memory for NAT Entry Info */
            pNatEntryInfo = (tNatEntryInfo *)
                sip_memget (DECODE_MEM_ID, sizeof (tNatEntryInfo), &err);
            if (pNatEntryInfo == SIP_NULL)
            {
                pTemp = pIPKey;
                sip_memfree (0, (SIP_Pvoid *) & (pTemp), &err);
                pIPKey = NULL;
                return;
            }
            sip_memset ((SIP_Pvoid) (pNatEntryInfo), 0, sizeof (tNatEntryInfo));
/*DeReg start*/
            if (dGlbSipStatus == en_sipDisable)
            {
                if (pSipAlgTimer->pDeRegAOR)
                {
                    sipAlgDeleteDeRegOnParExp ((SIP_S8bit *) pSipAlgTimer->
                                               pDeRegAOR,
                                               pNatHashElement->originalIP,
                                               pNatHashElement->originalPort);
                }
#ifdef SIPALG_UT_FLAG
                PRINTF ("\n\n--=Address of algtimer->pDeRegAOR in  "
                        "sipAlgTimerExpCbkFunc %p\n", &pSipAlgTimer->pDeRegAOR);
#endif
                sip_memfree (0, (SIP_Pvoid) & (pSipAlgTimer->pDeRegAOR), &err);
                pSipAlgTimer->pDeRegAOR = NULL;
            }
/*DeReg end*/

            pNatEntryInfo->u4SrcIP = OSIX_NTOHL (pNatHashElement->originalIP);
            pNatEntryInfo->u2SrcPort = pNatHashElement->originalPort;
            pNatEntryInfo->u4Direction = SipPktIn;
            pNatEntryInfo->u2Protocol = pNatHashElement->protocol;
            pNatEntryInfo->au1Padding[0] = 0;
            pNatEntryInfo->au1Padding[1] = 0;
            pNatEntryInfo->au1Padding[2] = 0;
            pNatEntryInfo->u1TimerFlag = NAT_SIP_ACTIVE;
            /* Outside IP/Port are kept as unknown */
            pNatEntryInfo->u4DestIP = 0;
            pNatEntryInfo->u2DestPort = 0;

            /*lakshmi added - NAT API Enhancements - START */
            if (pNatHashElement->isMediaElement == SipTrue)
            {
                pNatEntryInfo->u1BindingType = en_media;
                pNatEntryInfo->u2NumBindings = 2;
            }
            else
            {
                pNatEntryInfo->u1BindingType = en_signaling;
                pNatEntryInfo->u2NumBindings = 1;
            }
            pNatEntryInfo->u1Parity = (UINT1) pNatHashElement->parity;
            /* NAT API Enhancements - END */

            /* Irrespective of signaling or media, get the NAT port. */
            (void) sip_listSizeOf (pNatHashElement->pDestInfoList,
                                   &listSizeDest, &err);

            /* The list size will always be more than 0. */
            if (listSizeDest != 0)
            {
                dDestIPPortFlag = 1;
                listIter = 0;
                (void) sip_listGetAt (pNatHashElement->pDestInfoList,
                                      listIter, (SIP_Pvoid *) & pIPPort, &err);
                pDestIPPort = (SipAlgIpPortPair *) pIPPort;
                /* Update the NAT port */
                pNatEntryInfo->u2NATPort = pDestIPPort->mappedPort;
                pNatEntryInfo->u4NATIP = OSIX_NTOHL (pDestIPPort->mappedIP);
            }
            /* Cookie is passed with value 0 - Sync API */
            CFA_UNLOCK ();
#ifdef SDF_TIMESTAMP
            if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
            {

                Sdf_fn_printTimeStamp
                    ("sipAlgPerformTrans:Pre-AricentNATDeltePortMapping_ipc");
            }
#endif
            pNatResp = (tNatAsyncResponse *)
                sip_memget (DECODE_MEM_ID, sizeof (tNatAsyncResponse), &err);
            if (pNatResp == SIP_NULL)
            {
                sip_error (SIP_Minor, "Could not delete NAT binding.");
                sip_hashRelease (&dGlbNatHash,
                                 (SIP_Pvoid) pSipAlgTimer->pHashKey);
                pTemp = pNatEntryInfo;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pNatEntryInfo = NULL;
                /*  sip_memfree(DECODE_MEM_ID,(SIP_Pvoid *)&pDestIPPort,&err); */
                return;
            }
            NatSipAlgDeletePortMapping (pNatEntryInfo, 0, pNatResp);
#ifdef SDF_TIMESTAMP
            if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
            {

                Sdf_fn_printTimeStamp
                    ("sipAlgPerformTrans:Post-AricentNATDeltePortMapping_ipc");
            }
#endif
            CFA_LOCK ();
            if (pNatResp->u4Status != NAT_SIP_SUCCESS)
            {
                sip_error (SIP_Minor, "Could not delete NAT binding:");
                sip_error (SIP_Minor,
                           pSipAlgNATAPIErrorCode[pNatResp->u4ErrorCode]);
                sip_hashRelease (&dGlbNatHash,
                                 (SIP_Pvoid) pSipAlgTimer->pHashKey);
                /* free(pNatResp); */
                pTemp = pNatResp;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pNatResp = NULL;
                pTemp = pNatEntryInfo;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pNatEntryInfo = NULL;
                /*  sip_memfree(DECODE_MEM_ID,(SIP_Pvoid *)&pDestIPPort,&err); */
                return;
            }
            /* free(pNatResp); */
            pTemp = pNatResp;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
            pNatResp = NULL;
            /*handling for deletion of dynamic entry for signalling.
               If partial entry for signalling gets deleted as part 
               sipalg timer expiry handling then, it is our responsibilty
               now to explicitly call for the deletion of dynamic entry
               corresponding to it. */
            if (pNatHashElement->isMediaElement == SipFalse)
            {
                if (listSizeDest == 0)
                {
                    /*this condition shall not happen */
                    dIsDynEntryExist = SipFalse;
                }
                else if (listSizeDest == 1)
                {
                    if ((pDestIPPort->destPort == 0)
                        || (pDestIPPort->destIP == 0))
                    {
                        dIsDynEntryExist = SipFalse;
                    }
                }
                if (dIsDynEntryExist == SipTrue)
                {
                    /*Use the same NatEntryInfo which was used for partial 
                       entry deletion AricentNATDeletePortMapping_ipc 
                       but fill the DestIP and port also for deletion of 
                       dynamic entry AricentNATDeleteDynamicEntry_ipc */

                    pNatEntryInfo->u4DestIP = OSIX_NTOHL (pDestIPPort->destIP);
                    pNatEntryInfo->u2DestPort = pDestIPPort->destPort;

                    CFA_UNLOCK ();
#ifdef SIPALG_UT_FLAG
                    PRINTF
                        ("\n\ncalling AricentNATDeleteDynamicEntry_ipc for the tuple: "
                         "%x,%u,%x,%u,%x,%u\n\n", pNatEntryInfo->u4SrcIP,
                         pNatEntryInfo->u2SrcPort, pNatEntryInfo->u4DestIP,
                         pNatEntryInfo->u2DestPort, pNatEntryInfo->u4NATIP,
                         pNatEntryInfo->u2NATPort);
#endif
                    pNatResp = (tNatAsyncResponse *)
                        sip_memget (DECODE_MEM_ID, sizeof (tNatAsyncResponse),
                                    &err);
                    if (pNatResp == SIP_NULL)
                    {
                        sip_error (SIP_Minor, "Could not delete dynamic entry"
                                   " for signalling on partialtimer expiry.");
                        sip_hashRelease (&dGlbNatHash,
                                         (SIP_Pvoid) pSipAlgTimer->pHashKey);
                        pTemp = pNatEntryInfo;
                        sip_memfree (DECODE_MEM_ID,
                                     (SIP_Pvoid *) & pTemp, &err);
                        pNatEntryInfo = NULL;
                        return;
                    }
                    NatSipAlgDeleteDynamicEntry (pNatEntryInfo, 0, pNatResp);
                    CFA_LOCK ();
                    if (pNatResp->u4Status != NAT_SIP_SUCCESS)
                    {
                        sip_error (SIP_Minor, "Could not delete dynamic entry"
                                   " for signalling on partial timer expiry");
                        sip_error (SIP_Minor,
                                   pSipAlgNATAPIErrorCode[pNatResp->
                                                          u4ErrorCode]);
                        sip_hashRelease (&dGlbNatHash,
                                         (SIP_Pvoid) pSipAlgTimer->pHashKey);
                        /* free(pNatResp); */
                        pTemp = pNatResp;
                        sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp,
                                     &err);
                        pNatResp = NULL;
                        pTemp = pNatEntryInfo;
                        sip_memfree (DECODE_MEM_ID,
                                     (SIP_Pvoid *) & pTemp, &err);
                        pNatEntryInfo = NULL;
                        return;
                    }
                    pTemp = pNatResp;
                    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                    pNatResp = NULL;
                }
            }
            /*Free the NatEntryInfo */
            pTemp = pNatEntryInfo;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
            pNatEntryInfo = NULL;
            (void) sip_trace (SIP_Detailed, SIP_Init,
                              (SIP_S8bit *)
                              "SIP ALG TRACE: SIP ALG timer expired. Deleted"
                              " NAT binding");

        }
        else
        {
            /* Allocate memory for NAT Entry Info */
            pPinhole = (tPinholeInfo *)
                sip_memget (DECODE_MEM_ID, sizeof (tPinholeInfo), &err);
            if (pPinhole == SIP_NULL)
            {
                pTemp = pIPKey;
                sip_memfree (0, (SIP_Pvoid *) & (pTemp), &err);
                pIPKey = NULL;
                return;
            }

            /* Pinhole is created to allow traffic to flow from 
               inside network to outside. So the destination
               IP and port was fixed. */
            pPinhole->u4SrcIP = 0;
            pPinhole->u2SrcPort = 0;
            pPinhole->u4DestIP = OSIX_NTOHL (pNatHashElement->originalIP);
            pPinhole->u2DestPort = pNatHashElement->originalPort;
            pPinhole->u2Protocol = NAT_SIP_UDP;
            pPinhole->u2Direction = NAT_OUTBOUND;
            pPinhole->u1TimerFlag = NAT_SIP_ACTIVE;
            /* NAT API--rasheed 
               pPinhole->au1Padding[0] = 0;
               pPinhole->au1Padding[1] = 0;
               pPinhole->au1Padding[2] = 0;
             */
/*          lakshmi added - for NAT API Enhancements - START */
            if (pNatHashElement->isMediaElement == SipTrue)
            {
                pPinhole->u2NumPinholes = 2;
            }
            else
            {
                pPinhole->u2NumPinholes = 1;
            }
            /* NAT API Enhancements - END */
            /* Cookie is passed with value 0 - Sync API  */
            CFA_UNLOCK ();
#ifdef SDF_TIMESTAMP
            if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
            {

                Sdf_fn_printTimeStamp
                    ("sipAlgPerformTrans:Pre-AricentNATClosePinHole_ipc");
            }
#endif
            pNatResp = (tNatAsyncResponse *)
                sip_memget (DECODE_MEM_ID, sizeof (tNatAsyncResponse), &err);
            if (pNatResp == SIP_NULL)
            {
                sip_error (SIP_Minor, "Could not close pinholes.");
                sip_hashRelease (&dGlbNatHash,
                                 (SIP_Pvoid) pSipAlgTimer->pHashKey);
                pTemp = pPinhole;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pPinhole = NULL;
                return;
            }
            NatSipAlgClosePinhole (pPinhole, 0, pNatResp);
#ifdef SDF_TIMESTAMP
            if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
            {

                Sdf_fn_printTimeStamp
                    ("sipAlgPerformTrans:Post-AricentNATClosePinHole_ipc");
            }
#endif
            CFA_LOCK ();
            if (pNatResp->u4Status != NAT_SIP_SUCCESS)
            {
                sip_error (SIP_Minor, "Could not close pinholes.");
                sip_error (SIP_Minor,
                           pSipAlgNATAPIErrorCode[pNatResp->u4ErrorCode]);
                sip_hashRelease (&dGlbNatHash,
                                 (SIP_Pvoid) pSipAlgTimer->pHashKey);
                /* free(pNatResp); */
                pTemp = pNatResp;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pNatResp = NULL;
                pTemp = pPinhole;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pPinhole = NULL;

                return;
            }
            /* free(pNatResp); */
            pTemp = pNatResp;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
            pNatResp = NULL;

            pTemp = pPinhole;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
            pPinhole = NULL;
            (void) sip_trace (SIP_Detailed, SIP_Init,
                              (SIP_S8bit *)
                              "SIP ALG TRACE: SIP ALG timer expired. Closed "
                              "firewall pinhole");
        }
        /* For all signaling entries, clear both the keys */
        if (pNatHashElement->isMediaElement == SipFalse)
        {
            SipAlgNatHashKey    key;
            SipAlgNatHashKey    transKey;

            /* Delete the NAT hash element for the private IP/Port */
            key.ipAddress = pNatHashElement->originalIP;
            key.port = pNatHashElement->originalPort;
            key.ifIndex = pNatHashElement->ifIndex;
            key.direction = SipPktOut;

            /* Delete the NAT hash element for the translated IP/Port */
/*klocwork change*/
            if (dDestIPPortFlag == 1)
            {
                transKey.ipAddress = pDestIPPort->mappedIP;
                transKey.port = pDestIPPort->mappedPort;
                transKey.ifIndex = pNatHashElement->ifIndex;
                transKey.direction = SipPktIn;
            }

            sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pSipAlgTimer->pHashKey);
/*klocwork change */
            if (dDestIPPortFlag == 1)
            {
                if (SipFail == sip_hashRemove (&dGlbNatHash, &key))
                {
                    sip_error (SIP_Minor, "Could not remove the hash element.");
                    /*  sip_memfree(DECODE_MEM_ID,(SIP_Pvoid *)&pDestIPPort,&err); */
                    return;
                }
                if (SipFail == sip_hashRemove (&dGlbNatHash, &transKey))
                {
                    sip_error (SIP_Minor, "Could not remove the hash element.");
                    /*  sip_memfree(DECODE_MEM_ID,(SIP_Pvoid *)&pDestIPPort,&err); */
                    return;
                }
            }
        }
        else
        {
            /* First get the size of pDestInfoList */
            (void) sip_listSizeOf (pNatHashElement->pDestInfoList,
                                   &listSizeDest, &err);

            /* In case of media expiry, if there is no dynamic entry created,
               for the list of on-going calls, clear entry from Media hash
               Then clear the entry from NAT hash table. 

               In case of binding, there will be additional check as 
               listSizeDest will always be 1 */
            if (((pNatHashElement->isBindingCreated == SipFalse)
                 && (listSizeDest == 0)) ||
                ((pNatHashElement->isBindingCreated == SipTrue)
                 && (listSizeDest <= 1)))
            {
                /* Delete the NAT hash element */
                SipAlgNatHashKey    key;
                SipAlgNatHashKey    transKey;
                if (pNatHashElement->isBindingCreated == SipTrue)
                {
                    /* One binding will always be there for binding. 
                       Check if there is any dest IP/Port corresponding 
                       to that. only mapped IP/port will be there in the 
                       list if no RTP flowed. */
                    if (listSizeDest == 1)
                    {
                        (void) sip_listGetAt (pNatHashElement->pDestInfoList,
                                              0, (SIP_Pvoid *) & pIPPort, &err);

                        pDestIPPort = (SipAlgIpPortPair *) pIPPort;
                        if (pDestIPPort->destIP != 0)
                        {
                            /* This indicates that there exists a dynamic entry
                               As dynamic entry exists, media element will not 
                               be deleted So that on receiving a BYE, the 
                               dynamic entries can be deleted. Both media hash 
                               and NAT hash is not deleted. */
                            SIPDEBUG ((SIP_S8bit *)
                                      "Exiting sipAlgTimerExpCbkFunc");
                            sip_hashRelease (&dGlbNatHash,
                                             (SIP_Pvoid) pSipAlgTimer->
                                             pHashKey);
                            /* here the timer should be freed as it is expired */
                            pTemp = pSipAlgTimer->pHashKey;
                            sip_memfree (0, (SIP_Pvoid *) & (pTemp), &err);
                            pSipAlgTimer->pHashKey = NULL;
                            pTemp = pSipAlgTimer;
                            sip_memfree (0, (SIP_Pvoid *) & (pTemp), &err);
                            pSipAlgTimer = NULL;
                            pNatHashElement->pSipAlgTimerInfo = SIP_NULL;

                            /*Once partial table entry is deleted due to 
                               sipalgtimer expiry, set this flag to true, to 
                               avoid calling AricentNATDeletePortMapping_ipc 
                               again on receiving Bye */
                            pNatHashElement->isNatPartialEntryDeleted = SipTrue;

                            return;
                        }
                    }
                }
                /* For the incoming SDP, there was no RTP flow. */

                (void) sip_listSizeOf (pNatHashElement->pCallInfoList,
                                       &listSizeCall, &err);
                for (listIterCall = 0; listIterCall < listSizeCall;
                     listIterCall++)
                {
                    SipAlgIpPort        ipPortKey;
                    pTemp = pMediaKey;
                    (void) sip_listGetAt (pNatHashElement->pCallInfoList,
                                          listIterCall,
                                          (SIP_Pvoid *) & pTemp, &err);

                    ipPortKey.ipAddress = pNatHashElement->originalIP;
                    ipPortKey.port = pNatHashElement->originalPort;
                    if (sipAlgDeleteMediaElement
                        ((SipAlgMediaKey *) pMediaKey, &ipPortKey) == SipFail)
                    {
                        sip_error (SIP_Minor, "Could not delete entry "
                                   "from media hash");
                        sip_hashRelease (&dGlbNatHash,
                                         (SIP_Pvoid) pSipAlgTimer->pHashKey);
                        /* here the timer should be freed as it is expired */
                        pTemp = pSipAlgTimer->pHashKey;
                        sip_memfree (0, (SIP_Pvoid *) & (pTemp), &err);
                        pSipAlgTimer->pHashKey = NULL;
                        pTemp = pSipAlgTimer;
                        sip_memfree (0, (SIP_Pvoid *) & (pTemp), &err);
                        pSipAlgTimer = NULL;
                        pNatHashElement->pSipAlgTimerInfo = SIP_NULL;
                        return;
                    }
                }

                key.ipAddress = pNatHashElement->originalIP;
                key.port = pNatHashElement->originalPort;
                key.direction = SipPktIn;
                key.ifIndex = pNatHashElement->ifIndex;
                /* For intra-site call get the mapped ip, port for this 
                   nathash entry. As part of fix for intra-site call 
                   handling new entry was created in nat hash table for 
                   outgoing media  with mapped ip,port as key. 
                   Hence delete that entry from nat hash table */
                if (pNatHashElement->isBindingCreated == SipTrue)
                {
                    key.direction = SipPktOut;
                    if (listSizeDest == 1)
                    {
                        (void) sip_listGetAt (pNatHashElement->pDestInfoList,
                                              0, (SIP_Pvoid *) & pIPPort, &err);

                        pDestIPPort = (SipAlgIpPortPair *) pIPPort;
                        if (pDestIPPort->destIP == 0)
                        {
                            if (pNatHashElement->isNatEnabled == SipTrue)
                            {
                                transKey.ipAddress = pDestIPPort->mappedIP;
                                transKey.port = pDestIPPort->mappedPort;
                            }
                            else
                            {
                                transKey.ipAddress =
                                    pNatHashElement->originalIP;
                                transKey.port = pNatHashElement->originalPort;
                            }
                            transKey.direction = SipPktIn;
                            if (SipFail ==
                                sip_hashRemove (&dGlbNatHash, &transKey))
                            {
                                sip_error (SIP_Minor, "Could not remove"
                                           " the hash element.");
                                sip_hashRelease (&dGlbNatHash,
                                                 (SIP_Pvoid) pSipAlgTimer->
                                                 pHashKey);
                                return;
                            }
                        }
                    }
                }
                sip_hashRelease (&dGlbNatHash,
                                 (SIP_Pvoid) pSipAlgTimer->pHashKey);
                if (SipFail == sip_hashRemove (&dGlbNatHash, &key))
                {
                    sip_error (SIP_Minor, "Could not remove the hash element.");
                    return;
                }
            }
            else if ((pNatHashElement->isBindingCreated == SipFalse)
                     && (listSizeDest >= 1))
            {
                /* This indicates that for a pinhole, RTP flowed. 
                   So hash remove should not be done. But here the 
                   timer should be freed as it is expired */
                sip_hashRelease (&dGlbNatHash,
                                 (SIP_Pvoid) pSipAlgTimer->pHashKey);
                pTemp = pSipAlgTimer->pHashKey;
                sip_memfree (0, (SIP_Pvoid *) & (pTemp), &err);
                pSipAlgTimer->pHashKey = NULL;
                pTemp = pSipAlgTimer;
                sip_memfree (0, (SIP_Pvoid *) & (pTemp), &err);
                pSipAlgTimer = NULL;
                pNatHashElement->pSipAlgTimerInfo = SIP_NULL;

                /*Once partial table entry is deleted due to sipalgtimer
                   expiry, set this flag to true, to avoid calling 
                   AricentNATClosePinHole_ipc again on receiving Bye */
                pNatHashElement->isNatPartialEntryDeleted = SipTrue;

            }
            /* Invite is sent and no response received. Look into the 
               list of destintion IP address and Port (which will be 
               created when RTP flows). In case there is any dynamic 
               entry corresponding to this flow don't take any action. */
        }
#ifdef SIPALG_UT_FLAG
        PRINTF ("\n\nNAT HASH Entries at the end of timer exp "
                "for above port: \n\n");
        sipAlgNatHashPrint (&dGlbNatHash);
        /* SIPALG - 2.1 */
        sipAlgIPHashPrint (&dGlbIPHash);

        sipAlgMediaHashPrint (&dGlbMediaHash);
#endif
    }
    else
    {
        /* This part should never get executed */
        sip_error (SIP_Minor, "There should be an entry in NAT hash to handle"
                   " SIP ALG timer expiry.");
    }
    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgTimerExpCbkFunc");
}

/****************************************************************************
*    FUNCTION:     sipAlgUpdateMediaInfo
*
*    DESCRIPTION: 
*              This is the function which will update the media
*              information in the ALG Hash Table as well as NAT Hash
*              Table maintained in ALG. It will also invoke the NAT API 
*              to create the bindings and the pinholes for it.
****************************************************************************/
SipBool
sipAlgUpdateMediaInfo (SipAlgMediaKey * pDialogParams,
                       SipNatInfo ** ppNatInfo,
                       en_SipAValueInSdp a_line_attribute)
{
    SIPDEBUG ((SIP_S8bit *) "Inside sipAlgUpdateMediaInfo");
    if ((SIP_NULL == pDialogParams) || (SIP_NULL == ppNatInfo) ||
        (SIP_NULL == *ppNatInfo))
    {
        sip_error (SIP_Minor,
                   "SIP-ERROR: sipAlgUpdateMediaInfo: Invalid Parameters \n");
        return SipFail;
    }
    /* Update the entries in the  Media Hash Table */
    if (SipFail == sipAlgUpdateMediaTables (pDialogParams, *ppNatInfo,
                                            a_line_attribute))
    {
        sip_error (SIP_Minor,
                   "SIP-ERROR: sipAlgUpdateMediaInfo: sipAlgUpdateMediaTables failed\n");
        return SipFail;
    }
    if (sipAlgPerformTranslation (*ppNatInfo, en_udp,
                                  SipTrue, pDialogParams,
                                  (*ppNatInfo)->ifIndex) == SipFail)
    {
        SipAlgIpPort        ipPortKey;
        sip_error (SIP_Minor,
                   " In sipAlgUpdateMediaInfo:"
                   "sipAlgPerformTranslation failed\n");
        /* take care of clearing media hash element which got added 
           as part of sipAlgUpdateMediaTables */

        if ((*ppNatInfo)->direction == SipPktIn)
        {
            ipPortKey.ipAddress = (*ppNatInfo)->outsideIP;
            ipPortKey.port = (*ppNatInfo)->outsidePort;
        }
        else if ((*ppNatInfo)->direction == SipPktOut)
        {
            ipPortKey.ipAddress = (*ppNatInfo)->privateIP;
            ipPortKey.port = (*ppNatInfo)->privatePort;
        }
        if (sipAlgDeleteMediaElement (pDialogParams, &ipPortKey) == SipFail)
        {
            sip_error (SIP_Minor, "Could not delete entry from media hash");
        }
        return SipFail;
    }

    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgUpdateMediaInfo");
    return SipSuccess;
}

/****************************************************************************
*    FUNCTION:     sipAlgUpdateMediaTables
*
*    DESCRIPTION: 
*              This is the function which will update the media
*              information in the ALG Hash Table
****************************************************************************/
SipBool
sipAlgUpdateMediaTables (SipAlgMediaKey * pDialogParams,
                         SipNatInfo * pNatInfo,
                         en_SipAValueInSdp a_line_attribute)
{
    /* This pointer is used to avoid strict-aliasing warning */
    SIP_Pvoid           pTemp = SIP_NULL;
    SipAlgMediaKey     *pPartialKey = SIP_NULL;
    SipAlgMediaKey     *pTempKey = SIP_NULL;
    SipAlgNatHashKey    dKey;

    SipAlgMediaElement *pMediaElement = SIP_NULL;

    SipMediaHashElement *pMediaHashEntry = SIP_NULL;
    SipMediaHashElement *pNewMediaHashEntry = SIP_NULL;

    SIP_U32bit          dSize = 0;
    SIP_U32bit          u4Index = 0;
    SipError            dError = E_NO_ERROR;
    en_SipBoolean       isMatched = SipFalse;

    SIPDEBUG ((SIP_S8bit *) "Inside sipAlgUpdateMediaTables");
    if ((SIP_NULL == pDialogParams) || (SIP_NULL == pNatInfo))
    {
        sip_error (SIP_Minor,
                   "SIP-ERROR: sipAlgUpdateMediaTables: Invalid arguments\n");
        return SipFail;
    }
    (void) a_line_attribute;

    /* Fetch the SessionInfo Entry from the Media Hash Table */
    pNewMediaHashEntry =
        (SipMediaHashElement *) sip_hashFetch (&dGlbMediaHash, pDialogParams);

    if (SIP_NULL == pNewMediaHashEntry)
    {
        pPartialKey = (SipAlgMediaKey *) sip_memget (0,
                                                     sizeof (SipAlgMediaKey),
                                                     &dError);
        if (pPartialKey == SIP_NULL)
        {
            return SipFail;
        }
        pPartialKey->pFromTag = pDialogParams->pFromTag;
        pPartialKey->pCallID = pDialogParams->pCallID;
        pPartialKey->pToTag = SIP_NULL;

        /* Fetch the Media Element from the Media Hash Table using the partial
           Key */
        pMediaHashEntry =
            (SipMediaHashElement *) sip_hashFetch (&dGlbMediaHash, pPartialKey);
        if (SIP_NULL == pMediaHashEntry)
        {
            /* It means this is the new INVITE. Insert it into the Media Hash
               table. */
            if (sipAlgInitMediaHashElement (&pMediaHashEntry) == SipFail)
            {
                sip_error (SIP_Minor, "SIP-ERROR: sipAlgUpdateMediaTables: "
                           "sipAlgInitMediaHashElement failed\n");
                pTemp = pPartialKey;
                sip_memfree (0, (SIP_Pvoid *) & pTemp, &dError);
                pPartialKey = NULL;
                return SipFail;
            }
            /* Form the partial Key using From Tag and Call-ID */

            pPartialKey->pFromTag = sip_strdup (pDialogParams->pFromTag, 0);
            pPartialKey->pCallID = sip_strdup (pDialogParams->pCallID, 0);

            /* to handle the case of ReInvite; after the sipalg timer 
               expired for media */
            if (pDialogParams->pToTag != SIP_NULL)
                pPartialKey->pToTag = sip_strdup (pDialogParams->pToTag, 0);
            else
                pPartialKey->pToTag = SIP_NULL;

            if (SipFail == sipAlgAddMediaElementInMediaHash (pNatInfo,
                                                             pMediaHashEntry,
                                                             pPartialKey,
                                                             SipTrue))
            {
                sip_error (SIP_Minor, "SIP-ERROR: sipAlgUpdateMediaTables: "
                           "sipAlgAddMediaElementInMediaHash failed");

                sipAlgFreeDialogParam (&pPartialKey);
                sipAlgRemoveEntryFromMediaHash ((void *) pMediaHashEntry);
                return SipFail;
            }
        }                        /* End of if block which fetch Media Info using partial Key. */
        else
        {
            /* This is handling part for first media line in SIP response 
               for all the forked responses. The dialog param will be having
               a ToTag and so new hash entry will be created. */
            if (sipAlgInitMediaHashElement (&pNewMediaHashEntry) == SipFail)
            {
                sip_error (SIP_Minor, "SIP-ERROR: sipAlgUpdateMediaTables: "
                           "sipAlgInitMediaHashElement failed\n");
                sip_hashRelease (&dGlbMediaHash, pPartialKey);
                return SipFail;
            }

            /* Find the list of media lines in the hash entry created by
               'From Tag and Call Id'. Update the ref count of those 
               entries. */
            if ((SipSuccess == sip_listSizeOf (pMediaHashEntry->
                                               pMediaInfoList, &dSize, &dError))
                && (0 != dSize))
            {
                for (u4Index = 0; u4Index < dSize; u4Index++)
                {
                    sip_listGetAt (pMediaHashEntry->pMediaInfoList,
                                   u4Index, (SIP_Pvoid *) & pTemp, &dError);
                    pMediaElement = (SipAlgMediaElement *) pTemp;
                    /* Increment the RefCount. */
                    pMediaElement->refCount++;

                    dKey.ipAddress = pMediaElement->ipAddress;
                    dKey.port = pMediaElement->port;
                    dKey.direction = pMediaElement->direction;
                    dKey.ifIndex = pMediaElement->ifIndex;

                    /* Update the dialog key in NAT Hash Table. Basically
                       new dialog param are added to the m-line that was
                       there in initial INVITE. */
                    sipAlgUpdateMediaKeyInNatHash (&dKey, pDialogParams);
                }
            }                    /* End of if block which compare num of pSessionInfoEntry List */
            /* do hash release  */
            sip_hashRelease (&dGlbMediaHash, pPartialKey);
            /*Fix for memory leak - lakshmi */
            pTemp = pPartialKey;
            sip_memfree (0, (SIP_Pvoid *) & pTemp, &dError);
            pPartialKey = NULL;
            /* now form the new key */

            pTempKey = (SipAlgMediaKey *) sip_memget (0,
                                                      sizeof (SipAlgMediaKey),
                                                      &dError);
            if (pTempKey == SIP_NULL)
            {
                sipAlgRemoveEntryFromMediaHash ((void *) pNewMediaHashEntry);
                return SipFail;
            }
            pTempKey->pFromTag = sip_strdup (pDialogParams->pFromTag, 0);
            pTempKey->pCallID = sip_strdup (pDialogParams->pCallID, 0);
            pTempKey->pToTag = sip_strdup (pDialogParams->pToTag, 0);

            if (SipFail == sipAlgAddMediaElementInMediaHash (pNatInfo,
                                                             pNewMediaHashEntry,
                                                             pTempKey, SipTrue))
            {
                sip_error (SIP_Minor, "SIP-ERROR: sipAlgUpdateMediaTables: "
                           "sipAlgAddMediaElementInMediaHash failed\n");
                sipAlgFreeDialogParam (&pTempKey);
                sipAlgRemoveEntryFromMediaHash ((void *) pNewMediaHashEntry);
                return SipFail;
            }
        }                        /* End of else block which fetch Media Info using partial Key. */
    }                            /* End of if check which get data from Media hash using  */
    else
    {
        /* From, To and CallID

           This is the case for second media line in the response. The
           first media line of the response would have created a new 
           hash entry with dialog param having 'To Tag'.

           This is the case of RE-INVITE also and any mid-dialog case. */
        if ((SipSuccess == sip_listSizeOf (pNewMediaHashEntry->
                                           pMediaInfoList, &dSize, &dError)) &&
            (0 != dSize))
        {
            /* Compare the private IP, private port and outside IP
               outside port fetched from the pNatInfo (ANY, ANY)
               with the value stored in the Media Hash Table. */
            isMatched = SipFalse;
            for (u4Index = 0; u4Index < dSize; u4Index++)
            {
                sip_listGetAt (pNewMediaHashEntry->pMediaInfoList,
                               u4Index, (SIP_Pvoid *) & pTemp, &dError);
                pMediaElement = (SipAlgMediaElement *) pTemp;
                if ((pMediaElement->ipAddress == pNatInfo->privateIP) &&
                    (pMediaElement->port == pNatInfo->privatePort))
                {
                    /* This is the case for handing the SDP from private 
                       to public */
                    isMatched = SipTrue;
                    break;
                }
                else if ((pMediaElement->ipAddress == pNatInfo->outsideIP) &&
                         (pMediaElement->port == pNatInfo->outsidePort))
                {
                    /* This is the case for handing the SDP from public
                       to private */
                    isMatched = SipTrue;
                    break;
                }
                else if (sipAlgIsWANIPAddress (pNatInfo->mappedIP) == SipTrue)
                {
                    /* This is the case for handing the SDP in intrasite call */
                    isMatched = SipTrue;
                    break;
                }
            }
            /* If match is not found, this happens when re-invite is received
               from the caller.
               Comparision of re-invite's media element with full key media 
               element will not match, because for the caller's media partial
               key media entry will be created
               hence try to compare re-invite's media with partial key entry */
            if (SipFalse == isMatched)
            {
                SipAlgMediaKey     *pTempPartialKey = SIP_NULL;
                SipMediaHashElement *pPartialMediaHashEntry = SIP_NULL;
                SipError            err;

                pTempPartialKey = (SipAlgMediaKey *) sip_memget (0,
                                                                 sizeof
                                                                 (SipAlgMediaKey),
                                                                 &err);
                if (pTempPartialKey == SIP_NULL)
                {
                    return SipFail;
                }
                pTempPartialKey->pFromTag =
                    sip_strdup (pDialogParams->pFromTag, 0);
                pTempPartialKey->pCallID =
                    sip_strdup (pDialogParams->pCallID, 0);
                pTempPartialKey->pToTag = SIP_NULL;

                pPartialMediaHashEntry =
                    (SipMediaHashElement *) sip_hashFetch (&dGlbMediaHash,
                                                           pTempPartialKey);
                /* If partial key media entry couldnt be found, then 
                   swap the to-tag and try to fetch,
                   Incase re-invite is sent by callee then his from-tag 
                   to-tag will be swapped  (only if its not null -DUMP fixed) */
                if ((pPartialMediaHashEntry == SIP_NULL)
                    && (pDialogParams->pToTag != SIP_NULL))
                {
                    /* memory leak fix start */
                    if (pTempPartialKey->pFromTag != SIP_NULL)
                    {
                        sip_memfree (0,
                                     (SIP_Pvoid) &
                                     pTempPartialKey->pFromTag, &dError);
                    }
                    /* memory leak fix end */
                    pTempPartialKey->pFromTag =
                        sip_strdup (pDialogParams->pToTag, 0);
                    pPartialMediaHashEntry =
                        (SipMediaHashElement *) sip_hashFetch (&dGlbMediaHash,
                                                               pTempPartialKey);
                }
                if (pPartialMediaHashEntry != SIP_NULL)
                {
                    /* if partial key entry is found then compare the 
                       re-invite's media element with it */
                    if ((SipSuccess ==
                         sip_listSizeOf (pPartialMediaHashEntry->pMediaInfoList,
                                         &dSize, &dError)) && (0 != dSize))
                        for (u4Index = 0; u4Index < dSize; u4Index++)
                        {
                            /* compare every media hash entry with the re-invite's
                               media */
                            sip_listGetAt (pPartialMediaHashEntry->
                                           pMediaInfoList, u4Index,
                                           (SIP_Pvoid *) & pTemp, &dError);
                            pMediaElement = (SipAlgMediaElement *) pTemp;
                            if ((pMediaElement->ipAddress ==
                                 pNatInfo->privateIP) &&
                                (pMediaElement->port == pNatInfo->privatePort))
                            {
                                /* This is the case for handing the SDP 
                                   from private to public */
                                isMatched = SipTrue;
                                break;
                            }
                            else if ((pMediaElement->ipAddress == pNatInfo->
                                      outsideIP) && (pMediaElement->
                                                     port ==
                                                     pNatInfo->outsidePort))
                            {
                                /* This is the case for handing the SDP
                                   from public to private */
                                isMatched = SipTrue;
                                break;
                            }
                            else if (sipAlgIsWANIPAddress (pNatInfo->mappedIP))
                            {
                                /* This is the case for handing the SDP
                                   in intrasite call */
                                isMatched = SipTrue;
                                break;
                            }
                        }
                    sip_hashRelease (&dGlbMediaHash, pTempPartialKey);
                    /*Fix for memory leak - lakshmi */
                    /* sipAlgFreeDialogParam (&pTempPartialKey); */
                }
                /* memory leak fix start */
                sipAlgFreeDialogParam (&pTempPartialKey);
                /* memory leak fix end */
            }
            /* Even then if media doesnt match add that media in media hash */
            if (SipFalse == isMatched)
            {
                /* It means this is the new media line for the response
                   which already has a hash entry
                   Even if the pPartialKey is NULL, as there exists a
                   hash entry (pNewMediaHashEntry), it is expected to
                   append into the list and not create a hash entry */
                if (SipFail == sipAlgAddMediaElementInMediaHash (pNatInfo,
                                                                 pNewMediaHashEntry,
                                                                 pPartialKey,
                                                                 SipFalse))
                {
                    sip_error (SIP_Minor, "SIP-ERROR: sipAlgUpdateMediaTables: "
                               "sipAlgAddMediaElementInMediaHash failed\n");
                    sip_hashRelease (&dGlbMediaHash, pDialogParams);
                    sipAlgRemoveEntryFromMediaHash ((void *)
                                                    pNewMediaHashEntry);
                    return SipFail;
                }

            }                    /* End of if(SipFalse == isMatched) */
        }                        /* End of size of List in media Entries */
        /* Hash release should be done */
        sip_hashRelease (&dGlbMediaHash, pDialogParams);
    }                            /* End of else block which get data from Media hash
                                   using From, To and CallID */
    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgUpdateMediaTables");

    return SipSuccess;
}

/***************************************************************************
* Function Name : sipAlgAddMediaElementInMediaList
* Description     :  This function is invoked to add a new 
*                  Media element which contains media level
*                  info into Media hash table                 
* Input/Output(s) :  pNatInfo - 
*                         pSessionInfoEntry
*                      pKey -     
*                              
* Returns    : SUCCESS/FAILURE
****************************************************************************/
SipBool
sipAlgAddMediaElementInMediaHash (SipNatInfo * pNatInfo,
                                  SipMediaHashElement * pMediaHashElement,
                                  SipAlgMediaKey * pKey,
                                  en_SipBoolean dIsHashAdd)
{
    SIP_Pvoid           pTemp = SIP_NULL;
    SipAlgMediaElement *pMediaElement = SIP_NULL;
    SipError            dError = E_NO_ERROR;
    SIPDEBUG ((SIP_S8bit *) "Inside sipAlgAddMediaElementInMediaHash");
    /* It means this is the new media line.Insert it into Media Hash Table. */
    if ((SIP_NULL == pNatInfo) || (SIP_NULL == pMediaHashElement))
    {
        sip_error (SIP_Minor,
                   "SIP_ERROR:sipAlgAddMediaElementInMediaHash :Invalid Parameters");
        return SipFail;
    }

    pMediaElement = (SipAlgMediaElement *)
        sip_memget (0, sizeof (SipAlgMediaElement), &dError);
    if (pMediaElement == SIP_NULL)
    {
        sip_error (SIP_Minor,
                   "SIP_ERROR:sipAlgAddMediaElementInMediaHash :not sufficient memory");
        return SipFail;
    }
    /* Store the IP address and the port in the Media Element */
    if (pNatInfo->direction == SipPktIn)
    {
        pMediaElement->ipAddress = pNatInfo->outsideIP;
        pMediaElement->port = pNatInfo->outsidePort;
    }
    else if (pNatInfo->direction == SipPktOut)
    {
        pMediaElement->ipAddress = pNatInfo->privateIP;
        pMediaElement->port = pNatInfo->privatePort;
    }
    pMediaElement->refCount = 0;
    /* When the WAN IP is changed, based on the interface index, all the media
       hash entries will be removed */
    pMediaElement->ifIndex = pNatInfo->ifIndex;
    pMediaElement->direction = pNatInfo->direction;

    if (SIP_NULL == pMediaHashElement->pMediaInfoList)
    {
        pTemp = pMediaElement;
        sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &dError);
        pMediaElement = NULL;
        return SipFail;
    }
    /* Append the Media Element into the Media Info List. */
    sip_listAppend (pMediaHashElement->pMediaInfoList,
                    (SIP_Pvoid) pMediaElement, &dError);

    if ((pKey != SIP_NULL) && (dIsHashAdd == SipTrue))
    {
        if (SipFail == sip_hashAdd (&dGlbMediaHash,
                                    (SIP_Pvoid) pMediaHashElement,
                                    (SIP_Pvoid) pKey))
        {
            sip_error (SIP_Minor, "sip_hashAdd failed in "
                       "sipAlgAddMediaElementInMediaHash");
            /*sip_memfree(0,(SIP_Pvoid*)&pMediaElement,&dError); */
            sip_hashRelease (&dGlbMediaHash, (SIP_Pvoid) pKey);
            pTemp = pKey;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &dError);
            pKey = NULL;
            pTemp = pMediaHashElement;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &dError);
            pMediaHashElement = NULL;
            pTemp = pMediaElement;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &dError);
            pMediaElement = NULL;
            return SipFail;
        }
    }
    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgAddMediaElementInMediaHash");
    return SipSuccess;
}

/****************************************************************************
* Function Name : sipAlgUpdateMediaKeyInNatHash
* Description     : When the INVITE request gets a response, the To tag is 
*                  added in the dialog parameter. So a new entry is created in
*                  Media hash table. This dialg parameter is then added in
*                  Nat hash table for the corresponding IP/Port entry of the 
*                  media. The entry was already created which the dialog key as 
*                  from Tag and Call ID. Not a new entry will be added based on 
*                  from tag, callid and totag.
* Input/Output (s) :pKey - This will map to the IP address and Port of the
*                        media for which dialog key needs to be added
*                    pMediaKey - This dialog key will be appended in the list
* Returns    : SUCCESS/FAILURE
****************************************************************************/
SipBool
sipAlgUpdateMediaKeyInNatHash (SipAlgNatHashKey * pKey,
                               SipAlgMediaKey * pMediaKey)
{
    SipAlgNatHashElement *pNatHashElement = SIP_NULL;
    SipError            err;
    SipAlgMediaKey     *pTempMediaKey = SIP_NULL;

    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgUpdateMediaKeyInNatHash");

    /* Fetch the entry from the NAT hash table */
    pNatHashElement = (SipAlgNatHashElement *)
        sip_hashFetch (&dGlbNatHash, (SIP_Pvoid) pKey);

    /* There already exists an entry for this in the hash table */
    if (pNatHashElement != SIP_NULL)
    {

        pTempMediaKey = (SipAlgMediaKey *) sip_memget (0,
                                                       sizeof (SipAlgMediaKey),
                                                       &err);
        if (pTempMediaKey == SIP_NULL)
        {
            sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
            return SipFail;
        }
        pTempMediaKey->pFromTag = sip_strdup (pMediaKey->pFromTag, 0);
        pTempMediaKey->pCallID = sip_strdup (pMediaKey->pCallID, 0);
        if (pMediaKey->pToTag != SIP_NULL)
            pTempMediaKey->pToTag = sip_strdup (pMediaKey->pToTag, 0);
        else
            pTempMediaKey->pToTag = SIP_NULL;

        /* Add the session key along with the element */
        if (sip_listAppend (pNatHashElement->pCallInfoList,
                            (SIP_Pvoid) pTempMediaKey, &err) == SipFail)
        {
            sip_error (SIP_Minor, "Could not add dialog key in "
                       "NAT hash table");
            sipAlgFreeDialogParam (&pTempMediaKey);
            sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
            return SipFail;
        }
        sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
        SIPDEBUG ((SIP_S8bit *) "Added dialog key in NAT hash");
    }
    else
    {
        sip_error (SIP_Minor, "No element corresponding to this key");
        return SipFail;
    }
    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgUpdateMediaKeyInNatHash");
    return SipSuccess;
}

/****************************************************************************
* Function Name : sipAlgDeleteMediaElement
* Description     : This function is invoked to clear the media hash entry 
* Input/Output (s) : pDialogParams - The key for media hash
*                     pIpPort - The entry to be removed from media hash
* Returns    : VOID
****************************************************************************/
SipBool
sipAlgDeleteMediaElement (SipAlgMediaKey * pDialogParams,
                          SipAlgIpPort * pIpPort)
{
    SipMediaHashElement *pMediaInfoEntry = SIP_NULL;
    SipError            dError = E_NO_ERROR;
    SIPDEBUG ((SIP_S8bit *) "Inside sipAlgDeleteMediaElement");

    if ((SIP_NULL == pIpPort) || (SIP_NULL == pDialogParams))
    {
        sip_error (SIP_Minor, "sipAlgDeleteMediaElement: Invalid arguments \n");
        return SipFail;
    }
    /* First Fetch the MediaInfo Entry from the Media Hash Table
       with full dialog params and store it in the  pMediaInfoEntry. */
    pMediaInfoEntry =
        (SipMediaHashElement *) sip_hashFetch (&dGlbMediaHash, pDialogParams);

    if (pMediaInfoEntry != SIP_NULL)
    {
        /* This pointer is used to avoid strict-aliasing warning */
        SIP_Pvoid           pTemp = SIP_NULL;
        SipAlgMediaElement *pMediaInfo = SIP_NULL;
        SIP_U32bit          dSize = 0;
        SIP_U32bit          u4Index = 0;
        /* memory leak fix start */
        /*SIP_U8bit           count = 0; */
        SIP_U32bit          count = 0;
        /* memory leak fix end */

        if ((SipSuccess == sip_listSizeOf (pMediaInfoEntry->
                                           pMediaInfoList, &dSize, &dError)) &&
            (0 != dSize))
        {
            /* Compare the private IP, private port and outside IP
               outside port fetched from the pNatInfo (ANY, ANY)
               with the value stored in the session Info Hash Table. */
            for (u4Index = 0; u4Index < dSize; u4Index++)
            {
                sip_listGetAt (pMediaInfoEntry->pMediaInfoList,
                               count, (SIP_Pvoid *) & pTemp, &dError);
                pMediaInfo = (SipAlgMediaElement *) pTemp;
                /* memory leak fix start */
                if (pMediaInfo != SIP_NULL && pIpPort != SIP_NULL)
                {
                    /* memory leak fix end */

                    if ((pMediaInfo->ipAddress == pIpPort->ipAddress) &&
                        (pMediaInfo->port == pIpPort->port))
                    {
                        sip_listDeleteAt (pMediaInfoEntry->pMediaInfoList,
                                          u4Index, &dError);
                        count--;
                    }
                    /* memory leak fix start */
                }
                /* memory leak fix end */
                count++;
            }                    /* End of for Loop */
        }                        /* End of num of entries check */
        sip_hashRelease (&dGlbMediaHash, pDialogParams);

        sip_listSizeOf (pMediaInfoEntry->pMediaInfoList, &dSize, &dError);
        if (dSize == 0)
        {
            /* If there are no entries in the List, then remove the entire
               list. */
            if (SipFail == sip_hashRemove (&dGlbMediaHash, pDialogParams))
            {
                sip_error (SIP_Minor,
                           "Could not remove the  Media hash element.");
                return SipFail;
            }
            if (pDialogParams->pToTag != SIP_NULL)
            {
                /* As the hash entry with complete dialog param is
                   removed, decrease the refCount for partial key */
                /* memory leak fix start */
                SipError            err;
                sip_memfree (DECODE_MEM_ID,
                             (SIP_Pvoid) & (pDialogParams->pToTag), &err);
                /* memory leak fix end */
                pDialogParams->pToTag = SIP_NULL;
                pMediaInfoEntry =
                    (SipMediaHashElement *) sip_hashFetch (&dGlbMediaHash,
                                                           pDialogParams);

                if (pMediaInfoEntry != SIP_NULL)
                {
                    if ((SipSuccess == sip_listSizeOf (pMediaInfoEntry->
                                                       pMediaInfoList, &dSize,
                                                       &dError))
                        && (0 != dSize))
                    {
                        for (u4Index = 0; u4Index < dSize; u4Index++)
                        {
                            sip_listGetAt (pMediaInfoEntry->pMediaInfoList,
                                           u4Index, (SIP_Pvoid *) & pTemp,
                                           &dError);
                            pMediaInfo = (SipAlgMediaElement *) pTemp;
                            pMediaInfo->refCount--;
                        }        /* End of for Loop */
                    }            /* End of num of entries check */
                    sip_hashRelease (&dGlbMediaHash, pDialogParams);
                }
            }
        }
    }
    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgDeleteMediaElement ");
    return SipSuccess;
}

/****************************************************************************
* Function Name : sipAlgDeleteMediaInfoFromNATHash
* Description     : This function is invoked on receiving a BYE message or a 
*                  failure response for INVITE. In either case, clear the 
*                  entries that are present
* Input/Output (s) : pKey - The IP and Port entry key 
*                     isTransKey - Whether the NAT hash entry for media is
*                     mapped IP/Port or not
* Returns    : VOID
****************************************************************************/
void
sipAlgDeleteMediaInfoFromNATHash (SipAlgNatHashKey * pKey,
                                  en_SipBoolean isTransKey,
                                  en_SipBoolean isDeleteDynamicEntry)
{
    /* This pointer is used to avoid strict-aliasing warning */
    SIP_Pvoid           pTemp = SIP_NULL;
    SipAlgNatHashElement *pNatHashElement = SIP_NULL;
    tNatAsyncResponse  *pNatResp = SIP_NULL;
    SIP_U32bit          dRetVal;
    tPinholeInfo       *pPinhole = SIP_NULL;
    SipError            err;
    en_SipBoolean       dIsBindingCreated, dIsDynEntryExist = SipTrue;
    SIP_Pvoid           pIPPort = SIP_NULL;
    SIP_U32bit          listSizeDest = 0;
    SIP_U32bit          listIter = 0;
    SipAlgIpPortPair   *pDestIPPort = SIP_NULL;
    SIP_U16bit          dTempPort = 0;
    SIP_U32bit          dTempIP = 0;

    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgDeleteMediaInfoFromNATHash");

    /* Fetch the entry from the hash table */
    pNatHashElement = (SipAlgNatHashElement *)
        sip_hashFetch (&dGlbNatHash, (SIP_Pvoid) pKey);

    if ((pNatHashElement != SIP_NULL) &&
        (pNatHashElement->isMediaElement == SipTrue))
    {
        if (isTransKey == SipFalse)
        {
            dIsBindingCreated = pNatHashElement->isBindingCreated;

            /* First stop the timer */
            if (pNatHashElement->pSipAlgTimerInfo != SIP_NULL)
            {
                /*  Stop the SIP ALG timer */
                SIPDEBUG ((SIP_S8bit *) "Stopped Timer");
                dRetVal =
                    NatSipAlgStopTimer (pNatHashElement->pSipAlgTimerInfo);
                if (dRetVal != NAT_SUCCESS)
                {
                    sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
                    sip_error (SIP_Minor, "Could not stop SIP ALG timer");
                    /*sipAlgRemoveEntryFromNatHash((SIP_Pvoid *)pNatHashElement); */
                    return;
                }
            }
            if (dIsBindingCreated == SipTrue)
            {
                /*check if partial entry is already deleted as part of 
                   sipalg timer expiry callback. In that case we need not call
                   AricentNATDeletePortMapping_ipc again */
                if (pNatHashElement->isNatPartialEntryDeleted == SipFalse)
                {
                    /* if it is binding then delete the binding */
                    tNatEntryInfo      *pNatEntryInfo = SIP_NULL;

                    /* Allocate memory for NAT Entry Info */
                    pNatEntryInfo = (tNatEntryInfo *)
                        sip_memget (DECODE_MEM_ID, sizeof (tNatEntryInfo),
                                    &err);
                    if (pNatEntryInfo == SIP_NULL)
                    {
                        return;
                    }
                    sip_memset ((SIP_Pvoid) (pNatEntryInfo), 0,
                                sizeof (tNatEntryInfo));

                    pNatEntryInfo->u4SrcIP =
                        OSIX_NTOHL (pNatHashElement->originalIP);
                    pNatEntryInfo->u2SrcPort = pNatHashElement->originalPort;
                    pNatEntryInfo->u4Direction = SipPktIn;
                    pNatEntryInfo->u2Protocol = pNatHashElement->protocol;
                    pNatEntryInfo->au1Padding[0] = 0;
                    pNatEntryInfo->au1Padding[1] = 0;
                    pNatEntryInfo->au1Padding[2] = 0;
                    pNatEntryInfo->u1TimerFlag = NAT_SIP_ACTIVE;
                    /* Outside IP/Port are kept as unknown */
                    pNatEntryInfo->u4DestIP = 0;
                    pNatEntryInfo->u2DestPort = 0;

                    /*lakshmi  added - NAT API Enhancements - START */
                    if (pNatHashElement->isMediaElement == SipTrue)
                    {
                        pNatEntryInfo->u1BindingType = en_media;
                        pNatEntryInfo->u2NumBindings = 2;
                    }
                    else
                    {
                        pNatEntryInfo->u1BindingType = en_signaling;
                        pNatEntryInfo->u2NumBindings = 1;
                    }
                    pNatEntryInfo->u1Parity = (UINT1) pNatHashElement->parity;
                    /* NAT API Enhancements - END */

                    (void) sip_listSizeOf (pNatHashElement->pDestInfoList,
                                           &listSizeDest, &err);

                    /*  The list size will always be more than 0. */
                    if (listSizeDest != 0)
                    {
                        listIter = 0;
                        (void) sip_listGetAt (pNatHashElement->pDestInfoList,
                                              listIter, (SIP_Pvoid *) & pIPPort,
                                              &err);
                        pDestIPPort = (SipAlgIpPortPair *) pIPPort;
                        /* Update the NAT port and NAT IP */
                        pNatEntryInfo->u2NATPort = pDestIPPort->mappedPort;
                        pNatEntryInfo->u4NATIP =
                            OSIX_NTOHL (pDestIPPort->mappedIP);
                    }
                    /* Cookie is passed with value 0 - Sync API */
                    CFA_UNLOCK ();
#ifdef SDF_TIMESTAMP
                    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
                    {
                        Sdf_fn_printTimeStamp
                            ("sipAlgPerformTrans:Pre-AricentNATDeltePortMapping_ipc");
                    }
#endif
                    pNatResp = (tNatAsyncResponse *)
                        sip_memget (DECODE_MEM_ID, sizeof (tNatAsyncResponse),
                                    &err);
                    if (pNatResp == SIP_NULL)
                    {
                        sip_error (SIP_Minor, "Could not delete NAT binding.");
                        sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
                        pTemp = pNatEntryInfo;
                        sip_memfree (DECODE_MEM_ID,
                                     (SIP_Pvoid *) & pTemp, &err);
                        pNatEntryInfo = NULL;
                        return;
                    }
                    NatSipAlgDeletePortMapping (pNatEntryInfo, 0, pNatResp);
#ifdef SDF_TIMESTAMP
                    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
                    {

                        Sdf_fn_printTimeStamp
                            ("sipAlgPerformTrans:Post-AricentNATDltePortMapping_ipc");
                    }
#endif
                    CFA_LOCK ();
                    if (pNatResp->u4Status != NAT_SIP_SUCCESS)
                    {
                        /*if partial entries have got deleted after 212sec, then
                           AricentNATDeletePortMapping will result in binding not
                           existing error code.we need to proceed further with 
                           the deletion of dynamic entries if it exists and 
                           must not return from here */
                        if (pNatResp->u4ErrorCode != NATIPC_BINDING_NOT_EXISTS)
                        {
                            sip_error (SIP_Minor,
                                       "Could not delete NAT binding:");
                            sip_error (SIP_Minor,
                                       pSipAlgNATAPIErrorCode[pNatResp->
                                                              u4ErrorCode]);

                            sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);

                            pTemp = pNatResp;
                            sip_memfree (DECODE_MEM_ID,
                                         (SIP_Pvoid *) & pTemp, &err);
                            pNatResp = NULL;
                            pTemp = pNatEntryInfo;
                            sip_memfree (DECODE_MEM_ID,
                                         (SIP_Pvoid *) & pTemp, &err);
                            pNatEntryInfo = NULL;
                            return;
                        }
                    }
                    pTemp = pNatResp;
                    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                    pNatResp = NULL;
                    /* free(pNatResp); */
                    pTemp = pNatEntryInfo;
                    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                    pNatEntryInfo = NULL;
                    (void) sip_trace (SIP_Detailed, SIP_Init,
                                      (SIP_S8bit *)
                                      "SIP ALG TRACE: BYE received. Deleted "
                                      "the binding related to media");
                }
            }
            else
            {
                /*check if partial entry is already deleted as part of 
                   sipalg timer expiry callback. In that case we need not call
                   AricentNATClosePinHole_ipc again */
                if (pNatHashElement->isNatPartialEntryDeleted == SipFalse)
                {
                    /* Now close the pinhole */
                    pPinhole = (tPinholeInfo *)
                        sip_memget (DECODE_MEM_ID, sizeof (tPinholeInfo), &err);
                    if (pPinhole == SIP_NULL)
                    {
                        return;
                    }

                    /* Pinhole is created to allow traffic to flow from 
                       inside network to outside. So the destination
                       IP and port was fixed. */
                    pPinhole->u4SrcIP = 0;
                    pPinhole->u2SrcPort = 0;
                    pPinhole->u4DestIP =
                        OSIX_NTOHL (pNatHashElement->originalIP);
                    pPinhole->u2DestPort = pNatHashElement->originalPort;
                    pPinhole->u2Protocol = NAT_SIP_UDP;
                    pPinhole->u2Direction = NAT_OUTBOUND;
                    pPinhole->u1TimerFlag = NAT_SIP_ACTIVE;

                    /* NAT API enhancement..modified by rasheed 
                       pPinhole->au1Padding[0] = 0;
                       pPinhole->au1Padding[1] = 0;
                       pPinhole->au1Padding[2] = 0;
                     */
                    /*lakshmi added - for NAT API Enhancements - START */
                    if (pNatHashElement->isMediaElement == SipTrue)
                    {
                        pPinhole->u2NumPinholes = 2;
                    }
                    else
                    {
                        pPinhole->u2NumPinholes = 1;
                    }
                    /* NAT API Enhancements - END */
                    /* Cookie is passed with value 0 - Sync API */
                    CFA_UNLOCK ();
#ifdef SDF_TIMESTAMP
                    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
                    {

                        Sdf_fn_printTimeStamp
                            ("sipAlgPerformTrans:Pre-AricentNATClosePinHole_ipc");
                    }
#endif
                    pNatResp = (tNatAsyncResponse *)
                        sip_memget (DECODE_MEM_ID, sizeof (tNatAsyncResponse),
                                    &err);
                    if (pNatResp == SIP_NULL)
                    {
                        sip_error (SIP_Minor, "Could not close pinholes.");
                        sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
                        pTemp = pPinhole;
                        sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp,
                                     &err);
                        pPinhole = NULL;
                        return;
                    }

                    NatSipAlgClosePinhole (pPinhole, 0, pNatResp);
#ifdef SDF_TIMESTAMP
                    if (TIMESTAMP_ENABLE == SipGetAlgTimeStamp ())
                    {

                        Sdf_fn_printTimeStamp
                            ("sipAlgPerformTrans:Post-AricentNATClosePinhole_ipc");
                    }
#endif
                    CFA_LOCK ();
                    if (pNatResp->u4Status != NAT_SIP_SUCCESS)
                    {
                        /* free(pNatResp); */
                        /*if partial entries have got deleted after 212sec, then
                           AricentNATClosePinhole_ipc will result in pinhole not
                           existing error code.we need to proceed further with 
                           the deletion of dynamic entries if it exists and 
                           must not return from here */

                        if (pNatResp->u4ErrorCode != NATIPC_PINHOLE_NOT_EXISTS)
                        {
                            sip_error (SIP_Minor, "Could not close pinholes.");
                            sip_error (SIP_Minor,
                                       pSipAlgNATAPIErrorCode[pNatResp->
                                                              u4ErrorCode]);
                            sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);

                            pTemp = pNatResp;
                            sip_memfree (DECODE_MEM_ID,
                                         (SIP_Pvoid *) & pTemp, &err);
                            pNatResp = NULL;
                            pTemp = pPinhole;
                            sip_memfree (DECODE_MEM_ID,
                                         (SIP_Pvoid *) & pTemp, &err);
                            pPinhole = NULL;
                            return;
                        }
                    }
                    /* free(pNatResp); */
                    pTemp = pNatResp;
                    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                    pNatResp = NULL;
                    pTemp = pPinhole;
                    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                    pPinhole = NULL;
                    (void) sip_trace (SIP_Detailed, SIP_Init,
                                      (SIP_S8bit *)
                                      "SIP ALG TRACE: BYE received. Closed "
                                      "firewall pinhole");
                }
            }
            (void) sip_listSizeOf (pNatHashElement->pDestInfoList,
                                   &listSizeDest, &err);
            if ((listSizeDest == 0) && (dIsBindingCreated == SipFalse))
            {
                /* this is the case of pinhole for which there is now
                   media flowing ie no dynamic entry is present */
                dIsDynEntryExist = SipFalse;

            }
            else if ((listSizeDest == 1) && (dIsBindingCreated == SipTrue))
            {
                /* this is the case of binding */
                listIter = 0;
                (void) sip_listGetAt (pNatHashElement->pDestInfoList,
                                      listIter, (SIP_Pvoid *) & pIPPort, &err);
                pDestIPPort = (SipAlgIpPortPair *) pIPPort;
                if ((pDestIPPort->destPort == 0) || (pDestIPPort->destIP == 0))
                {
                    dIsDynEntryExist = SipFalse;
                }

            }
            /* The below case is for deleting the dynamic entry */
            if ((isDeleteDynamicEntry == SipTrue)
                && (dIsDynEntryExist == SipTrue))
            {
                tNatEntryInfo      *pNatEntry = SIP_NULL;
                pNatEntry = (tNatEntryInfo *)
                    sip_memget (DECODE_MEM_ID, sizeof (tNatEntryInfo), &err);
                if (pNatEntry == SIP_NULL)
                {
                    return;
                }
                sip_memset ((SIP_Pvoid) (pNatEntry), 0, sizeof (tNatEntryInfo));

                pNatEntry->u2Protocol = NAT_UDP;
                pNatEntry->au1Padding[0] = 0;
                pNatEntry->au1Padding[1] = 0;
                pNatEntry->au1Padding[2] = 0;
                pNatEntry->u1TimerFlag = 0;
                pNatEntry->u4Direction = 0;
                (void) sip_listSizeOf (pNatHashElement->pDestInfoList,
                                       &listSizeDest, &err);

                /* The list size will always be more than 0. */
                if (listSizeDest != 0)
                {
                    listIter = 0;
                    (void) sip_listGetAt (pNatHashElement->pDestInfoList,
                                          listIter, (SIP_Pvoid *) & pIPPort,
                                          &err);
                    pDestIPPort = (SipAlgIpPortPair *) pIPPort;
                    /* Update the NAT port and NAT IP */
                    pNatEntry->u2NATPort = pDestIPPort->mappedPort;
                    pNatEntry->u4NATIP = OSIX_NTOHL (pDestIPPort->mappedIP);
                    dTempIP = OSIX_NTOHL (pDestIPPort->destIP);
                    dTempPort = pDestIPPort->destPort;
                }

                if (dIsBindingCreated == SipTrue)
                {
                    pNatEntry->u4SrcIP =
                        OSIX_NTOHL (pNatHashElement->originalIP);
                    pNatEntry->u2SrcPort = pNatHashElement->originalPort;
                    pNatEntry->u4DestIP = dTempIP;
                    pNatEntry->u2DestPort = dTempPort;
                }
                else
                {
                    pNatEntry->u4SrcIP = dTempIP;
                    pNatEntry->u2SrcPort = dTempPort;
                    pNatEntry->u4DestIP =
                        OSIX_NTOHL (pNatHashElement->originalIP);
                    pNatEntry->u2DestPort = pNatHashElement->originalPort;
                }
                /* Cookie is passed with value 0 - Sync API */
                CFA_UNLOCK ();
#ifdef SIPALG_UT_FLAG
                PRINTF
                    ("\n\ncalling AricentNATDeleteDynamicEntry_ipc for the tuple: "
                     "%x,%u,%x,%u,%x,%u\n\n", pNatEntry->u4SrcIP,
                     pNatEntry->u2SrcPort, pNatEntry->u4DestIP,
                     pNatEntry->u2DestPort, pNatEntry->u4NATIP,
                     pNatEntry->u2NATPort);
#endif
                pNatResp = (tNatAsyncResponse *)
                    sip_memget (DECODE_MEM_ID, sizeof (tNatAsyncResponse),
                                &err);
                if (pNatResp == SIP_NULL)
                {
                    sip_error (SIP_Minor, "Could not NAT dynamic entry.");
                    sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
                    pTemp = pNatEntry;
                    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                    pNatEntry = NULL;
                    return;
                }
                NatSipAlgDeleteDynamicEntry (pNatEntry, 0, pNatResp);
                CFA_LOCK ();
                if (pNatResp->u4Status != NAT_SIP_SUCCESS)
                {
                    sip_error (SIP_Minor,
                               "Could not delete NAT dynamic entry:");
                    sip_error (SIP_Minor,
                               pSipAlgNATAPIErrorCode[pNatResp->u4ErrorCode]);
                    sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
                    /* free(pNatResp);   */
                    pTemp = pNatResp;
                    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                    pNatResp = NULL;
                    pTemp = pNatEntry;
                    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                    pNatEntry = NULL;
                    return;
                }
                /* now delete the RTCP entries */
                pNatEntry->u2SrcPort = (UINT2) (pNatEntry->u2SrcPort + 1);
                /* The list size will always be more than 0. */
                if (listSizeDest != 0)
                {
                    pNatEntry->u2NATPort = (UINT2) (pNatEntry->u2NATPort + 1);
                    pNatEntry->u2DestPort = (UINT2) (pNatEntry->u2DestPort + 1);
                }
                /* Cookie is passed with value 0 - Sync API */
#ifdef SIPALG_UT_FLAG
                PRINTF
                    ("\n\ncalling AricentNATDeleteDynamicEntry_ipc for the tuple: "
                     "%x,%u,%x,%u,%x,%u\n\n", pNatEntry->u4SrcIP,
                     pNatEntry->u2SrcPort, pNatEntry->u4DestIP,
                     pNatEntry->u2DestPort, pNatEntry->u4NATIP,
                     pNatEntry->u2NATPort);
#endif

                CFA_UNLOCK ();
                NatSipAlgDeleteDynamicEntry (pNatEntry, 0, pNatResp);
                CFA_LOCK ();
                if (pNatResp == SIP_NULL)
                {
                    sip_error (SIP_Minor, "Could not delete NAT dynamic entry");
                    sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
                    pTemp = pNatEntry;
                    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                    pNatEntry = SIP_NULL;
                    return;
                }
                else if (pNatResp->u4Status != NAT_SIP_SUCCESS)
                {
                    sip_error (SIP_Minor, "Could not delete NAT dynamic entry");
                    sip_error (SIP_Minor,
                               pSipAlgNATAPIErrorCode[pNatResp->u4ErrorCode]);
                    sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
                    /* free(pNatResp); */
                    pTemp = pNatResp;
                    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                    pNatResp = SIP_NULL;
                    pTemp = pNatEntry;
                    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                    pNatEntry = SIP_NULL;
                    return;
                }
                /* free(pNatResp); */
                pTemp = pNatResp;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pNatResp = SIP_NULL;
                pTemp = pNatEntry;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pNatEntry = SIP_NULL;
                (void) sip_trace (SIP_Detailed, SIP_Init,
                                  (SIP_S8bit *)
                                  "SIP ALG TRACE: BYE received. Deleted "
                                  "the dynamic entry");
            }
        }

        /* In case of translated IP/Port key, there will be no timer started.
           So no need to stop the timer. Also only for private IP/Port, the
           NAT bindings were created. So that also need not be deleted. */
        sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
        /* Finally remove this entry from NAT hash */
        if (SipFail == sip_hashRemove (&dGlbNatHash, pKey))
        {
            sip_error (SIP_Minor, "Could not remove the hash element.");
            return;
        }
    }
    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgDeleteMediaInfoFromNATHash");
    return;
}

/****************************************************************************
*    Function Name : sipAlgDeleteNatBindingsNPinholes
*
*    Description    :
*        This is the function which is internally used by the SIP 
*        ALG to delete the Bindings and the pinholes. This will be used only 
*        by the SIP signalling request message. ie. BYE / final failure resp.
****************************************************************************/
SipBool
sipAlgDeleteNatBindingsNPinholes (SipAlgMediaKey * pDialogParams)
{
    /*   Fetch the SessionInfo Entry from the session Hash Table
       and store it in the  pSessionInfoEntry. */
    SipMediaHashElement *pMediaHashEntry = SIP_NULL;
    SipMediaHashElement *pTempMediaHashEntry = SIP_NULL;
    en_SipBoolean       isFullKeyEntryFound = SipFalse;

    SipAlgMediaKey      dPartialKey;
    SipError            dError = E_NO_ERROR;

    SIPDEBUG ((SIP_S8bit *) "Inside sipAlgDeleteNatBindingsNPinholes");

#ifdef SIPALG_UT_FLAG
    PRINTF ("\n\nBefore handling BYE\n\n ");
    sipAlgMediaHashPrint (&dGlbMediaHash);
    sipAlgNatHashPrint (&dGlbNatHash);
    /* SIPALG - 2.1 */
    sipAlgIPHashPrint (&dGlbIPHash);

#endif
    /* Fetch the media hash entry with all the three dialog params */
    pMediaHashEntry =
        (SipMediaHashElement *) sip_hashFetch (&dGlbMediaHash, pDialogParams);

    if (pMediaHashEntry != SIP_NULL)
    {
        SIP_U32bit          dSize = 0;
        SIP_U32bit          u4Index = 0;
        SIP_Pvoid           pTemp = SIP_NULL;
        SipAlgMediaElement *pMediaInfo;
        SipAlgNatHashKey    dKey;

        /* For this dialog either a 18x or 2xx was received with SDP param.
           Now for that dialog BYE or final failure response is received */
        isFullKeyEntryFound = SipTrue;
        if ((SipSuccess == sip_listSizeOf (pMediaHashEntry->
                                           pMediaInfoList, &dSize, &dError)) &&
            (0 != dSize))
        {
            /* Compare the private IP, private port and outside IP
               outside port fetched from the pNatInfo (ANY, ANY)
               with the value stored in the session Info Hash Table. */
            for (u4Index = 0; u4Index < dSize; u4Index++)
            {
                /* For intra-site call handling */
                SIP_U32bit          listIter = 0;
                SIP_Pvoid           pIPPort = SIP_NULL;
                SIP_U32bit          listSizeDest = 0;
                SipAlgIpPortPair   *pDestIPPort = SIP_NULL;
                SipAlgNatHashElement *pNatHashElement = SIP_NULL;
                SipAlgNatHashKey    transKey;
                SipError            err;

                sip_listGetAt (pMediaHashEntry->pMediaInfoList,
                               u4Index, (SIP_Pvoid *) & pTemp, &dError);
                pMediaInfo = (SipAlgMediaElement *) pTemp;

                dKey.ipAddress = pMediaInfo->ipAddress;
                dKey.port = pMediaInfo->port;
                dKey.direction = pMediaInfo->direction;
                dKey.ifIndex = pMediaInfo->ifIndex;

                /* For intra-site call handling. As part of intra-site call 
                   handling, new entry in nat hash will be created for out 
                   media with mapped-ip, port as key. So we need to delete 
                   that element from nat hash fetch the element from nat 
                   hash based on original key */
                pNatHashElement = (SipAlgNatHashElement *)
                    sip_hashFetch (&dGlbNatHash, (SIP_Pvoid) & dKey);
                if (pNatHashElement != SIP_NULL)
                {
                    /* Only in case NAT is enabled on this interface,
                       do the necessary changes */
                    if (pNatHashElement->isBindingCreated == SipTrue)
                    {
                        /* Fetch the mapped ip,port from its destinfolist */
                        (void) sip_listSizeOf (pNatHashElement->pDestInfoList,
                                               &listSizeDest, &err);
                        if (listSizeDest != 0)
                        {
                            listIter = 0;
                            (void) sip_listGetAt (pNatHashElement->
                                                  pDestInfoList, listIter,
                                                  (SIP_Pvoid *) & pIPPort,
                                                  &err);
                            pDestIPPort = (SipAlgIpPortPair *) pIPPort;
                            transKey.ipAddress = pDestIPPort->mappedIP;
                            transKey.port = pDestIPPort->mappedPort;
                            /* For the translated IP/Port, the direction should be in.
                               So for hashing, the ifIndex is also not needed */
                            transKey.direction = SipPktIn;
                            /* Delete from nat hash the entry with translated
                               ip port as key */
                            sipAlgDeleteMediaInfoFromNATHash (&transKey,
                                                              SipTrue,
                                                              SipFalse);
                        }
                    }
                    sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) & dKey);
                }
                /* Delete from NAT hash  */
                sipAlgDeleteMediaInfoFromNATHash (&dKey, SipFalse, SipFalse);
            }                    /* End of for Loop */
        }                        /* End of num of entries check */
        sip_hashRelease (&dGlbMediaHash, pDialogParams);
        if (SipFail == sip_hashRemove (&dGlbMediaHash, pDialogParams))
        {
            sip_error (SIP_Minor,
                       "sipAlgDeleteNatBindingsNPinholes :sip_hashRemove failed");
            return SipFail;
        }

#ifdef SIPALG_UT_FLAG
        PRINTF ("\n\nAfter removing from full key\n\n ");
        sipAlgMediaHashPrint (&dGlbMediaHash);
        sipAlgNatHashPrint (&dGlbNatHash);
        /* SIPALG - 2.1 */
        sipAlgIPHashPrint (&dGlbIPHash);

#endif
    }
    /* Form the partial Key and remove the entry in it. */
    dPartialKey.pFromTag = pDialogParams->pFromTag;
    dPartialKey.pCallID = pDialogParams->pCallID;
    dPartialKey.pToTag = SIP_NULL;
    /* Fetch the media hash entry with partial dialog params  */
    pTempMediaHashEntry =
        (SipMediaHashElement *) sip_hashFetch (&dGlbMediaHash, &dPartialKey);

    /* Reverse the From Tag and To Tag and fetch the element */
    if ((pTempMediaHashEntry == SIP_NULL) && (SIP_NULL != pDialogParams->pToTag))    /*Dump fix */
    {
        /* Form the partial Key and remove the entry in it. */
        dPartialKey.pFromTag = pDialogParams->pToTag;
        /* Fetch the media hash entry with partial dialog params  */
        pTempMediaHashEntry =
            (SipMediaHashElement *) sip_hashFetch (&dGlbMediaHash,
                                                   &dPartialKey);
    }
    if (pTempMediaHashEntry != SIP_NULL)
    {
        SIP_U32bit          dSize = 0;
        SIP_U32bit          u4Index = 0;
        SIP_Pvoid           pTemp = SIP_NULL;
        SipAlgMediaElement *pMediaInfo;
        SipAlgNatHashKey    dKey;

        if ((SipSuccess == sip_listSizeOf (pTempMediaHashEntry->
                                           pMediaInfoList, &dSize, &dError)) &&
            (0 != dSize))
        {
            for (u4Index = 0; u4Index < dSize; u4Index++)
            {
                /* As it was a match with full key, the element deletion
                   should reduce the refCount of 'from tag and call Id' */
                sip_listGetAt (pTempMediaHashEntry->pMediaInfoList,
                               u4Index, (SIP_Pvoid *) & pTemp, &dError);
                pMediaInfo = (SipAlgMediaElement *) pTemp;
                /* - This is to handle, INV - forked to multiple UAs. Final 
                   failure resp came from one UA. For that failure resp, 
                   remove entry from partial key if there was 18x resp with 
                   SDP params. isFullKeyEntryFound = true 
                   isFullKeyEntryFound = true,  refCount > 0 
                   - This will also handle INV - final failure resp. Delete
                   the entries from partial key.
                   isFullKeyEntryFound = false,  refCount = 0 
                   - This will also handle, INV - forked to multiple UAs. Final 
                   failure resp came from one UA. For that failure resp, 
                   DON'T remove entry from partial key if there was no 
                   18x resp with SDP params.  
                   isFullKeyEntryFound = false,  refCount > 0 */

                if (((isFullKeyEntryFound == SipFalse) &&
                     (pMediaInfo->refCount == 0)) ||
                    (isFullKeyEntryFound == SipTrue))
                {
                    if (pMediaInfo->refCount > 0)
                        pMediaInfo->refCount--;

                    if (pMediaInfo->refCount == 0)
                    {
                        /* For intra-site call handling */
                        SIP_U32bit          listIter = 0;
                        SIP_Pvoid           pIPPort = SIP_NULL;
                        SIP_U32bit          listSizeDest = 0;
                        SipAlgIpPortPair   *pDestIPPort = SIP_NULL;
                        SipAlgNatHashElement *pNatHashElement = SIP_NULL;
                        SipAlgNatHashKey    transKey;
                        SipError            err;

                        /* Close the pinhole or binding for the media ports 
                           of call originator */
                        dKey.ipAddress = pMediaInfo->ipAddress;
                        dKey.port = pMediaInfo->port;
                        dKey.direction = pMediaInfo->direction;
                        dKey.ifIndex = pMediaInfo->ifIndex;

                        /* For intra-site call handling fetch the element 
                           from nat hash based on original key */
                        pNatHashElement = (SipAlgNatHashElement *)
                            sip_hashFetch (&dGlbNatHash, (SIP_Pvoid) & dKey);
                        if (pNatHashElement != SIP_NULL)
                        {
                            /* Only in case NAT is enabled on this interface,
                               do the necessary changes */
                            if (pNatHashElement->isBindingCreated == SipTrue)
                            {
                                /* fetch the mapped ip,port from its destinfolist */
                                (void) sip_listSizeOf (pNatHashElement->
                                                       pDestInfoList,
                                                       &listSizeDest, &err);
                                if (listSizeDest != 0)
                                {
                                    listIter = 0;
                                    (void) sip_listGetAt (pNatHashElement->
                                                          pDestInfoList,
                                                          listIter,
                                                          (SIP_Pvoid *) &
                                                          pIPPort, &err);
                                    pDestIPPort = (SipAlgIpPortPair *) pIPPort;
                                    transKey.ipAddress = pDestIPPort->mappedIP;
                                    transKey.port = pDestIPPort->mappedPort;
                                    /* For the translated IP/Port, the direction should be in.
                                       So for hashing, the ifIndex is also not needed */
                                    transKey.direction = SipPktIn;
                                    /* delete from nat hash the entry with 
                                       translated ip port as key */
                                    sipAlgDeleteMediaInfoFromNATHash (&transKey,
                                                                      SipTrue,
                                                                      SipFalse);
                                }
                            }
                            sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) & dKey);
                        }

                        sipAlgDeleteMediaInfoFromNATHash (&dKey, SipFalse,
                                                          SipTrue);
                        if (SipFail ==
                            sip_hashRemove (&dGlbMediaHash, &dPartialKey))
                        {
                            sip_error (SIP_Minor,
                                       "sipAlgDeleteNatBindingsNPinholes :sip_hashRemove failed");
                            return SipFail;
                        }
                    }            /*End of refcount ==0 check */
                }                /* If Condition */
            }                    /* end of for loop */
        }                        /* End of num of entries check */
        sip_hashRelease (&dGlbMediaHash, &dPartialKey);
    }
#ifdef SIPALG_UT_FLAG
    PRINTF ("After handling Bye\n");
    sipAlgMediaHashPrint (&dGlbMediaHash);
    sipAlgNatHashPrint (&dGlbNatHash);
    /* SIPALG - 2.1 */
    sipAlgIPHashPrint (&dGlbIPHash);

#endif

    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgDeleteNatBindingsNPinholes");
    return SipSuccess;
}

/****************************************************************************
* Function Name : sipAlgUpdateRegContactInfo
* Description     : This is the function which will set the SIP ALG timer  
*                  for the contact information present in NAT hash table.
*                 
* Input/Output (s) :pNatInfo - The contact's translated IP will be passed 
*                        here, which is mapped back to the private address
*                    expiryValue - SIP ALG timer will be started for this
*                        duration of seconds     
* Returns    : SUCCESS/FAILURE
****************************************************************************/
SipBool
sipAlgUpdateRegContactInfo (SipNatInfo * pNatInfo, SIP_U32bit expiryValue,
                            SIP_U32bit ifIndex, sipAlgError * pAlgError,
                            SIP_S8bit * pAORString)
{
    /* This pointer is used to avoid strict-aliasing warning */
    SIP_Pvoid           pTemp = SIP_NULL;
    SipAlgNatHashKey   *pTransKey = SIP_NULL, *pHashKey = SIP_NULL;
    SipAlgNatHashElement *pNatHashElement = SIP_NULL, *pHashElement = SIP_NULL;

    SipError            err;

    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgUpdateRegContactInfo");

    /* Allocate memory for NAT hash key */
    pTransKey = (SipAlgNatHashKey *)
        sip_memget (DECODE_MEM_ID, sizeof (SipAlgNatHashKey), &err);
    if (pTransKey == SIP_NULL)
    {
        return SipFail;
    }
    /* Get the translated IP and port from pNatInfo and form the Key to 
       identify whether the entries are present in the NAT Hash Table. */
    pTransKey->ipAddress = pNatInfo->mappedIP;
    pTransKey->port = pNatInfo->mappedPort;

    /* This direction is used to map the hash key. Based of direction
       hashing function differs */
    pTransKey->direction = SipPktIn;

    /* Fetch the entry from NAT hash table */
    pNatHashElement = (SipAlgNatHashElement *)
        sip_hashFetch (&dGlbNatHash, (SIP_Pvoid) pTransKey);

    if (pNatHashElement == SIP_NULL)
    {
        sip_error (SIP_Minor, "No translation can be performed for "
                   "this contact.");
        pTemp = pTransKey;
        sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
        pTransKey = SIP_NULL;
        *pAlgError = sipAlgHashEntryNotFound;
        return SipSuccess;
        /* Rasheed- We are returning success from here becoz
           When a user registers with SSE(when sip is enabled),the
           tranlated ip and port will be WAN/listen port;;after this
           sip is disabled and all the sip-alg hashes are cleared...
           now if the same user registers(sip is disabled)..
           the 200 OK contains both the prev contact and the current contact.
           there wont be any entry in sip alg hash corresponding to the prev 
           contact and it was returning failure from here.. */

        /*return SipFail; */
    }

    /* Update the information with private IP/Port */
    pNatInfo->privateIP = pNatHashElement->originalIP;
    pNatInfo->privatePort = pNatHashElement->originalPort;

    if (dGlbSipStatus == en_sipDisable)
    {                            /* if B2B is not present */
        /* Start the SIP ALG timer with the duration of expiry header
           To restart the timer, fetch the entries with private IP/port
           Timer is running only for that NAT hash element. */
        pHashKey = (SipAlgNatHashKey *)
            sip_memget (DECODE_MEM_ID, sizeof (SipAlgNatHashKey), &err);
        if (pHashKey == SIP_NULL)
        {
            return SipFail;
        }
        pHashKey->ipAddress = pNatInfo->privateIP;
        pHashKey->port = pNatInfo->privatePort;
        pHashKey->direction = SipPktOut;
        pHashKey->ifIndex = ifIndex;

        pHashElement = (SipAlgNatHashElement *)
            sip_hashFetch (&dGlbNatHash, (SIP_Pvoid) pHashKey);
        if (pHashElement != SIP_NULL)
        {
            /*De Reg Partial Exp start */
            /*retrans of REG should npt cause another strdup so
             *check if pDeRegAOR is not NULL*/
            if ((expiryValue != 0)
                && (pHashElement->pSipAlgTimerInfo->pDeRegAOR == NULL))
            {
                pHashElement->pSipAlgTimerInfo->pDeRegAOR =
                    sip_strdup (pAORString, 0);
#ifdef SIPALG_UT_FLAG
                PRINTF
                    ("\n\n--=Address of algtimer->pDeRegAOR in sipAlgUpdateRegContactInfo"
                     " %p\n", &pHashElement->pSipAlgTimerInfo->pDeRegAOR);
#endif
            }
            /*De Reg Partial Exp end */

            if (pHashElement->pSipAlgTimerInfo != SIP_NULL)
            {
                SIPDEBUG ((SIP_S8bit *) "Restarting Timer");
                if (NatSipAlgReStartTimer (pHashElement->pSipAlgTimerInfo,
                                           expiryValue) != NAT_SUCCESS)
                {
                    sip_error (SIP_Minor,
                               "Could not start SIP ALG timer for contact");
                    sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pHashKey);
                    sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pTransKey);
                    pTemp = pTransKey;
                    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                    pTransKey = SIP_NULL;
                    pTemp = pHashKey;
                    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                    pHashKey = SIP_NULL;
                    return SipFail;
                }
            }
            sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pHashKey);
        }
        else
        {
            sip_error (SIP_Minor, "No translation can be performed for"
                       "this contact.");
            sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pTransKey);
            pTemp = pTransKey;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
            pTransKey = SIP_NULL;
            pTemp = pHashKey;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
            pHashKey = SIP_NULL;
            return SipFail;
        }
        /* This is updated only for contact */
        pHashElement->isRegTimerStarted = SipTrue;
        /* decrement refcount with hashRelease */
        sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pTransKey);

        (void) sip_trace (SIP_Detailed, SIP_Init,
                          (SIP_S8bit *)
                          "SIP ALG TRACE: Started the reg expiry timer");

        SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgUpdateRegContactInfo");
        pTemp = pTransKey;
        sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
        pTransKey = SIP_NULL;
        pTemp = pHashKey;
        sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
        pHashKey = SIP_NULL;
    }
    else
    {                            /* if B2B is present */
        /* No action will be taken when B2B is present. The B2B binding
           will be there forever */

        /* decrement refcount with hashRelease */
        sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pTransKey);

        SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgUpdateRegContactInfo");
        pTemp = pTransKey;
        sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
        pTransKey = SIP_NULL;
    }
    return SipSuccess;

}

/* new function for port change notification */
void
sipAlgSipPortChangeNotification (SIP_U16bit dPort)
{
    en_SipBoolean       dIfIndexFound = SipFalse;
    SIP_U16bit          i = 0;

    /* first check whether the port same as the previous one;
       if so ; return */
#ifdef SIPALG_UT_FLAG
    PRINTF ("\n\nin sipAlgSipPortChangeNotification function\n");
#endif
    if (dPort == dGlbSipAlgPortInfo.dSipAlgPort)
    {
#ifdef SIPALG_UT_FLAG
        PRINTF ("The port configured is same as the older port\n\n");
        sipAlgNatHashPrint (&dGlbNatHash);
#endif
        return;
    }
    else
    {
        for (i = 0; i < dGlbInterfaceIndex; i++)
        {
#ifdef SIPALG_UT_FLAG
            PRINTF ("\n\nthe ifindix in glbinterface struct is:%d\n\n",
                    dGlbInterfaceInfo[i].ifIndex);
            PRINTF ("the value of ifindex in port struct is:%d",
                    dGlbSipAlgPortInfo.ifIndex);
#endif
            if (dGlbInterfaceInfo[i].ifIndex == dGlbSipAlgPortInfo.ifIndex)
            {
                dIfIndexFound = SipTrue;
                break;
            }
        }
        if (dIfIndexFound == SipTrue)
        {
            dGlbInterfaceInfo[i].isSignalingEntryCreated = SipFalse;
        }
        return;
    }
}

/* function for global nat disable notification */
void
sipAlgNatDisableNotification (void)
{
    SipHashIterator     dIterator;
    SipAlgNatHashElement *pNatHashElement = SIP_NULL;
    SIP_Pvoid           pTempKey = SIP_NULL;

    SIPDEBUG ((SIP_S8bit *)
              "SIP_DEBUG - Entering sipAlgNatDisableNotification");

    /*  Clear NAT hash table  */
    sip_hashInitIterator (&dGlbNatHash, &dIterator);

    /*  Go through the hash table entries and remove them */
    while (SIP_NULL != dIterator.pCurrentElement)
    {
        pTempKey = dIterator.pCurrentElement->pKey;
        pNatHashElement =
            (SipAlgNatHashElement *) (dIterator.pCurrentElement->pElement);
        if (pNatHashElement != SIP_NULL)
        {
            if (pNatHashElement->isB2bSingallingEntry == SipTrue)
            {
                sip_hashNext (&dGlbNatHash, &dIterator);
                continue;
            }
            /*  Stop the SIP ALG timer */
            if (pNatHashElement->pSipAlgTimerInfo != SIP_NULL)
            {
                NatSipAlgStopTimer (pNatHashElement->pSipAlgTimerInfo);
            }
        }
        sip_hashRemove (&dGlbNatHash, (SIP_Pvoid) pTempKey);
        sip_hashNext (&dGlbNatHash, &dIterator);
    }

    /*  Clear Media hash table  */
    sip_hashInitIterator (&dGlbMediaHash, &dIterator);

    /*  Go through the hash table entries and remove them */
    while (SIP_NULL != dIterator.pCurrentElement)
    {
        pTempKey = dIterator.pCurrentElement->pKey;
        sip_hashRemove (&dGlbMediaHash, (SIP_Pvoid) pTempKey);
        sip_hashNext (&dGlbMediaHash, &dIterator);
    }

    /* De Reg: Removal of DeReg hash entries flushing on Nat disable */
    /* Clear DeReg hash table  */
    sip_hashInitIterator (&dGlbDeRegHash, &dIterator);

    /*  Go through the hash table entries and remove them */
    while (SIP_NULL != dIterator.pCurrentElement)
    {
        pTempKey = dIterator.pCurrentElement->pKey;
        sip_hashRemove (&dGlbDeRegHash, (SIP_Pvoid) pTempKey);
        sip_hashNext (&dGlbDeRegHash, &dIterator);
    }

    SIPDEBUG ((SIP_S8bit *) "SIP_DEBUG - Exiting sipAlgNatDisableNotification");
    return;
}

/****************************************************************************
* Function Name : sipAlgAddPortMapping
* Description     : This function is invoked at the time of initialization
*    if B2B server is running along with CAS. That time, one binding 
*    will be created for the IP/Port of B2B for which there will
*     be no timer running. This binding will be used for all singaling
*     entries.
* Input/Output (s) : b2bIP - This is the VLAN IP from where B2B send pkt
*                    ifIndex - WAN interface index
* Returns    : SUCCESS/FAILURE
****************************************************************************/
SipBool
sipAlgAddPortMapping (SIP_U32bit b2bIP, SIP_U32bit ifIndex,
                      SIP_U32bit natIP, en_SipBoolean isNatEnabled)
{
    /* This pointer is used to avoid strict-aliasing warning */
    SIP_Pvoid           pTemp = SIP_NULL;
    SipAlgNatHashKey   *pKey = SIP_NULL;
    SipAlgNatHashKey   *pTransKey = SIP_NULL;
    SipAlgNatHashElement *pNatHashElement = SIP_NULL;
    SipAlgNatHashElement *pTempNatHashElement = SIP_NULL;
    SipAlgIpPortPair   *pDestIPPort = SIP_NULL;
    SipAlgIpPortPair   *pTempDestIPPort = SIP_NULL;
    SIP_U32bit          mappedIP = 0;
    /*SIP_U32bit    natIP = 0; 
       SIP_U32bit i=0; */
    SIP_U16bit          mappedPort = 0;
    SipError            err = E_NO_ERROR;
    /*en_SipBoolean isNatEnabled = SipTrue; */

    mappedIP = OSIX_HTONL (natIP);
    mappedPort = (UINT2) gSipSignalPort.u2UdpListenPort;    /*NAT_SIP_TCPUDP_PORT; */

    /* Allocate memory for NAT hash key */
    pKey = (SipAlgNatHashKey *)
        sip_memget (DECODE_MEM_ID, sizeof (SipAlgNatHashKey), &err);
    if (pKey == SIP_NULL)
    {
        return SipFail;
    }
    pKey->ipAddress = b2bIP;
    pKey->port = (UINT2) gSipSignalPort.u2UdpListenPort;    /*NAT_SIP_TCPUDP_PORT; */

    /* Store the direction in hash key. Direction is used while 
       performing a hash comparison. For outgoing packets ifIndex
       will be used in hash comparison. */
    pKey->direction = SipPktOut;
    pKey->ifIndex = ifIndex;

    /* As there is no entry in the NAT hash table, create a new entry
       This is NOT a media element and there will be no SIP ALG timer
       started for this. */
    if (sipAlgInitNatHashElement (&pNatHashElement, pKey, SipFalse, SipFalse)
        == SipFail)
    {
        sip_error (SIP_Minor, "Could not initialize NAT hash element.");
        sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) pKey);
        pTemp = pKey;
        sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
        pKey = SIP_NULL;
        return SipFail;
    }

    /* Store the status of NAT */
    pNatHashElement->isNatEnabled = isNatEnabled;
    /* set the isB2bSingallingEntry to true */
    pNatHashElement->isB2bSingallingEntry = SipTrue;
    /* Store the private IP address and port in NAT table */
    pNatHashElement->originalIP = b2bIP;
    pNatHashElement->originalPort = (UINT2) gSipSignalPort.u2UdpListenPort;
    /*NAT_SIP_TCPUDP_PORT; */

    /* As this entry is never going to expire, protocol need not be 
       stored. The stored protocol is used on SIP ALG timer expiry
       to close the binding. (pNatHashElement->protocol) */

    /* For the outgoing packet, store the translated IP and port 
       so that it can be reused in next translation */
    pDestIPPort = (SipAlgIpPortPair *)
        sip_memget (DECODE_MEM_ID, sizeof (SipAlgIpPortPair), &err);
    if (pDestIPPort == SIP_NULL)
    {
        pTemp = pKey;
        sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
        pKey = SIP_NULL;
        sipAlgRemoveEntryFromNatHash ((SIP_Pvoid) pNatHashElement);
        return SipFail;
    }

    /* Store the translated IP address and port and outside 
       IP address and port in NAT table. Outside IP/Port
       will always be unknown. */
    pDestIPPort->destIP = 0;
    pDestIPPort->destPort = 0;
    pDestIPPort->mappedIP = mappedIP;
    pDestIPPort->mappedPort = mappedPort;

    /* Add the destination IP/Port entry */
    if (sip_listAppend (pNatHashElement->pDestInfoList,
                        (SIP_Pvoid) pDestIPPort, &err) == SipFail)
    {
        sip_error (SIP_Minor, "Could not append dest info in"
                   " NAT hash table");
        pTemp = pKey;
        sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
        pKey = SIP_NULL;
        /* As append is failed, dest list will be null. So freeing
           it here */
        pTemp = pDestIPPort;
        sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
        pDestIPPort = SIP_NULL;
        sipAlgRemoveEntryFromNatHash ((SIP_Pvoid) pNatHashElement);
        return SipFail;
    }
    /* For signaling the value will always be SipTrue. */
    pNatHashElement->isBindingCreated = SipTrue;
    /* For signaling the value will always be SipFalse. */
    pNatHashElement->isMediaElement = SipFalse;
    /* As there is no timer running, setting this value will
       never restart the timer */
    pNatHashElement->isRegTimerStarted = SipTrue;

    /* The interface index is also stored in NAT hash element. While
       deleting the entries from hash table on WAN IP change, the 
       interface index can be used to delete the specific entry */
    pNatHashElement->ifIndex = ifIndex;

    /* Add the pNatHashElement in the NAT hash table. */
    if (sip_hashAdd (&dGlbNatHash,
                     ((SIP_Pvoid) pNatHashElement),
                     ((SIP_Pvoid) pKey)) != SipSuccess)
    {
        sip_error (SIP_Minor, "Could not add hash element in "
                   " NAT hash table");
        pTemp = pKey;
        sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
        pKey = SIP_NULL;
        sipAlgRemoveEntryFromNatHash ((SIP_Pvoid) pNatHashElement);
        return SipFail;
    }

    /* The translation with the mapped IP/Port entry will
       be created only for signaling entry which needs to be
       translated back to private entry in the response. */
    pTransKey = (SipAlgNatHashKey *)
        sip_memget (DECODE_MEM_ID, sizeof (SipAlgNatHashKey), &err);

    if (pTransKey == SIP_NULL)
    {
        return SipFail;
    }
    /* Get the translated IP and port from pNatInfo and form the 
       Key. This will be used to map any response or request 
       coming from WAN side. */
    if (isNatEnabled == SipFalse)
    {
        pTransKey->ipAddress = pNatHashElement->originalIP;
        pTransKey->port = pNatHashElement->originalPort;
    }
    else
    {
        pTransKey->ipAddress = mappedIP;
        pTransKey->port = mappedPort;
    }

    /* These signaling entries are created for outgoing packet only 
       Interface index is not required for hash comparison logic for
       incoming packets. This entry is to allow incoming packets */
    pTransKey->direction = SipPktIn;

    if (sipAlgInitNatHashElement (&pTempNatHashElement, pTransKey,
                                  SipFalse, SipFalse) == SipFail)
    {
        sip_error (SIP_Minor, "Could not initialize NAT hash element.");
        pTemp = pTransKey;
        sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
        pTransKey = SIP_NULL;
        sip_hashRemove (&dGlbNatHash, pKey);
        return SipFail;
    }
    /* Store the status of NAT */
    pTempNatHashElement->isNatEnabled = isNatEnabled;
    /* set the isB2bSingallingEntry to true */
    pTempNatHashElement->isB2bSingallingEntry = SipTrue;

    /* Store the private IP address and port in NAT table */
    pTempNatHashElement->originalIP = b2bIP;
    pTempNatHashElement->originalPort = (UINT2) gSipSignalPort.u2UdpListenPort;
    /*NAT_SIP_TCPUDP_PORT; */

    /* For the outgoin packet, store the translated IP and port 
       so that it can be reused in next translation */
    pTempDestIPPort = (SipAlgIpPortPair *)
        sip_memget (DECODE_MEM_ID, sizeof (SipAlgIpPortPair), &err);
    if (pTempDestIPPort == SIP_NULL)
    {
        sipAlgRemoveEntryFromNatHash ((SIP_Pvoid) pTempNatHashElement);
        pTemp = pTransKey;
        sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
        pTransKey = SIP_NULL;
        return SipFail;
    }
    /* Store the translated IP address and port and outside 
       IP address and port in NAT table. Outside IP/Port
       will always be unknown. */
    pTempDestIPPort->destIP = 0;
    pTempDestIPPort->destPort = 0;
    pTempDestIPPort->mappedIP = mappedIP;
    pTempDestIPPort->mappedPort = mappedPort;

    /* Add the destination IP/Port entry */
    if (sip_listAppend (pTempNatHashElement->pDestInfoList,
                        (SIP_Pvoid) pTempDestIPPort, &err) == SipFail)
    {
        sip_error (SIP_Minor, "Could not append dest info in"
                   " NAT hash table");
        pTemp = pTransKey;
        sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
        pTransKey = SIP_NULL;
        /* As append is failed, dest list will be null. So freeing
           it here */
        pTemp = pTempDestIPPort;
        sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
        pTempDestIPPort = SIP_NULL;
        sipAlgRemoveEntryFromNatHash ((SIP_Pvoid) pTempNatHashElement);
        sip_hashRemove (&dGlbNatHash, pKey);
        return SipFail;
    }

    /* For signaling the value will always be SipTrue. */
    pTempNatHashElement->isBindingCreated = SipTrue;
    /* For signaling the value will always be SipFalse. */
    pTempNatHashElement->isMediaElement = SipFalse;
    /* As there is no timer running, setting this value will 
       never restart the timer */
    pTempNatHashElement->isRegTimerStarted = SipTrue;

    /* The interface index is also stored in NAT hash element. While
       deleting the entries from hash table on WAN IP change, the 
       interface index can be used to delete the specific entry */
    pTempNatHashElement->ifIndex = ifIndex;
    /* Add the pNatHashElement in the hash table using a 
       different key */
    if (sip_hashAdd (&dGlbNatHash,
                     ((SIP_Pvoid) pTempNatHashElement),
                     ((SIP_Pvoid) pTransKey)) != SipSuccess)
    {
        sip_error (SIP_Minor, "Could not add hash element in "
                   "NAT hash table");
        pTemp = pTransKey;
        sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
        pTransKey = SIP_NULL;
        sipAlgRemoveEntryFromNatHash ((SIP_Pvoid) pTempNatHashElement);
        sip_hashRemove (&dGlbNatHash, pKey);
        return SipFail;
    }
    /* fill the info in sipalgportinfo structure */
    dGlbSipAlgPortInfo.dSipAlgPort = gSipSignalPort.u2UdpListenPort;
    dGlbSipAlgPortInfo.dIPAddress = b2bIP;
    dGlbSipAlgPortInfo.ifIndex = ifIndex;

    (void) sip_trace (SIP_Detailed, SIP_Init,
                      (SIP_S8bit *) "SIP ALG TRACE: Added entry in NAT hash");

    return SipSuccess;
}

/* //////////////////////////////////////////////////////////////////////////// */
/* FUNCTION:     sipAlgDeInitOnInterfaceChange */
/*  */
/*  DESCRIPTION: Function is to de-initialize all the data structure used by */
/*  the SIP ALG for the given interface. Basically it clears the entries */
/*  from NAT and media hash for that interface. No deletion of the hash table */
/*  is done. It is used when the WAN IP is changed.  */
/* //////////////////////////////////////////////////////////////////////////// */
void
sipAlgDeInitOnInterfaceChange (tLinkInfo linkInfo)
{
    /* This pointer is used to avoid strict-aliasing warning */
    SIP_Pvoid           pTemp = SIP_NULL;
    SipError            dError = E_NO_ERROR;
    SipHashIterator     dIterator;
    SipAlgNatHashElement *pNatHashElement = SIP_NULL;
    SIP_Pvoid           pTempKey = SIP_NULL;
    SIP_U32bit          dSize = 0;
    SIP_U32bit          u4Index = 0;
    SIP_U16bit          i = 0, dFoundInterfaceIndex = 0;
    en_SipBoolean       deleteEntry = SipFalse, dInterfaceFound = SipFalse;

    SIP_U32bit          listSizeCall = 0;
    SIP_U32bit          listIter = 0;
    SipAlgMediaKey     *pMediaKey = SIP_NULL;
    SipList            *pMediaIpPortList = SIP_NULL;
    UINT4               u4IfNum = 0;

    SIPDEBUG ((SIP_S8bit *) "SIP_DEBUG - Inside sipAlgDeInitOnInterfaceChange");

    /* check the interface type..if it is not WAN then return */
    if ((linkInfo.u2LinkType != CFA_NETWORK_TYPE_WAN) ||
        (linkInfo.u2Status != NAT_SIP_LINK_STATUS_UP))
    {
        return;
    }
    /* First search in the dGlbInterfaceInfo  whether this interface exists,
       if it is not..add this info in the array..if it is found, update
       the new ip and clear the hash entries */
    for (i = 0; i < dGlbInterfaceIndex; i++)
    {
        if (sip_strcasecmp
            ((const SIP_S8bit *) linkInfo.au1LinkPort,
             (const SIP_S8bit *) dGlbInterfaceInfo[i].dInterfaceName) == 0)
        {
            dInterfaceFound = SipTrue;
            dFoundInterfaceIndex = i;
            break;
        }
    }
    /* if interface is not found in the glbarray; add it */
    if (dInterfaceFound == SipFalse)
    {
        /* update the glb interface array */
        dGlbInterfaceInfo[dGlbInterfaceIndex].dWanIP = linkInfo.u4IpAddr;
        sip_strncpy ((SIP_S8bit *) dGlbInterfaceInfo[dGlbInterfaceIndex].
                     dInterfaceName, (SIP_S8bit *) linkInfo.au1LinkPort,
                     sizeof (dGlbInterfaceInfo[dGlbInterfaceIndex].
                             dInterfaceName));
        /* Lakshmi change - start */
        if (SecUtilIpIfGetIfIndexFromIpAddress (OSIX_NTOHL (linkInfo.u4IpAddr),
                                                &u4IfNum) == OSIX_SUCCESS)
        {
            dGlbInterfaceInfo[dGlbInterfaceIndex].ifIndex = u4IfNum;
        }
        /* Lakshmi change -end */
        /* increment the interface index */
        dGlbInterfaceIndex++;
    }
    else
    {

        /* interface already exists; update the wanip; and clear
           the nat and media hash entries */
        if (dGlbInterfaceInfo[dFoundInterfaceIndex].dWanIP == linkInfo.u4IpAddr)
        {
            return;
        }
#ifdef SIPALG_UT_FLAG
        sipAlgNatHashPrint (&dGlbNatHash);
        /* SIPALG - 2.1 */
        sipAlgIPHashPrint (&dGlbIPHash);

        sipAlgMediaHashPrint (&dGlbMediaHash);
#endif

        dGlbInterfaceInfo[dFoundInterfaceIndex].dWanIP = linkInfo.u4IpAddr;
        dGlbInterfaceInfo[dFoundInterfaceIndex].isSignalingEntryCreated =
            SipFalse;

        pMediaIpPortList = (SipList *)
            sip_memget (DECODE_MEM_ID, sizeof (SipList), &dError);

        /* init the mediaipport list */
        if (sip_listInit (pMediaIpPortList,
                          sipAlgFreeIpPortList, &dError) == SipFail)
        {
            sip_error (SIP_Minor, "Could not initialize media info list.");
            pTemp = pMediaIpPortList;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) (&pTemp), &dError);
            pMediaIpPortList = SIP_NULL;
            return;
        }
        /*  Clear NAT hash table  */
        sip_hashInitIterator (&dGlbNatHash, &dIterator);

        /*  Go through the hash table entries and remove them */
        while (SIP_NULL != dIterator.pCurrentElement)
        {
            deleteEntry = SipFalse;
            pNatHashElement =
                (SipAlgNatHashElement *) (dIterator.pCurrentElement->pElement);

            if ((pNatHashElement != SIP_NULL) &&
                (pNatHashElement->ifIndex ==
                 dGlbInterfaceInfo[dFoundInterfaceIndex].ifIndex))
            {
                /* Interface index will be stored in case of any outgoing entry
                   (The nat hash entry for which binding is created). Also the 
                   interface index will be stored for the corresponding mapped entry */

                pTempKey = dIterator.pCurrentElement->pKey;
                /*  Stop the SIP ALG timer */
                if (pNatHashElement->pSipAlgTimerInfo != SIP_NULL)
                {
                    NatSipAlgStopTimer (pNatHashElement->pSipAlgTimerInfo);

                }
                deleteEntry = SipTrue;
            }
            sip_hashNext (&dGlbNatHash, &dIterator);
            if (deleteEntry == SipTrue)
            {
                /* Get the dialog param from the NAT hash element. Based on these
                   values, delete the entry from media hash. From media hash
                   get the entries (ip/port) corresponding to explicit pinholes. 
                   Later delete them from NAT hash */

                (void) sip_listSizeOf (pNatHashElement->pCallInfoList,
                                       &listSizeCall, &dError);

                listIter = 0;
                while (listIter != listSizeCall)
                {
                    pTemp = pMediaKey;
                    (void) sip_listGetAt (pNatHashElement->pCallInfoList,
                                          listIter, (SIP_Pvoid *) & pTemp,
                                          &dError);
                    if (sipAlgDeleteMediaOnInterfaceChange
                        (pMediaKey, pMediaIpPortList) == SipFail)
                    {
                        sip_error (SIP_Minor, "Could not delete entry "
                                   "from media hash on interface change");
                    }
                    listIter++;
                }
                sip_hashRemove (&dGlbNatHash, (SIP_Pvoid) pTempKey);
                pTemp = pMediaIpPortList;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) (&pTemp), &dError);
                pMediaIpPortList = SIP_NULL;
            }
            else
            {
                pTemp = pMediaIpPortList;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) (&pTemp), &dError);
                pMediaIpPortList = SIP_NULL;
                return;

            }

        }
        if (deleteEntry == SipFail)
        {
            pTemp = pMediaIpPortList;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) (&pTemp), &dError);
            pMediaIpPortList = SIP_NULL;
            return;
        }
        /* Now delete the entries that are present in the list updated
           in sipAlgDeleteMediaOnInterfaceChange */
        /* first get the size of the list */
        (void) sip_listSizeOf (pMediaIpPortList, &dSize, &dError);

        for (u4Index = 0; u4Index < dSize; u4Index++)
        {
            SIP_Pvoid           pTempIPPort = SIP_NULL;
            SipAlgIpPort       *pIpPort = SIP_NULL;
            SipAlgNatHashKey    dKey;
            /* get the ip and port at u4Index; form the hashkey */
            sip_listGetAt (pMediaIpPortList, u4Index,
                           (SIP_Pvoid *) & pTempIPPort, &dError);
            pIpPort = (SipAlgIpPort *) pTempIPPort;
            dKey.ipAddress = pIpPort->ipAddress;
            dKey.port = pIpPort->port;
            dKey.direction = SipPktIn;
            sip_hashRemove (&dGlbNatHash, (SIP_Pvoid) & dKey);
        }
#ifdef SIPALG_UT_FLAG
        sipAlgNatHashPrint (&dGlbNatHash);
        /* SIPALG - 2.1 */
        sipAlgIPHashPrint (&dGlbIPHash);

        sipAlgMediaHashPrint (&dGlbMediaHash);
#endif

    }

    SIPDEBUG ((SIP_S8bit *)
              "SIP_DEBUG - Exiting sipAlgDeInitOnInterfaceChange");
}

SipBool
sipAlgDeleteMediaOnInterfaceChange (SipAlgMediaKey * pMediaKey,
                                    SipList * pMediaIpPortList)
{
    SipMediaHashElement *pMediaHashElement = SIP_NULL;
    SipError            dError = E_NO_ERROR;
    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgDeleteMediaOnInterfaceChange");

    if (SIP_NULL == pMediaKey)
    {
        sip_error (SIP_Minor,
                   "sipAlgDeleteMediaOnInterfaceChange: Invalid arguments\n");
        return SipFail;
    }
    /* First Fetch the Media hash Entry from the Media Hash Table
       with dialog params and store it in the pMediaHashElement. */
    pMediaHashElement =
        (SipMediaHashElement *) sip_hashFetch (&dGlbMediaHash, pMediaKey);

    if (pMediaHashElement != SIP_NULL)
    {
        SIP_Pvoid           pTemp = SIP_NULL;
        SipAlgMediaElement *pMediaElement = SIP_NULL;
        SIP_U32bit          dSize = 0;
        SIP_U32bit          u4Index = 0;

        if ((SipSuccess == sip_listSizeOf (pMediaHashElement->
                                           pMediaInfoList, &dSize, &dError)) &&
            (0 != dSize))
        {
            for (u4Index = 0; u4Index < dSize; u4Index++)
            {
                sip_listGetAt (pMediaHashElement->pMediaInfoList,
                               u4Index, (SIP_Pvoid *) & pTemp, &dError);
                pMediaElement = (SipAlgMediaElement *) pTemp;
                if (pMediaElement->direction == SipPktIn)
                {
                    SipAlgIpPort       *pIpPort = SIP_NULL;
                    pIpPort = (SipAlgIpPort *)
                        sip_memget (DECODE_MEM_ID, sizeof (SipAlgIpPort),
                                    &dError);
                    if (pIpPort == SIP_NULL)
                    {
                        return SipFail;
                    }
                    /* update the ip and port of the media element into
                       the list */
                    pIpPort->ipAddress = pMediaElement->ipAddress;
                    pIpPort->port = pMediaElement->port;
                    sip_listAppend (pMediaIpPortList, (SIP_Pvoid) pIpPort,
                                    &dError);
                }
                sip_listDeleteAt (pMediaHashElement->pMediaInfoList, u4Index,
                                  &dError);
            }
        }
        sip_hashRelease (&dGlbMediaHash, pMediaKey);
        sip_listSizeOf (pMediaHashElement->pMediaInfoList, &dSize, &dError);
        if (dSize == 0)
        {
            /* If there are no entries in the List, then remove the entire
               list. */
            if (SipFail == sip_hashRemove (&dGlbMediaHash, pMediaKey))
            {
                sip_error (SIP_Minor,
                           "Could not remove the  Media hash element.");
                pTemp = pMediaIpPortList;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) (&pTemp), &dError);
                pMediaIpPortList = SIP_NULL;
                return SipFail;
            }
        }
    }
    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgDeleteMediaOnInterfaceChange");
    return SipSuccess;
}

/****************************************************************************
* Function Name : sipAlgFreeCallList
* Description     : This is the function registered at the time of list init
*    Once list element is deleted, this function gets called (callback)
*                 
* Input/Output (s) :  pCallElement - The element in the list
* Returns    : Nothing
****************************************************************************/
void                (sipAlgFreeCallList) (SIP_Pvoid pCallElement)
{
    SIP_Pvoid           pVoidPtr = SIP_NULL;
    SipError            err;
    SipAlgMediaKey     *pTemp = (SipAlgMediaKey *) pCallElement;
    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgFreeCallList");
    if (pTemp != SIP_NULL)
    {
        sip_memfree (DECODE_MEM_ID, (SIP_Pvoid) & (pTemp->pFromTag), &err);
        sip_memfree (DECODE_MEM_ID, (SIP_Pvoid) & (pTemp->pToTag), &err);
        sip_memfree (DECODE_MEM_ID, (SIP_Pvoid) & (pTemp->pCallID), &err);
    }
    pVoidPtr = pTemp;
    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pVoidPtr, &err);
    pTemp = SIP_NULL;
    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgFreeCallList");
}

/****************************************************************************
* Function Name : sipAlgFreeDestList
* Description     : This is the function registered at the time of list init
*    Once list element is deleted, this function gets called (callback)
*                 
* Input/Output (s) :  pDestElement - The element in the list
* Returns    : Nothing
****************************************************************************/
void                (sipAlgFreeDestList) (SIP_Pvoid pDestElement)
{
    SipError            err;
    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgFreeDestList");
    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pDestElement, &err);
    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgFreeDestList");
}

/****************************************************************************
* Function Name : sipAlgFreeMediaInfoList
* Description     : This is the function registered at the time of list init
*    Once list element is deleted, this function gets called (callback)
*                 
* Input/Output (s) :  pMediaElement - The element in the list
* Returns    : Nothing
****************************************************************************/
void                (sipAlgFreeMediaInfoList) (SIP_Pvoid pMediaElement)
{
    SipError            err;
    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgFreeMediaInfoList");
    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pMediaElement, &err);
    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgFreeMediaInfoList");
}

/****************************************************************************
* Function Name : sipAlgFreeIpPortList
* Description     : This is the function registered at the time of list init
*    Once list element is deleted, this function gets called (callback)
*                 
* Input/Output (s) :  pIpPort - The element in the list
* Returns    : Nothing
****************************************************************************/
void                (sipAlgFreeIpPortList) (SIP_Pvoid pIpPort)
{
    SipError            err;
    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgFreeIpPortList");
    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pIpPort, &err);
    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgFreeIpPortList");
}

    /* SIPALG - 2.1 */
/****************************************************************************
* Function Name : sipAlgInitIPHashElement
* Description     : Initialize the IP Hash table for a new entry.
*                 
* Input/Output (s) :  ppIPHashElement - The element to be initialed
*                      pKey - Key for IP hash table    
* Returns    : SUCCESS/FAILURE
****************************************************************************/
SipBool
sipAlgInitIPHashElement (SipAlgIPHashElement ** ppIPHashElement,
                         SipAlgIPHashKey * pKey)
{
    SipError            err;

    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgInitIPHashElement");

    (*ppIPHashElement) = (SipAlgIPHashElement *) sip_memget (DECODE_MEM_ID,
                                                             sizeof
                                                             (SipAlgIPHashElement),
                                                             &err);
    if ((*ppIPHashElement) == SIP_NULL)
    {
        sip_error (SIP_Minor, "Could not get the required memory");
        return SipFail;
    }
    (*ppIPHashElement)->pSipAlgTimerInfo = SIP_NULL;

    /* Allocate memory for timer */
    (*ppIPHashElement)->pSipAlgTimerInfo =
        (tNatSipAlgTimer *) sip_memget (DECODE_MEM_ID,
                                        sizeof (tNatSipAlgTimer), &err);

    if ((*ppIPHashElement)->pSipAlgTimerInfo == SIP_NULL)
    {
        sip_error (SIP_Minor, "Could not get the required memory");
        sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) (ppIPHashElement), &err);
        return SipFail;
    }

    sip_memset ((SIP_Pvoid) ((*ppIPHashElement)->pSipAlgTimerInfo),
                0, sizeof (tNatSipAlgTimer));
    /* 
       Even if we are initializing the IP hash entry, the key in the
       timer will be that of NAT hash. Only the pCallID and 
       isMediaPortZero variables will be used. Rest of the things
       are not needed. This is done becasue on SIP ALG timer expry
       the hash key used is SipAlgNATHashKey.
     */
    (*ppIPHashElement)->pSipAlgTimerInfo->pHashKey =
        (SipAlgNatHashKey *) sip_memget (DECODE_MEM_ID,
                                         sizeof (SipAlgNatHashKey), &err);

    if ((*ppIPHashElement)->pSipAlgTimerInfo->pHashKey == SIP_NULL)
    {
        sip_error (SIP_Minor, "Could not get the required memory");
        sip_memfree (DECODE_MEM_ID,
                     (SIP_Pvoid) ((*ppIPHashElement)->pSipAlgTimerInfo), &err);
        sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) (ppIPHashElement), &err);
        return SipFail;
    }

    /* All the NAT hash elements will have this value. Only the NAT IP 
       hash table (used for o-line) will have the value as SipTrue */
    (*ppIPHashElement)->pSipAlgTimerInfo->pHashKey->isMediaPortZero = SipTrue;
    (*ppIPHashElement)->pSipAlgTimerInfo->pHashKey->pCallID =
        sip_strdup (pKey->pCallID, 0);
    (*ppIPHashElement)->pSipAlgTimerInfo->pHashKey->pVersionId =
        sip_strdup (pKey->pVersionId, 0);
    (*ppIPHashElement)->pSipAlgTimerInfo->pHashKey->isRequest = pKey->isRequest;

    (*ppIPHashElement)->pSipAlgTimerInfo->TimerNode.u2Flags = 0;

    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgInitIPHashElement");
    return SipSuccess;
}

/****************************************************************************
* Function Name : sipAlgInitNatHashElement
* Description     : Initialize the Nat Hash table for a new entry.
*                 
* Input/Output (s) :  ppNatHashElement - The element to be initialed
*                      pKey - Key for NAT hash table    
*                      isMediaElement - Whether the element created is for
*                      media or a contact            
* Returns    : SUCCESS/FAILURE
****************************************************************************/
SipBool
sipAlgInitNatHashElement (SipAlgNatHashElement ** ppNatHashElement,
                          SipAlgNatHashKey * pKey, en_SipBoolean isMediaElement,
                          en_SipBoolean dIsTimerReqd)
{
    SipError            err;
    /* This pointer is used to avoid strict-aliasing warning */
    SIP_Pvoid           pTemp = SIP_NULL;

    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgInitNatHashElement");

    (*ppNatHashElement) = (SipAlgNatHashElement *) sip_memget (DECODE_MEM_ID,
                                                               sizeof
                                                               (SipAlgNatHashElement),
                                                               &err);
    if ((*ppNatHashElement) == SIP_NULL)
    {
        sip_error (SIP_Minor, "Could not get the required memory");
        return SipFail;
    }
    (*ppNatHashElement)->pSipAlgTimerInfo = SIP_NULL;
    /* Allocate memory for the list */
    (*ppNatHashElement)->pDestInfoList =
        (SipList *) sip_memget (DECODE_MEM_ID, sizeof (SipList), &err);

    /* Initialize the Destination IP/Port list */
    if (sip_listInit ((*ppNatHashElement)->pDestInfoList,
                      sipAlgFreeDestList, &err) == SipFail)
    {
        sip_error (SIP_Minor, "Could not initialize dest info list.");
        sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) (ppNatHashElement), &err);
        return SipFail;
    }
    if (dIsTimerReqd == SipTrue)
    {
        /* Allocate memory for timer */
        (*ppNatHashElement)->pSipAlgTimerInfo =
            (tNatSipAlgTimer *) sip_memget (DECODE_MEM_ID,
                                            sizeof (tNatSipAlgTimer), &err);
        if ((*ppNatHashElement)->pSipAlgTimerInfo == NULL)
        {
            pTemp = (*ppNatHashElement)->pDestInfoList;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & (pTemp), &err);
            (*ppNatHashElement)->pDestInfoList = SIP_NULL;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) (ppNatHashElement), &err);
            return SipFail;
        }

/*De Reg Partial Exp Start */
/*This AOR is updated in dereg hash SipAlgAddContactOnDeReg fn*/
        (*ppNatHashElement)->pSipAlgTimerInfo->pDeRegAOR = SIP_NULL;
/*De Reg Partial Exp end */

        sip_memset ((SIP_Pvoid) ((*ppNatHashElement)->pSipAlgTimerInfo),
                    0, sizeof (tNatSipAlgTimer));

        /* Allocate memory for NAT hash key which will be stored */
        (*ppNatHashElement)->pSipAlgTimerInfo->pHashKey =
            (SipAlgNatHashKey *) sip_memget (DECODE_MEM_ID,
                                             sizeof (SipAlgNatHashKey), &err);
        if ((*ppNatHashElement)->pSipAlgTimerInfo->pHashKey == NULL)
        {
            pTemp = (*ppNatHashElement)->pDestInfoList;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & (pTemp), &err);
            (*ppNatHashElement)->pDestInfoList = SIP_NULL;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) (ppNatHashElement), &err);
            return SipFail;
        }
        (*ppNatHashElement)->pSipAlgTimerInfo->pHashKey->ipAddress =
            pKey->ipAddress;
        (*ppNatHashElement)->pSipAlgTimerInfo->pHashKey->port = pKey->port;
        (*ppNatHashElement)->pSipAlgTimerInfo->pHashKey->direction =
            pKey->direction;
        (*ppNatHashElement)->pSipAlgTimerInfo->pHashKey->ifIndex =
            pKey->ifIndex;
        /* All the NAT hash elements will have this value. Only the NAT IP 
           hash table (used for o-line) will have the value as SipTrue */
        (*ppNatHashElement)->pSipAlgTimerInfo->pHashKey->isMediaPortZero =
            SipFalse;
        (*ppNatHashElement)->pSipAlgTimerInfo->pHashKey->pCallID = SIP_NULL;
        (*ppNatHashElement)->pSipAlgTimerInfo->pHashKey->pVersionId = SIP_NULL;

        /* This entry is not used in NAT hash table. Just initializing it */
        (*ppNatHashElement)->pSipAlgTimerInfo->pHashKey->isRequest = SipTrue;

        (*ppNatHashElement)->pSipAlgTimerInfo->TimerNode.u2Flags = 0;
    }
    /* Indicates whether it was registration timer expiry or for media
       On receiving a 200 OK for register that contact's value is 
       updated. */
    (*ppNatHashElement)->isRegTimerStarted = SipFalse;
    /* set the isB2bSingallingEntry to false */
    (*ppNatHashElement)->isB2bSingallingEntry = SipFalse;

    /* set the isNatPartialEntryDeleted to false */
    (*ppNatHashElement)->isNatPartialEntryDeleted = SipFalse;

    if (isMediaElement == SipTrue)
    {
        /* Update the protocol for NAT entry. For media it will always be UDP */
        (*ppNatHashElement)->protocol = en_udp;

        /* Is this a media element  */
        (*ppNatHashElement)->isMediaElement = SipTrue;

        /* Allocate memory for the list */
        (*ppNatHashElement)->pCallInfoList =
            (SipList *) sip_memget (DECODE_MEM_ID, sizeof (SipList), &err);

        /* Initialize the Destination IP/Port list */
        if (sip_listInit ((*ppNatHashElement)->pCallInfoList,
                          sipAlgFreeCallList, &err) == SipFail)
        {
            sip_error (SIP_Minor, "Could not initialize call info list.");
            pTemp = (*ppNatHashElement)->pDestInfoList;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & (pTemp), &err);
            (*ppNatHashElement)->pDestInfoList = SIP_NULL;
            if ((*ppNatHashElement)->pSipAlgTimerInfo != NULL)
            {
                pTemp = (*ppNatHashElement)->pSipAlgTimerInfo->pHashKey;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & (pTemp), &err);
                (*ppNatHashElement)->pSipAlgTimerInfo->pHashKey = SIP_NULL;
                pTemp = (*ppNatHashElement)->pSipAlgTimerInfo;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) (pTemp), &err);
                (*ppNatHashElement)->pSipAlgTimerInfo = SIP_NULL;
            }
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) (ppNatHashElement), &err);
            return SipFail;
        }
    }
    else
    {
        /* There will be a binding created for a contact entry */
        if (dIsTimerReqd == SipTrue)
        {
            (*ppNatHashElement)->isBindingCreated = SipTrue;
        }
        /* Initialize the Call Info List to NULL */
        (*ppNatHashElement)->pCallInfoList = SIP_NULL;

        /* Is this a media element */
        (*ppNatHashElement)->isMediaElement = SipFalse;
    }

    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgInitNatHashElement");
    return SipSuccess;
}

/****************************************************************************
* Function Name : sipAlgInitMediaHashElement
* Description     : Initialize the Media Hash table for a new entry.
*                 
* Input/Output (s) :  ppNatHashElement - The element to be initialed
*                      pKey - Key for NAT hash table    
* Returns    : SUCCESS/FAILURE
****************************************************************************/
SipBool
sipAlgInitMediaHashElement (SipMediaHashElement ** ppMediaHashElement)
{
    SipError            err;
    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgInitMediaHashElement");

    (*ppMediaHashElement) =
        (SipMediaHashElement *) sip_memget (DECODE_MEM_ID,
                                            sizeof (SipMediaHashElement), &err);
    if ((*ppMediaHashElement) == SIP_NULL)
    {
        sip_error (SIP_Minor, "Could not get the required memory");
        return SipFail;
    }

    /* Allocate memory for the list */
    (*ppMediaHashElement)->pMediaInfoList =
        (SipList *) sip_memget (DECODE_MEM_ID, sizeof (SipList), &err);

    /* Initialize the Destination IP/Port list */
    if (sip_listInit ((*ppMediaHashElement)->pMediaInfoList,
                      sipAlgFreeMediaInfoList, &err) == SipFail)
    {
        sip_error (SIP_Minor, "Could not initialize media info list.");
        sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) (ppMediaHashElement), &err);
        return SipFail;
    }

    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgInitMediaHashElement");
    return SipSuccess;
}

/****************************************************************************
* Function Name : sipAlgDeleteDeRegOnParExp
* Description     :Hash fetch Dereg Hash (key = pAORString) and delete the entry 
* which matches with the provided dPvtIP and dPvtPort. This is called when partial
* expiry has happened for this pIP and port.
* Input/Output (s) :  pAORString - "user@host"
*                  Pvt ip and port for which partial timer has expired.
* Returns    : SUCCESS/FAILURE
****************************************************************************/
SipBool
sipAlgDeleteDeRegOnParExp (SIP_S8bit * pAORString, SIP_U32bit dPvtIP,
                           SIP_U16bit dPvtPort)
{
    /* This pointer is used to avoid strict-aliasing warning */
    SIP_Pvoid           pTemp = SIP_NULL;
    SipError            err;
    SipAlgDeRegHashElement *pDeRegHashElement = SIP_NULL;
    SipAlgDeRegHashKey *pDeRegKey = SIP_NULL;

    SipError            dError = E_NO_ERROR;

    SIPDEBUG ((SIP_S8bit *) "SIP_DEBUG - Entering sipAlgDeleteDeRegOnParExp");

#ifdef SIPALG_UT_FLAG
    PRINTF
        ("\n\n***The DeReg hash &  NAT hash entry before partial Exp for DeReg \n\n");
    PRINTF ("\nAORstring is : %s\n", pAORString);
    sipAlgDeRegHashPrint (&dGlbDeRegHash);
    sipAlgNatHashPrint (&dGlbNatHash);
#endif

    /* Allocate memory for NAT hash key */
    pDeRegKey = (SipAlgDeRegHashKey *)
        sip_memget (DECODE_MEM_ID, sizeof (SipAlgDeRegHashKey), &err);

    if (pDeRegKey == SIP_NULL)
    {
        return SipFail;
    }
    pDeRegKey->pAOR = sip_strdup (pAORString, 0);

    /* First Fetch the NATInfo Entry from the De Reg Hash Table
       with AOR  */
    pDeRegHashElement =
        (SipAlgDeRegHashElement *) sip_hashFetch (&dGlbDeRegHash, pDeRegKey);

    if (pDeRegHashElement != SIP_NULL)
    {
        SipAlgDeRegElement *pDeRegInfo = SIP_NULL;
        SIP_U32bit          dSize = 0;
        SIP_U32bit          u4Index = 0;

        if ((SipSuccess == sip_listSizeOf (pDeRegHashElement->
                                           pDeRegInfoList, &dSize, &dError)) &&
            (0 != dSize))
        {
            for (u4Index = 0; u4Index < dSize; u4Index++)
            {
                sip_listGetAt (pDeRegHashElement->pDeRegInfoList,
                               u4Index, (SIP_Pvoid *) & pTemp, &dError);
                pDeRegInfo = (SipAlgDeRegElement *) pTemp;

                if (dPvtPort == pDeRegInfo->port
                    && dPvtIP == pDeRegInfo->ipAddress)
                {
                    sip_listDeleteAt (pDeRegHashElement->pDeRegInfoList,
                                      u4Index, &dError);
                    break;
                }

            }                    /* End of for Loop */
        }                        /* End of num of entries check */

        sip_listSizeOf (pDeRegHashElement->pDeRegInfoList, &dSize, &dError);
        sip_hashRelease (&dGlbDeRegHash, (SIP_Pvoid) pDeRegKey);
        if (dSize == 0)
        {
#ifdef SIPALG_UT_FLAG
            PRINTF
                ("----:NoError sipAlgDeleteDeRegOnParExp with listsize 0 so delete node.--");
#endif
            /* If there are no entries in the List, then remove the entire list. */
            if (SipFail == sip_hashRemove (&dGlbDeRegHash, pDeRegKey))
            {
                sip_error (SIP_Minor,
                           "Could not remove the  De Reg hash element.");
                SIPDEBUG ((SIP_S8bit *)
                          "SIP_DEBUG - Exiting sipAlgDeleteDeRegOnParExp 3");
                sip_memfree (DECODE_MEM_ID,
                             (SIP_Pvoid) & pDeRegKey->pAOR, &err);
                pTemp = pDeRegKey;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pDeRegKey = SIP_NULL;
                return SipFail;
            }
        }
    }
    else
    {                            /*pDeRegHashElement is not present i.e. it's NULL */
        SIPDEBUG ((SIP_S8bit *)
                  "We got partial expiry for ip/port which is not present in"
                  " our de reg hash ");
        SIPDEBUG ((SIP_S8bit *)
                  "SIP_DEBUG - Exiting sipAlgDeleteDeRegOnParExp 4");
    }

    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid) & pDeRegKey->pAOR, &err);
    pTemp = pDeRegKey;
    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
    pDeRegKey = SIP_NULL;

    /* element is list of contacts
       select the expired contacts. based on isExpired -
       reset the timer to very small time i.e. 4secs so that it expires soon */
#ifdef SIPALG_UT_FLAG
    PRINTF
        ("\n\n***The DeReg hash &  NAT hash entry after partial Exp for DeReg \n\n");
    sipAlgDeRegHashPrint (&dGlbDeRegHash);
    sipAlgNatHashPrint (&dGlbNatHash);
#endif
    SIPDEBUG ((SIP_S8bit *) "SIP_DEBUG - Exiting sipAlgDeleteDeRegOnParExp 5");
    return SipSuccess;
}

/****************************************************************************
* Function Name : sipAlgDeleteContactsOnDeRegister
* Description     :Hash fetch Dereg Hash (key = pAORString) and Get the ip /port for which the 
*      expiry has happend. Now do hash fetch to NAT hash with this ip/port & reset
*      the alg timer for this =1 secs so that it expitres soon.  
* Input/Output (s) :  pAORString - "user@host"
* Returns    : SUCCESS/FAILURE
****************************************************************************/
SipBool
sipAlgDeleteContactsOnDeRegister (SIP_S8bit * pAORString)
{
    /* This pointer is used to avoid strict-aliasing warning */
    SIP_Pvoid           pTemp = SIP_NULL;
    SipError            err;
    SipAlgNatHashElement *pNatHashElement = SIP_NULL;
    SipAlgDeRegHashElement *pDeRegHashElement = SIP_NULL;
    SipAlgDeRegHashKey *pDeRegKey = SIP_NULL;

    SipError            dError = E_NO_ERROR;

    SIPDEBUG ((SIP_S8bit *)
              "SIP_DEBUG - Entering sipAlgDeleteContactsOnDeRegister");

#ifdef SIPALG_UT_FLAG
    PRINTF ("\n\n***The DeReg hash &  NAT hash entry before deletion\n\n");
    sipAlgNatHashPrint (&dGlbNatHash);
    sipAlgDeRegHashPrint (&dGlbDeRegHash);
#endif

    /* Allocate memory for NAT hash key */
    pDeRegKey = (SipAlgDeRegHashKey *)
        sip_memget (DECODE_MEM_ID, sizeof (SipAlgDeRegHashKey), &err);
    if (pDeRegKey == SIP_NULL)
    {
        return SipFail;
    }
    pDeRegKey->pAOR = sip_strdup (pAORString, 0);

    /* First Fetch the NATInfo Entry from the De Reg Hash Table
       with AOR  */
    pDeRegHashElement =
        (SipAlgDeRegHashElement *) sip_hashFetch (&dGlbDeRegHash, pDeRegKey);

    if (pDeRegHashElement != SIP_NULL)
    {
        SipAlgDeRegElement *pDeRegInfo = SIP_NULL;
        SIP_U32bit          dSize = 0;
        SIP_U32bit          u4Index = 0;
        SIP_U32bit          count = 0;

        if ((SipSuccess == sip_listSizeOf (pDeRegHashElement->
                                           pDeRegInfoList, &dSize, &dError)) &&
            (0 != dSize))
        {
            SipAlgNatHashKey    dHashKey;

            for (u4Index = 0; u4Index < dSize; u4Index++)
            {
                sip_listGetAt (pDeRegHashElement->pDeRegInfoList,
                               count, (SIP_Pvoid *) & pTemp, &dError);
                pDeRegInfo = (SipAlgDeRegElement *) pTemp;
                if (SipTrue == pDeRegInfo->isExpired)
                {
                    dHashKey.ipAddress = pDeRegInfo->ipAddress;
                    dHashKey.port = pDeRegInfo->port;
                    dHashKey.ifIndex = pDeRegInfo->ifIndex;
                    dHashKey.direction = SipPktOut;
                    /* Fetch the entry from the hash table */
                    pNatHashElement = (SipAlgNatHashElement *)
                        sip_hashFetch (&dGlbNatHash, (SIP_Pvoid) & dHashKey);

                    /*Donot delete the actual NAT Entries by calling deletenatportIPC why -  
                       because inthat case we will be not able to forward the 200 ok to end 
                       user for the DeReg case if via and contact use same port  
                       rather reset partial timer to 2 secs , which is sufficient time 
                       for NP to frwrd 200ok to end usr */

                    /*SipAlgDeleteNatEntryOnDeReg(pNatHashElement,&dHashKey); */

                    /* Start the SIP ALG timer with the duration of 1 secs 
                       To restart the timer, fetch the entries with private IP/port
                       Timer is running only for that NAT hash element. */
                    if (pNatHashElement != SIP_NULL)
                    {
                        if (pNatHashElement->pSipAlgTimerInfo != SIP_NULL)
                        {
                            SIP_U16bit          expiryValue = 1;
                            SIPDEBUG ((SIP_S8bit *) "Restarting Timer");

                            if (NatSipAlgReStartTimer
                                (pNatHashElement->pSipAlgTimerInfo,
                                 expiryValue) != NAT_SUCCESS)
                            {
                                sip_error (SIP_Minor,
                                           "Could not start SIP ALG timer for contact- for expiry");
                                sip_hashRelease (&dGlbNatHash,
                                                 (SIP_Pvoid) & dHashKey);
                                sip_hashRelease (&dGlbDeRegHash,
                                                 (SIP_Pvoid) pDeRegKey);
                                sip_memfree (DECODE_MEM_ID,
                                             (SIP_Pvoid) &
                                             pDeRegKey->pAOR, &err);
                                pTemp = pDeRegKey;
                                sip_memfree (DECODE_MEM_ID,
                                             (SIP_Pvoid *) & pTemp, &err);
                                pDeRegKey = SIP_NULL;
                                SIPDEBUG ((SIP_S8bit *) "SIP_DEBUG - "
                                          "Exiting sipAlgDeleteContactsOnDeRegister 1");
                                return SipFail;
                            }
                            (void) sip_trace (SIP_Detailed, SIP_Init,
                                              (SIP_S8bit *)
                                              "SIP ALG TRACE: About to expire in another 1 sec");
                        }
                        /* decrement refcount with hashRelease */
                        sip_hashRelease (&dGlbNatHash, (SIP_Pvoid) & dHashKey);
                    }
                    else        /*no NAT hash element */
                    {
                        SIPDEBUG ((SIP_S8bit *)
                                  "SIP_DEBUG -  No nat hash element present in NAT hash for"
                                  "this AOR of De Reg Hash.");
                        SIPDEBUG ((SIP_S8bit *) "SIP_DEBUG - "
                                  "Exiting sipAlgDeleteContactsOnDeRegister 2");
                    }

                    sip_listDeleteAt (pDeRegHashElement->pDeRegInfoList, count,
                                      &dError);
                    count--;
                }                /*If pDeRegInfo->isExpired */
                count++;
            }                    /* End of for Loop */
        }                        /* End of num of entries check */

        sip_listSizeOf (pDeRegHashElement->pDeRegInfoList, &dSize, &dError);
        sip_hashRelease (&dGlbDeRegHash, (SIP_Pvoid) pDeRegKey);
        if (dSize == 0)
        {
#ifdef SIPALG_UT_FLAG
            PRINTF ("----:NoError sipAlgDeleteContactsOnDeRegister "
                    "DeRegHashElement with listsize 0 so delete node.--");
#endif
            /* If there are no entries in the List, then remove the entire
               list. */
            if (SipFail == sip_hashRemove (&dGlbDeRegHash, pDeRegKey))
            {
                sip_error (SIP_Minor,
                           "Could not remove the  De Reg hash element.");
                SIPDEBUG ((SIP_S8bit *) "SIP_DEBUG - "
                          "Exiting sipAlgDeleteContactsOnDeRegister 3");
                sip_memfree (DECODE_MEM_ID,
                             (SIP_Pvoid) & pDeRegKey->pAOR, &err);
                pTemp = pDeRegKey;
                sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                pDeRegKey = SIP_NULL;
                return SipFail;
            }
        }
    }
    else                        /*pDeRegHashElement is not present i.e. it's NULL */
    {
        SIPDEBUG ((SIP_S8bit *) "We got 200 ok for DeReg & we dont havethis in"
                  " our de reg hash ");
    }

    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid) & pDeRegKey->pAOR, &err);
    pTemp = pDeRegKey;
    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
    pDeRegKey = SIP_NULL;

    /* element is list of contacts
       select the expired contacts. based on isExpired -
       reset the timer to very small time i.e. 4secs so that it expires soon */
#ifdef SIPALG_UT_FLAG
    PRINTF
        ("\n\n***The DeReg hash & NAT hash entry AFTER NAT & DEREG HASH deletion\n\n");
    sipAlgNatHashPrint (&dGlbNatHash);
    sipAlgDeRegHashPrint (&dGlbDeRegHash);
#endif
    SIPDEBUG ((SIP_S8bit *)
              "SIP_DEBUG - Exiting sipAlgDeleteContactsOnDeRegister 5");
    return SipSuccess;
}

/****************************************************************************
* Function Name : sipAlgAddContactToDeRegHash
* Description     :Hash fetch Dereg Hash (key = pAORString) and Get the ip /port for which the 
*      expiry has happend. Now do hash fetch to NAT hash with this ip/port & reset
*      the alg timer for this =1 secs so that it expitres soon.  
* Input/Output (s) :  pAORString - "user@host"
*                     ipaddr - ip address of contact
*                     port   - port of contact
*                     ifindex - interface index
* Returns    : SUCCESS/FAILURE
****************************************************************************/
SipBool
    sipAlgAddContactToDeRegHash
    (SIP_S8bit * pAORString, SIP_U32bit ipaddr, SIP_U16bit port,
     SIP_U32bit ifindex)
{
    /* This pointer is used to avoid strict-aliasing warning */
    SIP_Pvoid           pTemp = SIP_NULL;
    SipAlgDeRegHashElement *pDeRegHashElement = SIP_NULL;
    SipError            err;
    SipError            dError = E_NO_ERROR;
    SipAlgDeRegHashKey *pDeRegKey = SIP_NULL;
    SipAlgDeRegElement *pDeRegInfo = SIP_NULL;

    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgAddContactToDeRegHash");
#ifdef SIPALG_UT_FLAG
    PRINTF
        ("\n\n***The DeReg hash & NAT hash entry before adding contacts\n\n");
    sipAlgNatHashPrint (&dGlbNatHash);
    sipAlgDeRegHashPrint (&dGlbDeRegHash);
#endif

    /* form the key
       fetch the De Reg hash element
       if fetch pass then add into the list 
       else do hash init, listadd then hashadd  only if its not already in list */

    /* Allocate memory for NAT hash key */
    pDeRegKey = (SipAlgDeRegHashKey *)
        sip_memget (DECODE_MEM_ID, sizeof (SipAlgDeRegHashKey), &err);

    if (pDeRegKey == SIP_NULL)
    {
        return SipFail;
    }
    pDeRegKey->pAOR = sip_strdup (pAORString, 0);

    /* First Fetch the De Reg hash Entry from the De Reg Hash Table
       with AOR key and store it in the pDeRegHashElement. */
    pDeRegHashElement =
        (SipAlgDeRegHashElement *) sip_hashFetch (&dGlbDeRegHash, pDeRegKey);

    if (pDeRegHashElement != SIP_NULL)
    {
        SipAlgDeRegElement *pDeRegElement = SIP_NULL;
        SIP_U32bit          dSize = 0;
        SIP_U32bit          u4Index = 0;

        if ((SipSuccess == sip_listSizeOf (pDeRegHashElement->
                                           pDeRegInfoList, &dSize, &dError)) &&
            (0 != dSize))
        {
            for (u4Index = 0; u4Index < dSize; u4Index++)
            {
                sip_listGetAt (pDeRegHashElement->pDeRegInfoList,
                               u4Index, (SIP_Pvoid *) & pTemp, &dError);
                pDeRegElement = (SipAlgDeRegElement *) pTemp;

                /*check the ip and port in the dereg element */
                /*if it is same; return else append to list */
                if (pDeRegElement->ipAddress == ipaddr
                    && pDeRegElement->port == port)
                {                /*Dont add hash for the already existing contact */
#ifdef SIPALG_UT_FLAG
                    PRINTF
                        ("\n\n***The DeReg hash & NAT did not add since the ip n port are same \n\n");
                    sipAlgDeRegHashPrint (&dGlbDeRegHash);
#endif
                    sip_hashRelease (&dGlbDeRegHash, (SIP_Pvoid) pDeRegKey);
                    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid)
                                 & pDeRegKey->pAOR, &err);
                    pTemp = pDeRegKey;
                    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
                    pDeRegKey = SIP_NULL;

                    return SipSuccess;
                }
            }

            /* Allocate memory for dereginfo */
            pDeRegInfo = (SipAlgDeRegElement *)
                sip_memget (DECODE_MEM_ID, sizeof (SipAlgDeRegElement), &err);

            if (pDeRegInfo == SIP_NULL)
            {
                return SipFail;
            }
            pDeRegInfo->ipAddress = ipaddr;    /*Pvt IP */
            pDeRegInfo->port = port;
            pDeRegInfo->ifIndex = ifindex;
            pDeRegInfo->isExpired = 0;

            /* append the ip and port of the DeReg element into
               the list */
            sip_listAppend (pDeRegHashElement->pDeRegInfoList,
                            (SIP_Pvoid) pDeRegInfo, &dError);
            sip_hashRelease (&dGlbDeRegHash, (SIP_Pvoid) pDeRegKey);
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid) & pDeRegKey->pAOR, &err);
            pTemp = pDeRegKey;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
            pDeRegKey = SIP_NULL;
        }
#ifdef SIPALG_UT_FLAG
        if (dSize == 0)
            PRINTF
                ("----:ERROR: sipAlgAddContactToDeRegHash DeRegHashElement with listsize 0.--");
#endif
    }                            /*No De Reg Hash element */
    else                        /*Didnt get the hash element so do hash init, listadd then hashadd  */
    {
        SipAlgDeRegHashElement *pNewDeRegHashEntry = SIP_NULL;

        /*Init DeReg hash */
        if (sipAlgInitDeRegHashElement (&pNewDeRegHashEntry) == SipFail)
        {
            sip_error (SIP_Minor, "SIP-ERROR: sipAlgAddContactToDeRegHash: "
                       "sipAlgInitDeRegHashElement failed\n");
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid) & pDeRegKey->pAOR, &err);
            pTemp = pDeRegKey;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
            pDeRegKey = SIP_NULL;
            return SipFail;
        }

        /* Allocate memory for dereginfo */
        pDeRegInfo = (SipAlgDeRegElement *)
            sip_memget (DECODE_MEM_ID, sizeof (SipAlgDeRegElement), &err);
        if (pDeRegInfo == SIP_NULL)
        {
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid) (&pNewDeRegHashEntry),
                         &err);
            return SipFail;
        }
        pDeRegInfo->ipAddress = ipaddr;    /*Pvt IP */
        pDeRegInfo->port = port;
        pDeRegInfo->ifIndex = ifindex;
        pDeRegInfo->isExpired = 0;
        /* Append the DeReg Element into the DeReg Info List. */
        sip_listAppend (pNewDeRegHashEntry->pDeRegInfoList,
                        (SIP_Pvoid) pDeRegInfo, &dError);

        if (SipFail == sip_hashAdd (&dGlbDeRegHash,
                                    (SIP_Pvoid) pNewDeRegHashEntry,
                                    (SIP_Pvoid) pDeRegKey))
        {
            sip_error (SIP_Minor, "sip_hashAdd failed in "
                       "sipAlgAddContactToDeRegHash");
            pTemp = pDeRegInfo;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
            pDeRegInfo = SIP_NULL;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid) & pDeRegKey->pAOR, &err);
            pTemp = pDeRegKey;
            sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
            pDeRegKey = SIP_NULL;
            return SipFail;
        }
    }                            /*else didnt get hash element */

#ifdef SIPALG_UT_FLAG
    PRINTF ("\n\n***The DeReg hash & NAT hash entry after adding contacts\n\n");
    sipAlgNatHashPrint (&dGlbNatHash);
    sipAlgDeRegHashPrint (&dGlbDeRegHash);
#endif

    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgAddContactToDeRegHash");
    return SipSuccess;
}

/****************************************************************************
* Function Name : sipAlgUpdateExpiredContacts
* Description     : isexpired field is set to true fo rthe contacts which are expired 
* Input/Output (s) :  pAORString - "user@host"
*                     isWildcard - is it wild card or not in contact- 
*                                  (expires all contacts)
*                     ipaddr - ip address of contact
*                     port   - port of contact
*                     ifindex - interface index
* Returns    : SUCCESS/FAILURE
****************************************************************************/
void 
     
     
     
     
     
     
     
    sipAlgUpdateExpiredContacts
    (SIP_S8bit * pAORString, en_SipBoolean isWildcard, SIP_U32bit ipaddr,
     SIP_U16bit port)
{
    /* This pointer is used to avoid strict-aliasing warning */
    SIP_Pvoid           pTemp = SIP_NULL;
    SipAlgDeRegHashElement *pDeRegHashElement = SIP_NULL;
    SipError            err;
    SipError            dError = E_NO_ERROR;
    SipAlgDeRegHashKey *pDeRegKey = SIP_NULL;

    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgUpdateExpiredContacts");

    /* form the key
       fetch the De Reg hash element
       if fetch pass - update the isExpired =true 
       else do nothing just say an error msg   */

    /* Allocate memory for NAT hash key */
    pDeRegKey = (SipAlgDeRegHashKey *)
        sip_memget (DECODE_MEM_ID, sizeof (SipAlgDeRegHashKey), &err);
    if (pDeRegKey == SIP_NULL)
    {
        return;
    }
    pDeRegKey->pAOR = sip_strdup (pAORString, 0);

    /* First Fetch the De Reg hash Entry from the De Reg Hash Table
       with AOR key and store it in the pDeRegHashElement. */
    pDeRegHashElement =
        (SipAlgDeRegHashElement *) sip_hashFetch (&dGlbDeRegHash, pDeRegKey);

    if (pDeRegHashElement != SIP_NULL)
    {                            /*if IsExpired =True, update the IsExpired of all the contacts for this AOR */
        SipAlgDeRegElement *pDeRegElement = SIP_NULL;
        SIP_U32bit          dSize = 0;
        SIP_U32bit          u4Index = 0;

        if ((SipSuccess == sip_listSizeOf (pDeRegHashElement->
                                           pDeRegInfoList, &dSize, &dError)) &&
            (0 != dSize))
        {
            for (u4Index = 0; u4Index < dSize; u4Index++)
            {
                sip_listGetAt (pDeRegHashElement->pDeRegInfoList, u4Index,
                               (SIP_Pvoid *) & pTemp, &dError);
                pDeRegElement = (SipAlgDeRegElement *) pTemp;
                /* check the ip and port in the dereg element */
                if (SipTrue == isWildcard)
                {
                    pDeRegElement->isExpired = SipTrue;
                }
                else if (pDeRegElement->ipAddress == ipaddr &&
                         pDeRegElement->port == port)
                {
                    pDeRegElement->isExpired = SipTrue;
                    break;
                }
            }
        }

#ifdef SIPALG_UT_FLAG
        if (dSize == 0)
            PRINTF
                ("----:ERROR:sipAlgUpdateExpiredContacts DeRegHashElement with list size 0.--");
#endif
        sip_hashRelease (&dGlbDeRegHash, (SIP_Pvoid) pDeRegKey);
    }
    else
    {
        SIPDEBUG ((SIP_S8bit *) "No De Reg hash element to update isExpired");
    }
    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid) & pDeRegKey->pAOR, &err);
    pTemp = pDeRegKey;
    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pTemp, &err);
    pDeRegKey = SIP_NULL;
    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgUpdateExpiredContacts");
}

/****************************************************************************
* Function Name : sipAlgInitDeRegHashElement
* Description     : Initialize the DeReg Hash table for a new entry.
*                 
* Input/Output (s) :  ppNatHashElement - The element to be initialed
*                      pKey - Key for NAT hash table    
* Returns    : SUCCESS/FAILURE
****************************************************************************/
SipBool
sipAlgInitDeRegHashElement (SipAlgDeRegHashElement ** ppDeRegHashElement)
{
    SipError            err;
    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgInitDeRegHashElement");

    (*ppDeRegHashElement) =
        (SipAlgDeRegHashElement *) sip_memget (DECODE_MEM_ID,
                                               sizeof (SipAlgDeRegHashElement),
                                               &err);
    if ((*ppDeRegHashElement) == SIP_NULL)
    {
        sip_error (SIP_Minor, "Could not get the required memory");
        return SipFail;
    }

    /* Allocate memory for the list */
    (*ppDeRegHashElement)->pDeRegInfoList =
        (SipList *) sip_memget (DECODE_MEM_ID, sizeof (SipList), &err);

    /* Initialize the Destination IP/Port list */
    if (sip_listInit ((*ppDeRegHashElement)->pDeRegInfoList,
                      sipAlgFreeDeRegInfoList, &err) == SipFail)
    {
        sip_error (SIP_Minor, "Could not initialize DeReginfo list.");
        sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) (ppDeRegHashElement), &err);
        return SipFail;
    }

    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgInitDeRegHashElement");
    return SipSuccess;
}

/****************************************************************************
* Function Name : sipAlgFreeDeRegInfoList
* Description     : This is the function registered at the time of list init
*    Once list element is deleted, this function gets called (callback)
*                 
* Input/Output (s) :  pDeRegElement - The element in the list
* Returns    : Nothing
****************************************************************************/
void                (sipAlgFreeDeRegInfoList) (SIP_Pvoid pDeRegElement)
{
    SipError            err;
    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgFreeDeRegInfoList");
    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid *) & pDeRegElement, &err);
    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgFreeDeRegInfoList");
}
