/********************************************************************
 * Copyright (C) Aricent Communications Sotware,2007
 *
 * $Id: sipalg_hash.c,v 1.6 2012/12/21 12:00:42 siva Exp $
 *
 *******************************************************************/

#include "natinc.h"
#include "sipalg_inc.h"
#ifdef SDF_PASSTHROUGH
Sdf_st_logPassthruDelay *st_Invite;
Sdf_st_logPassthruDelay *st_200INV;
Sdf_st_logPassthruDelay *st_ACK;
Sdf_st_logPassthruDelay *st_BYE;
Sdf_st_logPassthruDelay *st_200BYE;
Sdf_st_logPassthruDelay *st_REG;
Sdf_st_logPassthruDelay *st_200REG;
#endif

/*This is the Data structure used by the Session Hash Table
  maintained in the SIP ALG. */
SipHash             dGlbMediaHash;

/* This is the Data structure used by the Register Contact Information
   Hash Table maintained in the SIP ALG. */
SipHash             dGlbNatHash;

/* SIPALG - 2.1 */
/* 
    This is the Data structure used by the SIP ALG where the
       IP address and mapped IP is maintaiend. (Used for translation
       of O-line) 
*/
SipHash             dGlbIPHash;

/* De-Register Hash : for handling the DeRegistration case, 
   remove nathash entries which are expired  or deregistered*/
SipHash             dGlbDeRegHash;

/* sipserver status global variable initialization */
en_sipStatus        dGlbSipStatus = en_sipDisable;

#define SIPLAG_PORTMAPPING_TIMEOUT 60

/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:     sipAlgInitialization  */
/*  */
/*  DESCRIPTION: Function is to initialize all the data structure used by */
/*  the SIP ALG. It initialize the RegContact Info Hash Table  which is used  */
/*  for maintaining the private used registered contacts with the SSE. . It */
/*  initializes the Session Information Hash Table which maintains  the NAT */
/*  addresss ,Bindings and the pinholes. It also initializes the Timer which */
/*  is maintained by the SIP ALG for the Register Contact Information as */
/*  well as SDP information. */
/* //////////////////////////////////////////////////////////////////////////// */
SipBool
sipAlgInitialization (void)
{
    /* Initialize the  BindingInfo Hash Table and register all the hashing  */
    /* function used by the SIP ALG. This hash Table  which is used  */
    /*  for maintaining the private used registered contacts with the SSE. */

    SipError            dError = E_NO_ERROR;
    SIPDEBUG ((SIP_S8bit *) "SIP_DEBUG - Inside sipAlgInitialization");
    sip_hashInit (&dGlbNatHash, sipAlgNatHashFunc,
                  sipAlgCompareNatHashKeys,
                  sipAlgRemoveEntryFromNatHash,
                  sipAlgFreeNatHashKey,
                  SIP_ALG_NUMBER_OF_HASH_BUCKETS,
                  SIP_ALG_MAX_NUMCALLS, &dError);

    /* Initialize the  Session Hash Table and register all the hashing function */
    /* used by the SIP ALG. It maintains  the created NAT addresss ,Bindings and  */
    /* the pinholes. */
    sip_hashInit (&dGlbMediaHash, sipAlgMediaHashFunc,
                  sipAlgCompareMediaHashKeys, sipAlgRemoveEntryFromMediaHash,
                  sipAlgFreeMediaHashKey, SIP_ALG_NUMBER_OF_HASH_BUCKETS,
                  SIP_ALG_MAX_NUMCALLS, &dError);

    /* SIPALG - 2.1 */
    /* Initialize the IP Hash Table and register all the hashing function */
    /* used by the SIP ALG. It maintains the private and mapped addresss */
    sip_hashInit (&dGlbIPHash, sipAlgIPHashFunc,
                  sipAlgCompareIPHashKeys, sipAlgRemoveEntryFromIPHash,
                  sipAlgFreeIPHashKey, SIP_ALG_NUMBER_OF_HASH_BUCKETS,
                  SIP_ALG_MAX_NUMCALLS, &dError);

    /* Initialize the DeReg Hash */
    sip_hashInit (&dGlbDeRegHash, sipAlgDeRegHashFunc,
                  sipAlgCompareDeRegHashKeys, sipAlgRemoveEntryFromDeRegHash,
                  sipAlgFreeDeRegHashKey, SIP_ALG_NUMBER_OF_HASH_BUCKETS,
                  SIP_ALG_MAX_NUMCALLS, &dError);

    /* Dynamic entries were getting deleted even if the media was flowing.
       Now by calling this API, the dynamic entries are not getting deleted. 
       This is the fix suggested by chennai datacom team  
       AricentNATSetPortMappingTimeout_ipc (SIPLAG_PORTMAPPING_TIMEOUT); */

#ifdef SDF_PASSTHROUGH
    st_Invite =
        MEM_MALLOC (sizeof (Sdf_st_logPassthruDelay), Sdf_st_logPassthruDelay);
    st_200INV =
        MEM_MALLOC (sizeof (Sdf_st_logPassthruDelay), Sdf_st_logPassthruDelay);
    st_ACK =
        MEM_MALLOC (sizeof (Sdf_st_logPassthruDelay), Sdf_st_logPassthruDelay);
    st_BYE =
        MEM_MALLOC (sizeof (Sdf_st_logPassthruDelay), Sdf_st_logPassthruDelay);
    st_200BYE =
        MEM_MALLOC (sizeof (Sdf_st_logPassthruDelay), Sdf_st_logPassthruDelay);
    st_REG =
        MEM_MALLOC (sizeof (Sdf_st_logPassthruDelay), Sdf_st_logPassthruDelay);
    st_200REG =
        MEM_MALLOC (sizeof (Sdf_st_logPassthruDelay), Sdf_st_logPassthruDelay);
#endif
    return SipSuccess;
}

/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:     sipAlgInit  */
/*  */
/*  DESCRIPTION: Function is to reset the global variables used in interface */
/*  info strcuture. On "sip disable" the hash entries are cleared. On "sip enable" */
/*  initialize the interface info structure so that again on a new "Via", the  */
/*  entries for signaling will be created. */
/*  sipAlgInitialization function will be invoked only once in CAS. But this  */
/*  function will be invoked multiple times (based on "sip enable" command) */
/* //////////////////////////////////////////////////////////////////////////// */
void
sipAlgInit (void)
{
    SIP_U32bit          i;
    SIPDEBUG ((SIP_S8bit *) "SIP_DEBUG - Inside sipAlgInit");

    /* set the global sipserver status */
    dGlbSipStatus = en_sipEnable;

    for (i = 0; i < MAX_WAN_LINKS; i++)
    {
        sip_memset ((SIP_Pvoid) (&dGlbInterfaceInfo[i]), 0,
                    sizeof (sipAlgInterfaceInfo));
    }
#ifdef SDF_PASSTHROUGH
    if (PASSTHROUGH_ENABLE == SipGetAlgPassThrough ())
    {
        st_Invite->i4PeakSec = 0;
        st_Invite->i4PeakUsec = 0;
        st_Invite->AvgSec = 0;
        st_Invite->AvgUsec = 0;
        st_Invite->i2NumInvPeak = 0;
        st_Invite->i2NumInvite = 0;
        for (i = 0; i <= 15; i++)
        {
            st_Invite->ai2Band[i] = 0;
        }

        st_200INV->i4PeakSec = 0;
        st_200INV->i4PeakUsec = 0;
        st_200INV->AvgSec = 0;
        st_200INV->AvgUsec = 0;
        st_200INV->i2NumInvPeak = 0;
        st_200INV->i2NumInvite = 0;
        for (i = 0; i <= 15; i++)
        {
            st_200INV->ai2Band[i] = 0;
        }

        st_ACK->i4PeakSec = 0;
        st_ACK->i4PeakUsec = 0;
        st_ACK->AvgSec = 0;
        st_ACK->AvgUsec = 0;
        st_ACK->i2NumInvPeak = 0;
        st_ACK->i2NumInvite = 0;
        for (i = 0; i <= 15; i++)
        {
            st_ACK->ai2Band[i] = 0;
        }

        st_BYE->i4PeakSec = 0;
        st_BYE->i4PeakUsec = 0;
        st_BYE->AvgSec = 0;
        st_BYE->AvgUsec = 0;
        st_BYE->i2NumInvPeak = 0;
        st_BYE->i2NumInvite = 0;
        for (i = 0; i <= 15; i++)
        {
            st_BYE->ai2Band[i] = 0;
        }

        st_200BYE->i4PeakSec = 0;
        st_200BYE->i4PeakUsec = 0;
        st_200BYE->AvgSec = 0;
        st_200BYE->AvgUsec = 0;
        st_200BYE->i2NumInvPeak = 0;
        st_200BYE->i2NumInvite = 0;
        for (i = 0; i <= 15; i++)
        {
            st_200BYE->ai2Band[i] = 0;
        }

        st_200REG->i4PeakSec = 0;
        st_200REG->i4PeakUsec = 0;
        st_200REG->AvgSec = 0;
        st_200REG->AvgUsec = 0;
        st_200REG->i2NumInvPeak = 0;
        st_200REG->i2NumInvite = 0;
        for (i = 0; i <= 15; i++)
        {
            st_200REG->ai2Band[i] = 0;
        }

        st_REG->i4PeakSec = 0;
        st_REG->i4PeakUsec = 0;
        st_REG->AvgSec = 0;
        st_REG->AvgUsec = 0;
        st_REG->i2NumInvPeak = 0;
        st_REG->i2NumInvite = 0;
        for (i = 0; i <= 15; i++)
        {
            st_REG->ai2Band[i] = 0;
        }
    }
#endif

    SIPDEBUG ((SIP_S8bit *) "SIP_DEBUG - Existing sipAlgInit");
}

/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:     sipAlgDeInit */
/*  */
/*  DESCRIPTION: Function is to de-initialize all the data structure used by */
/*  the SIP ALG. It de-initializes the NAT Hash Table which is used for */
/*  all the binding and pinhole information for any IP/port combination.     */
/*  It de-initializes the Media Hash Table which maintains the NAT */
/*  addresss, Bindings and the pinholes corresponding to a dialog. */
/*  When "sip disable" is invoked, that is when the hash de-init done. */
/* //////////////////////////////////////////////////////////////////////////// */
void
sipAlgDeInit (void)
{
    SipHashIterator     dIterator;
    SipAlgNatHashElement *pNatHashElement = SIP_NULL;
    SIP_Pvoid           pTempKey = SIP_NULL;

    SIPDEBUG ((SIP_S8bit *) "SIP_DEBUG - Inside sipAlgDeInit");
    /* set the global sipserver status */
    dGlbSipStatus = en_sipDisable;

#ifndef SECURITY_KERNEL_WANTED
#ifdef SDF_PASSTHROUGH
    /*open the file, write the passthru delays for all msgs and close it */
    if (PASSTHROUGH_ENABLE == SipGetAlgPassThrough ())
    {
        Sdf_fn_passThruPrintToFile ();
    }
#endif
#endif
    /*  Clear NAT hash table  */
    sip_hashInitIterator (&dGlbNatHash, &dIterator);

    /*  Go through the hash table entries and remove them */
    while (SIP_NULL != dIterator.pCurrentElement)
    {
        pTempKey = dIterator.pCurrentElement->pKey;
        pNatHashElement =
            (SipAlgNatHashElement *) (dIterator.pCurrentElement->pElement);
        if (pNatHashElement != SIP_NULL)
        {
            /*  Stop the SIP ALG timer */
            if (pNatHashElement->pSipAlgTimerInfo != SIP_NULL)
            {
                if (pNatHashElement->pSipAlgTimerInfo->u1TimerStatus ==
                    NAT_ENABLE)
                {
                    NatSipAlgStopTimer (pNatHashElement->pSipAlgTimerInfo);
                }
            }
        }
        sip_hashRemove (&dGlbNatHash, (SIP_Pvoid) pTempKey);
        sip_hashNext (&dGlbNatHash, &dIterator);
    }
    /*  Even on "sip disable" the hashfree need not be called as it is a  */
    /*  global hash table. */
    /*  sip_hashFree(&dGlbNatHash, &dError); */

    /*  Clear Media hash table  */
    sip_hashInitIterator (&dGlbMediaHash, &dIterator);

    /*  Go through the hash table entries and remove them */
    while (SIP_NULL != dIterator.pCurrentElement)
    {
        pTempKey = dIterator.pCurrentElement->pKey;
        sip_hashRemove (&dGlbMediaHash, (SIP_Pvoid) pTempKey);
        sip_hashNext (&dGlbMediaHash, &dIterator);
    }
    /*  Even on "sip disable" the hashfree need not be called as it is a  */
    /*  global hash table. */
    /*  sip_hashFree(&dGlbMediaHash, &dError); */

    /*  De Reg start: flush dereg on sip disable */
    /* Clear DeReg hash table  */
    sip_hashInitIterator (&dGlbDeRegHash, &dIterator);

    /*  Go through the hash table entries and remove them */
    while (SIP_NULL != dIterator.pCurrentElement)
    {
        pTempKey = dIterator.pCurrentElement->pKey;
        sip_hashRemove (&dGlbDeRegHash, (SIP_Pvoid) pTempKey);
        sip_hashNext (&dGlbDeRegHash, &dIterator);
    }
    /*  De Reg end */

    SIPDEBUG ((SIP_S8bit *) "SIP_DEBUG - Exiting sipAlgDeInit");
}

/* //////////////////////////////////////////////////////////////////////////// */
/* FUNCTION:      sipAlgFreeNatHashKey */
/*  */
/*  DESCRIPTION: Function to free the key for the hash table (call-id) */
/* //////////////////////////////////////////////////////////////////////////// */
void
sipAlgFreeNatHashKey (void *pKey)
{
    sip_memfree (0, (void **) &pKey, SIP_NULL);
}

/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:      sipAlgRemoveEntryFromNatHash */
/*   */
/*  DESCRIPTION: Function to free the data that is stored in the hash. */
/*  */
/* //////////////////////////////////////////////////////////////////////////// */
void
sipAlgRemoveEntryFromNatHash (void *pElement)
{
    /* This pointer is used to avoid strict-aliasing warning */
    SIP_Pvoid           pTemp = SIP_NULL;
    SIP_U32bit          dSize = 0;
    SIP_U32bit          dSize1 = 0;

    SipError            dError = E_NO_ERROR;

#ifdef SIP_DEBUG
    SIPDEBUG ((SIP_S8bit *) "SIP_DEBUG - Inside sipAlgRemoveEntryFromNatHash");
#endif
    SipAlgNatHashElement *pTempElement = (SipAlgNatHashElement *) pElement;

    if (SIP_NULL == pTempElement)
    {
        return;
    }

    if ((SipSuccess == sip_listSizeOf (pTempElement->
                                       pDestInfoList, &dSize, &dError)) &&
        (0 != dSize))
    {
        /*  Compare the private IP, private port and outside IP */
        /*  outside port fetched from the pNatInfo (ANY, ANY) */
        /*  with the value stored in the session Info Hash Table. */
        if (sip_listDeleteAll (pTempElement->pDestInfoList, &dError) == SipFail)
        {
            sip_error (SIP_Minor, "sipAlgRemoveEntryFromNatHash "
                       "sip_listDeleteAll Failed for pDestInfoList");
        }
    }                            /* End of num of entries check */

    if ((SipSuccess == sip_listSizeOf (pTempElement->
                                       pCallInfoList, &dSize1, &dError)) &&
        (0 != dSize1))
    {
        /*  Compare the private IP, private port and outside IP */
        /*  outside port fetched from the pNatInfo (ANY, ANY) */
        /*  with the value stored in the session Info Hash Table. */
        if (sip_listDeleteAll (pTempElement->pCallInfoList, &dError) == SipFail)
        {
            sip_error (SIP_Minor, "SIP-ERROR : sipAlgRemoveEntryFromNatHash: "
                       "sip_listDeleteAll Failed for pCallInfoList ");
        }
    }
    /*deleting timer hash key */
/* #ifndef SIPALG_UT_FLAG */
    if (pTempElement->pSipAlgTimerInfo != SIP_NULL)
    {
        if (pTempElement->pSipAlgTimerInfo->u1TimerStatus == NAT_ENABLE)
        {
            NatSipAlgStopTimer (pTempElement->pSipAlgTimerInfo);
        }

        /*for De Reg */
        /*De Reg hash for partial entry removal */
        if (pTempElement->pSipAlgTimerInfo->pDeRegAOR)
        {
            sipAlgDeleteDeRegOnParExp (pTempElement->pSipAlgTimerInfo->
                                       pDeRegAOR, pTempElement->originalIP,
                                       pTempElement->originalPort);

#ifdef SIPALG_UT_FLAG
            PRINTF ("\n\n--=Address of algtimer->pDeRegAOR in "
                    " sipAlgRemoveEntryFromNatHash %p\n",
                    &pTempElement->pSipAlgTimerInfo->pDeRegAOR);
#endif
            sip_memfree (0,
                         (SIP_Pvoid) & (pTempElement->
                                        pSipAlgTimerInfo->pDeRegAOR), &dError);
            pTempElement->pSipAlgTimerInfo->pDeRegAOR = NULL;
        }

        sip_memfree (0,
                     (SIP_Pvoid) & (pTempElement->
                                    pSipAlgTimerInfo->pHashKey), &dError);
        sip_memfree (0,
                     (SIP_Pvoid) & (pTempElement->pSipAlgTimerInfo), &dError);
    }
/* #endif */
    sip_listSizeOf (pTempElement->pDestInfoList, &dSize, &dError);
    sip_listSizeOf (pTempElement->pCallInfoList, &dSize1, &dError);

    if (dSize == 0 && dSize1 == 0)
    {
        /* If there  is no entries in the List , then  remove the entire */
        /* list. */
        pTemp = pTempElement->pDestInfoList;
        sip_memfree (0, (SIP_Pvoid *) & (pTemp), &dError);
        pTempElement->pDestInfoList = NULL;
        pTemp = pTempElement->pCallInfoList;
        sip_memfree (0, (SIP_Pvoid *) & (pTemp), &dError);
        pTempElement->pCallInfoList = NULL;
        sip_memfree (0, (SIP_Pvoid *) & pElement, &dError);
    }
#ifdef SIP_DEBUG
    SIPDEBUG ((SIP_S8bit *) "SIP_DEBUG - Exiting sipAlgRemoveEntryFromNatHash");
#endif
    return;
}

/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:      sipAlgCompareNatHashKeys */
/*  */
/*  DESCRIPTION: Comparison function for Register Contact Info hash keys. */
/*  If both keys matches , then this function return Zero as the success */
/*  value otherwise it return 1 as the failure value */
/*  */
/* //////////////////////////////////////////////////////////////////////////// */
SIP_U8bit
sipAlgCompareNatHashKeys (void *pKey1, void *pKey2)
{
#ifdef SIP_DEBUG
    SIPDEBUG ((SIP_S8bit *) "SIP_DEBUG -  sipAlgCompareNatHashKeys ");
#endif
    SipAlgNatHashKey   *pStoredKey = (SipAlgNatHashKey *) pKey1;
    SipAlgNatHashKey   *pQueryKey = (SipAlgNatHashKey *) pKey2;

    /* Allow the packets to come in on any interface. This is to
       handle the case when one WAN interface is down and the
       packet is still routable through another WAN interface */
    if (pQueryKey->direction == SipPktIn)
    {
        if ((pStoredKey->ipAddress == pQueryKey->ipAddress) &&
            (pStoredKey->port == pQueryKey->port))
            return 0;
        else
            return 1;
    }
    else
    {
        /* For an outgoing packet, the comparison should be 
           interface specific */
        if ((pStoredKey->ipAddress == pQueryKey->ipAddress) &&
            (pStoredKey->port == pQueryKey->port) &&
            (pStoredKey->ifIndex == pQueryKey->ifIndex))
            return 0;
        else
            return 1;
    }
}

/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:      sipAlgNatHashFunc */
/*  */
/*  DESCRIPTION: Function to calculate the hash value given a key as input. */
/*  */
/* //////////////////////////////////////////////////////////////////////////// */
SIP_U32bit
sipAlgNatHashFunc (void *pData)
{
    SipAlgNatHashKey   *pKey = (SipAlgNatHashKey *) pData;
    SIP_U8bit           pTempKey[SIP_ALG_MAX_100CHARS];
    SIP_U32bit          dKey;
    if (pKey->direction == SipPktOut)
    {
        /* When the packet flows out, the nat entries will be interface
           specific. Based on the wan interface the hash comparison
           should be done */
        sip_snprintf ((CHR1 *) pTempKey, SIP_ALG_MAX_100CHARS - 1, "%d%d%d",
                      pKey->ipAddress, pKey->port, pKey->ifIndex);
    }
    else
    {
        /* When the packet flows in, the reverse mapping will be based 
           on ip address and port. The incoming packet is allowed to come on 
           any interface. So the hashing should be done without interface 
           for reverse lookup (while adding the entry) */
        sip_snprintf ((CHR1 *) pTempKey, SIP_ALG_MAX_100CHARS - 1, "%d%d",
                      pKey->ipAddress, pKey->port);
    }
    dKey = sip_elfHash (pTempKey);
    return dKey;
}

/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:      sipAlgFreeMediaHashKey */
/*  */
/*  DESCRIPTION: Function to free the key for the hash table (call-id) */
/* //////////////////////////////////////////////////////////////////////////// */
void
sipAlgFreeMediaHashKey (void *pKey)
{

    SipAlgMediaKey     *pTempKey = (SipAlgMediaKey *) pKey;
#ifdef SIP_DEBUG
    SIPDEBUG ((SIP_S8bit *) "SIP_DEBUG -  Inside sipAlgFreeMediaHashKey ");
#endif

    if (pKey != SIP_NULL)
    {
        sip_memfree (0, (SIP_Pvoid) & (pTempKey->pFromTag), SIP_NULL);
        sip_memfree (0, (SIP_Pvoid) & (pTempKey->pToTag), SIP_NULL);
        sip_memfree (0, (SIP_Pvoid) & (pTempKey->pCallID), SIP_NULL);
        sip_memfree (0, (SIP_Pvoid *) & pKey, SIP_NULL);
    }
#ifdef SIP_DEBUG
    SIPDEBUG ((SIP_S8bit *) "SIP_DEBUG -  Exiting sipAlgFreeMediaHashKey ");
#endif
}

/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:      sipAlgRemoveEntryFromMediaHash */
/*  */
/*  DESCRIPTION: Function to free the data that is stored in the hash. */
/*  */
/* //////////////////////////////////////////////////////////////////////////// */
void
sipAlgRemoveEntryFromMediaHash (void *pData)
{
    /* This pointer is used to avoid strict-aliasing warning */
    SIP_Pvoid           pTemp = SIP_NULL;
    SipMediaHashElement *pMediaInfoEntry = (SipMediaHashElement *) pData;
    SIP_U32bit          dSize = 0;
    SipError            dError = E_NO_ERROR;
    SIP_U32bit          dElementFreed = 0;
#ifdef SIP_DEBUG
    SIPDEBUG ((SIP_S8bit *)
              "SIP_DEBUG -  Inside sipAlgRemoveEntryFromMediaHash ");
#endif

    if ((SipSuccess == sip_listSizeOf (pMediaInfoEntry->
                                       pMediaInfoList, &dSize, &dError)) &&
        (0 != dSize))
    {
        /*  Compare the private IP, private port and outside IP */
        /*  outside port fetched from the pNatInfo (ANY, ANY) */
        /*  with the value stored in the session Info Hash Table. */
        if (sip_listDeleteAll (pMediaInfoEntry->pMediaInfoList, &dError)
            == SipFail)
        {
            sip_error (SIP_Minor, "SIP-ERROR: sipAlgRemoveEntryFromMediaHash:"
                       "sip_listDeleteAll Failed for pMediaInfoList");
        }
    }                            /* End of num of entries check */

    sip_listSizeOf (pMediaInfoEntry->pMediaInfoList, &dSize, &dError);
    if (dSize == dElementFreed)
    {
        /* If there  is no entries in the List , then  remove the entire */
        /* list. */
        pTemp = pMediaInfoEntry->pMediaInfoList;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, &dError);
        pMediaInfoEntry->pMediaInfoList = NULL;
        pTemp = pMediaInfoEntry;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, &dError);
        pMediaInfoEntry = NULL;
    }
#ifdef SIP_DEBUG
    SIPDEBUG ((SIP_S8bit *)
              "SIP_DEBUG -  Exiting sipAlgRemoveEntryFromMediaHash ");
#endif

}

/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:      sipAlgCompareMediaHashKeys */
/*  */
/*  DESCRIPTION: Comparison function for session hash keys. */
/*  If both keys matches , then this function return 0 as the success */
/*  value otherwise it return 1 as the failure value */
/*  */
/* //////////////////////////////////////////////////////////////////////////// */
SIP_U8bit
sipAlgCompareMediaHashKeys (void *pKey1, void *pKey2)
{
    SipAlgMediaKey     *pStoredKey = (SipAlgMediaKey *) pKey1;
    SipAlgMediaKey     *pQueryKey = (SipAlgMediaKey *) pKey2;
#ifdef SIP_DEBUG
    SIPDEBUG ((SIP_S8bit *) "SIP_DEBUG -  Inside sipAlgCompareMediaHashKeys ");
#endif
    if (SIP_NULL == pQueryKey->pToTag)
    {
        if (pStoredKey->pToTag != SIP_NULL)
            return 1;

        /*To handle strcmp from dumping */
        if (SIP_NULL == pQueryKey->pFromTag)
            return 1;

        /* Case : where ToTag is not present in the Query Key. */
        if ((sip_strcmp (pStoredKey->pFromTag,
                         pQueryKey->pFromTag) == 0) &&
            (sip_strcmp (pStoredKey->pCallID, pQueryKey->pCallID) == 0))
        {
            return 0;
        }
    }                            /* End of if case ->NULL check for (SIP_NULL == pQueryKey->pToTag) */
    else
    {
        /* Case : where ToTag present in the Query Key. */
        if (SIP_NULL == pStoredKey->pToTag)
            return 1;

        /*To handle strcmp from dumping */
        if (SIP_NULL == pQueryKey->pFromTag)
            return 1;

        /* Now both Query Key and Stored Key has To-Tag. */
        if ((sip_strcmp (pStoredKey->pFromTag,
                         pQueryKey->pFromTag) == 0) &&
            (sip_strcmp (pStoredKey->pCallID,
                         pQueryKey->pCallID) == 0) &&
            (sip_strcmp (pStoredKey->pToTag, pQueryKey->pToTag) == 0))
        {
            return 0;
        }
        else
        {
            /*  Now compare by swapping From Tag and To Tag  */
            if ((sip_strcmp (pStoredKey->pFromTag,
                             pQueryKey->pToTag) == 0) &&
                (sip_strcmp (pStoredKey->pCallID,
                             pQueryKey->pCallID) == 0) &&
                (sip_strcmp (pStoredKey->pToTag, pQueryKey->pFromTag) == 0))
            {
                return 0;
            }
        }
    }                            /* End of else case -> NULL check for (SIP_NULL == pQueryKey->pToTag) */
#ifdef SIP_DEBUG
    SIPDEBUG ((SIP_S8bit *) "SIP_DEBUG -  Exiting sipAlgCompareMediaHashKeys ");
#endif
    return 1;

}

/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:      sipAlgMediaHashFunc */
/*  */
/*  DESCRIPTION: Function to calculate the hash value given a key as input. */
/*  */
/* //////////////////////////////////////////////////////////////////////////// */
SIP_U32bit
sipAlgMediaHashFunc (void *pData)
{
    SipAlgMediaKey     *pKey = (SipAlgMediaKey *) pData;

    if (pKey->pCallID != SIP_NULL)
    {
        /* Hash the Call-ID and get  the bucket. */
        return (sip_elfHash (pKey->pCallID));
    }
    return 0;
}

    /* SIPALG - 2.1 */
/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:      sipAlgIPHashFunc */
/*  */
/*  DESCRIPTION: Function to calculate the hash value given a key as input. */
/*  */
/* //////////////////////////////////////////////////////////////////////////// */
SIP_U32bit
sipAlgIPHashFunc (void *pData)
{
    SipAlgIPHashKey    *pKey = (SipAlgIPHashKey *) pData;

    if (pKey->pCallID != SIP_NULL)
    {
        /* Hash the Call-ID and get  the bucket. */
        return (sip_elfHash (pKey->pCallID));
    }
    return 0;
}

/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:      sipAlgCompareIPHashKeys */
/*  */
/*  DESCRIPTION: Comparison function for session hash keys. */
/*  If both keys matches , then this function return 0 as the success */
/*  value otherwise it return 1 as the failure value */
/*  */
/* //////////////////////////////////////////////////////////////////////////// */
SIP_U8bit
sipAlgCompareIPHashKeys (void *pKey1, void *pKey2)
{
    SipAlgIPHashKey    *pStoredKey = (SipAlgIPHashKey *) pKey1;
    SipAlgIPHashKey    *pQueryKey = (SipAlgIPHashKey *) pKey2;
    SIPDEBUG ((SIP_S8bit *) "SIP_DEBUG -  Inside sipAlgCompareIPHashKeys ");

    if ((sip_strcmp (pStoredKey->pCallID, pQueryKey->pCallID) == 0) &&
        (sip_strcmp (pStoredKey->pVersionId, pQueryKey->pVersionId) == 0) &&
        (pStoredKey->isRequest == pQueryKey->isRequest))
        return 0;
    else
        return 1;
}

/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:      sipAlgRemoveEntryFromIPHash */
/*  */
/*  DESCRIPTION: Function to free the data that is stored in the hash. */
/*  */
/* //////////////////////////////////////////////////////////////////////////// */
void
sipAlgRemoveEntryFromIPHash (void *pData)
{
    SipAlgIPHashElement *pTempElement = (SipAlgIPHashElement *) pData;
    SipError            dError = E_NO_ERROR;
    SIPDEBUG ((SIP_S8bit *) "SIP_DEBUG -  Inside sipAlgRemoveEntryFromIPHash ");

    if (pTempElement->pSipAlgTimerInfo != SIP_NULL)
    {
        if (pTempElement->pSipAlgTimerInfo->u1TimerStatus == NAT_ENABLE)
        {
            NatSipAlgStopTimer (pTempElement->pSipAlgTimerInfo);
        }
        sip_memfree (0,
                     (SIP_Pvoid) & (pTempElement->
                                    pSipAlgTimerInfo->pHashKey->
                                    pCallID), &dError);
        sip_memfree (0,
                     (SIP_Pvoid) & (pTempElement->
                                    pSipAlgTimerInfo->pHashKey->
                                    pVersionId), &dError);
        sip_memfree (0,
                     (SIP_Pvoid) & (pTempElement->
                                    pSipAlgTimerInfo->pHashKey), &dError);
        sip_memfree (0,
                     (SIP_Pvoid) & (pTempElement->pSipAlgTimerInfo), &dError);
    }

    SIPDEBUG ((SIP_S8bit *)
              "SIP_DEBUG -  Exiting sipAlgRemoveEntryFromIPHash ");
}

/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:      sipAlgFreeIPHashKey */
/*  */
/*  DESCRIPTION: Function to free the key for the hash table (call-id) */
/* //////////////////////////////////////////////////////////////////////////// */
void
sipAlgFreeIPHashKey (void *pKey)
{
    SipAlgIPHashKey    *pTempKey = (SipAlgIPHashKey *) pKey;
    SIPDEBUG ((SIP_S8bit *) "SIP_DEBUG -  Inside sipAlgFreeIPHashKey ");

    if (pKey != SIP_NULL)
    {
        sip_memfree (0, (SIP_Pvoid) & (pTempKey->pCallID), SIP_NULL);
        sip_memfree (0, (SIP_Pvoid) & (pTempKey->pVersionId), SIP_NULL);
        sip_memfree (0, (SIP_Pvoid *) & pKey, SIP_NULL);
    }
    SIPDEBUG ((SIP_S8bit *) "SIP_DEBUG -  Exiting sipAlgFreeIPHashKey ");
}

/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:      sipAlgDeRegHashFunc */
/*  */
/*  DESCRIPTION: Function to calculate the hash value given a key as input. */
/*  */
/* //////////////////////////////////////////////////////////////////////////// */
SIP_U32bit
sipAlgDeRegHashFunc (void *pData)
{
    SipAlgDeRegHashKey *pKey = (SipAlgDeRegHashKey *) pData;

    if (pKey->pAOR != SIP_NULL)
    {
        /* Hash the Call-ID and get  the bucket. */
        return (sip_elfHash (pKey->pAOR));
    }
    return 0;
}

/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:      sipAlgFreeDeRegHashKey */
/*  */
/*  DESCRIPTION: Function to free the key for the hash table (AOR) */
/* //////////////////////////////////////////////////////////////////////////// */
void
sipAlgFreeDeRegHashKey (void *pKey)
{

    SipAlgDeRegHashKey *pTempKey = (SipAlgDeRegHashKey *) pKey;
#ifdef SIP_DEBUG
    SIPDEBUG ((SIP_S8bit *) "SIP_DEBUG -  Inside sipAlgFreeDeRegHashKey ");
#endif

    if (pKey != SIP_NULL)
    {
        sip_memfree (0, (SIP_Pvoid) & (pTempKey->pAOR), SIP_NULL);
        sip_memfree (0, (SIP_Pvoid *) & pKey, SIP_NULL);
    }
#ifdef SIP_DEBUG
    SIPDEBUG ((SIP_S8bit *) "SIP_DEBUG -  Exiting sipAlgFreeDeRegHashKey ");
#endif
}

/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:      sipAlgCompareDeRegHashKeys */
/*  */
/*  DESCRIPTION: Comparison function for session hash keys. */
/*  If both keys matches , then this function return 0 as the success */
/*  value otherwise it return 1 as the failure value */
/*  */
/* //////////////////////////////////////////////////////////////////////////// */
SIP_U8bit
sipAlgCompareDeRegHashKeys (void *pKey1, void *pKey2)
{
    SipAlgDeRegHashKey *pStoredKey = (SipAlgDeRegHashKey *) pKey1;
    SipAlgDeRegHashKey *pQueryKey = (SipAlgDeRegHashKey *) pKey2;

    SIPDEBUG ((SIP_S8bit *) "SIP_DEBUG -  Inside sipAlgCompareDeRegHashKeys ");

    if (sip_strcmp (pStoredKey->pAOR, pQueryKey->pAOR) == 0)
        return 0;
    else
        return 1;
}

/* //////////////////////////////////////////////////////////////////////////// */
/*  FUNCTION:      sipAlgRemoveEntryFromDeRegHash */
/*  */
/*  DESCRIPTION: Function to free the data that is stored in the hash. */
/*  */
/* //////////////////////////////////////////////////////////////////////////// */
void
sipAlgRemoveEntryFromDeRegHash (void *pData)
{
    /* This pointer is used to avoid strict-aliasing warning */
    SIP_Pvoid           pTemp = SIP_NULL;
#ifdef SIP_DEBUG
    SIPDEBUG ((SIP_S8bit *)
              "SIP_DEBUG -  Inside sipAlgRemoveEntryFromDeRegHash ");
#endif

    SipAlgDeRegHashElement *pTempElement = (SipAlgDeRegHashElement *) pData;

    SIP_U32bit          dSize = 0;
    SipError            dError = E_NO_ERROR;
    SIP_U32bit          dElementFreed = 0;

    if ((SipSuccess ==
         sip_listSizeOf (pTempElement->pDeRegInfoList, &dSize, &dError))
        && (0 != dSize))
    {
        if (sip_listDeleteAll (pTempElement->pDeRegInfoList, &dError)
            == SipFail)
        {
            sip_error (SIP_Minor, "SIP-ERROR: sipAlgRemoveEntryFromDeRegHash:"
                       "sip_listDeleteAll Failed for pDeRegInfoList");
        }
    }                            /* End of num of entries check */

    sip_listSizeOf (pTempElement->pDeRegInfoList, &dSize, &dError);
    if (dSize == dElementFreed)
    {
        /* If there  is no entries in the List , then  remove the entire */
        /* list. */
        pTemp = pTempElement->pDeRegInfoList;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, &dError);
        pTempElement->pDeRegInfoList = NULL;
        pTemp = pTempElement;
        sip_memfree (0, (SIP_Pvoid *) & pTemp, &dError);
        pTempElement = NULL;
    }

#ifdef SIP_DEBUG
    SIPDEBUG ((SIP_S8bit *)
              "SIP_DEBUG -  Exiting sipAlgRemoveEntryFromDeRegHash ");

#endif

}

#ifdef SDF_PASSTHROUGH
/*open the file, write the passthru delays for all msgs and close it*/
#ifndef SECURITY_KERNEL_WANTED
void
Sdf_fn_passThruPrintToFile ()
{
    FILE               *pGlbHeaderFilePtr =
        FOPEN ("/tmp/Passthru_delayALG.log", "a+");
    if (pGlbHeaderFilePtr == NULL)
    {
        PRINTF (" !!! could not open passthru log file to write \n");
    }

    if (NULL != pGlbHeaderFilePtr)
    {
        fprintf (pGlbHeaderFilePtr, "\n INVITE \n");
        fprintf (pGlbHeaderFilePtr, "Peak passthru Latency ");
        fprintf (pGlbHeaderFilePtr, "sec::%ld ", st_Invite->i4PeakSec);
        fprintf (pGlbHeaderFilePtr, "microSec::%ld", st_Invite->i4PeakUsec);
        fprintf (pGlbHeaderFilePtr, "\n");
        fprintf (pGlbHeaderFilePtr, "Average passthru Latency ");
        fprintf (pGlbHeaderFilePtr, "sec::%g ", st_Invite->AvgSec);
        fprintf (pGlbHeaderFilePtr, " microsec::%g", st_Invite->AvgUsec);
        fprintf (pGlbHeaderFilePtr, "\n");
        fprintf (pGlbHeaderFilePtr, "Number of INVITEs for peak delay: %d\n",
                 st_Invite->i2NumInvPeak);
        fprintf (pGlbHeaderFilePtr, "Total Number of INVITEs : %d\n",
                 st_Invite->i2NumInvite);

        fprintf (pGlbHeaderFilePtr, "\n 200 OK to INVITE \n");
        fprintf (pGlbHeaderFilePtr, "Peak passthru Latency ");
        fprintf (pGlbHeaderFilePtr, "sec::%ld ", st_200INV->i4PeakSec);
        fprintf (pGlbHeaderFilePtr, "microSec::%ld", st_200INV->i4PeakUsec);
        fprintf (pGlbHeaderFilePtr, "\n");
        fprintf (pGlbHeaderFilePtr, "Average passthru Latency ");
        fprintf (pGlbHeaderFilePtr, "Sec::%g ", st_200INV->AvgSec);
        fprintf (pGlbHeaderFilePtr, "microsec::%g", st_200INV->AvgUsec);
        fprintf (pGlbHeaderFilePtr, "\n");
        fprintf (pGlbHeaderFilePtr, "Number of 200s for peak delay: %d\n",
                 st_200INV->i2NumInvPeak);
        fprintf (pGlbHeaderFilePtr, "Total Number of 200s: %d\n",
                 st_200INV->i2NumInvite);

        fprintf (pGlbHeaderFilePtr, "\n ACK  \n");
        fprintf (pGlbHeaderFilePtr, "Peak passthru Latency ");
        fprintf (pGlbHeaderFilePtr, "sec::%ld ", st_ACK->i4PeakSec);
        fprintf (pGlbHeaderFilePtr, "microSec::%ld", st_ACK->i4PeakUsec);
        fprintf (pGlbHeaderFilePtr, "\n");
        fprintf (pGlbHeaderFilePtr, "Average passthru Latency ");
        fprintf (pGlbHeaderFilePtr, "Sec::%g ", st_ACK->AvgSec);
        fprintf (pGlbHeaderFilePtr, "microsec::%g", st_ACK->AvgUsec);
        fprintf (pGlbHeaderFilePtr, "\n");
        fprintf (pGlbHeaderFilePtr, "Number of ACKs for peak delay: %d\n",
                 st_ACK->i2NumInvPeak);
        fprintf (pGlbHeaderFilePtr, "Total Number of ACKs: %d\n",
                 st_ACK->i2NumInvite);

        fprintf (pGlbHeaderFilePtr, "\n BYE \n");
        fprintf (pGlbHeaderFilePtr, "Peak passthru Latency ");
        fprintf (pGlbHeaderFilePtr, "sec::%ld ", st_BYE->i4PeakSec);
        fprintf (pGlbHeaderFilePtr, "microSec::%ld", st_BYE->i4PeakUsec);
        fprintf (pGlbHeaderFilePtr, "\n");
        fprintf (pGlbHeaderFilePtr, "Average passthru Latency ");
        fprintf (pGlbHeaderFilePtr, "Sec::%g ", st_BYE->AvgSec);
        fprintf (pGlbHeaderFilePtr, "microsec::%g", st_BYE->AvgUsec);
        fprintf (pGlbHeaderFilePtr, "\n");
        fprintf (pGlbHeaderFilePtr, "Number of BYEs for peak delay: %d\n",
                 st_BYE->i2NumInvPeak);
        fprintf (pGlbHeaderFilePtr, "Total Number of BYEs: %d\n",
                 st_BYE->i2NumInvite);

        fprintf (pGlbHeaderFilePtr, "\n 200 ok to BYE \n");
        fprintf (pGlbHeaderFilePtr, "Peak passthru Latency ");
        fprintf (pGlbHeaderFilePtr, "sec::%ld ", st_200BYE->i4PeakSec);
        fprintf (pGlbHeaderFilePtr, "microSec::%ld", st_200BYE->i4PeakUsec);
        fprintf (pGlbHeaderFilePtr, "\n");
        fprintf (pGlbHeaderFilePtr, "Average passthru Latency ");
        fprintf (pGlbHeaderFilePtr, "Sec::%g ", st_200BYE->AvgSec);
        fprintf (pGlbHeaderFilePtr, "microsec::%g", st_200BYE->AvgUsec);
        fprintf (pGlbHeaderFilePtr, "\n");
        fprintf (pGlbHeaderFilePtr, "Number of 200s for peak delay: %d\n",
                 st_200BYE->i2NumInvPeak);
        fprintf (pGlbHeaderFilePtr, "Total Number of 200s: %d\n",
                 st_200BYE->i2NumInvite);

        fprintf (pGlbHeaderFilePtr, "\n REG \n");
        fprintf (pGlbHeaderFilePtr, "Peak passthru Latency ");
        fprintf (pGlbHeaderFilePtr, "sec::%ld ", st_REG->i4PeakSec);
        fprintf (pGlbHeaderFilePtr, "microSec::%ld", st_REG->i4PeakUsec);
        fprintf (pGlbHeaderFilePtr, "\n");
        fprintf (pGlbHeaderFilePtr, "Average passthru Latency ");
        fprintf (pGlbHeaderFilePtr, "Sec::%g ", st_REG->AvgSec);
        fprintf (pGlbHeaderFilePtr, "microsec::%g", st_REG->AvgUsec);
        fprintf (pGlbHeaderFilePtr, "\n");
        fprintf (pGlbHeaderFilePtr, "Number of 200s for peak delay: %d\n",
                 st_REG->i2NumInvPeak);
        fprintf (pGlbHeaderFilePtr, "Total Number of 200s: %d\n",
                 st_REG->i2NumInvite);

        fprintf (pGlbHeaderFilePtr, "\n 200 ok to REG\n");
        fprintf (pGlbHeaderFilePtr, "Peak passthru Latency ");
        fprintf (pGlbHeaderFilePtr, "sec::%ld ", st_200REG->i4PeakSec);
        fprintf (pGlbHeaderFilePtr, "microSec::%ld", st_200REG->i4PeakUsec);
        fprintf (pGlbHeaderFilePtr, "\n");
        fprintf (pGlbHeaderFilePtr, "Average passthru Latency ");
        fprintf (pGlbHeaderFilePtr, "Sec::%g ", st_200REG->AvgSec);
        fprintf (pGlbHeaderFilePtr, "microsec::%g", st_200REG->AvgUsec);
        fprintf (pGlbHeaderFilePtr, "\n");
        fprintf (pGlbHeaderFilePtr, "Number of 200s for peak delay: %d\n",
                 st_200REG->i2NumInvPeak);
        fprintf (pGlbHeaderFilePtr, "Total Number of 200s: %d\n",
                 st_200REG->i2NumInvite);

        /*Printing passthru Bands for individual req-resps */
        fprintf (pGlbHeaderFilePtr, "\n INVITE - \n");
        fprintf (pGlbHeaderFilePtr, "  0-  5msec  = %d \n",
                 st_Invite->ai2Band[0]);
        fprintf (pGlbHeaderFilePtr, "  5- 10msec  = %d \n",
                 st_Invite->ai2Band[1]);
        fprintf (pGlbHeaderFilePtr, " 10- 20msec  = %d \n",
                 st_Invite->ai2Band[2]);
        fprintf (pGlbHeaderFilePtr, " 20- 30msec  = %d \n",
                 st_Invite->ai2Band[3]);
        fprintf (pGlbHeaderFilePtr, " 30- 40msec  = %d \n",
                 st_Invite->ai2Band[4]);
        fprintf (pGlbHeaderFilePtr, " 40- 50msec  = %d \n",
                 st_Invite->ai2Band[5]);
        fprintf (pGlbHeaderFilePtr, " 50- 60msec  = %d \n",
                 st_Invite->ai2Band[6]);
        fprintf (pGlbHeaderFilePtr, " 60- 70msec  = %d \n",
                 st_Invite->ai2Band[7]);
        fprintf (pGlbHeaderFilePtr, " 70- 80msec  = %d \n",
                 st_Invite->ai2Band[8]);
        fprintf (pGlbHeaderFilePtr, " 80- 90msec  = %d \n",
                 st_Invite->ai2Band[9]);
        fprintf (pGlbHeaderFilePtr, " 90-100msec  = %d \n",
                 st_Invite->ai2Band[10]);
        fprintf (pGlbHeaderFilePtr, "100-110msec  = %d \n",
                 st_Invite->ai2Band[11]);
        fprintf (pGlbHeaderFilePtr, "110-120msec  = %d \n",
                 st_Invite->ai2Band[12]);
        fprintf (pGlbHeaderFilePtr, "120-130msec  = %d \n",
                 st_Invite->ai2Band[13]);
        fprintf (pGlbHeaderFilePtr, "130-140msec  = %d \n",
                 st_Invite->ai2Band[14]);
        fprintf (pGlbHeaderFilePtr, "    >140msec  = %d \n",
                 st_Invite->ai2Band[15]);

        fprintf (pGlbHeaderFilePtr, "\n 200 Invite - \n");
        fprintf (pGlbHeaderFilePtr, "  0-  5msec  = %d \n",
                 st_200INV->ai2Band[0]);
        fprintf (pGlbHeaderFilePtr, "  5- 10msec  = %d \n",
                 st_200INV->ai2Band[1]);
        fprintf (pGlbHeaderFilePtr, " 10- 20msec  = %d \n",
                 st_200INV->ai2Band[2]);
        fprintf (pGlbHeaderFilePtr, " 20- 30msec  = %d \n",
                 st_200INV->ai2Band[3]);
        fprintf (pGlbHeaderFilePtr, " 30- 40msec  = %d \n",
                 st_200INV->ai2Band[4]);
        fprintf (pGlbHeaderFilePtr, " 40- 50msec  = %d \n",
                 st_200INV->ai2Band[5]);
        fprintf (pGlbHeaderFilePtr, " 50- 60msec  = %d \n",
                 st_200INV->ai2Band[6]);
        fprintf (pGlbHeaderFilePtr, " 60- 70msec  = %d \n",
                 st_200INV->ai2Band[7]);
        fprintf (pGlbHeaderFilePtr, " 70- 80msec  = %d \n",
                 st_200INV->ai2Band[8]);
        fprintf (pGlbHeaderFilePtr, " 80- 90msec  = %d \n",
                 st_200INV->ai2Band[9]);
        fprintf (pGlbHeaderFilePtr, " 90-100msec  = %d \n",
                 st_200INV->ai2Band[10]);
        fprintf (pGlbHeaderFilePtr, "100-110msec  = %d \n",
                 st_200INV->ai2Band[11]);
        fprintf (pGlbHeaderFilePtr, "110-120msec  = %d \n",
                 st_200INV->ai2Band[12]);
        fprintf (pGlbHeaderFilePtr, "120-130msec  = %d \n",
                 st_200INV->ai2Band[13]);
        fprintf (pGlbHeaderFilePtr, "130-140msec  = %d \n",
                 st_200INV->ai2Band[14]);
        fprintf (pGlbHeaderFilePtr, "   >140msec  = %d \n",
                 st_200INV->ai2Band[15]);

        fprintf (pGlbHeaderFilePtr, "\n ACK - \n");
        fprintf (pGlbHeaderFilePtr, "  0-  5msec  = %d \n", st_ACK->ai2Band[0]);
        fprintf (pGlbHeaderFilePtr, "  5- 10msec  = %d \n", st_ACK->ai2Band[1]);
        fprintf (pGlbHeaderFilePtr, " 10- 20msec  = %d \n", st_ACK->ai2Band[2]);
        fprintf (pGlbHeaderFilePtr, " 20- 30msec  = %d \n", st_ACK->ai2Band[3]);
        fprintf (pGlbHeaderFilePtr, " 30- 40msec  = %d \n", st_ACK->ai2Band[4]);
        fprintf (pGlbHeaderFilePtr, " 40- 50msec  = %d \n", st_ACK->ai2Band[5]);
        fprintf (pGlbHeaderFilePtr, " 50- 60msec  = %d \n", st_ACK->ai2Band[6]);
        fprintf (pGlbHeaderFilePtr, " 60- 70msec  = %d \n", st_ACK->ai2Band[7]);
        fprintf (pGlbHeaderFilePtr, " 70- 80msec  = %d \n", st_ACK->ai2Band[8]);
        fprintf (pGlbHeaderFilePtr, " 80- 90msec  = %d \n", st_ACK->ai2Band[9]);
        fprintf (pGlbHeaderFilePtr, " 90-100msec  = %d \n",
                 st_ACK->ai2Band[10]);
        fprintf (pGlbHeaderFilePtr, "100-110msec  = %d \n",
                 st_ACK->ai2Band[11]);
        fprintf (pGlbHeaderFilePtr, "110-120msec  = %d \n",
                 st_ACK->ai2Band[12]);
        fprintf (pGlbHeaderFilePtr, "120-130msec  = %d \n",
                 st_ACK->ai2Band[13]);
        fprintf (pGlbHeaderFilePtr, "130-140msec  = %d \n",
                 st_ACK->ai2Band[14]);
        fprintf (pGlbHeaderFilePtr, "   >140msec  = %d \n",
                 st_ACK->ai2Band[15]);

        fprintf (pGlbHeaderFilePtr, "\n BYE - \n");
        fprintf (pGlbHeaderFilePtr, "  0-  5msec  = %d \n", st_BYE->ai2Band[0]);
        fprintf (pGlbHeaderFilePtr, "  5- 10msec  = %d \n", st_BYE->ai2Band[1]);
        fprintf (pGlbHeaderFilePtr, " 10- 20msec  = %d \n", st_BYE->ai2Band[2]);
        fprintf (pGlbHeaderFilePtr, " 20- 30msec  = %d \n", st_BYE->ai2Band[3]);
        fprintf (pGlbHeaderFilePtr, " 30- 40msec  = %d \n", st_BYE->ai2Band[4]);
        fprintf (pGlbHeaderFilePtr, " 40- 50msec  = %d \n", st_BYE->ai2Band[5]);
        fprintf (pGlbHeaderFilePtr, " 50- 60msec  = %d \n", st_BYE->ai2Band[6]);
        fprintf (pGlbHeaderFilePtr, " 60- 70msec  = %d \n", st_BYE->ai2Band[7]);
        fprintf (pGlbHeaderFilePtr, " 70- 80msec  = %d \n", st_BYE->ai2Band[8]);
        fprintf (pGlbHeaderFilePtr, " 80- 90msec  = %d \n", st_BYE->ai2Band[9]);
        fprintf (pGlbHeaderFilePtr, " 90-100msec  = %d \n",
                 st_BYE->ai2Band[10]);
        fprintf (pGlbHeaderFilePtr, "100-110msec  = %d \n",
                 st_BYE->ai2Band[11]);
        fprintf (pGlbHeaderFilePtr, "110-120msec  = %d \n",
                 st_BYE->ai2Band[12]);
        fprintf (pGlbHeaderFilePtr, "120-130msec  = %d \n",
                 st_BYE->ai2Band[13]);
        fprintf (pGlbHeaderFilePtr, "130-140msec  = %d \n",
                 st_BYE->ai2Band[14]);
        fprintf (pGlbHeaderFilePtr, "   >140msec  = %d \n",
                 st_BYE->ai2Band[15]);

        fprintf (pGlbHeaderFilePtr, "\n 200 BYE - \n");
        fprintf (pGlbHeaderFilePtr, "  0-  5msec  = %d \n",
                 st_200BYE->ai2Band[0]);
        fprintf (pGlbHeaderFilePtr, "  5- 10msec  = %d \n",
                 st_200BYE->ai2Band[1]);
        fprintf (pGlbHeaderFilePtr, " 10- 20msec  = %d \n",
                 st_200BYE->ai2Band[2]);
        fprintf (pGlbHeaderFilePtr, " 20- 30msec  = %d \n",
                 st_200BYE->ai2Band[3]);
        fprintf (pGlbHeaderFilePtr, " 30- 40msec  = %d \n",
                 st_200BYE->ai2Band[4]);
        fprintf (pGlbHeaderFilePtr, " 40- 50msec  = %d \n",
                 st_200BYE->ai2Band[5]);
        fprintf (pGlbHeaderFilePtr, " 50- 60msec  = %d \n",
                 st_200BYE->ai2Band[6]);
        fprintf (pGlbHeaderFilePtr, " 60- 70msec  = %d \n",
                 st_200BYE->ai2Band[7]);
        fprintf (pGlbHeaderFilePtr, " 70- 80msec  = %d \n",
                 st_200BYE->ai2Band[8]);
        fprintf (pGlbHeaderFilePtr, " 80- 90msec  = %d \n",
                 st_200BYE->ai2Band[9]);
        fprintf (pGlbHeaderFilePtr, " 90-100msec  = %d \n",
                 st_200BYE->ai2Band[10]);
        fprintf (pGlbHeaderFilePtr, "100-110msec  = %d \n",
                 st_200BYE->ai2Band[11]);
        fprintf (pGlbHeaderFilePtr, "110-120msec  = %d \n",
                 st_200BYE->ai2Band[12]);
        fprintf (pGlbHeaderFilePtr, "120-130msec  = %d \n",
                 st_200BYE->ai2Band[13]);
        fprintf (pGlbHeaderFilePtr, "130-140msec  = %d \n",
                 st_200BYE->ai2Band[14]);
        fprintf (pGlbHeaderFilePtr, "   >140msec  = %d \n",
                 st_200BYE->ai2Band[15]);

        fprintf (pGlbHeaderFilePtr, "\n REGISTER - \n");
        fprintf (pGlbHeaderFilePtr, "  0-  5msec  = %d \n", st_REG->ai2Band[0]);
        fprintf (pGlbHeaderFilePtr, "  5- 10msec  = %d \n", st_REG->ai2Band[1]);
        fprintf (pGlbHeaderFilePtr, " 10- 20msec  = %d \n", st_REG->ai2Band[2]);
        fprintf (pGlbHeaderFilePtr, " 20- 30msec  = %d \n", st_REG->ai2Band[3]);
        fprintf (pGlbHeaderFilePtr, " 30- 40msec  = %d \n", st_REG->ai2Band[4]);
        fprintf (pGlbHeaderFilePtr, " 40- 50msec  = %d \n", st_REG->ai2Band[5]);
        fprintf (pGlbHeaderFilePtr, " 50- 60msec  = %d \n", st_REG->ai2Band[6]);
        fprintf (pGlbHeaderFilePtr, " 60- 70msec  = %d \n", st_REG->ai2Band[7]);
        fprintf (pGlbHeaderFilePtr, " 70- 80msec  = %d \n", st_REG->ai2Band[8]);
        fprintf (pGlbHeaderFilePtr, " 80- 90msec  = %d \n", st_REG->ai2Band[9]);
        fprintf (pGlbHeaderFilePtr, " 90-100msec  = %d \n",
                 st_REG->ai2Band[10]);
        fprintf (pGlbHeaderFilePtr, "100-110msec  = %d \n",
                 st_REG->ai2Band[11]);
        fprintf (pGlbHeaderFilePtr, "110-120msec  = %d \n",
                 st_REG->ai2Band[12]);
        fprintf (pGlbHeaderFilePtr, "120-130msec  = %d \n",
                 st_REG->ai2Band[13]);
        fprintf (pGlbHeaderFilePtr, "130-140msec  = %d \n",
                 st_REG->ai2Band[14]);
        fprintf (pGlbHeaderFilePtr, "   >140msec  = %d \n",
                 st_REG->ai2Band[15]);

        fprintf (pGlbHeaderFilePtr, "\n 200 REGISTER - \n");
        fprintf (pGlbHeaderFilePtr, "  0-  5msec  = %d \n",
                 st_200REG->ai2Band[0]);
        fprintf (pGlbHeaderFilePtr, "  5- 10msec  = %d \n",
                 st_200REG->ai2Band[1]);
        fprintf (pGlbHeaderFilePtr, " 10- 20msec  = %d \n",
                 st_200REG->ai2Band[2]);
        fprintf (pGlbHeaderFilePtr, " 20- 30msec  = %d \n",
                 st_200REG->ai2Band[3]);
        fprintf (pGlbHeaderFilePtr, " 30- 40msec  = %d \n",
                 st_200REG->ai2Band[4]);
        fprintf (pGlbHeaderFilePtr, " 40- 50msec  = %d \n",
                 st_200REG->ai2Band[5]);
        fprintf (pGlbHeaderFilePtr, " 50- 60msec  = %d \n",
                 st_200REG->ai2Band[6]);
        fprintf (pGlbHeaderFilePtr, " 60- 70msec  = %d \n",
                 st_200REG->ai2Band[7]);
        fprintf (pGlbHeaderFilePtr, " 70- 80msec  = %d \n",
                 st_200REG->ai2Band[8]);
        fprintf (pGlbHeaderFilePtr, " 80- 90msec  = %d \n",
                 st_200REG->ai2Band[9]);
        fprintf (pGlbHeaderFilePtr, " 90-100msec  = %d \n",
                 st_200REG->ai2Band[10]);
        fprintf (pGlbHeaderFilePtr, "100-110msec  = %d \n",
                 st_200REG->ai2Band[11]);
        fprintf (pGlbHeaderFilePtr, "110-120msec  = %d \n",
                 st_200REG->ai2Band[12]);
        fprintf (pGlbHeaderFilePtr, "120-130msec  = %d \n",
                 st_200REG->ai2Band[13]);
        fprintf (pGlbHeaderFilePtr, "130-140msec  = %d \n",
                 st_200REG->ai2Band[14]);
        fprintf (pGlbHeaderFilePtr, "   >140msec  = %d \n",
                 st_200REG->ai2Band[15]);

        fflush (pGlbHeaderFilePtr);
        fclose (pGlbHeaderFilePtr);
    }
    return;
}
#endif
#endif
