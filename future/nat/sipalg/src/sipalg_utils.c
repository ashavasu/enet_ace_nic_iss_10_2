/********************************************************************
 * Copyright (C) Aricent Communications Sotware,2007
 *
 * $Id: sipalg_utils.c,v 1.6 2011/10/25 09:50:07 siva Exp $
 *
 *******************************************************************/

/***********************************************************************

** FUNCTION:
**            SIPALG util functionality
************************************************************************
**
** FILENAME:
** sipalg_utils.c
**
** DESCRIPTION:
**        This file util functions used by SIPALG.
**
** DATE             NAME             REFERENCE         REASON
** ----             ----             ---------         --------
** 16/07/2007    Rashed Ahammad        -            Initial                    
**
**
**     Copyright (C) 2007 Aricent Inc . All Rights Reserved
***********************************************************************/
#include "natinc.h"
#include "sipalg_inc.h"
#ifdef VPN_WANTED
#include "vpn.h"
#endif

#if (defined(SDF_TIMESTAMP) || defined(SDF_PASSTHROUGH))
UINT4               gu4AlgPassThrough;
UINT4               gu4AlgTimeStamp;
#endif

#ifdef SDF_TIMESTAMP
#ifndef SECURITY_KERNEL_WANTED
FILE               *pGlbFilePtr = NULL;
#endif
PRIVATE INT4        i4Counter = 0;
Sdf_st_logTimeStamp *patLogTimeStamp[1000];

#ifndef SECURITY_KERNEL_WANTED
/******************************************************************************
** FUNCTION: Sdf_fn_writeToFile
** DESCRIPTION: This function is used to write the Time Stamp values to file 
**
** PARAMETERS: None
**
** Returns   : None
*******************************************************************************/

VOID
Sdf_fn_writeToFile ()
{
    pGlbFilePtr = FOPEN ("/tmp/timestampAlg.log", "a+");
    if (NULL != pGlbFilePtr)
    {
        INT4                i = 0;
        for (i = 0; i < i4Counter; i++)
        {
            fprintf (pGlbFilePtr, "%s::", patLogTimeStamp[i]->stringData);
            struct timeval      tv = patLogTimeStamp[i]->tLogTime;
            fprintf (pGlbFilePtr, ctime (&tv.tv_sec));
            fprintf (pGlbFilePtr, "::%d", (SIP_U32bit) tv.tv_usec / 1000);
            fprintf (pGlbFilePtr, "::%d::", (SIP_U32bit) tv.tv_usec % 1000);
            fprintf (pGlbFilePtr, "\n");
        }
        fflush (pGlbFilePtr);
        fclose (pGlbFilePtr);
    }
}
#endif
/******************************************************************************
** FUNCTION: Sdf_fn_printTimeStamp
** DESCRIPTION: This function is used to write the Time Stamp values to tslog
**              structure 
**
** PARAMETERS: None
**
** Returns   : 1
*******************************************************************************/
INT4
Sdf_fn_printTimeStamp (UINT1 *pPrintString)
{
    struct timeval      tv;
    /*long int  dMicroseconds=0,dMilliseconds=0; */
    SipError            dError = E_NO_ERROR;
#ifdef SECURITY_KERNEL_WANTED
    do_gettimeofday (&tv);
#else
    gettimeofday (&tv);
#endif
    patLogTimeStamp[i4Counter] = (Sdf_st_logTimeStamp *) sip_memget (0,
                                                                     sizeof
                                                                     (Sdf_st_logTimeStamp),
                                                                     &dError);
    patLogTimeStamp[i4Counter]->stringData = sip_strdup (pPrintString, 0);
    patLogTimeStamp[i4Counter]->tLogTime = tv;
    i4Counter++;
    return 1;
}
#endif
/******************************************************************************
** FUNCTION: sipAlgGetDialogParamFromSipMsg
** DESCRIPTION: This function is used to get the dialog params from the SIP-MSG 
**
** PARAMETERS:
**          pSipMsg(IN)                - Pointer to SIP message 
**            pDialogKey(OUT)         - pointer to dialog params 
**
** Returns   : SUCCESS/FAILURE
*******************************************************************************/

SipBool
sipAlgGetDialogParamFromSipMsg (SipMessage * pSipMsg,
                                SipAlgMediaKey * pDialogKey)
{
    SipHeader           dCallidHeader, dToHeader, dFromHeader;
    SipError            derror;
    SIP_S8bit          *pCallid = SIP_NULL, *pToTag = SIP_NULL, *pFromTag =
        SIP_NULL;

    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgGetDialogParamFromSipMsg ");

    /* First get the call-id header */
    if (sip_getHeader (pSipMsg, SipHdrTypeCallId, &dCallidHeader, &derror) ==
        SipFail)
    {
        sip_error (SIP_Minor,
                   "In sipAlgGetDialogParamFromSipMsg:sip_getHeader failed\n");
        return SipFail;
    }
    /* Get teh Callid from the header */
    if (sip_getValueFromCallIdHdr (&dCallidHeader, &pCallid, &derror) ==
        SipFail)
    {
        sip_error (SIP_Minor,
                   "In sipAlgGetDialogParamFromSipMsg:sip_getValueFromCallIdHdr failed\n");
        sip_freeSipHeader (&dCallidHeader);
        return SipFail;
    }
    /* Get the To header from the sip msg */
    if (sip_getHeader (pSipMsg, SipHdrTypeTo, &dToHeader, &derror) == SipFail)
    {
        sip_error (SIP_Minor,
                   "In sipAlgGetDialogParamFromSipMsg:sip_getHeader failed\n");
        sip_freeSipHeader (&dCallidHeader);
        return SipFail;
    }
    /* Get the tag from To header */
    /* dont return failure incase this API fails */
    sip_getTagAtIndexFromToHdr (&dToHeader, &pToTag, 0, &derror);
    /* Get the From header from the sip msg */
    if (sip_getHeader (pSipMsg, SipHdrTypeFrom, &dFromHeader,
                       &derror) == SipFail)
    {
        sip_error (SIP_Minor,
                   "In sipAlgGetDialogParamFromSipMsg:sip_getHeader failed\n");
        sip_freeSipHeader (&dToHeader);
        sip_freeSipHeader (&dCallidHeader);
        return SipFail;
    }
    /* Get the tag from From header */
    if (sip_getTagAtIndexFromFromHdr (&dFromHeader, &pFromTag, 0,
                                      &derror) == SipFail)
    {
        sip_error (SIP_Minor,
                   "In sipAlgGetDialogParamFromSipMsg:sip_getTagAtIndexFromFromHdr failed\n");
        sip_freeSipHeader (&dFromHeader);
        sip_freeSipHeader (&dCallidHeader);
        return SipFail;
    }

    /* form the dialog params structure */
    pDialogKey->pCallID = sip_strdup (pCallid, APP_MEM_ID);
    pDialogKey->pFromTag = sip_strdup (pFromTag, APP_MEM_ID);
    if (pToTag != SIP_NULL)
        pDialogKey->pToTag = sip_strdup (pToTag, APP_MEM_ID);
    else
        pDialogKey->pToTag = SIP_NULL;
    sip_freeSipHeader (&dToHeader);
    sip_freeSipHeader (&dFromHeader);
    sip_freeSipHeader (&dCallidHeader);

    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgGetDialogParamFromSipMsg");
    return SipSuccess;
}

/******************************************************************************
** FUNCTION: sipAlgFreeDialogParam
** DESCRIPTION: This function is used to free the dialog params structure
**              and its members 
**
** PARAMETERS:
**        ppDialogKey(OUT)         - double pointer to dialog params 
**
** Returns   : void
******************************************************************************/
void
sipAlgFreeDialogParam (SipAlgMediaKey ** ppDialogKey)
{
    sip_memfree (0, (SIP_Pvoid) & ((*ppDialogKey)->pCallID), SIP_NULL);
    sip_memfree (0, (SIP_Pvoid) & ((*ppDialogKey)->pFromTag), SIP_NULL);
    sip_memfree (0, (SIP_Pvoid) & ((*ppDialogKey)->pToTag), SIP_NULL);
    sip_memfree (0, (SIP_Pvoid *) ppDialogKey, SIP_NULL);
}

/*****************************************************************************
** FUNCTION: sipAlgIsRegisterRequest
** DESCRIPTION: This function is used to check whether this is a REGISTER req. 
**
** PARAMETERS:
**          pSipMsg(IN)                - Pointer to SIP message 
**
** Returns   : SUCCESS/FAILURE
******************************************************************************/
SipBool
sipAlgIsRegisterRequest (SipMessage * pSipMsg)
{
    SipReqLine         *pReqLine = SIP_NULL;
    SipError            derror;
    SIP_S8bit          *pMethod = SIP_NULL;

    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgIsRegisterRequest");

    /* Get the Request-line from SIP Msg */
    if (sip_getReqLineFromSipReqMsg (pSipMsg, &pReqLine, &derror) == SipFail)
    {
        sip_error (SIP_Minor,
                   "In sipAlgIsRegisterRequest:sip_getReqLineFromSipReqMsg failed\n");
        return SipFail;
    }
    /* Get the method name from the reqline */
    if (sip_getMethodFromReqLine (pReqLine, &pMethod, &derror) == SipFail)
    {
        sip_error (SIP_Minor,
                   "In sipAlgIsRegisterRequest:sip_getMethodFromReqLine failed\n");
        sip_freeSipReqLine (pReqLine);
        return SipFail;
    }
    sip_freeSipReqLine (pReqLine);
    if (sip_strcasecmp (pMethod, "REGISTER") == 0)
    {
        return SipSuccess;
    }

    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgIsRegisterRequest ");
    return SipFail;
}

/******************************************************************************
** FUNCTION: sipAlgGetMethodFromSipReqest
** DESCRIPTION: This function is used to get the method name from the SIP request 
**
** PARAMETERS:
**          pSipMsg(IN)                - Pointer to SIP message 
**          ppMethod(OUT)           - Method name to be returned
**
** Returns   : SUCCESS/FAILURE
*****************************************************************************/
SipBool
sipAlgGetMethodFromSipReqest (SipMessage * pSipMsg, SIP_S8bit ** ppMethod)
{
    SipReqLine         *pReqLine = SIP_NULL;
    SipError            derror;
    SIP_S8bit          *pMethod = SIP_NULL;
    en_SipMessageType   dType;

    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgGetMethodFromSipReqest");

    /* first get the type of sip message; if it is not request then return */
    /* if this is a sip request then return the method name */
    if (sip_getMessageType (pSipMsg, &dType, &derror) == SipFail)
    {
        sip_error (SIP_Minor,
                   "In sipAlgGetMethodFromSipReqest:sip_getMessageType failed\n");
        return SipFail;
    }

    if (dType != SipMessageRequest)
    {
        return SipSuccess;
        /* print that this is not a sip request        */
    }

    /* Get the Request-line from SIP Msg */
    if (sip_getReqLineFromSipReqMsg (pSipMsg, &pReqLine, &derror) == SipFail)
    {
        sip_error (SIP_Minor,
                   "In sipAlgGetMethodFromSipReqest:sip_getReqLineFromSipReqMsg\
                 failed\n");
        return SipFail;
    }
    /* Get the method name from the reqline */
    if (sip_getMethodFromReqLine (pReqLine, &pMethod, &derror) == SipFail)
    {
        sip_error (SIP_Minor,
                   "In sipAlgGetMethodFromSipReqest:sip_getMethodFromReqLine\
                        failed\n");
        sip_freeSipReqLine (pReqLine);
        return SipFail;
    }

    *ppMethod = pMethod;
    sip_freeSipReqLine (pReqLine);

    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgGetMethodFromSipReqest");
    return SipSuccess;
}

/******************************************************************************
** FUNCTION: sipAlgGetMethodAndRespCodeFromSipResponse
** DESCRIPTION: This function gets the method and response code from the SIP-Response
**
** PARAMETERS:
**          pSipMsg(IN)                - Pointer to SIP message
**          ppMethod(OUT)           - Method name to be returned 
**          pRespCode(OUT)          - The response code to be returned
**
** Returns   : SUCCESS/FAILURE
*******************************************************************************/

SipBool
sipAlgGetMethodAndRespCodeFromSipResponse (SipMessage * pSipMsg,
                                           SIP_S8bit ** ppMethod,
                                           SIP_U16bit * pRespCode)
{

    SipError            derror;
    SIP_S8bit          *pMethod = SIP_NULL;
    en_SipMessageType   dType;
    SipHeader           dHeader;
    SipStatusLine      *pLine = SIP_NULL;
    SIP_U32bit          CodeNum;

    SIPDEBUG ((SIP_S8bit *)
              "Entering sipAlgGetMethodAndRespCodeFromSipResponse");

    if (sip_getMessageType (pSipMsg, &dType, &derror) == SipFail)
    {
        sip_error (SIP_Major,
                   "In sipAlgGetMethodAndRespCodeFromSipResponse:sip_getMessageType failed\n");
        return SipFail;
    }

    if (dType != SipMessageResponse)
    {
        SIPDEBUG ((SIP_S8bit *)
                  "Exiting sipAlgGetRespCodeFromSipInviteResponse");
        return SipSuccess;
        /* print that this is not a sip response          */
    }

    if (sip_getHeader (pSipMsg, SipHdrTypeCseq, &dHeader, &derror) == SipFail)
    {
        sip_error (SIP_Major,
                   "In sipAlgGetMethodAndRespCodeFromSipResponse:sip_getHeader failed\n");
        return SipFail;
    }

    if (sip_getMethodFromCseqHdr (&dHeader, &pMethod, &derror) == SipFail)
    {
        sip_freeSipHeader (&dHeader);
        sip_error (SIP_Major,
                   "In sipAlgGetMethodAndRespCodeFromSipResponse:sip_getMethodFromCseqHdr\
                       failed\n");
        return SipFail;
    }
    sip_freeSipHeader (&dHeader);

    /* now get the response code from the status line */
    if (sip_getStatusLineFromSipRespMsg (pSipMsg, &pLine, &derror) == SipFail)
    {
        sip_error (SIP_Major, "In sipAlgGetMethodAndRespCodeFromSipResponse:\
                 sip_getStatusLineFromSipRespMsg failed\n");
        return SipFail;
    }
    if (sip_getStatusCodeNumFromStatusLine (pLine, &CodeNum,
                                            &derror) == SipFail)
    {
        sip_freeSipStatusLine (pLine);
        sip_error (SIP_Major, "In sipAlgGetMethodAndRespCodeFromSipResponse:\
                sip_getStatusCodeNumFromStatusLine failed\n");
        return SipFail;
    }
    *ppMethod = pMethod;
    *pRespCode = (SIP_U16bit) CodeNum;
    sip_freeSipStatusLine (pLine);

    SIPDEBUG ((SIP_S8bit *)
              "Exiting sipAlgGetMethodAndRespCodeFromSipResponse");
    return SipSuccess;
}

/******************************************************************************
** FUNCTION: sipAlgGetRespCodeFromSipInviteResponse
** DESCRIPTION: This function gets the response code from the SIP-Response
**                 for INVITE 
**
** PARAMETERS:
**          pSipMsg(IN)                - Pointer to SIP message 
**          pRespCode(OUT)          - The response code to be returned
**
** Returns   : SUCCESS/FAILURE
*******************************************************************************/

SipBool
sipAlgGetRespCodeFromSipInviteResponse (SipMessage * pSipMsg,
                                        SIP_U16bit * pRespCode)
{

    SipError            derror;
    SIP_S8bit          *pMethod = SIP_NULL;
    en_SipMessageType   dType;
    SipHeader           dHeader;

    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgGetRespCodeFromSipInviteResponse");

    if (sip_getMessageType (pSipMsg, &dType, &derror) == SipFail)
    {
        sip_error (SIP_Major,
                   "In sipAlgGetRespCodeFromSipInviteResponse:sip_getMessageType failed\n");
        return SipFail;
    }

    if (dType != SipMessageResponse)
    {
        SIPDEBUG ((SIP_S8bit *)
                  "Exiting sipAlgGetRespCodeFromSipInviteResponse");
        return SipSuccess;
        /* print that this is not a sip response           */
    }

    /* First check whether this is a response to invite; if it is not then return */
    /* if it is a resposne to invite then return the resposne code */

    if (sip_getHeader (pSipMsg, SipHdrTypeCseq, &dHeader, &derror) == SipFail)
    {
        sip_error (SIP_Major,
                   "In sipAlgGetRespCodeFromSipInviteResponse:sip_getHeader failed\n");
        return SipFail;
    }

    if (sip_getMethodFromCseqHdr (&dHeader, &pMethod, &derror) == SipFail)
    {
        sip_freeSipHeader (&dHeader);
        sip_error (SIP_Major,
                   "In sipAlgGetRespCodeFromSipInviteResponse:sip_getMethodFromCseqHdr\
                       failed\n");
        return SipFail;
    }
    sip_freeSipHeader (&dHeader);
    if (sip_strcasecmp (pMethod, "INVITE") == 0)
    {
        SipStatusLine      *pLine = SIP_NULL;
        SIP_U32bit          CodeNum;

        /* now get the response code from the status line */
        if (sip_getStatusLineFromSipRespMsg (pSipMsg, &pLine,
                                             &derror) == SipFail)
        {
            sip_error (SIP_Major, "In sipAlgGetRespCodeFromSipInviteResponse:\
                 sip_getStatusLineFromSipRespMsg failed\n");
            return SipFail;
        }
        if (sip_getStatusCodeNumFromStatusLine (pLine, &CodeNum,
                                                &derror) == SipFail)
        {
            sip_freeSipStatusLine (pLine);
            sip_error (SIP_Major, "In sipAlgGetRespCodeFromSipInviteResponse:\
                sip_getStatusCodeNumFromStatusLine failed\n");
            return SipFail;
        }
        *pRespCode = (SIP_U16bit) CodeNum;
        sip_freeSipStatusLine (pLine);
    }

    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgGetRespCodeFromSipInviteResponse");
    return SipSuccess;
}

/****************************************************************************
** FUNCTION: sipAlgIsValidNumericAddress
** DESCRIPTION: This function gets is used to find whether the given ip is 
**              valid numeric or not 
**
** PARAMETERS:
**          pNumStr(IN)                - Pointer to the string 
**
** Returns   : SUCCESS/FAILURE
******************************************************************************/
SipBool
sipAlgIsValidNumericAddress (SIP_U8bit * pNumStr)
{
    SIP_U16bit          numDots = 0, octetCount = 0, octetVal = 0;
    en_SipBoolean       validOctet = SipFalse;

    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgIsValidNumericAddress");

    /* Validation of Input parameter. */
    if ((SIP_NULL == pNumStr) || ('\0' == *pNumStr))
    {
        return SipFail;
    }

    /* Check for special IP addresses. */
    if ((0 == sip_strcasecmp ((const SIP_S8bit *) pNumStr, "0.0.0.0")) ||
        (0 == sip_strcasecmp ((const SIP_S8bit *) pNumStr, "255.255.255.255")))
    {
        return SipFail;
    }

    /* 
     * Loop over all the characters of the input string. Check if the 
     * characters are numeric or not.
     */
    while ('\0' != *pNumStr)
    {
        validOctet = SipFalse;
        octetVal = 0;
        while (((*pNumStr) >= '0') && ((*pNumStr) <= '9'))
        {
            octetVal = (SIP_U16bit) ((10 * octetVal) + ((*pNumStr) - '0'));
            pNumStr++;
            /*  To Handle the cases like  x.x..x  as IP address */
            validOctet = SipTrue;
        }
        /*  Handle the cases like  x.x..x  as IP address */
        if (SipFalse == validOctet)
            return SipFail;
        /*  If valid Octet increment the octet count     */
        octetCount++;
        if (('.' == *pNumStr) && (octetVal < 256))
        {
            numDots++;
            pNumStr++;
            continue;
        }
        else
        {
            break;
        }
    }
    if ((numDots != 3) || ('\0' != *pNumStr) || (octetVal > 255) ||
        (octetCount != 4))
    {
        /*  need xactly 3 dots and shud not have terminated prematurely */
        /*  and last octet shud b ok (if parsed) */
        return SipFail;

    }
    /* All characters in the string are numeric. return true. */
    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgIsValidNumericAddress");
    return SipSuccess;
}

/****************************************************************************
** FUNCTION: sipAlgIsNumericAddress
** DESCRIPTION: This function gets is used to find whether the given string is 
**              numeric or not (numeric or hostname)
**
** PARAMETERS:
**          pNumStr(IN)                - Pointer to the string 
**
** Returns   : SUCCESS/FAILURE
******************************************************************************/
SipBool
sipAlgIsNumericAddress (SIP_U8bit * pNumStr)
{
    en_SipBoolean       validOctet = SipFalse;

    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgIsNumericAddress");

    /* Validation of Input parameter. */
    if ((SIP_NULL == pNumStr) || ('\0' == *pNumStr))
    {
        return SipFail;
    }

    /* 
     * Loop over all the characters of the input string. Check if the 
     * characters are numeric or not.
     */
    while ('\0' != *pNumStr)
    {
        validOctet = SipFalse;
        while ((((*pNumStr) >= '0') && ((*pNumStr) <= '9'))
               || ('.' == *pNumStr))
        {
            pNumStr++;
            /*  To Handle the cases like  x.x..x  as IP address */
            validOctet = SipTrue;
        }
        /*  Handle the cases like  x.x..x  as IP address */
        if (SipFalse == validOctet)
            return SipFail;
    }
    /* All characters in the string are numeric. return true. */
    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgIsNumericAddress");
    return SipSuccess;
}

/****************************************************************************
* FUNCTION: sipAlgIsWANIPAddress
* DESCRIPTION: This function identifies whether the given IP is a WAN IP 
*       or not.
*
* PARAMETERS:
*             dIPAdress (IN)      - IP Address 
*
* Returns   : SUCCESS/FAILURE
****************************************************************************/
en_SipBoolean
sipAlgIsWANIPAddress (SIP_U32bit dIPAdress)
{
    /*  Modifying array size to MAX_WAN_LINKS. So that we handle some */
    /*  invalid wanlink info given by CAS. */

    SIP_U32bit          ifIndex = 0;
    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgIsWANIPAddress");
    if (dIPAdress == 0)
    {
        SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgIsWANIPAddress - IP is zero");
        return SipFalse;
    }
    /* Lakshmi change - start */
    if (SipSuccess == sipAlgGetIfIndex (dIPAdress, &ifIndex))
    {
        return SipTrue;
    }
    /* Lakshmi change - end */
    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgIsWANIPAddress");
    return SipFalse;
}

/****************************************************************************
* FUNCTION: sipAlgGetIfIndex
* DESCRIPTION: This function provides the interface index for the given WAN IP.
*
* PARAMETERS: wanIP - The wan IP for which ifIndex is needed.
*              ifIndex - Out param
*
* Returns   : SUCCESS/FAILURE 
****************************************************************************/
SipBool
sipAlgGetIfIndex (SIP_U32bit wanIP, SIP_U32bit * pIfIndex)
{
    SIP_U32bit          i = 0;

    /* Get the interface index for the given WAN IP */
    for (i = 0; i < dGlbInterfaceIndex; i++)
    {
        if (dGlbInterfaceInfo[i].dWanIP == wanIP)
        {
            *pIfIndex = dGlbInterfaceInfo[i].ifIndex;
#ifdef SIPALG_UT_FLAG
            PRINTF ("If index for %d WAN IP is %d\n", wanIP,
                    dGlbInterfaceInfo[i].ifIndex);
#endif
            return SipSuccess;
        }
    }
#ifdef SIPALG_UT_FLAG
    PRINTF ("STACK ERROR: Could not get the if index\n");
#endif
    return SipFail;
}

/****************************************************************************
* FUNCTION: sipAlgGetWANIPAddress
* DESCRIPTION: This function provides the WAN IP of the system.
*
* PARAMETERS: ifIndex - WAN interface index
*              natIP - Out param     
*
* Returns   : IP address on success, and 0 on failure.
****************************************************************************/
SIP_U32bit
sipAlgGetWANIPAddress (SIP_U32bit ifIndex, SIP_U32bit * pNatIP)
{
    tCfaIfInfo          IfInfo;
    SIP_U32bit          retVal = OSIX_FAILURE;
    SIP_U32bit          i = 0;
    en_SipBoolean       isInterfaceFound = SipFalse;

    sip_memset ((SIP_Pvoid) & IfInfo, 0, sizeof (tCfaIfInfo));

    /* Get the WAN IP for the given interface index */
    for (i = 0; i < dGlbInterfaceIndex; i++)
    {
        if (dGlbInterfaceInfo[i].ifIndex == ifIndex)
        {
            /* Lakshmi change -start */
            /* *pNatIP = OSIX_NTOHL(dGlbInterfaceInfo[i].dWanIP); */
            *pNatIP = OSIX_HTONL (dGlbInterfaceInfo[i].dWanIP);
            /* Lakshmi change -end */
#ifdef SIPALG_UT_FLAG
            PRINTF ("WAN IP for Interface index %d is:%d\n", ifIndex,
                    dGlbInterfaceInfo[i].dWanIP);
#endif
            isInterfaceFound = SipTrue;
            break;
        }
    }
    if (isInterfaceFound == SipFalse)
    {
        SIP_U8bit           OperStatus = 0;

        SIPDEBUG ((SIP_S8bit *) "Entering sipAlgGetWANIPAddress");

        retVal = CfaGetIfIpAddr (ifIndex, pNatIP);
        if (retVal != OSIX_SUCCESS)
        {
            sip_error (SIP_Minor, "AricentNATGetWanInfo_ipc failed\n");
            return 0;
        }
#ifndef SECURITY_KERNEL_WANTED
        CfaGetIfOperStatus (ifIndex, &OperStatus);
#else
        SecUtilGetIfInfo (ifIndex, &IfInfo);
        OperStatus = IfInfo.u1IfOperStatus;
#endif
        if (NAT_SIP_LINK_STATUS_UP == OperStatus)
        {
            dGlbInterfaceInfo[dGlbInterfaceIndex].ifIndex = ifIndex;
            dGlbInterfaceInfo[dGlbInterfaceIndex].dWanIP =
                (SIP_U32bit) * pNatIP;
            dGlbInterfaceIndex++;
            if (MAX_WAN_LINKS - 1 == dGlbInterfaceIndex)
                dGlbInterfaceIndex = 0;
            else
                dGlbInterfaceIndex++;
        }
    }
    /* Check if NAT is enabled on a given interface or NAT 
       is disabled across all the interfaces */
    if ((gu4NatEnable == NAT_DISABLE) ||
        (NatCheckIfNatEnable (ifIndex) == NAT_DISABLE))
    {

        SIPDEBUG ((SIP_S8bit *) "NAT is disabled on this interface");
        SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgGetWANIPAddress");
        return NAT_DISABLE;
    }
    else
    {
        SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgGetWANIPAddress");
        return NAT_ENABLE;
    }
    sip_error (SIP_Minor, "Could not get WAN IP\n");
    return 0;
}

/****************************************************************************
* FUNCTION: sipAlgGetUserType
* DESCRIPTION: This function is used to find whether the destinatioin IP
*              is private/public/vpn .
*
* PARAMETERS:  dDestIP - input param 
*
* Returns   : en_IpAddrType.
****************************************************************************/
en_IpAddrType
sipAlgGetUserType (SIP_U32bit dDestIP)
{
    SIP_U32bit          retVal = OSIX_FAILURE;
    tRouteInfo         *pRouteInfo = SIP_NULL;
    tInIfInfo           dInIfInfo;
    en_IpAddrType       dIPAddrType;
    SipError            dError;

    dInIfInfo.u4SrcIPAddr = 0;
    dInIfInfo.u4DestIPAddr = OSIX_NTOHL (dDestIP);
    dInIfInfo.u2Protocol = en_udp;
    dInIfInfo.u2Reserved = 0;

    /* Allocate memory for pRouteInfo */
    pRouteInfo = (tRouteInfo *)
        sip_memget (DECODE_MEM_ID, sizeof (tRouteInfo), &dError);

    if (pRouteInfo == SIP_NULL)
    {
        return en_fail;
    }
    sip_memset ((SIP_Pvoid) (pRouteInfo), 0, sizeof (tRouteInfo));

    CFA_UNLOCK ();
    retVal = sipAlgGetIPInterfaceInfo (dInIfInfo, pRouteInfo);
    CFA_LOCK ();

    if (retVal != OSIX_SUCCESS)
    {
        sip_error (SIP_Minor, "sipAlgGetIPInterfaceInfo failed\n");
        sip_memfree (DECODE_MEM_ID, (SIP_Pvoid) & pRouteInfo, SIP_NULL);
        return en_fail;
    }
    if (CFA_NETWORK_TYPE_WAN == pRouteInfo->u4IfType)
    {
        dIPAddrType = en_wan;
    }
    else if (CFA_NETWORK_TYPE_LAN == pRouteInfo->u4IfType)
    {
        dIPAddrType = en_lan;
    }
    else if (NAT_SIP_LINK_RA_VPN_TYPE == pRouteInfo->u4IfType)
    {
        dIPAddrType = en_ravpn;
    }
    else if (NAT_SIP_LINK_SITE_SITE_VPN_TYPE == pRouteInfo->u4IfType)
    {
        dIPAddrType = en_sitesitevpn;
    }
    else
    {
        sip_error (SIP_Minor, "sipAlgGetIPInterfaceInfo returned "
                   "incorrect If type\n");
        sip_memfree (DECODE_MEM_ID, (SIP_Pvoid) & pRouteInfo, SIP_NULL);
        return en_fail;
    }
    sip_memfree (DECODE_MEM_ID, (SIP_Pvoid) & pRouteInfo, SIP_NULL);
    return dIPAddrType;
}

#if (defined(SDF_TIMESTAMP) || defined(SDF_PASSTHROUGH))
/*****************************************************************************/
/* Function     : SipSetPassThrough                                          */
/* Description  : This function sets the global PassThrough maintained in     */
/*                 sipalg                                                    */
/* Input        : UINT4 u4PassThrough                                        */
/* Output       : NONE                                                       */
/* Returns      : NONE                                                       */
/*****************************************************************************/
PUBLIC VOID
SipSetAlgPassThrough (UINT4 u4PassThrough)
{
    gu4AlgPassThrough = u4PassThrough;
    return;
}

/*****************************************************************************/
/* Function     : SipSetTimeStamp                                            */
/* Description  : This function sets the global TimeStamp maintained in       */
/*                sipalg                                                     */
/* Input        : UINT4 u4TimeStamp                                          */
/* Output       : NONE                                                       */
/* Returns      : NONE                                                       */
/*****************************************************************************/
PUBLIC VOID
SipSetAlgTimeStamp (UINT4 u4TimeStamp)
{
    gu4AlgTimeStamp = u4TimeStamp;
    return;
}

/*****************************************************************************/
/* Function     : SipGetAlgPassThrough                                       */
/* Description  : This function gets the global PassThrough maintained in    */
/*                sipalg                                                     */
/* Input        : NONE                                                       */
/* Output       : NONE                                                       */
/* Returns      : gu4AlgPassThrough                                          */
/*****************************************************************************/
PUBLIC UINT4
SipGetAlgPassThrough ()
{
    return gu4AlgPassThrough;
}

/*****************************************************************************/
/* Function     : SipGetAlgTimeStamp                                         */
/* Description  : This function getis the global TimeStamp   maintained in   */
/*                sipalg                                                     */
/* Input        : NONE                                                       */
/* Output       : NONE                                                       */
/* Returns      : gu4AlgTimeStamp                                            */
/*****************************************************************************/
PUBLIC UINT4
SipGetAlgTimeStamp ()
{
    return gu4AlgTimeStamp;
}

#endif

#ifdef SIPALG_UT_FLAG
/****************************************************************************
* FUNCTION: sipAlgNatHashPrint
* DESCRIPTION: This function prints all elements in NAT hash 
*        with their values
*
* PARAMETERS: NONE
*
* Returns   : NONE
****************************************************************************/
void
sipAlgNatHashPrint (SipHash * pNatHash)
{
    SipHashIterator     dIterator;
    SIP_U32bit          i = 0;

    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgNatHashPrint");

    /* initialize the hash table's iterator */
    sip_hashInitIterator (pNatHash, &dIterator);

    /* go through the hash table entries */
    while (SIP_NULL != dIterator.pCurrentElement)
    {
        SipAlgNatHashElement *pTempNatHashElement = SIP_NULL;
        pTempNatHashElement =
            (SipAlgNatHashElement *) (dIterator.pCurrentElement->pElement);
        if (SIP_NULL == pTempNatHashElement)
        {
            PRINTF
                ("************************************************************\n");
            PRINTF ("No NatHash Element found at location %d\n", i++);
            PRINTF
                ("************************************************************\n");
            sip_hashNext (pNatHash, &dIterator);
            continue;
        }
        else
        {
            SIP_U32bit          listSizeDest = 0;
            SIP_U32bit          listSizeCall = 0;
            SipError            err;
            SIP_U32bit          listIter = 0;
            SIP_Pvoid           pIPPort = SIP_NULL;
            SipAlgIpPortPair   *pDestIPPort = SIP_NULL;
            SIP_Pvoid          *pMKey = SIP_NULL;
            SipAlgMediaKey     *pMediaKey = SIP_NULL;

            PRINTF
                ("************************************************************\n");
            PRINTF ("NatHash Element found at location %d\n", i);
            PRINTF ("%d Element of NatHash Table \n", i++);
            /* print original ip and port */
            PRINTF ("Interface id = %d\n", pTempNatHashElement->ifIndex);
            PRINTF ("originalIP = %x\t", pTempNatHashElement->originalIP);
            PRINTF ("originalPort = %u\n", pTempNatHashElement->originalPort);

            /* print dest ip,port mapped ip,port */
            (void) sip_listSizeOf (pTempNatHashElement->pDestInfoList,
                                   &listSizeDest, &err);
            if (listSizeDest == 1)
            {
                PRINTF ("pDestInfoList is present\n");
                listIter = 0;
                (void) sip_listGetAt (pTempNatHashElement->pDestInfoList,
                                      listIter, (SIP_Pvoid *) & pIPPort, &err);
                pDestIPPort = (SipAlgIpPortPair *) pIPPort;
                if (SIP_NULL != pDestIPPort)
                {

                    PRINTF ("DstIP = %x\t", pDestIPPort->destIP);
                    PRINTF ("destPort = %u\t", pDestIPPort->destPort);

                    PRINTF ("mappedIP = %x\t", pDestIPPort->mappedIP);
                    PRINTF ("mappedPort = %u\n", pDestIPPort->mappedPort);
                }
            }
            PRINTF ("protocol = %u\n", pTempNatHashElement->protocol);
            /* PRINTF("isBinding created = %d\n",pTempNatHashElement->pSipAlgTimerInfo->isBindingCreated); */
            PRINTF ("isRegTimerStarted = %d\n",
                    pTempNatHashElement->isRegTimerStarted);
            /* print callinfoList */
            (void) sip_listSizeOf (pTempNatHashElement->pCallInfoList,
                                   &listSizeCall, &err);
            listIter = 0;
            while (listIter != listSizeCall)
            {
                (void) sip_listGetAt (pTempNatHashElement->pCallInfoList,
                                      listIter, (SIP_Pvoid *) & pMKey, &err);
                pMediaKey = (SipAlgMediaKey *) pMKey;
                if (SIP_NULL != pMediaKey)
                {
                    PRINTF ("fromTag= %s\t", pMediaKey->pFromTag);
                    PRINTF ("ToTag= %s\t", pMediaKey->pToTag);
                    PRINTF ("pCallID = %s\n", pMediaKey->pCallID);
                }
                listIter++;
            }
            PRINTF
                ("************************************************************\n");
            sip_hashNext (pNatHash, &dIterator);
        }
    }
    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgNatHashPrint\n");
}

/****************************************************************************
* FUNCTION: sipAlgMediaHashPrint
* DESCRIPTION: This function prints all elements in Media hash 
*        with their values
*
* PARAMETERS: NONE
*
* Returns   : NONE
****************************************************************************/
void
sipAlgMediaHashPrint (SipHash * pMediaHash)
{
    SipHashIterator     dIterator;
    SIP_U32bit          i = 0;
    SIP_U32bit          tempIP = 0;
    SIPDEBUG ((SIP_S8bit *) "Entering into sipAlgMediaHashPrint");

    sip_hashInitIterator (pMediaHash, &dIterator);
    while (SIP_NULL != dIterator.pCurrentElement)
    {
        SipMediaHashElement *pTempMediaHashElement = SIP_NULL;
        pTempMediaHashElement =
            (SipMediaHashElement *) (dIterator.pCurrentElement->pElement);
        if (SIP_NULL == pTempMediaHashElement)
        {
            PRINTF ("No MediaHash Element found at location %d\n", i++);
            sip_hashNext (pMediaHash, &dIterator);
            continue;
        }
        else
        {
            SIP_U32bit          dSize = 0;
            SIP_U32bit          index = 0;
            SipError            dError;
            SIP_Pvoid          *pTemp = SIP_NULL;
            SipAlgMediaElement *pMediaElement = SIP_NULL;
            SIP_S8bit          *pMediaIp = SIP_NULL;
            if ((SipSuccess == sip_listSizeOf (pTempMediaHashElement->
                                               pMediaInfoList, &dSize, &dError))
                && (0 != dSize))
            {
                for (index = 0; index < dSize; index++)
                {
                    sip_listGetAt (pTempMediaHashElement->pMediaInfoList,
                                   index, (SIP_Pvoid *) & pTemp, &dError);
                    pMediaElement = (SipAlgMediaElement *) pTemp;
                    if (SIP_NULL != pMediaElement)
                    {
                        pMediaIp = (SIP_S8bit *) sip_memget
                            (0, SIP_ALG_INET_ADDRSTRLEN, &dError);
                        sip_memset ((SIP_Pvoid) pMediaIp, 0,
                                    SIP_ALG_INET_ADDRSTRLEN);
                        tempIP = 0;
                        tempIP = OSIX_NTOHL (pMediaElement->ipAddress);
                        if (sipAlgInetNetToStr (inetAFINET, (void *) &(tempIP),
                                                pMediaIp,
                                                SIP_ALG_INET_ADDRSTRLEN,
                                                &dError) == SipFail)
                        {
                            sip_memfree (0, (SIP_Pvoid *) & pMediaIp, SIP_NULL);
                            SIPDEBUG ((SIP_S8bit *)
                                      "Exiting sipAlgMediaHashPrint due to error in converting media ip to string format\n");
                            return;
                        }
                        PRINTF ("ipAddress of mediaElement = %s\n", pMediaIp);
                        sip_memfree (0, (SIP_Pvoid *) & pMediaIp, SIP_NULL);
                        PRINTF ("port of mediaElement = %u\n",
                                pMediaElement->port);
                        PRINTF ("refCount = %u\n", pMediaElement->refCount);
                    }
                }
            }
            sip_hashNext (pMediaHash, &dIterator);
        }
    }
    PRINTF ("Exiting sipAlgMediaHashPrint\n");
}

/****************************************************************************
* FUNCTION: sipAlgDeRegHashPrint
* DESCRIPTION: This function prints all elements in DeReg hash 
*        with their values
*
* PARAMETERS: NONE
*
* Returns   : NONE
****************************************************************************/
void
sipAlgDeRegHashPrint (SipHash * pDeRegHash)
{
    SipHashIterator     dIterator;
    SIP_U32bit          i = 0;
    SIP_U32bit          tempIP = 0;
    SIPDEBUG ((SIP_S8bit *) "Entering into sipAlgDeRegHashPrint");

    sip_hashInitIterator (pDeRegHash, &dIterator);
    while (SIP_NULL != dIterator.pCurrentElement)
    {
        SipAlgDeRegHashElement *pTempDeRegHashElement = SIP_NULL;
        pTempDeRegHashElement =
            (SipAlgDeRegHashElement *) (dIterator.pCurrentElement->pElement);
        if (SIP_NULL == pTempDeRegHashElement)
        {
            PRINTF ("No DeRegHash Element found at location %d\n", i++);
            sip_hashNext (pDeRegHash, &dIterator);
            continue;
        }
        else
        {
            SIP_U32bit          dSize = 0;
            SIP_U32bit          index = 0;
            SipError            dError;
            SIP_Pvoid          *pTemp = SIP_NULL;
            SipAlgDeRegElement *pDeRegElement = SIP_NULL;
            SIP_S8bit          *pDeRegIp = SIP_NULL;
            if ((SipSuccess == sip_listSizeOf (pTempDeRegHashElement->
                                               pDeRegInfoList, &dSize, &dError))
                && (0 != dSize))
            {
                for (index = 0; index < dSize; index++)
                {
                    sip_listGetAt (pTempDeRegHashElement->pDeRegInfoList,
                                   index, (SIP_Pvoid *) & pTemp, &dError);
                    pDeRegElement = (SipAlgDeRegElement *) pTemp;
                    if (SIP_NULL != pDeRegElement)
                    {
                        pDeRegIp = (SIP_S8bit *) sip_memget
                            (0, SIP_ALG_INET_ADDRSTRLEN, &dError);
                        sip_memset ((SIP_Pvoid) pDeRegIp, 0,
                                    SIP_ALG_INET_ADDRSTRLEN);
                        tempIP = 0;
                        tempIP = OSIX_NTOHL (pDeRegElement->ipAddress);
                        if (sipAlgInetNetToStr (inetAFINET, (void *) &(tempIP),
                                                pDeRegIp,
                                                SIP_ALG_INET_ADDRSTRLEN,
                                                &dError) == SipFail)
                        {
                            sip_memfree (0, (SIP_Pvoid *) & pDeRegIp, SIP_NULL);
                            SIPDEBUG ((SIP_S8bit *)
                                      "Exiting sipAlgDeRegHashPrint due to error in converting DeReg ip to string format\n");
                            return;
                        }
                        PRINTF ("ipAddress of DeRegElement = %s\n", pDeRegIp);
                        sip_memfree (0, (SIP_Pvoid *) & pDeRegIp, SIP_NULL);
                        PRINTF ("port of DeRegElement = %u\n",
                                pDeRegElement->port);
                        PRINTF ("isExpired = %u\n", pDeRegElement->isExpired);
                        PRINTF ("ifIndex = %d\n", pDeRegElement->ifIndex);
                    }
                }
            }
            sip_hashNext (pDeRegHash, &dIterator);
        }
    }
    PRINTF ("Exiting sipAlgDeRegHashPrint\n");
}

/* SIPALG - 2.1 */
/****************************************************************************
* FUNCTION: sipAlgIPHashPrint
* DESCRIPTION: This function prints all elements in IP hash 
*        with their values
*
* PARAMETERS: NONE
*
* Returns   : NONE
****************************************************************************/
void
sipAlgIPHashPrint (SipHash * pIPHash)
{
    SipHashIterator     dIterator;
    SIP_U32bit          i = 0;
    SIPDEBUG ((SIP_S8bit *) "Entering sipAlgIPHashPrint");

    /* initialize the hash table's iterator */
    sip_hashInitIterator (pIPHash, &dIterator);

    /* go through the hash table entries */
    while (SIP_NULL != dIterator.pCurrentElement)
    {
        SipAlgIPHashElement *pTempIPHashElement = SIP_NULL;
        pTempIPHashElement =
            (SipAlgIPHashElement *) (dIterator.pCurrentElement->pElement);
        if (SIP_NULL == pTempIPHashElement)
        {
            PRINTF
                ("************************************************************\n");
            PRINTF ("No IPHash Element found at location %d\n", i++);
            PRINTF
                ("************************************************************\n");
            sip_hashNext (pIPHash, &dIterator);
            continue;
        }
        else
        {
            PRINTF
                ("************************************************************\n");
            PRINTF ("IP Hash Element found at location %d\n", i++);
            PRINTF ("Private IP = %d\n", pTempIPHashElement->privateIP);
            PRINTF ("Mapped IP = %x\t", pTempIPHashElement->mappedIP);
            if (pTempIPHashElement->pSipAlgTimerInfo == SIP_NULL)
                PRINTF ("SIP ALG Timer is NULL\n");
            else
                PRINTF ("SIP ALG Timer is not NULL\n");
            PRINTF
                ("************************************************************\n");
            sip_hashNext (pIPHash, &dIterator);
        }
    }
    SIPDEBUG ((SIP_S8bit *) "Exiting sipAlgIPHashPrint\n");
}

#endif
#ifdef SECURITY_KERNEL_WANTED
/******************************************************************************
 *                                                                            *
 * Function     : sipAlgGetIPInterfaceInfo                                   *
 *                                                                            *
 * Description  : This is a synchronous call and will be invoked              *
 *                by Sip ALG.                                                 *
 *                This function gets the information about the given ip       *
 *                (u4IPAddr) whether it is a LAN or WAN type. IN both the     *
 *                cases it should return the network type and next hop ip     *
 *                address.                                                    *
 *                                                                            *
 * Input (s)    : u4IpAddr - Ip Address                                       * 
 *                                                                            *
 * Output (s)   : pRouteInfo - Pointer to tRouteInfo .structure element       *
 *                u4IfType should be either LAN or WAN.Structure element      *
 *                u4NextHop should be next hop Ip Address either in lan or    *
 *                wan side.Calling function should allocate memory of         *
 *                tLinkIpInfo size which will be filled by this function,     *
 *                in case of failure this structure fields will be filled     *
 *                by zero.                                                    *
 *                                                                            *
 * Return       : OSIX_SUCCESS/OSIX_FAILURE                                   *
 *                                                                            *
 ******************************************************************************/

UINT4
sipAlgGetIPInterfaceInfo (tInIfInfo InIfInfo, tRouteInfo * pRouteInfo)
{
    tCfaIfInfo          IfInfo;
    UINT4               u4IfIndex = 0;

    sip_memset ((SIP_Pvoid) & IfInfo, 0, sizeof (tCfaIfInfo));

    if (SecUtilIpIfGetIfIndexFromIpAddress
        (InIfInfo.u4DestIPAddr, &u4IfIndex) == OSIX_SUCCESS)
    {
        if (SecUtilGetIfInfo (u4IfIndex, &IfInfo) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
        pRouteInfo->u4IfType = IfInfo.u1NwType;
        STRCPY (pRouteInfo->au1LinkName, IfInfo.au1IfName);
    }
    return OSIX_SUCCESS;
}
#else
/******************************************************************************
 *                                                                            *
 * Function     : sipAlgGetIPInterfaceInfo                                   *
 *                                                                            *
 * Description  : This is a synchronous call and will be invoked              *
 *                by Sip ALG.                                                 *
 *                This function gets the information about the given ip       *
 *                (u4IPAddr) whether it is a LAN or WAN type. IN both the     *
 *                cases it should return the network type and next hop ip     *                      *                address.                                                    *
 *                                                                            *
 * Input (s)    : u4IpAddr - Ip Address                                       *
 *                                                                            *
 * Output (s)   : pRouteInfo - Pointer to tRouteInfo .structure element       *
 *                u4IfType should be either LAN or WAN.Structure element      *
 *                u4NextHop should be next hop Ip Address either in lan or    *
 *                wan side.Calling function should allocate memory of         *
 *                tLinkIpInfo size which will be filled by this function,     *
 *                in case of failure this structure fields will be filled     *
 *                by zero.                                                    *
 *                                                                            *
 * Return       : OSIX_SUCCESS/OSIX_FAILURE                                   *
 *                                                                            *
 ******************************************************************************/

UINT4
sipAlgGetIPInterfaceInfo (tInIfInfo InIfInfo, tRouteInfo * pRouteInfo)
{
    tRtInfoQueryMsg     RtInfoQueryMsg;
    tNetIpv4RtInfo      NetIpRtInfo;
    UINT4               u4NextHop = 0;
    UINT4               u4IfIndex = 0;
    UINT1               u1IfType = 0;
    UINT4               u4IfIpAddr = 0;
    UINT1               au1LinkName[20];
#ifdef VPN_WANTED
    INT4                i4IfIndex = 0;
    UINT1               u1Found = FALSE;
    UINT1               u1VPNIfType = 0;
#endif /* VPN_WANTED */

    MEMSET (au1LinkName, 0, 20);

    if (pRouteInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pRouteInfo, 0, sizeof (tRouteInfo));
    MEMSET (&RtInfoQueryMsg, 0, sizeof (tRtInfoQueryMsg));
    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));

#ifdef NAT_WANTED
    RtInfoQueryMsg.u2AppIds = 0;
    RtInfoQueryMsg.u1QueryFlag = 0x01;
    RtInfoQueryMsg.u4DestinationIpAddress = InIfInfo.u4DestIPAddr;
    RtInfoQueryMsg.u4DestinationSubnetMask = 0xffffffff;

    /* Calling Api to get routing information for a given ip address */
    if (NetIpv4GetRoute (&RtInfoQueryMsg, &NetIpRtInfo) == NETIPV4_FAILURE)
    {
        pRouteInfo->u4IpAddr = OSIX_HTONL (InIfInfo.u4DestIPAddr);
        pRouteInfo->u4ErrCode = NATIPC_ROUTE_NOT_FOUND;
        pRouteInfo->u4NextHop = 0;
        pRouteInfo->u4IfIndex = 0;
        pRouteInfo->u4IfType = 0;
    }

    /* Getting the Cfa If Index from Ip If Index or Port no */
    else if (NetIpv4GetCfaIfIndexFromPort (NetIpRtInfo.u4RtIfIndx, &u4IfIndex)
             == NETIPV4_FAILURE)
    {
        pRouteInfo->u4IpAddr = OSIX_HTONL (InIfInfo.u4DestIPAddr);
        pRouteInfo->u4ErrCode = NATIPC_ROUTE_NOT_FOUND;
        pRouteInfo->u4NextHop = 0;
        pRouteInfo->u4IfIndex = 0;
        pRouteInfo->u4IfType = 0;
    }
    else
#endif
    {
        if (CfaGetIfNwType (u4IfIndex, &u1IfType) == CFA_FAILURE)
        {
            PRINTF ("GET IF NETWORK TYPE FAILED\r\n");
        }
        u4NextHop
            =
            (NetIpRtInfo.u4NextHop ==
             0) ? InIfInfo.u4DestIPAddr : NetIpRtInfo.u4NextHop;

#ifdef VPN_WANTED
        /* Checking whether Vpn policy is configured against these 3 tuples  */
        /* Any case we need to fetch Wan IP for which policy applied */
        if (VPNCheckIpInRAAddrPool (InIfInfo.u4DestIPAddr) == OSIX_FAILURE)
        {
            if (VpnUtilMatchAclDestIP (InIfInfo.u4DestIPAddr, &i4IfIndex) ==
                OSIX_SUCCESS)
            {
                u1Found = TRUE;
                u1VPNIfType = NAT_SIP_LINK_SITE_SITE_VPN_TYPE;
            }
        }
        else
        {
            u1Found = TRUE;
            u1VPNIfType = NAT_SIP_LINK_RA_VPN_TYPE;
        }
#endif /* VPN_WANTED */
        pRouteInfo->u4IpAddr = OSIX_HTONL (InIfInfo.u4DestIPAddr);
        pRouteInfo->u4NextHop = OSIX_HTONL (u4NextHop);
        pRouteInfo->u4IfIndex = u4IfIndex;
        if (u1IfType == CFA_NETWORK_TYPE_LAN)
        {
            pRouteInfo->u4IfType = CFA_NETWORK_TYPE_LAN;
        }
#ifdef VPN_WANTED

        /* For Lan ip if we get a success should not accept as vpn
         * if interface is not WAN because there is a scenario when
         * acl will be configured as src ip - x.x.x.x dest ip - 0.0.0.0
         * in that case even though x.x.x.x is a Lan IP Api will return
         * match which we can not avoid */
        else if (((u1IfType == CFA_NETWORK_TYPE_WAN) && (u1Found == TRUE)))
        {
            pRouteInfo->u4IfType = u1VPNIfType;
        }
#endif /* VPN_WANTED */
        else if (u1IfType == CFA_NETWORK_TYPE_WAN)
        {
            pRouteInfo->u4IfType = CFA_NETWORK_TYPE_WAN;
        }

        if (CfaGetIfIpAddr (u4IfIndex, &u4IfIpAddr) == OSIX_SUCCESS)
        {
            pRouteInfo->u4LinkIPAddr = OSIX_HTONL (u4IfIpAddr);
        }

        if (CfaGetInterfaceNameFromIndex (u4IfIndex, &(au1LinkName[0]))
            == OSIX_SUCCESS)
        {
            STRCPY (&(pRouteInfo->au1LinkName[0]), &(au1LinkName[0]));
        }

    }

    return OSIX_SUCCESS;
}
#endif
