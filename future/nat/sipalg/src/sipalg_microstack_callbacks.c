/********************************************************************
 * Copyright (C) Aricent Communications Sotware,2007
 *
 * $Id: sipalg_microstack_callbacks.c,v 1.2 2011/06/08 05:03:44 siva Exp $
 *
 *******************************************************************/

#ifdef __cplusplus
extern              "C"
{
#endif
#ifndef NUM_ITERATIONS
#define NUM_ITERATIONS  1000
#endif
#include "fsapsys.h"
#include "sipalg_inc.h"
#define MAXMESG 5000
    INT4                showMessage = 1;
    INT4                constructCLen;

    void                sip_freeEventContext (SipEventContext * context)
    {
        (void) context;
    }

/* Implementaion of the fast_stopTimer interface reqiuired by the stack
   Application developers may choose to implement this function in any
   manner while preserving the interface
*/
    SipBool
         
         
         
         
         
         
        fast_stopTimer (SipTimerKey * inkey,
                        SipTimerKey ** outkey, SIP_Pvoid * buffer,
                        SipError * err)
    {
        (void) inkey;
        (void) outkey;
        (void) buffer;
        (void) err;
        return SipSuccess;
    }

    SipBool             fast_startTimer
        _ARGS_ ((SIP_U32bit duration,
                 SIP_S8bit restart,
                 sip_timeoutFuncPtr timeoutfunc,
                 SIP_Pvoid buffer, SipTimerKey * key, SipError * err))
    {
        (void) duration;
        (void) restart;
        (void) buffer;
        (void) *key;
        (void) timeoutfunc;
        (void) *err;
        return SipSuccess;

    }

    SipBool             sip_sendToNetwork
#ifdef ANSI_PROTO
     
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        (SIP_S8bit * buffer, SIP_U32bit buflen, SipTranspAddr * addr,
         SIP_S8bit transptype, SipError * err)
#else
     
        
        
        
        
                      (buffer, buflen, addr, transptype, err) SIP_S8bit *buffer;
    SIP_U32bit          buflen;
    SipTranspAddr      *addr;
    SIP_S8bit           transptype;
    SipError           *err;
#endif
    {
        (void) *buffer;
        (void) buflen;
        (void) *addr;
        (void) transptype;
        (void) *err;
        return SipSuccess;
    }

#ifdef SIP_TXN_LAYER
#ifdef ANSI_PROTO
    void 
         
         
         
         
         
         
         
        sip_indicateTimeOut (SipEventContext * context, en_SipTimerType dTimer)
#else
    void 
         
         
         
                sip_indicateTimeOut (context, dTimer) SipEventContext *context;
    en_SipTimerType     dTimer;
#endif
#else
#ifdef ANSI_PROTO
    void                sip_indicateTimeOut (SipEventContext * context)
#else
    void                sip_indicateTimeOut (context) SipEventContext *context;
#endif
#endif
    {
        (void) context;
#ifdef SIP_TXN_LAYER
        (void) dTimer;
#endif
    }
