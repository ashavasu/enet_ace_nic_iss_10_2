
##############################################################
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
#
# $Id: make.h,v 1.6 2013/06/23 13:09:04 siva Exp $
#
# Description: This file contains all the warning options
#              that are used for building this module.
##############################################################

# +--------------------------------------------------------------------------+
# |   FILE  NAME      : make.h     |
# |           |
# |   PRINCIPAL AUTHOR : Future Software    |
# |           |
# |   MAKE TOOL(S) USED : Eg: GNU MAKE      |
# |           |
# |   TARGET ENVIRONMENT     : LINUX      |
# |           |
# |   DATE     : 13 march 2000     |
# |           |
# |   DESCRIPTION     : This file contains all the warning options    |
# |       that are used for building this module |
# +--------------------------------------------------------------------------+
#
#     CHANGE RECORD :
# +--------------------------------------------------------------------------+
# | VERSION | AUTHOR/ | DESCRIPTION OF CHANGE        |
# |  | DATE |         |
# +---------|------------|---------------------------------------------------+
# |   1 |  | Creation       |
# |  |  |         |
# |  | 19/10/1999 |         |
# +--------------------------------------------------------------------------+
# |   2 | 14/02/01 | Modification done for NAT packaging  |
# +--------------------------------------------------------------------------+

PROJECT_NAME  = FutureNAT
PROJECT_BASE_DIR = ${BASE_DIR}/nat

PROJECT_SOURCE_DIR = ${PROJECT_BASE_DIR}/src
PROJECT_INCLUDE_DIR = ${PROJECT_BASE_DIR}/inc
PROJECT_OBJECT_DIR = ${PROJECT_BASE_DIR}/obj
PROJECT_EXECUTABLE_DIR = ${PROJECT_BASE_DIR}/exe
NPAPI_INC_DIR           = ${BASE_DIR}/npapi/bcmx

MICROSTACK_DIR      = ${BASE_DIR}/nat/sip/microstack/sip_stack_3_1_src
PROJECT_ALG_INCLUDE_DIR = ${PROJECT_BASE_DIR}/sipalg/inc
PROJECT_ALG_SOURCE_DIR = ${PROJECT_BASE_DIR}/sipalg/src
CLIENT_INCLUDE_DIR = ${BASE_DIR}/inc
               
MICROSTACK_INCLUDES_DIR =  -I${MICROSTACK_DIR} \
            -I${MICROSTACK_DIR}/stack_headers 

#sip inlcude files path 
SIP_INCLUDE_DIR = -I${BASE_DIR}/sip/inc

CASSIP_INC_DIR             = ${BASE_DIR}/cassipipc/cassip/inc

#PROJECT_COMPILATION_SWITCHES += -I${PROJECT_BASE_DIR}/upnp/libupnp-1.4.0/upnp/src/inc

ALGINCFILES = \
      $(PROJECT_ALG_INCLUDE_DIR)/sipalg_utils.h \
      $(PROJECT_ALG_INCLUDE_DIR)/sipalg_portlayer.h \
      $(PROJECT_ALG_INCLUDE_DIR)/sipalg_internal.h \
      $(PROJECT_ALG_INCLUDE_DIR)/sipalg_parser.h \
      $(PROJECT_ALG_INCLUDE_DIR)/sipalg_defs.h \
      $(PROJECT_ALG_INCLUDE_DIR)/sipalg_inc.h

COMMONINCFILES  = \
         $(PROJECT_INCLUDE_DIR)/nattdfs.h \
         $(PROJECT_INCLUDE_DIR)/natmemac.h \
         $(PROJECT_INCLUDE_DIR)/natdebug.h \
         $(PROJECT_INCLUDE_DIR)/natport.h \
         $(PROJECT_INCLUDE_DIR)/natglobal.h \
         $(PROJECT_INCLUDE_DIR)/natdefn.h \
         $(PROJECT_INCLUDE_DIR)/natextn.h \
         $(PROJECT_INCLUDE_DIR)/natinc.h \
         $(PROJECT_BASE_DIR)/Makefile \
         $(PROJECT_BASE_DIR)/make.h

ifeq (${TARGET_ASIC}, BCMX)
   COMMONINCFILES      +=  $(NPAPI_INC_DIR)/npip.h
endif
   

INCLUDEFILES    = \
         $(COMMONINCFILES) \
         $(COMMON_DEPENDENCIES)

PROJECT_FINAL_INCLUDES = -I$(PROJECT_INCLUDE_DIR) \
   -I$(COMMON_INCLUDE_DIRS)\
   -I$(PROJECT_ALG_INCLUDE_DIR) \
   -I$(CLIENT_INCLUDE_DIR)
