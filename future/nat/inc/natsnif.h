/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: natsnif.h,v 1.1.1.1 2008/10/03 09:25:30 prabuc-iss Exp $
 *
 * Description:This file contains the typedefs for the SNMP
 *             related structures which are required for NAT
 *             low level routines. It is assumed that the
 *             standard definitons for OID type and OCTET
 *             STRING TYPE do not change in future.
 *
 *******************************************************************/
#define NATIFTABLE_INITIALIZE(NODE, STATUS) \
    NODE->u1NatEnable  = NAT_DISABLE ; \
    NODE->u1NaptEnable = NAT_DISABLE ; \
    NODE->u1TwoWayNatEnable = NAT_DISABLE; \
    NODE->u4NumOfTranslation = 0; \
    NODE->u4NumOfActiveSessions = 0; \
    NODE->u4NumOfPktsDropped  = 0; \
    NODE->u4NumOfSessionsClosed = 0; \
    NODE->u4CurrTranslatedLocIpAddr  = 0; \
    NODE->u4TranslatedLocIpRangeEnd = 0;  \
    NODE->u4CurrDnsTranslatedLocIpInUse = 0; \
    NODE->u4CurrDnsTranslatedLocIpRangeEnd  = 0; \
    NODE->i4RowStatus = STATUS; \
    TMO_SLL_Init (&NODE->TranslatedLocIpList); \
    TMO_SLL_Init (&NODE->StaticList);  \
    TMO_SLL_Init (&NODE->StaticNaptList);  \
    TMO_SLL_Init (&NODE->NatFreeGipList);

#define NATIFTABLE_RESET(NODE) \
    NODE->u1NatEnable  = NAT_DISABLE ; \
    NODE->u1NaptEnable = NAT_DISABLE ; \
    NODE->u1TwoWayNatEnable = NAT_DISABLE; \
    NODE->u4NumOfTranslation = 0; \
    NODE->u4NumOfActiveSessions = 0; \
    NODE->u4NumOfPktsDropped  = 0; \
    NODE->u4NumOfSessionsClosed = 0; \
    NODE->u4CurrTranslatedLocIpAddr  = 0; \
    NODE->u4TranslatedLocIpRangeEnd = 0;  \
    NODE->u4CurrDnsTranslatedLocIpInUse = 0; \
    NODE->u4CurrDnsTranslatedLocIpRangeEnd  = 0; \
    NODE->i4RowStatus = NAT_STATUS_INVLD;

