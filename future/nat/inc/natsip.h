/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: natsip.h,v 1.2 2013/10/25 10:50:07 siva Exp $
 *
 * Description:This file contains the NAT SIPALG macros.
 *
 ********************************************************************/

#ifndef _NAT_SIP_H
#define _NAT_SIP_H

#define  SIP_DEFAULT_LISTEN_PORT                   5060
#define  SIP_DEFAULT_TLS_PORT                      5061
#define  SIP_MAX_NO_SIG_PORT                       3
#define  NAT_CONVERT_MILLI_SEC                     1000
#define  NAT_ADD_1_SEC                             1000000

/****************************************************************************
* Function Name :  NatProcessSIPPkt
* Description   : This function translates Inbound/Outbound SIP pkt
*                 
* Input (s)  : 1. pBuf - Inbound/Outbound Pkt CRU buffer pointer
*            : 2. pHeaderInfo - Pkt Header Info
* Output (s) : The changed pkt payload
* Returns    : SUCCESS/FAILURE
*****************************************************************************/

PUBLIC INT4
NatProcessSIPPkt (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo);

/****************************************************************************
* Function Name :  NatTranslateSIPPktOut
* Description   : This function translates outbound SIP pkt
*                 
* Input (s)  : 1. pBuf - Incoming Pkt CRU buffer pointer
*            : 2. pHeaderInfo - Pkt Header Info
* Output (s) : The changed pkt payload
* Returns    : SUCCESS/FAILURE
*****************************************************************************/

PUBLIC INT4
NatTranslateSIPPktOut (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo);

/****************************************************************************
* Function Name :  NatTranslateSIPPktIn
* Description   : This function translates Inbound SIP pkt 
*                 
* Input (s)     : 1. pBuf - Inbound Pkt CRU buffer pointer
*               : 2. pHeaderInfo - Pkt Header Info
* Output (s)    : The changed pkt payload
* Returns       : SUCCESS/FAILURE
*****************************************************************************/

PUBLIC INT4
NatTranslateSIPPktIn (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo);


#endif
