/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: natextn.h,v 1.16 2013/10/25 10:50:07 siva Exp $
 *
 * Description: This file contains the functions used in most of
 *              the modules.
 *
 *******************************************************************/
#ifndef _NAT_EXTN_H
#define _NAT_EXTN_H

extern UINT4 gu4NatTmrEnable;
extern tTmrAppTimer gu4NatAppTimer;
extern UINT2  gau2NatGlobalFreePorts [512];
extern UINT2  gu2FreePortIndex;
extern tOsixSemId          gNatSemId;

#ifdef SDF_PASSTHROUGH
extern Sdf_st_logPassthruDelay *st_Invite; 
extern Sdf_st_logPassthruDelay *st_200INV; 
extern Sdf_st_logPassthruDelay *st_ACK; 
extern Sdf_st_logPassthruDelay *st_BYE; 
extern Sdf_st_logPassthruDelay *st_200BYE;
extern Sdf_st_logPassthruDelay *st_REG;
extern Sdf_st_logPassthruDelay *st_200REG;
#endif

#ifdef SDF_TIMESTAMP
extern Sdf_st_logTimeStamp *patLogTimeStamp[1000];
#endif

#ifndef UPNP_NAT_SERVICES
extern UINT4 gu4NatMapTimeout;
#endif

/* Singly linked list definition for static Policy NAT.
   This linked list is indexed by the policy Id and the filter Id. */
extern tTMO_SLL  gStaticPolicyNatList;

/* Singly linked list definition for dynamic Policy NAT.
   This linked list is indexed by the policy Id and the filter Id. */
extern tTMO_SLL  gDynamicPolicyNatList;
 
/* From natcom.c */
PUBLIC UINT4 NatGetGlobalIp ARG_LIST((UINT4 u4IfNum ));
PUBLIC UINT4 NatGetGlobalIpForDns ARG_LIST((UINT4 u4IfNum ));
PUBLIC UINT4 NatGetNextFreeGlobalIpPort ARG_LIST((UINT4 ,UINT2 ,UINT4, UINT2, 
                                                  UINT4 ,tGlobalInfo *));
PUBLIC VOID NatUpdateHeaderInfo ARG_LIST((tHeaderInfo *pHeaderInfo,tDynamicEntry *pDynamicEntry ));
PUBLIC UINT4 NatSearchLocIpAddrTable ARG_LIST((UINT4 u4LocIpAddr ));
PUBLIC UINT4 NatValidateIpAddress ARG_LIST((UINT4 u4IpAddr, UINT4
u4IpAddrType));
PUBLIC INT4
NatSearchString ARG_LIST((UINT1 *pData, UINT4 u4DataLen, CONST CHR1 *pStr));
PUBLIC UINT2
NatGetFreeGlobalPort ARG_LIST ((UINT4 u4IfNum, UINT2 u2LocPort));
UINT2
NatGetPrivilagedGlobalPort ARG_LIST ((VOID));
PUBLIC UINT4 
NatGetSubnetRangeIpAddress ARG_LIST ((UINT4 u4IpAddress, UINT4 u4Mask, 
                                      UINT4 *pu4StartIp, UINT4 *pu4EndIp));
/* From natmemac.c */


PUBLIC VOID NatAddFreeGipList ARG_LIST((UINT4 u4GlobalIpAddr, UINT4 u4IfNum ));
PUBLIC UINT4 NatMemCreate ARG_LIST((UINT4 u4NatTypicalNumOfEntries ));
PUBLIC VOID NatMemDeleteMemPool ARG_LIST((VOID ));
PUBLIC UINT4 NatDynamicAlloc ARG_LIST((tMemPoolId PoolId, UINT1 **ppu1Block));

/* From nattimer.c */
PUBLIC UINT4 NatStartTimer ARG_LIST((VOID ));
PUBLIC VOID NatStopTimer ARG_LIST((VOID ));
PUBLIC UINT4 NatInitTimer ARG_LIST((VOID ));
PUBLIC UINT4 NatDeInitTimer ARG_LIST((VOID ));

/* UPNP NAPT TIMER */
/* Function Prototype Declaration For Napt Timer */
PUBLIC INT4
NaptInitTimer (VOID);
PUBLIC INT4
NaptDeInitTimer (VOID);
INT4
NaptStartTimer (tNaptTimer * pTimer, UINT4 u4TimeOut);
INT4
NaptStopTimer (tNaptTimer * pTimer);
INT4
NaptReStartTimer (tNaptTimer * pTimer, UINT4 u4TimeOut);
INT4
NaptDeleteEntry (tNaptTimer * pTimer);
/* End of declaration  */ 
/* END */

/* From nattable.c */
PUBLIC  INT4  NatDeletePartialEntry ARG_LIST ((UINT4 u4IfNum));
PUBLIC UINT4 NatGetDynamicEntry ARG_LIST((tHeaderInfo *pHeaderInfo, UINT4 u4CreateSearchFlag));
PUBLIC UINT4 NatAddPartialLinksList ARG_LIST((tHeaderInfo *pHeaderInfo, UINT4 u4InIpAddr, UINT2 u2InPort, UINT1 u2PersistFlag ));
PUBLIC UINT4 NatSearchLocIpAddrTableUsingIp ARG_LIST((UINT4 u4LocIpAddr ));
PUBLIC VOID NatDeleteAllExpiredEntries ARG_LIST(( VOID ));
PUBLIC VOID NatDeleteTcpConnection ARG_LIST(( tHeaderInfo *pHeaderInfo, UINT1 u1AckBit ));
PUBLIC VOID NatDeleteUdpEntries ARG_LIST((VOID ));
PUBLIC VOID NatInitListsAndTables ARG_LIST(( VOID  ));
PUBLIC VOID NatCleanHashTables ARG_LIST(( VOID  ));
PUBLIC VOID NatDeleteHashTables ARG_LIST(( VOID  ));
PUBLIC UINT4 NatCreateDnsTable ARG_LIST((UINT4 u4IpAddr, UINT4 u4IfNum ));
PUBLIC UINT4 NatLookUpDnsTable ARG_LIST(( UINT4 u4IpAddr ));
PUBLIC UINT4 NatSearchNfsTable ARG_LIST((tHeaderInfo *pHeaderInfo));
PUBLIC VOID NatDeleteNfsEntry ARG_LIST((VOID));
PUBLIC UINT4 NatAddNatOnlyTable ARG_LIST((UINT4 u4LocIpAddr, UINT4 u4GlobalIpAddr, UINT4 u4IfNum, UINT1 u1MulticastFlag));
PUBLIC tNatOnlyNode *NatSearchNatOnlyTable ARG_LIST(( UINT4 u4IpAddr, UINT4 u4IfNum));
PUBLIC VOID NatDeleteDynamicEntryWithIPAndMask ARG_LIST ((UINT4 , UINT4, UINT4));
PUBLIC UINT4 NatAddNatOnlyTableForIpMul ARG_LIST((UINT4 u4LocIpAddr,     UINT4 u4TranslatedLocIpAddr, UINT4 u4IfNum));

PUBLIC INT4 NatDeleteEntry ARG_LIST(( tDynamicEntry *pNode ));
PUBLIC VOID NatReleaseDynamicStructs ARG_LIST((VOID)) ;
PUBLIC VOID NatReleaseFromSLL ARG_LIST((tTMO_SLL *pList,tMemPoolId MemPoolId));
PUBLIC UINT4 NatSearchListAndTable ARG_LIST ((tHeaderInfo * pHeaderInfo, UINT4 *pu4InIpAddr, UINT2 *pu2InPort));
PUBLIC UINT4 NatIsNfs ARG_LIST ((tHeaderInfo * pHeadInfo));

/* To support port neg. applications */
PUBLIC tNatAppDefn * NatCheckPortNegAppln (UINT4 u4Protocol, UINT2 u2Port);
PUBLIC INT4 NatUnRegisterAppln (tNatAppDefn *gNatAppl);
PUBLIC INT4 NatRegisterAppln (tNatAppDefn *gNatAppl, UINT4 u4Protocol, UINT2 u2Port, UINT1 u1App);
tStaticNaptEntry *
NatGetStaticNaptEntry ARG_LIST ((UINT4 *pu4IpAddr, UINT2 *pu2Port, UINT4 u4Direction, UINT4 u4IfNum, UINT2 u2PktType));
PUBLIC INT4
NatSearchTransPortInNaptTable ARG_LIST((UINT4 u4IfNum, UINT2 u2Port));

PUBLIC UINT4
NatGetTransIpFromStaticNaptTbl ARG_LIST((UINT4 u4IfNum, UINT4 u4IpAddr));
INT1
NatConfigureGlobalIp (UINT4 u4Interface,UINT4 u4IpAddress,UINT4 u4Status);

INT1
NatRemoveDynamicEntry (tDynamicEntry *pDynamicEntry);
INT1
NatSipDeletePartialEntries (tNatPartialLinkNode *pNatPartialRTPLinkNode,tDynamicEntry *pDynamicEntry);
INT1
NatSipDeleteNaptFwlPinhole(tStaticNaptEntry *pNatStaticNaptEntry);
INT4
NatUtilFwlAddPartialLink (UINT4 u4LocalIP, UINT2 u2LocPort, UINT4 u4RemoteIP,
                          UINT2 u2RemotePort, UINT1 u1Protocol, 
                          UINT1 u1Direction, UINT1 u1AppCallStatus);

/* From natmain.c */
PUBLIC UINT4 NatGetTransportHeaderLength ARG_LIST((tCRU_BUF_CHAIN_HEADER *pBuf, UINT1 u1PktType ));
PUBLIC UINT4 NatGetIpHeaderLength ARG_LIST(( tCRU_BUF_CHAIN_HEADER *pBuf  ));
PUBLIC UINT4 NatGetSrcIpAddr ARG_LIST((tCRU_BUF_CHAIN_HEADER *pBuf ));
PUBLIC UINT4 NatGetDestIpAddr ARG_LIST((tCRU_BUF_CHAIN_HEADER *pBuf ));
PUBLIC UINT2 NatGetSrcPort ARG_LIST(( tCRU_BUF_CHAIN_HEADER *pBuf  ));
PUBLIC UINT2 NatGetDestPort ARG_LIST(( tCRU_BUF_CHAIN_HEADER *pBuf  ));
PUBLIC VOID  NatInitHeaderInfo ARG_LIST((tCRU_BUF_CHAIN_HEADER *pRestBuf,tHeaderInfo *pHeaderInfo,UINT4 u4Direction,UINT4 u4IfNum ));
PUBLIC VOID  NatFragmentPktInitHeaderInfo ARG_LIST((tCRU_BUF_CHAIN_HEADER *pRestBuf,tHeaderInfo *pHeaderInfo,UINT4 u4Direction,UINT4 u4IfNum ));

PUBLIC INT1 NatDeInit ARG_LIST((VOID));
PUBLIC VOID NatIfInitFields ARG_LIST((UINT4 u4IfNum));



/* From nathead.c */
PUBLIC VOID NatDataAdjust ARG_LIST(( UINT1 *pNewData, INT4 i4OldLen, INT4 i4NewLen ));
PUBLIC VOID  NatChecksumAdjust ARG_LIST(( UINT1 *pu1Chksum, UINT1 *pOldData, INT4 i4OldDataLen, UINT1 *pNewData, INT4 i4NewDataLen   ));
PUBLIC VOID  NatTcpHeaderModify ARG_LIST((tCRU_BUF_CHAIN_HEADER *pRestBuf,tHeaderInfo *pHeaderInfo ));
PUBLIC VOID  NatUdpHeaderModify ARG_LIST((tCRU_BUF_CHAIN_HEADER *pRestBuf,tHeaderInfo *pHeaderInfo ));
PUBLIC VOID NatIpHeaderModify ARG_LIST((tCRU_BUF_CHAIN_HEADER *pRestBuf,tHeaderInfo *pHeaderInfo ));
PUBLIC VOID  NatChecksumAdjustForIpHeader ARG_LIST(( UINT1 *pu1Chksum, UINT1 *pu1TcpChksum, UINT1 *pOldData, INT4 i4OldDataLen,UINT1 *pNewData, INT4 i4NewDataLen   ));
PUBLIC VOID NatPortModify ARG_LIST(( tCRU_BUF_CHAIN_HEADER *pBuf, tHeaderInfo
*pHeaderInfo, UINT2 u2NewPort, UINT4 u4PortOffset, UINT4 u4CksumOffset));
PUBLIC UINT2 NatTransportCksumAdjust ARG_LIST ((tCRU_BUF_CHAIN_HEADER *pBuf,
          tHeaderInfo *pHeaderInfo));

PUBLIC INT4 NatProcessIcmp ARG_LIST((tCRU_BUF_CHAIN_HEADER *pBuf, UINT4,UINT4));
PUBLIC UINT4 NatProcessFtp ARG_LIST((tCRU_BUF_CHAIN_HEADER *pBuf, tHeaderInfo *pHeaderInfo));
PUBLIC UINT4 NatProcessSmtp ARG_LIST((tCRU_BUF_CHAIN_HEADER *pBuf, tHeaderInfo *pHeaderInfo));
PUBLIC UINT4 NatProcessHttp ARG_LIST((tCRU_BUF_CHAIN_HEADER *pBuf, tHeaderInfo *pHeaderInfo));
PUBLIC UINT4 NatProcessNfs ARG_LIST((tHeaderInfo *pHeaderInfo));
PUBLIC UINT4 NatProcessDnsTcpOrUdp ARG_LIST((tCRU_BUF_CHAIN_HEADER *pBuf, tHeaderInfo *pHeaderInfo));
PUBLIC UINT4 NatIsLocalOrGlobalIpAddress ARG_LIST((UINT4 u4IpAddr));
PUBLIC VOID NatIntToAscii ARG_LIST((UINT2 u2OldPort, INT1 ai1Port[]));

PUBLIC UINT4 NatGetDeltaForSeqAck ARG_LIST((tHeaderInfo *pHeaderInfo, 
                      tNatSeqAckHistory *pNatSeqAckHistory));
PUBLIC UINT4 NatAddDeltaForSeqAck ARG_LIST((tHeaderInfo *pHeaderInfo, 
                                     tNatSeqAckHistory *pNatSeqAckHistory));

/* From natdbg.c */

PUBLIC UINT4 NatDbgMalloc ARG_LIST((const INT1 *pi1FileName, const INT4 i4LineNo, tMemPoolId MemPoolId, UINT1 **AddrPtr));
PUBLIC VOID NatDbgFree ARG_LIST((tMemPoolId MemPoolId, UINT1 *pu1Block));
PUBLIC VOID NatDbgPrintUnFreed ARG_LIST((VOID));
PUBLIC UINT4 NatGetPoolUnitSizeFromPoolId  ARG_LIST((tMemPoolId MemPoolId));
PUBLIC VOID NatInitiateSessions ARG_LIST((UINT2,UINT1));
PUBLIC VOID NatDeleteSessions ARG_LIST((UINT2,UINT2, UINT1));
PUBLIC VOID NatInitiateFromDifferentHosts ARG_LIST((UINT2,UINT1));
PUBLIC VOID NatDeleteFromDifferentHosts ARG_LIST((UINT2,UINT2, UINT1));

/* From ALG module */
PUBLIC UINT4 NatProcessIpMulticast ARG_LIST((tHeaderInfo *pHeaderInfo ));
 

/* H323 */
PUBLIC INT4 NatProcessH245Pkt ARG_LIST((tCRU_BUF_CHAIN_HEADER *pBuf, tHeaderInfo *pHeaderInfo ));
PUBLIC INT4 NatProcessH225Pkt ARG_LIST((tCRU_BUF_CHAIN_HEADER *pBuf, tHeaderInfo *pHeaderInfo ));

/* Stream Media */
PUBLIC INT4
NatProcessSmediaPkt ARG_LIST((tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo));

/*CU-SEE-ME */
PUBLIC INT4
NatProcessCuSeeMePkt ARG_LIST((tCRU_BUF_CHAIN_HEADER *pBuf, tHeaderInfo *pHeaderInfo));
/*CU-SEE-ME */

extern tNatAppDefn *gapNatNegPortApplnList[]; /* H323 */
/* This contains the current Translated Local Port to be assigned */
extern UINT4 gu4NatNextFreeTranslatedLocPort;
extern tTMO_SLL gNatFreePortList;
extern UINT4 gu4NatTestTrc;
/*from natexport.c */
PUBLIC INT1 NatAutoIpAddressConfig ARG_LIST((UINT4 u4Interface,UINT4 u4IpAddress,UINT4 u4Status));


/* function prototypes from natike.c */

PUBLIC UINT4 natProcessIKE (tCRU_BUF_CHAIN_HEADER *pbuf, tHeaderInfo *pHeaderInfo);
PUBLIC VOID natInitIKEHeaderInfo (tCRU_BUF_CHAIN_HEADER *pbuf, tHeaderInfo *pHeaderInfo, tIKEHeaderInfo *pIKEHeaderInfo);
PUBLIC UINT4 natProcessIKEOutboundPkt (tIKEHeaderInfo *pIKEHeaderInfo);
PUBLIC UINT4 natProcessIKEInboundPkt (tIKEHeaderInfo *pIKEHeaderInfo);
PUBLIC UINT4 natFormIKEHashKey (UINT1 *u1InitCookie);
PUBLIC UINT4 natCreateOutboundIKENode (tIKEHeaderInfo *pIKEHeaderInfo, UINT4 u4Hashkey);
PUBLIC UINT4 natCreateInboundIKENode (tIKEHeaderInfo *pIKEHeaderInfo, UINT4 u4key);
PUBLIC VOID natFillIKEHashNode (tIKEHashNode *pIKEHashNode, tIKEHeaderInfo *pIKEHeaderInfo, UINT4 u4IPAddr);
PUBLIC VOID natDeleteIKEEntries (VOID);
PUBLIC VOID natDeleteIKEHashNode (tIKEHashNode *pIKEHashNode);
PUBLIC UINT4 natDeleteIKESessionRow(INT4 i4NatIKESessionInterfaceNum ,  UINT4 u4NatIKESessionLocalIp ,  UINT4 u4NatIKESessionOutsideIp , UINT1* pNatIKESessionInitCookie); 

/* function prototypes from natipsec.c */
PUBLIC UINT4 natProcessIPSec (tCRU_BUF_CHAIN_HEADER *pbuf, tHeaderInfo * pHeaderInfo);
PUBLIC VOID natInitIPSecHeaderInfo (tCRU_BUF_CHAIN_HEADER *pbuf, tHeaderInfo *pHeaderInfo, tIPSecHeaderInfo *pIPSecHeaderInfo);
PUBLIC UINT4 natProcessIPSecOutboundPkt (tCRU_BUF_CHAIN_HEADER *pbuf, tIPSecHeaderInfo *pIPSecHeaderInfo);
PUBLIC UINT4 natProcessIPSecInboundPkt (tCRU_BUF_CHAIN_HEADER *pbuf, tIPSecHeaderInfo *pIPSecHeaderInfo);
PUBLIC UINT4 natFormIPSecHashKey (UINT4 u4value1, UINT4 u4value2, UINT4 u4type );
PUBLIC UINT4 natSearchAndCreateIPSecOutboundPkt (tIPSecHeaderInfo *pIPSecHeaderInfo);
PUBLIC UINT4 natCreateIPSecNode (tIPSecHeaderInfo *pIPSecHeaderInfo, tIPSecPendHashNode *pIPsecPendHashNode);
PUBLIC VOID natFillIPSecInAndOutHashNode (tIPSecHeaderInfo *pIPSecHeaderInfo,
       tIPSecInHashNode *pIPsecInboundHashNode, tIPSecOutHashNode *pIPSecOutboundHashNode, tIPSecPendHashNode *pIPSecPendHashNode);
PUBLIC VOID natDeleteIPSecPendingEntry (tIPSecHeaderInfo *pIPSecHeaderInfo,
       tIPSecPendHashNode *pIPsecPendHashNode);
PUBLIC UINT4 natCreateIPSecPendNodeOutboundPkt (tIPSecHeaderInfo *pIPSecHeaderInfo, UINT4 u4Key);
PUBLIC VOID natFillIPSecPendingNode (tIPSecPendHashNode *pIPSecPendHashNode,
       tIPSecHeaderInfo *pIPSecHeaderInfo,UINT4 u4IPAddr);
PUBLIC UINT4 natSearchAndCreateIPSecInboundPkt (tIPSecHeaderInfo *pIPSecHeaderInfo);
PUBLIC UINT4 natCreateIPSecPendNodeInboundPkt(tIPSecHeaderInfo *pIPSecHeaderInfo, UINT4 u4Key);
PUBLIC VOID natDeleteIPSecEntries (VOID);
PUBLIC VOID natDeleteIPSecPendHashNode (tIPSecPendHashNode *pPendHashNode);
PUBLIC VOID natDeleteIPSecInOutHashNode (tIPSecOutHashNode *pIPSecOutHashNode );
PUBLIC UINT4 natDeleteIPSecPendingRow(INT4 i4NatIPSecPendingInterfaceNum , UINT4 u4NatIPSecPendingLocalIp , UINT4 u4NatIPSecPendingOutsideIp , UINT4 u4NatIPSecPendingSPIInside , UINT4 u4NatIPSecPendingSPIOutside);
PUBLIC UINT4 natDeleteIPSecSessionRow(INT4 i4NatIPSecSessionInterfaceNum , UINT4 u4NatIPSecSessionLocalIp ,  UINT4 u4NatIPSecSessionOutsideIp , UINT4 u4NatIPSecSessionSPIInside ,  UINT4 u4NatIPSecSessionSPIOutside ); 

extern INT4               gi4NatIKEPortTranslation;
extern INT4               gi4NatIKETimeOut;
extern INT4               gi4NatIPSecTimeOut;
extern INT4               gi4NatIPSecPendingTimeOut;
extern INT4               gi4NatIPSecMaxRetry;

extern tTMO_SLL    gNatIPSecListNode;     /* Pointer to IPSec session List */
extern tTMO_SLL    gNatIPSecPendListNode; /* Pointer to IPSec Pending List */
extern tTMO_SLL    gNatIKEList;    /* Pointer to IKE session List */
extern tTMO_HASH_TABLE   *gpNatIKEHashList;   /* Pointer to IKE Hash Table */
extern tTMO_HASH_TABLE   *gpNatIPSecInHashList; /* Pointer to IPSec In Hash Table */
extern tTMO_HASH_TABLE   *gpNatIPSecOutHashList; /* Pointer to IPSec Out Hash Table */
extern tTMO_HASH_TABLE   *gpNatIPSecPendHashList; /* Pointer to IPSec Pending Hash Table */

/* NAT_Enh End */
/* Function Prototypes from nattrigger.c */
PUBLIC UINT1 NatGetFreeTrigInfoEntry (VOID);
PUBLIC UINT1 NatGetFreeRsvdTrigInfoEntry (VOID);
PUBLIC UINT1 NatFreeTrigInfoEntry (UINT1 *pu1AppName);
PUBLIC VOID  NatFreeRsvdTrigInfoEntry (VOID);
PUBLIC UINT1 NatAddEntryToTrigInfoList (UINT1 *pu1AppName,
                                        UINT1 *pu1InPortString, 
                                 UINT1 *pu1OutPortString, 
                                 UINT2 u2Protocol);
PUBLIC UINT1 NatAddEntryToRsvdTrigInfoList (tTriggerInfo * pTrigInfo, 
                                             UINT4 u4InIpAddr,
                                             UINT4 u4OutIpAddr);
PUBLIC UINT1 NatPortString2PortRange (tPortRange * pPortRange, 
                                       UINT1 *pu1PortString, 
           UINT2 *pu2MaxPortVal);
PUBLIC UINT1 NatCheckDuplicateTriggerPort (UINT1 *pu1AppName, 
                                           tPortRange * pOutPortRange,
                                           tPortRange * pInPortRange, 
                                    UINT2 u2Protocol);
PUBLIC UINT1 NatSearchOutBoundRsvdPortTriggerInfo (tHeaderInfo * 
                                                    pHeaderInfo);
PUBLIC UINT1 NatSearchInBoundRsvdPortTrigInf (tHeaderInfo * pHeaderInfo);
PUBLIC UINT1 NatSearchOutBoundPortTrigInf (tHeaderInfo * pHeaderInfo);
PUBLIC  UINT4 NatPortTrigInfoLookUp (tCRU_BUF_CHAIN_HEADER *pBuf, 
                                    tHeaderInfo * pHeaderInfo);
PUBLIC UINT4
NatUtilGetGlobalArrayIndex(UINT1 *pu1InBoundPortStr,
                                UINT1 *pu1OutBoundPortStr, UINT2 u2Protocol);
extern tTriggerInfo  gaTrigInfo[];
extern tRsvdTrigInfo gaRsvdTrigInfo[];

VOID FsNatDisableOnIntf (INT4 i4Intf);
INT4 FsNatEnableOnIntf (INT4 i4Intf);

/* DSL_ADD -E- */


VOID  cli_get_app_name (UINT2 u2Port, UINT1* pu1App);
INT4  CfaValidateMask  (UINT4 u4Mask);
UINT4 cli_check_valid_ip_address (UINT4 u4IpAddr, UINT4 u4Mask);
UINT4 CfaIsLocalNetworkIpAddr (UINT4 u4IpAddress);
UINT1 cli_get_proto_index (UINT1 *pu1Prot);
PUBLIC UINT4     NatScanPartialLinksList
 ARG_LIST ((tHeaderInfo * pHeaderInfo, UINT4 *pu4InIpAddr, UINT2 *pu2InPort));

UINT4  NatGetWanInterfaceIpAddr (UINT4 u4WanIf);

PUBLIC INT4 SipClearAllInterfaceBindings (INT4 i4IfNum);
VOID NatApiPartialCleanup(tNatPartialLinkNode *pPartialNode);
tNatPartialLinkNode* 
NatAppFindPartialNode(tNatPartialLinkNode *pPartialNode);
tNatPartialLinkNode *
NatGetAppInPartialEntry (UINT4 u4LocIpAddr, UINT2 u2LocPort, UINT4 u4Direction,
                         UINT1 u1PktType);


INT4
NaptReConfigureTimer ARG_LIST  ((INT4 i4IfNum, UINT4 u4Timeout));

VOID
NatConvertPortListToArray ARG_LIST ((tPortList PortList,
                           UINT4 *pu4IfPortArray, UINT1 *u1NumPorts));
INT4
NatGetPhysPortNameFromIfIndex ARG_LIST ((UINT2 u2IfIndex, INT1 *pi1PhyIfName));

PUBLIC tSNMP_OCTET_STRING_TYPE *SNMP_AGT_FormOctetString PROTO((UINT1 *, INT4));

INT1 SecUtilGetIfIpAddr (INT4, UINT4 *);
/* IP Related */



extern tSNMP_OCTET_STRING_TYPE *
allocmem_octetstring      PROTO((INT4));

extern VOID 
free_octetstring          PROTO((tSNMP_OCTET_STRING_TYPE *));


#endif /* _NAT_EXTN_H */
