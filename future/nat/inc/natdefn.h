/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: natdefn.h,v 1.9 2015/07/16 10:50:31 siva Exp $
 *
 * Description:This file contains the macros used in all the
 *             modules.
 *
 *******************************************************************/
/* Overall used #defines */
#ifndef _NATDEFN_H_
#define _NATDEFN_H_

#define   NAT_API1_CALLED                       255

#define   NAT_ALL_TRC                          0xffffffff

#define   NAT_STATUS_ACTIVE                       1
#define   NAT_STATUS_NOT_IN_SERVICE               2
#define   NAT_STATUS_NOT_READY                    3
#define   NAT_STATUS_CREATE_AND_GO                4
#define   NAT_STATUS_CREATE_AND_WAIT              5
#define   NAT_STATUS_DESTROY                      6
#define   NAT_STATUS_INVLD                       99
#define   NAT_INVLD_IP_ADDR_1            0x00000000
#define   NAT_INVLD_IP_ADDR_2            0xffffffff
#define   NAT_INVLD_MASK                 0x00000000
#define   NAT_INIT_MASK                  0xffffffff
#define   NAT_LOOPBACK_IPADDR            0x7f000000
#define   NAT_CLASS_C_MAX                0xdfffffff
#define   NAT_IS_IPADDR_CLASS_A(u4Addr)   ((u4Addr &  0x80000000) == 0)
#define   NAT_IS_IPADDR_CLASS_B(u4Addr)   ((u4Addr &  0xc0000000) == 0x80000000)
#define   NAT_IS_IPADDR_CLASS_C(u4Addr)   ((u4Addr &  0xe0000000) == 0xc0000000)
#define   NAT_IS_IPADDR_CLASS_D(u4Addr)   ((u4Addr &  0xf0000000) == 0xe0000000)
#define   NAT_IS_IPADDR_CLASS_E(u4Addr)   ((u4Addr &  0xf8000000) == 0xf0000000)

#define   NAT_PPP_NAPT_IPADDR_ACTIVE              4 
#define   NAT_PPP_NAPT_IPADDR_DESTROY             6

#define   NAT_ENTRY_FOUND                         10
#define   NAT_ENABLE_ALL                          1

#define   NAT_PENDING                             3
#define   NAT_TRUE                                1
#define   NAT_FALSE                               2
#define   NAT_CREATE                              1
#define   NAT_SEARCH                              2
#define   NAT_ADD                                 1
#define   NAT_DELETE                              2

#define   NAT_NFS                              2049
#define   NAT_DHCP_SERV                         67
#define   NAT_DHCP_CLNT                         68
#define   NAT_RIP_PORT                          520
#define   NAT_OSPF_PROT                         89 
#define   NAT_IGMP_PROT                         2
#define   NAT_RPC                               111
#define   NAT_PM                                635
#define   NAT_HEADER_LEN                          1
#define   NAT_IP_ADDR_LEN                         4
#define   NAT_PORT_LEN                            2
#define   NAT_FTP_DATA_PORT                      20
#define   NAT_FTP_CTRL_PORT                      21
#define   NAT_TCP_SYN_BIT_MASK                 0x02
#define   NAT_TCP_SYN_BIT                      0x02
#define   NAT_TCP_CODE_BYTE_OFFSET               13
#define   NAT_TCP_OPTIONS_OFFSET                 20
#define   NAT_TCP_OPTIONS_MSS                    2
#define   NAT_TFTP_PORT                          69
#define   NAT_HTTP_PORT                          80
#define   NAT_TELNET_PORT                        23
#define   NAT_NTP_PORT                          123
#define   NAT_DNS_PORT                           53
#define   NAT_SMTP_PORT                          25
#define   NAT_HTTP_PORT                          80
#define   NAT_PROT_OFFSET_IN_IPHEADER             9
#define   NAT_PKTLEN_OFFSET_IN_IPHEADER           2
#define   NAT_IP_PROT_FIELD_LEN                   1
#define   NAT_IP_TOTAL_LEN                        2
#define   NAT_HEADER_LEN_MASK                  0x0f
#define   NAT_IP_LEN_OFFSET                       0
#define   MAX_UDP_PKT_SIZE                      512
#define   NAT_TCP_LENGTH_OFFSET                  12
#define   NAT_IP_HEADER_LENGTH                   20
#define   NAT_TCP_HEADER_LENGTH                  20
#define   NAT_ICMP_HEADER_LENGTH                  8
#define   NAT_UDP_HEADER_LENGTH                   8
#define   NAT_ADDL_HEADER_LENGTH                 60
           
#define  NAT_MIN_PRIVILAGED_PORT                512
#define  NAT_MAX_PRIVILAGED_PORT               1023
#define  NAT_RLOGIN_PORT                        513
#define  NAT_MAX_FREE_PRIV_PORTS                510
           
/* FTP PORT or PASV command carries <IP,Port>
 * AAA,BBB,CCC,DDD,PPP,ppp -- 23 Bytes
 * We add 2 Bytes more for NULL char and for even offset adjustment 
 */
#define   NAT_IPADDR_PORT_LEN_MAX                25

#define   NAT_IPADDR_LEN_MAX                     17
#define   NAT_IPADDR_LEN_MAX_VPN                 15
#define   NAT_PORT_LEN_MAX                       10
#define   NAT_STRING_END                       '\0'
#define   NAT_STRCMP_SUCCESS                      0
#define   NAT_DEST_IP_OFFSET                     16
#define   NAT_SRC_IP_OFFSET                      12
#define   NAT_NONSYS_PORT                      5000
#define   NAT_INIT_PORT_NUMBER                 6001
#define   NAT_PORT_LIMIT                     0xffff 

#define   NAT_TELNET                             23
#define   NAT_NTP                               123
#define   NAT_MAX_PORT_NUMBER                 65535
#define   NAT_IID_OID_ARRAY_LIMIT               100
#define   NAT_ONLY_HASH_MASK             0x000000ff
#define   NAT_HASH_LIMIT                       1024
#define   NAT_ONLY_HASH_LIMIT                   255

#define NAT_DUMP_BUF_SIZE 100
/* All macro values in seconds */
#define   NAT_ONE_SEC          1                                          
#define   NAT_ONE_MIN          60                                         
#define   NAT_IDLE_TIME_OUT    NAT_DEF_IDLE_TIMEOUT * NAT_NUM_OF_SYS_TIME_UNITS_IN_A_SEC    
#define   NAT_UDP_TIME_OUT     NAT_DEF_UDP_TIMEOUT * NAT_NUM_OF_SYS_TIME_UNITS_IN_A_SEC    
#define   NAT_TCP_TIME_OUT     NAT_DEF_TCP_TIMEOUT * NAT_NUM_OF_SYS_TIME_UNITS_IN_A_SEC   
#define   NAT_FIN_TCP_TIME_OUT 240 * NAT_NUM_OF_SYS_TIME_UNITS_IN_A_SEC   
#define   NAT_PROT_TIME_OUT    120 * NAT_NUM_OF_SYS_TIME_UNITS_IN_A_SEC   
#define   NAT_NFS_TIME_OUT     86400 * NAT_NUM_OF_SYS_TIME_UNITS_IN_A_SEC 
#define   NAT_TWO_BYTE_MASK    0xffff                                     
#define   NAT_TWO_BYTE_SHIFT   16                                         
#define   NAT_NFS_COUNT        10                                         
#define   NAT_PERSIST_PARTIAL_LINK_TIMEOUT  3600 * NAT_NUM_OF_SYS_TIME_UNITS_IN_A_SEC
#define   NAT_NON_PERSIST_PARTIAL_LINK_TIMEOUT  30 * NAT_NUM_OF_SYS_TIME_UNITS_IN_A_SEC
#define   NAT_PARTIAL_LINK_TIMEOUT  30 * NAT_NUM_OF_SYS_TIME_UNITS_IN_A_SEC
/* For Fragmentation */
#define   NAT_FRAG_BIT_OFFSET         6
#define   NAT_MORE_FRAG_BIT      0x2000
#define   NAT_FRAG_OFFSET        0x1fff
#define   NAT_FRAG_DATA_OFFSET   0xfff8
/* For IP Multicast */
#define   NAT_IP_MULTICAST           1                                         
#define   NAT_MULTICAST_START_ADDR   0xe0000000                                
#define   NAT_MULTICAST_END_ADDR     0xf0000000                                
#define   NAT_MULTICAST_TIME_OUT     1000 * NAT_NUM_OF_SYS_TIME_UNITS_IN_A_SEC 

#define   ARG_LIST(x)                x                                         
#define   NAT_GET_SYS_TIME           nmhGetSysUpTime

/* ALG Specific #defines */

/******************************************************************************
*   MACROS USED BY HEADER-TRANSLATION MODULE
******************************************************************************/
#define   NAT_SEQ_NUM_LEN              4
#define   NAT_ACK_NUM_LEN              4
#define   NAT_TOL_LEN_FIELD            2
#define   NAT_TCP_FIN_BIT_SET          1
#define   NAT_TCP_RST_BIT_SET          4
#define   NAT_TCP_ACK_BIT_SET         16
#define   NAT_TCP_FIN_BIT         0x0001
#define   NAT_TOT_LEN_OFFSET           2
#define   NAT_TCP_MSS_OFFSET           2
#define   NAT_TCP_MSS_LEN              2
#define   NAT_WORD_LEN                 2
#define   NAT_IP_CHKSUM_OFFSET        10
#define   NAT_TCP_CKSUM_OFFSET        16
#define   NAT_UDP_CKSUM_OFFSET         6
#define   NAT_CRU_BUF_FROM             1
#define   NAT_CRU_BUF_OVER             2


/********************************************************************************    MACROS USED BY THE FTP MODULE
*******************************************************************************/

#define   NAT_FTP_PORT_LEN              5
#define   NAT_FTP_PASV_REPLY_LEN        4
#define   NAT_FTP_IP_COMMA_COUNT        4
#define   NAT_FTP_PORT                  1
#define   NAT_FTP_PASV                  2
#define   NAT_FTP_PORT_DIV_FACTOR     256
#define   NAT_PASV_IP_START_OFFSET     27
/********************************************************************************    MACROS USED BY THE ICMP  MODULE
*******************************************************************************/

#define   NAT_ICMP_ERROR_MSG                       255
#define   NAT_ICMP_QUERY_MSG                       1
#define   NAT_ICMP_ECHO_REQUEST                    8
#define   NAT_ICMP_ECHO_REPLY                      0
#define   NAT_ICMP_MSG_START_OFFSET               20
#define   NAT_ICMP                                 1
#define   NAT_ICMP_STARTID                      3000    
#define   NAT_ICMP_QUERY_ID_OFFSET                 4
#define   NAT_ICMP_QUERY_CODE_OFFSET               1
#define   NAT_ICMP_PAYLOAD_TRANSLATION_SUCCESS     1

/*****************************************************************************
 * MACROS USED IN SMTP_ALG MODULE FUNCTIONS
*****************************************************************************/

#define   NAT_MAILFROM             1
#define   NAT_FROM                 3
#define   NAT_RECV                 4
#define   NAT_NO_STATIC_ENTRY      0

#define   NAT_RCPTTO               2
#define   NAT_REPLYTO              5
#define   NAT_CC                   6
#define   NAT_BCC                  7
#define   NAT_SMTP_FIELD_LIMIT    10

/*****************************************************************************
* MACROS USED IN DNS_ALG MODULE FUNCTIONS
*****************************************************************************/
#define   NAT_DNS_HEADER_LENGTH                  12
#define   NAT_DNS_PKT_IDENTIFIER_LENGTH           2
#define   NAT_DNS_RR_TYPE_CLASS_TTL_LEN           8
#define   NAT_DNS_RR_TYPE_CLASS_LEN               4
#define   NAT_DNS_RDL_FIELD_LEN                   2
#define   NAT_IP_SRC_OFFSET                      12
#define   NAT_IP_DST_OFFSET                      16
#define   NAT_DNS_QUERY                           1
#define   NAT_DNS_RESPONSE                        2
#define   NAT_DNS_ATYPE_QUERY                     1
#define   NAT_DNS_PTR_ANSWER_TYPE                12
#define   NAT_DNS_PTR_QUERY                      12
#define   NAT_DNS_PTR_QUERY_ALL                 255
#define   NAT_DNS_TC_BIT_VALUE                  512
#define   NAT_INDEFINITE                          1
#define   NAT_DNS_RESPONSE_VALUE             0x8000
#define   NAT_DNS_RCODE_VALUE                     5
#define   NAT_DNS_AN_OFFSET                       6
#define   NAT_DNS_AR_OFFSET                      10
#define   NAT_DNS_QD_OFFSET                       4
#define   NAT_DNS_NS_OFFSET                       8
#define   NAT_DNS_ARCOUNT_LEN                     2
#define   NAT_DNS_ANCOUNT_LEN                     2
#define   NAT_DNS_NSCOUNT_LEN                     2
#define   NAT_DNS_QDCOUNT_LEN                     2
#define   NAT_DNS_ATYPE_ANSWER                    1
#define   NAT_DNS_MESSAGE_COMMPRESS_VALUE      0xc0
#define   NAT_DNS_TCP_LEN_OFFSET                  2  

/*****************************************************************************
* MACROS USED FOR PPTP_ALG MODULE FUNCTIONS
*****************************************************************************/
#define NAT_PPTP_STARTID                       4000
/***************************************************************************
**
* MACROS USED IN H323 ALG MODULE
****************************************************************************
*/

/* Add the count here when new ALG is added that uses negotiated port */
#define   NAT_H225_PORT               1720
#define   NAT_H323_DATA_NEG_PORT      1503
#define   NAT_MAX_PORT_NEG_APPLN      1

#define   NAT_PORT_NEG_APPLN_H323      0

/***********************************************************************
* MACROS USED IN SIP ALG MODULE
***********************************************************************/
#define NAT_SIP_TCPUDP_PORT 5060

/***************************************************************************
**
* MACROS USED IN STREAM_MEDIA ALG MODULE
****************************************************************************
*/

#define NAT_RTSP_PORT  554
#define NAT_PNA_PORT   7070 

#define NAT_RTSP_PORT_GROUP   2
/***************************************************************************
**
* MACROS USED IN CU_SEE_ME ALG MODULE
****************************************************************************
*/
#define NAT_CUSME_PORT      7648

#define NAT_L2TP_UDP_PORT   1701   
#define NAT_PPTP_TCP_PORT   1723

/***************************************************************************
**
* MACROS USED IN IPSEC MODULE
****************************************************************************
*/
#define NAT_IPSEC_SPI_LENGTH     4
#define NAT_IPSEC_PENDING          3
/*UT_FIX-START*/ 
/*NAT_UT_COMMON_FN_FUNC_FLT_20 -START*/
#define NAT_IPSEC_TIMEOUT     28800 * NAT_NUM_OF_SYS_TIME_UNITS_IN_A_SEC    
#define NAT_IPSEC_PENDING_TIMEOUT  30 * NAT_NUM_OF_SYS_TIME_UNITS_IN_A_SEC    
/*NAT_UT_COMMON_FN_FUNC_FLT_20 -END*/
/*UT_FIX-END*/ 
#define NAT_IPSEC_MAX_RETRY_COUNT  3
#define NAT_IPSEC_ESP                50
#define NAT_IPSEC_AH                  51

#define NAT_IKE_COOKIE_LENGTH   8
#define NAT_IKE_STD_PORT             500
#define NATT_IKE_STD_PORT            4500
/*UT_FIX-START*/ 
/*NAT_UT_COMMON_FN_FUNC_FLT_20 -START*/
#define NAT_IKE_TIMEOUT           28800 * NAT_NUM_OF_SYS_TIME_UNITS_IN_A_SEC    
/*NAT_UT_COMMON_FN_FUNC_FLT_20 -END*/
#define NAT_MIN_IKE_TIMEOUT      180
#define NAT_MAX_IKE_TIMEOUT      86400
#define NAT_MIN_IPSEC_TIMEOUT    180 
#define NAT_MAX_IPSEC_TIMEOUT    86400
#define NAT_MIN_IPSEC_PENDING_TIMEOUT 10 
#define NAT_MAX_IPSEC_PENDING_TIMEOUT 60
#define NAT_MIN_IPSEC_RETRY 1
#define NAT_MAX_IPSEC_RETRY 10
/*UT_FIX-END*/ 
#define NAT_ENCH_HASH_MASK     0x000003ff
#define NAT_ENCH_FIVE_BIT_MASK 0x0000001f
#define NAT_ZERO                             0
#define NAT_FOUR   4
#define  NAT_UNIT_POSITION        10
#define  NAT_PORT_TRIG_PROCEED    3
#define NAT_INVALID_ARRAY_INDEX   -1
#define NAT_MIN_APP_NAME_LEN       1

#define CFA_MAX_CIDR              32

/* UPNP NAPT TIMER */
#define NAPT_TIMER_ID        123
#define NAT_UPNP_MAX_LEASE_DURATION 0xffffffff  
/* END */
#define NAT_PROTOCOL_LOCK "NATP"
#define   NAT_INVLD_IP_ADDR_ANY          0x00000000

#define   NAT_INIT_COMPLETE(u4Status)          lrInitComplete(u4Status)

#define NAT_NAPT_ENTRY_DESCRN_LEN 20

#define  NAT_MAX_TIMERS  1  /* it is 1 accounting only the NAT fragmentation reasembly timer*/
                            /* When the other timers are changed to follow the new method, the
                                MAX timers would take more value */
#define NAT_TMR_FUNC_OFFSET(x,y)   FSAP_OFFSETOF(x,y)

#define   NAT_RB_GREATER 1
#define   NAT_RB_EQUAL   0
#define   NAT_RB_LESSER   -1
#define   NAT_ZERO_OFFSET 0
#define   NAT_ZERO_LEN    0
#define   NAT_NEGATIVE_OFFSET -1
#define   NAT_ONE 1
#define   NAT_MAX_TRANS_PROT 2


#define IPC_LINK_STATUS_EVENT              0
#define IPC_PORT_MAPPING_EXPIRY_EVENT      1
#define IPC_PINHOLE_EXPIRY_EVENT           2
#define IPC_NAT_IP_POOL_CHANGE_EVENT       3
#define IPC_LINK_STATUS_ADDED              4
#define IPC_LINK_STATUS_DELETED            5
#define IPC_NAT_DYN_ENTRY_ADD_EVENT        6
#define IPC_NAT_DYN_ENTRY_EXP_EVENT        7

#define NATIPC_VALUE_ZERO                   0

/* -- NOTIFICATION TYPES -- */
/* Error codes */
#define NATIPC_NO_ERROR                    0
#define NATIPC_NOT_INITIALIZED             1
#define NATIPC_COMMUNICATON_ERROR          2
#define NATIPC_INVALID_PARAMETERS          3
#define NATIPC_NAT_INTERNAL_ERROR          4
#define NATIPC_INTERNAL_ERROR              5
#define NATIPC_BINDING_ALREADY_EXISTS      6
#define NATIPC_BINDING_NOT_EXISTS          7
#define NATIPC_PINHOLE_ALREADY_EXISTS      8
#define NATIPC_PINHOLE_NOT_EXISTS          9
#define NATIPC_REQUEST_TIMEOUT             10
#define NATIPC_LINK_DOWN                   11
#define NATIPC_LINK_NOT_CONFIGURED         12
#define NATIPC_EXACT_MATCH                 13
#define NATIPC_ROUTE_NOT_FOUND             14

#define NATIPC_COOKIE_NO_IPC                0
#define NATIPC_PORT_NUM_ZERO                0


#define NAT_STATIC_POLICY                 1 /* This macro identifies the static
                                               policy node. This value should be
                                               identical to the value of static
                                               option defined for natPolicyType 
                                               mib object*/

#define NAT_DYNAMIC_POLICY                2 /* This macro identifies the dynamic
                                               policy Node. This value should be
                                               identical to the value of dynamic
                                               option defined for natPolicyType
                                               mib object*/

#define NAT_MIN_POLICY_ID                 1   /* Minimum supported value of 
                                               * the Policy NAT ID. This value 
                                               * corresponds to the minimum 
                                               * value defined for natPolicyId
                                               * mib object. 
                                               */
#define NAT_MAX_POLICY_ID                 65535 /* Maximum Value of the Policy
                                                 * NAT ID. This value 
                                                 * corresponds to the max value
                                                 * defined for natPolicyId
                                                 * mib object.
                                                 */

#define NAT_MAX_ACL_NAME_LEN              36 /* Length of the Policy NAT Filter
                                              * String. This value corresponds
                                              * to the max lenght defined for
                                              *   
                                              */


#define NAT_TASK_NAME_LEN                10
#define  NAT_ICMP_TYPE_ECHO              8
#define NAT_ICMP_TYPE_TIMESTAMP          13
#define NAT_ICMP_TYPE_TIMESTAMP_REPLY    14
#define NAT_ICMP_TYPE_INFO_REQUEST       15
#define NAT_ICMP_TYPE_INFO_REPLY         16
#define NAT_ICMP_TYPE_ADDR_MASK_REQ      17
#define NAT_ICMP_TYPE_ADDR_MASK_REPLY    18

#define NAT_INVALID                             -1
#define MODULO_DIVISOR_2                          2
#define MODULO_DIVISOR_10                         10
#define  NAT_NOT_FOUND                            -1
#define  NAT_ONE_BYTE                             1
#define  NAT_TWO_BYTES                            2
#define  NAT_THREE_BYTES                          3
#define  NAT_FOUR_BYTES                           4
#define  NAT_FIVE_BYTES                           5
#define  NAT_SIX_BYTES                            6
#define  NAT_SEVEN_BYTES                          7
#define  NAT_EIGHT_BYTES                          8
#define  NAT_NINE_BYTES                           9
#define  NAT_TEN_BYTES                            10
#define  KERBEROS_TCP_PORT                        88
#define  NAT_RSVP                                 46
#define NAT_ONE                                   1
#define NAT_TWO                                   2
#define NAT_THREE                                 3
#define NAT_FOUR                                  4
#define NAT_FIVE                                  5
#define NAT_SEVEN                                 7
#define NAT_EIGHT                                 8
#define NAT_NINE                                  9
#define NAT_TEN                                   10
#define NAT_ELEVEN                                11
#define NAT_SIXTEEN                               16
#define NAT_SEVENTEEN                             17

#define NAT_MASK_WITH_VAL_127                    0x007f
#define NAT_MASK_WITH_VAL_7                      0x00000007
#define NAT_MASK_WITH_VAL_1023                   0x000003ff
#define NAT_MASK_WITH_VAL_031                    0x001f
#define NAT_MASK_WITH_VAL_31                     0x0000001f
#define NAT_MASK_WITH_VAL_3                      0x00000003
#define NAT_MASK_WITH_VAL_255                    0x00ff
#define NAT_LOOP_BACK_ADDR_MASK                  0xff000000


#define NAT_SET_16_BITS                   0xffff
#define NAT_16_BITS_SET                    0xffff
#define NAT_SET_16_BITS_LONG               0xffffL
#define NAT_CLASS_B_HOST_MASK              0x0000ffff
#define NAT_CLASS_A_HOST_MASK              0x00ffffff
#define NAT_SET_8_BITS_LONG                0xffL

#define NAT_PAYLOAD_2E                     0x2e
#define NAT_PAYLOAD_2C                     0x2c
#define NAT_PAYLOAD_0D                     0x0d
#define NAT_PAYLOAD_0A                     0x0a
#define NAT_PAYLOAD_NULL                   0x00
#define NAT_RTSP_0D                        0x0D
#define NAT_RTSP_0A                        0x0A
#define ASCII_HEX_39                       0x39
#define ASCII_HEX_30                       0x30
#define  NAT_10_MILLI_SEC                     10
#define  NAT_20_MILLI_SEC                     20
#define  NAT_30_MILLI_SEC                     30
#define  NAT_40_MILLI_SEC                     40
#define  NAT_50_MILLI_SEC                     50
#define  NAT_60_MILLI_SEC                     60
#define  NAT_70_MILLI_SEC                     70
#define  NAT_80_MILLI_SEC                     80
#define  NAT_90_MILLI_SEC                     90
#define  NAT_100_MILLI_SEC                    100
#define  NAT_110_MILLI_SEC                    110
#define  NAT_120_MILLI_SEC                    120
#define  NAT_130_MILLI_SEC                    130
#define  NAT_140_MILLI_SEC                    140
#define  NAT_16_BYTES                         16
#define  SIP_MAX_NO_SIG_PORT                  3
#define  NAT_CONVERT_MILLI_S                  1000
#define  NAT_ADD_1_SEC                        1000000
#define NAT_EXTRACT_LSB2_BITS                 0x03000000
#define NAT_3RD_BYTE_MASK                     0x00ff0000
#define NAT_EXTRACT_MSB8_BITS                 0x000000ff
#define NAT_MASK_WITH_VAL_768                 0x00000300
#define NAT_INVLD_IP_ADDR_1                   0x00000000
#define NAT_DNS_RCODE_OFFSET                  0xfff0
#define NAT_EXTRACT_LSB1_BIT_LONG             0x10000L
#define NAT_EXTRACT_LAST_ODD_BYTE             0x1
#define NAT_OFFSET_FRAG                       0x1
#define NAT_ICMP_CHECKSUM_OFFSET              22
#define NAT_VAL_256                           256
#define NAT_PROTO_ANY                         255
#define NAT_CLI_MAX_INTF_NAME_LEN             32
#define NAT_CLI_INTF_NAME_LEN                 20

#define CUSEEME_DATATYPE                   101
#define NAT_IP_DELIMITER                   0x0d0a
#define NAT_OFFSET_FRAG                    0x1
#define ICMP_CHK_OFF_22                    22

#define  NAT_INIT_SHUT_TRC                1
#define  NAT_MGMT_TRC                     2
#define  NAT_DATA_PATH_TRC                3
#define  NAT_CONTROL_PLANE_TRC            4
#define  NAT_DUMP_TRC                     5
#define  NAT_OS_RESOURCE_TRC              6
#define  NAT_ALL_FAILURE_TRC              7
#define  NAT_BUFFER_TRC                   8

#define NAT_INDEX_0                        0
#define NAT_INDEX_1                        1
#define NAT_INDEX_2                        2
#define NAT_INDEX_3                        3
#define NAT_INDEX_4                        4
#define NAT_INDEX_5                        5
#define NAT_INDEX_6                        6
#define NAT_INDEX_7                        7
#define NAT_INDEX_8                        8
#define NAT_INDEX_9                        9
#define NAT_INDEX_10                       10
#define NAT_INDEX_11                       11
#define NAT_INDEX_12                       12
#define NAT_INDEX_13                       13
#define NAT_INDEX_14                       14
#define NAT_INDEX_15                       15
#define NAT_INDEX_16                       16
#define NAT_INDEX_17                       17
#define NAT_MAX_LEN                        100

#endif /* _NATDEFN_H_ */
