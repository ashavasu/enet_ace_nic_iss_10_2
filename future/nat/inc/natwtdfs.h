/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: natwtdfs.h,v 1.6 2011/06/27 07:05:36 siva Exp $
 *
 * Description:This file contains the include files needed for
 *             nat kernel wrapper functions
 *
 *******************************************************************/

typedef struct {
    int     cmd;
    INT4 *  pi4RetValNatEnable;
    INT4     cliWebSetError;
    INT4    rval;
     
} tNatwnmhGetNatEnable;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValNatTypicalNumberOfEntries;
    INT4     cliWebSetError;
    INT4    rval;
     
} tNatwnmhGetNatTypicalNumberOfEntries;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValNatTranslatedLocalPortStart;
    INT4     cliWebSetError;
    INT4    rval;
     
} tNatwnmhGetNatTranslatedLocalPortStart;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValNatIdleTimeOut;
    INT4     cliWebSetError;
    INT4    rval;
     
} tNatwnmhGetNatIdleTimeOut;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValNatTcpTimeOut;
    INT4     cliWebSetError;
    INT4    rval;
     
} tNatwnmhGetNatTcpTimeOut;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValNatUdpTimeOut;
    INT4     cliWebSetError;
    INT4    rval;
     
} tNatwnmhGetNatUdpTimeOut;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValNatTrcFlag;
    INT4     cliWebSetError;
    INT4    rval;
     
} tNatwnmhGetNatTrcFlag;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValNatStatDynamicAllocFailureCount;
    INT4      cliWebSetError;
    INT4     rval;
     
} tNatwnmhGetNatStatDynamicAllocFailureCount;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValNatStatTotalNumberOfTranslations;
    INT4      cliWebSetError;
    INT4     rval;
     
} tNatwnmhGetNatStatTotalNumberOfTranslations;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValNatStatTotalNumberOfActiveSessions;
    INT4      cliWebSetError;
    INT4     rval;
     
} tNatwnmhGetNatStatTotalNumberOfActiveSessions;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValNatStatTotalNumberOfPktsDropped;
    INT4      cliWebSetError;
    INT4     rval;
     
} tNatwnmhGetNatStatTotalNumberOfPktsDropped;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValNatStatTotalNumberOfSessionsClosed;
    INT4      cliWebSetError;
    INT4     rval;
     
} tNatwnmhGetNatStatTotalNumberOfSessionsClosed;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValNatIKEPortTranslation;
    INT4     cliWebSetError;
    INT4    rval;
     
} tNatwnmhGetNatIKEPortTranslation;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValNatIKETimeout;
    INT4     cliWebSetError;
    INT4    rval;
     
} tNatwnmhGetNatIKETimeout;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValNatIPSecTimeout;
    INT4     cliWebSetError;
    INT4    rval;
     
} tNatwnmhGetNatIPSecTimeout;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValNatIPSecPendingTimeout;
    INT4     cliWebSetError;
    INT4    rval;
     
} tNatwnmhGetNatIPSecPendingTimeout;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValNatIPSecMaxRetry;
    INT4     cliWebSetError;
    INT4    rval;
     
} tNatwnmhGetNatIPSecMaxRetry;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValSipAlgPort;
    INT4    rval;
     
} tNatwnmhGetSipAlgPort;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValNatSipAlgPartialEntryTimeOut;
    INT4    rval;
     
} tNatwnmhGetNatSipAlgPartialEntryTimeOut;

typedef struct {
    int   cmd;
    INT4  i4SetValNatEnable;
    INT4   cliWebSetError;
    INT4  rval;
     
} tNatwnmhSetNatEnable;

typedef struct {
    int   cmd;
    INT4  i4SetValNatTypicalNumberOfEntries;
    INT4   cliWebSetError;
    INT4  rval;
     
} tNatwnmhSetNatTypicalNumberOfEntries;

typedef struct {
    int   cmd;
    INT4  i4SetValNatTranslatedLocalPortStart;
    INT4   cliWebSetError;
    INT4  rval;
     
} tNatwnmhSetNatTranslatedLocalPortStart;

typedef struct {
    int   cmd;
    INT4  i4SetValNatIdleTimeOut;
    INT4   cliWebSetError;
    INT4  rval;
     
} tNatwnmhSetNatIdleTimeOut;

typedef struct {
    int   cmd;
    INT4  i4SetValNatTcpTimeOut;
    INT4   cliWebSetError;
    INT4  rval;
} tNatwnmhSetNatTcpTimeOut;

typedef struct {
    int   cmd;
    INT4  i4SetValNatUdpTimeOut;
    INT4   cliWebSetError;
    INT4  rval; 
} tNatwnmhSetNatUdpTimeOut;

typedef struct {
    int   cmd;
    INT4  i4SetValNatTrcFlag;
    INT4   cliWebSetError;
    INT4  rval; 
} tNatwnmhSetNatTrcFlag;

typedef struct {
    int   cmd;
    INT4  i4SetValNatIKEPortTranslation;
    INT4   cliWebSetError;
    INT4  rval; 
} tNatwnmhSetNatIKEPortTranslation;

typedef struct {
    int   cmd;
    INT4  i4SetValNatIKETimeout;
    INT4   cliWebSetError;
    INT4  rval; 
} tNatwnmhSetNatIKETimeout;

typedef struct {
    int   cmd;
    INT4  i4SetValNatIPSecTimeout;
    INT4   cliWebSetError;
    INT4  rval; 
} tNatwnmhSetNatIPSecTimeout;

typedef struct {
    int   cmd;
    INT4  i4SetValNatIPSecPendingTimeout;
    INT4   cliWebSetError;
    INT4  rval; 
} tNatwnmhSetNatIPSecPendingTimeout;

typedef struct {
    int   cmd;
    INT4  i4SetValNatIPSecMaxRetry;
    INT4   cliWebSetError;
    INT4  rval; 
} tNatwnmhSetNatIPSecMaxRetry;

typedef struct {
    int   cmd;
    INT4  i4SetValSipAlgPort;
    INT4  rval; 
} tNatwnmhSetSipAlgPort;

typedef struct {
    int   cmd;
    INT4  i4SetValNatSipAlgPartialEntryTimeOut;
    INT4  rval; 
} tNatwnmhSetNatSipAlgPartialEntryTimeOut;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValNatEnable;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhTestv2NatEnable;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValNatTypicalNumberOfEntries;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhTestv2NatTypicalNumberOfEntries;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValNatTranslatedLocalPortStart;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhTestv2NatTranslatedLocalPortStart;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValNatIdleTimeOut;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhTestv2NatIdleTimeOut;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValNatTcpTimeOut;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhTestv2NatTcpTimeOut;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValNatUdpTimeOut;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhTestv2NatUdpTimeOut;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValNatTrcFlag;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhTestv2NatTrcFlag;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValNatIKEPortTranslation;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhTestv2NatIKEPortTranslation;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValNatIKETimeout;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhTestv2NatIKETimeout;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValNatIPSecTimeout;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhTestv2NatIPSecTimeout;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValNatIPSecPendingTimeout;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhTestv2NatIPSecPendingTimeout;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValNatIPSecMaxRetry;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhTestv2NatIPSecMaxRetry;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValSipAlgPort;
   INT4  rval; 
} tNatwnmhTestv2SipAlgPort;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValNatSipAlgPartialEntryTimeOut;
   INT4  rval; 
} tNatwnmhTestv2NatSipAlgPartialEntryTimeOut;

typedef struct {
    int    cmd;
    INT4   i4NatDynamicTransInterfaceNum;
    UINT4  u4NatDynamicTransLocalIp;
    INT4   i4NatDynamicTransLocalPort;
    UINT4  u4NatDynamicTransOutsideIp;
    INT4   i4NatDynamicTransOutsidePort;
    INT4    cliWebSetError;
    INT4   rval;
 
} tNatwnmhValidateIndexInstanceNatDynamicTransTable;

typedef struct {
    int      cmd;
    INT4 *   pi4NatDynamicTransInterfaceNum;
    UINT4 *  pu4NatDynamicTransLocalIp;
    INT4 *   pi4NatDynamicTransLocalPort;
    UINT4 *  pu4NatDynamicTransOutsideIp;
    INT4 *   pi4NatDynamicTransOutsidePort;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhGetFirstIndexNatDynamicTransTable;

typedef struct {
    int      cmd;
    INT4     i4NatDynamicTransInterfaceNum;
    INT4 *   pi4NextNatDynamicTransInterfaceNum;
    UINT4    u4NatDynamicTransLocalIp;
    UINT4 *  pu4NextNatDynamicTransLocalIp;
    INT4     i4NatDynamicTransLocalPort;
    INT4 *   pi4NextNatDynamicTransLocalPort;
    UINT4    u4NatDynamicTransOutsideIp;
    UINT4 *  pu4NextNatDynamicTransOutsideIp;
    INT4     i4NatDynamicTransOutsidePort;
    INT4 *   pi4NextNatDynamicTransOutsidePort;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhGetNextIndexNatDynamicTransTable;

typedef struct {
    int      cmd;
    INT4     i4NatDynamicTransInterfaceNum;
    UINT4    u4NatDynamicTransLocalIp;
    INT4     i4NatDynamicTransLocalPort;
    UINT4    u4NatDynamicTransOutsideIp;
    INT4     i4NatDynamicTransOutsidePort;
    UINT4 *  pu4RetValNatDynamicTransTranslatedLocalIp;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhGetNatDynamicTransTranslatedLocalIp;

typedef struct {
    int     cmd;
    INT4    i4NatDynamicTransInterfaceNum;
    UINT4   u4NatDynamicTransLocalIp;
    INT4    i4NatDynamicTransLocalPort;
    UINT4   u4NatDynamicTransOutsideIp;
    INT4    i4NatDynamicTransOutsidePort;
    INT4 *  pi4RetValNatDynamicTransTranslatedLocalPort;
    INT4     cliWebSetError;
    INT4    rval;
 
} tNatwnmhGetNatDynamicTransTranslatedLocalPort;

typedef struct {
    int     cmd;
    INT4    i4NatDynamicTransInterfaceNum;
    UINT4   u4NatDynamicTransLocalIp;
    INT4    i4NatDynamicTransLocalPort;
    UINT4   u4NatDynamicTransOutsideIp;
    INT4    i4NatDynamicTransOutsidePort;
    INT4 *  pi4RetValNatDynamicTransLastUseTime;
    INT4     cliWebSetError;
    INT4    rval;
 
} tNatwnmhGetNatDynamicTransLastUseTime;

typedef struct {
    int    cmd;
    INT4   i4NatGlobalAddressInterfaceNum;
    UINT4  u4NatGlobalAddressTranslatedLocalIp;
    INT4    cliWebSetError;
    INT4   rval;
 
} tNatwnmhValidateIndexInstanceNatGlobalAddressTable;

typedef struct {
    int      cmd;
    INT4 *   pi4NatGlobalAddressInterfaceNum;
    UINT4 *  pu4NatGlobalAddressTranslatedLocalIp;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhGetFirstIndexNatGlobalAddressTable;

typedef struct {
    int      cmd;
    INT4     i4NatGlobalAddressInterfaceNum;
    INT4 *   pi4NextNatGlobalAddressInterfaceNum;
    UINT4    u4NatGlobalAddressTranslatedLocalIp;
    UINT4 *  pu4NextNatGlobalAddressTranslatedLocalIp;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhGetNextIndexNatGlobalAddressTable;

typedef struct {
    int      cmd;
    INT4     i4NatGlobalAddressInterfaceNum;
    UINT4    u4NatGlobalAddressTranslatedLocalIp;
    UINT4 *  pu4RetValNatGlobalAddressMask;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhGetNatGlobalAddressMask;

typedef struct {
    int     cmd;
    INT4    i4NatGlobalAddressInterfaceNum;
    UINT4   u4NatGlobalAddressTranslatedLocalIp;
    INT4 *  pi4RetValNatGlobalAddressEntryStatus;
    INT4     cliWebSetError;
    INT4  rval; 
} tNatwnmhGetNatGlobalAddressEntryStatus;

typedef struct {
    int    cmd;
    INT4   i4NatGlobalAddressInterfaceNum;
    UINT4  u4NatGlobalAddressTranslatedLocalIp;
    UINT4  u4SetValNatGlobalAddressMask;
    INT4    cliWebSetError;
    INT4  rval; 
} tNatwnmhSetNatGlobalAddressMask;

typedef struct {
    int    cmd;
    INT4   i4NatGlobalAddressInterfaceNum;
    UINT4  u4NatGlobalAddressTranslatedLocalIp;
    INT4   i4SetValNatGlobalAddressEntryStatus;
    INT4    cliWebSetError;
    INT4  rval; 
} tNatwnmhSetNatGlobalAddressEntryStatus;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4NatGlobalAddressInterfaceNum;
    UINT4    u4NatGlobalAddressTranslatedLocalIp;
    UINT4    u4TestValNatGlobalAddressMask;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhTestv2NatGlobalAddressMask;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4NatGlobalAddressInterfaceNum;
    UINT4    u4NatGlobalAddressTranslatedLocalIp;
    INT4     i4TestValNatGlobalAddressEntryStatus;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhTestv2NatGlobalAddressEntryStatus;

typedef struct {
    int    cmd;
    INT4   i4NatLocalAddressInterfaceNumber;
    UINT4  u4NatLocalAddressLocalIp;
    INT4    cliWebSetError;
    INT4  rval; 
} tNatwnmhValidateIndexInstanceNatLocalAddressTable;

typedef struct {
    int      cmd;
    INT4 *   pi4NatLocalAddressInterfaceNumber;
    UINT4 *  pu4NatLocalAddressLocalIp;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhGetFirstIndexNatLocalAddressTable;

typedef struct {
    int      cmd;
    INT4     i4NatLocalAddressInterfaceNumber;
    INT4 *   pi4NextNatLocalAddressInterfaceNumber;
    UINT4    u4NatLocalAddressLocalIp;
    UINT4 *  pu4NextNatLocalAddressLocalIp;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhGetNextIndexNatLocalAddressTable;

typedef struct {
    int      cmd;
    INT4     i4NatLocalAddressInterfaceNumber;
    UINT4    u4NatLocalAddressLocalIp;
    UINT4 *  pu4RetValNatLocalAddressMask;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhGetNatLocalAddressMask;

typedef struct {
    int     cmd;
    INT4    i4NatLocalAddressInterfaceNumber;
    UINT4   u4NatLocalAddressLocalIp;
    INT4 *  pi4RetValNatLocalAddressEntryStatus;
    INT4     cliWebSetError;
    INT4  rval; 
} tNatwnmhGetNatLocalAddressEntryStatus;

typedef struct {
    int    cmd;
    INT4   i4NatLocalAddressInterfaceNumber;
    UINT4  u4NatLocalAddressLocalIp;
    UINT4  u4SetValNatLocalAddressMask;
    INT4    cliWebSetError;
    INT4  rval; 
} tNatwnmhSetNatLocalAddressMask;

typedef struct {
    int    cmd;
    INT4   i4NatLocalAddressInterfaceNumber;
    UINT4  u4NatLocalAddressLocalIp;
    INT4   i4SetValNatLocalAddressEntryStatus;
    INT4    cliWebSetError;
    INT4  rval; 
} tNatwnmhSetNatLocalAddressEntryStatus;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4NatLocalAddressInterfaceNumber;
    UINT4    u4NatLocalAddressLocalIp;
    UINT4    u4TestValNatLocalAddressMask;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhTestv2NatLocalAddressMask;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4NatLocalAddressInterfaceNumber;
    UINT4    u4NatLocalAddressLocalIp;
    INT4     i4TestValNatLocalAddressEntryStatus;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhTestv2NatLocalAddressEntryStatus;

typedef struct {
    int    cmd;
    INT4   i4NatStaticInterfaceNum;
    UINT4  u4NatStaticLocalIp;
    INT4    cliWebSetError;
    INT4  rval; 
} tNatwnmhValidateIndexInstanceNatStaticTable;

typedef struct {
    int      cmd;
    INT4 *   pi4NatStaticInterfaceNum;
    UINT4 *  pu4NatStaticLocalIp;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhGetFirstIndexNatStaticTable;

typedef struct {
    int      cmd;
    INT4     i4NatStaticInterfaceNum;
    INT4 *   pi4NextNatStaticInterfaceNum;
    UINT4    u4NatStaticLocalIp;
    UINT4 *  pu4NextNatStaticLocalIp;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhGetNextIndexNatStaticTable;

typedef struct {
    int      cmd;
    INT4     i4NatStaticInterfaceNum;
    UINT4    u4NatStaticLocalIp;
    UINT4 *  pu4RetValNatStaticTranslatedLocalIp;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhGetNatStaticTranslatedLocalIp;

typedef struct {
    int     cmd;
    INT4    i4NatStaticInterfaceNum;
    UINT4   u4NatStaticLocalIp;
    INT4 *  pi4RetValNatStaticEntryStatus;
    INT4     cliWebSetError;
    INT4  rval; 
} tNatwnmhGetNatStaticEntryStatus;

typedef struct {
    int    cmd;
    INT4   i4NatStaticInterfaceNum;
    UINT4  u4NatStaticLocalIp;
    UINT4  u4SetValNatStaticTranslatedLocalIp;
    INT4    cliWebSetError;
    INT4  rval; 
} tNatwnmhSetNatStaticTranslatedLocalIp;

typedef struct {
    int    cmd;
    INT4   i4NatStaticInterfaceNum;
    UINT4  u4NatStaticLocalIp;
    INT4   i4SetValNatStaticEntryStatus;
    INT4    cliWebSetError;
    INT4  rval; 
} tNatwnmhSetNatStaticEntryStatus;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4NatStaticInterfaceNum;
    UINT4    u4NatStaticLocalIp;
    UINT4    u4TestValNatStaticTranslatedLocalIp;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhTestv2NatStaticTranslatedLocalIp;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4NatStaticInterfaceNum;
    UINT4    u4NatStaticLocalIp;
    INT4     i4TestValNatStaticEntryStatus;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhTestv2NatStaticEntryStatus;

typedef struct {
    int    cmd;
    INT4   i4NatStaticNaptInterfaceNum;
    UINT4  u4NatStaticNaptLocalIp;
    INT4   i4NatStaticNaptStartLocalPort;
    INT4   i4NatStaticNaptEndLocalPort;
    INT4   i4NatStaticNaptProtocolNumber;
    INT4    cliWebSetError;
    INT4  rval; 
} tNatwnmhValidateIndexInstanceNatStaticNaptTable;

typedef struct {
    int      cmd;
    INT4 *   pi4NatStaticNaptInterfaceNum;
    UINT4 *  pu4NatStaticNaptLocalIp;
    INT4 *   pi4NatStaticNaptStartLocalPort;
    INT4 *   pi4NatStaticNaptEndLocalPort;
    INT4 *   pi4NatStaticNaptProtocolNumber;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhGetFirstIndexNatStaticNaptTable;

typedef struct {
    int      cmd;
    INT4     i4NatStaticNaptInterfaceNum;
    INT4 *   pi4NextNatStaticNaptInterfaceNum;
    UINT4    u4NatStaticNaptLocalIp;
    UINT4 *  pu4NextNatStaticNaptLocalIp;
    INT4     i4NatStaticNaptStartLocalPort;
    INT4 *   pi4NextNatStaticNaptStartLocalPort;
    INT4     i4NatStaticNaptEndLocalPort;
    INT4 *   pi4NextNatStaticNaptEndLocalPort;
    INT4     i4NatStaticNaptProtocolNumber;
    INT4 *   pi4NextNatStaticNaptProtocolNumber;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhGetNextIndexNatStaticNaptTable;

typedef struct {
    int      cmd;
    INT4     i4NatStaticNaptInterfaceNum;
    UINT4    u4NatStaticNaptLocalIp;
    INT4     i4NatStaticNaptStartLocalPort;
    INT4     i4NatStaticNaptEndLocalPort;
    INT4     i4NatStaticNaptProtocolNumber;
    UINT4 *  pu4RetValNatStaticNaptTranslatedLocalIp;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhGetNatStaticNaptTranslatedLocalIp;

typedef struct {
    int     cmd;
    INT4    i4NatStaticNaptInterfaceNum;
    UINT4   u4NatStaticNaptLocalIp;
    INT4    i4NatStaticNaptStartLocalPort;
    INT4    i4NatStaticNaptEndLocalPort;
    INT4    i4NatStaticNaptProtocolNumber;
    INT4 *  pi4RetValNatStaticNaptTranslatedLocalPort;
    INT4     cliWebSetError;
    INT4  rval; 
} tNatwnmhGetNatStaticNaptTranslatedLocalPort;

typedef struct {
    int                        cmd;
    INT4                       i4NatStaticNaptInterfaceNum;
    UINT4                      u4NatStaticNaptLocalIp;
    INT4                       i4NatStaticNaptStartLocalPort;
    INT4                       i4NatStaticNaptEndLocalPort;
    INT4                       i4NatStaticNaptProtocolNumber;
    tSNMP_OCTET_STRING_TYPE *  pRetValNatStaticNaptDescription;
    INT4                        cliWebSetError;
    INT4                       rval; 
} tNatwnmhGetNatStaticNaptDescription;

typedef struct {
    int     cmd;
    INT4    i4NatStaticNaptInterfaceNum;
    UINT4   u4NatStaticNaptLocalIp;
    INT4    i4NatStaticNaptStartLocalPort;
    INT4    i4NatStaticNaptEndLocalPort;
    INT4    i4NatStaticNaptProtocolNumber;
    INT4 *  pi4RetValNatStaticNaptLeaseDuration;
    INT4     cliWebSetError;
    INT4  rval; 
} tNatwnmhGetNatStaticNaptLeaseDuration;

typedef struct {
    int     cmd;
    INT4    i4NatStaticNaptInterfaceNum;
    UINT4   u4NatStaticNaptLocalIp;
    INT4    i4NatStaticNaptStartLocalPort;
    INT4    i4NatStaticNaptEndLocalPort;
    INT4    i4NatStaticNaptProtocolNumber;
    INT4 *  pi4RetValNatStaticNaptEntryStatus;
    INT4     cliWebSetError;
    INT4  rval; 
} tNatwnmhGetNatStaticNaptEntryStatus;

typedef struct {
    int    cmd;
    INT4   i4NatStaticNaptInterfaceNum;
    UINT4  u4NatStaticNaptLocalIp;
    INT4   i4NatStaticNaptStartLocalPort;
    INT4   i4NatStaticNaptEndLocalPort;
    INT4   i4NatStaticNaptProtocolNumber;
    UINT4  u4SetValNatStaticNaptTranslatedLocalIp;
    INT4    cliWebSetError;
    INT4  rval; 
} tNatwnmhSetNatStaticNaptTranslatedLocalIp;

typedef struct {
    int    cmd;
    INT4   i4NatStaticNaptInterfaceNum;
    UINT4  u4NatStaticNaptLocalIp;
    INT4   i4NatStaticNaptStartLocalPort;
    INT4   i4NatStaticNaptEndLocalPort;
    INT4   i4NatStaticNaptProtocolNumber;
    INT4   i4SetValNatStaticNaptTranslatedLocalPort;
    INT4    cliWebSetError;
    INT4  rval; 
} tNatwnmhSetNatStaticNaptTranslatedLocalPort;

typedef struct {
    int                        cmd;
    INT4                       i4NatStaticNaptInterfaceNum;
    UINT4                      u4NatStaticNaptLocalIp;
    INT4                       i4NatStaticNaptStartLocalPort;
    INT4                       i4NatStaticNaptEndLocalPort;
    INT4                       i4NatStaticNaptProtocolNumber;
    tSNMP_OCTET_STRING_TYPE *  pSetValNatStaticNaptDescription;
    INT4                        cliWebSetError;
    INT4                       rval; 
} tNatwnmhSetNatStaticNaptDescription;

typedef struct {
    int    cmd;
    INT4   i4NatStaticNaptInterfaceNum;
    UINT4  u4NatStaticNaptLocalIp;
    INT4   i4NatStaticNaptStartLocalPort;
    INT4   i4NatStaticNaptEndLocalPort;
    INT4   i4NatStaticNaptProtocolNumber;
    INT4   i4SetValNatStaticNaptLeaseDuration;
    INT4    cliWebSetError;
    INT4  rval; 
} tNatwnmhSetNatStaticNaptLeaseDuration;

typedef struct {
    int    cmd;
    INT4   i4NatStaticNaptInterfaceNum;
    UINT4  u4NatStaticNaptLocalIp;
    INT4   i4NatStaticNaptStartLocalPort;
    INT4   i4NatStaticNaptEndLocalPort;
    INT4   i4NatStaticNaptProtocolNumber;
    INT4   i4SetValNatStaticNaptEntryStatus;
    INT4    cliWebSetError;
    INT4  rval; 
} tNatwnmhSetNatStaticNaptEntryStatus;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4NatStaticNaptInterfaceNum;
    UINT4    u4NatStaticNaptLocalIp;
    INT4     i4NatStaticNaptStartLocalPort;
    INT4     i4NatStaticNaptEndLocalPort;
    INT4     i4NatStaticNaptProtocolNumber;
    UINT4    u4TestValNatStaticNaptTranslatedLocalIp;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhTestv2NatStaticNaptTranslatedLocalIp;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4NatStaticNaptInterfaceNum;
    UINT4    u4NatStaticNaptLocalIp;
    INT4     i4NatStaticNaptStartLocalPort;
    INT4     i4NatStaticNaptEndLocalPort;
    INT4     i4NatStaticNaptProtocolNumber;
    INT4     i4TestValNatStaticNaptTranslatedLocalPort;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhTestv2NatStaticNaptTranslatedLocalPort;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    INT4                       i4NatStaticNaptInterfaceNum;
    UINT4                      u4NatStaticNaptLocalIp;
    INT4                       i4NatStaticNaptStartLocalPort;
    INT4                       i4NatStaticNaptEndLocalPort;
    INT4                       i4NatStaticNaptProtocolNumber;
    tSNMP_OCTET_STRING_TYPE *  pTestValNatStaticNaptDescription;
    INT4                        cliWebSetError;
    INT4                       rval; 
} tNatwnmhTestv2NatStaticNaptDescription;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4NatStaticNaptInterfaceNum;
    UINT4    u4NatStaticNaptLocalIp;
    INT4     i4NatStaticNaptStartLocalPort;
    INT4     i4NatStaticNaptEndLocalPort;
    INT4     i4NatStaticNaptProtocolNumber;
    INT4     i4TestValNatStaticNaptLeaseDuration;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhTestv2NatStaticNaptLeaseDuration;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4NatStaticNaptInterfaceNum;
    UINT4    u4NatStaticNaptLocalIp;
    INT4     i4NatStaticNaptStartLocalPort;
    INT4     i4NatStaticNaptEndLocalPort;
    INT4     i4NatStaticNaptProtocolNumber;
    INT4     i4TestValNatStaticNaptEntryStatus;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhTestv2NatStaticNaptEntryStatus;

typedef struct {
    int   cmd;
    INT4  i4NatIfInterfaceNumber;
    INT4   cliWebSetError;
    INT4  rval; 
} tNatwnmhValidateIndexInstanceNatIfTable;

typedef struct {
    int     cmd;
    INT4 *  pi4NatIfInterfaceNumber;
    INT4     cliWebSetError;
    INT4  rval; 
} tNatwnmhGetFirstIndexNatIfTable;

typedef struct {
    int     cmd;
    INT4    i4NatIfInterfaceNumber;
    INT4 *  pi4NextNatIfInterfaceNumber;
    INT4     cliWebSetError;
    INT4  rval; 
} tNatwnmhGetNextIndexNatIfTable;

typedef struct {
    int     cmd;
    INT4    i4NatIfInterfaceNumber;
    INT4 *  pi4RetValNatIfNat;
    INT4     cliWebSetError;
    INT4  rval; 
} tNatwnmhGetNatIfNat;

typedef struct {
    int     cmd;
    INT4    i4NatIfInterfaceNumber;
    INT4 *  pi4RetValNatIfNapt;
    INT4     cliWebSetError;
    INT4  rval; 
} tNatwnmhGetNatIfNapt;

typedef struct {
    int     cmd;
    INT4    i4NatIfInterfaceNumber;
    INT4 *  pi4RetValNatIfTwoWayNat;
    INT4     cliWebSetError;
    INT4  rval; 
} tNatwnmhGetNatIfTwoWayNat;

typedef struct {
    int     cmd;
    INT4    i4NatIfInterfaceNumber;
    INT4 *  pi4RetValNatIfEntryStatus;
    INT4     cliWebSetError;
    INT4  rval; 
} tNatwnmhGetNatIfEntryStatus;

typedef struct {
    int   cmd;
    INT4  i4NatIfInterfaceNumber;
    INT4  i4SetValNatIfNat;
    INT4   cliWebSetError;
    INT4  rval; 
} tNatwnmhSetNatIfNat;

typedef struct {
    int   cmd;
    INT4  i4NatIfInterfaceNumber;
    INT4  i4SetValNatIfNapt;
    INT4   cliWebSetError;
    INT4  rval; 
} tNatwnmhSetNatIfNapt;

typedef struct {
    int   cmd;
    INT4  i4NatIfInterfaceNumber;
    INT4  i4SetValNatIfTwoWayNat;
    INT4   cliWebSetError;
    INT4  rval; 
} tNatwnmhSetNatIfTwoWayNat;

typedef struct {
    int   cmd;
    INT4  i4NatIfInterfaceNumber;
    INT4  i4SetValNatIfEntryStatus;
    INT4   cliWebSetError;
    INT4  rval; 
} tNatwnmhSetNatIfEntryStatus;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4NatIfInterfaceNumber;
    INT4     i4TestValNatIfNat;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhTestv2NatIfNat;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4NatIfInterfaceNumber;
    INT4     i4TestValNatIfNapt;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhTestv2NatIfNapt;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4NatIfInterfaceNumber;
    INT4     i4TestValNatIfTwoWayNat;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhTestv2NatIfTwoWayNat;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4NatIfInterfaceNumber;
    INT4     i4TestValNatIfEntryStatus;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhTestv2NatIfEntryStatus;

typedef struct {
    int    cmd;
    INT4   i4NatIPSecSessionInterfaceNum;
    UINT4  u4NatIPSecSessionLocalIp;
    UINT4  u4NatIPSecSessionOutsideIp;
    INT4   i4NatIPSecSessionSPIInside;
    INT4   i4NatIPSecSessionSPIOutside;
    INT4    cliWebSetError;
    INT4  rval; 
} tNatwnmhValidateIndexInstanceNatIPSecSessionTable;

typedef struct {
    int      cmd;
    INT4 *   pi4NatIPSecSessionInterfaceNum;
    UINT4 *  pu4NatIPSecSessionLocalIp;
    UINT4 *  pu4NatIPSecSessionOutsideIp;
    INT4 *   pi4NatIPSecSessionSPIInside;
    INT4 *   pi4NatIPSecSessionSPIOutside;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhGetFirstIndexNatIPSecSessionTable;

typedef struct {
    int      cmd;
    INT4     i4NatIPSecSessionInterfaceNum;
    INT4 *   pi4NextNatIPSecSessionInterfaceNum;
    UINT4    u4NatIPSecSessionLocalIp;
    UINT4 *  pu4NextNatIPSecSessionLocalIp;
    UINT4    u4NatIPSecSessionOutsideIp;
    UINT4 *  pu4NextNatIPSecSessionOutsideIp;
    INT4     i4NatIPSecSessionSPIInside;
    INT4 *   pi4NextNatIPSecSessionSPIInside;
    INT4     i4NatIPSecSessionSPIOutside;
    INT4 *   pi4NextNatIPSecSessionSPIOutside;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhGetNextIndexNatIPSecSessionTable;

typedef struct {
    int      cmd;
    INT4     i4NatIPSecSessionInterfaceNum;
    UINT4    u4NatIPSecSessionLocalIp;
    UINT4    u4NatIPSecSessionOutsideIp;
    INT4     i4NatIPSecSessionSPIInside;
    INT4     i4NatIPSecSessionSPIOutside;
    UINT4 *  pu4RetValNatIPSecSessionTranslatedLocalIp;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhGetNatIPSecSessionTranslatedLocalIp;

typedef struct {
    int     cmd;
    INT4    i4NatIPSecSessionInterfaceNum;
    UINT4   u4NatIPSecSessionLocalIp;
    UINT4   u4NatIPSecSessionOutsideIp;
    INT4    i4NatIPSecSessionSPIInside;
    INT4    i4NatIPSecSessionSPIOutside;
    INT4 *  pi4RetValNatIPSecSessionLastUseTime;
    INT4     cliWebSetError;
    INT4  rval; 
} tNatwnmhGetNatIPSecSessionLastUseTime;

typedef struct {
    int     cmd;
    INT4    i4NatIPSecSessionInterfaceNum;
    UINT4   u4NatIPSecSessionLocalIp;
    UINT4   u4NatIPSecSessionOutsideIp;
    INT4    i4NatIPSecSessionSPIInside;
    INT4    i4NatIPSecSessionSPIOutside;
    INT4 *  pi4RetValNatIPSecSessionEntryStatus;
    INT4     cliWebSetError;
    INT4  rval; 
} tNatwnmhGetNatIPSecSessionEntryStatus;

typedef struct {
    int    cmd;
    INT4   i4NatIPSecSessionInterfaceNum;
    UINT4  u4NatIPSecSessionLocalIp;
    UINT4  u4NatIPSecSessionOutsideIp;
    INT4   i4NatIPSecSessionSPIInside;
    INT4   i4NatIPSecSessionSPIOutside;
    INT4   i4SetValNatIPSecSessionEntryStatus;
    INT4    cliWebSetError;
    INT4  rval; 
} tNatwnmhSetNatIPSecSessionEntryStatus;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4NatIPSecSessionInterfaceNum;
    UINT4    u4NatIPSecSessionLocalIp;
    UINT4    u4NatIPSecSessionOutsideIp;
    INT4     i4NatIPSecSessionSPIInside;
    INT4     i4NatIPSecSessionSPIOutside;
    INT4     i4TestValNatIPSecSessionEntryStatus;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhTestv2NatIPSecSessionEntryStatus;

typedef struct {
    int    cmd;
    INT4   i4NatIPSecPendingInterfaceNum;
    UINT4  u4NatIPSecPendingLocalIp;
    UINT4  u4NatIPSecPendingOutsideIp;
    INT4   i4NatIPSecPendingSPIInside;
    INT4   i4NatIPSecPendingSPIOutside;
    INT4    cliWebSetError;
    INT4  rval; 
} tNatwnmhValidateIndexInstanceNatIPSecPendingTable;

typedef struct {
    int      cmd;
    INT4 *   pi4NatIPSecPendingInterfaceNum;
    UINT4 *  pu4NatIPSecPendingLocalIp;
    UINT4 *  pu4NatIPSecPendingOutsideIp;
    INT4 *   pi4NatIPSecPendingSPIInside;
    INT4 *   pi4NatIPSecPendingSPIOutside;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhGetFirstIndexNatIPSecPendingTable;

typedef struct {
    int      cmd;
    INT4     i4NatIPSecPendingInterfaceNum;
    INT4 *   pi4NextNatIPSecPendingInterfaceNum;
    UINT4    u4NatIPSecPendingLocalIp;
    UINT4 *  pu4NextNatIPSecPendingLocalIp;
    UINT4    u4NatIPSecPendingOutsideIp;
    UINT4 *  pu4NextNatIPSecPendingOutsideIp;
    INT4     i4NatIPSecPendingSPIInside;
    INT4 *   pi4NextNatIPSecPendingSPIInside;
    INT4     i4NatIPSecPendingSPIOutside;
    INT4 *   pi4NextNatIPSecPendingSPIOutside;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhGetNextIndexNatIPSecPendingTable;

typedef struct {
    int      cmd;
    INT4     i4NatIPSecPendingInterfaceNum;
    UINT4    u4NatIPSecPendingLocalIp;
    UINT4    u4NatIPSecPendingOutsideIp;
    INT4     i4NatIPSecPendingSPIInside;
    INT4     i4NatIPSecPendingSPIOutside;
    UINT4 *  pu4RetValNatIPSecPendingTranslatedLocalIp;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhGetNatIPSecPendingTranslatedLocalIp;

typedef struct {
    int     cmd;
    INT4    i4NatIPSecPendingInterfaceNum;
    UINT4   u4NatIPSecPendingLocalIp;
    UINT4   u4NatIPSecPendingOutsideIp;
    INT4    i4NatIPSecPendingSPIInside;
    INT4    i4NatIPSecPendingSPIOutside;
    INT4 *  pi4RetValNatIPSecPendingLastUseTime;
    INT4     cliWebSetError;
    INT4  rval; 
} tNatwnmhGetNatIPSecPendingLastUseTime;

typedef struct {
    int     cmd;
    INT4    i4NatIPSecPendingInterfaceNum;
    UINT4   u4NatIPSecPendingLocalIp;
    UINT4   u4NatIPSecPendingOutsideIp;
    INT4    i4NatIPSecPendingSPIInside;
    INT4    i4NatIPSecPendingSPIOutside;
    INT4 *  pi4RetValNatIPSecPendingNoOfRetry;
    INT4     cliWebSetError;
    INT4  rval; 
} tNatwnmhGetNatIPSecPendingNoOfRetry;

typedef struct {
    int     cmd;
    INT4    i4NatIPSecPendingInterfaceNum;
    UINT4   u4NatIPSecPendingLocalIp;
    UINT4   u4NatIPSecPendingOutsideIp;
    INT4    i4NatIPSecPendingSPIInside;
    INT4    i4NatIPSecPendingSPIOutside;
    INT4 *  pi4RetValNatIPSecPendingEntryStatus;
    INT4     cliWebSetError;
    INT4  rval; 
} tNatwnmhGetNatIPSecPendingEntryStatus;

typedef struct {
    int    cmd;
    INT4   i4NatIPSecPendingInterfaceNum;
    UINT4  u4NatIPSecPendingLocalIp;
    UINT4  u4NatIPSecPendingOutsideIp;
    INT4   i4NatIPSecPendingSPIInside;
    INT4   i4NatIPSecPendingSPIOutside;
    INT4   i4SetValNatIPSecPendingEntryStatus;
    INT4    cliWebSetError;
    INT4  rval; 
} tNatwnmhSetNatIPSecPendingEntryStatus;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4NatIPSecPendingInterfaceNum;
    UINT4    u4NatIPSecPendingLocalIp;
    UINT4    u4NatIPSecPendingOutsideIp;
    INT4     i4NatIPSecPendingSPIInside;
    INT4     i4NatIPSecPendingSPIOutside;
    INT4     i4TestValNatIPSecPendingEntryStatus;
    INT4      cliWebSetError;
   INT4  rval; 
} tNatwnmhTestv2NatIPSecPendingEntryStatus;

typedef struct {
    int                        cmd;
    INT4                       i4NatIKESessionInterfaceNum;
    UINT4                      u4NatIKESessionLocalIp;
    UINT4                      u4NatIKESessionOutsideIp;
    tSNMP_OCTET_STRING_TYPE *  pNatIKESessionInitCookie;
    INT4                        cliWebSetError;
    INT4                       rval; 
} tNatwnmhValidateIndexInstanceNatIKESessionTable;

typedef struct {
    int                        cmd;
    INT4 *                     pi4NatIKESessionInterfaceNum;
    UINT4 *                    pu4NatIKESessionLocalIp;
    UINT4 *                    pu4NatIKESessionOutsideIp;
    tSNMP_OCTET_STRING_TYPE *  pNatIKESessionInitCookie;
    INT4                        cliWebSetError;
    INT4                       rval; 
} tNatwnmhGetFirstIndexNatIKESessionTable;

typedef struct {
    int                        cmd;
    INT4                       i4NatIKESessionInterfaceNum;
    INT4 *                     pi4NextNatIKESessionInterfaceNum;
    UINT4                      u4NatIKESessionLocalIp;
    UINT4 *                    pu4NextNatIKESessionLocalIp;
    UINT4                      u4NatIKESessionOutsideIp;
    UINT4 *                    pu4NextNatIKESessionOutsideIp;
    tSNMP_OCTET_STRING_TYPE *  pNatIKESessionInitCookie;
    tSNMP_OCTET_STRING_TYPE *  pNextNatIKESessionInitCookie;
    INT4                        cliWebSetError;
    INT4                       rval; 
} tNatwnmhGetNextIndexNatIKESessionTable;

typedef struct {
    int                        cmd;
    INT4                       i4NatIKESessionInterfaceNum;
    UINT4                      u4NatIKESessionLocalIp;
    UINT4                      u4NatIKESessionOutsideIp;
    tSNMP_OCTET_STRING_TYPE *  pNatIKESessionInitCookie;
    UINT4 *                    pu4RetValNatIKESessionTranslatedLocalIp;
    INT4                        cliWebSetError;
    INT4                       rval; 
} tNatwnmhGetNatIKESessionTranslatedLocalIp;

typedef struct {
    int                        cmd;
    INT4                       i4NatIKESessionInterfaceNum;
    UINT4                      u4NatIKESessionLocalIp;
    UINT4                      u4NatIKESessionOutsideIp;
    tSNMP_OCTET_STRING_TYPE *  pNatIKESessionInitCookie;
    INT4 *                     pi4RetValNatIKESessionLastUseTime;
    INT4                        cliWebSetError;
    INT4                       rval; 
} tNatwnmhGetNatIKESessionLastUseTime;

typedef struct {
    int                        cmd;
    INT4                       i4NatIKESessionInterfaceNum;
    UINT4                      u4NatIKESessionLocalIp;
    UINT4                      u4NatIKESessionOutsideIp;
    tSNMP_OCTET_STRING_TYPE *  pNatIKESessionInitCookie;
    INT4 *                     pi4RetValNatIKESessionEntryStatus;
    INT4                        cliWebSetError;
    INT4                       rval; 
} tNatwnmhGetNatIKESessionEntryStatus;

typedef struct {
    int                        cmd;
    INT4                       i4NatIKESessionInterfaceNum;
    UINT4                      u4NatIKESessionLocalIp;
    UINT4                      u4NatIKESessionOutsideIp;
    tSNMP_OCTET_STRING_TYPE *  pNatIKESessionInitCookie;
    INT4                       i4SetValNatIKESessionEntryStatus;
    INT4                        cliWebSetError;
    INT4                       rval; 
} tNatwnmhSetNatIKESessionEntryStatus;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    INT4                       i4NatIKESessionInterfaceNum;
    UINT4                      u4NatIKESessionLocalIp;
    UINT4                      u4NatIKESessionOutsideIp;
    tSNMP_OCTET_STRING_TYPE *  pNatIKESessionInitCookie;
    INT4                       i4TestValNatIKESessionEntryStatus;
    INT4                        cliWebSetError;
    INT4                       rval; 
} tNatwnmhTestv2NatIKESessionEntryStatus;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pNatPortTrigInfoInBoundPortRange;
    tSNMP_OCTET_STRING_TYPE *  pNatPortTrigInfoOutBoundPortRange;
    INT4                       i4NatPortTrigInfoProtocol;
    INT4                        cliWebSetError;
    INT4                       rval; 
} tNatwnmhValidateIndexInstanceNatPortTrigInfoTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pNatPortTrigInfoInBoundPortRange;
    tSNMP_OCTET_STRING_TYPE *  pNatPortTrigInfoOutBoundPortRange;
    INT4 *                     pi4NatPortTrigInfoProtocol;
    INT4                        cliWebSetError;
    INT4                       rval; 
} tNatwnmhGetFirstIndexNatPortTrigInfoTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pNatPortTrigInfoInBoundPortRange;
    tSNMP_OCTET_STRING_TYPE *  pNextNatPortTrigInfoInBoundPortRange;
    tSNMP_OCTET_STRING_TYPE *  pNatPortTrigInfoOutBoundPortRange;
    tSNMP_OCTET_STRING_TYPE *  pNextNatPortTrigInfoOutBoundPortRange;
    INT4                       i4NatPortTrigInfoProtocol;
    INT4 *                     pi4NextNatPortTrigInfoProtocol;
    INT4                        cliWebSetError;
    INT4                       rval; 
} tNatwnmhGetNextIndexNatPortTrigInfoTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pNatPortTrigInfoInBoundPortRange;
    tSNMP_OCTET_STRING_TYPE *  pNatPortTrigInfoOutBoundPortRange;
    INT4                       i4NatPortTrigInfoProtocol;
    tSNMP_OCTET_STRING_TYPE *  pRetValNatPortTrigInfoAppName;
    INT4                        cliWebSetError;
    INT4                       rval; 
} tNatwnmhGetNatPortTrigInfoAppName;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pNatPortTrigInfoInBoundPortRange;
    tSNMP_OCTET_STRING_TYPE *  pNatPortTrigInfoOutBoundPortRange;
    INT4                       i4NatPortTrigInfoProtocol;
    INT4 *                     pi4RetValNatPortTrigInfoEntryStatus;
    INT4                        cliWebSetError;
    INT4                       rval; 
} tNatwnmhGetNatPortTrigInfoEntryStatus;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pNatPortTrigInfoInBoundPortRange;
    tSNMP_OCTET_STRING_TYPE *  pNatPortTrigInfoOutBoundPortRange;
    INT4                       i4NatPortTrigInfoProtocol;
    tSNMP_OCTET_STRING_TYPE *  pSetValNatPortTrigInfoAppName;
    INT4                        cliWebSetError;
    INT4                       rval; 
} tNatwnmhSetNatPortTrigInfoAppName;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pNatPortTrigInfoInBoundPortRange;
    tSNMP_OCTET_STRING_TYPE *  pNatPortTrigInfoOutBoundPortRange;
    INT4                       i4NatPortTrigInfoProtocol;
    INT4                       i4SetValNatPortTrigInfoEntryStatus;
    INT4                        cliWebSetError;
    INT4                       rval; 
} tNatwnmhSetNatPortTrigInfoEntryStatus;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pNatPortTrigInfoInBoundPortRange;
    tSNMP_OCTET_STRING_TYPE *  pNatPortTrigInfoOutBoundPortRange;
    INT4                       i4NatPortTrigInfoProtocol;
    tSNMP_OCTET_STRING_TYPE *  pTestValNatPortTrigInfoAppName;
    INT4                        cliWebSetError;
    INT4                       rval; 
} tNatwnmhTestv2NatPortTrigInfoAppName;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pNatPortTrigInfoInBoundPortRange;
    tSNMP_OCTET_STRING_TYPE *  pNatPortTrigInfoOutBoundPortRange;
    INT4                       i4NatPortTrigInfoProtocol;
    INT4                       i4TestValNatPortTrigInfoEntryStatus;
    INT4                        cliWebSetError;
    INT4                       rval; 
} tNatwnmhTestv2NatPortTrigInfoEntryStatus;

typedef struct {
    int    cmd;
    UINT4  u4IfNum;
    UINT4  rval;
} tNatwNatCheckIfNatEnable;

typedef struct {
    int    cmd;
    UINT4  u4IpAddr;
    UINT4  u4Direction;
    UINT4  u4IfNum;
    UINT4  rval;
} tNatwNatSearchStaticTable;

typedef struct {
    int    cmd;
    UINT4  u4GlobalIpAddr;
    UINT4  u4IfNum;
    UINT4  rval;
} tNatwNatSearchGlobalIpAddrTable;

typedef struct {
    int    cmd;
    UINT4  u4Interface;
    UINT4  u4IpAddress;
    UINT4  u4Status;
    INT4  rval; 
} tNatwNatHandleInterfaceIndication;

typedef struct {
    int    cmd;
    UINT4  u4TraceLevel;
    INT4  rval;
} tNatwNatCliSetDebugLevel;

typedef struct {
    int    cmd;
    UINT4  u4TraceLevel;
    INT4   rval;
} tNatwNatCliResetDebugLevel;

typedef struct {
    int  cmd;
    INT4  cliWebSetError;
    UINT4  rval;
} tNatwNatApiHandleCleanAllBinds;

typedef struct {
    int                       cmd;
    tEventNotificationInfo *  pEventNotificationInfo;
    INT4                       cliWebSetError;
} tNatwsipAlgNatNotificationCbkFunc;

typedef struct {
    int  cmd;
    INT4  cliWebSetError;
} tNatwsipAlgNatDisableNotification;

typedef struct {
    int    cmd;
    INT4    cliWebSetError;
    UINT2  dPort;
    INT1 aupad[2];
} tNatwsipAlgSipPortChangeNotification;

typedef struct {
    int    cmd;
    UINT4  u4PrintCount;
    tNatPartialLinkNode  NatPartialLinkNode;
    UINT1  u1EntryFound;
    UINT1  au1Padding[3];
} tNatSipShowPartialLinks;

typedef struct {
    int              cmd;
    UINT4 *          pu4ErrorCode;
    tSnmpIndexList*  pSnmpIndexList;
    tSNMP_VAR_BIND*  pSnmpVarBind;
    INT4             rval; 
} tNatwnmhDepv2NatEnable;

typedef struct {
    int              cmd;
    UINT4 *          pu4ErrorCode;
    tSnmpIndexList*  pSnmpIndexList;
    tSNMP_VAR_BIND*  pSnmpVarBind;
    INT4             rval; 
} tNatwnmhDepv2NatTypicalNumberOfEntries;

typedef struct {
    int              cmd;
    UINT4 *          pu4ErrorCode;
    tSnmpIndexList*  pSnmpIndexList;
    tSNMP_VAR_BIND*  pSnmpVarBind;
    INT4             rval; 
} tNatwnmhDepv2NatTranslatedLocalPortStart;

typedef struct {
    int              cmd;
    UINT4 *          pu4ErrorCode;
    tSnmpIndexList*  pSnmpIndexList;
    tSNMP_VAR_BIND*  pSnmpVarBind;
    INT4             rval; 
} tNatwnmhDepv2NatIdleTimeOut;

typedef struct {
    int              cmd;
    UINT4 *          pu4ErrorCode;
    tSnmpIndexList*  pSnmpIndexList;
    tSNMP_VAR_BIND*  pSnmpVarBind;
    INT4             rval; 
} tNatwnmhDepv2NatTcpTimeOut;

typedef struct {
    int              cmd;
    UINT4 *          pu4ErrorCode;
    tSnmpIndexList*  pSnmpIndexList;
    tSNMP_VAR_BIND*  pSnmpVarBind;
    INT4             rval; 
} tNatwnmhDepv2NatUdpTimeOut;

typedef struct {
    int               cmd;
    UINT4 *           pu4ErrorCode;
    tSnmpIndexList *  pSnmpIndexList;
    tSNMP_VAR_BIND*   pSnmpVarBind;
    INT4              rval; 
} tNatwnmhDepv2NatTrcFlag;

typedef struct {
    int              cmd;
    UINT4 *          pu4ErrorCode;
    tSnmpIndexList*  pSnmpIndexList;
    tSNMP_VAR_BIND*  pSnmpVarBind;
    INT4             rval; 
} tNatwnmhDepv2NatIKEPortTranslation;

typedef struct {
    int              cmd;
    UINT4 *          pu4ErrorCode;
    tSnmpIndexList*  pSnmpIndexList;
    tSNMP_VAR_BIND*  pSnmpVarBind;
    INT4             rval; 
} tNatwnmhDepv2NatIKETimeout;

typedef struct {
    int              cmd;
    UINT4 *          pu4ErrorCode;
    tSnmpIndexList*  pSnmpIndexList;
    tSNMP_VAR_BIND*  pSnmpVarBind;
    INT4             rval; 
} tNatwnmhDepv2NatIPSecTimeout;

typedef struct {
    int              cmd;
    UINT4 *          pu4ErrorCode;
    tSnmpIndexList*  pSnmpIndexList;
    tSNMP_VAR_BIND*  pSnmpVarBind;
    INT4             rval; 
} tNatwnmhDepv2NatIPSecPendingTimeout;

typedef struct {
    int              cmd;
    UINT4 *          pu4ErrorCode;
    tSnmpIndexList*  pSnmpIndexList;
    tSNMP_VAR_BIND*  pSnmpVarBind;
    INT4             rval; 
} tNatwnmhDepv2NatIPSecMaxRetry;

typedef struct {
    int              cmd;
    UINT4 *          pu4ErrorCode;
    tSnmpIndexList*  pSnmpIndexList;
    tSNMP_VAR_BIND*  pSnmpVarBind;
    INT4             rval; 
} tNatwnmhDepv2SipAlgPort;

typedef struct {
    int              cmd;
    UINT4 *          pu4ErrorCode;
    tSnmpIndexList*  pSnmpIndexList;
    tSNMP_VAR_BIND*  pSnmpVarBind;
    INT4             rval; 
} tNatwnmhDepv2NatSipAlgPartialEntryTimeOut;

typedef struct {
    int              cmd;
    UINT4 *          pu4ErrorCode;
    tSnmpIndexList*  pSnmpIndexList;
    tSNMP_VAR_BIND*  pSnmpVarBind;
    INT4             rval; 
} tNatwnmhDepv2NatGlobalAddressTable;

typedef struct {
    int              cmd;
    UINT4 *          pu4ErrorCode;
    tSnmpIndexList*  pSnmpIndexList;
    tSNMP_VAR_BIND*  pSnmpVarBind;
    INT4             rval; 
} tNatwnmhDepv2NatLocalAddressTable;

typedef struct {
    int              cmd;
    UINT4 *          pu4ErrorCode;
    tSnmpIndexList*  pSnmpIndexList;
    tSNMP_VAR_BIND*  pSnmpVarBind;
    INT4             rval; 
} tNatwnmhDepv2NatStaticTable;

typedef struct {
    int              cmd;
    UINT4 *          pu4ErrorCode;
    tSnmpIndexList*  pSnmpIndexList;
    tSNMP_VAR_BIND*  pSnmpVarBind;
    INT4             rval; 
} tNatwnmhDepv2NatStaticNaptTable;

typedef struct {
    int              cmd;
    UINT4 *          pu4ErrorCode;
    tSnmpIndexList*  pSnmpIndexList;
    tSNMP_VAR_BIND*  pSnmpVarBind;
    INT4             rval; 
} tNatwnmhDepv2NatIfTable;

typedef struct {
    int              cmd;
    UINT4 *          pu4ErrorCode;
    tSnmpIndexList*  pSnmpIndexList;
    tSNMP_VAR_BIND*  pSnmpVarBind;
    INT4             rval; 
} tNatwnmhDepv2NatIPSecSessionTable;

typedef struct {
    int              cmd;
    UINT4 *          pu4ErrorCode;
    tSnmpIndexList*  pSnmpIndexList;
    tSNMP_VAR_BIND*  pSnmpVarBind;
    INT4             rval; 
} tNatwnmhDepv2NatIPSecPendingTable;

typedef struct {
    int              cmd;
    UINT4 *          pu4ErrorCode;
    tSnmpIndexList*  pSnmpIndexList;
    tSNMP_VAR_BIND*  pSnmpVarBind;
    INT4             rval; 
} tNatwnmhDepv2NatIKESessionTable;

typedef struct {
    int              cmd;
    UINT4 *          pu4ErrorCode;
    tSnmpIndexList*  pSnmpIndexList;
    tSNMP_VAR_BIND*  pSnmpVarBind;
    INT4             rval; 
} tNatwnmhDepv2NatPortTrigInfoTable;

typedef struct {
    int                        cmd;
    INT4                       i4NatPolicyType;
    INT4                       i4NatPolicyId;
    tSNMP_OCTET_STRING_TYPE *  pNatPolicyAclName;
    INT4                       rval; 
} tNatwnmhValidateIndexInstanceNatPolicyTable;

typedef struct {
    int                        cmd;
    INT4 *                     pi4NatPolicyType;
    INT4 *                     pi4NatPolicyId;
    tSNMP_OCTET_STRING_TYPE *  pNatPolicyAclName;
    INT4                       rval; 
} tNatwnmhGetFirstIndexNatPolicyTable;

typedef struct {
    int                        cmd;
    INT4                       i4NatPolicyType;
    INT4 *                     pi4NextNatPolicyType;
    INT4                       i4NatPolicyId;
    INT4 *                     pi4NextNatPolicyId;
    tSNMP_OCTET_STRING_TYPE *  pNatPolicyAclName;
    tSNMP_OCTET_STRING_TYPE *  pNextNatPolicyAclName;
    INT4                       rval; 
} tNatwnmhGetNextIndexNatPolicyTable;

typedef struct {
    int                        cmd;
    INT4                       i4NatPolicyType;
    INT4                       i4NatPolicyId;
    tSNMP_OCTET_STRING_TYPE *  pNatPolicyAclName;
    UINT4 *                    pu4RetValNatPolicyTranslatedIp;
    INT4                       rval; 
} tNatwnmhGetNatPolicyTranslatedIp;

typedef struct {
    int                        cmd;
    INT4                       i4NatPolicyType;
    INT4                       i4NatPolicyId;
    tSNMP_OCTET_STRING_TYPE *  pNatPolicyAclName;
    INT4 *                     pi4RetValNatPolicyEntryStatus;
    INT4                       rval; 
} tNatwnmhGetNatPolicyEntryStatus;

typedef struct {
    int                        cmd;
    INT4                       i4NatPolicyType;
    INT4                       i4NatPolicyId;
    tSNMP_OCTET_STRING_TYPE *  pNatPolicyAclName;
    UINT4                      u4SetValNatPolicyTranslatedIp;
    INT4                       rval; 
} tNatwnmhSetNatPolicyTranslatedIp;

typedef struct {
    int                        cmd;
    INT4                       i4NatPolicyType;
    INT4                       i4NatPolicyId;
    tSNMP_OCTET_STRING_TYPE *  pNatPolicyAclName;
    INT4                       i4SetValNatPolicyEntryStatus;
    INT4                       rval; 
} tNatwnmhSetNatPolicyEntryStatus;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    INT4                       i4NatPolicyType;
    INT4                       i4NatPolicyId;
    tSNMP_OCTET_STRING_TYPE *  pNatPolicyAclName;
    UINT4                      u4TestValNatPolicyTranslatedIp;
    INT4                       rval; 
} tNatwnmhTestv2NatPolicyTranslatedIp;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    INT4                       i4NatPolicyType;
    INT4                       i4NatPolicyId;
    tSNMP_OCTET_STRING_TYPE *  pNatPolicyAclName;
    INT4                       i4TestValNatPolicyEntryStatus;
    INT4                       rval; 
} tNatwnmhTestv2NatPolicyEntryStatus;

typedef struct {
    int               cmd;
    UINT4 *           pu4ErrorCode;
    tSnmpIndexList *  pSnmpIndexList;
    tSNMP_VAR_BIND *  pSnmpVarBind;
    INT4              rval; 
} tNatwnmhDepv2NatPolicyTable;

typedef struct {
    int   cmd;
    INT4  i4NatRsvdPortTrigInfoAppIndex;
    INT4  rval; 
} tNatwnmhValidateIndexInstanceNatRsvdPortTrigInfoTable;

typedef struct {
    int     cmd;
    INT4 *  pi4NatRsvdPortTrigInfoAppIndex;
    INT4  rval; 
} tNatwnmhGetFirstIndexNatRsvdPortTrigInfoTable;

typedef struct {
    int     cmd;
    INT4    i4NatRsvdPortTrigInfoAppIndex;
    INT4 *  pi4NextNatRsvdPortTrigInfoAppIndex;
    INT4  rval; 
} tNatwnmhGetNextIndexNatRsvdPortTrigInfoTable;

typedef struct {
    int      cmd;
    INT4     i4NatRsvdPortTrigInfoAppIndex;
    UINT4 *  pu4RetValNatRsvdPortTrigInfoLocalIp;
   INT4  rval; 
} tNatwnmhGetNatRsvdPortTrigInfoLocalIp;

typedef struct {
    int      cmd;
    INT4     i4NatRsvdPortTrigInfoAppIndex;
    UINT4 *  pu4RetValNatRsvdPortTrigInfoRemoteIp;
   INT4  rval; 
} tNatwnmhGetNatRsvdPortTrigInfoRemoteIp;

typedef struct {
    int      cmd;
    INT4     i4NatRsvdPortTrigInfoAppIndex;
    UINT4 *  pu4RetValNatRsvdPortTrigInfoStartTime;
   INT4  rval; 
} tNatwnmhGetNatRsvdPortTrigInfoStartTime;

typedef struct {
    int                        cmd;
    INT4                       i4NatRsvdPortTrigInfoAppIndex;
    tSNMP_OCTET_STRING_TYPE *  pRetValNatRsvdPortTrigInfoAppName;
    INT4                       rval; 
} tNatwnmhGetNatRsvdPortTrigInfoAppName;

typedef struct {
    int                        cmd;
    INT4                       i4NatRsvdPortTrigInfoAppIndex;
    tSNMP_OCTET_STRING_TYPE *  pRetValNatRsvdPortTrigInfoInBoundPortRange;
    INT4                       rval; 
} tNatwnmhGetNatRsvdPortTrigInfoInBoundPortRange;

typedef struct {
    int                        cmd;
    INT4                       i4NatRsvdPortTrigInfoAppIndex;
    tSNMP_OCTET_STRING_TYPE *  pRetValNatRsvdPortTrigInfoOutBoundPortRange;
    INT4                       rval; 
} tNatwnmhGetNatRsvdPortTrigInfoOutBoundPortRange;

typedef struct {
    int     cmd;
    INT4    i4NatRsvdPortTrigInfoAppIndex;
    INT4 *  pi4RetValNatRsvdPortTrigInfoProtocol;
    INT4  rval; 
} tNatwnmhGetNatRsvdPortTrigInfoProtocol;
typedef struct {
    int    cmd;
    UINT4  u4IpAddress;
    INT4  rval; 
} tNatwNatApiSearchPolicyTable;



union unNatNmh {
    tNatwnmhGetNatEnable nmhGetNatEnable;
    tNatwnmhGetNatTypicalNumberOfEntries nmhGetNatTypicalNumberOfEntries;
    tNatwnmhGetNatTranslatedLocalPortStart nmhGetNatTranslatedLocalPortStart;
    tNatwnmhGetNatIdleTimeOut nmhGetNatIdleTimeOut;
    tNatwnmhGetNatTcpTimeOut nmhGetNatTcpTimeOut;
    tNatwnmhGetNatUdpTimeOut nmhGetNatUdpTimeOut;
    tNatwnmhGetNatTrcFlag nmhGetNatTrcFlag;
    tNatwnmhGetNatStatDynamicAllocFailureCount nmhGetNatStatDynamicAllocFailureCount;
    tNatwnmhGetNatStatTotalNumberOfTranslations nmhGetNatStatTotalNumberOfTranslations;
    tNatwnmhGetNatStatTotalNumberOfActiveSessions nmhGetNatStatTotalNumberOfActiveSessions;
    tNatwnmhGetNatStatTotalNumberOfPktsDropped nmhGetNatStatTotalNumberOfPktsDropped;
    tNatwnmhGetNatStatTotalNumberOfSessionsClosed nmhGetNatStatTotalNumberOfSessionsClosed;
    tNatwnmhGetNatIKEPortTranslation nmhGetNatIKEPortTranslation;
    tNatwnmhGetNatIKETimeout nmhGetNatIKETimeout;
    tNatwnmhGetNatIPSecTimeout nmhGetNatIPSecTimeout;
    tNatwnmhGetNatIPSecPendingTimeout nmhGetNatIPSecPendingTimeout;
    tNatwnmhGetNatIPSecMaxRetry nmhGetNatIPSecMaxRetry;
    tNatwnmhGetSipAlgPort nmhGetSipAlgPort;
    tNatwnmhGetNatSipAlgPartialEntryTimeOut nmhGetNatSipAlgPartialEntryTimeOut;
    tNatwnmhSetNatEnable nmhSetNatEnable;
    tNatwnmhSetNatTypicalNumberOfEntries nmhSetNatTypicalNumberOfEntries;
    tNatwnmhSetNatTranslatedLocalPortStart nmhSetNatTranslatedLocalPortStart;
    tNatwnmhSetNatIdleTimeOut nmhSetNatIdleTimeOut;
    tNatwnmhSetNatTcpTimeOut nmhSetNatTcpTimeOut;
    tNatwnmhSetNatUdpTimeOut nmhSetNatUdpTimeOut;
    tNatwnmhSetNatTrcFlag nmhSetNatTrcFlag;
    tNatwnmhSetNatIKEPortTranslation nmhSetNatIKEPortTranslation;
    tNatwnmhSetNatIKETimeout nmhSetNatIKETimeout;
    tNatwnmhSetNatIPSecTimeout nmhSetNatIPSecTimeout;
    tNatwnmhSetNatIPSecPendingTimeout nmhSetNatIPSecPendingTimeout;
    tNatwnmhSetNatIPSecMaxRetry nmhSetNatIPSecMaxRetry;
    tNatwnmhSetSipAlgPort nmhSetSipAlgPort;
    tNatwnmhSetNatSipAlgPartialEntryTimeOut nmhSetNatSipAlgPartialEntryTimeOut;
    tNatwnmhTestv2NatEnable nmhTestv2NatEnable;
    tNatwnmhTestv2NatTypicalNumberOfEntries nmhTestv2NatTypicalNumberOfEntries;
    tNatwnmhTestv2NatTranslatedLocalPortStart nmhTestv2NatTranslatedLocalPortStart;
    tNatwnmhTestv2NatIdleTimeOut nmhTestv2NatIdleTimeOut;
    tNatwnmhTestv2NatTcpTimeOut nmhTestv2NatTcpTimeOut;
    tNatwnmhTestv2NatUdpTimeOut nmhTestv2NatUdpTimeOut;
    tNatwnmhTestv2NatTrcFlag nmhTestv2NatTrcFlag;
    tNatwnmhTestv2NatIKEPortTranslation nmhTestv2NatIKEPortTranslation;
    tNatwnmhTestv2NatIKETimeout nmhTestv2NatIKETimeout;
    tNatwnmhTestv2NatIPSecTimeout nmhTestv2NatIPSecTimeout;
    tNatwnmhTestv2NatIPSecPendingTimeout nmhTestv2NatIPSecPendingTimeout;
    tNatwnmhTestv2NatIPSecMaxRetry nmhTestv2NatIPSecMaxRetry;
    tNatwnmhTestv2SipAlgPort nmhTestv2SipAlgPort;
    tNatwnmhTestv2NatSipAlgPartialEntryTimeOut nmhTestv2NatSipAlgPartialEntryTimeOut;
    tNatwnmhValidateIndexInstanceNatDynamicTransTable nmhValidateIndexInstanceNatDynamicTransTable;
    tNatwnmhGetFirstIndexNatDynamicTransTable nmhGetFirstIndexNatDynamicTransTable;
    tNatwnmhGetNextIndexNatDynamicTransTable nmhGetNextIndexNatDynamicTransTable;
    tNatwnmhGetNatDynamicTransTranslatedLocalIp nmhGetNatDynamicTransTranslatedLocalIp;
    tNatwnmhGetNatDynamicTransTranslatedLocalPort nmhGetNatDynamicTransTranslatedLocalPort;
    tNatwnmhGetNatDynamicTransLastUseTime nmhGetNatDynamicTransLastUseTime;
    tNatwnmhValidateIndexInstanceNatGlobalAddressTable nmhValidateIndexInstanceNatGlobalAddressTable;
    tNatwnmhGetFirstIndexNatGlobalAddressTable nmhGetFirstIndexNatGlobalAddressTable;
    tNatwnmhGetNextIndexNatGlobalAddressTable nmhGetNextIndexNatGlobalAddressTable;
    tNatwnmhGetNatGlobalAddressMask nmhGetNatGlobalAddressMask;
    tNatwnmhGetNatGlobalAddressEntryStatus nmhGetNatGlobalAddressEntryStatus;
    tNatwnmhSetNatGlobalAddressMask nmhSetNatGlobalAddressMask;
    tNatwnmhSetNatGlobalAddressEntryStatus nmhSetNatGlobalAddressEntryStatus;
    tNatwnmhTestv2NatGlobalAddressMask nmhTestv2NatGlobalAddressMask;
    tNatwnmhTestv2NatGlobalAddressEntryStatus nmhTestv2NatGlobalAddressEntryStatus;
    tNatwnmhValidateIndexInstanceNatLocalAddressTable nmhValidateIndexInstanceNatLocalAddressTable;
    tNatwnmhGetFirstIndexNatLocalAddressTable nmhGetFirstIndexNatLocalAddressTable;
    tNatwnmhGetNextIndexNatLocalAddressTable nmhGetNextIndexNatLocalAddressTable;
    tNatwnmhGetNatLocalAddressMask nmhGetNatLocalAddressMask;
    tNatwnmhGetNatLocalAddressEntryStatus nmhGetNatLocalAddressEntryStatus;
    tNatwnmhSetNatLocalAddressMask nmhSetNatLocalAddressMask;
    tNatwnmhSetNatLocalAddressEntryStatus nmhSetNatLocalAddressEntryStatus;
    tNatwnmhTestv2NatLocalAddressMask nmhTestv2NatLocalAddressMask;
    tNatwnmhTestv2NatLocalAddressEntryStatus nmhTestv2NatLocalAddressEntryStatus;
    tNatwnmhValidateIndexInstanceNatStaticTable nmhValidateIndexInstanceNatStaticTable;
    tNatwnmhGetFirstIndexNatStaticTable nmhGetFirstIndexNatStaticTable;
    tNatwnmhGetNextIndexNatStaticTable nmhGetNextIndexNatStaticTable;
    tNatwnmhGetNatStaticTranslatedLocalIp nmhGetNatStaticTranslatedLocalIp;
    tNatwnmhGetNatStaticEntryStatus nmhGetNatStaticEntryStatus;
    tNatwnmhSetNatStaticTranslatedLocalIp nmhSetNatStaticTranslatedLocalIp;
    tNatwnmhSetNatStaticEntryStatus nmhSetNatStaticEntryStatus;
    tNatwnmhTestv2NatStaticTranslatedLocalIp nmhTestv2NatStaticTranslatedLocalIp;
    tNatwnmhTestv2NatStaticEntryStatus nmhTestv2NatStaticEntryStatus;
    tNatwnmhValidateIndexInstanceNatStaticNaptTable nmhValidateIndexInstanceNatStaticNaptTable;
    tNatwnmhGetFirstIndexNatStaticNaptTable nmhGetFirstIndexNatStaticNaptTable;
    tNatwnmhGetNextIndexNatStaticNaptTable nmhGetNextIndexNatStaticNaptTable;
    tNatwnmhGetNatStaticNaptTranslatedLocalIp nmhGetNatStaticNaptTranslatedLocalIp;
    tNatwnmhGetNatStaticNaptTranslatedLocalPort nmhGetNatStaticNaptTranslatedLocalPort;
    tNatwnmhGetNatStaticNaptDescription nmhGetNatStaticNaptDescription;
    tNatwnmhGetNatStaticNaptLeaseDuration nmhGetNatStaticNaptLeaseDuration;
    tNatwnmhGetNatStaticNaptEntryStatus nmhGetNatStaticNaptEntryStatus;
    tNatwnmhSetNatStaticNaptTranslatedLocalIp nmhSetNatStaticNaptTranslatedLocalIp;
    tNatwnmhSetNatStaticNaptTranslatedLocalPort nmhSetNatStaticNaptTranslatedLocalPort;
    tNatwnmhSetNatStaticNaptDescription nmhSetNatStaticNaptDescription;
    tNatwnmhSetNatStaticNaptLeaseDuration nmhSetNatStaticNaptLeaseDuration;
    tNatwnmhSetNatStaticNaptEntryStatus nmhSetNatStaticNaptEntryStatus;
    tNatwnmhTestv2NatStaticNaptTranslatedLocalIp nmhTestv2NatStaticNaptTranslatedLocalIp;
    tNatwnmhTestv2NatStaticNaptTranslatedLocalPort nmhTestv2NatStaticNaptTranslatedLocalPort;
    tNatwnmhTestv2NatStaticNaptDescription nmhTestv2NatStaticNaptDescription;
    tNatwnmhTestv2NatStaticNaptLeaseDuration nmhTestv2NatStaticNaptLeaseDuration;
    tNatwnmhTestv2NatStaticNaptEntryStatus nmhTestv2NatStaticNaptEntryStatus;
    tNatwnmhValidateIndexInstanceNatIfTable nmhValidateIndexInstanceNatIfTable;
    tNatwnmhGetFirstIndexNatIfTable nmhGetFirstIndexNatIfTable;
    tNatwnmhGetNextIndexNatIfTable nmhGetNextIndexNatIfTable;
    tNatwnmhGetNatIfNat nmhGetNatIfNat;
    tNatwnmhGetNatIfNapt nmhGetNatIfNapt;
    tNatwnmhGetNatIfTwoWayNat nmhGetNatIfTwoWayNat;
    tNatwnmhGetNatIfEntryStatus nmhGetNatIfEntryStatus;
    tNatwnmhSetNatIfNat nmhSetNatIfNat;
    tNatwnmhSetNatIfNapt nmhSetNatIfNapt;
    tNatwnmhSetNatIfTwoWayNat nmhSetNatIfTwoWayNat;
    tNatwnmhSetNatIfEntryStatus nmhSetNatIfEntryStatus;
    tNatwnmhTestv2NatIfNat nmhTestv2NatIfNat;
    tNatwnmhTestv2NatIfNapt nmhTestv2NatIfNapt;
    tNatwnmhTestv2NatIfTwoWayNat nmhTestv2NatIfTwoWayNat;
    tNatwnmhTestv2NatIfEntryStatus nmhTestv2NatIfEntryStatus;
    tNatwnmhValidateIndexInstanceNatIPSecSessionTable nmhValidateIndexInstanceNatIPSecSessionTable;
    tNatwnmhGetFirstIndexNatIPSecSessionTable nmhGetFirstIndexNatIPSecSessionTable;
    tNatwnmhGetNextIndexNatIPSecSessionTable nmhGetNextIndexNatIPSecSessionTable;
    tNatwnmhGetNatIPSecSessionTranslatedLocalIp nmhGetNatIPSecSessionTranslatedLocalIp;
    tNatwnmhGetNatIPSecSessionLastUseTime nmhGetNatIPSecSessionLastUseTime;
    tNatwnmhGetNatIPSecSessionEntryStatus nmhGetNatIPSecSessionEntryStatus;
    tNatwnmhSetNatIPSecSessionEntryStatus nmhSetNatIPSecSessionEntryStatus;
    tNatwnmhTestv2NatIPSecSessionEntryStatus nmhTestv2NatIPSecSessionEntryStatus;
    tNatwnmhValidateIndexInstanceNatIPSecPendingTable nmhValidateIndexInstanceNatIPSecPendingTable;
    tNatwnmhGetFirstIndexNatIPSecPendingTable nmhGetFirstIndexNatIPSecPendingTable;
    tNatwnmhGetNextIndexNatIPSecPendingTable nmhGetNextIndexNatIPSecPendingTable;
    tNatwnmhGetNatIPSecPendingTranslatedLocalIp nmhGetNatIPSecPendingTranslatedLocalIp;
    tNatwnmhGetNatIPSecPendingLastUseTime nmhGetNatIPSecPendingLastUseTime;
    tNatwnmhGetNatIPSecPendingNoOfRetry nmhGetNatIPSecPendingNoOfRetry;
    tNatwnmhGetNatIPSecPendingEntryStatus nmhGetNatIPSecPendingEntryStatus;
    tNatwnmhSetNatIPSecPendingEntryStatus nmhSetNatIPSecPendingEntryStatus;
    tNatwnmhTestv2NatIPSecPendingEntryStatus nmhTestv2NatIPSecPendingEntryStatus;
    tNatwnmhValidateIndexInstanceNatIKESessionTable nmhValidateIndexInstanceNatIKESessionTable;
    tNatwnmhGetFirstIndexNatIKESessionTable nmhGetFirstIndexNatIKESessionTable;
    tNatwnmhGetNextIndexNatIKESessionTable nmhGetNextIndexNatIKESessionTable;
    tNatwnmhGetNatIKESessionTranslatedLocalIp nmhGetNatIKESessionTranslatedLocalIp;
    tNatwnmhGetNatIKESessionLastUseTime nmhGetNatIKESessionLastUseTime;
    tNatwnmhGetNatIKESessionEntryStatus nmhGetNatIKESessionEntryStatus;
    tNatwnmhSetNatIKESessionEntryStatus nmhSetNatIKESessionEntryStatus;
    tNatwnmhTestv2NatIKESessionEntryStatus nmhTestv2NatIKESessionEntryStatus;
    tNatwnmhValidateIndexInstanceNatPortTrigInfoTable nmhValidateIndexInstanceNatPortTrigInfoTable;
    tNatwnmhGetFirstIndexNatPortTrigInfoTable nmhGetFirstIndexNatPortTrigInfoTable;
    tNatwnmhGetNextIndexNatPortTrigInfoTable nmhGetNextIndexNatPortTrigInfoTable;
    tNatwnmhGetNatPortTrigInfoAppName nmhGetNatPortTrigInfoAppName;
    tNatwnmhGetNatPortTrigInfoEntryStatus nmhGetNatPortTrigInfoEntryStatus;
    tNatwnmhSetNatPortTrigInfoAppName nmhSetNatPortTrigInfoAppName;
    tNatwnmhSetNatPortTrigInfoEntryStatus nmhSetNatPortTrigInfoEntryStatus;
    tNatwnmhTestv2NatPortTrigInfoAppName nmhTestv2NatPortTrigInfoAppName;
    tNatwnmhTestv2NatPortTrigInfoEntryStatus nmhTestv2NatPortTrigInfoEntryStatus;
    tNatwnmhValidateIndexInstanceNatPolicyTable nmhValidateIndexInstanceNatPolicyTable;
    tNatwnmhGetFirstIndexNatPolicyTable nmhGetFirstIndexNatPolicyTable;
    tNatwnmhGetNextIndexNatPolicyTable nmhGetNextIndexNatPolicyTable;
    tNatwnmhGetNatPolicyTranslatedIp nmhGetNatPolicyTranslatedIp;
    tNatwnmhGetNatPolicyEntryStatus nmhGetNatPolicyEntryStatus;
    tNatwnmhSetNatPolicyTranslatedIp nmhSetNatPolicyTranslatedIp;
    tNatwnmhSetNatPolicyEntryStatus nmhSetNatPolicyEntryStatus;
    tNatwnmhTestv2NatPolicyTranslatedIp nmhTestv2NatPolicyTranslatedIp;
    tNatwnmhTestv2NatPolicyEntryStatus nmhTestv2NatPolicyEntryStatus;
    tNatwnmhDepv2NatPolicyTable nmhDepv2NatPolicyTable;
    tNatwNatSearchStaticTable NatSearchStaticTable;
    tNatwNatSearchGlobalIpAddrTable NatSearchGlobalIpAddrTable;
    tNatwNatHandleInterfaceIndication NatHandleInterfaceIndication;
    tNatwNatCliSetDebugLevel NatCliSetDebugLevel;
    tNatwNatCliResetDebugLevel NatCliResetDebugLevel;
 tNatwNatApiHandleCleanAllBinds NatApiHandleCleanAllBinds;
    tNatwsipAlgNatNotificationCbkFunc sipAlgNatNotificationCbkFunc;
    tNatwsipAlgNatDisableNotification sipAlgNatDisableNotification;
    tNatwsipAlgSipPortChangeNotification sipAlgSipPortChangeNotification;
    tNatSipShowPartialLinks  NatSipShowPartialLinks;
    tNatwnmhDepv2NatEnable nmhDepv2NatEnable;
    tNatwnmhDepv2NatTypicalNumberOfEntries nmhDepv2NatTypicalNumberOfEntries;
    tNatwnmhDepv2NatTranslatedLocalPortStart nmhDepv2NatTranslatedLocalPortStart;
    tNatwnmhDepv2NatIdleTimeOut nmhDepv2NatIdleTimeOut;
    tNatwnmhDepv2NatTcpTimeOut nmhDepv2NatTcpTimeOut;
    tNatwnmhDepv2NatUdpTimeOut nmhDepv2NatUdpTimeOut;
    tNatwnmhDepv2NatTrcFlag nmhDepv2NatTrcFlag;
    tNatwnmhDepv2NatIKEPortTranslation nmhDepv2NatIKEPortTranslation;
    tNatwnmhDepv2NatIKETimeout nmhDepv2NatIKETimeout;
    tNatwnmhDepv2NatIPSecTimeout nmhDepv2NatIPSecTimeout;
    tNatwnmhDepv2NatIPSecPendingTimeout nmhDepv2NatIPSecPendingTimeout;
    tNatwnmhDepv2NatIPSecMaxRetry nmhDepv2NatIPSecMaxRetry;
    tNatwnmhDepv2SipAlgPort nmhDepv2SipAlgPort;
    tNatwnmhDepv2NatSipAlgPartialEntryTimeOut nmhDepv2NatSipAlgPartialEntryTimeOut;
    tNatwnmhDepv2NatGlobalAddressTable nmhDepv2NatGlobalAddressTable;
    tNatwnmhDepv2NatLocalAddressTable nmhDepv2NatLocalAddressTable;
    tNatwnmhDepv2NatStaticTable nmhDepv2NatStaticTable;
    tNatwnmhDepv2NatStaticNaptTable nmhDepv2NatStaticNaptTable;
    tNatwnmhDepv2NatIfTable nmhDepv2NatIfTable;
    tNatwnmhDepv2NatIPSecSessionTable nmhDepv2NatIPSecSessionTable;
    tNatwnmhDepv2NatIPSecPendingTable nmhDepv2NatIPSecPendingTable;
    tNatwnmhDepv2NatIKESessionTable nmhDepv2NatIKESessionTable;
    tNatwnmhDepv2NatPortTrigInfoTable nmhDepv2NatPortTrigInfoTable;
    tNatwnmhValidateIndexInstanceNatRsvdPortTrigInfoTable nmhValidateIndexInstanceNatRsvdPortTrigInfoTable;
    tNatwnmhGetFirstIndexNatRsvdPortTrigInfoTable nmhGetFirstIndexNatRsvdPortTrigInfoTable;
    tNatwnmhGetNextIndexNatRsvdPortTrigInfoTable nmhGetNextIndexNatRsvdPortTrigInfoTable;
    tNatwnmhGetNatRsvdPortTrigInfoLocalIp nmhGetNatRsvdPortTrigInfoLocalIp;
    tNatwnmhGetNatRsvdPortTrigInfoRemoteIp nmhGetNatRsvdPortTrigInfoRemoteIp;
    tNatwnmhGetNatRsvdPortTrigInfoStartTime nmhGetNatRsvdPortTrigInfoStartTime;
    tNatwnmhGetNatRsvdPortTrigInfoAppName nmhGetNatRsvdPortTrigInfoAppName;
    tNatwnmhGetNatRsvdPortTrigInfoInBoundPortRange nmhGetNatRsvdPortTrigInfoInBoundPortRange;
    tNatwnmhGetNatRsvdPortTrigInfoOutBoundPortRange nmhGetNatRsvdPortTrigInfoOutBoundPortRange;
    tNatwnmhGetNatRsvdPortTrigInfoProtocol nmhGetNatRsvdPortTrigInfoProtocol;
    tNatwNatApiSearchPolicyTable NatApiSearchPolicyTable;
    };
