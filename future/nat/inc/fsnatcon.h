
# ifndef fsnatOCON_H
# define fsnatOCON_H
/*
 *  The Constant Declarations for
 *  natStatInfo
 */

# define NATENABLE                                         (1)
# define NATTYPICALNUMBEROFENTRIES                         (2)
# define NATTRANSLATEDLOCALPORTSTART                       (3)
# define NATIDLETIMEOUT                                    (4)
# define NATTCPTIMEOUT                                     (5)
# define NATUDPTIMEOUT                                     (6)
# define NATTRCFLAG                                        (7)
# define NATSTATDYNAMICALLOCFAILURECOUNT                   (8)
# define NATSTATTOTALNUMBEROFTRANSLATIONS                  (9)
# define NATSTATTOTALNUMBEROFACTIVESESSIONS                (10)
# define NATSTATTOTALNUMBEROFPKTSDROPPED                   (11)
# define NATSTATTOTALNUMBEROFSESSIONSCLOSED                (12)

/*
 *  The Constant Declarations for
 *  natDynamicTransTable
 */

# define NATDYNAMICTRANSINTERFACENUM                       (1)
# define NATDYNAMICTRANSLOCALIP                            (2)
# define NATDYNAMICTRANSTRANSLATEDLOCALIP                  (3)
# define NATDYNAMICTRANSLOCALPORT                          (4)
# define NATDYNAMICTRANSTRANSLATEDLOCALPORT                (5)
# define NATDYNAMICTRANSOUTSIDEIP                          (6)
# define NATDYNAMICTRANSOUTSIDEPORT                        (7)
# define NATDYNAMICTRANSLASTUSETIME                        (8)

/*
 *  The Constant Declarations for
 *  natGlobalAddressTable
 */

# define NATGLOBALADDRESSINTERFACENUM                      (1)
# define NATGLOBALADDRESSTRANSLATEDLOCALIP                 (2)
# define NATGLOBALADDRESSMASK                              (3)
# define NATGLOBALADDRESSENTRYSTATUS                       (4)

/*
 *  The Constant Declarations for
 *  natLocalAddressTable
 */

# define NATLOCALADDRESSINTERFACENUMBER                    (1)
# define NATLOCALADDRESSLOCALIP                            (2)
# define NATLOCALADDRESSMASK                               (3)
# define NATLOCALADDRESSENTRYSTATUS                        (4)

/*
 *  The Constant Declarations for
 *  natStaticTable
 */

# define NATSTATICINTERFACENUM                             (1)
# define NATSTATICLOCALIP                                  (2)
# define NATSTATICTRANSLATEDLOCALIP                        (3)
# define NATSTATICENTRYSTATUS                              (4)

/*
 *  The Constant Declarations for
 *  natStaticNaptTable
 */

# define NATSTATICNAPTINTERFACENUM                         (1)
# define NATSTATICNAPTLOCALIP                              (2)
# define NATSTATICNAPTTRANSLATEDLOCALIP                    (3)
# define NATSTATICNAPTLOCALPORT                            (4)
# define NATSTATICNAPTTRANSLATEDLOCALPORT                  (5)
# define NATSTATICNAPTENTRYSTATUS                          (6)

/*
 *  The Constant Declarations for
 *  natIfTable
 */

# define NATIFINTERFACENUMBER                              (1)
# define NATIFNAT                                          (2)
# define NATIFNAPT                                         (3)
# define NATIFTWOWAYNAT                                    (4)
# define NATIFENTRYSTATUS                                  (5)

#endif /*  fsnatOCON_H  */
