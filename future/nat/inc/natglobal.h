/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: natglobal.h,v 1.6 2015/07/10 10:28:19 siva Exp $
 *
 * Description: This file contains the global variables used in 
 *              all the modules.
 *
 *******************************************************************/
#ifndef NAT_GLOBALS
#define NAT_GLOBALS
/* For RLOGIN and RSH 
 * Can be used for any protocol, which requires privilaged
 * local port (512-1023)*/
UINT2  gau2NatGlobalFreePorts [512];
UINT2  gu2FreePortIndex = 0;

tTmrDesc       gaNatTimerDesc[NAT_MAX_TIMERS]; /* Max define in natdefn.h */
tTimerListId   gNatTimerListId; /* This list should obsolete other timer lists
                                   present in Nat, when new method for timer 
                                   implementation */
tTMO_DLL       gNatFragList;    /* This list contains the fragments to be sent out */
tSipSignalPort gSipSignalPort;

/* Singly linked list definition for static Policy NAT.
   This linked list is indexed by the policy Id and the filter Id. */
tTMO_SLL  gStaticPolicyNatList;

/* Singly linked list definition for dynamic Policy NAT.
   This linked list is indexed by the policy Id and the filter Id. */
tTMO_SLL  gDynamicPolicyNatList;
 
/*Sem ID for nat cli lock*/
tOsixSemId          gNatSemId;
#endif
