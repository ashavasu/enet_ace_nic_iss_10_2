/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: natwincs.h,v 1.5 2011/06/14 11:45:06 siva Exp $
 *
 * Description:This file contains the include files needed for   
 *             nat kernel wrapper functions
 *
 *******************************************************************/
#ifndef _NAT_W_INCS_H
#define _NAT_W_INCS_H

#include "lr.h"
#include "fssnmp.h"
#include "cli.h"
#include "nat.h"
#include "sipalg.h"
#include "natcli.h"
#include "natwtdfs.h"
#include "natwdefs.h"
#include "fsnatlw.h"
#include "cfa.h"
#include "natdebug.h"
#include "secmod.h"
#include "arsec.h"

int NatNmhIoctl (unsigned long p);
#endif
