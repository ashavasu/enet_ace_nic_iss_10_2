/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsnatdb.h,v 1.6 2011/05/16 07:05:31 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSNATDB_H
#define _FSNATDB_H

UINT1 NatDynamicTransTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 NatGlobalAddressTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 NatLocalAddressTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 NatStaticTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 NatStaticNaptTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 NatIfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 NatIPSecSessionTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 NatIPSecPendingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 NatIKESessionTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 NatPortTrigInfoTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER};
UINT1 NatPolicyTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 NatRsvdPortTrigInfoTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 fsnat [] ={1,3,6,1,4,1,2076,14};
tSNMP_OID_TYPE fsnatOID = {8, fsnat};


UINT4 NatEnable [ ] ={1,3,6,1,4,1,2076,14,1,1,1};
UINT4 NatTypicalNumberOfEntries [ ] ={1,3,6,1,4,1,2076,14,1,1,2};
UINT4 NatTranslatedLocalPortStart [ ] ={1,3,6,1,4,1,2076,14,1,1,3};
UINT4 NatIdleTimeOut [ ] ={1,3,6,1,4,1,2076,14,1,1,4};
UINT4 NatTcpTimeOut [ ] ={1,3,6,1,4,1,2076,14,1,1,5};
UINT4 NatUdpTimeOut [ ] ={1,3,6,1,4,1,2076,14,1,1,6};
UINT4 NatTrcFlag [ ] ={1,3,6,1,4,1,2076,14,1,1,7};
UINT4 NatStatDynamicAllocFailureCount [ ] ={1,3,6,1,4,1,2076,14,1,1,8};
UINT4 NatStatTotalNumberOfTranslations [ ] ={1,3,6,1,4,1,2076,14,1,1,9};
UINT4 NatStatTotalNumberOfActiveSessions [ ] ={1,3,6,1,4,1,2076,14,1,1,10};
UINT4 NatStatTotalNumberOfPktsDropped [ ] ={1,3,6,1,4,1,2076,14,1,1,11};
UINT4 NatStatTotalNumberOfSessionsClosed [ ] ={1,3,6,1,4,1,2076,14,1,1,12};
UINT4 NatIKEPortTranslation [ ] ={1,3,6,1,4,1,2076,14,1,1,13};
UINT4 NatIKETimeout [ ] ={1,3,6,1,4,1,2076,14,1,1,14};
UINT4 NatIPSecTimeout [ ] ={1,3,6,1,4,1,2076,14,1,1,15};
UINT4 NatIPSecPendingTimeout [ ] ={1,3,6,1,4,1,2076,14,1,1,16};
UINT4 NatIPSecMaxRetry [ ] ={1,3,6,1,4,1,2076,14,1,1,17};
UINT4 SipAlgPort [ ] ={1,3,6,1,4,1,2076,14,1,1,18};
UINT4 NatSipAlgPartialEntryTimeOut [ ] ={1,3,6,1,4,1,2076,14,1,1,19};
UINT4 NatDynamicTransInterfaceNum [ ] ={1,3,6,1,4,1,2076,14,1,2,1,1};
UINT4 NatDynamicTransLocalIp [ ] ={1,3,6,1,4,1,2076,14,1,2,1,2};
UINT4 NatDynamicTransTranslatedLocalIp [ ] ={1,3,6,1,4,1,2076,14,1,2,1,3};
UINT4 NatDynamicTransLocalPort [ ] ={1,3,6,1,4,1,2076,14,1,2,1,4};
UINT4 NatDynamicTransTranslatedLocalPort [ ] ={1,3,6,1,4,1,2076,14,1,2,1,5};
UINT4 NatDynamicTransOutsideIp [ ] ={1,3,6,1,4,1,2076,14,1,2,1,6};
UINT4 NatDynamicTransOutsidePort [ ] ={1,3,6,1,4,1,2076,14,1,2,1,7};
UINT4 NatDynamicTransLastUseTime [ ] ={1,3,6,1,4,1,2076,14,1,2,1,8};
UINT4 NatGlobalAddressInterfaceNum [ ] ={1,3,6,1,4,1,2076,14,1,3,1,1};
UINT4 NatGlobalAddressTranslatedLocalIp [ ] ={1,3,6,1,4,1,2076,14,1,3,1,2};
UINT4 NatGlobalAddressMask [ ] ={1,3,6,1,4,1,2076,14,1,3,1,3};
UINT4 NatGlobalAddressEntryStatus [ ] ={1,3,6,1,4,1,2076,14,1,3,1,4};
UINT4 NatLocalAddressInterfaceNumber [ ] ={1,3,6,1,4,1,2076,14,1,4,1,1};
UINT4 NatLocalAddressLocalIp [ ] ={1,3,6,1,4,1,2076,14,1,4,1,2};
UINT4 NatLocalAddressMask [ ] ={1,3,6,1,4,1,2076,14,1,4,1,3};
UINT4 NatLocalAddressEntryStatus [ ] ={1,3,6,1,4,1,2076,14,1,4,1,4};
UINT4 NatStaticInterfaceNum [ ] ={1,3,6,1,4,1,2076,14,1,5,1,1};
UINT4 NatStaticLocalIp [ ] ={1,3,6,1,4,1,2076,14,1,5,1,2};
UINT4 NatStaticTranslatedLocalIp [ ] ={1,3,6,1,4,1,2076,14,1,5,1,3};
UINT4 NatStaticEntryStatus [ ] ={1,3,6,1,4,1,2076,14,1,5,1,4};
UINT4 NatStaticNaptInterfaceNum [ ] ={1,3,6,1,4,1,2076,14,1,6,1,1};
UINT4 NatStaticNaptLocalIp [ ] ={1,3,6,1,4,1,2076,14,1,6,1,2};
UINT4 NatStaticNaptStartLocalPort [ ] ={1,3,6,1,4,1,2076,14,1,6,1,3};
UINT4 NatStaticNaptEndLocalPort [ ] ={1,3,6,1,4,1,2076,14,1,6,1,4};
UINT4 NatStaticNaptProtocolNumber [ ] ={1,3,6,1,4,1,2076,14,1,6,1,5};
UINT4 NatStaticNaptTranslatedLocalIp [ ] ={1,3,6,1,4,1,2076,14,1,6,1,6};
UINT4 NatStaticNaptTranslatedLocalPort [ ] ={1,3,6,1,4,1,2076,14,1,6,1,7};
UINT4 NatStaticNaptDescription [ ] ={1,3,6,1,4,1,2076,14,1,6,1,8};
UINT4 NatStaticNaptEntryStatus [ ] ={1,3,6,1,4,1,2076,14,1,6,1,9};
UINT4 NatIfInterfaceNumber [ ] ={1,3,6,1,4,1,2076,14,1,7,1,1};
UINT4 NatIfNat [ ] ={1,3,6,1,4,1,2076,14,1,7,1,2};
UINT4 NatIfNapt [ ] ={1,3,6,1,4,1,2076,14,1,7,1,3};
UINT4 NatIfTwoWayNat [ ] ={1,3,6,1,4,1,2076,14,1,7,1,4};
UINT4 NatIfEntryStatus [ ] ={1,3,6,1,4,1,2076,14,1,7,1,5};
UINT4 NatIPSecSessionInterfaceNum [ ] ={1,3,6,1,4,1,2076,14,1,8,1,1};
UINT4 NatIPSecSessionLocalIp [ ] ={1,3,6,1,4,1,2076,14,1,8,1,2};
UINT4 NatIPSecSessionTranslatedLocalIp [ ] ={1,3,6,1,4,1,2076,14,1,8,1,3};
UINT4 NatIPSecSessionOutsideIp [ ] ={1,3,6,1,4,1,2076,14,1,8,1,4};
UINT4 NatIPSecSessionSPIInside [ ] ={1,3,6,1,4,1,2076,14,1,8,1,5};
UINT4 NatIPSecSessionSPIOutside [ ] ={1,3,6,1,4,1,2076,14,1,8,1,6};
UINT4 NatIPSecSessionLastUseTime [ ] ={1,3,6,1,4,1,2076,14,1,8,1,7};
UINT4 NatIPSecSessionEntryStatus [ ] ={1,3,6,1,4,1,2076,14,1,8,1,8};
UINT4 NatIPSecPendingInterfaceNum [ ] ={1,3,6,1,4,1,2076,14,1,9,1,1};
UINT4 NatIPSecPendingLocalIp [ ] ={1,3,6,1,4,1,2076,14,1,9,1,2};
UINT4 NatIPSecPendingTranslatedLocalIp [ ] ={1,3,6,1,4,1,2076,14,1,9,1,3};
UINT4 NatIPSecPendingOutsideIp [ ] ={1,3,6,1,4,1,2076,14,1,9,1,4};
UINT4 NatIPSecPendingSPIInside [ ] ={1,3,6,1,4,1,2076,14,1,9,1,5};
UINT4 NatIPSecPendingSPIOutside [ ] ={1,3,6,1,4,1,2076,14,1,9,1,6};
UINT4 NatIPSecPendingLastUseTime [ ] ={1,3,6,1,4,1,2076,14,1,9,1,7};
UINT4 NatIPSecPendingNoOfRetry [ ] ={1,3,6,1,4,1,2076,14,1,9,1,8};
UINT4 NatIPSecPendingEntryStatus [ ] ={1,3,6,1,4,1,2076,14,1,9,1,9};
UINT4 NatIKESessionInterfaceNum [ ] ={1,3,6,1,4,1,2076,14,1,10,1,1};
UINT4 NatIKESessionLocalIp [ ] ={1,3,6,1,4,1,2076,14,1,10,1,2};
UINT4 NatIKESessionTranslatedLocalIp [ ] ={1,3,6,1,4,1,2076,14,1,10,1,3};
UINT4 NatIKESessionOutsideIp [ ] ={1,3,6,1,4,1,2076,14,1,10,1,4};
UINT4 NatIKESessionInitCookie [ ] ={1,3,6,1,4,1,2076,14,1,10,1,5};
UINT4 NatIKESessionLastUseTime [ ] ={1,3,6,1,4,1,2076,14,1,10,1,6};
UINT4 NatIKESessionEntryStatus [ ] ={1,3,6,1,4,1,2076,14,1,10,1,7};
UINT4 NatPortTrigInfoAppName [ ] ={1,3,6,1,4,1,2076,14,1,11,1,1};
UINT4 NatPortTrigInfoInBoundPortRange [ ] ={1,3,6,1,4,1,2076,14,1,11,1,2};
UINT4 NatPortTrigInfoOutBoundPortRange [ ] ={1,3,6,1,4,1,2076,14,1,11,1,3};
UINT4 NatPortTrigInfoProtocol [ ] ={1,3,6,1,4,1,2076,14,1,11,1,4};
UINT4 NatPortTrigInfoEntryStatus [ ] ={1,3,6,1,4,1,2076,14,1,11,1,5};
UINT4 NatPolicyType [ ] ={1,3,6,1,4,1,2076,14,1,12,1,1};
UINT4 NatPolicyId [ ] ={1,3,6,1,4,1,2076,14,1,12,1,2};
UINT4 NatPolicyAclName [ ] ={1,3,6,1,4,1,2076,14,1,12,1,3};
UINT4 NatPolicyTranslatedIp [ ] ={1,3,6,1,4,1,2076,14,1,12,1,4};
UINT4 NatPolicyEntryStatus [ ] ={1,3,6,1,4,1,2076,14,1,12,1,5};
UINT4 NatRsvdPortTrigInfoAppIndex [ ] ={1,3,6,1,4,1,2076,14,1,13,1,1};
UINT4 NatRsvdPortTrigInfoLocalIp [ ] ={1,3,6,1,4,1,2076,14,1,13,1,2};
UINT4 NatRsvdPortTrigInfoRemoteIp [ ] ={1,3,6,1,4,1,2076,14,1,13,1,3};
UINT4 NatRsvdPortTrigInfoStartTime [ ] ={1,3,6,1,4,1,2076,14,1,13,1,4};
UINT4 NatRsvdPortTrigInfoAppName [ ] ={1,3,6,1,4,1,2076,14,1,13,1,5};
UINT4 NatRsvdPortTrigInfoInBoundPortRange [ ] ={1,3,6,1,4,1,2076,14,1,13,1,6};
UINT4 NatRsvdPortTrigInfoOutBoundPortRange [ ] ={1,3,6,1,4,1,2076,14,1,13,1,7};
UINT4 NatRsvdPortTrigInfoProtocol [ ] ={1,3,6,1,4,1,2076,14,1,13,1,8};




tMbDbEntry fsnatMibEntry[]= {

{{11,NatEnable}, NULL, NatEnableGet, NatEnableSet, NatEnableTest, NatEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,NatTypicalNumberOfEntries}, NULL, NatTypicalNumberOfEntriesGet, NatTypicalNumberOfEntriesSet, NatTypicalNumberOfEntriesTest, NatTypicalNumberOfEntriesDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "9000"},

{{11,NatTranslatedLocalPortStart}, NULL, NatTranslatedLocalPortStartGet, NatTranslatedLocalPortStartSet, NatTranslatedLocalPortStartTest, NatTranslatedLocalPortStartDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "6001"},

{{11,NatIdleTimeOut}, NULL, NatIdleTimeOutGet, NatIdleTimeOutSet, NatIdleTimeOutTest, NatIdleTimeOutDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "60"},

{{11,NatTcpTimeOut}, NULL, NatTcpTimeOutGet, NatTcpTimeOutSet, NatTcpTimeOutTest, NatTcpTimeOutDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "3600"},

{{11,NatUdpTimeOut}, NULL, NatUdpTimeOutGet, NatUdpTimeOutSet, NatUdpTimeOutTest, NatUdpTimeOutDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "300"},

{{11,NatTrcFlag}, NULL, NatTrcFlagGet, NatTrcFlagSet, NatTrcFlagTest, NatTrcFlagDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,NatStatDynamicAllocFailureCount}, NULL, NatStatDynamicAllocFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,NatStatTotalNumberOfTranslations}, NULL, NatStatTotalNumberOfTranslationsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,NatStatTotalNumberOfActiveSessions}, NULL, NatStatTotalNumberOfActiveSessionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,NatStatTotalNumberOfPktsDropped}, NULL, NatStatTotalNumberOfPktsDroppedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,NatStatTotalNumberOfSessionsClosed}, NULL, NatStatTotalNumberOfSessionsClosedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,NatIKEPortTranslation}, NULL, NatIKEPortTranslationGet, NatIKEPortTranslationSet, NatIKEPortTranslationTest, NatIKEPortTranslationDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,NatIKETimeout}, NULL, NatIKETimeoutGet, NatIKETimeoutSet, NatIKETimeoutTest, NatIKETimeoutDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "28800"},

{{11,NatIPSecTimeout}, NULL, NatIPSecTimeoutGet, NatIPSecTimeoutSet, NatIPSecTimeoutTest, NatIPSecTimeoutDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "28800"},

{{11,NatIPSecPendingTimeout}, NULL, NatIPSecPendingTimeoutGet, NatIPSecPendingTimeoutSet, NatIPSecPendingTimeoutTest, NatIPSecPendingTimeoutDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "30"},

{{11,NatIPSecMaxRetry}, NULL, NatIPSecMaxRetryGet, NatIPSecMaxRetrySet, NatIPSecMaxRetryTest, NatIPSecMaxRetryDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "3"},

{{11,SipAlgPort}, NULL, SipAlgPortGet, SipAlgPortSet, SipAlgPortTest, SipAlgPortDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "5060"},

{{11,NatSipAlgPartialEntryTimeOut}, NULL, NatSipAlgPartialEntryTimeOutGet, NatSipAlgPartialEntryTimeOutSet, NatSipAlgPartialEntryTimeOutTest, NatSipAlgPartialEntryTimeOutDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "300"},

{{12,NatDynamicTransInterfaceNum}, GetNextIndexNatDynamicTransTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, NatDynamicTransTableINDEX, 5, 0, 0, NULL},

{{12,NatDynamicTransLocalIp}, GetNextIndexNatDynamicTransTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, NatDynamicTransTableINDEX, 5, 0, 0, NULL},

{{12,NatDynamicTransTranslatedLocalIp}, GetNextIndexNatDynamicTransTable, NatDynamicTransTranslatedLocalIpGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, NatDynamicTransTableINDEX, 5, 0, 0, NULL},

{{12,NatDynamicTransLocalPort}, GetNextIndexNatDynamicTransTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, NatDynamicTransTableINDEX, 5, 0, 0, NULL},

{{12,NatDynamicTransTranslatedLocalPort}, GetNextIndexNatDynamicTransTable, NatDynamicTransTranslatedLocalPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NatDynamicTransTableINDEX, 5, 0, 0, NULL},

{{12,NatDynamicTransOutsideIp}, GetNextIndexNatDynamicTransTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, NatDynamicTransTableINDEX, 5, 0, 0, NULL},

{{12,NatDynamicTransOutsidePort}, GetNextIndexNatDynamicTransTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, NatDynamicTransTableINDEX, 5, 0, 0, NULL},

{{12,NatDynamicTransLastUseTime}, GetNextIndexNatDynamicTransTable, NatDynamicTransLastUseTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NatDynamicTransTableINDEX, 5, 0, 0, NULL},

{{12,NatGlobalAddressInterfaceNum}, GetNextIndexNatGlobalAddressTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, NatGlobalAddressTableINDEX, 2, 0, 0, NULL},

{{12,NatGlobalAddressTranslatedLocalIp}, GetNextIndexNatGlobalAddressTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, NatGlobalAddressTableINDEX, 2, 0, 0, NULL},

{{12,NatGlobalAddressMask}, GetNextIndexNatGlobalAddressTable, NatGlobalAddressMaskGet, NatGlobalAddressMaskSet, NatGlobalAddressMaskTest, NatGlobalAddressTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NatGlobalAddressTableINDEX, 2, 0, 0, NULL},

{{12,NatGlobalAddressEntryStatus}, GetNextIndexNatGlobalAddressTable, NatGlobalAddressEntryStatusGet, NatGlobalAddressEntryStatusSet, NatGlobalAddressEntryStatusTest, NatGlobalAddressTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NatGlobalAddressTableINDEX, 2, 0, 1, NULL},

{{12,NatLocalAddressInterfaceNumber}, GetNextIndexNatLocalAddressTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, NatLocalAddressTableINDEX, 2, 0, 0, NULL},

{{12,NatLocalAddressLocalIp}, GetNextIndexNatLocalAddressTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, NatLocalAddressTableINDEX, 2, 0, 0, NULL},

{{12,NatLocalAddressMask}, GetNextIndexNatLocalAddressTable, NatLocalAddressMaskGet, NatLocalAddressMaskSet, NatLocalAddressMaskTest, NatLocalAddressTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NatLocalAddressTableINDEX, 2, 0, 0, NULL},

{{12,NatLocalAddressEntryStatus}, GetNextIndexNatLocalAddressTable, NatLocalAddressEntryStatusGet, NatLocalAddressEntryStatusSet, NatLocalAddressEntryStatusTest, NatLocalAddressTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NatLocalAddressTableINDEX, 2, 0, 1, NULL},

{{12,NatStaticInterfaceNum}, GetNextIndexNatStaticTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, NatStaticTableINDEX, 2, 0, 0, NULL},

{{12,NatStaticLocalIp}, GetNextIndexNatStaticTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, NatStaticTableINDEX, 2, 0, 0, NULL},

{{12,NatStaticTranslatedLocalIp}, GetNextIndexNatStaticTable, NatStaticTranslatedLocalIpGet, NatStaticTranslatedLocalIpSet, NatStaticTranslatedLocalIpTest, NatStaticTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NatStaticTableINDEX, 2, 0, 0, NULL},

{{12,NatStaticEntryStatus}, GetNextIndexNatStaticTable, NatStaticEntryStatusGet, NatStaticEntryStatusSet, NatStaticEntryStatusTest, NatStaticTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NatStaticTableINDEX, 2, 0, 1, NULL},

{{12,NatStaticNaptInterfaceNum}, GetNextIndexNatStaticNaptTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, NatStaticNaptTableINDEX, 5, 0, 0, NULL},

{{12,NatStaticNaptLocalIp}, GetNextIndexNatStaticNaptTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, NatStaticNaptTableINDEX, 5, 0, 0, NULL},

{{12,NatStaticNaptStartLocalPort}, GetNextIndexNatStaticNaptTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, NatStaticNaptTableINDEX, 5, 0, 0, NULL},

{{12,NatStaticNaptEndLocalPort}, GetNextIndexNatStaticNaptTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, NatStaticNaptTableINDEX, 5, 0, 0, NULL},

{{12,NatStaticNaptProtocolNumber}, GetNextIndexNatStaticNaptTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, NatStaticNaptTableINDEX, 5, 0, 0, NULL},

{{12,NatStaticNaptTranslatedLocalIp}, GetNextIndexNatStaticNaptTable, NatStaticNaptTranslatedLocalIpGet, NatStaticNaptTranslatedLocalIpSet, NatStaticNaptTranslatedLocalIpTest, NatStaticNaptTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NatStaticNaptTableINDEX, 5, 0, 0, NULL},

{{12,NatStaticNaptTranslatedLocalPort}, GetNextIndexNatStaticNaptTable, NatStaticNaptTranslatedLocalPortGet, NatStaticNaptTranslatedLocalPortSet, NatStaticNaptTranslatedLocalPortTest, NatStaticNaptTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NatStaticNaptTableINDEX, 5, 0, 0, NULL},

{{12,NatStaticNaptDescription}, GetNextIndexNatStaticNaptTable, NatStaticNaptDescriptionGet, NatStaticNaptDescriptionSet, NatStaticNaptDescriptionTest, NatStaticNaptTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NatStaticNaptTableINDEX, 5, 0, 0, NULL},

{{12,NatStaticNaptEntryStatus}, GetNextIndexNatStaticNaptTable, NatStaticNaptEntryStatusGet, NatStaticNaptEntryStatusSet, NatStaticNaptEntryStatusTest, NatStaticNaptTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NatStaticNaptTableINDEX, 5, 0, 1, NULL},

{{12,NatIfInterfaceNumber}, GetNextIndexNatIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, NatIfTableINDEX, 1, 0, 0, NULL},

{{12,NatIfNat}, GetNextIndexNatIfTable, NatIfNatGet, NatIfNatSet, NatIfNatTest, NatIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NatIfTableINDEX, 1, 0, 0, "2"},

{{12,NatIfNapt}, GetNextIndexNatIfTable, NatIfNaptGet, NatIfNaptSet, NatIfNaptTest, NatIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NatIfTableINDEX, 1, 0, 0, "2"},

{{12,NatIfTwoWayNat}, GetNextIndexNatIfTable, NatIfTwoWayNatGet, NatIfTwoWayNatSet, NatIfTwoWayNatTest, NatIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NatIfTableINDEX, 1, 0, 0, "2"},

{{12,NatIfEntryStatus}, GetNextIndexNatIfTable, NatIfEntryStatusGet, NatIfEntryStatusSet, NatIfEntryStatusTest, NatIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NatIfTableINDEX, 1, 0, 1, NULL},

{{12,NatIPSecSessionInterfaceNum}, GetNextIndexNatIPSecSessionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, NatIPSecSessionTableINDEX, 5, 0, 0, NULL},

{{12,NatIPSecSessionLocalIp}, GetNextIndexNatIPSecSessionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, NatIPSecSessionTableINDEX, 5, 0, 0, NULL},

{{12,NatIPSecSessionTranslatedLocalIp}, GetNextIndexNatIPSecSessionTable, NatIPSecSessionTranslatedLocalIpGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, NatIPSecSessionTableINDEX, 5, 0, 0, NULL},

{{12,NatIPSecSessionOutsideIp}, GetNextIndexNatIPSecSessionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, NatIPSecSessionTableINDEX, 5, 0, 0, NULL},

{{12,NatIPSecSessionSPIInside}, GetNextIndexNatIPSecSessionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, NatIPSecSessionTableINDEX, 5, 0, 0, NULL},

{{12,NatIPSecSessionSPIOutside}, GetNextIndexNatIPSecSessionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, NatIPSecSessionTableINDEX, 5, 0, 0, NULL},

{{12,NatIPSecSessionLastUseTime}, GetNextIndexNatIPSecSessionTable, NatIPSecSessionLastUseTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NatIPSecSessionTableINDEX, 5, 0, 0, NULL},

{{12,NatIPSecSessionEntryStatus}, GetNextIndexNatIPSecSessionTable, NatIPSecSessionEntryStatusGet, NatIPSecSessionEntryStatusSet, NatIPSecSessionEntryStatusTest, NatIPSecSessionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NatIPSecSessionTableINDEX, 5, 0, 1, NULL},

{{12,NatIPSecPendingInterfaceNum}, GetNextIndexNatIPSecPendingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, NatIPSecPendingTableINDEX, 5, 0, 0, NULL},

{{12,NatIPSecPendingLocalIp}, GetNextIndexNatIPSecPendingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, NatIPSecPendingTableINDEX, 5, 0, 0, NULL},

{{12,NatIPSecPendingTranslatedLocalIp}, GetNextIndexNatIPSecPendingTable, NatIPSecPendingTranslatedLocalIpGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, NatIPSecPendingTableINDEX, 5, 0, 0, NULL},

{{12,NatIPSecPendingOutsideIp}, GetNextIndexNatIPSecPendingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, NatIPSecPendingTableINDEX, 5, 0, 0, NULL},

{{12,NatIPSecPendingSPIInside}, GetNextIndexNatIPSecPendingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, NatIPSecPendingTableINDEX, 5, 0, 0, NULL},

{{12,NatIPSecPendingSPIOutside}, GetNextIndexNatIPSecPendingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, NatIPSecPendingTableINDEX, 5, 0, 0, NULL},

{{12,NatIPSecPendingLastUseTime}, GetNextIndexNatIPSecPendingTable, NatIPSecPendingLastUseTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NatIPSecPendingTableINDEX, 5, 0, 0, NULL},

{{12,NatIPSecPendingNoOfRetry}, GetNextIndexNatIPSecPendingTable, NatIPSecPendingNoOfRetryGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NatIPSecPendingTableINDEX, 5, 0, 0, NULL},

{{12,NatIPSecPendingEntryStatus}, GetNextIndexNatIPSecPendingTable, NatIPSecPendingEntryStatusGet, NatIPSecPendingEntryStatusSet, NatIPSecPendingEntryStatusTest, NatIPSecPendingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NatIPSecPendingTableINDEX, 5, 0, 1, NULL},

{{12,NatIKESessionInterfaceNum}, GetNextIndexNatIKESessionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, NatIKESessionTableINDEX, 4, 0, 0, NULL},

{{12,NatIKESessionLocalIp}, GetNextIndexNatIKESessionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, NatIKESessionTableINDEX, 4, 0, 0, NULL},

{{12,NatIKESessionTranslatedLocalIp}, GetNextIndexNatIKESessionTable, NatIKESessionTranslatedLocalIpGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, NatIKESessionTableINDEX, 4, 0, 0, NULL},

{{12,NatIKESessionOutsideIp}, GetNextIndexNatIKESessionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, NatIKESessionTableINDEX, 4, 0, 0, NULL},

{{12,NatIKESessionInitCookie}, GetNextIndexNatIKESessionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, NatIKESessionTableINDEX, 4, 0, 0, NULL},

{{12,NatIKESessionLastUseTime}, GetNextIndexNatIKESessionTable, NatIKESessionLastUseTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NatIKESessionTableINDEX, 4, 0, 0, NULL},

{{12,NatIKESessionEntryStatus}, GetNextIndexNatIKESessionTable, NatIKESessionEntryStatusGet, NatIKESessionEntryStatusSet, NatIKESessionEntryStatusTest, NatIKESessionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NatIKESessionTableINDEX, 4, 0, 1, NULL},

{{12,NatPortTrigInfoAppName}, GetNextIndexNatPortTrigInfoTable, NatPortTrigInfoAppNameGet, NatPortTrigInfoAppNameSet, NatPortTrigInfoAppNameTest, NatPortTrigInfoTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NatPortTrigInfoTableINDEX, 3, 0, 0, NULL},

{{12,NatPortTrigInfoInBoundPortRange}, GetNextIndexNatPortTrigInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, NatPortTrigInfoTableINDEX, 3, 0, 0, NULL},

{{12,NatPortTrigInfoOutBoundPortRange}, GetNextIndexNatPortTrigInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, NatPortTrigInfoTableINDEX, 3, 0, 0, NULL},

{{12,NatPortTrigInfoProtocol}, GetNextIndexNatPortTrigInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, NatPortTrigInfoTableINDEX, 3, 0, 0, NULL},

{{12,NatPortTrigInfoEntryStatus}, GetNextIndexNatPortTrigInfoTable, NatPortTrigInfoEntryStatusGet, NatPortTrigInfoEntryStatusSet, NatPortTrigInfoEntryStatusTest, NatPortTrigInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NatPortTrigInfoTableINDEX, 3, 0, 1, NULL},

{{12,NatPolicyType}, GetNextIndexNatPolicyTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, NatPolicyTableINDEX, 3, 0, 0, NULL},

{{12,NatPolicyId}, GetNextIndexNatPolicyTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, NatPolicyTableINDEX, 3, 0, 0, NULL},

{{12,NatPolicyAclName}, GetNextIndexNatPolicyTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, NatPolicyTableINDEX, 3, 0, 0, NULL},

{{12,NatPolicyTranslatedIp}, GetNextIndexNatPolicyTable, NatPolicyTranslatedIpGet, NatPolicyTranslatedIpSet, NatPolicyTranslatedIpTest, NatPolicyTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NatPolicyTableINDEX, 3, 0, 0, NULL},

{{12,NatPolicyEntryStatus}, GetNextIndexNatPolicyTable, NatPolicyEntryStatusGet, NatPolicyEntryStatusSet, NatPolicyEntryStatusTest, NatPolicyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NatPolicyTableINDEX, 3, 0, 1, NULL},

{{12,NatRsvdPortTrigInfoAppIndex}, GetNextIndexNatRsvdPortTrigInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, NatRsvdPortTrigInfoTableINDEX, 1, 0, 0, NULL},

{{12,NatRsvdPortTrigInfoLocalIp}, GetNextIndexNatRsvdPortTrigInfoTable, NatRsvdPortTrigInfoLocalIpGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, NatRsvdPortTrigInfoTableINDEX, 1, 0, 0, ""},

{{12,NatRsvdPortTrigInfoRemoteIp}, GetNextIndexNatRsvdPortTrigInfoTable, NatRsvdPortTrigInfoRemoteIpGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, NatRsvdPortTrigInfoTableINDEX, 1, 0, 0, ""},

{{12,NatRsvdPortTrigInfoStartTime}, GetNextIndexNatRsvdPortTrigInfoTable, NatRsvdPortTrigInfoStartTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NatRsvdPortTrigInfoTableINDEX, 1, 0, 0, "0"},

{{12,NatRsvdPortTrigInfoAppName}, GetNextIndexNatRsvdPortTrigInfoTable, NatRsvdPortTrigInfoAppNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NatRsvdPortTrigInfoTableINDEX, 1, 0, 0, ""},

{{12,NatRsvdPortTrigInfoInBoundPortRange}, GetNextIndexNatRsvdPortTrigInfoTable, NatRsvdPortTrigInfoInBoundPortRangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NatRsvdPortTrigInfoTableINDEX, 1, 0, 0, ""},

{{12,NatRsvdPortTrigInfoOutBoundPortRange}, GetNextIndexNatRsvdPortTrigInfoTable, NatRsvdPortTrigInfoOutBoundPortRangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NatRsvdPortTrigInfoTableINDEX, 1, 0, 0, ""},

{{12,NatRsvdPortTrigInfoProtocol}, GetNextIndexNatRsvdPortTrigInfoTable, NatRsvdPortTrigInfoProtocolGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NatRsvdPortTrigInfoTableINDEX, 1, 0, 0, "255"},
};
tMibData fsnatEntry = { 95, fsnatMibEntry };

#endif /* _FSNATDB_H */

