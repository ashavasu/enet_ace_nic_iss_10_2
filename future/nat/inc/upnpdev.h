/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: upnpdev.h,v 1.1.1.1 2008/10/03 09:25:30 prabuc-iss Exp $
 *
 * Description: This file contains the functions used in upnpdev.c
 *              the upnp modules.
 *
 *******************************************************************/

#ifndef UPNP_NAT_DEV_H
#define UPNP_NAT_DVE_H

/* This Sections contains  *C O N S T   V A L U E S* for UPNP Sub System 
 * in NAT System */
#ifndef _UPNP_NAT_CONST_H
#define _UPNP_NAT_CONST_H

/* Action Types */
#define NAT_ACTION_SET_CON_TYPE          0   /* SetConnectionType           */
#define NAT_ACTION_GET_CON_TYPE_INFO     1   /* GetConnectionTypeInfo       */
#define NAT_ACTION_SET_REQ_CON           2   /* RequestConnection           */
#define NAT_ACTION_SET_FORCE_TERM        3   /* ForceTermination            */
#define NAT_ACTION_GET_STATUS_INFO       4   /* GetStatusInfo               */
#define NAT_ACTION_GET_NATRSIP_STATUS    5   /* GetNATRSIPStatus            */
#define NAT_ACTION_GET_GEN_PORT_MAP_ENT  6   /* GetGenericPortMappingEntry  */
#define NAT_ACTION_GET_SPC_PORT_MAP_ENT  7   /* GetSpecificPortMappingEntry */
#define NAT_ACTION_SET_ADD_PORT_MAP      8   /* AddPortMapping              */
#define NAT_ACTION_SET_DEL_PORT_MAP      9   /* DeletePortMapping           */
#define NAT_ACTION_GET_EXTN_IP_ADDR      10  /* GetExternalIPAddress        */
/* End Of Action Types */

/* Variable Types */
#define NAT_VARIABLE_TYPE_CON_TYPE           0 /* ConnectionType             */
#define NAT_VARIABLE_TYPE_POS_CON_TYPE       1 /* PossibleConnectionTypes    */
#define NAT_VARIABLE_TYPE_CON_STATUS         2 /* ConnectionStatus           */
#define NAT_VARIABLE_TYPE_UPTIME             3 /* Uptime                     */
#define NAT_VARIABLE_TYPE_LAST_CON_ERR       4 /* LastConnectionError        */
#define NAT_VARIABLE_TYPE_RSIP_AVL           5 /* RSIPAvailable              */
#define NAT_VARIABLE_TYPE_NAT_ENABLE         6 /* NATEnabled                 */
#define NAT_VARIABLE_TYPE_EXT_IPADD          7 /* ExternalIPAddress          */
#define NAT_VARIABLE_TYPE_PORT_MAP_ENT_COUNT 8 /* PortMappingNumberOfEntries */
#define NAT_VARIABLE_TYPE_PORT_MAP_TABLE     9 /* PortMappingTable           */
/* End of Variable Types */

/* Mem size of values, Max & Min Size of Incomming & Outgoing
 * Pkts  Values.
 */
#define UPNP_NAT_ALLVAL_MIN_SIZE       0
#define NAT_SIZE_OF_CON_TYPE_VAL       15
#define NAT_SIZE_OF_POS_CON_TYPE_VAL   20
#define NAT_SIZE_OF_CON_STATUS_VAL     20
#define NAT_SIZE_OF_UPTIME_VAL         20
#define NAT_SIZE_OF_LAST_CON_ERR_VAL   35
#define NAT_SIZE_OF_RSIP_AVL_VAL       10
#define NAT_SIZE_OF_NAT_ENABLE_VAL     10
#define NAT_SIZE_OF_EXT_IPADD_VAL      16
#define NAT_SIZE_OF_PORM_MAP_ENT_VAL   11 
#define NAT_SIZE_OF_PM_ENABL_VAL       2
#define NAT_SIZE_OF_PM_LDUR_VAL        10
#define NAT_SIZE_OF_PM_IPADD_VAL       16
#define NAT_SIZE_OF_PM_PORT_VAL        6
#define NAT_SIZE_OF_PM_POROTO_VAL      5
#define NAT_SIZE_OF_PM_DESC_VAL        (NAT_NAPT_ENTRY_DESCRN_LEN)
/* End of Mem size of values */

/* Fault Code In Response Messages */
#define UPNP_ACT_E_INVALID_ACT       401
#define UPNP_ACT_E_INVALID_ARG       402
#define UPNP_ACT_E_ACT_FAILED        501
#define UPNP_ACT_E_ARG_INVALID       600
#define UPNP_ACT_E_ARG_OUT_OF_RANGE  601
#define UPNP_ACT_E_OPT_ACT_NOT_IMP   602
#define UPNP_ACT_E_OUT_OF_MEM        603
#define UPNP_ACT_E_HUMAN_INT_REG     604
#define UPNP_ACT_E_STRING_TOO_LONG   605
#define UPNP_ACT_E_VENDOR_SPE        800
/* End of Fault Code */

/* ERROR Codes */
#define  UPNP_NAT_SUCCESS 1
#define  UPNP_NAT_FAILURE 0

/*Comman Constants */
#define UPNP_NAT_DEL_PORT_MAP        0
#define UPNP_NAT_ADD_PORT_MAP        1
#define UPNP_NAT_GET_GEN_PORT_MAP    0
#define UPNP_NAT_GET_SPE_PORT_MAP    1
#define DESC_URL_SIZE                200
#define NAT_SERVICE_COUNT            1    /* Number of services    */
#define NAT_SERVICE_CONTROL          0    /* Type of service       */
#define NAT_MAX_VARCOUNT             10   /* Number of  variables  */
#define NAT_MAX_VAL_LEN              200  /* Max value length      */
#define NAT_MAXACTIONS               12   /* Max actions           */
#define NAT_MAX_PORT_MAP_ENTRY       100  /* Nat Max Entry for 
                                           * PortMapping Table. 
                                           */
/* End of Comman Constants */

#endif  /* _UPNP_NAT_CONST_H */


/* This Sections contains  *T Y P E D E F *for UPNP Sub System 
 * in NAT System */
#ifndef _UPNP_NAT_TDFS_H
#define _UPNP_NAT_TDFS_H

typedef VOID (*print_string)(const INT1 *string);

typedef INT4  (*upnp_action) PROTO ((IXML_Document *request, 
                                    IXML_Document **out,
                                    INT1 **errorString));

typedef struct _tUPNPPortMapTable { 
    INT1  PortMappingEnabled [NAT_SIZE_OF_PM_ENABL_VAL];    
                                                /*PortMappingEnabled       */
    INT1  PortMappingLeaseDuration [NAT_SIZE_OF_PM_LDUR_VAL]; 
                                                /*PortMappingLeaseDuration */
    INT1  RemoteHost [NAT_SIZE_OF_PM_IPADD_VAL]; 
                                                /*RemoteHost               */
    INT1  ExternalPort [NAT_SIZE_OF_PM_PORT_VAL];  
                                                /*ExternalPort             */
    INT1  InternalPort [NAT_SIZE_OF_PM_PORT_VAL];  
                                                /*InternalPort             */
    INT1  PortMappingProtocol [NAT_SIZE_OF_PM_POROTO_VAL]; 
                                                /*PortMappingProtocol      */
    INT1  InternalClient [NAT_SIZE_OF_PM_IPADD_VAL];  
                                                /*InternalClient           */
    INT1  PortMappingDescription[NAT_SIZE_OF_PM_DESC_VAL];  
                                                /*PortMappingDescription   */
} tUPNPPortMapTable;

/* Structure for storing UPNP_NAT Service  identifiers and state table */
typedef struct _tUPNPNATService {
  INT1 UDN[NAME_SIZE];                    /* Universally Unique Device Name */
  INT1 ServiceId[NAME_SIZE];
  INT1 ServiceType[NAME_SIZE];
  INT1 *VariableName[NAT_MAX_VARCOUNT]; 
  INT1 *VariableStrVal[NAT_MAX_VARCOUNT];
  INT1 *ActionNames[NAT_MAXACTIONS];
  upnp_action actions[NAT_MAXACTIONS];
  UINT4  VariableCount;
} tUPNPNATService;


#endif  /* _UPNP_NAT_TDFS_H  */


/* This Sections contains  *G L O B A L   V A R I A B L E S*
 * for UPNP Sub System in NAT System */
#ifndef _UPNP_NAT_GLOB_H
#define _UPNP_NAT_GLOB_H

#ifdef UPNP_NAT_SERVICES
/* Global structure for storing the state table for this device */
tUPNPNATService gUpnpNatServiceTable[1];
/* Device type for WANConnectionDevice */
INT1 gapu1UpnpWanConnDeviceType [] = {
                           "urn:schemas-upnp-org:device:WANConnectionDevice:1"
                           };
/* Service types for WANConnectionDevice */
INT1 *gapu1UpnpNatServiceType [] = { 
                             "urn:schemas-upnp-org:service:WANIPConnection:1" 
                            };
tUPNPPortMapTable  gaUPNPPortMapTable [NAT_MAX_PORT_MAP_ENTRY];

INT1 *gapu1UpnpNatVarNames[] = { "ConnectionType",  
                                "PossibleConnectionTypes",
                                "ConnectionStatus",
                                "Uptime",
                                "LastConnectionError", 
                                "RSIPAvailable",
                                "NATEnabled", 
                                "ExternalIPAddress", 
                                "PortMappingNumberOfEntries", 
                                "PortMappingTable"
                            };

INT1 gapu1UpnpNatVarVal[NAT_MAX_VARCOUNT][NAT_MAX_VAL_LEN];

INT1 *gapu1UpnpNatVarValDef[] = { "IP_Routed", /* ConnectionType */ 
                                 "IP_Routed", /* PossibleConnectionTypes */
                                 "Connected", /* ConnectionStatus */
                                 "0",     /* Uptime */
                                 "ERROR_NONE", /* LastConnectionError */
                                 "0",         /* RSIPAvailable */
                                 "0",         /* NATEnabled */
                                 "",    /* ExternalIPAddress */
                                 "0",         /* PortMappingNumberOfEntries */
                                 "0"          /* PortMappingTable */
                              }; 

/*The amount of time (in seconds) before advertisements will expire */
UINT4 gu4UpnpAddExpTimeOut = 100;
/* Device handle supplied by UPnP SDK */
UpnpDevice_Handle gUpnpNatSerHandle = -1;
INT1  *pu1Cookie =NULL;
ithread_mutex_t gUpnpNatSem4;

#endif /* UPNP_NAT_SERVICES */

#endif  /* _UPNP_NAT_GLOB_H  */


/* This Sections contains  *M A C O R S* and  *D E F I N E* for UPNP Sub System 
 * in NAT System */
#ifndef _UPNP_NAT_DEFS_H
#define _UPNP_NAT_DEFS_H

#define DEFAULT_WEB_DIR           "./web"
#define UPNP_NAT_RHOST_WILDCARD   "0"

/* Locks for UPNP NAT System */
#define UPNP_NAT_LOCK()           NAT_LOCK() 
#define UPNP_NAT_UNLOCK()         NAT_UNLOCK()

#define UPNP_NAT_MALLOC(s)             NAT_MALLOC(s)
#define UPNP_NAT_MFREE(p)              NAT_FREE(p)  
#define UPNP_NAT_SIZEOF(p)             sizeof(p)

#define   UPNP_NAT_STRNCPY(dst, src, size)  \
do{\
\
     STRNCPY(dst, src, (size-1)); \
     src[size] = '\0'; \
} while(0)

/* Global variable Macros  */

#define SET_UPNP_NAT_STATE_VAR_VAL(VarType ,Value)  \
           do \
             { \
                 gUpnpNatServiceTable[NAT_SERVICE_CONTROL].\
                         VariableStrVal[VarType] =  Value;         \
              } while(0) 
        
#define SEND_NOTIFY_TO_CONTROL_POINTS()  \
                     UpnpNotify( gUpnpNatSerHandle, \
                     gUpnpNatServiceTable[NAT_SERVICE_CONTROL].UDN, \
                     gUpnpNatServiceTable[NAT_SERVICE_CONTROL].ServiceId, \
                     (const char **) \
                     gUpnpNatServiceTable[NAT_SERVICE_CONTROL]. \
                     VariableName,   \
                     (const char **) \
                     gUpnpNatServiceTable[NAT_SERVICE_CONTROL]. \
                     VariableStrVal, \
                     10) 

#endif  /* _UPNP_NAT_DEFS_H  */



/* This Sections contains  *E X T E R N S* for UPNP Sub System
 * in NAT System */
#ifndef _UPNP_NAT_EXTN_H
#define _UPNP_NAT_EXTN_H

PUBLIC ithread_mutex_t TVDevMutex;
PUBLIC print_string gPrintFun;
PUBLIC INT1 gapu1UpnpWanConnDeviceType[];
PUBLIC INT1 *gapu1UpnpNatServiceType[];
/* Device handle returned from sdk */
PUBLIC UpnpDevice_Handle gUpnpNatSerHandle;
/* mutex to control displaying of events */
PUBLIC ithread_mutex_t display_mutex ;
PUBLIC tUPNPNATService gUpnpNatServiceTable[]; 


/* Extern Prototypes */
PUBLIC int vsnprintf(char *str, size_t size, const char *format, va_list ap);
PUBLIC int snprintf(char *str, size_t size, const char *format, ...);


PUBLIC tSNMP_OCTET_STRING_TYPE *SNMP_AGT_FormOctetString PROTO((UINT1 *, INT4));

/* End Of Extern Prototypes */

#endif  /* _UPNP_NAT_EXTN_H  */




/* This Sections contains  *P R O T O T Y P E S* for UPNP Sub System 
 * in NAT System */
#ifndef _UPNP_NAT_PROTO_H
#define _UPNP_NAT_PROTO_H

/* Function Proto Types  in  upnpserv.c */
INT4 
SetActionTable PROTO ((INT4 serviceType, tUPNPNATService * out));
INT4 
UpnpNatServiceStateTableInit PROTO ((INT1*));
INT4 
UpnpNatServiceHandleSubscriptionRequest 
                              PROTO ((struct Upnp_Subscription_Request *));
INT4 
UpnpNatServiceHandleGetVarRequest PROTO ((struct Upnp_State_Var_Request *));
INT4 
UpnpNatServiceHandleActionRequest PROTO ((struct Upnp_Action_Request *));
int  
UpnpNatServiceCallbackEventHandler PROTO ((Upnp_EventType, VOID *, VOID *));
INT4 
UpnpNatDeviceSetServiceTableVar PROTO ((UINT4, UINT4, INT1*));
INT4 
UpnpNatServiceSetConnectionType PROTO ((IXML_Document * in,
                                        IXML_Document ** out,
                                        INT1 **errorString));
INT4 
UpnpNatServiceGetConnectionTypeInfo PROTO ((IXML_Document * in,
                                            IXML_Document ** out,
                                            INT1 **errorString));
INT4 
UpnpNatServiceRequestConnection PROTO ((IXML_Document * in,
                                        IXML_Document ** out,
		                                INT1 **errorString));
INT4 
UpnpNatServiceForceTermination  PROTO ((IXML_Document * in,
                                        IXML_Document ** out,
                                        INT1 **errorString));
INT4 
UpnpNatServiceGetStatusInfo  PROTO ((IXML_Document * in,
                                     IXML_Document ** out,
                                     INT1 **errorString));
INT4 
UpnpNatServiceGetNATRSIPStatus PROTO ((IXML_Document * in,
                                       IXML_Document ** out,
                                       INT1 **errorString));
INT4 
UpnpNatServiceGetSpecificPortMappingEntry PROTO ((IXML_Document * in,
                                                  IXML_Document ** out,
                                                  INT1 **errorString));
INT4 
UpnpNatServiceGetGenericPortMappingEntry PROTO ((IXML_Document * in,
                                                 IXML_Document ** out,
                                                 INT1 **errorString));
INT4 
UpnpNatServiceAddPortMapping  PROTO ((IXML_Document * in,
                                      IXML_Document ** out,
                                      INT1 **errorString));
INT4 
UpnpNatServiceDeletePortMapping PROTO ((IXML_Document * in,
                                        IXML_Document ** out,
                                        INT1 **errorString));
INT4 
UpnpNatServiceGetExternalIPAddress  PROTO ((IXML_Document * in,
                                            IXML_Document ** out,
                                            INT1 **errorString));
INT4 
UpnpNatServiceStart PROTO ((INT1 * ip_address, UINT2 port, 
                            INT1 * desc_doc_name, INT1 *web_dir_path
                            ));

INT4 UpnpNatDeviceStop PROTO ((VOID));

INT4 
GetPortMappingEntery PROTO ((UINT4 u4InterfaceNum , 
                              tUPNPPortMapTable  *value));
INT4 
SetPortMappingEntery PROTO ((UINT4 u4InterfaceNum , 
                             tUPNPPortMapTable  *value));
VOID 
GetEventableVariablsAndValues PROTO ((INT1 *EVariabls ,
                                      INT1 *Evalues,
                                      INT4  *count));
INT4 
SetServiceTable PROTO ((INT4 serviceType, const INT1 *UDN,
                        const INT1 *serviceId, const INT1 *serviceTypeS,
                        tUPNPNATService *out));
INT4
UpnpNatDeviceGetServiceTableVar PROTO ((UINT4 service, UINT4 variable,
                                        INT1 *value));
UINT4 
GetNumberOfPortMapEntry  PROTO ((UINT4 u4InterfaceNum, UINT4 *Count));
UINT4 
GetNatCurrentTranIPAddr  PROTO ((UINT4  u4InterfaceNum,  
                                 UINT4 *pCurTranLocIPAddr));
VOID CfaConvertIntToStr PROTO ((UINT4, UINT1 *));
UINT4 
GetIndexFromValues  PROTO ((INT4 i4TranslatedLocPort, INT2 i2ProtocolNumber,
                            INT4  *i4NextIfNum,        
                            UINT4 *u4NextLocIpAddr,
                            INT4  *i4NextStartLocalPort,
                            INT4  *i4NextEndLocalPort,
                            INT4  *i4NextProtocolNumber));
UINT4 
UpnpAddPortMappingEntry  PROTO ((UINT4 u4IfNumb, 
                                 tUPNPPortMapTable *PortMappingEntry)); 

/* End of Function Prototypes in upnpserv.c */


/* Function Prototypes  in  upnputl.c */
INT1 *
GetFirstDocumentItem PROTO ((IXML_Document *doc, INT1 *item));
INT4
SampleUtil_PrintEvent PROTO (( Upnp_EventType EventType,  VOID *Event));
INT4
FindAndParseService PROTO (( IXML_Document *DescDoc,  INT1* location,
                                  INT1 *serviceType,  INT1 **serviceId, 
				                  INT1 **eventURL,  INT1 **controlURL));
INT4 
SampleUtil_Initialize  PROTO ((print_string print_function));
INT4 
SampleUtil_Finish  PROTO ((VOID));
INT4 
SampleUtil_Print  PROTO (( INT1 *fmt, ... ));
INT1 *
GetDeviceUDN  PROTO (( IXML_Document * doc, const INT1 *item ));
VOID 
linux_print PROTO (( const INT1 *string ));
IXML_NodeList * 
SampleUtil_GetFirstServiceList PROTO ((IXML_Document * doc ));
INT1 * 
SampleUtil_GetFirstElementItem  PROTO (( IXML_Element * element,
                                         INT1 *item ));
VOID 
SampleUtil_PrintEventType PROTO ((Upnp_EventType S ));
INT4 
UpnpGetSpePortMappingEntry PROTO ((UINT4 u4IfNum , 
                                   tUPNPPortMapTable *pPortMappingEntry ));
INT4
UpnpCopyStructure PROTO (( tUPNPPortMapTable *pPortMapEntry,
                           tStaticNaptEntry  *pNaptEntry ));
INT4
UpnpGetGenPortMappingEntry PROTO ((UINT4 u4IfNum, 
                                   tUPNPPortMapTable *pPortMappingEntry ));
UINT4 
UpnpDelPortMappingEntry  PROTO (( UINT4 u4IfNumb, 
                                  tUPNPPortMapTable *PortMappingEntry )); 
UINT4 
GetIndexFromValues  PROTO (( INT4 i4TranslatedLocPort, INT2 i2ProtocolNumber,
                             INT4  *i4NextIfNum,        
                             UINT4 *u4NextLocIpAddr,
                             INT4  *i4NextStartLocalPort,
                             INT4  *i4NextEndLocalPort,
                             INT4  *i4NextProtocolNumber));

INT4
UpnpNatNotifyEventableVariables PROTO ((VOID));
INT4
UpnpNatCreatePropertySet PROTO ((UINT4 u4ServiceType, 
                                 IXML_Document *pIXML_Document));
/* NEW-UPNP */
INT4 
UpnpTestUniqueExtPortProtoComb PROTO ((UINT4 u4InterfaceNum, UINT4 u4LocalIpAddr                                      ,INT4 i4StartPort, INT4 i4EndPort,
                                       INT4 i4ProtoNo, INT4 i4ExtPort));


/* End of Function Prototypes in upnputl.c */

#endif  /* _UPNP_NAT_PROTO_H */

#endif  /* End of  UPNP_NAT_DVE_H */

/*************************** END OF UPNPDEV.H ********************************/

