
#ifndef _NATFRAG_H
#define _NATFRAG_H



/* RED-BLACK TREE  */
typedef tRBTree     tNatFragRBTree;
typedef tRBElem     tNatFragRBElem;

#define NAT_FRAG_RB_TREE  pReasmRBTree

/* Used by fragment reassembly scheme */
#define   NAT_FRAG_INSERT     0
#define   NAT_FRAG_APPEND     1
#define   NAT_FRAG_PREPEND    2

#define   NAT_MEM_SUCCESS    MEM_SUCCESS
#define   NAT_MEM_FAILURE    MEM_FAILURE

#define   NAT_FRAG_REASS_TIME_VALUE                15 /* in seconds */
#define   NAT_FRAG_REASS_TICKS                     NAT_FRAG_REASS_TIME_VALUE * \
                                                   SYS_NUM_OF_TIME_UNITS_IN_A_SEC

#define   NAT_IP_ID_OFFSET_IN_IPHEADER             4
#define   NAT_IP_ID_FIELD_LEN                      2

#define   NAT_IP_HDRLEN_AND_VER_OFFSET               0
#define   NAT_IP_HDRLEN_AND_VER_FIELD_LEN            1
#define   NAT_IP_HDRLEN_MASK                         0x0f
#define   NAT_IP_4_BYTE_WORD_TO_NO_OF_BYTES          2


/*#define   NAT_IP_FRAG_OFFSET_IN_IP_HDR               6 defined in natdefn.h*/
#define   NAT_IP_FRAG_OFFSET_LEN                     2

#define   NAT_FRAG_TOTAL_LEN_OFFSET                   2
#define   NAT_TWO_BYTE_LEN                            2

#define   NAT_DF_AND_0_OFFSET                         0x4000

#define   NAT_FRAG_OFFSET_MASK     0x1fff
#define   NAT_OFFSET_IN_BYTES(fl_offs) ((fl_offs & NAT_FRAG_OFFSET_MASK) << 3)
#define   NAT_OFFSET_IN_IP_HDR_FORMAT(fl_offs)(fl_offs >> 3)

#define   NAT_OFFSET_FOR_CHKSUM                        10
#define   NAT_IP_HDR_CHKSUM_LEN                        2

#define   NAT_MAC_IP_HDR_LEN                           14+20 


#define NAT_CONCAT_BUFS(pBuf1, pBuf2)                             \
   CRU_BUF_Concat_MsgBufChains((pBuf1), (pBuf2))

#define NAT_IP_FRAGMENT_BUF(pBuf, u4Offset, ppFragBuf)               \
   CRU_BUF_Fragment_BufChain((pBuf), (u4Offset), (ppFragBuf))

#define   NAT_BUF_MOVE_VALID_OFFSET(pBuf, u4Size) \
          CRU_BUF_Move_ValidOffset((pBuf), (u4Size))

#define   NAT_ALLOCATE_BUF(u4Size, u4ValidOffset)                 \
          CRU_BUF_Allocate_MsgBufChain((u4Size), (u4ValidOffset))


#define   NAT_RELEASE_BUF(pBuf, u1ForceFlag)                      \
          CRU_BUF_Release_MsgBufChain((pBuf), (u1ForceFlag));     \
          pBuf = NULL

#define   NAT_FRAG_ALLOCATE_MEM_BLOCK(PoolId, pu1Block,type)       \
            (pu1Block = (type *)(MemAllocMemBlk ((tMemPoolId)PoolId)))


#define   NAT_RELEASE_MEM_BLOCK(PoolId, pBlk)                     \
          MemReleaseMemBlock((PoolId), (UINT1 *)(pBlk))


#define NAT_FRAG_START_TIMER(TimerListId, pTmrRef, u4Duration)  \
   TmrStartTimer((TimerListId), (pTmrRef), \
           SYS_NUM_OF_TIME_UNITS_IN_A_SEC  * (u4Duration))

#define   NAT_GET_BUF_LENGTH(pBuf) \
          CRU_BUF_Get_ChainValidByteCount((pBuf))

#define   NAT_COPY_FROM_BUF(pBuf, pDst, u4Offset, u4Size)         \
          CRU_BUF_Copy_FromBufChain((pBuf),(UINT1 *)(pDst),(u4Offset), (u4Size))

#define   NAT_COPY_TO_BUF(pBuf, pSrc, u4Offset, u4Size)           \
          CRU_BUF_Copy_OverBufChain((pBuf),(UINT1 *)(pSrc),(u4Offset),(u4Size))
#endif
