/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: nattdfs.h,v 1.12 2015/07/16 10:50:31 siva Exp $
 *
 * Description:This file contains the data structure typedefs
 *             used by FutureNAT
 *
 *******************************************************************/
/**** Type Definition for Dynamic Table *****/

#ifndef _NAT_TDFS_H
#define _NAT_TDFS_H

#if (defined(SDF_TIMESTAMP) || defined(SDF_PASSTHROUGH))
#include <sys/time.h>
#endif

#ifndef PACK_REQUIRED 
#pragma pack(1) 
#endif 
typedef struct CuSeeMeHeader 
{ 
    UINT4               u4Addr; 
    UINT4               u4Seq; 
    UINT4               u4DestAddr; 
    UINT2               u2DestFamily; 
    UINT2               u2DestPort; 
    UINT2               u2Family; 
    UINT2               u2Port; 
    UINT2               u2Msg; 
    UINT2               u2DataType; 
    UINT2               u2PktLen; 
    UINT1               au1pad[2]; 
} 
tCuSeeMeHeader; 

/* Open Continue Header */ 
typedef struct OpenContHeader 
{ 
    INT1                ai1UserName[20]; 
    UINT4               u4SeqNum; 
    UINT2               u2ClientCount; 
    INT1                ai1pad[2]; 
} 
tOCHeader; 

/* Client information */ 
typedef struct ClientInfo 
{ 
    UINT4               u4Addr; 
    INT1                ai1Reserved[8]; 
} 
tClientInfo; 
#ifndef PACK_REQUIRED 
#pragma pack() 
#endif  

/**** Type Definition for HeaderInformation *****/
typedef struct HeaderInfo {
    UINT4          u4IfNum;
    UINT4          u4InIpAddr;
    UINT4          u4OutIpAddr;
    UINT2          u2InPort;
    UINT2          u2OutPort;
    UINT4          u4Direction;
    UINT1          u1PktType;
    UINT1          u1IpHeadLen;
    UINT1          u1TransportHeadLen;
    UINT1          u1SynBit;
    INT4           i4Delta;
    UINT2          u2TotLen;
    UINT1          u1IfLinear;
    UINT1          u1Reserved;
    tDynamicEntry  *pDynamicEntry;

}tHeaderInfo;

/**** Type Definition for Local Address Table *****/

typedef struct LocIpAddrNode{
    tTMO_SLL_NODE  LocIpAddrNode;
    UINT4          u4InterfaceNumber;
    UINT4          u4LocIpNum;
    UINT4          u4Mask;
    INT4           i4RowStatus;
}tLocIpAddrNode;

/**** Type Definition for Static Table *****/


typedef struct StaticTable {
    tTMO_SLL_NODE  StaticTable;
    UINT4          u4LocIpAddr;
    UINT4          u4TranslatedLocIpAddr;
    INT4           i4RowStatus;
}tStaticEntryNode;

/* UPNP NAPT TIMER */
/* Timer Structure ForEach Entries in NAPT Table */
typedef struct NaptTimerNode {
    tTmrAppTimer            TimerNode;
    UINT1                   u1TimerId;
    UINT1                   u1TimerStatus;
    UINT2                   u2Allignment;
    struct StaticNaptTable  *pNaptBackPtr; 
    INT4                    i4IfIndex; 
 }tNaptTimer;   


/**** Type Definition for Static-Napt Table *****/

typedef struct StaticNaptTable {
    tTMO_SLL_NODE  StaticNaptTable;
    tNaptTimer     NaptTimerNode; /* Added Timer Field */
    UINT4          u4LocIpAddr;
    UINT4          u4TranslatedLocIpAddr;
    UINT2          u2StartLocPort;/* Start of port range */
    UINT2          u2EndLocPort; /* End of port range */
    UINT2          u2TranslatedLocPort;
    UINT2          u2ProtocolNumber; 
    INT1           i1Description[NAT_NAPT_ENTRY_DESCRN_LEN];
    INT4           i4RowStatus;
    UINT1          u1AppCallStatus; /*  HOLD or UNHOLD */
    UINT1          au1Padding[3];
}tStaticNaptEntry;

/* END */

/**** Type Definition for Global Address Table *****/


typedef struct TranslatedLocIpAddrNode{
    tTMO_SLL_NODE  TranslatedLocIpAddrNode;
    UINT4          u4TranslatedLocIpAddr;
    UINT4          u4Mask;
    INT4           i4RowStatus;
}tTranslatedLocIpAddrNode;

/**** Type Definition for Interface Information *****/


typedef struct InterfaceInfo {
    UINT1     u1NatEnable;
    UINT1     u1NaptEnable;
    UINT1     u1TwoWayNatEnable;
    UINT1     u1Reserved;
    tTMO_SLL  StaticList;
    tTMO_SLL  StaticNaptList;
    tTMO_SLL  TranslatedLocIpList;
    tTMO_SLL  NatFreeGipList;
    UINT4     u4NumOfTranslation;
    UINT4     u4NumOfActiveSessions;
    UINT4     u4NumOfPktsDropped;
    UINT4     u4NumOfSessionsClosed;
    UINT4     u4CurrTranslatedLocIpAddr;
    UINT4     u4TranslatedLocIpRangeEnd;
    UINT4     u4CurrDnsTranslatedLocIpInUse;
    UINT4     u4CurrDnsTranslatedLocIpRangeEnd;
    INT4      i4RowStatus;
}tInterfaceInfo;

/**** Type Definition for allocated Global Information *****/

typedef struct GlobalInfo {
    UINT4  u4TranslatedLocIpAddr;
    UINT2  u2TranslatedLocPort;
    UINT2  u2Reserved;
}tGlobalInfo ;

/**** Type Definition for Free Port List *****/

typedef struct FreePortNode {
    tTMO_SLL_NODE  NextFreePort;
    UINT2          u2Port;
    UINT2          u2Reserved;
}tFreePortNode;

/**** Type Definition for Free Id List *****/


typedef struct FreeIdNode {
    tTMO_SLL_NODE  FreeIdList;
    UINT4          u4Id;
}tFreeIdNode;

/**** Type Definition for Nat Free Global IP List *****/

typedef struct NatFreeGipListNode{
    tTMO_SLL_NODE  NatFreeGipListNode;
    UINT4          u4TranslatedLocIpAddr;
}tNatFreeGipListNode;

/**** Type Definition for Hash Table Node*****/

typedef struct LocOutHash {
    tTMO_SLL_NODE  LocOutHash;
    UINT4          u4IpAddr;
    UINT2          u2Port;
    UINT2          u2Reserved;
    UINT4          u4Id;
    UINT4          u4NoOfEntries;
 }tLocOutHashNode;

/**** Type Definition for Global IID List *****/


typedef struct GlobalIidList {
    tTMO_SLL_NODE  GlobalIidList;
    UINT4          u4InId;
    UINT4          u4NoOfEntries;
}tIidListNode;



typedef struct LinearBuf{
    UINT1 au1LinearBuf[NAT_MAX_LINEAR_BUF_SIZE];
}tLinearBuf;

/**** Type Definition for DNS Table *****/


typedef struct DnsEntryNode {
    tTMO_SLL_NODE  DnsTable;
    UINT4          u4LocIpAddr;
    UINT4          u4TranslatedLocIpAddr;
    UINT4          u4BindTimeStamp;
}tDnsEntryNode,tNfsIpNode;

/**** Type Definition Dynamic List in IidOidArray *****/

typedef struct IidOidArrayDynamicListNode  {
    tTMO_SLL_NODE  IidOidArrayDynamicListNode;
    tDynamicEntry  *pDynamicEntry;
}tIidOidArrayDynamicListNode,tUdpList;


/**** Type Definition for IidOidArray *****/

typedef struct ArrayNode {
    tTMO_SLL_NODE                ArrayNode;
    tIidOidArrayDynamicListNode  *pHead;
}tArrayNode;


/**** Type Definition for TCP Deletion List *****/

typedef struct TcpDelNode {
    tTMO_SLL_NODE  TcpDelNode;
    tDynamicEntry  *pDynamicEntry;
    UINT4          u4Direction;
    UINT4          u4TimeSpent;
}tTcpDelNode;

/**** Type Definition for NatOnlyNode *****/

typedef struct NatOnlyNode{
    tTMO_SLL_NODE  NatOnlyNode;
    UINT4          u4IfNum;
    UINT4          u4LocIpAddr;
    UINT4          u4TranslatedLocIpAddr;
    UINT4          u4NoOfConnections;
    UINT1          u1MulticastFlag;
    UINT1          u1Reserved;
    UINT2          u2Reserved;
    UINT4          u4TimeStamp;
}tNatOnlyNode;

/**** Type Definition for Address Information *****/

typedef struct AddrInfo{
    UINT4  u4IpAddr;
    UINT4  u4StartOffset;
    UINT4  u4EndOffset;
}tAddrInfo;


typedef struct MallocNode{
    UINT1  *pu1Block;
    INT1   i1FileName[52];
    INT4   i4LineNo;
    UINT4  u4Size;
    struct MallocNode *pPrev;
    struct MallocNode *pNext;
}tNatDbgMallocNode;

/* Application Masquerading Definition */
struct NatAppDefn {
    INT1            AppName[8]; /* Application Name */
    UINT4           u4AppId;     /* Unique Key that identifies the Appln */
    UINT4           u4AppUseCount; /* No of active sessions */
    INT4           (*pProcessPkt) (tCRU_BUF_CHAIN_HEADER *, tHeaderInfo *);
};

typedef struct NatAppDefn tNatAppDefn;


/* Client information */
typedef struct  CuSeeMeAddr
{
    INT1                ai1Reserved[4];
    UINT4               u4SrcAddr;
    UINT4               u4DestAddr;
} tCuSeeMeAddrInfo;

/* This structure defines the IPSec header Information */
typedef struct IPSecHeaderInfo  {

 UINT4 u4InIpAddr; /* Inside IP address */
 UINT4 u4OutIpAddr; /* Outside IP address */
 UINT4 u4SPI;   /* Contains SPI value of the packet */
 UINT4 u4Direction; /* Direction of the packet */
 UINT4 u4IfNum; /* The interface number on which the packet has come
                        or going depending upon the direction. */
}tIPSecHeaderInfo;

/* This structure defines the IPSec Inbound hash node.  */
typedef struct IPSecInHash  {
  tTMO_SLL_NODE     IPSecInHash;  /* Pointer to the next IPSec InHash Node */
  UINT4                  u4LocIpAddr;  /* Local IP address */
  UINT4                  u4OutIpAddr;  /* Outside IP address */
  UINT4                  u4TranslatedIpAddr; /* Translated IP address */
  UINT4                  u4SPIOutside;  /* SPI of the outside host */
}tIPSecInHashNode;

/* This structure defines the IPSec Outbound hash node. */
typedef struct IPSecOutHash  {
  tTMO_SLL_NODE    IPSecOutHash; /* Pointer to the next IPSec out hash node */
  UINT4                 u4LocIpAddr; /* Local IP address */
  UINT4                 u4OutIpAddr; /* Outside IP address */
  UINT4                 u4SPIInside; /* SPI value */
  UINT4                 u4IfNum;         /* Interface number */
  UINT4                 u4TranslatedIpAddr; /* Translated IP address */
  UINT4                 u4TimeStamp; /* Time Stamp Information for the session */
  tIPSecInHashNode *pIPSecInHashNode; /* Pointer to the IPSec Inbound hash node */

}tIPSecOutHashNode;

/* This structure defines the IPSec Pending hash node. */
typedef struct IPSecPendHash  {
  tTMO_SLL_NODE  IPSecPendHash;  /* Pointer to the next IPSec pending hash node */
  UINT4               u4LocIpAddr;           /* Local IP address */
  UINT4               u4OutIpAddr;         /* Outside IP address */
  UINT4               u4TranslatedIpAddr;  /* Translated IP address */
  UINT4               u4SPIInside;         /* SPI from the host behind NAT */
  UINT4               u4SPIOutside;         /* SPI from the host after NAT */
  UINT4               u4IfNum;                /* Interface Number */
  UINT4               u4TimeStamp;         /* Time Stamp Information for the session */
  UINT2               u2NoOfRetry;         /* Number of retries */
  UINT2               u2Reserved;                /* Reserved */
}tIPSecPendHashNode;

/* This data structure stores the IPSec Session information */
typedef struct IPSecList  {
  tTMO_SLL_NODE IPSecList;     /* Pointer to the next IPSec List node */
  tIPSecOutHashNode *pIPSecOutHashNode;  /* Pointer to the IPSec outbound hash node */
}tIPSecListNode;

/* This data structure stores the pending IPSec Session information */
typedef struct IPSecPendList  {
  tTMO_SLL_NODE IPSecPendList;    /* Pointer to the next pending IPSec List node */
  tIPSecPendHashNode *pIPSecPendHashNode;   /* Pointer to the IPSec pending hash node */
}tIPSecPendListNode;
/* This structure defines the IKE header Information */
typedef struct IKEHeaderInfo  {
  UINT4 u4InIpAddr;    /* Inside IP address */
  UINT4 u4OutIpAddr;   /* Outside IP address */
  UINT2 u2InPort;
  UINT2 u2OutPort;
  UINT4 u4IfNum;       /* The interface number on which the packet has come 
                          or going depending
                          upon the direction.*/
  UINT4 u4Direction;   /* Direction of the packet */
  /* RFC -6330 -START*/
  UINT1 au1InitCookie[NAT_IKE_COOKIE_LENGTH]; /* Initiator cookie of 
                                                 the session */
  /* RFC -6330 -END*/
}tIKEHeaderInfo;

/* This structure defines the IKE hash node information */
typedef struct IKEHash  {

  tTMO_SLL_NODE  IKEHash; /* Pointer to the next IKE hash node */
  UINT4               u4LocIpAddr;       /* Local IP address */
  UINT4               u4OutIpAddr;       /* Outside IP address */
  UINT4               u4TranslatedIpAddr; /* Translated IP address */
  UINT4               u4IfNum;       /* Interface number for the session */
  UINT4               u4TimeStamp;       /* Time Stamp Information for the session */
  UINT1               au1InitCookie[NAT_IKE_COOKIE_LENGTH];     /* Contains the initiator cookie */
}tIKEHashNode;

/* This data structure stores the IKE session hash nodes. */
typedef struct IKEList  {
  tTMO_SLL_NODE   IKEList;  /* Pointer to the next IKE List node */
  tIKEHashNode  *pIKEHashNode;         /* Pointer to the IKE hash node */
}tIKEListNode;


typedef struct _tNatSipInfo
{
    UINT4 u4LocalIp;
    UINT4 u4TransIp;
    UINT4 u4RemoteIp;
    UINT2 u2LocalPort;
    UINT2 u2TransPort;
    UINT2 u2RemotePort;
    UINT1 u1Protocol;
    UINT1 u1Direction;
    INT4  i4IfNum;
    UINT1 u1CallHold;
    UINT1 u1Padding;
    UINT1 au1Reserved[2]; /* Reserved variable. Currently not in use. */
} tNatSipInfo;

typedef struct _tSipSignalPort{
    UINT2       u2TcpListenPort;
    UINT2       u2UdpListenPort;
    UINT2       u2TlsListenPort;
    UINT2       u2Padding;
}tSipSignalPort;


typedef struct {
       tTmrAppTimer    timerNode;
       INT2            i2Status;
       UINT1           u1TimerId;
       UINT1           u1Rsvd;
} tNatTimer;

/* Type defnitions for Fragmentation feature */
typedef struct 
{
    UINT4         u4Src;            /* These fields uniquely identify a */
    UINT4         u4Dest;           /* datagram */
    UINT1         u1Proto;
    UINT1         u1Direction;      /* Byte for Direction */
    UINT2         u2NoOfFrgs;       /* Total no fragments allowed  */
    UINT2         u2Id;
    UINT2         u2Len;            /* Entire datagram length, if known */
    tNatTimer     FrgReasmTimer;    /* To implement reassembly timeout */
    tTMO_DLL      FragList;         /* List of fragments collected so far */
}tNatFragmentStream;



typedef struct
{
    tTMO_DLL_NODE          Link;

    tCRU_BUF_CHAIN_HEADER *pBuf;           /* Data buffer associated with this
                                            * fragment
                                            */
    UINT2                 u2StartOffset;  /* Begining and end offsets of this
                                            * fragment
                                            */
    UINT2                 u2EndOffset;
} tNatIpFrag;


#ifdef SDF_PASSTHROUGH
typedef struct Sdf_ty_logPassThruDelay 
{
    INT4 i4PeakSec;
    INT4 i4PeakUsec;
    DBL8 AvgSec;
    DBL8 AvgUsec;
    INT2 i2NumInvPeak;
    INT2 i2NumInvite;
    INT2 ai2Band[16];
}Sdf_st_logPassthruDelay;
#endif

#ifdef SDF_TIMESTAMP
typedef struct Sdf_ty_logTimeStamp
{
    UINT1 *stringData;
    struct timeval tLogTime;
}Sdf_st_logTimeStamp;
#endif

/* The following data structure corresponds to the NatPolicyEntry in fsnat.mib
 * The policy NAT table is realized using Singly Linked List. 
 * For each policy ID and Filter ID combination, a SLL node will be maintained. 
 * The node will be indexed by the policy ID and filter ID.
 */
typedef struct NatPolicyNode{
    tTMO_SLL_NODE  NatPolicyNode; /* pointer to the next node in SLL. */
    INT4           i4PolicyId;    /* Free flow identifier for Policy Table. 
                                   * Range (1 to 65535).
                                   */
    UINT1          au1AclName[NAT_MAX_ACL_NAME_LEN];    
                                  /* Filter Identifier. This corresponds to the
                                   * implementation of fwlAclAclName mib object
                                   * in firewall acl. Range (0 to 35)
                                   */
    UINT4          u4TranslatedLocIpAddr; /* Translated Local IP Address used 
                                             by the local host.*/
    INT4           i4RowStatus;           /* Row status identifier. 
                                           * Possible values are 
                                           * CREATE_AND_WAIT, CREATE_AND_GO, 
                                           * ACTIVE, DESTROY, NOT_IN_SERVICE.
                                           */
                                        
}tNatPolicyNode;

#endif /* _NAT_TDFS_H */

