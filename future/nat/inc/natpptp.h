/********************************************************************
 *   Copyright (C) Future Sotware,1997-98,2001
 *  
 *  $Id: natpptp.h,v 1.2 2011/06/29 11:50:58 siva Exp $
 * 
 * Description:This file contains the data structure typedefs
 *             used by FutureNAT
 *
 ********************************************************************/
/**** Type Definition for Dynamic Table *****/

#ifndef _NAT_PPTP_H
#define _NAT_PPTP_H

#ifndef PACK_REQUIRED 
#pragma pack(1) 
#endif 

/* defining data structures  to be used by PPTP-ALG */
typedef struct _PptpControlPkt{
UINT2 u2Length ;
UINT2 u2PptpMessageType ;
UINT4 u4MagicCookie ;
UINT2 u2CntrlMsgType ;
UINT2 u2Reserved ;
}tPptpCntrlPkt ;



typedef struct _PptpDataPkt {
UINT2 u2FlagsVer ;
UINT2 u2ProtoType ;
UINT2 u2KeyLen ;
UINT2 u2KeyCallId ;
}tPptpDataPkt ;
#ifndef PACK_REQUIRED 
#pragma pack() 
#endif  


/* prototype definition of the functions defined in PPTP-ALG and called by NAT*/
PUBLIC INT4 NatProcessPPTPCntrlPkt ARG_LIST (( tCRU_BUF_CHAIN_HEADER * pBuf,
tHeaderInfo * pHeaderInfo));

PUBLIC INT4 NatProcessPPTPDataPkt ARG_LIST (( tCRU_BUF_CHAIN_HEADER * pBuf,
tHeaderInfo * pHeaderInfo));


/*Constants to be used in PPTP ALG */
#define NAT_PPTP                         47
#define NAT_PPTP_PORT                  1723
#define PPTP_MAGIC_COOKIE        0x1A2B3C4D
#define NAT_PPTP_MIN_LEN                 16
#define NAT_GRE_MIN_LEN                   8

/* Standard values of different control packet types */
#define PPTP_OUT_CALL_REQUEST             7
#define PPTP_IN_CALL_REQUEST              9
#define PPTP_OUT_CALL_REPLY               8
#define PPTP_IN_CALL_REPLY               10 
#define PPTP_CALL_DISC_NOTIFY            13
#define PPTP_CALL_SET_LINK_INFO          15
#define PPTP_STOP_CONTROL_REQUEST        3


/* Fixed values of the Call Id Offsets and their field lengths */
#define NAT_PPTP_CALLID_OFFSET           12
#define NAT_GRE_CALLID_OFFSET             6
#define NAT_PPTP_PEERCALLID_OFFSET       14
#define NAT_PPTP_CALLID_LEN               2

/* GRE header field masks*/
#define GRE_FLAG_VAL                 0x0000
#define GRE_VER_VAL                  0x0001
#define GRE_PPTP_PROTO_TYPE          0x880b

/* Macros to Check the GRE ( PPTP data header ) header fields */
#define PPTPDATAHDR_INVALID_FLAGS(x)   ((x & 0x0078) !=0)
#define PPTPDATAHDR_INVALID_VER(x)     ((x & 0x0007) != 0x0001)
#define PPTPDATAHDR_INVALID_PROTO(x)   ((x != 0x880B)
#define PPTPDATA_HDR_SEQ_PRESENT(x)    ((x & 0x1000) != 0)
#define PPTPDATA_HDR_ACK_PRESENT(x)    ((x & 0x0080) != 0)

#endif
