/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: natdebug.h,v 1.5 2014/01/25 13:52:23 siva Exp $
 *
 * Description:This file contains the debug macros used in
 *             all the modules.
 *
 *******************************************************************/
#ifndef _NATDEBUG_H_
#define _NATDEBUG_H_ 

#if defined TRACE_WANTED 
#define   NAT_TRC_FLAG    gu4NatTrc

#define NAT_TRC(Value, Fmt)   UtlTrcLog(NAT_TRC_FLAG, \
        Value,\
        "NAT",\
        Fmt)
#define NAT_TRC1(Value, Fmt, Arg)     UtlTrcLog(NAT_TRC_FLAG, \
        Value,\
        "NAT",\
        Fmt,  \
        Arg)
#define NAT_TRC2(Value, Fmt, Arg1, Arg2)      \
        UtlTrcLog(NAT_TRC_FLAG, \
        Value,\
        "NAT",\
        Fmt,  \
        Arg1, Arg2)
#define NAT_TRC3(Value, Fmt, Arg1, Arg2, Arg3)\
        UtlTrcLog(NAT_TRC_FLAG, \
        Value,\
        "NAT",\
        Fmt,  \
        Arg1, Arg2, Arg3)
#define NAT_TRC4(Value, Fmt, Arg1, Arg2, Arg3, Arg4)  \
        UtlTrcLog(NAT_TRC_FLAG, \
        Value,\
        "NAT",\
        Fmt,   \
        Arg1, Arg2,   \
        Arg3, Arg4)
#define NAT_TRC5(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)    \
        UtlTrcLog(NAT_TRC_FLAG, \
        Value, \
        "NAT", \
        Fmt,   \
        Arg1, Arg2, Arg3, \
        Arg4, Arg5)
#define NAT_TRC6(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)  \
        UtlTrcLog(NAT_TRC_FLAG, \
        Value, \
        "NAT", \
        Fmt,   \
        Arg1, Arg2, Arg3, \
        Arg4, Arg5, Arg6)

#define NAT_TRC_COOKIE_DISP(Value, Fmt, Arg)     UtlTrcLog(NAT_TRC_FLAG, \
        Value,\
        "",\
        Fmt,  \
        Arg)

#else
#define NAT_TRC(Value, Fmt)
#define NAT_TRC1(Value, Fmt, Arg)
#define NAT_TRC2(Value, Fmt, Arg1, Arg2)
#define NAT_TRC3(Value, Fmt, Arg1, Arg2, Arg3)
#define NAT_TRC4(Value, Fmt, Arg1, Arg2, Arg3, Arg4)
#define NAT_TRC5(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)
#define NAT_TRC6(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)
#define NAT_TRC_COOKIE_DISP(Value, Fmt, Arg)
#endif /* TRC_NAT */

#ifdef DEBUG_NAT
#define NAT_DBG_FLAG gu4NatTrc

#define NAT_DBG(Value, Fmt)    UtlTrcLog(NAT_DBG_FLAG, \
        Value, \
        "NAT", \
        Fmt)
#define NAT_DBG1(Value, Fmt, Arg)     UtlTrcLog(NAT_DBG_FLAG, \
        Value, \
        "NAT", \
        Fmt,   \
        Arg)
#define NAT_DBG2(Value, Fmt, Arg1, Arg2)        \
        UtlTrcLog(NAT_DBG_FLAG, \
        Value, \
        "NAT", \
        Fmt,   \
        Arg1, Arg2)
#define NAT_DBG3(Value, Fmt, Arg1, Arg2, Arg3)  \
        UtlTrcLog(NAT_DBG_FLAG, \
        Value, \
        "NAT", \
        Fmt,   \
        Arg1, Arg2, Arg3)
#define NAT_DBG4(Value, Fmt, Arg1, Arg2, Arg3, Arg4)   \
        UtlTrcLog(NAT_DBG_FLAG, \
        Value, \
        "NAT", \
        Fmt,   \
        Arg1, Arg2,   \
        Arg3, Arg4)
#define NAT_DBG5(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)    \
        UtlTrcLog(NAT_DBG_FLAG, \
        Value, \
        "NAT", \
        Fmt,   \
        Arg1, Arg2, Arg3, \
        Arg4, Arg5)
#define NAT_DBG6(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)  \
        UtlTrcLog(NAT_DBG_FLAG, \
        Value, \
        "NAT", \
        Fmt,   \
        Arg1, Arg2, Arg3, \
        Arg4, Arg5, Arg6)

#define NAT_DBG_COOKIE_DISP(Value, Fmt, Arg)     UtlTrcLog(NAT_DBG_FLAG, \
        Value, \
        "", \
        Fmt,   \
        Arg)
#else
#define NAT_DBG(Value, Fmt)
#define NAT_DBG1(Value, Fmt, Arg)
#define NAT_DBG2(Value, Fmt, Arg1, Arg2)
#define NAT_DBG3(Value, Fmt, Arg1, Arg2, Arg3)
#define NAT_DBG4(Value, Fmt, Arg1, Arg2, Arg3, Arg4)
#define NAT_DBG5(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)
#define NAT_DBG6(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)
#define NAT_DBG_COOKIE_DISP(Value, Fmt, Arg)
#endif /* DEBUG_NAT */


/* Debug categories */
#define   NAT_DBG_OFF           UTL_DBG_OFF         
#define   NAT_DBG_ALL           UTL_DBG_ALL         
#define   NAT_DBG_INIT_SHUTDN   UTL_DBG_INIT_SHUTDN 
#define   NAT_DBG_CTRL_IF       UTL_DBG_CTRL_IF     
#define   NAT_DBG_STATUS_IF     UTL_DBG_STATUS_IF   
#define   NAT_DBG_SNMP_IF       UTL_DBG_SNMP_IF     
#define   NAT_DBG_TMR_IF        UTL_DBG_TMR_IF      
#define   NAT_DBG_BUF_IF        UTL_DBG_BUF_IF      
#define   NAT_DBG_MEM_IF        UTL_DBG_MEM_IF      
#define   NAT_DBG_RX            UTL_DBG_RX          
#define   NAT_DBG_TX            UTL_DBG_TX          

#define   NAT_DBG_ENTRY       0x00010000
#define   NAT_DBG_EXIT        0x00020000
#define   NAT_DBG_INTMD       0x00040000
#define   NAT_DBG_ABNX        0x00080000
#define   NAT_DBG_PKT_FLOW    0x00100000
#define   NAT_DBG_TABLE       0x00200000
#define   NAT_DBG_DNS         0x00400000
#define   NAT_DBG_FTP         0x00800000
#define   NAT_DBG_HTTP        0x01000000
#define   NAT_DBG_SMTP        0x02000000
#define   NAT_DBG_ICMP        0x04000000
#define   NAT_DBG_MEM         0x08000000
#define   NAT_DBG_LLR         0x10000000
#define   NAT_DBG_PPTP        0x20000000

#define   NAT_TRC_ON          0x00010000
INT4
NatSetDebugLevel PROTO ((UINT4));
INT4
NatResetDebugLevel PROTO ((UINT4));

#endif
