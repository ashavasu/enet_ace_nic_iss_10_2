/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: natmemac.h,v 1.12 2013/10/25 10:50:07 siva Exp $
 *
 * Description:This file contains the macros and prototypes of
 *             SMTP module.
 *
 *******************************************************************/
#ifndef NAT_MEMAC_H
#define NAT_MEMAC_H

/**** Type Definition for Memory Pool Ids *****/
typedef struct NatMemPoolIds {
    tMemPoolId  DynamicPoolId;
    tMemPoolId  InterfaceInfoPoolId;
    tMemPoolId  LocalAddrListPoolId;
    tMemPoolId  StaticListPoolId;
    tMemPoolId  StaticNaptListPoolId;
    tMemPoolId  LocOutHashNodePoolId;
    tMemPoolId  GlobalHashNodePoolId;
    tMemPoolId  DnsListPoolId;
    tMemPoolId  IidOidArrDynNodePoolId;
    tMemPoolId  IidListPoolId;
    tMemPoolId  GlobalAddrListPoolId;
    tMemPoolId  NatFreeGlobalListPoolId;
    tMemPoolId  TcpDelStackPoolId;
    tMemPoolId  FreePortPoolId;
    tMemPoolId  NatPartialLinksListPoolId;
    tMemPoolId  NatOnlyListPoolId;
    tMemPoolId  NatFragFragListPoolId;
    tMemPoolId  NatFragFragsPoolId;
    tMemPoolId  IPSecInHashNodePoolId;
    tMemPoolId  IPSecOutHashNodePoolId;
    tMemPoolId  IPSecPendHashNodePoolId;
    tMemPoolId  IPSecListPoolId;
    tMemPoolId  IPSecPendListPoolId;
    tMemPoolId IKEHashNodePoolId;
    tMemPoolId  IKEListPoolId;
    tMemPoolId  NatSipWanUaHashId;
    tMemPoolId NatSipAlgTaskletId;

}tNatMemPoolIds;


#define   NAT_DYNAMIC_POOL_ID          \
              gaNATMemPoolIds [MAX_NAT_DYNAMIC_ENTRIES_SIZING_ID]
#define   NAT_HASH_POOL_ID             \
              gaNATMemPoolIds [MAX_NAT_LOC_OUT_HASH_NODE_SIZING_ID]
#define   NAT_GLOBAL_HASH_POOL_ID      \
              gaNATMemPoolIds [MAX_NAT_GBL_HASH_NODE_SIZING_ID]
#define   NAT_INTERFACE_INFO_POOL_ID   \
              gaNATMemPoolIds [MAX_NAT_NUM_IF_SIZING_ID]
#define   NAT_STATIC_POOL_ID           \
              gaNATMemPoolIds [MAX_NAT_STATIC_BLOCKS_SIZING_ID]
#define   NAT_STATIC_NAPT_POOL_ID      \
              gaNATMemPoolIds [MAX_NAT_STATIC_NAPT_BLOCKS_SIZING_ID]
#define   NAT_LOCAL_ADDR_POOL_ID       \
              gaNATMemPoolIds [MAX_NAT_LOCAL_ADDR_TABLE_BLOCKS_SIZING_ID]
#define   NAT_DNS_LIST_POOL_ID         \
              gaNATMemPoolIds [MAX_NAT_DNS_TABLE_BLOCKS_SIZING_ID]
#define   NAT_ARR_DYNAMIC_NODE_POOL_ID \
              gaNATMemPoolIds [MAX_NAT_LID_OID_ARR_DYN_LIST_NODE_SIZING_ID]
#define   NAT_IID_LIST_POOL_ID         \
              gaNATMemPoolIds [MAX_NAT_LID_LIST_NODE_SIZING_ID]
#define   NAT_FREE_GLOBAL_LIST_POOL_ID \
              gaNATMemPoolIds [MAX_NAT_FREE_GIP_LIST_NODE_SIZING_ID]
#define   NAT_GLOBAL_ADDR_POOL_ID      \
              gaNATMemPoolIds [MAX_NAT_GLOBAL_ADDR_TABLE_BLOCKS_SIZING_ID]
#define   NAT_TCP_STACK_POOL_ID        \
              gaNATMemPoolIds [MAX_NAT_TCP_DEL_NODE_SIZING_ID]
#define   NAT_FREE_PORT_POOL_ID        \
              gaNATMemPoolIds [MAX_NAT_FREE_PORT_NODE_SIZING_ID]
#define   NAT_PARTIAL_LINKS_LIST_POOL_ID \
              gaNATMemPoolIds [MAX_NAT_PARTIAL_LINKS_LIST_BLOCKS_SIZING_ID]
#define   NAT_ONLY_POOL_ID             \
              gaNATMemPoolIds [MAX_NAT_ONLY_LIST_BLOCKS_SIZING_ID]
#define   NAT_FRAG_FRAG_LIST_POOL_ID   \
              gaNATMemPoolIds [MAX_NAT_NO_OF_FRAGMENT_STREAM_SIZING_ID]
#define   NAT_FRAG_FRAGS_POOL_ID       \
             gaNATMemPoolIds [MAX_NAT_FRAGMENTS_SIZE_SIZING_ID]
#define   NAT_LINEAR_BUF_POOL_ID       \
             gaNATMemPoolIds [MAX_NAT_LINEAR_BUF_SIZING_ID]
#define   NAT_POLICY_LIST_POOL_ID       \
             gaNATMemPoolIds [MAX_NAT_POLICY_NODE_SIZING_ID]
#define   NAT_UDP_POOL_ID                NAT_ARR_DYNAMIC_NODE_POOL_ID
#define   NAT_NFS_IP_LIST_POOL_ID        NAT_DNS_LIST_POOL_ID


#define NAT_IPSEC_IN_HASH_POOL_ID      \
             gaNATMemPoolIds [MAX_NAT_IPSEC_IN_HASH_NODE_SIZING_ID]
#define NAT_IPSEC_OUT_HASH_POOL_ID     \
             gaNATMemPoolIds [MAX_NAT_IPSEC_OUT_HASH_NODE_SIZING_ID]
#define NAT_IPSEC_PEND_HASH_POOL_ID    \
             gaNATMemPoolIds [MAX_NAT_IPSEC_PEND_HASH_NODE_SIZING_ID]
#define NAT_IPSEC_LIST_POOL_ID         \
             gaNATMemPoolIds [MAX_NAT_IPSEC_LIST_NODE_SIZING_ID]
#define NAT_IPSEC_PEND_LIST_POOL_ID    \
             gaNATMemPoolIds [MAX_NAT_IPSEC_PEND_LIST_NODE_SIZING_ID]
#define NAT_IKE_HASH_POOL_ID           \
             gaNATMemPoolIds [MAX_NAT_IKE_HASH_NODE_SIZING_ID]
#define NAT_IKE_LIST_POOL_ID           \
             gaNATMemPoolIds [MAX_NAT_IKE_LIST_NODE_SIZING_ID]
#define NAT_SIP_HASH_POOL_ID           \
             gaNATMemPoolIds [NAT_SIP_HASH_NODE_SIZING_ID]
#define NAT_SIP_POOL_ID           \
             gaNATMemPoolIds [NAT_SIP_NODE_SIZING_ID]
#define NAT_SIPALG_TASKLET_POOL_ID           \
             gaNATMemPoolIds [NAT_SIPALG_TASKLET_NODE_SIZING_ID]


#define NAT_MAX_LINEAR_BUF_SIZE                  300

#define NAT_MEM_ALLOCATE(PoolId,pu1Block,type) \
            (pu1Block = (type *)(MemAllocMemBlk ((tMemPoolId)PoolId)))
#endif /* NAT_MEMAC_H */
