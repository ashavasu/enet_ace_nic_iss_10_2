/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: natsiptimer.h,v 1.2 2011/06/14 11:35:58 siva Exp $
 *
 * Description:This file contains the NAT SIPALG macros.
 *
 ********************************************************************/

#ifndef _NAT_SIPTIMER_H
#define _NAT_SIPTIMER_H

#include "natinc.h"
#include "sipalg_inc.h"

extern tTimerListId        NatSipAlgTimerListId;


#define  SIP_ALG_PARTIAL_ENTRY_TIMER               300

PUBLIC INT4 NatSipAlgInitTimer (VOID);

PUBLIC INT4 NatSipAlgDeInitTimer (VOID);

INT4 NatSipAlgStartTimer (tNatSipAlgTimer *pTimer, UINT4 u4TimeOut);

INT4 NatSipAlgStopTimer (tNatSipAlgTimer *pTimer);

INT4 NatSipAlgReStartTimer (tNatSipAlgTimer *pTimer, UINT4 u4TimeOut);

VOID NatSipAlgTmrExpiryCallBkFun (UINT4 u4Arg);
#endif
