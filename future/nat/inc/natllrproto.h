/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: natllrproto.h,v 1.8 2011/06/14 11:35:58 siva Exp $
 *
 * Description: Prototypes for NAT low level routines
 *
 *******************************************************************/
/* Function Prototypes for Global Address Table */

PUBLIC INT1 NatGatActive(INT4 , UINT4 );

PUBLIC INT1 NatGatCreateAndGo (INT4 , UINT4 );

PUBLIC INT1 NatGatCreateAndWait (INT4 , UINT4 );

PUBLIC INT1 NatGatNotInService (INT4 , UINT4 );

PUBLIC INT1 NatGatDestroy (INT4 , UINT4);


/* Function Prototypes for Interface Table */

PUBLIC INT1 NatIftActive(INT4 );

PUBLIC INT1 NatIftCreateAndGo (INT4 );

PUBLIC INT1 NatIftCreateAndWait (INT4 );

PUBLIC INT1 NatIftNotInService (INT4 );

PUBLIC INT1 NatIftDestroy (INT4 );


/* Function Prototypes for Local Address Table */

PUBLIC INT1 NatLatActive(INT4 , UINT4 );

PUBLIC INT1 NatLatCreateAndGo (INT4 , UINT4 );

PUBLIC INT1 NatLatCreateAndWait (INT4 , UINT4 );

PUBLIC INT1 NatLatNotInService (INT4 , UINT4 );

PUBLIC INT1 NatLatDestroy (INT4 , UINT4 );


/* Function Prototypes for Static Address Table */

PUBLIC INT1 NatSttActive(INT4 , UINT4 );

PUBLIC INT1 NatSttCreateAndGo (INT4 , UINT4 );

PUBLIC INT1 NatSttCreateAndWait (INT4 , UINT4 );

PUBLIC INT1 NatSttNotInService (INT4 , UINT4 );

PUBLIC INT1 NatSttDestroy (INT4 , UINT4 );

/* Function Prototypes for Static Napt Table */


PUBLIC INT1 NatSttNaptActive(INT4 , UINT4 , UINT2, UINT2, UINT2);

PUBLIC INT1 NatSttNaptCreateAndGo (INT4 , UINT4 , UINT2, UINT2, UINT2);

PUBLIC INT1 NatSttNaptCreateAndWait (INT4 , UINT4 , UINT2, UINT2, UINT2);

PUBLIC INT1 NatSttNaptNotInService (INT4 , UINT4 , UINT2, UINT2, UINT2);

PUBLIC INT1 NatSttNaptDestroy (INT4 , UINT4 , UINT2, UINT2, UINT2);

/* To Update the Current Ip addr and End of Ip Addr Range in the
 * Interface Table
 */
PUBLIC INT1 NatUpdateMask (INT4, UINT4, UINT4);

/* natutils.c */
INT4
NatUtilTestUniqueExtPortProtoComb PROTO ((UINT4, UINT4, INT4, 
                                          INT4, INT4, INT4));
INT4
NatUtilAddPartialList             PROTO ((UINT4, UINT4, UINT2, UINT2,UINT4,
                                          INT4 *, UINT2, UINT2, INT4, UINT1));
INT4
NatUtilDelPartialList             PROTO ((UINT4, UINT2, UINT4, UINT2, UINT4,
                                          UINT2, UINT1, UINT1, INT4, UINT1));
INT4
NatUtilScanPartialLinksList       PROTO ((tNatSipInfo*, tNatPartialLinkNode**,
                                          tNatPartialLinkNode**));
UINT4
NatUtilAddToPartialList           PROTO ((tNatSipInfo*, UINT1));
INT4
NatUtilUpdateCallHoldInfo         PROTO ((tNatSipInfo*, tNatPartialLinkNode*,
                                          tNatPartialLinkNode*));
INT4
NatUtilVerifyInTuple              PROTO ((tNatSipInfo*, tNatPartialLinkNode*));
INT4
NatUtilVerifyOutTuple             PROTO ((tNatSipInfo*, tNatPartialLinkNode*));
VOID
NatSipUpdateInterfaceStatus PROTO((UINT2 , UINT4 , UINT1 ,
                          UINT1 *, UINT4 ));

tInterfaceInfo* 
NatAllocateMemory                 PROTO ((INT4));

/* Prototypes for Fragmentation and reassembly logic in NAT */

PUBLIC VOID NatTmrInitTimerDesc (VOID);
PUBLIC UINT4 NatTmrSetTimer (tNatTimer * pTimer, UINT1 u1TimerId, INT4 i4NrTicks);

/* Prototypes for Policy Based NAT */


VOID NatPolicyInitLists PROTO((VOID));
VOID NatPolicyDeInitList PROTO((VOID));
INT4 NatPolicyGetEntry PROTO((INT4 i4PolicyId, UINT1 *pAclName));
INT4 NatPolicyIsValid PROTO((INT4 i4PolicyId, INT4 i4PolicyType, 
                             UINT1 *pAclName));
INT4 NatPolicyValidateFilter PROTO((INT4 i4PolicyType,UINT1 *pAclName));
INT4 NatPolicyEntryDelete PROTO((INT4 i4PolicyType,INT4 i4PolicyId, 
                                 UINT1 *pAclName));
INT4 NatPolicyEntryUpdate PROTO((INT4 i4PolicyType, INT4 i4PolicyId,
                                 UINT1 *pAclName, INT4 i4RowStatus));
INT4 NatPolicyEntryCreate PROTO((INT4 i4PolicyType, INT4 i4PolicyId,
                                 UINT1 *pAclName,INT4 i4RowStatus));
INT4 NatPolicyGetStaticEntry PROTO((UINT4 *,UINT4 ,UINT4 ,UINT4, UINT4));
INT4 NatPolicyGetDynamicEntry PROTO((UINT4*,UINT4 ,UINT4 ,UINT4));
INT1 NatPolicySearchStaticTable PROTO((UINT4 ));
INT1 NatPolicySearchDynamicTable PROTO((UINT4 ));





#ifdef SDF_TIMESTAMP
VOID Sdf_fn_passThruPrintToFile (VOID);
#endif

#ifdef SDF_PASSTHROUGH
VOID Sdf_fn_writeToFile (VOID);
INT4 printTimeStamp (UINT1  *pPrintString);
#endif
