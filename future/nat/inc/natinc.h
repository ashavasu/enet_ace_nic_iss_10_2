/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: natinc.h,v 1.17 2017/12/13 10:47:21 siva Exp $
 *
 * Description: This file contains the common includes 
 *
 *******************************************************************/
#ifndef _NATINC_H
#define _NATINC_H 
#include "lr.h"
#include "iss.h"
#include "cfa.h"
#include "ip.h"
#include "rtm.h"
#include "natdefn.h"
#include "nat.h"
#include "msr.h"
#include "natport.h"
#include "natmemac.h"
#include "nattdfs.h"
#include "natextn.h"
#include "natdebug.h"
#include "natllrext.h"
#include "natllrproto.h"

#include "fsnatwr.h"
#include "natpptp.h"
#include "natcli.h"
#include "snmcdefn.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "fsvlan.h"
#include "fssnmp.h"
#include "natsz.h"
#include "sipalg.h"

/* added for supporting fragmentation */
#include "natfragext.h"

#include "fsike.h"
#ifdef IPSEC_WANTED
#include "secv4.h"
#endif

#ifdef NPAPI_WANTED 
#include "ipnp.h" 
#include "natnpwr.h" 
#include "secnp.h"
#endif


#ifdef FIREWALL_WANTED
#include "firewall.h"
#endif

#ifdef UPNP_WANTED

#include "ithread.h"
#include "ixml.h"

#endif /* End Of UPNP_WANTED */
#include "secmod.h"
#if !defined (SECURITY_KERNEL_WANTED) && defined (SNMP_WANTED)
extern INT4      gi4MibResStatus;
#endif
#define nmhGetSysUpTime(pValue) OsixGetSysTime(pValue)

#if (defined(SDF_TIMESTAMP) || defined(SDF_PASSTHROUGH))
#include <sys/time.h>
#endif
#define nmhGetSysUpTime(pValue) OsixGetSysTime(pValue)
#define NAT_TYPICAL_NUM_ENTRIES           9000
#define   NAT_DNS_TABLE_BLOCKS            100
#define   NAT_PARTIAL_LINKS_LIST_BLOCKS   2000
#define   NAT_TCP_STACK_BLOCKS             20
#define   NAT_ONLY_LIST_BLOCKS            100
#define   NAT_LOCAL_ADDR_TABLE_BLOCKS      10
#define   NAT_STATIC_BLOCKS               100
#define   NAT_STATIC_NAPT_BLOCKS           60
#define   NAT_GLOBAL_ADDR_TABLE_BLOCKS    100
#define   VLAN_DEF_VLAN_ID                 1
#define   IP_MF                   0x2000
#define IPC_LINK_VPN_TYPE                 3
#define NAT_BASE_PORT_PARITY(u2Port)  (((u2Port & 0x01) == 0) ? PARITY_EVEN : PARITY_ODD)

/*Added to delete the dynamic NAT entries if interface status is down*/
#define NAT_IFOPER_STAUS_DOWN           2
#endif
