
# ifndef fsnatOGP_H
# define fsnatOGP_H

 /* The Definitions of the OGP Index Constants.  */

# define SNMP_OGP_INDEX_NATSTATINFO                                  (0)
# define SNMP_OGP_INDEX_NATDYNAMICTRANSTABLE                         (1)
# define SNMP_OGP_INDEX_NATGLOBALADDRESSTABLE                        (2)
# define SNMP_OGP_INDEX_NATLOCALADDRESSTABLE                         (3)
# define SNMP_OGP_INDEX_NATSTATICTABLE                               (4)
# define SNMP_OGP_INDEX_NATSTATICNAPTTABLE                           (5)
# define SNMP_OGP_INDEX_NATIFTABLE                                   (6)

#endif /*  fsnatOGP_H  */
