/********************************************************************
* Copyright (C) Future Software Limited,2003
*
* $Id: fsnatlw.h,v 1.5 2011/05/16 07:05:31 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetNatEnable ARG_LIST((INT4 *));

INT1
nmhGetNatTypicalNumberOfEntries ARG_LIST((INT4 *));

INT1
nmhGetNatTranslatedLocalPortStart ARG_LIST((INT4 *));

INT1
nmhGetNatIdleTimeOut ARG_LIST((INT4 *));

INT1
nmhGetNatTcpTimeOut ARG_LIST((INT4 *));

INT1
nmhGetNatUdpTimeOut ARG_LIST((INT4 *));

INT1
nmhGetNatTrcFlag ARG_LIST((INT4 *));

INT1
nmhGetNatStatDynamicAllocFailureCount ARG_LIST((UINT4 *));

INT1
nmhGetNatStatTotalNumberOfTranslations ARG_LIST((UINT4 *));

INT1
nmhGetNatStatTotalNumberOfActiveSessions ARG_LIST((UINT4 *));

INT1
nmhGetNatStatTotalNumberOfPktsDropped ARG_LIST((UINT4 *));

INT1
nmhGetNatStatTotalNumberOfSessionsClosed ARG_LIST((UINT4 *));

INT1
nmhGetNatIKEPortTranslation ARG_LIST((INT4 *));

INT1
nmhGetNatIKETimeout ARG_LIST((INT4 *));

INT1
nmhGetNatIPSecTimeout ARG_LIST((INT4 *));

INT1
nmhGetNatIPSecPendingTimeout ARG_LIST((INT4 *));

INT1
nmhGetNatIPSecMaxRetry ARG_LIST((INT4 *));

INT1
nmhGetSipAlgPort ARG_LIST((INT4 *));

INT1
nmhGetNatSipAlgPartialEntryTimeOut ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetNatEnable ARG_LIST((INT4 ));

INT1
nmhSetNatTypicalNumberOfEntries ARG_LIST((INT4 ));

INT1
nmhSetNatTranslatedLocalPortStart ARG_LIST((INT4 ));

INT1
nmhSetNatIdleTimeOut ARG_LIST((INT4 ));

INT1
nmhSetNatTcpTimeOut ARG_LIST((INT4 ));

INT1
nmhSetNatUdpTimeOut ARG_LIST((INT4 ));

INT1
nmhSetNatTrcFlag ARG_LIST((INT4 ));

INT1
nmhSetNatIKEPortTranslation ARG_LIST((INT4 ));

INT1
nmhSetNatIKETimeout ARG_LIST((INT4 ));

INT1
nmhSetNatIPSecTimeout ARG_LIST((INT4 ));

INT1
nmhSetNatIPSecPendingTimeout ARG_LIST((INT4 ));

INT1
nmhSetNatIPSecMaxRetry ARG_LIST((INT4 ));

INT1
nmhSetSipAlgPort ARG_LIST((INT4 ));

INT1
nmhSetNatSipAlgPartialEntryTimeOut ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2NatEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2NatTypicalNumberOfEntries ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2NatTranslatedLocalPortStart ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2NatIdleTimeOut ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2NatTcpTimeOut ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2NatUdpTimeOut ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2NatTrcFlag ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2NatIKEPortTranslation ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2NatIKETimeout ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2NatIPSecTimeout ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2NatIPSecPendingTimeout ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2NatIPSecMaxRetry ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2SipAlgPort ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2NatSipAlgPartialEntryTimeOut ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2NatEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2NatTypicalNumberOfEntries ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2NatTranslatedLocalPortStart ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2NatIdleTimeOut ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2NatTcpTimeOut ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2NatUdpTimeOut ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2NatTrcFlag ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2NatIKEPortTranslation ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2NatIKETimeout ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2NatIPSecTimeout ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2NatIPSecPendingTimeout ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2NatIPSecMaxRetry ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2SipAlgPort ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2NatSipAlgPartialEntryTimeOut ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for NatDynamicTransTable. */
INT1
nmhValidateIndexInstanceNatDynamicTransTable ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for NatDynamicTransTable  */

INT1
nmhGetFirstIndexNatDynamicTransTable ARG_LIST((INT4 * , UINT4 * , INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexNatDynamicTransTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetNatDynamicTransTranslatedLocalIp ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetNatDynamicTransTranslatedLocalPort ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetNatDynamicTransLastUseTime ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for NatGlobalAddressTable. */
INT1
nmhValidateIndexInstanceNatGlobalAddressTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for NatGlobalAddressTable  */

INT1
nmhGetFirstIndexNatGlobalAddressTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexNatGlobalAddressTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetNatGlobalAddressMask ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetNatGlobalAddressEntryStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetNatGlobalAddressMask ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetNatGlobalAddressEntryStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2NatGlobalAddressMask ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2NatGlobalAddressEntryStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2NatGlobalAddressTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for NatLocalAddressTable. */
INT1
nmhValidateIndexInstanceNatLocalAddressTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for NatLocalAddressTable  */

INT1
nmhGetFirstIndexNatLocalAddressTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexNatLocalAddressTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetNatLocalAddressMask ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetNatLocalAddressEntryStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetNatLocalAddressMask ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetNatLocalAddressEntryStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2NatLocalAddressMask ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2NatLocalAddressEntryStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2NatLocalAddressTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for NatStaticTable. */
INT1
nmhValidateIndexInstanceNatStaticTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for NatStaticTable  */

INT1
nmhGetFirstIndexNatStaticTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexNatStaticTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetNatStaticTranslatedLocalIp ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetNatStaticEntryStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetNatStaticTranslatedLocalIp ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetNatStaticEntryStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2NatStaticTranslatedLocalIp ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2NatStaticEntryStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2NatStaticTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for NatStaticNaptTable. */
INT1
nmhValidateIndexInstanceNatStaticNaptTable ARG_LIST((INT4  , UINT4  , INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for NatStaticNaptTable  */

INT1
nmhGetFirstIndexNatStaticNaptTable ARG_LIST((INT4 * , UINT4 * , INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexNatStaticNaptTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetNatStaticNaptTranslatedLocalIp ARG_LIST((INT4  , UINT4  , INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetNatStaticNaptTranslatedLocalPort ARG_LIST((INT4  , UINT4  , INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetNatStaticNaptDescription ARG_LIST((INT4  , UINT4  , INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetNatStaticNaptEntryStatus ARG_LIST((INT4  , UINT4  , INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetNatStaticNaptTranslatedLocalIp ARG_LIST((INT4  , UINT4  , INT4  , INT4  , INT4  ,UINT4 ));

INT1
nmhSetNatStaticNaptTranslatedLocalPort ARG_LIST((INT4  , UINT4  , INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetNatStaticNaptDescription ARG_LIST((INT4  , UINT4  , INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetNatStaticNaptEntryStatus ARG_LIST((INT4  , UINT4  , INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2NatStaticNaptTranslatedLocalIp ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2NatStaticNaptTranslatedLocalPort ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2NatStaticNaptDescription ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2NatStaticNaptEntryStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2NatStaticNaptTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for NatIfTable. */
INT1
nmhValidateIndexInstanceNatIfTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for NatIfTable  */

INT1
nmhGetFirstIndexNatIfTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexNatIfTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetNatIfNat ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetNatIfNapt ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetNatIfTwoWayNat ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetNatIfEntryStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetNatIfNat ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetNatIfNapt ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetNatIfTwoWayNat ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetNatIfEntryStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2NatIfNat ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2NatIfNapt ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2NatIfTwoWayNat ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2NatIfEntryStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2NatIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for NatIPSecSessionTable. */
INT1
nmhValidateIndexInstanceNatIPSecSessionTable ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for NatIPSecSessionTable  */

INT1
nmhGetFirstIndexNatIPSecSessionTable ARG_LIST((INT4 * , UINT4 * , UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexNatIPSecSessionTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetNatIPSecSessionTranslatedLocalIp ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetNatIPSecSessionLastUseTime ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetNatIPSecSessionEntryStatus ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetNatIPSecSessionEntryStatus ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2NatIPSecSessionEntryStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2NatIPSecSessionTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for NatIPSecPendingTable. */
INT1
nmhValidateIndexInstanceNatIPSecPendingTable ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for NatIPSecPendingTable  */

INT1
nmhGetFirstIndexNatIPSecPendingTable ARG_LIST((INT4 * , UINT4 * , UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexNatIPSecPendingTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetNatIPSecPendingTranslatedLocalIp ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetNatIPSecPendingLastUseTime ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetNatIPSecPendingNoOfRetry ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetNatIPSecPendingEntryStatus ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetNatIPSecPendingEntryStatus ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2NatIPSecPendingEntryStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2NatIPSecPendingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for NatIKESessionTable. */
INT1
nmhValidateIndexInstanceNatIKESessionTable ARG_LIST((INT4  , UINT4  , UINT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for NatIKESessionTable  */

INT1
nmhGetFirstIndexNatIKESessionTable ARG_LIST((INT4 * , UINT4 * , UINT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexNatIKESessionTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetNatIKESessionTranslatedLocalIp ARG_LIST((INT4  , UINT4  , UINT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetNatIKESessionLastUseTime ARG_LIST((INT4  , UINT4  , UINT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetNatIKESessionEntryStatus ARG_LIST((INT4  , UINT4  , UINT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetNatIKESessionEntryStatus ARG_LIST((INT4  , UINT4  , UINT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2NatIKESessionEntryStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2NatIKESessionTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for NatPortTrigInfoTable. */
INT1
nmhValidateIndexInstanceNatPortTrigInfoTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for NatPortTrigInfoTable  */

INT1
nmhGetFirstIndexNatPortTrigInfoTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexNatPortTrigInfoTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetNatPortTrigInfoAppName ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetNatPortTrigInfoEntryStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetNatPortTrigInfoAppName ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetNatPortTrigInfoEntryStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2NatPortTrigInfoAppName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2NatPortTrigInfoEntryStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2NatPortTrigInfoTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhGetNatStaticNaptLeaseDuration ARG_LIST((INT4  , UINT4  , INT4  , INT4  , 
                                           INT4   ,INT4 *));
INT1
nmhSetNatStaticNaptLeaseDuration ARG_LIST((INT4  , UINT4  , INT4  , INT4  , 
                                           INT4   ,INT4 ));
INT1
nmhTestv2NatStaticNaptLeaseDuration ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  , INT4  ,INT4 ));

/* Proto Validate Index Instance for NatPolicyTable. */
INT1
nmhValidateIndexInstanceNatPolicyTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for NatPolicyTable  */

INT1
nmhGetFirstIndexNatPolicyTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexNatPolicyTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetNatPolicyTranslatedIp ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetNatPolicyEntryStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetNatPolicyTranslatedIp ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetNatPolicyEntryStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2NatPolicyTranslatedIp ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2NatPolicyEntryStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2NatPolicyTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for NatRsvdPortTrigInfoTable. */
INT1
nmhValidateIndexInstanceNatRsvdPortTrigInfoTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for NatRsvdPortTrigInfoTable  */

INT1
nmhGetFirstIndexNatRsvdPortTrigInfoTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexNatRsvdPortTrigInfoTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetNatRsvdPortTrigInfoLocalIp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetNatRsvdPortTrigInfoRemoteIp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetNatRsvdPortTrigInfoStartTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetNatRsvdPortTrigInfoAppName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetNatRsvdPortTrigInfoInBoundPortRange ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetNatRsvdPortTrigInfoOutBoundPortRange ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetNatRsvdPortTrigInfoProtocol ARG_LIST((INT4 ,INT4 *));
