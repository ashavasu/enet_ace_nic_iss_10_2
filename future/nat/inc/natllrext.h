/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: natllrext.h,v 1.5 2013/10/25 10:50:07 siva Exp $
 *
 * Description: This file contains the typedefs for the SNMP
 *              related structures which are required for NAT
 *              low level routines. It is assumed that the
 *              standard definitons for OID type and OCTET
 *              STRING TYPE do not change in future.
 *
 *******************************************************************/
#include "natsnif.h"

/* *************************************************************************
 * This type defintion for SNMP OID type structure    *
 * *************************************************************************
 */

extern tInterfaceInfo* gapNatIfTable[NAT_MAX_NUM_IF + 1];
extern tTMO_SLL         gNatDynamicList;
extern tTMO_SLL         gNatNfsList;
extern UINT4            gu4NatIdleTimeOut;
extern UINT4            gu4NatTcpTimeOut;
extern UINT4            gu4NatUdpTimeOut;
extern tTMO_SLL         gNatLocIpAddrList;
extern UINT4            gu4NatTypicalNumOfEntries;
extern UINT4            gu4NatStatDynamicAllocFailureCount;
extern UINT4            gu4NatInitTranslatedLocPort;
extern UINT4            gu4NatEnable;
extern UINT4            gu4NatTrc;
extern tTMO_SLL         gNatPartialLinksList;

extern UINT4 NatMemAllocateMemBlock ARG_LIST((tMemPoolId MemPoolId, 
                                              UINT1 **ppu1Block ));
extern VOID NatMemReleaseMemBlock ARG_LIST((tMemPoolId PoolId, 
                                            UINT1 *pu1Block ));

extern UINT4 NatInitOperFields(VOID);
extern VOID NatCheckDynamicList(UINT4 u4IfNum);
VOID * NatApiAllocateMemory(VOID);
VOID  NatApiReleaseMemory(VOID *);

