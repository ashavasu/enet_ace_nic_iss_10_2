/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: nattimer.h,v 1.1.1.1 2008/10/03 09:25:30 prabuc-iss Exp $
 *
 * Description: This file contains the macros used in timer mod.
 *
 *******************************************************************/
/******************************************************************************
    MACROS USED IN NAT TIMER HANDLING ROUTINES
 *****************************************************************************/

#define   NAT_FLUSH_TIMER_ID                13
