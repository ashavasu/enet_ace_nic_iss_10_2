/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: natsz.h,v 1.9 2013/10/25 10:50:07 siva Exp $
 *
 * Description:This file contains the sizing macros for NAT 
 *
 *******************************************************************/

#ifndef _NATSZ_H
#define _NATSZ_H
enum {
    MAX_NAT_DNS_TABLE_BLOCKS_SIZING_ID,
    MAX_NAT_DYNAMIC_ENTRIES_SIZING_ID,
    MAX_NAT_FRAGMENTS_SIZE_SIZING_ID,
    MAX_NAT_FREE_GIP_LIST_NODE_SIZING_ID,
    MAX_NAT_FREE_PORT_NODE_SIZING_ID,
    MAX_NAT_GBL_HASH_NODE_SIZING_ID,
    MAX_NAT_GLOBAL_ADDR_TABLE_BLOCKS_SIZING_ID,
    MAX_NAT_IKE_HASH_NODE_SIZING_ID,
    MAX_NAT_IKE_LIST_NODE_SIZING_ID,
    MAX_NAT_IPSEC_IN_HASH_NODE_SIZING_ID,
    MAX_NAT_IPSEC_LIST_NODE_SIZING_ID,
    MAX_NAT_IPSEC_OUT_HASH_NODE_SIZING_ID,
    MAX_NAT_IPSEC_PEND_HASH_NODE_SIZING_ID,
    MAX_NAT_IPSEC_PEND_LIST_NODE_SIZING_ID,
    MAX_NAT_LID_LIST_NODE_SIZING_ID,
    MAX_NAT_LID_OID_ARR_DYN_LIST_NODE_SIZING_ID,
    MAX_NAT_LOCAL_ADDR_TABLE_BLOCKS_SIZING_ID,
    MAX_NAT_LOC_OUT_HASH_NODE_SIZING_ID,
    MAX_NAT_NO_OF_FRAGMENT_STREAM_SIZING_ID,
    MAX_NAT_NUM_IF_SIZING_ID,
    MAX_NAT_ONLY_LIST_BLOCKS_SIZING_ID,
    MAX_NAT_PARTIAL_LINKS_LIST_BLOCKS_SIZING_ID,
    MAX_NAT_STATIC_BLOCKS_SIZING_ID,
    MAX_NAT_STATIC_NAPT_BLOCKS_SIZING_ID,
    MAX_NAT_TCP_DEL_NODE_SIZING_ID,
    MAX_NAT_LINEAR_BUF_SIZING_ID,
    MAX_NAT_POLICY_NODE_SIZING_ID,
    NAT_SIP_HASH_NODE_SIZING_ID, 
    NAT_SIPALG_TASKLET_NODE_SIZING_ID, 
    NAT_SIP_NODE_SIZING_ID,
    NAT_MAX_SIZING_ID
};


#ifdef  _NATSZ_C
tMemPoolId gaNATMemPoolIds[ NAT_MAX_SIZING_ID];
INT4  NatSizingMemCreateMemPools(VOID);
VOID  NatSizingMemDeleteMemPools(VOID);
INT4  NatSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _NATSZ_C  */
extern tMemPoolId gaNATMemPoolIds[ ];
extern INT4  NatSizingMemCreateMemPools(VOID);
extern VOID  NatSizingMemDeleteMemPools(VOID);
extern INT4  NatSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _NATSZ_C  */


#ifdef  _NATSZ_C
tFsModSizingParams gaFsNATSizingParams [] = {
{ "tDnsEntryNode", "MAX_NAT_DNS_TABLE_BLOCKS", sizeof(tDnsEntryNode),MAX_NAT_DNS_TABLE_BLOCKS, MAX_NAT_DNS_TABLE_BLOCKS,0 },
{ "tDynamicEntry", "MAX_NAT_DYNAMIC_ENTRIES", sizeof(tDynamicEntry),MAX_NAT_DYNAMIC_ENTRIES, MAX_NAT_DYNAMIC_ENTRIES,0 },
{ "tNatIpFrag", "MAX_NAT_FRAGMENTS_SIZE", sizeof(tNatIpFrag),MAX_NAT_FRAGMENTS_SIZE, MAX_NAT_FRAGMENTS_SIZE,0 },
{ "tNatFreeGipListNode", "MAX_NAT_FREE_GIP_LIST_NODE", sizeof(tNatFreeGipListNode),MAX_NAT_FREE_GIP_LIST_NODE, MAX_NAT_FREE_GIP_LIST_NODE,0 },
{ "tFreePortNode", "MAX_NAT_FREE_PORT_NODE", sizeof(tFreePortNode),MAX_NAT_FREE_PORT_NODE, MAX_NAT_FREE_PORT_NODE,0 },
{ "tGlobalHashNode", "MAX_NAT_GBL_HASH_NODE", sizeof(tGlobalHashNode),MAX_NAT_GBL_HASH_NODE, MAX_NAT_GBL_HASH_NODE,0 },
{ "tTranslatedLocIpAddrNode", "MAX_NAT_GLOBAL_ADDR_TABLE_BLOCKS", sizeof(tTranslatedLocIpAddrNode),MAX_NAT_GLOBAL_ADDR_TABLE_BLOCKS, MAX_NAT_GLOBAL_ADDR_TABLE_BLOCKS,0 },
{ "tIKEHashNode", "MAX_NAT_IKE_HASH_NODE", sizeof(tIKEHashNode),MAX_NAT_IKE_HASH_NODE, MAX_NAT_IKE_HASH_NODE,0 },
{ "tIKEListNode", "MAX_NAT_IKE_LIST_NODE", sizeof(tIKEListNode),MAX_NAT_IKE_LIST_NODE, MAX_NAT_IKE_LIST_NODE,0 },
{ "tIPSecInHashNode", "MAX_NAT_IPSEC_IN_HASH_NODE", sizeof(tIPSecInHashNode),MAX_NAT_IPSEC_IN_HASH_NODE, MAX_NAT_IPSEC_IN_HASH_NODE,0 },
{ "tIPSecListNode", "MAX_NAT_IPSEC_LIST_NODE", sizeof(tIPSecListNode),MAX_NAT_IPSEC_LIST_NODE, MAX_NAT_IPSEC_LIST_NODE,0 },
{ "tIPSecOutHashNode", "MAX_NAT_IPSEC_OUT_HASH_NODE", sizeof(tIPSecOutHashNode),MAX_NAT_IPSEC_OUT_HASH_NODE, MAX_NAT_IPSEC_OUT_HASH_NODE,0 },
{ "tIPSecPendHashNode", "MAX_NAT_IPSEC_PEND_HASH_NODE", sizeof(tIPSecPendHashNode),MAX_NAT_IPSEC_PEND_HASH_NODE, MAX_NAT_IPSEC_PEND_HASH_NODE,0 },
{ "tIPSecPendListNode", "MAX_NAT_IPSEC_PEND_LIST_NODE", sizeof(tIPSecPendListNode),MAX_NAT_IPSEC_PEND_LIST_NODE, MAX_NAT_IPSEC_PEND_LIST_NODE,0 },
{ "tIidListNode", "MAX_NAT_LID_LIST_NODE", sizeof(tIidListNode),MAX_NAT_LID_LIST_NODE, MAX_NAT_LID_LIST_NODE,0 },
{ "tIidOidArrayDynamicListNode", "MAX_NAT_LID_OID_ARR_DYN_LIST_NODE", sizeof(tIidOidArrayDynamicListNode),MAX_NAT_LID_OID_ARR_DYN_LIST_NODE, MAX_NAT_LID_OID_ARR_DYN_LIST_NODE,0 },
{ "tLocIpAddrNode", "MAX_NAT_LOCAL_ADDR_TABLE_BLOCKS", sizeof(tLocIpAddrNode),MAX_NAT_LOCAL_ADDR_TABLE_BLOCKS, MAX_NAT_LOCAL_ADDR_TABLE_BLOCKS,0 },
{ "tLocOutHashNode", "MAX_NAT_LOC_OUT_HASH_NODE", sizeof(tLocOutHashNode),MAX_NAT_LOC_OUT_HASH_NODE, MAX_NAT_LOC_OUT_HASH_NODE,0 },
{ "tNatFragmentStream", "MAX_NAT_NO_OF_FRAGMENT_STREAM", sizeof(tNatFragmentStream),MAX_NAT_NO_OF_FRAGMENT_STREAM, MAX_NAT_NO_OF_FRAGMENT_STREAM,0 },
{ "tInterfaceInfo", "MAX_NAT_NUM_IF", sizeof(tInterfaceInfo),MAX_NAT_NUM_IF, MAX_NAT_NUM_IF,0 },
{ "tNatOnlyNode", "MAX_NAT_ONLY_LIST_BLOCKS", sizeof(tNatOnlyNode),MAX_NAT_ONLY_LIST_BLOCKS, MAX_NAT_ONLY_LIST_BLOCKS,0 },
{ "tNatPartialLinkNode", "MAX_NAT_PARTIAL_LINKS_LIST_BLOCKS", sizeof(tNatPartialLinkNode),MAX_NAT_PARTIAL_LINKS_LIST_BLOCKS, MAX_NAT_PARTIAL_LINKS_LIST_BLOCKS,0 },
{ "tStaticEntryNode", "MAX_NAT_STATIC_BLOCKS", sizeof(tStaticEntryNode),MAX_NAT_STATIC_BLOCKS, MAX_NAT_STATIC_BLOCKS,0 },
{ "tStaticNaptEntry", "MAX_NAT_STATIC_NAPT_BLOCKS", sizeof(tStaticNaptEntry),MAX_NAT_STATIC_NAPT_BLOCKS, MAX_NAT_STATIC_NAPT_BLOCKS,0 },
{ "tTcpDelNode", "MAX_NAT_TCP_DEL_NODE", sizeof(tTcpDelNode),MAX_NAT_TCP_DEL_NODE, MAX_NAT_TCP_DEL_NODE,0 },
{ "tNatLinearBuf", "MAX_NAT_LINEAR_BUF_BLOCKS", sizeof(tLinearBuf),MAX_NAT_LINEAR_BUF_BLOCKS, MAX_NAT_LINEAR_BUF_BLOCKS,0 },
{ "tNatPolicyNode", "MAX_NAT_POLICY_NODE", sizeof(tNatPolicyNode),MAX_NAT_POLICY_NODE, MAX_NAT_POLICY_NODE,0 },
{ "tNatWanUaHashNode", "NAT_SIP_HASH_POOL_NUM_BLK", sizeof(tNatWanUaHashNode),NAT_SIP_HASH_POOL_NUM_BLK, NAT_SIP_HASH_POOL_NUM_BLK,0 },
{ "tSipAlgTaskletData", "NAT_SIP_HASH_POOL_NUM_BLK", sizeof(tSipAlgTaskletData),NAT_SIP_HASH_POOL_NUM_BLK, NAT_SIP_HASH_POOL_NUM_BLK,0 },
{ "UINT1[NAT_SIZE_OF_BLK]","NAT_SIP_MEM_POOL_NUM_BLK", sizeof(UINT1[NAT_SIZE_OF_BLK]),NAT_SIP_MEM_POOL_NUM_BLK, NAT_SIP_MEM_POOL_NUM_BLK,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _NATSZ_C  */
extern tFsModSizingParams gaFsNATSizingParams [];
#endif /*  _NATSZ_C  */
#endif /* _NATSZ_H */


