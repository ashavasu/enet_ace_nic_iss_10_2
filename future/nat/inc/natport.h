/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: natport.h,v 1.4 2013/06/14 12:14:48 siva Exp $
 *
 * Description:This file contains the NAT porting related
 *             #defines. Whenever NAT is ported to someother
 *             target, this file should be modified
 *             accordingly. When NAT starts using FSAP2, the
 *             FSAP2 related mappings in this file should be
 *             removed.
 *
 *******************************************************************/
#ifndef _NAT_PORT_H
#define _NAT_PORT_H

/* will allocate only fixed block of memory (1000bytes) irrespective of the size specified */
#define   NAT_MALLOC(size)   NatApiAllocateMemory()
#define   NAT_FREE           NatApiReleaseMemory

#define   NAT_ATOI     ATOI
#define   CHR1        char

/* Converts integer IP Address to string */
#define NAT_CONVERT_IPADDR_TO_STR(pString, u4Value) \
do{\
         struct in_addr IpAddr;\
\
         IpAddr.s_addr = OSIX_NTOHL (u4Value);\
\
         pString = (CHR1 *)INET_NTOA (IpAddr);\
\
} while(0)

/* Number of allocs on the this buffer pool with pool id u2_QueId */
#define CRU_BUF_POOL_ALLOC_UINTS_COUNT(u2_QueId)  (pCRU_BUF_Free_PoolRecList[(u2_QueId)].u4_AllocCount)

#define   NAT_TASK_NAME                        CFA_TASK_NAME                  
#define   NAT_MAX_NUM_IF                       CFA_MAX_INTERFACES_IN_SYS + 1  
#define   NAT_NUM_OF_SYS_TIME_UNITS_IN_A_SEC   SYS_NUM_OF_TIME_UNITS_IN_A_SEC 

#endif /* _NAT_PORT_H */
