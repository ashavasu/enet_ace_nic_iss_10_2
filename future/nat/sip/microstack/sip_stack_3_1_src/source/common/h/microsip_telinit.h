/* Ensure Names are not mangled by C++ compilers */
#ifdef __cplusplus
extern "C" {
#endif 


/***********************************************************************
 ** FUNCTION:
 **             Has Init Functions For all Structures

 *********************************************************************
 **
 ** FILENAME:
 ** telinit.h
 **
 ** $Id: microsip_telinit.h,v 1.1.1.1 2010/12/08 06:10:43 siva Exp $ 
 **
 ** DESCRIPTION:
 ** This file contains code to init all  tel-url structures
 **
 ** DATE        NAME                    REFERENCE              REASON
 ** ----        ----                    ---------              --------
 ** 4Jan01   	Rajasri 									   Initial Creation
 **
 **
 **      Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *********************************************************************/




#ifndef __TEL_INIT_H_
#define __TEL_INIT_H_

#include "microsip_common.h"
#include "microsip_init.h"
#include "microsip_telstruct.h"

extern SipBool sip_initTelUrl _ARGS_((TelUrl **ppTel,SipError *pErr));

extern SipBool sip_initTelLocalNum _ARGS_ ((TelLocalNum **ppLocal,SipError \
	*pErr));

extern SipBool sip_initTelGlobalNum _ARGS_((TelGlobalNum **ppGlobal,SipError\
	 *pErr));

#endif /*__TEL_INIT */

/* Ensure Names are not mangled by C++ compilers */
#ifdef __cplusplus
}
#endif 
