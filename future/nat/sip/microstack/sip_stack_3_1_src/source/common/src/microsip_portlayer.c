/* Ensure Names are not mangled by C++ compilers */
#ifdef __cplusplus
extern              "C"
{
#endif

/*******************************************************************************
** FUNCTION:
**        All the porting layer functions of the stack.
********************************************************************************      
**
** FILENAME:
** microsip_portlayer.c
**
** $Id: microsip_portlayer.c,v 1.4 2012/12/21 12:00:40 siva Exp $ 
**
** DESCRIPTION:
**         This file contains all the functions which need to be ported across
**        different O/S.  
**
** DATE                NAME            REFERENCE      REASON
** ----                ----            ---------      --------
** 06/03/2002     Mahesh Govind             -        portlayer.c of core stk. 
** 18/03/2002     Aparna Kuppachi        -        Added wrappers for strncmp,
**                                                strlen and memset.
** 26/03/2002     Aparna Kuppachi        -        Added wrappers for strncpy
**                                                strstr and sprintf.
**
**  Copyright (C) 2006 Aricent Inc . All Rights Reserved
*******************************************************************************/
#ifndef SIP_FSAP
#if !defined(SIP_VXWORKS) && !defined(SIP_WINDOWS) && !defined(SIP_WINCE)
#include <sys/time.h>
#endif
#else
#include"osxstd.h"
#include"srmbuf.h"
#include"osxsys.h"
#include"utldll.h"
#include"srmtmr.h"
#endif

#ifndef SIP_FSAP
#include <stdlib.h>
#include <stdarg.h>
#include <limits.h>
#endif
#include "microsip_common.h"
#include "microsip_portlayer.h"
#include "microsip_trace.h"

#ifndef SIP_FSAP
#ifdef SIP_THREAD_SAFE
#ifdef SIP_VXWORKS
#include <semLib.h>
#endif
#endif
#endif

#define MAX_LENGTH 4294967295u

#ifndef SIP_FSAP
#include <errno.h>
#endif

    SIP_U32bit          SIP_T1;    /* T1 retrans value */
    SIP_U32bit          SIP_T2;    /* Cap off value */
    SIP_U32bit          SIP_MAXRETRANS;    /*
                                         * No. of times non INVITE messages 
                                         * will be retransmitted 
                                         */
    SIP_U32bit          SIP_MAXINVRETRANS;    /* 
                                             * No. of times INVITE messages 
                                             * will be retransmitted 
                                             */

/* 
 * In VxWorks the stdout descriptor is not accessed in a thread safe manner
 * Hence if multiplethreads try to write using this variable, VxWorks
 * itself does not provide any safety, hence the application itself needs
 * to do writes in a thread safe mechanism. For this reason the stack uses
 * a mutex to ensure all prints/writes are done in a thread safe manner
 */

#ifndef SIP_FSAP
#ifdef SIP_THREAD_SAFE
#ifdef SIP_VXWORKS
    synch_id_t          glbPrintMutex;
#endif
#endif
#endif

/*****************************************************************
** FUNCTION:sip_releaseStackPortSpecific
**
**
** DESCRIPTION: This functions handles all the port specific handling 
**              while the sip_releaseStack API is called.
*******************************************************************/
    void                sip_releaseStackPortSpecific (void)
    {
#ifndef SIP_FSAP
#ifdef SIP_THREAD_SAFE
#ifdef SIP_VXWORKS
        fast_free_synch (&glbPrintMutex);
#endif
#endif
#endif
    }

/*****************************************************************
** FUNCTION:sip_initStackPortSpecific
**
**
** DESCRIPTION: This functions handles all the port specific handling 
**              while the sip_initStack API is called.
*******************************************************************/
    void                sip_initStackPortSpecific (void)
    {
#ifndef SIP_FSAP
#ifdef SIP_THREAD_SAFE
#ifdef SIP_VXWORKS
        fast_init_synch (&glbPrintMutex);
#endif
#endif
#endif
    }
#ifdef SIP_THREAD_SAFE
#ifndef SIP_FSAP
#if defined (SIP_WINDOWS) || defined (SIP_WINCE)
/*****************************************************************
** FUNCTION:fast_lock_mutex
**
**
** DESCRIPTION:
*******************************************************************/
    void                fast_lock_mutex
#ifdef ANSI_PROTO
                        (thread_id_t tid, mutex_id_t * mutex, SIP_U32bit flags)
#else
                        (tid, mutex, flags) thread_id_t tid;
    mutex_id_t         *mutex;
    SIP_U32bit          flags;
#endif
    {
        thread_id_t         dummy_tid;
        SIP_U32bit          dummy_flags;
        dummy_tid = tid;
        dummy_flags = flags;
        (void) dummy_flags;
        (void) dummy_tid;

        (void) sip_trace (SIP_Detailed, SIP_Init,
                          (SIP_S8bit *) "STACK TRACE: Grabbing Mutex Lock");
        (void) WaitForSingleObject (*mutex, INFINITE);
        (void) sip_trace (SIP_Detailed, SIP_Init,
                          (SIP_S8bit *)
                          "STACK TRACE: Successfully Grabbed Mutex Lock");
    }
/*****************************************************************
** FUNCTION:fast_trylock_mutex
**
**
** DESCRIPTION:
*******************************************************************/
    SipBool             fast_trylock_mutex
#ifdef ANSI_PROTO
                        (thread_id_t tid, mutex_id_t * mutex, SIP_U32bit flags)
#else
                        (tid, mutex, flags) thread_id_t tid;
    mutex_id_t         *mutex;
    SIP_U32bit          flags;
#endif
    {
        thread_id_t         dummy_tid;
        SIP_U32bit          dummy_flags;
        dummy_tid = tid;
        dummy_flags = flags;
        (void) dummy_flags;
        (void) dummy_tid;

/* Port according to system and library selection */
        (void) sip_trace (SIP_Detailed, SIP_Init,
                          (SIP_S8bit *)
                          "STACK TRACE: Trying to Grab Mutex Lock");
        if (WaitForSingleObject (*mutex, SIP_RETURN_IMMEDIATELY) ==
            WAIT_TIMEOUT)
            return SipFail;
        (void) sip_trace (SIP_Detailed, SIP_Init,
                          (SIP_S8bit *)
                          "STACK TRACE: Successfully Grabbed Mutex Lock");
        return SipSuccess;
    }

/*****************************************************************
** FUNCTION:fast_unlock_mutex
**
**
** DESCRIPTION:
*******************************************************************/
    void                fast_unlock_mutex
#ifdef ANSI_PROTO
                        (thread_id_t tid, mutex_id_t * mutex)
#else
                        (tid, mutex) thread_id_t tid;
    mutex_id_t         *mutex;
#endif
    {
        thread_id_t         dummy_tid;
        dummy_tid = tid;
        (void) dummy_tid;
        (void) sip_trace (SIP_Detailed, SIP_Init,
                          (SIP_S8bit *) "STACK TRACE: Released Mutex Lock");
        (void) ReleaseMutex (*mutex);
    }

/*****************************************************************
** FUNCTION:fast_init_mutex
**
**
** DESCRIPTION:
*******************************************************************/
    void                fast_init_mutex
#ifdef ANSI_PROTO
                        (mutex_id_t * mutex)
#else
                        (mutex) mutex_id_t *mutex;
#endif
    {
        /* removed PTHREAD_PROCESS_PRIVATE from the next statement & made null */
        *mutex = CreateMutex (NULL, FALSE, NULL);
        (void) sip_trace (SIP_Detailed, SIP_Init,
                          (SIP_S8bit *) "STACK TRACE: Initialised Mutex Lock");
    }
/*****************************************************************
** FUNCTION:fast_free_mutex
**
**
** DESCRIPTION:
*******************************************************************/
    void                fast_free_mutex
#ifdef ANSI_PROTO
                        (mutex_id_t * mutex)
#else
                        (mutex) mutex_id_t *mutex;
#endif
    {
        /* removed PTHREAD_PROCESS_PRIVATE from the next statement & made null */
        (void) ReleaseMutex (*mutex);
        (void) CloseHandle (*mutex);
        (void) sip_trace (SIP_Detailed, SIP_Init,
                          (SIP_S8bit *) "STACK TRACE: Destroy Mutex Lock");
    }
#endif // end of SIP_THREAD_SAFE FOR SIP_WINDOWS || SIP_WINCE
#endif

/****************************************************************************
 ** FUNCTION :        fast_lock_synch
 **
 ** DESCRIPTION:    This function locks\protects a critical section\global.
 ** PARAMETERS:
 **             thread_id_t(IN)            : The thread id.
 **            mutex         (IN)        : mutex/semaphore id
 **            flags        (IN)        : The flags to be used if any
 **            Return Value:
 **            void
 **
 ****************************************************************************/
    void                fast_lock_synch
#ifdef ANSI_PROTO
                        (thread_id_t tid, synch_id_t * mutex, SIP_U32bit flags)
#else
                        (tid, mutex, flags) thread_id_t tid;
    synch_id_t         *mutex;
    SIP_U32bit          flags;
#endif
    {
        (void) tid;
        (void) flags;
/* Port according to system and library selection */
#ifndef SIP_FSAP
#ifdef SIP_VXWORKS
        semTake (*mutex, WAIT_FOREVER);
#endif
#ifdef SIP_LINUX
        (void) pthread_mutex_lock (mutex);
#endif
#if defined (SIP_WINDOWS) || defined (SIP_WINCE)
#ifdef SIP_CRITICAL_SECTION
        EnterCriticalSection (mutex);
#else
        (void) WaitForSingleObject (*mutex, INFINITE);
#endif
#endif
#else
        OsixSemTake (*mutex);
#endif

    }

/*****************************************************************
** FUNCTION:fast_trylock_synch
**
**
** DESCRIPTION:
*******************************************************************/
    SipBool             fast_trylock_synch
#ifdef ANSI_PROTO
                        (thread_id_t tid, synch_id_t * mutex, SIP_U32bit flags)
#else
                        (tid, mutex, flags) thread_id_t tid;
    synch_id_t         *mutex;
    SIP_U32bit          flags;
#endif
    {
        (void) tid;
        (void) flags;
        /* Port according to system and library selection */
        (void) sip_trace (SIP_Detailed, SIP_Init,
                          (SIP_S8bit *)
                          "STACK TRACE: Trying to Grab Mutex Lock");
#ifndef SIP_FSAP
#ifdef SIP_VXWORKS
        if (semTake (*mutex, NO_WAIT) != OK)
            return SipFail;
#endif

#ifdef SIP_LINUX
        if (pthread_mutex_trylock (mutex) != 0)
            return SipFail;
#endif

#if defined (SIP_WINDOWS) || defined (SIP_WINCE)
#ifndef SIP_CRITICAL_SECTION
        if (WaitForSingleObject (*mutex, SIP_RETURN_IMMEDIATELY) ==
            WAIT_TIMEOUT)
            return SipFail;
#endif
#endif
#else
        (void) mutex;
#endif

        (void) sip_trace (SIP_Detailed, SIP_Init,
                          (SIP_S8bit *)
                          "STACK TRACE: Successfully Grabbed Mutex Lock");
        return SipSuccess;
    }

/****************************************************************************
 ** FUNCTION :        fast_unlock_synch
 **
 ** DESCRIPTION:    This function locks\protects a critical section\global.
 ** PARAMETERS:
 **             thread_id_t(IN)            : The thread id.
 **            mutex         (IN)        : mutex/semaphore id
 **            Return Value:
 **            void
 **
 ****************************************************************************/
    void                fast_unlock_synch
#ifdef ANSI_PROTO
                        (thread_id_t tid, synch_id_t * mutex)
#else
                        (tid, mutex) thread_id_t tid;
    synch_id_t         *mutex;
#endif
    {
        (void) tid;
        /* Port according to system and library selection */
#ifndef SIP_FSAP
#ifdef SIP_VXWORKS
        semGive (*mutex);
#endif
#ifdef SIP_LINUX
        (void) pthread_mutex_unlock (mutex);
#endif

#if defined (SIP_WINDOWS) || defined (SIP_WINCE)
#ifdef SIP_CRITICAL_SECTION
        LeaveCriticalSection (mutex);
#else
        (void) ReleaseMutex (*mutex);
#endif
#endif
#else
        OsixSemGive (*mutex);
#endif

    }

/****************************************************************************
 ** FUNCTION :        fast_init_synch
 **
 ** DESCRIPTION:    This function initializes mutex/semaphore.
 ** PARAMETERS:
 **            mutex (IN/OUT)        : mutex to be initialized
 **            Return Value:
 **            void
 **
 ****************************************************************************/
    void                fast_init_synch
#ifdef ANSI_PROTO
#ifdef SIP_FSAP
                        (const SIP_U8bit SemName[], synch_id_t * mutex)
#else
                        (synch_id_t * mutex)
#endif
#else
#ifdef SIP_FSAP
                        (SemName, mutex) const SIP_U8bit SemName[];
    synch_id_t         *mutex;
#else
                        (mutex) synch_id_t *mutex;
#endif
#endif
    {
/* Port according to system and library selection */
        /* removed PTHREAD_PROCESS_PRIVATE from the next statement & made null */

#ifndef SIP_FSAP
#ifdef SIP_VXWORKS
        *mutex = semMCreate (SEM_Q_PRIORITY);
#endif

#ifdef SIP_LINUX
        /*(void)pthread_mutex_init(mutex,NULL); */
        pthread_mutexattr_t attr;
        (void) pthread_mutexattr_init (&attr);
        (void) pthread_mutexattr_settype (&attr, PTHREAD_MUTEX_RECURSIVE);
        (void) pthread_mutex_init (mutex, &attr);
        (void) pthread_mutexattr_destroy (&attr);
#endif

#if defined (SIP_WINDOWS) || defined (SIP_WINCE)
#ifdef SIP_CRITICAL_SECTION
        InitializeCriticalSection (mutex);
#else
        *mutex = CreateMutex (NULL, FALSE, NULL);
#endif
#endif
#else
        OsixCreateSem (SemName, 1, 0, mutex);
#endif
        (void) sip_trace (SIP_Detailed, SIP_Init,
                          (SIP_S8bit *) "STACK TRACE: Initialised Mutex Lock");
    }

/*endif of #ifdef SIP_THREAD_SAFE*/
#endif

/*****************************************************************
* FUNCTION NAME :fast_memrealloc
*
*
* DESCRIPTION:
*******************************************************************/

    SIP_Pvoid           fast_memrealloc
#ifdef ANSI_PROTO
     
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        (SIP_U32bit module_id, SIP_Pvoid pBuffer, SIP_U32bit noctets,
         SipError * err)
#else
                        (moduleId, pBuffer, noctets, err) SIP_U32bit module_id;
    SIP_Pvoid           pBuffer;
    SIP_U32bit          noctets;
    SipError           *err;
#endif
    {
        SIP_Pvoid           pvoid = NULL;
        (void) module_id;

#ifdef DYNAMIC_MEMORY_CALC_BUGGY
        /* This calculation might be wrong we need to test and 
           fix this.
           However this is for internal purposes and should not
           affect the working of the stack.
         */
#endif

#ifndef SIP_FSAP
        pvoid = (SIP_Pvoid) realloc (pBuffer, noctets);
#else
#ifndef LINUX_KERN
        pvoid = MEM_REALLOC (pBuffer, noctets, SIP_Pvoid);
#endif
#endif

        if (pvoid == SIP_NULL)
        {
            if (err != SIP_NULL)
            {
                *err = E_NO_MEM;
            }
            return SIP_NULL;
        }

        if (err != SIP_NULL)
        {
            *err = E_NO_ERROR;
        }

#ifdef DYNAMIC_MEMORY_CAL_BUGGY
        {
            SIP_S32bit          indx = 0;
            SIP_S32bit          flag = 0;

            /*
             * If the allocation is done for parser modules
             * then add the memory allocated to the count
             */

            if (module_id == DECODE_MEM_ID)
            {
                glb_memory_alloc_parser[0] -= initSize;
                glb_memory_alloc_parser[0] += noctets;
            }
            else if (module_id == BISON_MEM_ID)
            {
                glb_memory_alloc_parser[1] -= initSize;
                glb_memory_alloc_parser[1] += noctets;
            }
            else if (module_id == FLEX_MEM_ID)
            {
                glb_memory_alloc_parser[1] -= initSize;
                glb_memory_alloc_parser[2] += noctets;
            }
            else
            {
                flag = 1;
            }

            /*
             * If allocation is done for parser module
             * then count the memory allocation
             * pattern
             */
            if (flag == 0)
            {
                if ((0 < noctets) && (noctets <= 10))
                    indx = 1;
                else if ((10 < noctets) && (noctets <= 100))
                    indx = 2;
                else if ((100 < noctets) && (noctets <= 1000))
                    indx = 3;
                else
                    indx = 4;
                glb_memory_alloc_dist[indx]++;
            }
        }
#endif
        return pvoid;
    }

/****************************************************************************
 ** FUNCTION :        fast_memget
 **
 ** DESCRIPTION:    This function allocates memory.This function can
 **            be ported for  having appropriate memory management scheme.
 ** PARAMETERS:
 **             module_id(IN):  The Module for which memory is allocated.
 **            noctets(IN):       The number of bytes of memory to be allocated
 **            SipError(IN):    The SipError value
 **            Return Value:
 **            Pointer to allocated buffer or Null
 **
 ****************************************************************************/

    SIP_Pvoid           fast_memget
#ifdef ANSI_PROTO
     
        
        
        
        
                      (SIP_U32bit module_id, SIP_U32bit noctets, SipError * err)
#else
                        (module_id, noctets, err) SIP_U32bit module_id;
    SIP_U32bit          noctets;
    SipError           *err;
#endif
    {
        SIP_Pvoid           pvoid;
        (void) module_id;
#ifdef SIP_FSAP
        pvoid = MEM_MALLOC (noctets, SIP_Pvoid);
#else
        pvoid = (SIP_Pvoid) malloc (noctets);
#endif
        if (pvoid == SIP_NULL)
        {
            if (err != SIP_NULL)
                *err = E_NO_MEM;
            return SIP_NULL;
        }

        if (err != SIP_NULL)
            *err = E_NO_ERROR;
        return pvoid;
    }
/****************************************************************************
 ** FUNCTION :        fast_memfree
 **
 ** DESCRIPTION:    This function allocates memory.This function can
 **            be ported for  having appropriate memory management scheme.
 ** PARAMETERS:
 **             module_id(IN):  The Module for which memory is allocated.
 **            noctets(IN):       The number of bytes of memory to be allocated
 **            SipError(IN):    The SipError value
 ** Return Value:
 **                SipBool
 ****************************************************************************/
    SipBool             fast_memfree
#ifdef ANSI_PROTO
                        (SIP_U32bit module_id, SIP_Pvoid p_buf, SipError * err)
#else
                        (module_id, p_buf, err) SIP_U32bit module_id;
    SIP_Pvoid           p_buf;
    SipError           *err;
#endif
    {
        (void) module_id;
        if (p_buf != SIP_NULL)
#ifdef SIP_FSAP
            MEM_FREE (p_buf);
#else
            free (p_buf);
#endif

        if (err != SIP_NULL)
            *err = E_NO_ERROR;
        return SipSuccess;
    }

/****************************************************************************
 ** FUNCTION :        sip_memget
 **
 ** DESCRIPTION:    This function allocates memory.This function can
 **            be ported for  having appropriate memory management scheme.
 ** PARAMETERS:
 **             module_id(IN):  The Module for which memory is allocated.
 **            noctets(IN):       The number of bytes of memory to be allocated
 **            SipError(IN):    The SipError value
 **
 **            Return Value:
 **            Pointer to allocated buffer or Null
 **
 ****************************************************************************/

    SIP_Pvoid           sip_memget
#ifdef ANSI_PROTO
     
        
        
        
        
                      (SIP_U32bit module_id, SIP_U32bit noctets, SipError * err)
#else
                        (module_id, noctets, err) SIP_U32bit module_id;
    SIP_U32bit          noctets;
    SipError           *err;
#endif
    {
        return (fast_memget (module_id, noctets, err));
    }

/****************************************************************************
 ** FUNCTION :        sip_memfree
 **
 ** DESCRIPTION:     frees memory and SIP_NULLifies pointer. Accepts pointer
** to pointer to area 
** Allows pUser to pass SIP_NULL in SipError* param
 ** PARAMETERS:
 **             module_id(IN)    : The Module for which memory is allocated.
 **            p_buf(IN)        : The chunk of memory to be freed
 **            err(IN)            : The SipError value
 ** Return Value:
 **                SipBool
 ****************************************************************************/

    SipBool             sip_memfree
#ifdef ANSI_PROTO
     
        
        
        
                       (SIP_U32bit module_id, SIP_Pvoid * p_buf, SipError * err)
#else
                        (module_id, p_buf, err) SIP_U32bit module_id;
    SIP_Pvoid          *p_buf;
    SipError           *err;
#endif
    {
        SipBool             x;

        x = fast_memfree (module_id, *p_buf, err);
        *p_buf = SIP_NULL;
        return x;

    }
/*******************************************************************************
** FUNCTION:sip_snprintf
** DESCRIPTION: wrapper function around C snprintf function.
** 
** PARAMETERS:
**            pStr (OUT) :the buffer to which the formatted string to be printed.
**            pFmt (IN) : the format string.
**            n    (IN) :    N bytes to be printed
**
** Return Value:
**            SIP_U32bit
** 
*******************************************************************************/

#ifdef SIP_OVERRIDE_SNPRINTF
#ifndef SIP_FSAP
    SIP_U32bit          sip_snprintf (SIP_S8bit * s, size_t n,
                                      const SIP_S8bit * fmt, ...)
    {

        va_list             ap;
        (void) n;

        va_start (ap, fmt);
        (void) vsprintf (s, fmt, ap);
        va_end (ap);
        return 1;                /* Should realy be returning the number of bytes formatted */
    }
#else
#ifdef VAR_ARGS_SUPPORT
    SIP_U32bit          sip_snprintf (SIP_S8bit * s, size_t n,
                                      const SIP_S8bit * fmt, ...)
    {
        va_list             ap;
        (void) n;
        va_start (ap, fmt);
        (void) UtlVsprintf (s, fmt, ap);
        va_end (ap);
        return 1;
    }
#else
    SIP_U32bit          sip_snprintf (SIP_S8bit * s, size_t n,
                                      const SIP_S8bit * fmt, SIP_U32bit arg1,
                                      SIP_U32bit arg2, SIP_U32bit arg3,
                                      SIP_U32bit arg4, SIP_U32bit arg5)
    {
        (void) n;
        SIP_U32bit          ArgCount = 0;
        SIP_S8bit          *tempFmtStr;
        SIP_U32bit          argValue[5];

        argValue[0] = arg1;
        argValue[1] = arg2;
        argValue[2] = arg3;
        argValue[3] = arg4;
        argValue[4] = arg5;

        tempFmtStr = fmt;
        while (*tempFmtStr)
        {
            if (*tempFmtStr == '%')
            {
                tempFmtStr++;

                if (*tempFmtStr != '%')
                {
                    ArgCount++;
                }
            }
            tempFmtStr++;
        }
        switch (ArgCount)
        {
            case 0:
                SPRINTF (s, fmt);
                break;
            case 1:
                SPRINTF (s, fmt, arg1);
                break;
            case 2:
                SPRINTF (s, fmt, arg1, arg2);
                break;
            case 3:
                SPRINTF (s, fmt, arg1, arg2, arg3);
                break;
            case 4:
                SPRINTF (s, fmt, arg1, arg2, arg3, arg4);
                break;
            case 5:
                SPRINTF (s, fmt, arg1, arg2, arg3, arg4, arg5);
                break;
            default:
                SPRINTF (s,
                         "Number of Arguments > 5. Need to enhance this function");
        }
    }
#endif
#endif
#endif

/****************************************************************************
** FUNCTION:STRTOLCAP
** DESCRIPTION: converts string to unsigned long.
** this function wont caps off pValue to 2^32-1 if larger pValue is
** given as input. 
**
** PARAMETERS:
**             str(IN)        :  The Module for which memory is allocated.
**            Return Value
**            SIP_U32bit
**            
**
 ****************************************************************************/

    SIP_U32bit          STRTOLCAP
#ifdef ANSI_PROTO
                        (SIP_S8bit * str)
#else
                        (str) SIP_S8bit *str;
#endif
    {
#ifdef SIP_FSAP
        FS_UINT8            result;
        (void) UtlStr2U8 (str, &result);
        return result.u4Lo;
#else
        return strtoul (str, NULL, 10);
#endif
        /* Capping functionality missing */
    }

/*******************************************************************************
** FUNCTION:sip_memset
** DESCRIPTION: wrapper function around C memset function.
** 
** PARAMETERS:
**            pStr (IN/OUT): starting point of input buffer
**            dVal (IN)     : The value to be set
**            n    (IN)     : n number of bytes , which has to be set with dVal
** 
*******************************************************************************/

    SIP_Pvoid           sip_memset
#ifdef ANSI_PROTO
                        (SIP_Pvoid pStr, SIP_U32bit dVal, SIP_U32bit dSize)
#else
                        (pStr, dVal, dSize) SIP_Pvoid *pStr;
    SIP_U32bit          dVal;
    SIP_U32bit          dSize;
#endif
    {
#ifdef SIP_FSAP
        return MEMSET (pStr, dVal, (size_t) dSize);
#else
        return memset (pStr, dVal, (size_t) dSize);
#endif
    }
/*******************************************************************************
** FUNCTION:sip_strncmp
** DESCRIPTION: wrapper function around C strcmp function.
** 
** PARAMETERS:
**            pStr1 (IN)     : String 1 to be compared
**            pStr2 (IN)     : String 2 to be compared
**            dSize (IN)     : n number of bytes , to be compared
** Return Value:
**            SIP_U32bit
** 
*******************************************************************************/
    SIP_U32bit          sip_strncmp
#ifdef ANSI_PROTO
                        (SIP_S8bit * pStr1, SIP_S8bit * pStr2, SIP_U32bit dSize)
#else
                        (pStr1, pStr2, dSize) SIP_S8bit *pStr1;
    SIP_S8bit          *pStr2;
    SIP_U32bit          dSize;
#endif
    {
#ifdef SIP_FSAP
        return STRNCMP (pStr1, pStr2, (size_t) dSize);
#else
        return strncmp (pStr1, pStr2, (size_t) dSize);
#endif
    }
/*******************************************************************************
** FUNCTION:sip_strcmp
** DESCRIPTION: wrapper function around C strncmp function.
** 
** PARAMETERS:
**            pStr1 (IN)     : String 1 to be compared
**            pStr2 (IN)     : String 2 to be compared
** Return Value:
**            SIP_U32bit
** 
*******************************************************************************/

    SIP_U32bit          sip_strcmp
#ifdef ANSI_PROTO
                        (SIP_S8bit * pStr1, SIP_S8bit * pStr2)
#else
                        (pStr1, pStr2) SIP_S8bit *pStr1;
    SIP_S8bit          *pStr2;
#endif
    {

#ifdef SIP_FSAP
        return STRCMP (pStr1, pStr2);
#else
        return strcmp (pStr1, pStr2);
#endif
    }

/*******************************************************************************
** FUNCTION:sip_strdup
** DESCRIPTION: wrapper function around C strdup function.
** 
** PARAMETERS:
**            pStr (OUT) :the string to be , duplicated.
** Return Value:
**            SIP_S8bit *
** 
*******************************************************************************/
    SIP_S8bit          *sip_strdup
#ifdef ANSI_PROTO
                        (const SIP_S8bit * pOriginalString, SIP_U32bit dMemId)
#else
     
        
        
        
        
        
                     (pOriginalString, dMemId) const SIP_S8bit *pOriginalString;
    SIP_U32bit          dMemId;
#endif
    {
        SIP_S8bit          *pRetVal = SIP_NULL;
        SIP_U32bit          dSize;

        if (pOriginalString == SIP_NULL)
            return SIP_NULL;
#ifdef SIP_FSAP
        dSize = sip_strlen (pOriginalString);
#else
        dSize = strlen (pOriginalString);
#endif
        pRetVal = (SIP_S8bit *) fast_memget (dMemId, (dSize + 1), SIP_NULL);

        if (pRetVal == SIP_NULL)
            return SIP_NULL;
#ifdef SIP_FSAP
        sip_strcpy (pRetVal, pOriginalString);
#else
        strcpy (pRetVal, pOriginalString);
#endif
        return pRetVal;
    }

/*******************************************************************************
** FUNCTION:sip_strcasecmp
** DESCRIPTION: wrapper function around C strcasecmp function.
** 
** PARAMETERS:
**            pStr1 (IN)     : String 1 to be compared
**            pStr2 (IN)     : String 2 to be compared
** Return Value:
**            SIP_U32bit
** 
*******************************************************************************/

    SIP_U32bit          sip_strcasecmp
#ifdef ANSI_PROTO
                        (const SIP_S8bit * pStr1, const SIP_S8bit * pStr2)
#else
                        (pStr1, pStr2) SIP_S8bit *pStr1;
    SIP_S8bit          *pStr2;
#endif
    {
#ifndef SIP_FSAP
#if defined(SIP_WINDOWS)
        return (stricmp (pStr1, pStr2));
#elif defined (SIP_WINCE)
        return (_stricmp (pStr1, pStr2));
#else
        return strcasecmp (pStr1, pStr2);
#endif
#else
        return STRCASECMP (pStr1, pStr2);
#endif

    }
/*******************************************************************************
** FUNCTION:sip_strncasecmp
** DESCRIPTION: wrapper function around C strncasecmp function.
** 
** PARAMETERS:
**            pStr1 (IN)     : String 1 to be compared
**            pStr2 (IN)     : String 2 to be compared
**             n       (n)     : n bytes to be compared
** Return Value:
**            SIP_U32bit
** 
*******************************************************************************/
    SIP_U32bit          sip_strncasecmp
#ifdef ANSI_PROTO
                        (SIP_S8bit * pStr1, SIP_S8bit * pStr2, SIP_U32bit n)
#else
                        (pStr1, pStr2) SIP_S8bit *pStr1;
    SIP_S8bit          *pStr2;
#endif
    {
#ifndef SIP_FSAP
#if defined(SIP_WINDOWS)
        return (strnicmp (pStr1, pStr2, n));
#elif defined SIP_WINCE
        return (_strnicmp (pStr1, pStr2, n));
#else
        return strncasecmp (pStr1, pStr2, n);
#endif
#else
        return STRNCASECMP (pStr1, pStr2, n);
#endif

    }

/*******************************************************************************
** FUNCTION:sip_strncpy
** DESCRIPTION: wrapper function around C strncpy function.
** 
** PARAMETERS:
**            pStr1 (OUT)     : destination
**            pStr2 (IN)     : source
**             n       (n)     : n bytes to be copied
** Return Value:
**            SIP_S8bit *
** 
*******************************************************************************/

    SIP_S8bit          *sip_strncpy
#ifdef ANSI_PROTO
                        (SIP_S8bit * pStr1, SIP_S8bit * pStr2, SIP_U32bit dSize)
#else
                        (pStr1, pStr2, dSize) SIP_S8bit *pStr1;
    SIP_S8bit          *pStr2;
    SIP_U32bit          dSize;
#endif
    {
#ifdef SIP_FSAP
        return STRNCPY (pStr1, pStr2, (size_t) dSize);
#else
        return strncpy (pStr1, pStr2, (size_t) dSize);
#endif
    }
/*******************************************************************************
** FUNCTION:sip_strlen
** DESCRIPTION: wrapper function around C strlen function.
** 
** PARAMETERS:
**            pStr1 (OUT)     : the string whose length to be found.
** Return Value:
**            SIP_U32bit
** 
*******************************************************************************/

    SIP_U32bit          sip_strlen
#ifdef ANSI_PROTO
                        (const SIP_S8bit * pStr)
#else
                        (pStr) SIP_S8bit *pStr;
#endif
    {
#ifdef SIP_FSAP
        return ((SIP_U32bit) STRLEN (pStr));
#else
        return ((SIP_U32bit) strlen (pStr));
#endif
    }
/*******************************************************************************
** FUNCTION:sip_strstr
** DESCRIPTION: wrapper function around C strstr function.
** 
** PARAMETERS:
**            pStr1 (IN)     : the pattern string .
**            pStr2 (OUT)     : the string in which the pattern has to be found.
** Return Value:
**            SIP_Sbit *
** 
*******************************************************************************/

    SIP_S8bit          *sip_strstr
#ifdef ANSI_PROTO
                        (SIP_S8bit * pStr1, SIP_S8bit * pStr2)
#else
                        (pStr1, pStr2) SIP_S8bit *pStr1;
    SIP_S8bit          *pStr2;
#endif
    {
#ifdef SIP_FSAP
        return STRSTR (pStr1, pStr2);
#else
        return strstr (pStr1, pStr2);
#endif
    }
/*******************************************************************************
** FUNCTION:sip_sprintf
** DESCRIPTION: wrapper function around C sprintf function.
** 
** PARAMETERS:
**            pStr (OUT) :the buffer to which the formatted string to be printed.
**            pFmt (OUT) : the format string.
** Return Value:
**            SIP_U32bit
** 
*******************************************************************************/
#ifndef SIP_FSAP
    SIP_U32bit          sip_sprintf
#ifdef ANSI_PROTO
                        (SIP_S8bit * pStr, SIP_S8bit * pFmt, ...)
#else
                        (pStr, pFmt, ...) SIP_S8bit *pStr;
    SIP_S8bit          *pFmt;
#endif
    {
        va_list             ap;
        va_start (ap, pFmt);
        (void) vsprintf (pStr, pFmt, ap);
        va_end (ap);

        return 1;
    }
#else
#ifdef VAR_ARGS_SUPPORT
    SIP_U32bit          sip_sprintf
#ifdef ANSI_PROTO
                        (SIP_S8bit * pStr, SIP_S8bit * pFmt, ...)
#else
                        (pStr, pFmt, ...) SIP_S8bit *pStr;
    SIP_S8bit          *pFmt;
#endif
    {
        va_list             ap;
        va_start (ap, pFmt);
        (void) UtlVsprintf (pStr, pFmt, ap);
        va_end (ap);

        return 1;
    }
#else
    SIP_U32bit          sip_sprintf
#ifdef ANSI_PROTO
     
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        (SIP_S8bit * pStr, SIP_S8bit * pFmt, SIP_U32bit arg1, SIP_U32bit arg2,
         SIP_U32bit arg3, SIP_U32bit arg4, SIP_U32bit arg5)
#else
     
        
        
        
        
        
                     (pStr, pFmt, arg1, arg2, arg3, arg4, arg5) SIP_S8bit *pStr;
    SIP_S8bit          *pFmt;
    SIP_U32bit          arg1;
    SIP_U32bit          arg2;
    SIP_U32bit          arg3;
    SIP_U32bit          arg4;
    SIP_U32bit          arg5;
#endif
    {

        SIP_U32bit          ArgCount = 0;
        SIP_S8bit          *tempFmtStr;
        SIP_U32bit          argValue[5];

        argValue[0] = arg1;
        argValue[1] = arg2;
        argValue[2] = arg3;
        argValue[3] = arg4;
        argValue[4] = arg5;

        tempFmtStr = pFmt;
        while (*tempFmtStr)
        {
            if (*tempFmtStr == '%')
            {
                tempFmtStr++;

                if (*tempFmtStr != '%')
                {
                    ArgCount++;
                }
            }
            tempFmtStr++;
        }
        switch (ArgCount)
        {
            case 0:
                SPRINTF (pStr, pFmt);
                break;
            case 1:
                SPRINTF (pStr, pFmt, arg1);
                break;
            case 2:
                SPRINTF (pStr, pFmt, arg1, arg2);
                break;
            case 3:
                SPRINTF (pStr, pFmt, arg1, arg2, arg3);
                break;
            case 4:
                SPRINTF (pStr, pFmt, arg1, arg2, arg3, arg4);
                break;
            case 5:
                SPRINTF (pStr, pFmt, arg1, arg2, arg3, arg4, arg5);
                break;
            default:
                SPRINTF (pStr,
                         "Number of Arguments > 5. Need to enhance this function");
        }
    }
#endif
#endif

/*******************************************************************************
** FUNCTION:sip_strcat
** DESCRIPTION: wrapper function around C strcat function.
** 
** PARAMETERS:
**            pDest (OUT) :the string to which another string is to be added
**            pSrc  (OUT) :the string , which must be appended to pDest
** Return Value:
**            SIP_S8bit *
** 
*******************************************************************************/

    SIP_S8bit          *sip_strcat
#ifdef ANSI_PROTO
                        (SIP_S8bit * pDest, SIP_S8bit * pSrc)
#else
                        (pDest, pSrc) SIP_S8bit *pDest;
    SIP_S8bit          *pSrc;
#endif
    {
#ifdef SIP_FSAP
        return STRCAT (pDest, pSrc);
#else
        return strcat (pDest, pSrc);
#endif
    }
/*******************************************************************************
** FUNCTION:sip_strcpy
** DESCRIPTION: wrapper function around C strcy function.
** 
** PARAMETERS:
**            pDest (OUT) :the string to which another string is to be copied
**            pSrc  (OUT) :the string , which must be copied to pDest
** Return Value:
**            SIP_S8bit *
** 
*******************************************************************************/
    SIP_S8bit          *sip_strcpy
#ifdef ANSI_PROTO
                        (SIP_S8bit * pDest, const SIP_S8bit * pSrc)
#else
                        (pDest, pSrc) SIP_S8bit *pDest;
    SIP_S8bit          *pSrc;
#endif
    {
#ifdef SIP_FSAP
        return STRCPY (pDest, pSrc);
#else
        return strcpy (pDest, pSrc);
#endif
    }

/****************************************************************************
** FUNCTION:sip_getTimeString
** DESCRIPTION: Gets the current time as a string
**
** PARAMETERS:
**             ppString(OUT)    :  Returned time string.
**            Return Value
**            void
**            
**
 ****************************************************************************/
    void                sip_getTimeString
#ifdef ANSI_PROTO
                        (SIP_S8bit ** pString)
#else
                        (pString) SIP_S8bit **pString;
#endif
    {

/*
 * Use the time function according to 
 * the OS
 */
#ifndef SECURITY_KERNEL_WANTED
#ifdef SIP_LINUX
        struct timeval      tv;
        SIP_S8bit          *pTime;
        SIP_S8bit           buffer[26];
#ifndef SIP_FSAP
        gettimeofday (&tv, NULL);
        pTime = ctime_r (&tv.tv_sec, buffer);
        strncpy (*pString, pTime, 26);
#else
        (void) (tv);
        (void) (pTime);
        (void) (buffer);
        UtlGetTimeStr ((CHR1 *) pString);
#endif
#endif
#ifdef SIP_VXWORKS
        struct timespec     tv;
        size_t              size = 100;
        SIP_S8bit           time[100];

        clock_gettime (CLOCK_REALTIME, &tv);
        ctime_r (&tv.tv_sec, &time[0], &size);
        strncpy (*pString, time, 26);
#endif
#else
        UtlGetTimeStr (pString);
#endif
    }

/****************************************************************************
** FUNCTION:STRTOU32LCAP
** DESCRIPTION: converts string to unsigned long.
** caps off pValue to 2^32-1 if larger pValue is given as input. \
**
** PARAMETERS:
**             str(IN)        :  The Module for which memory is allocated.
**            pErr(OUT)    :  The number of bytes of memory to be allocated
**            Return Value
**            SIP_U32bit
**            
**
 ****************************************************************************/

    SIP_U32bit          STRTOU32CAP
#ifdef ANSI_PROTO
                        (SIP_S8bit * str, SipError * pErr)
#else
                        (str, pErr) SIP_S8bit *str;
    SipError           *pErr;
#endif
    {
#ifndef SIP_FSAP
#if defined(SIP_SOLARIS) || defined(SIP_LINUX) || defined(SIP_VXWORKS) || defined(SIP_WINDOWS)

        unsigned long       result;
        errno = 0;
        /*
         * converting string to number
         */
        result = strtoul (str, NULL, 10);
        if (errno == ERANGE)
        {
            if (pErr != SIP_NULL)
                *pErr = E_INV_RANGE;
            return MAX_LENGTH;
        }
        else
        {
            if (pErr != SIP_NULL)
                *pErr = E_NO_ERROR;
            return result;
        }
#endif
#else
        FS_UINT8            result;
        (void) pErr;
        (void) UtlStr2U8 (str, &result);
        return result.u4Lo;
#endif
    }

/*****************************************************************
** FUNCTION:fast_free_synch
** DESCRIPTION: This function frees the mutex.
**
** PARAMETERS:
**            mutex(IN/OUT): mutex id to be freed
**
**
** RETURN VALUE: None
*******************************************************************/
#ifdef SIP_THREAD_SAFE
    void                fast_free_synch (synch_id_t * mutex)
    {
        /* Port according to system and library selection */
#ifndef SIP_FSAP
#ifdef SIP_VXWORKS
        semDelete (*mutex);
#endif
#ifdef SIP_LINUX
        (void) pthread_mutex_destroy (mutex);
#endif
#if defined (SIP_WINDOWS) || defined (SIP_WINCE)
#ifdef SIP_CRITICAL_SECTION
        DeleteCriticalSection (mutex);
#else
        (void) ReleaseMutex (*mutex);
        (void) CloseHandle (*mutex);
#endif
#endif
#else
        OsixSemDel (*mutex);
#endif
        (void) sip_trace (SIP_Detailed, SIP_Init,
                          (SIP_S8bit *) "STACK TRACE: Destroy Mutex Lock");
    }
#endif

/* These are required by Vxworks Port */
/*****************************************************************
        VxWorks port functions
*****************************************************************/
/******************************************************************
vxprintf is a Synchronised Wrapper For printf on VxWorks
***************************************************/

#ifndef SIP_FSAP
#if defined (SIP_THREAD_SAFE) && defined (SIP_VXWORKS)

    SIP_S32bit          vxprintf (const SIP_S8bit * fmt, ...)
    {

        va_list             ap;
        extern synch_id_t   glbPrintMutex;
        SIP_S32bit          dummy = 0;
        fast_lock_synch (dummy, &glbPrintMutex, dummy);
        va_start (ap, fmt);
        vprintf (fmt, ap);
        va_end (ap);
        fast_unlock_synch (dummy, &glbPrintMutex);

        return (1);

    }
#endif
#endif

/*******************************************************************************
** FUNCTION:sip_memcpy
** DESCRIPTION: wrapper function around C memcpy function.
** 
** PARAMETERS:
**            pDest(IN)    : Destination memory area
**            pSrc (IN)     : Source memory area
**            n    (IN)     : n number of bytes to be copied
** 
*******************************************************************************/

    SIP_Pvoid           sip_memcpy
#ifdef ANSI_PROTO
                        (SIP_Pvoid pDest, SIP_Pvoid pSrc, SIP_S32bit dSize)
#else
                        (pDest, pSrc, dSize) SIP_Pvoid pDest;
    SIP_Pvoid           pSrc;
    SIP_U32bit          dSize;
#endif
    {

#ifdef SIP_FSAP
        return MEMCPY (pDest, pSrc, dSize);
#else
        return memcpy (pDest, pSrc, dSize);
#endif
    }

/*******************************************************************************
** FUNCTION:sip_memmove
** DESCRIPTION: wrapper function around C memmove function.
** 
** PARAMETERS:
**            pDest(IN)    : Destination memory area
**            pSrc (IN)     : Source memory area
**            n    (IN)     : n number of bytes to be copied
** 
*******************************************************************************/

    SIP_Pvoid           sip_memmove
#ifdef ANSI_PROTO
                        (SIP_Pvoid pDest, SIP_Pvoid pSrc, SIP_S32bit dSize)
#else
                        (pDest, pSrc, dSize) SIP_Pvoid pDest;
    SIP_Pvoid           pSrc;
    SIP_U32bit          dSize;
#endif
    {

#ifdef SIP_FSAP
        return UtlMemMove (pDest, pSrc, dSize);
#else
        return memmove (pDest, pSrc, dSize);
#endif
    }

/****************************************************************************
** FUNCTION:sip_strtol
** DESCRIPTION: converts string to signed long.
** this function wont caps off pValue to 2^32-1 if larger pValue is
** given as input. 
**
** PARAMETERS:
**             pStr(IN)        :  The string to be converted to signed long.
**          pEndStr(IN/OUT) :  The string in which the first invalid char 
**                               will be placed
**          base            :  The base value of pstr string 
**            Return Value
**                                SIP_S32bit
**            
**
 ****************************************************************************/

    SIP_S32bit          sip_strtol
#ifdef ANSI_PROTO
     
        
        
        
                       (SIP_S8bit * pStr, SIP_S8bit ** pEndStr, SIP_S32bit base)
#else
                        (pStr, pEndStr, base) SIP_S8bit *pStr;
    SIP_S8bit          *pEndStr;
    SIP_S32bit          base;
#endif
    {
#ifdef SIP_FSAP
        return UtlStr2I4 (pStr, pEndStr, base);
#else
        return strtol (pStr, pEndStr, base);
#endif
        /* Capping functionality missing */
    }

/****************************************************************************
** FUNCTION:sip_isdigit
** DESCRIPTION: checks for digits
**
** PARAMETERS:
**             dValue(IN)        :  Value to be checked for digit
**            Return Value
**                               SIP_S32bit
**            
**
 ****************************************************************************/

    SIP_S32bit          sip_isdigit
#ifdef ANSI_PROTO
                        (SIP_S32bit dValue)
#else
                        (dValue) SIP_S32bit dValue;
#endif
    {
#ifdef SIP_FSAP
        return ISDIGIT (dValue);
#else
        return isdigit (dValue);
#endif
        /* Capping functionality missing */
    }

/****************************************************************************
** FUNCTION:sip_isspace
** DESCRIPTION: checks for white space characters
**
** PARAMETERS:
**             dValue(IN)        :  Value to be checked for white space character
**            Return Value
**                               SIP_S32bit
**            
**
 ****************************************************************************/

    SIP_S32bit          sip_isspace
#ifdef ANSI_PROTO
                        (SIP_S32bit dValue)
#else
                        (dValue) SIP_S32bit dValue;
#endif
    {
#ifdef SIP_FSAP
        return UtlisSpace (dValue);
#else
        return isspace (dValue);
#endif
        /* Capping functionality missing */
    }

#ifdef SIP_FSAP

#ifdef VAR_ARGS_SUPPORT

#define ZEROPAD 1                /* pad with zero */
#define SIGN    2                /* unsigned/signed long */
#define PLUS    4                /* show plus */
#define SPACE   8                /* space if plus */
#define LEFT    16                /* left justified */
#define SIP_SPECIAL 32            /* 0x */
#define LARGE   64                /* use 'ABCDEF' instead of 'abcdef' */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtlCnvertNumber                                  */
/*                                                                          */
/*    Description        : Non-exported function called from UtlVsnprintf   */
/*                         Converts the number i4Num to a string based on   */
/*                         input the radix, Fieldwidth, Flags.              */
/*    Input(s)           : pc1Buf - buffer which contains the formatted str */
/*                                  upon return from function call.         */
/*                         i4Num  - The no. to be converted.                */
/*                         i4Base - The radix of i4Num.                     */
/*                         i4Size - The fieldwidth.                         */
/*                         i4Precision - Precision.                         */
/*                         i4Type - Flags set in UtlSnprintf                */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : The buffer pc1Buf containing the string          */
/*                         representation of the number i4Num.              */
/*                                                                          */
/****************************************************************************/
    static CHR1        *UtlCnvertNumber (CHR1 * pc1Buf, INT4 i4Num, INT4 i4Base,
                                         INT4 i4Size, INT4 i4Precision,
                                         INT4 i4Type, INT4 isUnsigned)
    {
        CHR1                c1Pad, c1Sign, ac1TmpBuf[66];
        const CHR1         *pc1Digits;
        const CHR1          ac1SmallDigits[] =
            "0123456789abcdefghijklmnopqrstuvwxyz";
        const CHR1          ac1LargeDigits[] =
            "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        INT4                i4Idx;
        UINT4               u4Remainder;
        UINT4               u4Num;

        pc1Digits = (i4Type & LARGE) ? ac1LargeDigits : ac1SmallDigits;
        if (i4Type & LEFT)
            i4Type &= ~ZEROPAD;
        if (i4Base < 2 || i4Base > 36)
            return (NULL);
        c1Pad = (i4Type & ZEROPAD) ? '0' : ' ';
        c1Sign = 0;
        if (i4Type & SIGN)
        {
            if (i4Num < 0)
            {
                c1Sign = '-';
                i4Num = -i4Num;
                i4Size--;
            }
            else if (i4Type & PLUS)
            {
                c1Sign = '+';
                i4Size--;
            }
            else if (i4Type & SPACE)
            {
                c1Sign = ' ';
                i4Size--;
            }
        }
        if (i4Type & SIP_SPECIAL)
        {
            if (i4Base == 16)
                i4Size -= 2;
            else if (i4Base == 8)
                i4Size--;
        }

        i4Idx = 0;
        if (i4Num == 0)
            ac1TmpBuf[i4Idx++] = '0';
        else
        {
            if (isUnsigned)
            {
                u4Num = (UINT4) i4Num;
                while (u4Num != 0)
                {
                    u4Remainder = u4Num % i4Base;
                    u4Num = u4Num / i4Base;
                    ac1TmpBuf[i4Idx++] = pc1Digits[u4Remainder];
                }
            }
            else
            {
                while (i4Num != 0)
                {
                    u4Remainder = i4Num % i4Base;
                    i4Num = i4Num / i4Base;
                    ac1TmpBuf[i4Idx++] = pc1Digits[u4Remainder];
                }
            }
        }

        if (i4Idx > i4Precision)
            i4Precision = i4Idx;
        i4Size -= i4Precision;
        if (!(i4Type & (ZEROPAD + LEFT)))
        {
            while (i4Size-- > 0)
            {
                *pc1Buf = ' ';
                ++pc1Buf;
            }
        }
        if (c1Sign)
        {
            *pc1Buf = c1Sign;
            ++pc1Buf;
        }
        if (i4Type & SIP_SPECIAL)
        {
            if (i4Base == 8)
            {
                *pc1Buf = '0';
                ++pc1Buf;
            }
            else if (i4Base == 16)
            {
                *pc1Buf = '0';
                ++pc1Buf;
                *pc1Buf = pc1Digits[33];
                ++pc1Buf;
            }
        }
        if (!(i4Type & LEFT))
        {
            while (i4Size-- > 0)
            {
                *pc1Buf = c1Pad;
                ++pc1Buf;
            }
        }
        while (i4Idx < i4Precision--)
        {
            *pc1Buf = '0';
            ++pc1Buf;
        }
        while (i4Idx-- > 0)
        {
            *pc1Buf = ac1TmpBuf[i4Idx];
            ++pc1Buf;
        }
        while (i4Size-- > 0)
        {
            *pc1Buf = ' ';
            ++pc1Buf;
        }
        return (pc1Buf);
    }

/****************************************************************************/
/* Internal function called from UtlVsnprintf.                              */
/* Does an intelligent atoi (by skipping spaces).                           */
/****************************************************************************/

    static INT4         UtlGetNumFrmString (const CHR1 ** s)
    {
        INT4                i4Val = 0;

        while (isdigit (**s))
            i4Val = i4Val * 10 + *((*s)++) - '0';
        return i4Val;
    }

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtlVsprintf                                     */
/*                                                                          */
/*    Description        : An Implementation of vsnprintf function.         */
/*                         Note: This is not a full implementation.         */
/*                         i/e, not all fmt. specifiers are supported       */
/*                         Currently supports 'c' 'd' 'x' 's'.              */
/*                         Using any other fmt. spec. than the above        */
/*                         would result in undefined behaviour.             */
/*                         Supports precision, justification & fieldwidth.  */
/*                                                                          */
/*    Input(s)           : pc1Buf - buffer which contains the formatted str */
/*                                upon return from function call.           */
/*                         u4Sz - Limit of bytes to write.                  */
/*                         c1fmt  - fmt spec.                               */
/*                         args - argument list                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : No. of bytes written.                            */
/*                         or would have been written in case of overflow   */
/*                                                                          */
/****************************************************************************/
    UINT4
         
         
                  UtlVsprintf (CHR1 * pc1Buf, const CHR1 * c1fmt, va_list args)
    {
        UINT4               u4Idx;
        UINT4               u4Len;
        UINT4               u4Flags;
        UINT4               u4Base;
        UINT4               u4Num;
        INT4                i4FieldWidth;
        INT4                i4Precision;
        INT4                i4val;
        INT4                i4Qualifier;
        INT4                i4IsUnsigned;
        CHR1                c1val;
        CHR1                c1FmtFlag;
        CHR1               *pc1Str;
        const CHR1         *pc1StrVal;

        pc1Str = pc1Buf;

        for (; *c1fmt; ++c1fmt)
        {
            if (*c1fmt != '%')
            {
                *pc1Str = *c1fmt;
                ++pc1Str;
                continue;
            }

            /* process flags */
            u4Flags = 0;
          repeat:
            ++c1fmt;            /* this also skips first '%' */
            switch (*c1fmt)
            {
                case '-':
                    u4Flags |= LEFT;
                    goto repeat;
                case '+':
                    u4Flags |= PLUS;
                    goto repeat;
                case ' ':
                    u4Flags |= SPACE;
                    goto repeat;
                case '#':
                    u4Flags |= SIP_SPECIAL;
                    goto repeat;
                case '0':
                    u4Flags |= ZEROPAD;
                    goto repeat;
            }

            /* get field width */
            i4FieldWidth = -1;
            if (isdigit (*c1fmt))
                i4FieldWidth = UtlGetNumFrmString (&c1fmt);
            else if (*c1fmt == '*')
            {
                ++c1fmt;
                /* it's the next argument */
                i4FieldWidth = va_arg (args, int);
                if (i4FieldWidth < 0)
                {
                    i4FieldWidth = -i4FieldWidth;
                    u4Flags |= LEFT;
                }
            }

            /* get the precision */
            i4Precision = -1;
            if (*c1fmt == '.')
            {
                ++c1fmt;
                if (isdigit (*c1fmt))
                    i4Precision = UtlGetNumFrmString (&c1fmt);
                else if (*c1fmt == '*')
                {
                    ++c1fmt;
                    /* it's the next argument */
                    i4Precision = va_arg (args, int);
                }
                if (i4Precision < 0)
                    i4Precision = 0;
            }

            /* get the conversion qualifier */
            i4Qualifier = -1;
            i4IsUnsigned = 0;
            if (*c1fmt == 'h' || *c1fmt == 'l' || *c1fmt == 'L')
            {
                i4Qualifier = *c1fmt;
                i4IsUnsigned = 1;
                ++c1fmt;
            }

            /* Merely transfer any sequence of %s */
            if (*c1fmt == '%')
            {
                c1FmtFlag = 0;
                while (*c1fmt == '%')
                {
                    if (!c1FmtFlag)
                    {
                        *pc1Str = '%';
                        ++pc1Str;
                        c1FmtFlag = 1;
                    }
                    else
                    {
                        c1FmtFlag = 0;
                    }
                    ++c1fmt;
                }
                /* To balance the normal increment in the for loop above */
                --c1fmt;
                if (!c1FmtFlag)
                    --c1fmt;
                continue;
            }

            /* default base */
            u4Base = 10;

            switch (*c1fmt)
            {
                case 'c':
                    if (!(u4Flags & LEFT))
                    {
                        while (--i4FieldWidth > 0)
                        {
                            *pc1Str = ' ';
                            ++pc1Str;
                        }
                    }

                    c1val = (unsigned char) va_arg (args, int);
                    *pc1Str = c1val;
                    ++pc1Str;
                    while (--i4FieldWidth > 0)
                    {
                        *pc1Str = ' ';
                        ++pc1Str;
                    }
                    continue;

                case 'd':
                    u4Flags |= SIGN;

                case 'u':
                    i4IsUnsigned = 1;
                    break;

                case 'X':
                    u4Flags |= LARGE;
                case 'x':
                    u4Base = 16;
                    break;

                case 's':
                    pc1StrVal = va_arg (args, char *);
                    if (!pc1StrVal)
                        pc1StrVal = "<null>";

                    u4Len = STRNLEN (pc1StrVal, i4Precision);

                    if (!(u4Flags & LEFT))
                    {
                        while ((INT4) u4Len < i4FieldWidth--)
                        {
                            *pc1Str = ' ';
                            ++pc1Str;
                        }
                    }

                    for (u4Idx = 0; u4Idx < u4Len; u4Idx++)
                    {
                        *pc1Str = *pc1StrVal;
                        ++pc1Str;
                        ++pc1StrVal;
                    }
                    while ((INT4) u4Len < i4FieldWidth--)
                    {
                        *pc1Str = ' ';
                        ++pc1Str;
                    }
                    continue;

                case 'f':
                case 'e':
                    /* float and double are not supported. Swallow 8 bytes */
                    i4val = (UINT4) va_arg (args, int);
                    i4val = (UINT4) va_arg (args, int);
                    continue;

                default:
                    /* Assume it is 4 bytes and swallow it. */
                    i4val = (UINT4) va_arg (args, int);
                    continue;
            }

            if (i4Qualifier == 'l')
            {
                u4Num = va_arg (args, UINT4);
            }
            else if (i4Qualifier == 'h')
            {
                u4Num = (UINT2) va_arg (args, INT4);
            }
            else
            {
                u4Num = va_arg (args, INT4);
            }

            pc1Str = UtlCnvertNumber (pc1Str, u4Num, u4Base,
                                      i4FieldWidth, i4Precision, u4Flags,
                                      i4IsUnsigned);
        }

        va_end (args);

        /* Null terminate */
        *pc1Str = '\0';

        /* Return the number of characters that is written
         * This is as per the C99 standard and the snprintf man page on Linux
         */
        return STRLEN (pc1Buf);
    }
#endif

/****************************************************************************/
/*    Function Name      : UtlStr2I4                                        */
/*                                                                          */
/*    Description        : Convert a string to its INT4 representaiton.    */
/*                                                                          */
/*    Input(s)           : pc1Str - String to be converted.                 */
/*                         i4base - Base value of pc1Str                    */
/*                                                                          */
/*    Output(s)          : pc1End - String to store the first invalid       */
/*                                   character                              */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
    INT4                UtlStr2I4 (CHR1 * pc1Str, CHR1 ** pc1End, INT4 i4base)
    {
        CHR1               *s = pc1Str;
        INT4                i4Base = i4base;
        INT4                i4c;
        INT4                i4Sign = 0;
        INT4                i4Result = 0;

        /* Argument check */
        if ((i4Base != 0) && (i4Base < 2 || i4Base > 36))
            return 0;

        /* skip any spaces */
        do
        {
            i4c = *s++;
        }
        while (i4c == ' ');

        /* Sign check */
        if (i4c == '+')
            i4c = *s++;
        else if (i4c == '-')
        {
            i4Sign = 1;
            i4c = *s++;
        }

        /* check if it is a hexadecimal no */
        if (i4c == '0' && (*s == 'x' || *s == 'X'))
        {
            i4c = s[1];
            s += 2;
            i4Base = 16;
        }

        if (i4c == 0)
        {
            return 0;
        }

        /* it is either octal or decimal */
        if (i4Base == 0)
            i4Base = i4c == '0' ? 8 : 10;

        /* Get the leftmost digit */
        for (;; i4c = *s++)
        {
            if (isdigit (i4c))
                i4c -= '0';
            else if (isalpha (i4c))
                i4c -= isupper (i4c) ? 'A' - 10 : 'a' - 10;
            else
            {
                if (pc1End != NULL)
                    *pc1End = s - 1;
                break;
            }

            if (i4c >= i4Base)
            {
                if (pc1End != NULL)
                    *pc1End = s - 1;
                break;
            }

            i4Result = (i4Result * i4Base) + i4c;
        }

        if (i4Sign == 1)
            return -i4Result;
        else
            return i4Result;
    }

/****************************************************************************/
/*    Function Name      : UtlisSpace                                       */
/*                                                                          */
/*    Description        : Checks for white space character                 */
/*                                                                          */
/*    Input(s)           : i4Num  - input to be checked for white space     */
/*                                                                          */
/*    Output(s)          : OSIX_TRUE/OSIX_FALSE                             */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
    INT4                UtlisSpace (INT4 i4Num)
    {
        switch (i4Num)
        {
            case 0x9:
            case 0xA:
            case 0xB:
            case 0xC:
            case 0xD:
            case 0x20:
                return OSIX_TRUE;
        }
        return OSIX_FALSE;
    }

/****************************************************************************/
/*    Function Name      : UtlMemMove                                       */
/*                                                                          */
/*    Description        : Copies n bytes of memory area from src to dest   */
/*                                                                          */
/*    Input(s)           : dest  - destination memory area to which the     */
/*                                 copy is to be done                       */
/*                            src  - source memory area from where the copy   */
/*                                    copy is to be done                      */
/*                              n  - number of bytes to be copied             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : dest - the copied bytes                          */
/****************************************************************************/
    VOID               *UtlMemMove (VOID *Dest, VOID *Src, size_t n)
    {
        CHR1               *dest = (CHR1 *) Dest;
        CHR1               *src = (CHR1 *) Src;

        if (dest <= src)
        {
            INT4                i4count = 0;
            while (n > 0)
            {
                dest[i4count] = src[i4count];
                i4count++;
                n--;
            }
        }
        else
        {
            while (n > 0)
            {
                n--;
                dest[n] = src[n];
            }
        }

        return Dest;
    }

#endif

#ifdef __cplusplus
}
#endif
