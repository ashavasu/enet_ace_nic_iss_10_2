
 /******************************************************************************
 ** FUNCTION:
 **	 This file has all the source for init functions for Instant Messaging 
 **	 and Presence Related Headers
 **
 ******************************************************************************
 **
 ** FILENAME:
 ** 		imppinit.c
 **
 ** $Id: microsip_imurlinit.c,v 1.1.1.1 2010/12/08 06:10:43 siva Exp $ 
 **
 ** DESCRIPTION:
 **	 
 **
 ** DATE		NAME				REFERENCE	REASON
 ** ----		----				--------	------
 ** 16/08/2002	Mahesh							Original
 **
 **
 **	 Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************/
#include "microsip_imurlinit.h"
#include "microsip_init.h"
/******************************************************************************
**** FUNCTION:	sip_initImUrl
****
****
**** DESCRIPTION:
*******************************************************************************/
SipBool sip_initImUrl(ImUrl **ppIm,SipError *pErr)
{
	*ppIm = (ImUrl *) fast_memget(0, sizeof(ImUrl),pErr);
	if (*ppIm==SIP_NULL)
		return SipFail;
	INIT((*ppIm)->pDispName);
	INIT((*ppIm)->pUser);
	INIT((*ppIm)->pHost);
	(void)sip_listInit(& ((*ppIm)->slRoute),__sip_freeString, pErr);
	(void)sip_listInit(& ((*ppIm)->slParams),__sip_freeSipParam,pErr);
	HSS_INITREF((*ppIm)->dRefCount);
	return SipSuccess;
}

