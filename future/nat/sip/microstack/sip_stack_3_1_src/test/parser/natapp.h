/********************************************************************
 * Copyright (C) Aricent Communications Sotware,2007
 *
 * $Id: natapp.h,v 1.1.1.1 2010/12/08 06:10:43 siva Exp $ 
 *
 * Description:This file contains the data structure typedefs
 *             used by SIP ALG 
 *******************************************************************/
#ifdef __cplusplus
extern "C" {
#endif 

#ifndef _NAT_SIP_H
#define _NAT_SIP_H

#include "natsipdefs.h"

SipBool sipAlgTranslateSipMsg (SIP_U8bit *pSipBuf, SipAlgPktInOut dInOut);

SipBool sipAlgTranslateIncomingHeaders (SipMessage *pSipMsg); 

SipBool sipAlgTranslateOutgoingHeaders (SipMessage *pSipMsg); 

SipBool
sipAlgTranslateReqUri(SipMessage *pSipMsg);

SipBool
sipAlgTranslateViaHdr(SipMessage *pSipMsg);

SipBool
sipAlgTranslateContactHdr(SipMessage *pSipMsg);

SipBool
sipAlgTranslateRouteHdr(SipMessage *pSipMsg);

SipBool
sipAlgTranslateRecordRouteHdr(SipMessage *pSipMsg);

SipBool
sipAlgTranslateMedia (SipMessage *pSipMsg); 

SipBool sipAlgIsNumericAddress(SIP_U8bit *pNumStr);


#endif 
