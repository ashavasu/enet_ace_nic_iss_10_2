/********************************************************************
 * Copyright (C) Aricent Communications Sotware,2007
 *
 * $Id: natsipdefs.h,v 1.1.1.1 2010/12/08 06:10:43 siva Exp $ 
 *
 * Description:This file contains the data structure typedefs
 *             used by SIP ALG 
 *******************************************************************/

#ifndef _NAT_SIP_DEFS_H
#define _NAT_SIP_DEFS_H

#include "microsip_common.h"
#include "microsip_struct.h"
#include "microsip_list.h"

typedef enum SipAlgPktInOut    
{
  SipPktOut = 0,
  SipPktIn = 1
} SipAlgPktInOut ;


//--------------------------------------------------------------------
// SipNatInfo :
// 			This is the data structure defined by the ALG to store the
// 			NAT information. It groups all the information which are
// 			commonly used by both media hash table as well as NAT
// 			Hash Table maintained in the ALG.It contains private IP and 
//			port, Translated IP and port and Outside IP and port.
//--------------------------------------------------------------------
typedef struct SipNatInfo
{
//original IP address contain  private IP address if the request is 
//originated from private end user, otherwise it contains the public IP
//address.
	SIP_S8bit			*pOriginalIP;
//original port  contain  private port if the request is originated from 
//private end user, otherwise it contains the public port
//address.
	SIP_U16bit       originalPort;
//Mapped IP address contain  translated IP address if the request is 
//originated from private end user, otherwise it contains NULL.
	SIP_S8bit	       	*pMappedIP;	
//Mapped port contain  translated port if the request is 
//originated from private end user, otherwise it contains NULL.
	SIP_U16bit       mappedPort;	
//It will contain the destination IP address. This will be present in
//the signalling case the moment binding or pinhole API is invoked.
//But For media, It will be updated only after receiving the answer.
	SIP_S8bit	       	*pOutsideIP;	
//It will contain the destination port. This will be present in
//the signalling case the moment binding or pinhole API is invoked.
//But For media, It will be updated only after receiving the answer.
	SIP_U16bit       outsidePort;	

}SipNatInfo;


//--------------------------------------------------------------------
//SipAlgNATHashKey:
//		This is the data structure used as a key for the NAT
//		Hash Table 	maintained in the ALG. It contains IP address and port 
//		as a key.
//--------------------------------------------------------------------
typedef struct SipAlgNATHashKey
{
	// If the message is from public end, then the IP is translated IP.
	// Otherwise it is private IP.
	SIP_U32bit	dIPAddress;

	// If the message is from public end, then port is translated port. 
	//Otherwise it is private port.
	SIP_U16bit	port;

} SipAlgNATHashKey;

//--------------------------------------------------------------------
//SipAlgMediaHashKey:
//		This is the data structure used as a key for the Media Hash Table 
//		maintained in the ALG. It contains From Tag, To Tag and Call-ID 
//		as a key.
//--------------------------------------------------------------------
typedef struct SipAlgMediaHashKey
{
	//From Tag in SIP Message
	SIP_S8bit	*pFromTag;

	//To Tag in SIP Message
	SIP_S8bit	*pToTag;

	//CAll-ID in SIP Message
	SIP_S8bit	*pCallID;

} SipAlgMediaHashKey;

//--------------------------------------------------------------------
//SipAlgNATHashElement:
//		This is the data structure which the NAT hash table maintains   
//--------------------------------------------------------------------
typedef struct SipAlgNATHashElement
{
	
	// If the message is from public end, then the IP is translated IP.
	// Otherwise it is private IP.
	SIP_S8bit	*pOriginalIP;

	// If the message is from public end, then port is translated port. 
	// Otherwise it is private port.
	SIP_U16bit	originalPort;

	// Translated IP address.
	SIP_S8bit	*pMappedIP;

	// Translated Port - For media it will be UDP for contacts 
	// the value will be based on transport parameter.
	SIP_U16bit	mappedPort;

	// Protocol
	SIP_U16bit	protocol;

	// List of destination IP and Port
	SipList		*pDestInfoList;

	// Sip Alg timer information
	/*SipAlgTimer		sipAlgTimerInfo;*/

	SipBool 		isRegTimerStarted;

	

	// List of ongoing calls
	SipList 		*pCallInfoList;
	
} SipAlgNATHashElement;

//--------------------------------------------------------------------
// SipMediaInfo :
// 			This is the datastructure defined by the ALG to store the
// 			media level information.
// 			This is the datastructure which contains media level
// 			information. It stores the m-line index as well as the Original
// 			IP and port present in the SDP's C-line and m-line, Translated
// 			IP and port , Outside IP and port.
//--------------------------------------------------------------------

typedef struct SipMediaInfo 
{
	//This is used for identifing the m-line index.It maintain the
	//m-lineindex.
	SIP_U16bit       mLineIndex;
	//Nat information for this particular m-line index.
	SipNatInfo	*pNatInformation;
	
} SipMediaInfo;

//--------------------------------------------------------------------
// SipSignallingInfo :
// 			This is the datastructure defined by the ALG to store the
// 			SIP signalling information.This is the datastructure which
// 			contains SipNatInfo which has  the Original	IP and port , 
// 			Translated	IP and port  and  Outside IP and port.
//--------------------------------------------------------------------
typedef struct SipSignallingInfo
{
	SipNatInfo *pNatInformation;
	
} SipSignallingInfo;



//--------------------------------------------------------------------
// SipsessionHashElement :
// 			This is the datastructure which defines the data attribute
// 			needed by the session Hash element.This contains the list of the 
// 			signalling information and list of  media level
// 			information .It is used by ALG in the session hash table.
//--------------------------------------------------------------------
typedef struct SipSessionHashElement
{
	//List of SipSignallingInfo
	SipList *pSignallingInfoList;
	//List of  SipSignallingInfo 
	SipList  *pMediaInfoList;
	
}SipSessionHashElement;

//--------------------------------------------------------------------
//SipalgSessionKey:
//		This is the datastructure used as a key for the Session Hash Table 
//		maintained in the ALG. It contains From Tag, To Tag and Call-ID 
//		as a key.
//--------------------------------------------------------------------
typedef struct SipAlgSessionKey
{
	//From Tag in SIP Message
	SIP_S8bit	*pFromTag;
	//To Tag in SIP Message
	SIP_S8bit	*pToTag;
	//CAll-ID in SIP Message
	SIP_S8bit	*pCallID;
} SipAlgSessionKey;



/*REGISTRATION DATA STRUCTURE */

typedef struct SipRegContactInfo
{
	SipNatInfo	*pNatInformation;
	//NAT Idle Timer Vaule
	SIP_U16bit	natIdleTimerValue;
	// Start Time Stamp of  NAT idle Timer for this binding.
	SIP_U16bit	natIdleTimerStartTimeStamp;
	
} SipRegContactInfo;


//--------------------------------------------------------------------
//SipAlgRegContactKey:
//		This is the datastructure used as a key for the Registration
//		Hash Table 	maintained in the ALG. It contains IP address and port 
//		as a key.
//--------------------------------------------------------------------
typedef struct SipAlgRegContactKey
{
	// From Tag in SIP Message if the message is from 
// public end, then IP address is translated IP.
// Otherwise it is private IP.
	SIP_S8bit	*pIPAddress;
	// If the message is from public end, then port
     // is translated port. Otherwise it is private port.
	SIP_U16bit	port;

} SipalgRegContactKey;

typedef enum
{
    en_odd=1,
    en_even=2

}en_ParityOfPort;



typedef enum
{
    en_any = 0,
    en_tcp=1,
    en_udp=2
        
}en_Protocol;


typedef enum SipHdrType
{
  SipHdrReqUri,
  SipHdrVia,
  SipHdrContact,
  SipHdrRoute,
  SipHdrRecordRoute

}SipHdrType;

//-------------------------------------------------------
// SipAlgHeaderInfo:
// This is the data structure that contains the SIP 
// header type along with IP address and port which
// needs ALG operation
//-------------------------------------------------------
typedef struct SipAlgHeaderInfo
{
	// SIP header type
	SipHdrType	headerType;
	// IP address 
	SIP_S8bit	*pIPAddress;
	// Port 
	SIP_U16bit	port;
} SipAlgHeaderInfo;

/* enum for sdp type or xml type*/
typedef enum SipMessageBodyType 
{
//SDP message Type
	SipSdpMessage,
 	// All xml related message Type
	SipXmlMessage

}SipMessageBodyType ;

typedef enum Sip_aValueInSdp    
{
//SDP a line is inactive
	sendrecv,
//SDP a line is active
	active,
//SDP a line is send only
	sendonly,
//SDP a line is recv only
	recvonly,
//SDP a line is send and recv
	inactive

} Sip_aValueInSdp  ;


//----------------------------------------------------------
// SipAlgMediaInfo:
// This is the data structure that contains the IP address 
// and port which needs ALG operation in message body
//----------------------------------------------------------
typedef struct SipAlgMediaInfo
{
	// SIP header type
	/*SipMessageBodyType     messageBodyType;*/
	SIP_U16bit       mLineIndex;
  // IP address 
	SIP_S8bit	*pIPAddress;
	// Port 
	SIP_U16bit	port;
	Sip_aValueInSdp  aValueInSdp;
} SipAlgMediaInfo;


#endif /* _NAT_SIP_DEFS_H */

