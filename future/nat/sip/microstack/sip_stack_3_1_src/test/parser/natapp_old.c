/*nat sip utils file */
/** $Id: natapp_old.c,v 1.1.1.1 2010/12/08 06:10:43 siva Exp $ **/


#ifdef __cplusplus
extern "C" {
#endif

//#include <netinet/in.h>
//#include <arpa/inet.h>
//#include <sys/socket.h>
//#include <sys/time.h>
//#include <stdio.h>
//#include <unistd.h>
//#include <time.h>
//#include <sys/stat.h>
//#include <fcntl.h>
#include <string.h>

//#include <sys/types.h>
//#include <stdlib.h>
#include "microsip_portlayer.h"
#include "natapp.h"
#include "natsipdefs.h"
#include "microsip_decode.h"
#include "microsip_struct.h"
#include "microsip_sendmessage.h"
#include "microsip_list.h"
#include "microsip_init.h"
#include "microsip_free.h"
#include "microsip_accessors.h"
#include "microsip_statistics.h"
#include "microsip_timer.h"
#include "microsip_trace.h"
#include "microsip_startline.h"
#include "microsip_subapi.h"
#include "microsip_header.h"
#include "microsip_msgbody.h"

/////////////////////////////////////////////////////////////
// Function Name : sipAlgUpdateSipMsgHdr
// Description  : This function updates the sip message
//                with the translated ip/port for a specified
//                specified header  
// Input/output (s)  : pSipMessage -Incoming Sip message
// Input (s) : translated ip/port, header type
// Returns    : SUCCESS/FAILURE
///////////////////////////////////////////////////////////
#if 0
SipBool
sipAlgUpdateSipMsgHdr (SipMessage **pSipMsg, 
              SipAlgIpPort *pTranslatedIpPort, SipHdrType dSipHdr )
{
  SipError    derror;
  
  switch(dSipHdr)
  {
    case SipHdrReqUri:
    {
      
    }
    break;
    case SipHdrVia:
    {
      
    }
    break;
    case SipHdrContact:
    {
    }
    break;
    case SipHdrRoute:
    {
    }
    break;
    case SipHdrRecordRoute:
    {
    }
    break;

  }  
  return SipSuccess;
}
#endif 

/////////////////////////////////////////////////////////////
// Function Name : sipAlgGetMessageBodyInfo
// Description  : This function gets the all the media info 
// 			 which requires ALG translation for a SIP
//  			 message. 
//                 
// Input (s)  : pSipMessage -Incoming Sip message
// Output (s) : pSLSipAlgMediaInfo - contains the list of header
//		     type and their corresponding IP address and port
// Returns    : SUCCESS/FAILURE
///////////////////////////////////////////////////////////
SipBool
sipAlgGetMessageBodyInfo (SipMessage *pSipMsg, 
               SipList *slSipAlgMediaInfo)
{
  SipError    derror;
  SipAlgMediaInfo *pMediaInfo[5];
  SIP_U32bit i=0;
	SipMsgBody *pMsgBody = SIP_NULL;
	SipBool sdpFound = SipFail;
	SIP_U32bit dCount=0,dMediaCount=0,dAttrCount=0; 
  SdpMessage *pSdpMsg;
  SdpConnection *pSessionConnection = SIP_NULL;
  SIP_U8bit *pConnectionIP = SIP_NULL;
  Sip_aValueInSdp dSessionLevelDirAttr = 0;
  
  /* This block of code is to find whether SDP msg body is present
     in the SIP message */
  if(sip_getMsgBodyCount(pSipMsg, &dCount, \
		          &derror) == SipFail)
  {
    return SipFail;  
  }  
  
  if(dCount == 0)
  {
    /* No message body present in the sip message */
  }
  
  for (i = 0; i < dCount; i++)
	{
		en_SipMsgBodyType dType;
		if (sip_getMsgBodyTypeAtIndex(pSipMsg, \
			&dType, i, &derror) == SipFail)
		{
      return SipFail;
		}
		if (dType == SipSdpBody)
		{
			if (sip_getMsgBodyAtIndex(pSipMsg, \
				&pMsgBody, i, &derror) == SipFail)
			{
				return SipFail;
			}
			sdpFound = SipSuccess;	
			break;	
		}
	}
	if (sdpFound == SipFail)
	{
		/* free local variables */
	  sip_freeSipMsgBody(pMsgBody);
		return SipFail;
	}

	/* 	=========================================
		  get the "incoming" SdpMessage struct
		========================================= */
	if (sip_getSdpFromMsgBody(pMsgBody, &pSdpMsg, \
		&derror) == SipFail)
	{
	  /* free local variables */
	  sip_freeSipMsgBody(pMsgBody);

		return SipFail;
	}
	/* free local variables */
	sip_freeSipMsgBody(pMsgBody);
  
  /* get the media count from SDP */
  if(sdp_getMediaCount(pSdpMsg,&dMediaCount,\
			 &derror)==SipFail)
	{
     return SipFail; 
  } 
  if (dMediaCount == 0)
  {
    return SipFail;
  }  
  /* get the connection line from the sdp-msg body */
  if(sdp_getConnection(pSdpMsg,&pSessionConnection,\
			&derror)==SipFail)
  {
     return SipFail; 
  }  
  sip_strcpy(pConnectionIP, pSessionConnection->pAddr);
  
  /* get the session level direction attribute from sdp */
  if(sdp_getAttrCount(pSdpMsg,&dAttrCount,\
			    &derror)==SipFail)
	{
		return SipFail;
	}
	for(i=0;i<dAttrCount;i++)	
	{
		SdpAttr			*pTempAttr=SIP_NULL;
		SIP_S8bit	*pName=SIP_NULL;

		/*Get the Attribute*/
		if(sdp_getAttrAtIndex(pSdpMsg,&pTempAttr,i,\
							&derror)==SipFail)
		{
			return SipFail;
		}
		if(sdp_getNameFromAttr(pTempAttr,&pName,\
			          &derror)==SipFail)
		{
			sip_freeSdpAttr(pTempAttr);
			return SipFail;
		}
		if(sip_strcasecmp(pName,"sendonly")==0)
		{
      dSessionLevelDirAttr = sendonly;
      break;
		}else if(sip_strcasecmp(pName,"recvonly")==0)
		{
      dSessionLevelDirAttr = recvonly;
      break;
		}else if(sip_strcasecmp(pName,"sendrecv")==0)
		{
      dSessionLevelDirAttr = sendrecv;
      break;
		}else if(sip_strcasecmp(pName,"inactive")==0)
		{
      dSessionLevelDirAttr = inactive;
      break;
		}
  } 
  /* now iterate thru all the media lines */
  for(i=0; i < dMediaCount; i++)
	{
		SdpMedia	*pSdpMedia=SIP_NULL;
    SIP_U32bit connectionLines=0, dAttrCount=0,j=0;
    SIP_U8bit *pConnIPInMedia = SIP_NULL;
    SIP_U16bit dPort=0;
    Sip_aValueInSdp dMediaLevelDirAttr = 0;
    
    /* get the media at the given index */
    if(sdp_getMediaAtIndex(pSdpMsg,&pSdpMedia,i,\
			&derror)==SipFail)
		{
      return SipFail;
    }
    /* check if there is any connection line present in this 
       media*/
    if(sdp_getConnectionCountFromMedia(pSdpMedia,&connectionLines,\
					&derror)== SipFail)
    {
      return SipFail;
    }  
    if(connectionLines !=  0)
    { 
      SdpConnection	*pConnection;
	  	if(sdp_getConnectionAtIndexFromMedia(pSdpMedia,&pConnection,0,\
					&derror)==SipFail)
      {
        return SipFail;
      }  
      sip_strcpy(pConnIPInMedia, pConnection->pAddr);
    } 
    if(sdp_getPortFromMedia(pSdpMedia,&dPort,\
		        &derror)==SipFail)
    {
      return SipFail;
    } 
    
    /* now we need to get the direction attribute from the media */
    if(sdp_getAttrCountFromMedia(pSdpMedia,&dAttrCount,
			        &derror)==SipFail)
    {
      return SipFail;
    }  
    for (j=0; j<dAttrCount; j++)
    {
        SdpAttr			*pMediaAttr=SIP_NULL;
        SIP_U8bit   *pAttrName = SIP_NULL;

        if(sdp_getAttrAtIndexFromMedia(pSdpMedia,&pMediaAttr,j,\
					&derror)==SipFail)
        {
          return SipFail;
        } 
        if(sdp_getNameFromAttr(pMediaAttr,&pAttrName,\
				       &derror)==SipFail)
		  	{
          sip_freeSdpAttr(pMediaAttr);
          return SipFail;
        }  
        if(sip_strcasecmp(pAttrName,"sendonly")==0)
		    {
          dMediaLevelDirAttr = sendonly;
          break;
	    	}else if(sip_strcasecmp(pAttrName,"recvonly")==0)
		    {
          dMediaLevelDirAttr = recvonly;
          break;
	      }else if(sip_strcasecmp(pAttrName,"sendrecv")==0)
		    {
          dMediaLevelDirAttr = sendrecv;
          break;
	    	}else if(sip_strcasecmp(pAttrName,"inactive")==0)
		    {
          dMediaLevelDirAttr = inactive;
          break;
		    }
    }  
    
    /* now allocate memory to the Mediainfo strucute*/
    pMediaInfo[i]=(SipAlgMediaInfo *)fast_memget\
						(0,sizeof(SipAlgMediaInfo),&derror);
    
    if (pMediaInfo[i] == SIP_NULL)
		  return SipFail;
      
    pMediaInfo[i]->mLineIndex = i;
    pMediaInfo[i]->port = dPort;
    if (connectionLines !=  0)
      pMediaInfo[i]->pIPAddress=sip_strdup(pConnIPInMedia,APP_MEM_ID);
    else
      pMediaInfo[i]->pIPAddress=sip_strdup(pConnectionIP,APP_MEM_ID);
    if (dAttrCount != 0)
      pMediaInfo[i]->aValueInSdp = dMediaLevelDirAttr;
    else
      pMediaInfo[i]->aValueInSdp = dSessionLevelDirAttr;
    /* add the element into the list*/
    if( sip_listAppend( slSipAlgMediaInfo, \
		(SIP_Pvoid) pMediaInfo[i], &derror) == SipFail)
  	{
	   	return SipFail;
  	}	

  } 
  
  return SipSuccess;
}  
#if 0
/////////////////////////////////////////////////////////////
// Function Name : sipAlgGetOutHeaderInfo
// Description  : This function gets the all the SIP 
// 			 headers which requires ALG translation for an 
//  			 outbound message. 
//                 
// Input (s)  : pSipMessage - Outgoing Sip message
// Output (s) : pSLSipAlgHeaderInfo - contains the list of header
//		     type and their corresponding IP address and port
// Returns    : SUCCESS/FAILURE
///////////////////////////////////////////////////////////
SipBool
sipAlgGetOutHeaderInfo (SipMessage *pSipMsg, 
               SipList *slSipAlgHeaderInfo)
{
  
  SipAlgHeaderInfo *pViaInfo, *pRecordRouteInfo, *pContactInfo[5];
  SipError    derror;

  /* If it is outgoing request then translation needs to done on
     Via and the contact headers */
  if (pSipMsg->dType == SipMessageRequest)
  {
     SIP_S8bit *pTempViaSentBy  = SIP_NULL;
     SIP_S8bit  *pViaSentBy		= SIP_NULL;
     SIP_S8bit *pTempPort			= SIP_NULL;
     SIP_S8bit dTempArray[100];
     SipHeader  dTempViaHeader;


     /* get the ip and port from the via header */
     if (sip_getHeaderAtIndex(pSipMsg, SipHdrTypeVia, \
					&dTempViaHeader, 0, &derror) == SipFail)
     {
        return SipFail;
     }
     if (sip_getSentByFromViaHdr(&dTempViaHeader, &pTempViaSentBy, \
		         &derror) == SipFail)
     {
        return SipFail;
     }
     sip_strcpy(dTempArray, pTempViaSentBy);

     /* Here dTempArray contains either the domain name or ip:port
       if it is ip:port, tokenize it and store ip and port */

     pViaSentBy = strtok_r(dTempArray, ":",&pTempPort);
    
     /* allocate memory for viainfo*/
     pViaInfo=(SipAlgHeaderInfo *)fast_memget\
						(0,sizeof(SipAlgHeaderInfo),&derror);
   
     if (pViaInfo == SIP_NULL)
		  return SipFail;
    
      pViaInfo->pIPAddress = sip_strdup(pViaSentBy, APP_MEM_ID);
      pViaInfo->port = atoi(pTempPort);
    
      /* add the element into the list*/
      if( sip_listAppend( slSipAlgHeaderInfo, \
		             (SIP_Pvoid) pViaInfo, &derror) == SipFail)
      {
	     	return SipFail;
      }	
  }   
  else
  {
  } 
  return SipSuccess;
}
#endif
/////////////////////////////////////////////////////////////
// Function Name : sipAlgGetInHeaderInfo
// Description  : This function identifies all the SIP 
// 			 headers which requires ALG translation for an 
//  			 inbound message. 
//                 
// Input (s)  : pSipMessage -Incoming Sip message
// Output (s) : slSipAlgHeaderInfo - contains the list of header
//		     type and their corresponding IP address and port
// Returns    : SUCCESS/FAILURE
///////////////////////////////////////////////////////////

SipBool sipAlgTranslateIncomingHeaders (SipMessage *pSipMsg, 
               SipList *slSipAlgHeaderInfo)
{
  SipAlgHeaderInfo *pReqUriInfo, *pViaInfo, *pRouteInfo, *pRecordRouteInfo;
  SipReqLine *pReqline = SIP_NULL;
  SipAddrSpec		    *pAddressSpec = SIP_NULL;
  SipAddrSpec 	*pAddrSpec = SIP_NULL;
  SipUrl             *pSipUrl  =  SIP_NULL, *pSipUrl1 =SIP_NULL; 
  SipError    derror;
  SipHeader  dTempViaHeader;
  SipHeader		dSipHeader;
  SIP_U32bit size =0;

  
  if (pSipMsg->dType == SipMessageRequest)
  { 
    /* if the incoming message is a request then translation needs to be 
     performed only on Request URI and Route header if present*/
    
    /* get the ip and port from the request uri */
    sip_getReqLineFromSipReqMsg(pSipMsg,&pReqline, \
                &derror);
    sip_getAddrSpecFromReqLine(pReqline,&pAddressSpec, \
                &derror);
    if (sip_getUrlFromAddrSpec(pAddressSpec,&pSipUrl,
                    &derror) == SipFail )
    {
        //TODO       
    }
    /* allocate memory for pReqline*/
    pReqUriInfo=(SipAlgHeaderInfo *)fast_memget\
						(0,sizeof(SipAlgHeaderInfo),&derror);
   
   if (pReqUriInfo == SIP_NULL)
		  return SipFail;

    //pReqUriInfo->headerType = ;
    pReqUriInfo->pIPAddress = sip_strdup(pSipUrl->pHost, APP_MEM_ID);
    pReqUriInfo->port = *(pSipUrl->dPort);
    
    /* add the element into the list*/
    if( sip_listAppend( slSipAlgHeaderInfo, \
		(SIP_Pvoid) pReqUriInfo, &derror) == SipFail)
  	{
	   	return SipFail;
  	}	

    /* Get the Route header info */
    sip_getHeaderCount(pSipMsg,SipHdrTypeRoute, &size, \
					&derror);

	  if (size != 0)
	  {
	  	 (void)sip_getHeaderAtIndex(pSipMsg, SipHdrTypeRoute,
					&dSipHeader, 0, &derror);

			 (void)sip_getAddrSpecFromRouteHdr(&dSipHeader, \
					&pAddrSpec, &derror);

       if (sip_getUrlFromAddrSpec(pAddressSpec,&pSipUrl1,
                    &derror) == SipFail )
       {
          //TODO       
       }
       /* allocate memory for pReqline*/
       pRouteInfo=(SipAlgHeaderInfo *)fast_memget\
						(0,sizeof(SipAlgHeaderInfo),&derror);
   
       if (pRouteInfo == SIP_NULL)
		       return SipFail;

       pRouteInfo->pIPAddress = sip_strdup(pSipUrl->pHost, APP_MEM_ID);
       pRouteInfo->port = *(pSipUrl1->dPort);
       /* add the element into the list*/
      if( sip_listAppend( slSipAlgHeaderInfo, \
		            (SIP_Pvoid) pRouteInfo, &derror) == SipFail)
  	  {
	      	return SipFail;
  	  }	

     } 

  }
  else
  {  
    /* if the incoming message is a response then translation needs to be 
     performed only on Via, Record-Route if present and contact if it is
     a response to REGISTER*/
     
    /* check if it a response to REGISTER */
    SipHeader header;
    SIP_S8bit	*pTemp = SIP_NULL;
    SIP_S8bit *pTempViaSentBy  = SIP_NULL;
    SIP_S8bit  *pViaSentBy		= SIP_NULL;
  	SIP_S8bit *pTempPort			= SIP_NULL;
    SIP_S8bit dTempArray[100];
    
    /* get the ip and port from the via header */
    if (sip_getHeaderAtIndex(pSipMsg, SipHdrTypeVia, \
					&dTempViaHeader, 0, &derror) == SipFail)
    {
    
    }
    if (sip_getSentByFromViaHdr(&dTempViaHeader, &pTempViaSentBy, \
		         &derror) == SipFail)
    {

    }
    sip_strcpy(dTempArray, pTempViaSentBy);

    /* Here dTempArray contains either the domain name or ip:port
       if it is ip:port, tokenize it and store ip and port */

    pViaSentBy = strtok_r(dTempArray, ":",&pTempPort);
    
    /* allocate memory for viainfo*/
    pViaInfo=(SipAlgHeaderInfo *)fast_memget\
						(0,sizeof(SipAlgHeaderInfo),&derror);
   
   if (pViaInfo == SIP_NULL)
		  return SipFail;
    
    pViaInfo->pIPAddress = sip_strdup(pViaSentBy, APP_MEM_ID);
    pViaInfo->port = atoi(pTempPort);
    
    /* add the element into the list*/
    if( sip_listAppend( slSipAlgHeaderInfo, \
		            (SIP_Pvoid) pViaInfo, &derror) == SipFail)
    {
	    	return SipFail;
    }	

    /* Get the Recored-Route info */
     sip_getHeaderCount(pSipMsg,SipHdrTypeRecordRoute, &size, \
					&derror);

	  if (size != 0)
	  {
	  	 (void)sip_getHeaderAtIndex(pSipMsg, SipHdrTypeRecordRoute,
					&dSipHeader, 0, &derror);

			 (void)sip_getAddrSpecFromRouteHdr(&dSipHeader, \
					&pAddrSpec, &derror);

       if (sip_getUrlFromAddrSpec(pAddressSpec,&pSipUrl1,
                    &derror) == SipFail )
       {
          //TODO       
       }
       /* allocate memory for pReqline*/
       pRecordRouteInfo=(SipAlgHeaderInfo *)fast_memget\
						(0,sizeof(SipAlgHeaderInfo),&derror);
   
       if (pRecordRouteInfo == SIP_NULL)
		       return SipFail;

       pRecordRouteInfo->pIPAddress = sip_strdup(pSipUrl->pHost, APP_MEM_ID);
       pRecordRouteInfo->port = *(pSipUrl1->dPort);
       /* add the element into the list*/
      if( sip_listAppend( slSipAlgHeaderInfo, \
		            (SIP_Pvoid) pRecordRouteInfo, &derror) == SipFail)
  	  {
	      	return SipFail;
  	  }	
    }
      
        /* the if condition should check whether this is a 200 OK for 
           REGISTER, then get all the contacts */
    if (sip_getHeader(pSipMsg, SipHdrTypeCseq, &header, \
			&derror) == SipFail)
		{
			
		}

		if (sip_getMethodFromCseqHdr(&header, &pTemp, \
			&derror) ==  SipFail)
		{
      
		}
    sip_freeSipHeader(&header);
    if (sip_strcasecmp(pTemp,"REGISTER") == 0 )
	  {
      SIP_U16bit i=0;
      /* get the contact header */
      sip_getHeaderCount(pSipMsg,SipHdrTypeContactAny, &size, \
					&derror);
      if (size != 0)
	    { 
	  	  for (i=0 ; i< size; i++)
        { 
          SipAddrSpec 	*pTempAddrSpec = SIP_NULL;
          SipUrl         *pTempSipUrl  =  SIP_NULL;
          SipHeader      dSipHdr;
          SipAlgHeaderInfo *pContactInfo[5] ;
 
          (void)sip_getHeaderAtIndex(pSipMsg, SipHdrTypeContactAny,
					&dSipHdr, i, &derror);

			    (void)sip_getAddrSpecFromContactHdr(&dSipHeader, \
					&pTempAddrSpec, &derror);

          if (sip_getUrlFromAddrSpec(pTempAddrSpec,&pTempSipUrl,
                    &derror) == SipFail )
          {
          }

          pContactInfo[i]=(SipAlgHeaderInfo *)fast_memget\
						(0,sizeof(SipAlgHeaderInfo),&derror);
   
          if (pContactInfo[i] == SIP_NULL)
		       return SipFail;

          pContactInfo[i]->pIPAddress = sip_strdup(pSipUrl->pHost, APP_MEM_ID);
          pContactInfo[i]->port = *(pSipUrl1->dPort);
          /* add the element into the list*/
          if( sip_listAppend( slSipAlgHeaderInfo, \
		            (SIP_Pvoid) pContactInfo[i], &derror) == SipFail)
  	      {
	         	return SipFail;
  	      }	

        }    
      }    

      
    }
  
  }
  return SipSuccess;
}


#if 0
///////////////////////////////////////////////////////////////
// Function Name : sipAlgTranslatePktIn
// Description  : This is the function that does all the ALG 
//                operation for incoming packets. 
//                 
// Input/Output (s) : ppSipMessageBuffer - Incoming Sip message
//			     pHeaderInfo - Pkt Header Info	
// Returns    : SUCCESS/FAILURE
///////////////////////////////////////////////////////////

SipBool sipAlgTranslatePktIn (SIP_S8bit *pSipMsgBuffer/*, tHeaderInfo * pHeaderInfo*/)
{

  SipMessage *pParsedMsg;
  SipOptions opt;
  SipEventContext context;
  SIP_S8bit *nextmesg;
  SipError derror;
  SipBool r;
  SipList slSipAlgHeaderInfo;

  /* init the list */
  sip_listInit(&slSipAlgHeaderInfo,SIP_NULL,&derror);

  
  /* call the stack API to decode the message */
  r= sip_decodeMessage((SIP_S8bit *)pSipMsgBuffer,&pParsedMsg,&opt, strlen(pSipMsgBuffer),\
                                     &nextmesg,&context, &derror);

  r= sipAlgGetInHeaderInfo (pParsedMsg, 
               &slSipAlgHeaderInfo);

  return SipSuccess;

}
#endif


/* The following function is used to translate the Req-URI */
/* SipAlgTranslateReqUri(SipMessage *pSipMsg, tHeaderInfo * pHeaderInfo)*/

SipBool
SipAlgTranslateReqUri(SipMessage *pSipMsg)
{
  SipReqLine *pReqline = SIP_NULL;
  SipAddrSpec 	*pAddrSpec = SIP_NULL;
  SipUrl             *pSipUrl  =  SIP_NULL;
  SipError derror;
  SipNatInfo *pReqUriInfo;
  SIP_S8bit  			*pTempHost = SIP_NULL;
  SIP_U16bit dPort = 0;
  
  if(sip_getReqLineFromSipReqMsg(pSipMsg,&pReqline, \
                &derror)==SipFail)
  {
    return SipFail;  
  }  
  if(sip_getAddrSpecFromReqLine(pReqline,&pAddrSpec, \
                &derror)==SipFail)
  {
    sip_freeSipReqLine(pRequest) ; 
    return SipFail;  
  }  
  if (sip_getUrlFromAddrSpec(pAddrSpec,&pSipUrl,
                    &derror) == SipFail )
  {
     sip_freeSipAddrSpec(pAddrSpec);
     sip_freeSipReqLine(pRequest) ; 
     return SipFail;
  }
  if (sip_getHostFromUrl(pSipUrl, &pTempHost,\
				&derror) == SipFail)
  {
    sip_freeSipUrl(pSipUrl);
		sip_freeSipAddrSpec(pAddrSpec);
    sip_freeSipReqLine(pRequest) ; 
    return SipFail;  
  }  
  
  if( sip_getPortFromUrl(pSipUrl, &dPort,\
           &derror)==SipFail)
  {
    sip_freeSipUrl(pSipUrl);
		sip_freeSipAddrSpec(pAddrSpec);
    sip_freeSipReqLine(pRequest) ;
    return SipFail;  
  }  
  
  /* TODO...invoke the function to see whether psipUrl->pHost
     a domain or ip */
  
  /*if it is ip then form the *pReqUriInfo structure */
   
  /* allocate memory to natinfo structure*/
  pReqUriInfo = (SipNatInfo *)fast_memget\
						(0,sizeof(SipNatInfo),&derror);
   
  if (pReqUriInfo == SIP_NULL)
  {
    sip_freeSipUrl(pSipUrl);
		sip_freeSipAddrSpec(pAddrSpec);
    sip_freeSipReqLine(pRequest) ;
	  return SipFail;
  }     

  pReqUriInfo->pOriginalIP = pTempHost;
  pReqUriInfo->originalPort = dPort;
  /*pReqUriInfo->pOutsideIP = pHeaderInfo->u4OutIpAddr;
  pReqUriInfo->outsidePort = pHeaderInfo->u2OutPort;*/
  
  /* Invoke sipAlgUpdateSignalingTables
		(pDialogParams, SipNatInfo) */
    
  /* here we have the translated ip and port */
  if (sip_setPortInUrl(pSipUrl, pReqUriInfo->mappedPort,
								&derror) == SipFail)
  {
    sip_freeSipUrl(pSipUrl);
		sip_freeSipAddrSpec(pAddrSpec);
    sip_freeSipReqLine(pRequest) ;
    return SipFail;   
  }
  if (sip_setHostInUrl(pSipUrl, pReqUriInfo->pMappedIP,
								&derror) == SipFail)
  {
    sip_freeSipUrl(pSipUrl);
		sip_freeSipAddrSpec(pAddrSpec);
    sip_freeSipReqLine(pRequest) ;
    return SipFail;  
  }  
  /* free all the local refeences */
  sip_freeSipUrl(pSipUrl);
	sip_freeSipAddrSpec(pAddrSpec);
  sip_freeSipReqLine(pRequest) ;
  return SipSuccess;
}

/* The following function is used to translate the Via Header */
/* SipAlgTranslateViaHdr(SipMessage *pSipMsg, tHeaderInfo * pHeaderInfo)*/

SipBool
SipAlgTranslateViaHdr(SipMessage *pSipMsg)
{
  SIP_S8bit *pTempViaSentBy  = SIP_NULL;
  SIP_S8bit  *pViaSentBy = SIP_NULL, *pTransViaSentBy = SIP_NULL;
  SIP_S8bit *pTempPort			= SIP_NULL;
  SIP_S8bit dTempArray[100];
  SipError    derror;
  SipHeader  *pViaHeader=SIP_NULL;
  SipNatInfo *pViaInfo;

  /* get the ip and port from the via header */
  if (sip_getHeaderAtIndex(pSipMsg, SipHdrTypeVia, \
        pViaHeader, 0, &derror) == SipFail)
  {
    return SipFail;
  }
  if (sip_getSentByFromViaHdr(pViaHeader, &pTempViaSentBy, \
        &derror) == SipFail)
  {
    return SipFail;
  }
  sip_strcpy(dTempArray, pTempViaSentBy);

  /* Here dTempArray contains either the domain name or ip:port
     if it is ip:port, tokenize it and store ip and port */

  pViaSentBy = strtok_r(dTempArray, ":",&pTempPort);
  /* TODO...invoke the function to see whether psipUrl->pHost
     a domain or ip */
  
  /*if it is ip then form the *pReqUriInfo structure */
   
  /* allocate memory to natinfo structure*/
  pViaInfo = (SipNatInfo *)fast_memget\
						(0,sizeof(SipNatInfo),&derror);
   
  if (pViaInfo == SIP_NULL)
	     return SipFail;

  pViaInfo->pOriginalIP = pViaSentBy;
  pViaInfo->originalPort = atoi(pTempPort);
  /*pReqUriInfo->pOutsideIP = pHeaderInfo->u4OutIpAddr;
  pReqUriInfo->outsidePort = pHeaderInfo->u2OutPort;*/
  
  /* Invoke sipAlgUpdateSignalingTables
	  	(pDialogParams, SipNatInfo) */
    
  /* here we have the translated ip and port */
  /* now form the translated ip and port in ip:port form */
  /*pTransViaSentBy = ip and port concatenation */ 
  if(sip_setSentByInViaHdr(pViaHeader, pTransViaSentBy, \
		   	&derror) == SipFail)
  {
    return SipFail;  
  }  

  return SipSuccess;
}

/* The following function is used to translate the Contact Header */
/* SipAlgTranslateContactHdr(SipMessage *pSipMsg, tHeaderInfo * pHeaderInfo)*/
SipBool
SipAlgTranslateContactHdr(SipMessage *pSipMsg)
{
  SipAddrSpec 	*pAddrSpec = SIP_NULL;
  SipUrl             *pSipUrl  =  SIP_NULL;  
  SipError    derror;
  SIP_U32bit size =0;
  SipNatInfo *pContactInfo;
  SIP_S8bit  			*pTempHost = SIP_NULL;
  SIP_U16bit dPort = 0, i=0;
  
  if(sip_getHeaderCount(pSipMsg,SipHdrTypeContactAny, &size, \
					&derror)==SipFail)
  {
    return SipFail;
  }  
  if (size != 0)
	{ 
	  for (i=0 ; i< size; i++)
    { 
      SipAddrSpec 	*pTempAddrSpec = SIP_NULL;
      SipUrl         *pTempSipUrl  =  SIP_NULL;
      SipHeader      *pSipHdr = SIP_NULL;
 
      if(sip_getHeaderAtIndex(pSipMsg, SipHdrTypeContactAny,
					pSipHdr, i, &derror)==SipFail)
      {
        return  SipFail;  
      }  
		  if (sip_getAddrSpecFromContactHdr(pSipHdr, \
					&pTempAddrSpec, &derror)==SipFail)
      {
        return SipFail;  
      }  

      if (sip_getUrlFromAddrSpec(pTempAddrSpec,&pTempSipUrl,
                    &derror) == SipFail )
      {
        return SipFail;
      }
      if (sip_getHostFromUrl(pSipUrl, &pTempHost,\
				&derror) == SipFail)
      {
        sip_freeSipUrl(pSipUrl);
		    sip_freeSipAddrSpec(pAddrSpec);
        return SipFail;  
      }  
  
      if( sip_getPortFromUrl(pSipUrl, &dPort,\
           &derror)==SipFail)
      {
        return SipFail;  
      }  
  
      /* TODO...invoke the function to see whether psipUrl->pHost
        a domain or ip */
  
      /*if it is ip then form the *pReqUriInfo structure */
   
      /* allocate memory to natinfo structure*/
      pContactInfo = (SipNatInfo *)fast_memget\
						(0,sizeof(SipNatInfo),&derror);
   
      if (pContactInfo == SIP_NULL)
	         return SipFail;

      pContactInfo->pOriginalIP = pTempHost;
      pContactInfo->originalPort = dPort;
     /*pReqUriInfo->pOutsideIP = pHeaderInfo->u4OutIpAddr;
     pReqUriInfo->outsidePort = pHeaderInfo->u2OutPort;*/
  
     /* Invoke sipAlgUpdateSignalingTables
		    (pDialogParams, SipNatInfo) */
    
     /* here we have the translated ip and port */
     if (sip_setPortInUrl(pSipUrl, pContactInfo->mappedPort,
								&derror) == SipFail)
     {
       return SipFail;   
     }
     if (sip_setHostInUrl(pSipUrl, pContactInfo->pMappedIP,
								&derror) == SipFail)
     {
       return SipFail;  
     }  
    }
  }  
  return SipSuccess;
}

/* The following function is used to translate the Route Header */
/* SipAlgTranslateRouteHdr(SipMessage *pSipMsg, tHeaderInfo * pHeaderInfo)*/
SipBool
SipAlgTranslateRouteHdr(SipMessage *pSipMsg)
{
  SipAddrSpec 	*pAddrSpec = SIP_NULL;
  SipUrl             *pSipUrl  =  SIP_NULL;  
  SipError    derror;
  SipHeader		dSipHeader;
  SIP_U32bit size =0;
  SipNatInfo *pRouteInfo;
  SIP_S8bit  			*pTempHost = SIP_NULL;
  SIP_U16bit dPort = 0, i=0;

  if (sip_getHeaderCount(pSipMsg,SipHdrTypeRoute, &size, \
					&derror)==SipFail)
  {
    return SipFail;  
  }  

	if (size == 0)
  {
    return SipFail;  
  }  
  for (i=0; i<size; i++)
	{
	  if(sip_getHeaderAtIndex(pSipMsg, SipHdrTypeRoute,
					&dSipHeader, i, &derror)== SipFail)
    {
      return SipFail;  
    }  

	  if (sip_getAddrSpecFromRouteHdr(&dSipHeader, \
					&pAddrSpec, &derror) == SipFail)
    {
       return SipFail;  
    }  

    if (sip_getUrlFromAddrSpec(pAddrSpec,&pSipUrl,
                    &derror) == SipFail )
    {
      return SipFail;       
    }
    if (sip_getHostFromUrl(pSipUrl, &pTempHost,\
				&derror) == SipFail)
    {
      sip_freeSipUrl(pSipUrl);
		  sip_freeSipAddrSpec(pAddrSpec);
      return SipFail;  
    }  
  
    if( sip_getPortFromUrl(pSipUrl, &dPort,\
           &derror)==SipFail)
    {
      return SipFail;  
    }  

    /* TODO...invoke the function to see whether psipUrl->pHost
     a domain or ip */
  
    /*if it is ip then form the *pReqUriInfo structure */
   
    /* allocate memory to natinfo structure*/
    pRouteInfo = (SipNatInfo *)fast_memget\
						(0,sizeof(SipNatInfo),&derror);
   
    if (pRouteInfo == SIP_NULL)
	     return SipFail;

    pRouteInfo->pOriginalIP = pTempHost;
    pRouteInfo->originalPort = dPort;
    /*pReqUriInfo->pOutsideIP = pHeaderInfo->u4OutIpAddr;
    pReqUriInfo->outsidePort = pHeaderInfo->u2OutPort;*/
  
    /* Invoke sipAlgUpdateSignalingTables
	  	(pDialogParams, SipNatInfo) */
    
    /* here we have the translated ip and port */
    if (sip_setPortInUrl(pSipUrl, pRouteInfo->mappedPort,
								&derror) == SipFail)
    {
      return SipFail;   
    }
    if (sip_setHostInUrl(pSipUrl, pRouteInfo->pMappedIP,
								&derror) == SipFail)
    {
      return SipFail;  
    }  

  }
  return SipSuccess;
}

/* The following function is used to translate the Record-Route Header */
/* SipAlgTranslateRecordRouteHdr(SipMessage *pSipMsg, tHeaderInfo * pHeaderInfo)*/
SipBool
SipAlgTranslateRecordRouteHdr(SipMessage *pSipMsg)
{
  SipAddrSpec 	*pAddrSpec = SIP_NULL;
  SipUrl             *pSipUrl  =  SIP_NULL;  
  SipError    derror;
  SipHeader		dSipHeader;
  SIP_U32bit size =0;
  SipNatInfo *pRecordRouteInfo;
  SIP_S8bit  			*pTempHost = SIP_NULL;
  SIP_U16bit dPort = 0, i=0;

  if (sip_getHeaderCount(pSipMsg,SipHdrTypeRecordRoute, &size, \
					&derror)==SipFail)
  {
    return SipFail;  
  }  

	if (size == 0)
  {
    return SipFail;  
  }  
  for (i=0; i<size; i++)
	{
	  if(sip_getHeaderAtIndex(pSipMsg, SipHdrTypeRecordRoute,
					&dSipHeader, i, &derror)== SipFail)
    {
      return SipFail;  
    }  

	  if (sip_getAddrSpecFromRouteHdr(&dSipHeader, \
					&pAddrSpec, &derror) == SipFail)
    {
       return SipFail;  
    }  

    if (sip_getUrlFromAddrSpec(pAddrSpec,&pSipUrl,
                    &derror) == SipFail )
    {
      return SipFail;       
    }
    if (sip_getHostFromUrl(pSipUrl, &pTempHost,\
				&derror) == SipFail)
    {
      sip_freeSipUrl(pSipUrl);
		  sip_freeSipAddrSpec(pAddrSpec);
      return SipFail;  
    }  
  
    if( sip_getPortFromUrl(pSipUrl, &dPort,\
           &derror)==SipFail)
    {
      return SipFail;  
    }  
    /* TODO...invoke the function to see whether psipUrl->pHost
     a domain or ip */
  
    /*if it is ip then form the *pReqUriInfo structure */
   
    /* allocate memory to natinfo structure*/
    pRecordRouteInfo = (SipNatInfo *)fast_memget\
						(0,sizeof(SipNatInfo),&derror);
   
    if (pRecordRouteInfo == SIP_NULL)
	     return SipFail;

    pRecordRouteInfo->pOriginalIP = pTempHost;
    pRecordRouteInfo->originalPort = dPort;
    /*pReqUriInfo->pOutsideIP = pHeaderInfo->u4OutIpAddr;
    pReqUriInfo->outsidePort = pHeaderInfo->u2OutPort;*/
  
    /* Invoke sipAlgUpdateSignalingTables
	  	(pDialogParams, SipNatInfo) */
    
    /* here we have the translated ip and port */
    if (sip_setPortInUrl(pSipUrl, pRecordRouteInfo->mappedPort,
								&derror) == SipFail)
    {
      return SipFail;   
    }
    if (sip_setHostInUrl(pSipUrl, pRecordRouteInfo->pMappedIP,
								&derror) == SipFail)
    {
      return SipFail;  
    }  
  }
  return SipSuccess;
}

