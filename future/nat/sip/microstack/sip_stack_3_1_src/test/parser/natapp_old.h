/********************************************************************
 * Copyright (C) Aricent Communications Sotware,2007
 *
 * $Id: natapp_old.h,v 1.1.1.1 2010/12/08 06:10:43 siva Exp $ 
 *
 * Description:This file contains the data structure typedefs
 *             used by SIP ALG 
 *******************************************************************/
#ifdef __cplusplus
extern "C" {
#endif 

#ifndef _NAT_SIP_H
#define _NAT_SIP_H

#include "natsipdefs.h"

SipBool sipAlgTranslateIncomingHeaders (SipMessage *pSipMsg, 
             SipList *slSipAlgHeaderInfo);
 
SipBool
SipAlgTranslateReqUri(SipMessage *pSipMsg);

SipBool
SipAlgTranslateViaHdr(SipMessage *pSipMsg);

SipBool
SipAlgTranslateContactHdr(SipMessage *pSipMsg);

SipBool
SipAlgTranslateRouteHdr(SipMessage *pSipMsg);

SipBool
SipAlgTranslateRecordRouteHdr(SipMessage *pSipMsg);

SipBool
sipAlgGetMessageBodyInfo (SipMessage *pSipMessage, 

SipList *pSLSipAlgMediaInfo);

SipBool sipAlgGetOutHeaderInfo (SipMessage *pSipMsg, 
               SipList *slSipAlgHeaderInfo);

SipBool sipAlgTranslatePktIn (SIP_S8bit *pSipMsgBuffer);

SipBool
sipAlgUpdateSipMsgHdr (SipMessage **pSipMsg, 
              SipAlgIpPort *pTranslatedIpPort, SipHdrType dSipHdr );


#endif 
