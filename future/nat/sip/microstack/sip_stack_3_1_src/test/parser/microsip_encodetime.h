
/* Ensure Names are not mangled by C++ compilers */
#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************
** FUNCTION: Ths file has all the SDP headers exposed to the user
**
*******************************************************************
**
** FILENAME:
**	Sdp.c
**
** $Id: microsip_encodetime.h,v 1.1.1.1 2010/12/08 06:10:43 siva Exp $
** DESCRIPTION
**
**
**   DATE              NAME                      REFERENCE
**  ------			---------					------------
** 23/11/99            B.Borthakur, K. Deepali   Added Origin APIs
**
**   Copyright (C) 2006 Aricent Inc . All Rights Reserved
*************************************************************/

#ifndef __SDP_H__
#define __SDP_H__

#include "microsip_common.h"
#include "microsip_struct.h"

/***********************************************************************
** FUNCTION: sdp_getVersion
** DESCRIPTION: get Version field from Sdp Message
** PARAMETERS:
**		msg (IN) 		- Sdp Message
**		version(OUT)	- version retrieved
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_getVersion _ARGS_(( SdpMessage *msg, \
		SIP_S8bit **version, SipError *err));

/***********************************************************************
** FUNCTION: sdp_setVersion
** DESCRIPTION: set Version field in Sdp Message
** PARAMETERS:
**		msg (IN/OUT) 	- Sdp Message
**		version(IN) 	- Version to set
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_setVersion _ARGS_(( SdpMessage *msg, \
		SIP_S8bit *version, SipError *err));

#ifdef SIP_BY_REFERENCE
/***********************************************************************
** FUNCTION: sdp_getOrigin
** DESCRIPTION: get Origin field from Sdp Message
** PARAMETERS:
**		msg (IN) 	- Sdp Message
**		origin(OUT) - origin retrieved
**		err	(OUT)	- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
**
************************************************************************/
extern 	SipBool sdp_getOrigin _ARGS_(( SdpMessage *msg, \
		SdpOrigin **origin, SipError *err));

#else
/***********************************************************************
** FUNCTION: sdp_getOrigin
** DESCRIPTION: get Origin field from Sdp Message
** PARAMETERS:
**		msg (IN) 	- Sdp Message
**		origin(OUT) - origin retrieved
**		err	(OUT)	- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
**
************************************************************************/
extern 	SipBool sdp_getOrigin _ARGS_(( SdpMessage *msg, \
		SdpOrigin *origin, SipError *err));

#endif
/***********************************************************************
** FUNCTION: sdp_setOrigin
** DESCRIPTION: set Origin field in Sdp Message
** PARAMETERS:
**		msg (IN/OUT) 	- Sdp Message
**		origin(IN)		- Origin to set
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_setOrigin _ARGS_(( SdpMessage *msg, \
		SdpOrigin *origin, SipError *err));

#ifdef SIP_BY_REFERENCE
/***********************************************************************
** FUNCTION: sdp_getConnection
** DESCRIPTION: get Connection field from Sdp Message
** PARAMETERS:
**		msg (IN) 		- Sdp Message
**		connection(OUT)	- Retrieved connection field
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_getConnection _ARGS_(( SdpMessage *msg, \
		SdpConnection **connection, SipError *err));

#else
/***********************************************************************
** FUNCTION: sdp_getConnection
** DESCRIPTION: get Connection field from Sdp Message
** PARAMETERS:
**		msg (IN) 		- Sdp Message
**		connection(OUT)	- Retrieved connection field
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_getConnection _ARGS_(( SdpMessage *msg, \
		SdpConnection *connection, SipError *err));

#endif
/***********************************************************************
** FUNCTION: sdp_setConnection
** DESCRIPTION: set connection field in Sdp Message
** PARAMETERS:
**		msg (IN/OUT)	- Sp Message
**		connection(IN)	- connection to set
**		err	(OUT)	- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_setConnection _ARGS_(( SdpMessage *msg, \
		SdpConnection *connection, SipError *err));

/***********************************************************************
** FUNCTION: sdp_getUserFromOrigin
** DESCRIPTION: get user field from Origin line
** PARAMETERS:
**		msg (IN)		- Sdp Origin line
**		user (OUT)		- retrieved user field
**		err	(OUT)	- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_getUserFromOrigin _ARGS_(( SdpOrigin * msg,	\
		SIP_S8bit ** user, SipError * err));


/***********************************************************************
** FUNCTION: sdp_setUserInOrigin
** DESCRIPTION: set user field in origin line
** PARAMETERS:
**		msg (IN/OUT) -	Sdp Origin line
**		user (IN) 	 - 	User value to set
**		err	(OUT)	- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_setUserInOrigin _ARGS_(( SdpOrigin * msg,	\
		SIP_S8bit * user, SipError * err));

/***********************************************************************
** FUNCTION: sdp_getSessionIdFromOrigin
** DESCRIPTION: get session identifier from origin field
** PARAMETERS:
**		origin (IN)		- Sdp Origin line
**		id	(OUT)		- Retrieved Session Id field
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_getSessionIdFromOrigin _ARGS_(( SdpOrigin * origin,	\
		SIP_S8bit ** id, SipError * err));

/***********************************************************************
** FUNCTION: sdp_setSessionIdInOrigin
** DESCRIPTION: set session identifier in origin field
** PARAMETERS:
**		origin (IN/OUT)		- Sdp Origin line
**		id (IN)				- Session id to set
**		err	(OUT)			- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_setSessionIdInOrigin _ARGS_(( SdpOrigin * origin,	\
		SIP_S8bit * id, SipError * err));

/***********************************************************************
** FUNCTION: sdp_getVersionFromOrigin
** DESCRIPTION: get version field from Origin line
** PARAMETERS:
**		origin (IN)		- Sdp Origin line
**		version (OUT)	- retrieved version field
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_getVersionFromOrigin _ARGS_(( SdpOrigin * origin,	\
		SIP_S8bit ** version, SipError * err));

/***********************************************************************
** FUNCTION: sdp_setVersionInOrigin
** DESCRIPTION: set version field in Origin line
** PARAMETERS:
**		origin (IN/OUT)	- Sdp Origin line
**		version (IN)	- Version to set
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_setVersionInOrigin _ARGS_(( SdpOrigin * origin,	\
		SIP_S8bit * version, SipError * err));

/***********************************************************************
** FUNCTION: sdp_getNetTypeFromOrigin
** DESCRIPTION: get Network Type field from Origin line
** PARAMETERS:
**		origin (IN) 		- Sdp Origin line
**		ntype	(OUT)		- retrieved network type
**		err	(OUT)			- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_getNetTypeFromOrigin _ARGS_(( SdpOrigin * origin,	\
		SIP_S8bit ** ntype, SipError * err));

/***********************************************************************
** FUNCTION: sdp_setNetTypeInOrigin
** DESCRIPTION: set Network Type field in Origin line
** PARAMETERS:
**		origin (IN/OUT)		- Sdp Origin line
**		ntype (IN)			- Network Type to set
**		err	(OUT)			- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_setNetTypeInOrigin _ARGS_(( SdpOrigin * origin,	\
		SIP_S8bit * ntype, SipError * err));

/***********************************************************************
** FUNCTION: sdp_getAddrFromOrigin
** DESCRIPTION:get Address field from Origin Line
** PARAMETERS:
**		origin 	(IN) 		- Sdp Origin line
**		addr	(OUT)		- retrieved address field
**		err		(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_getAddrFromOrigin _ARGS_(( SdpOrigin * origin,	\
		SIP_S8bit ** addr, SipError * err));

/***********************************************************************
** FUNCTION: sdp_setAddrInOrigin
** DESCRIPTION: Set address field in Origin Line
** PARAMETERS:
**		origin (IN/OUT)		- Sdp Origin line
**		addr	(IN)		- Address field to set
**		err	(OUT)			- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_setAddrInOrigin _ARGS_(( SdpOrigin * origin,	\
		SIP_S8bit * addr, SipError * err));

/***********************************************************************
** FUNCTION: sdp_getAddrTypeFromOrigin
** DESCRIPTION: get address type field from origin line
** PARAMETERS:
**		origin(IN)		- Sdp Origin line
**		addr_type(OUT)	- retrieved address type field
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_getAddrTypeFromOrigin _ARGS_(( SdpOrigin * origin, \
		SIP_S8bit ** addr_type, SipError * err));

/***********************************************************************
** FUNCTION: sdp_setAddrTypeInOrigin
** DESCRIPTION: set address type field in origin line
** PARAMETERS:
**		origin (IN/OUT)	- Sdp Origin line
**		addr_type(IN)	- Address Type field to set
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_setAddrTypeInOrigin _ARGS_(( SdpOrigin * origin, \
		SIP_S8bit * addr_type, SipError * err));

/***********************************************************************
** FUNCTION: sdp_getSession
** DESCRIPTION: get Session line from Sdp Message
** PARAMETERS:
**		msg (IN)		- Sdp Message
**		session(OUT)	- retrieved session line
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_getSession _ARGS_(( SdpMessage *msg, \
		SIP_S8bit **session, SipError *err));

/***********************************************************************
** FUNCTION: sdp_setSession
** DESCRIPTION: set Session line in Sdp Message
** PARAMETERS:
**		msg (IN/OUT)	- Sdp Message
**		session (IN)	- session value to set
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_setSession _ARGS_(( SdpMessage *msg, \
		SIP_S8bit *session, SipError *err));

/***********************************************************************
** FUNCTION: sdp_getUri
** DESCRIPTION: get uri line from Sdp Message
** PARAMETERS:
**		msg (IN)		- Sdp Message
**		uri (OUT)		- retrieved uri field
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_getUri _ARGS_(( SdpMessage *msg, \
		SIP_S8bit **uri, SipError *err));

/***********************************************************************
** FUNCTION: sdp_setUri
** DESCRIPTION: set uri line in Sdp Message
** PARAMETERS:
**		msg (IN/OUT)	- Sdp Message
**		uri (IN)		- Uri to set
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_setUri _ARGS_(( SdpMessage *msg, \
		SIP_S8bit *uri, SipError *err));

/***********************************************************************
** FUNCTION: sdp_getInfo
** DESCRIPTION: get information line from Sdp Message
** PARAMETERS:
**		msg (IN)		- Sdp Message
**		info (OUT)		- retrieved uri line
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_getInfo _ARGS_(( SdpMessage *msg, \
		SIP_S8bit **info, SipError *err));

/***********************************************************************
** FUNCTION: sdp_setInfo
** DESCRIPTION: set information line in Sdp Message (i=)
** PARAMETERS:
**		msg (IN/OUT)	- Sdp Message
**		info (IN)		- Information field to set
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_setInfo _ARGS_(( SdpMessage *msg, \
		SIP_S8bit *info, SipError *err));

/***********************************************************************
** FUNCTION: sdp_getKey
** DESCRIPTION: get key line from sdp Message (k=)
** PARAMETERS:
**		msg (IN)		- Sdp Message
**		key (OUT)		- Retrieved key value
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
**
************************************************************************/
extern 	SipBool sdp_getKey _ARGS_(( SdpMessage *msg, \
		SIP_S8bit **key, SipError *err));

/***********************************************************************
** FUNCTION: sdp_setKey
** DESCRIPTION: set key line in Sdp Message (k=)
** PARAMETERS:
**		msg (IN/OUT)		- Sdp Message
**		key (IN)			- key value to set
**		err	(OUT)			- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_setKey _ARGS_(( SdpMessage *msg, \
		SIP_S8bit *key, SipError *err));

/***********************************************************************
** FUNCTION: sdp_getBandwidthCount
** DESCRIPTION: get number of bandwidth lines in Sdp Message (b=)
** PARAMETERS:
**		msg (IN)		- Sdp Message
**		cnt (OUT)		- number of bandwidth lines
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_getBandwidthCount _ARGS_(( SdpMessage *msg, \
		SIP_U32bit *cnt, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_getBandwidthAtIndex
** DESCRIPTION: get bandwidth line at a specified index from Sdp Message
** PARAMETERS:
**		msg (IN)		- Sdp Message
**		bandwidth(OUT)	- retrieved bandwidh field
**		cnt (IN)		- Index at which to retrieve
************************************************************************/
extern 	SipBool sdp_getBandwidthAtIndex _ARGS_(( SdpMessage *msg, \
		SIP_S8bit **bandwidth, SIP_U32bit cnt, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_setBandwidthAtIndex
** DESCRIPTION:	set bandwidth line at a specified index in Sdp Message
** PARAMETERS:
**		msg (IN/OUT)		- Sdp Message
**		bandwidth(IN)		- bandwidth value to set
** 		cnt (IN)			- Index at which to set
**		err	(OUT)			- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_setBandwidthAtIndex _ARGS_(( SdpMessage *msg, \
		SIP_S8bit *bandwidth, SIP_U32bit cnt, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_insertBandwidthAtIndex
** DESCRIPTION:	insert bandwidth line at a specified index in Sdp Message
** PARAMETERS:
**		msg (IN/OUT)		- Sdp Message
**		bandwidth(IN)		- bandwidth value to insert
** 		cnt (IN)			- Index at which to insert
**		err	(OUT)			- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_insertBandwidthAtIndex _ARGS_(( SdpMessage *msg, \
		SIP_S8bit *bandwidth, SIP_U32bit cnt, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_deleteBandwidthAtIndex
** DESCRIPTION:	delete bandwidth line at a specified index in Sdp Message
** PARAMETERS:
**		msg (IN/OUT)		- Sdp Message
** 		cnt (IN)			- Index at which to delete
**		err	(OUT)			- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_deleteBandwidthAtIndex _ARGS_(( SdpMessage *msg, \
		SIP_U32bit cnt, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_getPhoneCount
** DESCRIPTION: get number of phone lines in Sdp Message
** PARAMETERS:
**		msg (IN)			- Sdp Message
**		cnt (OUT)			- number of phone lines
**		err	(OUT)			- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_getPhoneCount _ARGS_(( SdpMessage *msg, \
		SIP_U32bit *cnt, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_getPhoneAtIndex
** DESCRIPTION: get phone line at specified index from Sdp Message
** PARAMETERS:
**		msg (IN)		- Sdp Message
**		phone (OUT)		- retrieved phone line
**		cnt (IN)		- specified index to retrieve from
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_getPhoneAtIndex _ARGS_(( SdpMessage *msg, \
		SIP_S8bit **phone, SIP_U32bit cnt, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_setPhoneAtIndex
** DESCRIPTION: set phone line at specified index in Sdp Message
** PARAMETERS:
**		msg (IN/OUT)	- Sdp Message
**		phone (IN)		- phone line to set
**		cnt (IN)		- specified index to set into
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_setPhoneAtIndex _ARGS_(( SdpMessage *msg, \
		SIP_S8bit *phone, SIP_U32bit cnt, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_insertPhoneAtIndex
** DESCRIPTION: insert phone line at specified index in Sdp Message
** PARAMETERS:
**		msg (IN/OUT)	- Sdp Message
**		phone (IN)		- phone line to insert
**		cnt (IN)		- specified index to insert into
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_insertPhoneAtIndex _ARGS_(( SdpMessage *msg, \
		SIP_S8bit *phone, SIP_U32bit cnt, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_deletePhoneAtIndex
** DESCRIPTION: delete phone line at specified index in Sdp Message
** PARAMETERS:
**		msg (IN/OUT)	- Sdp Message
**		cnt (IN)		- Index to delete phone line from
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_deletePhoneAtIndex _ARGS_(( SdpMessage *msg, \
		SIP_U32bit cnt, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_getEmailCount
** DESCRIPTION: get number of email lines in Sdp Message
** PARAMETERS:
**		msg (IN)		- Sdp Message
**		cnt (OUT)		- number of email lines
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_getEmailCount _ARGS_(( SdpMessage *msg, \
		SIP_U32bit *cnt, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_getEmailAtIndex
** DESCRIPTION: get email line at specified index from Sdp Message
** PARAMETERS:
**		msg (IN)		- Sdp Message
**		email (OUT)		- retrieved email line
**		cnt (IN)		- Index at which to retrieve
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_getEmailAtIndex _ARGS_(( SdpMessage *msg, \
		SIP_S8bit **email, SIP_U32bit cnt, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_setEmailAtIndex
** DESCRIPTION: set email line at specified index in Sdp Message
** PARAMETERS:
**		msg (IN/OUT)	- Sdp Message
**		email (IN)		- email line to set
**		err	(OUT)		- Possible error value (see API ref doc)
************************************************************************/
extern 	SipBool sdp_setEmailAtIndex _ARGS_(( SdpMessage *msg, \
		SIP_S8bit *email, SIP_U32bit cnt, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_insertEmailAtIndex
** DESCRIPTION: insert email line at specified index in Sdp Message
* PARAMETERS:
**		msg (IN/OUT)	- Sdp Message
**		email (IN)		- email line to set
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_insertEmailAtIndex _ARGS_(( SdpMessage *msg, \
		SIP_S8bit *email, SIP_U32bit cnt, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_deleteEmailAtIndex
** DESCRIPTION: delete email line at specified index from Sdp Message
** PARAMETERS:
**		msg (IN/OUT)	- Sdp Message
**		cnt (IN)		- specified index to delete from
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_deleteEmailAtIndex _ARGS_(( SdpMessage *msg, \
		SIP_U32bit cnt, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_getMediaCount
** DESCRIPTION: get number of media lines from Sdp Message
** PARAMETERS:
**		msg(IN)			- Sdp Message
**		cnt (OUT)		- retrieved number of media lines
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_getMediaCount _ARGS_(( SdpMessage	*msg, \
		SIP_U32bit	*cnt, SipError	*err  ));

#ifdef SIP_BY_REFERENCE
/***********************************************************************
** FUNCTION: sdp_getMediaAtIndex
** DESCRIPTION: get media line from Sdp Message at specified index
** PARAMETERS:
**		msg (IN)	- Sdp Message
**		media (OUT)	- retrieved media line
**		cnt (IN)	- Index to retrieve Media line from
**		err	(OUT)	- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_getMediaAtIndex _ARGS_(( SdpMessage 	*msg, \
		SdpMedia 	**media, SIP_U32bit 	cnt, SipError 	*err ));

#else
/***********************************************************************
** FUNCTION: sdp_getMediaAtIndex
** DESCRIPTION: get media line from Sdp Message at specified index
** PARAMETERS:
**		msg (IN)	- Sdp Message
**		media (OUT)	- retrieved media line
**		cnt (IN)	- Index to retrieve Media line from
**		err	(OUT)	- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_getMediaAtIndex _ARGS_(( SdpMessage 	*msg, \
		SdpMedia 	*media, SIP_U32bit 	cnt, SipError 	*err ));

#endif
/***********************************************************************
** FUNCTION: sdp_setMediaAtIndex
** DESCRIPTION: set media line at specified index in Sdp Message
** PARAMETERS:
**		msg (IN/OUT)	- Sdp Message
**		media (IN)		- Media line to set
**		cnt (IN)		- Index to set at
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_setMediaAtIndex _ARGS_(( SdpMessage 	*msg, \
		SdpMedia	*media, SIP_U32bit 	cnt, SipError 	*err ));

/***********************************************************************
** FUNCTION: sdp_insertMediaAtIndex
** DESCRIPTION: insert media at specified  index in Sdp Message
** PARAMETERS:
**		msg (IN/OUT)	- Sdp Message
**		media (IN)		- Media line to insert
**		cnt (IN)		- Index to insert at
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_insertMediaAtIndex _ARGS_(( SdpMessage 	*msg, \
		SdpMedia	*media, SIP_U32bit 	cnt, SipError 	*err ));

/***********************************************************************
** FUNCTION: sdp_deleteMediaAtIndex
** DESCRIPTION: delete media at specified index from Sdp Message
** PARAMETERS:
**		msg (IN/OUT)	- Sdp Message
**		cnt (IN)		- Index to delete at
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_deleteMediaAtIndex _ARGS_(( SdpMessage 	*msg, \
		SIP_U32bit 	cnt, SipError 	*err ));

/***********************************************************************
** FUNCTION: sdp_getTimeCount
** DESCRIPTION: get number of time lines in Sdp Message
** PARAMETERS:
**		msg (IN)		- Sdp Message
**		cnt (OUT)		- retrieved number of times lines
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_getTimeCount _ARGS_(( SdpMessage	*msg, \
		SIP_U32bit	*cnt, SipError	*err  ));

#ifdef SIP_BY_REFERENCE
/***********************************************************************
** FUNCTION: sdp_getTimeAtIndex
** DESCRIPTION: get time line at specified index from Sdp Message
** PARAMETERS:
**		msg (IN)		- Sdp Message
**		time (OUT)		- retrieved time line
**		cnt (IN)		- Index to retrieve from
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_getTimeAtIndex _ARGS_(( SdpMessage 	*msg, \
		SdpTime 	**time, SIP_U32bit 	cnt, SipError 	*err ));

#else
/***********************************************************************
** FUNCTION: sdp_getTimeAtIndex
** DESCRIPTION: get time line at specified index from Sdp Message
** PARAMETERS:
**		msg (IN)		- Sdp Message
**		time (OUT)		- retrieved time line
**		cnt (IN)		- Index to retrieve from
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_getTimeAtIndex _ARGS_(( SdpMessage 	*msg, \
		SdpTime 	*time, SIP_U32bit 	cnt, SipError 	*err ));

#endif
/***********************************************************************
** FUNCTION: sdp_setTimeAtIndex
** DESCRIPTION: set time line at specified index in Sdp Message
** PARAMETERS:
**		msg (IN/OUT)	- Sdp Message
**		time (IN)		- time line to set
**		cnt (IN)		- Index to set at
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_setTimeAtIndex _ARGS_(( SdpMessage 	*msg, \
		SdpTime	*time, SIP_U32bit 	cnt, SipError 	*err ));

/***********************************************************************
** FUNCTION: sdp_insertTimeAtIndex
** DESCRIPTION: insert time line at specified index in Sdp Message
** PARAMETERS:
**		msg (IN/OUT)	- Sdp Message
**		time (IN)		- time line to insert
**		cnt (IN)		- Index to insert at
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
**
************************************************************************/
extern 	SipBool sdp_insertTimeAtIndex _ARGS_(( SdpMessage 	*msg, \
		SdpTime	*time, SIP_U32bit 	cnt, SipError 	*err ));

/***********************************************************************
** FUNCTION: sdp_deleteTimeAtIndex
** DESCRIPTION: delete time line at specified index from Sdp Message
** PARAMETERS:
**		msg (IN/OUT)	- Sdp Message
**		cnt (IN)		- Index to delete at
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_deleteTimeAtIndex _ARGS_(( SdpMessage 	*msg, \
		SIP_U32bit 	cnt, SipError 	*err ));

/***********************************************************************
** FUNCTION: sdp_getAttrCount
** DESCRIPTION: get number of attribute lines from Sdp Message
** PARAMETERS:
**		msg (IN)		- Sdp Message
**		cnt (OUT)		- retrieved number of attribute lines
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_getAttrCount _ARGS_(( SdpMessage	*msg, \
		SIP_U32bit	*cnt, SipError	*err  ));

#ifdef SIP_BY_REFERENCE
/***********************************************************************
** FUNCTION: sdp_getAttrAtIndex
** DESCRIPTION: get attribute line at specified index from Sdp Message
** PARAMETERS:
**		msg (IN)		- Sdp Message
**		attr(OUT)		- retrieved attribute line
**		cnt (IN)		- Index to retrieve from
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_getAttrAtIndex _ARGS_(( SdpMessage 	*msg, \
		SdpAttr 	**attr, SIP_U32bit 	cnt, SipError 	*err ));

#else
/***********************************************************************
** FUNCTION: sdp_getAttrAtIndex
** DESCRIPTION: get attribute line at specified index from Sdp Message
** PARAMETERS:
**		msg (IN)		- Sdp Message
**		attr(OUT)		- retrieved attribute line
**		cnt (IN)		- Index to retrieve from
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_getAttrAtIndex _ARGS_(( SdpMessage 	*msg, \
		SdpAttr 	*attr, SIP_U32bit 	cnt, SipError 	*err ));

#endif
/***********************************************************************
** FUNCTION: sdp_setAttrAtIndex
** DESCRIPTION: set attribute line at specified index in Sdp Message
** PARAMETERS:
**		msg (IN/OUT) 		- Sdp Message
**		attr (IN)			- attribute line to set
**		cnt (IN)			- index to set into
**		err	(OUT)			- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_setAttrAtIndex _ARGS_(( SdpMessage 	*msg, \
		SdpAttr	*attr, SIP_U32bit 	cnt, SipError 	*err ));

/***********************************************************************
** FUNCTION: sdp_insertAttrAtIndex
** DESCRIPTION: insert attribute line at specified index in Sdp Message
** PARAMETERS:
**		msg (IN/OUT) 		- Sdp Message
**		attr (IN)			- attribute line to insert
**		cnt (IN)			- index to insert into
**		err	(OUT)			- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_insertAttrAtIndex _ARGS_(( SdpMessage 	*msg, \
		SdpAttr	*attr, SIP_U32bit 	cnt, SipError 	*err ));

/***********************************************************************
** FUNCTION: sdp_deleteAttrAtIndex
** DESCRIPTION: delete attribute line at specified index in Sdp Message
** PARAMETERS:
**		msg (IN/OUT) 		- Sdp Message
**		attr (IN)			- attribute line to delete
**		cnt (IN)			- index to delete from
**		err	(OUT)			- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_deleteAttrAtIndex _ARGS_(( SdpMessage 	*msg, \
		SIP_U32bit 	cnt, SipError 	*err ));


/***********************************************************************
** FUNCTION: sdp_getNetTypeFromConnection
** DESCRIPTION: get network type field from connection line
** PARAMETERS:
**	connection (IN)		- Sdp Connection line
**		ntype (OUT)		- retrieved network type field
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_getNetTypeFromConnection _ARGS_(( SdpConnection *connection, \
		SIP_S8bit **ntype,SipError *err ));

/***********************************************************************
** FUNCTION: sdp_setNetTypeInConnection
** DESCRIPTION: set network type field in connection line
** PARAMETERS:
**		connection (IN/OUT)	- Sdp Connection line
**		ntype (IN)			- network type field to set
**		err	(OUT)			- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool

************************************************************************/
extern SipBool sdp_setNetTypeInConnection _ARGS_(( SdpConnection*connection, \
		SIP_S8bit *ntype, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_getAddrTypeFromConnection
** DESCRIPTION: get address type field from connection line
** PARAMETERS:
**		connection (IN)		- Sdp connection line
**		atype (OUT)			- retrieved address type field
**		err	(OUT)			- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_getAddrTypeFromConnection _ARGS_(( SdpConnection *connection, \
		SIP_S8bit **atype, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_setAddrTypeInConnection
** DESCRIPTION: set address type field in connection line
** PARAMETERS:
**		connection (IN/OUT)	- Sdp Connection line
**		atype (IN)			- Addres type field to set
**		err	(OUT)			- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_setAddrTypeInConnection _ARGS_(( SdpConnection *connection,  \
		SIP_S8bit *atype, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_getStartFromTime
** DESCRIPTION: retrieve start time from time line
** PARAMETERS:
**		time (IN)		- Sdp Time
**		start (OUT)		- retrieved start time
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_getStartFromTime _ARGS_(( SdpTime *time,SIP_S8bit **start, 	  \
		SipError *err ));

/***********************************************************************
** FUNCTION: sdp_setStartInTime
** DESCRIPTION: set start time in time  line
** PARAMETERS:
**		time (IN/OUT)	- Sdp Time
**		start (IN)		- Start time to set
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_setStartInTime _ARGS_(( SdpTime *time, SIP_S8bit *start, \
		SipError *err ));

/***********************************************************************
** FUNCTION: sdp_getStopFromTime
** DESCRIPTION: get stop time from time line
** PARAMETERS:
**		time (IN)		- Sdp Time
**		stop(OUT)		- stop time retrieved
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_getStopFromTime _ARGS_(( SdpTime *time,SIP_S8bit	**stop, \
		SipError *err ));

/***********************************************************************
** FUNCTION: sdp_setStopInTime
** DESCRIPTION: set stop time in time line
** PARAMETERS:
**		time (IN/OUT)	- Sdp Time
**		stop (IN)		- stop time to set
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_setStopInTime _ARGS_(( SdpTime *time, \
		SIP_S8bit *stop,  SipError *err ));

/***********************************************************************
** FUNCTION: sdp_getRepeatCountFromTime
** DESCRIPTION: get number of repeat lines in time line (r=)
** PARAMETERS:
**		time (IN)		-  Sdp Time line
**		index (OUT)		- number of repeat lines
**		err	(OUT)		- Possible error value (see API ref doc)
************************************************************************/
extern SipBool sdp_getRepeatCountFromTime _ARGS_(( SdpTime *time, \
		SIP_U32bit *index, SipError	*err ));

/***********************************************************************
** FUNCTION: sdp_getRepeatAtIndexFromTime
** DESCRIPTION: get repeat line at specified index from Sdp Time line
** PARAMETERS:
**		time (IN)		- Sdp Time
**		repeat (OUT)	- retrieved repeat line
**		index (IN)		- index to retrieve from
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_getRepeatAtIndexFromTime _ARGS_(( SdpTime *time, \
		SIP_S8bit **repeat,SIP_U32bit index, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_setRepeatAtIndexInTime
** DESCRIPTION: set repeat line at specified index in Sdp Time line
** PARAMETERS:
**		time (IN/OUT)	- Sdp Time
**		repeat (IN)		- repeat line to set
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_setRepeatAtIndexInTime _ARGS_(( SdpTime *time,  \
		SIP_S8bit 	*repeat, SIP_U32bit index, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_insertRepeatAtIndexInTime
** DESCRIPTION: insert repeat line at specified index in Sdp Time line
** PARAMETERS:
**		time (IN/OUT)	- Sdp Time
**		repeat (IN)		- repeat line to insert
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_insertRepeatAtIndexInTime _ARGS_(( SdpTime *time, \
		SIP_S8bit *repeat, SIP_U32bit index, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_deleteRepeatAtIndexInTime
** DESCRIPTION: delete repeat line at specified index in Sdp Time line
** PARAMETERS:
**		time (IN/OUT)	- Sdp Time
**		index (IN)		- Index to delte repeat line from
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_deleteRepeatAtIndexInTime _ARGS_(( SdpTime *time, \
		SIP_U32bit index,SipError *err ));

/***********************************************************************
** FUNCTION: sdp_getZoneFromTime
** DESCRIPTION: retrieve Zone field from Sdp Time line
** PARAMETERS:
**	time (IN)		- Sdp Time line
**	zone (OUT)		- retrieved zone line
**	err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_getZoneFromTime _ARGS_(( SdpTime *time, \
		SIP_S8bit **zone, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_setZoneInTime
** DESCRIPTION: set Zone field in Sdp Time line
** PARAMETERS:
**		time (IN)		- Sdp Time line
**		zone (IN)		- zone field to set
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_setZoneInTime _ARGS_(( SdpTime *time, \
		SIP_S8bit *zone, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_getAddrFromConnection
** DESCRIPTION: get address field from connection line
** PARAMETERS:
**		connection (IN)		- Sdp Connection line
**		addr (OUT)			- retrieved address field
**		err	(OUT)			- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_getAddrFromConnection _ARGS_(( SdpConnection *connection,\
		SIP_S8bit **addr, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_setAddrInConnection
** DESCRIPTION: set address field in connection line
** PARAMETERS:
**		connection (IN/OUT)	- Sdp Connection line
**		addr (IN)			- address field to set
**		err	(OUT)			- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_setAddrInConnection	 _ARGS_(( SdpConnection	*connection, \
		SIP_S8bit *addr, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_getNameFromAttr
** DESCRIPTION: get name field from attribute line
** PARAMETERS:
**		attr (IN)		- Sdp Attribute line
**		name (OUT)		- retrieved name field
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_getNameFromAttr _ARGS_(( SdpAttr *attr, \
		SIP_S8bit **name, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_setNameInAttr
** DESCRIPTION: set name field in attribute line
** PARAMETERS:
**		attr (IN/OUT)		- Sdp attribute line
**		name (IN)			- name field to set
**		err	(OUT)			- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_setNameInAttr _ARGS_(( SdpAttr *attr, \
		SIP_S8bit *name, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_getValueFromAttr
** DESCRIPTION: get value field from attribute line
** PARAMETERS:
**		attr (IN)		- Sdp Attribute line
**		value (OUT)		- value field retrieved
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_getValueFromAttr _ARGS_(( SdpAttr *attr, \
		SIP_S8bit **value, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_setValueInAttr
** DESCRIPTION: set value field in attribute line
** PARAMETERS:
**		attr (IN/OUT)	- Sdp Attribute line
**		value (IN)		- Sdp value field to set
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_setValueInAttr _ARGS_(( SdpAttr *attr, \
		SIP_S8bit *value,  SipError *err ));

/***********************************************************************
** FUNCTION: sdp_getInfoFromMedia
** DESCRIPTION: get information line from Media line
** PARAMETERS:
**		media (IN)	- Sdp Media line
**		info (OUT)	- retrieved information field
**		err	(OUT)	- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_getInfoFromMedia _ARGS_(( SdpMedia	*media, \
		SIP_S8bit 	**info,SipError	*err ));

/***********************************************************************
** FUNCTION: sdp_setInfoInMedia
** DESCRIPTION: set information line of media line
** PARAMETERS:
**		media (IN/OUT)	- Sdp Media line
**		info (IN)	- retrieved information field
**		err	(OUT)	- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_setInfoInMedia _ARGS_(( SdpMedia *media,  \
		SIP_S8bit *info,  SipError *err ));

/***********************************************************************
** FUNCTION: sdp_getBandwidthCountFromMedia
** DESCRIPTION: get number of bandwidth lines in Sdp Medialine
** PARAMETERS:
**  	pMedia (IN)		- Sdp Media
**	pBWidthCount (OUT)	- number of bandwidth lines
**		pErr	(OUT)	- Possible error value (see API ref doc)
************************************************************************/
extern 	SipBool sdp_getBandwidthCountFromMedia _ARGS_(( SdpMedia *pMedia, \
		SIP_U32bit *pBWidthCount, SipError *pErr ));

/***********************************************************************
** FUNCTION: sdp_getBandwidthAtIndexFromMedia
** DESCRIPTION: get bandwidth line at a specified index from Sdp Media line
** PARAMETERS:
**		pMedia (IN)		- Sdp Media line
**		ppBwidth(OUT)	- retrieved bandwidh field
**		dIndex (IN)		- Index at which to retrieve
**		pErr	(OUT)	- Possible error value (see API ref doc)
************************************************************************/
extern 	SipBool sdp_getBandwidthAtIndexFromMedia _ARGS_(( SdpMedia *pMedia, \
		SIP_S8bit **ppBwidth, SIP_U32bit dIndex, SipError *pErr ));


/***********************************************************************
** FUNCTION: sdp_setBandwidthAtIndexInMedia
** DESCRIPTION:	set bandwidth line at a specified index in Sdp Media line
** PARAMETERS:
**		pMedia (IN/OUT)		- Sdp Media line
**		pBWidth(IN) 		- bandwidth value to set
** 		dIndex (IN)			- Index at which to set
**		pErr	(OUT)			- Possible error value (see API ref doc)
************************************************************************/
extern 	SipBool sdp_setBandwidthAtIndexInMedia _ARGS_(( SdpMedia *pMedia, \
		SIP_S8bit *pBWidth, SIP_U32bit dIndex, SipError *pErr ));


/***********************************************************************
** FUNCTION: sdp_insertBandwidthAtIndexInMedia
** DESCRIPTION:	insert bandwidth line at a specified index in Sdp Media line
** PARAMETERS:
**		pMedia (IN/OUT)		- Sdp Media line
**		pBWidth(IN) 		- bandwidth value to insert
** 		dIndex (IN)			- Index at which to insert
**		pErr(OUT)			- Possible error value (see API ref doc)
************************************************************************/
extern 	SipBool sdp_insertBandwidthAtIndexInMedia _ARGS_(( SdpMedia *pMedia, \
		SIP_S8bit *pBWidth, SIP_U32bit dIndex, SipError *pErr ));


/***********************************************************************
** FUNCTION: sdp_deleteBandwidthAtIndexInMedia
** DESCRIPTION:	delete bandwidth line at a specified index in Sdp Media line
** PARAMETERS:
**		pMedia (IN/OUT)		- Sdp Media line
** 		dIndex  (IN)			- Index at which to delete
**		pErr   (OUT)		- Possible error value (see API ref doc)
************************************************************************/
extern 	SipBool sdp_deleteBandwidthAtIndexInMedia _ARGS_(( SdpMedia *pMedia, \
		SIP_U32bit dIndex, SipError *err ));


/***********************************************************************
** FUNCTION: sdp_getKeyFromMedia
** DESCRIPTION: get key line from Media line
** PARAMETERS:
**		media (IN)		- Sdp Media line
**		key (OUT)		- retrieved key line
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_getKeyFromMedia _ARGS_(( SdpMedia *media, \
		SIP_S8bit **key,  SipError *err ));

/***********************************************************************
** FUNCTION: sdp_setKeyInMedia
** DESCRIPTION: set key line in media line
** PARAMETERS:
**		media (IN/OUT)		- Sdp Media line
**		key (IN)			- key line to set
**		err	(OUT)			- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_setKeyInMedia _ARGS_(( SdpMedia *media, \
		SIP_S8bit *key,  SipError *err ));

/***********************************************************************
** FUNCTION: sdp_getMvalueFromMedia
** DESCRIPTION: get media value from media line
** PARAMETERS:
**		media (IN)		- Sdp Media line
**		mvalue (OUT)	- retrieved media value line
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_getMvalueFromMedia _ARGS_(( SdpMedia *media,\
		SIP_S8bit **mvalue, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_setMvalueInMedia
** DESCRIPTION: set media value in media line
** PARAMETERS:
**		media (IN/OUT)		- Sdp Media line
**		mvalue (IN)			- media value line to set
**		err	(OUT)			- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool

************************************************************************/
extern SipBool sdp_setMvalueInMedia _ARGS_(( SdpMedia *media,\
		SIP_S8bit *mvalue,  SipError *err ));

/***********************************************************************
** FUNCTION: sdp_getFormatFromMedia
** DESCRIPTION: get format field from Media line
** PARAMETERS:
**		media (IN)		- Sdp Media line
**		format (OUT)	- retrieved Sdp Format field
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_getFormatFromMedia _ARGS_(( SdpMedia *media, \
		SIP_S8bit **format,SipError *err ));

/***********************************************************************
** FUNCTION: sdp_setFormatInMedia
** DESCRIPTION: set format field in Media line
** PARAMETERS:
**		media (IN/OUT)	- Sdp Media line
**		format (IN)		- format field to set
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
**
************************************************************************/
extern SipBool sdp_setFormatInMedia _ARGS_(( SdpMedia *media, \
		SIP_S8bit *format, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_getProtoFromMedia
** DESCRIPTION: get protocol field from Media line
** PARAMETERS:
**		media (IN)		- Sdp Media
**		proto (OUT)		- retrieved protocol field
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_getProtoFromMedia _ARGS_(( SdpMedia *media,\
		SIP_S8bit **proto, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_setProtoInMedia
* DESCRIPTION: set protocol field in Media line
** PARAMETERS:
**		media (IN/OUT)	- Sdp Media
**		proto (IN)		- protocol field to set
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
**
************************************************************************/
extern SipBool sdp_setProtoInMedia _ARGS_(( SdpMedia *media, \
		SIP_S8bit *proto, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_getPortFromMedia
** DESCRIPTION: get port field from Media line
** PARAMETERS:
**		media (IN)		- Sdp Media
**		port (OUT)		- port field retrieved
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_getPortFromMedia _ARGS_(( SdpMedia *media, \
		SIP_U16bit *port, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_setPortInMedia
** DESCRIPTION: set port field in Media line
** PARAMETERS:
**		media (IN/OUT)	- Sdp Media
**		port (IN)		- port field to set
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_setPortInMedia _ARGS_(( SdpMedia *media, \
		SIP_U16bit port, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_getNumportFromMedia
** DESCRIPTION: get number of ports field from media line
** PARAMETERS:
**		media (IN)		- Sdp Media
**		numport (OUT)	- retrieved number of ports  field
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_getNumportFromMedia _ARGS_(( SdpMedia *media,\
		SIP_U32bit *numport, SipError 	*err));

/***********************************************************************
** FUNCTION: sdp_setNumportInMedia
** DESCRIPTION: set number of ports field in media line
** PARAMETERS:
**		media (IN/OUT)	- Sdp Media
**		numport (IN)	- retrieved number of ports  field
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_setNumportInMedia _ARGS_(( SdpMedia *media,\
		SIP_U32bit numport,SipError *err));

/***********************************************************************
** FUNCTION: sdp_getConnectionCountFromMedia
** DESCRIPTION: get number of connection lines in media line
** PARAMETERS:
**		media (IN)		- Sdp Media
**		index (OUT)		- retrieved number of connection lines
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_getConnectionCountFromMedia _ARGS_(( SdpMedia *media,\
		SIP_U32bit *index, SipError *err  ));

#ifdef SIP_BY_REFERENCE
/***********************************************************************
** FUNCTION: sdp_getConnectionAtIndexFromMedia
** DESCRIPTION: get connection line at specified index from Media line
** PARAMETERS:
**		media (IN)		- Sdp Media
**		connection(OUT)	- retrieved connection line
**		index (IN)		- index to retrieve connection line from
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_getConnectionAtIndexFromMedia _ARGS_(( SdpMedia *media, \
		SdpConnection **connection, SIP_U32bit index,  SipError *err ));

#else
/***********************************************************************
** FUNCTION: sdp_getConnectionAtIndexFromMedia
** DESCRIPTION: get connection line at specified index from Media line
** PARAMETERS:
**		media (IN)		- Sdp Media
**		connection(OUT)	- retrieved connection line
**		index (IN)		- index to retrieve connection line from
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_getConnectionAtIndexFromMedia _ARGS_(( SdpMedia *media, \
		SdpConnection *connection, SIP_U32bit index,  SipError *err ));

#endif
/***********************************************************************
** FUNCTION: sdp_setConnectionAtIndexInMedia
** DESCRIPTION: set connection line at specified index in Media line
** PARAMETERS:
**		media (IN/OUT)	- Sdp Media
**		connection(IN)	- connection line to set
**		index (IN)		- index to set connection in
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_setConnectionAtIndexInMedia _ARGS_(( SdpMedia *media,  \
		SdpConnection *connection,SIP_U32bit index, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_insertConnectionAtIndexInMedia
** DESCRIPTION: insert connection line at specified index in Media line
** PARAMETERS:
**		media (IN/OUT)	- Sdp Media
**		connection(IN)	- connection line to insert
**		index (IN)		- index to insert connection in
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_insertConnectionAtIndexInMedia _ARGS_(( SdpMedia *media,\
		SdpConnection *connection,SIP_U32bit index,SipError *err ));

/***********************************************************************
** FUNCTION: sdp_deleteConnectionAtIndexInMedia
** DESCRIPTION: delete connection at specified index in Media line
** PARAMETERS:
**		media (IN/OUT)	- Sdp Media
**		index (IN)		- index to insert connection in
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_deleteConnectionAtIndexInMedia _ARGS_(( SdpMedia *media,\
		SIP_U32bit index, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_getAttrCountFromMedia
** DESCRIPTION: get number of attribute lines in media line
** PARAMETERS:
**		media (IN)			- Sdp Media
**		index (OUT)			- number of attribute lines in media
**		err	(OUT)			- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_getAttrCountFromMedia _ARGS_(( SdpMedia *media, \
		SIP_U32bit	*index, SipError *err  ));

#ifdef SIP_BY_REFERENCE
/***********************************************************************
** FUNCTION: sdp_getAttrAtIndexFromMedia
** DESCRIPTION: get attribute line at specifed index from Media line
** PARAMETERS:
**		media (IN/OUT)		- Sdp Media
**		attr (OUT)			- retrieved attribute line
**		index (IN)			- index to retrieve from
**		err	(OUT)			- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_getAttrAtIndexFromMedia _ARGS_(( SdpMedia *media,SdpAttr **attr, \
		SIP_U32bit index,  SipError *err ));

#else
/***********************************************************************
** FUNCTION: sdp_getAttrAtIndexFromMedia
** DESCRIPTION: get attribute line at specifed index from Media line
** PARAMETERS:
**		media (IN)		- Sdp Media
**		attr (OUT)		- retrieved attribute line
**		index (IN)		- index to retrieve from
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_getAttrAtIndexFromMedia _ARGS_(( SdpMedia *media,SdpAttr *attr, \
		SIP_U32bit index,  SipError *err ));

#endif
/***********************************************************************
** FUNCTION: sdp_setAttrAtIndexInMedia
** DESCRIPTION: set attribute line at specifed index in Media line
** PARAMETERS:
**		media (IN/OUT)		- Sdp Media
**		attr (IN)			- attribute line to set
**		index (IN)			- index to set at
**		err	(OUT)			- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_setAttrAtIndexInMedia _ARGS_(( SdpMedia  *media, SdpAttr	*attr, \
		SIP_U32bit index,SipError *err ));

/***********************************************************************
** FUNCTION: sdp_insertAttrAtIndexInMedia
** DESCRIPTION: insert attribute line at specifed index in Media line
** PARAMETERS:
**		media (IN/OUT)		- Sdp Media
**		attr (IN)			- attribute line to insert at
**		index (IN)			- index to insert at
**		err	(OUT)			- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_insertAttrAtIndexInMedia _ARGS_(( SdpMedia 	*media, SdpAttr	*attr, \
		SIP_U32bit index,SipError *err ));

/***********************************************************************
** FUNCTION: sdp_deleteAttrAtIndexInMedia
** DESCRIPTION:  delete attribute line at specifed index in Media line
** PARAMETERS:
**		media (IN/OUT)		- Sdp Media
**		index (IN)			- index to  delete
**		err	(OUT)			- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sdp_deleteAttrAtIndexInMedia _ARGS_(( SdpMedia *media, \
		SIP_U32bit index, SipError *err ));


/***********************************************************************
** FUNCTION: sip_getLengthFromUnknownMessage
** DESCRIPTION: get length of unknow message body
** PARAMETERS:
**		msg(IN)		- Unknown message
**		length(OUT)	- retrieved length
**		err	(OUT)	- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sip_getLengthFromUnknownMessage _ARGS_( ( SipUnknownMessage *msg, \
		SIP_U32bit *length, SipError *err));

/***********************************************************************
** FUNCTION: sip_getBufferFromUnknownMessage
** DESCRIPTION: get unknown message body
** PARAMETERS:
**		msg (IN)		- Unknown message
**		buffer (OUT)	- retrieved message buffer
************************************************************************/
extern SipBool sip_getBufferFromUnknownMessage _ARGS_(( SipUnknownMessage *msg, \
		SIP_S8bit **buffer, SipError *err));

/***********************************************************************
** FUNCTION: sip_setBufferInUnknownMessage
** DESCRIPTION: set unknown message body
** PARAMETERS:
**		msg (IN/OUT)	- unknown message
**		buffer (IN)		- buffer to set
**		length (IN)		- length of buffer
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sip_setBufferInUnknownMessage _ARGS_(( SipUnknownMessage *msg, \
		SIP_S8bit *buffer, SIP_U32bit length,SipError *err));

/***********************************************************************
** FUNCTION: sip_getMsgBodyType
** DESCRIPTION: get type of message body that is present
** PARAMETERS:
**		body (IN)		- sip message body
**		type (OUT)		- type of message body retrieved
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sip_getMsgBodyType _ARGS_(( SipMsgBody	*body, \
		en_SipMsgBodyType *type, SipError *err));

#ifdef SIP_BY_REFERENCE
/***********************************************************************
** FUNCTION: sip_getUnknownFromMsgBody
** DESCRIPTION: get unknown message body element from sip message body
** PARAMETERS:
**		msg (IN)		- sip message body
**		unknown(OUT)	- unknown message body retrieved
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sip_getUnknownFromMsgBody _ARGS_( ( SipMsgBody *msg, \
		SipUnknownMessage **unknown, SipError *err));

#else
/***********************************************************************
** FUNCTION: sip_getUnknownFromMsgBody
** DESCRIPTION: get unknown message body element from sip message body
** PARAMETERS:
**		msg (IN)		- sip message body
**		unknown(OUT)	- unknown message body retrieved
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sip_getUnknownFromMsgBody _ARGS_( ( SipMsgBody *msg, \
		SipUnknownMessage *unknown, SipError *err));

#endif
/***********************************************************************
** FUNCTION: sip_setUnknownInMsgBody
** DESCRIPTION: set unknown message body  in sip message body
** PARAMETERS:
**		msg (IN/OUT)- sip message body
**		unknown(IN)	- unknown message body  to set
**		err	(OUT)	- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sip_setUnknownInMsgBody _ARGS_( ( SipMsgBody *msg, \
		SipUnknownMessage *unknown, SipError *err));

#ifdef SIP_BY_REFERENCE
/***********************************************************************
** FUNCTION: sip_getSdpFromMsgBody
** DESCRIPTION: get sdp body from sip message body
** PARAMETERS:
**		msg (IN)	- sip message body
**		sdp (OUT)	- retrieved sdp message body
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sip_getSdpFromMsgBody _ARGS_(( SipMsgBody *msg, \
		SdpMessage **sdp, SipError *err));

#else
/***********************************************************************
** FUNCTION: sip_getSdpFromMsgBody
** DESCRIPTION: get sdp body from sip message body
** PARAMETERS:
**		msg (IN)	- sip message body
**		sdp (OUT)	- retrieved sdp message body
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sip_getSdpFromMsgBody _ARGS_(( SipMsgBody *msg, \
		SdpMessage *sdp, SipError *err));

#endif
/***********************************************************************
** FUNCTION: sip_setSdpInMsgBody
** DESCRIPTION: set sdp body in sip message body
** PARAMETERS:
**		msg (IN/OUT)	- sip message body
**		sdp (IN)		- sdp message body to set
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern SipBool sip_setSdpInMsgBody _ARGS_(( SipMsgBody *msg, \
		SdpMessage *sdp, SipError *err));


/***********************************************************************
** FUNCTION: sdp_getIncorrectLineAtIndex
** DESCRIPTION: get incorrect line at specified index from Sdp Message
** PARAMETERS:
**		msg (IN)		- Sdp Message
**		line (OUT)		- retrieved incorrect line (string)
**		cnt (IN)		- Index at which to retrieve
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_getIncorrectLineAtIndex _ARGS_(( SdpMessage *msg, \
		SIP_S8bit **line, SIP_U32bit cnt, SipError *err ));

/********************************************************************
** FUNCTION:sdp_deleteIncorrectLineAtIndex
** DESCRIPTION: This function deletes an incorrect SDP line (allowed by
**				the application during decode) at the specified index in
**				an SDP message structure
** PARAMETERS:
**		msg (IN)		- Sdp Message
**		cnt (IN)		- Index at which to delete
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
**********************************************************************/
SipBool sdp_deleteIncorrectLineAtIndex _ARGS_((SdpMessage *msg, \
			SIP_U32bit cnt, SipError *err ));

/***********************************************************************
** FUNCTION: sdp_getIncorrectLineAtIndexFromMedia
** DESCRIPTION: get incorrect line at specified index from Sdp Media
** PARAMETERS:
**		media (IN)		- Sdp Media
**		line (OUT)		- retrieved incorrect line (string)
**		cnt (IN)		- Index at which to retrieve
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
************************************************************************/
extern 	SipBool sdp_getIncorrectLineAtIndexFromMedia _ARGS_(( SdpMedia *media, \
		SIP_S8bit **line, SIP_U32bit cnt, SipError *err ));

/********************************************************************
** FUNCTION:sdp_deleteIncorrectLineAtIndexInMedia
** DESCRIPTION: This function deletes an incorrect SDP line (allowed by
**				the application during decode) at the specified index in
**				an SDP media structure
** PARAMETERS:
**		media (IN)		- Sdp Media
**		cnt (IN)		- Index at which to delete
**		err	(OUT)		- Possible error value (see API ref doc)
** ReturnValue:
**		SipBool
**********************************************************************/
SipBool sdp_deleteIncorrectLineAtIndexInMedia _ARGS_((SdpMedia *media, \
			SIP_U32bit cnt, SipError *err ));

#endif
#ifdef SIP_ATM
/********************************************************************
** FUNCTION:sdp_getConnectionIDFromMedia
** DESCRIPTION: This function gets the ConnectionID from the Sdp Media
**
**
** PARAMETERS:
**		pMedia (IN)		- Sdp Media
**		ppVirtualID (OUT)	- Connection ID, if existed.
**		pErr	(OUT)		- Possible error value (see API ref doc)
**********************************************************************/
extern 	SipBool sdp_getConnectionIDFromMedia _ARGS_(( SdpMedia *pMedia, \
			SIP_S8bit **ppVirtualID, SipError *pErr ));

/********************************************************************
** FUNCTION:sdp_setConnectionIDFromMedia
** DESCRIPTION: This function sets the ConnectionID from the Sdp Media
**
**
** PARAMETERS:
**		pMedia (IN)		- Sdp Media
**		pVirtualID (IN)	- Connection ID to be set
**		pErr	(OUT)		- Possible error value (see API ref doc)
**********************************************************************/
extern 	SipBool sdp_setConnectionIDInMedia _ARGS_(( SdpMedia *pMedia, \
			SIP_S8bit *pVirtualID, SipError *pErr ));

/********************************************************************
** FUNCTION:sdp_getProtoFmtCountFromMedia
** DESCRIPTION: This function gets the count of Protocol-Format
**								from the Sdp Media
**
** PARAMETERS:
**		pMedia (IN)		- Sdp Media
**		pCount (OUT)	- Count of Protocol-Format Pair
**		pErr	(OUT)		- Possible error value (see API ref doc)
**********************************************************************/
extern 	SipBool sdp_getProtoFmtCountFromMedia _ARGS_(( SdpMedia *pMedia, \
			SIP_U32bit *pCount, SipError *pErr ));

#ifdef SIP_BY_REFERENCE
/********************************************************************
** FUNCTION:sdp_getProtoFmtAtIndexFromMedia
** DESCRIPTION: This function gets the Protocol-Format at a specified
**					index from the Sdp Media
**
** PARAMETERS:
**		pMedia (IN)		- Sdp Media
**		dIndex (IN)			- Index at which to get
**		ppProtofmt (OUT)    - Protocol-Format Pair
**		pErr	(OUT)		- Possible error value (see API ref doc)
**********************************************************************/
extern 	SipBool sdp_getProtoFmtAtIndexFromMedia _ARGS_(( SdpMedia *pMedia, \
			SipNameValuePair **ppProtofmt, SIP_U32bit dIndex, SipError *pErr ));
#else
/********************************************************************
** FUNCTION:sdp_getProtoFmtAtIndexFromMedia
** DESCRIPTION: This function gets the Protocol-Format at a specified
**					index from the Sdp Media
**
** PARAMETERS:
**		pMedia (IN)		- Sdp Media
**		dIndex (IN)			- Index at which to get
**		pProtofmt (OUT)     - Protocol-Format Pair
**		pErr	(OUT)		- Possible error value (see API ref doc)
**********************************************************************/
extern 	SipBool sdp_getProtoFmtAtIndexFromMedia _ARGS_(( SdpMedia *pMedia, \
			SipNameValuePair *pProtofmt, SIP_U32bit dIndex, SipError *pErr ));
#endif

/********************************************************************
** FUNCTION:sdp_setProtoFmtAtIndexInMedia
** DESCRIPTION: This function sets the Protocol-Format at a specified
**					index in the Sdp Media
**
** PARAMETERS:
**		pMedia (IN)		- Sdp Media
**		dIndex (IN)			- Index at which to set
**		pProtofmt (IN)     - Protocol-Format Pair
**		pErr	(OUT)		- Possible error value (see API ref doc)
**********************************************************************/
extern 	SipBool sdp_setProtoFmtAtIndexInMedia _ARGS_(( SdpMedia *pMedia, \
			SipNameValuePair *pProtoFmt, SIP_U32bit dIndex, SipError *pErr ));
/********************************************************************
** FUNCTION:sdp_insertProtoFmtAtIndexInMedia
** DESCRIPTION: This function inserts the Protocol-Format at a specified
**					index in the Sdp Media
**
** PARAMETERS:
**		pMedia (IN)		- Sdp Media
**		dIndex (IN)			- Index at which to insert
**		pProtofmt (IN)     - Protocol-Format Pair
**		pErr	(OUT)		- Possible error value (see API ref doc)
**********************************************************************/
extern 	SipBool sdp_insertProtoFmtAtIndexInMedia _ARGS_(( SdpMedia *pMedia, \
			SipNameValuePair *pProtoFmt, SIP_U32bit dIndex, SipError *pErr ));
/********************************************************************
** FUNCTION:sdp_deleteProtoFmtAtIndexInMedia
** DESCRIPTION: This function inserts the Protocol-Format at a specified
**					index in the Sdp Media
**
** PARAMETERS:
**		pMedia (IN)		- Sdp Media
**		dIndex (IN)			- Index at which to delete
**		pErr	(OUT)		- Possible error value (see API ref doc)
**********************************************************************/
extern 	SipBool sdp_deleteProtoFmtAtIndexInMedia _ARGS_(( SdpMedia *pMedia, \
			SIP_U32bit dIndex, SipError *pErr ));

#endif






/***********************************************************************
** FUNCTION: sip_setMethodInReqLine
** DESCRIPTION:sets the Method in the Sip Status Line
** PARAMETERS:
**			line(IN/OUT) 			- Sip Status Line
**			method(IN)			- The method to set
**			err(OUT)			- Possible error value(see API ref doc)
**
************************************************************************/
extern SipBool sip_setMethodInReqLine _ARGS_ ((SipReqLine *line, \
		SIP_S8bit *method, SipError *err));







/***********************************************************************
** FUNCTION: sip_setDispNameInToHdr
** DESCRIPTION:sets the Display Name in the Sip To Header
** PARAMETERS:
**			hdr(IN/OUT) 			- Sip To Header
**			name(IN)			- Name value to be set
**			err(OUT)			- Possible error value(see API ref doc)
**
************************************************************************/
extern SipBool sip_setDispNameInToHdr _ARGS_ ((SipHeader *hdr, \
		SIP_S8bit *name, SipError *err));

/***********************************************************************
** FUNCTION: sip_setVersionInReqLine
** DESCRIPTION:gets the Version in the Sip Request Line
** PARAMETERS:
**			line(IN/OUT) 			- Sip RequestLine
**			version(IN)			- Version to set
**			err(OUT)			- Possible error value(see API ref doc)
**
************************************************************************/
extern SipBool sip_setVersionInReqLine _ARGS_ ((SipReqLine *line, \
		SIP_S8bit *version, SipError *err));

/***********************************************************************
** FUNCTION: sip_setAddrSpecInFromHdr
** DESCRIPTION:sets the Addr Spec structure in the Sip From Header
** PARAMETERS:
**			hdr(IN/OUT) 			- Sip From Header
**			pAddrSpec(IN)		- The AddrSpec structure which is set
**			err(OUT)			- Possible error value(see API ref doc)
**
************************************************************************/
extern SipBool sip_setAddrSpecInFromHdr _ARGS_ ((SipHeader *hdr, \
		SipAddrSpec *addr_spec, SipError *err));

/***********************************************************************
** FUNCTION: sip_setUserInUrl 
** DESCRIPTION: set user field from Url
** PARAMETERS:
**		url (IN/OUT)	- Sip Url
**		user (IN)		- user field to set
**		pErr (OUT)		- Possible Error value (see API ref doc)
************************************************************************/
extern SipBool sip_setUserInUrl _ARGS_((SipUrl *url,SIP_S8bit *user, SipError *error));
/***********************************************************************
** FUNCTION: sip_setHostInUrl 
** DESCRIPTION: set host token in url
** PARAMETERS:
**		url (IN/OUT)	- Sip Url
**		host (IN)		- host to set
**		pErr (OUT)		- Possible Error value (see API ref doc)
************************************************************************/
extern SipBool sip_setHostInUrl _ARGS_((SipUrl *url,SIP_S8bit *host,SipError *error));
/***********************************************************************
** FUNCTION: sip_setUrlInAddrSpec 
** DESCRIPTION: set Url parameter in Addr Spec
** PARAMETERS: 
**		addrspec (IN/OUT)	- Addr Spec parameter
**		url (OUT)			- url to set
**		pErr (OUT)			-	Possible Error value (see API ref doc)
**
** (NOTE: "sip:" schemes are Url, non "sip:"schemes are Uri (as per grammar)
************************************************************************/
extern SipBool sip_setUrlInAddrSpec _ARGS_((SipAddrSpec *addrspec,\
		SipUrl *url, SipError *err));
/***********************************************************************
** FUNCTION: sip_setAddrSpecInReqLine
** DESCRIPTION:sets the Sip Addr Spec structure in the Sip Request Line
** PARAMETERS:
**			line(IN/OUT) 			- Sip RequestLine
**			pReqUri(IN)			- Addr Spec structure to set
**			err(OUT)			- Possible error value(see API ref doc)
**
************************************************************************/
extern SipBool sip_setAddrSpecInReqLine _ARGS_ ((SipReqLine *line, \
		SipAddrSpec *req_uri, SipError *err));









/* Ensure Names are not mangled by C++ compilers */
#ifdef __cplusplus
}
#endif

