/*  ==============================================================
    Filename: parsetime.c
    --------

    Details:
    -------
    This test stub can be used to calculate the time taken by the stack to
    parse a message. The application waits for the receipt of a SIP
    message in the form of a text buffer. On receipt of the same it 
    performs sip_decodeMessage() on the message for a finite number of times.
    This takes place within a loop. At the end of every iteration the decoded
    SipMessage is freed to prevent memory build-up.

    There is a call to 
    i) clock in case of Solaris
    ii) clock_gettime in case of VxWorks
    iii) get_ticks() in case of OSE
    at the beginning of the loop and at the end which gives us the
    CPU time (or absolute time in case of VxWorks/OSE) taken for those
    many decodes.

    Dividing the time taken by the number of iterations gives the 
    decode time for one message.

    Notes:
    -----
    1. The stack should be compiled in SIP_BY_REFERENCE mode to execute
        this program.
    
    2. For VxWorks please change the define for TARGET_IP at the
        beginning of the file.

    2.  The number of iterations is configurable and can be changed
        by changing the define for NUM_ITERATIONS

    Usage:
    -----
         parsetime <listening_port>
    
    ==============================================================
                  Copyright (C) 2006 Aricent Inc . All Rights Reserved
    ============================================================== */

#ifdef __cplusplus
extern "C" {
#endif

#define TARGET_IP "139.85.229.176"

#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/time.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>
#include <strings.h>

#include "microsip_portlayer.h"
#include "microsip_decode.h"
#include "microsip_struct.h"
#include "microsip_sendmessage.h"
#include "microsip_list.h"
#include "microsip_init.h"
#include "microsip_free.h"
#include "microsip_accessors.h"
#include "microsip_statistics.h"
#include "microsip_timer.h"
#include "microsip_trace.h"
#include "microsip_callbacks.h"

#define MAXMESG 5000

#ifndef NUM_ITERATIONS
#define NUM_ITERATIONS 1000
#endif



void sip_indicateMessage(SipMessage *s, SipEventContext *context); 
void sip_freeEventContext(SipEventContext *context)
{
(void) context;
}


SipBool fast_startTimer( SIP_U32bit duration, SIP_S8bit restart,SipBool (*timeoutfunc)(SipTimerKey *key, SIP_Pvoid buf), SIP_Pvoid buffer, SipTimerKey *key, SipError *err)
{
	(void)duration;
	(void)restart;
	(void)timeoutfunc;
	(void)buffer;
	(void)key;
	(void)err;
	return SipSuccess;
}

/* Implementaion of the fast_stopTimer interface reqiuired by the stack 
   Application developers may choose to implement this function in any
   manner while preserving the interface
*/
SipBool fast_stopTimer(SipTimerKey *inkey, SipTimerKey **outkey, SIP_Pvoid *buffer,  SipError *err)
{
	(void)inkey;
	(void)outkey;
	(void)buffer;
	(void)err;
	return SipSuccess;
}

SipBool sip_sendToNetwork 
#ifdef ANSI_PROTO
( SIP_S8bit *buffer, SIP_U32bit buflen,SipTranspAddr *addr, SIP_S8bit transptype, SipError *err)
#else
	(buffer, buflen, addr, transptype, err)
	SIP_S8bit *buffer;
	SIP_U32bit buflen;
	SipTranspAddr *addr;
	SIP_S8bit transptype;
	SipError *err;
#endif
{
	(void)buffer;
	(void)buflen;
	(void)addr;
	(void)transptype;
	(void)err;
	return SipSuccess;
}

#ifdef SIP_TXN_LAYER
#ifdef ANSI_PROTO
void sip_indicateTimeOut( SipEventContext *context,en_SipTimerType dTimer)
#else
void sip_indicateTimeOut(context,dTimer)
        SipEventContext *context;
        en_SipTimerType dTimer;
#endif
#else
#ifdef ANSI_PROTO
void sip_indicateTimeOut( SipEventContext *context)
#else
void sip_indicateTimeOut(context)
        SipEventContext *context;
#endif
#endif
{
	(void)context;
#ifdef SIP_TXN_LAYER
	(void)dTimer;
#endif
}



#ifdef SIP_RETRANSCALLBACK
#ifdef  SIP_TXN_LAYER
/******************************************************************************
 ** FUNCTION: 		sip_indicateMessageRetransmission
 **
 ** DESCRIPTION: 	This is a callback function that is invoked by the stack
 **					when the SIP_RETRANSCALLBACK option is enabled.
 **
 ******************************************************************************/
void sip_indicateMessageRetransmission (SipEventContext *pContext,\
	SipTxnKey *pKey, SIP_S8bit *pBuffer,\
	SIP_U32bit dBufferLength, SipTranspAddr *pAddr, SIP_U8bit dRetransCount,\
	SIP_U32bit dDuration)
{
	(void) pContext;
	(void) pKey;
	(void) dBufferLength;
	(void) pAddr;
	(void ) dRetransCount;
	(void ) pBuffer;
	(void ) dDuration;
	SIPDEBUG("Inside sip_indicateMessageRetransmission.\n");
}
#else
/*******************************************************************************
 ** FUNCTION: 		sip_indicateMessageRetransmission
 **
 ** DESCRIPTION: 	This is a callback function that is invoked by the stack
 **					when the SIP_RETRANSCALLBACK option is enabled.
 **
 ******************************************************************************/
void sip_indicateMessageRetransmission (SipEventContext *pContext,\
	SipTimerKey *pKey, SIP_S8bit *pBuffer,\
	SIP_U32bit dBufferLength, SipTranspAddr *pAddr, SIP_U8bit dRetransCount,\
	SIP_U32bit dDuration)
{
	(void) pContext;
	(void) pKey;
	(void) dBufferLength;
	(void) pAddr;
	(void ) dRetransCount;
	(void ) pBuffer;
	(void ) dDuration;
	SIPDEBUG("Inside sip_indicateMessageRetransmission.\n");
}
#endif
#endif

#ifndef SIP_NO_CALLBACK
void sip_indicateInvite(SipMessage *s, SipEventContext *context) 
{
	(void)context;
	sip_freeSipMessage(s);
}

void sip_indicateRegister(SipMessage *s, SipEventContext *context) 
{
	(void)context;
	sip_freeSipMessage(s);
}

void sip_indicateCancel(SipMessage *s, SipEventContext *context) 
{
	(void)context;
	sip_freeSipMessage(s);
}

void sip_indicateOptions(SipMessage *s, SipEventContext *context) 
{
	(void)context;
	sip_freeSipMessage(s);
}

void sip_indicateBye(SipMessage *s, SipEventContext *context) 
{
	(void)context;
	sip_freeSipMessage(s);
}


void sip_indicateAck(SipMessage *s, SipEventContext *context) 
{
	(void)context;
	sip_freeSipMessage(s);
}

void sip_indicateInfo(SipMessage *s, SipEventContext *context) 
{
	(void)context;
	sip_freeSipMessage(s);
}

void sip_indicatePropose(SipMessage *s, SipEventContext *context) 
{
	(void)context;
	sip_freeSipMessage(s);
}

void sip_indicatePrack(SipMessage *s, SipEventContext *context) 
{
	(void)context;
	sip_freeSipMessage(s);
}

void sip_indicateUnknownRequest(SipMessage *s, SipEventContext *context) 
{
	(void)context;
	sip_freeSipMessage(s);
}

void sip_indicateInformational(SipMessage *s, SipEventContext *context) 
{
	(void)context;
	sip_freeSipMessage(s);
}

void sip_indicateFinalResponse(SipMessage *s, SipEventContext *context) 
{
	(void)context;
	sip_freeSipMessage(s);
}

void sip_indicateRefer(SipMessage *s, SipEventContext *context) 
{
	(void)context;
	sip_freeSipMessage(s);
}
#ifdef SIP_IMPP
void sip_indicateSubscribe(SipMessage *s, SipEventContext *context) 
{
	(void)context;
	sip_freeSipMessage(s);
}
void sip_indicateNotify(SipMessage *s, SipEventContext *context) 
{
	(void)context;
	sip_freeSipMessage(s);
}

#endif
void sip_indicateMessage(SipMessage *s, SipEventContext *context) 
{
	(void)context;
	sip_freeSipMessage(s);
}

void sip_indicateComet(SipMessage *s, SipEventContext *context) 
{
	(void)s;
	(void)context;
	sip_freeSipMessage(s);
}

#endif

/* End of callback implementations */
int main(int argc, char * argv[])
{	
	int			sockfd, clilen,n;
	struct sockaddr_in	serv_addr, cli_addr;
	fd_set readfs;
	char *message;
	SIP_S8bit *nextmesg;
	SipEventContext context;
	SipError error;
	SipBool r;
	SIP_U32bit stat;
#if defined(SIP_SOLARIS) || defined(SIP_LINUX)
	clock_t pstime1, pstime;
#endif

#ifdef SIP_INCREMENTAL_PARSING	
	SIP_U32bit j;
#endif	
#ifdef SIP_INCREMENTAL_PARSING
	context.pList=SIP_NULL;
	if(context.pList == SIP_NULL)
		context.pList=(SipHdrTypeList *)\
	fast_memget(0,sizeof(SipHdrTypeList), SIP_NULL);
	for(j=0;j<HEADERTYPENUM;j++)
		context.pList->enable[j]=SipSuccess;

#endif	
	
	if(argc<2)
	{
		printf("Usage:\n");
		printf("%s my_port\n",argv[0]);
		exit(0);
	}	

	
	if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
	{
		printf("Server could not open dgram socket\n");
		close(sockfd);
		exit(0);
	}

	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family      = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port        = htons(atoi(argv[1]));

	bzero((char *) &cli_addr, sizeof(cli_addr));	/* zero out */
	cli_addr.sin_family      = AF_INET;
	cli_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	cli_addr.sin_port        = htons(0);

	clilen=sizeof(cli_addr);
	
	if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
	{
		printf("Server couldn't bind to local address.\n");
		close(sockfd);
		exit(0);
	}

	/* Stack and trace initialization */
	sip_initStack();

	printf("\nMicro Sip stack product id is:%s\n", (char *)(sip_getPart()) );

	if (sip_setErrorLevel(SIP_Major|SIP_Minor|SIP_Critical, &error)==SipFail)
	{
		printf ("########## Error Disabled at compile time #####\n");
	}
		

	if (sip_setTraceLevel(SIP_Brief,&error)== SipFail)
	{
		printf ("########## Trace Disabled at compile time #####\n");
	}
	sip_setTraceType(SIP_All,&error);

	
	for(;;) 
	{
		int i;
		struct timeval tv1, tv2;
#ifdef SIP_NO_CALLBACK
		SipMessage *pMsg;
#endif

		/* get minimum timeout from the timer list */
		FD_ZERO(&readfs);
		FD_SET(sockfd,&readfs);
		FD_SET(0,&readfs);

		gettimeofday(&tv1, NULL);
		if (select(sockfd+1, &readfs, NULL, NULL, NULL) < 0)
		{
				printf("select returned < 0\n");
				fflush(stdout);
				exit(0);
		}	

		gettimeofday(&tv2, NULL);

		if(FD_ISSET(sockfd,&readfs))
		{
			/* Message on the receive port */
			message = (char *) malloc(sizeof(char)*MAXMESG);
			n = recvfrom(sockfd, message, MAXMESG, 0,(struct sockaddr *) \
				&cli_addr, &clilen);

			if (n < 0)
			{
				printf("Server : Error in receive.\n");
				close(sockfd);
				exit(0);
			}
			message[n]='\0';
			do
			{
				SipOptions opt;

				/* calclulate time to parse one complete message */
				opt.dOption  = SIP_OPT_NOTIMER;
				pstime = clock();

				for(i=0; i<NUM_ITERATIONS; i++)
				{
#ifndef SIP_NO_CALLBACK
					r= sip_decodeMessage((SIP_S8bit *)message,&opt, \
						strlen(message),&nextmesg,&context, &error);
#else
					r= sip_decodeMessage((SIP_S8bit *)message,&pMsg,&opt, \
						strlen(message),&nextmesg,&context, &error);
					sip_freeSipMessage(pMsg);
#endif
				}
				pstime1 = clock();

#if defined(SIP_SOLARIS) || defined(SIP_LINUX)
				printf("\n=========== Results for complete message parsing"\
					"=========\n\n");
				printf("CPU clock ticks for %d messages - %d\n", \
					NUM_ITERATIONS, pstime1-pstime);
				printf("CPU time for %d messages - %f sec\n", \
					NUM_ITERATIONS, (float)(pstime1-pstime)/CLOCKS_PER_SEC);
				printf("\n=========== Results for complete message parsing"\
					"=========\n");
#endif


				if (r==SipFail)
				{
					printf ("+++++++ BAD MESSAGE %d++++++++++\n",error);
				}

				/* calclulate time to parse one message without parsing message*/
				opt.dOption  = SIP_OPT_NOPARSEBODY;
				opt.dOption  |= SIP_OPT_NOTIMER;

				pstime = clock();

				for(i=0; i<NUM_ITERATIONS; i++)
#ifndef SIP_NO_CALLBACK
				r= sip_decodeMessage((SIP_S8bit *)message,&opt,	\
					strlen(message),&nextmesg,&context, &error);
#else
				r= sip_decodeMessage((SIP_S8bit *)message,&pMsg,&opt, \
					strlen(message),&nextmesg,&context, &error);
				sip_freeSipMessage(pMsg);
#endif
				
				pstime1 = clock();

#if defined(SIP_SOLARIS) || defined(SIP_LINUX)
				printf("\n=========== Results for message parsing (without "\
					"body) =========\n\n");
				
				printf("CPU clock ticks for %d messages - %d\n", \
					NUM_ITERATIONS, pstime1-pstime);
				printf("CPU time for %d messages - %f sec\n", \
					NUM_ITERATIONS, (float)(pstime1-pstime)/CLOCKS_PER_SEC);
				printf("\n=========== Results for message parsing "\
					"(without body) =========\n");
#endif


				fast_memfree(0,message, SIP_NULL);
				message = nextmesg;
			} while (message != SIP_NULL);
		} 
		else if(FD_ISSET(0,&readfs))
		{
			/* Key pressed */
			char c;

			c=getchar();
			if ( sip_getStatistics(SIP_STAT_TYPE_API, SIP_STAT_API_COUNT, \
				SIP_STAT_NO_RESET, &stat, &error)==SipFail)
			{
				printf ("##### Cannot display Statistics - Stats disabled \
					#####\n");
			}
			else
			{
			printf("\n######## Stack statistics #######\n");
			sip_getStatistics(SIP_STAT_TYPE_API, SIP_STAT_API_REQ_PARSED, \
				SIP_STAT_NO_RESET, &stat, &error);
			printf("######## Requests parsed 	:%d\n",stat);
			}
			if (c == 'q')
				exit(0);
		}
	}
}


#ifdef __cplusplus
}
#endif
