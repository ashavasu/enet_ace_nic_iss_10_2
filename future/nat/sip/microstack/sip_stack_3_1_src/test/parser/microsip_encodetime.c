/* 	==============================================================
	Filename: encodetime.c
	--------
        $Id: microsip_encodetime.c,v 1.1.1.1 2010/12/08 06:10:43 siva Exp $
	Details:
	-------
	This test stub can be used to calculate the encoding time of the stack.
	Encoding time is calculated in two ways :
	a) sip_formMessage is called NUM_ITERATIONS times and the average time
		to encode the message is computed.
	b) A sample SipMessage is formed using the SIP stack accessor API's and
		finally sip_formMessage is called. This is done NUM_ITERATIONS times
		and the average time to encode the message is computed.

	There is a call to
	i) clock in case of Solaris
	ii) clock_gettime in case of VxWorks
	iii) get_ticks() in case of OSE
	at the beginning of the loop and at the end which gives us the
	CPU time (or absolute time in case of VxWorks/OSE) taken for those
	many decodes.

	Dividing the time taken by the number of iterations gives the
	encode time for one message.

	Notes:
	-----
	1. The stack should be compiled in SIP_BY_REFERENCE mode to execute
		this program.

	2. For VxWorks please change the define for TARGET_IP at the
		beginning of the file.

	2. 	The number of iterations is configurable and can be changed
		by changing the define for NUM_ITERATIONS

	Usage:
	-----
		 encodetime <listening_port>

	==============================================================
				(c) Aricent , 2001
	============================================================== */
#ifdef __cplusplus
extern "C"{
#endif

#define TARGET_IP "139.85.229.176"

#ifndef NUM_ITERATIONS
#define NUM_ITERATIONS  1000
#endif

#if defined(SIP_SOLARIS) || defined(SIP_LINUX)
#include <sys/types.h>
#include <sys/time.h>
#include <strings.h>
#endif

#include <sys/types.h>
#include <time.h>
#include <string.h>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include "microsip_encodetime.h"
#include "microsip_portlayer.h"
#include "microsip_decode.h"
#include "microsip_struct.h"
#include "microsip_sendmessage.h"
#include "microsip_list.h"
#include "microsip_init.h"
#include "microsip_free.h"
#include "microsip_accessors.h"
#include "microsip_statistics.h"
#include "microsip_timer.h"
#include "microsip_trace.h"
#include "microsip_callbacks.h"
#define MAXMESG 5000

int showMessage = 1;
int constructCLen;




void sip_indicateMessage(SipMessage *s, SipEventContext *context);
void sip_indicateUpdate(SipMessage *s, SipEventContext *context);
SipBool sip_decryptBuffer(SipMessage *s, SIP_S8bit *encinbuffer, SIP_U32bit clen,SIP_S8bit **encoutbuffer, SIP_U32bit *outlen);

void sip_freeEventContext(SipEventContext *context)
{
	(void)context;
}

SipBool sip_decryptBuffer(SipMessage *s, SIP_S8bit *encinbuffer, SIP_U32bit clen,SIP_S8bit **encoutbuffer, SIP_U32bit *outlen)
{
	(void) s;
	(void) encinbuffer;
	(void) clen;
	(void) encoutbuffer;
	(void) outlen;
	return SipFail;
}

SipBool fast_startTimer( SIP_U32bit duration, SIP_S8bit restart,SipBool (*timeoutfunc)(SipTimerKey *key, SIP_Pvoid buf), SIP_Pvoid buffer, SipTimerKey *key, SipError *err)
{
	(void) duration;
	(void) restart;
	(void) buffer;
	(void) *key;
	(void) timeoutfunc;
	(void) *err;
	return SipSuccess;
}

/* Implementaion of the fast_stopTimer interface reqiuired by the stack
   Application developers may choose to implement this function in any
   manner while preserving the interface
*/
SipBool fast_stopTimer(SipTimerKey *inkey, SipTimerKey **outkey, SIP_Pvoid *buffer,  SipError *err)
{
	(void) inkey;
	(void) outkey;
	(void) buffer;
	(void) err;
	return SipSuccess;
}

SipBool sip_sendToNetwork
#ifdef ANSI_PROTO
( SIP_S8bit *buffer, SIP_U32bit buflen,SipTranspAddr *addr, SIP_S8bit transptype, SipError *err)
#else
	(buffer, buflen, addr, transptype, err)
	SIP_S8bit *buffer;
	SIP_U32bit buflen;
	SipTranspAddr *addr;
	SIP_S8bit transptype;
	SipError *err;
#endif
{
	(void) *buffer;
	(void) buflen;
	(void) *addr;
	(void) transptype;
	(void) *err;
	return SipSuccess;
}

#ifdef SIP_TXN_LAYER
#ifdef ANSI_PROTO
void sip_indicateTimeOut( SipEventContext *context,en_SipTimerType dTimer)
#else
void sip_indicateTimeOut(context,dTimer)
        SipEventContext *context;
        en_SipTimerType dTimer;
#endif
#else
#ifdef ANSI_PROTO
void sip_indicateTimeOut( SipEventContext *context)
#else
void sip_indicateTimeOut(context)
        SipEventContext *context;
#endif
#endif
{
	(void)context;
#ifdef SIP_TXN_LAYER
	(void)dTimer;
#endif
}







/* Function prototype to avoid warning */
void do_simple_encode(SipMessage *pMsg);
SipBool do_full_encode(void);

void do_simple_encode(SipMessage *pMsg)
{
	SipBool r;
	SipError error;
	SIP_U32bit dLength;
	char *out;
	int i;
	SipOptions opt;
#if defined(SIP_SOLARIS) || defined(SIP_LINUX)
	clock_t pstime1, pstime;
#endif



	opt.dOption  = SIP_OPT_NOTIMER;

#if defined(SIP_SOLARIS) || defined(SIP_LINUX)

	pstime = clock();
#endif


	out = (SIP_S8bit *) fast_memget(0, SIP_MAX_MSG_SIZE, &error);
	for(i=0; i<NUM_ITERATIONS; i++)
	{
		strcpy(out, "");
		r = sip_formMessage(pMsg, &opt, out, &dLength, &error);
	}
	fast_memfree(0, out, NULL);
#if defined(SIP_SOLARIS) || defined(SIP_LINUX)
	pstime1 = clock();
#endif

	printf("\n=========== Results for simple message encoding =========\n\n");

#if defined(SIP_SOLARIS) || defined(SIP_LINUX)
	printf("CPU clock ticks for %d messages - %d\n", NUM_ITERATIONS, pstime1-pstime);
	printf("CPU time for %d messages - %f sec\n", NUM_ITERATIONS, (float)(pstime1-pstime)/CLOCKS_PER_SEC);
#endif






	printf("\n=========== Results for simple message encoding =========\n");

	if (r==SipFail)
	{
		printf ("+++++++ sip_formMessage failed. Error %d++++++++++\n",error);
	}
}

SipBool do_full_encode(void)
{
	/* Method of request being formed */
	SIP_S8bit pMethod[] = "INVITE";
	SIP_S8bit pSipVersion[] = "SIP/2.0";

	/* Values to be used in the Req-URI header */
	SIP_S8bit pReqURIUser[] = "UserB";
	SIP_S8bit pReqURIHost[] = "www-db.research.bell-labs.com";

	/* Values to be used in the From header */
	SIP_S8bit pFromHeaderDispName[] = "BigGuy";
	SIP_S8bit pFromHeaderUser[] = "UserA";
	SIP_S8bit pFromHeaderHost[] = "here.com";

	/* Values to be used in the To header */
	SIP_S8bit pToHeaderDispName[] = "LittleGuy";
	SIP_S8bit pToHeaderUser[] = "UserB";
	SIP_S8bit pToHeaderHost[] = "there.com";

	/* Value to be used in the Call-Id header */
	SIP_S8bit pCallidHeaderValue[] = "12345601@here.com";

	/* Value to be used in the CSeq header */
	SIP_U32bit dCseq = 1;

	/* Value to be used in the Via header */
	SIP_S8bit pViaHeaderTransport[] = "SIP/2.0/UDP";
	SIP_S8bit pViaHeaderHost[] = "hr450f.eng.ascend.com:5060";

	/* Values to be used in Content-type header */
	SIP_S8bit pContentTypeHeaderValue[] = "application/sdp";

	/* Values to be used in Content-length header */
	SIP_U32bit dContentLengthHeaderValue = 147;

	/* Values to be used for the SDP body */
	SIP_U16bit dMediaPort = 49172;
	SIP_S8bit pMediaFormats[] = "0";
	SIP_S8bit pMediaMvalue[] = "audio";
	SIP_S8bit pMediaProto[] = "RTP/AVP";

	SIP_S8bit pSdpAttrName[] = "rtpmap";
	SIP_S8bit pSdpAttrValue[] = "0 PCMU/8000";

	SIP_S8bit pSdpSessionString[] = "Session SDP";

	SIP_S8bit pSdpStartTime[] = "0";
	SIP_S8bit pSdpStopTime[] = "0";

	SIP_S8bit pSdpVersion[] = "0";

	SIP_S8bit pSdpOriginDispName[] = "UserA";
	SIP_S8bit pSdpOriginVersion[] = "2890844526";
	SIP_S8bit pSdpOriginSessionId[] = "2890844526";
	SIP_S8bit pSdpOriginNetType[] = "IN";
	SIP_S8bit pSdpOriginAddrType[] = "IP4";
	SIP_S8bit pSdpOriginAddr[] = "here.com";

	SIP_S8bit pSdpConnectionNetType[] = "IN";
	SIP_S8bit pSdpConnectionAddrType[] = "IP4";
	SIP_S8bit pSdpConnectionAddr[] = "100.101.102.103";


	SipMessage *pMessage;
	SipError *pErr;
	SipReqLine *pReqLine;
	SipAddrSpec *pAddrspec;
	SipUrl *pSipUrl;
	SipHeader *pHeader;
	SipMsgBody *pMsgBody;
	SdpMessage *pSdpMessage;
	SdpTime *pTime;
	SdpMedia *pSdpMedia;
	SdpOrigin *pOrigin;
	SdpConnection *pConnection;
	SdpAttr *pAttr;

	int i;
	char *out;
	SipOptions opt;
	SIP_U32bit dLength;
	SipBool r;
#if defined(SIP_SOLARIS) || defined(SIP_LINUX)
	clock_t pstime1, pstime;
#endif


	opt.dOption  = SIP_OPT_NOTIMER;
	pErr = (SipError *)fast_memget(0, sizeof(SipError), NULL);

#if defined(SIP_SOLARIS) || defined(SIP_LINUX)
	pstime = clock();
#endif
	printf("FULL ENCODE");

	out = (SIP_S8bit *) fast_memget(0, SIP_MAX_MSG_SIZE, pErr);
	for (i = 0; i < NUM_ITERATIONS; i++)
	{
		strcpy(out, "");
		/* Initialize the request message */
		if (sip_initSipMessage(&pMessage, SipMessageRequest, pErr) == SipFail)
		{
			printf("sip_initSipMessage failed\n");
			return SipFail;
		}

		/* 	----------------------------
				Set the Request Line
			---------------------------- */
		if (sip_initSipReqLine(&pReqLine, pErr) == SipFail)
		{
			printf("sip_initSipReqLine failed\n");
			return SipFail;
		}
		if (SipFail == sip_initSipAddrSpec(&pAddrspec,SipAddrSipUri,pErr))
		{
			printf("sip_initSipAddrSpec for Request line failed\n");
			sip_freeSipMessage(pMessage);
			return SipFail;
		}
		if (SipFail == sip_initSipUrl(&pSipUrl,pErr))
		{
			printf("sip_initSipUrl failed\n");
			sip_freeSipMessage(pMessage);
			return SipFail;
		}
		sip_setMethodInReqLine(pReqLine, sip_strdup(pMethod, 0),  pErr);
		sip_setVersionInReqLine(pReqLine, sip_strdup(pSipVersion, 0), pErr);
		sip_setUserInUrl(pSipUrl, sip_strdup(pReqURIUser, 0), pErr);
		sip_setHostInUrl(pSipUrl, sip_strdup(pReqURIHost, 0), pErr);
		sip_setUrlInAddrSpec(pAddrspec,pSipUrl,pErr);
		sip_setAddrSpecInReqLine(pReqLine, pAddrspec, pErr);
		sip_setReqLine (pMessage, pReqLine, pErr);

		/* Free local variables */
		sip_freeSipUrl(pSipUrl);
		sip_freeSipAddrSpec(pAddrspec);
		sip_freeSipReqLine(pReqLine);


		
		/* 	----------------------------
				Set the From Header
			---------------------------- */
		if (SipFail == sip_initSipHeader(&pHeader,SipHdrTypeFrom,pErr))
		{
			printf("sip_initSipHeader failed\n");
			sip_freeSipMessage(pMessage);
			return SipFail;
		}
		if (SipFail == sip_initSipAddrSpec(&pAddrspec,SipAddrSipUri,pErr))
		{
			printf("sip_initSipAddrSpec for From header failed\n");
			sip_freeSipMessage(pMessage);
			return SipFail;
		}
		if (SipFail == sip_initSipUrl(&pSipUrl,pErr))
		{
			printf("sip_initSipUrl for From header failed\n");
			sip_freeSipMessage(pMessage);
			return SipFail;
		}
		sip_setDispNameInToHdr(pHeader, sip_strdup(pFromHeaderDispName, 0), pErr);
		sip_setUserInUrl(pSipUrl, sip_strdup(pFromHeaderUser, 0), pErr);
		sip_setHostInUrl(pSipUrl, sip_strdup(pFromHeaderHost, 0), pErr);
		sip_setUrlInAddrSpec(pAddrspec,pSipUrl,pErr);
		sip_setAddrSpecInFromHdr(pHeader,pAddrspec,pErr);
		sip_setHeader(pMessage, pHeader, pErr);
	
		sip_freeSipUrl(pSipUrl);
		sip_freeSipAddrSpec(pAddrspec);
		sip_freeSipHeader(pHeader);
		free(pHeader);

		
		/* 	----------------------------
				Set the To Header
			---------------------------- */
		if (SipFail == sip_initSipHeader(&pHeader,SipHdrTypeTo,pErr))
		{
			printf("sip_initSipHeader for To header failed\n");
			sip_freeSipMessage(pMessage);
			return SipFail;
		}
		if (SipFail == sip_initSipAddrSpec(&pAddrspec,SipAddrSipUri,pErr))
		{
			printf("sip_initSipAddrSpec for To header failed\n");
			sip_freeSipMessage(pMessage);
			return SipFail;
		}
		if (SipFail == sip_initSipUrl(&pSipUrl,pErr))
		{
			printf("sip_initSipUrl for To header failed\n");
			sip_freeSipMessage(pMessage);
			return SipFail;
		}
		((SipToHeader *)(pHeader->pHeader))->pDispName = \
					sip_strdup(pToHeaderDispName, 0);
		
		pSipUrl->pUser = sip_strdup(pToHeaderUser, 0);
		pSipUrl->pHost = sip_strdup(pToHeaderHost, 0);
		pAddrspec->u.pSipUrl = pSipUrl;
		pAddrspec->dType = SipAddrReqUri;
		((SipToHeader *)(pHeader->pHeader))->pAddrSpec = pAddrspec;
		sip_setHeader(pMessage, pHeader, pErr);

		/* Free local variables */
		/*sip_freeSipUrl(pSipUrl);*/
		/*sip_freeSipAddrSpec(pAddrspec);*/
		sip_freeSipHeader(pHeader);
		free(pHeader);

		/* 	----------------------------
				Set the Call-id Header
			---------------------------- */
		if (SipFail == sip_initSipHeader(&pHeader,SipHdrTypeCallId,pErr))
		{
			printf("sip_initSipHeader for Call-Id header failed\n");
			sip_freeSipMessage(pMessage);
			return SipFail;
		}
		((SipCallIdHeader *)(pHeader->pHeader))->pString = sip_strdup(pCallidHeaderValue, 0);
		sip_setHeader(pMessage, pHeader, pErr);
		sip_freeSipHeader(pHeader);
		free(pHeader);

		/* 	----------------------------
				Set the Cseq Header
			---------------------------- */
		if (SipFail == sip_initSipHeader(&pHeader,SipHdrTypeCseq,pErr))
		{
			printf("sip_initSipHeader for Cseq header failed\n");
			sip_freeSipMessage(pMessage);
			return SipFail;
		}
		((SipCseqHeader *)(pHeader->pHeader))->pMethod = sip_strdup(pMethod, 0);
		((SipCseqHeader *)(pHeader->pHeader))->dSeqNum = dCseq;
		sip_freeSipHeader(pHeader);
		free(pHeader);

		/* 	----------------------------
				Set the Via Header
			---------------------------- */
		if (SipFail == sip_initSipHeader(&pHeader, \
			SipHdrTypeVia,pErr))
		{
			printf("sip_initSipHeader for Via header failed\n");
			sip_freeSipMessage(pMessage);
			return SipFail;
		}
		((SipViaHeader *)(pHeader->pHeader))->pSentProtocol = sip_strdup(pViaHeaderTransport, 0);
		((SipViaHeader *)(pHeader->pHeader))->pSentBy = sip_strdup(pViaHeaderHost, 0);
		sip_insertHeaderAtIndex(pMessage, pHeader, (SIP_U32bit)0, pErr);
		sip_freeSipHeader(pHeader);
		free(pHeader);

		/* 	---------------------------------
				Set the Content-type Header
			--------------------------------- */
		if (SipFail == sip_initSipHeader(&pHeader, \
			SipHdrTypeContentType,pErr))
		{
			printf("sip_initSipHeader for Content-type header failed\n");
			sip_freeSipMessage(pMessage);
			return SipFail;
		}
		((SipContentTypeHeader *)(pHeader->pHeader))->pMediaType =sip_strdup(pContentTypeHeaderValue, 0);
		sip_setHeader(pMessage, pHeader, pErr);
		sip_freeSipHeader(pHeader);
		free(pHeader);

		/* 	---------------------------------
				Set the Content-length Header
			--------------------------------- */
		if (SipFail == sip_initSipHeader(&pHeader, \
			SipHdrTypeContentLength,pErr))
		{
			printf("sip_initSipHeader for Content-Length header failed\n");
			sip_freeSipMessage(pMessage);
			return SipFail;
		}
		
		((SipContentLengthHeader *)(pHeader->pHeader))->dLength = dContentLengthHeaderValue;
		sip_setHeader(pMessage, pHeader, pErr);
		sip_freeSipHeader(pHeader);
		free(pHeader);


		/* 	--------------------------------------------
				All SIP headers have been set now.
				Proceed to create a new SDP Message.
			-------------------------------------------- */
		/* 	Initialize variables for message body and
			SDP Message */
		if (SipFail == sip_initSipMsgBody(&pMsgBody, \
			SipSdpBody, pErr))
		{
			printf("sip_initSipMsgBody failed\n");
			sip_freeSipMessage(pMessage);
			return SipFail;
		}
		if (SipFail == sip_initSdpMessage(&pSdpMessage, pErr))
		{
			printf("sip_initSdpMessage failed\n");
			sip_freeSipMsgBody(pMsgBody);
			sip_freeSipMessage(pMessage);
			return SipFail;
		}

		/* Set the v= line */
		sdp_setVersion(pSdpMessage, sip_strdup(pSdpVersion, 0), pErr);

		/* Set the o= line */
		if (sip_initSdpOrigin(&pOrigin, pErr) == SipFail)
		{
			printf("sip_initSdpOrigin failed\n");
			sip_freeSdpMessage(pSdpMessage);
			sip_freeSipMsgBody(pMsgBody);
			sip_freeSipMessage(pMessage);
			return SipFail;
		}
		sdp_setUserInOrigin(pOrigin, sip_strdup(pSdpOriginDispName, 0), pErr);
		sdp_setSessionIdInOrigin(pOrigin, sip_strdup(pSdpOriginSessionId, 0), pErr);
		sdp_setVersionInOrigin(pOrigin, sip_strdup(pSdpOriginVersion, 0), pErr);
		sdp_setNetTypeInOrigin(pOrigin, sip_strdup(pSdpOriginNetType, 0), pErr);
		sdp_setAddrTypeInOrigin(pOrigin, sip_strdup(pSdpOriginAddrType, 0), pErr);
		sdp_setAddrInOrigin(pOrigin, sip_strdup(pSdpOriginAddr, 0), pErr);
		sdp_setOrigin(pSdpMessage, pOrigin, pErr);
		sip_freeSdpOrigin(pOrigin);

		/* Set the c= line */
		if ((sip_initSdpConnection(&pConnection, pErr))==SipFail)
		{
			printf("sip_initSdpConnection failed\n");
			sip_freeSdpMessage(pSdpMessage);
			sip_freeSipMsgBody(pMsgBody);
			sip_freeSipMessage(pMessage);
			return SipFail;
		}
		sdp_setNetTypeInConnection(pConnection, \
			sip_strdup(pSdpConnectionNetType, 0), pErr);
		sdp_setAddrTypeInConnection(pConnection, \
			sip_strdup(pSdpConnectionAddrType, 0), pErr);
		sdp_setAddrInConnection(pConnection, \
			sip_strdup(pSdpConnectionAddr, 0), pErr);
		sdp_setConnection(pSdpMessage, pConnection, pErr);
		sip_freeSdpConnection(pConnection);

		/* set the s= line */
		sdp_setSession(pSdpMessage, sip_strdup(pSdpSessionString, 0), pErr);

		/* Setting the t= field in SDP */
		if (sip_initSdpTime(&pTime, pErr) == SipFail)
		{
			printf("sip_initSdpTime failed\n");
			sip_freeSdpMessage(pSdpMessage);
			sip_freeSipMsgBody(pMsgBody);
			sip_freeSipMessage(pMessage);
			return SipFail;
		}
		sdp_setStartInTime(pTime, sip_strdup(pSdpStartTime, 0), pErr);
		sdp_setStopInTime(pTime, sip_strdup(pSdpStopTime, 0), pErr);
		sdp_insertTimeAtIndex(pSdpMessage, pTime, (SIP_U32bit)0, pErr);
		sip_freeSdpTime(pTime);

		/* Setting the m= line */
		if (sip_initSdpMedia(&pSdpMedia, pErr) == SipFail)
		{
			printf("sip_initSdpMedia failed\n");
			sip_freeSdpMessage(pSdpMessage);
			sip_freeSipMsgBody(pMsgBody);
			sip_freeSipMessage(pMessage);
			return SipFail;
		}
		sdp_setMvalueInMedia( \
			pSdpMedia, sip_strdup(pMediaMvalue, 0), pErr);
		sdp_setProtoInMedia(pSdpMedia, sip_strdup(pMediaProto, 0), pErr);
		sdp_setPortInMedia(pSdpMedia, dMediaPort, pErr);
		sdp_setFormatInMedia(pSdpMedia, sip_strdup(pMediaFormats, 0), pErr);

		/* Setting the a= line */
		if (sip_initSdpAttr(&pAttr, pErr) == SipFail)
		{
			printf("sip_initSdpAttr failed\n");
			sip_freeSdpMessage(pSdpMessage);
			sip_freeSipMsgBody(pMsgBody);
			sip_freeSipMessage(pMessage);
			return SipFail;
		}
		sdp_setNameInAttr(pAttr, sip_strdup(pSdpAttrName, 0), pErr);
		sdp_setValueInAttr(pAttr, sip_strdup(pSdpAttrValue, 0), pErr);
		sdp_insertAttrAtIndexInMedia(pSdpMedia, pAttr, 0, pErr);

		sdp_insertMediaAtIndex(pSdpMessage, pSdpMedia, (SIP_U32bit)0, pErr);

		/* Freeing the local reference */
		sip_freeSdpAttr(pAttr);
		sip_freeSdpMedia(pSdpMedia);

		/* Set the filled SdpMessage into the message body now */
		sip_setSdpInMsgBody(pMsgBody, pSdpMessage, pErr);
		sip_insertMsgBodyAtIndex(pMessage, pMsgBody, (SIP_U32bit)0, pErr);

		/* Free local variables */
		sip_freeSdpMessage(pSdpMessage);
		sip_freeSipMsgBody(pMsgBody);

		/* The message has been formed into the SipMessage structure now.
			Call sip_formMessage to encode it into the buffer */
		r = sip_formMessage(pMessage, &opt, out, &dLength, pErr);

		/* Free the SipMessage structure */
		sip_freeSipMessage(pMessage);
	}
	fast_memfree(0, out, NULL);

#if defined(SIP_SOLARIS) || defined(SIP_LINUX)
	pstime1 = clock();
#endif

	printf("\n=========== Results for message formation + encoding =========\n\n");

#if defined(SIP_SOLARIS) || defined(SIP_LINUX)
	printf("CPU clock ticks for %d messages - %d\n", NUM_ITERATIONS, pstime1-pstime);
	printf("CPU time for %d messages - %f sec\n", NUM_ITERATIONS, (float)(pstime1-pstime)/CLOCKS_PER_SEC);
#endif


	printf("\n=========== Results for message formation + encoding =========\n\n");

	if (r==SipFail)
	{
		printf ("+++++++ sip_formMessage failed. Error %d++++++++++\n", pErr);
		return SipFail;
	}
	else
		{
		
			fast_memfree(0, pErr, SIP_NULL);
			return SipSuccess;
		}

}

#ifdef SIP_RETRANSCALLBACK
#ifdef  SIP_TXN_LAYER
/******************************************************************************
 ** FUNCTION: 		sip_indicateMessageRetransmission
 **
 ** DESCRIPTION: 	This is a callback function that is invoked by the stack
 **					when the SIP_RETRANSCALLBACK option is enabled.
 **
 ******************************************************************************/
void sip_indicateMessageRetransmission (SipEventContext *pContext,\
	SipTxnKey *pKey, SIP_S8bit *pBuffer,\
	SIP_U32bit dBufferLength, SipTranspAddr *pAddr, SIP_U8bit dRetransCount,\
	SIP_U32bit dDuration)
{
	(void) pContext;
	(void) pKey;
	(void) dBufferLength;
	(void) pAddr;
	(void ) dRetransCount;
	(void ) pBuffer;
	(void ) dDuration;
	SIPDEBUG("Inside sip_indicateMessageRetransmission.\n");
}
#else
/*******************************************************************************
 ** FUNCTION: 		sip_indicateMessageRetransmission
 **
 ** DESCRIPTION: 	This is a callback function that is invoked by the stack
 **					when the SIP_RETRANSCALLBACK option is enabled.
 **
 ******************************************************************************/
void sip_indicateMessageRetransmission (SipEventContext *pContext,\
	SipTimerKey *pKey, SIP_S8bit *pBuffer,\
	SIP_U32bit dBufferLength, SipTranspAddr *pAddr, SIP_U8bit dRetransCount,\
	SIP_U32bit dDuration)
{
	(void) pContext;
	(void) pKey;
	(void) dBufferLength;
	(void) pAddr;
	(void ) dRetransCount;
	(void ) pBuffer;
	(void ) dDuration;
	SIPDEBUG("Inside sip_indicateMessageRetransmission.\n");
}
#endif
#endif

#ifndef SIP_NO_CALLBACK
void sip_indicateInvite(SipMessage *s, SipEventContext *context)
{
	(void)context;
	do_simple_encode(s);
	sip_freeSipMessage(s);
	if (do_full_encode() == SipFail)
		printf("do_full_encode failed\n");
}

void sip_indicateUpdate(SipMessage *s, SipEventContext *context)
{
	(void)context;
	do_simple_encode(s);
	sip_freeSipMessage(s);
	if (do_full_encode() == SipFail)
		printf("do_full_encode failed\n");
}

void sip_indicateRegister(SipMessage *s, SipEventContext *context)
{
	(void)context;
	do_simple_encode(s);
	sip_freeSipMessage(s);
	if (do_full_encode() == SipFail)
		printf("do_full_encode failed\n");
}

void sip_indicateCancel(SipMessage *s, SipEventContext *context)
{
	(void)context;
	do_simple_encode(s);
	sip_freeSipMessage(s);
	if (do_full_encode() == SipFail)
		printf("do_full_encode failed\n");
}

void sip_indicateOptions(SipMessage *s, SipEventContext *context)
{
	(void)context;
	do_simple_encode(s);
	sip_freeSipMessage(s);
	if (do_full_encode() == SipFail)
		printf("do_full_encode failed\n");
}

void sip_indicateBye(SipMessage *s, SipEventContext *context)
{
	(void)context;
	do_simple_encode(s);
	sip_freeSipMessage(s);
	if (do_full_encode() == SipFail)
		printf("do_full_encode failed\n");
}


void sip_indicateAck(SipMessage *s, SipEventContext *context)
{
	(void)context;
	do_simple_encode(s);
	sip_freeSipMessage(s);
	if (do_full_encode() == SipFail)
		printf("do_full_encode failed\n");
}

void sip_indicateInfo(SipMessage *s, SipEventContext *context)
{
	(void)context;
	do_simple_encode(s);
	sip_freeSipMessage(s);
	if (do_full_encode() == SipFail)
		printf("do_full_encode failed\n");
}

void sip_indicatePropose(SipMessage *s, SipEventContext *context)
{
	(void)context;
	do_simple_encode(s);
	sip_freeSipMessage(s);
	if (do_full_encode() == SipFail)
		printf("do_full_encode failed\n");
}

void sip_indicatePrack(SipMessage *s, SipEventContext *context)
{
	(void)context;
	do_simple_encode(s);
	sip_freeSipMessage(s);
	if (do_full_encode() == SipFail)
		printf("do_full_encode failed\n");
}

void sip_indicateUnknownRequest(SipMessage *s, SipEventContext *context)
{
	(void)context;
	do_simple_encode(s);
	sip_freeSipMessage(s);
	if (do_full_encode() == SipFail)
		printf("do_full_encode failed\n");
}

void sip_indicateInformational(SipMessage *s, SipEventContext *context)
{
	(void)context;
	do_simple_encode(s);
	sip_freeSipMessage(s);
	if (do_full_encode() == SipFail)
		printf("do_full_encode failed\n");
}

void sip_indicateFinalResponse(SipMessage *s, SipEventContext *context)
{
	(void)context;
	do_simple_encode(s);
	sip_freeSipMessage(s);
	if (do_full_encode() == SipFail)
		printf("do_full_encode failed\n");
}

void sip_indicateRefer(SipMessage *s, SipEventContext *context)
{
	(void)context;
	do_simple_encode(s);
	sip_freeSipMessage(s);
	if (do_full_encode() == SipFail)
		printf("do_full_encode failed\n");
}
#ifdef SIP_IMPP
void sip_indicateSubscribe(SipMessage *s, SipEventContext *context)
{
	(void)context;
	do_simple_encode(s);
	sip_freeSipMessage(s);
	if (do_full_encode() == SipFail)
		printf("do_full_encode failed\n");
}

void sip_indicateNotify(SipMessage *s, SipEventContext *context)
{
	(void)context;
	do_simple_encode(s);
	sip_freeSipMessage(s);
	if (do_full_encode() == SipFail)
		printf("do_full_encode failed\n");
}
#endif
void sip_indicateMessage(SipMessage *s, SipEventContext *context)
{
	(void)context;
	do_simple_encode(s);
	sip_freeSipMessage(s);
	if (do_full_encode() == SipFail)
		printf("do_full_encode failed\n");
}
void sip_indicateComet(SipMessage *s, SipEventContext *context)
{
	(void)context;
	do_simple_encode(s);
	sip_freeSipMessage(s);
	if (do_full_encode() == SipFail)
		printf("do_full_encode failed\n");
}


#endif

/* End of callback implementations */


#if defined(SIP_SOLARIS) || defined(SIP_LINUX)
int main(int argc, char * argv[])
#endif
{
	int sockfd;
	int	clilen,n;
	struct sockaddr_in	serv_addr, cli_addr;
	fd_set readfs;
	char *message;
	SIP_S8bit *nextmesg;
	SipError error;
	SipEventContext *pContext;

#ifdef SIP_INCREMENTAL_PARSING	
	SIP_U32bit j;
#endif	
	if(argc<2)
	{
		printf("Usage:\n");
		printf("%s my_port\n",argv[0]);
		exit(0);
	}
	pContext = (SipEventContext*)malloc(sizeof (SipEventContext));
#ifdef SIP_INCREMENTAL_PARSING
	pContext->pList=SIP_NULL;
	if(pContext->pList == SIP_NULL)
		pContext->pList=(SipHdrTypeList *)\
	fast_memget(0,sizeof(SipHdrTypeList), SIP_NULL);
	for(j=0;j<HEADERTYPENUM;j++)
		pContext->pList->enable[j]=SipSuccess;
#endif

	if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
	{
		printf("Server could not open dgram socket\n");
		exit(0);
	}

	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family      = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port        = htons(atoi(argv[1]));


	if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
	{
		printf("Server couldn't bind to local address.\n");
		exit(0);
	}

	/* Stack and trace initialization */
	sip_initStack();

	printf("\nMicro Sip stack product id is:%s\n", (char *)(sip_getPart()) );

	if (sip_setErrorLevel(SIP_Major|SIP_Minor|SIP_Critical, &error)==SipFail)
	{
		printf ("########## Error Disabled at compile time #####\n");
	}


	if (sip_setTraceLevel(SIP_Brief,&error)== SipFail)
	{
		printf ("########## Trace Disabled at compile time #####\n");
	}
	sip_setTraceType(SIP_All,&error);



	for(;;)
	{
#if defined(SIP_SOLARIS) || defined(SIP_LINUX)
	struct timeval tv1, tv2;
#endif



#ifdef SIP_NO_CALLBACK
		SipMessage *pMsg;
#endif

		/* get minimum timeout from the timer list */

		FD_ZERO(&readfs);
		FD_SET(sockfd,&readfs);
		FD_SET(0,&readfs);

#if defined(SIP_SOLARIS) || defined(SIP_LINUX)
		gettimeofday(&tv1, NULL);
#endif

		if (select(sockfd+1, &readfs, NULL, NULL, NULL) < 0)
		{
				printf("select returned < 0\n");
				fflush(stdout);
				exit(0);
		}

#if defined(SIP_SOLARIS) || defined(SIP_LINUX)
		gettimeofday(&tv2, NULL);
#endif

		if(FD_ISSET(sockfd,&readfs))
		{
			/* Message on the receive port */
			message = (char *) malloc(sizeof(char)*MAXMESG);

			clilen = sizeof(cli_addr);
			n = recvfrom(sockfd, message, MAXMESG, 0,(struct sockaddr *) &cli_addr, &clilen);

			if (n < 0)
			{
				printf("Server : Error in receive.\n");
				close(sockfd);
				exit(0);
			}
			message[n]='\0';
			do
			{
				SipOptions opt;
				/*begin SPR 1*/
				opt.dOption  = SIP_OPT_NOTIMER;
				/*end SPR 1*/

#ifndef SIP_NO_CALLBACK
				if (sip_decodeMessage((SIP_S8bit *)message,&opt, \
					strlen(message),&nextmesg,pContext, &error) == SipFail)
				{
					printf ("+++++++ BAD MESSAGE %d++++++++++\n",error);
				}
#else
				if (sip_decodeMessage((SIP_S8bit *)message,&pMsg,&opt, \
					strlen(message),&nextmesg,pContext, &error) == SipFail)
				{
					printf ("+++++++ BAD MESSAGE %d++++++++++\n",error);
				}
				else
				{
					do_simple_encode(pMsg);
					sip_freeSipMessage(pMsg);
					if (do_full_encode() == SipFail)
						printf("do_full_encode failed\n");
				}
#endif
				fast_memfree(0,message, SIP_NULL);
				message = nextmesg;

			} while (message != SIP_NULL);
		}
		else if (FD_ISSET(0,&readfs))
		{
			/* Key pressed */
			char c;

			c = getchar();

			if (c == 'q')
			{
				exit(0);
			}
		}
	}
}


#ifdef __cplusplus
}
#endif


/****************************************************/

/*********************************************************************
** FUNCTION:  sdp_setPortInMedia
**
**********************************************************************
**
** DESCRIPTION: Sets the dPort field pValue in sdp slMedia structure.
**
*********************************************************************/
SipBool sdp_setPortInMedia
#ifdef ANSI_PROTO
	( SdpMedia	*slMedia,
	  SIP_U16bit 	dPort,
	  SipError 	*err )
#else
	( slMedia, dPort, err)
	  SdpMedia	*slMedia;
	  SIP_U16bit 	dPort;
	  SipError 	*err;
#endif
{
	SIPDEBUGFN ( "Entering setPortInMedia");
#ifndef SIP_NO_CHECK
	if( err == SIP_NULL )
		return SipFail;

	if ( slMedia == SIP_NULL )
	{
		*err = E_INV_PARAM;
		return SipFail;
	}
#endif

	slMedia->dPort = dPort;

	SIPDEBUGFN ( "Exitting setPortInMedia");

	*err = E_NO_ERROR;
	return SipSuccess;

}

/*******************************************************************
** FUNCTION: sdp_setSessionIdInOrigin
**
** DESCRIPTION: This function sets the pSession-id field in an SDP
**		Origin
**
*******************************************************************/
SipBool sdp_setSessionIdInOrigin
#ifdef ANSI_PROTO
	( SdpOrigin * pOrigin,	/*Origin pHeader */
	  SIP_S8bit * id,
	  SipError * err)
#else
	( pOrigin, id, err )	/*Origin pHeader */
	  SdpOrigin * pOrigin;
	  SIP_S8bit * id;
	  SipError * err;
#endif

{
#ifndef SIP_BY_REFERENCE
	SIP_U32bit dLength;
#endif
	SIP_S8bit * temp_id;
	SdpOrigin *temp_origin;
	SIPDEBUGFN("Entering setSessionIdInOrigin\n");
#ifndef SIP_NO_CHECK
	if( err == SIP_NULL )
		return SipFail;

	if ( pOrigin == SIP_NULL)
	{
		*err = E_INV_PARAM;
		return SipFail;
	}
#endif

	if( id == SIP_NULL)
		temp_id = SIP_NULL;
	else
	{
#ifndef SIP_BY_REFERENCE
		dLength = strlen( id );
		temp_id = ( SIP_S8bit * )fast_memget(ACCESSOR_MEM_ID,dLength+1,err);
		if ( temp_id == SIP_NULL )
			return SipFail;
		strcpy( temp_id, id );
#else
		temp_id = id;
#endif
	}

	temp_origin = ( SdpOrigin *) ( pOrigin);
	if ( temp_origin->pSessionid != SIP_NULL )
	{
		if ( fast_memfree(ACCESSOR_MEM_ID, temp_origin->pSessionid, err) == SipFail)
			return SipFail;
	}


	temp_origin->pSessionid = temp_id;



	SIPDEBUGFN("Exitiing setSessionIdInOrigin\n");
	*err = E_NO_ERROR;
	return SipSuccess;
}



/*******************************************************************
** FUNCTION: sdp_setAddrTypeInOrigin
**
** DESCRIPTION: This function sets the pTranspAddr-dType field in an SDP
**		Origin
**
*******************************************************************/
SipBool sdp_setAddrTypeInOrigin
#ifdef ANSI_PROTO
	( SdpOrigin * pOrigin,
	  SIP_S8bit * pAddrType,
	  SipError * err)
#else
	( pOrigin, pAddrType, err )
	  SdpOrigin * pOrigin;
	  SIP_S8bit * pAddrType;
	  SipError * err;
#endif

{
#ifndef SIP_BY_REFERENCE
	SIP_U32bit dLength;
#endif
	SIP_S8bit * temp_addr_type;
	SdpOrigin * temp_origin;
	SIPDEBUGFN("Entering setAddrTypeInOrigin\n");
#ifndef SIP_NO_CHECK
	if( err == SIP_NULL )
		return SipFail;

	if ( pOrigin == SIP_NULL)
	{
		*err = E_INV_PARAM;
		return SipFail;
	}
#endif

	if( pAddrType == SIP_NULL)
		temp_addr_type = SIP_NULL;
	else
	{
#ifndef SIP_BY_REFERENCE
		dLength = strlen( pAddrType );
		temp_addr_type = ( SIP_S8bit * )fast_memget(ACCESSOR_MEM_ID,dLength+1,err);
		if ( temp_addr_type == SIP_NULL )
			return SipFail;
		strcpy( temp_addr_type, pAddrType );
#else
		temp_addr_type = pAddrType;
#endif
	}

	temp_origin = ( SdpOrigin *) ( pOrigin);
	if ( temp_origin->pAddrType != SIP_NULL )
	{
		if ( fast_memfree(ACCESSOR_MEM_ID, temp_origin->pAddrType, err) == SipFail)
			return SipFail;
	}


	temp_origin->pAddrType = temp_addr_type;



	 SIPDEBUGFN("Exitiing setAddrTypeInOrigin\n");
	*err = E_NO_ERROR;
	return SipSuccess;
}

/*********************************************************************
** FUNCTION:  sdp_setNameInAttr
**
**********************************************************************
**
** DESCRIPTION: Sets the pName field pValue in attribute strucutre.
**
*********************************************************************/
SipBool sdp_setNameInAttr
#ifdef ANSI_PROTO
	( SdpAttr	*slAttr,
	  SIP_S8bit 	*pName,
	  SipError 	*err )
#else
	( slAttr, pName, err)
	  SdpAttr	*slAttr;
	  SIP_S8bit 	*pName;
	  SipError 	*err;
#endif
{
#ifndef SIP_BY_REFERENCE
	SIP_U32bit dLength;
#endif
	SIP_S8bit * temp_name;

	SIPDEBUGFN ( "Entering setNameInAttr");
#ifndef SIP_NO_CHECK
	if( err == SIP_NULL )
		return SipFail;

	if ( slAttr == SIP_NULL)
	{
		*err = E_INV_PARAM;
		return SipFail;
	}
#endif
	if( pName == SIP_NULL)
		temp_name = SIP_NULL;
	else
	{
#ifndef SIP_BY_REFERENCE
		dLength = strlen( pName );
		temp_name = ( SIP_S8bit * )fast_memget(ACCESSOR_MEM_ID,dLength+1,err);
		if ( temp_name == SIP_NULL )
			return SipFail;

		strcpy( temp_name, pName );
#else
		temp_name = pName;
#endif
	}

	if ( slAttr->pName != SIP_NULL )
	{
		if ( sip_memfree(ACCESSOR_MEM_ID, (SIP_Pvoid*)(&(slAttr->pName)), err) == SipFail)
			return SipFail;
	}


	slAttr->pName = temp_name;

	SIPDEBUGFN ( "Exitting setNameInAttr");

	*err = E_NO_ERROR;
	return SipSuccess;

}

/*********************************************************************
** FUNCTION:  sdp_setProtoInMedia
**
**********************************************************************
**
** DESCRIPTION: Sets the pProtocol field pValue in sdp slMedia
** structure.
**
*********************************************************************/
SipBool sdp_setProtoInMedia
#ifdef ANSI_PROTO
	( SdpMedia	*slMedia,
	  SIP_S8bit 	*pProtocol,
	  SipError 	*err )
#else
	( slMedia, pProtocol, err)
	  SdpMedia	*slMedia;
	  SIP_S8bit 	*pProtocol;
	  SipError 	*err;
#endif
{
#ifndef SIP_BY_REFERENCE
	SIP_U32bit dLength;
#endif
	SIP_S8bit * temp_proto;

	SIPDEBUGFN ( "Entering setProtoInMedia");
#ifndef SIP_NO_CHECK
	if( err == SIP_NULL )
		return SipFail;

	if ( slMedia == SIP_NULL)
	{
		*err = E_INV_PARAM;
		return SipFail;
	}
#endif
	if( pProtocol == SIP_NULL)
		temp_proto = SIP_NULL;
	else
	{
#ifndef SIP_BY_REFERENCE
		dLength = strlen( pProtocol );
		temp_proto = ( SIP_S8bit * )fast_memget(ACCESSOR_MEM_ID,dLength+1,err);
		if ( temp_proto == SIP_NULL )
			return SipFail;

		strcpy( temp_proto, pProtocol );
#else
		temp_proto = pProtocol;
#endif
	}

	if ( slMedia->pProtocol != SIP_NULL )
	{
		if ( sip_memfree(ACCESSOR_MEM_ID, (SIP_Pvoid *)(&(slMedia->pProtocol)), err) == SipFail)
			return SipFail;
	}


	slMedia->pProtocol = temp_proto;

	SIPDEBUGFN ( "Exitting setProtoInMedia");

	*err = E_NO_ERROR;
	return SipSuccess;

}

/*********************************************************************
** FUNCTION:  sdp_setFormatInMedia
**
**********************************************************************
**
** DESCRIPTION: Sets the pFormat field pValue in sdp slMedia structure.
**
*********************************************************************/
SipBool sdp_setFormatInMedia
#ifdef ANSI_PROTO
	( SdpMedia	*slMedia,
	  SIP_S8bit 	*pFormat,
	  SipError 	*err )
#else
	( slMedia, pFormat, err)
	  SdpMedia	*slMedia;
	  SIP_S8bit 	*pFormat;
	  SipError 	*err;
#endif
{
#ifndef SIP_BY_REFERENCE
	SIP_U32bit dLength;
#endif
	SIP_S8bit * temp_format;

	SIPDEBUGFN ( "Entering setFormatInMedia");
#ifndef SIP_NO_CHECK
	if( err == SIP_NULL )
		return SipFail;

	if ( slMedia == SIP_NULL)
	{
		*err = E_INV_PARAM;
		return SipFail;
	}
#endif
	if( pFormat == SIP_NULL)
		temp_format = SIP_NULL;
	else
	{
#ifndef SIP_BY_REFERENCE
		dLength = strlen( pFormat );
		temp_format = ( SIP_S8bit * )fast_memget(ACCESSOR_MEM_ID,dLength+1,err);
		if ( temp_format == SIP_NULL )
			return SipFail;

		strcpy( temp_format, pFormat );
#else
		temp_format = pFormat;
#endif
	}

	if ( slMedia->pFormat != SIP_NULL )
	{
		if ( sip_memfree(ACCESSOR_MEM_ID, (SIP_Pvoid *)(&(slMedia->pFormat)), err) == SipFail)
			return SipFail;
	}


	slMedia->pFormat = temp_format;

	SIPDEBUGFN ( "Exitting setFormatInMedia");

	*err = E_NO_ERROR;
	return SipSuccess;

}
/*******************************************************************
** FUNCTION: sdp_setOrigin
**
** DESCRIPTION: This function sets the pOrigin filed in an SDP message
**		structure
**
*******************************************************************/
SipBool sdp_setOrigin
#ifdef ANSI_PROTO
	( SdpMessage *msg, SdpOrigin *pOrigin, SipError *err)
#else
	( msg, pOrigin, err )
	  SdpMessage *msg;
	  SdpOrigin *pOrigin;
	  SipError *err;
#endif
{
	SIPDEBUGFN("Entering setOrigin\n");
#ifndef SIP_NO_CHECK
	if( err == SIP_NULL )
		return SipFail;

	if ( msg == SIP_NULL)
	{
		*err = E_INV_PARAM;
		return SipFail;
	}
#endif
	if(msg->pOrigin!=SIP_NULL)
	{
		sip_freeSdpOrigin(msg->pOrigin);
	}
	if(pOrigin == SIP_NULL)
	{
		msg->pOrigin = SIP_NULL;
	}
	else
	{
#ifdef SIP_BY_REFERENCE
		HSS_LOCKEDINCREF(pOrigin->dRefCount);
		msg->pOrigin = pOrigin;
#else
		if(sip_initSdpOrigin(&(msg->pOrigin),err)==SipFail)
				return SipFail;
		if(__sip_cloneSdpOrigin(msg->pOrigin,pOrigin,err)==SipFail)
		{
			return SipFail;
		}
#endif
	}

	SIPDEBUGFN("Exitting setOrigin\n");

	*err = E_NO_ERROR;
	return SipSuccess;

}

/*********************************************************************
** FUNCTION:  sdp_insertAttrAtIndexInMedia
**
**********************************************************************
**
** DESCRIPTION: Inserts the attribute structure at a specified index
**		( starting from 0 ) in the slMedia strucutre.
**
*********************************************************************/
SipBool sdp_insertAttrAtIndexInMedia
#ifdef ANSI_PROTO
	( SdpMedia 	*slMedia,
	  SdpAttr	*slAttr,
	  SIP_U32bit 	index,
	  SipError 	*err )
#else
	( slMedia,slAttr,index,err)
	  SdpMedia 	*slMedia;
	  SdpAttr	*slAttr;
	  SIP_U32bit 	index;
	  SipError 	*err;
#endif
{
	SdpAttr 	*element_in_list;

	SIPDEBUGFN ( "Entering insertAttrAtIndexInMedia");
#ifndef SIP_NO_CHECK
	if( err == SIP_NULL )
		return SipFail;

	if ( slMedia == SIP_NULL )
	{
		*err = E_INV_PARAM;
		return SipFail;
	}
#endif
	/* copying the slAttr structure */
	if ( slAttr == SIP_NULL )
		element_in_list = SIP_NULL;
	else
	{
#ifndef SIP_BY_REFERENCE
		if ( sip_initSdpAttr(&element_in_list,err) == SipFail)
			return SipFail;
		if ( __sip_cloneSdpAttr(element_in_list, slAttr,err) == SipFail)
		{
			sip_freeSdpAttr(element_in_list);
			return SipFail;
		}
#else
		HSS_LOCKEDINCREF(slAttr->dRefCount);
		element_in_list = slAttr;
#endif
	}

	if( sip_listInsertAt(&(slMedia->slAttr), index, element_in_list, err) == SipFail)
	{
#ifndef SIP_BY_REFERENCE
		if ( element_in_list != SIP_NULL )
			sip_freeSdpAttr(element_in_list);
#else
		HSS_LOCKEDDECREF(slAttr->dRefCount);
#endif
		return SipFail;
	}

	SIPDEBUGFN ( "Exitting insertAttrAtIndexInMedia");
	*err = E_NO_ERROR;
	return SipSuccess;
}

/********************************************************************
** FUNCTION:sdp_setSdpInMsgBody
**
** DESCRIPTION: This function sets the SDP part in a SIP message
**
**********************************************************************/

SipBool sip_setSdpInMsgBody
#ifdef ANSI_PROTO
	( SipMsgBody *msg, SdpMessage *sdp, SipError *err)
#else
	( msg, sdp, err )
	  SipMsgBody *msg;
	  SdpMessage *sdp;
	  SipError *err;
#endif
{
	SdpMessage	*temp_sdp;

	SIPDEBUGFN("Entering sip_setSdpInBody\n");
#ifndef SIP_NO_CHECK
	if( err == SIP_NULL )
	{
		return SipFail;
	}

	if (msg == SIP_NULL)
	{
		*err = E_INV_PARAM;
		return SipFail;
	}

	if( msg->dType!=SipSdpBody )
	{
		*err = E_INV_TYPE;
		return SipFail;
	}
#endif
	if ( msg->u.pSdpMessage != SIP_NULL)
		sip_freeSdpMessage( msg->u.pSdpMessage);

	if (sdp==SIP_NULL)
	{
		temp_sdp=SIP_NULL;	
	}
	else
	{	
#ifndef SIP_BY_REFERENCE
		if ( sip_initSdpMessage(&temp_sdp, err) == SipFail)
			return SipFail;

		if ( __sip_cloneSdpMessage(temp_sdp, sdp, err) == SipFail)
		{
			sip_freeSdpMessage( temp_sdp );
			return SipFail;
		}
#else
		HSS_LOCKEDINCREF(sdp->dRefCount);
		temp_sdp = sdp;
#endif
	}	

	msg->u.pSdpMessage = temp_sdp;

	SIPDEBUGFN("Leaving sip_setSdpBody\n");
	*err = E_NO_ERROR;
	return SipSuccess;
}
/********************************************************************
** FUNCTION:sdp_setVersion
**
** DESCRIPTION: This function sets the pVersion field in an SDP message
**		structure
**
**********************************************************************/

SipBool sdp_setVersion
#ifdef ANSI_PROTO
	( SdpMessage *msg, SIP_S8bit *pVersion, SipError *err)
#else
	( msg, pVersion, err )
	  SdpMessage *msg;
	  SIP_S8bit *pVersion;
	  SipError *err;
#endif
{
#ifndef SIP_BY_REFERENCE
	SIP_U32bit dLength;
#endif
	SIP_S8bit * temp_version;
	SdpMessage *temp_mssg;
	SIPDEBUGFN("Entering setVersion\n");
#ifndef SIP_NO_CHECK
	if( err == SIP_NULL )
		return SipFail;

	if ( msg == SIP_NULL)
	{
		*err = E_INV_PARAM;
		return SipFail;
	}
#endif
	if( pVersion == SIP_NULL)
		temp_version = SIP_NULL;
	else
	{
#ifndef SIP_BY_REFERENCE
		dLength = strlen( pVersion );
		temp_version = ( SIP_S8bit * )fast_memget(ACCESSOR_MEM_ID,dLength+1,err);
		if ( temp_version== SIP_NULL )
			return SipFail;

		strcpy( temp_version, pVersion );
#else
		temp_version = pVersion;
#endif

	}

	temp_mssg = ( SdpMessage *)msg ;

	if ( temp_mssg->pVersion != SIP_NULL )
	{
		if ( fast_memfree(ACCESSOR_MEM_ID, temp_mssg->pVersion, err) == SipFail)
			return SipFail;
	}
        temp_mssg->pVersion=temp_version;
	SIPDEBUGFN("Exitting setVersion\n");

	*err = E_NO_ERROR;
	return SipSuccess;

}
/*********************************************************************
** FUNCTION:  sdp_setStopInTime
**
**********************************************************************
**
** DESCRIPTION: Sets the pStop field pValue in slTime structure.
**
*********************************************************************/
SipBool sdp_setStopInTime
#ifdef ANSI_PROTO
	( SdpTime	*slTime,
	  SIP_S8bit 	*pStop,
	  SipError 	*err )
#else
	( slTime, pStop, err)
	  SdpTime	*slTime;
	  SIP_S8bit 	*pStop;
	  SipError 	*err;
#endif
{
#ifndef SIP_BY_REFERENCE
	SIP_U32bit dLength;
#endif
	SIP_S8bit * temp_stop;

	SIPDEBUGFN ( "Entering setStopInTime");
#ifndef SIP_NO_CHECK
	if( err == SIP_NULL )
		return SipFail;

	if ( slTime == SIP_NULL)
	{
		*err = E_INV_PARAM;
		return SipFail;
	}
#endif
	if( pStop == SIP_NULL)
		temp_stop = SIP_NULL;
	else
	{
#ifndef SIP_BY_REFERENCE
		dLength = strlen( pStop );
		temp_stop = ( SIP_S8bit * )fast_memget(ACCESSOR_MEM_ID,dLength+1,err);
		if ( temp_stop == SIP_NULL )
			return SipFail;

		strcpy( temp_stop, pStop );
#else
		temp_stop = pStop;
#endif
	}

	if ( slTime->pStop != SIP_NULL )
	{
		if ( sip_memfree(ACCESSOR_MEM_ID, (SIP_Pvoid *)(&(slTime->pStop)), err) == SipFail)
			return SipFail;
	}


	slTime->pStop = temp_stop;

	SIPDEBUGFN ( "Exitting setStopInTime");

	*err = E_NO_ERROR;
	return SipSuccess;

}
/*********************************************************************
** FUNCTION:  sdp_setValueInAttr
**
**********************************************************************
**
** DESCRIPTION: Sets the pValue field in sdp attribute structure.
**
*********************************************************************/
SipBool sdp_setValueInAttr
#ifdef ANSI_PROTO
	( SdpAttr	*slAttr,
	  SIP_S8bit 	*pValue,
	  SipError 	*err )
#else
	( slAttr, pValue, err)
	  SdpAttr	*slAttr;
	  SIP_S8bit 	*pValue;
	  SipError 	*err;
#endif
{
#ifndef SIP_BY_REFERENCE
	SIP_U32bit dLength;
#endif
	SIP_S8bit * temp_value;

	SIPDEBUGFN ( "Entering setValueInAttr");
#ifndef SIP_NO_CHECK
	if( err == SIP_NULL )
		return SipFail;

	if ( slAttr == SIP_NULL)
	{
		*err = E_INV_PARAM;
		return SipFail;
	}
#endif
	if( pValue == SIP_NULL)
		temp_value = SIP_NULL;
	else
	{
#ifndef SIP_BY_REFERENCE
		dLength = strlen( pValue );
		temp_value = ( SIP_S8bit * )fast_memget(ACCESSOR_MEM_ID,dLength+1,err);
		if ( temp_value == SIP_NULL )
			return SipFail;

		strcpy( temp_value, pValue );
#else
		temp_value = pValue;
#endif
	}

	if ( slAttr->pValue != SIP_NULL )
	{
		if ( sip_memfree(ACCESSOR_MEM_ID, (SIP_Pvoid *)(&(slAttr->pValue)), err) == SipFail)
			return SipFail;
	}


	slAttr->pValue = temp_value;

	SIPDEBUGFN ( "Exitting setValueInAttr");

	*err = E_NO_ERROR;
	return SipSuccess;

}

/*********************************************************************
** FUNCTION:  sdp_setNetTypeInConnection
**
**********************************************************************
**
** DESCRIPTION: Sets the net dType field pValue in slConnection structure.
**
*********************************************************************/
SipBool sdp_setNetTypeInConnection
#ifdef ANSI_PROTO
	( SdpConnection	*slConnection,
	  SIP_S8bit 	*ntype,
	  SipError 	*err )
#else
	( slConnection, ntype, err)
	  SdpConnection	*slConnection;
	  SIP_S8bit 	*ntype;
	  SipError 	*err;
#endif
{
#ifndef SIP_BY_REFERENCE
	SIP_U32bit dLength;
#endif
	SIP_S8bit * temp_net_type;

	SIPDEBUGFN ( "Entering setNetTypeInConnection");
#ifndef SIP_NO_CHECK
	if( err == SIP_NULL )
		return SipFail;

	if ( slConnection == SIP_NULL)
	{
		*err = E_INV_PARAM;
		return SipFail;
	}
#endif
	if( ntype == SIP_NULL)
		temp_net_type = SIP_NULL;
	else
	{
#ifndef SIP_BY_REFERENCE
		dLength = strlen( ntype );
		temp_net_type = ( SIP_S8bit * )fast_memget(ACCESSOR_MEM_ID,dLength+1,err);
		if ( temp_net_type == SIP_NULL )
			return SipFail;

		strcpy( temp_net_type, ntype );
#else
		temp_net_type = ntype;
#endif

	}

	if ( slConnection->pNetType != SIP_NULL )
	{
		if ( sip_memfree(ACCESSOR_MEM_ID, (SIP_Pvoid *)(&(slConnection->pNetType)), err) == SipFail)
			return SipFail;
	}


	slConnection->pNetType = temp_net_type;

	SIPDEBUGFN ( "Exitting setNetTypeInConnection");

	*err = E_NO_ERROR;
	return SipSuccess;

}
/*********************************************************************
** FUNCTION:  sdp_setStartInTime
**
**********************************************************************
**
** DESCRIPTION: Sets the pStart slTime field pValue in slTime structure.
**
*********************************************************************/
SipBool sdp_setStartInTime
#ifdef ANSI_PROTO
	( SdpTime	*slTime,
	  SIP_S8bit 	*pStart,
	  SipError 	*err )
#else
	( slTime, pStart, err)
	  SdpTime	*slTime;
	  SIP_S8bit 	*pStart;
	  SipError 	*err;
#endif
{
#ifndef SIP_BY_REFERENCE
	SIP_U32bit dLength;
#endif
	SIP_S8bit * temp_start;

	SIPDEBUGFN ( "Entering setStartInTime");
#ifndef SIP_NO_CHECK
	if( err == SIP_NULL )
		return SipFail;

	if ( slTime == SIP_NULL)
	{
		*err = E_INV_PARAM;
		return SipFail;
	}
#endif
	if( pStart == SIP_NULL)
		temp_start = SIP_NULL;
	else
	{
#ifndef SIP_BY_REFERENCE
		dLength = strlen( pStart );
		temp_start = ( SIP_S8bit * )fast_memget(ACCESSOR_MEM_ID,dLength+1,err);
		if ( temp_start == SIP_NULL )
			return SipFail;

		strcpy( temp_start, pStart );
#else
		temp_start = pStart;
#endif
	}

	if ( slTime->pStart != SIP_NULL )
	{
		if ( sip_memfree(ACCESSOR_MEM_ID, (SIP_Pvoid *)(&(slTime->pStart)), err) == SipFail)
			return SipFail;
	}


	slTime->pStart = temp_start;

	SIPDEBUGFN ( "Exitting setStartInTime");

	*err = E_NO_ERROR;
	return SipSuccess;

}

/*******************************************************************
** FUNCTION: sdp_setAddrInOrigin
**
** DESCRIPTION: This function sets the pTranspAddr field in an SDP pOrigin
**
*******************************************************************/
SipBool sdp_setAddrInOrigin
#ifdef ANSI_PROTO
	( SdpOrigin * pOrigin,	/*Origin pHeader */
	  SIP_S8bit * dAddr,
	  SipError * err)
#else
	( pOrigin, dAddr, err )	/*Origin pHeader */
	  SdpOrigin * pOrigin;
	  SIP_S8bit * dAddr;
	  SipError * err;
#endif

{
#ifndef SIP_BY_REFERENCE
	SIP_U32bit dLength;
#endif
	SIP_S8bit * temp_addr;
	SdpOrigin * temp_origin;
	SIPDEBUGFN("Entering setAddrInOrigin\n");
#ifndef SIP_NO_CHECK
	if( err == SIP_NULL )
		return SipFail;

	if ( pOrigin == SIP_NULL)
	{
		*err = E_INV_PARAM;
		return SipFail;
	}
#endif
	if( dAddr == SIP_NULL)
		temp_addr = SIP_NULL;
	else
	{
#ifndef SIP_BY_REFERENCE
		dLength = strlen( dAddr );
		temp_addr = ( SIP_S8bit * )fast_memget(ACCESSOR_MEM_ID,dLength+1,err);
		if ( temp_addr == SIP_NULL )
			return SipFail;
		strcpy( temp_addr, dAddr );
#else
		temp_addr = dAddr;
#endif
	}

	temp_origin = ( SdpOrigin *) ( pOrigin);
	if ( temp_origin->pAddr != SIP_NULL )
	{
		if ( fast_memfree(ACCESSOR_MEM_ID, temp_origin->pAddr, err) == SipFail)
			return SipFail;
	}


	temp_origin->pAddr = temp_addr;



	SIPDEBUGFN("Exitiing setAddrInOrigin\n");
	*err = E_NO_ERROR;
	return SipSuccess;
}

/*******************************************************************
** FUNCTION: sdp_setSession
**
** DESCRIPTION: This function sets the pSession field in an SDP
**		message structure
**
*******************************************************************/
SipBool sdp_setSession
#ifdef ANSI_PROTO
	( SdpMessage *msg, SIP_S8bit *pSession, SipError *err)
#else
	( msg, pSession, err )
	  SdpMessage *msg;
	  SIP_S8bit *pSession;
	  SipError *err;
#endif
{
#ifndef SIP_BY_REFERENCE
	SIP_U32bit dLength;
#endif
	SIP_S8bit * temp_session;
	SdpMessage *temp_mssg;
	 SIPDEBUGFN("Entering setSession\n") ;
#ifndef SIP_NO_CHECK
	if( err == SIP_NULL )
		return SipFail;

	if ( msg == SIP_NULL)
	{
		*err = E_INV_PARAM;
		return SipFail;
	}
#endif
	if( pSession == SIP_NULL)
		temp_session = SIP_NULL;
	else
	{
#ifndef SIP_BY_REFERENCE
		dLength = strlen( pSession );
		temp_session = ( SIP_S8bit * )fast_memget(ACCESSOR_MEM_ID,dLength+1,err);
		if ( temp_session== SIP_NULL )
			return SipFail;

		strcpy( temp_session, pSession );
#else
		temp_session = pSession;
#endif

	}

	temp_mssg = ( SdpMessage *)msg ;

	if ( temp_mssg->pSession != SIP_NULL )
	{
		if ( fast_memfree(ACCESSOR_MEM_ID, temp_mssg->pSession, err) == SipFail)
			return SipFail;
	}
        temp_mssg->pSession=temp_session;



	SIPDEBUGFN("Exitting setSession\n") ;
	*err = E_NO_ERROR;
	return SipSuccess;

}

/*****************************************************************
**
** FUNCTION:  sdp_insertTimeAtIndex
**
** DESCRIPTION: This function inserts a slTime-parameter at specified
**		index from an SDP message structure
**
******************************************************************/
SipBool sdp_insertTimeAtIndex
#ifdef ANSI_PROTO
	( SdpMessage 	*msg,
	  SdpTime	*slTime,
	  SIP_U32bit 	cnt,
	  SipError 	*err )
#else
	( msg,slTime,cnt,err)
	  SdpMessage 	*msg;
	  SdpTime	*slTime;
	  SIP_U32bit 	cnt;
	  SipError 	*err;
#endif
{
	SdpTime 	*element_in_list;

	SIPDEBUGFN("Entering InsertTimeAtIndex\n");
#ifndef SIP_NO_CHECK
	if( err == SIP_NULL )
		return SipFail;

	if ( msg == SIP_NULL )
	{
		*err = E_INV_PARAM;
		return SipFail;
	}
#endif
	if ( slTime == SIP_NULL )
		element_in_list = SIP_NULL;
	else
	{
#ifndef SIP_BY_REFERENCE
		if ( sip_initSdpTime(&element_in_list,err) == SipFail)
			return SipFail;
		if ( __sip_cloneSdpTime(element_in_list, slTime,err) == SipFail)
		{
			sip_freeSdpTime(element_in_list);
			return SipFail;
		}
#else
		HSS_LOCKEDINCREF(slTime->dRefCount);
		element_in_list = slTime;
#endif
	}

	if( sip_listInsertAt(&(msg->slTime), cnt, (SIP_Pvoid) element_in_list, err) == SipFail)
	{
#ifndef SIP_BY_REFERENCE
		sip_freeSdpTime(element_in_list);
#else
		HSS_LOCKEDDECREF(slTime->dRefCount);
#endif
		return SipFail;
	}
	SIPDEBUGFN("Exitting InsertTimeAtIndex\n");
	*err = E_NO_ERROR;
	return SipSuccess;
}

/*********************************************************************
** FUNCTION:  sdp_setMvalueInMedia
**
**********************************************************************
**
** DESCRIPTION: Sets the m pValue in sdp slMedia structure.
**
*********************************************************************/
SipBool sdp_setMvalueInMedia
#ifdef ANSI_PROTO
	( SdpMedia	*slMedia,
	  SIP_S8bit 	*pMediaValue,
	  SipError 	*err )
#else
	( slMedia, pMediaValue, err)
	  SdpMedia	*slMedia;
	  SIP_S8bit 	*pMediaValue;
	  SipError 	*err;
#endif
{
#ifndef SIP_BY_REFERENCE
	SIP_U32bit dLength;
#endif
	SIP_S8bit * temp_mvalue;

	SIPDEBUGFN ( "Entering setMvalueInMedia");
#ifndef SIP_NO_CHECK
	if( err == SIP_NULL )
		return SipFail;

	if ( slMedia == SIP_NULL)
	{
		*err = E_INV_PARAM;
		return SipFail;
	}
#endif
	if( pMediaValue == SIP_NULL)
		temp_mvalue = SIP_NULL;
	else
	{
#ifndef SIP_BY_REFERENCE
		dLength = strlen( pMediaValue );
		temp_mvalue = ( SIP_S8bit * )fast_memget(ACCESSOR_MEM_ID,dLength+1,err);
		if ( temp_mvalue == SIP_NULL )
			return SipFail;

		strcpy( temp_mvalue, pMediaValue );
#else
		temp_mvalue = pMediaValue;
#endif

	}

	if ( slMedia->pMediaValue != SIP_NULL )
	{
		if ( sip_memfree(ACCESSOR_MEM_ID, (SIP_Pvoid *)(&(slMedia->pMediaValue)), err) == SipFail)
			return SipFail;
	}


	slMedia->pMediaValue = temp_mvalue;

	SIPDEBUGFN ( "Exitting setMvalueInMedia");

	*err = E_NO_ERROR;
	return SipSuccess;

}

/*********************************************************************
** FUNCTION:  sdp_setAddrTypeInConnection
**
**********************************************************************
**
** DESCRIPTION: Sets the dAddr dType field pValue in slConnection structure.
**
*********************************************************************/
SipBool sdp_setAddrTypeInConnection
#ifdef ANSI_PROTO
	( SdpConnection	*slConnection,
	  SIP_S8bit 	*atype,
	  SipError 	*err )
#else
	( slConnection, atype, err)
	  SdpConnection	*slConnection;
	  SIP_S8bit 	*atype;
	  SipError 	*err;
#endif
{
#ifndef SIP_BY_REFERENCE
	SIP_U32bit dLength;
#endif
	SIP_S8bit * temp_addr_type;

	SIPDEBUGFN ( "Entering setAddrTypeInConnection");
#ifndef SIP_NO_CHECK
	if( err == SIP_NULL )
		return SipFail;

	if ( slConnection == SIP_NULL)
	{
		*err = E_INV_PARAM;
		return SipFail;
	}
#endif
	if( atype == SIP_NULL)
		temp_addr_type = SIP_NULL;
	else
	{
#ifndef SIP_BY_REFERENCE
		dLength = strlen( atype );
		temp_addr_type = ( SIP_S8bit * )fast_memget(ACCESSOR_MEM_ID,dLength+1,err);
		if ( temp_addr_type == SIP_NULL )
			return SipFail;

		strcpy( temp_addr_type, atype );
#else
		temp_addr_type = atype;
#endif
	}

	if ( slConnection->pAddrType != SIP_NULL )
	{
		if ( sip_memfree(ACCESSOR_MEM_ID, (SIP_Pvoid *)(&(slConnection->pAddrType)), err) == SipFail)
			return SipFail;
	}


	slConnection->pAddrType = temp_addr_type;

	SIPDEBUGFN ( "Exitting setAddrTypeInConnection");

	*err = E_NO_ERROR;
	return SipSuccess;

}

/*******************************************************************
** FUNCTION: sdp_setVersionInOrigin
**
** DESCRIPTION: This function sets the pVersion filed in an SDP pOrigin
**
*******************************************************************/
SipBool sdp_setVersionInOrigin
#ifdef ANSI_PROTO
	( SdpOrigin * pOrigin,	/*Origin pHeader */
	  SIP_S8bit * pVersion,
	  SipError * err)
#else
	( pOrigin, pVersion, err )	/*Origin pHeader */
	  SdpOrigin * pOrigin;
	  SIP_S8bit * pVersion;
	  SipError * err;
#endif

{
#ifndef SIP_BY_REFERENCE
	SIP_U32bit dLength;
#endif
	SIP_S8bit * temp_version;
	SdpOrigin * temp_origin;
	SIPDEBUGFN("Entering setVersionInOrigin\n") ;
#ifndef SIP_NO_CHECK
	if( err == SIP_NULL )
		return SipFail;

	if ( pOrigin == SIP_NULL)
	{
		*err = E_INV_PARAM;
		return SipFail;
	}
#endif

	if( pVersion == SIP_NULL)
		temp_version = SIP_NULL;
	else
	{
#ifndef SIP_BY_REFERENCE
		dLength = strlen( pVersion );
		temp_version = ( SIP_S8bit * )fast_memget(ACCESSOR_MEM_ID,dLength+1,err);
		if ( temp_version == SIP_NULL )
			return SipFail;
		strcpy( temp_version, pVersion );
#else
		temp_version = pVersion;
#endif
	}
	temp_origin = ( SdpOrigin *) ( pOrigin);
	if ( temp_origin->pVersion != SIP_NULL )
	{
		if ( fast_memfree(ACCESSOR_MEM_ID, temp_origin->pVersion, err) == SipFail)
			return SipFail;
	}


	temp_origin->pVersion = temp_version;

	SIPDEBUGFN("Exitiing setVersionInOrigin\n");
	*err = E_NO_ERROR;
	return SipSuccess;
}
/*********************************************************************
** FUNCTION:  sdp_setAddrInConnection
**
**********************************************************************
**
** DESCRIPTION: Sets the dAddr field pValue in slConnection strucutre.
**
*********************************************************************/
SipBool sdp_setAddrInConnection
#ifdef ANSI_PROTO
	( SdpConnection	*slConnection,
	  SIP_S8bit 	*dAddr,
	  SipError 	*err )
#else
	( slConnection, dAddr, err)
	  SdpConnection	*slConnection;
	  SIP_S8bit 	*dAddr;
	  SipError 	*err;
#endif
{
#ifndef SIP_BY_REFERENCE
	SIP_U32bit dLength;
#endif
	SIP_S8bit * temp_addr;

	SIPDEBUGFN ( "Entering setAddrInConnection");
#ifndef SIP_NO_CHECK
	if( err == SIP_NULL )
		return SipFail;

	if ( slConnection == SIP_NULL)
	{
		*err = E_INV_PARAM;
		return SipFail;
	}
#endif
	if( dAddr == SIP_NULL)
		temp_addr = SIP_NULL;
	else
	{
#ifndef SIP_BY_REFERENCE
		dLength = strlen( dAddr );
		temp_addr = ( SIP_S8bit * )fast_memget(ACCESSOR_MEM_ID,dLength+1,err);
		if ( temp_addr == SIP_NULL )
			return SipFail;

		strcpy( temp_addr, dAddr );
#else
		temp_addr = dAddr;
#endif
	}

	if ( slConnection->pAddr != SIP_NULL )
	{
		if ( sip_memfree(ACCESSOR_MEM_ID, (SIP_Pvoid*)(&(slConnection->pAddr)), err) == SipFail)
			return SipFail;
	}


	slConnection->pAddr = temp_addr;

	SIPDEBUGFN ( "Exitting setAddrInConnection");

	*err = E_NO_ERROR;
	return SipSuccess;

}
/*******************************************************************
** FUNCTION: sdp_setNetTypeInOrigin
**
** DESCRIPTION: This function sets the network-dType field in an SDP
**		Origin
**
*******************************************************************/
SipBool sdp_setNetTypeInOrigin
#ifdef ANSI_PROTO
	( SdpOrigin * pOrigin,	/*Origin pHeader */
	  SIP_S8bit * ntype,
	  SipError * err)
#else
	( pOrigin, ntype, err )	/*Origin pHeader */
	  SdpOrigin * pOrigin;
	  SIP_S8bit * ntype;
	  SipError * err;
#endif

{
#ifndef SIP_BY_REFERENCE
	SIP_U32bit dLength;
#endif
	SIP_S8bit * temp_net_type;
	SdpOrigin * temp_origin;
	SIPDEBUGFN("Entering setNetTypeInOrigin\n");
#ifndef SIP_NO_CHECK
	if( err == SIP_NULL )
		return SipFail;

	if ( pOrigin == SIP_NULL)
	{
		*err = E_INV_PARAM;
		return SipFail;
	}

#endif
	if( ntype == SIP_NULL)
		temp_net_type = SIP_NULL;
	else
	{
#ifndef SIP_BY_REFERENCE
		dLength = strlen( ntype );
		temp_net_type = ( SIP_S8bit * )fast_memget(ACCESSOR_MEM_ID,dLength+1,err);
		if ( temp_net_type == SIP_NULL )
			return SipFail;
		strcpy( temp_net_type, ntype );
#else
		temp_net_type = ntype;
#endif
	}


	temp_origin = ( SdpOrigin *) ( pOrigin);
	if ( temp_origin->pNetType != SIP_NULL )
	{
		if ( fast_memfree(ACCESSOR_MEM_ID, temp_origin->pNetType, err) == SipFail)
			return SipFail;
	}


	temp_origin->pNetType = temp_net_type;



	SIPDEBUGFN("Exitiing setNetTypeInOrigin\n");
	*err = E_NO_ERROR;
	return SipSuccess;
}
/********************************************************************
** FUNCTION:sdp_insertMediaAtIndex
**
** DESCRIPTION: This function inserts a slMedia-parameter at a specified
**		index in an SDP message
**
**********************************************************************/

SipBool sdp_insertMediaAtIndex
#ifdef ANSI_PROTO
	( SdpMessage 	*msg,
	  SdpMedia	*slMedia,
	  SIP_U32bit 	cnt,
	  SipError 	*err )
#else
	( msg,slMedia,cnt,err)
	  SdpMessage 	*msg;
	  SdpMedia	*slMedia;
	  SIP_U32bit 	cnt;
	  SipError 	*err;
#endif
{
	SdpMedia 	*element_in_list;

	SIPDEBUGFN("Entering InsertMediaAtIndex\n");
#ifndef SIP_NO_CHECK
	if( err == SIP_NULL )
		return SipFail;

	if ( msg == SIP_NULL )
	{
		*err = E_INV_PARAM;
		return SipFail;
	}
#endif
	/* copying the slMedia structure */
	if ( slMedia == SIP_NULL )
		element_in_list = SIP_NULL;
	else
	{
#ifndef SIP_BY_REFERENCE
		if(sip_initSdpMedia(&element_in_list,err) == SipFail)
			return SipFail;

		if ( __sip_cloneSdpMedia(element_in_list, slMedia,err) == SipFail)
		{
			sip_freeSdpMedia(element_in_list);
			return SipFail;
		}
#else
		HSS_LOCKEDINCREF(slMedia->dRefCount);
		element_in_list = (SdpMedia *)slMedia;
#endif
	}

	if( sip_listInsertAt(&(msg->slMedia), cnt, element_in_list, err) == SipFail)
	{
#ifndef SIP_BY_REFERENCE
		sip_freeSdpMedia(element_in_list);
#else
		HSS_LOCKEDDECREF(slMedia->dRefCount);
#endif
		return SipFail;
	}

	SIPDEBUGFN("Exitting InsertMediaAtIndex\n");
	*err = E_NO_ERROR;
	return SipSuccess;
}



/*********************************************************************
** FUNCTION:  sdp_setConnectionAtIndexInMedia
**
**********************************************************************
**
** DESCRIPTION: Sets the slConnection structure at a specified index
**		( starting from 0 ) in the slMedia strucutre.
**
*********************************************************************/
SipBool sdp_setConnectionAtIndexInMedia
#ifdef ANSI_PROTO
	( SdpMedia 	*slMedia,
	  SdpConnection	*slConnection,
	  SIP_U32bit 	index,
	  SipError 	*err )
#else
	( slMedia,slConnection,index,err)
	  SdpMedia 	*slMedia;
	  SdpConnection	*slConnection;
	  SIP_U32bit 	index;
	  SipError 	*err;
#endif
{
	SdpConnection 	*element_in_list;

	SIPDEBUGFN ( "Entering setConnectionAtIndexInMedia");
#ifndef SIP_NO_CHECK
	if( err == SIP_NULL )
		return SipFail;

	if (slMedia == SIP_NULL)
	{
		*err = E_INV_PARAM;
		return SipFail;
	}
#endif
	if ( slConnection == SIP_NULL )
		element_in_list = SIP_NULL;
	else
	{
#ifndef SIP_BY_REFERENCE
		if (sip_initSdpConnection(&element_in_list, err) == SipFail)
			return SipFail;
		if ( __sip_cloneSdpConnection(element_in_list, slConnection,err) == SipFail)
		{
			sip_freeSdpConnection(element_in_list );
			return SipFail;
		}
#else
		HSS_LOCKEDINCREF(slConnection->dRefCount);
		element_in_list = (SdpConnection *)slConnection;
#endif
	}

	if( sip_listSetAt(&(slMedia->slConnection), index, (SIP_Pvoid) element_in_list, err) == SipFail)
	{
#ifndef SIP_BY_REFERENCE
		if ( element_in_list != SIP_NULL )
			 sip_freeSdpConnection(element_in_list );
#else
		HSS_LOCKEDDECREF(slConnection->dRefCount);
#endif
		return SipFail;
	}
	SIPDEBUGFN ( "Exitting setConnectionAtIndexInMedia");
	*err = E_NO_ERROR;
	return SipSuccess;
}
/*******************************************************************
** FUNCTION: sdp_setUserInOrigin
**
** DESCRIPTION: This function sets the pOrigin in an SDP pOrigin
**
*******************************************************************/
SipBool sdp_setUserInOrigin
#ifdef ANSI_PROTO
	( SdpOrigin * pOrigin,	/*Origin pHeader */
	  SIP_S8bit * pUser,
	  SipError * err)
#else
	( pOrigin, pUser, err )	/*Origin pHeader */
	  SdpOrigin * pOrigin;
	  SIP_S8bit * pUser;
	  SipError * err;
#endif

{
#ifndef SIP_BY_REFERENCE
	SIP_U32bit dLength;
#endif
	SIP_S8bit * temp_user;
	SdpOrigin * temp_origin;
	SIPDEBUGFN("Entering setUserInOrigin\n");
#ifndef SIP_NO_CHECK
	if( err == SIP_NULL )
		return SipFail;
	if ( pOrigin == SIP_NULL)
	{
		*err = E_INV_PARAM;
		return SipFail;
	}

#endif
	if( pUser == SIP_NULL)
		temp_user = SIP_NULL;
	else
	{
#ifndef SIP_BY_REFERENCE
		dLength = strlen( pUser );
		temp_user = ( SIP_S8bit * )fast_memget(ACCESSOR_MEM_ID,dLength+1,err);
		if ( temp_user == SIP_NULL )
			return SipFail;
		strcpy( temp_user, pUser );
#else
		temp_user = pUser;
#endif
	}


	temp_origin = ( SdpOrigin *) ( pOrigin);
	if ( temp_origin->pUser != SIP_NULL )
	{
		if ( fast_memfree(ACCESSOR_MEM_ID, temp_origin->pUser, err) == SipFail)
			return SipFail;
	}


	temp_origin->pUser = temp_user;



	 SIPDEBUGFN("Exitiing setUserInOrigin\n");

	*err = E_NO_ERROR;
	return SipSuccess;
}
/********************************************************************
**
** FUNCTION:  sip_insertMsgBodyAtIndex
**
** DESCRIPTION:  This function inserts a MsgBody "msgbody" at the index "index" in a SipMessage "s".
**
*********************************************************************/
SipBool sip_insertMsgBodyAtIndex
#ifdef ANSI_PROTO
(SipMessage *s, SipMsgBody *msgbody, SIP_U32bit index, SipError *err)
#else
	(s, msgbody, index, err)
	SipMessage *s; 
	SipMsgBody *msgbody; 
	SIP_U32bit index; 
	SipError *err;
#endif
{
	SipMsgBody *body_in_list;

#ifndef SIP_NO_CHECK
	/* Validate parameters */
	if(err==SIP_NULL) return SipFail;

	if((s==SIP_NULL)||(msgbody==SIP_NULL)) 
	{
		*err = E_INV_PARAM;
		return SipFail;
	}
	
	/* Validate pBody dType */
	switch(msgbody->dType)
	{
		case SipSdpBody:
		case SipUnknownBody:
		case SipIsupBody:                     /* bcpt ext */
		case SipMultipartMimeBody:			  /* bcpt ext */
		case SipAppSipBody:
			break;
		default:
			*err = E_INV_PARAM;
			return SipFail;
	}
#endif
	
	/* clone pBody */
#ifdef SIP_BY_REFERENCE
	body_in_list=msgbody;
	HSS_LOCKEDINCREF(body_in_list->dRefCount);
#else
	if(sip_initSipMsgBody(&body_in_list, SipBodyAny, err)==SipFail)
		return SipFail;
	if(__sip_cloneSipMsgBody(body_in_list,msgbody,err)==SipFail)
	{
		sip_freeSipMsgBody(body_in_list);
		return SipFail;
	}
#endif
	/* insert message pBody in list */
	if(sip_listInsertAt(&(s->slMessageBody), index, (SIP_Pvoid)body_in_list, err) == SipFail)
	{
		sip_freeSipMsgBody(body_in_list);
		return SipFail;
	}

	*err = E_NO_ERROR;
	return SipSuccess;
}

/*******************************************************************
** FUNCTION: sdp_setConnection
**
** DESCRIPTION: This function sets the slConnection field in an SDP
**		message structure
**
*******************************************************************/
SipBool sdp_setConnection
#ifdef ANSI_PROTO
	( SdpMessage *msg, SdpConnection *slConnection, SipError *err)
#else
	( msg, slConnection, err )
	  SdpMessage *msg;
	  SdpConnection *slConnection;
	  SipError *err;
#endif
{

	SIPDEBUGFN("Entering setVersion\n");
#ifndef SIP_NO_CHECK
	if( err == SIP_NULL )
		return SipFail;

	if ( msg == SIP_NULL)
	{
		*err = E_INV_PARAM;
		return SipFail;
	}
#endif

	if(msg->slConnection!=SIP_NULL)
	{
		sip_freeSdpConnection(msg->slConnection);
	}
	if(slConnection == SIP_NULL)
	{
		msg->slConnection = SIP_NULL;
	}
	else
	{
#ifndef SIP_BY_REFERENCE
		if(sip_initSdpConnection(&(msg->slConnection),err)==SipFail)
				return SipFail;
		if(__sip_cloneSdpConnection(msg->slConnection,slConnection,err)==SipFail)
		{
			return SipFail;
		}
#else
		HSS_LOCKEDINCREF(slConnection->dRefCount);
		msg->slConnection = slConnection;
#endif
	}

	SIPDEBUGFN("Exitting setVersion\n");

	*err = E_NO_ERROR;
	return SipSuccess;

}

/***************************************************************
** FUNCTION: sip_setUserInUrl
**
** DESCRIPTION: this function sets the pUser in a SIP Url
**
***************************************************************/

SipBool sip_setUserInUrl
#ifdef ANSI_PROTO
	( SipUrl *pUrl, SIP_S8bit *pUser, SipError *pErr)
#else
	( pUrl, pUser, pErr )
	  SipUrl *pUrl;
	  SIP_S8bit *pUser;
	  SipError *pErr;
#endif
{

#ifndef SIP_BY_REFERENCE
	SIP_U32bit dLength;
	SIP_S8bit * pTemp_user;
#endif
	SIPDEBUGFN( "Entering setUserInUrl");
#ifndef SIP_NO_CHECK
	if( pErr == SIP_NULL )
		return SipFail;

	if ( pUrl == SIP_NULL)
	{
		*pErr = E_INV_PARAM;
		return SipFail;
	}
#endif

	if(pUrl->pUser !=SIP_NULL)
	{
		if(sip_memfree(ACCESSOR_MEM_ID,(SIP_Pvoid*)&(pUrl->pUser),pErr)\
																	==SipFail)
		return SipFail;
	}
#ifdef SIP_BY_REFERENCE
	pUrl->pUser = pUser;
#else
	if( pUser == SIP_NULL)
		pTemp_user = SIP_NULL;
	else
	{
		dLength = strlen( pUser );
		pTemp_user = ( SIP_S8bit * )fast_memget(ACCESSOR_MEM_ID,dLength+1,pErr);
		if ( pTemp_user== SIP_NULL )
			return SipFail;

		strcpy( pTemp_user, pUser );

	}
	pUrl->pUser = pTemp_user;
#endif

	SIPDEBUGFN( "Exitting setUserInUrl");

	*pErr = E_NO_ERROR;
	return SipSuccess;

}

/***************************************************************
** FUNCTION: sip_setUrlInAddrSpec
**
** DESCRIPTION: This function sets the url field in a Sip dAddr-spec
**
***************************************************************/

SipBool sip_setUrlInAddrSpec
#ifdef ANSI_PROTO
	(SipAddrSpec *pAddrspec,
	SipUrl *pUrl,
	SipError *pErr)
#else
	( pAddrspec, pUrl, pErr )
	  SipAddrSpec *pAddrspec;
	  SipUrl *pUrl;
	  SipError *pErr;
#endif
{
#ifndef SIP_BY_REFERENCE
	SipUrl *pTo;
#endif
	SIPDEBUGFN( "Entering setUrlInAddrSpec");
#ifndef SIP_NO_CHECK
	if(pErr == SIP_NULL)
		return SipFail;
	if ( pAddrspec == SIP_NULL)
	{
		*pErr = E_INV_PARAM;
		return SipFail;
	}
	if( pAddrspec->dType != SipAddrSipUri )
	{
		*pErr = E_INV_TYPE;
		return SipFail;
	}
#endif
	sip_freeSipUrl(pAddrspec->u.pSipUrl);
	if (pUrl==SIP_NULL)
	{
		pAddrspec->u.pSipUrl = SIP_NULL;
	}
	else
	{	
#ifdef SIP_BY_REFERENCE
		HSS_LOCKEDINCREF(pUrl->dRefCount);
		pAddrspec->u.pSipUrl = pUrl;
#else
		if(sip_initSipUrl(&pTo,pErr)==SipFail)
		{
			return SipFail;
		}
		if(__sip_cloneSipUrl(pTo,pUrl,pErr)==SipFail)
		{
			sip_freeSipUrl(pTo);
			return SipFail;
		}
		pAddrspec->u.pSipUrl = pTo;
#endif
	}	
	*pErr=E_NO_ERROR;
	SIPDEBUGFN( "Exitting setUrlInAddrSpec");
	return SipSuccess;
}

/*****************************************************************
**
** FUNCTION:  sip_setAddrSpecInReqLine
**
** DESCRIPTION: This function sets the dAddr-spec field in a SIP
**		Request line
**
***************************************************************/
SipBool sip_setAddrSpecInReqLine
#ifdef ANSI_PROTO
	(SipReqLine *line, SipAddrSpec *pRequestUri, SipError *err)
#else
	(line, pRequestUri, err)
	SipReqLine *line;
	SipAddrSpec *pRequestUri;
	SipError *err;
#endif
{
#ifndef SIP_BY_REFERENCE
 	SipAddrSpec *temp_addr_spec;
#endif
	SIPDEBUGFN("Entering function sip_setAddrSpecInReqLine");
#ifndef SIP_NO_CHECK
	if( err == SIP_NULL)
		return SipFail;
	if( line == SIP_NULL)
	{
		*err = E_INV_PARAM;
		return SipFail;
	}
#endif
 	if (pRequestUri == SIP_NULL)
	{
		sip_freeSipAddrSpec(line->pRequestUri);
 		line->pRequestUri = SIP_NULL;
	}
	else
	{
#ifndef SIP_BY_REFERENCE
		if ( sip_initSipAddrSpec(&temp_addr_spec, pRequestUri->dType , err ) == SipFail)
			return SipFail;
 		if (__sip_cloneSipAddrSpec(temp_addr_spec, pRequestUri, err) == SipFail)
		{
			sip_freeSipAddrSpec(temp_addr_spec);
			return SipFail;
       		}
		sip_freeSipAddrSpec(line->pRequestUri);
		line->pRequestUri = temp_addr_spec;
#else
		HSS_LOCKEDINCREF(pRequestUri->dRefCount);
		sip_freeSipAddrSpec (line->pRequestUri);
		line->pRequestUri = pRequestUri;
#endif
	}

	*err = E_NO_ERROR;
	SIPDEBUGFN("Exitting function sip_setAddrSpecInReqLine");
	return SipSuccess;
}
/********************************************************************
**
** FUNCTION:  sip_setReqLineInSipReqMsg
**
** DESCRIPTION: This function sets the Request Line in a SipMessage of dType SipMessageRequest.
**
*********************************************************************/
SipBool sip_setReqLineInSipReqMsg
#ifdef ANSI_PROTO
	(SipMessage *msg, SipReqLine *line, SipError *err)
#else
	(msg, line, err)
	SipMessage *msg;
	SipReqLine *line;
	SipError *err;
#endif
{
#ifndef SIP_BY_REFERENCE
	SipReqLine *temp_req_line;
#endif

	SIPDEBUGFN("Entering function sip_setReqLineInSipReqMsg");

#ifndef SIP_NO_CHECK
	if (err == SIP_NULL)
		return SipFail;
	if (msg == SIP_NULL)
	{
		*err = E_INV_PARAM;
		return SipFail;
	}
	if (msg->dType != SipMessageRequest)
	{
		*err = E_INV_TYPE;
		return SipFail;
	}
#endif
	if (msg->u.pRequest == SIP_NULL)
	{
		if (sip_initSipReqMessage(&((msg->u).pRequest), err) == SipFail)
				return SipFail;
	}
	
	if (line == SIP_NULL)
	{
		if (((msg->u).pRequest)->pRequestLine != SIP_NULL)
			sip_freeSipReqLine(((msg->u).pRequest)->pRequestLine);
		((msg->u).pRequest)->pRequestLine = SIP_NULL;	
	}
	else
	{
#ifndef SIP_BY_REFERENCE
		if(sip_initSipReqLine(&temp_req_line, err ) == SipFail)
			return SipFail;
		if (__sip_cloneSipReqLine(temp_req_line, line, err) == SipFail)
		{
			sip_freeSipReqLine(temp_req_line);
			return SipFail;
		}
		sip_freeSipReqLine(((msg->u).pRequest)->pRequestLine);
		((msg->u).pRequest)->pRequestLine = temp_req_line;
#else
		if (((msg->u).pRequest)->pRequestLine != SIP_NULL)
			sip_freeSipReqLine(((msg->u).pRequest)->pRequestLine);
		HSS_LOCKEDINCREF(line->dRefCount);
		((msg->u).pRequest)->pRequestLine = line;
#endif
	}
			
	*err = E_NO_ERROR;
	SIPDEBUGFN("Exitting function sip_setReqLineInSipReqMsg");
	return SipSuccess;
}

/*****************************************************************
**
** FUNCTION:  sip_setMethodInReqLine
**
** DESCRIPTION: This function sets the pMethod field in a SIP
**		Request line
**
***************************************************************/
SipBool sip_setMethodInReqLine
#ifdef ANSI_PROTO
	(SipReqLine *line, SIP_S8bit *pMethod, SipError *err)
#else
	(line, pMethod, err)
	SipReqLine *line;
	SIP_S8bit *pMethod;
	SipError *err;
#endif
{
	SIP_S8bit *temp_method, *mthd;
 	SIPDEBUGFN("Entering function sip_setMethodInReqLine");
#ifndef SIP_NO_CHECK
	if( err == SIP_NULL)
		return SipFail;
	if( line == SIP_NULL)
	{
		*err = E_INV_PARAM;
		return SipFail;
	}
#endif
        if( pMethod == SIP_NULL)
                temp_method = SIP_NULL;
        else
        {
#ifndef SIP_BY_REFERENCE
		temp_method = (SIP_S8bit *) STRDUPACCESSOR(pMethod);
		if (temp_method == SIP_NULL)
		{
			*err = E_NO_MEM;
			return SipFail;
		}
#else
		temp_method = pMethod;
#endif
	}

        mthd = line->pMethod;

	if ( mthd != SIP_NULL)
	{
		if (fast_memfree(ACCESSOR_MEM_ID, mthd, err) == SipFail)
			return SipFail;
	}

	line->pMethod = temp_method;

	*err = E_NO_ERROR;
	SIPDEBUGFN("Exitting function sip_setMethodInReqLine");
        return SipSuccess;
}

/*****************************************************************
**
** FUNCTION:  sip_setVersionInReqLine
**
** DESCRIPTION: This function sets the pVersion field in a SIP
**		Request line
**
***************************************************************/
SipBool sip_setVersionInReqLine
#ifdef ANSI_PROTO
	(SipReqLine *line, SIP_S8bit *pVersion, SipError *err)
#else
	(line, pVersion, err)
	SipReqLine *line;
	SIP_S8bit *pVersion;
	SipError *err;
#endif
{
	SIP_S8bit *temp_version, *ver;
 	SIPDEBUGFN("Entering function sip_setVersionInReqLine");
#ifndef SIP_NO_CHECK
	if( err == SIP_NULL)
		return SipFail;
	if( line == SIP_NULL)
	{
		*err = E_INV_PARAM;
		return SipFail;
	}
#endif
        if( pVersion == SIP_NULL)
                temp_version = SIP_NULL;
        else
        {
#ifndef SIP_BY_REFERENCE
		temp_version = (SIP_S8bit *) STRDUPACCESSOR(pVersion);
		if (temp_version == SIP_NULL)
		{
			*err = E_NO_MEM;
			return SipFail;
		}
#else
		temp_version = pVersion;
#endif
	}

        ver = line->pVersion;

	if ( ver != SIP_NULL)
	{
		if (fast_memfree(ACCESSOR_MEM_ID, ver, err) == SipFail)
			return SipFail;
	}

	line->pVersion = temp_version;

	*err = E_NO_ERROR;
	SIPDEBUGFN("Exitting function sip_setVersionInReqLine");
        return SipSuccess;
}

/*****************************************************************
**
** FUNCTION:  sip_setDispNameInToHdr
**
** DESCRIPTION: This functions sets the display-pName field in a SIP
**		To Header
**
***************************************************************/
SipBool sip_setDispNameInToHdr
#ifdef ANSI_PROTO
	(SipHeader *hdr, SIP_S8bit *pDispName, SipError *err)
#else
	(hdr, pDispName, err)
	SipHeader *hdr;
	SIP_S8bit *pDispName;
	SipError *err;
#endif
{
	SIP_S8bit *temp_disp_name, *dname;
	SIPDEBUGFN("Entering function sip_setDispNameInToHdr");
#ifndef SIP_NO_CHECK
	if( err == SIP_NULL)
		return SipFail;
	if( hdr == SIP_NULL)
	{
		*err = E_INV_PARAM;
		return SipFail;
	}
	if( hdr->dType != SipHdrTypeTo)
	{
		*err = E_INV_TYPE;
		return SipFail;
	}
	if( hdr->pHeader == SIP_NULL)
	{
		*err = E_INV_HEADER;
		return SipFail;
	}
#endif
        if( pDispName == SIP_NULL)
                temp_disp_name = SIP_NULL;
        else
        {
#ifndef SIP_BY_REFERENCE
	        temp_disp_name = (SIP_S8bit *) STRDUPACCESSOR (pDispName);
		if (temp_disp_name == SIP_NULL)
		{
			*err = E_NO_MEM;
			return SipFail;
		}
#else
		temp_disp_name = pDispName;
#endif
	}

        dname = ((SipToHeader *)(hdr->pHeader))->pDispName;
        if (dname != SIP_NULL)
        {
        	if (sip_memfree(ACCESSOR_MEM_ID, (SIP_Pvoid *)(&dname), err) == SipFail)
		{
			sip_freeString(temp_disp_name);
        		return SipFail;
		}
        }
	((SipToHeader *)(hdr->pHeader))->pDispName = temp_disp_name;

	*err = E_NO_ERROR;
	SIPDEBUGFN("Exitting function sip_setDispNameInToHdr");
	return SipSuccess;
}


/***************************************************************
** FUNCTION: sip_setHostInUrl
**
** DESCRIPTION: this function sets the pHost field in a SIP Url
**
***************************************************************/

SipBool sip_setHostInUrl
#ifdef ANSI_PROTO
	( SipUrl *pUrl, SIP_S8bit *pHost, SipError *pErr)
#else
	( pUrl, pHost, pErr )
	  SipUrl *pUrl;
	  SIP_S8bit *pHost;
	  SipError *pErr;
#endif
{

#ifndef SIP_BY_REFERENCE
	SIP_U32bit dLength;
	SIP_S8bit * pTemp_host;
#endif
	SIPDEBUGFN( "Entering setHostInUrl");
#ifndef SIP_NO_CHECK
	if( pErr == SIP_NULL )
		return SipFail;
	if ( pUrl == SIP_NULL)
	{
		*pErr = E_INV_PARAM;
		return SipFail;
	}
#endif
	if ( pUrl->pHost != SIP_NULL )
	{
		if ( sip_memfree(ACCESSOR_MEM_ID,(SIP_Pvoid*)&( pUrl->pHost), pErr)\
																	== SipFail)
		return SipFail;
	}
#ifdef SIP_BY_REFERENCE
	pUrl->pHost = pHost;
#else
	if( pHost == SIP_NULL)
		pTemp_host = SIP_NULL;
	else
	{
		dLength = strlen( pHost );
		pTemp_host = ( SIP_S8bit * )fast_memget(ACCESSOR_MEM_ID,dLength+1,pErr);
		if ( pTemp_host== SIP_NULL )
			return SipFail;

		strcpy( pTemp_host, pHost );

	}

	pUrl->pHost = pTemp_host;
#endif
	SIPDEBUGFN( "Exitting setHostInUrl");
	*pErr = E_NO_ERROR;
	return SipSuccess;

}
/*****************************************************************
**
** FUNCTION:  sip_setAddrSpecInFromHdr
**
** DESCRIPTION: This function sets the dAddr-spec field in a SIP
**		From pHeader
**
***************************************************************/
SipBool sip_setAddrSpecInFromHdr
#ifdef ANSI_PROTO
	(SipHeader *hdr, SipAddrSpec *pAddrSpec, SipError *err)
#else
	(hdr, pAddrSpec, err)
	SipHeader *hdr;
	SipAddrSpec *pAddrSpec;
	SipError *err;
#endif
{
#ifndef SIP_BY_REFERENCE
	SipAddrSpec *temp_addr_spec;
#endif
	SIPDEBUGFN("Entering function sip_setAddrSpecInFromHdr");
#ifndef SIP_NO_CHECK
	if( err == SIP_NULL)
		return SipFail;
	if( hdr == SIP_NULL)
	{
		*err = E_INV_PARAM;
		return SipFail;
	}
	if( hdr->dType != SipHdrTypeFrom)
	{
		*err = E_INV_TYPE;
		return SipFail;
	}
	if( hdr->pHeader == SIP_NULL)
	{
		*err = E_INV_HEADER;
		return SipFail;
	}
#endif
 	if (pAddrSpec == SIP_NULL)
	{
		sip_freeSipAddrSpec(((SipFromHeader *)(hdr ->pHeader))->pAddrSpec);
 		((SipFromHeader *)(hdr ->pHeader))->pAddrSpec = SIP_NULL;
	}
	else
	{
#ifndef SIP_BY_REFERENCE
		if(sip_initSipAddrSpec(&temp_addr_spec, pAddrSpec->dType, err ) == SipFail)
			return SipFail;
		if (__sip_cloneSipAddrSpec(temp_addr_spec, pAddrSpec, err) == SipFail)
		{
			sip_freeSipAddrSpec(temp_addr_spec);
			return SipFail;
		}
		sip_freeSipAddrSpec(((SipFromHeader *)(hdr ->pHeader))->pAddrSpec);
		((SipFromHeader *)(hdr ->pHeader))->pAddrSpec = temp_addr_spec;
#else
		HSS_LOCKEDINCREF(pAddrSpec->dRefCount);
		sip_freeSipAddrSpec(((SipFromHeader *)(hdr ->pHeader))->pAddrSpec);
		((SipFromHeader *)(hdr ->pHeader))->pAddrSpec = pAddrSpec;
#endif

 	}

	*err = E_NO_ERROR;
	SIPDEBUGFN("Exitting function sip_setAddrSpecInFromHdr");
	return SipSuccess;

}

