/********************************************************************
  * Copyright (C) Aricent Inc . All Rights Reserved
  *
  * $Id: microsip_test.c,v 1.2 2013/12/18 12:50:12 siva Exp $
  *
  * Description: This file contains timer function implemetation
  *              for SIP module
  **********************************************************************/

/*
    This test stub can be used to send simple SIP Requests/Responses
    by UDP with retransmission. This program shows a sample timer function
    implementation. The timer uses a linked list in this program and relies
    on the select system call to handle timeout callbacks required by the
    stack. The developer may use other methods to implement the timer
    functionality expected by the stack.

    Usage:
        siptest <my_port> <dest_addredd> [dest_port]

        myport: local port on which the program listens for
                incoming sip messages
        dest_addr: address to which the messages are sent
        dest_port: port to which the messages are sent
                defaults to 5060 if not given.
*/

#ifdef __cplusplus
extern              "C"
{
#endif

#undef SIP_NO_FILESYS_DEP

#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <strings.h>

#include <sys/types.h>
#include <stdlib.h>
#include "microsip_portlayer.h"

#include "microsip_decode.h"
#include "microsip_struct.h"
#include "microsip_sendmessage.h"
#include "microsip_list.h"
#include "microsip_init.h"
#include "microsip_free.h"
#include "microsip_accessors.h"
#include "microsip_statistics.h"
#include "microsip_timer.h"
#include "microsip_trace.h"
#ifdef SIP_IMURL
#include "microsip_imurlfree.h"
#include "microsip_imurl.h"
#endif
#ifdef SIP_TEL
#include "microsip_telfree.h"
#include "microsip_telparser.h"
#endif
#ifdef SIP_TXN_LAYER
#include "microsip_txntimer.h"
#include "microsip_txnstruct.h"
#include "microsip_txndecode.h"
#include "microsip_txnfree.h"
#include "microsip_txnhash.h"
#endif

#if !defined(SIP_VXWORKS) && !defined(SIP_WINDOWS)
#include <pthread.h>
#endif

#define sip_setTagInToHdr sip_setTagInFromHdr
#define MAXMESG 5000

#define HSS_FREE(x) \
do \
{ \
    if ((x!=SIP_NULL)) (void)fast_memfree(0,x,SIP_NULL);\
} \
while(0)

    SIP_U16bit          requestFlag = 0;    /*Disabled */
    SIP_U16bit          responseClassFlag = 0;    /*Disabled */
    SIP_U16bit          responseCodeFlag = 0;    /*Disabled */
    SipList             slRequestl;

    SIP_S8bit           glbtransptype = 0;
#ifdef SIP_TXN_LAYER
    int                 glbtransactiontype = 0;
    int                 glbsetTimerValue = 0;
    SipTimeOutValues    dTimeOut;
    SIP_U32bit          glbTimeTimeoutOption;
#endif
#ifdef SIP_IMURL
    SipBool             do_stuff_imurl_byref (SipMessage * s);
#endif
#ifdef SIP_TEL
    SipBool             do_stuff_tel (SipMessage * s);
#endif

/* Internal Function prototypes */
    SipBool             sip_setTagInFromHdr (SipHeader * hdr, SIP_S8bit * tag,
                                             SipError * err);

#ifndef SIP_TXN_LAYER
    void                timerElementFree (SIP_Pvoid element);
    SIP_S32bit          getMinimumTimeout (SipList * list);
#else
    SIP_U32bit          getTimerTimeoutOptions (void);
#endif

    char                sip_getchar (void);

    void                displayParsedMessage (SipMessage * s);

#ifdef SIP_TEL
    SipBool             do_stuff_tel (SipMessage * s);
    SipBool             do_stuff_tel_nonbyref (SipMessage * s);
#endif

    SipBool             sendRequest (const char *method,
                                     SipTranspAddr * sendaddr, SipError * err);
    SipBool             sendResponse (int code, const char *reason,
                                      const char *method,
                                      SipTranspAddr * sendaddr, SipError * err);
    SIP_S8bit           sip_willParseSdpLine (SIP_S8bit * in, SIP_S8bit ** out);
    SipBool             sip_acceptIncorrectSdpLine (SIP_S8bit * in);
    SIP_S8bit           sip_willParseHeader (en_HeaderType type,
                                             SipUnknownHeader * hdr);
    SipBool             sip_acceptIncorrectHeader (en_HeaderType type,
                                                   SipUnknownHeader * hdr);
    void                sip_indicateUpdate (SipMessage * s,
                                            SipEventContext * context);
    void                showErrorInformation (SipMessage * pMessage);

/* Constants and globals */

    const char          test_tool_desc[] = "\n"
        "|----------------------------------------------------------------------|\n"
        "| microSIP Stack Test Tool                                                  |\n"
        "|----------------------------------------------------------------------|\n"
        "| This test program  demonstrates the parsing  capabilities of the SIP |\n"
        "| stack. It also demonstrates the retransmission handling capabilities |\n"
        "| of the stack. This program is not a SIP user agent.                  |\n"
        "|----------------------------------------------------------------------|\n";
    const char         *menu = "\n\n"
        "|--- Menu ------------------------------------------------------------|\n"
        "|                                                                     |\n"
#if defined (SIP_BADMESSAGE_PARSING)|| defined(SIP_INCREMENTAL_PARSING)
        "| z: for parser options menu                                          |\n"
#endif
        "| q: exit test tool                                                   |\n"
        "|                                                                     |\n"
        "| Options for sending requests:                                       |\n"
        "| i:INVITE    b:BYE     r:REGISTER  o:OPTIONS    k:PRACK     c:CANCEL |\n"
        "| a:ACK       e:REFER   f:INFO      g:COMET         v:UNKNOWN            |\n"
#ifdef SIP_IMPP
        "| u:SUBSCRIBE n:NOTIFY  m:MESSAGE                                     |\n"
#endif
        "|                                                                     |\n"
        "| Options for sending 200 OK responses:                               |\n"
        "| I:INVITE    B:BYE     R:REGISTER   O:OPTIONS   C-CANCEL   K-PRACK   |\n"
        "| E:REFER     G:COMET                                                 |\n"
#ifdef SIP_IMPP
        "| U:SUBSCRIBE N:NOTIFY                                                |\n"
#endif
        "|                                                                     |\n"
        "| Options for sending 100 Trying responses:                           |\n"
        "| Tb:BYE       Tc:CANCEL Tr:REGISTER  To:OPTIONS  Te:REFER  t:INVITE  |\n"
#ifdef SIP_IMPP
        "| Tu:SUBSCRIBE Tn:NOTIFY Tm:MESSAGE                                   |\n"
#endif
        "|                                                                     |\n"
        "| Options for sending other responses for INVITE:                     |\n"
        "| 3:300  4:401  0-180  1-181  2-182  s-183                            |\n"
        "|                                                                     |\n"
        "|---------------------------------------------------------------------|\n";

    int                 showMessage = 1;
    int                 constructCLen;
    int                 glbSipOption = 0;
    FILE               *ffp;
    FILE               *fp;

#if defined(SIP_WINDOWS) || defined(SIP_OSE)
    void                bzero (void *s, int n)
    {
        memset (s, 0, n);
    }
#endif

/*
   Modified implementation of getchar that ensures that the enter key
   pressed in response for a getchar() does not cause the next getchar()
   to return with the enter key character.fflush seems to have inconsistent
   behaviour in LINUX
*/
    char                sip_getchar (void)
    {
        char                retVal;
        retVal = getchar ();
        if (retVal != 0X0A)
        {
            char                tempVal;
            tempVal = getchar ();
            if (tempVal != 0X0A)
                ungetc (tempVal, stdin);
        }
        return retVal;
    }

/*
   Call back implementation
   The EventContext data structure contains data filled by the user.
   The stack does not understand the contents of this structure, but it
   often needs to release the structure. The user must implement this
   function depending on what is being passed in the pData field of
   the event-context structure.
*/
    void                sip_freeEventContext (SipEventContext * context)
    {
        if (context->pData != NULL)
            free (context->pData);
        if (context->pDirectBuffer != NULL)
            free (context->pDirectBuffer);
        free (context);
    }

/*=======================================================================

Selective parsing Call-backs

The following functions are callback functions that need to be implemented
only if the stack is compiled with the selective parsing option.

In this demonstration, they print out the header/SDP line when invoked.
In real applications they will contain logic to determine whether a header
needs to be parsed or to ignore errors based on the importance of the
header/SDP line in which the error occured.

========================================================================*/

/*
    Callback implementation
    This callback needs to be implemented if the stack is compiled with
    the SIP_SELECTIVE_PARSE option. This function will be called each time
    the stack is about to parse an SDP line. The application has the option
    of deciding on whether to ignore the line or modify it or parse it
    unmodified
*/

/*
    Callback implementation
    This callback needs to be implemented if the stack is compiled with
    the SIP_SELECTIVE_PARSE option. This function will be called each time
    the stack finds an SDP line with syntax errors. The application has
    the option of ignoring the bad line.
    If a bad line is ignored, it is stored in a list (slIncorrectLines)
    inside the SdpMessage structure. If the ignored line is part of a
    media description it is stored in list in the SdpMedia structure.
*/

/*
    Callback implementation
    This callback needs to be implemented if the stack is compiled with
    the SIP_SELECTIVE_PARSE option.
    This callback is invoked each time the parser is about to parse
    a header. The application has the option of skipping the header,
    treating it as an unknown header or parsing it normally.
    Treating the header as unknown means storing the name and the unparsed
    field value in the message structure in a list of unknown headers.
    For selectively parsing headers please also refer to the new decode
    option - SIP_OPT_SELECTIVE. This allows selection using a preset list
    without the overhead of callback function invocations. Documentation
    for the new selective parsing mechanism can be found in the API reference
    under the node for SipMessage.
*/

/*
    Callback implementation
    This callback needs to be implemented if the stack is compiled with
    the SIP_SELECTIVE_PARSE option.
    This callback is invoked each time the parser encounters an error while
    parsing a header. The application has the option of ignoring the error
    after examinig the type of the header.
    Please refer to the SIP_OPT_BADMESSAGE option for another way to ignore
    errors in messages without the use of callbacks. Documentation for this
    decode option can be found in the API reference under the node for
    SipMessage.
*/

/*=======================================================================
End of selective parsing callback implementations.
========================================================================*/

/*=======================================================================
New callback introduced in 4.0.1 for notification of retransmission
events.
========================================================================*/
#ifdef SIP_RETRANSCALLBACK
#ifdef ANSI_PROTO
#ifndef SIP_TXN_LAYER
    void                sip_indicateMessageRetransmission (SipEventContext *
                                                           pContext,
                                                           SipTimerKey * pKey,
                                                           SIP_S8bit * pBuffer,
                                                           SIP_U32bit
                                                           dBufferLength,
                                                           SipTranspAddr *
                                                           pAddr,
                                                           SIP_U8bit
                                                           retransCount,
                                                           SIP_U32bit duration)
#else
    void                sip_indicateMessageRetransmission (SipEventContext *
                                                           pContext,
                                                           SipTxnKey * pKey,
                                                           SIP_S8bit * pBuffer,
                                                           SIP_U32bit
                                                           dBufferLength,
                                                           SipTranspAddr *
                                                           pAddr,
                                                           SIP_U8bit
                                                           retransCount,
                                                           SIP_U32bit duration)
#endif
#else
#ifndef SIP_TXN_LAYER
    void                sip_indicateMessageRetransmission (pContext, pKey,
                                                           pBuffer,
                                                           dBufferLength, pAddr,
                                                           retransCount,
                                                           duration)
        SipEventContext *pContext;
    SipTimerKey        *pKey;
    SIP_S8bit          *pBuffer;
    SIP_U32bit          dBufferLength;
    SipTranspAddr      *pAddr;
    SIP_U8bit           retransCount;
    SIP_U32bit          duration;
#else
    void                sip_indicateMessageRetransmission (pContext, pKey,
                                                           pBuffer,
                                                           dBufferLength, pAddr,
                                                           retransCount,
                                                           duration)
        SipEventContext *pContext;
    SipTxnKey          *pKey;
    SIP_S8bit          *pBuffer;
    SIP_U32bit          dBufferLength;
    SipTranspAddr      *pAddr;
    SIP_U8bit           retransCount;
    SIP_U32bit          duration;
#endif
#endif
    {
        (void) pContext;
        (void) pKey;
        (void) dBufferLength;
        (void) pAddr;
        printf
            ("\n-----Message Retransmission Indication Callback--------------\n");
        fflush (stdout);
        printf
            ("Inside sip_indicateMessageRetransmission demo implementation.\n");
        fflush (stdout);
        printf ("Buffer being retransmitted (truncated to 100 bytes):\n");
        fflush (stdout);
        printf ("--------------------\n");
        fflush (stdout);
        printf ("%.100s", pBuffer);
        fflush (stdout);
        printf ("\n------------------");
        fflush (stdout);
        printf ("\nRetransmission count: %d\n", retransCount);
        fflush (stdout);
        printf ("Retransmission duration: %d\n", duration);
        fflush (stdout);
        printf
            ("-----Message Retransmission Indication Callback Ends---------\n\n");
        fflush (stdout);
    }
#endif

/*
 Decrypt buffer call-back implementation.
 This function is called by the stack when it encounters an encryption
 header in the message. In a real implementation, the encryption header will
 be extracted from the message and the required decryption algorithm will be
 applied on encinbuffer. The decrypted result would be given to encoutbuffer.
 If decryption failed for some reason, SipFail would be returned.

 In this dummy implementation, we always return a constant string containing
 two headers and an SDP body. These headers should replace the headers and
 the message body of the original message.
*/
#ifndef SIP_TXN_LAYER
/* Structure definition for the user's timer implementation */
    typedef struct
    {
        SIP_S32bit          duration;
        SIP_S8bit           restart;
             
             
             
                      SipBool (*timeoutfunc) (SipTimerKey * key, SIP_Pvoid buf);
        SIP_Pvoid           buffer;
        SipTimerKey        *key;
    } TimerListElement;

    SipList             timerlist;

    void                timerElementFree (SIP_Pvoid element)
    {
        TimerListElement   *el;

        el = (TimerListElement *) element;
        sip_freeSipTimerKey (el->key);
        sip_freeSipTimerBuffer ((SipTimerBuffer *) el->buffer);
        /* free(element); */
    }
#endif

/* Implementaion of the fast_startTimer interface reqiuired by the stack
   Application developers may choose to implement this function in any
   manner while preserving the interface
*/
#ifdef ANSI_PROTO
    SipBool             fast_startTimer (SIP_U32bit duration, SIP_S8bit restart,
                                         sip_timeoutFuncPtr timeoutfunc,
                                         SIP_Pvoid buffer, SipTimerKey * key,
                                         SipError * err)
#else
    SipBool             fast_startTimer (duration, restart, timeoutfunc, buffer,
                                         key, err) SIP_U32bit duration;
    SIP_S8bit           restart;
    sip_timeoutFuncPtr  timeoutfunc;
    SIP_Pvoid           buffer;
    SipTimerKey        *key;
    SipError           *err;
#endif
    {
#ifndef SIP_TXN_LAYER
        SipError           *dummy;
        TimerListElement   *elem;
        SipError            error;
        SIP_U32bit          size;

        dummy = err;
        sip_listSizeOf (&timerlist, &size, &error);
        elem = (TimerListElement *) malloc (sizeof (TimerListElement));
        elem->duration = duration;
        elem->restart = restart;
        elem->timeoutfunc = timeoutfunc;
        elem->buffer = buffer;
        elem->key = key;

        sip_listAppend (&timerlist, elem, &error);
#else
        SIP_U32bit          dummyDuration;
        SIP_S8bit           dummyRestart;
        SIP_Pvoid           dummyBuffer;
        sip_timeoutFuncPtr  dummyTempfunc;
        SipTimerKey        *dummyTimerKey;
        SipError           *dummyError;

        dummyDuration = duration;
        dummyRestart = restart;
        dummyBuffer = buffer;
        dummyTimerKey = key;
        dummyTempfunc = timeoutfunc;
        dummyError = err;
#endif
        return SipSuccess;
    }

/* Implementaion of the fast_stopTimer interface reqiuired by the stack
   Application developers may choose to implement this function in any
   manner while preserving the interface
*/

#ifdef ANSI_PROTO
    SipBool             fast_stopTimer (SipTimerKey * inkey,
                                        SipTimerKey ** outkey,
                                        SIP_Pvoid * buffer, SipError * err)
#else
    SipBool             fast_stopTimer (inkey, outkey, buffer, err)
        SipTimerKey *inkey;
    SipTimerKey       **outkey;
    SIP_Pvoid          *buffer;
    SipError           *err;
#endif

    {
#ifndef SIP_TXN_LAYER
        TimerListElement   *elem;
        SipError            error;
        SIP_U32bit          i;
        SIP_U32bit          size;

        sip_listSizeOf (&timerlist, &size, &error);
        for (i = 0; i < size; i++)
        {
            sip_listGetAt (&timerlist, (SIP_U32bit) i, (SIP_Pvoid *) & elem,
                           &error);
            if (sip_compareTimerKeys (inkey, elem->key, err) == SipSuccess)
            {
                *outkey = elem->key;
                *buffer = elem->buffer;
                sip_listDeleteAt (&timerlist, (SIP_U32bit) i, &error);
                return SipSuccess;
            }
        }
#else
        SipTimerKey        *dummyInkey, *dummyOutkey;
        SIP_Pvoid          *dummyBuffer;
        SipError           *dummyErr;

        dummyInkey = inkey;
        dummyOutkey = *outkey;
        dummyBuffer = buffer;
        dummyErr = err;

#endif

        return SipFail;
    }

#ifndef SIP_TXN_LAYER
/* Function to get the minimum timout value from the timer list */
    SIP_S32bit          getMinimumTimeout (SipList * list)
    {
        TimerListElement   *elem;
        SipError            error;
        SIP_U32bit          size, i;
        SIP_S32bit          minval;
        SipList            *dummy;
        dummy = list;

        minval = 1000;
        sip_listSizeOf (&timerlist, &size, &error);
        for (i = 0; i < size; i++)
        {
            sip_listGetAt (&timerlist, (SIP_U32bit) i, (SIP_Pvoid *) & elem,
                           &error);
            if (elem->duration < minval)
                minval = elem->duration;
        }
        return minval;
    }
#endif
/* Implementaion of the sendToNetwork interface reqiuired by the stack
   This implementation does not respect the transptype parameter
   The buffer is dispatched via UDP irrespective of the transptype value
   Application developers may choose to implement this function in any
   manner while preserving the interface
*/

    SipBool             sip_sendToNetwork
#ifdef ANSI_PROTO
     
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        (SIP_S8bit * buffer, SIP_U32bit buflen, SipTranspAddr * addr,
         SIP_S8bit transptype, SipError * err)
#else
     
        
        
        
        
                      (buffer, buflen, addr, transptype, err) SIP_S8bit *buffer;
    SIP_U32bit          buflen;
    SipTranspAddr      *addr;
    SIP_S8bit           transptype;
    SipError           *err;
#endif
    {
        int                 sockfd;

        int                 len;

        struct sockaddr_in  cli_addr, serv_addr;
        SIP_S8bit           dummy;
        SipError           *err_dummy;

        dummy = transptype;
        err_dummy = err;

        bzero ((char *) &serv_addr, sizeof (serv_addr));
        serv_addr.sin_family = AF_INET;
        serv_addr.sin_addr.s_addr = inet_addr (addr->dIpv4);
        serv_addr.sin_port = htons (addr->dPort);

        if ((transptype & SIP_UDP) == SIP_UDP)
        {
            if ((sockfd = socket (AF_INET, SOCK_DGRAM, 0)) < 0)
            {
                printf ("Client : Could not open datagram socket.\n");
                fflush (stdout);
                close (sockfd);
                exit (0);
            }
        }
        else if ((transptype & SIP_TCP) == SIP_TCP)
        {
            if ((sockfd = socket (AF_INET, SOCK_STREAM, 0)) < 0)
            {
                printf ("Client : Could not open stream socket.\n");
                fflush (stdout);
                close (sockfd);
                exit (0);
            }
        }
        else
        {
            sockfd = 0;
            printf ("No Transport Type specified\n");
            fflush (stdout);
            exit (0);
        }

        bzero ((char *) &cli_addr, sizeof (cli_addr));    /* zero out */
        cli_addr.sin_family = AF_INET;
        cli_addr.sin_addr.s_addr = htonl (INADDR_ANY);
        cli_addr.sin_port = htons (0);

        if (bind (sockfd, (struct sockaddr *) &cli_addr, sizeof (cli_addr)) < 0)
        {
            printf ("Client : Could not bind to local address.\n");
            fflush (stdout);
            close (sockfd);
            exit (0);
        }
        if ((transptype & SIP_UDP) == SIP_UDP)
        {
            len = sendto (sockfd, buffer, buflen, 0, (struct sockaddr *)
                          &serv_addr, sizeof (serv_addr));
            if (len == -1 || (unsigned int) len != buflen)
            {
                printf ("Client : send to failed.\n");
                fflush (stdout);
#ifdef SIP_WINDOWS
                closesocket (sockfd);
#else
                close (sockfd);
#endif
                return SipFail;
            }
        }
        else if ((transptype & SIP_TCP) == SIP_TCP)
        {
            if (connect (sockfd, (struct sockaddr *) &serv_addr,
                         sizeof (serv_addr)) < 0)
            {
                printf ("Client : connect failed.\n");
                fflush (stdout);
                close (sockfd);
                return SipFail;
            }
            if (write (sockfd, buffer, buflen) != (SIP_S32bit) buflen)
            {
                printf ("Client : send to failed.\n");
                fflush (stdout);
                close (sockfd);
                return SipFail;
            }
        }
        close (sockfd);

        return SipSuccess;
    }
/*Fucntion called inside displayParsedMessage () to check the parsing of im-url structure BY_REF case */
#ifdef SIP_IMURL
    SipBool             do_stuff_imurl_byref (SipMessage * s)
    {
        SipHeader           hdr;
        SipAddrSpec        *addrspec;
        ImUrl              *pImUrl;
        SipError            err;
/* handling for from header */
        if (sip_getHeader (s, SipHdrTypeFrom, &hdr, &err) == SipFail)
        {
            if (err != E_NO_EXIST)
            {
                printf
                    ("##IM-URL:##Error in Accessing From header; Error no: %d\n",
                     err);
                fflush (stdout);
                return SipFail;
            }
        }
        else
        {
            addrspec = ((SipFromHeader *) (hdr.pHeader))->pAddrSpec;
            HSS_LOCKEDINCREF (addrspec->dRefCount);
            if (addrspec == SIP_NULL)
            {
                printf
                    ("##IM-URL:##Error in accessing AddrSpec from FROM Header; ERRNO::%d\n",
                     err);
                fflush (stdout);
                sip_freeSipFromHeader ((SipFromHeader *) (hdr.pHeader));
                return SipFail;
            }
            if (SipSuccess == sip_isImUrl (addrspec, &err))
            {
                if (sip_getImUrlFromAddrSpec (addrspec, &pImUrl, &err) ==
                    SipFail)
                {
                    printf
                        ("##IM-URL:##Error in accessing ImUrl from AddrSpec in FROMHeader; ERRNO::%d\n",
                         err);
                    fflush (stdout);
                    sip_freeSipAddrSpec (addrspec);
                    sip_freeSipFromHeader ((SipFromHeader *) (hdr.pHeader));
                    return SipFail;
                }
                else
                {
                    printf
                        ("##IM-URL:##Retrieved IM URL from AddrSpec in FROM Header\n");
                    fflush (stdout);
                    if (sip_setImUrlInAddrSpec (addrspec, pImUrl, &err) ==
                        SipFail)
                    {
                        printf
                            ("##IM-URL:##Error in setting the TIM URL in AddrSpec in FROM Header; ERRNO::%d\n",
                             err);
                        fflush (stdout);
                        sip_freeImUrl (pImUrl);
                        sip_freeSipAddrSpec (addrspec);
                        sip_freeSipFromHeader ((SipFromHeader *) (hdr.pHeader));
                        return SipFail;

                    }
                    else
                    {
                        printf
                            ("##IM-URL:##Set the IM URL in AddrSpec in FROM Header\n");
                        fflush (stdout);
                        sip_freeImUrl (pImUrl);
                        sip_freeSipAddrSpec (addrspec);
                        sip_freeSipFromHeader ((SipFromHeader *) (hdr.pHeader));
                    }
                }
                printf ("##IM-URL:##Parsed IM URL successfully\n");
                fflush (stdout);
            }
            else
            {
                sip_freeSipAddrSpec (addrspec);
                sip_freeSipFromHeader ((SipFromHeader *) (hdr.pHeader));
            }
        }
        return SipSuccess;
    }
#endif

    void                headerListFreeFunction (void *pData);
    void                headerListFreeFunction (void *pData)
    {
        sip_freeSipHeader ((SipHeader *) pData);
#ifdef SIP_BY_REFERENCE
        fast_memfree (0, pData, SIP_NULL);
#endif
    }

/* Function called inside all callbacks to print out
   the message received. The message is available in the decoded form
   to the callbacks. This function converts the structure back into
   a text message and prints it out.
*/
#ifdef SIP_TEL
/*Fucntion called inside displayParsedMessage () to check the parsing of tel url structure*/

    SipBool             do_stuff_tel (SipMessage * s)
    {
        SipHeader           hdr;
        SipAddrSpec        *addrspec;
        TelUrl             *telUrl;
        SipError            err;
/* handling for from header */
        if (sip_getHeader (s, SipHdrTypeFrom, &hdr, &err) == SipFail)
        {
            if (err != E_NO_EXIST)
            {
                printf
                    ("##TEL:##Error in Accessing From header; Error no: %d\n",
                     err);
                fflush (stdout);
                return SipFail;
            }
        }
        else
        {
            addrspec = ((SipFromHeader *) (hdr.pHeader))->pAddrSpec;
            HSS_LOCKEDINCREF (addrspec->dRefCount);
            if (addrspec == SIP_NULL)
            {
                printf
                    ("##TEL:##Error in accessing AddrSpec from FROM Header; ERRNO::%d\n",
                     err);
                fflush (stdout);
                sip_freeSipFromHeader ((SipFromHeader *) (hdr.pHeader));
                return SipFail;
            }
            if (SipSuccess == sip_isTelUrl (addrspec, &err))
            {
                if (sip_getTelUrlFromAddrSpec (addrspec, &telUrl, &err) ==
                    SipFail)
                {
                    printf
                        ("##TEL:##Error in accessing TelUrl from AddrSpec in FROMHeader; ERRNO::%d\n",
                         err);
                    fflush (stdout);
                    sip_freeSipAddrSpec (addrspec);
                    sip_freeSipFromHeader ((SipFromHeader *) (hdr.pHeader));
                    return SipFail;
                }
                else
                {
                    printf
                        ("##TEL:##Retrieved TEL URL from AddrSpec in FROM Header\n");
                    fflush (stdout);
                    if (sip_setTelUrlInAddrSpec (addrspec, telUrl, &err) ==
                        SipFail)
                    {
                        printf
                            ("##TEL:##Error in setting the TEL URL in AddrSpec in FROM Header; ERRNO::%d\n",
                             err);
                        fflush (stdout);
                        sip_freeTelUrl (telUrl);
                        sip_freeSipAddrSpec (addrspec);
                        sip_freeSipFromHeader ((SipFromHeader *) (hdr.pHeader));
                        return SipFail;

                    }
                    else
                    {
                        printf
                            ("##TEL:##Set the TEL URL in AddrSpec in FROM Header\n");
                        fflush (stdout);
                        sip_freeTelUrl (telUrl);
                        sip_freeSipAddrSpec (addrspec);
                        sip_freeSipFromHeader ((SipFromHeader *) (hdr.pHeader));
                    }
                }
                printf ("##TEL:##Parsed Tel Url successfully\n");
                fflush (stdout);
            }
            else
            {
                sip_freeSipAddrSpec (addrspec);
                sip_freeSipFromHeader ((SipFromHeader *) (hdr.pHeader));
            }
        }
        return SipSuccess;
    }
#endif

/* Parsing IM-URL  */

/* end of parsing IM-URL */

    void                displayParsedMessage (SipMessage * s)
    {
        char               *out;
        char                fullForm;
        SipError            err;
        SipBool             res;
        SIP_U32bit          length;
        SipOptions          options;
#ifdef SIP_TEL
        SipBool             restel;
#endif

#ifdef SIP_IMURL
        SipBool             resimurl;
#endif

        out = (char *) malloc (64000);
        if (!out)
        {
            printf ("Memory allocation error\n");
            exit (0);
        }

        options.dOption = SIP_OPT_SINGLE | SIP_OPT_AUTHCANONICAL;
        if (glbSipOption == 1)
        {
            options.dOption = 0;
            fflush (stdin);
            printf
                ("\n Use short form/full form for headers in formed message ?(s/f):[f]");
            fflush (stdout);
            fullForm = sip_getchar ();
            if ((fullForm == 's') || (fullForm == 'S'))
                options.dOption |= SIP_OPT_SHORTFORM;
            else
                options.dOption |= SIP_OPT_FULLFORM;
            fflush (stdin);
            printf ("\n Use comma separated headers ?(y/n): [n] ");
            fflush (stdout);
            fullForm = sip_getchar ();
            if ((fullForm == 'y') || (fullForm == 'Y'))
                options.dOption |= SIP_OPT_COMMASEPARATED;
            else
                options.dOption |= SIP_OPT_SINGLE;
            fflush (stdin);
            printf ("\n Enable content-length correction ?(y/n): [y] ");
            fflush (stdout);
            fullForm = sip_getchar ();
            if ((fullForm == 'n') || (fullForm == 'N'))
            {
            }
            else
                options.dOption |= SIP_OPT_CLEN;
        }
        glbSipOption = 0;
#ifdef SIP_TEL
        /*testing of the tel-url structure */
        printf ("*****Testing The TEL-URL BY REFERNCE MODE*****\n");
        fflush (stdout);
        restel = do_stuff_tel (s);
        if (restel != SipSuccess)
        {
            printf ("##TEL:##Error in parsing Tel Url\n");
            fflush (stdout);
            free (out);
            sip_freeSipMessage (s);
            return;
        }
#endif
#ifdef SIP_IMURL
        /*testing of the im-url structure */
        printf ("*****Testing The IM-URL BY REFERNCE MODE*****\n");
        fflush (stdout);
        resimurl = do_stuff_imurl_byref (s);
        if (resimurl != SipSuccess)
        {
            printf ("##IM-URL:##Error in parsing IM-URL\n");
            fflush (stdout);
            free (out);
            sip_freeSipMessage (s);
            return;
        }
#endif

        res = sip_formMessage (s, &options, out, &length, &err);
        if (res != SipSuccess)
        {
            printf ("\nError in SipFormMessage: %d\n", err);
            free (out);
            sip_freeSipMessage (s);
            return;
        }
        if (showMessage)
            printf ("\n|==Decoded Message====================================="
                    "========================|\n\n");

#if defined(SIP_VXWORKS) || defined(SIP_OSE)
        out[length] = '\0';
        printf ("%s", out);
        fflush (stdout);

#endif
#if defined(SIP_SOLARIS) || defined(SIP_LINUX)
        write (2, out, length);
#endif
        printf ("\n|==Decoded Message Ends================================"
                "========================|\n");

    /*=========================================================*/
        /* Parsing unrecognized header types using grammars of     */
        /* recognized type                                         */
        /*                                                         */
        /* This example shows Diversion header parsing.            */
        /* The SIP stack does not support the Diversion header     */
        /* defined in draft-levy-sip-diversion-01.                 */
        /* Since the Diversion header grammar is similar to the    */
        /* Route header grammar, the new stack API for parsing     */
        /* unknown headers can be used as shown below to parse     */
        /* and form this header with ease.                         */
    /*=========================================================*/

        free (out);
        sip_freeSipMessage (s);
/*    fast_memfree(0,s,SIP_NULL);*/
    }

/* These are the callbacks which which the stack calls when
   it successfully decodes a SIP message */
#ifdef SIP_TXN_LAYER
    void                sip_freeTimerHandle (SIP_Pvoid pTimerHandle)
    {
        /*Making this a dummy function since the actual freeing of the
         * timer handle is being handled thru the application itself.
         */
        (void) pTimerHandle;
    }

#ifdef ANSI_PROTO
    void                sip_indicateTimeOut (SipEventContext * context,
                                             en_SipTimerType dTimer)
#else
    void                sip_indicateTimeOut (context, dTimer)
        SipEventContext *context;
    en_SipTimerType     dTimer;
#endif
#else
#ifdef ANSI_PROTO
    void                sip_indicateTimeOut (SipEventContext * context)
#else
    void                sip_indicateTimeOut (context) SipEventContext *context;
#endif
#endif
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside retransmission timeout callback------\n");
        if (context->pData)
            printf ("%s timed out.\n", (char *) context->pData);
#ifdef SIP_TXN_LAYER
        switch (dTimer)
        {
            case SipTimerA_B:
                printf ("***TimerB has been TimedOut******\n");
                fflush (stdout);
                break;
            case SipTimerB:
                printf ("***TimerB has been TimedOut******\n");
                fflush (stdout);
                break;
            case SipTimerC:
                printf ("***TimerC has been TimedOut******\n");
                fflush (stdout);
                break;
            case SipTimerD:
                printf ("***TimerD has been TimedOut******\n");
                fflush (stdout);
                break;
            case SipTimerE_F:
                printf ("***TimerF has been TimedOut******\n");
                fflush (stdout);
                break;
            case SipTimerF:
                printf ("***TimerF has been TimedOut******\n");
                fflush (stdout);
                break;
            case SipTimerG_H:
                printf ("***TimerH has been TimedOut******\n");
                fflush (stdout);
                break;
            case SipTimerH:
                printf ("***TimerH has been TimedOut******\n");
                fflush (stdout);
                break;
            case SipTimerI:
                printf ("***TimerI has been TimedOut******\n");
                fflush (stdout);
                break;
            case SipTimerJ:
                printf ("***TimerJ has been TimedOut******\n");
                fflush (stdout);
                break;
            case SipTimerK:
                printf ("***TimerK has been TimedOut******\n");
                fflush (stdout);
                break;
            default:
                printf ("***Unknown Timer******\n");
                fflush (stdout);
        }
#endif
        sip_freeEventContext (context);
    }

#ifndef SIP_NO_CALLBACK
    void                sip_indicateInvite (SipMessage * s,
                                            SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;

        printf ("Inside INVITE callback------\n");

        /* Following code demonstrates accessor API usage to access elements
           from the Expires header if one is found in the INVITE message */
        /* Check if the message has an Expires header */

        displayParsedMessage (s);
    }

    void                sip_indicateComet (SipMessage * s,
                                           SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside COMET callback----\n");
        displayParsedMessage (s);
    }

    void                sip_indicateRegister (SipMessage * s,
                                              SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside REGISTER callback----\n");
        displayParsedMessage (s);
    }

    void                sip_indicateCancel (SipMessage * s,
                                            SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside CANCEL callback------\n");
        displayParsedMessage (s);
    }

    void                sip_indicateOptions (SipMessage * s,
                                             SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside OPTIONS callback-----\n");
        displayParsedMessage (s);
    }

    void                sip_indicateBye (SipMessage * s,
                                         SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside BYE callback---------\n");
        displayParsedMessage (s);
    }

    void                sip_indicateRefer (SipMessage * s,
                                           SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside REFER callback---------\n");
        displayParsedMessage (s);
    }

    void                sip_indicateAck (SipMessage * s,
                                         SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside ACK callback---------\n");
        displayParsedMessage (s);
    }

    void                sip_indicateInfo (SipMessage * s,
                                          SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside INFO callback---------\n");
        displayParsedMessage (s);
    }

    void                sip_indicatePropose (SipMessage * s,
                                             SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside PROPOSE callback---------\n");
        displayParsedMessage (s);
    }

    void                sip_indicatePrack (SipMessage * s,
                                           SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside PRACK callback---------\n");
        displayParsedMessage (s);
    }

#ifdef SIP_IMPP
    void                sip_indicateSubscribe (SipMessage * s,
                                               SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside SUBSCRIBE callback---------\n");
        displayParsedMessage (s);
    }

    void                sip_indicateNotify (SipMessage * s,
                                            SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside NOTIFY callback---------\n");
        displayParsedMessage (s);
    }

    void                sip_indicateMessage (SipMessage * s,
                                             SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside MESSAGE callback---------\n");
        displayParsedMessage (s);
    }
#endif

    void                sip_indicateUpdate (SipMessage * s,
                                            SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside UPDATE callback---------\n");
        displayParsedMessage (s);
    }

    void                sip_indicateUnknownRequest (SipMessage * s,
                                                    SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside UNKNOWN REQUEST callback\n");
        displayParsedMessage (s);
    }

    void                sip_indicateInformational (SipMessage * s,
                                                   SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside Informational callback\n");
        displayParsedMessage (s);
    }

    void                sip_indicateFinalResponse (SipMessage * s,
                                                   SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside Final callback-------\n");
        displayParsedMessage (s);
    }

/* of SIP_NO_CALLBACK check */
#endif

/* End of callback implementations */

/* Function used to send simple requests */
    SipBool             sendResponse (int code, const char *reason,
                                      const char *method,
                                      SipTranspAddr * sendaddr, SipError * err)
    {
        SipMessage         *sipmesg;
        SipStatusLine      *statline;
        SipHeader          *header;
        SipAddrSpec        *addrspec, *toaddr, *conaddr;
        SipUrl             *sipurl, *tosipurl, *consipurl;
        SipParam           *viaParam;
        char               *pFromTag, *pToTag;
        SipBool             res;
        SipEventContext    *context;
        char                contextstr[100];
        SipOptions          options;
#ifndef SIP_TXN_LAYER
        int                 T1, T2;
        char                retrans;
#else
        SipTranspAddr      *pTempTranspAddr = SIP_NULL;
#endif

        if (sip_initSipMessage (&sipmesg, SipMessageResponse, err) == SipFail)
        {
            printf ("Could not form message.\n");
            fflush (stdout);
            exit (0);
        }
        sip_initSipStatusLine (&statline, err);

        statline->pStr1 = sip_strdup (reason, APP_MEM_ID);
        statline->pStr2 = sip_strdup ("SIP/2.0", APP_MEM_ID);
        statline->dIntVar1 = (SIP_U16bit) code;
        sip_freeSipStatusLine (sipmesg->u.pResponse->pStatusLine);
        /*sip_setStatusLineInSipRespMsg (sipmesg, statline, err); */
        sipmesg->u.pResponse->pStatusLine = statline;
        HSS_LOCKEDINCREF (statline->dRefCount);

        sip_freeSipStatusLine (statline);

        /*Creation and setting of from header */
        sip_initSipHeader (&header, SipHdrTypeFrom, err);
        sip_initSipAddrSpec (&addrspec, SipAddrSipUri, err);
        sip_initSipUrl (&sipurl, err);
        sipurl->pHost = sip_strdup ("mydomain.com", APP_MEM_ID);
        addrspec->u.pSipUrl = sipurl;
        addrspec->dType = SipAddrSipUri;
        ((SipFromHeader *) (header->pHeader))->pAddrSpec = addrspec;
        pFromTag = sip_strdup ("fromtag1947", APP_MEM_ID);
        sip_setTagInFromHdr (header, pFromTag, err);
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
        /*Creation and setting of from header ends here */

/*Creation and setting of To header */

        sip_initSipHeader (&header, SipHdrTypeTo, err);
        sip_initSipAddrSpec (&toaddr, SipAddrSipUri, err);
        sip_initSipUrl (&tosipurl, err);
        tosipurl->pHost = sip_strdup ("yourdomain.com", APP_MEM_ID);
        toaddr->u.pSipUrl = tosipurl;
        toaddr->dType = SipAddrSipUri;
        ((SipFromHeader *) (header->pHeader))->pAddrSpec = toaddr;
        pToTag = sip_strdup ("totag2002", APP_MEM_ID);
        sip_setTagInToHdr (header, pToTag, err);
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
/*Creation and setting of To header ends here */

/*Creation of Callid Header */

        sip_initSipHeader (&header, SipHdrTypeCallId, err);
        ((SipCallIdHeader *) (header->pHeader))->pStr1 =
            sip_strdup ("1234324453@mydomain.com", APP_MEM_ID);
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
/*Creation and setting of Callid header ends here */

/*Creation of CSeq Header */
        sip_initSipHeader (&header, SipHdrTypeCseq, err);
        if (strcmp (method, "BYE") == 0 || (strcmp (method, "PRACK") == 0))
            ((SipCseqHeader *) (header->pHeader))->dIntVar1 = 2;
        else
            ((SipCseqHeader *) (header->pHeader))->dIntVar1 = 1;
        ((SipCseqHeader *) (header->pHeader))->pStr1 =
            sip_strdup (method, APP_MEM_ID);
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
/*Creation and setting of CSeq header ends here */
/*
 * Via Header Creation
 */

        sip_initSipHeader (&header, SipHdrTypeVia, err);

        if ((glbtransptype & SIP_UDP) == SIP_UDP)
        {

            ((SipViaHeader *) (header->pHeader))->pStr1 =
                sip_strdup ("SIP/2.0/UDP", APP_MEM_ID);
        }
        else
        {
            ((SipViaHeader *) (header->pHeader))->pStr1 =
                sip_strdup ("SIP/2.0/TCP", APP_MEM_ID);

        }
        ((SipViaHeader *) (header->pHeader))->pStr2 =
            sip_strdup ("135.180.130.133", APP_MEM_ID);

        sip_initSipParam (&viaParam, err);
        viaParam->pName = sip_strdup ("branch", APP_MEM_ID);
        sip_listAppend (&(viaParam->slValue),
                        sip_strdup ("Aricent", APP_MEM_ID), err);

        sip_listAppend (&(((SipViaHeader *) (header->pHeader))->slParam),
                        viaParam, err);
        sip_insertHeaderAtIndex (sipmesg, header, 0, err);
        sip_freeSipHeader (header);
        free (header);

/*Via Header Ends */
/*Contact Header */

        sip_initSipHeader (&header, SipHdrTypeContactNormal, err);
        sip_initSipAddrSpec (&conaddr, SipAddrSipUri, err);
        sip_initSipUrl (&consipurl, err);
        consipurl->pHost = sip_strdup ("yourdomain.com", APP_MEM_ID);
        conaddr->u.pSipUrl = consipurl;
        conaddr->dType = SipAddrSipUri;
        ((SipContactHeader *) (header->pHeader))->pAddrSpec = conaddr;
        ((SipContactHeader *) (header->pHeader))->dIntVar1 = SipContactNormal;
        sip_insertHeaderAtIndex (sipmesg, header, 0, err);
        sip_freeSipHeader (header);
        free (header);

/*Contact Header Ends*/

/*Content Length */
        sip_initSipHeader (&header, SipHdrTypeContentLength, err);
        ((SipContentLengthHeader *) (header->pHeader))->dIntVar1 = 0;
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);

        if ((strcmp (method, "INVITE") == 0) && ((code < 200) && (code != 100)))
        {
#ifdef SIP_RPR
            sip_initSipHeader (&header, SipHdrTypeRSeq, err);
            /*sip_rpr_setRespNumInRSeqHdr(header,1,err); */
            ((SipRseqHeader *) (header->pHeader))->dIntVar1 = 1;
            sip_setHeader (sipmesg, header, err);
            sip_freeSipHeader (header);
            free (header);
#endif
        }
        context = (SipEventContext *) malloc (sizeof (SipEventContext));
        sprintf (contextstr, "Response with code %d ", code);
        context->pData = sip_strdup (contextstr, APP_MEM_ID);
        context->pDirectBuffer = SIP_NULL;
        options.dOption = SIP_OPT_FULLFORM | SIP_OPT_SINGLE |
            SIP_OPT_RETRANSCALLBACK;
#ifndef SIP_TXN_LAYER
        if (glbSipOption == 1)
        {
            printf
                ("\n Do you want customise the retransmission value(y/n):[n]");
            fflush (stdout);
            fflush (stdin);
            retrans = sip_getchar ();
            if ((retrans == 'y') || (retrans == 'Y'))
            {
                char                value[10];
                options.dOption |= SIP_OPT_PERMSGRETRANS;
                fflush (stdout);
                printf ("\n enter value of T1:");
                fflush (stdin);
                fflush (stdout);
                scanf ("%s", value);
                T1 = atoi (value);
                fflush (stdin);
                fflush (stdout);
                printf ("\n enter value of T2:");
                fflush (stdin);
                fflush (stdout);
                scanf ("%s", value);
                T2 = atoi (value);

                context->dRetransT1 = T1;
                context->dRetransT2 = T2;
            }
        }
        glbSipOption = 0;
#endif

/*    Uncomment to test per message retransmission interval setting.
    context->dRetransT1 = 2000;
    context->dRetransT2 = 8000;
    options.dOption = SIP_OPT_FULLFORM | SIP_OPT_SINGLE | SIP_OPT_PERMSGRETRANS;
*/

/*    Uncomment to test per message retransmission interval setting with
     retranscount
    context->dRetransT1 = 2000;
    context->dRetransT2 = 8000;
    context->dMaxRetransCount = 4;
    context->dMaxInviteRetransCount = 2;
    options.dOption = SIP_OPT_FULLFORM | SIP_OPT_SINGLE | SIP_OPT_PERMSGRETRANS\
                        SIP_OPT_PERMSGRETRANSCOUNT;
*/
/* Uncomment to test for sending a buffer directly into the sendmessage
{
    char* sendbuffer;
    int dLength;
    sendbuffer= (char*) malloc(SIP_MAX_MSG_SIZE);
    if(sendbuffer== SIP_NULL)
            return SipFail;
    if(sip_formMessage(sipmesg, &options, sendbuffer, &dLength, err) == SipFail)
    {
            free(sendbuffer);
            return SipFail;
    }
    options.dOption |= SIP_OPT_DIRECTBUFFER;
    context->pDirectBuffer= (SIP_Pvoid) sendbuffer;
*/

#ifdef SIP_TXN_LAYER
        {
            SipTxnContext      *txncontext;
            SipTxnKey          *pTxnKey = SIP_NULL;
            en_SipTxnBypass     dbypass;
            sip_txn_initSipTxnContext (&txncontext, err);
            txncontext->pEventContext = context;
            txncontext->txnOption.dOption = options;
            dbypass = SipTxnNoByPass;

            if (!glbtransactiontype)
                txncontext->dTxnType = SipUATypeTxn;
            else
                txncontext->dTxnType = SipProxyTypeTxn;

            txncontext->txnOption.dTimeoutCbkOption = glbTimeTimeoutOption;

            if (glbsetTimerValue)
            {
                txncontext->timeoutValues.dT1 = dTimeOut.dT1;
                txncontext->timeoutValues.dT2 = dTimeOut.dT2;
                txncontext->timeoutValues.dTimerB = dTimeOut.dTimerB;
                txncontext->timeoutValues.dTimerC = dTimeOut.dTimerC;
                txncontext->timeoutValues.dTimerD_T3 = dTimeOut.dTimerD_T3;
                txncontext->timeoutValues.dTimerF_T3 = dTimeOut.dTimerF_T3;
                txncontext->timeoutValues.dTimerH = dTimeOut.dTimerH;
                txncontext->timeoutValues.dTimerI_T4 = dTimeOut.dTimerI_T4;
                txncontext->timeoutValues.dTimerJ_T3 = dTimeOut.dTimerJ_T3;
                txncontext->timeoutValues.dTimerK_T4 = dTimeOut.dTimerK_T4;
                txncontext->txnOption.dTimerOption = SIP_OPT_TIMERALL;
            }
            pTempTranspAddr = (SipTranspAddr *) fast_memget
                (0, sizeof (SipTranspAddr), err);
            *pTempTranspAddr = *sendaddr;

            res = sip_txn_sendMessage (sipmesg, pTempTranspAddr, glbtransptype,
                                       txncontext, dbypass, &pTxnKey, err);

            if (res == SipFail)
            {
                sip_freeEventContext (context);
                switch (*err)
                {
                    case E_TXN_NO_EXIST:
                        printf ("** ERROR : The Txn for this message"
                                "doesn't exist**\n");
                        fflush (stdout);
                        break;
                    case E_TXN_EXISTS:
                        printf ("** ERROR : The Txn for this message"
                                "already exists**\n");
                        fflush (stdout);
                        break;
                    case E_TXN_INV_STATE:
                        printf ("** ERROR : This message leads to"
                                "an invalid transaction state**\n");
                        fflush (stdout);
                        break;
                    case E_TXN_INV_MSG:
                        printf ("** ERROR : This is an invalid message"
                                "for received for the Txn**\n");
                        fflush (stdout);
                        break;
                    default:
                        break;
                }
                printf ("** ERROR : Send Message Failed**\n");
                fflush (stdout);
            }
            else
            {
                sip_txn_freeTxnKey (pTxnKey, err);
            }
            free (txncontext);
        }
#else
        res =
            sip_sendMessage (sipmesg, &options, sendaddr, glbtransptype,
                             context, err);
#endif
        sip_freeSipMessage (sipmesg);

/* Uncomment to test for sending a buffer directly into the sendmessage/
}
*/
        if (res == SipSuccess)
        {
            printf ("Response with code %d sent successfully.\n", code);
            fflush (stdout);
        }
        return SipSuccess;
    }

/* Function used to send a simple response */
    SipBool             sendRequest (const char *method,
                                     SipTranspAddr * sendaddr, SipError * err)
    {
        SipMessage         *sipmesg;
        SipReqLine         *reqline;
        SipAddrSpec        *addrspec, *toaddr, *conaddr;
        SipUrl             *sipurl, *tosipurl, *consipurl;
        SipHeader          *header;
        SipParam           *viaParam;
        SipBool             res;
        SipEventContext    *context;
        char               *pFromTag, *pToTag;

        char                contextstr[100];
        SipOptions          options;
        char                unknownMethod[100];
#ifndef SIP_TXN_LAYER
        int                 T1, T2;
        char                retrans;
#else
        SipTranspAddr      *pTempTranspAddr = SIP_NULL;
#endif

        if (sip_strcasecmp (method, "UNKNOWN") == 0)
        {
            printf ("Method To be Sent:");
            scanf ("%s", unknownMethod);
            fflush (stdin);
            fflush (stdout);
        }

        if (sip_initSipMessage (&sipmesg, SipMessageRequest, err) == SipFail)
        {
            printf ("Could not form message.\n");
            fflush (stdout);
            exit (0);
        }
        sip_initSipReqLine (&reqline, err);

        if (sip_strcasecmp (method, "UNKNOWN") == 0)
        {

            reqline->pStr1 = sip_strdup (unknownMethod, APP_MEM_ID);
        }
        else
        {
            reqline->pStr1 = sip_strdup (method, APP_MEM_ID);
        }
        reqline->pStr2 = sip_strdup ("SIP/2.0", APP_MEM_ID);

        sip_initSipAddrSpec (&addrspec, SipAddrSipUri, err);
        sip_initSipUrl (&sipurl, err);
        sipurl->pHost = sip_strdup ("mydomain.com", APP_MEM_ID);

        addrspec->u.pSipUrl = sipurl;
        addrspec->dType = SipAddrSipUri;
        reqline->pAddrSpec = addrspec;
        /*sip_setReqLineInSipReqMsg (sipmesg, reqline, err); */
        sip_freeSipReqLine (sipmesg->u.pRequest->pRequestLine);
        sipmesg->u.pRequest->pRequestLine = reqline;
        HSS_LOCKEDINCREF (reqline->dRefCount);

        sip_freeSipReqLine (reqline);

        /*Creation and setting of from header */
        sip_initSipHeader (&header, SipHdrTypeFrom, err);
        ((SipFromHeader *) (header->pHeader))->pAddrSpec = addrspec;
        HSS_LOCKEDINCREF (addrspec->dRefCount);
        pFromTag = sip_strdup ("fromtag1947", APP_MEM_ID);
        sip_setTagInFromHdr (header, pFromTag, err);
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
        /*Creation and setting of from header ends here */
/*Creation and setting of To header */

        sip_initSipHeader (&header, SipHdrTypeTo, err);
        sip_initSipAddrSpec (&toaddr, SipAddrSipUri, err);
        sip_initSipUrl (&tosipurl, err);
        tosipurl->pHost = sip_strdup ("yourdomain.com", APP_MEM_ID);
        toaddr->u.pSipUrl = tosipurl;
        toaddr->dType = SipAddrSipUri;
        ((SipFromHeader *) (header->pHeader))->pAddrSpec = toaddr;
        pToTag = sip_strdup ("totag2002", APP_MEM_ID);
        sip_setTagInFromHdr (header, pToTag, err);
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
/*Creation and setting of To header ends here */
/*Creation of Callid Header */

        sip_initSipHeader (&header, SipHdrTypeCallId, err);
        ((SipCallIdHeader *) (header->pHeader))->pStr1 =
            sip_strdup ("1234324453@mydomain.com", APP_MEM_ID);
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
/*Creation and setting of Callid header ends here */

/*Creation of CSeq Header */
        sip_initSipHeader (&header, SipHdrTypeCseq, err);
        if ((strcmp (method, "BYE") == 0) || (strcmp (method, "PRACK") == 0))
            ((SipCseqHeader *) (header->pHeader))->dIntVar1 = 2;
        else
            ((SipCseqHeader *) (header->pHeader))->dIntVar1 = 1;

        ((SipCseqHeader *) (header->pHeader))->pStr1 =
            sip_strdup (method, APP_MEM_ID);
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
/*Creation and setting of CSeq header ends here */
/*
 * Via Header Creation
 */

        sip_initSipHeader (&header, SipHdrTypeVia, err);
        if ((glbtransptype & SIP_UDP) == SIP_UDP)
        {

            ((SipViaHeader *) (header->pHeader))->pStr1 =
                sip_strdup ("SIP/2.0/UDP", APP_MEM_ID);
        }
        else
        {
            ((SipViaHeader *) (header->pHeader))->pStr1 =
                sip_strdup ("SIP/2.0/TCP", APP_MEM_ID);

        }
        ((SipViaHeader *) (header->pHeader))->pStr2 =
            sip_strdup ("135.180.130.133", APP_MEM_ID);

        sip_initSipParam (&viaParam, err);
        viaParam->pName = sip_strdup ("branch", APP_MEM_ID);
        sip_listAppend (&(viaParam->slValue),
                        sip_strdup ("Aricent", APP_MEM_ID), err);

        sip_listAppend (&(((SipViaHeader *) (header->pHeader))->slParam),
                        viaParam, err);
        sip_insertHeaderAtIndex (sipmesg, header, 0, err);
        sip_freeSipHeader (header);
        free (header);

/*Via Header Ends */

/*Contact Header */

        sip_initSipHeader (&header, SipHdrTypeContactNormal, err);
        sip_initSipAddrSpec (&conaddr, SipAddrSipUri, err);
        sip_initSipUrl (&consipurl, err);
        consipurl->pHost = sip_strdup ("yourdomain.com", APP_MEM_ID);
        conaddr->u.pSipUrl = consipurl;
        conaddr->dType = SipAddrSipUri;
        ((SipContactHeader *) (header->pHeader))->pAddrSpec = conaddr;
        ((SipContactHeader *) (header->pHeader))->dIntVar1 = SipContactNormal;
        sip_insertHeaderAtIndex (sipmesg, header, 0, err);
        sip_freeSipHeader (header);
        free (header);
/*Content Length */
        sip_initSipHeader (&header, SipHdrTypeContentLength, err);
        ((SipContentLengthHeader *) (header->pHeader))->dIntVar1 = 0;
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
#ifdef SIP_RPR
        if (strcmp (method, "PRACK") == 0)
        {
            sip_initSipHeader (&header, SipHdrTypeRAck, err);
            ((SipRackHeader *) (header->pHeader))->dIntVar1 = 1;
            ((SipRackHeader *) (header->pHeader))->dIntVar2 = 1;
            ((SipRackHeader *) (header->pHeader))->pStr1 =
                sip_strdup ("INVITE", APP_MEM_ID);
            sip_setHeader (sipmesg, header, err);
            sip_freeSipHeader (header);
            free (header);
        }
#endif
        context = (SipEventContext *) malloc (sizeof (SipEventContext));
        sprintf (contextstr, "%s request ", method);
        context->pData = sip_strdup (contextstr, APP_MEM_ID);
        context->pDirectBuffer = SIP_NULL;
        options.dOption = SIP_OPT_FULLFORM | SIP_OPT_SINGLE |
            SIP_OPT_RETRANSCALLBACK;

#ifndef SIP_TXN_LAYER
        if (glbSipOption == 1)
        {
            fflush (stdin);
            fflush (stdout);
            printf ("\n Do you want    retransmission (y/n):[y]");
            fflush (stdin);
            fflush (stdout);
            retrans = sip_getchar ();
            if ((retrans == 'n') || (retrans == 'N'))
            {
                res = sip_sendMessage (sipmesg, &options, sendaddr,
                                       (SIP_S8bit) (glbtransptype |
                                                    SIP_NORETRANS), context,
                                       err);
                sip_freeSipMessage (sipmesg);
                if (res == SipSuccess)
                {
                    printf ("%s sent successfully.\n", method);
                    fflush (stdin);
                    fflush (stdout);
                }
                glbSipOption = 0;
                return SipSuccess;
            }
            else
            {
                char                value[10];
                options.dOption |= SIP_OPT_PERMSGRETRANS;
                fflush (stdin);
                fflush (stdout);
                printf ("\n enter value of T1:");
                fflush (stdin);
                fflush (stdout);
                scanf ("%s", value);
                T1 = atoi (value);
                fflush (stdin);
                fflush (stdout);
                printf ("\n enter value of T2:");
                fflush (stdin);
                fflush (stdout);
                scanf ("%s", value);
                T2 = atoi (value);
                fflush (stdin);
                fflush (stdin);
                printf ("\n Maximum Retransmission for INVITE:");
                fflush (stdin);
                fflush (stdout);
                scanf ("%s", value);
                SIP_MAXINVRETRANS = atoi (value) - 1;
                fflush (stdin);
                fflush (stdout);
                printf ("\n Maximum Retransmission for other Request:");
                fflush (stdin);
                fflush (stdout);
                scanf ("%s", value);
                SIP_MAXRETRANS = atoi (value) - 1;
                context->dRetransT1 = T1;
                context->dRetransT2 = T2;
            }
        }
#endif

/*    Uncomment to test per message retransmission interval setting.
    context->dRetransT1 = 2000;
    context->dRetransT2 = 8000;
    options.dOption = SIP_OPT_FULLFORM | SIP_OPT_SINGLE | SIP_OPT_PERMSGRETRANS;
*/

/*    Uncomment to test per message retransmission interval setting with
    retranscount
    context->dRetransT1 = 2000;
    context->dRetransT2 = 8000;
    context->dMaxRetransCount = 4;
    context->dMaxInviteRetransCount = 2;
    options.dOption = SIP_OPT_FULLFORM | SIP_OPT_SINGLE | SIP_OPT_PERMSGRETRANS\
                        SIP_OPT_PERMSGRETRANSCOUNT;
*/

/* Uncomment to test for sending direct buffer into sendMessage
{
    char* sendbuffer;
    int dLength;
    sendbuffer= (char*) malloc(SIP_MAX_MSG_SIZE);
    if(sendbuffer== SIP_NULL)
            return SipFail;
    if(sip_formMessage(sipmesg, &options, sendbuffer, &dLength, err) == SipFail)
    {
            free(sendbuffer);
            return SipFail;
    }
    options.dOption |= SIP_OPT_DIRECTBUFFER;
    context->pDirectBuffer = (SIP_Pvoid) sendbuffer;
*/

#ifdef SIP_TXN_LAYER
        {
            SipTxnContext      *txncontext;
            SipTxnKey          *pTxnKey = SIP_NULL;
            en_SipTxnBypass     dbypass;
            sip_txn_initSipTxnContext (&txncontext, err);
            txncontext->pEventContext = context;
            txncontext->txnOption.dOption = options;

            dbypass = SipTxnNoByPass;

            if (!glbtransactiontype)
                txncontext->dTxnType = SipUATypeTxn;
            else
                txncontext->dTxnType = SipProxyTypeTxn;

            txncontext->txnOption.dTimeoutCbkOption = glbTimeTimeoutOption;

            if (glbsetTimerValue)
            {
                txncontext->timeoutValues.dT1 = dTimeOut.dT1;
                txncontext->timeoutValues.dT2 = dTimeOut.dT2;
                txncontext->timeoutValues.dTimerB = dTimeOut.dTimerB;
                txncontext->timeoutValues.dTimerC = dTimeOut.dTimerC;
                txncontext->timeoutValues.dTimerD_T3 = dTimeOut.dTimerD_T3;
                txncontext->timeoutValues.dTimerF_T3 = dTimeOut.dTimerF_T3;
                txncontext->timeoutValues.dTimerH = dTimeOut.dTimerH;
                txncontext->timeoutValues.dTimerI_T4 = dTimeOut.dTimerI_T4;
                txncontext->timeoutValues.dTimerJ_T3 = dTimeOut.dTimerJ_T3;
                txncontext->timeoutValues.dTimerK_T4 = dTimeOut.dTimerK_T4;
                txncontext->txnOption.dTimerOption = SIP_OPT_TIMERALL;
            }
            pTempTranspAddr = (SipTranspAddr *) fast_memget
                (0, sizeof (SipTranspAddr), err);
            *pTempTranspAddr = *sendaddr;

            res = sip_txn_sendMessage (sipmesg, pTempTranspAddr, glbtransptype,
                                       txncontext, dbypass, &pTxnKey, err);
            if (res == SipFail)
            {
                sip_freeEventContext (context);
                switch (*err)
                {
                    case E_TXN_NO_EXIST:
                        printf ("** ERROR : The Txn for this message"
                                "doesn't exist**\n");
                        fflush (stdout);
                        break;
                    case E_TXN_EXISTS:
                        printf ("** ERROR : The Txn for this message"
                                "already exists**\n");
                        fflush (stdout);
                        break;
                    case E_TXN_INV_STATE:
                        printf ("** ERROR : This message leads to"
                                "an invalid transaction state**\n");
                        fflush (stdout);
                        break;
                    case E_TXN_INV_MSG:
                        printf ("** ERROR : This is an invalid message"
                                "for received for the Txn**\n");
                        fflush (stdout);
                        break;
                    default:
                        break;
                }
                printf ("** ERROR : Send Message Failed**\n");
                fflush (stdout);
            }
            else
            {
                sip_txn_freeTxnKey (pTxnKey, err);
            }
            free (txncontext);
        }
#else
        res = sip_sendMessage (sipmesg, &options, sendaddr, glbtransptype,
                               context, err);
#endif
        sip_freeSipMessage (sipmesg);
/* Uncomment to test for sending direct buffer into sendMessage
}
*/
        if (res == SipSuccess)
        {
            printf ("%s sent successfully.\n", method);
            fflush (stdin);
            fflush (stdout);
        }
        glbSipOption = 0;
        return SipSuccess;
    }

#if 0
    void                showErrorInformation (SipMessage * pMessage)
    {
        SipError            err;
        SIP_U32bit          count;
#ifdef SIP_BY_REFERENCE
        SIP_U32bit          i;
#endif
        (void) i;
        (void) count;
        (void) err;
        (void) pMessage;
        printf ("\n");
        fflush (stdout);
        printf ("-----Error Information in decoded message ----------------\n");
        fflush (stdout);
#if 0
        if (sip_getIncorrectHeadersCount (pMessage, &count, &err) != SipFail)
        {
            printf ("Errors in parsing headers    : %d\n", count);
            fflush (stdout);
        }
        if (sip_getEntityErrorCount (pMessage, &count, &err) != SipFail)
        {
            printf ("Errors in parsing entity body: %d\n", count);
            fflush (stdout);
        }
#endif
#ifdef SIP_BADMESSAGE_PARSING
        if (sip_getBadHeaderCount (pMessage, &count, &err) == SipFail)
            return;
        for (i = 0; i < count; i++)
        {
            SipBadHeader       *pBadHeader;
            SIP_S8bit          *pStr;
            if (sip_getBadHeaderAtIndex (pMessage, &pBadHeader, i, &err) ==
                SipFail)
                return;
            printf ("Bad header in message.\n");
            fflush (stdout);
            if (sip_getNameFromBadHdr (pBadHeader, &pStr, &err) == SipFail)
            {
                sip_freeSipBadHeader (pBadHeader);
                return;
            }
            printf ("Name: %s\n", pStr);
            fflush (stdout);
            if (sip_getBodyFromBadHdr (pBadHeader, &pStr, &err) == SipFail)
            {
                sip_freeSipBadHeader (pBadHeader);
                return;
            }
            printf ("Body: %s\n", pStr);
            fflush (stdout);
            sip_freeSipBadHeader (pBadHeader);
        }
#endif
        printf ("----------------------------------------------------------\n");
        fflush (stdout);
    }
#endif

#ifdef SIP_TXN_LAYER
    SIP_U32bit          getTimerTimeoutOptions (void)
    {
        char                inputtype;
        SIP_U32bit          dRetVal = 0;

        fflush (stdout);
        printf
            ("\n<<The Siptest utility allows for the configuration\n of receipt"
             " of callbacks on expiry of diff timers.");
        printf ("\nBy default you receive callbacks for "
                "the B,C,D,F,H,I,J,K Timers>>");
        printf ("\nDo you want to configure the callbacks[y/n]?[n]");
        fflush (stdin);
        fflush (stdout);
        inputtype = sip_getchar ();
        fflush (stdin);
        fflush (stdout);
        if (inputtype == 'y' || inputtype == 'Y')
        {
            printf
                ("\n For the following interactive session the default option"
                 " is [n]");
            printf
                ("\n============================================================");
            printf
                ("\n Do you want to receive a callback when Timer B expires(y/n)");
            inputtype = sip_getchar ();
            if (inputtype == 'y' || inputtype == 'Y')
            {
                dRetVal |= SIP_ISSUE_CBKFOR_TIMER_B;
            }

            fflush (stdin);
            fflush (stdout);

            printf
                ("\n Do you want to receive a callback when Timer C expires(y/n)");
            inputtype = sip_getchar ();
            if (inputtype == 'y' || inputtype == 'Y')
            {
                dRetVal |= SIP_ISSUE_CBKFOR_TIMER_C;
            }

            fflush (stdin);
            fflush (stdout);
            printf
                ("\n Do you want to receive a callback when Timer D expires(y/n)");
            inputtype = sip_getchar ();
            if (inputtype == 'y' || inputtype == 'Y')
            {
                dRetVal |= SIP_ISSUE_CBKFOR_TIMER_D;
            }

            fflush (stdin);
            fflush (stdout);
            printf
                ("\n Do you want to receive a callback when Timer F expires(y/n)");
            inputtype = sip_getchar ();
            if (inputtype == 'y' || inputtype == 'Y')
            {
                dRetVal |= SIP_ISSUE_CBKFOR_TIMER_F;
            }

            fflush (stdin);
            fflush (stdout);
            printf
                ("\n Do you want to receive a callback when Timer H expires(y/n)");
            inputtype = sip_getchar ();
            if (inputtype == 'y' || inputtype == 'Y')
            {
                dRetVal |= SIP_ISSUE_CBKFOR_TIMER_H;
            }

            fflush (stdin);
            fflush (stdout);
            printf
                ("\n Do you want to receive a callback when Timer I expires(y/n)");
            inputtype = sip_getchar ();
            if (inputtype == 'y' || inputtype == 'Y')
            {
                dRetVal |= SIP_ISSUE_CBKFOR_TIMER_I;
            }

            fflush (stdin);
            fflush (stdout);
            printf
                ("\n Do you want to receive a callback when Timer J expires(y/n)");
            inputtype = sip_getchar ();
            if (inputtype == 'y' || inputtype == 'Y')
            {
                dRetVal |= SIP_ISSUE_CBKFOR_TIMER_J;
            }

            fflush (stdin);
            fflush (stdout);
            printf
                ("\n Do you want to receive a callback when Timer K expires(y/n)");
            inputtype = sip_getchar ();
            if (inputtype == 'y' || inputtype == 'Y')
            {
                dRetVal |= SIP_ISSUE_CBKFOR_TIMER_K;
            }

            printf ("\nTimer Expiry Callbacks are configured");
            printf
                ("\n===========================================================\n");

            return dRetVal;
        }
        else
            return SIP_ISSUE_CBKFOR_ALLTIMERS;
    }
#endif

    int                 main (int argc, char *argv[])
    {
        int                 sockfd;
        SIP_U32bit          stat;
        int                 n;
        SIP_U32bit          size = 0;
        socklen_t           clilen;
/*
*/

        struct sockaddr_in  serv_addr, cli_addr;
        fd_set              readfs;
        struct timeval      timeout;
        char               *message;
        SIP_S8bit          *nextmesg;
        SipError            error;
        SipBool             r;
        SIP_U32bit          mintimeout;
#ifdef SIP_INCREMENTAL_PARSING
        SIP_U32bit          j;
#endif

        SipOptions          opt;
        SipEventContext     context;
        char                inputtype;
#ifdef SIP_TXN_LAYER
        en_SipTxnDecodeResult dResult;
        SipTxnContext       txncontext;
        SipTxnKey          *pTxnKey = SIP_NULL;
#if !defined(SIP_VXWORKS) && !defined(SIP_WINDOWS)
        thread_id_t         dTimerThread;
#endif
#endif

        SipTranspAddr       sendaddr;

        if (argc < 3)
        {
            printf ("Usage:\n");
            fflush (stdout);
            printf ("%s my_port dest_address [dest_port]\n", argv[0]);
            fflush (stdout);
            exit (0);
        }

#ifndef SIP_TXN_LAYER
        sip_listInit (&timerlist, free, &error);
#endif

        size = 0;

        if (argc == 5)
        {
            if (strcmp (argv[4], "clen") == 0)
            {
                constructCLen = 1;
                showMessage = 1;
            }
            else
            {
                constructCLen = 0;
                showMessage = 0;
            }
        }
#ifdef SIP_INCREMENTAL_PARSING
        context.pList = SIP_NULL;
        if (context.pList == SIP_NULL)
            context.pList = (SipHdrTypeList *)
                fast_memget (0, sizeof (SipHdrTypeList), SIP_NULL);
        for (j = 0; j < HEADERTYPENUM; j++)
            context.pList->enable[j] = SipSuccess;

#endif
        opt.dOption = 0;
        context.pData = SIP_NULL;
        context.pDirectBuffer = SIP_NULL;

        bzero (sendaddr.dIpv4, 16);
        strncpy (sendaddr.dIpv4, argv[2], 15);
        sendaddr.pHost = SIP_NULL;

        if (argc >= 4)
            sendaddr.dPort = atoi (argv[3]);
        else
            sendaddr.dPort = 5060;

        /* Stack and trace initialization */
        sip_initStack ();

#ifdef SIP_TXN_LAYER
        sip_initTimerWheel (0, 0);
#if !defined(SIP_VXWORKS) && !defined(SIP_WINDOWS)
        pthread_create (&dTimerThread, NULL, sip_timerThread, NULL);
#endif
        sip_initHashTbl ((CompareKeyFuncPtr) sip_txn_compareTxnKeys);
#endif

        printf ("\nSip stack product id is:%s\n", (char *) (sip_getPart ()));
        fflush (stdout);
        printf ("%s", test_tool_desc);
        fflush (stdout);

        if (sip_setErrorLevel (SIP_Major | SIP_Minor | SIP_Critical, &error) ==
            SipFail)
            printf ("########## Error Disabled at compile time #####\n");

        if (sip_setTraceLevel (SIP_Brief, &error) == SipFail)
            printf ("########## Trace Disabled at compile time #####\n");

        sip_setTraceType (SIP_All, &error);
        clilen = sizeof (cli_addr);

#ifdef SIP_TXN_LAYER
        printf
            ("\n########## The Stack has been compiled with SIP_TXN_LAYER ######\n ");
        fflush (stdout);
        printf
            ("\n########## Siptest supports the transaction layer as defined in RFC 3261 ######\n ");
        fflush (stdout);
#endif
/* For Selecting the transport type  TCP/UDP */
        printf
            ("\n<<The Siptest utility can be configured to run using either TCP/UDP protocol>>\n\n");
        fflush (stdout);
        fflush (stdin);
        printf ("1 :\tUDP\n");
        printf ("2 :\tTCP\n");
        fflush (stdout);
        printf ("Select the type of transport mode(Default:1):");
        fflush (stdout);
        fflush (stdin);
        inputtype = sip_getchar ();
        if (inputtype == '2')
        {
            glbtransptype = SIP_TCP;
            printf ("TCP Selected.\n");
            fflush (stdout);
        }
        else
        {
            glbtransptype = SIP_UDP;
            printf ("UDP Selected.\n");
            fflush (stdout);
        }

#ifdef SIP_TXN_LAYER
/* For Selecting the transsaction type (UA/Proxy)*/
        printf
            ("\n<<The Siptest utility can be configured to run in either the UA mode or the Proxy mode>>\n\n");
        fflush (stdout);
        fflush (stdin);
        printf ("1 :\tUA\n");
        printf ("2 :\tProxy\n");
        fflush (stdout);
        printf ("Select the type of Transaction(Default:1):");
        fflush (stdout);
        fflush (stdin);
        fflush (stdout);
        inputtype = sip_getchar ();
        if (inputtype != '2')
        {
            glbtransactiontype = 0;
            printf ("UA Selected.\n");
            fflush (stdout);
        }
        else
        {
            glbtransactiontype = 1;
            printf ("Proxy Selected.\n");
            fflush (stdout);
        }

        /* Filling up the event context */
        txncontext.pEventContext = &context;
        txncontext.dTxnType = (en_SipTxnType) glbtransactiontype;
        txncontext.txnOption.dOption = opt;
        txncontext.txnOption.dTimerOption = 0;

        /*Query for which timeOut cbks the applicn wants to receive */
        glbTimeTimeoutOption = getTimerTimeoutOptions ();
        txncontext.txnOption.dTimeoutCbkOption = glbTimeTimeoutOption;

/* For Selecting the timer values*/
        fflush (stdout);
        printf ("\n<<The Siptest utility allows for the configuration "
                " of the various timers \n as defined in");
        printf (" RFC 3261 for the transaction layer>>");
        fflush (stdout);
        printf ("\nDo you want to configure the Timer Values(y/n):");
        fflush (stdin);
        fflush (stdout);
        inputtype = sip_getchar ();
        if (inputtype == 'y' || inputtype == 'Y')
        {
            int                 dTimerValue;
            glbsetTimerValue = 1;
            dTimeOut.dT1 = SIP_DEFAULT_T1;
            dTimeOut.dT2 = SIP_DEFAULT_T2;
            dTimeOut.dTimerB = SIP_DEFAULT_B;
            dTimeOut.dTimerC = SIP_DEFAULT_C;
            dTimeOut.dTimerD_T3 = SIP_DEFAULT_D_T3;
            dTimeOut.dTimerF_T3 = SIP_DEFAULT_F_T3;
            dTimeOut.dTimerH = SIP_DEFAULT_H;
            dTimeOut.dTimerI_T4 = SIP_DEFAULT_I_T4;
            dTimeOut.dTimerJ_T3 = SIP_DEFAULT_J_T3;
            dTimeOut.dTimerK_T4 = SIP_DEFAULT_K_T4;

            fflush (stdin);
            printf ("\nDefault Value of Timer T1:%d\n", SIP_DEFAULT_T1);
            fflush (stdout);
            printf ("Do You want to change the timer value(y/n):");
            fflush (stdout);
            inputtype = sip_getchar ();
            if (inputtype == 'y' || inputtype == 'Y')
            {
                printf ("Enter the customized value for T1:");
                fflush (stdout);
                dTimerValue = 0;
                scanf ("%d", &dTimerValue);
                if (dTimerValue)
                    dTimeOut.dT1 = dTimerValue;
            }
            fflush (stdin);

            printf ("\nDefault Value of Timer T2:%d\n", SIP_DEFAULT_T2);
            fflush (stdout);
            printf ("Do You want to change the timer value(y/n):");
            fflush (stdout);
            fflush (stdin);
            inputtype = sip_getchar ();
            if (inputtype == 'y' || inputtype == 'Y')
            {
                printf ("Enter the customized value for T2:");
                fflush (stdout);
                fflush (stdin);
                dTimerValue = 0;
                scanf ("%d", &dTimerValue);
                if (dTimerValue)
                    dTimeOut.dT2 = dTimerValue;
            }
            fflush (stdin);

            printf ("\nDefault Value of Timer B:%d\n", SIP_DEFAULT_B);
            fflush (stdout);
            printf ("Do You want to change the timer value(y/n):");
            fflush (stdout);
            inputtype = sip_getchar ();
            if (inputtype == 'y' || inputtype == 'Y')
            {
                printf ("Enter the customized value for B:");
                fflush (stdout);
                fflush (stdin);
                dTimerValue = 0;
                scanf ("%d", &dTimerValue);
                if (dTimerValue)
                    dTimeOut.dTimerB = dTimerValue;
            }
            fflush (stdin);

            printf ("\nDefault Value of Timer C:%d\n", SIP_DEFAULT_C);
            fflush (stdout);
            printf ("Do You want to change the timer value(y/n):");
            fflush (stdout);
            inputtype = sip_getchar ();
            if (inputtype == 'y' || inputtype == 'Y')
            {
                printf ("Enter the customized value for C:");
                fflush (stdout);
                fflush (stdin);
                dTimerValue = 0;
                scanf ("%d", &dTimerValue);
                if (dTimerValue)
                    dTimeOut.dTimerC = dTimerValue;
            }
            fflush (stdin);

            printf ("\nDefault Value of Timer D:%d\n", SIP_DEFAULT_D_T3);
            fflush (stdout);
            printf ("Do You want to change the timer value(y/n):");
            fflush (stdout);
            inputtype = sip_getchar ();
            if (inputtype == 'y' || inputtype == 'Y')
            {
                printf ("Enter the customized value for D:");
                fflush (stdout);
                fflush (stdin);
                dTimerValue = 0;
                scanf ("%d", &dTimerValue);
                if (dTimerValue)
                    dTimeOut.dTimerD_T3 = dTimerValue;
            }
            fflush (stdin);

            printf ("\nDefault Value of Timer F:%d\n", SIP_DEFAULT_F_T3);
            fflush (stdout);
            printf ("Do You want to change the timer value(y/n):");
            fflush (stdout);
            inputtype = sip_getchar ();
            if (inputtype == 'y' || inputtype == 'Y')
            {
                printf ("Enter the customized value for F:");
                fflush (stdout);
                fflush (stdin);
                dTimerValue = 0;
                scanf ("%d", &dTimerValue);
                if (dTimerValue)
                    dTimeOut.dTimerF_T3 = dTimerValue;
            }
            fflush (stdin);

            printf ("\nDefault Value of Timer H:%d\n", SIP_DEFAULT_H);
            fflush (stdout);
            printf ("Do You want to change the timer value(y/n):");
            fflush (stdout);
            inputtype = sip_getchar ();
            if (inputtype == 'y' || inputtype == 'Y')
            {
                printf ("Enter the customized value for H:");
                fflush (stdout);
                fflush (stdin);
                dTimerValue = 0;
                scanf ("%d", &dTimerValue);
                if (dTimerValue)
                    dTimeOut.dTimerH = dTimerValue;
            }
            fflush (stdin);

            printf ("\nDefault Value of Timer I:%d\n", SIP_DEFAULT_I_T4);
            fflush (stdout);
            printf ("Do You want to change the timer value(y/n):");
            fflush (stdout);
            inputtype = sip_getchar ();
            if (inputtype == 'y' || inputtype == 'Y')
            {
                printf ("Enter the customized value for I:");
                fflush (stdout);
                fflush (stdin);
                dTimerValue = 0;
                scanf ("%d", &dTimerValue);
                if (dTimerValue)
                    dTimeOut.dTimerI_T4 = dTimerValue;
            }
            fflush (stdin);

            printf ("\nDefault Value of Timer J:%d\n", SIP_DEFAULT_J_T3);
            fflush (stdout);
            printf ("Do You want to change the timer value(y/n):");
            fflush (stdout);
            inputtype = sip_getchar ();
            if (inputtype == 'y' || inputtype == 'Y')
            {
                printf ("Enter the customized value for J:");
                fflush (stdout);
                fflush (stdin);
                dTimerValue = 0;
                scanf ("%d", &dTimerValue);
                if (dTimerValue)
                    dTimeOut.dTimerJ_T3 = dTimerValue;
            }
            fflush (stdin);

            printf ("\nDefault Value of Timer K:%d\n", SIP_DEFAULT_K_T4);
            fflush (stdout);
            printf ("Do You want to change the timer value(y/n):");
            fflush (stdout);
            inputtype = sip_getchar ();
            if (inputtype == 'y' || inputtype == 'Y')
            {
                printf ("Enter the customized value for K:");
                fflush (stdout);
                fflush (stdin);
                dTimerValue = 0;
                scanf ("%d", &dTimerValue);
                if (dTimerValue)
                    dTimeOut.dTimerK_T4 = dTimerValue;
            }
            fflush (stdin);
        }
#endif
        printf ("%s", menu);
        fflush (stdout);

        if ((glbtransptype & SIP_UDP) == SIP_UDP)
        {
            if ((sockfd = socket (AF_INET, SOCK_DGRAM, 0)) < 0)
            {
                printf ("Server could not open dgram socket\n");
                fflush (stdout);
                close (sockfd);
                exit (0);
            }
        }
        else
        {
            if ((sockfd = socket (AF_INET, SOCK_STREAM, 0)) < 0)
            {
                printf ("Server could not open stream socket\n");
                fflush (stdout);
                close (sockfd);
                exit (0);
            }
        }

        /* initialize structures for binding to listen port */
        bzero ((char *) &serv_addr, sizeof (serv_addr));
        serv_addr.sin_family = AF_INET;
        serv_addr.sin_addr.s_addr = htonl (INADDR_ANY);
        serv_addr.sin_port = htons ((SIP_U16bit) atoi (argv[1]));

        if (bind (sockfd, (struct sockaddr *) &serv_addr, sizeof (serv_addr)) <
            0)
        {
            printf ("Server couldn't bind to local address.\n");
            fflush (stdout);
            close (sockfd);
            exit (0);
        }

        if ((glbtransptype & SIP_TCP) == SIP_TCP)
            listen (sockfd, 5);

        for (;;)
        {
#ifndef SIP_TXN_LAYER
            TimerListElement   *elem;
            SIP_U32bit          i;
#endif
            SipError            derror;

            struct timeval      tv1, tv2;

#ifndef SIP_TXN_LAYER
            /* get minimum timeout from the timer list */
            mintimeout = getMinimumTimeout (&timerlist);
#else
            mintimeout = 1000;
#endif

            timeout.tv_sec = (int) (mintimeout / 1000);
            timeout.tv_usec = ((int) mintimeout) % 1000;
            FD_ZERO (&readfs);
            FD_SET (sockfd, &readfs);
            FD_SET (0, &readfs);
            /* Enter into select
               Comes out of select if a key is pressed
               or a network message arrives
               or if the minimum timout occurs
             */

            gettimeofday (&tv1, NULL);
            if (select (sockfd + 1, &readfs, NULL, NULL, &timeout) < 0)
            {
                printf ("Select system call failed\n\n");
                fflush (stdin);
                fflush (stdout);
                continue;
            }
            gettimeofday (&tv2, NULL);
            mintimeout = (((tv2.tv_sec) * 1000 + (tv2.tv_usec / 1000))
                          - ((tv1.tv_sec) * 1000 + (tv1.tv_usec / 1000)));

#ifndef SIP_TXN_LAYER
            /* update timer lists  and call registered funcs if required */

            sip_listSizeOf (&timerlist, &size, &derror);
            for (i = 0; i < size; i++)
            {
                sip_listGetAt (&timerlist, (SIP_U32bit) i, (SIP_Pvoid *) & elem,
                               &derror);
                elem->duration -= mintimeout;
                if (elem->duration <= 0)
                {
                    SipBool (*tempfunc) (SipTimerKey *, SIP_Pvoid);
                    SipTimerKey        *tempkey;
                    SIP_Pvoid           tempbuffer;

                    tempfunc = elem->timeoutfunc;
                    tempkey = elem->key;
                    tempbuffer = elem->buffer;
                    sip_listDeleteAt (&timerlist, (SIP_U32bit) i, &derror);
                    i--;
                    size--;
                    tempfunc (tempkey, tempbuffer);
                }
            }
#endif

            if (FD_ISSET (sockfd, &readfs))
            {
                if ((glbtransptype & SIP_UDP) == SIP_UDP)
                {
                    message = (char *) malloc (sizeof (char) * MAXMESG);
                    n = recvfrom (sockfd, message, MAXMESG, 0,
                                  (struct sockaddr *) &cli_addr, &clilen);
                }
                else
                {
                    int                 dacceptfd;
                    dacceptfd = accept (sockfd, NULL, NULL);
                    if (dacceptfd < 0)
                    {
                        printf ("Failed: Accept on a TCP socket failed");
                        fflush (stdout);
                        close (sockfd);
                        exit (0);
                    }
                    message = (char *) malloc (sizeof (char) * MAXMESG);
                    n = recv (dacceptfd, message, MAXMESG, 0);
                }

                if (showMessage)
                    printf ("\n\n\n\n");
                fflush (stdout);

                if (n < 0)
                {
                    printf ("Server : Error in receive.\n");
                    fflush (stdout);
                    close (sockfd);
                    exit (0);
                }
                message[n] = '\0';

                do
                {
#ifdef SIP_NO_CALLBACK
                    SipMessage         *outMsg;
#endif
                    printf
                        ("|---Message Received from the network----------|\n");
                    fflush (stdout);
                    printf ("\n%s\n", message);
                    fflush (stdout);
                    printf
                        ("|---End of message received from the network---|\n");
                    fflush (stdout);
#ifndef SIP_NO_CALLBACK
                    r = sip_decodeMessage ((SIP_S8bit *) message, &opt,
                                           strlen (message), &nextmesg,
                                           &context, &derror);
#else
#ifdef SIP_TXN_LAYER
                    txncontext.txnOption.dOption = opt;
                    r = sip_txn_decodeMessage ((SIP_S8bit *) message, &outMsg,
                                               &txncontext, n, &nextmesg,
                                               &dResult, &pTxnKey, &derror);
#else
                    r = sip_decodeMessage ((SIP_S8bit *) message, &outMsg, &opt,
                                           strlen (message), &nextmesg,
                                           &context, &derror);
#endif
#endif
                    if (r == SipFail)
                    {
                        switch (derror)
                        {
                            case E_PARSER_ERROR:
                                printf
                                    ("** ERROR : Parsing of the Message Failed**\n");
                                fflush (stdout);
                                break;
                            case E_INCOMPLETE:
                                printf ("** ERROR : Message Incomplete**\n");
                                fflush (stdout);
                                break;
#ifdef SIP_TXN_LAYER
                            case E_TXN_NO_EXIST:
                                printf
                                    ("** ERROR : The Txn for this message doesn't exist**\n");
                                fflush (stdout);
                                break;
                            case E_TXN_EXISTS:
                                printf
                                    ("** ERROR : The Txn for this message already exists**\n");
                                fflush (stdout);
                                break;
                            case E_TXN_INV_STATE:
                                printf
                                    ("** ERROR : This message leads to an invalid transaction state**\n");
                                fflush (stdout);
                                break;
                            case E_TXN_INV_MSG:
                                printf
                                    ("** ERROR : This is an invalid message received for the Txn**\n");
                                fflush (stdout);
                                break;
#endif
                            default:
                                break;
                        }
                        printf ("** ERROR : Decode Message Failed**\n");
                        fflush (stdout);

                    }
#ifdef SIP_NO_CALLBACK
                    else
                    {
#ifdef SIP_TXN_LAYER
                        switch (dResult)
                        {
                            case SipTxnIgnorable:
                                printf
                                    (" The message can be ignored by the application.\n");
                                fflush (stdout);
                                break;
                            case SipTxnNonIgnorable:
                                printf
                                    ("The message needs to be handled by the application.\n");
                                fflush (stdout);
                                break;
                            case SipTxnStrayMessage:
                                printf
                                    ("The message is a Stray message, It can be ignored.\n");
                                fflush (stdout);
                                break;
                            case SipTxnQueued:
                                printf
                                    ("The message is a retransmission.It is queued.\n");
                                fflush (stdout);
                                break;
                            case SipTxnConfirmnNeeded:
                                printf
                                    ("The message needs to be handled by the application.\n");
                                fflush (stdout);
                                break;
                            case SipTxnRetrans:
                                printf
                                    ("The message decoded is a retransmission and has"
                                     "been received after cancellation of the Txn.\n");
                            default:
                                printf
                                    ("The message received is not of recognized type.\n");
                                fflush (stdout);
                                break;
                        }
                        sip_txn_freeTxnKey (pTxnKey, &derror);
#endif
                        /* In no callback mode, the user would have to figure
                         * out which is the decoded method.
                         * See sipstruct.h for more details
                         */
                        printf
                            ("** Received message without issuing a callback !\n");
                        fflush (stdout);
                        displayParsedMessage (outMsg);
                    }
#endif

                    fast_memfree (0, message, SIP_NULL);
                    /* See if read buffer had more than one message in it */
                    message = nextmesg;
                    /* Length of the remaining segment is in event-context */
                    n = context.dNextMessageLength;
#ifdef SIP_INCREMENTAL_PARSING
                    for (j = 0; j < HEADERTYPENUM; j++)
                        context.pList->enable[j] = SipSuccess;
#endif
#ifdef SIP_BADMESSAGE_PARSING
                    opt.dOption = 0;
#endif

                    if (showMessage)
                    {
                        if (sip_getStatistics
                            (SIP_STAT_TYPE_API, SIP_STAT_API_COUNT,
                             SIP_STAT_NO_RESET, &stat, &derror) == SipFail)
                        {
                            printf
                                ("##### Cannot display Statistics - Stats disabled #####\n");
                        }
                        else
                        {
                            printf ("######## Stack statistics ############\n");
                            fflush (stdout);
                            sip_getStatistics (SIP_STAT_TYPE_API,
                                               SIP_STAT_API_REQ_PARSED,
                                               SIP_STAT_NO_RESET, &stat,
                                               &derror);
                            printf ("######## Requests parsed    :%d\n", stat);
                            fflush (stdout);
                            sip_getStatistics (SIP_STAT_TYPE_API,
                                               SIP_STAT_API_RESP_PARSED,
                                               SIP_STAT_NO_RESET, &stat,
                                               &derror);
                            printf ("######## Responses parsed    :%d\n", stat);
                            fflush (stdout);
                            sip_getStatistics (SIP_STAT_TYPE_API,
                                               SIP_STAT_API_REQ_SENT,
                                               SIP_STAT_NO_RESET, &stat,
                                               &derror);
                            printf ("######## Requests sent        :%d\n", stat);
                            fflush (stdout);
                            sip_getStatistics (SIP_STAT_TYPE_API,
                                               SIP_STAT_API_RESP_SENT,
                                               SIP_STAT_NO_RESET, &stat,
                                               &derror);
                            printf ("######## Responses sent     :%d\n", stat);
                            fflush (stdout);
                            sip_getStatistics (SIP_STAT_TYPE_ERROR,
                                               SIP_STAT_ERROR_PROTOCOL,
                                               SIP_STAT_NO_RESET, &stat,
                                               &derror);
                            printf ("######## Failed Message     :%d\n", stat);
                            fflush (stdout);

                            fflush (stdout);

                            printf ("######################################\n");
                            fflush (stdout);
                        }
                        printf ("%s", menu);
                        fflush (stdout);
                    }

                }
                while (message != SIP_NULL);
            }
            else if (FD_ISSET (0, &readfs))
            {
                /* Key pressed */
                char                c;
                fflush (stdin);
                c = sip_getchar ();
                switch (c)
                {
                    case 'i':
                        sendRequest ("INVITE", &sendaddr, &derror);
                        break;
                    case 'r':
                        sendRequest ("REGISTER", &sendaddr, &derror);
                        break;
                    case 'o':
                        sendRequest ("OPTIONS", &sendaddr, &derror);
                        break;
                    case 'b':
                        sendRequest ("BYE", &sendaddr, &derror);
                        break;
                    case 'c':
                        sendRequest ("CANCEL", &sendaddr, &derror);
                        break;
                    case 'a':
                        sendRequest ("ACK", &sendaddr, &derror);
                        break;
                    case 'k':
                        sendRequest ("PRACK", &sendaddr, &derror);
                        break;
#ifdef SIP_IMPP
                    case 'u':
                        sendRequest ("SUBSCRIBE", &sendaddr, &derror);
                        break;
                    case 'n':
                        sendRequest ("NOTIFY", &sendaddr, &derror);
                        break;
                    case 'm':
                        sendRequest ("MESSAGE", &sendaddr, &derror);
                        break;
#endif
                    case 'v':
                        sendRequest ("UNKNOWN", &sendaddr, &derror);
                        break;
                    case 'e':
                        sendRequest ("REFER", &sendaddr, &derror);
                        break;
                    case 'f':
                        sendRequest ("INFO", &sendaddr, &derror);
                        break;
                    case 'g':
                        sendRequest ("COMET", &sendaddr, &derror);
                        break;
                    case '3':
                        sendResponse (300, "Multiple Choices", "INVITE",
                                      &sendaddr, &derror);
                        break;
                    case '4':
                        sendResponse (401, "UnAuthorized", "INVITE", &sendaddr,
                                      &derror);
                        break;
                    case 'G':
                        sendResponse (200, "OK", "COMET", &sendaddr, &derror);
                        break;
                    case 'I':
                        sendResponse (200, "OK", "INVITE", &sendaddr, &derror);
                        break;
                    case 'C':
                        sendResponse (200, "OK", "CANCEL", &sendaddr, &derror);
                        break;
                    case 'R':
                        sendResponse (200, "OK", "REGISTER", &sendaddr,
                                      &derror);
                        break;
                    case 'B':
                        sendResponse (200, "OK", "BYE", &sendaddr, &derror);
                        break;
                    case 'O':
                        sendResponse (200, "OK", "OPTIONS", &sendaddr, &derror);
                        break;
                    case 'K':
                        sendResponse (200, "OK", "PRACK", &sendaddr, &derror);
                        break;
                    case 'F':
                        sendResponse (200, "OK", "INFO", &sendaddr, &derror);
                        break;
                    case 'E':
                        sendResponse (200, "OK", "REFER", &sendaddr, &derror);
                        break;
#ifdef SIP_IMPP
                    case 'U':
                        sendResponse (200, "OK", "SUBSCRIBE", &sendaddr,
                                      &derror);
                        break;
                    case 'N':
                        sendResponse (200, "OK", "NOTIFY", &sendaddr, &derror);
                        break;
                    case 'M':
                        sendResponse (200, "OK", "MESSAGE", &sendaddr, &derror);
                        break;
#endif

                    case 'T':
                        c = sip_getchar ();
                        switch (c)
                        {
                            case 'c':
                                sendResponse (100, "Trying", "CANCEL",
                                              &sendaddr, &derror);
                                break;
                            case 'b':
                                sendResponse (100, "Trying", "BYE", &sendaddr,
                                              &derror);
                                break;
                            case 'o':
                                sendResponse (100, "Trying", "OPTIONS",
                                              &sendaddr, &derror);
                                break;
                            case 'r':
                                sendResponse (100, "Trying", "REGISTER",
                                              &sendaddr, &derror);
                                break;
                            case 'e':
                                sendResponse (100, "Trying", "REFER", &sendaddr,
                                              &derror);
                                break;
#ifdef SIP_IMPP
                            case 'u':
                                sendResponse
                                    (100, "Trying", "SUBSCRIBE", &sendaddr,
                                     &derror);
                                break;
                            case 'n':
                                sendResponse (100, "Trying", "NOTIFY",
                                              &sendaddr, &derror);
                                break;
                            case 'm':
                                sendResponse (100, "Trying", "MESSAGE",
                                              &sendaddr, &derror);
                                break;
                            default:
                                if (c != 0x0a)
                                    printf ("Unrecognized Key Pressed\n");
                                fflush (stdout);
                                fflush (stdin);
                                break;
#endif
                        }
                        break;
                    case 't':
                        sendResponse (100, "Trying", "INVITE", &sendaddr,
                                      &derror);
                        break;
                    case '0':
                        sendResponse (180, "Ringing", "INVITE", &sendaddr,
                                      &derror);
                        break;
                    case '1':
                        sendResponse (181, "Call Forward", "INVITE", &sendaddr,
                                      &derror);
                        break;
                    case '2':
                        sendResponse (182, "Queued", "INVITE", &sendaddr,
                                      &derror);
                        break;
                    case 's':
                        sendResponse
                            (183, "Session progress", "INVITE", &sendaddr,
                             &derror);
                        break;
                    case 'z':
                    {
                        char                selectHeader1 = '0';

                        int                 j1;
                        FD_CLR (0, &readfs);
                        /*
                         * When we enable incremental 
                         * parsing by default all the headers
                         * will be parsed .This is done by 
                         * making SipEventContext->SipHdrTypeList
                         * to SipSuccess
                         */
                        glbSipOption = 1;
                        (void) j1;
                        (void) selectHeader1;
#ifdef SIP_INCREMENTAL_PARSING
                        if (context.pList == SIP_NULL)
                            context.pList = (SipHdrTypeList *)
                                fast_memget (0, sizeof (SipHdrTypeList),
                                             SIP_NULL);
                        for (j1 = 0; j1 < HEADERTYPENUM; j1++)
                            context.pList->enable[j1] = SipSuccess;
#endif
                        opt.dOption = 0;

#ifdef SIP_INCREMENTAL_PARSING

                        printf ("Here we are doing runtime enabling"
                                "/disabiling of incremental parsing \n");
                        printf ("\nParse Via Header Incrementally): [n] ");
                        fflush (stdin);

                        selectHeader1 = sip_getchar ();
                        if ((selectHeader1 == 'y') || (selectHeader1 == 'Y'))
                            context.pList->enable[SipHdrTypeVia] = SipFail;
                        printf ("\nParse Contact Header Incrementally): "
                                "[n] ");
                        fflush (stdin);

                        selectHeader1 = sip_getchar ();
                        if ((selectHeader1 == 'y') || (selectHeader1 == 'Y'))
                            context.pList->enable[SipHdrTypeContactAny] =
                                SipFail;
                        printf ("\nParse RecordRoute Header Incremental):"
                                "[n] ");
                        fflush (stdin);

                        selectHeader1 = sip_getchar ();
                        if ((selectHeader1 == 'y') || (selectHeader1 == 'Y'))
                            context.pList->enable[SipHdrTypeRecordRoute] =
                                SipFail;
                        printf ("\nParse Route Header Incremental): [n] ");
                        fflush (stdin);

                        selectHeader1 = sip_getchar ();
                        if ((selectHeader1 == 'y') || (selectHeader1 == 'Y'))
                            context.pList->enable[SipHdrTypeRoute] = SipFail;
#endif
#ifdef SIP_BADMESSAGE_PARSING
                        printf ("\nYou want to enable Bad Message parsing):"
                                " [n] ");
                        fflush (stdin);

                        selectHeader1 = sip_getchar ();
                        if ((selectHeader1 == 'y') || (selectHeader1 == 'Y'))

                            opt.dOption |= SIP_OPT_BADMESSAGE;
                        else
                            opt.dOption = 0;
                        /*opt.dOption |= SIP_OPT_BADMESSAGE; */
                        context.dOptions = opt;
#endif

#ifndef SIP_TXN_LAYER
                        printf ("The following option disables retransmission"
                                " related checks in the decode function. With this"
                                " option, the decode function will not call the "
                                "stop timer callback to stop retransmissions after"
                                " decoding a message. Mandaory header check is "
                                "also disabled with this option.\n");
                        fflush (stdout);
                        printf ("Disable timer checks in decode (y/n): [n]");
                        fflush (stdout);
                        fflush (stdin);
                        selectHeader1 = sip_getchar ();
                        if ((selectHeader1 == 'y') || (selectHeader1 == 'Y'))
                        {
                            opt.dOption |= SIP_OPT_NOTIMER;
                        }
#endif
                        printf ("%s", menu);
                        fflush (stdout);

                        break;
                    }

                    case 'q':
#ifndef SIP_TXN_LAYER
                        sip_listForEach (&timerlist, &timerElementFree,
                                         &derror);
                        sip_listDeleteAll (&timerlist, &derror);
#else
#if !defined(SIP_VXWORKS) && !defined(SIP_WINDOWS)
                        pthread_cancel (dTimerThread);
#endif
                        __sip_flushTimer ();
                        __sip_flushHash ();
                        sip_freeTimerWheel ();
                        sip_freeHashTable ();
#endif
                        sip_releaseStack ();
#ifdef SIP_INCREMENTAL_PARSING
                        if (context.pList != SIP_NULL)
                            fast_memfree (0, context.pList, SIP_NULL);
#endif
#ifdef SIP_OSE
                        terminateSipTest ();
                        kill_proc (current_process ());
#endif
                        exit (0);
                    case ' ':
                        printf ("%s", menu);
                        fflush (stdin);
                        fflush (stdout);
                        break;
                    default:
                        if (c != 0x0a)
                            printf ("Unrecognized Key Pressed\n");
                        fflush (stdin);
                        fflush (stdout);
                        break;
                }
            }
        }
    }

    SipBool             sip_setTagInFromHdr (SipHeader * hdr, SIP_S8bit * tag,
                                             SipError * err)
    {
        SipParam           *pParam = SIP_NULL;
        SIPDEBUGFN ("Entering function sip_setTaginFrom/To Headers");
#ifdef SIP_VALIDATE
        if (err == SIP_NULL)
            return SipFail;

        if (hdr == SIP_NULL)
        {
            *err = E_INV_HEADER;
            return SipFail;
        }

#endif

        sip_initSipParam (&pParam, err);
        pParam->pName = sip_strdup ((SIP_S8bit *) "tag", APP_MEM_ID);
        if (sip_listInsertAt (&pParam->slValue, 0, (SIP_Pvoid) tag, err) ==
            SipFail)
        {
            sip_freeSipParam (pParam);
            return SipFail;
        }

        if (sip_listAppend (&(((SipFromHeader *) (hdr->pHeader))->slParam),
                            (SIP_Pvoid) pParam, err) == SipFail)
        {
            sip_freeSipParam (pParam);
            return SipFail;
        }
        SIPDEBUGFN ("Entering function sip_setTaginFrom/To Headers");
        return SipSuccess;
    }
#ifdef __cplusplus
}
#endif
