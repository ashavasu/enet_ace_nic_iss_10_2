/*nat sip utils file */
/** $Id: natapp.c,v 1.1.1.1 2010/12/08 06:10:43 siva Exp $*/


#ifdef __cplusplus
extern "C" {
#endif

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <string.h>
#include "microsip_portlayer.h"
#include "natapp.h"
#include "natsipdefs.h"
#include "microsip_decode.h"
#include "microsip_struct.h"
#include "microsip_sendmessage.h"
#include "microsip_list.h"
#include "microsip_init.h"
#include "microsip_free.h"
#include "microsip_statistics.h"
#include "microsip_timer.h"
#include "microsip_trace.h"
#include "microsip_startline.h"
#include "microsip_subapi.h"
#include "microsip_header.h"
#include "microsip_msgbody.h"
#include "microsip_accessors.h"

/* this function is the interface to NAT module 
   to perform the SIP msg translation for both
   incoming and outgoing packet */

/*TODO..the return type should be as per NAT's guidlines */
/******************************************************/
/* this function is for translating the entire sip msg*/
/******************************************************/
SipBool sipAlgTranslateSipMsg (SIP_U8bit *pSipBuf, SipAlgPktInOut dInOut)
{
  SipMessage *pParsedMsg;
  SipOptions opt;
  SipEventContext context;
  SIP_S8bit *nextmesg;
  SipError derror;
  SipBool r;

  /* first decode the sip msg then depending on the
     direction invoke the corresponding header,msgbody functions
     finally, form the buffer again the sip msg*/
   
   /* call the stack API to decode the message */
  r= sip_decodeMessage((SIP_S8bit *)pSipBuf,&pParsedMsg,&opt,
                                  strlen(pSipBuf),\
                                     &nextmesg,&context, &derror);
    
  if(dInOut == SipPktOut)
  {
    if(sipAlgTranslateOutgoingHeaders(pParsedMsg) == SipFail) 
    {
      return SipFail;  
    }  
    if(sipAlgTranslateMedia(pParsedMsg) == SipFail) 
    {
      return SipFail;  
    }  
  }
  else if(dInOut == SipPktIn)
  {
    if(sipAlgTranslateIncomingHeaders(pParsedMsg) == SipFail) 
    {
      return SipFail;  
    }  
    if(sipAlgTranslateMedia(pParsedMsg) == SipFail) 
    {
      return SipFail;  
    }  
  }  
  return SipSuccess;
}

/****************************************************/
/* this function is for translating incoming headers*/
/****************************************************/
SipBool sipAlgTranslateIncomingHeaders (SipMessage *pSipMsg )
{
  SipError    derror;

  if (pSipMsg->dType == SipMessageRequest)
  { 
    /* if the incoming message is a request then translation needs to be 
     performed only on Request URI and Route header if present*/
    if(sipAlgTranslateReqUri(pSipMsg) == SipFail)
    {
      return SipFail;  
    }  
    if(sipAlgTranslateRouteHdr(pSipMsg) == SipFail)
    {
      return SipFail;  
    }  
  }
  else if(pSipMsg->dType == SipMessageResponse)
  {  
    SipHeader header;
    SIP_S8bit	*pTemp = SIP_NULL;

    /* if the incoming message is a response then translation needs to be 
     performed only on Via, Record-Route if present and contact if it is
     a response to REGISTER*/
    
    if(sipAlgTranslateViaHdr(pSipMsg) == SipFail)
    {
      return SipFail;  
    }  

    if(sipAlgTranslateRecordRouteHdr(pSipMsg) == SipFail)
    {
      return SipFail;  
    }  

     /* check whether this is a 200 OK for 
           REGISTER, then get all the contacts */
    if (sip_getHeader(pSipMsg, SipHdrTypeCseq, &header, \
			&derror) == SipFail)
		{
			return SipFail;
		}

		if (sip_getMethodFromCseqHdr(&header, &pTemp, \
			&derror) ==  SipFail)
		{
      return SipFail;
		}
    sip_freeSipHeader(&header);
    if (sip_strcasecmp(pTemp,"REGISTER") == 0 )
	  {
      if(sipAlgTranslateContactHdr(pSipMsg) == SipFail)
      {
        return SipFail;  
      }  
    } 
  }
  return SipSuccess;
}  

/*************************************************/
/* this function is for translating out headers*/
/*************************************************/
SipBool sipAlgTranslateOutgoingHeaders (SipMessage *pSipMsg )
{
  /* the below block is for the outgoing message */
  if (pSipMsg->dType == SipMessageRequest)
  { 
    /* if the outgoing message is a request then translation needs to be 
     performed viahdr, contact and if present, the record-route header*/
    if(sipAlgTranslateViaHdr(pSipMsg) == SipFail)
    {
      return SipFail;  
    }  
    if(sipAlgTranslateContactHdr(pSipMsg) == SipFail)
    {
      return SipFail;  
    }  
    if(sipAlgTranslateRecordRouteHdr(pSipMsg) == SipFail)
    {
      return SipFail;  
    }  
  }
  else if(pSipMsg->dType == SipMessageResponse)
  {  
    /* if the outgoing message is a response then translation needs to be 
     performed only on contact hdr */ 
    if(sipAlgTranslateContactHdr(pSipMsg) == SipFail)
    {
      return SipFail;  
    }  
  }
  return SipSuccess;
}

/* The following function is used to translate the Req-URI */
/* SipAlgMedia(SipMessage *pSipMsg, tHeaderInfo * pHeaderInfo)*/
SipBool
sipAlgTranslateMedia (SipMessage *pSipMsg) 
{
  SipError    derror;
  SIP_U32bit i=0;
	SipMsgBody *pMsgBody = SIP_NULL;
	SipBool sdpFound = SipFail;
	SIP_U32bit dCount=0, dAttrCount=0, dMediaCount=0; 
  SdpMessage *pSdpMsg;
  SdpConnection *pSessionConnection = SIP_NULL;
  SIP_U8bit *pConnectionIP = SIP_NULL, *pTransConnectionIP=SIP_NULL;
  SipBool dIsSessionLevelConnection = SipFail;
  /*SipBool dIsConnectionIPPrivate = SipSuccess;*/
  Sip_aValueInSdp dSessionLevelDirAttr = 0;
 
  /* This block of code is to find whether SDP msg body is present
     in the SIP message */
  if(sip_getMsgBodyCount(pSipMsg, &dCount, \
		          &derror) == SipFail)
  {
    return SipFail;  
  }  
  
  if(dCount == 0)
  {
    return SipFail;
    /* No message body present in the sip message */
  }
  
  for (i = 0; i < dCount; i++)
	{
		en_SipMsgBodyType dType;
		if (sip_getMsgBodyTypeAtIndex(pSipMsg, \
			&dType, i, &derror) == SipFail)
		{
      return SipFail;
		}
		if (dType == SipSdpBody)
		{
			if (sip_getMsgBodyAtIndex(pSipMsg, \
				&pMsgBody, i, &derror) == SipFail)
			{
				return SipFail;
			}
			sdpFound = SipSuccess;	
			break;	
		}
	}
	if (sdpFound == SipFail)
	{
	  /* TODO: freeing the pMsgBody here is not reqd..it seems */
    sip_freeSipMsgBody(pMsgBody);
		return SipFail;
	}

	/* 	=========================================
		  get the "incoming" SdpMessage struct
		========================================= */
	if (sip_getSdpFromMsgBody(pMsgBody, &pSdpMsg, \
		&derror) == SipFail)
	{
	  sip_freeSipMsgBody(pMsgBody);
		return SipFail;
	}
  /* get the media count from SDP */
  if(sdp_getMediaCount(pSdpMsg,&dMediaCount,\
			 &derror)==SipFail)
	{
    sip_freeSdpMessage(pSdpMsg);
    sip_freeSipMsgBody(pMsgBody);
    return SipFail; 
  } 
  if (dMediaCount == 0)
  {
    /* TODO: trace, no media present in the sdp*/
    sip_freeSdpMessage(pSdpMsg);
    sip_freeSipMsgBody(pMsgBody);
    return SipSuccess;
  }  
  /* get the connection line from the sdp-msg body */
  if(sdp_getConnection(pSdpMsg,&pSessionConnection,\
			&derror)==SipFail)
  {
    sip_freeSdpMessage(pSdpMsg);
    sip_freeSipMsgBody(pMsgBody); 
    return SipFail; 
  } 
  if(pSessionConnection->pAddr != SIP_NULL)
  {  
    sip_strcpy(pConnectionIP, pSessionConnection->pAddr);
  }
  /* invoke the API to check whether this a private ip or public ip */
  /* if it is public then set dIsConnectionIPPrivate to SipFail */
  /*dIsConnectionIPPrivate = SipFail;*/
  
  /* The below block of code extracts the session level direction attribute*/
  /* get the session level direction attribute from sdp */
  if(sdp_getAttrCount(pSdpMsg,&dAttrCount,\
			    &derror)==SipFail)
	{
		sip_freeSdpMessage(pSdpMsg);
    sip_freeSipMsgBody(pMsgBody); 
    return SipFail;
	}
	for(i=0;i<dAttrCount;i++)	
	{
		SdpAttr			*pTempAttr=SIP_NULL;
		SIP_S8bit	*pName=SIP_NULL;

		/*Get the Attribute*/
		if(sdp_getAttrAtIndex(pSdpMsg,&pTempAttr,i,\
							&derror)==SipFail)
		{
      sip_freeSdpMessage(pSdpMsg);
      sip_freeSipMsgBody(pMsgBody); 
			return SipFail;
		}
		if(sdp_getNameFromAttr(pTempAttr,&pName,\
			          &derror)==SipFail)
		{
      sip_freeSdpMessage(pSdpMsg);
      sip_freeSipMsgBody(pMsgBody); 
			sip_freeSdpAttr(pTempAttr);
			return SipFail;
		}
		if(sip_strcasecmp(pName,"sendonly")==0)
		{
      dSessionLevelDirAttr = sendonly;
      break;
		}else if(sip_strcasecmp(pName,"recvonly")==0)
		{
      dSessionLevelDirAttr = recvonly;
      break;
		}else if(sip_strcasecmp(pName,"sendrecv")==0)
		{
      dSessionLevelDirAttr = sendrecv;
      break;
		}else if(sip_strcasecmp(pName,"inactive")==0)
		{
      dSessionLevelDirAttr = inactive;
      break;
		}
  } /* end of block to get the session level dir attribute*/

  /* now iterate thru all the media lines */
  for(i=0; i < dMediaCount; i++)
	{
		SdpMedia	*pSdpMedia=SIP_NULL;
    SIP_U32bit connectionLines=0, dAttrCount=0,j=0;
    SIP_U8bit *pConnIPInMedia = SIP_NULL;
    SIP_U16bit dPort=0;
    SipAlgMediaInfo *pMediaInfo = SIP_NULL; 
    Sip_aValueInSdp dMediaLevelDirAttr = 0;
    SdpConnection	*pConnection;
    SipNatInfo  *pMediaNatInfo = SIP_NULL;
    
    /* get the media at the given index */
    if(sdp_getMediaAtIndex(pSdpMsg,&pSdpMedia,i,\
			&derror)==SipFail)
		{
      sip_freeSdpMessage(pSdpMsg);
      sip_freeSipMsgBody(pMsgBody); 
      return SipFail;
    }
    /* check if there is any connection line present in this 
       media*/
    if(sdp_getConnectionCountFromMedia(pSdpMedia,&connectionLines,\
					&derror)== SipFail)
    {
      sip_freeSdpMedia(pSdpMedia);
      sip_freeSdpMessage(pSdpMsg);
      sip_freeSipMsgBody(pMsgBody);
      return SipFail;
    }  
    if(connectionLines !=  0)
    { 
	  	if(sdp_getConnectionAtIndexFromMedia(pSdpMedia,&pConnection,0,\
					&derror)==SipFail)
      {
        sip_freeSdpMedia(pSdpMedia);
        sip_freeSdpMessage(pSdpMsg);
        sip_freeSipMsgBody(pMsgBody);
        return SipFail;
      }
      if(pConnection->pAddr != SIP_NULL)
      {  
        sip_strcpy(pConnIPInMedia, pConnection->pAddr);
      }
    }
    else if (connectionLines ==  0)
    {
      dIsSessionLevelConnection = SipSuccess; 
    }  
    if(sdp_getPortFromMedia(pSdpMedia,&dPort,\
		        &derror)==SipFail)
    {
      sip_freeSdpMedia(pSdpMedia);
      sip_freeSdpMessage(pSdpMsg);
      sip_freeSipMsgBody(pMsgBody);
      return SipFail;
    }
    /* the following block of code gets the dir attribute from media*/
    /* now we need to get the direction attribute from the media */
    if(sdp_getAttrCountFromMedia(pSdpMedia,&dAttrCount,
			        &derror)==SipFail)
    {
      sip_freeSdpMedia(pSdpMedia);
      sip_freeSdpMessage(pSdpMsg);
      sip_freeSipMsgBody(pMsgBody);
      return SipFail;
    }  
    for (j=0; j<dAttrCount; j++)
    {
       SdpAttr			*pMediaAttr=SIP_NULL;
       SIP_U8bit   *pAttrName = SIP_NULL;

       if(sdp_getAttrAtIndexFromMedia(pSdpMedia,&pMediaAttr,j,\
					&derror)==SipFail)
       {
         sip_freeSdpMedia(pSdpMedia);
         sip_freeSdpMessage(pSdpMsg);
         sip_freeSipMsgBody(pMsgBody);
         return SipFail;
       } 
       if(sdp_getNameFromAttr(pMediaAttr,&pAttrName,\
				       &derror)==SipFail)
		   {
         sip_freeSdpAttr(pMediaAttr);
         sip_freeSdpMedia(pSdpMedia);
         sip_freeSdpMessage(pSdpMsg);
         sip_freeSipMsgBody(pMsgBody);
         return SipFail;
       }  
       if(sip_strcasecmp(pAttrName,"sendonly")==0)
		   {
         dMediaLevelDirAttr = sendonly;
         break;
	     }
       else if(sip_strcasecmp(pAttrName,"recvonly")==0)
		   {
         dMediaLevelDirAttr = recvonly;
         break;
	     }
       else if(sip_strcasecmp(pAttrName,"sendrecv")==0)
		   {
          dMediaLevelDirAttr = sendrecv;
          break;
	     }
       else if(sip_strcasecmp(pAttrName,"inactive")==0)
		   {
          dMediaLevelDirAttr = inactive;
          break;
		   }
    }  

    /* allocate memory to natinfo structure*/
    pMediaInfo = (SipAlgMediaInfo *)sip_memget\
						(0,sizeof(SipAlgMediaInfo),&derror);
   
    if (pMediaInfo == SIP_NULL)
    {  
      sip_freeSdpMedia(pSdpMedia);
      sip_freeSdpMessage(pSdpMsg);
      sip_freeSipMsgBody(pMsgBody);
	    return SipFail;
    }       
    
    /*pMediaInfo->port = dPort;*/
    pMediaInfo->mLineIndex = i;
    if (dAttrCount != 0)
      pMediaInfo->aValueInSdp = dMediaLevelDirAttr;
    else
      pMediaInfo->aValueInSdp = dSessionLevelDirAttr;
    
    /* allocate memory to natinfo structure*/
    pMediaNatInfo = (SipNatInfo *)sip_memget\
  						(0,sizeof(SipNatInfo),&derror);
   
    if (pMediaNatInfo == SIP_NULL)
    {
      sip_freeSdpMedia(pSdpMedia);
      sip_freeSdpMessage(pSdpMsg);
      sip_freeSipMsgBody(pMsgBody);
      return SipFail;
    }     
    if (connectionLines !=  0)
      pMediaNatInfo->pOriginalIP=sip_strdup(pConnIPInMedia,APP_MEM_ID);
    else
      pMediaNatInfo->pOriginalIP=sip_strdup(pConnectionIP,APP_MEM_ID);
    
    pMediaNatInfo->originalPort= dPort;
    /*pMediaNatInfo->pOutsideIP = pHeaderInfo->u4OutIpAddr;
     pMediaNatInfo->outsidePort = pHeaderInfo->u2OutPort;*/
     
    /* Invoke sipAlgUpdateSessionHash
		    (pDialogParams, SipNatInfo) */
    if (sdp_setPortInMedia(pSdpMedia,pMediaNatInfo->mappedPort, \
		           &derror) == SipFail)
    {
      sip_freeSdpMedia(pSdpMedia);
      sip_freeSdpMessage(pSdpMsg);
      sip_freeSipMsgBody(pMsgBody);
      return SipFail;  
    } 
    if (dIsSessionLevelConnection == SipFail)
    {
      if (sdp_setAddrInConnection(pConnection, pMediaNatInfo->pMappedIP, 
			        &derror)==SipFail)
			{
        sip_freeSdpMedia(pSdpMedia);
        sip_freeSdpMessage(pSdpMsg);
        sip_freeSipMsgBody(pMsgBody);
        return SipFail;
      }  
    } 
    else if (dIsSessionLevelConnection == SipSuccess)
    {
      sip_strcpy(pTransConnectionIP,pMediaNatInfo->pMappedIP);
    }  
  }
  if (sdp_setAddrInConnection(pSessionConnection,pTransConnectionIP , 
			        &derror)==SipFail)
	{
    sip_freeSdpMessage(pSdpMsg);
    sip_freeSipMsgBody(pMsgBody);
    return SipFail;
  }  
  sip_freeSdpMessage(pSdpMsg);
  sip_freeSipMsgBody(pMsgBody);
  return SipSuccess;
}    
  
/* The following function is used to translate the Req-URI */
/* SipAlgTranslateReqUri(SipMessage *pSipMsg, tHeaderInfo * pHeaderInfo)*/

SipBool
sipAlgTranslateReqUri(SipMessage *pSipMsg)
{
  SipReqLine *pReqline = SIP_NULL;
  SipAddrSpec 	*pAddrSpec = SIP_NULL;
  SipUrl             *pSipUrl  =  SIP_NULL;
  SipError derror;
  SipNatInfo *pReqUriInfo;
  SIP_S8bit  			*pTempHost = SIP_NULL;
  SIP_U16bit dPort = 0;
  
  if(sip_getReqLineFromSipReqMsg(pSipMsg,&pReqline, \
                &derror)==SipFail)
  {
    sip_error(SIP_Minor, \
                  "SIP-ERROR: sipAlgTranslateReqUri: Getting Request-line from sip msg failed\n");
    return SipFail;  
  }  
  if(sip_getAddrSpecFromReqLine(pReqline,&pAddrSpec, \
                &derror)==SipFail)
  {
    sip_error(SIP_Minor, \
                    "SIP-ERROR: sipAlgTranslateReqUri: Getting addr-spec from Req-line failed\n");
    sip_freeSipReqLine(pReqline) ; 
    return SipFail;  
  }  
  if (sip_getUrlFromAddrSpec(pAddrSpec,&pSipUrl,
                    &derror) == SipFail )
  {
     sip_error(SIP_Minor, \
               "SIP-ERROR: sipAlgTranslateReqUri: Getting url from addr-spec failed\n");
     sip_freeSipAddrSpec(pAddrSpec);
     sip_freeSipReqLine(pReqline) ;
     return SipFail;
  }
  if (sip_getHostFromUrl(pSipUrl, &pTempHost,\
				&derror) == SipFail)
  {
    sip_error(SIP_Minor, \
               "SIP-ERROR: sipAlgTranslateReqUri: Getting host from url failed\n");
    sip_freeSipUrl(pSipUrl);
		sip_freeSipAddrSpec(pAddrSpec);
    sip_freeSipReqLine(pReqline) ;
    return SipFail;  
  } 
  /* invoke the function to see whether psipUrl->pHost
     a domain or ip */
  if(sipAlgIsNumericAddress(pTempHost)==SipSuccess) 
  {
    
    sip_freeSipUrl(pSipUrl);
		sip_freeSipAddrSpec(pAddrSpec);
    sip_freeSipReqLine(pReqline) ;
    /* TODO: print that its a host name and cant be translated */
    return SipSuccess;
  }  
  if( sip_getPortFromUrl(pSipUrl, &dPort,\
           &derror)==SipFail)
  {
    sip_error(SIP_Minor, \
               "SIP-ERROR: sipAlgTranslateReqUri: Getting port from url failed\n");
    sip_freeSipUrl(pSipUrl);
		sip_freeSipAddrSpec(pAddrSpec);
    sip_freeSipReqLine(pReqline) ;
    return SipFail;  
  }  
  
  /*if it is ip then form the *pReqUriInfo structure */
  /* also check if is private ip or public */
  /* if public no translation reqd, if private do following */
  
  /* allocate memory to natinfo structure*/
  pReqUriInfo = (SipNatInfo *)sip_memget\
						(0,sizeof(SipNatInfo),&derror);
   
  if (pReqUriInfo == SIP_NULL)
  {
    sip_freeSipUrl(pSipUrl);
		sip_freeSipAddrSpec(pAddrSpec);
	  sip_freeSipReqLine(pReqline) ;
    return SipFail;
  }     
  /* TODO: needs to change this as per the new structure*/
  pReqUriInfo->pOriginalIP = pTempHost;
  pReqUriInfo->originalPort = dPort;
  /*pReqUriInfo->pOutsideIP = pHeaderInfo->u4OutIpAddr;
  pReqUriInfo->outsidePort = pHeaderInfo->u2OutPort;*/
  
 
  /* TODO Sdf_ty_u32bit 
  sipAlgUpdateSignalingInfo (SipNatInfo **ppNatInfo, 
 	en_Protocol protocol, SipBool isRegRequest) */

    
  /* here we have the translated ip and port */
  if (sip_setPortInUrl(pSipUrl, pReqUriInfo->mappedPort,
								&derror) == SipFail)
  {
    sip_freeSipUrl(pSipUrl);
		sip_freeSipAddrSpec(pAddrSpec);
    sip_freeSipReqLine(pReqline) ;
    return SipFail;   
  }
  if (sip_setHostInUrl(pSipUrl, pReqUriInfo->pMappedIP,
								&derror) == SipFail)
  {
    sip_freeSipUrl(pSipUrl);
		sip_freeSipAddrSpec(pAddrSpec);
    sip_freeSipReqLine(pReqline) ;
    return SipFail;  
  }  
  /* free all the local refeences */
  sip_freeSipUrl(pSipUrl);
	sip_freeSipAddrSpec(pAddrSpec);
  sip_freeSipReqLine(pReqline) ;
  return SipSuccess;
}

/* The following function is used to translate the Via Header */
/* SipAlgTranslateViaHdr(SipMessage *pSipMsg, tHeaderInfo * pHeaderInfo)*/

SipBool
sipAlgTranslateViaHdr(SipMessage *pSipMsg)
{
  SIP_S8bit *pTempViaSentBy  = SIP_NULL;
  SIP_S8bit  *pViaSentBy = SIP_NULL, *pTransViaSentBy = SIP_NULL;
  SIP_S8bit *pTempPort			= SIP_NULL;
  SIP_S8bit dTempArray[100];
  SipError    derror;
  SipHeader  *pViaHeader=SIP_NULL;
  SipNatInfo *pViaInfo;

  /* get the ip and port from the via header */
  if (sip_getHeaderAtIndex(pSipMsg, SipHdrTypeVia, \
        pViaHeader, 0, &derror) == SipFail)
  {
    return SipFail;
  }
  if (sip_getSentByFromViaHdr(pViaHeader, &pTempViaSentBy, \
        &derror) == SipFail)
  {
    sip_freeSipHeader(pViaHeader);
    return SipFail;
  }
  if(pTempViaSentBy != SIP_NULL)
  {  
    sip_strcpy(dTempArray, pTempViaSentBy);
  }
  /* Here dTempArray contains either the domain name or ip:port
     if it is ip:port, tokenize it and store ip and port */

  pViaSentBy = strtok_r(dTempArray, ":",&pTempPort);
  /* Invoke the function to see whether pViaSentBy
     a domain or ip */
  if(sipAlgIsNumericAddress(pViaSentBy)==SipFail)
  {
    /* TODO: print that its a host name and cant be translated */
    sip_freeSipHeader(pViaHeader);
    return SipSuccess;
  }  
  
  /*if it is ip then form the *pReqUriInfo structure */
  /* allocate memory to natinfo structure*/
  pViaInfo = (SipNatInfo *)sip_memget\
						(0,sizeof(SipNatInfo),&derror);
   
  if (pViaInfo == SIP_NULL)
  { 
    sip_freeSipHeader(pViaHeader);
	  return SipFail;
  }
  pViaInfo->pOriginalIP = pViaSentBy;
  pViaInfo->originalPort = atoi(pTempPort);
  /*pReqUriInfo->pOutsideIP = pHeaderInfo->u4OutIpAddr;
  pReqUriInfo->outsidePort = pHeaderInfo->u2OutPort;*/
  
  /* Invoke sipAlgUpdateSignalingTables
	  	(pDialogParams, SipNatInfo) */
    
  /* here we have the translated ip and port */
  /* now form the translated ip and port in ip:port form */
  /*pTransViaSentBy = ip and port concatenation */ 
  if(sip_setSentByInViaHdr(pViaHeader, pTransViaSentBy, \
		   	&derror) == SipFail)
  {
    sip_freeSipHeader(pViaHeader);
    return SipFail;  
  }  
  sip_freeSipHeader(pViaHeader);
  return SipSuccess;
}

/* The following function is used to translate the Contact Header */
/* SipAlgTranslateContactHdr(SipMessage *pSipMsg, tHeaderInfo * pHeaderInfo)*/
SipBool
sipAlgTranslateContactHdr(SipMessage *pSipMsg)
{
  SipError    derror;
  SIP_U32bit size =0;
  SipNatInfo *pContactInfo;
  SIP_S8bit  			*pTempHost = SIP_NULL;
  SIP_U16bit dPort = 0, i=0;
  
  if(sip_getHeaderCount(pSipMsg,SipHdrTypeContactAny, &size, \
					&derror)==SipFail)
  {
    return SipFail;
  }  
  if (size != 0)
	{ 
	  for (i=0 ; i< size; i++)
    { 
      SipAddrSpec 	*pTempAddrSpec = SIP_NULL;
      SipUrl         *pTempSipUrl  =  SIP_NULL;
      SipHeader      *pSipHdr = SIP_NULL;
      SipAddrSpec 	*pAddrSpec = SIP_NULL;
      SipUrl             *pSipUrl  =  SIP_NULL;  

      if(sip_getHeaderAtIndex(pSipMsg, SipHdrTypeContactAny,
					pSipHdr, i, &derror)==SipFail)
      {
        return  SipFail;  
      }  
		  if (sip_getAddrSpecFromContactHdr(pSipHdr, \
					&pTempAddrSpec, &derror)==SipFail)
      {
        sip_freeSipHeader(pSipHdr);
        return SipFail;  
      }  
      if (sip_getUrlFromAddrSpec(pTempAddrSpec,&pTempSipUrl,
                    &derror) == SipFail )
      {
        sip_freeSipAddrSpec(pAddrSpec);
        sip_freeSipHeader(pSipHdr);
        return SipFail;
      }
      if (sip_getHostFromUrl(pSipUrl, &pTempHost,\
				&derror) == SipFail)
      {
        sip_freeSipUrl(pSipUrl);
		    sip_freeSipAddrSpec(pAddrSpec);
        sip_freeSipHeader(pSipHdr);
        return SipFail;  
      }  
      /* invoke the function to see whether psipUrl->pHost
        a domain or ip */
      if(sipAlgIsNumericAddress(pTempHost)==SipFail)
      {
        sip_freeSipUrl(pSipUrl);
		    sip_freeSipAddrSpec(pAddrSpec);
        sip_freeSipHeader(pSipHdr);
        return SipSuccess;  
      } 
      if( sip_getPortFromUrl(pSipUrl, &dPort,\
           &derror)==SipFail)
      {
        sip_freeSipUrl(pSipUrl);
		    sip_freeSipAddrSpec(pAddrSpec);
        sip_freeSipHeader(pSipHdr);
        return SipFail;  
      }  
      /*if it is ip then form the *pReqUriInfo structure */
      /* allocate memory to natinfo structure*/
      pContactInfo = (SipNatInfo *)sip_memget\
						(0,sizeof(SipNatInfo),&derror);
   
      if (pContactInfo == SIP_NULL)
	    { 
        sip_freeSipUrl(pSipUrl);
		    sip_freeSipAddrSpec(pAddrSpec);
        sip_freeSipHeader(pSipHdr);
        return SipFail;
      }
      pContactInfo->pOriginalIP = pTempHost;
      pContactInfo->originalPort = dPort;
     /*pReqUriInfo->pOutsideIP = pHeaderInfo->u4OutIpAddr;
     pReqUriInfo->outsidePort = pHeaderInfo->u2OutPort;*/
 
      /* For 200 OK response of REGISTER message,
        call 
        Sdf_ty_u32bit 
sipAlgUpdateRegContactInfo (SipNatInfo *pNatInfo, SIP_U32bit expiryValue)
      
          for any other translation of contacts
            
Sdf_ty_u32bit 
sipAlgUpdateSignalingInfo (SipNatInfo **ppNatInfo, 
 	en_Protocol protocol, SipBool isRegRequest)
    */
      

     /* Invoke sipAlgUpdateSignalingTables
		    (pDialogParams, SipNatInfo) */
    
     /* here we have the translated ip and port */
     if (sip_setPortInUrl(pSipUrl, pContactInfo->mappedPort,
								&derror) == SipFail)
     {
       sip_freeSipUrl(pSipUrl);
		   sip_freeSipAddrSpec(pAddrSpec);
       sip_freeSipHeader(pSipHdr);
       return SipFail;   
     }
     if (sip_setHostInUrl(pSipUrl, pContactInfo->pMappedIP,
								&derror) == SipFail)
     {
       sip_freeSipUrl(pSipUrl);
		   sip_freeSipAddrSpec(pAddrSpec);
       sip_freeSipHeader(pSipHdr);
       return SipFail;  
     }  
    }
  } 
  return SipSuccess;
}

/* The following function is used to translate the Route Header */
/* SipAlgTranslateRouteHdr(SipMessage *pSipMsg, tHeaderInfo * pHeaderInfo)*/
SipBool
sipAlgTranslateRouteHdr(SipMessage *pSipMsg)
{
  SipError    derror;
  SIP_U32bit size =0;
  SipNatInfo *pRouteInfo;
  SIP_S8bit  			*pTempHost = SIP_NULL;
  SIP_U16bit dPort = 0, i=0;

  if (sip_getHeaderCount(pSipMsg,SipHdrTypeRoute, &size, \
					&derror)==SipFail)
  {
    return SipFail;  
  }  

	if (size == 0)
  {
    return SipFail;  
  }  
  for (i=0; i<size; i++)
	{
    SipAddrSpec 	*pAddrSpec = SIP_NULL;
    SipUrl        *pSipUrl  =  SIP_NULL;
    SipHeader		*pSipHeader=SIP_NULL;
    
	  if(sip_getHeaderAtIndex(pSipMsg, SipHdrTypeRoute,
					pSipHeader, i, &derror)== SipFail)
    {
      return SipFail;  
    }  

	  if (sip_getAddrSpecFromRouteHdr(pSipHeader, \
					&pAddrSpec, &derror) == SipFail)
    {
       sip_freeSipHeader(pSipHeader);
       return SipFail;  
    }  

    if (sip_getUrlFromAddrSpec(pAddrSpec,&pSipUrl,
                    &derror) == SipFail )
    {
      sip_freeSipAddrSpec(pAddrSpec);
      sip_freeSipHeader(pSipHeader);
      return SipFail;       
    }
    if (sip_getHostFromUrl(pSipUrl, &pTempHost,\
				&derror) == SipFail)
    {
      sip_freeSipUrl(pSipUrl);
		  sip_freeSipAddrSpec(pAddrSpec);
      sip_freeSipHeader(pSipHeader);
      return SipFail;  
    }  
  
    if( sip_getPortFromUrl(pSipUrl, &dPort,\
           &derror)==SipFail)
    {
      sip_freeSipUrl(pSipUrl);
		  sip_freeSipAddrSpec(pAddrSpec);
      sip_freeSipHeader(pSipHeader);
      return SipFail;  
    }  

    /* .invoke the function to see whether psipUrl->pHost
     a domain or ip */
     
    if(sipAlgIsNumericAddress(pTempHost)==SipFail)
    {
        sip_freeSipUrl(pSipUrl);
		    sip_freeSipAddrSpec(pAddrSpec);
        sip_freeSipHeader(pSipHeader);
        return SipSuccess;  
    } 
 
    /*if it is ip then form the *pReqUriInfo structure */
   
    /* allocate memory to natinfo structure*/
    pRouteInfo = (SipNatInfo *)sip_memget\
						(0,sizeof(SipNatInfo),&derror);
   
    if (pRouteInfo == SIP_NULL)
    { 
      sip_freeSipUrl(pSipUrl);
		  sip_freeSipAddrSpec(pAddrSpec);
      sip_freeSipHeader(pSipHeader);
	    return SipFail;
    }
    pRouteInfo->pOriginalIP = pTempHost;
    pRouteInfo->originalPort = dPort;
    /*pReqUriInfo->pOutsideIP = pHeaderInfo->u4OutIpAddr;
    pReqUriInfo->outsidePort = pHeaderInfo->u2OutPort;*/
  
    /* Invoke sipAlgUpdateSignalingTables
	  	(pDialogParams, SipNatInfo) */
    
    /* here we have the translated ip and port */
    if (sip_setPortInUrl(pSipUrl, pRouteInfo->mappedPort,
								&derror) == SipFail)
    {
      sip_freeSipUrl(pSipUrl);
		  sip_freeSipAddrSpec(pAddrSpec);
      sip_freeSipHeader(pSipHeader);
      return SipFail;   
    }
    if (sip_setHostInUrl(pSipUrl, pRouteInfo->pMappedIP,
								&derror) == SipFail)
    {
      sip_freeSipUrl(pSipUrl);
		  sip_freeSipAddrSpec(pAddrSpec);
      sip_freeSipHeader(pSipHeader);
      return SipFail;  
    }  
    sip_freeSipUrl(pSipUrl);
		sip_freeSipAddrSpec(pAddrSpec);
    sip_freeSipHeader(pSipHeader);
  }
  return SipSuccess;
}

/* The following function is used to translate the Record-Route Header */
/* SipAlgTranslateRecordRouteHdr(SipMessage *pSipMsg, tHeaderInfo * pHeaderInfo)*/
SipBool
sipAlgTranslateRecordRouteHdr(SipMessage *pSipMsg)
{
  SipError    derror;
  SIP_U32bit size =0;
  SipNatInfo *pRecordRouteInfo;
  SIP_S8bit  			*pTempHost = SIP_NULL;
  SIP_U16bit dPort = 0, i=0;

  if (sip_getHeaderCount(pSipMsg,SipHdrTypeRecordRoute, &size, \
					&derror)==SipFail)
  {
    return SipFail;  
  }  

	if (size == 0)
  {
    return SipFail;  
  }  
  for (i=0; i<size; i++)
	{
	  SipAddrSpec 	*pAddrSpec = SIP_NULL;
    SipUrl        *pSipUrl  =  SIP_NULL;
    SipHeader		*pSipHeader=SIP_NULL;

    if(sip_getHeaderAtIndex(pSipMsg, SipHdrTypeRecordRoute,
					pSipHeader, i, &derror)== SipFail)
    {
      return SipFail;  
    }  

	  if (sip_getAddrSpecFromRouteHdr(pSipHeader, \
					&pAddrSpec, &derror) == SipFail)
    {
       sip_freeSipHeader(pSipHeader);
       return SipFail;  
    }  

    if (sip_getUrlFromAddrSpec(pAddrSpec,&pSipUrl,
                    &derror) == SipFail )
    {
      sip_freeSipAddrSpec(pAddrSpec);
      sip_freeSipHeader(pSipHeader);
      return SipFail;       
    }
    if (sip_getHostFromUrl(pSipUrl, &pTempHost,\
				&derror) == SipFail)
    {
      sip_freeSipUrl(pSipUrl);
		  sip_freeSipAddrSpec(pAddrSpec);
      sip_freeSipHeader(pSipHeader);
      return SipFail;  
    }  
  
    if( sip_getPortFromUrl(pSipUrl, &dPort,\
           &derror)==SipFail)
    {
      sip_freeSipUrl(pSipUrl);
		  sip_freeSipAddrSpec(pAddrSpec);
      sip_freeSipHeader(pSipHeader);
      return SipFail;  
    }  
    
    /* TODO...invoke the function to see whether psipUrl->pHost
     a domain or ip */
    if(sipAlgIsNumericAddress(pTempHost)==SipFail)
    {
        sip_freeSipUrl(pSipUrl);
		    sip_freeSipAddrSpec(pAddrSpec);
        sip_freeSipHeader(pSipHeader);
        return SipSuccess;  
    } 
    /*if it is ip then form the *pReqUriInfo structure */
   
    /* allocate memory to natinfo structure*/
    pRecordRouteInfo = (SipNatInfo *)sip_memget\
						(0,sizeof(SipNatInfo),&derror);
   
    if (pRecordRouteInfo == SIP_NULL)
    {  
      sip_freeSipUrl(pSipUrl);
		  sip_freeSipAddrSpec(pAddrSpec);
      sip_freeSipHeader(pSipHeader);
	    return SipFail;
    }
    pRecordRouteInfo->pOriginalIP = pTempHost;
    pRecordRouteInfo->originalPort = dPort;
    /*pReqUriInfo->pOutsideIP = pHeaderInfo->u4OutIpAddr;
    pReqUriInfo->outsidePort = pHeaderInfo->u2OutPort;*/
  
    /* Invoke sipAlgUpdateSignalingTables
	  	(pDialogParams, SipNatInfo) */
    
    /* here we have the translated ip and port */
    if (sip_setPortInUrl(pSipUrl, pRecordRouteInfo->mappedPort,
								&derror) == SipFail)
    {
      sip_freeSipUrl(pSipUrl);
		  sip_freeSipAddrSpec(pAddrSpec);
      sip_freeSipHeader(pSipHeader);
      return SipFail;   
    }
    if (sip_setHostInUrl(pSipUrl, pRecordRouteInfo->pMappedIP,
								&derror) == SipFail)
    {
      sip_freeSipUrl(pSipUrl);
		  sip_freeSipAddrSpec(pAddrSpec);
      sip_freeSipHeader(pSipHeader);
      return SipFail;  
    }
    sip_freeSipUrl(pSipUrl);
		sip_freeSipAddrSpec(pAddrSpec);
    sip_freeSipHeader(pSipHeader);
  }
  return SipSuccess;
}

/* This is the util function to check whether a string is hostname or ip */
SipBool sipAlgIsNumericAddress(SIP_U8bit *pNumStr)
{
	SIP_U16bit        numDots = 0,octetCount = 0,octetVal = 0;
	en_SipBoolean 	     validOctet = SipFalse;
	/* Validation of Input parameter. */
	if( (SIP_NULL == pNumStr) || ('\0' == *pNumStr) )
	{
		return SipFail;
	}

	/* Check for special IP addresses. */
	if ((0 == strcasecmp(pNumStr, "0.0.0.0")) ||
			(0 == strcasecmp(pNumStr, "255.255.255.255")))
	{
		return SipFail;
	}

	/* 
	 * Loop over all the characters of the input string. Check if the 
	 * characters are numeric or not.
	 */
	while( '\0' != *pNumStr )
	{
		validOctet = SipFalse;
		octetVal = 0;
		while ( ((*pNumStr) >= '0') && ((*pNumStr) <= '9') )
		{
			octetVal = (10 * octetVal) + ((*pNumStr)-'0');
			pNumStr++;
			// To Handle the cases like  x.x..x  as IP address
			validOctet = SipTrue;
		}
		// Handle the cases like  x.x..x  as IP address
		if (SipFalse == validOctet )
			return SipFail;
		// If valid Octet increment the octet count	
		octetCount++;	
		if( ('.' == *pNumStr) && (octetVal < 256) )
		{
			numDots++;
			pNumStr++;
			continue;
		}
		else
		{
			break;
		}
	}
	if( (numDots != 3) || ('\0' != *pNumStr) || (octetVal > 255) || 
			(octetCount != 4))
	{
		// need xactly 3 dots and shud not have terminated prematurely
		// and last octet shud b ok (if parsed)
		return SipFail;
    
	}
	else
	{
		/* All characters in the string are numeric. return true. */
		return SipSuccess;
	}
}

void sipAlgStrToNet(char *src, SIP_U32bit *dest)
{

  

}
