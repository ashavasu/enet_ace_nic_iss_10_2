    /*
       $Id: microsip_txnthreaded.c,v 1.2 2013/12/18 12:50:12 siva Exp $
       * This test stub can be used to send simple SIP Requests/Responses
       by UDP with retransmission. This program shows a sample timer function
       implementation. The timer uses a linked list in this program and relies
       on the select system call to handle timeout callbacks required by the
       stack. The developer may use other methods to implement the timer
       functionality expected by the stack.

       Usage:
       siptest <my_port> <dest_addredd> [dest_port]

       myport: local port on which the program listens for
       incoming sip messages
       dest_addr: address to which the messages are sent
       dest_port: port to which the messages are sent
       defaults to 5060 if not given.
     */
#ifdef __cplusplus
extern              "C"
{
#endif

#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <strings.h>

#include <sys/types.h>
#include <stdlib.h>
#include "microsip_portlayer.h"
#include "microsip_decode.h"
#include "microsip_struct.h"
#include "microsip_sendmessage.h"
#include "microsip_list.h"
#include "microsip_init.h"
#include "microsip_free.h"
#include "microsip_accessors.h"
#include "microsip_header.h"

#include "microsip_statistics.h"
#include "microsip_timer.h"
#include "microsip_trace.h"
#include "microsip_txnhash.h"

#ifdef SIP_TXN_LAYER
#include "microsip_txntimer.h"
#include "microsip_txnstruct.h"
#include "microsip_txndecode.h"
#include "microsip_txnclone.h"
#include "microsip_txnfree.h"
#include "microsip_txninit.h"
#include "microsip_txnmidway.h"
#include <pthread.h>
#endif
#define sip_setTagInToHdr sip_setTagInFromHdr

#define MAXMESG 5000

#define HSS_FREE(x) \
do \
{ \
    if ((x!=SIP_NULL)) fast_memfree(0,x,SIP_NULL);\
} \
while(0)

#ifdef SIP_TXN_LAYER
    int                 glbtransptype = 0;
    int                 glbtransactiontype = 0;
    int                 glbenableretrans = 0;
    int                 glbsetTimerValue = 0;
    SipTimeOutValues    dTimeOut;
    SipList             glbslTxnKey;
    SipList             slMessageQueue;
    mutex_id_t          dMessageQueueMutex;
    mutex_id_t          dTxnKeyMutex;
#endif
    SipBool             sip_setTagInFromHdr (SipHeader * hdr, SIP_S8bit * tag,
                                             SipError * err);

    void                sip_freeTimerHandle (SIP_Pvoid pTimerHandle);

/* Internal Function prototypes */
#ifndef SIP_TXN_LAYER
    void                timerElementFree (SIP_Pvoid element);
    SIP_S32bit          getMinimumTimeout (SipList * list);
#endif
    char                sip_getchar (void);
    void                displayParsedMessage (SipMessage * s);

#ifdef SIP_TEL
    SipBool             do_stuff_tel (SipMessage * s);
    SipBool             do_stuff_tel_nonbyref (SipMessage * s);
#endif

#ifdef SIP_IMPP
    SipBool             do_stuff_imurl_byref (SipMessage * s);
    SipBool             do_stuff_imurl_nonbyref (SipMessage * s);
#endif

    SIP_S8bit           sip_willParseSdpLine (SIP_S8bit * in, SIP_S8bit ** out);
    SipBool             sip_acceptIncorrectSdpLine (SIP_S8bit * in);
    SIP_S8bit           sip_willParseHeader (en_HeaderType type,
                                             SipUnknownHeader * hdr);
    SipBool             sip_acceptIncorrectHeader (en_HeaderType type,
                                                   SipUnknownHeader * hdr);
    void                showErrorInformation (SipMessage * pMessage);

    SipBool             sendRemoteRetranmission (SipTxnKey * pKey,
                                                 SipTranspAddr * pAddr);
    SipBool             sendRequestOnTransaction (SipTxnKey * pKey,
                                                  int dCseqIncr, char *method,
                                                  SipTranspAddr * sendaddr,
                                                  SipError * err);
    SipBool             sendResponseOnTransaction (SipTxnKey * pKey,
                                                   int dCseqIncr, int dRpr,
                                                   int code, char *reason,
                                                   char *method,
                                                   SipTranspAddr * sentaddr,
                                                   SipError * err);
    SipBool             sendRequest (const char *method,
                                     SipTranspAddr * sendaddr, SipError * err);
    SipBool             sendResponse (int code, const char *reason,
                                      const char *method,
                                      SipTranspAddr * sendaddr, SipError * err);
    void                sendNewMessage (int c, SipTranspAddr * sendaddr);
    void                sendMessageOnTxn (SipTxnKey * pkey, int c,
                                          SipTranspAddr * sendaddr);
    void                freeTxnKeyList (SipTxnKey ** ppTxnKeys, int dCountTxn);
    void                sdf_fn_uaSleep (int dSecs, int dMicroSecs);
    void                __sip_freeSipTxnKeyElems (SipTxnKey * pTxnKey);

    SipBool             sendMidRequest (const char *method,
                                        SipTranspAddr * sendaddr,
                                        SipError * err);
    SipBool             sendMidResponse (int code, const char *reason,
                                         const char *method,
                                         SipTranspAddr * sendaddr,
                                         SipError * err);
    void                sendMidMessage (int c, SipTranspAddr * sendaddr);

/* Constants and globals */

    const char          test_tool_desc[] = "\n"
        "|----------------------------------------------------------------------|\n"
        "| SIP Stack Test Tool                                                  |\n"
        "|----------------------------------------------------------------------|\n"
        "| This test program  demonstrates the parsing  capabilities of the SIP |\n"
        "| stack. It also demonstrates the retransmission handling capabilities |\n"
        "| of the stack. This program is not a SIP user agent.                  |\n"
        "|----------------------------------------------------------------------|\n";

    const char          mainmenu[] = "\n\n"
        "|---Main Menu------------------------------------|\n"
        "| 1 : Send New Message                           |\n"
        "| 2 : Send a Message on Txn                      |\n"
        "| 3 : Test for Remote Retransmission             |\n"
        "| 4 : List all the transactions in the Hash      |\n"
        "| 5 : Abort a transaction                        |\n"
        /* For testing cancelling a transaciton */
        "| 6 : Test for cancelling a trasaction           |\n"
        "| 7 : List the TxnKeys returned from decode/send |\n"
        "| 8 : Create a MidWay transaction                |\n"
        "| q : Quit                                       |\n"
        "|                                                |\n"
        "|------------------------------------------------|\n";

    const char         *menu = "\n\n"
        "|--- Menu ------------------------------------------------------------|\n"
        "|                                                                     |\n"
        "| z: for parser options menu      q: exit test tool                   |\n"
        "|                                                                     |\n"
        "| Options for sending requests:                                       |\n"
        "| i:INVITE    b:BYE     r:REGISTER  o:OPTIONS    k:PRACK     c:CANCEL |\n"
        "| a:ACK       p:PROPOSE f:INFO      e:REFER      v:UNKNOWN            |\n"
#ifdef SIP_IMPP
        "| u:SUBSCRIBE n:NOTIFY  m:MESSAGE                                     |\n"
#endif
        "|                                                                     |\n"
        "| Options for sending responses to the request received:              |\n"
        "| 2:200   3:300  4:401                                                  |\n"
        "| 0-180   1-181   provisional response                                |\n"
        "|                                                                     |\n"
        "| Options for sending other responses for INVITE:                     |\n"
        "| S-182   s-183   reliable provisional response                       |\n"
        "|                                                                     |\n"
        "|---------------------------------------------------------------------|\n";

    const char         *midmenu = "\n\n"
        "|--- Menu ------------------------------------------------------------|\n"
        "|                                                                     |\n"
        "| Options for sending requests:                                       |\n"
        "| i:INVITE    o:OPTIONS    a:ACK                                      |\n"
        "|                                                                     |\n"
        "| Options for sending 200 OK responses:                               |\n"
        "| I:INVITE    O:OPTIONS                                               |\n"
        "|                                                                     |\n"
        "| Options for sending 100 Trying responses:                           |\n"
        "| t:INVITE                                                            |\n"
        "|                                                                     |\n"
        "| Options for sending other responses for INVITE:                     |\n"
        "| 3:300                                                               |\n"
        "| 0-180   1-181   provisional response                                |\n"
        "| 2-182   s-183   reliable provisional response                       |\n"
        "|                                                                     |\n"
        "|---------------------------------------------------------------------|\n";

    int                 showMessage = 1;
    int                 constructCLen;
    int                 glbSipOption = 0;
    FILE               *ffp;
    FILE               *fp;

/*
   Modified implementation of getchar that ensures that the enter key
   pressed in response for a getchar() does not cause the next getchar()
   to return with the enter key character.fflush seems to have inconsistent
   behaviour in LINUX
*/
    char                sip_getchar (void)
    {
        char                retVal;
                            retVal = getchar ();
        if                  (retVal != 0X0A)
        {
            char                tempVal;
                                tempVal = getchar ();
            if                  (tempVal != 0X0A)
                                    ungetc (tempVal, stdin);
        }
        return              retVal;
    }

/*
   Call back implementation
   The EventContext data structure contains data filled by the user.
   The stack does not understand the contents of this structure, but it
   often needs to release the structure. The user must implement this
   function depending on what is being passed in the pData field of
   the event-context structure.
*/
    void                sip_freeEventContext (SipEventContext * context)
    {
        if (context->pData != NULL)
            free (context->pData);
        if (context->pDirectBuffer != NULL)
            free (context->pDirectBuffer);
        free (context);
    }

/*=======================================================================

Selective parsing Call-backs

The following functions are callback functions that need to be implemented
only if the stack is compiled with the selective parsing option.

In this demonstration, they print out the header/SDP line when invoked.
In real applications they will contain logic to determine whether a header
needs to be parsed or to ignore errors based on the importance of the
header/SDP line in which the error occured.

========================================================================*/

/*
    Callback implementation
    This callback needs to be implemented if the stack is compiled with
    the SIP_SELECTIVE_PARSE option. This function will be called each time
    the stack is about to parse an SDP line. The application has the option
    of deciding on whether to ignore the line or modify it or parse it
    unmodified
*/

/*
    Callback implementation
    This callback needs to be implemented if the stack is compiled with
    the SIP_SELECTIVE_PARSE option. This function will be called each time
    the stack finds an SDP line with syntax errors. The application has
    the option of ignoring the bad line.
    If a bad line is ignored, it is stored in a list (slIncorrectLines)
    inside the SdpMessage structure. If the ignored line is part of a
    media description it is stored in list in the SdpMedia structure.
*/

/*
    Callback implementation
    This callback needs to be implemented if the stack is compiled with
    the SIP_SELECTIVE_PARSE option.
    This callback is invoked each time the parser is about to parse
    a header. The application has the option of skipping the header,
    treating it as an unknown header or parsing it normally.
    Treating the header as unknown means storing the name and the unparsed
    field value in the message structure in a list of unknown headers.
    For selectively parsing headers please also refer to the new decode
    option - SIP_OPT_SELECTIVE. This allows selection using a preset list
    without the overhead of callback function invocations. Documentation
    for the new selective parsing mechanism can be found in the API reference
    under the node for SipMessage.
*/

/*
    Callback implementation
    This callback needs to be implemented if the stack is compiled with
    the SIP_SELECTIVE_PARSE option.
    This callback is invoked each time the parser encounters an error while
    parsing a header. The application has the option of ignoring the error
    after examinig the type of the header.
    Please refer to the SIP_OPT_BADMESSAGE option for another way to ignore
    errors in messages without the use of callbacks. Documentation for this
    decode option can be found in the API reference under the node for
    SipMessage.
*/

/*=======================================================================
End of selective parsing callback implementations.
========================================================================*/

/*=======================================================================
New callback introduced in 4.0.1 for notification of retransmission
events.
========================================================================*/
#ifdef SIP_RETRANSCALLBACK
#ifdef ANSI_PROTO
#ifndef SIP_TXN_LAYER
    void                sip_indicateMessageRetransmission (SipEventContext *
                                                           pContext,
                                                           SipTimerKey * pKey,
                                                           SIP_S8bit * pBuffer,
                                                           SIP_U32bit
                                                           dBufferLength,
                                                           SipTranspAddr *
                                                           pAddr,
                                                           SIP_U8bit
                                                           retransCount,
                                                           SIP_U32bit duration)
#else
    void                sip_indicateMessageRetransmission (SipEventContext *
                                                           pContext,
                                                           SipTxnKey * pKey,
                                                           SIP_S8bit * pBuffer,
                                                           SIP_U32bit
                                                           dBufferLength,
                                                           SipTranspAddr *
                                                           pAddr,
                                                           SIP_U8bit
                                                           retransCount,
                                                           SIP_U32bit duration)
#endif
#else
#ifndef SIP_TXN_LAYER
    void                sip_indicateMessageRetransmission (pContext, pKey,
                                                           pBuffer,
                                                           dBufferLength, pAddr,
                                                           retransCount,
                                                           duration)
        SipEventContext *pContext;
    SipTimerKey        *pKey;
    SIP_S8bit          *pBuffer;
    SIP_U32bit          dBufferLength;
    SipTranspAddr      *pAddr;
    SIP_U8bit           retransCount;
    SIP_U32bit          duration;
#else
    void                sip_indicateMessageRetransmission (pContext, pKey,
                                                           pBuffer,
                                                           dBufferLength, pAddr,
                                                           retransCount,
                                                           duration)
        SipEventContext *pContext;
    SipTxnKey          *pKey;
    SIP_S8bit          *pBuffer;
    SIP_U32bit          dBufferLength;
    SipTranspAddr      *pAddr;
    SIP_U8bit           retransCount;
    SIP_U32bit          duration;
#endif
#endif
    {
        (void) pContext;
        (void) pKey;
        (void) dBufferLength;
        (void) pAddr;
        printf
            ("\n-----Message Retransmission Indication Callback--------------\n");
        printf
            ("Inside sip_indicateMessageRetransmission demo implementation.\n");
        printf ("Buffer being retransmitted (truncated to 100 bytes):\n");
        printf ("--------------------\n");
        printf ("%.100s", pBuffer);
        printf ("\n------------------");
        printf ("\nRetransmission count: %d\n", retransCount);
        printf ("Retransmission duration: %d\n", duration);
        printf
            ("-----Message Retransmission Indication Callback Ends---------\n\n");
    }
#endif

#ifndef SIP_TXN_LAYER
/* Structure definition for the user's timer implementation */
    typedef struct
    {
        SIP_S32bit          duration;
        SIP_S8bit           restart;
             
             
             
                      SipBool (*timeoutfunc) (SipTimerKey * key, SIP_Pvoid buf);
        SIP_Pvoid           buffer;
        SipTimerKey        *key;
    } TimerListElement;

    SipList             timerlist;

    void                timerElementFree (SIP_Pvoid element)
    {
        TimerListElement   *el;

        el = (TimerListElement *) element;
        sip_freeSipTimerKey (el->key);
        sip_freeSipTimerBuffer ((SipTimerBuffer *) el->buffer);
        /* free(element); */
    }
#endif

/* Implementaion of the fast_startTimer interface reqiuired by the stack
   Application developers may choose to implement this function in any
   manner while preserving the interface
*/
#ifdef ANSI_PROTO
    SipBool             fast_startTimer (SIP_U32bit duration, SIP_S8bit restart,
                                         sip_timeoutFuncPtr timeoutfunc,
                                         SIP_Pvoid buffer, SipTimerKey * key,
                                         SipError * err)
#else
    SipBool             fast_startTimer (duration, restart, timeoutfunc, buffer,
                                         key, err) SIP_U32bit duration;
    SIP_S8bit           restart;
    sip_timeoutFuncPtr  timeoutfunc;
    SIP_Pvoid           buffer;
    SipTimerKey        *key;
    SipError           *err;
#endif
    {
#ifndef SIP_TXN_LAYER
        SipError           *dummy;
        TimerListElement   *elem;
        SipError            error;
        SIP_U32bit          size;

        dummy = err;
        sip_listSizeOf (&timerlist, &size, &error);
        elem = (TimerListElement *) malloc (sizeof (TimerListElement));
        elem->duration = duration;
        elem->restart = restart;
        elem->timeoutfunc = timeoutfunc;
        elem->buffer = buffer;
        elem->key = key;

        sip_listAppend (&timerlist, elem, &error);
#else
        SIP_U32bit          dummyDuration;
        SIP_S8bit           dummyRestart;
        SIP_Pvoid           dummyBuffer;
        sip_timeoutFuncPtr  dummyTempfunc;
        SipTimerKey        *dummyTimerKey;
        SipError           *dummyError;

        dummyDuration = duration;
        dummyRestart = restart;
        dummyBuffer = buffer;
        dummyTimerKey = key;
        dummyTempfunc = timeoutfunc;
        dummyError = err;
#endif
        return SipSuccess;
    }

/* Implementaion of the fast_stopTimer interface reqiuired by the stack
   Application developers may choose to implement this function in any
   manner while preserving the interface
*/

#ifdef ANSI_PROTO
    SipBool             fast_stopTimer (SipTimerKey * inkey,
                                        SipTimerKey ** outkey,
                                        SIP_Pvoid * buffer, SipError * err)
#else
    SipBool             fast_stopTimer (inkey, outkey, buffer, err)
        SipTimerKey *inkey;
    SipTimerKey       **outkey;
    SIP_Pvoid          *buffer;
    SipError           *err;
#endif

    {
#ifndef SIP_TXN_LAYER
        TimerListElement   *elem;
        SipError            error;
        SIP_U32bit          i;
        SIP_U32bit          size;

        sip_listSizeOf (&timerlist, &size, &error);
        for (i = 0; i < size; i++)
        {
            sip_listGetAt (&timerlist, (SIP_U32bit) i, (SIP_Pvoid *) & elem,
                           &error);
            if (sip_compareTimerKeys (inkey, elem->key, err) == SipSuccess)
            {
                *outkey = elem->key;
                *buffer = elem->buffer;
                sip_listDeleteAt (&timerlist, (SIP_U32bit) i, &error);
                return SipSuccess;
            }
        }
#else
        SipTimerKey        *dummyInkey, *dummyOutkey;
        SIP_Pvoid          *dummyBuffer;
        SipError           *dummyErr;

        dummyInkey = inkey;
        dummyOutkey = *outkey;
        dummyBuffer = buffer;
        dummyErr = err;

#endif

        return SipFail;
    }

#ifndef SIP_TXN_LAYER
/* Function to get the minimum timout value from the timer list */
    SIP_S32bit          getMinimumTimeout (SipList * list)
    {
        TimerListElement   *elem;
        SipError            error;
        SIP_U32bit          size, i;
        SIP_S32bit          minval;
        SipList            *dummy;
        dummy = list;

        minval = 10000;
        sip_listSizeOf (&timerlist, &size, &error);
        for (i = 0; i < size; i++)
        {
            sip_listGetAt (&timerlist, (SIP_U32bit) i, (SIP_Pvoid *) & elem,
                           &error);
            if (elem->duration < minval)
                minval = elem->duration;
        }
        return minval;
    }
#endif
/* Implementaion of the sendToNetwork interface reqiuired by the stack
   This implementation does not respect the transptype parameter
   The buffer is dispatched via UDP irrespective of the transptype value
   Application developers may choose to implement this function in any
   manner while preserving the interface
*/

    SipBool             sip_sendToNetwork
#ifdef ANSI_PROTO
     
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        (SIP_S8bit * buffer, SIP_U32bit buflen, SipTranspAddr * addr,
         SIP_S8bit transptype, SipError * err)
#else
     
        
        
        
        
                      (buffer, buflen, addr, transptype, err) SIP_S8bit *buffer;
    SIP_U32bit          buflen;
    SipTranspAddr      *addr;
    SIP_S8bit           transptype;
    SipError           *err;
#endif
    {
        int                 sockfd;
        int                 len;
        struct sockaddr_in  cli_addr, serv_addr;
        SIP_S8bit           dummy;
        SipError           *err_dummy;

        dummy = transptype;
        err_dummy = err;

        bzero ((char *) &serv_addr, sizeof (serv_addr));
        serv_addr.sin_family = AF_INET;
        serv_addr.sin_addr.s_addr = inet_addr (addr->dIpv4);
        serv_addr.sin_port = htons (addr->dPort);

        if ((transptype & SIP_UDP) == SIP_UDP)
        {
            if ((sockfd = socket (AF_INET, SOCK_DGRAM, 0)) < 0)
            {
                printf ("Client : Could not open datagram socket.\n");
                close (sockfd);
                exit (0);
            }
        }
        else if ((transptype & SIP_TCP) == SIP_TCP)
        {
            if ((sockfd = socket (AF_INET, SOCK_STREAM, 0)) < 0)
            {
                printf ("Client : Could not open datagram socket.\n");
                close (sockfd);
                exit (0);
            }
        }
        else
        {
            sockfd = 0;
            printf ("No Transport Type specified\n");
            exit (0);
        }

        bzero ((char *) &cli_addr, sizeof (cli_addr));    /* zero out */
        cli_addr.sin_family = AF_INET;
        cli_addr.sin_addr.s_addr = htonl (INADDR_ANY);
        cli_addr.sin_port = htons (0);

        if (bind (sockfd, (struct sockaddr *) &cli_addr, sizeof (cli_addr)) < 0)
        {
            printf ("Client : Could not bind to local address.\n");
            close (sockfd);
            exit (0);
        }

        if ((transptype & SIP_UDP) == SIP_UDP)
        {
            len = sendto (sockfd, buffer, buflen, 0, (struct sockaddr *)
                          &serv_addr, sizeof (serv_addr));
            if (len == -1 || (unsigned int) len != buflen)
            {
                printf ("Client : send to failed.\n");
                close (sockfd);
                exit (0);
            }
        }
        else if ((transptype & SIP_TCP) == SIP_TCP)
        {
            if (connect (sockfd, (struct sockaddr *) &serv_addr,
                         sizeof (serv_addr)) < 0)
            {
                printf ("Client : send to failed.\n");
                close (sockfd);
                exit (0);
            }
            if (write (sockfd, buffer, buflen) != (SIP_S32bit) buflen)
            {
                printf ("Client : send to failed.\n");
                close (sockfd);
                exit (0);
            }
        }
        close (sockfd);

        return SipSuccess;
    }
#ifdef SIP_TEL
#ifndef SIP_BY_REFERENCE
    SipBool             do_stuff_tel_nonbyref (SipMessage * s)
    {
        SipHeader          *hdr;
        SipAddrSpec        *addrspec;
        TelUrl             *telUrl;
        SipError            err;

    /*****handling the from header*****/
        /*initialize the from header */
        if (sip_initSipHeader (&hdr, SipHdrTypeFrom, &err) == SipFail)
        {
            printf ("##TEL:##Error in initializing FROM Header; ERRNO::%d\n",
                    err);
            return SipFail;
        }
        /*get the FROM header from the message */
        if (sip_getHeader (s, SipHdrTypeFrom, hdr, &err) == SipFail)
        {
            printf ("##TEL:##Error in accessing FROM Header; ERRNO::%d\n", err);
            return SipFail;
        }

        if (sip_initSipAddrSpec (&addrspec, SipAddrReqUri, &err) == SipFail)
        {
            printf ("##TEL:##Error in initializing AddrSpec in From Header; \
             ERRNO::%d\n", err);
            sip_freeSipHeader (hdr);
            return SipFail;
        }
        if (sip_getAddrSpecFromFromHdr (hdr, addrspec, &err) == SipFail)
        {
            printf ("##TEL:##Error in accessing AddrSpec from FROM Header;NON BY REF \
         ERRNO::%d\n",
                    err);
            sip_freeSipHeader (hdr);
            sip_freeSipAddrSpec (addrspec);
            return SipFail;
        }
        if (SipSuccess == sip_isTelUrl (addrspec, &err))
        {
            if ((sip_initTelUrl (&telUrl, &err)) == SipFail)
            {
                printf ("##TEL:##Error in initializing TelUrl from AddrSpec \
                     NON BY REF in FROMHeader; ERRNO::%d\n", err);
                sip_freeSipAddrSpec (addrspec);
                sip_freeSipHeader (hdr);
                return SipFail;

            }
            if (sip_getTelUrlFromAddrSpec (addrspec, telUrl, &err) == SipFail)
            {
                printf ("##TEL:##Error in accessing TelUrl from AddrSpec NON BY REF \
                 in FROMHeader; ERRNO::%d\n",
                        err);
                sip_freeSipAddrSpec (addrspec);
                sip_freeSipHeader (hdr);
                sip_freeTelUrl (telUrl);
                return SipFail;
            }
            else
            {
                printf ("##TEL:##Retrieved TEL URL from AddrSpec in \
                FROM Header---NON BY REF\n");
                if (sip_setTelUrlInAddrSpec (addrspec, telUrl, &err) == SipFail)
                {
                    printf ("##TEL:##Error in setting the TEL URL in AddrSpec in\
                     FROM Header---NON BY REF; ERRNO::%d\n",
                            err);
                    sip_freeTelUrl (telUrl);
                    sip_freeSipAddrSpec (addrspec);
                    sip_freeSipHeader (hdr);
                    return SipFail;

                }
                else
                {
                    printf ("##TEL:##Set the TEL URL in AddrSpec in FROM Header\
                    ---NON BY REF\n");
                    sip_freeTelUrl (telUrl);
                    sip_freeSipAddrSpec (addrspec);
                    sip_freeSipHeader (hdr);
                }
            }
            printf ("##TEL:##Parsed Tel Url successfully\n");
        }
        else
        {
            sip_freeSipAddrSpec (addrspec);
            sip_freeSipHeader (hdr);
        }
        return SipSuccess;
    }
#else
/*Fucntion called inside displayParsedMessage () to check the parsing of tel url structure*/

    SipBool             do_stuff_tel (SipMessage * s)
    {
        SipHeader           hdr;
        SipAddrSpec        *addrspec;
        TelUrl             *telUrl;
        SipError            err;
/* handling for from header */
        if (sip_getHeader (s, SipHdrTypeFrom, &hdr, &err) == SipFail)
        {
            if (err != E_NO_EXIST)
            {
                printf
                    ("##TEL:##Error in Accessing From header; Error no: %d\n",
                     err);
                return SipFail;
            }
        }
        else
        {
            if (sip_getAddrSpecFromFromHdr (&hdr, &addrspec, &err) == SipFail)
            {
                printf
                    ("##TEL:##Error in accessing AddrSpec from FROM Header; ERRNO::%d\n",
                     err);
                sip_freeSipFromHeader ((SipFromHeader *) (hdr.pHeader));
                return SipFail;
            }
            if (SipSuccess == sip_isTelUrl (addrspec, &err))
            {
                if (sip_getTelUrlFromAddrSpec (addrspec, &telUrl, &err) ==
                    SipFail)
                {
                    printf
                        ("##TEL:##Error in accessing TelUrl from AddrSpec in FROMHeader; ERRNO::%d\n",
                         err);
                    sip_freeSipAddrSpec (addrspec);
                    sip_freeSipFromHeader ((SipFromHeader *) (hdr.pHeader));
                    return SipFail;
                }
                else
                {
                    printf
                        ("##TEL:##Retrieved TEL URL from AddrSpec in FROM Header\n");
                    if (sip_setTelUrlInAddrSpec (addrspec, telUrl, &err) ==
                        SipFail)
                    {
                        printf
                            ("##TEL:##Error in setting the TEL URL in AddrSpec in FROM Header; ERRNO::%d\n",
                             err);
                        sip_freeTelUrl (telUrl);
                        sip_freeSipAddrSpec (addrspec);
                        sip_freeSipFromHeader ((SipFromHeader *) (hdr.pHeader));
                        return SipFail;

                    }
                    else
                    {
                        printf
                            ("##TEL:##Set the TEL URL in AddrSpec in FROM Header\n");
                        sip_freeTelUrl (telUrl);
                        sip_freeSipAddrSpec (addrspec);
                        sip_freeSipFromHeader ((SipFromHeader *) (hdr.pHeader));
                    }
                }
                printf ("##TEL:##Parsed Tel Url successfully\n");
            }
            else
            {
                sip_freeSipAddrSpec (addrspec);
                sip_freeSipFromHeader ((SipFromHeader *) (hdr.pHeader));
            }
        }
        return SipSuccess;
    }
#endif
#endif

/* Parsing IM-URL  */
#if 0
#ifdef SIP_IMPP
#ifndef SIP_BY_REFERENCE
    SipBool             do_stuff_imurl_nonbyref (SipMessage * s)
    {
        SipHeader          *hdr;
        SipAddrSpec        *addrspec;
        ImUrl              *pImUrl;
        SipError            err;

    /*****handling the from header*****/
        /*initialize the from header */
        if (sip_initSipHeader (&hdr, SipHdrTypeFrom, &err) == SipFail)
        {
            printf ("##IM-URL:##Error in initializing FROM Header; ERRNO::%d\n",
                    err);
            return SipFail;
        }
        /*get the FROM header from the message */
        if (sip_getHeader (s, SipHdrTypeFrom, hdr, &err) == SipFail)
        {
            printf ("##IM-URL:##Error in accessing FROM Header; ERRNO::%d\n",
                    err);
            return SipFail;
        }

        if (sip_initSipAddrSpec (&addrspec, SipAddrReqUri, &err) == SipFail)
        {
            printf ("##IM-URL:##Error in initializing AddrSpec in From Header; \
             ERRNO::%d\n", err);
            sip_freeSipHeader (hdr);
            return SipFail;
        }
        if (sip_getAddrSpecFromFromHdr (hdr, addrspec, &err) == SipFail)
        {
            printf ("##IM-URL:##Error in accessing AddrSpec from FROM Header;NON BY REF \
         ERRNO::%d\n",
                    err);
            sip_freeSipHeader (hdr);
            sip_freeSipAddrSpec (addrspec);
            return SipFail;
        }
        if (SipSuccess == sip_isImUrl (addrspec, &err))
        {
            if ((sip_initImUrl (&pImUrl, &err)) == SipFail)
            {
                printf ("##IM-URL:##Error in initializing ImUrl from AddrSpec \
                     NON BY REF in FROMHeader; ERRNO::%d\n", err);
                sip_freeSipAddrSpec (addrspec);
                sip_freeSipHeader (hdr);
                return SipFail;

            }
            if (sip_getImUrlFromAddrSpec (addrspec, pImUrl, &err) == SipFail)
            {
                printf ("##IM-URL:##Error in accessing ImUrl from AddrSpec NON BY REF \
                 in FROMHeader; ERRNO::%d\n",
                        err);
                sip_freeSipAddrSpec (addrspec);
                sip_freeSipHeader (hdr);
                sip_freeImUrl (pImUrl);
                return SipFail;
            }
            else
            {
                printf ("##IM-URL:##Retrieved IM URL from AddrSpec in \
                FROM Header---NON BY REF\n");
                if (sip_setImUrlInAddrSpec (addrspec, pImUrl, &err) == SipFail)
                {
                    printf ("##IM-URLL:##Error in setting the IM URL in AddrSpec in\
                     FROM Header---NON BY REF; ERRNO::%d\n",
                            err);
                    sip_freeImUrl (pImUrl);
                    sip_freeSipAddrSpec (addrspec);
                    sip_freeSipHeader (hdr);
                    return SipFail;
                }
                else
                {
                    printf
                        ("##IM-URL:##Set the IM URL in AddrSpec in FROM Header\
                    ---NON BY REF\n");
                    sip_freeImUrl (pImUrl);
                    sip_freeSipAddrSpec (addrspec);
                    sip_freeSipHeader (hdr);
                }
            }
            printf ("##IM-URL:##Parsed IM URL successfully\n");
        }
        else
        {
            sip_freeSipAddrSpec (addrspec);
            sip_freeSipHeader (hdr);
        }
        return SipSuccess;
    }
#else
/*Fucntion called inside displayParsedMessage () to check the parsing of im-url structure BY_REF case */

    SipBool             do_stuff_imurl_byref (SipMessage * s)
    {
        SipHeader           hdr;
        SipAddrSpec        *addrspec;
        ImUrl              *pImUrl;
        SipError            err;
/* handling for from header */
        if (sip_getHeader (s, SipHdrTypeFrom, &hdr, &err) == SipFail)
        {
            if (err != E_NO_EXIST)
            {
                printf
                    ("##IM-URL:##Error in Accessing From header; Error no: %d\n",
                     err);
                return SipFail;
            }
        }
        else
        {
            if (sip_getAddrSpecFromFromHdr (&hdr, &addrspec, &err) == SipFail)
            {
                printf
                    ("##IM-URL:##Error in accessing AddrSpec from FROM Header; ERRNO::%d\n",
                     err);
                sip_freeSipFromHeader ((SipFromHeader *) (hdr.pHeader));
                return SipFail;
            }
            if (SipSuccess == sip_isImUrl (addrspec, &err))
            {
                if (sip_getImUrlFromAddrSpec (addrspec, &pImUrl, &err) ==
                    SipFail)
                {
                    printf
                        ("##IM-URL:##Error in accessing ImUrl from AddrSpec in FROMHeader; ERRNO::%d\n",
                         err);
                    sip_freeSipAddrSpec (addrspec);
                    sip_freeSipFromHeader ((SipFromHeader *) (hdr.pHeader));
                    return SipFail;
                }
                else
                {
                    printf
                        ("##IM-URL:##Retrieved IM URL from AddrSpec in FROM Header\n");
                    if (sip_setImUrlInAddrSpec (addrspec, pImUrl, &err) ==
                        SipFail)
                    {
                        printf
                            ("##IM-URL:##Error in setting the TIM URL in AddrSpec in FROM Header; ERRNO::%d\n",
                             err);
                        sip_freeImUrl (pImUrl);
                        sip_freeSipAddrSpec (addrspec);
                        sip_freeSipFromHeader ((SipFromHeader *) (hdr.pHeader));
                        return SipFail;

                    }
                    else
                    {
                        printf
                            ("##IM-URL:##Set the IM URL in AddrSpec in FROM Header\n");
                        sip_freeImUrl (pImUrl);
                        sip_freeSipAddrSpec (addrspec);
                        sip_freeSipFromHeader ((SipFromHeader *) (hdr.pHeader));
                    }
                }
                printf ("##IM-URL:##Parsed IM URL successfully\n");
            }
            else
            {
                sip_freeSipAddrSpec (addrspec);
                sip_freeSipFromHeader ((SipFromHeader *) (hdr.pHeader));
            }
        }
        return SipSuccess;
    }
#endif
#endif
/*Remove until here if u dont need IMURL */
#endif

/* end of parsing IM-URL */

    void                headerListFreeFunction (void *pData);
    void                headerListFreeFunction (void *pData)
    {
        sip_freeSipHeader ((SipHeader *) pData);
#ifdef SIP_BY_REFERENCE
        fast_memfree (0, pData, SIP_NULL);
#endif
    }

/* Function called inside all callbacks to print out
   the message received. The message is available in the decoded form
   to the callbacks. This function converts the structure back into
   a text message and prints it out.
*/
    void                displayParsedMessage (SipMessage * s)
    {
        char               *out;
        char                fullForm;
        SipError            err;
        SipBool             res;
        SIP_U32bit          length;
        SipOptions          options;
#ifdef SIP_TEL
        SipBool             restel;
#endif

        out = (char *) malloc (64000);
        if (!out)
        {
            printf ("Memory allocation error\n");
            exit (0);
        }

        options.dOption = SIP_OPT_SINGLE | SIP_OPT_AUTHCANONICAL;
        if (glbSipOption == 1)
        {
            options.dOption = 0;
            fflush (stdin);
            printf
                ("\n Use short form/full form for headers in formed message ?(s/f): [f] ");
            fullForm = sip_getchar ();
            if ((fullForm == 's') || (fullForm == 'S'))
                options.dOption |= SIP_OPT_SHORTFORM;
            else
                options.dOption |= SIP_OPT_FULLFORM;
            fflush (stdin);
            printf ("\n Use comma separated headers ?(y/n): [n] ");
            fullForm = sip_getchar ();
            if ((fullForm == 'y') || (fullForm == 'Y'))
                options.dOption |= SIP_OPT_COMMASEPARATED;
            else
                options.dOption |= SIP_OPT_SINGLE;
            fflush (stdin);
            printf ("\n Enable content-length correction ?(y/n): [y] ");
            fullForm = sip_getchar ();
            if ((fullForm == 'n') || (fullForm == 'N'))
            {
            }
            else
                options.dOption |= SIP_OPT_CLEN;
        }
        glbSipOption = 0;
#ifdef SIP_TEL
        /*testing of the tel-url structure */
#ifdef SIP_BY_REFERENCE
        printf ("*****Testing The TEL-URL BY REFERNCE MODE*****\n");
        restel = do_stuff_tel (s);
#else
        printf ("*****Testing The TEL-URL NON BY REFERNCE MODE*****\n");
        restel = do_stuff_tel_nonbyref (s);
#endif
        if (restel != SipSuccess)
        {
            printf ("##TEL:##Error in parsing Tel Url\n");
            free (out);
            sip_freeSipMessage (s);
            return;
        }
#endif
#if 0
#ifdef SIP_IMPP
        /*testing of the im-url structure */
#ifdef SIP_BY_REFERENCE
        printf ("*****Testing The IM-URL BY REFERNCE MODE*****\n");
        resimurl = do_stuff_imurl_byref (s);
#else
        printf ("*****Testing The IM-URL NON BY REFERNCE MODE*****\n");
        resimurl = do_stuff_imurl_nonbyref (s);
#endif
        if (resimurl != SipSuccess)
        {
            printf ("##IM-URL:##Error in parsing IM-URL\n");
            free (out);
            sip_freeSipMessage (s);
            return;
        }
#endif
/*Remove this part if you dont need IMURL */
#endif
        res = sip_formMessage (s, &options, out, &length, &err);
        if (res != SipSuccess)
        {
            printf ("\nError in SipFormMessage: %d\n", err);
            free (out);
            sip_freeSipMessage (s);
            return;
        }
        if (showMessage)
            printf ("\n|==Decoded Message====================================="
                    "========================|\n\n");

        write (2, out, length);

        printf ("\n|==Decoded Message Ends================================"
                "========================|\n");

        /*showErrorInformation(s); */
        free (out);
        sip_freeSipMessage (s);
/*    fast_memfree(0,s,SIP_NULL);*/
    }

/* These are the callbacks which which the stack calls when
   it successfully decodes a SIP message */
#ifdef SIP_TXN_LAYER

/****************************************************************************
** FUNCTION: sip_indicateTimeout 
**
** DESCRIPTION:
**        Callback function to be implemented by the application.
**        Invoked if a message that was sent through UDP with 
**        retransmissions did not receive a matching response
**        after the maximum retransmissions have been sent.
**
** Parameters:
**        context(IN):
**            The event context structure that was passed to the 
**            sip_sendMessage invocation that resulted in this
**            callback.
**
**
**        dTimer(IN):Provides information to the application that 
**                the firing of which timer caused the timeOut    
**
** ReturnValue:
**        SipBool    
**
****************************************************************************/

#ifdef ANSI_PROTO
    void                sip_indicateTimeOut (SipEventContext * context,
                                             en_SipTimerType dTimer)
#else
    void                sip_indicateTimeOut (context, dTimer)
        SipEventContext *context;
    en_SipTimerType     dTimer;
#endif
#else
#ifdef ANSI_PROTO
    void                sip_indicateTimeOut (SipEventContext * context)
#else
    void                sip_indicateTimeOut (context) SipEventContext *context;
#endif
#endif

    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside retransmission timeout callback------\n");
        fflush (stdout);
        if (context->pData)
            printf ("%s timed out.\n", (char *) context->pData);
#ifdef SIP_TXN_LAYER
        switch (dTimer)
        {
            case SipTimerA_B:
                printf ("***TimerA_B has been TimedOut******\n");
                break;
            case SipTimerB:
                printf ("***TimerB has been TimedOut******\n");
                break;
            case SipTimerC:
                printf ("***TimerC has been TimedOut******\n");
                break;
            case SipTimerD:
                printf ("***TimerD has been TimedOut******\n");
                break;
            case SipTimerE_F:
                printf ("***TimerE_F has been TimedOut******\n");
                break;
            case SipTimerF:
                printf ("***TimerF has been TimedOut******\n");
                break;
            case SipTimerG_H:
                printf ("***TimerG_H has been TimedOut******\n");
                break;
            case SipTimerH:
                printf ("***TimerH has been TimedOut******\n");
                break;
            case SipTimerI:
                printf ("***TimerI has been TimedOut******\n");
                break;
            case SipTimerJ:
                printf ("***TimerJ has been TimedOut******\n");
                break;
            case SipTimerK:
                printf ("***TimerK has been TimedOut******\n");
                break;
            default:
                printf ("***Unknown Timer******\n");
        }
        fflush (stdout);
        /*
         * For firing of all timers we need to free the event context that comes
         * here. Only for the firing of TimerC we must not do so, since after the
         * TimerC fires there is a timer TimerCr that fires which requires the
         * presence of the EventContext
         */
        if (dTimer != SipTimerC)
#endif
            sip_freeEventContext (context);
        fflush (stdout);
    }

#ifndef SIP_NO_CALLBACK
    void                sip_indicateInvite (SipMessage * s,
                                            SipEventContext * context)
    {
        SipError            err;
        SIP_U32bit          count;
        SipEventContext    *dummy;
        dummy = context;

        printf ("Inside INVITE callback------\n");

        /* Following code demonstrates accessor API usage to access elements
           from the Expires header if one is found in the INVITE message */
        /* Check if the message has an Expires header */

        if (sip_getHeaderCount (s, SipHdrTypeExpiresAny, &count, &err) ==
            SipSuccess)
        {
#ifdef SIP_BY_REFERENCE
            if (count == 1)
            {
                SipHeader          *hdr;
                SipDateStruct      *datestruct;
                en_ExpiresType      etype;
                SIP_U32bit          seconds;

                printf ("Message contains Expires header.\n");
                /* SipHeader initialized as type SipHdrTypeAny for references */
                if (sip_initSipHeader (&hdr, SipHdrTypeAny, &err) != SipSuccess)
                {
                    printf ("Error in initializing Expires Header\n");
                    return;
                }
                if (sip_getHeader (s, SipHdrTypeExpiresAny, hdr, &err) !=
                    SipSuccess)
                {
                    printf ("Error in accessing Expires Header\n");
                    sip_freeSipHeader (hdr);
                    return;
                }
                if (sip_getTypeFromExpiresHdr (hdr, &etype, &err) != SipSuccess)
                {
                    printf ("Error in accessing Type from Expires Header\n");
                    sip_freeSipHeader (hdr);
                    return;
                }
                switch (etype)
                {
                    case SipExpDate:
                        /* No init for structure required in reference mode */
                        /* get function takes pointer to pointer */
                        if (sip_getDateStructFromExpiresHdr
                            (hdr, &datestruct, &err) == SipSuccess)
                        {
                            /* Get dateformat and timeformat from the datestruct */
                            SipDateFormat      *dformat;
                            SipTimeFormat      *tformat;
                            SIP_U16bit          year, month;
                            SIP_U8bit           date;
                            SIP_S8bit           hour, minute, sec;
                            en_Month            emonth;

                            sip_getDateFormatFromDateStruct (datestruct,
                                                             &dformat, &err);
                            sip_getTimeFormatFromDateStruct (datestruct,
                                                             &tformat, &err);
                            sip_getDayFromDateFormat (dformat, &date, &err);
                            sip_getYearFromDateFormat (dformat, &year, &err);
                            sip_getMonthFromDateFormat (dformat, &emonth, &err);
                            sip_getHourFromTimeFormat (tformat, &hour, &err);
                            sip_getMinFromTimeFormat (tformat, &minute, &err);
                            sip_getSecFromTimeFormat (tformat, &sec, &err);
                            month = (SIP_U16bit) emonth + 1;
                            printf
                                ("INVITE to expire on %d/%d/%d at %d:%d:%d\n",
                                 date, month, year, hour, minute, sec);
                            sip_freeSipDateFormat (dformat);
                            sip_freeSipTimeFormat (tformat);
                        }
                        sip_freeSipDateStruct (datestruct);
                        break;
                    case SipExpSeconds:
                        if (sip_getSecondsFromExpiresHdr (hdr, &seconds, &err)
                            == SipSuccess)
                        {
                            printf ("INVITE to expire in %d seconds\n",
                                    seconds);
                        }
                        break;
                    default:
                        break;
                }                /* switch */
                sip_freeSipHeader (hdr);
                free (hdr);
            }                    /* if count = 1 */
#else
            if (count == 1)
            {
                SipHeader          *hdr;
                SipDateStruct      *datestruct;
                en_ExpiresType      etype;
                SIP_U32bit          seconds;

                printf ("Message contains Expires header.\n");
                if (sip_initSipHeader (&hdr, SipHdrTypeExpiresAny, &err) !=
                    SipSuccess)
                {
                    printf ("Error in initializing Expires Header\n");
                    return;
                }
                if (sip_getHeader (s, SipHdrTypeExpiresAny, hdr, &err) !=
                    SipSuccess)
                {
                    printf ("Error in accessing Expires Header\n");
                    sip_freeSipHeader (hdr);
                    return;
                }
                if (sip_getTypeFromExpiresHdr (hdr, &etype, &err) != SipSuccess)
                {
                    printf ("Error in accessing Type from Expires Header\n");
                    sip_freeSipHeader (hdr);
                    return;
                }
                switch (etype)
                {
                    case SipExpDate:
                        if (sip_initSipDateStruct (&datestruct, &err) !=
                            SipSuccess)
                        {
                            printf
                                ("Error in accessing Date from Expires Header\n");
                            sip_freeSipHeader (hdr);
                            return;
                        }
                        if (sip_getDateStructFromExpiresHdr
                            (hdr, datestruct, &err) == SipSuccess)
                        {
                            /* Get dateformat and timeformat from the datestruct */
                            SipDateFormat      *dformat;
                            SipTimeFormat      *tformat;
                            SIP_U16bit          year, month;
                            SIP_U8bit           date;
                            SIP_S8bit           hour, minute, sec;
                            en_Month            emonth;

                            sip_initSipDateFormat (&dformat, &err);
                            sip_initSipTimeFormat (&tformat, &err);
                            sip_getDateFormatFromDateStruct (datestruct,
                                                             dformat, &err);
                            sip_getTimeFormatFromDateStruct (datestruct,
                                                             tformat, &err);
                            sip_getDayFromDateFormat (dformat, &date, &err);
                            sip_getYearFromDateFormat (dformat, &year, &err);
                            sip_getMonthFromDateFormat (dformat, &emonth, &err);
                            sip_getHourFromTimeFormat (tformat, &hour, &err);
                            sip_getMinFromTimeFormat (tformat, &minute, &err);
                            sip_getSecFromTimeFormat (tformat, &sec, &err);
                            month = (SIP_U16bit) emonth + 1;
                            printf
                                ("INVITE to expire on %d/%d/%d at %d:%d:%d\n",
                                 date, month, year, hour, minute, sec);
                            sip_freeSipDateFormat (dformat);
                            sip_freeSipTimeFormat (tformat);
                        }
                        sip_freeSipDateStruct (datestruct);
                        break;
                    case SipExpSeconds:
                        if (sip_getSecondsFromExpiresHdr (hdr, &seconds, &err)
                            == SipSuccess)
                        {
                            printf ("INVITE to expire in %d seconds\n",
                                    seconds);
                        }
                        break;
                    default:
                        break;
                }                /* switch */
                sip_freeSipHeader (hdr);
            }                    /* if count = 1 */
#endif
        }
        displayParsedMessage (s);
    }

#ifdef SIP_DCS
    void                sip_indicateComet (SipMessage * s,
                                           SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside PRECONDITION-MET callback----\n");
        displayParsedMessage (s);
    }
#endif

    void                sip_indicateRegister (SipMessage * s,
                                              SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside REGISTER callback----\n");
        displayParsedMessage (s);
    }

    void                sip_indicateCancel (SipMessage * s,
                                            SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside CANCEL callback------\n");
        displayParsedMessage (s);
    }

    void                sip_indicateOptions (SipMessage * s,
                                             SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside OPTIONS callback-----\n");
        displayParsedMessage (s);
    }

    void                sip_indicateBye (SipMessage * s,
                                         SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside BYE callback---------\n");
        displayParsedMessage (s);
    }

    void                sip_indicateRefer (SipMessage * s,
                                           SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside REFER callback---------\n");
        displayParsedMessage (s);
    }

    void                sip_indicateAck (SipMessage * s,
                                         SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside ACK callback---------\n");
        displayParsedMessage (s);
    }

    void                sip_indicateInfo (SipMessage * s,
                                          SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside INFO callback---------\n");
        displayParsedMessage (s);
    }

    void                sip_indicatePropose (SipMessage * s,
                                             SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside PROPOSE callback---------\n");
        displayParsedMessage (s);
    }

    void                sip_indicatePrack (SipMessage * s,
                                           SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside PRACK callback---------\n");
        displayParsedMessage (s);
    }

#ifdef SIP_IMPP
    void                sip_indicateSubscribe (SipMessage * s,
                                               SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside SUBSCRIBE callback---------\n");
        displayParsedMessage (s);
    }

    void                sip_indicateNotify (SipMessage * s,
                                            SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside NOTIFY callback---------\n");
        displayParsedMessage (s);
    }

    void                sip_indicateMessage (SipMessage * s,
                                             SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside MESSAGE callback---------\n");
        displayParsedMessage (s);
    }
#endif

    void                sip_indicateUnknownRequest (SipMessage * s,
                                                    SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside UNKNOWN REQUEST callback\n");
        displayParsedMessage (s);
    }

    void                sip_indicateInformational (SipMessage * s,
                                                   SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside Informational callback\n");
        displayParsedMessage (s);
    }

    void                sip_indicateFinalResponse (SipMessage * s,
                                                   SipEventContext * context)
    {
        SipEventContext    *dummy;
        dummy = context;
        printf ("Inside Final callback-------\n");
        displayParsedMessage (s);
    }

/* of SIP_NO_CALLBACK check */
#endif

    SipTranspAddr       sendaddr;
    int                 myPort;
    void               *listenToNetwork (void *ptr);
    void               *displayInterface (void *ptr);
    void               *decodeMessageStats (void *ptr);

    void                sdf_fn_uaSleep (int dSecs, int dMicroSecs)
    {
        struct timeval      tv;

        tv.tv_sec = dSecs;
        tv.tv_usec = dMicroSecs;
        select (1, NULL, NULL, NULL, &tv);
    }

/* 
 * Only the Txn Key Elements are released 
 */
    void                __sip_freeSipTxnKeyElems
#ifdef ANSI_PROTO
                        (SipTxnKey * pKey)
#else
                        (pKey) SipTxnKey *pKey;
#endif
    {
        if (pKey == SIP_NULL)
            return;
        HSS_LOCKREF (pKey->dRefCount);
        HSS_DECREF (pKey->dRefCount);
        if (HSS_CHECKREF (pKey->dRefCount))
        {
            HSS_FREE (pKey->pMethod);
            HSS_FREE (pKey->pToTag);
            HSS_FREE (pKey->pFromTag);

            HSS_FREE (pKey->pViaBranch);
            HSS_FREE (pKey->pCallid);
            if (pKey->pRackHdr)
                sip_freeSipRackHeader (pKey->pRackHdr);
        }
        else
        {
            HSS_UNLOCKREF (pKey->dRefCount);
        }

    }

    int                 main (int argc, char *argv[])
    {
        SipError            error;
        char                inputtype;
        int                 i = 0;
        int                 noOfDecodethreads = 0;
        thread_id_t         dTimerThread;
        thread_id_t         dDecodeThread[10];
        SipEventContext     context;
        SipOptions          opt;
        SipTxnContext       txncontext;
#ifdef SIP_INCREMENTAL_PARSING
        int                 j;
#endif

        if (argc < 3)
        {
            printf ("Usage:\n");
            printf ("%s my_port dest_address [dest_port]\n", argv[0]);
            exit (0);
        }

        strncpy (sendaddr.dIpv4, argv[2], 15);
        sendaddr.pHost = SIP_NULL;
        if (argc >= 4)
            sendaddr.dPort = atoi (argv[3]);
        else
            sendaddr.dPort = 5060;
        myPort = atoi (argv[1]);
#ifdef SIP_INCREMENTAL_PARSING
        context.pList = SIP_NULL;
        if (context.pList == SIP_NULL)
            context.pList = (SipHdrTypeList *)
                fast_memget (0, sizeof (SipHdrTypeList), SIP_NULL);
        for (j = 0; j < HEADERTYPENUM; j++)
            context.pList->enable[j] = SipSuccess;

#endif

        opt.dOption = 0;
        context.pData = SIP_NULL;
        context.pDirectBuffer = SIP_NULL;

        /* Stack and trace initialization */
        sip_initStack ();

        printf ("\nSip stack product id is:%s\n", (char *) (sip_getPart ()));
        printf ("%s", test_tool_desc);

        if (sip_setErrorLevel (SIP_Major | SIP_Minor | SIP_Critical, &error) ==
            SipFail)
            printf ("########## Error Disabled at compile time #####\n");

        if (sip_setTraceLevel (SIP_Brief, &error) == SipFail)
            printf ("########## Trace Disabled at compile time #####\n");

        sip_setTraceType (SIP_All, &error);

/* For Selecting the transsaction type (UA/Proxy)*/
        fflush (stdout);
        printf ("1 :\tUA\n");
        printf ("2 :\tProxy\n");
        printf ("Select the type of Transaction(Default:1):");
        fflush (stdin);
        fflush (stdout);
        inputtype = sip_getchar ();
        if (inputtype != '2')
            glbtransactiontype = 0;
        else
            glbtransactiontype = 1;

        if (glbtransactiontype)
        {
            /* For Selecting the transsaction type for Proxy */
            fflush (stdout);
            printf ("1 :\tStateful Proxy\n");
            printf ("2 :\tStateless Proxy\n");
            printf ("Select the type of Transaction(Default:1):");
            fflush (stdin);
            inputtype = sip_getchar ();
            if (inputtype != '2')
                glbtransactiontype = 1;
            else
                glbtransactiontype = 2;
        }

/* For Selecting the transport type  TCP/UDP */
        fflush (stdout);
        printf ("\n");
        printf ("1 :\tUDP\n");
        printf ("2 :\tTCP\n");
        printf ("Select the type of transport(Default:1):");
        fflush (stdin);
        inputtype = sip_getchar ();
        if (inputtype == '2')
            glbtransptype = SIP_TCP;
        else
            glbtransptype = SIP_UDP;

/* For selecting whether remote-retransmission Testing needs be enabled*/
        if (glbtransactiontype != 2)
        {
            fflush (stdout);
            printf ("\n");
            printf ("Enable Remote-Retransmission-Testing(y/n):");
            fflush (stdin);
            inputtype = sip_getchar ();
            if (inputtype == 'y' || inputtype == 'Y')
                glbenableretrans = 1;
            else
                glbenableretrans = 0;
        }
        else
        {
            glbenableretrans = 0;
        }

/* Filling up the event context*/
        txncontext.pEventContext = &context;
        txncontext.dTxnType = (en_SipTxnType) glbtransactiontype;
        txncontext.txnOption.dOption = opt;
        txncontext.txnOption.dTimerOption = 0;

        /* For Selecting the timer values */
        fflush (stdout);
        printf ("\n\nDo you want to change the Defualt Timer Values(y/n):");
        fflush (stdin);
        inputtype = sip_getchar ();
        if (inputtype == 'y' || inputtype == 'Y')
        {
            int                 dTimerValue;
            glbsetTimerValue = 1;
            dTimeOut.dT1 = SIP_DEFAULT_T1;
            dTimeOut.dT2 = SIP_DEFAULT_T2;
            dTimeOut.dTimerB = SIP_DEFAULT_B;
            dTimeOut.dTimerC = SIP_DEFAULT_C;
            dTimeOut.dTimerD_T3 = SIP_DEFAULT_D_T3;
            dTimeOut.dTimerF_T3 = SIP_DEFAULT_F_T3;
            dTimeOut.dTimerH = SIP_DEFAULT_H;
            dTimeOut.dTimerI_T4 = SIP_DEFAULT_I_T4;
            dTimeOut.dTimerJ_T3 = SIP_DEFAULT_J_T3;
            dTimeOut.dTimerK_T4 = SIP_DEFAULT_K_T4;
            fflush (stdin);
            printf ("\nDefault Value of Timer T1:%d\n", SIP_DEFAULT_T1);
            printf ("Do You want to change the timer value(y/n):");
            inputtype = sip_getchar ();
            if (inputtype == 'y' || inputtype == 'Y')
            {
                printf ("Enter the customized value for T1:");
                fflush (stdout);
                dTimerValue = 0;
                scanf ("%d", &dTimerValue);
                if (dTimerValue)
                    dTimeOut.dT1 = dTimerValue;
            }
            fflush (stdin);

            printf ("\nDefault Value of Timer T2:%d\n", SIP_DEFAULT_T2);
            printf ("Do You want to change the timer value(y/n):");
            inputtype = sip_getchar ();
            if (inputtype == 'y' || inputtype == 'Y')
            {
                printf ("Enter the customized value for T2:");
                fflush (stdout);
                dTimerValue = 0;
                scanf ("%d", &dTimerValue);
                if (dTimerValue)
                    dTimeOut.dT2 = dTimerValue;
            }
            fflush (stdin);

            printf ("\nDefault Value of Timer B:%d\n", SIP_DEFAULT_B);
            printf ("Do You want to change the timer value(y/n):");
            inputtype = sip_getchar ();
            if (inputtype == 'y' || inputtype == 'Y')
            {
                printf ("Enter the customized value for B:");
                fflush (stdout);
                dTimerValue = 0;
                scanf ("%d", &dTimerValue);
                if (dTimerValue)
                    dTimeOut.dTimerB = dTimerValue;
            }
            fflush (stdin);

            printf ("\nDefault Value of Timer C:%d\n", SIP_DEFAULT_C);
            printf ("Do You want to change the timer value(y/n):");
            inputtype = sip_getchar ();
            if (inputtype == 'y' || inputtype == 'Y')
            {
                printf ("Enter the customized value for C:");
                fflush (stdout);
                dTimerValue = 0;
                scanf ("%d", &dTimerValue);
                if (dTimerValue)
                    dTimeOut.dTimerC = dTimerValue;
            }
            fflush (stdin);

            printf ("\nDefault Value of Timer D_T3:%d\n", SIP_DEFAULT_D_T3);
            printf ("Do You want to change the timer value(y/n):");
            inputtype = sip_getchar ();
            if (inputtype == 'y' || inputtype == 'Y')
            {
                printf ("Enter the customized value for D_T3:");
                fflush (stdout);
                dTimerValue = 0;
                scanf ("%d", &dTimerValue);
                if (dTimerValue)
                    dTimeOut.dTimerD_T3 = dTimerValue;
            }
            fflush (stdin);

            printf ("\nDefault Value of Timer F_T3:%d\n", SIP_DEFAULT_F_T3);
            printf ("Do You want to change the timer value(y/n):");
            inputtype = sip_getchar ();
            if (inputtype == 'y' || inputtype == 'Y')
            {
                printf ("Enter the customized value for F_T3:");
                fflush (stdout);
                dTimerValue = 0;
                scanf ("%d", &dTimerValue);
                if (dTimerValue)
                    dTimeOut.dTimerF_T3 = dTimerValue;
            }
            fflush (stdin);

            printf ("\nDefault Value of Timer H:%d\n", SIP_DEFAULT_H);
            printf ("Do You want to change the timer value(y/n):");
            inputtype = sip_getchar ();
            if (inputtype == 'y' || inputtype == 'Y')
            {
                printf ("Enter the customized value for H:");
                fflush (stdout);
                dTimerValue = 0;
                scanf ("%d", &dTimerValue);
                if (dTimerValue)
                    dTimeOut.dTimerH = dTimerValue;
            }
            fflush (stdin);

            printf ("\nDefault Value of Timer I_T4:%d\n", SIP_DEFAULT_I_T4);
            printf ("Do You want to change the timer value(y/n):");
            inputtype = sip_getchar ();
            if (inputtype == 'y' || inputtype == 'Y')
            {
                printf ("Enter the customized value for I_T4:");
                fflush (stdout);
                dTimerValue = 0;
                scanf ("%d", &dTimerValue);
                if (dTimerValue)
                    dTimeOut.dTimerI_T4 = dTimerValue;
            }
            fflush (stdin);

            printf ("\nDefault Value of Timer J_T3:%d\n", SIP_DEFAULT_J_T3);
            printf ("Do You want to change the timer value(y/n):");
            inputtype = sip_getchar ();
            if (inputtype == 'y' || inputtype == 'Y')
            {
                printf ("Enter the customized value for J_T3:");
                fflush (stdout);
                dTimerValue = 0;
                scanf ("%d", &dTimerValue);
                if (dTimerValue)
                    dTimeOut.dTimerJ_T3 = dTimerValue;
            }
            fflush (stdin);

            printf ("\nDefault Value of Timer K_T4:%d\n", SIP_DEFAULT_K_T4);
            printf ("Do You want to change the timer value(y/n):");
            inputtype = sip_getchar ();
            if (inputtype == 'y' || inputtype == 'Y')
            {
                printf ("Enter the customized value for K_T4:");
                fflush (stdout);
                dTimerValue = 0;
                scanf ("%d", &dTimerValue);
                if (dTimerValue)
                    dTimeOut.dTimerK_T4 = dTimerValue;
            }
            fflush (stdin);
        }

        sip_listInit (&slMessageQueue, free, &error);
        sip_listInit (&glbslTxnKey, (sip_listFuncPtr) sip_freeSipTxnKey,
                      &error);
        sip_initTimerWheel (0, 0);
        pthread_create (&dTimerThread, NULL, sip_timerThread, NULL);
        sip_initHashTbl ((CompareKeyFuncPtr) sip_txn_compareTxnKeys);

        noOfDecodethreads = 3;

        for (i = 0; i < noOfDecodethreads; i++)
            pthread_create (&(dDecodeThread[i]), NULL, decodeMessageStats,
                            NULL);

        printf ("%s", mainmenu);
        {
            int                 sockfd;
            int                 n;
            socklen_t           clilen;
            struct sockaddr_in  serv_addr, cli_addr;
            fd_set              readfs;
            struct timeval      timeout;
            SipError            derror;

            if ((glbtransptype & SIP_UDP) == SIP_UDP)
            {
                if ((sockfd = socket (AF_INET, SOCK_DGRAM, 0)) < 0)
                {
                    printf ("Server could not open dgram socket\n");
                    close (sockfd);
                    exit (0);
                }
            }
            else
            {
                if ((sockfd = socket (AF_INET, SOCK_STREAM, 0)) < 0)
                {
                    printf ("Server could not open stream socket\n");
                    close (sockfd);
                    exit (0);
                }
            }
            clilen = sizeof (cli_addr);

            /* initialize structures for binding to listen port */
            bzero ((char *) &serv_addr, sizeof (serv_addr));
            serv_addr.sin_family = AF_INET;
            serv_addr.sin_addr.s_addr = htonl (INADDR_ANY);
            serv_addr.sin_port = htons ((SIP_U16bit) myPort);

            if (bind
                (sockfd, (struct sockaddr *) &serv_addr,
                 sizeof (serv_addr)) < 0)
            {
                printf ("Server couldn't bind to local address.\n");
                close (sockfd);
                exit (0);
            }

            if ((glbtransptype & SIP_TCP) == SIP_TCP)
                listen (sockfd, 5);

            for (;;)
            {
                int                 mintimeout;
                char               *message;

                mintimeout = 1000;
                timeout.tv_sec = (int) (mintimeout / 1000);
                timeout.tv_usec = ((int) mintimeout) % 1000;
                FD_ZERO (&readfs);
                FD_SET (sockfd, &readfs);
                FD_SET (0, &readfs);
                /* Enter into select
                   Comes out of select if a key is pressed
                   or a network message arrives
                   or if the minimum timout occurs
                 */

                select (sockfd + 1, &readfs, NULL, NULL, &timeout);

                if (FD_ISSET (sockfd, &readfs))
                {
                    if ((glbtransptype & SIP_UDP) == SIP_UDP)
                    {
                        message = (char *) malloc (sizeof (char) * MAXMESG);
                        n = recvfrom (sockfd, message, MAXMESG, 0,
                                      (struct sockaddr *) &cli_addr, &clilen);
                    }
                    else
                    {
                        int                 dacceptfd;
                        dacceptfd = accept (sockfd, NULL, NULL);
                        if (dacceptfd < 0)
                        {
                            printf ("Failed: Accept on a TCP socket failed");
                            close (sockfd);
                            exit (0);
                        }
                        message = (char *) malloc (sizeof (char) * MAXMESG);
                        n = recv (dacceptfd, message, MAXMESG, 0);
                    }

                    if (n < 0)
                    {
                        printf ("Server : Error in receive.\n");
                        close (sockfd);
                        exit (0);
                    }
                    message[n] = '\0';
                    /* Append into the Queue */
                    fast_lock_synch (0, &(dMessageQueueMutex), 0);
                    if (sip_listAppend (&slMessageQueue, (void *) message,
                                        &derror) == SipFail)
                    {
                        printf ("could not append to the list\n");
                        exit (0);
                    }
                    fast_unlock_synch (0, &(dMessageQueueMutex));
                }
                else if (FD_ISSET (0, &readfs))
                {
                    /* Key pressed */
                    char                c;
                    char                input;
                    SIP_U32bit          index;
                    SipTxnKey          *pTxnKeys, *pDesTKey;
                    SIP_U32bit          dCountTxn, dI, numbTxn, dSize;
                    /*SipError derror; */

                    c = sip_getchar ();
                    switch (c)
                    {
                        case '1':
                            printf ("%s", menu);
                            fflush (stdin);
                            input = sip_getchar ();
                            sendNewMessage (input, &sendaddr);
                            break;
                        case '2':
                        case '3':
                        case '4':
                        case '5':
                        case '6':
                            if (c == '3')
                            {
                                if (!glbenableretrans)
                                {
                                    printf
                                        ("\n The Remote Retransmission option is"
                                         " dis-abled.\n");
                                    fflush (stdin);
                                    break;
                                }
                            }
                            /* fetch all the trasactions from the hash table */
                            sip_getAllKeysOfTxnElem (&pTxnKeys, &dCountTxn,
                                                     &derror);
                            /* List all the trasactions on the screen */
                            numbTxn = dCountTxn;
                            if (dCountTxn == 0)
                            {
                                printf
                                    ("ERROR: No Transaction Element Found\n");
                                break;
                            }
                            else
                            {
                                dI = 0;
                                while (dCountTxn != 0)
                                {
                                    printf ("Txn Key%d#->\n", dI);
                                    if (pTxnKeys[dI].pCallid != NULL)
                                        printf ("Callid=%s\t",
                                                pTxnKeys[dI].pCallid);
                                    if (pTxnKeys[dI].pMethod != NULL)
                                        printf ("Method=%s\t",
                                                pTxnKeys[dI].pMethod);
                                    if (pTxnKeys[dI].pFromTag != NULL)
                                        printf ("FromTag=%s\t",
                                                pTxnKeys[dI].pFromTag);
                                    if (pTxnKeys[dI].pToTag != NULL)
                                        printf ("ToTag=%s\t",
                                                pTxnKeys[dI].pToTag);
                                    if (pTxnKeys[dI].pViaBranch != NULL)
                                        printf ("Viabranch=%s\t",
                                                pTxnKeys[dI].pViaBranch);
                                    printf ("\n");

                                    dI++;
                                    dCountTxn--;
                                }
                            }
                            if (c != '4')
                            {
                                printf ("Select the TxnKey[<%d]:", dI);
                                fflush (stdin);
                                input = sip_getchar ();
                                index = input - 48;

                                if (index < numbTxn)
                                {
                                    sip_initSipTxnKey (&pDesTKey, &derror);
                                    sip_cloneSipTxnKey (pDesTKey,
                                                        &pTxnKeys[index],
                                                        &derror);
                                }
                                else
                                {
                                    printf
                                        ("############Invalid Txn ################\n");
                                    for (dI = 0; dI < numbTxn; dI++)
                                    {
                                        __sip_freeSipTxnKeyElems (&pTxnKeys
                                                                  [dI]);
                                    }
                                    fast_memfree (TIMER_MEM_ID, pTxnKeys, NULL);
                                    break;
                                }
                                if (c == '2')
                                {
                                    printf ("%s", menu);
                                    fflush (stdin);
                                    input = sip_getchar ();
                                    sendMessageOnTxn (pDesTKey, input,
                                                      &sendaddr);
                                    /*freeTxnKeyList(&pTxnKeys,numbTxn);
                                     */
                                    for (dI = 0; dI < numbTxn; dI++)
                                    {
                                        __sip_freeSipTxnKeyElems (&pTxnKeys
                                                                  [dI]);
                                    }
                                    fast_memfree (TIMER_MEM_ID, pTxnKeys, NULL);
                                }
                                else if (c == '3')
                                {
                                    sendRemoteRetranmission (pDesTKey,
                                                             &sendaddr);
                                    /*freeTxnKeyList(&pTxnKeys,numbTxn); */
                                    for (dI = 0; dI < numbTxn; dI++)
                                    {
                                        __sip_freeSipTxnKeyElems (&pTxnKeys
                                                                  [dI]);
                                    }
                                    fast_memfree (TIMER_MEM_ID, pTxnKeys, NULL);
                                }
                                else if (c == '5')
                                {
                                    SipTxnContext      *pContext = SIP_NULL;
                                    if (sip_txn_abortTxn
                                        (pDesTKey, pContext,
                                         &derror) == SipFail)
                                        printf
                                            (" Failed to abort the Transaciton\n");
                                    else
                                        printf
                                            ("Successfully Aborted the Transaction\n");
                                    /*freeTxnKeyList(&pTxnKeys,numbTxn); */
                                    for (dI = 0; dI < numbTxn; dI++)
                                    {
                                        __sip_freeSipTxnKeyElems (&pTxnKeys
                                                                  [dI]);
                                    }
                                    fast_memfree (TIMER_MEM_ID, pTxnKeys, NULL);
                                }
                                else if (c == '6')
                                {
                                    SipList            *pQList = SIP_NULL;
                                    if (sip_txn_cancelTxn
                                        (pDesTKey, &pQList, &derror) == SipFail)
                                    {
                                        printf
                                            (" Cancellation of the transaciton was"
                                             " un- successful");
                                    }
                                    else
                                    {
                                        printf (" The transaction was cancelled"
                                                "successfully");
                                        dSize = 0;
                                        if (pQList != SIP_NULL)
                                            sip_listSizeOf (pQList, &dSize,
                                                            &derror);
                                        if (dSize == 0)
                                        {
                                            printf
                                                ("\n***************************\n");
                                            printf
                                                ("There is no message queued\n");
                                            printf
                                                ("*****************************\n\n");
                                        }
                                        else
                                        {
                                            SipMessage         *pMsg;
                                            printf
                                                ("\n***************************\n");
                                            printf ("Size of the Queue=%d",
                                                    dSize);
                                            for (dI = 0; dI < dSize; dI++)
                                            {
                                                char               *sendbuffer;
                                                SIP_U32bit          dLength;
                                                SipOptions          options;
                                                options.dOption = 0;
                                                sendbuffer =
                                                    (char *)
                                                    malloc (sizeof (char) *
                                                            MAXMESG);
                                                sip_listGetAt (pQList, dI,
                                                               (SIP_Pvoid *) &
                                                               pMsg, &error);
                                                sip_formMessage (pMsg, &options,
                                                                 sendbuffer,
                                                                 &dLength,
                                                                 &derror);
                                                printf
                                                    ("*****Message**************\n");
                                                printf ("%s\n", sendbuffer);
                                                printf
                                                    ("\n*************************\n");
                                                free (sendbuffer);
                                            }
                                        }
                                        for (dI = 0; dI < numbTxn; dI++)
                                        {
                                            __sip_freeSipTxnKeyElems (&pTxnKeys
                                                                      [dI]);
                                        }
                                        fast_memfree (TIMER_MEM_ID, pTxnKeys,
                                                      NULL);
                                    }
                                }
                                sip_freeSipTxnKey (pDesTKey);
                            }
                            else
                            {
                                for (dI = 0; dI < numbTxn; dI++)
                                {
                                    __sip_freeSipTxnKeyElems (&pTxnKeys[dI]);
                                }
                                fast_memfree (TIMER_MEM_ID, pTxnKeys, NULL);
                            }
                            break;
                        case '7':
                            sip_listSizeOf (&glbslTxnKey, &dSize, &error);
                            if (dSize == 0)
                            {
                                printf
                                    ("\n****************************************\n");
                                printf ("There is no TxnKey in the list\n");
                                printf
                                    ("****************************************\n\n");
                            }
                            else
                            {
                                /*int dI=0; */
                                SipTxnKey          *pTempTxnKey;
                                for (dI = 0; dI < dSize; dI++)
                                {
                                    sip_listGetAt (&glbslTxnKey, dI,
                                                   (SIP_Pvoid *) & pTempTxnKey,
                                                   &error);
                                    printf ("Txn Key%d#->\n", dI);
                                    if (pTempTxnKey->pCallid != NULL)
                                        printf ("Callid=%s\t",
                                                pTempTxnKey->pCallid);
                                    if (pTempTxnKey->pMethod != NULL)
                                        printf ("Method=%s\t",
                                                pTempTxnKey->pMethod);
                                    if (pTempTxnKey->pFromTag != NULL)
                                        printf ("FromTag=%s\t",
                                                pTempTxnKey->pFromTag);
                                    if (pTempTxnKey->pToTag != NULL)
                                        printf ("ToTag=%s\t",
                                                pTempTxnKey->pToTag);
                                    if (pTempTxnKey->pViaBranch != NULL)
                                        printf ("Viabranch=%s\t",
                                                pTempTxnKey->pViaBranch);
                                    printf ("\n");
                                }
                            }

                            break;
                        case '8':
                            printf ("%s", midmenu);
                            fflush (stdin);
                            input = sip_getchar ();
                            sendMidMessage (input, &sendaddr);
                            break;
                        case 'q':
                            __sip_flushTimer ();
                            __sip_flushHash ();
                            sip_listDeleteAll (&glbslTxnKey, &derror);
                            sip_releaseStack ();
#ifdef SIP_INCREMENTAL_PARSING
                            if (context.pList != SIP_NULL)
                                fast_memfree (0, context.pList, SIP_NULL);
#endif
                            exit (0);
                        case ' ':
                            fflush (stdin);
                            break;
                        default:
                            if (c != 0x0a)
                                printf ("Unrecognized Key Pressed\n");
                            fflush (stdin);
                            break;
                    }
                    printf ("%s", mainmenu);
                    fflush (stdin);
                }
            }
        }
    }

    void                sendNewMessage (int c, SipTranspAddr * pSendAddr)
    {
        SipError            derror;
        switch (c)
        {
            case 'i':
                sendRequest ("INVITE", pSendAddr, &derror);
                break;
            case 'r':
                sendRequest ("REGISTER", pSendAddr, &derror);
                break;
            case 'o':
                sendRequest ("OPTIONS", pSendAddr, &derror);
                break;
            case 'b':
                sendRequest ("BYE", pSendAddr, &derror);
                break;
            case 'c':
                sendRequest ("CANCEL", pSendAddr, &derror);
                break;
            case 'a':
                sendRequest ("ACK", pSendAddr, &derror);
                break;
            case 'k':
                sendRequest ("PRACK", pSendAddr, &derror);
                break;
            case 'p':
                sendRequest ("PROPOSE", pSendAddr, &derror);
                break;
#ifdef SIP_IMPP
            case 'u':
                sendRequest ("SUBSCRIBE", pSendAddr, &derror);
                break;
            case 'n':
                sendRequest ("NOTIFY", pSendAddr, &derror);
                break;
            case 'm':
                sendRequest ("MESSAGE", pSendAddr, &derror);
                break;
#endif
            case 'v':
                sendRequest ("UNKNOWN", pSendAddr, &derror);
                break;
            case 'e':
                sendRequest ("REFER", pSendAddr, &derror);
                break;
            case 'f':
                sendRequest ("INFO", pSendAddr, &derror);
                break;
            case '2':
            case '3':
            case '4':
            case '0':
            case '1':
            case 's':
            case 'S':
                printf ("\nA new response cannot be sent\n"
                        "A response can be sent only for a "
                        "request received\n");
                break;
            case ' ':
                fflush (stdin);
                break;
            default:
                if (c != 0x0a)
                    printf ("Unrecognized Key Pressed\n");
                fflush (stdin);
                break;
        }
        printf ("%s", mainmenu);
    }
    void                sendMidMessage (int c, SipTranspAddr * sendaddr)
    {
        SipError            derror;
        switch (c)
        {
            case 'i':
                sendMidRequest ("INVITE", sendaddr, &derror);
                break;
            case 'o':
                sendMidRequest ("OPTIONS", sendaddr, &derror);
                break;
            case 'a':
                sendMidRequest ("ACK", sendaddr, &derror);
                break;
            case '3':
                sendMidResponse (300, "Multiple Choices", "INVITE", sendaddr,
                                 &derror);
                break;
            case 'I':
                sendMidResponse (200, "OK", "INVITE", sendaddr, &derror);
                break;
            case 'O':
                sendMidResponse (200, "OK", "OPTIONS", sendaddr, &derror);
                break;
            case 't':
                sendMidResponse (100, "Trying", "INVITE", sendaddr, &derror);
                break;
            case '0':
                sendMidResponse (180, "Ringing", "INVITE", sendaddr, &derror);
                break;
            case '1':
                sendMidResponse (181, "Call Forward", "INVITE", sendaddr,
                                 &derror);
                break;
            case '2':
                sendMidResponse (182, "Queued", "INVITE", sendaddr, &derror);
                break;
            case 's':
                sendMidResponse (183, "Session progress", "INVITE", sendaddr,
                                 &derror);
                break;
            case ' ':
                fflush (stdin);
                break;
            default:
                if (c != 0x0a)
                    printf ("Unrecognized Key Pressed\n");
                fflush (stdin);
                break;
        }
        printf ("%s", mainmenu);
    }

    void                sendMessageOnTxn (SipTxnKey * pKey, int c,
                                          SipTranspAddr * pSendAddr)
    {
        SipError            derror;
        switch (c)
        {
            case 'i':
                sendRequestOnTransaction (pKey, 0, (char *) "INVITE", pSendAddr,
                                          &derror);
                break;
            case 'r':
                sendRequestOnTransaction (pKey, 0, (char *) "REGISTER",
                                          pSendAddr, &derror);
                break;
            case 'o':
                sendRequestOnTransaction (pKey, 0, (char *) "OPTIONS",
                                          pSendAddr, &derror);
                break;
            case 'b':
                sendRequestOnTransaction (pKey, 0, (char *) "BYE", pSendAddr,
                                          &derror);
                break;
            case 'c':
                sendRequestOnTransaction (pKey, 0, (char *) "CANCEL", pSendAddr,
                                          &derror);
                break;
            case 'a':
                sendRequestOnTransaction (pKey, 0, (char *) "ACK", pSendAddr,
                                          &derror);
                break;
            case 'k':
                sendRequestOnTransaction (pKey, 0, (char *) "PRACK", pSendAddr,
                                          &derror);
                break;
            case 'p':
                sendRequestOnTransaction (pKey, 0, (char *) "PROPOSE",
                                          pSendAddr, &derror);
                break;
#ifdef SIP_IMPP
            case 'u':
                sendRequestOnTransaction (pKey, 0, (char *) "SUBSCRIBE",
                                          pSendAddr, &derror);
                break;
            case 'n':
                sendRequestOnTransaction (pKey, 0, (char *) "NOTIFY", pSendAddr,
                                          &derror);
                break;
            case 'm':
                sendRequestOnTransaction (pKey, 0, (char *) "MESSAGE",
                                          pSendAddr, &derror);
                break;
#endif
            case 'v':
                sendRequestOnTransaction (pKey, 0, (char *) "UNKNOWN",
                                          pSendAddr, &derror);
                break;
            case 'e':
                sendRequestOnTransaction (pKey, 0, (char *) "REFER", pSendAddr,
                                          &derror);
                break;
            case 'f':
                sendRequestOnTransaction (pKey, 1, (char *) "INFO", pSendAddr,
                                          &derror);
                break;
            case '3':
                sendResponseOnTransaction (pKey, 1, 0, 300,
                                           (char *) "Multiple Choices",
                                           (char *) "INVITE", pSendAddr,
                                           &derror);
                break;
            case '4':
                sendResponseOnTransaction (pKey, 1, 0, 401,
                                           (char *) "UnAuthorized",
                                           (char *) "INVITE", pSendAddr,
                                           &derror);
                break;
            case '2':
                sendResponseOnTransaction (pKey, 1, 0, 200, (char *) "OK",
                                           (char *) "INVITE", pSendAddr,
                                           &derror);
                break;
            case '0':
                sendResponseOnTransaction (pKey, 1, 0, 180, (char *) "Ringing",
                                           (char *) "INVITE", pSendAddr,
                                           &derror);
                break;
            case '1':
                sendResponseOnTransaction (pKey, 1, 0, 181,
                                           (char *) "Call Forward",
                                           (char *) "INVITE", pSendAddr,
                                           &derror);
                break;
            case 'S':
                sendResponseOnTransaction (pKey, 1, 1, 182, (char *) "Queued",
                                           (char *) "INVITE", pSendAddr,
                                           &derror);
                break;
            case 's':
                sendResponseOnTransaction (pKey, 1, 1, 183,
                                           (char *) "Session progress",
                                           (char *) "INVITE", pSendAddr,
                                           &derror);
                break;
            case ' ':
                fflush (stdin);
                break;
            default:
                if (c != 0x0a)
                    printf ("Unrecognized Key Pressed\n");
                fflush (stdin);
                break;
        }
        printf ("%s", mainmenu);
    }

/*sending remote retransmissions*/
    SipBool             sendRemoteRetranmission (SipTxnKey * pKey,
                                                 SipTranspAddr * pAddr)
    {
        SipTxnBuffer       *pTxnBuf;
        SipError            err;
        SIP_Pvoid           pABC = SIP_NULL;
        char               *pSendBuf;
        if (sip_cbk_fetchTxn ((void **) &pTxnBuf, (void *) pKey, pABC, 0, &err)
            == SipFail)
            return SipFail;
        if (pTxnBuf->pEventContext == SIP_NULL)
        {
            printf (" Cannot send a remote re-transmission. There is no "
                    "buffer to remote-retransmit\n");
            return SipFail;
        }

        if (pTxnBuf->pEventContext->pData == SIP_NULL)
        {
            printf (" Cannot send a remote re-transmission. There is no "
                    "buffer to remote-retransmit\n");
            return SipFail;
        }

        pSendBuf = (char *) pTxnBuf->pEventContext->pData;
        if (pSendBuf == SIP_NULL)
        {
            printf (" Cannot send a remote re-transmission. There is no "
                    "buffer to remote-retransmit\n");
            return SipFail;
        }
        sip_sendToNetwork (pSendBuf, strlen (pSendBuf), pAddr, glbtransptype,
                           &err);
        sip_cbk_releaseTxn ((void *) pKey, (void **) &pKey, (void **) &pTxnBuf,
                            pABC, 0, &err);
        return SipSuccess;
    }

/************ Sending a SIP Message on an existing Txn***********************/
/*START TO REMOVE */
    SipBool             sendRequestOnTransaction (SipTxnKey * pTxnKey,
                                                  int dCseqIncr, char *method,
                                                  SipTranspAddr * pSendAddr,
                                                  SipError * err)
    {
        SipMessage         *sipmesg;
        SipHeader          *header;
        SipAddrSpec        *addrspec, *toaddr, *conaddr;
        SipUrl             *sipurl, *tosipurl, *consipurl;
        SipBool             res;
        SipEventContext    *context;
        SipOptions          options;
        /*char *callid; */
        /*char viastring[500]; */
        /*char *tempStr; */
        char               *pToTag, *pFromTag;
        SipTxnKey          *pSendTxnKey = SIP_NULL;
        SipParam           *viaParam;
        SipTranspAddr      *pTempTranspAddr = SIP_NULL;

        char                unknownMethod[100];

        if (strcasecmp (method, "UNKNOWN") == 0)
        {
            printf ("Method To be Sent:");
            scanf ("%s", unknownMethod);
            fflush (stdin);
        }

        if (sip_initSipMessage (&sipmesg, SipMessageRequest, err) == SipFail)
        {
            printf ("Could not form message.\n");
            exit (0);
        }
        /*
         * Starts the creation of request Line 
         */

        if (strcasecmp (method, "UNKNOWN") == 0)
            sipmesg->u.pRequest->pRequestLine->pMethod = strdup (unknownMethod);
        else
            sipmesg->u.pRequest->pRequestLine->pMethod = strdup (method);
        sipmesg->u.pRequest->pRequestLine->pVersion = strdup ("SIP/2.0");

        sip_initSipAddrSpec (&addrspec, SipAddrSipUri, err);
        sip_initSipUrl (&sipurl, err);
        sipurl->pHost = strdup ("mydomain.com");
        addrspec->u.pSipUrl = sipurl;
        addrspec->dType = SipAddrSipUri;
        sipmesg->u.pRequest->pRequestLine->pRequestUri = addrspec;

        /*
         * End of creation of request Line 
         */

        /*Creation and setting of from header */
        sip_initSipHeader (&header, SipHdrTypeFrom, err);
        ((SipFromHeader *) (header->pHeader))->pAddrSpec = addrspec;
        HSS_LOCKEDINCREF (addrspec->dRefCount);
        pFromTag = strdup ("fromtag1947");
        sip_setTagInFromHdr (header, pFromTag, err);
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
        /*Creation and setting of from header ends here */

        /*Creation and setting of To header */

        sip_initSipHeader (&header, SipHdrTypeTo, err);
        sip_initSipAddrSpec (&toaddr, SipAddrSipUri, err);
        sip_initSipUrl (&tosipurl, err);
        tosipurl->pHost = strdup ("yourdomain.com");
        toaddr->u.pSipUrl = tosipurl;
        toaddr->dType = SipAddrSipUri;
        ((SipFromHeader *) (header->pHeader))->pAddrSpec = toaddr;
        if (pTxnKey->pToTag != NULL)
        {
            pToTag = strdup (pTxnKey->pToTag);
            sip_setTagInToHdr (header, pToTag, err);
        }

/*    pToTag = strdup("totag2002");
    sip_setTagInToHdr(header,pToTag,err);*/
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
/*Creation and setting of To header ends here */

/*Creation of Callid Header */

        sip_initSipHeader (&header, SipHdrTypeCallId, err);
        if (pTxnKey->pCallid != NULL)
            ((SipCallIdHeader *) (header->pHeader))->pString =
                strdup (pTxnKey->pCallid);
        else
            ((SipCallIdHeader *) (header->pHeader))->pString =
                strdup ("1234324453@mydomain.com");
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
/*Creation and setting of Callid header ends here */

/*Creation of CSeq Header */
        sip_initSipHeader (&header, SipHdrTypeCseq, err);
        if (dCseqIncr == 1)
            ((SipCseqHeader *) (header->pHeader))->dSeqNum = pTxnKey->dCSeq + 1;
        else
            ((SipCseqHeader *) (header->pHeader))->dSeqNum = pTxnKey->dCSeq;
        ((SipCseqHeader *) (header->pHeader))->pMethod = strdup (method);
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
/*Creation and setting of CSeq header ends here */
/*
 * Via Header Creation
 */

        sip_initSipHeader (&header, SipHdrTypeVia, err);
        ((SipViaHeader *) (header->pHeader))->pSentProtocol =
            strdup ("SIP/2.0/UDP");
        ((SipViaHeader *) (header->pHeader))->pSentBy =
            strdup ("135.180.130.133");

        sip_initSipParam (&viaParam, err);
        viaParam->pName = strdup ("branch");
        sip_listAppend (&(viaParam->slValue), strdup ("Aricent"), err);

        sip_listAppend (&(((SipViaHeader *) (header->pHeader))->slParam),
                        viaParam, err);
        sip_insertHeaderAtIndex (sipmesg, header, 0, err);
        sip_freeSipHeader (header);
        free (header);

/*Via Header Ends */
/*Contact Header */

        sip_initSipHeader (&header, SipHdrTypeContactNormal, err);
        sip_initSipAddrSpec (&conaddr, SipAddrSipUri, err);
        sip_initSipUrl (&consipurl, err);
        consipurl->pHost = strdup ("yourdomain.com");
        conaddr->u.pSipUrl = consipurl;
        conaddr->dType = SipAddrSipUri;
        ((SipContactHeader *) (header->pHeader))->pAddrSpec = conaddr;
        ((SipContactHeader *) (header->pHeader))->dType = SipContactNormal;
        sip_insertHeaderAtIndex (sipmesg, header, 0, err);
        sip_freeSipHeader (header);
        free (header);

/*Contact Header Ends*/

/*Content Length */
        sip_initSipHeader (&header, SipHdrTypeContentLength, err);
        ((SipContentLengthHeader *) (header->pHeader))->dLength = 0;
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);

        if (strcmp (method, "PRACK") == 0)
        {
            if (pTxnKey->dRseq <= 0)
            {
                printf
                    ("\nERROR: Cannot send a PRACK request on this transaction."
                     "Invalid RSeq number\n");
                sip_freeSipMessage (sipmesg);
                return SipFail;
            }

            sip_initSipHeader (&header, SipHdrTypeRAck, err);
            ((SipRackHeader *) (header->pHeader))->dRespNum = pTxnKey->dRseq;
            ((SipRackHeader *) (header->pHeader))->dCseqNum = pTxnKey->dCSeq;
            ((SipRackHeader *) (header->pHeader))->pMethod = strdup ("INVITE");
            sip_setHeader (sipmesg, header, err);
            sip_freeSipHeader (header);
            free (header);

        }

        {
            SipTxnContext      *txncontext;
            en_SipTxnBypass     dbypass;

            sip_txn_initSipTxnContext (&txncontext, err);
            options.dOption = 0;
            txncontext->txnOption.dOption = options;

            context = (SipEventContext *) malloc (sizeof (SipEventContext));
            txncontext->pEventContext = context;
            txncontext->pEventContext->pData = NULL;

            if (glbenableretrans == 1)
            {
                char               *sendbuffer;
                SIP_U32bit          dLength;
                sendbuffer = (char *) malloc (sizeof (char) * MAXMESG);
                if (sip_formMessage
                    (sipmesg, &options, sendbuffer, &dLength, err) == SipFail)
                {
                    free (sendbuffer);
                    return SipFail;
                }
                txncontext->pEventContext->pData = (void *) sendbuffer;
            }
            else
            {
                txncontext->pEventContext->pData = strdup ("user data");
            }
            txncontext->pEventContext->pDirectBuffer = SIP_NULL;
            if (glbtransactiontype == 2)
                dbypass = SipTxnByPass;
            else
                dbypass = SipTxnNoByPass;

            if (!glbtransactiontype)
                txncontext->dTxnType = SipUATypeTxn;
            else
                txncontext->dTxnType = SipProxyTypeTxn;

            txncontext->txnOption.dTimerOption = 0;
            if (glbsetTimerValue)
            {
                txncontext->timeoutValues.dT1 = dTimeOut.dT1;
                txncontext->timeoutValues.dT2 = dTimeOut.dT2;
                txncontext->timeoutValues.dTimerB = dTimeOut.dTimerB;
                txncontext->timeoutValues.dTimerC = dTimeOut.dTimerC;
                txncontext->timeoutValues.dTimerD_T3 = dTimeOut.dTimerD_T3;
                txncontext->timeoutValues.dTimerF_T3 = dTimeOut.dTimerF_T3;
                txncontext->timeoutValues.dTimerH = dTimeOut.dTimerH;
                txncontext->timeoutValues.dTimerI_T4 = dTimeOut.dTimerI_T4;
                txncontext->timeoutValues.dTimerJ_T3 = dTimeOut.dTimerJ_T3;
                txncontext->timeoutValues.dTimerK_T4 = dTimeOut.dTimerK_T4;
                txncontext->txnOption.dTimerOption = SIP_OPT_TIMERALL;
            }
            pTempTranspAddr = (SipTranspAddr *) fast_memget
                (0, sizeof (SipTranspAddr), err);
            *pTempTranspAddr = *pSendAddr;

            res =
                sip_txn_sendMessage (sipmesg, pTempTranspAddr, glbtransptype,
                                     txncontext, dbypass, &pSendTxnKey, err);

            if (res == SipSuccess)
            {
                if (pSendTxnKey != SIP_NULL)
                {
                    printf
                        ("\n***********************************************\n");
                    printf
                        ("The message resulted in creation of a new transaciton\n");
                    printf
                        ("***********************************************\n\n");
                    fast_lock_synch (0, &(dTxnKeyMutex), 0);
                    if (sip_listAppend (&glbslTxnKey, (void *) pSendTxnKey,
                                        err) == SipFail)
                    {
                        printf ("could not append to the list\n");
                        exit (0);
                    }
                    fast_unlock_synch (0, &(dTxnKeyMutex));
                }
                printf ("%s sent successfully.\n", method);
            }
            else
            {
                sip_freeEventContext (context);

                switch (*err)
                {
                    case E_TXN_NO_EXIST:
                        printf ("** ERROR : The Txn for this message"
                                "doesn't exist**\n");
                        break;
                    case E_TXN_EXISTS:
                        printf ("** ERROR : The Txn for this message"
                                "already exists**\n");
                        break;
                    case E_TXN_INV_STATE:
                        printf ("** ERROR : This message leads to"
                                "an invalid transaction state**\n");
                        break;
                    case E_TXN_INV_MSG:
                        printf ("** ERROR : This is an invalid message"
                                "for received for the Txn**\n");
                        break;
                    default:
                        break;
                }
                printf ("** ERROR : Send Message Failed**\n");
            }
            free (txncontext);
            sip_freeSipMessage (sipmesg);
        }
        return SipSuccess;
    }

/*END TO REMOVE */
    SipBool             sendResponseOnTransaction (SipTxnKey * pTxnKey,
                                                   int dCseqIncr, int dRpr,
                                                   int code, char *reason,
                                                   char *method,
                                                   SipTranspAddr * pSendAddr,
                                                   SipError * err)
    {
        SipMessage         *sipmesg;
        SipHeader          *header;
        SipAddrSpec        *addrspec, *toaddr, *conaddr;
        SipUrl             *sipurl, *tosipurl, *consipurl;
        SipBool             res;
        SipParam           *viaParam;
        SipEventContext    *context;
        SipOptions          options;
        char               *tempStr;
        /*char viastring[500]; */
        char               *pToTag = SIP_NULL;
        char               *pFromTag = SIP_NULL;
        /*char *callid; */
        SipTxnKey          *pSendTxnKey = SIP_NULL;
        SipTranspAddr      *pTempTranspAddr = SIP_NULL;

        int                 dummyCseq;
        dummyCseq = dCseqIncr;
        (void) tempStr;
        method = pTxnKey->pMethod;

        if (sip_initSipMessage (&sipmesg, SipMessageResponse, err) == SipFail)
        {
            printf ("Could not form message.\n");
            exit (0);
        }
        sipmesg->u.pResponse->pStatusLine->pReason = strdup (reason);
        sipmesg->u.pResponse->pStatusLine->pVersion = strdup ("SIP/2.0");
        sipmesg->u.pResponse->pStatusLine->dCodeNum = (SIP_U16bit) code;
        /*  sip_setStatusLineInSipRespMsg (sipmesg, statline, err); */

        /*Creation and setting of from header */
        sip_initSipHeader (&header, SipHdrTypeFrom, err);
        sip_initSipAddrSpec (&addrspec, SipAddrSipUri, err);
        sip_initSipUrl (&sipurl, err);
        sipurl->pHost = strdup ("yourdomain.com");
        addrspec->u.pSipUrl = sipurl;
        addrspec->dType = SipAddrSipUri;

        ((SipFromHeader *) (header->pHeader))->pAddrSpec = addrspec;
        HSS_LOCKEDINCREF (addrspec->dRefCount);
        pFromTag = strdup ("fromtag1947");
        sip_setTagInFromHdr (header, pFromTag, err);
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
        sip_freeSipAddrSpec (addrspec);

        /*Creation and setting of from header ends here */

        /*Creation and setting of To header */

        sip_initSipHeader (&header, SipHdrTypeTo, err);
        sip_initSipAddrSpec (&toaddr, SipAddrSipUri, err);
        sip_initSipUrl (&tosipurl, err);
        tosipurl->pHost = strdup ("yourdomain.com");
        toaddr->u.pSipUrl = tosipurl;
        toaddr->dType = SipAddrSipUri;
        ((SipFromHeader *) (header->pHeader))->pAddrSpec = toaddr;
        pToTag = strdup ("totag2002");
        sip_setTagInToHdr (header, pToTag, err);
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
/*Creation and setting of To header ends here */

/*Creation of Callid Header */

        sip_initSipHeader (&header, SipHdrTypeCallId, err);
        if (pTxnKey->pCallid != NULL)
            ((SipCallIdHeader *) (header->pHeader))->pString =
                strdup (pTxnKey->pCallid);
        else
            ((SipCallIdHeader *) (header->pHeader))->pString =
                strdup ("1234324453@mydomain.com");
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
/*Creation and setting of Callid header ends here */

/*Creation of CSeq Header */
        sip_initSipHeader (&header, SipHdrTypeCseq, err);
        ((SipCseqHeader *) (header->pHeader))->dSeqNum = pTxnKey->dCSeq;
        ((SipCseqHeader *) (header->pHeader))->pMethod = strdup (method);
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
/*Creation and setting of CSeq header ends here */
/*
 * Via Header Creation
 */

        sip_initSipHeader (&header, SipHdrTypeVia, err);
        ((SipViaHeader *) (header->pHeader))->pSentProtocol =
            strdup ("SIP/2.0/UDP");
        ((SipViaHeader *) (header->pHeader))->pSentBy =
            strdup ("135.180.130.133");

        sip_initSipParam (&viaParam, err);
        viaParam->pName = strdup ("branch");
        sip_listAppend (&(viaParam->slValue), strdup ("Aricent"), err);

        sip_listAppend (&(((SipViaHeader *) (header->pHeader))->slParam),
                        viaParam, err);
        sip_insertHeaderAtIndex (sipmesg, header, 0, err);
        sip_freeSipHeader (header);
        free (header);

/*Via Header Ends */
/*Contact Header */

        sip_initSipHeader (&header, SipHdrTypeContactNormal, err);
        sip_initSipAddrSpec (&conaddr, SipAddrSipUri, err);
        sip_initSipUrl (&consipurl, err);
        consipurl->pHost = strdup ("yourdomain.com");
        conaddr->u.pSipUrl = consipurl;
        conaddr->dType = SipAddrSipUri;
        ((SipContactHeader *) (header->pHeader))->pAddrSpec = conaddr;
        ((SipContactHeader *) (header->pHeader))->dType = SipContactNormal;
        sip_insertHeaderAtIndex (sipmesg, header, 0, err);
        sip_freeSipHeader (header);
        free (header);

/*Contact Header Ends*/

/*Content Length */
        sip_initSipHeader (&header, SipHdrTypeContentLength, err);
        ((SipContentLengthHeader *) (header->pHeader))->dLength = 0;
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);

        if ((strcmp (method, "INVITE") == 0) && ((code < 200) && (code != 100))
            && (dRpr == 1))
        {
            int                 dRseq;
            dRseq = pTxnKey->dRseq;
            sip_initSipHeader (&header, SipHdrTypeRSeq, err);
            ((SipRseqHeader *) (header->pHeader))->dLength = dRseq + 1;
            sip_setHeader (sipmesg, header, err);
            sip_freeSipHeader (header);
            free (header);

        }
        {
            SipTxnContext      *txncontext;
            en_SipTxnBypass     dbypass;

            options.dOption = SIP_OPT_FULLFORM | SIP_OPT_SINGLE;
            sip_txn_initSipTxnContext (&txncontext, err);
            context = (SipEventContext *) malloc (sizeof (SipEventContext));
            txncontext->pEventContext = context;
            options.dOption = 0;
            txncontext->txnOption.dOption = options;

            if (glbenableretrans == 1)
            {
                char               *sendbuffer;
                SIP_U32bit          dLength;
                sendbuffer = (char *) malloc (sizeof (char) * MAXMESG);
                if (sip_formMessage
                    (sipmesg, &options, sendbuffer, &dLength, err) == SipFail)
                {
                    free (sendbuffer);
                    return SipFail;
                }
                txncontext->pEventContext->pData = (void *) sendbuffer;
            }
            else
            {
                txncontext->pEventContext->pData = strdup ("user data");
            }
            txncontext->pEventContext->pDirectBuffer = SIP_NULL;
            if (glbtransactiontype == 2)
                dbypass = SipTxnByPass;
            else
                dbypass = SipTxnNoByPass;

            if (!glbtransactiontype)
                txncontext->dTxnType = SipUATypeTxn;
            else
                txncontext->dTxnType = SipProxyTypeTxn;
            txncontext->txnOption.dTimerOption = 0;

            if (glbsetTimerValue)
            {
                txncontext->timeoutValues.dT1 = dTimeOut.dT1;
                txncontext->timeoutValues.dT2 = dTimeOut.dT2;
                txncontext->timeoutValues.dTimerB = dTimeOut.dTimerB;
                txncontext->timeoutValues.dTimerC = dTimeOut.dTimerC;
                txncontext->timeoutValues.dTimerD_T3 = dTimeOut.dTimerD_T3;
                txncontext->timeoutValues.dTimerF_T3 = dTimeOut.dTimerF_T3;
                txncontext->timeoutValues.dTimerH = dTimeOut.dTimerH;
                txncontext->timeoutValues.dTimerI_T4 = dTimeOut.dTimerI_T4;
                txncontext->timeoutValues.dTimerJ_T3 = dTimeOut.dTimerJ_T3;
                txncontext->timeoutValues.dTimerK_T4 = dTimeOut.dTimerK_T4;
                txncontext->txnOption.dTimerOption = SIP_OPT_TIMERALL;
            }
            pTempTranspAddr = (SipTranspAddr *) fast_memget
                (0, sizeof (SipTranspAddr), err);
            *pTempTranspAddr = *pSendAddr;

            res =
                sip_txn_sendMessage (sipmesg, pTempTranspAddr, glbtransptype,
                                     txncontext, dbypass, &pSendTxnKey, err);
            sip_freeSipMessage (sipmesg);

            if (res == SipSuccess)
            {
                if (pSendTxnKey != SIP_NULL)
                {
                    printf
                        ("\n***********************************************\n");
                    printf
                        ("The message resulted in creation of a new transaciton\n");
                    printf
                        ("***********************************************\n\n");
                    fast_lock_synch (0, &(dTxnKeyMutex), 0);
                    if (sip_listAppend (&glbslTxnKey, (void *) pSendTxnKey,
                                        err) == SipFail)
                    {
                        printf ("could not append to the list\n");
                        exit (0);
                    }
                    fast_unlock_synch (0, &(dTxnKeyMutex));
                }
                printf ("Response with code %d sent successfully.\n", code);
            }
            else
            {
                sip_freeEventContext (context);
                switch (*err)
                {
                    case E_TXN_NO_EXIST:
                        printf ("** ERROR : The Txn for this message"
                                "doesn't exist**\n");
                        break;
                    case E_TXN_EXISTS:
                        printf ("** ERROR : The Txn for this message"
                                "already exists**\n");
                        break;
                    case E_TXN_INV_STATE:
                        printf ("** ERROR : This message leads to"
                                "an invalid transaction state**\n");
                        break;
                    case E_TXN_INV_MSG:
                        printf ("** ERROR : This is an invalid message"
                                "for received for the Txn**\n");
                        break;
                    default:
                        break;
                }
                printf ("** ERROR : Send Message Failed**\n");
            }
            free (txncontext);
        }
        return SipSuccess;
    }

/*REMOVE UNTIL Here */

/************ Sending a New SIP MEssage ****************************/
    SipBool             sendRequest (const char *method,
                                     SipTranspAddr * pSendAddr, SipError * err)
    {
        SipMessage         *sipmesg;
        SipHeader          *header;
        SipAddrSpec        *addrspec, *toaddr, *conaddr;
        SipUrl             *sipurl, *tosipurl, *consipurl;
        SipBool             res;
        SipEventContext    *context;
        SipParam           *viaParam;
        SIP_S8bit           TempCallId[101];
        SIP_U32bit          dRand1, dRand2;
        SIP_S8bit           TempTag[101];
        SipTxnKey          *pSendTxnKey = SIP_NULL;
        SIP_S8bit          *pFromTag;
        SIP_S8bit          *pToTag;
        SipTranspAddr      *pTempTranspAddr = SIP_NULL;

        char                contextstr[100];
        SipOptions          options;
        char                unknownMethod[100];
        (void) pToTag;
        if (strcasecmp (method, "UNKNOWN") == 0)
        {
            printf ("Method To be Sent:");
            scanf ("%s", unknownMethod);
            fflush (stdin);
        }

        if (sip_initSipMessage (&sipmesg, SipMessageRequest, err) == SipFail)
        {
            printf ("Could not form message.\n");
            exit (0);
        }

        dRand1 = rand () % 100000;
        dRand2 = rand () % 100000;
        sprintf (TempCallId, "%d%d@139.85.229.21", dRand1, dRand2);
        sprintf (TempTag, "goAwayFrom%d", dRand2);

/*
    sprintf(TempTag,"goAwayFrom"2);
*/

        if (strcasecmp (method, "UNKNOWN") == 0)
            sipmesg->u.pRequest->pRequestLine->pMethod = strdup (unknownMethod);
        else
            sipmesg->u.pRequest->pRequestLine->pMethod = strdup (method);
        sipmesg->u.pRequest->pRequestLine->pVersion = strdup ("SIP/2.0");

        sip_initSipAddrSpec (&addrspec, SipAddrSipUri, err);
        sip_initSipUrl (&sipurl, err);
        sipurl->pHost = strdup ("mydomain.com");
        addrspec->u.pSipUrl = sipurl;
        addrspec->dType = SipAddrSipUri;
        sipmesg->u.pRequest->pRequestLine->pRequestUri = addrspec;
        /*sip_setReqLineInSipReqMsg (sipmesg, reqline, err); */
        /*
         * End of creation of request Line 
         */

        /*Creation and setting of from header */
        sip_initSipHeader (&header, SipHdrTypeFrom, err);
        ((SipFromHeader *) (header->pHeader))->pAddrSpec = addrspec;
        HSS_LOCKEDINCREF (addrspec->dRefCount);
        pFromTag = strdup ("fromtag1947");
        sip_setTagInFromHdr (header, pFromTag, err);
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
        /*Creation and setting of from header ends here */

        /*Creation and setting of To header */

        sip_initSipHeader (&header, SipHdrTypeTo, err);
        sip_initSipAddrSpec (&toaddr, SipAddrSipUri, err);
        sip_initSipUrl (&tosipurl, err);
        tosipurl->pHost = strdup ("yourdomain.com");
        toaddr->u.pSipUrl = tosipurl;
        toaddr->dType = SipAddrSipUri;
        ((SipFromHeader *) (header->pHeader))->pAddrSpec = toaddr;
        /*pToTag = strdup("totag2002");
           sip_setTagInToHdr(header,pToTag,err); */
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
/*Creation and setting of To header ends here */

/*Creation of Callid Header */

        sip_initSipHeader (&header, SipHdrTypeCallId, err);
        ((SipCallIdHeader *) (header->pHeader))->pString = strdup (TempCallId);
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
/*Creation and setting of Callid header ends here */

/*Creation of CSeq Header */
        sip_initSipHeader (&header, SipHdrTypeCseq, err);
        if ((strcmp (method, "BYE") == 0) || (strcmp (method, "PRACK") == 0))
            ((SipCseqHeader *) (header->pHeader))->dSeqNum = 2;
        else
            ((SipCseqHeader *) (header->pHeader))->dSeqNum = 1;

        ((SipCseqHeader *) (header->pHeader))->pMethod = strdup (method);
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
/*Creation and setting of CSeq header ends here */
/*
 * Via Header Creation
 */

        sip_initSipHeader (&header, SipHdrTypeVia, err);
        ((SipViaHeader *) (header->pHeader))->pSentProtocol =
            strdup ("SIP/2.0/UDP");
        ((SipViaHeader *) (header->pHeader))->pSentBy =
            strdup ("135.180.130.133");

        sip_initSipParam (&viaParam, err);
        viaParam->pName = strdup ("branch");
        sip_listAppend (&(viaParam->slValue), strdup ("Aricent"), err);

        sip_listAppend (&(((SipViaHeader *) (header->pHeader))->slParam),
                        viaParam, err);
        sip_insertHeaderAtIndex (sipmesg, header, 0, err);
        sip_freeSipHeader (header);
        free (header);

/*Via Header Ends */
/*Contact Header */

        sip_initSipHeader (&header, SipHdrTypeContactNormal, err);
        sip_initSipAddrSpec (&conaddr, SipAddrSipUri, err);
        sip_initSipUrl (&consipurl, err);
        consipurl->pHost = strdup ("hss.hns.com");
        consipurl->pUser = strdup ("kbinu");
        conaddr->u.pSipUrl = consipurl;
        conaddr->dType = SipAddrSipUri;
        ((SipContactHeader *) (header->pHeader))->pAddrSpec = conaddr;
        ((SipContactHeader *) (header->pHeader))->dType = SipContactNormal;
        sip_insertHeaderAtIndex (sipmesg, header, 0, err);
        sip_freeSipHeader (header);
        free (header);

/*Contact Header Ends*/

/*Content Length */
        sip_initSipHeader (&header, SipHdrTypeContentLength, err);
        ((SipContentLengthHeader *) (header->pHeader))->dLength = 0;
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);

        if (strcmp (method, "PRACK") == 0)
        {
            sip_initSipHeader (&header, SipHdrTypeRAck, err);
            ((SipRackHeader *) (header->pHeader))->dRespNum = 1;
            ((SipRackHeader *) (header->pHeader))->dCseqNum = 1;
            ((SipRackHeader *) (header->pHeader))->pMethod = strdup ("INVITE");
            sip_setHeader (sipmesg, header, err);
            sip_freeSipHeader (header);
            free (header);

        }

        options.dOption = SIP_OPT_FULLFORM | SIP_OPT_SINGLE;

        {
            SipTxnContext      *txncontext;
            en_SipTxnBypass     dbypass;
            sip_txn_initSipTxnContext (&txncontext, err);
            context = (SipEventContext *) malloc (sizeof (SipEventContext));
            txncontext->pEventContext = context;
            options.dOption = 0;
            txncontext->txnOption.dOption = options;
            if (glbenableretrans == 1)
            {
                char               *sendbuffer;
                SIP_U32bit          dLength;
                sendbuffer = (char *) malloc (sizeof (char) * MAXMESG);
                if (sip_formMessage
                    (sipmesg, &options, sendbuffer, &dLength, err) == SipFail)
                {
                    free (sendbuffer);
                    return SipFail;
                }
                txncontext->pEventContext->pData = (void *) sendbuffer;
            }
            else
            {
                sprintf (contextstr, "%s request to %s %d",
                         method, pSendAddr->dIpv4, pSendAddr->dPort);
                context->pData = strdup (contextstr);
            }
            txncontext->pEventContext->pDirectBuffer = SIP_NULL;

            if (glbtransactiontype == 2)
                dbypass = SipTxnByPass;
            else
                dbypass = SipTxnNoByPass;

            if (!glbtransactiontype)
                txncontext->dTxnType = SipUATypeTxn;
            else
                txncontext->dTxnType = SipProxyTypeTxn;

            txncontext->txnOption.dTimerOption = 0;
            if (glbsetTimerValue)
            {
                txncontext->timeoutValues.dT1 = dTimeOut.dT1;
                txncontext->timeoutValues.dT2 = dTimeOut.dT2;
                txncontext->timeoutValues.dTimerB = dTimeOut.dTimerB;
                txncontext->timeoutValues.dTimerC = dTimeOut.dTimerC;
                txncontext->timeoutValues.dTimerD_T3 = dTimeOut.dTimerD_T3;
                txncontext->timeoutValues.dTimerF_T3 = dTimeOut.dTimerF_T3;
                txncontext->timeoutValues.dTimerH = dTimeOut.dTimerH;
                txncontext->timeoutValues.dTimerI_T4 = dTimeOut.dTimerI_T4;
                txncontext->timeoutValues.dTimerJ_T3 = dTimeOut.dTimerJ_T3;
                txncontext->timeoutValues.dTimerK_T4 = dTimeOut.dTimerK_T4;
                txncontext->txnOption.dTimerOption = SIP_OPT_TIMERALL;
            }

            pTempTranspAddr = (SipTranspAddr *) fast_memget
                (0, sizeof (SipTranspAddr), err);
            *pTempTranspAddr = *pSendAddr;

            res = sip_txn_sendMessage (sipmesg, pTempTranspAddr, glbtransptype,
                                       txncontext, dbypass, &pSendTxnKey, err);
            free (txncontext);
            if (res == SipFail)
            {
                sip_freeEventContext (context);
            }
            else
            {
                if (pSendTxnKey != SIP_NULL)
                {
                    printf
                        ("\n***********************************************\n");
                    printf
                        ("The message resulted in creation of a new transaciton\n");
                    printf
                        ("***********************************************\n\n");
                    fast_lock_synch (0, &(dTxnKeyMutex), 0);
                    if (sip_listAppend (&glbslTxnKey, (void *) pSendTxnKey,
                                        err) == SipFail)
                    {
                        printf ("could not append to the list\n");
                        exit (0);
                    }
                    fast_unlock_synch (0, &(dTxnKeyMutex));
                }
                printf ("%s sent successfully.\n", method);
            }
        }
        sip_freeSipMessage (sipmesg);
        if (res == SipFail)
        {
            switch (*err)
            {
                case E_TXN_NO_EXIST:
                    printf ("** ERROR : The Txn for this message"
                            "doesn't exist**\n");
                    break;
                case E_TXN_EXISTS:
                    printf ("** ERROR : The Txn for this message"
                            "already exists**\n");
                    break;
                case E_TXN_INV_STATE:
                    printf ("** ERROR : This message leads to"
                            "an invalid transaction state**\n");
                    break;
                case E_TXN_INV_MSG:
                    printf ("** ERROR : This is an invalid message"
                            "for received for the Txn**\n");
                    break;
                default:
                    break;
            }
            printf ("** ERROR : Send Message Failed**\n");
        }
        return SipSuccess;
    }

    SipBool             sendResponse (int code, const char *reason,
                                      const char *method,
                                      SipTranspAddr * pSendAddr, SipError * err)
    {
        SipMessage         *sipmesg;
        SipHeader          *header;
        SipAddrSpec        *addrspec, *toaddr, *conaddr;
        SipUrl             *sipurl, *tosipurl, *consipurl;
        SipBool             res;
        SipEventContext    *context;
        char                contextstr[100];
        SipOptions          options;
        SipParam           *viaParam;
        SIP_U32bit          dRand2, dRand1;
        SIP_S8bit           TempCallId[101];
        SIP_S8bit           TempTag[101];
        SipTxnKey          *pSendTxnKey = SIP_NULL;
        SIP_S8bit          *pFromTag;
        SIP_S8bit          *pToTag;
        SipTranspAddr      *pTempTranspAddr = SIP_NULL;

        if (sip_initSipMessage (&sipmesg, SipMessageResponse, err) == SipFail)
        {
            printf ("Could not form message.\n");
            exit (0);
        }

        dRand1 = rand () % 100000;
        dRand2 = rand () % 100000;
        sprintf (TempCallId, "%d%d@139.85.229.21", dRand1, dRand2);
        sprintf (TempTag, "goAwayFrom%d", dRand2);

        sipmesg->u.pResponse->pStatusLine->pReason = strdup (reason);
        sipmesg->u.pResponse->pStatusLine->pVersion = strdup ("SIP/2.0");
        sipmesg->u.pResponse->pStatusLine->dCodeNum = (SIP_U16bit) code;

        /*Creation and setting of from header */
        sip_initSipHeader (&header, SipHdrTypeFrom, err);
        sip_initSipAddrSpec (&addrspec, SipAddrSipUri, err);
        sip_initSipUrl (&sipurl, err);
        sipurl->pHost = strdup ("yourdomain.com");
        addrspec->u.pSipUrl = sipurl;
        addrspec->dType = SipAddrSipUri;

        ((SipFromHeader *) (header->pHeader))->pAddrSpec = addrspec;
        pFromTag = strdup ("fromtag1947");
        sip_setTagInFromHdr (header, pFromTag, err);
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
        /*Creation and setting of from header ends here */

        /*Creation and setting of To header */

        sip_initSipHeader (&header, SipHdrTypeTo, err);
        sip_initSipAddrSpec (&toaddr, SipAddrSipUri, err);
        sip_initSipUrl (&tosipurl, err);
        tosipurl->pHost = strdup ("yourdomain.com");
        toaddr->u.pSipUrl = tosipurl;
        toaddr->dType = SipAddrSipUri;
        ((SipFromHeader *) (header->pHeader))->pAddrSpec = toaddr;
        pToTag = strdup ("totag2002");
        sip_setTagInToHdr (header, pToTag, err);
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
/*Creation and setting of To header ends here */

/*Creation of Callid Header */

        sip_initSipHeader (&header, SipHdrTypeCallId, err);
        ((SipCallIdHeader *) (header->pHeader))->pString =
            strdup ("1234324453@mydomain.com");
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
/*Creation and setting of Callid header ends here */

/*Creation of CSeq Header */
        sip_initSipHeader (&header, SipHdrTypeCseq, err);
        if (strcmp (method, "BYE") == 0 || (strcmp (method, "PRACK") == 0))
            ((SipCseqHeader *) (header->pHeader))->dSeqNum = 2;
        else
            ((SipCseqHeader *) (header->pHeader))->dSeqNum = 1;

        ((SipCseqHeader *) (header->pHeader))->pMethod = strdup (method);
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
/*Creation and setting of CSeq header ends here */
/*
 * Via Header Creation
 */

        sip_initSipHeader (&header, SipHdrTypeVia, err);
        ((SipViaHeader *) (header->pHeader))->pSentProtocol =
            strdup ("SIP/2.0/UDP");
        ((SipViaHeader *) (header->pHeader))->pSentBy =
            strdup ("135.180.130.133");

        sip_initSipParam (&viaParam, err);
        viaParam->pName = strdup ("branch");
        sip_listAppend (&(viaParam->slValue), strdup ("Aricent"), err);

        sip_listAppend (&(((SipViaHeader *) (header->pHeader))->slParam),
                        viaParam, err);
        sip_insertHeaderAtIndex (sipmesg, header, 0, err);
        sip_freeSipHeader (header);
        free (header);

/*Via Header Ends */
/*Contact Header */

        sip_initSipHeader (&header, SipHdrTypeContactNormal, err);
        sip_initSipAddrSpec (&conaddr, SipAddrSipUri, err);
        sip_initSipUrl (&consipurl, err);
        consipurl->pHost = strdup ("hss.hns.com");
        consipurl->pUser = strdup ("kbinu");
        conaddr->u.pSipUrl = consipurl;
        conaddr->dType = SipAddrSipUri;
        ((SipContactHeader *) (header->pHeader))->pAddrSpec = conaddr;
        ((SipContactHeader *) (header->pHeader))->dType = SipContactNormal;
        sip_insertHeaderAtIndex (sipmesg, header, 0, err);
        sip_freeSipHeader (header);
        free (header);

/*Contact Header Ends*/

/*Content Length */
        sip_initSipHeader (&header, SipHdrTypeContentLength, err);
        ((SipContentLengthHeader *) (header->pHeader))->dLength = 0;
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);

        if ((strcmp (method, "INVITE") == 0) && ((code < 200) && (code != 100)))
        {
            sip_initSipHeader (&header, SipHdrTypeRSeq, err);
            /*sip_rpr_setRespNumInRSeqHdr(header,1,err); */
            ((SipRseqHeader *) (header->pHeader))->dLength = 1;
            sip_setHeader (sipmesg, header, err);
            sip_freeSipHeader (header);
            free (header);
        }

        {
            SipTxnContext      *txncontext;
            en_SipTxnBypass     dbypass;
            sip_txn_initSipTxnContext (&txncontext, err);
            context = (SipEventContext *) malloc (sizeof (SipEventContext));
            context->pData = NULL;

            txncontext->pEventContext = context;
            options.dOption = 0;
            txncontext->txnOption.dOption = options;
            if (glbenableretrans == 1)
            {
                char               *sendbuffer;
                SIP_U32bit          dLength;
                sendbuffer = (char *) malloc (sizeof (char) * MAXMESG);
                if (sip_formMessage
                    (sipmesg, &options, sendbuffer, &dLength, err) == SipFail)
                {
                    free (sendbuffer);
                    return SipFail;
                }
                txncontext->pEventContext->pData = (void *) sendbuffer;
            }
            else
            {
                sprintf (contextstr, "%s request to %s %d",
                         method, pSendAddr->dIpv4, pSendAddr->dPort);
                context->pData = strdup (contextstr);
            }
            txncontext->pEventContext->pDirectBuffer = SIP_NULL;
            if (glbtransactiontype == 2)
                dbypass = SipTxnByPass;
            else
                dbypass = SipTxnNoByPass;

            if (!glbtransactiontype)
                txncontext->dTxnType = SipUATypeTxn;
            else
                txncontext->dTxnType = SipProxyTypeTxn;

            txncontext->txnOption.dTimerOption = 0;

            if (glbsetTimerValue)
            {
                txncontext->timeoutValues.dT1 = dTimeOut.dT1;
                txncontext->timeoutValues.dT2 = dTimeOut.dT2;
                txncontext->timeoutValues.dTimerB = dTimeOut.dTimerB;
                txncontext->timeoutValues.dTimerC = dTimeOut.dTimerC;
                txncontext->timeoutValues.dTimerD_T3 = dTimeOut.dTimerD_T3;
                txncontext->timeoutValues.dTimerF_T3 = dTimeOut.dTimerF_T3;
                txncontext->timeoutValues.dTimerH = dTimeOut.dTimerH;
                txncontext->timeoutValues.dTimerI_T4 = dTimeOut.dTimerI_T4;
                txncontext->timeoutValues.dTimerJ_T3 = dTimeOut.dTimerJ_T3;
                txncontext->timeoutValues.dTimerK_T4 = dTimeOut.dTimerK_T4;
                txncontext->txnOption.dTimerOption = SIP_OPT_TIMERALL;
            }

            pTempTranspAddr = (SipTranspAddr *) fast_memget
                (0, sizeof (SipTranspAddr), err);
            *pTempTranspAddr = *pSendAddr;
            res = sip_txn_sendMessage (sipmesg, pTempTranspAddr, glbtransptype,
                                       txncontext, dbypass, &pSendTxnKey, err);
            if (res == SipFail)
            {
                sip_freeEventContext (context);
            }
            else
            {
                if (pSendTxnKey != SIP_NULL)
                {
                    printf
                        ("\n***********************************************\n");
                    printf
                        ("The message resulted in creation of a new transaciton\n");
                    printf
                        ("***********************************************\n\n");
                    fast_lock_synch (0, &(dTxnKeyMutex), 0);
                    if (sip_listAppend (&glbslTxnKey, (void *) pSendTxnKey,
                                        err) == SipFail)
                    {
                        printf ("could not append to the list\n");
                        exit (0);
                    }
                    fast_unlock_synch (0, &(dTxnKeyMutex));
                }
                printf ("Response with code %d sent successfully.\n", code);
            }
        }
        sip_freeSipMessage (sipmesg);
        if (res == SipSuccess)
        {
            printf ("Response with code %d sent successfully.\n", code);
        }
        else
        {
            switch (*err)
            {
                case E_TXN_NO_EXIST:
                    printf ("** ERROR : The Txn for this message"
                            "doesn't exist**\n");
                    break;
                case E_TXN_EXISTS:
                    printf ("** ERROR : The Txn for this message"
                            "already exists**\n");
                    break;
                case E_TXN_INV_STATE:
                    printf ("** ERROR : This message leads to"
                            "an invalid transaction state**\n");
                    break;
                case E_TXN_INV_MSG:
                    printf ("** ERROR : This is an invalid message"
                            "for received for the Txn**\n");
                    break;
                default:
                    break;
            }
            printf ("** ERROR : Send Message Failed**\n");
        }
        return SipSuccess;
    }
    SipBool             sendMidRequest (const char *method,
                                        SipTranspAddr * sendaddr,
                                        SipError * err)
    {
        SipMessage         *sipmesg;
        SipReqLine         *reqline;
        SipHeader          *header;
        SipAddrSpec        *addrspec, *conaddr, *toaddr;
        SipUrl             *sipurl, *tosipurl, *consipurl;
        SipBool             res;
        SipEventContext    *context;
        SIP_S8bit           TempCallId[101];
        SIP_U32bit          dRand1, dRand2;
        SIP_S8bit           TempTag[101];
        SipTxnKey          *pSendTxnKey = SIP_NULL;
        SipParam           *viaParam;
        SIP_S8bit          *pToTag, *pFromTag;
        en_SipTxnAPICalled  dAPICalled = SipTxnMesgSent;
        SipTranspAddr      *pTempTranspAddr = SIP_NULL;

        char                contextstr[100];
        SipOptions          options;
        char                unknownMethod[100];

        if (strcasecmp (method, "UNKNOWN") == 0)
        {
            printf ("Method To be Sent:");
            scanf ("%s", unknownMethod);
            fflush (stdin);
        }

        if (sip_initSipMessage (&sipmesg, SipMessageRequest, err) == SipFail)
        {
            printf ("Could not form message.\n");
            exit (0);
        }

        dRand1 = 1234;
        dRand2 = 5678;
        sprintf (TempCallId, "%d%d@139.85.229.21", dRand1, dRand2);
        sprintf (TempTag, "goAwayFrom%d", dRand2);

/*
    sprintf(TempTag,"goAwayFrom"2);
*/

        sip_initSipReqLine (&reqline, err);
        if (strcasecmp (method, "UNKNOWN") == 0)
            reqline->pMethod = strdup (unknownMethod);
        else
            reqline->pMethod = strdup (method);
        reqline->pVersion = strdup ("SIP/2.0");

        sip_initSipAddrSpec (&addrspec, SipAddrSipUri, err);
        sip_initSipUrl (&sipurl, err);
        sipurl->pHost = strdup ("mydomain.com");
        addrspec->u.pSipUrl = sipurl;
        addrspec->dType = SipAddrSipUri;
        reqline->pRequestUri = addrspec;
        /*sip_setReqLineInSipReqMsg (sipmesg, reqline, err); */
        sipmesg->u.pRequest->pRequestLine = reqline;
        HSS_LOCKEDINCREF (reqline->dRefCount);

        sip_freeSipReqLine (reqline);
        /*
         * End of creation of request Line 
         */

        /*Creation and setting of from header */
        sip_initSipHeader (&header, SipHdrTypeFrom, err);
        ((SipFromHeader *) (header->pHeader))->pAddrSpec = addrspec;
        HSS_LOCKEDINCREF (addrspec->dRefCount);
        pFromTag = strdup ("fromtag1947");
        sip_setTagInFromHdr (header, pFromTag, err);
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
        /*Creation and setting of from header ends here */

        /*Creation and setting of To header */

        sip_initSipHeader (&header, SipHdrTypeTo, err);
        sip_initSipAddrSpec (&toaddr, SipAddrSipUri, err);
        sip_initSipUrl (&tosipurl, err);
        tosipurl->pHost = strdup ("yourdomain.com");
        toaddr->u.pSipUrl = tosipurl;
        toaddr->dType = SipAddrSipUri;
        ((SipFromHeader *) (header->pHeader))->pAddrSpec = toaddr;
        pToTag = strdup ("totag2002");
        sip_setTagInToHdr (header, pToTag, err);
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
/*Creation and setting of To header ends here */

/*Creation of Callid Header */

        sip_initSipHeader (&header, SipHdrTypeCallId, err);
        ((SipCallIdHeader *) (header->pHeader))->pString = strdup (TempCallId);
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
/*Creation and setting of Callid header ends here */

/*Creation of CSeq Header */
        sip_initSipHeader (&header, SipHdrTypeCseq, err);
        if ((strcmp (method, "BYE") == 0) || (strcmp (method, "PRACK") == 0))
            ((SipCseqHeader *) (header->pHeader))->dSeqNum = 2;
        else
            ((SipCseqHeader *) (header->pHeader))->dSeqNum = 1;

        ((SipCseqHeader *) (header->pHeader))->pMethod = strdup (method);
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
/*Creation and setting of CSeq header ends here */
/*
 * Via Header Creation
 */

        sip_initSipHeader (&header, SipHdrTypeVia, err);
        ((SipViaHeader *) (header->pHeader))->pSentProtocol =
            strdup ("SIP/2.0/UDP");
        ((SipViaHeader *) (header->pHeader))->pSentBy =
            strdup ("135.180.130.133");

        sip_initSipParam (&viaParam, err);
        viaParam->pName = strdup ("branch");
        sip_listAppend (&(viaParam->slValue), strdup ("Aricent"), err);

        sip_listAppend (&(((SipViaHeader *) (header->pHeader))->slParam),
                        viaParam, err);
        sip_insertHeaderAtIndex (sipmesg, header, 0, err);
        sip_freeSipHeader (header);
        free (header);

/*Via Header Ends */
/*Contact Header */

        sip_initSipHeader (&header, SipHdrTypeContactNormal, err);
        sip_initSipAddrSpec (&conaddr, SipAddrSipUri, err);
        sip_initSipUrl (&consipurl, err);
        consipurl->pHost = strdup ("hss.hns.com");
        consipurl->pUser = strdup ("kbinu");
        conaddr->u.pSipUrl = consipurl;
        conaddr->dType = SipAddrSipUri;
        ((SipContactHeader *) (header->pHeader))->pAddrSpec = conaddr;
        ((SipContactHeader *) (header->pHeader))->dType = SipContactNormal;
        sip_insertHeaderAtIndex (sipmesg, header, 0, err);
        sip_freeSipHeader (header);
        free (header);

/*Contact Header Ends*/

/*Content Length */
        sip_initSipHeader (&header, SipHdrTypeContentLength, err);
        ((SipContentLengthHeader *) (header->pHeader))->dLength = 0;
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);

        if (strcmp (method, "PRACK") == 0)
        {
            sip_initSipHeader (&header, SipHdrTypeRAck, err);
            ((SipRackHeader *) (header->pHeader))->dRespNum = 1;
            ((SipRackHeader *) (header->pHeader))->dCseqNum = 1;
            ((SipRackHeader *) (header->pHeader))->pMethod = strdup ("INVITE");
            sip_setHeader (sipmesg, header, err);
            sip_freeSipHeader (header);
            free (header);

        }

        options.dOption = SIP_OPT_FULLFORM | SIP_OPT_SINGLE;

        {
            SipTxnContext      *txncontext;
            en_SipTxnBypass     dbypass;
            sip_txn_initSipTxnContext (&txncontext, err);
            context = (SipEventContext *) malloc (sizeof (SipEventContext));
            txncontext->pEventContext = context;
            options.dOption = 0;
            txncontext->txnOption.dOption = options;
            if (glbenableretrans == 1)
            {
                char               *sendbuffer;
                SIP_U32bit          dLength;
                sendbuffer = (char *) malloc (sizeof (char) * MAXMESG);
                if (sip_formMessage
                    (sipmesg, &options, sendbuffer, &dLength, err) == SipFail)
                {
                    free (sendbuffer);
                    return SipFail;
                }
                txncontext->pEventContext->pData = (void *) sendbuffer;
            }
            else
            {
                sprintf (contextstr, "%s request to %s %d",
                         method, sendaddr->dIpv4, sendaddr->dPort);
                context->pData = strdup (contextstr);
            }
            txncontext->pEventContext->pDirectBuffer = SIP_NULL;

            if (glbtransactiontype == 2)
                dbypass = SipTxnByPass;
            else
                dbypass = SipTxnNoByPass;

            if (!glbtransactiontype)
                txncontext->dTxnType = SipUATypeTxn;
            else
                txncontext->dTxnType = SipProxyTypeTxn;

            txncontext->txnOption.dTimerOption = 0;
            if (glbsetTimerValue)
            {
                txncontext->timeoutValues.dT1 = dTimeOut.dT1;
                txncontext->timeoutValues.dT2 = dTimeOut.dT2;
                txncontext->timeoutValues.dTimerB = dTimeOut.dTimerB;
                txncontext->timeoutValues.dTimerC = dTimeOut.dTimerC;
                txncontext->timeoutValues.dTimerD_T3 = dTimeOut.dTimerD_T3;
                txncontext->timeoutValues.dTimerF_T3 = dTimeOut.dTimerF_T3;
                txncontext->timeoutValues.dTimerH = dTimeOut.dTimerH;
                txncontext->timeoutValues.dTimerI_T4 = dTimeOut.dTimerI_T4;
                txncontext->timeoutValues.dTimerJ_T3 = dTimeOut.dTimerJ_T3;
                txncontext->timeoutValues.dTimerK_T4 = dTimeOut.dTimerK_T4;
                txncontext->txnOption.dTimerOption = SIP_OPT_TIMERALL;
            }

            pTempTranspAddr = (SipTranspAddr *) fast_memget
                (0, sizeof (SipTranspAddr), err);
            *pTempTranspAddr = *sendaddr;

            res =
                sip_txn_createMidwayTxn (sipmesg, pTempTranspAddr,
                                         glbtransptype, txncontext, dAPICalled,
                                         &pSendTxnKey, err);
            if (res == SipSuccess)
            {
                printf ("%s sent successfully.\n", method);
                if (pSendTxnKey != SIP_NULL)
                {
                    printf
                        ("\n***********************************************\n");
                    printf
                        ("The message resulted in creation of a new transaciton\n");
                    printf
                        ("***********************************************\n\n");
                    fast_lock_synch (0, &(dTxnKeyMutex), 0);
                    if (sip_listAppend (&glbslTxnKey, (void *) pSendTxnKey,
                                        err) == SipFail)
                    {
                        printf ("could not append to the list\n");
                        exit (0);
                    }
                    fast_unlock_synch (0, &(dTxnKeyMutex));
                }
                free (txncontext);
            }
            else
            {
                if (res == SipFail)
                    sip_freeEventContext (context);
                free (txncontext);
            }
        }
        sip_freeSipMessage (sipmesg);
        if (res == SipFail)
        {
            switch (*err)
            {
                case E_TXN_NO_EXIST:
                    printf ("** ERROR : The Txn for this message"
                            "doesn't exist**\n");
                    break;
                case E_TXN_EXISTS:
                    printf ("** ERROR : The Txn for this message"
                            "already exists**\n");
                    break;
                case E_TXN_INV_STATE:
                    printf ("** ERROR : This message leads to"
                            "an invalid transaction state**\n");
                    break;
                case E_TXN_INV_MSG:
                    printf ("** ERROR : This is an invalid message"
                            "for received for the Txn**\n");
                    break;
                default:
                    break;
            }
            printf ("** ERROR : Send Message Failed**\n");
        }
        return SipSuccess;
    }

/* Function used to send simple requests */
    SipBool             sendMidResponse (int code, const char *reason,
                                         const char *method,
                                         SipTranspAddr * sendaddr,
                                         SipError * err)
    {
        SipMessage         *sipmesg;
        SipStatusLine      *statline;
        SipHeader          *header;
        SipAddrSpec        *addrspec, *toaddr, *conaddr;
        SipUrl             *sipurl, *tosipurl, *consipurl;
        SipBool             res;
        SipEventContext    *context;
        char                contextstr[100];
        SipOptions          options;
        SIP_U32bit          dRand2, dRand1;
        SIP_S8bit           TempCallId[101];
        SIP_S8bit           TempTag[101];
        SipTxnKey          *pSendTxnKey = SIP_NULL;
        SIP_S8bit          *pToTag, *pFromTag;
        SipParam           *viaParam;
        en_SipTxnAPICalled  dAPICalled = SipTxnMesgSent;

        if (sip_initSipMessage (&sipmesg, SipMessageResponse, err) == SipFail)
        {
            printf ("Could not form message.\n");
            exit (0);
        }

        dRand1 = rand () % 100000;
        dRand2 = rand () % 100000;
        sprintf (TempCallId, "%d%d@139.85.229.21", dRand1, dRand2);
        sprintf (TempTag, "goAwayFrom%d", dRand2);

        sip_initSipStatusLine (&statline, err);
        statline->pReason = strdup (reason);
        statline->pVersion = strdup ("SIP/2.0");
        statline->dCodeNum = (SIP_U16bit) 200;
        /*sip_setStatusLineInSipRespMsg (sipmesg, statline, err); */
        sipmesg->u.pResponse->pStatusLine = statline;
        HSS_LOCKEDINCREF (statline->dRefCount);

        sip_freeSipStatusLine (statline);

        /*Creation and setting of from header */
        sip_initSipHeader (&header, SipHdrTypeFrom, err);
        sip_initSipAddrSpec (&addrspec, SipAddrSipUri, err);
        sip_initSipUrl (&sipurl, err);
        sipurl->pHost = strdup ("yourdomain.com");
        addrspec->u.pSipUrl = sipurl;
        addrspec->dType = SipAddrSipUri;

        ((SipFromHeader *) (header->pHeader))->pAddrSpec = addrspec;
        pFromTag = strdup ("fromtag1947");
        sip_setTagInFromHdr (header, pFromTag, err);
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
        /*Creation and setting of from header ends here */

        /*Creation and setting of To header */

        sip_initSipHeader (&header, SipHdrTypeTo, err);
        sip_initSipAddrSpec (&toaddr, SipAddrSipUri, err);
        sip_initSipUrl (&tosipurl, err);
        tosipurl->pHost = strdup ("yourdomain.com");
        toaddr->u.pSipUrl = tosipurl;
        toaddr->dType = SipAddrSipUri;
        ((SipFromHeader *) (header->pHeader))->pAddrSpec = toaddr;
        pToTag = strdup ("totag2002");
        sip_setTagInToHdr (header, pToTag, err);
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
/*Creation and setting of To header ends here */

/*Creation of Callid Header */

        sip_initSipHeader (&header, SipHdrTypeCallId, err);
        ((SipCallIdHeader *) (header->pHeader))->pString =
            strdup ("1234324453@mydomain.com");
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
/*Creation and setting of Callid header ends here */

/*Creation of CSeq Header */
        sip_initSipHeader (&header, SipHdrTypeCseq, err);
        if (strcmp (method, "BYE") == 0 || (strcmp (method, "PRACK") == 0))
            ((SipCseqHeader *) (header->pHeader))->dSeqNum = 2;
        else
            ((SipCseqHeader *) (header->pHeader))->dSeqNum = 1;

        ((SipCseqHeader *) (header->pHeader))->pMethod = strdup (method);
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);
/*Creation and setting of CSeq header ends here */
/*
 * Via Header Creation
 */

        sip_initSipHeader (&header, SipHdrTypeVia, err);
        ((SipViaHeader *) (header->pHeader))->pSentProtocol =
            strdup ("SIP/2.0/UDP");
        ((SipViaHeader *) (header->pHeader))->pSentBy =
            strdup ("135.180.130.133");

        sip_initSipParam (&viaParam, err);
        viaParam->pName = strdup ("branch");
        sip_listAppend (&(viaParam->slValue), strdup ("Aricent"), err);

        sip_listAppend (&(((SipViaHeader *) (header->pHeader))->slParam),
                        viaParam, err);
        sip_insertHeaderAtIndex (sipmesg, header, 0, err);
        sip_freeSipHeader (header);
        free (header);

/*Via Header Ends */
/*Contact Header */

        sip_initSipHeader (&header, SipHdrTypeContactNormal, err);
        sip_initSipAddrSpec (&conaddr, SipAddrSipUri, err);
        sip_initSipUrl (&consipurl, err);
        consipurl->pHost = strdup ("hss.hns.com");
        consipurl->pUser = strdup ("kbinu");
        conaddr->u.pSipUrl = consipurl;
        conaddr->dType = SipAddrSipUri;
        ((SipContactHeader *) (header->pHeader))->pAddrSpec = conaddr;
        ((SipContactHeader *) (header->pHeader))->dType = SipContactNormal;
        sip_insertHeaderAtIndex (sipmesg, header, 0, err);
        sip_freeSipHeader (header);
        free (header);

/*Contact Header Ends*/

/*Content Length */
        sip_initSipHeader (&header, SipHdrTypeContentLength, err);
        ((SipContentLengthHeader *) (header->pHeader))->dLength = 0;
        sip_setHeader (sipmesg, header, err);
        sip_freeSipHeader (header);
        free (header);

        if ((strcmp (method, "INVITE") == 0) && ((code < 200) && (code != 100)))
        {
            sip_initSipHeader (&header, SipHdrTypeRSeq, err);
            /*sip_rpr_setRespNumInRSeqHdr(header,1,err); */
            ((SipRseqHeader *) (header->pHeader))->dLength = 1;
            sip_setHeader (sipmesg, header, err);
            sip_freeSipHeader (header);
            free (header);
        }

        {
            SipTxnContext      *txncontext;
            en_SipTxnBypass     dbypass;
            sip_txn_initSipTxnContext (&txncontext, err);
            context = (SipEventContext *) malloc (sizeof (SipEventContext));
            context->pData = NULL;

            txncontext->pEventContext = context;
            options.dOption = 0;
            txncontext->txnOption.dOption = options;
            if (glbenableretrans == 1)
            {
                char               *sendbuffer;
                SIP_U32bit          dLength;
                sendbuffer = (char *) malloc (sizeof (char) * MAXMESG);
                if (sip_formMessage
                    (sipmesg, &options, sendbuffer, &dLength, err) == SipFail)
                {
                    free (sendbuffer);
                    return SipFail;
                }
                txncontext->pEventContext->pData = (void *) sendbuffer;
            }
            else
            {
                sprintf (contextstr, "%s request to %s %d",
                         method, sendaddr->dIpv4, sendaddr->dPort);
                context->pData = strdup (contextstr);
            }
            txncontext->pEventContext->pDirectBuffer = SIP_NULL;
            if (glbtransactiontype == 2)
                dbypass = SipTxnByPass;
            else
                dbypass = SipTxnNoByPass;

            if (!glbtransactiontype)
                txncontext->dTxnType = SipUATypeTxn;
            else
                txncontext->dTxnType = SipProxyTypeTxn;

            txncontext->txnOption.dTimerOption = 0;

            if (glbsetTimerValue)
            {
                txncontext->timeoutValues.dT1 = dTimeOut.dT1;
                txncontext->timeoutValues.dT2 = dTimeOut.dT2;
                txncontext->timeoutValues.dTimerB = dTimeOut.dTimerB;
                txncontext->timeoutValues.dTimerC = dTimeOut.dTimerC;
                txncontext->timeoutValues.dTimerD_T3 = dTimeOut.dTimerD_T3;
                txncontext->timeoutValues.dTimerF_T3 = dTimeOut.dTimerF_T3;
                txncontext->timeoutValues.dTimerH = dTimeOut.dTimerH;
                txncontext->timeoutValues.dTimerI_T4 = dTimeOut.dTimerI_T4;
                txncontext->timeoutValues.dTimerJ_T3 = dTimeOut.dTimerJ_T3;
                txncontext->timeoutValues.dTimerK_T4 = dTimeOut.dTimerK_T4;
                txncontext->txnOption.dTimerOption = SIP_OPT_TIMERALL;
            }

            res = sip_txn_createMidwayTxn (sipmesg, sendaddr, glbtransptype,
                                           txncontext, dAPICalled, &pSendTxnKey,
                                           err);
            if (res == SipFail)
            {
                sip_freeEventContext (context);
            }
            else
            {
                if (pSendTxnKey != SIP_NULL)
                {
                    printf
                        ("\n***********************************************\n");
                    printf
                        ("The message resulted in creation of a new transaciton\n");
                    printf
                        ("***********************************************\n\n");
                    fast_lock_synch (0, &(dTxnKeyMutex), 0);
                    if (sip_listAppend (&glbslTxnKey, (void *) pSendTxnKey,
                                        err) == SipFail)
                    {
                        printf ("could not append to the list\n");
                        exit (0);
                    }
                    fast_unlock_synch (0, &(dTxnKeyMutex));
                }
                printf ("Response with code %d sent successfully.\n", code);
            }
        }
        sip_freeSipMessage (sipmesg);
        if (res == SipSuccess)
        {
            printf ("Response with code %d sent successfully.\n", code);
        }
        else
        {
            switch (*err)
            {
                case E_TXN_NO_EXIST:
                    printf ("** ERROR : The Txn for this message"
                            "doesn't exist**\n");
                    break;
                case E_TXN_EXISTS:
                    printf ("** ERROR : The Txn for this message"
                            "already exists**\n");
                    break;
                case E_TXN_INV_STATE:
                    printf ("** ERROR : This message leads to"
                            "an invalid transaction state**\n");
                    break;
                case E_TXN_INV_MSG:
                    printf ("** ERROR : This is an invalid message"
                            "for received for the Txn**\n");
                    break;
                default:
                    break;
            }
            printf ("** ERROR : Send Message Failed**\n");
        }
        return SipSuccess;
    }

    void               *decodeMessageStats (void *ptr)
    {
        SipBool             r;
        en_SipTxnDecodeResult dResult;
        SipTxnContext       txncontext;
        SipEventContext     context;
        SipOptions          opt;
        char               *message = NULL;
        SIP_S8bit          *nextmesg = NULL;
        int                 n;
#ifdef SIP_INCREMENTAL_PARSING
        int                 j;
#endif
        SipError            error;
        SipTxnKey          *pTxnKey = SIP_NULL;

        ptr = NULL;
#ifdef SIP_INCREMENTAL_PARSING
        context.pList = SIP_NULL;
        if (context.pList == SIP_NULL)
            context.pList = (SipHdrTypeList *)
                fast_memget (0, sizeof (SipHdrTypeList), SIP_NULL);
        for (j = 0; j < HEADERTYPENUM; j++)
            context.pList->enable[j] = SipSuccess;

#endif

        context.pData = SIP_NULL;
        context.pDirectBuffer = SIP_NULL;

        opt.dOption = 0;
        /* Filling up the event context */
        txncontext.pEventContext = &context;
        txncontext.dTxnType = (en_SipTxnType) glbtransactiontype;
        txncontext.txnOption.dOption = opt;
        txncontext.txnOption.dTimerOption = 0;

        for (;;)
        {
            unsigned            dSize = 0;
            /* pick up from the queue */
            /* Lock Mutex before accessing message queue */
            fast_lock_synch (0, &(dMessageQueueMutex), 0);

            if (sip_listSizeOf (&slMessageQueue, &dSize, &error) == SipFail)
            {
                fast_lock_synch (0, &(dMessageQueueMutex), 0);
                sdf_fn_uaSleep (0, 10);
                continue;
            }

            if (dSize == 0)
                /*
                 * Queue has no elements currently. Continue in loop
                 */
            {
                fast_unlock_synch (0, &(dMessageQueueMutex));
                sdf_fn_uaSleep (0, 10);
                continue;
            }
            else
            {
                char               *pMessage;
                if (sip_listGetAt (&slMessageQueue, 0, (void **) &pMessage,
                                   &error) == SipFail)
                {
                    fast_unlock_synch (0, &(dMessageQueueMutex));
                    sdf_fn_uaSleep (0, 10);
                    continue;
                }
                message = strdup (pMessage);
                n = strlen (message);
                sip_listDeleteAt (&slMessageQueue, 0, &error);

                do
                {
                    SipMessage         *outMsg;
                    SipError            derror;
                    printf ("|---Message Received from the network----------"
                            "--------------------------------|\n");
                    printf ("\n%s\n", message);
                    printf ("|---End of message received from the network---"
                            "--------------------------------|\n\n");
                    r = sip_txn_decodeMessage ((SIP_S8bit *) message, &outMsg,
                                               &txncontext, n, &nextmesg,
                                               &dResult, &pTxnKey, &derror);
                    if (r == SipFail)
                    {
                        printf ("Txn Decode Failed: %d\n", derror);
                        switch (derror)
                        {
                            case E_INCOMPLETE:
                                printf ("Missing Bytes: %d\n",
                                        context.dRemainingLength);
                                break;
                            case E_TXN_NO_EXIST:
                                printf
                                    ("\n*****************************************\n");
                                printf (" Txn doesn't exist\n");
                                printf
                                    ("*****************************************\n\n");
                                break;
                            case E_TXN_EXISTS:
                                printf
                                    ("\n*****************************************\n");
                                printf ("Txn Already exists\n");
                                printf
                                    ("*****************************************\n\n");
                                break;
                            case E_TXN_INV_MSG:
                                printf
                                    ("\n*****************************************\n");
                                printf
                                    ("Invalid Message for the transaciton\n");
                                printf
                                    ("*****************************************\n\n");
                                break;
                            case E_TXN_INV_STATE:
                                printf
                                    ("\n*****************************************\n");
                                printf
                                    ("Message received for an invalid state\n");
                                printf
                                    ("*****************************************\n\n");
                                break;
                            case E_PARSER_ERROR:
                                printf
                                    ("\n*****************************************\n");
                                printf
                                    ("Parser error for the message received\n");
                                printf
                                    ("*****************************************\n\n");
                                break;
                            default:
                                printf ("Error in transaction decode\n");
                        }
                    }
                    else
                    {
                        switch (dResult)
                        {
                            case SipTxnIgnorable:
                                printf
                                    ("\n***********************************************\n");
                                printf
                                    (" The message can be ignored by the application.\n");
                                printf
                                    ("***********************************************\n\n");
                                break;
                            case SipTxnNonIgnorable:
                                printf
                                    ("\n***********************************************\n");
                                printf
                                    ("The message needs to be handled by the application.\n");
                                printf
                                    ("***********************************************\n\n");
                                break;
                            case SipTxnStrayMessage:
                                printf
                                    ("\n***********************************************\n");
                                printf
                                    ("The message is a Stray message, It can be ignored.\n");
                                printf
                                    ("***********************************************\n\n");
                                break;
                            case SipTxnQueued:
                                printf
                                    ("\n***********************************************\n");
                                printf
                                    ("The message is a retransmission.It is queued.\n");
                                printf
                                    ("***********************************************\n\n");
                                break;
                            case SipTxnConfirmnNeeded:
                                printf
                                    ("\n***********************************************\n");
                                printf
                                    ("The message needs to be handled by the application.\n");
                                printf
                                    ("***********************************************\n\n");
                                break;
                            default:
                                printf
                                    ("\n***********************************************\n");
                                printf
                                    ("The message received is not of recognized type.\n");
                                printf
                                    ("***********************************************\n\n");
                        }
                        if (pTxnKey != SIP_NULL)
                        {
                            printf
                                ("\n***********************************************\n");
                            printf
                                ("The message resulted in creation of a new transaciton\n");
                            printf
                                ("***********************************************\n\n");
                            fast_lock_synch (0, &(dTxnKeyMutex), 0);
                            if (sip_listAppend (&glbslTxnKey, (void *) pTxnKey,
                                                &derror) == SipFail)
                            {
                                printf ("could not append to the list\n");
                                exit (0);
                            }
                            fast_unlock_synch (0, &(dTxnKeyMutex));
                        }
                        displayParsedMessage (outMsg);
                    }

                    fast_memfree (0, message, SIP_NULL);
                    /* See if read buffer had more than one message in it */
                    message = nextmesg;
                    /* Length of the remaining segment is in event-context */
                    n = context.dNextMessageLength;

                }
                while (message != SIP_NULL);
#ifdef SIP_INCREMENTAL_PARSING
                if (context.pList != SIP_NULL)
                    fast_memfree (0, context.pList, SIP_NULL);
#endif

                fast_unlock_synch (0, &(dMessageQueueMutex));
                printf ("%s\n", mainmenu);
            }
        }
        return NULL;
    }

    SipBool             sip_setTagInFromHdr (SipHeader * hdr, SIP_S8bit * tag,
                                             SipError * err)
    {
        SipParam           *pParam = SIP_NULL;
        SIPDEBUGFN ("Entering function sip_setTaginFrom/To Headers");
#ifdef SIP_VALIDATE
        if (err == SIP_NULL)
            return SipFail;

        if (hdr == SIP_NULL)
        {
            *err = E_INV_HEADER;
            return SipFail;
        }

#endif

        sip_initSipParam (&pParam, err);
        pParam->pName = sip_strdup ((SIP_S8bit *) "tag", 0);
        if (sip_listInsertAt (&pParam->slValue, 0, (SIP_Pvoid) tag, err) ==
            SipFail)
        {
            sip_freeSipParam (pParam);
            return SipFail;
        }

        if (sip_listAppend
            (&(((SipFromHeader *) (hdr->pHeader))->slParam), (SIP_Pvoid) pParam,
             err) == SipFail)
        {
            sip_freeSipParam (pParam);
            return SipFail;
        }

        SIPDEBUGFN ("Entering function sip_setTaginFrom/To Headers");
        return SipSuccess;

    }

    void                sip_freeTimerHandle (SIP_Pvoid pTimerHandle)
    {
        /*Making this a dummy function since the actual freeing of the
         * timer handle is being handled thru the application itself.
         */
        (void) pTimerHandle;
    }

#ifdef __cplusplus
}
#endif
