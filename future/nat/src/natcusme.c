/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: natcusme.c,v 1.7 2014/02/14 13:57:30 siva Exp $
 *
 * Description:This file contains routines to support CuSeeMe Appln.
 *
 *******************************************************************/

/********************************************************************
* FILE NAME         : natcusme.c
* PRINCIPLE AUTHOR  : Arumugam. V
* SUBSYSTEM NAME    : IP
* MODULE NAME       : Nat
* LANGUAGE          : C
* TARGET ENVIRONMENT: Any
* DATE OF FIRST REL : 
* AUTHOR            : Arumugam. V
* DESCRIPTION       : This file contains routines to support CuSeeMe Appln.
*********************************************************************
* 
* Change History
* Version           :
* Date (DD/MM/YYYY) :
* Modified by       :
* Description       :
*********************************************************************/
#ifndef _NATCUSME_C
#define _NATCUSME_C
#include "natinc.h"

/****************************************************************************
* Function Name :  NatTranslateCuSeeMePktIn
* Description   : This function Translates IP address in Inbound CuSeeMe pkt.
*                 
* Input (s)  : 1. pBuf - Inbound Pkt CRU buffer pointer
*            : 2. pHeaderInfo - Pkt Header Info
* Output (s) : The changed pkt payload
* Returns    : SUCCESS/FAILURE
*****************************************************************************/

PRIVATE INT4
NatTranslateCuSeeMePktIn (tCRU_BUF_CHAIN_HEADER * pBuf,
                          tHeaderInfo * pHeaderInfo)
{
    tCuSeeMeHeader     *pCuSeeMeHeader = NULL;
    tOCHeader          *pOCHeader = NULL;
    tClientInfo        *pClientInfo = NULL;
    UINT4               u4LocIpAddr = NAT_ZERO;
    INT4                i4ChangeFlag = NAT_FALSE;
    UINT2               u2Count = NAT_ZERO;
    UINT2               u2Offset = NAT_ZERO;
    UINT2               u2TransportCksum = NAT_ZERO;
    UINT2               u2CksumOffset = NAT_ZERO;
    UINT2               u2DataLen = NAT_ZERO;
    UINT2               u2CuSeeMeOffset = NAT_ZERO;
    tLinearBuf         *pLinearBuf = NULL;

    u2DataLen = (UINT2) (pHeaderInfo->u2TotLen -
                         (pHeaderInfo->u1IpHeadLen +
                          pHeaderInfo->u1TransportHeadLen));

    if (u2DataLen >= sizeof (tCuSeeMeHeader))
    {
        /* allocate memory to copy data portion to linear buffer */
        if (NAT_MEM_ALLOCATE
            (NAT_LINEAR_BUF_POOL_ID, pLinearBuf, tLinearBuf) == NULL)
            /* pLinearBuf = (UINT1 *) NAT_MALLOC (u2DataLen * sizeof (UINT1)); */
        {
            NAT_TRC (NAT_TRC_ON, "\n CUSeeMe: Buffer allocation failure");
            return (NAT_FAILURE);
        }

        /* Copy CuSeeMe Header from CRU to linear buffer for translation */
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pLinearBuf,
                                   (UINT4) (pHeaderInfo->u1IpHeadLen +
                                            pHeaderInfo->u1TransportHeadLen),
                                   u2DataLen);

        u2CksumOffset =
            (UINT2) (NAT_UDP_CKSUM_OFFSET + pHeaderInfo->u1IpHeadLen);
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2TransportCksum,
                                   u2CksumOffset, NAT_WORD_LEN);

        u4LocIpAddr = OSIX_HTONL (pHeaderInfo->u4InIpAddr);
        pCuSeeMeHeader = (tCuSeeMeHeader *) (VOID *) pLinearBuf;

        if (pCuSeeMeHeader->u4DestAddr)
        {
            /* adjust UDP checksum */
            NatChecksumAdjust ((UINT1 *) &u2TransportCksum,
                               (UINT1 *) &(pCuSeeMeHeader->u4DestAddr),
                               NAT_IP_ADDR_LEN,
                               (UINT1 *) &u4LocIpAddr, NAT_IP_ADDR_LEN);

            pCuSeeMeHeader->u4DestAddr = u4LocIpAddr;
            i4ChangeFlag = NAT_TRUE;
        }

        if ((OSIX_NTOHS (pCuSeeMeHeader->u2DataType) == CUSEEME_DATATYPE) &&
            (u2DataLen > ((sizeof (tCuSeeMeHeader) + sizeof (tOCHeader)))))
        {

            u2Offset = sizeof (tCuSeeMeHeader) + sizeof (tOCHeader);
            u2CuSeeMeOffset = sizeof (tCuSeeMeHeader);
            pOCHeader = (tOCHeader *) (VOID *) (pLinearBuf + u2CuSeeMeOffset);

            /* Translate the address present in the client information */
            for (u2Count = NAT_ZERO;
                 ((u2Offset + sizeof (tClientInfo)) <= u2DataLen) &&
                 (u2Count < OSIX_NTOHS (pOCHeader->u2ClientCount)); u2Count++)
            {
                pClientInfo = (tClientInfo *) (VOID *) (pLinearBuf + u2Offset);

                if (pClientInfo->u4Addr ==
                    OSIX_HTONL (pHeaderInfo->pDynamicEntry->
                                u4TranslatedLocIpAddr))
                {
                    /* adjust UDP checksum */
                    NatChecksumAdjust ((UINT1 *) &u2TransportCksum,
                                       (UINT1 *) &(pClientInfo->u4Addr),
                                       NAT_IP_ADDR_LEN,
                                       (UINT1 *) &u4LocIpAddr, NAT_IP_ADDR_LEN);

                    pClientInfo->u4Addr = u4LocIpAddr;
                    i4ChangeFlag = NAT_TRUE;
                    break;
                }
                else
                {
                    u2Offset =
                        (UINT2) (u2Offset + (UINT2) (sizeof (tClientInfo)));
                }
            }
        }

        if (i4ChangeFlag == NAT_TRUE)
        {
            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2TransportCksum,
                                       u2CksumOffset, NAT_WORD_LEN);
            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) pLinearBuf,
                                       (UINT4) (pHeaderInfo->u1IpHeadLen +
                                                pHeaderInfo->
                                                u1TransportHeadLen), u2DataLen);
        }
        NatMemReleaseMemBlock (NAT_LINEAR_BUF_POOL_ID, (UINT1 *) pLinearBuf);
    }
    return (NAT_SUCCESS);
}

/****************************************************************************
* Function Name :  NatTranslateCuSeeMePktOut
* Description   : This function Translates IP Address in outbound CuSeeMe pkt. 
*                 
* Input (s)  : 1. pBuf - Outbound Pkt CRU buffer pointer
*            : 2. pHeaderInfo - Pkt Header Info
* Output (s) : The changed pkt payload
* Returns    : SUCCESS/FAILURE
*****************************************************************************/

PRIVATE INT4
NatTranslateCuSeeMePktOut (tCRU_BUF_CHAIN_HEADER * pBuf,
                           tHeaderInfo * pHeaderInfo)
{
    tCuSeeMeHeader      CuSeeMeHeader;
    UINT4               u4TransLocIpAddr = NAT_ZERO;
    UINT4               u4CksumOffset = NAT_ZERO;
    UINT2               u2TransportCksum = NAT_ZERO;
    UINT2               u2DataLen = NAT_ZERO;
    INT4                i4ChangeFlag = NAT_FALSE;

    u2DataLen = (UINT2) (pHeaderInfo->u2TotLen -
                         (pHeaderInfo->u1IpHeadLen + NAT_UDP_HEADER_LENGTH));

    if (u2DataLen >= sizeof (tCuSeeMeHeader))
    {
        /* Copy CuSeeMe Header from CRU to linear buffer for translation */
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &CuSeeMeHeader,
                                   (UINT4) pHeaderInfo->u1IpHeadLen +
                                   NAT_UDP_HEADER_LENGTH,
                                   sizeof (tCuSeeMeHeader));

        u4TransLocIpAddr = OSIX_HTONL (pHeaderInfo->u4InIpAddr);
        if (CuSeeMeHeader.u4Addr)
        {

            /* adjust UDP checksum */
            u4CksumOffset =
                NAT_UDP_CKSUM_OFFSET + (UINT4) pHeaderInfo->u1IpHeadLen;
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2TransportCksum,
                                       u4CksumOffset, NAT_WORD_LEN);
            NatChecksumAdjust ((UINT1 *) &u2TransportCksum,
                               (UINT1 *) &CuSeeMeHeader.u4Addr, NAT_IP_ADDR_LEN,
                               (UINT1 *) &u4TransLocIpAddr, NAT_IP_ADDR_LEN);

            CuSeeMeHeader.u4Addr = u4TransLocIpAddr;

            /* Copy data to CRU buffer from linear buffer */
            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &CuSeeMeHeader,
                                       (UINT4) pHeaderInfo->u1IpHeadLen +
                                       NAT_UDP_HEADER_LENGTH,
                                       sizeof (tCuSeeMeHeader));
            if (i4ChangeFlag != NAT_TRUE)
            {
                i4ChangeFlag = NAT_TRUE;
            }

            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2TransportCksum,
                                       u4CksumOffset, NAT_WORD_LEN);
        }
    }
    return (NAT_SUCCESS);
}

/****************************************************************************
* Function Name :  NatProcessCuSeeMePkt
* Description   : This function Translates IP Address in Inbound/Outbound
*                 CuSeeMe pkt.
*                 
* Input (s)  : 1. pBuf - Incoming Pkt CRU buffer pointer
*            : 2. pHeaderInfo - Pkt Header Info
* Output (s) : The changed pkt payload
* Returns    : SUCCESS/FAILURE
*****************************************************************************/

PUBLIC INT4
NatProcessCuSeeMePkt (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo)
{
    INT4                i4Status = NAT_ZERO;

    if (pHeaderInfo->u4Direction == NAT_OUTBOUND)
    {
        i4Status = NatTranslateCuSeeMePktOut (pBuf, pHeaderInfo);
    }
    else
    {
        i4Status = NatTranslateCuSeeMePktIn (pBuf, pHeaderInfo);
    }
    return (i4Status);
}
#endif /* _NATCUSME_C */
