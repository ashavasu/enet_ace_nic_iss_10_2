/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: nathead.c,v 1.10 2015/04/08 14:28:37 siva Exp $
 *
 * Description: This file contains header translation functions.
 *
 *******************************************************************/
#ifndef _NATHEAD_C
#define _NATHEAD_C
#include "natinc.h"
extern tTMO_SLL     gNatTcpDelStack;
/***************************************************************************
* Function Name    :  NatTransportCksumAdjust                 *
* Description    :  This function calculates the checksum for the    *
*             TCP/UDP if the Translated Local IP is of length 12   *
*                                      *
* Input (s)    :  1. pBuf - Total IP Payload               *
*             2. pHeaderInfo - Contains Header information     *
*                                      *
* Output (s)    :  None                         *
* Returns      :  New TCP/UDP checksum                 *
*                                      *
****************************************************************************/

PUBLIC UINT2
NatTransportCksumAdjust (tCRU_BUF_CHAIN_HEADER * pBuf,
                         tHeaderInfo * pHeaderInfo)
{
    INT4                i4Temp = NAT_ZERO;
    UINT4               u4ChkSum = NAT_ZERO;
    UINT4               u4Src = NAT_ZERO, u4Dst = NAT_ZERO;
    UINT4               u4Count = NAT_ZERO;
    UINT2               u2TotLen = NAT_ZERO, u2Temp = NAT_ZERO;
    INT2                i2Len = NAT_ZERO;
    UINT1               u1TransportHdrStart = NAT_ZERO;
    UINT1               u1TransportHlen = NAT_ZERO;
    UINT2               u2Val = NAT_ZERO;
    UINT1               u1Val = NAT_ZERO;
    UINT4               u4NoOfValidBytes = NAT_ZERO;

    u1TransportHdrStart = pHeaderInfo->u1IpHeadLen;
    u4Src = OSIX_HTONL (NatGetSrcIpAddr (pBuf));
    u4Dst = OSIX_HTONL (NatGetDestIpAddr (pBuf));
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2TotLen, NAT_TOT_LEN_OFFSET,
                               NAT_TOL_LEN_FIELD);
    u2TotLen = OSIX_NTOHS (u2TotLen);
    /* The Pseudo Header Part for Cksum Calculation */
    u4ChkSum = NAT_ZERO;
    u4ChkSum += u4Src & NAT_SET_16_BITS;
    u4ChkSum += (u4Src >> NAT_SIXTEEN) & NAT_SET_16_BITS;
    u4ChkSum += u4Dst & NAT_SET_16_BITS;
    u4ChkSum += (u4Dst >> NAT_SIXTEEN) & NAT_SET_16_BITS;
    u2Temp = pHeaderInfo->u1PktType & NAT_MASK_WITH_VAL_255;
    u4ChkSum += OSIX_HTONS (u2Temp);
    i2Len = (INT2) (u2TotLen - (UINT2) u1TransportHdrStart);

    u4ChkSum += OSIX_HTONS (i2Len);

    /* Computation of checksum of transport header and payload */
    u1TransportHlen = pHeaderInfo->u1TransportHeadLen;

    u4NoOfValidBytes = CRU_BUF_Get_ChainValidByteCount (pBuf);
    u4NoOfValidBytes -= (UINT4) pHeaderInfo->u1IpHeadLen;

    for (u4Count = NAT_ZERO; u4Count < u4NoOfValidBytes
         - NAT_ONE; u4Count += NAT_TWO)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2Val,
                                   (pHeaderInfo->u1IpHeadLen + u4Count),
                                   NAT_TWO_BYTES);
        u4ChkSum += u2Val;
    }
    /* Adding the Last Odd Byte */
    if ((u4NoOfValidBytes & NAT_EXTRACT_LAST_ODD_BYTE) != NAT_ZERO)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1Val,
                                   (pHeaderInfo->u1IpHeadLen + u4Count),
                                   NAT_ONE_BYTE);
        ((UINT1 *) &i4Temp)[NAT_INDEX_0] = u1Val;
        u4ChkSum += (UINT4) i4Temp;
    }

    u4ChkSum = (u4ChkSum >> NAT_SIXTEEN) + (u4ChkSum & NAT_SET_16_BITS);
    u4ChkSum = (u4ChkSum >> NAT_SIXTEEN) + (u4ChkSum & NAT_SET_16_BITS);

    if (u1TransportHlen == NAT_ZERO)
    {
        return ((UINT2) ((~((UINT2) u4ChkSum)) & NAT_SET_16_BITS));
    }
    return ((UINT2) ((~((UINT2) u4ChkSum)) & NAT_SET_16_BITS));
}

/***************************************************************************
* Function Name    :  NatDataAdjust                    *
* Description    :  This function does bit stuffing.            *
*                                      *
* Input (s)    :  1. pNewData - The data where bit stuffing should    *
*            be done.                      *
*             2. i4NewDataLen - Length of the data         *
*             3. i4OldDataLen - Length of the data for the new    *
*            data should be stuffed for.            *
*                                      *
* Output (s)    :  None                         *
* Returns      :  None                         *
*                                      *
****************************************************************************/

PUBLIC VOID
NatDataAdjust (UINT1 *pNewData, INT4 i4NewDataLen, INT4 i4OldDataLen)
{
    UINT4               u4Index = NAT_ZERO;
    UINT4               u4CommaCount = NAT_ZERO;
    UINT4               u4Status = NAT_ZERO;

    u4Index = NAT_ZERO;
    u4CommaCount = NAT_ZERO;
    u4Status = NAT_FAILURE;

    /*
     * This function checks whether the length of the new and the old IP addr
     * are both even or odd  .
     * If one of the two  is odd and the other even then the new IP address has
     * to stuffed with '0' byte to make the length odd (if the length of old IP
     * address is odd) or even (if the length of the old IP address is even).
     */
    if ((((i4NewDataLen % MODULO_DIVISOR_2) != NAT_ZERO)
         && ((i4OldDataLen % MODULO_DIVISOR_2) == NAT_ZERO))
        || (((i4NewDataLen % MODULO_DIVISOR_2) == NAT_ZERO)
            && ((i4OldDataLen % MODULO_DIVISOR_2) != NAT_ZERO)))
    {

        /* stuff in NewData; */
        if (pNewData[u4Index] == ' ')
        {
            u4Index++;
        }

        if (((pNewData[u4Index + NAT_ONE] == ',')
             || (pNewData[u4Index + NAT_ONE] == '.'))
            || ((pNewData[u4Index + NAT_TWO] == ',')
                || (pNewData[u4Index + NAT_TWO] == '.')))
        {
            pNewData[i4NewDataLen + NAT_ONE] = '\0';
            while (i4NewDataLen > (INT4) u4Index)
            {
                pNewData[i4NewDataLen] = pNewData[i4NewDataLen - NAT_ONE];
                i4NewDataLen--;
            }
            pNewData[u4Index] = '0';
            u4Status = NAT_SUCCESS;
        }
        else
        {
            while (u4Index <= (UINT4) i4NewDataLen)
            {
                if ((pNewData[u4Index] == ',') || (pNewData[u4Index] == '.'))
                {
                    if ((pNewData[u4Index + NAT_INDEX_2] == ',')
                        || (pNewData[u4Index + NAT_INDEX_2] == '.')
                        || (pNewData[u4Index + NAT_INDEX_3] == ',')
                        || (pNewData[u4Index + NAT_INDEX_3] == '.'))
                    {
                        pNewData[i4NewDataLen + NAT_ONE] = '\0';
                        while (i4NewDataLen > (INT4) u4Index)
                        {
                            pNewData[i4NewDataLen] = pNewData[i4NewDataLen
                                                              - NAT_ONE];
                            i4NewDataLen--;
                        }

                        pNewData[u4Index + NAT_ONE] = '0';
                        u4Status = NAT_SUCCESS;
                        break;
                    }
                    u4CommaCount++;
                    if (u4CommaCount == NAT_INDEX_3)
                    {
                        break;
                    }
                }
                u4Index++;
            }
        }
        if (u4Status == NAT_FAILURE)
        {
            if ((i4NewDataLen - (INT4) u4Index) < NAT_FOUR)
            {
                pNewData[i4NewDataLen + NAT_ONE] = '\0';
                while (i4NewDataLen > (INT4) u4Index)
                {
                    pNewData[i4NewDataLen] = pNewData[i4NewDataLen - NAT_ONE];
                    i4NewDataLen--;
                }
                pNewData[u4Index + NAT_ONE] = '0';
            }
        }
    }
}

/***************************************************************************
* Function Name    :  NatChecksumAdjustForIpHeader             *
* Description    :  This function calculates the checksum header.    *
*                                      *
* Input (s)    :  1. pu1Chksum - IP Checksum  with TCP/UDP header    *
*            and payload.                    *
*             2. pu1TcpChksum - pointer to TCP checksum        *
*             3. pOldData - Old Data which is to be replaced.    *
*             4. i4OldDataLen - Old Data's length          *
*             5. pNewData - New Data which is going to replace    *
*            the old one.                    *
*             6. i4NewDataLen - New Data's Length          *
* Output (s)    :  None                         *
* Returns      :  Length of the IP header.                *
*                                      *
****************************************************************************/

PUBLIC VOID
NatChecksumAdjustForIpHeader (UINT1 *pu1Chksum, UINT1 *pu1TcpChksum,
                              UINT1 *pOldData, INT4 i4OldDataLen,
                              UINT1 *pNewData, INT4 i4NewDataLen)
{
    INT4                i4Temp = NAT_ZERO;
    INT4                i4Old = NAT_ZERO;
    INT4                i4New = NAT_ZERO;
    INT4                i4TcpTemp = NAT_ZERO;
    INT4                i4TcpOld = NAT_ZERO;
    INT4                i4TcpNew = NAT_ZERO;

    i4New = NAT_ZERO;
    i4Old = NAT_ZERO;
    i4TcpNew = NAT_ZERO;
    i4TcpOld = NAT_ZERO;

    i4Temp = pu1Chksum[NAT_INDEX_0] * NAT_VAL_256 + pu1Chksum[NAT_INDEX_1];
    i4Temp = ~i4Temp & NAT_SET_16_BITS_LONG /*0xffffL */ ;
    i4TcpTemp = pu1TcpChksum[NAT_INDEX_0] * NAT_VAL_256 +
        pu1TcpChksum[NAT_INDEX_1];
    i4TcpTemp = ~i4TcpTemp & NAT_SET_16_BITS_LONG;
    while (i4OldDataLen > NAT_ZERO)
    {
        i4Old = pOldData[NAT_INDEX_0] * NAT_VAL_256 + pOldData[NAT_INDEX_1];
        i4TcpOld = pOldData[NAT_INDEX_0] * NAT_VAL_256 + pOldData[NAT_INDEX_1];
        pOldData += NAT_TWO;
        i4Temp -= i4Old & NAT_SET_16_BITS_LONG;
        i4TcpTemp -= i4TcpOld & NAT_SET_16_BITS_LONG;
        if (i4Temp <= NAT_ZERO)
        {
            i4Temp--;
            i4Temp &= NAT_SET_16_BITS_LONG;
        }
        if (i4TcpTemp <= NAT_ZERO)
        {
            i4TcpTemp--;
            i4TcpTemp &= NAT_SET_16_BITS_LONG;
        }
        i4OldDataLen -= NAT_TWO;
    }
    while (i4NewDataLen > NAT_ZERO)
    {
        i4New = pNewData[NAT_INDEX_0] * NAT_VAL_256 + pNewData[NAT_INDEX_1];
        i4TcpNew = pNewData[NAT_INDEX_0] * NAT_VAL_256 + pNewData[NAT_INDEX_1];
        pNewData += NAT_TWO;
        i4Temp += i4New & NAT_SET_16_BITS_LONG;
        i4TcpTemp += i4TcpNew & NAT_SET_16_BITS_LONG;
        if (i4Temp & NAT_EXTRACT_LSB1_BIT_LONG /*0x10000L */ )
        {
            i4Temp++;
            i4Temp &= NAT_SET_16_BITS_LONG;
        }
        if (i4TcpTemp & NAT_EXTRACT_LSB1_BIT_LONG /* 0x10000L */ )
        {
            i4TcpTemp++;
            i4TcpTemp &= NAT_SET_16_BITS_LONG;
        }
        i4NewDataLen -= NAT_TWO;
    }
    i4Temp = ~i4Temp & NAT_SET_16_BITS_LONG;
    pu1Chksum[NAT_INDEX_0] = (UINT1) (i4Temp / NAT_VAL_256);
    pu1Chksum[NAT_INDEX_1] = (UINT1) (i4Temp & NAT_SET_8_BITS_LONG /*0xffL */ );
    i4TcpTemp = ~i4TcpTemp & NAT_SET_16_BITS_LONG;
    pu1TcpChksum[NAT_INDEX_0] = (UINT1) (i4TcpTemp / NAT_VAL_256);
    pu1TcpChksum[NAT_INDEX_1] = (UINT1) (i4TcpTemp & NAT_SET_8_BITS_LONG);
    /* 0xffL */
}

/***************************************************************************
* Function Name    :  NatChecksumAdjust                    *
* Description    :  This function calculates the checksum header.
*                                      *
* Input (s)    :  1. pu1Chksum - pointer to old Cksum   
*                 2. pOldData -  pointer to the old data "which will be 
*                                replaced".
*                 3.i4OldDataLen - Number of bytes in pOldData               
*                 4. pNewData - pointer to the new data "which will replace" the
*                               old data.
*                 5.i4NewDataLen - Number of bytes in pNewData               
*
* Output (s)    :  pointer to the New Cksum (pu1Chksum)                         *
* Returns      :  None                *
*                                      *
****************************************************************************/
PUBLIC VOID
NatChecksumAdjust (UINT1 *pu1Chksum, UINT1 *pOldData, INT4 i4OldDataLen,
                   UINT1 *pNewData, INT4 i4NewDataLen)
{
    INT4                i4Temp = NAT_ZERO;
    INT4                i4Old = NAT_ZERO;
    INT4                i4New = NAT_ZERO;

    /*
     * this function does not calculate the Cksum.It just adjusts the Cksum
     * according to the change in data .
     */
    i4New = NAT_ZERO;
    i4Old = NAT_ZERO;
    i4Temp = pu1Chksum[NAT_INDEX_0] * NAT_VAL_256 + pu1Chksum[NAT_INDEX_1];
    /* Get the complement of Cksum */
    i4Temp = ~i4Temp & NAT_SET_16_BITS_LONG;
    if ((i4OldDataLen % MODULO_DIVISOR_2) != NAT_ZERO)
    {
        pOldData[i4OldDataLen - NAT_ONE] = NAT_ZERO;
    }
    /*Subtract old data from the ~Cksum */
    while (i4OldDataLen > NAT_ZERO)
    {
        i4Old = pOldData[NAT_ZERO] * NAT_VAL_256 + pOldData[NAT_INDEX_1];
        pOldData += NAT_TWO;
        i4Temp -= i4Old & NAT_SET_16_BITS_LONG;
        if (i4Temp <= NAT_ZERO)
        {
            i4Temp--;
            i4Temp &= NAT_SET_16_BITS_LONG;
        }
        i4OldDataLen -= NAT_TWO;
    }
    /*Add new data to the ~Cksum */
    while (i4NewDataLen > NAT_ZERO)
    {
        i4New = pNewData[NAT_INDEX_0] * NAT_VAL_256 + pNewData[NAT_INDEX_1];
        pNewData += NAT_TWO;
        i4Temp += i4New & NAT_SET_16_BITS_LONG;
        if (i4Temp & NAT_EXTRACT_LSB1_BIT_LONG /*0x10000L */ )
        {
            i4Temp++;
            i4Temp &= NAT_SET_16_BITS_LONG;
        }
        i4NewDataLen -= NAT_TWO;
    }
    i4Temp = ~i4Temp & NAT_SET_16_BITS_LONG;
    pu1Chksum[NAT_INDEX_0] = (UINT1) (i4Temp / NAT_VAL_256);
    pu1Chksum[NAT_INDEX_1] = (UINT1) (i4Temp & NAT_SET_8_BITS_LONG /*0xffL */ );
}

/***************************************************************************
* Function Name    :  NatIpHeaderModify
* Description    :  This function modifies the IP header with corresponding
*             values from  pHeaderInfo.
*             The fields modified are source IP address , destination
*             IP address , Total length and Cksum
*
* Input (s)    :  1. pBuf - This stores the full IP packet along
*            with TCP/UDP header and payload.
*             2. pHeaderInfo - Contains the modified information.
*
* Output (s)    :  Modified pBuf
* Returns      :  None
*
****************************************************************************/

PUBLIC VOID
NatIpHeaderModify (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo)
{
    UINT4               u4SrcIpAddr = NAT_ZERO;
    UINT4               u4DstIpAddr = NAT_ZERO;
    UINT2               u2TotLen = NAT_ZERO;
    UINT2               u2NewTotLen = NAT_ZERO;
    UINT2               u2CheckSum = NAT_ZERO;
    UINT2               u2TransportCksum = NAT_ZERO;
    UINT4               u4OldIpAddr = NAT_ZERO;
    UINT4               u4NewIpAddr = NAT_ZERO;
    UINT2               u2NewTcpLen = NAT_ZERO;
    UINT2               u2TcpLen = NAT_ZERO;
    UINT4               u4PktLen = NAT_ZERO;
    UINT2               u2Flag = TRUE;

    u2TransportCksum = NAT_ZERO;
    u4SrcIpAddr = u4DstIpAddr = NAT_ZERO;
    /* Depending upon the direction update the IP header */
    if (pHeaderInfo->u4Direction == NAT_OUTBOUND)
    {
        u4SrcIpAddr = OSIX_HTONL (pHeaderInfo->u4InIpAddr);
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u4OldIpAddr,
                                   NAT_IP_SRC_OFFSET, NAT_IP_ADDR_LEN);
    }
    else
    {
        u4DstIpAddr = OSIX_HTONL (pHeaderInfo->u4InIpAddr);
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u4OldIpAddr,
                                   NAT_IP_DST_OFFSET, NAT_IP_ADDR_LEN);
    }
    /* changing the total length */
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2TotLen, NAT_TOT_LEN_OFFSET,
                               NAT_TOL_LEN_FIELD);
    u2TotLen = OSIX_NTOHS (u2TotLen);
    u2NewTotLen = (UINT2) (u2TotLen + (UINT2) pHeaderInfo->i4Delta);
    u2NewTotLen = OSIX_HTONS (u2NewTotLen);
    u2TcpLen = (UINT2) (u2TotLen - (UINT2) pHeaderInfo->u1IpHeadLen);
    u2NewTcpLen = (UINT2) (u2TcpLen + (UINT2) pHeaderInfo->i4Delta);
    u2TcpLen = OSIX_HTONS (u2TcpLen);
    u2NewTcpLen = OSIX_HTONS (u2NewTcpLen);
    u2TotLen = OSIX_HTONS (u2TotLen);

    u4PktLen = CRU_BUF_Get_ChainValidByteCount (pBuf);
    if (pHeaderInfo->u4Direction == NAT_OUTBOUND)
    {
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4SrcIpAddr,
                                   NAT_SRC_IP_OFFSET, NAT_IP_ADDR_LEN);
        u4NewIpAddr = u4SrcIpAddr;
        if (u4NewIpAddr != u4OldIpAddr)
        {
            /*
             * the old IP address and the new IP address are different so
             * adjust the IP header Cksum and hence the TCP/UDP header Cksum.
             */
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2CheckSum,
                                       NAT_IP_CHKSUM_OFFSET, NAT_WORD_LEN);
            if (pHeaderInfo->u1PktType == NAT_TCP)
            {
                if (u4PktLen >= (UINT4) (NAT_TCP_CKSUM_OFFSET +
                                         pHeaderInfo->u1IpHeadLen +
                                         NAT_WORD_LEN))
                {
                    CRU_BUF_Copy_FromBufChain (pBuf,
                                               (UINT1 *) &u2TransportCksum,
                                               NAT_TCP_CKSUM_OFFSET +
                                               (UINT4) pHeaderInfo->u1IpHeadLen,
                                               NAT_WORD_LEN);
                }
                else
                {
                    u2Flag = FALSE;
                }
            }
            else
            {
                if (pHeaderInfo->u1PktType == NAT_UDP)
                {
                    if (u4PktLen >= (UINT4) (NAT_UDP_CKSUM_OFFSET +
                                             pHeaderInfo->u1IpHeadLen +
                                             NAT_WORD_LEN))
                    {
                        CRU_BUF_Copy_FromBufChain (pBuf,
                                                   (UINT1 *) &u2TransportCksum,
                                                   NAT_UDP_CKSUM_OFFSET +
                                                   (UINT4) pHeaderInfo->
                                                   u1IpHeadLen, NAT_WORD_LEN);
                        if (u2TransportCksum == NAT_PAYLOAD_NULL /*0x00 */ )
                        {
                            u2Flag = FALSE;
                        }
                    }
                    else
                    {
                        u2Flag = FALSE;
                    }
                }
            }
            NatChecksumAdjustForIpHeader ((UINT1 *) &u2CheckSum,
                                          (UINT1 *) &u2TransportCksum,
                                          (UINT1 *) &u4OldIpAddr,
                                          NAT_IP_ADDR_LEN,
                                          (UINT1 *) &u4NewIpAddr,
                                          NAT_IP_ADDR_LEN);
            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2CheckSum,
                                       NAT_IP_CHKSUM_OFFSET, NAT_WORD_LEN);
            if ((pHeaderInfo->u1PktType == NAT_TCP) && (u2Flag == TRUE))
            {
                CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2TransportCksum,
                                           NAT_TCP_CKSUM_OFFSET +
                                           (UINT4) pHeaderInfo->u1IpHeadLen,
                                           NAT_WORD_LEN);
            }
            else
            {
                if ((pHeaderInfo->u1PktType == NAT_UDP) && (u2Flag == TRUE))
                {
                    CRU_BUF_Copy_OverBufChain (pBuf,
                                               (UINT1 *) &u2TransportCksum,
                                               NAT_UDP_CKSUM_OFFSET +
                                               (UINT4) pHeaderInfo->u1IpHeadLen,
                                               NAT_WORD_LEN);
                }
            }
        }
    }
    else
    {
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4DstIpAddr,
                                   NAT_DEST_IP_OFFSET, NAT_IP_ADDR_LEN);
        u4NewIpAddr = u4DstIpAddr;
        if (u4NewIpAddr != u4OldIpAddr)
        {
            /*
             * the old IP address and the new IP address are different so
             * adjust the IP header Cksum and hence the TCP/UDP header Cksum.
             */
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2CheckSum,
                                       NAT_IP_CHKSUM_OFFSET, NAT_WORD_LEN);
            if (pHeaderInfo->u1PktType == NAT_TCP)
            {
                if (u4PktLen >= (UINT4) (NAT_TCP_CKSUM_OFFSET +
                                         pHeaderInfo->u1IpHeadLen +
                                         NAT_WORD_LEN))
                {
                    CRU_BUF_Copy_FromBufChain (pBuf,
                                               (UINT1 *) &u2TransportCksum,
                                               NAT_TCP_CKSUM_OFFSET +
                                               (UINT4) pHeaderInfo->u1IpHeadLen,
                                               NAT_WORD_LEN);
                }
                else
                {
                    u2Flag = FALSE;
                }
            }
            else
            {
                if (pHeaderInfo->u1PktType == NAT_UDP)
                {
                    if (u4PktLen >= (UINT4) (NAT_UDP_CKSUM_OFFSET +
                                             pHeaderInfo->u1IpHeadLen +
                                             NAT_WORD_LEN))
                    {
                        CRU_BUF_Copy_FromBufChain (pBuf,
                                                   (UINT1 *) &u2TransportCksum,
                                                   NAT_UDP_CKSUM_OFFSET +
                                                   (UINT4) pHeaderInfo->
                                                   u1IpHeadLen, NAT_WORD_LEN);
                        if (u2TransportCksum == NAT_PAYLOAD_NULL)
                        {
                            u2Flag = FALSE;
                        }
                    }
                    else
                    {
                        u2Flag = FALSE;
                    }
                }

            }
            /* For IPSec and IKE the Dynamic Entries
             * are maintained  in IKE and IPSec Tables
             * Refer Files natike.c and natipsec.c */
            if (pHeaderInfo->pDynamicEntry != NULL)
            {
                u4OldIpAddr =
                    OSIX_HTONL (pHeaderInfo->pDynamicEntry->
                                u4TranslatedLocIpAddr);
            }
            NatChecksumAdjustForIpHeader ((UINT1 *) &u2CheckSum,
                                          (UINT1 *) &u2TransportCksum,
                                          (UINT1 *) &u4OldIpAddr,
                                          NAT_IP_ADDR_LEN,
                                          (UINT1 *) &u4NewIpAddr,
                                          NAT_IP_ADDR_LEN);
            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2CheckSum,
                                       NAT_IP_CHKSUM_OFFSET, NAT_WORD_LEN);
            if ((pHeaderInfo->u1PktType == NAT_TCP) && (u2Flag == TRUE))
            {
                CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2TransportCksum,
                                           NAT_TCP_CKSUM_OFFSET +
                                           (UINT4) pHeaderInfo->u1IpHeadLen,
                                           NAT_WORD_LEN);
            }
            else
            {
                if ((pHeaderInfo->u1PktType == NAT_UDP) && (u2Flag == TRUE))
                {
                    CRU_BUF_Copy_OverBufChain (pBuf,
                                               (UINT1 *) &u2TransportCksum,
                                               NAT_UDP_CKSUM_OFFSET +
                                               (UINT4) pHeaderInfo->u1IpHeadLen,
                                               NAT_WORD_LEN);
                }
            }
        }
    }
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2CheckSum,
                               NAT_IP_CHKSUM_OFFSET, NAT_WORD_LEN);
    if (pHeaderInfo->u1PktType == NAT_TCP)
    {
        if (u2Flag == TRUE)
        {
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2TransportCksum,
                                       NAT_TCP_CKSUM_OFFSET +
                                       (UINT4) pHeaderInfo->u1IpHeadLen,
                                       NAT_WORD_LEN);
        }
    }
    else
    {
        if ((pHeaderInfo->u1PktType == NAT_UDP) && (u2Flag == TRUE))
        {
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2TransportCksum,
                                       NAT_UDP_CKSUM_OFFSET +
                                       (UINT4) pHeaderInfo->u1IpHeadLen,
                                       NAT_WORD_LEN);
        }
    }
    /*
     * if the Total length differs then adjust the Cksum accordingly
     */
    if (u2TotLen != u2NewTotLen)
    {
        NatChecksumAdjust ((UINT1 *) &u2CheckSum, (UINT1 *) &u2TotLen,
                           NAT_WORD_LEN, (UINT1 *) &u2NewTotLen, NAT_WORD_LEN);
        NatChecksumAdjust ((UINT1 *) &u2TransportCksum, (UINT1 *) &u2TcpLen,
                           NAT_WORD_LEN, (UINT1 *) &u2NewTcpLen, NAT_WORD_LEN);
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2NewTotLen,
                                   NAT_TOT_LEN_OFFSET, NAT_TOL_LEN_FIELD);
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2CheckSum,
                                   NAT_IP_CHKSUM_OFFSET, NAT_WORD_LEN);
        if ((pHeaderInfo->u1PktType == NAT_TCP) && (u2Flag == TRUE))
        {
            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2TransportCksum,
                                       NAT_TCP_CKSUM_OFFSET +
                                       (UINT4) pHeaderInfo->u1IpHeadLen,
                                       NAT_WORD_LEN);
        }
    }

}

/***************************************************************************
* Function Name    :  NatTcpHeaderModify
* Description    :  This function modifies the TCP header.
*             The fields modified are Source Port/Destination Port,
*             , Sequence Number and Cksum.
*             Also delete expired TCP entries if FIN bit is set.
*
*
* Input (s)    :  1. pBuf - This stores the full IP packet along
*            with TCP/UDP header and payload.
*             2. pHeaderInfo - Contains the modified information.
*
* Output (s)    :  Modified pBuf
* Returns      :  None
*
****************************************************************************/

PUBLIC VOID
NatTcpHeaderModify (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo)
{
    UINT4               u4SrcPortOffset = NAT_ZERO;
    UINT4               u4DestPortOffset = NAT_ZERO;
    UINT2               u2TransportCksum = NAT_ZERO;
    UINT4               u4SeqNoOffset = NAT_ZERO;
    UINT4               u4AckNoOffset = NAT_ZERO;
    UINT4               u4CurrentSeqNo = NAT_ZERO;
    UINT4               u4OldCurrentSeqNo = NAT_ZERO;
    UINT4               u4CurrentAckNo = NAT_ZERO;
    UINT4               u4OldCurrentAckNo = NAT_ZERO;
    UINT4               u4FinByteOffset = NAT_ZERO;
    UINT4               u4Status = NAT_FAILURE;
    UINT4               u4NextSeqNo = NAT_ZERO;
    UINT4               u4AckNo = NAT_ZERO;
    UINT1               u1FinBit = NAT_ZERO;
    UINT1               u1SynBit = NAT_ZERO;
    UINT1               u1AckBit = NAT_ZERO;
    UINT1               u1RstBit = NAT_ZERO;
    UINT1               u1CodeByte = NAT_ZERO;
    UINT4               u4PktLen = NAT_ZERO;
    UINT2               u2Flag = TRUE;
    UINT2               u2LenOfPkt = NAT_ZERO;
    UINT2               u2Mss = NAT_ZERO;
    UINT2               u2OldMss = NAT_ZERO;
    UINT4               u4MssOffset = NAT_ZERO;
    UINT4               u4Mtu = NAT_ZERO;
    UINT4               u4OptionsOffset = NAT_ZERO;
    UINT1               u1Optionkind;
    tNatSeqAckHistory   NatSeqAckNDelta, *pNatPrevSeqAckHistory = NULL;
    tTcpDelNode        *pTcpDelNode = NULL;
    UINT1               u1DelFlag = FALSE;

    u1FinBit = NAT_ZERO;
    u4FinByteOffset = NAT_ZERO;
    u4CurrentAckNo = u4CurrentSeqNo = NAT_ZERO;
    u4DestPortOffset = u4SrcPortOffset = NAT_ZERO;

    MEMSET (&NatSeqAckNDelta, NAT_ZERO, sizeof (tNatSeqAckHistory));

    u4PktLen = CRU_BUF_Get_ChainValidByteCount (pBuf);
    u4SrcPortOffset = pHeaderInfo->u1IpHeadLen;
    u4DestPortOffset = u4SrcPortOffset + NAT_PORT_LEN;

    u4SeqNoOffset = u4DestPortOffset + NAT_PORT_LEN;
    u4AckNoOffset = u4SeqNoOffset + NAT_SEQ_NUM_LEN;

    u4FinByteOffset = u4SrcPortOffset + NAT_TCP_CODE_BYTE_OFFSET;
    CRU_BUF_Copy_FromBufChain (pBuf, &u1CodeByte, u4FinByteOffset,
                               NAT_ONE_BYTE);

    u4OptionsOffset = (UINT4) pHeaderInfo->u1IpHeadLen + NAT_TCP_OPTIONS_OFFSET;
    CRU_BUF_Copy_FromBufChain (pBuf, &u1Optionkind, u4OptionsOffset,
                               NAT_ONE_BYTE);

    u1SynBit = u1CodeByte & NAT_TCP_SYN_BIT;

    /* If Fin bit is set, update the Nat TCP Del Stack so that the TCP session
     * entry can be deleted in the Dynamic Table
     */
    u1FinBit = u1CodeByte & NAT_TCP_FIN_BIT;
    if (u1FinBit == NAT_TCP_FIN_BIT_SET)
    {
        u1AckBit = u1CodeByte & NAT_TCP_ACK_BIT_SET;
        NatDeleteTcpConnection (pHeaderInfo, u1AckBit);
    }

    /* TcpHdrModify does the following changes:
     * 1) Modify the TCP port in the TCP header.
     * 2) Based on the Direction Seq No & Ack No in the TCP header values 
     *    are changed according to the values of delta in SeqAckHistory 
     *    Table.
     * 3) Tcp ChkSum modification.
     */

    /* Calculating the length of the TCP payload which is used in 
     * delta history. */
    u2LenOfPkt = (UINT2) ((UINT2) (pHeaderInfo->u2TotLen) -
                          (UINT2) pHeaderInfo->u1TransportHeadLen -
                          (UINT2) pHeaderInfo->u1IpHeadLen);

    if (pHeaderInfo->u4Direction == NAT_OUTBOUND)
    {
        /* 1) Modify the Tcp port in TCP header. */
        NatPortModify (pBuf, pHeaderInfo, pHeaderInfo->u2InPort,
                       u4SrcPortOffset,
                       NAT_TCP_CKSUM_OFFSET + (UINT4) pHeaderInfo->u1IpHeadLen);

        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u4CurrentSeqNo,
                                   u4SeqNoOffset, NAT_SEQ_NUM_LEN);
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u4CurrentAckNo,
                                   u4AckNoOffset, NAT_ACK_NUM_LEN);
        u4OldCurrentSeqNo = u4CurrentSeqNo;
        u4OldCurrentAckNo = u4CurrentAckNo;
        u4CurrentSeqNo = OSIX_NTOHL (u4CurrentSeqNo);
        u4CurrentAckNo = OSIX_NTOHL (u4CurrentAckNo);

        /* 
         * If Payload translation is done before then we need to apply Delta
         * accordingly for Seq No, Ack No for the subsequent packets of 
         * session. 
         */
        if (pHeaderInfo->pDynamicEntry->u1DeltaChangeFlag ==
            NAT_SEQ_ACK_HISTORY)
        {
            /* 
             * As the packet is an OutBound pkt we copy 
             * Seq no of pBuf --> InSeq(Inside seq no) 
             * Ack no of pBuf --> OutSeqDelta (Outside seq no with 
             *                                   prev delta change)
             */
            NatSeqAckNDelta.u4InSeq = u4CurrentSeqNo;
            NatSeqAckNDelta.u4OutSeqDelta = u4CurrentAckNo;

            /* Get the Delta change to be applied for the current packet. */
            u4Status = NatGetDeltaForSeqAck (pHeaderInfo, &NatSeqAckNDelta);

            /* 
             * If the Status of Get operation is not failed then we have 
             * InDelta & OutDelta which is to be applied for the packet. 
             */
            if (u4Status != NAT_FAILURE)
            {
                /* 
                 * Session has payload change update the Seq & Ack no fields 
                 * with OutDelta & InDelta respectively.
                 * 1) Current Seq no = Current Seq no + OutDeltachange.
                 * 2) Current Ack no = Current Ack no - InDeltaChange.
                 */

                u4CurrentSeqNo += (UINT4) NatSeqAckNDelta.i4OutDelta;
                u4CurrentAckNo -= (UINT4) NatSeqAckNDelta.i4InDelta;
            }
        }

        /* 
         * If the current packet(pBuf) has any payload change then send the 
         * information to update the SeqAckHistory-Delta table.
         */
        if (pHeaderInfo->i4Delta != NAT_ZERO)
        {
            /* InSeq      = CurrentSeqNo + Len of Tcp payload 
             * InSeqDelta = CurrentSeqNo + Len of Tcp payload + 
             *                Out Delta Change(incl prev changes)*/

            NatSeqAckNDelta.u4InSeq = OSIX_NTOHL (u4OldCurrentSeqNo) +
                (UINT4) u2LenOfPkt;
            NatSeqAckNDelta.u4InSeqDelta = NatSeqAckNDelta.u4InSeq +
                (UINT4) pHeaderInfo->i4Delta +
                (UINT4) NatSeqAckNDelta.i4OutDelta;

            /* OutSeq  =  OuSeqDelta  =  CurrentAckNo */
            NatSeqAckNDelta.u4OutSeq = NatSeqAckNDelta.u4OutSeqDelta =
                OSIX_NTOHL (u4OldCurrentAckNo);
            pNatPrevSeqAckHistory =
                &pHeaderInfo->pDynamicEntry->aNatSeqAckHistory[NAT_INDEX_0];

            if (pNatPrevSeqAckHistory->u1Status == NAT_STATUS_ACTIVE)
            {
                u4NextSeqNo =
                    OSIX_NTOHL (u4OldCurrentSeqNo) + (UINT4) u2LenOfPkt;
                u4AckNo = OSIX_NTOHL (u4OldCurrentAckNo);

                /* In case of retransmission, same preivious delta should be 
                   applied again, not the current one */
                if ((pNatPrevSeqAckHistory->u4InSeq == u4NextSeqNo) &&
                    (pNatPrevSeqAckHistory->u4OutSeq == u4AckNo) &&
                    (pNatPrevSeqAckHistory->u4Direction
                     == pHeaderInfo->u4Direction))
                {
                    u4CurrentSeqNo =
                        u4CurrentSeqNo - (UINT4) pHeaderInfo->i4Delta;
                }
                else
                {
                    /* Send this Information to Delta table for updating 
                     * the history. */
                    NatAddDeltaForSeqAck (pHeaderInfo, &NatSeqAckNDelta);
                }

            }
            else
            {
                /* Send this Information to Delta table for updating 
                 * the history. */
                NatAddDeltaForSeqAck (pHeaderInfo, &NatSeqAckNDelta);
            }

        }

        /* Update the pBuf with New Seq No, & Ack No. */
        u4CurrentSeqNo = OSIX_HTONL (u4CurrentSeqNo);
        u4CurrentAckNo = OSIX_HTONL (u4CurrentAckNo);
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4CurrentSeqNo,
                                   u4SeqNoOffset, NAT_SEQ_NUM_LEN);
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4CurrentAckNo,
                                   u4AckNoOffset, NAT_ACK_NUM_LEN);

        if (u4PktLen >=
            (UINT4) (NAT_TCP_CKSUM_OFFSET + pHeaderInfo->u1IpHeadLen +
                     NAT_WORD_LEN))
        {
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2TransportCksum,
                                       NAT_TCP_CKSUM_OFFSET +
                                       (UINT4) pHeaderInfo->u1IpHeadLen,
                                       NAT_WORD_LEN);
        }
        else
        {
            u2Flag = FALSE;
        }

        if (u2Flag == TRUE)
        {
            /* 3) Tcp Chksum adjustment.  */
            NatChecksumAdjust ((UINT1 *) &u2TransportCksum,
                               (UINT1 *) &u4OldCurrentSeqNo, NAT_SEQ_NUM_LEN,
                               (UINT1 *) &u4CurrentSeqNo, NAT_SEQ_NUM_LEN);
            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2TransportCksum,
                                       NAT_TCP_CKSUM_OFFSET +
                                       (UINT4) pHeaderInfo->u1IpHeadLen,
                                       NAT_WORD_LEN);
            NatChecksumAdjust ((UINT1 *) &u2TransportCksum,
                               (UINT1 *) &u4OldCurrentAckNo, NAT_ACK_NUM_LEN,
                               (UINT1 *) &u4CurrentAckNo, NAT_ACK_NUM_LEN);
            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2TransportCksum,
                                       NAT_TCP_CKSUM_OFFSET +
                                       (UINT4) pHeaderInfo->u1IpHeadLen,
                                       NAT_WORD_LEN);
        }

    }
    else
    {
        /* InBound Traffic
         * 1) Modify the Tcp port in TCP header. */
        NatPortModify (pBuf, pHeaderInfo, pHeaderInfo->u2InPort,
                       u4DestPortOffset,
                       NAT_TCP_CKSUM_OFFSET + (UINT4) pHeaderInfo->u1IpHeadLen);

        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u4CurrentAckNo,
                                   u4AckNoOffset, NAT_ACK_NUM_LEN);
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u4CurrentSeqNo,
                                   u4SeqNoOffset, NAT_SEQ_NUM_LEN);
        u4OldCurrentAckNo = u4CurrentAckNo;
        u4OldCurrentSeqNo = u4CurrentSeqNo;
        u4CurrentAckNo = OSIX_NTOHL (u4CurrentAckNo);
        u4CurrentSeqNo = OSIX_NTOHL (u4CurrentSeqNo);

        /* 
         * If Payload translation is done b4 then we need to apply Delta
         * accordingly for Seq No, Ack No for the subsequent packets of 
         * session. 
         */
        if (pHeaderInfo->pDynamicEntry->u1DeltaChangeFlag ==
            NAT_SEQ_ACK_HISTORY)
        {
            /* 
             * As the packet is an OutBound pkt we copy 
             * Seq no of pBuf --> OutSeq(Outside seq no) 
             * Ack no of pBuf --> InSeqDelta (Inside seq no with 
             *                                    prev delta change)
             */
            NatSeqAckNDelta.u4OutSeq = u4CurrentSeqNo;
            NatSeqAckNDelta.u4InSeqDelta = u4CurrentAckNo;

            /* Get the Delta change to be applied for the current packet. */
            u4Status = NatGetDeltaForSeqAck (pHeaderInfo, &NatSeqAckNDelta);

            /* 
             * If the Status of Get operation is not failure then we have 
             * InDelta & OutDelta which is to be applied for the packet. 
             */
            if (u4Status != NAT_FAILURE)
            {
                /* 
                 * Session has payload change update the Seq & Ack no fields 
                 * with OutDelta & InDelta respectively.
                 * 1) Current Seq no = Current Seq no + InDeltachange.
                 * 2) Current Ack no = Current Ack no - OutDeltaChange.
                 */
                u4CurrentSeqNo += (UINT4) NatSeqAckNDelta.i4InDelta;
                u4CurrentAckNo -= (UINT4) NatSeqAckNDelta.i4OutDelta;
            }
        }

        /* 
         * If the current packet(pBuf) has any payload change then send the 
         * information to update the SeqAckHistory-Delta table.
         */
        if (pHeaderInfo->i4Delta != NAT_ZERO)
        {
            /* InSeq  =  InSeqDelta  =  CurrentAckNo */
            NatSeqAckNDelta.u4InSeq = OSIX_NTOHL (u4OldCurrentAckNo);
            NatSeqAckNDelta.u4InSeqDelta = OSIX_NTOHL (u4OldCurrentAckNo);

            /* OutSeq      = CurrentSeqNo + Len of Tcp payload 
             * OutSeqDelta = CurrentSeqNo + Len of Tcp payload + 
             *                   Out Delta Change(incl prev changes)*/

            NatSeqAckNDelta.u4OutSeq = OSIX_NTOHL (u4OldCurrentSeqNo) +
                (UINT4) u2LenOfPkt;
            NatSeqAckNDelta.u4OutSeqDelta = NatSeqAckNDelta.u4OutSeq +
                (UINT4) pHeaderInfo->i4Delta +
                (UINT4) NatSeqAckNDelta.i4InDelta;

            /* Logic for handling re-transmission */
            pNatPrevSeqAckHistory =
                &pHeaderInfo->pDynamicEntry->aNatSeqAckHistory[NAT_INDEX_0];

            if (pNatPrevSeqAckHistory->u1Status == NAT_STATUS_ACTIVE)
            {
                u4NextSeqNo =
                    OSIX_NTOHL (u4OldCurrentSeqNo) + (UINT4) u2LenOfPkt;
                u4AckNo = OSIX_NTOHL (u4OldCurrentAckNo);

                /* In case of retransmission, same preivious delta should be 
                   applied again, not the current one */
                if ((pNatPrevSeqAckHistory->u4OutSeq == u4NextSeqNo) &&
                    (pNatPrevSeqAckHistory->u4InSeq == u4AckNo) &&
                    (pNatPrevSeqAckHistory->u4Direction ==
                     pHeaderInfo->u4Direction))
                {
                    u4CurrentSeqNo =
                        u4CurrentSeqNo - (UINT4) pHeaderInfo->i4Delta;
                }
                else
                {
                    /* Send this Information to Delta table for updating 
                       the history. */
                    NatAddDeltaForSeqAck (pHeaderInfo, &NatSeqAckNDelta);
                }
            }
            else
            {
                /* Send this Information to Delta table for updating the 
                   history. */
                NatAddDeltaForSeqAck (pHeaderInfo, &NatSeqAckNDelta);
            }
        }

        /* Update the pBuf with New Seq No, & Ack No. */
        u4CurrentAckNo = OSIX_HTONL (u4CurrentAckNo);
        u4CurrentSeqNo = OSIX_HTONL (u4CurrentSeqNo);
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4CurrentAckNo,
                                   u4AckNoOffset, NAT_ACK_NUM_LEN);
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4CurrentSeqNo,
                                   u4SeqNoOffset, NAT_SEQ_NUM_LEN);
        if (u4PktLen >=
            (UINT4) (NAT_TCP_CKSUM_OFFSET + pHeaderInfo->u1IpHeadLen +
                     NAT_WORD_LEN))
        {
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2TransportCksum,
                                       NAT_TCP_CKSUM_OFFSET +
                                       (UINT4) pHeaderInfo->u1IpHeadLen,
                                       NAT_WORD_LEN);
        }
        else
        {
            u2Flag = FALSE;
        }
        if (u2Flag == TRUE)
        {
            /* 3) Tcp Chksum adjustment.  */
            NatChecksumAdjust ((UINT1 *) &u2TransportCksum,
                               (UINT1 *) &u4OldCurrentAckNo, NAT_ACK_NUM_LEN,
                               (UINT1 *) &u4CurrentAckNo, NAT_ACK_NUM_LEN);
            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2TransportCksum,
                                       NAT_TCP_CKSUM_OFFSET +
                                       (UINT4) pHeaderInfo->u1IpHeadLen,
                                       NAT_WORD_LEN);
            NatChecksumAdjust ((UINT1 *) &u2TransportCksum,
                               (UINT1 *) &u4OldCurrentSeqNo, NAT_SEQ_NUM_LEN,
                               (UINT1 *) &u4CurrentSeqNo, NAT_SEQ_NUM_LEN);
            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2TransportCksum,
                                       NAT_TCP_CKSUM_OFFSET +
                                       (UINT4) pHeaderInfo->u1IpHeadLen,
                                       NAT_WORD_LEN);
        }

    }

    if (u1SynBit == NAT_TCP_SYN_BIT)
    {
        /* Get the MSS field in the packet and update it 
         * less than the WAN Interfaces MTU */
        if (u1Optionkind == NAT_TCP_OPTIONS_MSS)
        {
            u4MssOffset =
                (UINT4) pHeaderInfo->u1IpHeadLen + NAT_TCP_OPTIONS_OFFSET +
                NAT_TCP_MSS_OFFSET;

            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2Mss, u4MssOffset,
                                       NAT_TCP_MSS_LEN);

            SecUtilGetIfMtu (pHeaderInfo->u4IfNum, &u4Mtu);
            u2OldMss = u2Mss;
            u2Mss = OSIX_NTOHS (u2Mss);

            /* NAT_ADDL_HEADER_LENGTH included to reduce the chance of
             * fragmentation by the intermediate routers. This will be 
             * useful in case VPN has to be performed after NAT. */
            if (u2Mss > ((UINT2) (u4Mtu -
                                  (NAT_IP_HEADER_LENGTH +
                                   NAT_TCP_HEADER_LENGTH +
                                   NAT_ADDL_HEADER_LENGTH))))
            {
                /* Modify the pacets MSS value */
                u2Mss = OSIX_HTONS ((UINT2) (u4Mtu -
                                             (NAT_IP_HEADER_LENGTH +
                                              NAT_TCP_HEADER_LENGTH +
                                              NAT_ADDL_HEADER_LENGTH)));

                CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2Mss,
                                           u4MssOffset, NAT_TCP_MSS_LEN);

                NatChecksumAdjust ((UINT1 *) &u2TransportCksum,
                                   (UINT1 *) &u2OldMss, NAT_TCP_MSS_LEN,
                                   (UINT1 *) &u2Mss, NAT_TCP_MSS_LEN);

                CRU_BUF_Copy_OverBufChain (pBuf,
                                           (UINT1 *) &u2TransportCksum,
                                           NAT_TCP_CKSUM_OFFSET +
                                           (UINT4) pHeaderInfo->u1IpHeadLen,
                                           NAT_WORD_LEN);
            }
        }

    }

    /*
     * if the TCP pkt is received with TCP Reset Bit then delete the session
     * entry corresponding to the pkt.
     */
    u1RstBit = u1CodeByte & NAT_TCP_RST_BIT_SET;
    if (u1RstBit == NAT_TCP_RST_BIT_SET)
    {
        TMO_SLL_Scan (&gNatTcpDelStack, pTcpDelNode, tTcpDelNode *)
        {
            if (pTcpDelNode->pDynamicEntry == pHeaderInfo->pDynamicEntry)
            {
                u1DelFlag = TRUE;
                break;
            }
        }
        if (u1DelFlag != TRUE)
        {
            NatDeleteEntry (pHeaderInfo->pDynamicEntry);
        }
    }

}

/***************************************************************************
* Function Name    :  NatUdpHeaderModify
* Description    :  This function modifies the UDP header.
*             The fields modified are Source Port/Destination Port,
*
* Input (s)    :  1. pBuf - This stores the full IP packet along
*            with TCP/UDP header and payload.
*             2. pHeaderInfo - Contains the modified information.
*
* Output (s)    :  Modified pBuf
* Returns      :  None
*
****************************************************************************/

PUBLIC VOID
NatUdpHeaderModify (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo)
{
    UINT4               u4SrcPortOffset = NAT_ZERO;
    UINT4               u4DestPortOffset = NAT_ZERO;

    u4SrcPortOffset = pHeaderInfo->u1IpHeadLen;
    u4DestPortOffset = u4SrcPortOffset + NAT_PORT_LEN;

    /* Updates port and Cksum information based on direction */
    if (pHeaderInfo->u4Direction == NAT_OUTBOUND)
    {
        NatPortModify (pBuf, pHeaderInfo, pHeaderInfo->u2InPort,
                       u4SrcPortOffset,
                       NAT_UDP_CKSUM_OFFSET + (UINT4) pHeaderInfo->u1IpHeadLen);
    }
    else
    {
        NatPortModify (pBuf, pHeaderInfo, pHeaderInfo->u2InPort,
                       u4DestPortOffset,
                       NAT_UDP_CKSUM_OFFSET + (UINT4) pHeaderInfo->u1IpHeadLen);
    }

}

/***************************************************************************
* Function Name    :  NatPortModify
* Description    :  This function modifies the port information in TCP and
*                   UDP headers.
*
* Input (s)    :  1. pBuf - This stores the full IP packet along
*            with TCP/UDP header and payload.
*             2. u2NewPort - The port number to be substituted.
*             3. u4PortOffset - The offset of the port.
*             4. u4CksumOffset - offset.
* Output (s)    :  Modified pBuf
* Returns      :  None
*
****************************************************************************/

PUBLIC VOID
NatPortModify (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo,
               UINT2 u2NewPort, UINT4 u4PortOffset, UINT4 u4CksumOffset)
{
    UINT2               u2OldPort = NAT_ZERO;
    UINT2               u2TransportCksum = NAT_ZERO;
    UINT4               u4PktLen = NAT_ZERO;

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2OldPort, u4PortOffset,
                               NAT_PORT_LEN);
    u2NewPort = OSIX_HTONS (u2NewPort);
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2NewPort, u4PortOffset,
                               NAT_PORT_LEN);
    u4PktLen = CRU_BUF_Get_ChainValidByteCount (pBuf);
    if (u4PktLen >= u4CksumOffset)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2TransportCksum,
                                   u4CksumOffset, NAT_WORD_LEN);
        if ((pHeaderInfo->u1PktType == NAT_TCP) ||
            ((pHeaderInfo->u1PktType == NAT_UDP) && (u2TransportCksum
                                                     != NAT_PAYLOAD_NULL)))
        {
            NatChecksumAdjust ((UINT1 *) &u2TransportCksum,
                               (UINT1 *) &u2OldPort, NAT_PORT_LEN,
                               (UINT1 *) &u2NewPort, NAT_PORT_LEN);
            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2TransportCksum,
                                       u4CksumOffset, NAT_WORD_LEN);
        }
    }
}

/***************************************************************************
* Function Name    :  NatGetDeltaForSeqAck
* Description    :  This function gets the Delta to be applied for the 
*                    pacet based on the Direction Seq No & Ack No.
*
* Input (s)    : pHeaderInfo - Contains the modified information.
*                pNatSeqAckHistory - Seq n Ack no to get the Delta change
*                                      information.
*
* Output (s)    :  Modified pBuf
* Returns      :  None
*
****************************************************************************/

PUBLIC UINT4
NatGetDeltaForSeqAck (tHeaderInfo * pHeaderInfo,
                      tNatSeqAckHistory * pNatSeqAckHistory)
{
    tNatSeqAckHistory  *pNatLocalSeqAckHistory = NULL;
    INT4                i4InSeqDiff = NAT_ZERO;
    INT4                i4OutSeqDiff = NAT_ZERO;
    INT4                i4InDiffMin = NAT_NOT_FOUND;
    INT4                i4OutDiffMin = NAT_NOT_FOUND;
    UINT1               u1LoopIndex = NAT_ZERO;
    UINT1               u1Temp = NAT_ZERO;
    UINT1               u1InIndex = NAT_ZERO;
    UINT1               u1OutIndex = NAT_ZERO;
    UINT1               u1NewIndex = NAT_ZERO;

    NAT_TRC (NAT_TRC_ON, " \nEntering NatGetDeltaForSeqAck \n");

    /* 
     * Our table of Seq Ack History is maintaining a history of
     * NAT_SEQ_ACK_HISTORY(3) take the difference of the current 
     * traffic entry with the the all the entries in the table and
     * find the entry which is having minimum diff, the correspoding
     * InSeqDelta & OutSeqDelta should be applied to the packet.
     */

    /* 
     * To get the minimum difference entry we should start looping 
     * from the least recently changed index.
     */
    u1LoopIndex = pHeaderInfo->pDynamicEntry->u1DeltaIndex;

    for (u1Temp = NAT_ZERO; u1Temp < NAT_SEQ_ACK_HISTORY;
         u1Temp++, u1LoopIndex++)
    {
        if (u1LoopIndex == NAT_SEQ_ACK_HISTORY)
        {
            u1LoopIndex = NAT_ZERO;
        }

        pNatLocalSeqAckHistory =
            &pHeaderInfo->pDynamicEntry->aNatSeqAckHistory[u1LoopIndex];

        /* 
         * If the Status of current pointing entry is not Active continue 
         * searching in the next entries.
         */
        if (pNatLocalSeqAckHistory->u1Status != NAT_STATUS_ACTIVE)
        {
            continue;
        }

        if (pHeaderInfo->u4Direction == NAT_OUTBOUND)
        {
            /* 
             * For OutBound Traffic we check for 
             * InSide Seq No & OutSide Seq with Delta change.
             */
            i4InSeqDiff = (INT4) pNatSeqAckHistory->u4InSeq -
                (INT4) pNatLocalSeqAckHistory->u4InSeq;
            i4OutSeqDiff = (INT4) pNatSeqAckHistory->u4OutSeqDelta -
                (INT4) pNatLocalSeqAckHistory->u4OutSeqDelta;
        }
        else
        {
            /*
             * For InBound Traffic we check for
             * OutSide Seq No & InSide Seq with Delta change.
             */
            i4OutSeqDiff = (INT4) pNatSeqAckHistory->u4OutSeq -
                (INT4) pNatLocalSeqAckHistory->u4OutSeq;
            i4InSeqDiff = (INT4) pNatSeqAckHistory->u4InSeqDelta -
                (INT4) pNatLocalSeqAckHistory->u4InSeqDelta;
        }

        /* Calculate the Minimum InSeq Difference. */
        if (i4InSeqDiff >= NAT_ZERO)
        {
/*            if (i4InDiffMin >= NAT_ZERO)
            {
                if (i4InSeqDiff < i4InDiffMin)
                {
                    i4InDiffMin = i4InSeqDiff;
                    u1InIndex = u1LoopIndex;
                }
            }
            else
            {*/
            i4InDiffMin = i4InSeqDiff;
            u1InIndex = u1LoopIndex;
            /*    } */
        }

        /* Calculate the Minimum OutSeq Difference. */
        if (i4OutSeqDiff >= NAT_ZERO)
        {
            /* if (i4OutDiffMin >= NAT_ZERO)
               {
               if (i4OutSeqDiff < i4OutDiffMin)
               {
               i4OutDiffMin = i4OutSeqDiff;
               u1OutIndex = u1LoopIndex;
               }
               }
               else
               { */
            i4OutDiffMin = i4OutSeqDiff;
            u1OutIndex = u1LoopIndex;
            /* } */
        }
    }

    if (u1InIndex != u1OutIndex)
    {
        /* 
         * InSeqDiff has minumum in X index whereas OutSeqDiff
         * has minimum in Y index. => The packet might be an 
         * retransmission packet. 
         * We need to take the index which will be Recently 
         * added - 1. As history is 3 we can also point to
         * about-to-change index + 1 
         */

        u1NewIndex = ((u1InIndex < u1OutIndex) ? u1InIndex : u1OutIndex);

        if ((u1InIndex == (NAT_SEQ_ACK_HISTORY - NAT_ONE)) ||
            (u1OutIndex == (NAT_SEQ_ACK_HISTORY - NAT_ONE)))
        {
            if (u1NewIndex == NAT_ZERO)
            {
                u1NewIndex = NAT_SEQ_ACK_HISTORY - NAT_ONE;
            }
        }
        if (u1NewIndex >= NAT_SEQ_ACK_HISTORY)
        {
            return NAT_FAILURE;
        }

        pNatLocalSeqAckHistory =
            &pHeaderInfo->pDynamicEntry->aNatSeqAckHistory[u1NewIndex];

        NAT_TRC3 (NAT_TRC_ON, " May be a retransmission packet because "
                  "Min InSeqDiff Index %u, Min OutSeqDiff Index %u, "
                  "Applied Index %u\n", u1InIndex, u1OutIndex, u1NewIndex);
    }
    else
    {
        if (u1InIndex >= NAT_SEQ_ACK_HISTORY)
        {
            return NAT_FAILURE;
        }
        pNatLocalSeqAckHistory =
            &pHeaderInfo->pDynamicEntry->aNatSeqAckHistory[u1InIndex];
    }

    if (pNatLocalSeqAckHistory->u1Status != NAT_STATUS_ACTIVE)
    {
        NAT_TRC (NAT_TRC_ON, "Status of current index in inactive "
                 "returning NAT_FAILURE\n");
        return NAT_FAILURE;
    }

    pNatSeqAckHistory->i4InDelta = pNatLocalSeqAckHistory->i4InDelta;
    pNatSeqAckHistory->i4OutDelta = pNatLocalSeqAckHistory->i4OutDelta;
    UNUSED_PARAM (i4OutDiffMin);
    UNUSED_PARAM (i4InDiffMin);

    NAT_TRC3 (NAT_TRC_ON,
              "Retreived Index is %u, Apply InDelta %u OutDelta %u\n",
              u1InIndex, pNatSeqAckHistory->i4InDelta,
              pNatSeqAckHistory->i4OutDelta);

    NAT_TRC (NAT_TRC_ON, "Exiting NatGetDeltaForSeqAck\n");

    return NAT_ENTRY_FOUND;
}

/***************************************************************************
* Function Name  :  NatAddDeltaForSeqAck 
* Description    :  This function maintains a history of InDelta and OutDelta
*                    If a payload translation is done add the seq & ack no 
*                    info with appropriate delta change. So that we can 
*                    retreive the delta based on Seq n Ack No.
*
* Input (s)      :  pHeaderInfo - Contains the modified information.
*                   pNatSeqAckHistory - Information regarding the 
*                     InSeq - Inside Seq no. OutSeq - Outside Seq no
*                     InSeqDelta - Inside Seq no after delta change.
*                     OutSeqDelta - Outside seq no after delta change.
*
* Output (s)     :  
* Returns        :  None
*
****************************************************************************/

PUBLIC UINT4
NatAddDeltaForSeqAck (tHeaderInfo * pHeaderInfo,
                      tNatSeqAckHistory * pNatSeqAckHistory)
{
    tNatSeqAckHistory  *pNatPrevSeqAckHistory = NULL;
    tNatSeqAckHistory  *pNatCurrentSeqAckHistory = NULL;
    UINT1               u1Index = NAT_ZERO;

    NAT_TRC (NAT_TRC_ON, "\nEntering NatAddDeltaForSeqAck\n");

    /* Index which should to be replaced. */
    u1Index = pHeaderInfo->pDynamicEntry->u1DeltaIndex;

    pNatCurrentSeqAckHistory =
        &pHeaderInfo->pDynamicEntry->aNatSeqAckHistory[u1Index];

    if (u1Index == NAT_ZERO)
    {
        u1Index = NAT_SEQ_ACK_HISTORY - NAT_ONE;
    }
    else
        u1Index--;

    pNatPrevSeqAckHistory =
        &pHeaderInfo->pDynamicEntry->aNatSeqAckHistory[u1Index];

    if (pNatPrevSeqAckHistory->u1Status == NAT_STATUS_ACTIVE)
    {
        NAT_TRC2 (NAT_TRC_ON, "Prev u4InSeq %u u4InSeqDelta %u \n",
                  pNatPrevSeqAckHistory->u4InSeq,
                  pNatPrevSeqAckHistory->u4InSeqDelta);
        NAT_TRC2 (NAT_TRC_ON, "Prev u4OutSeq %u u4OutSeqDelta %u \n",
                  pNatPrevSeqAckHistory->u4OutSeq,
                  pNatPrevSeqAckHistory->u4OutSeqDelta);

        /* Entry already added in the table return NAT_SUCCESS */
        if ((pNatPrevSeqAckHistory->u4InSeq == pNatSeqAckHistory->u4InSeq) &&
            (pNatPrevSeqAckHistory->u4InSeqDelta ==
             pNatSeqAckHistory->u4InSeqDelta) &&
            (pNatPrevSeqAckHistory->u4OutSeq == pNatSeqAckHistory->u4OutSeq) &&
            (pNatPrevSeqAckHistory->u4OutSeqDelta ==
             pNatSeqAckHistory->u4OutSeqDelta))
        {
            NAT_TRC (NAT_TRC_ON, "NatAddDeltaForSeqAck Entry already added \n");
            NAT_TRC (NAT_TRC_ON, "Exiting NatAddDeltaForSeqAck \n");
            return NAT_SUCCESS;
        }

        if (pHeaderInfo->u4Direction == NAT_OUTBOUND)
        {
            /* 
             * For OutBound Traffic =>
             * New OutDelta is sum of Current Delta & Previous OutDelta.
             * New InDelta is same as Previous InDelta. 
             */
            pNatCurrentSeqAckHistory->i4InDelta =
                pNatPrevSeqAckHistory->i4InDelta;
            pNatCurrentSeqAckHistory->i4OutDelta =
                pNatPrevSeqAckHistory->i4OutDelta + pHeaderInfo->i4Delta;
        }
        else
        {
            /* 
             * For InBound Traffic =>
             * New InDelta is sum of Current Delta & Previous InDelta.
             * New OutDelta is same as Previous OutDelta.
             */
            pNatCurrentSeqAckHistory->i4InDelta =
                pNatPrevSeqAckHistory->i4InDelta + pHeaderInfo->i4Delta;
            pNatCurrentSeqAckHistory->i4OutDelta =
                pNatPrevSeqAckHistory->i4OutDelta;
        }
    }
    else
    {
        /* 
         * As Previous Entry is Inactive =>
         * For OutBound only OutDelta is changed untill now. 
         * For InBound only InDelta is changed untill now.
         */
        if (pHeaderInfo->u4Direction == NAT_OUTBOUND)
        {
            pNatCurrentSeqAckHistory->i4InDelta = NAT_ZERO;
            pNatCurrentSeqAckHistory->i4OutDelta = pHeaderInfo->i4Delta;
        }
        else
        {
            pNatCurrentSeqAckHistory->i4InDelta = pHeaderInfo->i4Delta;
            pNatCurrentSeqAckHistory->i4OutDelta = NAT_ZERO;
        }
    }

    /* Update Current Entries InSeq, InSeqDelta, OutSeq, OutSeqDelta */
    pNatCurrentSeqAckHistory->u4InSeq = pNatSeqAckHistory->u4InSeq;
    pNatCurrentSeqAckHistory->u4InSeqDelta = pNatSeqAckHistory->u4InSeqDelta;

    pNatCurrentSeqAckHistory->u4OutSeq = pNatSeqAckHistory->u4OutSeq;
    pNatCurrentSeqAckHistory->u4OutSeqDelta = pNatSeqAckHistory->u4OutSeqDelta;

    pNatCurrentSeqAckHistory->u4Direction = pHeaderInfo->u4Direction;
    /* Update the Status of this Index as Active. */
    pNatCurrentSeqAckHistory->u1Status = NAT_STATUS_ACTIVE;

    NAT_TRC3 (NAT_TRC_ON, "Retreived information index %u InDelta change %d "
              "OutDelta change %d\n", pHeaderInfo->pDynamicEntry->u1DeltaIndex,
              pNatCurrentSeqAckHistory->i4InDelta,
              pNatCurrentSeqAckHistory->i4OutDelta);

    /* 
     * Updating the Delta Change Index to the Next Index. The New Entry which
     * needs to be added in the table will be added at that index. 
     */
    pHeaderInfo->pDynamicEntry->u1DeltaIndex++;

    if (pHeaderInfo->pDynamicEntry->u1DeltaIndex == NAT_SEQ_ACK_HISTORY)
    {
        pHeaderInfo->pDynamicEntry->u1DeltaIndex = NAT_ZERO;
    }

    /* 
     * Update the DeltaChangeFlag in pDynamic Entry.
     * If it is not Active => No deltachange for this session. 
     */
    pHeaderInfo->pDynamicEntry->u1DeltaChangeFlag = NAT_SEQ_ACK_HISTORY;

    NAT_TRC (NAT_TRC_ON, "Exiting NatAddDeltaForSeqAck \n\n");
    return NAT_SUCCESS;
}
#endif /* _NATHEAD_C */
