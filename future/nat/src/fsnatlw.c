/********************************************************************
* Copyright (C) Future Software Limited,2003
*
* $Id: fsnatlw.c,v 1.32 2014/06/24 11:36:29 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#ifndef _FSNATLW_C
#define _FSNATLW_C
#include  "natinc.h"
#include "fsnatcli.h"
#include "fsnatlw.h"
extern tSipSignalPort gSipSignalPort;
extern INT4         gi4NatSipAlgPartialEntryTimer;

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetNatEnable
 Input       :  The Indices

                The Object 
                retValNatEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatEnable (INT4 *pi4RetValNatEnable)
{
    *pi4RetValNatEnable = (INT4) gu4NatEnable;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNatTypicalNumberOfEntries
 Input       :  The Indices

                The Object 
                retValNatTypicalNumberOfEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatTypicalNumberOfEntries (INT4 *pi4RetValNatTypicalNumberOfEntries)
{
    *pi4RetValNatTypicalNumberOfEntries = (INT4) gu4NatTypicalNumOfEntries;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNatTranslatedLocalPortStart
 Input       :  The Indices

                The Object 
                retValNatTranslatedLocalPortStart
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatTranslatedLocalPortStart (INT4 *pi4RetValNatTranslatedLocalPortStart)
{
    *pi4RetValNatTranslatedLocalPortStart = (INT4) gu4NatInitTranslatedLocPort;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNatIdleTimeOut
 Input       :  The Indices

                The Object 
                retValNatIdleTimeOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatIdleTimeOut (INT4 *pi4RetValNatIdleTimeOut)
{

    /* type casting UINT4 value to INT4 value will not cause a
     * problem as the UINT4 value will not exceed the INT4 value
     */

    *pi4RetValNatIdleTimeOut =
        (INT4) (gu4NatIdleTimeOut / NAT_NUM_OF_SYS_TIME_UNITS_IN_A_SEC);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNatTcpTimeOut
 Input       :  The Indices

                The Object 
                retValNatTcpTimeOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatTcpTimeOut (INT4 *pi4RetValNatTcpTimeOut)
{
    *pi4RetValNatTcpTimeOut =
        (INT4) (gu4NatTcpTimeOut / NAT_NUM_OF_SYS_TIME_UNITS_IN_A_SEC);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNatUdpTimeOut
 Input       :  The Indices

                The Object 
                retValNatUdpTimeOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatUdpTimeOut (INT4 *pi4RetValNatUdpTimeOut)
{
    *pi4RetValNatUdpTimeOut =
        (INT4) (gu4NatUdpTimeOut / NAT_NUM_OF_SYS_TIME_UNITS_IN_A_SEC);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNatTrcFlag
 Input       :  The Indices

                The Object 
                retValNatTrcFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatTrcFlag (INT4 *pi4RetValNatTrcFlag)
{
    *pi4RetValNatTrcFlag = (INT4) gu4NatTrc;
    return (SNMP_SUCCESS);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetNatStatDynamicAllocFailureCount
 Input       :  The Indices

                The Object 
                retValNatStatDynamicAllocFailureCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetNatStatDynamicAllocFailureCount
    (UINT4 *pu4RetValNatStatDynamicAllocFailureCount)
{
    /* type casting UINT4 value to INT4 value will not cause a
     * problem as the UINT4 value will not exceed the INT4 value
     */

    *pu4RetValNatStatDynamicAllocFailureCount =
        (UINT4) gu4NatStatDynamicAllocFailureCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNatStatTotalNumberOfTranslations
 Input       :  The Indices

                The Object 
                retValNatStatTotalNumberOfTranslations
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetNatStatTotalNumberOfTranslations
    (UINT4 *pu4RetValNatStatTotalNumberOfTranslations)
{
    UINT4               u4Total = NAT_ZERO;
    INT4                i4IfNum = NAT_ZERO;

    u4Total = NAT_ZERO;

    /* Total number of translations is the sum of translations on all
     * Interface
     */
    for (i4IfNum = NAT_ONE; i4IfNum <= NAT_MAX_NUM_IF; i4IfNum++)
    {
        if (gapNatIfTable[i4IfNum] != NULL)
        {
            if (gapNatIfTable[i4IfNum]->i4RowStatus != NAT_STATUS_INVLD)
            {
                u4Total += gapNatIfTable[i4IfNum]->u4NumOfTranslation;
            }
        }

    }

    *pu4RetValNatStatTotalNumberOfTranslations = u4Total;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNatStatTotalNumberOfActiveSessions
 Input       :  The Indices

                The Object 
                retValNatStatTotalNumberOfActiveSessions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetNatStatTotalNumberOfActiveSessions
    (UINT4 *pu4RetValNatStatTotalNumberOfActiveSessions)
{

    UINT4               u4Total = NAT_ZERO;
    INT4                i4IfNum = NAT_ZERO;

    u4Total = NAT_ZERO;

    /* Total number of Active sessions is the sum of Active sessions on all
     * Interface
     */
    for (i4IfNum = NAT_ONE; i4IfNum <= NAT_MAX_NUM_IF; i4IfNum++)
    {
        if (gapNatIfTable[i4IfNum] != NULL)
        {
            if (gapNatIfTable[i4IfNum]->i4RowStatus != NAT_STATUS_INVLD)
            {
                u4Total += gapNatIfTable[i4IfNum]->u4NumOfActiveSessions;
            }
        }

    }

    *pu4RetValNatStatTotalNumberOfActiveSessions = u4Total;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNatStatTotalNumberOfPktsDropped
 Input       :  The Indices

                The Object 
                retValNatStatTotalNumberOfPktsDropped
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetNatStatTotalNumberOfPktsDropped
    (UINT4 *pu4RetValNatStatTotalNumberOfPktsDropped)
{

    UINT4               u4Total = NAT_ZERO;
    INT4                i4IfNum = NAT_ZERO;

    u4Total = NAT_ZERO;
    /* Total number of packets dropped is the sum of packets dropped on all
     * Interfaces
     */
    for (i4IfNum = NAT_ONE; i4IfNum <= NAT_MAX_NUM_IF; i4IfNum++)
    {
        if (gapNatIfTable[i4IfNum] != NULL)
        {
            if (gapNatIfTable[i4IfNum]->i4RowStatus != NAT_STATUS_INVLD)
            {
                u4Total += gapNatIfTable[i4IfNum]->u4NumOfPktsDropped;
            }
        }

    }

    *pu4RetValNatStatTotalNumberOfPktsDropped = u4Total;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNatStatTotalNumberOfSessionsClosed
 Input       :  The Indices

                The Object 
                retValNatStatTotalNumberOfSessionsClosed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetNatStatTotalNumberOfSessionsClosed
    (UINT4 *pu4RetValNatStatTotalNumberOfSessionsClosed)
{

    UINT4               u4Total = NAT_ZERO;
    INT4                i4IfNum = NAT_ZERO;

    u4Total = NAT_ZERO;
    /* Total number of sessions closed is the sum of sessions closed on all
     * Interfaces
     */
    for (i4IfNum = NAT_ONE; i4IfNum <= NAT_MAX_NUM_IF; i4IfNum++)
    {
        if (gapNatIfTable[i4IfNum] != NULL)
        {
            if (gapNatIfTable[i4IfNum]->i4RowStatus != NAT_STATUS_INVLD)
            {
                u4Total += gapNatIfTable[i4IfNum]->u4NumOfSessionsClosed;
            }
        }

    }

    *pu4RetValNatStatTotalNumberOfSessionsClosed = u4Total;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNatIKEPortTranslation
 Input       :  The Indices

                The Object 
                retValNatIKEPortTranslation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatIKEPortTranslation (INT4 *pi4RetValNatIKEPortTranslation)
{
    *pi4RetValNatIKEPortTranslation = gi4NatIKEPortTranslation;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNatIKETimeout
 Input       :  The Indices

                The Object 
                retValNatIKETimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatIKETimeout (INT4 *pi4RetValNatIKETimeout)
{
    *pi4RetValNatIKETimeout =
        (INT4) (gi4NatIKETimeOut / (INT4) (NAT_NUM_OF_SYS_TIME_UNITS_IN_A_SEC));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNatIPSecTimeout
 Input       :  The Indices

                The Object 
                retValNatIPSecTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatIPSecTimeout (INT4 *pi4RetValNatIPSecTimeout)
{
    *pi4RetValNatIPSecTimeout =
        (INT4) (gi4NatIPSecTimeOut /
                (INT4) (NAT_NUM_OF_SYS_TIME_UNITS_IN_A_SEC));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNatIPSecPendingTimeout
 Input       :  The Indices

                The Object 
                retValNatIPSecPendingTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatIPSecPendingTimeout (INT4 *pi4RetValNatIPSecPendingTimeout)
{
    *pi4RetValNatIPSecPendingTimeout =
        (INT4) (gi4NatIPSecPendingTimeOut /
                (INT4) (NAT_NUM_OF_SYS_TIME_UNITS_IN_A_SEC));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNatIPSecMaxRetry
 Input       :  The Indices

                The Object 
                retValNatIPSecMaxRetry
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatIPSecMaxRetry (INT4 *pi4RetValNatIPSecMaxRetry)
{
    *pi4RetValNatIPSecMaxRetry = gi4NatIPSecMaxRetry;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSipAlgPort
 Input       :  The Indices

                The Object 
                retValSipAlgPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSipAlgPort (INT4 *pi4RetValSipAlgPort)
{
    *pi4RetValSipAlgPort = gSipSignalPort.u2UdpListenPort;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNatSipAlgPartialEntryTimeOut
 Input       :  The Indices

                The Object 
                retValNatSipAlgPartialEntryTimeOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatSipAlgPartialEntryTimeOut (INT4 *pi4RetValNatSipAlgPartialEntryTimeOut)
{
    *pi4RetValNatSipAlgPartialEntryTimeOut = gi4NatSipAlgPartialEntryTimer;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetNatEnable
 Input       :  The Indices

                The Object 
                setValNatEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatEnable (INT4 i4SetValNatEnable)
{

    UINT4               u4IfNum = NAT_ZERO;
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    NAT_DBG1 (NAT_DBG_LLR, "NatEnable = %d\n", i4SetValNatEnable);
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (gu4NatEnable != (UINT4) i4SetValNatEnable)
    {
        if (i4SetValNatEnable == NAT_ENABLE)
        {
            /*Initialise the Interfaces */
            for (u4IfNum = NAT_ONE; u4IfNum <= NAT_MAX_NUM_IF; u4IfNum++)
            {
                if (gapNatIfTable[u4IfNum] != NULL)
                {
                    NatIfInitFields (u4IfNum);
                    /* remove arp entries learnt from hardware */
#ifdef NPAPI_WANTED
#ifndef LNXIP4_WANTED
                    if ((gapNatIfTable[u4IfNum]->i4RowStatus ==
                         NAT_STATUS_ACTIVE) &&
                        (gapNatIfTable[u4IfNum]->u1NatEnable == NAT_ENABLE))
                    {
                        FsNatEnableOnIntf (u4IfNum);
                    }
#endif
#endif
                }
            }
            MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT", "\n Nat is Enabled\n");
            gu4NatEnable = (UINT4) i4SetValNatEnable;
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatEnable, u4SeqNum, FALSE,
                                  NatLock, NatUnLock, NAT_ZERO, SNMP_SUCCESS);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValNatEnable));

            return SNMP_SUCCESS;
        }
        else
        {
            gu4NatEnable = (UINT4) i4SetValNatEnable;
            /*
             * The Timer is stopped ..It will be started again when global NAT
             * is enabled and the first session entry is added in the
             * DynamicTransaltion Table or NFS Session Table( For NFS we
             * maintain a seperate Session Table.).
             */
            NatStopTimer ();
            MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT", "\n Nat is Disabled\n");
            for (u4IfNum = NAT_ONE; u4IfNum <= NAT_MAX_NUM_IF; u4IfNum++)
            {
                if (gapNatIfTable[u4IfNum] != NULL)
                {
                    /*
                     *deleting the Session entries concerned with that interface
                     */
                    NatDeleteDynamicEntry (u4IfNum);
                    /* add arp entries learnt to hardware */
#ifdef NPAPI_WANTED
                    if ((gapNatIfTable[u4IfNum]->i4RowStatus ==
                         NAT_STATUS_ACTIVE) &&
                        (gapNatIfTable[u4IfNum]->u1NatEnable == NAT_ENABLE))
                    {
#ifndef LNXIP4_WANTED
                        FsNatDisableOnIntf (u4IfNum);
#endif
                    }
#endif
                    /*
                     * deleting the Free-GlbIpList when GlobalNat is disabled
                     */
                    NatReleaseFromSLL
                        (&(gapNatIfTable[u4IfNum]->NatFreeGipList),
                         NAT_FREE_GLOBAL_LIST_POOL_ID);
                }
            }
            /*
             * We delete all the Dynamic data-structures from Linked Lists and
             * release them to their mem-pools before .The mem-pools are then
             * deleted.
             */
            NatReleaseDynamicStructs ();
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatEnable, u4SeqNum, FALSE,
                                  NatLock, NatUnLock, NAT_ZERO, SNMP_SUCCESS);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValNatEnable));
            return SNMP_SUCCESS;
        }
    }
    else
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatEnable, u4SeqNum, FALSE,
                              NatLock, NatUnLock, NAT_ZERO, SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValNatEnable));
        return SNMP_SUCCESS;
    }

}

/****************************************************************************
 Function    :  nmhSetNatTypicalNumberOfEntries
 Input       :  The Indices

                The Object 
                setValNatTypicalNumberOfEntries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatTypicalNumberOfEntries (INT4 i4SetValNatTypicalNumberOfEntries)
{
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    NAT_DBG1 (NAT_DBG_LLR, "NatTypicalNumOfEntries = %d\n",
              i4SetValNatTypicalNumberOfEntries);

    MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT",
                  "\n NatTypicalNumOfEntries is set to %d",
                  i4SetValNatTypicalNumberOfEntries);
    gu4NatTypicalNumOfEntries = (UINT4) i4SetValNatTypicalNumberOfEntries;
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatTypicalNumberOfEntries, u4SeqNum,
                          FALSE, NatLock, NatUnLock, NAT_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValNatTypicalNumberOfEntries));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetNatTranslatedLocalPortStart
 Input       :  The Indices

                The Object 
                setValNatTranslatedLocalPortStart
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatTranslatedLocalPortStart (INT4 i4SetValNatTranslatedLocalPortStart)
{
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (NAT_ENABLE == gu4NatEnable)
    {
        MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT",
                 "Global Nat switch is enabled, Can not Set Port Value\n");
        return SNMP_FAILURE;
    }

    gu4NatInitTranslatedLocPort = (UINT4) i4SetValNatTranslatedLocalPortStart;
    NAT_DBG1 (NAT_DBG_LLR, "TestNatTranslatedLocalPort = %d\n",
              i4SetValNatTranslatedLocalPortStart);
    MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT",
                  "\n NatNextFreeTranslatedLocPort is set to %d",
                  i4SetValNatTranslatedLocalPortStart);
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatTranslatedLocalPortStart, u4SeqNum,
                          FALSE, NatLock, NatUnLock, NAT_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                      i4SetValNatTranslatedLocalPortStart));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetNatIdleTimeOut
 Input       :  The Indices

                The Object 
                setValNatIdleTimeOut
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatIdleTimeOut (INT4 i4SetValNatIdleTimeOut)
{

    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    NAT_DBG1 (NAT_DBG_LLR, "NatIdleTimeOut = %d\n", i4SetValNatIdleTimeOut);

    gu4NatIdleTimeOut =
        (UINT4) i4SetValNatIdleTimeOut *NAT_NUM_OF_SYS_TIME_UNITS_IN_A_SEC;

    MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT", "\n NatIdleTimeOut is set to %d",
                  i4SetValNatIdleTimeOut);
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatIdleTimeOut, u4SeqNum, FALSE,
                          NatLock, NatUnLock, NAT_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValNatIdleTimeOut));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetNatTcpTimeOut
 Input       :  The Indices

                The Object 
                setValNatTcpTimeOut
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatTcpTimeOut (INT4 i4SetValNatTcpTimeOut)
{
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    NAT_DBG1 (NAT_DBG_LLR, "NatTcpTimeOut = %d\n", i4SetValNatTcpTimeOut);
    gu4NatTcpTimeOut =
        (UINT4) i4SetValNatTcpTimeOut *NAT_NUM_OF_SYS_TIME_UNITS_IN_A_SEC;
    MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT", "\n NatTcpTimeOut is set to %d",
                  i4SetValNatTcpTimeOut);
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatTcpTimeOut, u4SeqNum, FALSE,
                          NatLock, NatUnLock, NAT_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValNatTcpTimeOut));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetNatUdpTimeOut
 Input       :  The Indices

                The Object 
                setValNatUdpTimeOut
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatUdpTimeOut (INT4 i4SetValNatUdpTimeOut)
{
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    NAT_DBG1 (NAT_DBG_LLR, "NatUdpTimeOut = %d\n", i4SetValNatUdpTimeOut);
    gu4NatUdpTimeOut =
        (UINT4) i4SetValNatUdpTimeOut *NAT_NUM_OF_SYS_TIME_UNITS_IN_A_SEC;
    MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT", "\n NatUdpTimeOut is set to %d",
                  i4SetValNatUdpTimeOut);
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatUdpTimeOut, u4SeqNum, FALSE,
                          NatLock, NatUnLock, NAT_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValNatUdpTimeOut));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetNatTrcFlag
 Input       :  The Indices

                The Object 
                setValNatTrcFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatTrcFlag (INT4 i4SetValNatTrcFlag)
{
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    switch (i4SetValNatTrcFlag)
    {
        case NAT_INDEX_0:
            gu4NatTrc = NAT_ZERO;
            break;
        case NAT_INIT_SHUT_TRC:
            gu4NatTrc |= INIT_SHUT_TRC;
            break;
        case NAT_MGMT_TRC:
            gu4NatTrc |= MGMT_TRC;
            break;
        case NAT_DATA_PATH_TRC:
            gu4NatTrc |= DATA_PATH_TRC;
            break;
        case NAT_CONTROL_PLANE_TRC:
            gu4NatTrc |= CONTROL_PLANE_TRC;
            break;
        case NAT_DUMP_TRC:
            gu4NatTrc |= DUMP_TRC;
            break;
        case NAT_OS_RESOURCE_TRC:
            gu4NatTrc |= OS_RESOURCE_TRC;
            break;
        case NAT_ALL_FAILURE_TRC:
            gu4NatTrc |= ALL_FAILURE_TRC;
            break;
        case NAT_BUFFER_TRC:
            gu4NatTrc |= BUFFER_TRC;
            break;
        default:
            gu4NatTrc = NAT_ALL_TRC;
            break;
    }
    MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT", "\n NatTrcFlag is set to %d",
                  i4SetValNatTrcFlag);
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatTrcFlag, u4SeqNum, FALSE, NatLock,
                          NatUnLock, NAT_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValNatTrcFlag));

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetNatIKEPortTranslation
 Input       :  The Indices

                The Object 
                setValNatIKEPortTranslation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatIKEPortTranslation (INT4 i4SetValNatIKEPortTranslation)
{
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    NAT_DBG1 (NAT_DBG_LLR, "NatIkePortTranslation = %d\n",
              i4SetValNatIKEPortTranslation);
    gi4NatIKEPortTranslation = i4SetValNatIKEPortTranslation;
    MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT",
                  "\n NatIKEPortTranslation is set to %d",
                  gi4NatIKEPortTranslation);
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatIKEPortTranslation, u4SeqNum,
                          FALSE, NatLock, NatUnLock, NAT_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValNatIKEPortTranslation));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetNatIKETimeout
 Input       :  The Indices

                The Object 
                setValNatIKETimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatIKETimeout (INT4 i4SetValNatIKETimeout)
{
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    NAT_DBG1 (NAT_DBG_LLR, "NatIkeTimeOut = %d\n", i4SetValNatIKETimeout);
    gi4NatIKETimeOut =
        (INT4) (i4SetValNatIKETimeout *
                (INT4) (NAT_NUM_OF_SYS_TIME_UNITS_IN_A_SEC));

    MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT", "\n NatIKeTimeOut is set to %d",
                  gi4NatIKETimeOut);
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatIKETimeout, u4SeqNum, FALSE,
                          NatLock, NatUnLock, NAT_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValNatIKETimeout));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetNatIPSecTimeout
 Input       :  The Indices

                The Object 
                setValNatIPSecTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatIPSecTimeout (INT4 i4SetValNatIPSecTimeout)
{
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecTimeOut = %d\n", i4SetValNatIPSecTimeout);
    gi4NatIPSecTimeOut =
        (INT4) (i4SetValNatIPSecTimeout *
                (INT4) (NAT_NUM_OF_SYS_TIME_UNITS_IN_A_SEC));

    MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT", "\n NatIPSecTimeOut is set to %d",
                  gi4NatIPSecTimeOut);
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatIPSecTimeout, u4SeqNum, FALSE,
                          NatLock, NatUnLock, NAT_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValNatIPSecTimeout));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetNatIPSecPendingTimeout
 Input       :  The Indices

                The Object 
                setValNatIPSecPendingTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatIPSecPendingTimeout (INT4 i4SetValNatIPSecPendingTimeout)
{
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingTimeOut = %d\n",
              i4SetValNatIPSecPendingTimeout);
    gi4NatIPSecPendingTimeOut =
        (INT4) (i4SetValNatIPSecPendingTimeout *
                (INT4) (NAT_NUM_OF_SYS_TIME_UNITS_IN_A_SEC));

    MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT", "\n NatIPSecTimeOut is set to %d",
                  gi4NatIPSecPendingTimeOut);
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatIPSecPendingTimeout, u4SeqNum,
                          FALSE, NatLock, NatUnLock, NAT_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValNatIPSecPendingTimeout));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetNatIPSecMaxRetry
 Input       :  The Indices

                The Object 
                setValNatIPSecMaxRetry
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatIPSecMaxRetry (INT4 i4SetValNatIPSecMaxRetry)
{
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecMaxRetry = %d\n", i4SetValNatIPSecMaxRetry);
    gi4NatIPSecMaxRetry = i4SetValNatIPSecMaxRetry;

    MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT",
                  "\n NatIPSecMaxRetry is set to %d", gi4NatIPSecMaxRetry);
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatIPSecMaxRetry, u4SeqNum, FALSE,
                          NatLock, NatUnLock, NAT_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValNatIPSecMaxRetry));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetSipAlgPort
 Input       :  The Indices

                The Object 
                setValSipAlgPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSipAlgPort (INT4 i4SetValSipAlgPort)
{
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    NAT_DBG1 (NAT_DBG_LLR, "SipAlgPort = %d\n", i4SetValSipAlgPort);
    gSipSignalPort.u2UdpListenPort = (UINT2) i4SetValSipAlgPort;
    MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT", "\n SipAlgPort is set to %d",
                  i4SetValSipAlgPort);
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, SipAlgPort, u4SeqNum, FALSE, NatLock,
                          NatUnLock, NAT_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValSipAlgPort));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetNatSipAlgPartialEntryTimeOut
 Input       :  The Indices

                The Object 
                setValNatSipAlgPartialEntryTimeOut
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatSipAlgPartialEntryTimeOut (INT4 i4SetValNatSipAlgPartialEntryTimeOut)
{
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    NAT_DBG1 (NAT_DBG_LLR, "NatSipAlgPartialEntryTimeOut = %d\n",
              i4SetValNatSipAlgPartialEntryTimeOut);
    gi4NatSipAlgPartialEntryTimer = i4SetValNatSipAlgPartialEntryTimeOut;
    MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT",
                  "\n NatSipAlgPartialEntryTimeOut is set to %d",
                  i4SetValNatSipAlgPartialEntryTimeOut);
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatSipAlgPartialEntryTimeOut,
                          u4SeqNum, FALSE, NatLock, NatUnLock, NAT_ZERO,
                          SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                      i4SetValNatSipAlgPartialEntryTimeOut));

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2NatEnable
 Input       :  The Indices

                The Object 
                testValNatEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatEnable (UINT4 *pu4ErrorCode, INT4 i4TestValNatEnable)
{
    NAT_DBG1 (NAT_DBG_LLR, "NatEnable = %d\n", i4TestValNatEnable);

    if ((i4TestValNatEnable == NAT_ENABLE) ||
        (i4TestValNatEnable == NAT_DISABLE))
    {
        NAT_TRC (NAT_TRC_ON, "Exiting with SUCCESS: Entry found \n");
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT", "\nIllegal value for NatEnable\n");
    NAT_TRC (NAT_TRC_ON, "No Such Entry exists Test Failed\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2NatTypicalNumberOfEntries
 Input       :  The Indices

                The Object 
                testValNatTypicalNumberOfEntries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatTypicalNumberOfEntries (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValNatTypicalNumberOfEntries)
{

    NAT_DBG1 (NAT_DBG_LLR, "NatTypicalNumOfEntries = %d\n",
              i4TestValNatTypicalNumberOfEntries);
    if (i4TestValNatTypicalNumberOfEntries <= NAT_ZERO)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT",
                 "\nIllegal value for NatTypicalNumOfEntries\n");
        NAT_TRC (NAT_TRC_ON, "No Such Entry exists Test Failed\n");
        NAT_TRC (NAT_TRC_ON, "No Such Entry exists Test Failed\n");
        return SNMP_FAILURE;
    }
    NAT_TRC (NAT_TRC_ON, "Exiting with SUCCESS: Entry found \n");
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2NatTranslatedLocalPortStart
 Input       :  The Indices

                The Object 
                testValNatTranslatedLocalPortStart
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatTranslatedLocalPortStart (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValNatTranslatedLocalPortStart)
{
    NAT_DBG1 (NAT_DBG_LLR, "TestNatTranslatedLocalPort = %d\n",
              i4TestValNatTranslatedLocalPortStart);
    /* the Translated Port should be a non-system port */
    if ((i4TestValNatTranslatedLocalPortStart < NAT_NONSYS_PORT)
        || (i4TestValNatTranslatedLocalPortStart > NAT_PORT_LIMIT))
    {
        NAT_TRC (NAT_TRC_ON, "Exiting with FAILURE: Entry found \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (NAT_ENABLE == gu4NatEnable)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    NAT_TRC (NAT_TRC_ON, "Existing with SUCCESS: Entry Found\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2NatIdleTimeOut
 Input       :  The Indices

                The Object 
                testValNatIdleTimeOut
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatIdleTimeOut (UINT4 *pu4ErrorCode, INT4 i4TestValNatIdleTimeOut)
{
    NAT_DBG1 (NAT_DBG_LLR, "NatIdleTimeOut = %d\n", i4TestValNatIdleTimeOut);
    if ((i4TestValNatIdleTimeOut < NAT_DEF_IDLE_TIMEOUT) ||
        (i4TestValNatIdleTimeOut > NAT_MAX_IDLE_TIMEOUT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT",
                 "\nIllegal value for NatIdleTimeOut\n");
        NAT_TRC (NAT_TRC_ON, "No Such Entry exists Test Failed\n");
        return SNMP_FAILURE;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }

}

/****************************************************************************
 Function    :  nmhTestv2NatTcpTimeOut
 Input       :  The Indices

                The Object 
                testValNatTcpTimeOut
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatTcpTimeOut (UINT4 *pu4ErrorCode, INT4 i4TestValNatTcpTimeOut)
{
    NAT_DBG1 (NAT_DBG_LLR, "NatTcpTimeOut = %d\n", i4TestValNatTcpTimeOut);
    if ((i4TestValNatTcpTimeOut < NAT_MIN_TCP_TIMEOUT) ||
        (i4TestValNatTcpTimeOut > NAT_MAX_TCP_TIMEOUT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT",
                 "\nIllegal value for NatTcpTimeOut\n");
        NAT_TRC (NAT_TRC_ON, "No Such Entry exists Test Failed\n");
        CLI_SET_ERR (CLI_NAT_ERR_INVALID_TCP_TIMEOUT_RANGE);
        return SNMP_FAILURE;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhTestv2NatUdpTimeOut
 Input       :  The Indices

                The Object 
                testValNatUdpTimeOut
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatUdpTimeOut (UINT4 *pu4ErrorCode, INT4 i4TestValNatUdpTimeOut)
{
    NAT_DBG1 (NAT_DBG_LLR, "NatUdpTimeOut = %d\n", i4TestValNatUdpTimeOut);
    if ((i4TestValNatUdpTimeOut < NAT_DEF_UDP_TIMEOUT) ||
        (i4TestValNatUdpTimeOut > NAT_MAX_UDP_TIMEOUT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT",
                 "\nIllegal value for NatUdpTimeOut\n");
        NAT_TRC (NAT_TRC_ON, "No Such Entry exists Test Failed\n");
        CLI_SET_ERR (CLI_NAT_ERR_INVALID_UDP_TIMEOUT_RANGE);
        return SNMP_FAILURE;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhTestv2NatTrcFlag
 Input       :  The Indices

                The Object 
                testValNatTrcFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatTrcFlag (UINT4 *pu4ErrorCode, INT4 i4TestValNatTrcFlag)
{
   /*** $$TRACE_LOG (ENTRY, "NatTrcFlag = %d\n", i4TestValNatTrcFlag); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    if (i4TestValNatTrcFlag < NAT_ZERO)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2NatIKEPortTranslation
 Input       :  The Indices

                The Object 
                testValNatIKEPortTranslation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatIKEPortTranslation (UINT4 *pu4ErrorCode,
                                INT4 i4TestValNatIKEPortTranslation)
{
    NAT_DBG1 (NAT_DBG_LLR, "NatIKEPortTranslation = %d\n",
              i4TestValNatIKEPortTranslation);
    /*UT_FIX -START */
    if ((i4TestValNatIKEPortTranslation == NAT_ENABLE) ||
        (i4TestValNatIKEPortTranslation == NAT_DISABLE))
        /*UT_FIX -END */
    {
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        NAT_TRC (NAT_TRC_ON, "Exiting with SUCCESS: Entry found \n");
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT",
                 "\nIllegal value for NatIKEPortTranslation\n");
        NAT_TRC (NAT_TRC_ON, "No Such Entry exists Test Failed\n");
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2NatIKETimeout
 Input       :  The Indices

                The Object 
                testValNatIKETimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatIKETimeout (UINT4 *pu4ErrorCode, INT4 i4TestValNatIKETimeout)
{
    NAT_DBG1 (NAT_DBG_LLR, "NatIKETimeout = %d\n", i4TestValNatIKETimeout);
    /*UT_FIX -START */
    if ((i4TestValNatIKETimeout >= NAT_MIN_IKE_TIMEOUT) &&
        (i4TestValNatIKETimeout <= NAT_MAX_IKE_TIMEOUT))
        /*UT_FIX -END */
    {
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        NAT_TRC (NAT_TRC_ON, "Exiting with SUCCESS: Entry found \n");
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT",
                 "\nIllegal value for NatIKETimeout\n");
        NAT_TRC (NAT_TRC_ON, "No Such Entry exists Test Failed\n");
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2NatIPSecTimeout
 Input       :  The Indices

                The Object 
                testValNatIPSecTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatIPSecTimeout (UINT4 *pu4ErrorCode, INT4 i4TestValNatIPSecTimeout)
{
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecTimeout = %d\n", i4TestValNatIPSecTimeout);
    /* UT_FIX -START */
    if ((i4TestValNatIPSecTimeout >= NAT_MIN_IPSEC_TIMEOUT) &&
        (i4TestValNatIPSecTimeout <= NAT_MAX_IPSEC_TIMEOUT))
        /* UT_FIX -END */
    {
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        NAT_TRC (NAT_TRC_ON, "Exiting with SUCCESS: Entry found \n");
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT",
                 "\nIllegal value for NatIPSecTimeout\n");
        NAT_TRC (NAT_TRC_ON, "No Such Entry exists Test Failed\n");
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2NatIPSecPendingTimeout
 Input       :  The Indices

                The Object 
                testValNatIPSecPendingTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatIPSecPendingTimeout (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValNatIPSecPendingTimeout)
{
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingTimeOut = %d\n",
              i4TestValNatIPSecPendingTimeout);
    /*UT_FIX -START */
    if ((i4TestValNatIPSecPendingTimeout >= NAT_MIN_IPSEC_PENDING_TIMEOUT)
        && (i4TestValNatIPSecPendingTimeout <= NAT_MAX_IPSEC_PENDING_TIMEOUT))
        /*UT_FIX-END */
    {
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        NAT_TRC (NAT_TRC_ON, "Exiting with SUCCESS: Entry found \n");
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT",
                 "\nIllegal value for NatIPSecPendingTimeout\n");
        NAT_TRC (NAT_TRC_ON, "No Such Entry exists Test Failed\n");
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2NatIPSecMaxRetry
 Input       :  The Indices

                The Object 
                testValNatIPSecMaxRetry
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatIPSecMaxRetry (UINT4 *pu4ErrorCode, INT4 i4TestValNatIPSecMaxRetry)
{
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecMaxRetry = %d\n",
              i4TestValNatIPSecMaxRetry);
    /*UT_FIX -START */
    if ((i4TestValNatIPSecMaxRetry >= NAT_MIN_IPSEC_RETRY) &&
        (i4TestValNatIPSecMaxRetry <= NAT_MAX_IPSEC_RETRY))
        /*UT_FIX -END */
    {
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        NAT_TRC (NAT_TRC_ON, "Exiting with SUCCESS: Entry found \n");
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT",
                 "\nIllegal value for NatIPSecMaxRetry\n");
        NAT_TRC (NAT_TRC_ON, "No Such Entry exists Test Failed\n");
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2SipAlgPort
 Input       :  The Indices

                The Object 
                testValSipAlgPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SipAlgPort (UINT4 *pu4ErrorCode, INT4 i4TestValSipAlgPort)
{
    NAT_DBG1 (NAT_DBG_LLR, "SipAlgPort = %d\n", i4TestValSipAlgPort);

    if ((NAT_MIN_SIP_ALG_PORT > i4TestValSipAlgPort) ||
        (NAT_MAX_SIP_ALG_PORT < i4TestValSipAlgPort))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT",
                 "\nIllegal value for SipAlgPort\n");
        NAT_TRC (NAT_TRC_ON, "No Such Entry exists Test Failed\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2NatSipAlgPartialEntryTimeOut
 Input       :  The Indices

                The Object 
                testValNatSipAlgPartialEntryTimeOut
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatSipAlgPartialEntryTimeOut (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValNatSipAlgPartialEntryTimeOut)
{
    NAT_DBG1 (NAT_DBG_LLR, "SipAlgPartialEntryTimeOut = %d\n",
              i4TestValNatSipAlgPartialEntryTimeOut);

    if ((NAT_MIN_SIP_ALG_TIMEOUT > i4TestValNatSipAlgPartialEntryTimeOut) ||
        (NAT_MAX_SIP_ALG_TIMEOUT < i4TestValNatSipAlgPartialEntryTimeOut))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT",
                 "\nIllegal value for SipAlgPartialEntryTimeOut\n");
        NAT_TRC (NAT_TRC_ON, "No Such Entry exists Test Failed\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2NatEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2NatEnable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2NatTypicalNumberOfEntries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2NatTypicalNumberOfEntries (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2NatTranslatedLocalPortStart
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2NatTranslatedLocalPortStart (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2NatIdleTimeOut
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2NatIdleTimeOut (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2NatTcpTimeOut
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2NatTcpTimeOut (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2NatUdpTimeOut
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2NatUdpTimeOut (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2NatTrcFlag
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2NatTrcFlag (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2NatIKEPortTranslation
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2NatIKEPortTranslation (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2NatIKETimeout
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2NatIKETimeout (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2NatIPSecTimeout
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2NatIPSecTimeout (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2NatIPSecPendingTimeout
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2NatIPSecPendingTimeout (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2NatIPSecMaxRetry
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2NatIPSecMaxRetry (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2SipAlgPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SipAlgPort (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2NatSipAlgPartialEntryTimeOut
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2NatSipAlgPartialEntryTimeOut (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2NatGlobalAddressTable
 Input       :  The Indices
                NatGlobalAddressInterfaceNum
                NatGlobalAddressTranslatedLocalIp
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2NatGlobalAddressTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2NatLocalAddressTable
 Input       :  The Indices
                NatLocalAddressInterfaceNumber
                NatLocalAddressLocalIp
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2NatLocalAddressTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2NatStaticTable
 Input       :  The Indices
                NatStaticInterfaceNum
                NatStaticLocalIp
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2NatStaticTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2NatStaticNaptTable
 Input       :  The Indices
                NatStaticNaptInterfaceNum
                NatStaticNaptLocalIp
                NatStaticNaptStartLocalPort
                NatStaticNaptEndLocalPort
                NatStaticNaptProtocolNumber
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2NatStaticNaptTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2NatIfTable
 Input       :  The Indices
                NatIfInterfaceNumber
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2NatIfTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2NatIPSecSessionTable
 Input       :  The Indices
                NatIPSecSessionInterfaceNum
                NatIPSecSessionLocalIp
                NatIPSecSessionOutsideIp
                NatIPSecSessionSPIInside
                NatIPSecSessionSPIOutside
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2NatIPSecSessionTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2NatIPSecPendingTable
 Input       :  The Indices
                NatIPSecPendingInterfaceNum
                NatIPSecPendingLocalIp
                NatIPSecPendingOutsideIp
                NatIPSecPendingSPIInside
                NatIPSecPendingSPIOutside
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2NatIPSecPendingTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2NatIKESessionTable
 Input       :  The Indices
                NatIKESessionInterfaceNum
                NatIKESessionLocalIp
                NatIKESessionOutsideIp
                NatIKESessionInitCookie
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2NatIKESessionTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2NatPortTrigInfoTable
 Input       :  The Indices
                NatPortTrigInfoInBoundPortRange
                NatPortTrigInfoOutBoundPortRange
                NatPortTrigInfoProtocol
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2NatPortTrigInfoTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : NatDynamicTransTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceNatDynamicTransTable
 Input       :  The Indices
                NatDynamicTransInterfaceNum
                NatDynamicTransLocalIp
                NatDynamicTransLocalPort
                NatDynamicTransOutsideIp
                NatDynamicTransOutsidePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceNatDynamicTransTable (INT4
                                              i4NatDynamicTransInterfaceNum,
                                              UINT4 u4NatDynamicTransLocalIp,
                                              INT4 i4NatDynamicTransLocalPort,
                                              UINT4 u4NatDynamicTransOutsideIp,
                                              INT4 i4NatDynamicTransOutsidePort)
{
    tDynamicEntry      *pDynamicNode = NULL;
    NAT_TRC (NAT_TRC_ON,
             "Entering the function: "
             "ValidateIndexInstance for NatDynamicTransTable\n");
    NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransInterfaceNum = %d\n",
              i4NatDynamicTransInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransLocalIp = %u\n",
              u4NatDynamicTransLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransLocalPort = %d\n",
              i4NatDynamicTransLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransOutsideIp = %u\n",
              u4NatDynamicTransOutsideIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransOutsidePort = %d\n",
              i4NatDynamicTransOutsidePort);

    pDynamicNode = NULL;
    TMO_SLL_Scan (&gNatDynamicList, pDynamicNode, tDynamicEntry *)
    {
        if ((pDynamicNode->u4IfNum == (UINT4) i4NatDynamicTransInterfaceNum) &&
            (pDynamicNode->u4LocIpAddr == u4NatDynamicTransLocalIp) &&
            (pDynamicNode->u2LocPort == (UINT2) i4NatDynamicTransLocalPort) &&
            (pDynamicNode->u4OutIpAddr == u4NatDynamicTransOutsideIp) &&
            (pDynamicNode->u2OutPort == (UINT2) i4NatDynamicTransOutsidePort))
        {
            NAT_TRC (NAT_TRC_ON,
                     "Exiting with SUCCESS: Entry found in gNatDymamicList\n");
            return SNMP_SUCCESS;
        }

    }
    TMO_SLL_Scan (&gNatNfsList, pDynamicNode, tDynamicEntry *)
    {
        if ((pDynamicNode->u4IfNum == (UINT4) i4NatDynamicTransInterfaceNum) &&
            (pDynamicNode->u4LocIpAddr == u4NatDynamicTransLocalIp) &&
            (pDynamicNode->u2LocPort == (UINT2) i4NatDynamicTransLocalPort) &&
            (pDynamicNode->u4OutIpAddr == u4NatDynamicTransOutsideIp) &&
            (pDynamicNode->u2OutPort == (UINT2) i4NatDynamicTransOutsidePort))
        {
            NAT_TRC (NAT_TRC_ON,
                     "Exiting with SUCCESS: Entry found in gNatNfsList\n");
            return SNMP_SUCCESS;
        }

    }

    NAT_TRC (NAT_TRC_ON, "No Such Entry exists Test Failed\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexNatDynamicTransTable
 Input       :  The Indices
                NatDynamicTransInterfaceNum
                NatDynamicTransLocalIp
                NatDynamicTransLocalPort
                NatDynamicTransOutsideIp
                NatDynamicTransOutsidePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexNatDynamicTransTable (INT4 *pi4NatDynamicTransInterfaceNum,
                                      UINT4 *pu4NatDynamicTransLocalIp,
                                      INT4 *pi4NatDynamicTransLocalPort,
                                      UINT4 *pu4NatDynamicTransOutsideIp,
                                      INT4 *pi4NatDynamicTransOutsidePort)
{

    tDynamicEntry      *pDynamicNode = NULL;

    pDynamicNode = NULL;

    if ((pDynamicNode = (tDynamicEntry *) TMO_SLL_First (&gNatDynamicList)) !=
        NULL)
    {
        *pi4NatDynamicTransInterfaceNum = (INT4) pDynamicNode->u4IfNum;
        *pu4NatDynamicTransLocalIp = pDynamicNode->u4LocIpAddr;
        *pi4NatDynamicTransLocalPort = (INT4) pDynamicNode->u2LocPort;
        *pu4NatDynamicTransOutsideIp = pDynamicNode->u4OutIpAddr;
        *pi4NatDynamicTransOutsidePort = (INT4) pDynamicNode->u2OutPort;
        NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransInterfaceNum = %d\n",
                  *pi4NatDynamicTransInterfaceNum);
        NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransLocalIp = %u\n",
                  *pu4NatDynamicTransLocalIp);
        NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransLocalPort = %d\n",
                  *pi4NatDynamicTransLocalPort);
        NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransOutsideIp = %u\n",
                  *pu4NatDynamicTransOutsideIp);
        NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransOutsidePort = %d\n",
                  *pi4NatDynamicTransOutsidePort);
        return SNMP_SUCCESS;
    }

    if ((pDynamicNode = (tDynamicEntry *) TMO_SLL_First (&gNatNfsList)) != NULL)
    {
        *pi4NatDynamicTransInterfaceNum = (INT4) pDynamicNode->u4IfNum;
        *pu4NatDynamicTransLocalIp = pDynamicNode->u4LocIpAddr;
        *pi4NatDynamicTransLocalPort = (INT4) pDynamicNode->u2LocPort;
        *pu4NatDynamicTransOutsideIp = pDynamicNode->u4OutIpAddr;
        *pi4NatDynamicTransOutsidePort = (INT4) pDynamicNode->u2OutPort;
        NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransInterfaceNum = %d\n",
                  *pi4NatDynamicTransInterfaceNum);
        NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransLocalIp = %u\n",
                  *pu4NatDynamicTransLocalIp);
        NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransLocalPort = %d\n",
                  *pi4NatDynamicTransLocalPort);
        NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransOutsideIp = %u\n",
                  *pu4NatDynamicTransOutsideIp);
        NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransOutsidePort = %d\n",
                  *pi4NatDynamicTransOutsidePort);
        return SNMP_SUCCESS;
    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexNatDynamicTransTable
 Input       :  The Indices
                NatDynamicTransInterfaceNum
                nextNatDynamicTransInterfaceNum
                NatDynamicTransLocalIp
                nextNatDynamicTransLocalIp
                NatDynamicTransLocalPort
                nextNatDynamicTransLocalPort
                NatDynamicTransOutsideIp
                nextNatDynamicTransOutsideIp
                NatDynamicTransOutsidePort
                nextNatDynamicTransOutsidePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexNatDynamicTransTable (INT4 i4NatDynamicTransInterfaceNum,
                                     INT4 *pi4NextNatDynamicTransInterfaceNum,
                                     UINT4 u4NatDynamicTransLocalIp,
                                     UINT4 *pu4NextNatDynamicTransLocalIp,
                                     INT4 i4NatDynamicTransLocalPort,
                                     INT4 *pi4NextNatDynamicTransLocalPort,
                                     UINT4 u4NatDynamicTransOutsideIp,
                                     UINT4 *pu4NextNatDynamicTransOutsideIp,
                                     INT4 i4NatDynamicTransOutsidePort,
                                     INT4 *pi4NextNatDynamicTransOutsidePort)
{
    UINT1               u1DynNodeFound = NAT_FALSE;

    tDynamicEntry      *pDynamicNode = NULL;

    NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransInterfaceNum = %d\n",
              i4NatDynamicTransInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransLocalIp = %u\n",
              u4NatDynamicTransLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransLocalPort = %d\n",
              i4NatDynamicTransLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransOutsideIp = %u\n",
              u4NatDynamicTransOutsideIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransOutsidePort = %d\n",
              i4NatDynamicTransOutsidePort);

    pDynamicNode = NULL;
    TMO_SLL_Scan (&gNatDynamicList, pDynamicNode, tDynamicEntry *)
    {
        if ((pDynamicNode->u4IfNum == (UINT4) i4NatDynamicTransInterfaceNum) &&
            (pDynamicNode->u4LocIpAddr == u4NatDynamicTransLocalIp) &&
            (pDynamicNode->u2LocPort == (UINT2) i4NatDynamicTransLocalPort) &&
            (pDynamicNode->u4OutIpAddr == u4NatDynamicTransOutsideIp) &&
            (pDynamicNode->u2OutPort == (UINT2) i4NatDynamicTransOutsidePort))
        {
            u1DynNodeFound = NAT_TRUE;
            break;
        }

    }

    if (pDynamicNode != NULL)
    {
        pDynamicNode =
            (tDynamicEntry *) TMO_SLL_Next ((tTMO_SLL *) & gNatDynamicList,
                                            &(pDynamicNode->DynamicTableEntry));
    }

    if (pDynamicNode == NULL)
    {
        if (u1DynNodeFound == NAT_TRUE)
        {
            /* When control reaches here, we would traversed the dynamic 
             * table fully and first node of gNatNfsList has to be returned */
            pDynamicNode = (tDynamicEntry *) TMO_SLL_First (&gNatNfsList);
        }
        else
        {
            TMO_SLL_Scan (&gNatNfsList, pDynamicNode, tDynamicEntry *)
            {
                if ((pDynamicNode->u4IfNum ==
                     (UINT4) i4NatDynamicTransInterfaceNum)
                    && (pDynamicNode->u4LocIpAddr == u4NatDynamicTransLocalIp)
                    && (pDynamicNode->u2LocPort ==
                        (UINT2) i4NatDynamicTransLocalPort)
                    && (pDynamicNode->u4OutIpAddr == u4NatDynamicTransOutsideIp)
                    && (pDynamicNode->u2OutPort ==
                        (UINT2) i4NatDynamicTransOutsidePort))
                {
                    break;
                }

            }
            if (pDynamicNode != NULL)
            {
                pDynamicNode =
                    (tDynamicEntry *) TMO_SLL_Next (&gNatNfsList,
                                                    &(pDynamicNode->
                                                      DynamicTableEntry));
            }
        }
    }

    if (pDynamicNode != NULL)
    {
        *pi4NextNatDynamicTransInterfaceNum = (INT4) pDynamicNode->u4IfNum;
        *pu4NextNatDynamicTransLocalIp = pDynamicNode->u4LocIpAddr;
        *pi4NextNatDynamicTransLocalPort = (INT4) pDynamicNode->u2LocPort;
        *pu4NextNatDynamicTransOutsideIp = pDynamicNode->u4OutIpAddr;
        *pi4NextNatDynamicTransOutsidePort = (INT4) pDynamicNode->u2OutPort;
        NAT_DBG1 (NAT_DBG_LLR, "NatNextDynamicTransInterfaceNum = %d\n",
                  *pi4NextNatDynamicTransInterfaceNum);
        NAT_DBG1 (NAT_DBG_LLR, "NatNextDynamicTransLocalIp = %u\n",
                  *pu4NextNatDynamicTransLocalIp);
        NAT_DBG1 (NAT_DBG_LLR, "NatNextDynamicTransLocalPort = %d\n",
                  *pi4NextNatDynamicTransLocalPort);
        NAT_DBG1 (NAT_DBG_LLR, "NatNextDynamicTransOutsideIp = %u\n",
                  *pu4NextNatDynamicTransOutsideIp);
        NAT_DBG1 (NAT_DBG_LLR, "NatNextDynamicTransOutsidePort = %d\n",
                  *pi4NextNatDynamicTransOutsidePort);
        return SNMP_SUCCESS;
    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetNatDynamicTransTranslatedLocalIp
 Input       :  The Indices
                NatDynamicTransInterfaceNum
                NatDynamicTransLocalIp
                NatDynamicTransLocalPort
                NatDynamicTransOutsideIp
                NatDynamicTransOutsidePort

                The Object 
                retValNatDynamicTransTranslatedLocalIp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatDynamicTransTranslatedLocalIp (INT4 i4NatDynamicTransInterfaceNum,
                                        UINT4 u4NatDynamicTransLocalIp,
                                        INT4 i4NatDynamicTransLocalPort,
                                        UINT4 u4NatDynamicTransOutsideIp,
                                        INT4 i4NatDynamicTransOutsidePort,
                                        UINT4
                                        *pu4RetValNatDynamicTransTranslatedLocalIp)
{

    tDynamicEntry      *pDynamicNode = NULL;

    pDynamicNode = NULL;
    NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransInterfaceNum = %d\n",
              i4NatDynamicTransInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransLocalIp = %u\n",
              u4NatDynamicTransLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransLocalPort = %d\n",
              i4NatDynamicTransLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransOutsideIp = %u\n",
              u4NatDynamicTransOutsideIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransOutsidePort = %d\n",
              i4NatDynamicTransOutsidePort);

    TMO_SLL_Scan (&gNatDynamicList, pDynamicNode, tDynamicEntry *)
    {
        if ((pDynamicNode->u4IfNum == (UINT4) i4NatDynamicTransInterfaceNum) &&
            (pDynamicNode->u4LocIpAddr == u4NatDynamicTransLocalIp) &&
            (pDynamicNode->u2LocPort == (UINT2) i4NatDynamicTransLocalPort) &&
            (pDynamicNode->u4OutIpAddr == u4NatDynamicTransOutsideIp) &&
            (pDynamicNode->u2OutPort == (UINT2) i4NatDynamicTransOutsidePort))
        {
            *pu4RetValNatDynamicTransTranslatedLocalIp =
                pDynamicNode->u4TranslatedLocIpAddr;
            NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransTranslatedLocIp = %u\n",
                      *pu4RetValNatDynamicTransTranslatedLocalIp);
            return SNMP_SUCCESS;
        }

    }

    TMO_SLL_Scan (&gNatNfsList, pDynamicNode, tDynamicEntry *)
    {
        if ((pDynamicNode->u4IfNum == (UINT4) i4NatDynamicTransInterfaceNum) &&
            (pDynamicNode->u4LocIpAddr == u4NatDynamicTransLocalIp) &&
            (pDynamicNode->u2LocPort == (UINT2) i4NatDynamicTransLocalPort) &&
            (pDynamicNode->u4OutIpAddr == u4NatDynamicTransOutsideIp) &&
            (pDynamicNode->u2OutPort == (UINT2) i4NatDynamicTransOutsidePort))
        {
            *pu4RetValNatDynamicTransTranslatedLocalIp =
                pDynamicNode->u4TranslatedLocIpAddr;
            NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransTranslatedLocIp = %u\n",
                      *pu4RetValNatDynamicTransTranslatedLocalIp);
            return SNMP_SUCCESS;
        }

    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNatDynamicTransTranslatedLocalPort
 Input       :  The Indices
                NatDynamicTransInterfaceNum
                NatDynamicTransLocalIp
                NatDynamicTransLocalPort
                NatDynamicTransOutsideIp
                NatDynamicTransOutsidePort

                The Object 
                retValNatDynamicTransTranslatedLocalPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatDynamicTransTranslatedLocalPort (INT4 i4NatDynamicTransInterfaceNum,
                                          UINT4 u4NatDynamicTransLocalIp,
                                          INT4 i4NatDynamicTransLocalPort,
                                          UINT4 u4NatDynamicTransOutsideIp,
                                          INT4 i4NatDynamicTransOutsidePort,
                                          INT4
                                          *pi4RetValNatDynamicTransTranslatedLocalPort)
{

    tDynamicEntry      *pDynamicNode = NULL;

    NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransInterfaceNum = %d\n",
              i4NatDynamicTransInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransLocalIp = %u\n",
              u4NatDynamicTransLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransLocalPort = %d\n",
              i4NatDynamicTransLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransOutsideIp = %u\n",
              u4NatDynamicTransOutsideIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransOutsidePort = %d\n",
              i4NatDynamicTransOutsidePort);

    pDynamicNode = NULL;
    TMO_SLL_Scan (&gNatDynamicList, pDynamicNode, tDynamicEntry *)
    {
        if ((pDynamicNode->u4IfNum == (UINT4) i4NatDynamicTransInterfaceNum) &&
            (pDynamicNode->u4LocIpAddr == u4NatDynamicTransLocalIp) &&
            (pDynamicNode->u2LocPort == (UINT2) i4NatDynamicTransLocalPort) &&
            (pDynamicNode->u4OutIpAddr == u4NatDynamicTransOutsideIp) &&
            (pDynamicNode->u2OutPort == (UINT2) i4NatDynamicTransOutsidePort))
        {
            *pi4RetValNatDynamicTransTranslatedLocalPort =
                (INT4) pDynamicNode->u2TranslatedLocPort;
            NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransTranslatedLocPort = %d\n",
                      *pi4RetValNatDynamicTransTranslatedLocalPort);
            return SNMP_SUCCESS;
        }

    }
    TMO_SLL_Scan (&gNatNfsList, pDynamicNode, tDynamicEntry *)
    {
        if ((pDynamicNode->u4IfNum == (UINT4) i4NatDynamicTransInterfaceNum) &&
            (pDynamicNode->u4LocIpAddr == u4NatDynamicTransLocalIp) &&
            (pDynamicNode->u2LocPort == (UINT2) i4NatDynamicTransLocalPort) &&
            (pDynamicNode->u4OutIpAddr == u4NatDynamicTransOutsideIp) &&
            (pDynamicNode->u2OutPort == (UINT2) i4NatDynamicTransOutsidePort))
        {
            *pi4RetValNatDynamicTransTranslatedLocalPort =
                (INT4) pDynamicNode->u2TranslatedLocPort;
            NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransTranslatedLocPort = %d\n",
                      *pi4RetValNatDynamicTransTranslatedLocalPort);
            return SNMP_SUCCESS;
        }
    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNatDynamicTransLastUseTime
 Input       :  The Indices
                NatDynamicTransInterfaceNum
                NatDynamicTransLocalIp
                NatDynamicTransLocalPort
                NatDynamicTransOutsideIp
                NatDynamicTransOutsidePort

                The Object 
                retValNatDynamicTransLastUseTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatDynamicTransLastUseTime (INT4 i4NatDynamicTransInterfaceNum,
                                  UINT4 u4NatDynamicTransLocalIp,
                                  INT4 i4NatDynamicTransLocalPort,
                                  UINT4 u4NatDynamicTransOutsideIp,
                                  INT4 i4NatDynamicTransOutsidePort,
                                  INT4 *pi4RetValNatDynamicTransLastUseTime)
{

    tDynamicEntry      *pDynamicNode = NULL;
    UINT4               u4CurrTime = NAT_ZERO;

    NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransInterfaceNum = %d\n",
              i4NatDynamicTransInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransLocalIp = %u\n",
              u4NatDynamicTransLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransLocalPort = %d\n",
              i4NatDynamicTransLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransOutsideIp = %u\n",
              u4NatDynamicTransOutsideIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransOutsidePort = %d\n",
              i4NatDynamicTransOutsidePort);

    pDynamicNode = NULL;
    TMO_SLL_Scan (&gNatDynamicList, pDynamicNode, tDynamicEntry *)
    {
        if ((pDynamicNode->u4IfNum == (UINT4) i4NatDynamicTransInterfaceNum) &&
            (pDynamicNode->u4LocIpAddr == u4NatDynamicTransLocalIp) &&
            (pDynamicNode->u2LocPort == (UINT2) i4NatDynamicTransLocalPort) &&
            (pDynamicNode->u4OutIpAddr == u4NatDynamicTransOutsideIp) &&
            (pDynamicNode->u2OutPort == (UINT2) i4NatDynamicTransOutsidePort))
        {
            NAT_GET_SYS_TIME (&u4CurrTime);
            *pi4RetValNatDynamicTransLastUseTime =
                (INT4) ((u4CurrTime -
                         pDynamicNode->u4TimeStamp) /
                        NAT_NUM_OF_SYS_TIME_UNITS_IN_A_SEC);
            NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransLastUseTime = %d\n",
                      *pi4RetValNatDynamicTransLastUseTime);
            return SNMP_SUCCESS;
        }

    }
    TMO_SLL_Scan (&gNatNfsList, pDynamicNode, tDynamicEntry *)
    {
        if ((pDynamicNode->u4IfNum == (UINT4) i4NatDynamicTransInterfaceNum) &&
            (pDynamicNode->u4LocIpAddr == u4NatDynamicTransLocalIp) &&
            (pDynamicNode->u2LocPort == (UINT2) i4NatDynamicTransLocalPort) &&
            (pDynamicNode->u4OutIpAddr == u4NatDynamicTransOutsideIp) &&
            (pDynamicNode->u2OutPort == (UINT2) i4NatDynamicTransOutsidePort))
        {
            *pi4RetValNatDynamicTransLastUseTime =
                (INT4) pDynamicNode->u4TimeStamp;
            NAT_DBG1 (NAT_DBG_LLR, "NatDynamicTransLastUseTime = %d\n",
                      *pi4RetValNatDynamicTransLastUseTime);
            return SNMP_SUCCESS;
        }

    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : NatGlobalAddressTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceNatGlobalAddressTable
 Input       :  The Indices
                NatGlobalAddressInterfaceNum
                NatGlobalAddressTranslatedLocalIp
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceNatGlobalAddressTable (INT4
                                               i4NatGlobalAddressInterfaceNum,
                                               UINT4
                                               u4NatGlobalAddressTranslatedLocalIp)
{
    tInterfaceInfo     *pIfNode = NULL;

    tTranslatedLocIpAddrNode *pTranslatedLocIpAddrNode = NULL;

    NAT_TRC (NAT_TRC_ON,
             "Entering the function: "
             "ValidateIndexInstance for Nat GlobalAddressTable\n");
    NAT_DBG1 (NAT_DBG_LLR, "NatGlobalAddressInterfaceNum = %d\n",
              i4NatGlobalAddressInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatGlobalAddressTranslatedLocIp = %u\n",
              u4NatGlobalAddressTranslatedLocalIp);

    pTranslatedLocIpAddrNode = NULL;
    /* FInd if the Interface is a valid Interface */
    if (SecUtilValidateIfIndex ((UINT2) i4NatGlobalAddressInterfaceNum) ==
        OSIX_SUCCESS)
    {
        if (gapNatIfTable[i4NatGlobalAddressInterfaceNum] != NULL)
        {
            pIfNode = gapNatIfTable[i4NatGlobalAddressInterfaceNum];
            /* MSAD ADD */
            TMO_SLL_Scan (&(pIfNode->TranslatedLocIpList),
                          pTranslatedLocIpAddrNode, tTranslatedLocIpAddrNode *)
            {
                if (pTranslatedLocIpAddrNode->u4TranslatedLocIpAddr ==
                    u4NatGlobalAddressTranslatedLocalIp)
                {
                    NAT_TRC (NAT_TRC_ON,
                             "Exiting with SUCCESS: Entry found \n");
                    MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                                  "The row of Global Address Table "
                                  "of  index %d and %d    exist\n",
                                  i4NatGlobalAddressInterfaceNum,
                                  u4NatGlobalAddressTranslatedLocalIp);
                    return SNMP_SUCCESS;
                }
            }

        }
    }
    MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Global Address Table of  index %d and %d"
                  "does not exist\n",
                  i4NatGlobalAddressInterfaceNum,
                  u4NatGlobalAddressTranslatedLocalIp);
    NAT_TRC (NAT_TRC_ON, "No Such Entry exists Test Failed\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexNatGlobalAddressTable
 Input       :  The Indices
                NatGlobalAddressInterfaceNum
                NatGlobalAddressTranslatedLocalIp
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexNatGlobalAddressTable (INT4 *pi4NatGlobalAddressInterfaceNum,
                                       UINT4
                                       *pu4NatGlobalAddressTranslatedLocalIp)
{

    tInterfaceInfo     *pIfNode = NULL;
    tTranslatedLocIpAddrNode *pTranslatedLocIpAddrNode = NULL;
    INT4                i4IfNum = NAT_ZERO;

    pTranslatedLocIpAddrNode = NULL;
    i4IfNum = NAT_ONE;

    while (i4IfNum <= NAT_MAX_NUM_IF)
    {
        if (gapNatIfTable[i4IfNum] != NULL)
        {
            pIfNode = gapNatIfTable[i4IfNum];
            *pi4NatGlobalAddressInterfaceNum = i4IfNum;
            /* MSAD ADD */
            pTranslatedLocIpAddrNode = (tTranslatedLocIpAddrNode *)
                TMO_SLL_First (&(pIfNode->TranslatedLocIpList));
            if (pTranslatedLocIpAddrNode != NULL)
            {
                *pu4NatGlobalAddressTranslatedLocalIp =
                    pTranslatedLocIpAddrNode->u4TranslatedLocIpAddr;
                NAT_DBG1 (NAT_DBG_LLR, "NatGlobalAddressInterfaceNum = %d\n",
                          *pi4NatGlobalAddressInterfaceNum);
                NAT_DBG1 (NAT_DBG_LLR, "NatGlobalAddressTranslatedLocIp = %u\n",
                          *pu4NatGlobalAddressTranslatedLocalIp);
                return SNMP_SUCCESS;
            }

        }
        i4IfNum++;
    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexNatGlobalAddressTable
 Input       :  The Indices
                NatGlobalAddressInterfaceNum
                nextNatGlobalAddressInterfaceNum
                NatGlobalAddressTranslatedLocalIp
                nextNatGlobalAddressTranslatedLocalIp
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexNatGlobalAddressTable (INT4 i4NatGlobalAddressInterfaceNum,
                                      INT4
                                      *pi4NextNatGlobalAddressInterfaceNum,
                                      UINT4
                                      u4NatGlobalAddressTranslatedLocalIp,
                                      UINT4
                                      *pu4NextNatGlobalAddressTranslatedLocalIp)
{

    tInterfaceInfo     *pIfNode = NULL;
    tTranslatedLocIpAddrNode *pTranslatedLocIpAddrNode = NULL;
    INT4                i4IfNum = NAT_ZERO;

    NAT_DBG1 (NAT_DBG_LLR, "NatGlobalAddressInterfaceNum = %d\n",
              i4NatGlobalAddressInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatGlobalAddressTranslatedLocIp = %u\n",
              u4NatGlobalAddressTranslatedLocalIp);

    pTranslatedLocIpAddrNode = NULL;
    i4IfNum = i4NatGlobalAddressInterfaceNum;

    if ((i4IfNum > NAT_MAX_NUM_IF) || (i4IfNum < NAT_ZERO))
    {
        return SNMP_FAILURE;
    }

    /* Search in the same interface list for next Global IP */
    pIfNode = gapNatIfTable[i4IfNum];
    if (pIfNode == NULL)
    {
        return SNMP_FAILURE;
    }
    /* MSAD ADD */
    TMO_SLL_Scan (&(pIfNode->TranslatedLocIpList), pTranslatedLocIpAddrNode,
                  tTranslatedLocIpAddrNode *)
    {
        if (pTranslatedLocIpAddrNode->u4TranslatedLocIpAddr ==
            u4NatGlobalAddressTranslatedLocalIp)
        {
            pTranslatedLocIpAddrNode = (tTranslatedLocIpAddrNode *)
                TMO_SLL_Next (&(pIfNode->TranslatedLocIpList),
                              &(pTranslatedLocIpAddrNode->
                                TranslatedLocIpAddrNode));

            if (pTranslatedLocIpAddrNode != NULL)
            {
                *pu4NextNatGlobalAddressTranslatedLocalIp =
                    pTranslatedLocIpAddrNode->u4TranslatedLocIpAddr;
                *pi4NextNatGlobalAddressInterfaceNum = i4IfNum;
                NAT_DBG1 (NAT_DBG_LLR,
                          "NatNextGlobalAddressInterfaceNum = %d\n",
                          *pi4NextNatGlobalAddressInterfaceNum);
                NAT_DBG1 (NAT_DBG_LLR,
                          "NatNextGlobalAddressTranslatedLocIp = %u\n",
                          *pu4NextNatGlobalAddressTranslatedLocalIp);
                return SNMP_SUCCESS;
            }
            else
            {
                break;
            }
        }
    }

    /*Search in the next interface entry */
    i4IfNum = i4NatGlobalAddressInterfaceNum + NAT_ONE;
    pTranslatedLocIpAddrNode = NULL;

    while (i4IfNum <= NAT_MAX_NUM_IF)
    {
        if (gapNatIfTable[i4IfNum] != NULL)
        {
            pIfNode = gapNatIfTable[i4IfNum];
            /* MSAD ADD */
            pTranslatedLocIpAddrNode = (tTranslatedLocIpAddrNode *)
                TMO_SLL_First (&(pIfNode->TranslatedLocIpList));
            if (pTranslatedLocIpAddrNode != NULL)
            {
                *pi4NextNatGlobalAddressInterfaceNum = i4IfNum;
                *pu4NextNatGlobalAddressTranslatedLocalIp =
                    pTranslatedLocIpAddrNode->u4TranslatedLocIpAddr;
                NAT_DBG1 (NAT_DBG_LLR,
                          "NatNextGlobalAddressInterfaceNum = %d\n",
                          *pi4NextNatGlobalAddressInterfaceNum);
                NAT_DBG1 (NAT_DBG_LLR,
                          "NatNextGlobalAddressTranslatedLocIp = %u\n",
                          *pu4NextNatGlobalAddressTranslatedLocalIp);
                return SNMP_SUCCESS;
            }

        }
        i4IfNum++;
    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetNatGlobalAddressMask
 Input       :  The Indices
                NatGlobalAddressInterfaceNum
                NatGlobalAddressTranslatedLocalIp

                The Object 
                retValNatGlobalAddressMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatGlobalAddressMask (INT4 i4NatGlobalAddressInterfaceNum,
                            UINT4 u4NatGlobalAddressTranslatedLocalIp,
                            UINT4 *pu4RetValNatGlobalAddressMask)
{

    tInterfaceInfo     *pIfNode = NULL;
    tTranslatedLocIpAddrNode *pTranslatedLocIpAddrNode = NULL;
    INT4                i4IfNum = NAT_ZERO;

    NAT_DBG1 (NAT_DBG_LLR, "NatGlobalAddressInterfaceNum = %d\n",
              i4NatGlobalAddressInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatGlobalAddressTranslatedLocIp = %u\n",
              u4NatGlobalAddressTranslatedLocalIp);

    pTranslatedLocIpAddrNode = NULL;
    i4IfNum = i4NatGlobalAddressInterfaceNum;

    if (i4IfNum <= NAT_MAX_NUM_IF)
    {
        if (gapNatIfTable[i4IfNum] != NULL)
        {
            pIfNode = gapNatIfTable[i4IfNum];
            TMO_SLL_Scan (&(pIfNode->TranslatedLocIpList),
                          pTranslatedLocIpAddrNode, tTranslatedLocIpAddrNode *)
            {
                if (u4NatGlobalAddressTranslatedLocalIp ==
                    pTranslatedLocIpAddrNode->u4TranslatedLocIpAddr)
                {
                    *pu4RetValNatGlobalAddressMask =
                        pTranslatedLocIpAddrNode->u4Mask;
                    NAT_DBG1 (NAT_DBG_LLR,
                              "NatGlobalAddressGlobalAddressMask = %u\n",
                              *pu4RetValNatGlobalAddressMask);
                    return SNMP_SUCCESS;
                }
            }

        }
    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNatGlobalAddressEntryStatus
 Input       :  The Indices
                NatGlobalAddressInterfaceNum
                NatGlobalAddressTranslatedLocalIp

                The Object 
                retValNatGlobalAddressEntryStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatGlobalAddressEntryStatus (INT4 i4NatGlobalAddressInterfaceNum,
                                   UINT4 u4NatGlobalAddressTranslatedLocalIp,
                                   INT4 *pi4RetValNatGlobalAddressEntryStatus)
{

    tInterfaceInfo     *pIfNode = NULL;
    tTranslatedLocIpAddrNode *pTranslatedLocIpAddrNode = NULL;
    INT4                i4IfNum = NAT_ZERO;

    NAT_DBG1 (NAT_DBG_LLR, "NatGlobalAddressInterfaceNum = %d\n",
              i4NatGlobalAddressInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatGlobalAddressTranslatedLocIp = %u\n",
              u4NatGlobalAddressTranslatedLocalIp);

    pTranslatedLocIpAddrNode = NULL;
    i4IfNum = i4NatGlobalAddressInterfaceNum;

    if (i4IfNum <= NAT_MAX_NUM_IF)
    {
        if (gapNatIfTable[i4IfNum] != NULL)
        {
            pIfNode = gapNatIfTable[i4IfNum];
            TMO_SLL_Scan (&(pIfNode->TranslatedLocIpList),
                          pTranslatedLocIpAddrNode, tTranslatedLocIpAddrNode *)
            {
                if (u4NatGlobalAddressTranslatedLocalIp ==
                    pTranslatedLocIpAddrNode->u4TranslatedLocIpAddr)
                {
                    *pi4RetValNatGlobalAddressEntryStatus =
                        pTranslatedLocIpAddrNode->i4RowStatus;
                    NAT_DBG1 (NAT_DBG_LLR, "NatGlobalAddressEntryStatus = %d\n",
                              *pi4RetValNatGlobalAddressEntryStatus);
                    return SNMP_SUCCESS;
                }
            }

        }
    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetNatGlobalAddressMask
 Input       :  The Indices
                NatGlobalAddressInterfaceNum
                NatGlobalAddressTranslatedLocalIp

                The Object 
                setValNatGlobalAddressMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatGlobalAddressMask (INT4 i4NatGlobalAddressInterfaceNum,
                            UINT4 u4NatGlobalAddressTranslatedLocalIp,
                            UINT4 u4SetValNatGlobalAddressMask)
{
    tInterfaceInfo     *pIfNode = NULL;
    tTranslatedLocIpAddrNode *pTranslatedLocIpAddrNode = NULL;
    INT4                i4IfNum = NAT_ZERO;
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    NAT_DBG1 (NAT_DBG_LLR, "NatGlobalAddressInterfaceNum = %d\n",
              i4NatGlobalAddressInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatGlobalAddressTranslatedLocIp = %u\n",
              u4NatGlobalAddressTranslatedLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatGlobalAddressMask = %u\n",
              u4SetValNatGlobalAddressMask);

    pTranslatedLocIpAddrNode = NULL;
    RM_GET_SEQ_NUM (&u4SeqNum);

    i4IfNum = i4NatGlobalAddressInterfaceNum;

    if (i4IfNum <= NAT_MAX_NUM_IF)
    {
        if (gapNatIfTable[i4IfNum] != NULL)
        {
            pIfNode = gapNatIfTable[i4IfNum];
            TMO_SLL_Scan (&(pIfNode->TranslatedLocIpList),
                          pTranslatedLocIpAddrNode, tTranslatedLocIpAddrNode *)
            {
                if (u4NatGlobalAddressTranslatedLocalIp ==
                    pTranslatedLocIpAddrNode->u4TranslatedLocIpAddr)
                {
                    if (pTranslatedLocIpAddrNode->i4RowStatus ==
                        NAT_STATUS_ACTIVE)
                    {

                        /* Entry is ACTIVE, Can not Set */

                        MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT",
                                 "Row is Active, Can not Set\n");
                        return SNMP_FAILURE;
                    }
                    else
                    {

                        /* If Status is not ready, set the mask */

                        if (pTranslatedLocIpAddrNode->i4RowStatus ==
                            NAT_STATUS_NOT_READY)
                        {
                            pTranslatedLocIpAddrNode->u4Mask =
                                u4SetValNatGlobalAddressMask;
                            pTranslatedLocIpAddrNode->i4RowStatus =
                                NAT_STATUS_NOT_IN_SERVICE;
                            MOD_TRC_ARG3 (gu4NatTrc, MGMT_TRC, "NAT",
                                          "Successfully Set the Global Address\
                                          Mask of index \
                                          %d and %d to %d \n", i4IfNum, pTranslatedLocIpAddrNode->u4TranslatedLocIpAddr, u4SetValNatGlobalAddressMask);
                            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                                  NatGlobalAddressMask,
                                                  u4SeqNum, FALSE, NatLock,
                                                  NatUnLock, NAT_TWO,
                                                  SNMP_SUCCESS);
                            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p",
                                              i4NatGlobalAddressInterfaceNum,
                                              u4NatGlobalAddressTranslatedLocalIp,
                                              u4SetValNatGlobalAddressMask));
                            return SNMP_SUCCESS;
                        }
                        else
                        {
                            /* If mask is being modified, update the fields
                             * u4CurrentTranslatedLocIp and u4Range in the NAT
                             * Interface Table
                             */
                            /*
                             * Mask value ,if modified can only be changed to
                             * a value lower than the present value.It can't be
                             * greater than the present value
                             */

                            if (u4SetValNatGlobalAddressMask ==
                                pTranslatedLocIpAddrNode->u4Mask)
                            {

                                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                                      NatGlobalAddressMask,
                                                      u4SeqNum, FALSE, NatLock,
                                                      NatUnLock, NAT_TWO,
                                                      SNMP_SUCCESS);
                                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p",
                                                  i4NatGlobalAddressInterfaceNum,
                                                  u4NatGlobalAddressTranslatedLocalIp,
                                                  u4SetValNatGlobalAddressMask));

                                return SNMP_SUCCESS;
                            }

                            if (u4SetValNatGlobalAddressMask <
                                pTranslatedLocIpAddrNode->u4Mask)
                            {
                                return (NatUpdateMask
                                        (i4NatGlobalAddressInterfaceNum,
                                         u4NatGlobalAddressTranslatedLocalIp,
                                         u4SetValNatGlobalAddressMask));
                            }
                            else
                            {
                                MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT",
                                         "\n Create a new row with the "
                                         "remaining IP address against the "
                                         "existing one \n");
                                return SNMP_FAILURE;
                            }

                        }        /* End of else - modifying the existing row */

                    }            /* End of else - status other than active */

                }                /* End of Setting the mask for an already
                                   existing entry
                                   * whose status is not Active */
            }

        }
        /*
         * If Nat Interface entry not present for this Interface then create
         * one
         */
        if (gapNatIfTable[i4IfNum] == NULL)
        {
            pIfNode = NatAllocateMemory (i4IfNum);
            if (pIfNode == NULL)
            {
                MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT",
                         "Memory Allocation Failed\n");
                return SNMP_FAILURE;
            }
            /* Initialise the Interface Entry */
            NATIFTABLE_INITIALIZE (gapNatIfTable[i4IfNum], NAT_STATUS_INVLD);
        }

        if (NAT_MEM_ALLOCATE
            (NAT_GLOBAL_ADDR_POOL_ID,
             pTranslatedLocIpAddrNode, tTranslatedLocIpAddrNode) == NULL)
        {
            MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT", "Memory Allocation Failed\n");
            return SNMP_FAILURE;
        }

        pTranslatedLocIpAddrNode->u4TranslatedLocIpAddr =
            u4NatGlobalAddressTranslatedLocalIp;
        pTranslatedLocIpAddrNode->u4Mask = u4SetValNatGlobalAddressMask;
        pTranslatedLocIpAddrNode->i4RowStatus = NAT_STATUS_NOT_IN_SERVICE;
        TMO_SLL_Add (&(gapNatIfTable[i4IfNum]->TranslatedLocIpList),
                     &(pTranslatedLocIpAddrNode->TranslatedLocIpAddrNode));
        /* Added by Revati For dynamic configuration of Global Adress in the
         * interface Table
         */
        MOD_TRC_ARG3 (gu4NatTrc, MGMT_TRC, "NAT",
                      "Successfully Set the Global Address Mask\
                      of index %d and %d to %d \n", i4IfNum, pTranslatedLocIpAddrNode->u4TranslatedLocIpAddr, u4SetValNatGlobalAddressMask);
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatGlobalAddressMask, u4SeqNum,
                              FALSE, NatLock, NatUnLock, NAT_TWO, SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p",
                          i4NatGlobalAddressInterfaceNum,
                          u4NatGlobalAddressTranslatedLocalIp,
                          u4SetValNatGlobalAddressMask));

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetNatGlobalAddressEntryStatus
 Input       :  The Indices
                NatGlobalAddressInterfaceNum
                NatGlobalAddressTranslatedLocalIp

                The Object 
                setValNatGlobalAddressEntryStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatGlobalAddressEntryStatus (INT4 i4NatGlobalAddressInterfaceNum,
                                   UINT4 u4NatGlobalAddressTranslatedLocalIp,
                                   INT4 i4SetValNatGlobalAddressEntryStatus)
{
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    INT1                i1RetStatus = NAT_ZERO;
    NAT_DBG1 (NAT_DBG_LLR, "NatGlobalAddressInterfaceNum = %d\n",
              i4NatGlobalAddressInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatGlobalAddressTranslatedLocIp = %u\n",
              u4NatGlobalAddressTranslatedLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatGlobalAddressEntryStatus = %d\n",
              i4SetValNatGlobalAddressEntryStatus);
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (SecUtilValidateIfIndex ((UINT2) i4NatGlobalAddressInterfaceNum) !=
        OSIX_SUCCESS)
    {
#ifdef RM_WANTED
        /* In case of standby node, we can ignore the Nat destroy status
         * syncup failure, because its already destroyed in standby */
        if ((i4SetValNatGlobalAddressEntryStatus == NAT_STATUS_DESTROY)
            && (RmGetNodeState () == RM_STANDBY))
        {
            return SNMP_SUCCESS;
        }
#endif
        return SNMP_FAILURE;
    }

    switch (i4SetValNatGlobalAddressEntryStatus)
    {

        case NAT_STATUS_ACTIVE:
            i1RetStatus =
                NatGatActive (i4NatGlobalAddressInterfaceNum,
                              u4NatGlobalAddressTranslatedLocalIp);
            break;

        case NAT_STATUS_CREATE_AND_GO:
            i1RetStatus =
                NatGatCreateAndGo (i4NatGlobalAddressInterfaceNum,
                                   u4NatGlobalAddressTranslatedLocalIp);
            break;

        case NAT_STATUS_CREATE_AND_WAIT:
            i1RetStatus =
                NatGatCreateAndWait (i4NatGlobalAddressInterfaceNum,
                                     u4NatGlobalAddressTranslatedLocalIp);
            break;

        case NAT_STATUS_NOT_IN_SERVICE:
            i1RetStatus =
                NatGatNotInService (i4NatGlobalAddressInterfaceNum,
                                    u4NatGlobalAddressTranslatedLocalIp);
            break;

        case NAT_STATUS_DESTROY:
            NatDeleteDynamicEntry ((UINT4) i4NatGlobalAddressInterfaceNum);
            i1RetStatus =
                NatGatDestroy (i4NatGlobalAddressInterfaceNum,
                               u4NatGlobalAddressTranslatedLocalIp);
            break;

        default:
            MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT",
                     "\n Unknown Row Status Option \n");
            i1RetStatus = SNMP_FAILURE;
            break;

    }
    if (i1RetStatus == SNMP_SUCCESS)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatLocalAddressEntryStatus,
                              u4SeqNum, TRUE, NatLock, NatUnLock, NAT_TWO,
                              SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                          i4NatGlobalAddressInterfaceNum,
                          u4NatGlobalAddressTranslatedLocalIp,
                          i4SetValNatGlobalAddressEntryStatus));

    }

    return i1RetStatus;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2NatGlobalAddressMask
 Input       :  The Indices
                NatGlobalAddressInterfaceNum
                NatGlobalAddressTranslatedLocalIp

                The Object 
                testValNatGlobalAddressMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatGlobalAddressMask (UINT4 *pu4ErrorCode,
                               INT4 i4NatGlobalAddressInterfaceNum,
                               UINT4 u4NatGlobalAddressTranslatedLocalIp,
                               UINT4 u4TestValNatGlobalAddressMask)
{
    tInterfaceInfo     *pIfNode = NULL;
    tTranslatedLocIpAddrNode *pTranslatedLocIpAddrNode = NULL;
    INT4                i4IfNum = NAT_ZERO;

    NAT_DBG1 (NAT_DBG_LLR, "NatGlobalAddressInterfaceNum = %d\n",
              i4NatGlobalAddressInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatGlobalAddressTranslatedLocIp = %u\n",
              u4NatGlobalAddressTranslatedLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatGlobalAddressMask = %u\n",
              u4TestValNatGlobalAddressMask);
    if ((i4NatGlobalAddressInterfaceNum <= NAT_ZERO)
        || (i4NatGlobalAddressInterfaceNum > NAT_MAX_NUM_IF)
        || (u4NatGlobalAddressTranslatedLocalIp == NAT_INVLD_IP_ADDR_1)
        || (u4NatGlobalAddressTranslatedLocalIp == NAT_INVLD_IP_ADDR_2)
        || (u4TestValNatGlobalAddressMask == NAT_INVLD_MASK))
    {
        NAT_TRC (NAT_TRC_ON, "Exiting with FAILURE: Entry found \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    pTranslatedLocIpAddrNode = NULL;
    i4IfNum = i4NatGlobalAddressInterfaceNum;

    if (i4IfNum <= NAT_MAX_NUM_IF)
    {
        if (gapNatIfTable[i4IfNum] != NULL)
        {
            pIfNode = gapNatIfTable[i4IfNum];
            TMO_SLL_Scan (&(pIfNode->TranslatedLocIpList),
                          pTranslatedLocIpAddrNode, tTranslatedLocIpAddrNode *)
            {
                if (u4NatGlobalAddressTranslatedLocalIp ==
                    pTranslatedLocIpAddrNode->u4TranslatedLocIpAddr)
                {
                    if (pTranslatedLocIpAddrNode->i4RowStatus ==
                        NAT_STATUS_ACTIVE)
                    {
                        /* Entry is ACTIVE, Can not Set */
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        return SNMP_FAILURE;
                    }
                }
            }
        }
    }
    NAT_TRC (NAT_TRC_ON, "Exiting with SUCCESS: Entry found \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2NatGlobalAddressEntryStatus
 Input       :  The Indices
                NatGlobalAddressInterfaceNum
                NatGlobalAddressTranslatedLocalIp

                The Object 
                testValNatGlobalAddressEntryStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatGlobalAddressEntryStatus (UINT4 *pu4ErrorCode,
                                      INT4 i4NatGlobalAddressInterfaceNum,
                                      UINT4
                                      u4NatGlobalAddressTranslatedLocalIp,
                                      INT4 i4TestValNatGlobalAddressEntryStatus)
{
    tInterfaceInfo     *pIfNode = NULL;
    tTranslatedLocIpAddrNode *pTranslatedLocalIpAddrNode = NULL;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    pTranslatedLocalIpAddrNode = NULL;

    if ((i4NatGlobalAddressInterfaceNum < NAT_ZERO) ||
        (i4NatGlobalAddressInterfaceNum > NAT_MAX_NUM_IF))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIfNode = gapNatIfTable[i4NatGlobalAddressInterfaceNum];

    NAT_DBG1 (NAT_DBG_LLR, "NatGlobalAddressInterfaceNum = %d\n",
              i4NatGlobalAddressInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatGlobalAddressTranslatedLocIp = %u\n",
              u4NatGlobalAddressTranslatedLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatGlobalAddressEntryStatus = %d\n",
              i4TestValNatGlobalAddressEntryStatus);

    if ((i4NatGlobalAddressInterfaceNum > NAT_ZERO)
        && (i4NatGlobalAddressInterfaceNum <= NAT_MAX_NUM_IF)
        && (NatValidateIpAddress (u4NatGlobalAddressTranslatedLocalIp,
                                  NAT_GLOBAL) == NAT_SUCCESS))
    {
        switch (i4TestValNatGlobalAddressEntryStatus)
        {
            case NAT_STATUS_ACTIVE:
            case NAT_STATUS_NOT_IN_SERVICE:
            case NAT_STATUS_CREATE_AND_GO:
            case NAT_STATUS_CREATE_AND_WAIT:
                return SNMP_SUCCESS;

            case NAT_STATUS_DESTROY:
                /* Validity Check for presence has to be made */
                if (pIfNode != NULL)
                {
                    /* Check whether the given global ip exists in 
                     * the given interface */
                    TMO_SLL_Scan (&(pIfNode->TranslatedLocIpList),
                                  pTranslatedLocalIpAddrNode,
                                  tTranslatedLocIpAddrNode *)
                    {
                        if (pTranslatedLocalIpAddrNode->u4TranslatedLocIpAddr ==
                            u4NatGlobalAddressTranslatedLocalIp)
                        {
                            MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                                          "The row of Global Address Table "
                                          "of  index %d and %d    exist\n",
                                          i4NatGlobalAddressInterfaceNum,
                                          u4NatGlobalAddressTranslatedLocalIp);

                            NAT_TRC (NAT_TRC_ON,
                                     "Exiting with SUCCESS: Entry found \n");
                            return SNMP_SUCCESS;

                        }
                    }
                }

                break;

            default:
                break;
        }
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Global Address Table of \
                  index %d and %d does not exist\n", i4NatGlobalAddressInterfaceNum, u4NatGlobalAddressTranslatedLocalIp);
    return SNMP_FAILURE;

}

/* LOW LEVEL Routines for Table : NatLocalAddressTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceNatLocalAddressTable
 Input       :  The Indices
                NatLocalAddressInterfaceNumber
                NatLocalAddressLocalIp
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceNatLocalAddressTable (INT4
                                              i4NatLocalAddressInterfaceNumber,
                                              UINT4 u4NatLocalAddressLocalIp)
{
    tLocIpAddrNode     *pLocalNode = NULL;

    NAT_TRC (NAT_TRC_ON,
             "Entering the function: "
             "ValidateIndexInstance for Nat LocalAddressTable\n");
    NAT_DBG1 (NAT_DBG_LLR, "NatLocalAddressInterfaceNumber = %d\n",
              i4NatLocalAddressInterfaceNumber);
    NAT_DBG1 (NAT_DBG_LLR, "NatLocalAddressLocalIp = %u\n",
              u4NatLocalAddressLocalIp);
    pLocalNode = NULL;
    if (SecUtilValidateIfIndex ((UINT2) i4NatLocalAddressInterfaceNumber) ==
        OSIX_SUCCESS)
    {
        TMO_SLL_Scan (&gNatLocIpAddrList, pLocalNode, tLocIpAddrNode *)
        {
            if ((pLocalNode->u4InterfaceNumber ==
                 (UINT4) (i4NatLocalAddressInterfaceNumber)) &&
                (pLocalNode->u4LocIpNum == u4NatLocalAddressLocalIp))
            {
                NAT_TRC (NAT_TRC_ON, "Exiting with SUCCESS: Entry found \n");
                return SNMP_SUCCESS;
            }
        }

    }
    MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Local Address Table of \
                  index %d and %d does not exist\n", i4NatLocalAddressInterfaceNumber, u4NatLocalAddressLocalIp);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexNatLocalAddressTable
 Input       :  The Indices
                NatLocalAddressInterfaceNumber
                NatLocalAddressLocalIp
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexNatLocalAddressTable (INT4 *pi4NatLocalAddressInterfaceNumber,
                                      UINT4 *pu4NatLocalAddressLocalIp)
{

    tLocIpAddrNode     *pLocalNode = NULL;

    pLocalNode = NULL;
    pLocalNode = (tLocIpAddrNode *) TMO_SLL_First (&gNatLocIpAddrList);
    if (pLocalNode != NULL)
    {
        *pi4NatLocalAddressInterfaceNumber =
            (INT4) pLocalNode->u4InterfaceNumber;
        *pu4NatLocalAddressLocalIp = pLocalNode->u4LocIpNum;
        NAT_DBG1 (NAT_DBG_LLR, "NatLocalAddressInterfaceNumber = %d\n",
                  *pi4NatLocalAddressInterfaceNumber);
        NAT_DBG1 (NAT_DBG_LLR, "NatLocalAddressLocalIp = %u\n",
                  *pu4NatLocalAddressLocalIp);
        return SNMP_SUCCESS;
    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexNatLocalAddressTable
 Input       :  The Indices
                NatLocalAddressInterfaceNumber
                nextNatLocalAddressInterfaceNumber
                NatLocalAddressLocalIp
                nextNatLocalAddressLocalIp
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexNatLocalAddressTable (INT4 i4NatLocalAddressInterfaceNumber,
                                     INT4
                                     *pi4NextNatLocalAddressInterfaceNumber,
                                     UINT4 u4NatLocalAddressLocalIp,
                                     UINT4 *pu4NextNatLocalAddressLocalIp)
{
    tLocIpAddrNode     *pLocalNode = NULL;

    NAT_DBG1 (NAT_DBG_LLR, "NatLocalAddressInterfaceNumber = %d\n",
              i4NatLocalAddressInterfaceNumber);
    NAT_DBG1 (NAT_DBG_LLR, "NatLocalAddressLocalIp = %u\n",
              u4NatLocalAddressLocalIp);

    pLocalNode = NULL;
    TMO_SLL_Scan (&gNatLocIpAddrList, pLocalNode, tLocIpAddrNode *)
    {
        if ((pLocalNode->u4LocIpNum == u4NatLocalAddressLocalIp) &&
            (pLocalNode->u4InterfaceNumber ==
             (UINT4) (i4NatLocalAddressInterfaceNumber)))
        {
            pLocalNode =
                (tLocIpAddrNode *) TMO_SLL_Next (&gNatLocIpAddrList,
                                                 &(pLocalNode->LocIpAddrNode));

            if (pLocalNode != NULL)
            {
                *pu4NextNatLocalAddressLocalIp = pLocalNode->u4LocIpNum;
                *pi4NextNatLocalAddressInterfaceNumber =
                    (INT4) pLocalNode->u4InterfaceNumber;
                NAT_DBG1 (NAT_DBG_LLR,
                          "NatNextLocalAddressInterfaceNumber = %d\n",
                          *pi4NextNatLocalAddressInterfaceNumber);
                NAT_DBG1 (NAT_DBG_LLR, "NatNextLocalAddressLocalIp = %u\n",
                          *pu4NextNatLocalAddressLocalIp);
                return SNMP_SUCCESS;
            }
            else
                break;
        }
    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetNatLocalAddressMask
 Input       :  The Indices
                NatLocalAddressInterfaceNumber
                NatLocalAddressLocalIp

                The Object 
                retValNatLocalAddressMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatLocalAddressMask (INT4 i4NatLocalAddressInterfaceNumber,
                           UINT4 u4NatLocalAddressLocalIp,
                           UINT4 *pu4RetValNatLocalAddressMask)
{
    tLocIpAddrNode     *pLocalNode = NULL;

    NAT_DBG1 (NAT_DBG_LLR, "NatLocalAddressInterfaceNumber = %d\n",
              i4NatLocalAddressInterfaceNumber);
    NAT_DBG1 (NAT_DBG_LLR, "NatLocalAddressLocalIp = %u\n",
              u4NatLocalAddressLocalIp);

    pLocalNode = NULL;

    TMO_SLL_Scan (&gNatLocIpAddrList, pLocalNode, tLocIpAddrNode *)
    {
        if ((pLocalNode->u4LocIpNum == u4NatLocalAddressLocalIp) &&
            (pLocalNode->u4InterfaceNumber ==
             (UINT4) (i4NatLocalAddressInterfaceNumber)))
        {
            *pu4RetValNatLocalAddressMask = pLocalNode->u4Mask;
            NAT_DBG1 (NAT_DBG_LLR, "NatLocalAddressLocalAddressMask = %u\n",
                      *pu4RetValNatLocalAddressMask);
            return SNMP_SUCCESS;
        }
    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNatLocalAddressEntryStatus
 Input       :  The Indices
                NatLocalAddressInterfaceNumber
                NatLocalAddressLocalIp

                The Object 
                retValNatLocalAddressEntryStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatLocalAddressEntryStatus (INT4 i4NatLocalAddressInterfaceNumber,
                                  UINT4 u4NatLocalAddressLocalIp,
                                  INT4 *pi4RetValNatLocalAddressEntryStatus)
{

    tLocIpAddrNode     *pLocalNode = NULL;

    NAT_DBG1 (NAT_DBG_LLR, "NatLocalAddressInterfaceNumber = %d\n",
              i4NatLocalAddressInterfaceNumber);
    NAT_DBG1 (NAT_DBG_LLR, "NatLocalAddressLocalIp = %u\n",
              u4NatLocalAddressLocalIp);

    pLocalNode = NULL;
    TMO_SLL_Scan (&gNatLocIpAddrList, pLocalNode, tLocIpAddrNode *)
    {
        if ((pLocalNode->u4LocIpNum == u4NatLocalAddressLocalIp) &&
            (pLocalNode->u4InterfaceNumber ==
             (UINT4) (i4NatLocalAddressInterfaceNumber)))
        {
            *pi4RetValNatLocalAddressEntryStatus = pLocalNode->i4RowStatus;
            NAT_DBG1 (NAT_DBG_LLR, "NatLocalAddressEntryStatus = %d\n",
                      *pi4RetValNatLocalAddressEntryStatus);
            return SNMP_SUCCESS;
        }
    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetNatLocalAddressMask
 Input       :  The Indices
                NatLocalAddressInterfaceNumber
                NatLocalAddressLocalIp

                The Object 
                setValNatLocalAddressMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatLocalAddressMask (INT4 i4NatLocalAddressInterfaceNumber,
                           UINT4 u4NatLocalAddressLocalIp,
                           UINT4 u4SetValNatLocalAddressMask)
{
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tLocIpAddrNode     *pIfNode = NULL;

    NAT_DBG1 (NAT_DBG_LLR, "NatLocalAddressInterfaceNumber = %d\n",
              i4NatLocalAddressInterfaceNumber);
    NAT_DBG1 (NAT_DBG_LLR, "NatLocalAddressLocalIp = %u\n",
              u4NatLocalAddressLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatLocalAddressMask = %u\n",
              u4SetValNatLocalAddressMask);

    RM_GET_SEQ_NUM (&u4SeqNum);

    if ((i4NatLocalAddressInterfaceNumber > NAT_ZERO)
        && (i4NatLocalAddressInterfaceNumber <= NAT_MAX_NUM_IF))
    {

        TMO_SLL_Scan (&gNatLocIpAddrList, pIfNode, tLocIpAddrNode *)
        {

            if ((pIfNode->u4InterfaceNumber ==
                 (UINT4) (i4NatLocalAddressInterfaceNumber))
                && (pIfNode->u4LocIpNum == u4NatLocalAddressLocalIp))
            {
                if (pIfNode->i4RowStatus == NAT_STATUS_ACTIVE)
                {

                    /* Can not set the mask , Entry is ACTIVE */

                    MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT",
                             "\nRow is Active, Can not Set\n");
                    return SNMP_FAILURE;
                }
                else
                {
                    pIfNode->u4Mask = u4SetValNatLocalAddressMask;
                    pIfNode->i4RowStatus = NAT_STATUS_NOT_IN_SERVICE;
                    NAT_TRC (NAT_TRC_ON, "Successfully Set the Value\n");
                    MOD_TRC_ARG3 (gu4NatTrc, MGMT_TRC, "NAT",
                                  "Successfully Set the Local Address Mask "
                                  "of index %d and %d to %d \n",
                                  pIfNode->u4InterfaceNumber,
                                  pIfNode->u4LocIpNum,
                                  u4SetValNatLocalAddressMask);
                    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatLocalAddressMask,
                                          u4SeqNum, FALSE, NatLock, NatUnLock,
                                          NAT_TWO, SNMP_SUCCESS);
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p",
                                      i4NatLocalAddressInterfaceNumber,
                                      u4NatLocalAddressLocalIp,
                                      u4SetValNatLocalAddressMask));

                    return SNMP_SUCCESS;
                }
            }
        }

        /*Create a new entry in the LLR list */

        if (NAT_MEM_ALLOCATE
            (NAT_LOCAL_ADDR_POOL_ID, pIfNode, tLocIpAddrNode) == NULL)
        {
            MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT", "Memory Allocation Failed\n");
            return SNMP_FAILURE;
        }

        pIfNode->u4InterfaceNumber = (UINT4) i4NatLocalAddressInterfaceNumber;
        pIfNode->u4LocIpNum = u4NatLocalAddressLocalIp;
        pIfNode->u4Mask = u4SetValNatLocalAddressMask;
        pIfNode->i4RowStatus = NAT_STATUS_NOT_IN_SERVICE;
        TMO_SLL_Add (&gNatLocIpAddrList, &(pIfNode->LocIpAddrNode));
        MOD_TRC_ARG3 (gu4NatTrc, MGMT_TRC, "NAT", "Successfully Set the Local\
                      Address Mask of index %d and %d to %d \n", pIfNode->u4InterfaceNumber, pIfNode->u4LocIpNum, u4SetValNatLocalAddressMask);
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatLocalAddressMask, u4SeqNum,
                              FALSE, NatLock, NatUnLock, NAT_TWO, SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p",
                          i4NatLocalAddressInterfaceNumber,
                          u4NatLocalAddressLocalIp,
                          u4SetValNatLocalAddressMask));
        return SNMP_SUCCESS;

    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetNatLocalAddressEntryStatus
 Input       :  The Indices
                NatLocalAddressInterfaceNumber
                NatLocalAddressLocalIp

                The Object 
                setValNatLocalAddressEntryStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatLocalAddressEntryStatus (INT4 i4NatLocalAddressInterfaceNumber,
                                  UINT4 u4NatLocalAddressLocalIp,
                                  INT4 i4SetValNatLocalAddressEntryStatus)
{

    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    INT1                i1RetStatus = NAT_ZERO;

    NAT_DBG1 (NAT_DBG_LLR, "NatLocalAddressInterfaceNumber = %d\n",
              i4NatLocalAddressInterfaceNumber);
    NAT_DBG1 (NAT_DBG_LLR, "NatLocalAddressLocalIp = %u\n",
              u4NatLocalAddressLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatLocalAddressEntryStatus = %d\n",
              i4SetValNatLocalAddressEntryStatus);

    RM_GET_SEQ_NUM (&u4SeqNum);

    switch (i4SetValNatLocalAddressEntryStatus)
    {

        case NAT_STATUS_ACTIVE:

            i1RetStatus =
                NatLatActive (i4NatLocalAddressInterfaceNumber,
                              u4NatLocalAddressLocalIp);
            break;

        case NAT_STATUS_CREATE_AND_GO:

            i1RetStatus =
                NatLatCreateAndGo (i4NatLocalAddressInterfaceNumber,
                                   u4NatLocalAddressLocalIp);
            break;

        case NAT_STATUS_CREATE_AND_WAIT:

            i1RetStatus =
                NatLatCreateAndWait (i4NatLocalAddressInterfaceNumber,
                                     u4NatLocalAddressLocalIp);
            break;

        case NAT_STATUS_NOT_IN_SERVICE:
            i1RetStatus =
                NatLatNotInService (i4NatLocalAddressInterfaceNumber,
                                    u4NatLocalAddressLocalIp);
            break;

        case NAT_STATUS_DESTROY:
            i1RetStatus =
                NatLatDestroy (i4NatLocalAddressInterfaceNumber,
                               u4NatLocalAddressLocalIp);
            break;

        default:
            MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT",
                     "\n Unknown Row Status Option \n");
            i1RetStatus = SNMP_FAILURE;
            break;

    }
    if (i1RetStatus == SNMP_SUCCESS)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatLocalAddressEntryStatus,
                              u4SeqNum, TRUE, NatLock, NatUnLock, NAT_TWO,
                              SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                          i4NatLocalAddressInterfaceNumber,
                          u4NatLocalAddressLocalIp,
                          i4SetValNatLocalAddressEntryStatus));

    }
    return i1RetStatus;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2NatLocalAddressMask
 Input       :  The Indices
                NatLocalAddressInterfaceNumber
                NatLocalAddressLocalIp

                The Object 
                testValNatLocalAddressMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatLocalAddressMask (UINT4 *pu4ErrorCode,
                              INT4 i4NatLocalAddressInterfaceNumber,
                              UINT4 u4NatLocalAddressLocalIp,
                              UINT4 u4TestValNatLocalAddressMask)
{
    NAT_DBG1 (NAT_DBG_LLR, "NatLocalAddressInterfaceNumber = %d\n",
              i4NatLocalAddressInterfaceNumber);
    NAT_DBG1 (NAT_DBG_LLR, "NatLocalAddressLocalIp = %u\n",
              u4NatLocalAddressLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatLocalAddressMask = %u\n",
              u4TestValNatLocalAddressMask);
    if ((i4NatLocalAddressInterfaceNumber > NAT_ZERO)
        && (i4NatLocalAddressInterfaceNumber <= NAT_MAX_NUM_IF)
        && (u4NatLocalAddressLocalIp != NAT_INVLD_IP_ADDR_1)
        && (u4NatLocalAddressLocalIp != NAT_INVLD_IP_ADDR_2))
    {

        if (u4TestValNatLocalAddressMask == NAT_INVLD_MASK)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                          "The illegal mask in Local\
                          Address Table of  \
                          index %d and %d                               \n", i4NatLocalAddressInterfaceNumber, u4NatLocalAddressLocalIp);
            return SNMP_FAILURE;
        }

        MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                      "The row of Local Address Table "
                      "of index %d and %d exist\n",
                      i4NatLocalAddressInterfaceNumber,
                      u4NatLocalAddressLocalIp);
        NAT_TRC (NAT_TRC_ON, "Exiting with SUCCESS: Entry found \n");
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }

    MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Local Address Table "
                  "of index %d and %d does not exist\n",
                  i4NatLocalAddressInterfaceNumber, u4NatLocalAddressLocalIp);
    NAT_TRC (NAT_TRC_ON, "No Such Entry exists Test Failed\n");
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2NatLocalAddressEntryStatus
 Input       :  The Indices
                NatLocalAddressInterfaceNumber
                NatLocalAddressLocalIp

                The Object 
                testValNatLocalAddressEntryStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatLocalAddressEntryStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4NatLocalAddressInterfaceNumber,
                                     UINT4 u4NatLocalAddressLocalIp,
                                     INT4 i4TestValNatLocalAddressEntryStatus)
{

    NAT_DBG1 (NAT_DBG_LLR, "NatLocalAddressInterfaceNumber = %d\n",
              i4NatLocalAddressInterfaceNumber);
    NAT_DBG1 (NAT_DBG_LLR, "NatLocalAddressLocalIp = %u\n",
              u4NatLocalAddressLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatLocalAddressEntryStatus = %d\n",
              i4TestValNatLocalAddressEntryStatus);
    if ((i4NatLocalAddressInterfaceNumber > NAT_ZERO)
        && (i4NatLocalAddressInterfaceNumber <= NAT_MAX_NUM_IF)
        && (NatValidateIpAddress (u4NatLocalAddressLocalIp, NAT_LOCAL)
            == NAT_SUCCESS))
    {
        switch (i4TestValNatLocalAddressEntryStatus)
        {

            case NAT_STATUS_ACTIVE:

            case NAT_STATUS_NOT_IN_SERVICE:

            case NAT_STATUS_CREATE_AND_GO:

            case NAT_STATUS_CREATE_AND_WAIT:

            case NAT_STATUS_DESTROY:
                MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                              "The row of Local Address Table "
                              "of  index %d and %d  exist\n",
                              i4NatLocalAddressInterfaceNumber,
                              u4NatLocalAddressLocalIp);
                NAT_TRC (NAT_TRC_ON, "Exiting with SUCCESS: Entry found \n");
                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                return SNMP_SUCCESS;
            default:
                break;
        }
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Local Address Table of \
                  index %d and %d does not exist\n", i4NatLocalAddressInterfaceNumber, u4NatLocalAddressLocalIp);
    return SNMP_FAILURE;

}

/* LOW LEVEL Routines for Table : NatStaticTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceNatStaticTable
 Input       :  The Indices
                NatStaticInterfaceNum
                NatStaticLocalIp
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceNatStaticTable (INT4 i4NatStaticInterfaceNum,
                                        UINT4 u4NatStaticLocalIp)
{
    tInterfaceInfo     *pIfNode = NULL;
    tStaticEntryNode   *pStaticEntryNode = NULL;

    NAT_TRC (NAT_TRC_ON,
             "Entering the function: "
             "ValidateIndexInstance for Nat StaticTable\n");
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticInterfaceNum = %d\n",
              i4NatStaticInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticLocalIp = %u\n", u4NatStaticLocalIp);

    pStaticEntryNode = NULL;

    if ((i4NatStaticInterfaceNum < NAT_ZERO) ||
        (i4NatStaticInterfaceNum > NAT_MAX_NUM_IF))
    {
        return SNMP_FAILURE;
    }

    if (SecUtilValidateIfIndex ((UINT2) i4NatStaticInterfaceNum) ==
        OSIX_SUCCESS)
    {
        if (gapNatIfTable[i4NatStaticInterfaceNum] != NULL)
        {
            pIfNode = gapNatIfTable[i4NatStaticInterfaceNum];
            TMO_SLL_Scan (&pIfNode->StaticList, pStaticEntryNode,
                          tStaticEntryNode *)
            {
                if (pStaticEntryNode->u4LocIpAddr == u4NatStaticLocalIp)
                {
                    MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                                  "The row of Static Address Table "
                                  "of  index %d and %d    exist\n",
                                  i4NatStaticInterfaceNum, u4NatStaticLocalIp);
                    NAT_TRC (NAT_TRC_ON,
                             "Exiting with SUCCESS: Entry found \n");
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    NAT_TRC (NAT_TRC_ON, "No Such Entry exists Test Failed\n");
    MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Static Address Table of \
                  index %d and %d does not exist\n", i4NatStaticInterfaceNum, u4NatStaticLocalIp);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexNatStaticTable
 Input       :  The Indices
                NatStaticInterfaceNum
                NatStaticLocalIp
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexNatStaticTable (INT4 *pi4NatStaticInterfaceNum,
                                UINT4 *pu4NatStaticLocalIp)
{
    tInterfaceInfo     *pIfNode = NULL;
    tStaticEntryNode   *pStaticEntryNode = NULL;
    INT4                i4IfNum = NAT_ZERO;

    pStaticEntryNode = NULL;
    i4IfNum = NAT_ONE;
    while (i4IfNum <= NAT_MAX_NUM_IF)
    {
        if (gapNatIfTable[i4IfNum] != NULL)
        {
            pIfNode = gapNatIfTable[i4IfNum];
            pStaticEntryNode =
                (tStaticEntryNode *) TMO_SLL_First (&pIfNode->StaticList);
            if (pStaticEntryNode != NULL)
            {
                *pi4NatStaticInterfaceNum = i4IfNum;
                *pu4NatStaticLocalIp = pStaticEntryNode->u4LocIpAddr;
                NAT_DBG1 (NAT_DBG_LLR, "NatStaticInterfaceNum = %d\n",
                          *pi4NatStaticInterfaceNum);
                NAT_DBG1 (NAT_DBG_LLR, "NatStaticLocalIp = %u\n",
                          *pu4NatStaticLocalIp);
                return SNMP_SUCCESS;
            }

        }
        i4IfNum++;
    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexNatStaticTable
 Input       :  The Indices
                NatStaticInterfaceNum
                nextNatStaticInterfaceNum
                NatStaticLocalIp
                nextNatStaticLocalIp
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexNatStaticTable (INT4 i4NatStaticInterfaceNum,
                               INT4 *pi4NextNatStaticInterfaceNum,
                               UINT4 u4NatStaticLocalIp,
                               UINT4 *pu4NextNatStaticLocalIp)
{
    tInterfaceInfo     *pIfNode = NULL;
    tStaticEntryNode   *pStaticEntryNode = NULL;
    INT4                i4IfNum = NAT_ZERO;

    NAT_DBG1 (NAT_DBG_LLR, "NatStaticInterfaceNum = %d\n",
              i4NatStaticInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticLocalIp = %u\n", u4NatStaticLocalIp);

    pStaticEntryNode = NULL;

    /* Search for the next static ip address in the same interface */

    i4IfNum = i4NatStaticInterfaceNum;
    if (i4IfNum <= NAT_MAX_NUM_IF)
    {
        pIfNode = gapNatIfTable[i4IfNum];
        if (pIfNode != NULL)
        {
            TMO_SLL_Scan (&pIfNode->StaticList, pStaticEntryNode,
                          tStaticEntryNode *)
            {
                if (pStaticEntryNode->u4LocIpAddr == u4NatStaticLocalIp)
                {
                    pStaticEntryNode =
                        (tStaticEntryNode *) TMO_SLL_Next ((tTMO_SLL *) &
                                                           pIfNode->StaticList,
                                                           &(pStaticEntryNode->
                                                             StaticTable));
                    if (pStaticEntryNode != NULL)
                    {
                        *pi4NextNatStaticInterfaceNum = i4IfNum;
                        *pu4NextNatStaticLocalIp =
                            pStaticEntryNode->u4LocIpAddr;
                        NAT_DBG1 (NAT_DBG_LLR,
                                  "NatNextStaticInterfaceNum = %d\n",
                                  *pi4NextNatStaticInterfaceNum);
                        NAT_DBG1 (NAT_DBG_LLR, "NatNextStaticLocalIp = %u\n",
                                  *pu4NextNatStaticLocalIp);
                        return SNMP_SUCCESS;
                    }
                    else
                    {
                        break;
                    }
                }

            }
        }
    }
    else
    {
        return SNMP_FAILURE;
    }
    if (i4NatStaticInterfaceNum > NAT_MAX_NUM_IF)
    {
        NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
        return SNMP_FAILURE;
    }

    i4IfNum = i4NatStaticInterfaceNum + NAT_ONE;

    while (i4IfNum <= NAT_MAX_NUM_IF)
    {
        if (gapNatIfTable[i4IfNum] != NULL)
        {
            pIfNode = gapNatIfTable[i4IfNum];
            pStaticEntryNode =
                (tStaticEntryNode *) TMO_SLL_First (&pIfNode->StaticList);
            if (pStaticEntryNode != NULL)
            {
                *pi4NextNatStaticInterfaceNum = i4IfNum;
                *pu4NextNatStaticLocalIp = pStaticEntryNode->u4LocIpAddr;
                NAT_DBG1 (NAT_DBG_LLR, "NatNextStaticInterfaceNum = %d\n",
                          *pi4NextNatStaticInterfaceNum);
                NAT_DBG1 (NAT_DBG_LLR, "NatNextStaticLocalIp = %u\n",
                          *pu4NextNatStaticLocalIp);
                return SNMP_SUCCESS;
            }
        }
        i4IfNum++;
    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetNatStaticTranslatedLocalIp
 Input       :  The Indices
                NatStaticInterfaceNum
                NatStaticLocalIp

                The Object 
                retValNatStaticTranslatedLocalIp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatStaticTranslatedLocalIp (INT4 i4NatStaticInterfaceNum,
                                  UINT4 u4NatStaticLocalIp,
                                  UINT4 *pu4RetValNatStaticTranslatedLocalIp)
{

    tInterfaceInfo     *pIfNode = NULL;
    tStaticEntryNode   *pStaticEntryNode = NULL;
    INT4                i4IfNum = NAT_ZERO;

    NAT_DBG1 (NAT_DBG_LLR, "NatStaticInterfaceNum = %d\n",
              i4NatStaticInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticLocalIp = %u\n", u4NatStaticLocalIp);

    pStaticEntryNode = NULL;

    /* Search for the next static ip address in the same interface */
    i4IfNum = i4NatStaticInterfaceNum;
    if (i4IfNum <= NAT_MAX_NUM_IF)
    {
        pIfNode = gapNatIfTable[i4IfNum];

        if (pIfNode != NULL)
        {
            TMO_SLL_Scan (&pIfNode->StaticList, pStaticEntryNode,
                          tStaticEntryNode *)
            {
                if (pStaticEntryNode->u4LocIpAddr == u4NatStaticLocalIp)
                {
                    *pu4RetValNatStaticTranslatedLocalIp =
                        pStaticEntryNode->u4TranslatedLocIpAddr;
                    NAT_DBG1 (NAT_DBG_LLR, "NatStaticTranslatedLocIp = %u\n",
                              *pu4RetValNatStaticTranslatedLocalIp);
                    return SNMP_SUCCESS;

                }
            }
        }

    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNatStaticEntryStatus
 Input       :  The Indices
                NatStaticInterfaceNum
                NatStaticLocalIp

                The Object 
                retValNatStaticEntryStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatStaticEntryStatus (INT4 i4NatStaticInterfaceNum,
                            UINT4 u4NatStaticLocalIp,
                            INT4 *pi4RetValNatStaticEntryStatus)
{

    tInterfaceInfo     *pIfNode = NULL;
    tStaticEntryNode   *pStaticEntryNode = NULL;
    INT4                i4IfNum = NAT_ZERO;

    NAT_DBG1 (NAT_DBG_LLR, "NatStaticInterfaceNum = %d\n",
              i4NatStaticInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticLocalIp = %u\n", u4NatStaticLocalIp);

    pStaticEntryNode = NULL;

    i4IfNum = i4NatStaticInterfaceNum;
    if (i4IfNum <= NAT_MAX_NUM_IF)
    {
        if (gapNatIfTable[i4IfNum] != NULL)
        {
            pIfNode = gapNatIfTable[i4IfNum];
            TMO_SLL_Scan (&pIfNode->StaticList, pStaticEntryNode,
                          tStaticEntryNode *)
            {
                if (u4NatStaticLocalIp == pStaticEntryNode->u4LocIpAddr)
                {
                    *pi4RetValNatStaticEntryStatus =
                        pStaticEntryNode->i4RowStatus;
                    NAT_DBG1 (NAT_DBG_LLR, "NatStaticEntryStatus = %d\n",
                              *pi4RetValNatStaticEntryStatus);
                    return SNMP_SUCCESS;
                }
            }
        }

    }
    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetNatStaticTranslatedLocalIp
 Input       :  The Indices
                NatStaticInterfaceNum
                NatStaticLocalIp

                The Object 
                setValNatStaticTranslatedLocalIp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatStaticTranslatedLocalIp (INT4 i4NatStaticInterfaceNum,
                                  UINT4 u4NatStaticLocalIp,
                                  UINT4 u4SetValNatStaticTranslatedLocalIp)
{
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    tInterfaceInfo     *pIfNode = NULL;
    tStaticEntryNode   *pStaticEntryNode = NULL;
    INT4                i4IfNum = NAT_ZERO;

    NAT_DBG1 (NAT_DBG_LLR, "NatStaticInterfaceNum = %d\n",
              i4NatStaticInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticLocalIp = %u\n", u4NatStaticLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticTranslatedLocIp = %u\n",
              u4SetValNatStaticTranslatedLocalIp);

    RM_GET_SEQ_NUM (&u4SeqNum);

    i4IfNum = i4NatStaticInterfaceNum;
    pIfNode = gapNatIfTable[i4IfNum];

    if (i4IfNum <= NAT_MAX_NUM_IF)
    {
        if (gapNatIfTable[i4IfNum] != NULL)
        {
            TMO_SLL_Scan (&pIfNode->StaticList, pStaticEntryNode,
                          tStaticEntryNode *)
            {
                if (u4NatStaticLocalIp == pStaticEntryNode->u4LocIpAddr)
                {
                    if (pStaticEntryNode->i4RowStatus == NAT_STATUS_ACTIVE)
                    {
                        /* Entry is active , can not set the value */

                        MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT",
                                 "Row is Active, Can not Set\n");
                        return SNMP_FAILURE;
                    }
                    else
                    {
                        pStaticEntryNode->u4TranslatedLocIpAddr =
                            u4SetValNatStaticTranslatedLocalIp;
                        pStaticEntryNode->i4RowStatus =
                            NAT_STATUS_NOT_IN_SERVICE;
                        MOD_TRC_ARG3 (gu4NatTrc, MGMT_TRC, "NAT",
                                      "Successfully Set the \
                                      Static Global IP Address\
                                      of index %d and %d to %d \n", i4IfNum, pStaticEntryNode->u4LocIpAddr, u4SetValNatStaticTranslatedLocalIp);
                        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                              NatStaticTranslatedLocalIp,
                                              u4SeqNum, FALSE, NatLock,
                                              NatUnLock, NAT_TWO, SNMP_SUCCESS);
                        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p",
                                          i4NatStaticInterfaceNum,
                                          u4NatStaticLocalIp,
                                          u4SetValNatStaticTranslatedLocalIp));

                        return SNMP_SUCCESS;
                    }
                }

            }
        }

        /*If entry doesn't already exist, create */

        if (gapNatIfTable[i4IfNum] == NULL)
        {
            pIfNode = NatAllocateMemory (i4IfNum);
            if (pIfNode == NULL)
            {
                MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT",
                         "Memory Allocation Failed\n");
                return SNMP_FAILURE;
            }
            NATIFTABLE_INITIALIZE (gapNatIfTable[i4IfNum], NAT_STATUS_INVLD);
        }

        if (NAT_MEM_ALLOCATE
            (NAT_STATIC_POOL_ID, pStaticEntryNode, tStaticEntryNode) == NULL)
        {
            MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT", "Memory Allocation Failed\n");
            return SNMP_FAILURE;
        }

        TMO_SLL_Init_Node (&pStaticEntryNode->StaticTable);
        pStaticEntryNode->u4LocIpAddr = u4NatStaticLocalIp;
        pStaticEntryNode->u4TranslatedLocIpAddr =
            u4SetValNatStaticTranslatedLocalIp;
        pStaticEntryNode->i4RowStatus = NAT_STATUS_NOT_IN_SERVICE;
        TMO_SLL_Add (&gapNatIfTable[i4IfNum]->StaticList,
                     &(pStaticEntryNode->StaticTable));
        NAT_TRC (NAT_TRC_ON, "Successfully Set the Value\n");
        MOD_TRC_ARG3 (gu4NatTrc, MGMT_TRC, "NAT",
                      "Successfully Set the Static Global IP Address "
                      "of index %d and %d to %d \n",
                      i4IfNum, pStaticEntryNode->u4LocIpAddr,
                      u4SetValNatStaticTranslatedLocalIp);
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatStaticTranslatedLocalIp,
                              u4SeqNum, FALSE, NatLock, NatUnLock, NAT_TWO,
                              SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p", i4NatStaticInterfaceNum,
                          u4NatStaticLocalIp,
                          u4SetValNatStaticTranslatedLocalIp));
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetNatStaticEntryStatus
 Input       :  The Indices
                NatStaticInterfaceNum
                NatStaticLocalIp

                The Object 
                setValNatStaticEntryStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatStaticEntryStatus (INT4 i4NatStaticInterfaceNum,
                            UINT4 u4NatStaticLocalIp,
                            INT4 i4SetValNatStaticEntryStatus)
{

    INT1                i1RetStatus = NAT_ZERO;
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    NAT_DBG1 (NAT_DBG_LLR, "NatStaticInterfaceNum = %d\n",
              i4NatStaticInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticLocalIp = %u\n", u4NatStaticLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticEntryStatus = %d\n",
              i4SetValNatStaticEntryStatus);
    RM_GET_SEQ_NUM (&u4SeqNum);

    switch (i4SetValNatStaticEntryStatus)
    {
        case NAT_STATUS_ACTIVE:
            i1RetStatus =
                NatSttActive (i4NatStaticInterfaceNum, u4NatStaticLocalIp);
            break;

        case NAT_STATUS_CREATE_AND_GO:
            i1RetStatus =
                NatSttCreateAndGo (i4NatStaticInterfaceNum, u4NatStaticLocalIp);
            break;

        case NAT_STATUS_CREATE_AND_WAIT:
            i1RetStatus =
                NatSttCreateAndWait (i4NatStaticInterfaceNum,
                                     u4NatStaticLocalIp);
            break;

        case NAT_STATUS_NOT_IN_SERVICE:
            i1RetStatus =
                NatSttNotInService (i4NatStaticInterfaceNum,
                                    u4NatStaticLocalIp);
            break;
        case NAT_STATUS_DESTROY:
            i1RetStatus =
                NatSttDestroy (i4NatStaticInterfaceNum, u4NatStaticLocalIp);
            break;

        default:
            MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT",
                     "\n Unknown Row Status Option \n");
            i1RetStatus = SNMP_FAILURE;
            break;

    }
    if (i1RetStatus == SNMP_SUCCESS)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatStaticEntryStatus, u4SeqNum,
                              TRUE, NatLock, NatUnLock, NAT_TWO, SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i", i4NatStaticInterfaceNum,
                          u4NatStaticLocalIp, i4SetValNatStaticEntryStatus));

    }
    return i1RetStatus;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2NatStaticTranslatedLocalIp
 Input       :  The Indices
                NatStaticInterfaceNum
                NatStaticLocalIp

                The Object 
                testValNatStaticTranslatedLocalIp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatStaticTranslatedLocalIp (UINT4 *pu4ErrorCode,
                                     INT4 i4NatStaticInterfaceNum,
                                     UINT4 u4NatStaticLocalIp,
                                     UINT4 u4TestValNatStaticTranslatedLocalIp)
{
    tInterfaceInfo     *pIfNode = NULL;
    tStaticEntryNode   *pStaticEntryNode = NULL;
    tStaticNaptEntry   *pStaticNaptEntryNode = NULL;
    INT4                i4IfNum = NAT_ZERO;

    NAT_DBG1 (NAT_DBG_LLR, "NatStaticInterfaceNum = %d\n",
              i4NatStaticInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticLocalIp = %u\n", u4NatStaticLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticTranslatedLocIp = %u\n",
              u4TestValNatStaticTranslatedLocalIp);

    if ((NatValidateIpAddress (u4TestValNatStaticTranslatedLocalIp,
                               NAT_GLOBAL) == NAT_FAILURE)
        || (u4NatStaticLocalIp == u4TestValNatStaticTranslatedLocalIp)
        || (u4TestValNatStaticTranslatedLocalIp == NAT_ZERO))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                      "The illegal Global Address in statis Table "
                      "of  index %d and %d \n",
                      i4NatStaticInterfaceNum, u4NatStaticLocalIp);
        return SNMP_FAILURE;
    }

    if ((i4NatStaticInterfaceNum > NAT_ZERO)
        && (i4NatStaticInterfaceNum <= NAT_MAX_NUM_IF))
    {
        pStaticEntryNode = NULL;
        i4IfNum = i4NatStaticInterfaceNum;

        if (i4IfNum <= NAT_MAX_NUM_IF)
        {
            if (gapNatIfTable[i4IfNum] != NULL)
            {
                /* Check whether Translated ip is already in static table */
                pIfNode = gapNatIfTable[i4IfNum];
                TMO_SLL_Scan (&pIfNode->StaticList,
                              pStaticEntryNode, tStaticEntryNode *)
                {
                    if ((u4TestValNatStaticTranslatedLocalIp ==
                         pStaticEntryNode->u4TranslatedLocIpAddr) &&
                        (pStaticEntryNode->u4LocIpAddr != u4NatStaticLocalIp))
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                                      "The Global Address Already exists "
                                      "in static Table "
                                      "of  index %d and %d \n",
                                      i4NatStaticInterfaceNum,
                                      u4NatStaticLocalIp);
                        return SNMP_FAILURE;

                    }
                    if (u4NatStaticLocalIp == pStaticEntryNode->u4LocIpAddr)
                    {
                        if (pStaticEntryNode->i4RowStatus == NAT_STATUS_ACTIVE)
                        {
                            /* Entry is active , can not set the value */
                            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                            return SNMP_FAILURE;
                        }
                    }
                }
                /* Check whether the Local ip is present in 
                 * the static napt list */

                pIfNode = gapNatIfTable[i4IfNum];
                pStaticNaptEntryNode = NULL;
                TMO_SLL_Scan (&pIfNode->StaticNaptList,
                              pStaticNaptEntryNode, tStaticNaptEntry *)
                {
                    if (u4NatStaticLocalIp == pStaticNaptEntryNode->u4LocIpAddr)
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                                      "Local Address is already in\
                                      StaticNaptList " "for  index %d and %d \n", i4NatStaticInterfaceNum, u4NatStaticLocalIp);
                        return SNMP_FAILURE;
                    }
                }

            }
        }

        NAT_TRC (NAT_TRC_ON, "Exiting with SUCCESS: Entry found \n");
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Static Address Table "
                  "of  index %d and %d does not exist\n",
                  i4NatStaticInterfaceNum, u4NatStaticLocalIp);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2NatStaticEntryStatus
 Input       :  The Indices
                NatStaticInterfaceNum
                NatStaticLocalIp

                The Object 
                testValNatStaticEntryStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatStaticEntryStatus (UINT4 *pu4ErrorCode,
                               INT4 i4NatStaticInterfaceNum,
                               UINT4 u4NatStaticLocalIp,
                               INT4 i4TestValNatStaticEntryStatus)
{

    NAT_DBG1 (NAT_DBG_LLR, "NatStaticInterfaceNum = %d\n",
              i4NatStaticInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticLocalIp = %u\n", u4NatStaticLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticEntryStatus = %d\n",
              i4TestValNatStaticEntryStatus);
    /*
     * Validating the Local IP address .Here the address type used
     * is NAT_GLOBAL because IP address with host Id all zero
     * (like -20.0.0.0 ) is not allowed in the Static Table.
     */
    if ((i4NatStaticInterfaceNum > NAT_ZERO)
        && (i4NatStaticInterfaceNum <= NAT_MAX_NUM_IF)
        && (NatValidateIpAddress (u4NatStaticLocalIp, NAT_GLOBAL)
            == NAT_SUCCESS))
    {
        switch (i4TestValNatStaticEntryStatus)
        {
            case NAT_STATUS_ACTIVE:

            case NAT_STATUS_NOT_IN_SERVICE:

            case NAT_STATUS_CREATE_AND_GO:

            case NAT_STATUS_CREATE_AND_WAIT:

            case NAT_STATUS_DESTROY:
                MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                              "The row of Static Address Table "
                              "of  index %d and %d    exist\n",
                              i4NatStaticInterfaceNum, u4NatStaticLocalIp);
                NAT_TRC (NAT_TRC_ON, "Exiting with SUCCESS: Entry found \n");
                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                return SNMP_SUCCESS;

            default:
                break;
        }
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT", "The row of Static Address Table\
                  of  index %d and %d does not exist\n", i4NatStaticInterfaceNum, u4NatStaticLocalIp);
    return SNMP_FAILURE;

}

/* LOW LEVEL Routines for Table : NatStaticNaptTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceNatStaticNaptTable
 Input       :  The Indices
                NatStaticNaptInterfaceNum
                NatStaticNaptLocalIp
                NatStaticNaptStartLocalPort
                NatStaticNaptEndLocalPort
                NatStaticNaptProtocolNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceNatStaticNaptTable (INT4 i4NatStaticNaptInterfaceNum,
                                            UINT4 u4NatStaticNaptLocalIp,
                                            INT4
                                            i4NatStaticNaptStartLocalPort,
                                            INT4 i4NatStaticNaptEndLocalPort,
                                            INT4 i4NatStaticNaptProtocolNumber)
{
    tInterfaceInfo     *pIfNode = NULL;
    tStaticNaptEntry   *pStaticNaptEntryNode = NULL;

    NAT_TRC (NAT_TRC_ON,
             "Entering the function: "
             "ValidateIndexInstance for Nat StaticNaptTable\n");
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptInterfaceNum = %d\n",
              i4NatStaticNaptInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptLocalIp = %u\n",
              u4NatStaticNaptLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptStartLocalPort = %d\n",
              i4NatStaticNaptStartLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptEndLocalPort = %d\n",
              i4NatStaticNaptEndLocalPort);

    pStaticNaptEntryNode = NULL;

    if ((i4NatStaticNaptInterfaceNum < NAT_ZERO) ||
        (i4NatStaticNaptInterfaceNum > NAT_MAX_NUM_IF))
    {
        return SNMP_FAILURE;
    }

    if (SecUtilValidateIfIndex ((UINT2) i4NatStaticNaptInterfaceNum) ==
        OSIX_SUCCESS)
    {
        if (gapNatIfTable[i4NatStaticNaptInterfaceNum] != NULL)
        {
            pIfNode = gapNatIfTable[i4NatStaticNaptInterfaceNum];
            TMO_SLL_Scan (&pIfNode->StaticNaptList, pStaticNaptEntryNode,
                          tStaticNaptEntry *)
            {
                if ((pStaticNaptEntryNode->u4LocIpAddr ==
                     u4NatStaticNaptLocalIp)
                    && ((INT4) pStaticNaptEntryNode->u2StartLocPort ==
                        i4NatStaticNaptStartLocalPort)
                    && ((INT4) pStaticNaptEntryNode->u2EndLocPort ==
                        i4NatStaticNaptEndLocalPort)
                    && ((INT4) pStaticNaptEntryNode->u2ProtocolNumber ==
                        i4NatStaticNaptProtocolNumber))
                {
                    MOD_TRC_ARG5 (gu4NatTrc, MGMT_TRC, "NAT",
                                  "The row of StaticNapt Table "
                                  "of  index %d, %u %d %dand %d    exist\n",
                                  i4NatStaticNaptInterfaceNum,
                                  u4NatStaticNaptLocalIp,
                                  i4NatStaticNaptStartLocalPort,
                                  i4NatStaticNaptEndLocalPort,
                                  i4NatStaticNaptProtocolNumber);
                    NAT_TRC (NAT_TRC_ON,
                             "Exiting with SUCCESS: Entry found \n");
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    NAT_TRC (NAT_TRC_ON, "No Such Entry exists Test Failed\n");
    MOD_TRC_ARG5 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of StaticNapt Table of  index %d,"
                  "%u %d %d and %d does not exist\n",
                  i4NatStaticNaptInterfaceNum,
                  u4NatStaticNaptLocalIp,
                  i4NatStaticNaptStartLocalPort,
                  i4NatStaticNaptEndLocalPort, i4NatStaticNaptProtocolNumber);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexNatStaticNaptTable
 Input       :  The Indices
                NatStaticNaptInterfaceNum
                NatStaticNaptLocalIp
                NatStaticNaptStartLocalPort
                NatStaticNaptEndLocalPort
                NatStaticNaptProtocolNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexNatStaticNaptTable (INT4 *pi4NatStaticNaptInterfaceNum,
                                    UINT4 *pu4NatStaticNaptLocalIp,
                                    INT4 *pi4NatStaticNaptStartLocalPort,
                                    INT4 *pi4NatStaticNaptEndLocalPort,
                                    INT4 *pi4NatStaticNaptProtocolNumber)
{
    tInterfaceInfo     *pIfNode = NULL;
    tStaticNaptEntry   *pStaticNaptEntryNode = NULL;
    INT4                i4IfNum = NAT_ZERO;

    pStaticNaptEntryNode = NULL;
    i4IfNum = NAT_ONE;
    while (i4IfNum <= NAT_MAX_NUM_IF)
    {
        if (gapNatIfTable[i4IfNum] != NULL)
        {
            pIfNode = gapNatIfTable[i4IfNum];
            pStaticNaptEntryNode =
                (tStaticNaptEntry *) TMO_SLL_First (&pIfNode->StaticNaptList);
            while (pStaticNaptEntryNode != NULL)
            {
                if ((pStaticNaptEntryNode->u1AppCallStatus != NAT_ON_HOLD) &&
                    (pStaticNaptEntryNode->u1AppCallStatus != NAT_NOT_ON_HOLD))
                {
                    *pi4NatStaticNaptInterfaceNum = i4IfNum;
                    *pu4NatStaticNaptLocalIp =
                        pStaticNaptEntryNode->u4LocIpAddr;
                    *pi4NatStaticNaptStartLocalPort =
                        (INT4) pStaticNaptEntryNode->u2StartLocPort;
                    *pi4NatStaticNaptEndLocalPort =
                        (INT4) pStaticNaptEntryNode->u2EndLocPort;
                    *pi4NatStaticNaptProtocolNumber =
                        (INT4) pStaticNaptEntryNode->u2ProtocolNumber;
                    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptInterfaceNum = %d\n",
                              *pi4NatStaticNaptInterfaceNum);
                    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptLocalIp = %u\n",
                              *pu4NatStaticNaptLocalIp);
                    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptStartLocalPort = %d\n",
                              *pi4NatStaticNaptStartLocalPort);
                    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptEndLocalPort = %d\n",
                              *pi4NatStaticNaptEndLocalPort);
                    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptProtocolNumber = %d\n",
                              *pi4NatStaticNaptProtocolNumber);
                    return SNMP_SUCCESS;
                }
                else
                {
                    pStaticNaptEntryNode = (tStaticNaptEntry *) TMO_SLL_Next
                        (&pIfNode->StaticNaptList,
                         (tTMO_SLL_NODE *) (VOID *) pStaticNaptEntryNode);
                }
            }

        }
        i4IfNum++;
    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexNatStaticNaptTable
 Input       :  The Indices
                NatStaticNaptInterfaceNum
                nextNatStaticNaptInterfaceNum
                NatStaticNaptLocalIp
                nextNatStaticNaptLocalIp
                NatStaticNaptStartLocalPort
                nextNatStaticNaptStartLocalPort
                NatStaticNaptEndLocalPort
                nextNatStaticNaptEndLocalPort
                NatStaticNaptProtocolNumber
                nextNatStaticNaptProtocolNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexNatStaticNaptTable (INT4 i4NatStaticNaptInterfaceNum,
                                   INT4 *pi4NextNatStaticNaptInterfaceNum,
                                   UINT4 u4NatStaticNaptLocalIp,
                                   UINT4 *pu4NextNatStaticNaptLocalIp,
                                   INT4 i4NatStaticNaptStartLocalPort,
                                   INT4 *pi4NextNatStaticNaptStartLocalPort,
                                   INT4 i4NatStaticNaptEndLocalPort,
                                   INT4 *pi4NextNatStaticNaptEndLocalPort,
                                   INT4 i4NatStaticNaptProtocolNumber,
                                   INT4 *pi4NextNatStaticNaptProtocolNumber)
{
    tInterfaceInfo     *pIfNode = NULL;
    tStaticNaptEntry   *pStaticNaptEntryNode = NULL;
    INT4                i4IfNum = NAT_ZERO;

    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptInterfaceNum = %d\n",
              i4NatStaticNaptInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptLocalIp = %u\n",
              u4NatStaticNaptLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptStartLocalPort = %d\n",
              i4NatStaticNaptStartLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptEndLocalPort = %d\n",
              i4NatStaticNaptEndLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptProtocolNumber = %d\n",
              i4NatStaticNaptProtocolNumber);

    pStaticNaptEntryNode = NULL;

    /* Search for the next static ip address in the same interface */

    i4IfNum = i4NatStaticNaptInterfaceNum;
    if (i4IfNum <= NAT_MAX_NUM_IF)
    {
        pIfNode = gapNatIfTable[i4IfNum];
        if (pIfNode != NULL)
        {
            TMO_SLL_Scan (&pIfNode->StaticNaptList, pStaticNaptEntryNode,
                          tStaticNaptEntry *)
            {
                if ((pStaticNaptEntryNode->u4LocIpAddr ==
                     u4NatStaticNaptLocalIp)
                    && ((INT4) pStaticNaptEntryNode->u2StartLocPort ==
                        i4NatStaticNaptStartLocalPort)
                    && ((INT4) pStaticNaptEntryNode->u2EndLocPort ==
                        i4NatStaticNaptEndLocalPort)
                    && ((INT4) pStaticNaptEntryNode->u2ProtocolNumber ==
                        i4NatStaticNaptProtocolNumber))
                {
                    pStaticNaptEntryNode = (tStaticNaptEntry *)
                        TMO_SLL_Next ((tTMO_SLL *) & pIfNode->StaticNaptList,
                                      &(pStaticNaptEntryNode->StaticNaptTable));

                    while (pStaticNaptEntryNode != NULL)
                    {
                        if ((pStaticNaptEntryNode->u1AppCallStatus
                             != NAT_ON_HOLD) &&
                            (pStaticNaptEntryNode->u1AppCallStatus
                             != NAT_NOT_ON_HOLD))
                        {
                            *pi4NextNatStaticNaptInterfaceNum = i4IfNum;
                            *pu4NextNatStaticNaptLocalIp =
                                pStaticNaptEntryNode->u4LocIpAddr;
                            *pi4NextNatStaticNaptStartLocalPort =
                                (INT4) pStaticNaptEntryNode->u2StartLocPort;
                            *pi4NextNatStaticNaptEndLocalPort =
                                (INT4) pStaticNaptEntryNode->u2EndLocPort;
                            *pi4NextNatStaticNaptProtocolNumber =
                                (INT4) pStaticNaptEntryNode->u2ProtocolNumber;
                            NAT_DBG1 (NAT_DBG_LLR,
                                      "NatNextStaticNaptInterfaceNum = %d\n",
                                      *pi4NextNatStaticNaptInterfaceNum);
                            NAT_DBG1 (NAT_DBG_LLR,
                                      "NatNextStaticNaptLocalIp = %u\n",
                                      *pu4NextNatStaticNaptLocalIp);
                            NAT_DBG1 (NAT_DBG_LLR,
                                      "NatNextStaticNaptStartLocalPort = %d\n",
                                      *pi4NextNatStaticNaptStartLocalPort);
                            NAT_DBG1 (NAT_DBG_LLR,
                                      "NatNextStaticNaptEndLocalPort = %d\n",
                                      *pi4NextNatStaticNaptEndLocalPort);
                            NAT_DBG1 (NAT_DBG_LLR,
                                      "NatNextStaticNaptLocalPort = %d\n",
                                      *pi4NextNatStaticNaptProtocolNumber);
                            return SNMP_SUCCESS;
                        }
                        else
                        {
                            pStaticNaptEntryNode = (tStaticNaptEntry *)
                                TMO_SLL_Next (&pIfNode->StaticNaptList,
                                              (tTMO_SLL_NODE *) (VOID *)
                                              pStaticNaptEntryNode);
                        }
                    }
                    break;
                }

            }
        }
    }
    if (i4NatStaticNaptInterfaceNum > NAT_MAX_NUM_IF)
    {
        NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
        return SNMP_FAILURE;
    }

    i4IfNum = i4NatStaticNaptInterfaceNum + NAT_ONE;

    while (i4IfNum <= NAT_MAX_NUM_IF)
    {
        if (gapNatIfTable[i4IfNum] != NULL)
        {
            pIfNode = gapNatIfTable[i4IfNum];
            pStaticNaptEntryNode =
                (tStaticNaptEntry *) TMO_SLL_First (&pIfNode->StaticNaptList);
            while (pStaticNaptEntryNode != NULL)
            {
                if ((pStaticNaptEntryNode->u1AppCallStatus != NAT_ON_HOLD) &&
                    (pStaticNaptEntryNode->u1AppCallStatus != NAT_NOT_ON_HOLD))
                {
                    *pi4NextNatStaticNaptInterfaceNum = i4IfNum;
                    *pu4NextNatStaticNaptLocalIp =
                        pStaticNaptEntryNode->u4LocIpAddr;
                    *pi4NextNatStaticNaptStartLocalPort =
                        (INT4) pStaticNaptEntryNode->u2StartLocPort;
                    *pi4NextNatStaticNaptEndLocalPort =
                        (INT4) pStaticNaptEntryNode->u2EndLocPort;
                    *pi4NextNatStaticNaptProtocolNumber =
                        (INT4) pStaticNaptEntryNode->u2ProtocolNumber;
                    NAT_DBG1 (NAT_DBG_LLR,
                              "NatNextStaticNaptInterfaceNum = %d\n",
                              *pi4NextNatStaticNaptInterfaceNum);
                    NAT_DBG1 (NAT_DBG_LLR, "NatNextStaticNaptLocalIp = %u\n",
                              *pu4NextNatStaticNaptLocalIp);
                    NAT_DBG1 (NAT_DBG_LLR,
                              "NatNextStaticNaptStartLocalPort = %d\n",
                              *pi4NextNatStaticNaptStartLocalPort);
                    NAT_DBG1 (NAT_DBG_LLR,
                              "NatNextStaticNaptEndLocalPort = %d\n",
                              *pi4NextNatStaticNaptEndLocalPort);
                    NAT_DBG1 (NAT_DBG_LLR,
                              "NatNextStaticNaptProtocolNumber = %d\n",
                              *pi4NextNatStaticNaptProtocolNumber);
                    return SNMP_SUCCESS;
                }
                else
                {
                    pStaticNaptEntryNode = (tStaticNaptEntry *) TMO_SLL_Next
                        (&pIfNode->StaticNaptList,
                         (tTMO_SLL_NODE *) (VOID *) pStaticNaptEntryNode);
                }
            }
        }
        i4IfNum++;
    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetNatStaticNaptTranslatedLocalIp
 Input       :  The Indices
                NatStaticNaptInterfaceNum
                NatStaticNaptLocalIp
                NatStaticNaptStartLocalPort
                NatStaticNaptEndLocalPort
                NatStaticNaptProtocolNumber

                The Object 
                retValNatStaticNaptTranslatedLocalIp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatStaticNaptTranslatedLocalIp (INT4 i4NatStaticNaptInterfaceNum,
                                      UINT4 u4NatStaticNaptLocalIp,
                                      INT4 i4NatStaticNaptStartLocalPort,
                                      INT4 i4NatStaticNaptEndLocalPort,
                                      INT4 i4NatStaticNaptProtocolNumber,
                                      UINT4
                                      *pu4RetValNatStaticNaptTranslatedLocalIp)
{

    tInterfaceInfo     *pIfNode = NULL;
    tStaticNaptEntry   *pStaticNaptEntryNode = NULL;
    INT4                i4IfNum = NAT_ZERO;

    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptInterfaceNum = %d\n",
              i4NatStaticNaptInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptLocalIp = %u\n",
              u4NatStaticNaptLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptStartLocalPort = %d\n",
              i4NatStaticNaptStartLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptEndLocalPort = %d\n",
              i4NatStaticNaptEndLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptProtocolNumber = %d\n",
              i4NatStaticNaptProtocolNumber);

    pStaticNaptEntryNode = NULL;

    /* Search for the next static ip address in the same interface */
    i4IfNum = i4NatStaticNaptInterfaceNum;
    if (i4IfNum <= NAT_MAX_NUM_IF)
    {
        pIfNode = gapNatIfTable[i4IfNum];

        if (pIfNode != NULL)
        {
            TMO_SLL_Scan (&pIfNode->StaticNaptList, pStaticNaptEntryNode,
                          tStaticNaptEntry *)
            {
                if ((pStaticNaptEntryNode->u4LocIpAddr ==
                     u4NatStaticNaptLocalIp)
                    && ((INT4) pStaticNaptEntryNode->u2StartLocPort ==
                        i4NatStaticNaptStartLocalPort)
                    && ((INT4) pStaticNaptEntryNode->u2EndLocPort ==
                        i4NatStaticNaptEndLocalPort)
                    && ((INT4) pStaticNaptEntryNode->u2ProtocolNumber ==
                        i4NatStaticNaptProtocolNumber))

                {
                    *pu4RetValNatStaticNaptTranslatedLocalIp =
                        pStaticNaptEntryNode->u4TranslatedLocIpAddr;
                    NAT_DBG1 (NAT_DBG_LLR,
                              "NatStaticNaptTranslatedLocIp = %u\n",
                              *pu4RetValNatStaticNaptTranslatedLocalIp);
                    return SNMP_SUCCESS;

                }
            }
        }

    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNatStaticNaptTranslatedLocalPort
 Input       :  The Indices
                NatStaticNaptInterfaceNum
                NatStaticNaptLocalIp
                NatStaticNaptStartLocalPort
                NatStaticNaptEndLocalPort
                NatStaticNaptProtocolNumber

                The Object 
                retValNatStaticNaptTranslatedLocalPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatStaticNaptTranslatedLocalPort (INT4 i4NatStaticNaptInterfaceNum,
                                        UINT4 u4NatStaticNaptLocalIp,
                                        INT4 i4NatStaticNaptStartLocalPort,
                                        INT4 i4NatStaticNaptEndLocalPort,
                                        INT4 i4NatStaticNaptProtocolNumber,
                                        INT4
                                        *pi4RetValNatStaticNaptTranslatedLocalPort)
{

    tInterfaceInfo     *pIfNode = NULL;
    tStaticNaptEntry   *pStaticNaptEntryNode = NULL;
    INT4                i4IfNum = NAT_ZERO;

    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptInterfaceNum = %d\n",
              i4NatStaticNaptInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptLocalIp = %u\n",
              u4NatStaticNaptLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptStartLocalPort = %d\n",
              i4NatStaticNaptStartLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptEndLocalPort = %d\n",
              i4NatStaticNaptEndLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptProtocolNumber = %d\n",
              i4NatStaticNaptProtocolNumber);

    pStaticNaptEntryNode = NULL;

    /* Search for the next static ip address in the same interface */
    i4IfNum = i4NatStaticNaptInterfaceNum;
    if (i4IfNum <= NAT_MAX_NUM_IF)
    {
        pIfNode = gapNatIfTable[i4IfNum];

        if (pIfNode != NULL)
        {
            TMO_SLL_Scan (&pIfNode->StaticNaptList, pStaticNaptEntryNode,
                          tStaticNaptEntry *)
            {
                if ((pStaticNaptEntryNode->u4LocIpAddr ==
                     u4NatStaticNaptLocalIp)
                    && ((INT4) pStaticNaptEntryNode->u2StartLocPort ==
                        i4NatStaticNaptStartLocalPort)
                    && ((INT4) pStaticNaptEntryNode->u2EndLocPort ==
                        i4NatStaticNaptEndLocalPort)
                    && ((INT4) pStaticNaptEntryNode->u2ProtocolNumber ==
                        i4NatStaticNaptProtocolNumber))
                {
                    *pi4RetValNatStaticNaptTranslatedLocalPort =
                        (INT4) pStaticNaptEntryNode->u2TranslatedLocPort;
                    NAT_DBG1 (NAT_DBG_LLR,
                              "NatStaticNaptTranslatedLocPort = %d\n",
                              *pi4RetValNatStaticNaptTranslatedLocalPort);
                    return SNMP_SUCCESS;

                }
            }
        }

    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNatStaticNaptDescription
 Input       :  The Indices
                NatStaticNaptInterfaceNum
                NatStaticNaptLocalIp
                NatStaticNaptStartLocalPort
                NatStaticNaptEndLocalPort
                NatStaticNaptProtocolNumber

                The Object 
                retValNatStaticNaptDescription
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatStaticNaptDescription (INT4 i4NatStaticNaptInterfaceNum,
                                UINT4 u4NatStaticNaptLocalIp,
                                INT4 i4NatStaticNaptStartLocalPort,
                                INT4 i4NatStaticNaptEndLocalPort,
                                INT4 i4NatStaticNaptProtocolNumber,
                                tSNMP_OCTET_STRING_TYPE
                                * pRetValNatStaticNaptDescription)
{
    tInterfaceInfo     *pIfNode = NULL;
    tStaticNaptEntry   *pStaticNaptEntryNode = NULL;
    INT4                i4IfNum = NAT_ZERO;

    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptInterfaceNum = %d\n",
              i4NatStaticNaptInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptLocalIp = %u\n",
              u4NatStaticNaptLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptStartLocalPort = %d\n",
              i4NatStaticNaptStartLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptEndLocalPort = %d\n",
              i4NatStaticNaptEndLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptProtocolNumber = %d\n",
              i4NatStaticNaptProtocolNumber);

    pStaticNaptEntryNode = NULL;

    /* Search for the next static ip address in the same interface */
    i4IfNum = i4NatStaticNaptInterfaceNum;
    if (i4IfNum <= NAT_MAX_NUM_IF)
    {
        pIfNode = gapNatIfTable[i4IfNum];

        if (pIfNode != NULL)
        {
            TMO_SLL_Scan (&pIfNode->StaticNaptList, pStaticNaptEntryNode,
                          tStaticNaptEntry *)
            {
                if ((pStaticNaptEntryNode->u4LocIpAddr ==
                     u4NatStaticNaptLocalIp)
                    && ((INT4) pStaticNaptEntryNode->u2StartLocPort ==
                        i4NatStaticNaptStartLocalPort)
                    && ((INT4) pStaticNaptEntryNode->u2EndLocPort ==
                        i4NatStaticNaptEndLocalPort)
                    && ((INT4) pStaticNaptEntryNode->u2ProtocolNumber ==
                        i4NatStaticNaptProtocolNumber))
                {
                    MEMCPY (pRetValNatStaticNaptDescription->pu1_OctetList,
                            pStaticNaptEntryNode->i1Description,
                            NAT_NAPT_ENTRY_DESCRN_LEN);
                    pRetValNatStaticNaptDescription->i4_Length =
                        (INT4) STRLEN (pStaticNaptEntryNode->i1Description);
                    return SNMP_SUCCESS;

                }
            }
        }

    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;

}

/* UPNP NAPT TIMER  */
/****************************************************************************
 Function    :  nmhGetNatStaticNaptLeaseDuration
 Input       :  The Indices
                NatStaticNaptInterfaceNum
                NatStaticNaptLocalIp
                NatStaticNaptStartLocalPort
                NatStaticNaptEndLocalPort
                NatStaticNaptTranslatedLocalPort
                NatStaticNaptProtocolNumber

                The Object 
                retValNatStaticNaptLeaseDuration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatStaticNaptLeaseDuration (INT4 i4NatStaticNaptInterfaceNum,
                                  UINT4 u4NatStaticNaptLocalIp,
                                  INT4 i4NatStaticNaptStartLocalPort,
                                  INT4 i4NatStaticNaptEndLocalPort,
                                  INT4 i4NatStaticNaptProtocolNumber,
                                  INT4 *pi4RetValNatStaticNaptLeaseDuration)
{
    tInterfaceInfo     *pIfNode = NULL;
    tStaticNaptEntry   *pStaticNaptEntryNode = NULL;
    INT4                i4IfNum = NAT_ZERO;

    /* Search for the next static ip address in the same interface */
    i4IfNum = i4NatStaticNaptInterfaceNum;
    if (i4IfNum <= NAT_MAX_NUM_IF)
    {
        pIfNode = gapNatIfTable[i4IfNum];

        if (pIfNode != NULL)
        {
            TMO_SLL_Scan (&pIfNode->StaticNaptList, pStaticNaptEntryNode,
                          tStaticNaptEntry *)
            {
                if ((pStaticNaptEntryNode->u4LocIpAddr ==
                     u4NatStaticNaptLocalIp)
                    && ((INT4) pStaticNaptEntryNode->u2StartLocPort ==
                        i4NatStaticNaptStartLocalPort)
                    && ((INT4) pStaticNaptEntryNode->u2EndLocPort ==
                        i4NatStaticNaptEndLocalPort)
                    && ((INT4) pStaticNaptEntryNode->u2ProtocolNumber ==
                        i4NatStaticNaptProtocolNumber))
                {
                    *pi4RetValNatStaticNaptLeaseDuration =
                        (INT4) pStaticNaptEntryNode->NaptTimerNode.TimerNode.
                        u4Data;
                    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptLeaseDuration = %d\n",
                              *pi4RetValNatStaticNaptLeaseDuration);

                    return (SNMP_SUCCESS);
                }
            }
        }

    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return (SNMP_FAILURE);

}

/* END */
/****************************************************************************
 Function    :  nmhGetNatStaticNaptEntryStatus
 Input       :  The Indices
                NatStaticNaptInterfaceNum
                NatStaticNaptLocalIp
                NatStaticNaptStartLocalPort
                NatStaticNaptEndLocalPort
                NatStaticNaptProtocolNumber

                The Object 
                retValNatStaticNaptEntryStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatStaticNaptEntryStatus (INT4 i4NatStaticNaptInterfaceNum,
                                UINT4 u4NatStaticNaptLocalIp,
                                INT4 i4NatStaticNaptStartLocalPort,
                                INT4 i4NatStaticNaptEndLocalPort,
                                INT4 i4NatStaticNaptProtocolNumber,
                                INT4 *pi4RetValNatStaticNaptEntryStatus)
{

    tInterfaceInfo     *pIfNode = NULL;
    tStaticNaptEntry   *pStaticNaptEntryNode = NULL;
    INT4                i4IfNum = NAT_ZERO;

    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptInterfaceNum = %d\n",
              i4NatStaticNaptInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptLocalIp = %u\n",
              u4NatStaticNaptLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptStartLocalPort = %d\n",
              i4NatStaticNaptStartLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptEndLocalPort = %d\n",
              i4NatStaticNaptEndLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptProtocolNumber = %d\n",
              i4NatStaticNaptProtocolNumber);

    pStaticNaptEntryNode = NULL;

    i4IfNum = i4NatStaticNaptInterfaceNum;
    if ((i4IfNum > NAT_ZERO) && (i4IfNum <= NAT_MAX_NUM_IF))
    {
        if (gapNatIfTable[i4IfNum] != NULL)
        {
            pIfNode = gapNatIfTable[i4IfNum];
            TMO_SLL_Scan (&pIfNode->StaticNaptList, pStaticNaptEntryNode,
                          tStaticNaptEntry *)
            {
                if ((pStaticNaptEntryNode->u4LocIpAddr ==
                     u4NatStaticNaptLocalIp)
                    && ((INT4) pStaticNaptEntryNode->u2StartLocPort ==
                        i4NatStaticNaptStartLocalPort)
                    && ((INT4) pStaticNaptEntryNode->u2EndLocPort ==
                        i4NatStaticNaptEndLocalPort)
                    && ((INT4) pStaticNaptEntryNode->u2ProtocolNumber ==
                        i4NatStaticNaptProtocolNumber))
                {
                    *pi4RetValNatStaticNaptEntryStatus =
                        pStaticNaptEntryNode->i4RowStatus;
                    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptEntryStatus = %d\n",
                              *pi4RetValNatStaticNaptEntryStatus);
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetNatStaticNaptTranslatedLocalIp
 Input       :  The Indices
                NatStaticNaptInterfaceNum
                NatStaticNaptLocalIp
                NatStaticNaptStartLocalPort
                NatStaticNaptEndLocalPort
                NatStaticNaptProtocolNumber

                The Object 
                setValNatStaticNaptTranslatedLocalIp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatStaticNaptTranslatedLocalIp (INT4 i4NatStaticNaptInterfaceNum,
                                      UINT4 u4NatStaticNaptLocalIp,
                                      INT4 i4NatStaticNaptStartLocalPort,
                                      INT4 i4NatStaticNaptEndLocalPort,
                                      INT4 i4NatStaticNaptProtocolNumber,
                                      UINT4
                                      u4SetValNatStaticNaptTranslatedLocalIp)
{
    tInterfaceInfo     *pIfNode = NULL;
    tStaticNaptEntry   *pStaticNaptEntryNode = NULL;
    INT4                i4IfNum = NAT_ZERO;
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
#ifdef UPNP_WANTED
    CHR1                au1Value[NAT_SIZE_OF_PM_PORT_VAL];
#endif

    NAT_DBG1 (NAT_DBG_LLR, "NatStaticInterfaceNum = %d\n",
              i4NatStaticNaptInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticLocalIp = %u\n", u4NatStaticNaptLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticStartLocalPort = %d\n",
              i4NatStaticNaptStartLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticEndLocalPort = %d\n",
              i4NatStaticNaptEndLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticTranslatedLocIp = %u\n",
              u4SetValNatStaticNaptTranslatedLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticProtocolNumber = %u\n",
              i4NatStaticNaptProtocolNumber);

    RM_GET_SEQ_NUM (&u4SeqNum);

    i4IfNum = i4NatStaticNaptInterfaceNum;
    pIfNode = gapNatIfTable[i4IfNum];

    if (pIfNode == NULL)
    {
        if ((pIfNode = NatAllocateMemory (i4IfNum)) == NULL)
        {
            return SNMP_FAILURE;
        }
    }

    if ((pIfNode->StaticNaptList).u4_Count >= NAT_STATIC_NAPT_BLOCKS)
    {
        return SNMP_FAILURE;
    }

    if (i4IfNum <= NAT_MAX_NUM_IF)
    {
        if (gapNatIfTable[i4IfNum] != NULL)
        {
            TMO_SLL_Scan (&pIfNode->StaticNaptList, pStaticNaptEntryNode,
                          tStaticNaptEntry *)
            {
                if ((u4NatStaticNaptLocalIp ==
                     pStaticNaptEntryNode->u4LocIpAddr)
                    && (i4NatStaticNaptStartLocalPort ==
                        (INT4) pStaticNaptEntryNode->u2StartLocPort)
                    && (i4NatStaticNaptEndLocalPort ==
                        (INT4) pStaticNaptEntryNode->u2EndLocPort)
                    &&
                    ((i4NatStaticNaptProtocolNumber ==
                      pStaticNaptEntryNode->u2ProtocolNumber)
                     || (i4NatStaticNaptProtocolNumber == NAT_PROTO_ANY)))
                {

                    if (CfaGetIfIpAddr (i4NatStaticNaptInterfaceNum,
                                        &pStaticNaptEntryNode->
                                        u4TranslatedLocIpAddr) == CFA_FAILURE)
                    {
                        CLI_SET_ERR (CLI_NAT_ERR_INVALID_INTF);
                        return CLI_FAILURE;
                    }

                    if (pStaticNaptEntryNode->u2TranslatedLocPort != NAT_ZERO)
                    {
                        pStaticNaptEntryNode->i4RowStatus =
                            NAT_STATUS_NOT_IN_SERVICE;
                    }
                    else
                    {
                        pStaticNaptEntryNode->i4RowStatus =
                            NAT_STATUS_NOT_READY;
                    }
                    MOD_TRC_ARG3 (gu4NatTrc, MGMT_TRC, "NAT",
                                  "Successfully Set the StaticNapt Global IP"
                                  "Address of index %d and %d to %d \n",
                                  i4IfNum,
                                  pStaticNaptEntryNode->u4LocIpAddr,
                                  u4SetValNatStaticNaptTranslatedLocalIp);
                    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                          NatStaticNaptTranslatedLocalIp,
                                          u4SeqNum, FALSE, NatLock, NatUnLock,
                                          NAT_FIVE, SNMP_SUCCESS);
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i %i %p",
                                      i4NatStaticNaptInterfaceNum,
                                      u4NatStaticNaptLocalIp,
                                      i4NatStaticNaptStartLocalPort,
                                      i4NatStaticNaptEndLocalPort,
                                      i4NatStaticNaptProtocolNumber,
                                      u4SetValNatStaticNaptTranslatedLocalIp));

                    return SNMP_SUCCESS;
                }
            }
        }

        if (NAT_MEM_ALLOCATE
            (NAT_STATIC_NAPT_POOL_ID, pStaticNaptEntryNode, tStaticNaptEntry)
            == NULL)
        {
            MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT", "Memory Allocation Failed\n");
            return SNMP_FAILURE;
        }

        TMO_SLL_Init_Node (&pStaticNaptEntryNode->StaticNaptTable);
        pStaticNaptEntryNode->u4LocIpAddr = u4NatStaticNaptLocalIp;
        pStaticNaptEntryNode->u4TranslatedLocIpAddr =
            u4SetValNatStaticNaptTranslatedLocalIp;
        pStaticNaptEntryNode->u2TranslatedLocPort = NAT_ZERO;
        pStaticNaptEntryNode->i4RowStatus = NAT_STATUS_NOT_READY;
        TMO_SLL_Add (&gapNatIfTable[i4IfNum]->StaticNaptList,
                     &(pStaticNaptEntryNode->StaticNaptTable));
#ifdef UPNP_WANTED
        SPRINTF (au1Value, "%d", (unsigned int)
                 ((pIfNode->StaticNaptList).u4_Count));
        SET_UPNP_NAT_STATE_VAR_VAL (NAT_VARIABLE_TYPE_PORT_MAP_ENT_COUNT,
                                    au1Value);
        SEND_NOTIFY_TO_CONTROL_POINTS ();
        NAT_TRC1 (NAT_TRC_ON, "PortMapEntryCounts :%d\n", (unsigned int)
                  ((pIfNode->StaticNaptList).u4_Count));
#endif /* UPNP_WANTED */
        NAT_TRC (NAT_TRC_ON, "Successfully Set the Value\n");
        MOD_TRC_ARG3 (gu4NatTrc, MGMT_TRC, "NAT",
                      "Successfully Set the Static Global IP Address "
                      "of index %d and %d to %d \n",
                      i4IfNum, pStaticNaptEntryNode->u4LocIpAddr,
                      u4SetValNatStaticNaptTranslatedLocalIp);
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatStaticNaptTranslatedLocalIp,
                              u4SeqNum, FALSE, NatLock, NatUnLock, NAT_FIVE,
                              SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i %i %p",
                          i4NatStaticNaptInterfaceNum, u4NatStaticNaptLocalIp,
                          i4NatStaticNaptStartLocalPort,
                          i4NatStaticNaptEndLocalPort,
                          i4NatStaticNaptProtocolNumber,
                          u4SetValNatStaticNaptTranslatedLocalIp));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetNatStaticNaptTranslatedLocalPort
 Input       :  The Indices
                NatStaticNaptInterfaceNum
                NatStaticNaptLocalIp
                NatStaticNaptStartLocalPort
                NatStaticNaptEndLocalPort
                NatStaticNaptProtocolNumber

                The Object 
                setValNatStaticNaptTranslatedLocalPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatStaticNaptTranslatedLocalPort (INT4 i4NatStaticNaptInterfaceNum,
                                        UINT4 u4NatStaticNaptLocalIp,
                                        INT4 i4NatStaticNaptStartLocalPort,
                                        INT4 i4NatStaticNaptEndLocalPort,
                                        INT4 i4NatStaticNaptProtocolNumber,
                                        INT4
                                        i4SetValNatStaticNaptTranslatedLocalPort)
{
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tInterfaceInfo     *pIfNode = NULL;
    tStaticNaptEntry   *pStaticNaptEntryNode = NULL;
    INT4                i4IfNum = NAT_ZERO;
#ifdef UPNP_WANTED
    CHR1                au1Value[NAT_SIZE_OF_PM_PORT_VAL];
#endif

    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptInterfaceNum = %d\n",
              i4NatStaticNaptInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptLocalIp = %u\n",
              u4NatStaticNaptLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptStartLocalPort = %u\n",
              i4NatStaticNaptStartLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptEndLocalPort = %u\n",
              i4NatStaticNaptEndLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptEndLocalPort = %u\n",
              i4NatStaticNaptProtocolNumber);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticEndTranslatedLocPort = %u\n",
              i4SetValNatStaticNaptTranslatedLocalPort);

    RM_GET_SEQ_NUM (&u4SeqNum);

    i4IfNum = i4NatStaticNaptInterfaceNum;
    pIfNode = gapNatIfTable[i4IfNum];

    if (i4IfNum <= NAT_MAX_NUM_IF)
    {
        if (gapNatIfTable[i4IfNum] != NULL)
        {
            TMO_SLL_Scan (&pIfNode->StaticNaptList, pStaticNaptEntryNode,
                          tStaticNaptEntry *)
            {
                if ((u4NatStaticNaptLocalIp ==
                     pStaticNaptEntryNode->u4LocIpAddr)
                    && (i4NatStaticNaptStartLocalPort ==
                        (INT4) pStaticNaptEntryNode->u2StartLocPort)
                    && (i4NatStaticNaptEndLocalPort ==
                        (INT4) pStaticNaptEntryNode->u2EndLocPort)
                    && ((i4NatStaticNaptProtocolNumber ==
                         pStaticNaptEntryNode->u2ProtocolNumber) ||
                        (i4NatStaticNaptProtocolNumber == NAT_PROTO_ANY)))
                {
                    pStaticNaptEntryNode->u2TranslatedLocPort =
                        (UINT2) i4SetValNatStaticNaptTranslatedLocalPort;
                    if (pStaticNaptEntryNode->u4TranslatedLocIpAddr != NAT_ZERO)
                    {
                        pStaticNaptEntryNode->i4RowStatus =
                            NAT_STATUS_NOT_IN_SERVICE;
                    }
                    else
                    {
                        pStaticNaptEntryNode->i4RowStatus =
                            NAT_STATUS_NOT_READY;
                    }
                    MOD_TRC_ARG3 (gu4NatTrc, MGMT_TRC, "NAT",
                                  "Successfully Set the StaticNapt Global IP "
                                  "Address of index %d and %d to %d \n",
                                  i4IfNum,
                                  pStaticNaptEntryNode->u4LocIpAddr,
                                  i4SetValNatStaticNaptTranslatedLocalPort);

                    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                          NatStaticNaptTranslatedLocalPort,
                                          u4SeqNum, FALSE, NatLock, NatUnLock,
                                          NAT_FIVE, SNMP_SUCCESS);
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i %i %i",
                                      i4NatStaticNaptInterfaceNum,
                                      u4NatStaticNaptLocalIp,
                                      i4NatStaticNaptStartLocalPort,
                                      i4NatStaticNaptEndLocalPort,
                                      i4NatStaticNaptProtocolNumber,
                                      i4SetValNatStaticNaptTranslatedLocalPort));
                    return SNMP_SUCCESS;
                }
            }
        }

        /*If entry doesn't already exist, create */

        if (gapNatIfTable[i4IfNum] == NULL)
        {
            pIfNode = NatAllocateMemory (i4IfNum);
            if (pIfNode == NULL)
            {
                return SNMP_FAILURE;
            }
            NATIFTABLE_INITIALIZE (gapNatIfTable[i4IfNum], NAT_STATUS_INVLD);
        }

        if (NAT_MEM_ALLOCATE
            (NAT_STATIC_NAPT_POOL_ID, pStaticNaptEntryNode, tStaticNaptEntry)
            == NULL)
        {
            MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT", "Memory Allocation Failed\n");
            return SNMP_FAILURE;
        }

        TMO_SLL_Init_Node (&pStaticNaptEntryNode->StaticNaptTable);
        pStaticNaptEntryNode->u4LocIpAddr = u4NatStaticNaptLocalIp;
        pStaticNaptEntryNode->u4TranslatedLocIpAddr = NAT_ZERO;
        pStaticNaptEntryNode->u2TranslatedLocPort =
            (UINT2) i4SetValNatStaticNaptTranslatedLocalPort;
        pStaticNaptEntryNode->i4RowStatus = NAT_STATUS_NOT_READY;
        TMO_SLL_Add (&gapNatIfTable[i4IfNum]->StaticNaptList,
                     &(pStaticNaptEntryNode->StaticNaptTable));
#ifdef UPNP_WANTED
        SPRINTF (au1Value, "%d", (unsigned int)
                 ((pIfNode->StaticNaptList).u4_Count));
        SET_UPNP_NAT_STATE_VAR_VAL (NAT_VARIABLE_TYPE_PORT_MAP_ENT_COUNT,
                                    au1Value);
        SEND_NOTIFY_TO_CONTROL_POINTS ();
        NAT_TRC1 (NAT_TRC_ON, "PortMapEntryCounts :%d\n", (unsigned int)
                  ((pIfNode->StaticNaptList).u4_Count));

#endif /* UPNP_WANTED */
        NAT_TRC (NAT_TRC_ON, "Successfully Set the Value\n");
        MOD_TRC_ARG3 (gu4NatTrc, MGMT_TRC, "NAT",
                      "Successfully Set the Static Global IP Address "
                      "of index %d and %d to %d \n",
                      i4IfNum, pStaticNaptEntryNode->u4LocIpAddr,
                      i4SetValNatStaticNaptTranslatedLocalPort);
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatStaticNaptTranslatedLocalPort,
                              u4SeqNum, FALSE, NatLock, NatUnLock, NAT_FIVE,
                              SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i %i %i",
                          i4NatStaticNaptInterfaceNum, u4NatStaticNaptLocalIp,
                          i4NatStaticNaptStartLocalPort,
                          i4NatStaticNaptEndLocalPort,
                          i4NatStaticNaptProtocolNumber,
                          i4SetValNatStaticNaptTranslatedLocalPort));
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetNatStaticNaptDescription
 Input       :  The Indices
                NatStaticNaptInterfaceNum
                NatStaticNaptLocalIp
                NatStaticNaptStartLocalPort
                NatStaticNaptEndLocalPort
                NatStaticNaptProtocolNumber

                The Object 
                setValNatStaticNaptDescription
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatStaticNaptDescription (INT4 i4NatStaticNaptInterfaceNum,
                                UINT4 u4NatStaticNaptLocalIp,
                                INT4 i4NatStaticNaptStartLocalPort,
                                INT4 i4NatStaticNaptEndLocalPort,
                                INT4 i4NatStaticNaptProtocolNumber,
                                tSNMP_OCTET_STRING_TYPE
                                * pSetValNatStaticNaptDescription)
{
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tInterfaceInfo     *pIfNode = NULL;
    tStaticNaptEntry   *pStaticNaptEntryNode = NULL;
    INT4                i4IfNum = NAT_ZERO;

    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptInterfaceNum = %d\n",
              i4NatStaticNaptInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptLocalIp = %u\n",
              u4NatStaticNaptLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptStartLocalPort = %u\n",
              i4NatStaticNaptStartLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptEndLocalPort = %u\n",
              i4NatStaticNaptEndLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptEndLocalPort = %u\n",
              i4NatStaticNaptProtocolNumber);
    if (pSetValNatStaticNaptDescription->i4_Length != NAT_ZERO)
    {
        NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptDescription = %s\n",
                  pSetValNatStaticNaptDescription->pu1_OctetList);
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    i4IfNum = i4NatStaticNaptInterfaceNum;
    pIfNode = gapNatIfTable[i4IfNum];
    if (i4IfNum <= NAT_MAX_NUM_IF)
    {
        if (gapNatIfTable[i4IfNum] != NULL)
        {
            TMO_SLL_Scan (&pIfNode->StaticNaptList, pStaticNaptEntryNode,
                          tStaticNaptEntry *)
            {
                if ((u4NatStaticNaptLocalIp ==
                     pStaticNaptEntryNode->u4LocIpAddr)
                    && (i4NatStaticNaptStartLocalPort ==
                        (INT4) pStaticNaptEntryNode->u2StartLocPort)
                    && (i4NatStaticNaptEndLocalPort ==
                        (INT4) pStaticNaptEntryNode->u2EndLocPort)
                    && ((i4NatStaticNaptProtocolNumber ==
                         pStaticNaptEntryNode->u2ProtocolNumber) ||
                        (i4NatStaticNaptProtocolNumber == NAT_PROTO_ANY)))
                {
                    /* V S FIX */
                    MEMSET (pStaticNaptEntryNode->i1Description,
                            NAT_ZERO, NAT_NAPT_ENTRY_DESCRN_LEN);
                    /* END */
                    MEMCPY (pStaticNaptEntryNode->i1Description,
                            pSetValNatStaticNaptDescription->pu1_OctetList,
                            pSetValNatStaticNaptDescription->i4_Length);
                    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                          NatStaticNaptDescription, u4SeqNum,
                                          FALSE, NatLock, NatUnLock, NAT_FIVE,
                                          SNMP_SUCCESS);
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i %i %s",
                                      i4NatStaticNaptInterfaceNum,
                                      u4NatStaticNaptLocalIp,
                                      i4NatStaticNaptStartLocalPort,
                                      i4NatStaticNaptEndLocalPort,
                                      i4NatStaticNaptProtocolNumber,
                                      pSetValNatStaticNaptDescription));
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    return SNMP_FAILURE;

}

/* UPNP NAPT TIMER */
/****************************************************************************
 Function    :  nmhSetNatStaticNaptLeaseDuration
 Input       :  The Indices
                NatStaticNaptInterfaceNum
                NatStaticNaptLocalIp
                NatStaticNaptStartLocalPort
                NatStaticNaptEndLocalPort
                NatStaticNaptTranslatedLocalPort
                NatStaticNaptProtocolNumber

                The Object 
                setValNatStaticNaptLeaseDuration
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatStaticNaptLeaseDuration (INT4 i4NatStaticNaptInterfaceNum,
                                  UINT4 u4NatStaticNaptLocalIp,
                                  INT4 i4NatStaticNaptStartLocalPort,
                                  INT4 i4NatStaticNaptEndLocalPort,
                                  INT4 i4NatStaticNaptProtocolNumber,
                                  INT4 i4SetValNatStaticNaptLeaseDuration)
{
    tInterfaceInfo     *pIfNode = NULL;
    tStaticNaptEntry   *pStaticNaptEntryNode = NULL;
    INT4                i4IfNum = NAT_ZERO;

    i4IfNum = i4NatStaticNaptInterfaceNum;
    pIfNode = gapNatIfTable[i4IfNum];
    if (i4IfNum <= NAT_MAX_NUM_IF)
    {
        if (gapNatIfTable[i4IfNum] != NULL)
        {
            TMO_SLL_Scan (&pIfNode->StaticNaptList, pStaticNaptEntryNode,
                          tStaticNaptEntry *)
            {
                if ((u4NatStaticNaptLocalIp ==
                     pStaticNaptEntryNode->u4LocIpAddr)
                    && (i4NatStaticNaptStartLocalPort ==
                        (INT4) pStaticNaptEntryNode->u2StartLocPort)
                    && (i4NatStaticNaptEndLocalPort ==
                        (INT4) pStaticNaptEntryNode->u2EndLocPort)
                    && ((i4NatStaticNaptProtocolNumber ==
                         pStaticNaptEntryNode->u2ProtocolNumber) ||
                        (i4NatStaticNaptProtocolNumber == NAT_PROTO_ANY)))
                {
                    pStaticNaptEntryNode->NaptTimerNode.TimerNode.u4Data
                        = (UINT4) i4SetValNatStaticNaptLeaseDuration;
                    return (SNMP_SUCCESS);
                }
            }
        }
    }
    return (SNMP_FAILURE);
}

/* END */
/****************************************************************************
 Function    :  nmhSetNatStaticNaptEntryStatus
 Input       :  The Indices
                NatStaticNaptInterfaceNum
                NatStaticNaptLocalIp
                NatStaticNaptStartLocalPort
                NatStaticNaptEndLocalPort
                NatStaticNaptProtocolNumber

                The Object 
                setValNatStaticNaptEntryStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatStaticNaptEntryStatus (INT4 i4NatStaticNaptInterfaceNum,
                                UINT4 u4NatStaticNaptLocalIp,
                                INT4 i4NatStaticNaptStartLocalPort,
                                INT4 i4NatStaticNaptEndLocalPort,
                                INT4 i4NatStaticNaptProtocolNumber,
                                INT4 i4SetValNatStaticNaptEntryStatus)
{

    INT1                i1RetStatus = NAT_ZERO;
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    NAT_DBG1 (NAT_DBG_LLR, "NatStaticInterfaceNum = %d\n",
              i4NatStaticNaptInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticLocalIp = %u\n", u4NatStaticNaptLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticStartLocalPort = %d\n",
              i4NatStaticNaptStartLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticEndLocalIp = %d\n",
              i4NatStaticNaptEndLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "Packet Type = %d\n", i4NatStaticNaptProtocolNumber);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptEntryStatus = %d\n",
              i4SetValNatStaticNaptEntryStatus);

    RM_GET_SEQ_NUM (&u4SeqNum);

    switch (i4SetValNatStaticNaptEntryStatus)
    {
        case NAT_STATUS_ACTIVE:
            i1RetStatus =
                NatSttNaptActive (i4NatStaticNaptInterfaceNum,
                                  u4NatStaticNaptLocalIp,
                                  (UINT2) i4NatStaticNaptStartLocalPort,
                                  (UINT2) i4NatStaticNaptEndLocalPort,
                                  (UINT2) i4NatStaticNaptProtocolNumber);

            break;

        case NAT_STATUS_CREATE_AND_GO:
            i1RetStatus =
                NatSttNaptCreateAndGo (i4NatStaticNaptInterfaceNum,
                                       u4NatStaticNaptLocalIp,
                                       (UINT2) i4NatStaticNaptStartLocalPort,
                                       (UINT2) i4NatStaticNaptEndLocalPort,
                                       (UINT2) i4NatStaticNaptProtocolNumber);
            break;

        case NAT_STATUS_CREATE_AND_WAIT:
            i1RetStatus =
                NatSttNaptCreateAndWait (i4NatStaticNaptInterfaceNum,
                                         u4NatStaticNaptLocalIp,
                                         (UINT2) i4NatStaticNaptStartLocalPort,
                                         (UINT2) i4NatStaticNaptEndLocalPort,
                                         (UINT2) i4NatStaticNaptProtocolNumber);
            break;

        case NAT_STATUS_NOT_IN_SERVICE:
            i1RetStatus =
                NatSttNaptNotInService (i4NatStaticNaptInterfaceNum,
                                        u4NatStaticNaptLocalIp,
                                        (UINT2) i4NatStaticNaptStartLocalPort,
                                        (UINT2) i4NatStaticNaptEndLocalPort,
                                        (UINT2) i4NatStaticNaptProtocolNumber);
            break;
        case NAT_STATUS_DESTROY:
            i1RetStatus =
                NatSttNaptDestroy (i4NatStaticNaptInterfaceNum,
                                   u4NatStaticNaptLocalIp,
                                   (UINT2) i4NatStaticNaptStartLocalPort,
                                   (UINT2) i4NatStaticNaptEndLocalPort,
                                   (UINT2) i4NatStaticNaptProtocolNumber);
            break;

        default:
            MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT",
                     "\n Unknown Row Status Option \n");
            i1RetStatus = SNMP_FAILURE;
            break;
    }
    if (i1RetStatus == SNMP_SUCCESS)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatStaticNaptEntryStatus,
                              u4SeqNum, TRUE, NatLock, NatUnLock, NAT_FIVE,
                              SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i %i %i",
                          i4NatStaticNaptInterfaceNum, u4NatStaticNaptLocalIp,
                          i4NatStaticNaptStartLocalPort,
                          i4NatStaticNaptEndLocalPort,
                          i4NatStaticNaptProtocolNumber,
                          i4SetValNatStaticNaptEntryStatus));

    }

    return i1RetStatus;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2NatStaticNaptTranslatedLocalIp
 Input       :  The Indices
                NatStaticNaptInterfaceNum
                NatStaticNaptLocalIp
                NatStaticNaptStartLocalPort
                NatStaticNaptEndLocalPort
                NatStaticNaptProtocolNumber

                The Object 
                testValNatStaticNaptTranslatedLocalIp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatStaticNaptTranslatedLocalIp (UINT4 *pu4ErrorCode,
                                         INT4 i4NatStaticNaptInterfaceNum,
                                         UINT4 u4NatStaticNaptLocalIp,
                                         INT4 i4NatStaticNaptStartLocalPort,
                                         INT4 i4NatStaticNaptEndLocalPort,
                                         INT4 i4NatStaticNaptProtocolNumber,
                                         UINT4
                                         u4TestValNatStaticNaptTranslatedLocalIp)
{
    tInterfaceInfo     *pIfNode = NULL;
    tStaticNaptEntry   *pStaticNaptEntryNode = NULL;

    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptInterfaceNum = %d\n",
              i4NatStaticNaptInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptLocalIp = %u\n",
              u4NatStaticNaptLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptStartLocalPort = %d\n",
              i4NatStaticNaptStartLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptEndLocalPort = %d\n",
              i4NatStaticNaptEndLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptProtocolNumber = %d\n",
              i4NatStaticNaptProtocolNumber);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptTranslatedLocIp = %u\n",
              u4TestValNatStaticNaptTranslatedLocalIp);

    if (i4NatStaticNaptStartLocalPort != i4NatStaticNaptEndLocalPort)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /*
     *  add virtual server configuration should not depend on 
     *  Translated Local IP so translatedLocalIP validation is
     *  commented - for HDC 45559 
     */

/*
    if ((NatValidateIpAddress (u4TestValNatStaticNaptTranslatedLocalIp,
                               NAT_GLOBAL) == NAT_FAILURE)
        || (u4NatStaticNaptLocalIp == u4TestValNatStaticNaptTranslatedLocalIp)
        || (u4TestValNatStaticNaptTranslatedLocalIp == 0))
*/
    if (u4NatStaticNaptLocalIp == u4TestValNatStaticNaptTranslatedLocalIp)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MOD_TRC_ARG5 (gu4NatTrc, MGMT_TRC, "NAT",
                      "The illegal Global Address in StaticNapt Table "
                      "of  index %d, %u %d %d and %u \n",
                      i4NatStaticNaptInterfaceNum,
                      u4NatStaticNaptLocalIp,
                      i4NatStaticNaptStartLocalPort,
                      i4NatStaticNaptEndLocalPort,
                      i4NatStaticNaptProtocolNumber);
        return SNMP_FAILURE;
    }

    if ((i4NatStaticNaptInterfaceNum > NAT_ZERO)
        && (i4NatStaticNaptInterfaceNum <= NAT_MAX_NUM_IF))
    {
        pIfNode = gapNatIfTable[i4NatStaticNaptInterfaceNum];

        if (pIfNode == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return (SNMP_FAILURE);
        }

        TMO_SLL_Scan (&pIfNode->StaticNaptList, pStaticNaptEntryNode,
                      tStaticNaptEntry *)
        {
            if ((u4NatStaticNaptLocalIp ==
                 pStaticNaptEntryNode->u4LocIpAddr)
                && (i4NatStaticNaptStartLocalPort ==
                    (INT4) pStaticNaptEntryNode->u2StartLocPort)
                && (i4NatStaticNaptEndLocalPort ==
                    (INT4) pStaticNaptEntryNode->u2EndLocPort)
                && (i4NatStaticNaptProtocolNumber ==
                    pStaticNaptEntryNode->u2ProtocolNumber))
            {
                if (pStaticNaptEntryNode->i4RowStatus == NAT_STATUS_ACTIVE)
                {
                    /* Entry is active , can not set the value */
                    *pu4ErrorCode = SNMP_ERR_BAD_VALUE;
                    return SNMP_FAILURE;
                }
            }
        }

        NAT_TRC (NAT_TRC_ON, "Exiting with SUCCESS: Entry found \n");
        return SNMP_SUCCESS;
    }
    MOD_TRC_ARG5 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of StaticNapt Address Table "
                  "of  index %d, %u %d %d and %u does not exist\n",
                  i4NatStaticNaptInterfaceNum,
                  u4NatStaticNaptLocalIp,
                  i4NatStaticNaptStartLocalPort,
                  i4NatStaticNaptEndLocalPort, i4NatStaticNaptProtocolNumber);
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2NatStaticNaptTranslatedLocalPort
 Input       :  The Indices
                NatStaticNaptInterfaceNum
                NatStaticNaptLocalIp
                NatStaticNaptStartLocalPort
                NatStaticNaptEndLocalPort
                NatStaticNaptProtocolNumber

                The Object 
                testValNatStaticNaptTranslatedLocalPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatStaticNaptTranslatedLocalPort (UINT4 *pu4ErrorCode,
                                           INT4 i4NatStaticNaptInterfaceNum,
                                           UINT4 u4NatStaticNaptLocalIp,
                                           INT4 i4NatStaticNaptStartLocalPort,
                                           INT4 i4NatStaticNaptEndLocalPort,
                                           INT4 i4NatStaticNaptProtocolNumber,
                                           INT4
                                           i4TestValNatStaticNaptTranslatedLocalPort)
{
    tInterfaceInfo     *pIfNode = NULL;
    tStaticNaptEntry   *pStaticNaptEntryNode = NULL;

    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptInterfaceNum = %d\n",
              i4NatStaticNaptInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptLocalIp = %u\n",
              u4NatStaticNaptLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptStartLocalPort = %d\n",
              i4NatStaticNaptStartLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptEndLocalPort = %d\n",
              i4NatStaticNaptEndLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptProtocolNumber = %d\n",
              i4NatStaticNaptProtocolNumber);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptTranslatedLocPort = %d\n",
              i4TestValNatStaticNaptTranslatedLocalPort);
    if ((i4NatStaticNaptInterfaceNum > NAT_ZERO)
        && (i4NatStaticNaptInterfaceNum <= NAT_MAX_NUM_IF))
    {
        if ((i4TestValNatStaticNaptTranslatedLocalPort <= NAT_ZERO) ||
            (i4TestValNatStaticNaptTranslatedLocalPort > NAT_MAX_PORT_NUMBER))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    if (i4NatStaticNaptInterfaceNum > NAT_MAX_NUM_IF)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIfNode = gapNatIfTable[i4NatStaticNaptInterfaceNum];

    if (pIfNode != NULL)
    {
        /* In this table, the indices are wrong. Actually it should be Global IP
         * and Global Port, but we are having Local IP and Local Port.
         * So we are putting a temporary fix so that, assumin, global IP will
         * always be, WAN IP, we should not have same global port for two different
         * entries */
        TMO_SLL_Scan (&pIfNode->StaticNaptList, pStaticNaptEntryNode,
                      tStaticNaptEntry *)
        {
            if ((u4NatStaticNaptLocalIp ==
                 pStaticNaptEntryNode->u4LocIpAddr)
                && (i4NatStaticNaptStartLocalPort ==
                    (INT4) pStaticNaptEntryNode->u2StartLocPort)
                && (i4NatStaticNaptEndLocalPort ==
                    (INT4) pStaticNaptEntryNode->u2EndLocPort)
                && ((i4NatStaticNaptProtocolNumber ==
                     pStaticNaptEntryNode->u2ProtocolNumber) ||
                    (i4NatStaticNaptProtocolNumber == NAT_PROTO_ANY)))
            {
                if (pStaticNaptEntryNode->i4RowStatus == NAT_STATUS_ACTIVE)
                {
                    /* Entry is active , can not set the value */
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
        }
    }

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    NAT_TRC (NAT_TRC_ON, "Exiting with SUCCESS \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2NatStaticNaptDescription
 Input       :  The Indices
                NatStaticNaptInterfaceNum
                NatStaticNaptLocalIp
                NatStaticNaptStartLocalPort
                NatStaticNaptEndLocalPort
                NatStaticNaptProtocolNumber

                The Object 
                testValNatStaticNaptDescription
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatStaticNaptDescription (UINT4 *pu4ErrorCode,
                                   INT4 i4NatStaticNaptInterfaceNum,
                                   UINT4 u4NatStaticNaptLocalIp,
                                   INT4 i4NatStaticNaptStartLocalPort,
                                   INT4 i4NatStaticNaptEndLocalPort,
                                   INT4 i4NatStaticNaptProtocolNumber,
                                   tSNMP_OCTET_STRING_TYPE
                                   * pTestValNatStaticNaptDescription)
{
    tInterfaceInfo     *pIfNode = NULL;
    tStaticNaptEntry   *pStaticNaptEntryNode = NULL;
    INT4                i4IfNum = NAT_ZERO;

    if (OSIX_FAILURE ==
        SNMPCheckForNVTChars (pTestValNatStaticNaptDescription->pu1_OctetList,
                              pTestValNatStaticNaptDescription->i4_Length))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((pTestValNatStaticNaptDescription->i4_Length
         >= NAT_NAPT_ENTRY_DESCRN_LEN)
        || (pTestValNatStaticNaptDescription->i4_Length <= NAT_ZERO))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    i4IfNum = i4NatStaticNaptInterfaceNum;
    pIfNode = gapNatIfTable[i4IfNum];
    if (i4IfNum <= NAT_MAX_NUM_IF)
    {
        if (gapNatIfTable[i4IfNum] != NULL)
        {
            TMO_SLL_Scan (&pIfNode->StaticNaptList, pStaticNaptEntryNode,
                          tStaticNaptEntry *)
            {
                if ((u4NatStaticNaptLocalIp ==
                     pStaticNaptEntryNode->u4LocIpAddr)
                    && (i4NatStaticNaptStartLocalPort ==
                        (INT4) pStaticNaptEntryNode->u2StartLocPort)
                    && (i4NatStaticNaptEndLocalPort ==
                        (INT4) pStaticNaptEntryNode->u2EndLocPort)
                    && (i4NatStaticNaptProtocolNumber ==
                        pStaticNaptEntryNode->u2ProtocolNumber))
                {
                    if (pStaticNaptEntryNode->i4RowStatus == NAT_STATUS_ACTIVE)
                    {
                        /* Entry is active , can not set the value */
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        return SNMP_FAILURE;
                    }
                }
            }
        }
    }

    return SNMP_SUCCESS;
}

/* UPNP NAPT TIMER */
/****************************************************************************
 Function    :  nmhTestv2NatStaticNaptLeaseDuration
 Input       :  The Indices
                NatStaticNaptInterfaceNum
                NatStaticNaptLocalIp
                NatStaticNaptStartLocalPort
                NatStaticNaptEndLocalPort
                NatStaticNaptTranslatedLocalPort
                NatStaticNaptProtocolNumber

                The Object 
                testValNatStaticNaptLeaseDuration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatStaticNaptLeaseDuration (UINT4 *pu4ErrorCode,
                                     INT4 i4NatStaticNaptInterfaceNum,
                                     UINT4 u4NatStaticNaptLocalIp,
                                     INT4 i4NatStaticNaptStartLocalPort,
                                     INT4 i4NatStaticNaptEndLocalPort,
                                     INT4 i4NatStaticNaptProtocolNumber,
                                     INT4 i4TestValNatStaticNaptLeaseDuration)
{
    UNUSED_PARAM (i4NatStaticNaptInterfaceNum);
    UNUSED_PARAM (u4NatStaticNaptLocalIp);
    UNUSED_PARAM (i4NatStaticNaptStartLocalPort);
    UNUSED_PARAM (i4NatStaticNaptEndLocalPort);
    UNUSED_PARAM (i4NatStaticNaptProtocolNumber);

    if ((i4TestValNatStaticNaptLeaseDuration < NAT_ZERO) ||
        ((UINT4) i4TestValNatStaticNaptLeaseDuration >
         NAT_UPNP_MAX_LEASE_DURATION - NAT_ONE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    return (SNMP_SUCCESS);
}

/* END */
/****************************************************************************
 Function    :  nmhTestv2NatStaticNaptEntryStatus
 Input       :  The Indices
                NatStaticNaptInterfaceNum
                NatStaticNaptLocalIp
                NatStaticNaptStartLocalPort
                NatStaticNaptEndLocalPort
                NatStaticNaptProtocolNumber

                The Object 
                testValNatStaticNaptEntryStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatStaticNaptEntryStatus (UINT4 *pu4ErrorCode,
                                   INT4 i4NatStaticNaptInterfaceNum,
                                   UINT4 u4NatStaticNaptLocalIp,
                                   INT4 i4NatStaticNaptStartLocalPort,
                                   INT4 i4NatStaticNaptEndLocalPort,
                                   INT4 i4NatStaticNaptProtocolNumber,
                                   INT4 i4TestValNatStaticNaptEntryStatus)
{
    tInterfaceInfo     *pIfNode = NULL;
    tStaticEntryNode   *pStaticEntryNode = NULL;
    tStaticNaptEntry   *pStaticNaptEntryNode = NULL;
    tStaticNaptEntry   *pTmpStaticNaptEntryNode = NULL;
    tStaticNaptEntry   *pCurStaticNaptEntryNode = NULL;
    INT4                i4IfNum = NAT_ZERO;
    INT4                i4NatStaticNaptEntryStatus = NAT_ZERO;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptInterfaceNum = %d\n",
              i4NatStaticNaptInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptLocalIp = %u\n",
              u4NatStaticNaptLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptStartLocalPort = %d\n",
              i4NatStaticNaptStartLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptEndLocalPort = %d\n",
              i4NatStaticNaptEndLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptProtocolNumber = %d\n",
              i4NatStaticNaptProtocolNumber);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticNaptEntryStatus = %d\n",
              i4TestValNatStaticNaptEntryStatus);

    if ((i4NatStaticNaptInterfaceNum < NAT_ZERO) ||
        (i4NatStaticNaptInterfaceNum > NAT_MAX_NUM_IF))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    i4IfNum = i4NatStaticNaptInterfaceNum;
    pIfNode = gapNatIfTable[i4IfNum];

    switch (i4TestValNatStaticNaptEntryStatus)
    {
        case NAT_STATUS_CREATE_AND_WAIT:
        case NAT_STATUS_CREATE_AND_GO:

            if (pIfNode != NULL)
            {
                TMO_SLL_Scan (&pIfNode->StaticNaptList, pTmpStaticNaptEntryNode,
                              tStaticNaptEntry *)
                {
                    if ((pTmpStaticNaptEntryNode->u4LocIpAddr ==
                         u4NatStaticNaptLocalIp)
                        && ((INT4) pTmpStaticNaptEntryNode->u2StartLocPort ==
                            i4NatStaticNaptStartLocalPort)
                        && ((INT4) pTmpStaticNaptEntryNode->u2EndLocPort ==
                            i4NatStaticNaptEndLocalPort)
                        && (((INT4) pTmpStaticNaptEntryNode->u2ProtocolNumber
                             == i4NatStaticNaptProtocolNumber)
                            || (pTmpStaticNaptEntryNode->u2ProtocolNumber
                                == NAT_PROTO_ANY)
                            || (NAT_PROTO_ANY ==
                                i4NatStaticNaptProtocolNumber)))
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        NAT_TRC (NAT_TRC_ON, "Row exists - Create Failed\n");
                        MOD_TRC_ARG5 (gu4NatTrc, MGMT_TRC, "NAT",
                                      "The row of Static Napt Table of index "
                                      "%ld %u %ld %ld and %ld already exist\n",
                                      i4IfNum, u4NatStaticNaptLocalIp,
                                      i4NatStaticNaptStartLocalPort,
                                      i4NatStaticNaptEndLocalPort,
                                      i4NatStaticNaptProtocolNumber);
                        return SNMP_FAILURE;
                    }
                }
            }
            break;

        case NAT_STATUS_ACTIVE:

            if (pIfNode != NULL)
            {
                /* Get the current entry */
                TMO_SLL_Scan (&pIfNode->StaticNaptList, pTmpStaticNaptEntryNode,
                              tStaticNaptEntry *)
                {
                    if ((pTmpStaticNaptEntryNode->u4LocIpAddr ==
                         u4NatStaticNaptLocalIp)
                        && ((INT4) pTmpStaticNaptEntryNode->u2StartLocPort ==
                            i4NatStaticNaptStartLocalPort)
                        && ((INT4) pTmpStaticNaptEntryNode->u2EndLocPort ==
                            i4NatStaticNaptEndLocalPort)
                        && ((INT4) pTmpStaticNaptEntryNode->u2ProtocolNumber ==
                            i4NatStaticNaptProtocolNumber))
                    {
                        pCurStaticNaptEntryNode = pTmpStaticNaptEntryNode;
                        break;
                    }
                }
                if (pCurStaticNaptEntryNode == NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    return SNMP_FAILURE;
                }
                /* Check if the Translated IP, 
                 * Port combination of the current entry
                 * is the same as that of an existing entry */
                TMO_SLL_Scan (&pIfNode->StaticNaptList, pStaticNaptEntryNode,
                              tStaticNaptEntry *)
                {
                    if (((pStaticNaptEntryNode->u2TranslatedLocPort ==
                          pCurStaticNaptEntryNode->u2TranslatedLocPort) &&
                         (pStaticNaptEntryNode->u4TranslatedLocIpAddr ==
                          pCurStaticNaptEntryNode->u4TranslatedLocIpAddr)) &&
                        ((pCurStaticNaptEntryNode != pStaticNaptEntryNode)))
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        return SNMP_FAILURE;
                    }
                }
            }
            break;

        case NAT_STATUS_NOT_IN_SERVICE:

        case NAT_STATUS_DESTROY:

            if (nmhGetNatStaticNaptEntryStatus (i4NatStaticNaptInterfaceNum,
                                                u4NatStaticNaptLocalIp,
                                                i4NatStaticNaptStartLocalPort,
                                                i4NatStaticNaptEndLocalPort,
                                                i4NatStaticNaptProtocolNumber,
                                                &i4NatStaticNaptEntryStatus) ==
                SNMP_SUCCESS)
            {
                MOD_TRC_ARG5 (gu4NatTrc, MGMT_TRC, "NAT",
                              "The row of StaticNapt Table "
                              "of  index %d, %u %d %d and %d    exist\n",
                              i4NatStaticNaptInterfaceNum,
                              u4NatStaticNaptLocalIp,
                              i4NatStaticNaptStartLocalPort,
                              i4NatStaticNaptEndLocalPort,
                              i4NatStaticNaptProtocolNumber);
                NAT_TRC (NAT_TRC_ON, "Exiting with SUCCESS: Entry found \n");
                return SNMP_SUCCESS;
            }
            else
            {

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                MOD_TRC_ARG5 (gu4NatTrc, MGMT_TRC, "NAT",
                              "The row of StaticNapt Table of  index %d,"
                              " %u %d %d and %u does not exist\n",
                              i4NatStaticNaptInterfaceNum,
                              u4NatStaticNaptLocalIp,
                              i4NatStaticNaptStartLocalPort,
                              i4NatStaticNaptEndLocalPort,
                              i4NatStaticNaptProtocolNumber);
                return SNMP_FAILURE;
            }

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    pIfNode = gapNatIfTable[i4NatStaticNaptInterfaceNum];

    if (pIfNode != NULL)
    {
        if ((pIfNode->StaticNaptList).u4_Count >= NAT_STATIC_NAPT_BLOCKS)
        {
            *pu4ErrorCode = SNMP_ERR_RESOURCE_UNAVAILABLE;
            return SNMP_FAILURE;
        }
    }
    /*
     * Validating the Local IP address .Here the address type used
     * is NAT_GLOBAL because IP address with host Id all zero
     * (like -20.0.0.0 ) is not allowed in the StaticNapt Table.
     */

    if ((NatValidateIpAddress (u4NatStaticNaptLocalIp, NAT_GLOBAL)
         == NAT_SUCCESS)
        && (i4NatStaticNaptStartLocalPort != NAT_ZERO)
        && (i4NatStaticNaptEndLocalPort != NAT_ZERO)
        && ((NAT_TCP == i4NatStaticNaptProtocolNumber) ||
            (NAT_UDP == i4NatStaticNaptProtocolNumber) ||
            (NAT_PROTO_ANY == i4NatStaticNaptProtocolNumber)))
    {
        /* Search for the static local ip address in the same interface */
        pStaticEntryNode = NULL;

        if (pIfNode != NULL)
        {

            TMO_SLL_Scan (&pIfNode->StaticList, pStaticEntryNode,
                          tStaticEntryNode *)
            {
                if (pStaticEntryNode->u4LocIpAddr == u4NatStaticNaptLocalIp)
                {
                    if (pStaticEntryNode->i4RowStatus == NAT_STATUS_ACTIVE)
                    {
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                                      " Local Address is already assigned "
                                      "to %d and of index %d in static Table \n",
                                      pStaticEntryNode->
                                      u4TranslatedLocIpAddr,
                                      i4NatStaticNaptInterfaceNum);
                        return SNMP_FAILURE;
                    }
                    else
                    {
                        break;
                    }
                }
            }

            /* Check whether the portrange overlaps */
            TMO_SLL_Scan (&pIfNode->StaticNaptList, pStaticNaptEntryNode,
                          tStaticNaptEntry *)
            {
                if ((pStaticNaptEntryNode->u2ProtocolNumber ==
                     NAT_PROTO_ANY) ||
                    (pStaticNaptEntryNode->u2ProtocolNumber ==
                     (UINT2) i4NatStaticNaptProtocolNumber) ||
                    ((UINT2) i4NatStaticNaptProtocolNumber == NAT_PROTO_ANY))
                {
                    if ((i4NatStaticNaptStartLocalPort >
                         i4NatStaticNaptEndLocalPort)
                        || (!(((i4NatStaticNaptStartLocalPort <
                                pStaticNaptEntryNode->u2StartLocPort) &&
                               (i4NatStaticNaptEndLocalPort <
                                pStaticNaptEntryNode->u2StartLocPort)) ||
                              ((i4NatStaticNaptStartLocalPort >
                                pStaticNaptEntryNode->u2StartLocPort) &&
                               (i4NatStaticNaptStartLocalPort >
                                pStaticNaptEntryNode->u2EndLocPort)))))
                    {
                        /* if it is not a virtual server entry,
                         * we should not allow
                         * overlapping of ports. If it is a virtual
                         * server entry, 
                         * we can have multiple local hosts, supporting same 
                         * applications (local ports). In case of 
                         * Portrange , this 
                         * range is taken as global range and 
                         * not local port range,
                         * The mib should be updated to correct this*/
                        if (i4NatStaticNaptStartLocalPort !=
                            i4NatStaticNaptEndLocalPort)
                        {
                            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                            return SNMP_FAILURE;
                        }
                    }
                }
            }
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : NatIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceNatIfTable
 Input       :  The Indices
                NatIfInterfaceNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceNatIfTable (INT4 i4NatIfInterfaceNumber)
{

    NAT_TRC (NAT_TRC_ON,
             "Entering the function: ValidateIndexInstance for Nat IfTable\n");
    NAT_DBG1 (NAT_DBG_LLR, "NatIfInterfaceNumber = %d\n",
              i4NatIfInterfaceNumber);
    if (SecUtilValidateIfIndex ((UINT2) i4NatIfInterfaceNumber) == OSIX_SUCCESS)
    {
        if ((gapNatIfTable[i4NatIfInterfaceNumber] != NULL) &&
            (gapNatIfTable[i4NatIfInterfaceNumber]->i4RowStatus !=
             NAT_STATUS_INVLD))
        {
            NAT_TRC (NAT_TRC_ON, "Exiting with SUCCESS: Entry found \n");
            MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT",
                          "The row of Interface Table of  index %d  exist\n",
                          i4NatIfInterfaceNumber);
            return SNMP_SUCCESS;
        }
    }
    NAT_TRC (NAT_TRC_ON, "No Such Entry exists Test Failed\n");
    MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Interface Table of  index %d does not exist\n",
                  i4NatIfInterfaceNumber);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexNatIfTable
 Input       :  The Indices
                NatIfInterfaceNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexNatIfTable (INT4 *pi4NatIfInterfaceNumber)
{

    INT4                i4IfNum = NAT_ZERO;

    i4IfNum = NAT_ONE;
    while (i4IfNum <= NAT_MAX_NUM_IF)
    {
        if ((gapNatIfTable[i4IfNum] != NULL) &&
            (gapNatIfTable[i4IfNum]->i4RowStatus != NAT_STATUS_INVLD))
        {
            *pi4NatIfInterfaceNumber = i4IfNum;
            NAT_DBG1 (NAT_DBG_LLR, "NatIfInterfaceNumber = %d\n",
                      *pi4NatIfInterfaceNumber);
            return SNMP_SUCCESS;
        }
        i4IfNum++;
    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexNatIfTable
 Input       :  The Indices
                NatIfInterfaceNumber
                nextNatIfInterfaceNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexNatIfTable (INT4 i4NatIfInterfaceNumber,
                           INT4 *pi4NextNatIfInterfaceNumber)
{

    INT4                i4IfNum = NAT_ZERO;

    NAT_DBG1 (NAT_DBG_LLR, "NatIfInterfaceNumber = %d\n",
              itIfTable4NatIfInterfaceNumber);
    if (i4NatIfInterfaceNumber > NAT_MAX_NUM_IF)
    {
        NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
        return SNMP_FAILURE;
    }
    i4IfNum = i4NatIfInterfaceNumber + NAT_ONE;
    while (i4IfNum <= NAT_MAX_NUM_IF)
    {
        if ((gapNatIfTable[i4IfNum] != NULL) &&
            (gapNatIfTable[i4IfNum]->i4RowStatus != NAT_STATUS_INVLD))
        {
            *pi4NextNatIfInterfaceNumber = i4IfNum;
            NAT_DBG1 (NAT_DBG_LLR, "NatNextIfInterfaceNumber = %d\n",
                      *pi4NextNatIfInterfaceNumber);
            return SNMP_SUCCESS;
        }
        i4IfNum++;
    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetNatIfNat
 Input       :  The Indices
                NatIfInterfaceNumber

                The Object 
                retValNatIfNat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatIfNat (INT4 i4NatIfInterfaceNumber, INT4 *pi4RetValNatIfNat)
{

    INT4                i4IfNumfNat = NAT_ZERO;

    NAT_DBG1 (NAT_DBG_LLR, "NatIfInterfaceNumber = %d\n",
              i4NatIfInterfaceNumber);
    if ((gapNatIfTable[i4NatIfInterfaceNumber] != NULL)
        && (gapNatIfTable[i4NatIfInterfaceNumber]->i4RowStatus !=
            NAT_STATUS_INVLD))
    {
        i4IfNumfNat = gapNatIfTable[i4NatIfInterfaceNumber]->u1NatEnable;
        *pi4RetValNatIfNat = i4IfNumfNat;
        NAT_DBG1 (NAT_DBG_LLR, "NatIfNat = %d\n", *pi4RetValNatIfNat);
        return SNMP_SUCCESS;
    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNatIfNapt
 Input       :  The Indices
                NatIfInterfaceNumber

                The Object 
                retValNatIfNapt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatIfNapt (INT4 i4NatIfInterfaceNumber, INT4 *pi4RetValNatIfNapt)
{

    INT4                i4IfNumfNapt = NAT_ZERO;

    NAT_DBG1 (NAT_DBG_LLR, "NatIfInterfaceNumber = %d\n",
              i4NatIfInterfaceNumber);
    if ((gapNatIfTable[i4NatIfInterfaceNumber] != NULL)
        && (gapNatIfTable[i4NatIfInterfaceNumber]->i4RowStatus !=
            NAT_STATUS_INVLD))
    {
        i4IfNumfNapt = gapNatIfTable[i4NatIfInterfaceNumber]->u1NaptEnable;
        *pi4RetValNatIfNapt = i4IfNumfNapt;
        NAT_DBG1 (NAT_DBG_LLR, "NatIfNapt = %d\n", *pi4RetValNatIfNapt);
        return SNMP_SUCCESS;
    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNatIfTwoWayNat
 Input       :  The Indices
                NatIfInterfaceNumber

                The Object 
                retValNatIfTwoWayNat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatIfTwoWayNat (INT4 i4NatIfInterfaceNumber,
                      INT4 *pi4RetValNatIfTwoWayNat)
{

    INT4                i4IfNumfTwoWayNat = NAT_ZERO;

    NAT_DBG1 (NAT_DBG_LLR, "NatIfInterfaceNumber = %d\n",
              i4NatIfInterfaceNumber);
    if ((gapNatIfTable[i4NatIfInterfaceNumber] != NULL)
        && (gapNatIfTable[i4NatIfInterfaceNumber]->i4RowStatus !=
            NAT_STATUS_INVLD))
    {
        i4IfNumfTwoWayNat =
            gapNatIfTable[i4NatIfInterfaceNumber]->u1TwoWayNatEnable;
        *pi4RetValNatIfTwoWayNat = i4IfNumfTwoWayNat;
        NAT_DBG1 (NAT_DBG_LLR, "NatIfTwoWayNat = %d\n",
                  *pi4RetValNatIfTwoWayNat);
        return SNMP_SUCCESS;
    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNatIfEntryStatus
 Input       :  The Indices
                NatIfInterfaceNumber

                The Object 
                retValNatIfEntryStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatIfEntryStatus (INT4 i4NatIfInterfaceNumber,
                        INT4 *pi4RetValNatIfEntryStatus)
{
    NAT_DBG1 (NAT_DBG_LLR, "NatIfInterfaceNumber = %d\n",
              i4NatIfInterfaceNumber);

    if ((gapNatIfTable[i4NatIfInterfaceNumber] != NULL) &&
        (gapNatIfTable[i4NatIfInterfaceNumber]->i4RowStatus
         != NAT_STATUS_INVLD))
    {
        *pi4RetValNatIfEntryStatus =
            gapNatIfTable[i4NatIfInterfaceNumber]->i4RowStatus;
        NAT_DBG1 (NAT_DBG_LLR, "NatIfEntryStatus = %d\n",
                  *pi4RetValNatIfEntryStatus);
        return SNMP_SUCCESS;
    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetNatIfNat
 Input       :  The Indices
                NatIfInterfaceNumber

                The Object 
                setValNatIfNat
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatIfNat (INT4 i4NatIfInterfaceNumber, INT4 i4SetValNatIfNat)
{
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    INT4                i4IfNum = NAT_ZERO;

    NAT_DBG1 (NAT_DBG_LLR, "NatIfInterfaceNumber = %d\n",
              i4NatIfInterfaceNumber);
    NAT_DBG1 (NAT_DBG_LLR, "NatIfNat = %d\n", i4SetValNatIfNat);

    RM_GET_SEQ_NUM (&u4SeqNum);

    i4IfNum = i4NatIfInterfaceNumber;
    if (gapNatIfTable[i4IfNum] != NULL)
    {
        if ((i4SetValNatIfNat == NAT_DISABLE)
            && (gapNatIfTable[i4IfNum]->u1NatEnable == NAT_ENABLE))
        {
            /*Delete all the Session entries created through this interface */
            NatDeleteDynamicEntry ((UINT4) i4IfNum);
            gapNatIfTable[i4IfNum]->u1NaptEnable = NAT_DISABLE;
            gapNatIfTable[i4IfNum]->u1TwoWayNatEnable = NAT_DISABLE;
        }
        gapNatIfTable[i4IfNum]->u1NatEnable = (UINT1) (i4SetValNatIfNat);
        gapNatIfTable[i4IfNum]->i4RowStatus = NAT_STATUS_NOT_IN_SERVICE;
        /* if (NatIfTable[i4IfNum]->i4RowStatus == NAT_STATUS_INVLD)
           NatIfTable[i4IfNum]->i4RowStatus = NAT_STATUS_NOT_READY; */
    }
    if (i4SetValNatIfNat == NAT_ENABLE)
    {
        nmhSetNatIfTwoWayNat (i4IfNum, i4SetValNatIfNat);
    }

    /* If Interface entry is absent create one */
    if (gapNatIfTable[i4IfNum] == NULL)
    {
        if (NAT_MEM_ALLOCATE
            (NAT_INTERFACE_INFO_POOL_ID,
             gapNatIfTable[i4IfNum], tInterfaceInfo) == NULL)
        {
            MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT", "Memory Allocation Failed\n");
            return SNMP_FAILURE;
        }

        gapNatIfTable[i4IfNum]->u1NatEnable = (UINT1) (i4SetValNatIfNat);
        gapNatIfTable[i4IfNum]->u1NaptEnable = NAT_DISABLE;
        gapNatIfTable[i4IfNum]->u1TwoWayNatEnable = NAT_DISABLE;
        gapNatIfTable[i4IfNum]->i4RowStatus = NAT_STATUS_NOT_IN_SERVICE;
        /* MSAD ADD */
        TMO_SLL_Init (&(gapNatIfTable[i4IfNum]->TranslatedLocIpList));
        TMO_SLL_Init (&gapNatIfTable[i4IfNum]->StaticList);
    }

    MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT",
                  "\n NAT is enabled on interface %d", i4IfNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatIfNat, u4SeqNum, FALSE, NatLock,
                          NatUnLock, NAT_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4NatIfInterfaceNumber,
                      i4SetValNatIfNat));
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetNatIfNapt
 Input       :  The Indices
                NatIfInterfaceNumber

                The Object 
                setValNatIfNapt
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatIfNapt (INT4 i4NatIfInterfaceNumber, INT4 i4SetValNatIfNapt)
{

    INT4                i4IfNum = NAT_ZERO;
    UINT4               u4WanIfIpAddr = NAT_ZERO;
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT1               u1CurrNaptStatus = NAT_ZERO;

    NAT_DBG1 (NAT_DBG_LLR, "NatIfInterfaceNumber = %d\n",
              i4NatIfInterfaceNumber);
    NAT_DBG1 (NAT_DBG_LLR, "NatIfInterfaceNumber = %d\n",
              i4NatIfInterfaceNumber);
    NAT_DBG1 (NAT_DBG_LLR, "NatIfNapt = %d\n", i4SetValNatIfNapt);

    RM_GET_SEQ_NUM (&u4SeqNum);
    i4IfNum = i4NatIfInterfaceNumber;
    u4WanIfIpAddr = NatGetWanInterfaceIpAddr ((UINT4) i4IfNum);

    if (gapNatIfTable[i4IfNum] != NULL)
    {

        if (gapNatIfTable[i4IfNum]->i4RowStatus == NAT_STATUS_ACTIVE)
        {
            NAT_TRC (NAT_TRC_ON, "Row is Active, Can not Set\n");
            return SNMP_FAILURE;
        }
        else
        {
            u1CurrNaptStatus = gapNatIfTable[i4IfNum]->u1NaptEnable;
            gapNatIfTable[i4IfNum]->u1NaptEnable = (UINT1) i4SetValNatIfNapt;
            if (gapNatIfTable[i4IfNum]->i4RowStatus == NAT_STATUS_INVLD)
            {
                gapNatIfTable[i4IfNum]->i4RowStatus = NAT_STATUS_NOT_READY;
            }
        }

        /* Flush All the existing sessions on this interface */
        if (i4SetValNatIfNapt != u1CurrNaptStatus)
        {
            NatDeleteDynamicEntry ((UINT4) i4IfNum);
            /*
             * Dynamic Entries Deletion will not remove correspondig
             * Partial Entry. So we go ahead in deleting Partial Entry
             * Which belongs to this interface.
             */
            NatDeletePartialEntry ((UINT4) i4IfNum);
        }

    }
    if (i4SetValNatIfNapt == NAT_ENABLE)
    {
        if (NatAutoIpAddressConfig ((UINT4) i4IfNum, u4WanIfIpAddr,
                                    NAT_PPP_NAPT_IPADDR_ACTIVE) != SUCCESS)
        {
            CLI_SET_ERR (CLI_NAT_ERR_AUTOIP_CFG_FAILURE);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (NatAutoIpAddressConfig ((UINT4) i4IfNum, u4WanIfIpAddr,
                                    NAT_PPP_NAPT_IPADDR_DESTROY) != SUCCESS)
        {
            CLI_SET_ERR (CLI_NAT_ERR_AUTOIP_CFG_FAILURE);
            return CLI_FAILURE;
        }
        NatDeleteDynamicEntry ((UINT4) i4IfNum);
    }

    if (gapNatIfTable[i4IfNum] == NULL)
    {
        if (NAT_MEM_ALLOCATE
            (NAT_INTERFACE_INFO_POOL_ID,
             gapNatIfTable[i4IfNum], tInterfaceInfo) == NULL)
        {
            NAT_TRC (NAT_TRC_ON, "Memory Allocation Failed\n");
            return SNMP_FAILURE;
        }
        gapNatIfTable[i4IfNum]->u1NaptEnable = (UINT1) (i4SetValNatIfNapt);
        gapNatIfTable[i4IfNum]->u1TwoWayNatEnable = NAT_DISABLE;
        gapNatIfTable[i4IfNum]->i4RowStatus = NAT_STATUS_NOT_READY;
        /* MSAD ADD */
        TMO_SLL_Init (&(gapNatIfTable[i4IfNum]->TranslatedLocIpList));
        TMO_SLL_Init (&gapNatIfTable[i4IfNum]->StaticList);
    }
    if (i4SetValNatIfNapt == NAT_ENABLE)
    {
        if (gapNatIfTable[i4IfNum]->u1NatEnable == NAT_DISABLE)
        {
            gapNatIfTable[i4IfNum]->u1NatEnable = (UINT1) (i4SetValNatIfNapt);
        }
        NatCheckDynamicList ((UINT4) i4IfNum);

    }

    MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT",
                  "\n NAPT is enabled on interface %d", i4IfNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatIfNapt, u4SeqNum, FALSE, NatLock,
                          NatUnLock, NAT_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4NatIfInterfaceNumber,
                      i4SetValNatIfNapt));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetNatIfTwoWayNat
 Input       :  The Indices
                NatIfInterfaceNumber

                The Object 
                setValNatIfTwoWayNat
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatIfTwoWayNat (INT4 i4NatIfInterfaceNumber, INT4 i4SetValNatIfTwoWayNat)
{
    INT4                i4IfNum = NAT_ZERO;
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    NAT_DBG1 (NAT_DBG_LLR, "NatIfInterfaceNumber = %d\n",
              i4NatIfInterfaceNumber);
    NAT_DBG1 (NAT_DBG_LLR, "NatIfInterfaceNumber = %d\n",
              i4NatIfInterfaceNumber);
    NAT_DBG1 (NAT_DBG_LLR, "NatIfTwoWayNat = %d\n", i4SetValNatIfTwoWayNat);

    RM_GET_SEQ_NUM (&u4SeqNum);
    i4IfNum = i4NatIfInterfaceNumber;
    if (gapNatIfTable[i4IfNum] != NULL)
    {

        if (gapNatIfTable[i4IfNum]->i4RowStatus == NAT_STATUS_ACTIVE)
        {
            NAT_TRC (NAT_TRC_ON, "Row is Active, Can not Set\n");
            return SNMP_FAILURE;
        }
        else
        {
            gapNatIfTable[i4IfNum]->u1TwoWayNatEnable =
                (UINT1) (i4SetValNatIfTwoWayNat);
            if (gapNatIfTable[i4IfNum]->i4RowStatus == NAT_STATUS_INVLD)
            {
                gapNatIfTable[i4IfNum]->i4RowStatus = NAT_STATUS_NOT_READY;
            }

        }
    }

    if (gapNatIfTable[i4IfNum] == NULL)
    {
        if (NAT_MEM_ALLOCATE
            (NAT_INTERFACE_INFO_POOL_ID,
             gapNatIfTable[i4IfNum], tInterfaceInfo) == NULL)
        {
            NAT_TRC (NAT_TRC_ON, "Memory Allocation Failed\n");
            return SNMP_FAILURE;
        }
        gapNatIfTable[i4IfNum]->u1NaptEnable = NAT_DISABLE;
        gapNatIfTable[i4IfNum]->u1TwoWayNatEnable =
            (UINT1) (i4SetValNatIfTwoWayNat);
        gapNatIfTable[i4IfNum]->i4RowStatus = NAT_STATUS_NOT_READY;
        /* MSAD ADD */
        TMO_SLL_Init (&(gapNatIfTable[i4IfNum]->TranslatedLocIpList));
        TMO_SLL_Init (&gapNatIfTable[i4IfNum]->StaticList);
    }

    if (i4SetValNatIfTwoWayNat == NAT_ENABLE)
    {
        if (gapNatIfTable[i4IfNum]->u1NatEnable == NAT_DISABLE)
        {
            gapNatIfTable[i4IfNum]->u1NatEnable = NAT_ENABLE;
        }
    }

    MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT",
                  "\n Two Way NAT is enabled on interface%d", i4IfNum);
    NAT_TRC (NAT_TRC_ON, "Successfully Set the Value\n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatIfTwoWayNat, u4SeqNum, FALSE,
                          NatLock, NatUnLock, NAT_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4NatIfInterfaceNumber,
                      i4SetValNatIfTwoWayNat));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetNatIfEntryStatus
 Input       :  The Indices
                NatIfInterfaceNumber

                The Object 
                setValNatIfEntryStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatIfEntryStatus (INT4 i4NatIfInterfaceNumber,
                        INT4 i4SetValNatIfEntryStatus)
{

    INT1                i1RetStatus = NAT_ZERO;
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    NAT_DBG1 (NAT_DBG_LLR, "NatIfInterfaceNumber = %d\n",
              i4NatIfInterfaceNumber);
    NAT_DBG1 (NAT_DBG_LLR, "NatIfEntryStatus = %d\n", i4SetValNatIfEntryStatus);

    RM_GET_SEQ_NUM (&u4SeqNum);

    switch (i4SetValNatIfEntryStatus)
    {

        case NAT_STATUS_ACTIVE:

            i1RetStatus = NatIftActive (i4NatIfInterfaceNumber);
            break;

        case NAT_STATUS_CREATE_AND_GO:

            i1RetStatus = NatIftCreateAndGo (i4NatIfInterfaceNumber);
            break;

        case NAT_STATUS_CREATE_AND_WAIT:

            i1RetStatus = NatIftCreateAndWait (i4NatIfInterfaceNumber);
            break;

        case NAT_STATUS_NOT_IN_SERVICE:
            i1RetStatus = NatIftNotInService (i4NatIfInterfaceNumber);
            break;

        case NAT_STATUS_DESTROY:
            i1RetStatus = NatIftDestroy (i4NatIfInterfaceNumber);
            break;

        default:
            MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT",
                     "\n Unknown Row Status Option \n");
            i1RetStatus = SNMP_FAILURE;
            break;

    }
    if (i1RetStatus == SNMP_SUCCESS)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatIfEntryStatus, u4SeqNum, TRUE,
                              NatLock, NatUnLock, NAT_ONE, i1RetStatus);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4NatIfInterfaceNumber,
                          i4SetValNatIfEntryStatus));
    }

    return i1RetStatus;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2NatIfNat
 Input       :  The Indices
                NatIfInterfaceNumber

                The Object 
                testValNatIfNat
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatIfNat (UINT4 *pu4ErrorCode,
                   INT4 i4NatIfInterfaceNumber, INT4 i4TestValNatIfNat)
{
    NAT_DBG1 (NAT_DBG_LLR, "NatIfInterfaceNumber = %d\n",
              i4NatIfInterfaceNumber);
    NAT_DBG1 (NAT_DBG_LLR, "NatIfNat = %d\n", i4TestValNatIfNat);
    if ((i4NatIfInterfaceNumber > NAT_ZERO) &&
        (i4NatIfInterfaceNumber <= NAT_MAX_NUM_IF))
    {
        if (gapNatIfTable[i4NatIfInterfaceNumber]->i4RowStatus
            == NAT_STATUS_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT",
                     "Row is Active, Can not Set\n");
            return SNMP_FAILURE;
        }

        if ((i4TestValNatIfNat == NAT_ENABLE) ||
            (i4TestValNatIfNat == NAT_DISABLE))
        {
            MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT",
                          "The row of Interface Table of  index %d  exist\n",
                          i4NatIfInterfaceNumber);
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Interface Table of  index %d does not exist\n",
                  i4NatIfInterfaceNumber);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2NatIfNapt
 Input       :  The Indices
                NatIfInterfaceNumber

                The Object 
                testValNatIfNapt
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatIfNapt (UINT4 *pu4ErrorCode,
                    INT4 i4NatIfInterfaceNumber, INT4 i4TestValNatIfNapt)
{
    NAT_DBG1 (NAT_DBG_LLR, "NatIfInterfaceNumber = %d\n",
              i4NatIfInterfaceNumber);
    NAT_DBG1 (NAT_DBG_LLR, "NatIfNapt = %d\n", i4TestValNatIfNapt);
    if ((i4NatIfInterfaceNumber > NAT_ZERO) &&
        (i4NatIfInterfaceNumber <= NAT_MAX_NUM_IF))
    {
        if ((i4TestValNatIfNapt == NAT_ENABLE) ||
            (i4TestValNatIfNapt == NAT_DISABLE))
        {
            MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT",
                          "The row of Interface Table of  index %d  exist\n",
                          i4NatIfInterfaceNumber);
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Interface Table of  index %d does not exist\n",
                  i4NatIfInterfaceNumber);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2NatIfTwoWayNat
 Input       :  The Indices
                NatIfInterfaceNumber

                The Object 
                testValNatIfTwoWayNat
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatIfTwoWayNat (UINT4 *pu4ErrorCode,
                         INT4 i4NatIfInterfaceNumber,
                         INT4 i4TestValNatIfTwoWayNat)
{
    NAT_DBG1 (NAT_DBG_LLR, "NatIfInterfaceNumber = %d\n",
              i4NatIfInterfaceNumber);
    NAT_DBG1 (NAT_DBG_LLR, "NatIfTwoWayNat = %d\n", i4TestValNatIfTwoWayNat);
    if ((i4NatIfInterfaceNumber > NAT_ZERO) &&
        (i4NatIfInterfaceNumber <= NAT_MAX_NUM_IF))
    {
        if ((i4TestValNatIfTwoWayNat == NAT_ENABLE) ||
            (i4TestValNatIfTwoWayNat == NAT_DISABLE))
        {
            MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT",
                          "The row of Interface Table of  index %d  exist\n",
                          i4NatIfInterfaceNumber);
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Interface Table of  index %d does not exist\n",
                  i4NatIfInterfaceNumber);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2NatIfEntryStatus
 Input       :  The Indices
                NatIfInterfaceNumber

                The Object 
                testValNatIfEntryStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatIfEntryStatus (UINT4 *pu4ErrorCode,
                           INT4 i4NatIfInterfaceNumber,
                           INT4 i4TestValNatIfEntryStatus)
{
    NAT_DBG1 (NAT_DBG_LLR, "NatIfInterfaceNumber = %d\n",
              i4NatIfInterfaceNumber);
    NAT_DBG1 (NAT_DBG_LLR, "NatIfEntryStatus = %d\n",
              i4TestValNatIfEntryStatus);

    if ((i4NatIfInterfaceNumber > NAT_ZERO) &&
        (i4NatIfInterfaceNumber <= NAT_MAX_NUM_IF))
    {
        switch (i4TestValNatIfEntryStatus)
        {
            case NAT_STATUS_ACTIVE:

            case NAT_STATUS_NOT_IN_SERVICE:

            case NAT_STATUS_CREATE_AND_GO:

            case NAT_STATUS_CREATE_AND_WAIT:

            case NAT_STATUS_DESTROY:
                MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT",
                              "The row of Interface Table of\
                              index %d  exist\n", i4NatIfInterfaceNumber);
                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                return SNMP_SUCCESS;

            default:
                break;
        }
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Interface Table of  index %d does not exist\n",
                  i4NatIfInterfaceNumber);
    return SNMP_FAILURE;

}

/* LOW LEVEL Routines for Table : NatIPSecSessionTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceNatIPSecSessionTable
 Input       :  The Indices
                NatIPSecSessionInterfaceNum
                NatIPSecSessionLocalIp
                NatIPSecSessionOutsideIp
                NatIPSecSessionSPIInside
                NatIPSecSessionSPIOutside
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceNatIPSecSessionTable (INT4
                                              i4NatIPSecSessionInterfaceNum,
                                              UINT4 u4NatIPSecSessionLocalIp,
                                              UINT4 u4NatIPSecSessionOutsideIp,
                                              INT4 i4NatIPSecSessionSPIInside,
                                              INT4 i4NatIPSecSessionSPIOutside)
{
    tIPSecListNode     *pIPSecListNode = NULL;

    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionInterfaceNum = %d\n",
              i4NatIPSecSessionInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionLocalIp = %x\n",
              u4NatIPSecSessionLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionOutsideIp = %x\n",
              u4NatIPSecSessionOutsideIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionSPIInside = %d\n",
              i4NatIPSecSessionSPIInside);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionSPIOutside = %d\n",
              i4NatIPSecSessionSPIOutside);

    TMO_SLL_Scan (&gNatIPSecListNode, pIPSecListNode, tIPSecListNode *)
    {
        if ((pIPSecListNode->pIPSecOutHashNode->u4IfNum ==
             (UINT4) i4NatIPSecSessionInterfaceNum) &&
            (pIPSecListNode->pIPSecOutHashNode->u4LocIpAddr ==
             u4NatIPSecSessionLocalIp) &&
            (pIPSecListNode->pIPSecOutHashNode->u4OutIpAddr ==
             u4NatIPSecSessionOutsideIp) &&
            (pIPSecListNode->pIPSecOutHashNode->u4SPIInside ==
             (UINT4) i4NatIPSecSessionSPIInside) &&
            (pIPSecListNode->pIPSecOutHashNode->pIPSecInHashNode->
             u4SPIOutside == (UINT4) i4NatIPSecSessionSPIOutside))
        {
            MOD_TRC_ARG5 (gu4NatTrc, MGMT_TRC, "NAT",
                          "The row of IPSec Session Table "
                          "of  index %x, %x, %x, %d and %d    exist\n",
                          i4NatIPSecSessionInterfaceNum,
                          u4NatIPSecSessionLocalIp, u4NatIPSecSessionOutsideIp,
                          i4NatIPSecSessionSPIInside,
                          i4NatIPSecSessionSPIOutside);
            NAT_TRC (NAT_TRC_ON, "Exiting with SUCCESS: Entry found \n");
            return SNMP_SUCCESS;
        }
    }
    NAT_TRC (NAT_TRC_ON, "No Such Instance exists Test Failed\n");
    MOD_TRC_ARG5 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of IpSecSession Table of  index %d,"
                  "%x, %x, %d and %d does not exist\n",
                  i4NatIPSecSessionInterfaceNum,
                  u4NatIPSecSessionLocalIp, u4NatIPSecSessionOutsideIp,
                  i4NatIPSecSessionSPIInside, i4NatIPSecSessionSPIOutside);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexNatIPSecSessionTable
 Input       :  The Indices
                NatIPSecSessionInterfaceNum
                NatIPSecSessionLocalIp
                NatIPSecSessionOutsideIp
                NatIPSecSessionSPIInside
                NatIPSecSessionSPIOutside
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexNatIPSecSessionTable (INT4 *pi4NatIPSecSessionInterfaceNum,
                                      UINT4 *pu4NatIPSecSessionLocalIp,
                                      UINT4 *pu4NatIPSecSessionOutsideIp,
                                      INT4 *pi4NatIPSecSessionSPIInside,
                                      INT4 *pi4NatIPSecSessionSPIOutside)
{
    tIPSecListNode     *pIPSecListNode = NULL;

    if ((pIPSecListNode =
         (tIPSecListNode *) TMO_SLL_First (&gNatIPSecListNode)) != NULL)
    {
        *pi4NatIPSecSessionInterfaceNum =
            (INT4) pIPSecListNode->pIPSecOutHashNode->u4IfNum;
        *pu4NatIPSecSessionLocalIp =
            pIPSecListNode->pIPSecOutHashNode->u4LocIpAddr;
        *pu4NatIPSecSessionOutsideIp =
            pIPSecListNode->pIPSecOutHashNode->u4OutIpAddr;
        *pi4NatIPSecSessionSPIInside =
            (INT4) pIPSecListNode->pIPSecOutHashNode->u4SPIInside;
        *pi4NatIPSecSessionSPIOutside =
            (INT4) pIPSecListNode->pIPSecOutHashNode->pIPSecInHashNode->
            u4SPIOutside;

        NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionInterfaceNum = %d\n",
                  *pi4NatIPSecSessionInterfaceNum);
        NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionLocalIp = %x\n",
                  *pu4NatIPSecSessionLocalIp);
        NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionOutsideIp = %x\n",
                  *pu4NatIPSecSessionOutsideIp);
        NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionSPIInside = %d\n",
                  *pi4NatIPSecSessionSPIInside);
        NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionSPIOutside = %d\n",
                  *pi4NatIPSecSessionSPIOutside);
        return SNMP_SUCCESS;
    }
    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexNatIPSecSessionTable
 Input       :  The Indices
                NatIPSecSessionInterfaceNum
                nextNatIPSecSessionInterfaceNum
                NatIPSecSessionLocalIp
                nextNatIPSecSessionLocalIp
                NatIPSecSessionOutsideIp
                nextNatIPSecSessionOutsideIp
                NatIPSecSessionSPIInside
                nextNatIPSecSessionSPIInside
                NatIPSecSessionSPIOutside
                nextNatIPSecSessionSPIOutside
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexNatIPSecSessionTable (INT4 i4NatIPSecSessionInterfaceNum,
                                     INT4 *pi4NextNatIPSecSessionInterfaceNum,
                                     UINT4 u4NatIPSecSessionLocalIp,
                                     UINT4 *pu4NextNatIPSecSessionLocalIp,
                                     UINT4 u4NatIPSecSessionOutsideIp,
                                     UINT4 *pu4NextNatIPSecSessionOutsideIp,
                                     INT4 i4NatIPSecSessionSPIInside,
                                     INT4 *pi4NextNatIPSecSessionSPIInside,
                                     INT4 i4NatIPSecSessionSPIOutside,
                                     INT4 *pi4NextNatIPSecSessionSPIOutside)
{
    tIPSecListNode     *pIPSecListNode = NULL;

    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionInterfaceNum = %d\n",
              i4NatIPSecSessionInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionLocalIp = %x\n",
              u4NatIPSecSessionLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionOutsideIp = %x\n",
              u4NatIPSecSessionOutsideIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionSPIInside = %d\n",
              i4NatIPSecSessionSPIInside);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionSPIOutside = %d\n",
              i4NatIPSecSessionSPIOutside);

    TMO_SLL_Scan (&gNatIPSecListNode, pIPSecListNode, tIPSecListNode *)
    {
        if ((pIPSecListNode->pIPSecOutHashNode->u4IfNum ==
             (UINT4) i4NatIPSecSessionInterfaceNum) &&
            (pIPSecListNode->pIPSecOutHashNode->u4LocIpAddr ==
             u4NatIPSecSessionLocalIp) &&
            (pIPSecListNode->pIPSecOutHashNode->u4OutIpAddr ==
             u4NatIPSecSessionOutsideIp) &&
            (pIPSecListNode->pIPSecOutHashNode->u4SPIInside ==
             (UINT4) i4NatIPSecSessionSPIInside) &&
            (pIPSecListNode->pIPSecOutHashNode->pIPSecInHashNode->
             u4SPIOutside == (UINT4) i4NatIPSecSessionSPIOutside))
        {
            break;
        }
    }
    if (pIPSecListNode != NULL)
    {
        pIPSecListNode = (tIPSecListNode *)
            TMO_SLL_Next ((tTMO_SLL *) & gNatIPSecListNode,
                          &(pIPSecListNode->IPSecList));

/*UT_FIX -START*/
        if (pIPSecListNode != NULL)
        {
/*UT_FIX -END*/
            *pi4NextNatIPSecSessionInterfaceNum =
                (INT4) pIPSecListNode->pIPSecOutHashNode->u4IfNum;
            *pu4NextNatIPSecSessionLocalIp =
                pIPSecListNode->pIPSecOutHashNode->u4LocIpAddr;
            *pu4NextNatIPSecSessionOutsideIp =
                pIPSecListNode->pIPSecOutHashNode->u4OutIpAddr;
            *pi4NextNatIPSecSessionSPIInside =
                (INT4) pIPSecListNode->pIPSecOutHashNode->u4SPIInside;
            *pi4NextNatIPSecSessionSPIOutside =
                (INT4) pIPSecListNode->pIPSecOutHashNode->pIPSecInHashNode->
                u4SPIOutside;

            NAT_DBG1 (NAT_DBG_LLR, "NextNatIPSecSessionInterfaceNum = %d\n",
                      *pi4NextNatIPSecSessionInterfaceNum);
            NAT_DBG1 (NAT_DBG_LLR, "NextNatIPSecSessionLocalIp = %x\n",
                      *pu4NextNatIPSecSessionLocalIp);
            NAT_DBG1 (NAT_DBG_LLR, "NextNatIPSecSessionOutsideIp = %x\n",
                      *pu4NextNatIPSecSessionOutsideIp);
            NAT_DBG1 (NAT_DBG_LLR, "NextNatIPSecSessionSPIInside = %d\n",
                      *pi4NextNatIPSecSessionSPIInside);
            NAT_DBG1 (NAT_DBG_LLR, "NextNatIPSecSessionSPIOutside = %d\n",
                      *pi4NextNatIPSecSessionSPIOutside);
            return SNMP_SUCCESS;
        }
    }
    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetNatIPSecSessionTranslatedLocalIp
 Input       :  The Indices
                NatIPSecSessionInterfaceNum
                NatIPSecSessionLocalIp
                NatIPSecSessionOutsideIp
                NatIPSecSessionSPIInside
                NatIPSecSessionSPIOutside

                The Object 
                retValNatIPSecSessionTranslatedLocalIp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatIPSecSessionTranslatedLocalIp (INT4 i4NatIPSecSessionInterfaceNum,
                                        UINT4 u4NatIPSecSessionLocalIp,
                                        UINT4 u4NatIPSecSessionOutsideIp,
                                        INT4 i4NatIPSecSessionSPIInside,
                                        INT4 i4NatIPSecSessionSPIOutside,
                                        UINT4
                                        *pu4RetValNatIPSecSessionTranslatedLocalIp)
{
    tIPSecListNode     *pIPSecListNode = NULL;
    TMO_SLL_Scan (&gNatIPSecListNode, pIPSecListNode, tIPSecListNode *)
    {
        if ((pIPSecListNode->pIPSecOutHashNode->u4IfNum ==
             (UINT4) i4NatIPSecSessionInterfaceNum) &&
            (pIPSecListNode->pIPSecOutHashNode->u4LocIpAddr ==
             u4NatIPSecSessionLocalIp) &&
            (pIPSecListNode->pIPSecOutHashNode->u4OutIpAddr ==
             u4NatIPSecSessionOutsideIp) &&
            (pIPSecListNode->pIPSecOutHashNode->u4SPIInside ==
             (UINT4) i4NatIPSecSessionSPIInside) &&
            (pIPSecListNode->pIPSecOutHashNode->pIPSecInHashNode->
             u4SPIOutside == (UINT4) i4NatIPSecSessionSPIOutside))
        {
            break;
        }
    }
    if (pIPSecListNode != NULL)
    {
        *pu4RetValNatIPSecSessionTranslatedLocalIp =
            pIPSecListNode->pIPSecOutHashNode->u4TranslatedIpAddr;
        NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionTranslatedLocalIp = %x\n",
                  *pu4RetValNatIPSecSessionTranslatedLocalIp);
        return SNMP_SUCCESS;
    }
    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNatIPSecSessionLastUseTime
 Input       :  The Indices
                NatIPSecSessionInterfaceNum
                NatIPSecSessionLocalIp
                NatIPSecSessionOutsideIp
                NatIPSecSessionSPIInside
                NatIPSecSessionSPIOutside

                The Object 
                retValNatIPSecSessionLastUseTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatIPSecSessionLastUseTime (INT4 i4NatIPSecSessionInterfaceNum,
                                  UINT4 u4NatIPSecSessionLocalIp,
                                  UINT4 u4NatIPSecSessionOutsideIp,
                                  INT4 i4NatIPSecSessionSPIInside,
                                  INT4 i4NatIPSecSessionSPIOutside,
                                  INT4 *pi4RetValNatIPSecSessionLastUseTime)
{
    tIPSecListNode     *pIPSecListNode = NULL;
    TMO_SLL_Scan (&gNatIPSecListNode, pIPSecListNode, tIPSecListNode *)
    {
        if ((pIPSecListNode->pIPSecOutHashNode->u4IfNum ==
             (UINT4) i4NatIPSecSessionInterfaceNum) &&
            (pIPSecListNode->pIPSecOutHashNode->u4LocIpAddr ==
             u4NatIPSecSessionLocalIp) &&
            (pIPSecListNode->pIPSecOutHashNode->u4OutIpAddr ==
             u4NatIPSecSessionOutsideIp) &&
            (pIPSecListNode->pIPSecOutHashNode->u4SPIInside ==
             (UINT4) i4NatIPSecSessionSPIInside) &&
            (pIPSecListNode->pIPSecOutHashNode->pIPSecInHashNode->
             u4SPIOutside == (UINT4) i4NatIPSecSessionSPIOutside))
        {
            break;
        }
    }
    if (pIPSecListNode != NULL)
    {
        *pi4RetValNatIPSecSessionLastUseTime =
            (INT4) pIPSecListNode->pIPSecOutHashNode->u4TimeStamp;
        NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionLastUseTime = %d\n",
                  *pi4RetValNatIPSecSessionLastUseTime);
        return SNMP_SUCCESS;
    }
    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNatIPSecSessionEntryStatus
 Input       :  The Indices
                NatIPSecSessionInterfaceNum
                NatIPSecSessionLocalIp
                NatIPSecSessionOutsideIp
                NatIPSecSessionSPIInside
                NatIPSecSessionSPIOutside

                The Object 
                retValNatIPSecSessionEntryStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatIPSecSessionEntryStatus (INT4 i4NatIPSecSessionInterfaceNum,
                                  UINT4 u4NatIPSecSessionLocalIp,
                                  UINT4 u4NatIPSecSessionOutsideIp,
                                  INT4 i4NatIPSecSessionSPIInside,
                                  INT4 i4NatIPSecSessionSPIOutside,
                                  INT4 *pi4RetValNatIPSecSessionEntryStatus)
{
    tIPSecListNode     *pIPSecListNode = NULL;

    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionInterfaceNum = %d\n",
              i4NatIPSecSessionInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionLocalIp = %x\n",
              u4NatIPSecSessionLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionOutsideIp = %x\n",
              u4NatIPSecSessionOutsideIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionSPIInside = %d\n",
              i4NatIPSecSessionSPIInside);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionSPIOutside = %d\n",
              i4NatIPSecSessionSPIOutside);
    TMO_SLL_Scan (&gNatIPSecListNode, pIPSecListNode, tIPSecListNode *)
    {
        if ((pIPSecListNode->pIPSecOutHashNode->u4IfNum ==
             (UINT4) i4NatIPSecSessionInterfaceNum) &&
            (pIPSecListNode->pIPSecOutHashNode->u4LocIpAddr ==
             u4NatIPSecSessionLocalIp) &&
            (pIPSecListNode->pIPSecOutHashNode->u4OutIpAddr ==
             u4NatIPSecSessionOutsideIp) &&
            (pIPSecListNode->pIPSecOutHashNode->u4SPIInside ==
             (UINT4) i4NatIPSecSessionSPIInside) &&
            (pIPSecListNode->pIPSecOutHashNode->pIPSecInHashNode->
             u4SPIOutside == (UINT4) i4NatIPSecSessionSPIOutside))
        {
            *pi4RetValNatIPSecSessionEntryStatus = NAT_STATUS_ACTIVE;
            NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionEntryStatus= %d\n",
                      *pi4RetValNatIPSecSessionEntryStatus);
            return SNMP_SUCCESS;
        }
    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetNatIPSecSessionEntryStatus
 Input       :  The Indices
                NatIPSecSessionInterfaceNum
                NatIPSecSessionLocalIp
                NatIPSecSessionOutsideIp
                NatIPSecSessionSPIInside
                NatIPSecSessionSPIOutside

                The Object 
                setValNatIPSecSessionEntryStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatIPSecSessionEntryStatus (INT4 i4NatIPSecSessionInterfaceNum,
                                  UINT4 u4NatIPSecSessionLocalIp,
                                  UINT4 u4NatIPSecSessionOutsideIp,
                                  INT4 i4NatIPSecSessionSPIInside,
                                  INT4 i4NatIPSecSessionSPIOutside,
                                  INT4 i4SetValNatIPSecSessionEntryStatus)
{
    INT1                i1RetStatus = NAT_ZERO;
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionInterfaceNum = %d\n",
              i4NatIPSecSessionInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionLocalIp = %x\n",
              u4NatIPSecSessionLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionOutsideIp = %x\n",
              u4NatIPSecSessionOutsideIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionSPIInside = %d\n",
              i4NatIPSecSessionSPIInside);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionSPIOutside = %d\n",
              i4NatIPSecSessionSPIOutside);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionEntryStatus= %d\n",
              i4SetValNatIPSecSessionEntryStatus);

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (SecUtilValidateIfIndex ((UINT2) i4NatIPSecSessionInterfaceNum) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    switch (i4SetValNatIPSecSessionEntryStatus)
    {

        case NAT_STATUS_DESTROY:

            i1RetStatus =
                (INT1) (natDeleteIPSecSessionRow (i4NatIPSecSessionInterfaceNum,
                                                  u4NatIPSecSessionLocalIp,
                                                  u4NatIPSecSessionOutsideIp,
                                                  (UINT4)
                                                  i4NatIPSecSessionSPIInside,
                                                  (UINT4)
                                                  i4NatIPSecSessionSPIOutside));
            break;

        default:
            MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT",
                     "\n Unknown Row Status Option \n");
            i1RetStatus = SNMP_FAILURE;
            break;

    }

    if (i1RetStatus == SNMP_SUCCESS)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatIPSecSessionEntryStatus,
                              u4SeqNum, TRUE, NatLock, NatUnLock, NAT_FIVE,
                              i1RetStatus);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i %i",
                          i4NatIPSecSessionInterfaceNum,
                          u4NatIPSecSessionLocalIp, u4NatIPSecSessionOutsideIp,
                          i4NatIPSecSessionSPIInside,
                          i4NatIPSecSessionSPIOutside,
                          i4SetValNatIPSecSessionEntryStatus));
    }
    return i1RetStatus;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2NatIPSecSessionEntryStatus
 Input       :  The Indices
                NatIPSecSessionInterfaceNum
                NatIPSecSessionLocalIp
                NatIPSecSessionOutsideIp
                NatIPSecSessionSPIInside
                NatIPSecSessionSPIOutside

                The Object 
                testValNatIPSecSessionEntryStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatIPSecSessionEntryStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4NatIPSecSessionInterfaceNum,
                                     UINT4 u4NatIPSecSessionLocalIp,
                                     UINT4 u4NatIPSecSessionOutsideIp,
                                     INT4 i4NatIPSecSessionSPIInside,
                                     INT4 i4NatIPSecSessionSPIOutside,
                                     INT4 i4TestValNatIPSecSessionEntryStatus)
{
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionInterfaceNum = %d\n",
              i4NatIPSecSessionInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionLocalIp = %x\n",
              u4NatIPSecSessionLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionOutsideIp = %x\n",
              u4NatIPSecSessionOutsideIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionSPIInside = %d\n",
              i4NatIPSecSessionSPIInside);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionSPIOutside = %d\n",
              i4NatIPSecSessionSPIOutside);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecSessionEntryStatus= %d\n",
              i4TestValNatIPSecSessionEntryStatus);
    if ((i4NatIPSecSessionInterfaceNum > NAT_ZERO)
        && (i4NatIPSecSessionInterfaceNum <= NAT_MAX_NUM_IF)
        && (NatValidateIpAddress (u4NatIPSecSessionLocalIp, NAT_LOCAL)
            != NAT_ZERO)
        && (NatValidateIpAddress (u4NatIPSecSessionOutsideIp, NAT_LOCAL)
            != NAT_ZERO) == NAT_SUCCESS)
    {
        switch (i4TestValNatIPSecSessionEntryStatus)
        {

            case NAT_STATUS_DESTROY:
                MOD_TRC_ARG5 (gu4NatTrc, MGMT_TRC, "NAT",
                              "The row of IPSec Session Table "
                              "of  index %d %x, %x, %d and %d    exist\n",
                              i4NatIPSecSessionInterfaceNum,
                              u4NatIPSecSessionLocalIp,
                              u4NatIPSecSessionOutsideIp,
                              i4NatIPSecSessionSPIInside,
                              i4NatIPSecSessionSPIOutside);
                NAT_TRC (NAT_TRC_ON, "Exiting with SUCCESS: Entry found \n");
                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                return SNMP_SUCCESS;
            case NAT_STATUS_ACTIVE:

            case NAT_STATUS_NOT_IN_SERVICE:

            case NAT_STATUS_CREATE_AND_GO:

            case NAT_STATUS_CREATE_AND_WAIT:

            default:
                break;
        }
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    MOD_TRC_ARG5 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of IPSec Session Table of\
                  index %d, %x, %x, %d and %d does not exist\n", i4NatIPSecSessionInterfaceNum, u4NatIPSecSessionLocalIp, u4NatIPSecSessionOutsideIp, i4NatIPSecSessionSPIInside, i4NatIPSecSessionSPIOutside);
    UNUSED_PARAM (i4NatIPSecSessionSPIInside);
    UNUSED_PARAM (i4NatIPSecSessionSPIOutside);
    return SNMP_FAILURE;

}

/* LOW LEVEL Routines for Table : NatIPSecPendingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceNatIPSecPendingTable
 Input       :  The Indices
                NatIPSecPendingInterfaceNum
                NatIPSecPendingLocalIp
                NatIPSecPendingOutsideIp
                NatIPSecPendingSPIInside
                NatIPSecPendingSPIOutside
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceNatIPSecPendingTable (INT4
                                              i4NatIPSecPendingInterfaceNum,
                                              UINT4 u4NatIPSecPendingLocalIp,
                                              UINT4 u4NatIPSecPendingOutsideIp,
                                              INT4 i4NatIPSecPendingSPIInside,
                                              INT4 i4NatIPSecPendingSPIOutside)
{
    tIPSecPendListNode *pIPSecPendListNode = NULL;

    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingInterfaceNum = %d\n",
              i4NatIPSecPendingInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingocalIp = %x\n",
              u4NatIPSecPendingLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingOutsideIp = %x\n",
              u4NatIPSecPendingOutsideIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingSPIInside = %d\n",
              i4NatIPSecPendingSPIInside);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingSPIOutside = %d\n",
              i4NatIPSecPendingSPIOutside);

    TMO_SLL_Scan (&gNatIPSecListNode, pIPSecPendListNode, tIPSecPendListNode *)
    {
        if ((pIPSecPendListNode->pIPSecPendHashNode->u4IfNum ==
             (UINT4) i4NatIPSecPendingInterfaceNum) &&
            (pIPSecPendListNode->pIPSecPendHashNode->u4LocIpAddr ==
             u4NatIPSecPendingLocalIp) &&
            (pIPSecPendListNode->pIPSecPendHashNode->u4OutIpAddr ==
             u4NatIPSecPendingOutsideIp) &&
            (pIPSecPendListNode->pIPSecPendHashNode->u4SPIInside ==
             (UINT4) i4NatIPSecPendingSPIInside) &&
            (pIPSecPendListNode->pIPSecPendHashNode->u4SPIOutside ==
             (UINT4) i4NatIPSecPendingSPIOutside))
        {
            MOD_TRC_ARG5 (gu4NatTrc, MGMT_TRC, "NAT",
                          "The row of IPSec Pending Table "
                          "of  index %x, %x, %x, %d and %d    exist\n",
                          i4NatIPSecPendingInterfaceNum,
                          u4NatIPSecPendingLocalIp, u4NatIPSecPendingOutsideIp,
                          i4NatIPSecPendingSPIInside,
                          i4NatIPSecPendingSPIOutside);
            NAT_TRC (NAT_TRC_ON, "Exiting with SUCCESS: Entry found \n");
            return SNMP_SUCCESS;

        }
    }
    NAT_TRC (NAT_TRC_ON, "No Such Instance exists Test Failed\n");
    MOD_TRC_ARG5 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of IpSecSession Table of  index %d,"
                  "%x, %x, %d and %d does not exist\n",
                  i4NatIPSecPendingInterfaceNum, u4NatIPSecPendingLocalIp,
                  u4NatIPSecPendingOutsideIp, i4NatIPSecPendingSPIInside,
                  i4NatIPSecPendingSPIOutside);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexNatIPSecPendingTable
 Input       :  The Indices
                NatIPSecPendingInterfaceNum
                NatIPSecPendingLocalIp
                NatIPSecPendingOutsideIp
                NatIPSecPendingSPIInside
                NatIPSecPendingSPIOutside
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexNatIPSecPendingTable (INT4 *pi4NatIPSecPendingInterfaceNum,
                                      UINT4 *pu4NatIPSecPendingLocalIp,
                                      UINT4 *pu4NatIPSecPendingOutsideIp,
                                      INT4 *pi4NatIPSecPendingSPIInside,
                                      INT4 *pi4NatIPSecPendingSPIOutside)
{
    tIPSecPendListNode *pIPSecPendListNode = NULL;
    if ((pIPSecPendListNode =
         (tIPSecPendListNode *) TMO_SLL_First (&gNatIPSecPendListNode)) != NULL)
    {
        *pi4NatIPSecPendingInterfaceNum =
            (INT4) pIPSecPendListNode->pIPSecPendHashNode->u4IfNum;
        *pu4NatIPSecPendingLocalIp =
            pIPSecPendListNode->pIPSecPendHashNode->u4LocIpAddr;
        *pu4NatIPSecPendingOutsideIp =
            pIPSecPendListNode->pIPSecPendHashNode->u4OutIpAddr;
        *pi4NatIPSecPendingSPIInside =
            (INT4) pIPSecPendListNode->pIPSecPendHashNode->u4SPIInside;
        *pi4NatIPSecPendingSPIOutside =
            (INT4) pIPSecPendListNode->pIPSecPendHashNode->u4SPIOutside;

        NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingInterfaceNum = %d\n",
                  *pi4NatIPSecPendingInterfaceNum);
        NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingLocalIp = %x\n",
                  *pu4NatIPSecPendingLocalIp);
        NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingOutsideIp = %x\n",
                  *pu4NatIPSecPendingOutsideIp);
        NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingSPIInside = %d\n",
                  *pi4NatIPSecPendingSPIInside);
        NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingSPIOutside = %d\n",
                  *pi4NatIPSecPendingSPIOutside);
        return SNMP_SUCCESS;
    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexNatIPSecPendingTable
 Input       :  The Indices
                NatIPSecPendingInterfaceNum
                nextNatIPSecPendingInterfaceNum
                NatIPSecPendingLocalIp
                nextNatIPSecPendingLocalIp
                NatIPSecPendingOutsideIp
                nextNatIPSecPendingOutsideIp
                NatIPSecPendingSPIInside
                nextNatIPSecPendingSPIInside
                NatIPSecPendingSPIOutside
                nextNatIPSecPendingSPIOutside
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexNatIPSecPendingTable (INT4 i4NatIPSecPendingInterfaceNum,
                                     INT4 *pi4NextNatIPSecPendingInterfaceNum,
                                     UINT4 u4NatIPSecPendingLocalIp,
                                     UINT4 *pu4NextNatIPSecPendingLocalIp,
                                     UINT4 u4NatIPSecPendingOutsideIp,
                                     UINT4 *pu4NextNatIPSecPendingOutsideIp,
                                     INT4 i4NatIPSecPendingSPIInside,
                                     INT4 *pi4NextNatIPSecPendingSPIInside,
                                     INT4 i4NatIPSecPendingSPIOutside,
                                     INT4 *pi4NextNatIPSecPendingSPIOutside)
{

    tIPSecPendListNode *pIPSecPendListNode = NULL;

    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingInterfaceNum = %d\n",
              i4NatIPSecPendingInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingocalIp = %x\n",
              u4NatIPSecPendingLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingOutsideIp = %x\n",
              u4NatIPSecPendingOutsideIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingSPIInside = %d\n",
              i4NatIPSecPendingSPIInside);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingSPIOutside = %d\n",
              i4NatIPSecPendingSPIOutside);

    TMO_SLL_Scan (&gNatIPSecPendListNode, pIPSecPendListNode,
                  tIPSecPendListNode *)
    {
        if ((pIPSecPendListNode->pIPSecPendHashNode->u4IfNum ==
             (UINT4) i4NatIPSecPendingInterfaceNum) &&
            (pIPSecPendListNode->pIPSecPendHashNode->u4LocIpAddr ==
             u4NatIPSecPendingLocalIp) &&
            (pIPSecPendListNode->pIPSecPendHashNode->u4OutIpAddr ==
             u4NatIPSecPendingOutsideIp) &&
            (pIPSecPendListNode->pIPSecPendHashNode->u4SPIInside ==
             (UINT4) i4NatIPSecPendingSPIInside) &&
            (pIPSecPendListNode->pIPSecPendHashNode->u4SPIOutside ==
             (UINT4) i4NatIPSecPendingSPIOutside))
        {
            break;
        }
    }
    if (pIPSecPendListNode != NULL)
    {
        pIPSecPendListNode = (tIPSecPendListNode *)
            TMO_SLL_Next ((tTMO_SLL *) & gNatIPSecPendListNode,
                          &(pIPSecPendListNode->IPSecPendList));
/*UT_FIX -START*/
        if (pIPSecPendListNode != NULL)
        {
/*UT_FIX -END*/
            *pi4NextNatIPSecPendingInterfaceNum =
                (INT4) pIPSecPendListNode->pIPSecPendHashNode->u4IfNum;
            *pu4NextNatIPSecPendingLocalIp =
                pIPSecPendListNode->pIPSecPendHashNode->u4LocIpAddr;
            *pu4NextNatIPSecPendingOutsideIp =
                pIPSecPendListNode->pIPSecPendHashNode->u4OutIpAddr;
            *pi4NextNatIPSecPendingSPIInside =
                (INT4) pIPSecPendListNode->pIPSecPendHashNode->u4SPIInside;
            *pi4NextNatIPSecPendingSPIOutside =
                (INT4) pIPSecPendListNode->pIPSecPendHashNode->u4SPIOutside;

            NAT_DBG1 (NAT_DBG_LLR, "NextNatIPSecPendingInterfaceNum = %d\n",
                      *pi4NextNatIPSecPendingInterfaceNum);
            NAT_DBG1 (NAT_DBG_LLR, "NextNatIPSecPendingIPSecLocalIp = %x\n",
                      *pu4NextNatIPSecPendingLocalIp);
            NAT_DBG1 (NAT_DBG_LLR, "NextNatIPSecPendingOutsideIp = %x\n",
                      *pu4NextNatIPSecPendingOutsideIp);
            NAT_DBG1 (NAT_DBG_LLR, "NextNatIPSecPendingSPIInside = %d\n",
                      *pi4NextNatIPSecPendingSPIInside);
            NAT_DBG1 (NAT_DBG_LLR, "NextNatIPSecPendingSPIOutside = %d\n",
                      *pi4NextNatIPSecPendingSPIOutside);
            return SNMP_SUCCESS;
        }
    }
    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetNatIPSecPendingTranslatedLocalIp
 Input       :  The Indices
                NatIPSecPendingInterfaceNum
                NatIPSecPendingLocalIp
                NatIPSecPendingOutsideIp
                NatIPSecPendingSPIInside
                NatIPSecPendingSPIOutside

                The Object 
                retValNatIPSecPendingTranslatedLocalIp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetNatIPSecPendingTranslatedLocalIp
    (INT4 i4NatIPSecPendingInterfaceNum,
     UINT4 u4NatIPSecPendingLocalIp,
     UINT4 u4NatIPSecPendingOutsideIp,
     INT4 i4NatIPSecPendingSPIInside,
     INT4 i4NatIPSecPendingSPIOutside,
     UINT4 *pu4RetValNatIPSecPendingTranslatedLocalIp)
{
    tIPSecPendListNode *pIPSecPendListNode = NULL;
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingInterfaceNum = %d\n",
              i4NatIPSecPendingInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingocalIp = %x\n",
              u4NatIPSecPendingLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingOutsideIp = %x\n",
              u4NatIPSecPendingOutsideIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingSPIInside = %d\n",
              i4NatIPSecPendingSPIInside);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingSPIOutside = %d\n",
              i4NatIPSecPendingSPIOutside);

    TMO_SLL_Scan (&gNatIPSecPendListNode, pIPSecPendListNode,
                  tIPSecPendListNode *)
    {
        if ((pIPSecPendListNode->pIPSecPendHashNode->u4IfNum ==
             (UINT4) i4NatIPSecPendingInterfaceNum) &&
            (pIPSecPendListNode->pIPSecPendHashNode->u4LocIpAddr ==
             u4NatIPSecPendingLocalIp) &&
            (pIPSecPendListNode->pIPSecPendHashNode->u4OutIpAddr ==
             u4NatIPSecPendingOutsideIp) &&
            (pIPSecPendListNode->pIPSecPendHashNode->u4SPIInside ==
             (UINT4) i4NatIPSecPendingSPIInside) &&
            (pIPSecPendListNode->pIPSecPendHashNode->u4SPIOutside ==
             (UINT4) i4NatIPSecPendingSPIOutside))
        {
            *pu4RetValNatIPSecPendingTranslatedLocalIp =
                pIPSecPendListNode->pIPSecPendHashNode->u4TranslatedIpAddr;
            NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingTranslatedLocalIp = %x\n",
                      *pu4RetValNatIPSecPendingTranslatedLocalIp);
            return SNMP_SUCCESS;
        }
    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNatIPSecPendingLastUseTime
 Input       :  The Indices
                NatIPSecPendingInterfaceNum
                NatIPSecPendingLocalIp
                NatIPSecPendingOutsideIp
                NatIPSecPendingSPIInside
                NatIPSecPendingSPIOutside

                The Object 
                retValNatIPSecPendingLastUseTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatIPSecPendingLastUseTime (INT4 i4NatIPSecPendingInterfaceNum,
                                  UINT4 u4NatIPSecPendingLocalIp,
                                  UINT4 u4NatIPSecPendingOutsideIp,
                                  INT4 i4NatIPSecPendingSPIInside,
                                  INT4 i4NatIPSecPendingSPIOutside,
                                  INT4 *pi4RetValNatIPSecPendingLastUseTime)
{
    tIPSecPendListNode *pIPSecPendListNode = NULL;

    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingInterfaceNum = %d\n",
              i4NatIPSecPendingInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingocalIp = %x\n",
              u4NatIPSecPendingLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingOutsideIp = %x\n",
              u4NatIPSecPendingOutsideIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingSPIInside = %d\n",
              i4NatIPSecPendingSPIInside);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingSPIOutside = %d\n",
              i4NatIPSecPendingSPIOutside);

    TMO_SLL_Scan (&gNatIPSecPendListNode, pIPSecPendListNode,
                  tIPSecPendListNode *)
    {
        if ((pIPSecPendListNode->pIPSecPendHashNode->u4IfNum ==
             (UINT4) i4NatIPSecPendingInterfaceNum) &&
            (pIPSecPendListNode->pIPSecPendHashNode->u4LocIpAddr ==
             u4NatIPSecPendingLocalIp) &&
            (pIPSecPendListNode->pIPSecPendHashNode->u4OutIpAddr ==
             u4NatIPSecPendingOutsideIp) &&
            (pIPSecPendListNode->pIPSecPendHashNode->u4SPIInside ==
             (UINT4) i4NatIPSecPendingSPIInside) &&
            (pIPSecPendListNode->pIPSecPendHashNode->u4SPIOutside ==
             (UINT4) i4NatIPSecPendingSPIOutside))
        {
            *pi4RetValNatIPSecPendingLastUseTime =
                (INT4) pIPSecPendListNode->pIPSecPendHashNode->u4TimeStamp;
            NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingLastUseTime = %d\n",
                      *pi4RetValNatIPSecPendingLastUseTime);
            return SNMP_SUCCESS;
        }
    }
    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNatIPSecPendingNoOfRetry
 Input       :  The Indices
                NatIPSecPendingInterfaceNum
                NatIPSecPendingLocalIp
                NatIPSecPendingOutsideIp
                NatIPSecPendingSPIInside
                NatIPSecPendingSPIOutside

                The Object 
                retValNatIPSecPendingNoOfRetry
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatIPSecPendingNoOfRetry (INT4 i4NatIPSecPendingInterfaceNum,
                                UINT4 u4NatIPSecPendingLocalIp,
                                UINT4 u4NatIPSecPendingOutsideIp,
                                INT4 i4NatIPSecPendingSPIInside,
                                INT4 i4NatIPSecPendingSPIOutside,
                                INT4 *pi4RetValNatIPSecPendingNoOfRetry)
{
    tIPSecPendListNode *pIPSecPendListNode = NULL;

    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingInterfaceNum = %d\n",
              i4NatIPSecPendingInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingocalIp = %x\n",
              u4NatIPSecPendingLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingOutsideIp = %x\n",
              u4NatIPSecPendingOutsideIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingSPIInside = %d\n",
              i4NatIPSecPendingSPIInside);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingSPIOutside = %d\n",
              i4NatIPSecPendingSPIOutside);

    TMO_SLL_Scan (&gNatIPSecPendListNode, pIPSecPendListNode,
                  tIPSecPendListNode *)
    {
        if ((pIPSecPendListNode->pIPSecPendHashNode->u4IfNum ==
             (UINT4) i4NatIPSecPendingInterfaceNum) &&
            (pIPSecPendListNode->pIPSecPendHashNode->u4LocIpAddr ==
             u4NatIPSecPendingLocalIp) &&
            (pIPSecPendListNode->pIPSecPendHashNode->u4OutIpAddr ==
             u4NatIPSecPendingOutsideIp) &&
            (pIPSecPendListNode->pIPSecPendHashNode->u4SPIInside ==
             (UINT4) i4NatIPSecPendingSPIInside) &&
            (pIPSecPendListNode->pIPSecPendHashNode->u4SPIOutside ==
             (UINT4) i4NatIPSecPendingSPIOutside))
        {
            *pi4RetValNatIPSecPendingNoOfRetry =
                (INT4) pIPSecPendListNode->pIPSecPendHashNode->u2NoOfRetry;
            NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingNoOfRetry = %d\n",
                      *pi4RetValNatIPSecPendingNoOfRetry);
            return SNMP_SUCCESS;
        }
    }
    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNatIPSecPendingEntryStatus
 Input       :  The Indices
                NatIPSecPendingInterfaceNum
                NatIPSecPendingLocalIp
                NatIPSecPendingOutsideIp
                NatIPSecPendingSPIInside
                NatIPSecPendingSPIOutside

                The Object 
                retValNatIPSecPendingEntryStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatIPSecPendingEntryStatus (INT4 i4NatIPSecPendingInterfaceNum,
                                  UINT4 u4NatIPSecPendingLocalIp,
                                  UINT4 u4NatIPSecPendingOutsideIp,
                                  INT4 i4NatIPSecPendingSPIInside,
                                  INT4 i4NatIPSecPendingSPIOutside,
                                  INT4 *pi4RetValNatIPSecPendingEntryStatus)
{
    tIPSecPendListNode *pIPSecPendListNode = NULL;

    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingInterfaceNum = %d\n",
              i4NatIPSecPendingInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingocalIp = %x\n",
              u4NatIPSecPendingLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingOutsideIp = %x\n",
              u4NatIPSecPendingOutsideIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingSPIInside = %d\n",
              i4NatIPSecPendingSPIInside);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingSPIOutside = %d\n",
              i4NatIPSecPendingSPIOutside);

    TMO_SLL_Scan (&gNatIPSecPendListNode, pIPSecPendListNode,
                  tIPSecPendListNode *)
    {
        if ((pIPSecPendListNode->pIPSecPendHashNode->u4IfNum ==
             (UINT4) i4NatIPSecPendingInterfaceNum) &&
            (pIPSecPendListNode->pIPSecPendHashNode->u4LocIpAddr ==
             u4NatIPSecPendingLocalIp) &&
            (pIPSecPendListNode->pIPSecPendHashNode->u4OutIpAddr ==
             u4NatIPSecPendingOutsideIp) &&
            (pIPSecPendListNode->pIPSecPendHashNode->u4SPIInside ==
             (UINT4) i4NatIPSecPendingSPIInside) &&
            (pIPSecPendListNode->pIPSecPendHashNode->u4SPIOutside ==
             (UINT4) i4NatIPSecPendingSPIOutside))
        {
            *pi4RetValNatIPSecPendingEntryStatus = NAT_STATUS_ACTIVE;
            NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingEntryStatus = %d\n",
                      *pi4RetValNatIPSecPendingEntryStatus);
            return SNMP_SUCCESS;
        }
    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetNatIPSecPendingEntryStatus
 Input       :  The Indices
                NatIPSecPendingInterfaceNum
                NatIPSecPendingLocalIp
                NatIPSecPendingOutsideIp
                NatIPSecPendingSPIInside
                NatIPSecPendingSPIOutside

                The Object 
                setValNatIPSecPendingEntryStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatIPSecPendingEntryStatus (INT4 i4NatIPSecPendingInterfaceNum,
                                  UINT4 u4NatIPSecPendingLocalIp,
                                  UINT4 u4NatIPSecPendingOutsideIp,
                                  INT4 i4NatIPSecPendingSPIInside,
                                  INT4 i4NatIPSecPendingSPIOutside,
                                  INT4 i4SetValNatIPSecPendingEntryStatus)
{
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
 /********** MODIFY THE INPUTS FOR THIS FUNCTION  from INT4 to UINT4 ***** */
    INT1                i1RetStatus = NAT_ZERO;
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingInterfaceNum = %d\n",
              i4NatIPSecPendingInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingLocalIp = %x\n",
              u4NatIPSecPendingLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingOutsideIp = %x\n",
              u4NatIPSecPendingOutsideIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingSPIInside = %d\n",
              i4NatIPSecPendingSPIInside);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingSPIOutside = %d\n",
              i4NatIPSecPendingSPIOutside);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingEntryStatus = %d\n",
              i4SetValNatIPSecPendingEntryStatus);

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (SecUtilValidateIfIndex ((UINT2) i4NatIPSecPendingInterfaceNum) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    switch (i4SetValNatIPSecPendingEntryStatus)
    {

        case NAT_STATUS_DESTROY:

            i1RetStatus =
                (INT1) (natDeleteIPSecPendingRow (i4NatIPSecPendingInterfaceNum,
                                                  u4NatIPSecPendingLocalIp,
                                                  u4NatIPSecPendingOutsideIp,
                                                  (UINT4)
                                                  i4NatIPSecPendingSPIInside,
                                                  (UINT4)
                                                  i4NatIPSecPendingSPIOutside));
            break;

        default:
            MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT",
                     "\n Unknown Row Status Option \n");
            i1RetStatus = SNMP_FAILURE;
            break;

    }
    if (i1RetStatus == SNMP_SUCCESS)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatIPSecPendingEntryStatus,
                              u4SeqNum, TRUE, NatLock, NatUnLock, NAT_FIVE,
                              SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%p %p %i %i %i",
                          u4NatIPSecPendingLocalIp, u4NatIPSecPendingOutsideIp,
                          i4NatIPSecPendingSPIInside,
                          i4NatIPSecPendingSPIOutside,
                          i4SetValNatIPSecPendingEntryStatus));
    }

    return i1RetStatus;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2NatIPSecPendingEntryStatus
 Input       :  The Indices
                NatIPSecPendingInterfaceNum
                NatIPSecPendingLocalIp
                NatIPSecPendingOutsideIp
                NatIPSecPendingSPIInside
                NatIPSecPendingSPIOutside

                The Object 
                testValNatIPSecPendingEntryStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatIPSecPendingEntryStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4NatIPSecPendingInterfaceNum,
                                     UINT4 u4NatIPSecPendingLocalIp,
                                     UINT4 u4NatIPSecPendingOutsideIp,
                                     INT4 i4NatIPSecPendingSPIInside,
                                     INT4 i4NatIPSecPendingSPIOutside,
                                     INT4 i4TestValNatIPSecPendingEntryStatus)
{
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingInterfaceNum = %d\n",
              i4NatIPSecPendingInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingLocalIp = %x\n",
              u4NatIPSecPendingLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingOutsidelIp = %x\n",
              u4NatIPSecPendingOutsideIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingSPIInside = %d\n",
              i4NatIPSecPendingSPIInside);
    NAT_DBG1 (NAT_DBG_LLR, "NatIPSecPendingSPIOutside = %d\n",
              i4NatIPSecPendingSPIOutside);
    NAT_DBG1 (NAT_DBG_LLR, "NatStaticEntryStatus = %d\n",
              i4TestValNatIPSecPendingEntryStatus);
    if (((i4NatIPSecPendingInterfaceNum > NAT_ZERO)
         && (i4NatIPSecPendingInterfaceNum <= NAT_MAX_NUM_IF)
         && (NatValidateIpAddress (u4NatIPSecPendingLocalIp, NAT_LOCAL))
         && (NatValidateIpAddress (u4NatIPSecPendingOutsideIp, NAT_LOCAL)))
        == NAT_SUCCESS)
    {
        switch (i4TestValNatIPSecPendingEntryStatus)
        {

            case NAT_STATUS_DESTROY:
                MOD_TRC_ARG5 (gu4NatTrc, MGMT_TRC, "NAT",
                              "The row of IPSec Pending Table "
                              "of  index %d %x, %x, %d and %d    exist\n",
                              i4NatIPSecPendingInterfaceNum,
                              u4NatIPSecPendingLocalIp,
                              u4NatIPSecPendingOutsideIp,
                              i4NatIPSecPendingSPIInside,
                              i4NatIPSecPendingSPIOutside);
                NAT_TRC (NAT_TRC_ON, "Exiting with SUCCESS: Entry found \n");
                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                return SNMP_SUCCESS;
            case NAT_STATUS_ACTIVE:

            case NAT_STATUS_NOT_IN_SERVICE:

            case NAT_STATUS_CREATE_AND_GO:

            case NAT_STATUS_CREATE_AND_WAIT:

            default:
                break;
        }
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    MOD_TRC_ARG5 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of IPSec Pending Table of \
                  index %d, %x, %x, %d and %d does not exist\n", i4NatIPSecPendingInterfaceNum, u4NatIPSecPendingLocalIp, u4NatIPSecPendingOutsideIp, i4NatIPSecPendingSPIInside, i4NatIPSecPendingSPIOutside);
    UNUSED_PARAM (i4NatIPSecPendingSPIOutside);
    UNUSED_PARAM (i4NatIPSecPendingSPIInside);
    return SNMP_FAILURE;

}

/* LOW LEVEL Routines for Table : NatIKESessionTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceNatIKESessionTable
 Input       :  The Indices
                NatIKESessionInterfaceNum
                NatIKESessionLocalIp
                NatIKESessionOutsideIp
                NatIKESessionInitCookie
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceNatIKESessionTable (INT4 i4NatIKESessionInterfaceNum,
                                            UINT4 u4NatIKESessionLocalIp,
                                            UINT4 u4NatIKESessionOutsideIp,
                                            tSNMP_OCTET_STRING_TYPE
                                            * pNatIKESessionInitCookie)
{
    tIKEListNode       *pIKEListNode = NULL;

    NAT_DBG1 (NAT_DBG_LLR, "NatIKESessionInterfaceNum = %d\n",
              i4NatIKESessionInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatIKESessionLocalIp = %x\n",
              u4NatIKESessionLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIKESessionOutsideIp = %x\n",
              u4NatIKESessionOutsideIp);
    TMO_SLL_Scan (&gNatIKEList, pIKEListNode, tIKEListNode *)
    {
        if (((UINT4) i4NatIKESessionInterfaceNum ==
             pIKEListNode->pIKEHashNode->u4IfNum)
            && (u4NatIKESessionLocalIp ==
                pIKEListNode->pIKEHashNode->u4LocIpAddr)
            && (u4NatIKESessionOutsideIp ==
                (pIKEListNode->pIKEHashNode->u4OutIpAddr)) &&
            (MEMCMP
             (pNatIKESessionInitCookie->pu1_OctetList,
              pIKEListNode->pIKEHashNode->au1InitCookie,
              NAT_IKE_COOKIE_LENGTH) == NAT_ZERO)
/*UT_FIX -END*/
            )
        {
            MOD_TRC_ARG3 (gu4NatTrc, MGMT_TRC, "NAT",
                          "The row of IKE Session List "
                          "of  index %x, %x, %x    \n",
                          i4NatIKESessionInterfaceNum, u4NatIKESessionLocalIp,
                          u4NatIKESessionOutsideIp);

            NAT_TRC (NAT_TRC_ON, "Exiting with SUCCESS: Entry found \n");
            return SNMP_SUCCESS;

        }
    }
    NAT_TRC (NAT_TRC_ON, "No Such Instance exists Test Failed\n");
    MOD_TRC_ARG3 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of IKE Session List of  index %d,"
                  "%x, %x \n",
                  i4NatIKESessionInterfaceNum, u4NatIKESessionLocalIp,
                  u4NatIKESessionOutsideIp);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexNatIKESessionTable
 Input       :  The Indices
                NatIKESessionInterfaceNum
                NatIKESessionLocalIp
                NatIKESessionOutsideIp
                NatIKESessionInitCookie
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexNatIKESessionTable (INT4 *pi4NatIKESessionInterfaceNum,
                                    UINT4 *pu4NatIKESessionLocalIp,
                                    UINT4 *pu4NatIKESessionOutsideIp,
                                    tSNMP_OCTET_STRING_TYPE
                                    * pNatIKESessionInitCookie)
{
    tIKEListNode       *pIKEListNode = NULL;
    if ((pIKEListNode = (tIKEListNode *) TMO_SLL_First (&gNatIKEList)) != NULL)
    {
        *pi4NatIKESessionInterfaceNum =
            (INT4) pIKEListNode->pIKEHashNode->u4IfNum;
        *pu4NatIKESessionLocalIp = pIKEListNode->pIKEHashNode->u4LocIpAddr;
        *pu4NatIKESessionOutsideIp = pIKEListNode->pIKEHashNode->u4OutIpAddr;
        /*UT_FIX -START */
        MEMCPY (pNatIKESessionInitCookie->pu1_OctetList,
                pIKEListNode->pIKEHashNode->au1InitCookie,
                NAT_IKE_COOKIE_LENGTH);
        pNatIKESessionInitCookie->i4_Length = NAT_IKE_COOKIE_LENGTH;
        /*UT_FIX -END */
        NAT_DBG1 (NAT_DBG_LLR, "NatIKESessionInterfaceNum = %d\n",
                  *pi4NatIKESessionInterfaceNum);
        NAT_DBG1 (NAT_DBG_LLR, "NatIKESessionLocalIp = %x\n",
                  *pu4NatIKESessionLocalIp);
        NAT_DBG1 (NAT_DBG_LLR, "NatIKESessionOutsideIp = %x\n",
                  *pu4NatIKESessionOutsideIp);
        return SNMP_SUCCESS;
    }

    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexNatIKESessionTable
 Input       :  The Indices
                NatIKESessionInterfaceNum
                nextNatIKESessionInterfaceNum
                NatIKESessionLocalIp
                nextNatIKESessionLocalIp
                NatIKESessionOutsideIp
                nextNatIKESessionOutsideIp
                NatIKESessionInitCookie
                nextNatIKESessionInitCookie
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexNatIKESessionTable (INT4 i4NatIKESessionInterfaceNum,
                                   INT4 *pi4NextNatIKESessionInterfaceNum,
                                   UINT4 u4NatIKESessionLocalIp,
                                   UINT4 *pu4NextNatIKESessionLocalIp,
                                   UINT4 u4NatIKESessionOutsideIp,
                                   UINT4 *pu4NextNatIKESessionOutsideIp,
                                   tSNMP_OCTET_STRING_TYPE
                                   * pNatIKESessionInitCookie,
                                   tSNMP_OCTET_STRING_TYPE
                                   * pNextNatIKESessionInitCookie)
{
    tIKEListNode       *pIKEListNode = NULL;

    NAT_DBG1 (NAT_DBG_LLR, "NatIKESessionInterfaceNum = %d\n",
              i4NatIKESessionInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatIKESessionLocalIp = %x\n",
              u4NatIKESessionLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIKESessionOutsideIp = %x\n",
              u4NatIKESessionOutsideIp);
    TMO_SLL_Scan (&gNatIKEList, pIKEListNode, tIKEListNode *)
    {
        if ((((UINT4) i4NatIKESessionInterfaceNum ==
              pIKEListNode->pIKEHashNode->u4IfNum)
             && (u4NatIKESessionLocalIp ==
                 pIKEListNode->pIKEHashNode->u4LocIpAddr)
             && (u4NatIKESessionOutsideIp ==
                 (pIKEListNode->pIKEHashNode->u4OutIpAddr)) &&
             (MEMCMP
              (pNatIKESessionInitCookie->pu1_OctetList,
               pIKEListNode->pIKEHashNode->au1InitCookie,
               NAT_IKE_COOKIE_LENGTH)) == NAT_ZERO)
/* RFC -6330 -END*/
            )
        {
            break;
        }
    }

    if (pIKEListNode != NULL)
    {
        pIKEListNode =
            (tIKEListNode *) TMO_SLL_Next ((tTMO_SLL *) & gNatIKEList,
                                           &(pIKEListNode->IKEList));

/*UT_FIX -START*/
        if (pIKEListNode != NULL)
        {
/*UT_FIX -END*/
            *pi4NextNatIKESessionInterfaceNum =
                (INT4) pIKEListNode->pIKEHashNode->u4IfNum;
            *pu4NextNatIKESessionLocalIp =
                pIKEListNode->pIKEHashNode->u4LocIpAddr;
            *pu4NextNatIKESessionOutsideIp =
                pIKEListNode->pIKEHashNode->u4OutIpAddr;
/* RFC -6330 -START*/
            MEMCPY (pNextNatIKESessionInitCookie->pu1_OctetList,
                    pIKEListNode->pIKEHashNode->au1InitCookie,
                    NAT_IKE_COOKIE_LENGTH);
            pNextNatIKESessionInitCookie->i4_Length = NAT_IKE_COOKIE_LENGTH;
/* RFC -6330 -END*/

            NAT_DBG1 (NAT_DBG_LLR, "NextNatIKESessionInterfaceNum = %d\n",
                      *pi4NextNatIKESessionInterfaceNum);
            NAT_DBG1 (NAT_DBG_LLR, "NextNatIKESessionLocalIp = %x\n",
                      *pu4NextNatIKESessionLocalIp);
            NAT_DBG1 (NAT_DBG_LLR, "NextNatIKESessionOutsideIp = %x\n",
                      *pu4NextNatIKESessionOutsideIp);
            /*UT_FIX -END */
            return SNMP_SUCCESS;

        }
    }
    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetNatIKESessionTranslatedLocalIp
 Input       :  The Indices
                NatIKESessionInterfaceNum
                NatIKESessionLocalIp
                NatIKESessionOutsideIp
                NatIKESessionInitCookie

                The Object 
                retValNatIKESessionTranslatedLocalIp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatIKESessionTranslatedLocalIp (INT4 i4NatIKESessionInterfaceNum,
                                      UINT4 u4NatIKESessionLocalIp,
                                      UINT4 u4NatIKESessionOutsideIp,
                                      tSNMP_OCTET_STRING_TYPE
                                      * pNatIKESessionInitCookie,
                                      UINT4
                                      *pu4RetValNatIKESessionTranslatedLocalIp)
{

    tIKEListNode       *pIKEListNode = NULL;

    NAT_DBG1 (NAT_DBG_LLR, "NatIKESessionInterfaceNum = %d\n",
              i4NatIKESessionInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatIKESessionLocalIp = %x\n",
              u4NatIKESessionLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIKESessionOutsideIp = %x\n",
              u4NatIKESessionOutsideIp);
    TMO_SLL_Scan (&gNatIKEList, pIKEListNode, tIKEListNode *)
    {
        if ((((UINT4) i4NatIKESessionInterfaceNum ==
              pIKEListNode->pIKEHashNode->u4IfNum)
             && (u4NatIKESessionLocalIp ==
                 pIKEListNode->pIKEHashNode->u4LocIpAddr)
             && (u4NatIKESessionOutsideIp ==
                 (pIKEListNode->pIKEHashNode->u4OutIpAddr)) &&
             (MEMCMP
              (pNatIKESessionInitCookie->pu1_OctetList,
               pIKEListNode->pIKEHashNode->au1InitCookie,
               NAT_IKE_COOKIE_LENGTH))) == NAT_ZERO)
/*UT_FIX -END*/
        {

            *pu4RetValNatIKESessionTranslatedLocalIp =
                pIKEListNode->pIKEHashNode->u4TranslatedIpAddr;
            /*UT_FIX -START */
            NAT_DBG1 (NAT_DBG_LLR, "NatIKETranslatedlIpAddr = %x\n",
                      *pu4RetValNatIKESessionTranslatedLocalIp);
            /*UT_FIX -END */
            return SNMP_SUCCESS;
        }
    }
    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNatIKESessionLastUseTime
 Input       :  The Indices
                NatIKESessionInterfaceNum
                NatIKESessionLocalIp
                NatIKESessionOutsideIp
                NatIKESessionInitCookie

                The Object 
                retValNatIKESessionLastUseTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatIKESessionLastUseTime (INT4 i4NatIKESessionInterfaceNum,
                                UINT4 u4NatIKESessionLocalIp,
                                UINT4 u4NatIKESessionOutsideIp,
                                tSNMP_OCTET_STRING_TYPE
                                * pNatIKESessionInitCookie,
                                INT4 *pi4RetValNatIKESessionLastUseTime)
{
    tIKEListNode       *pIKEListNode = NULL;

    NAT_DBG1 (NAT_DBG_LLR, "NatIKESessionInterfaceNum = %d\n",
              i4NatIKESessionInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatIKESessionLocalIp = %x\n",
              u4NatIKESessionLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIKESessionOutsideIp = %x\n",
              u4NatIKESessionOutsideIp);
    TMO_SLL_Scan (&gNatIKEList, pIKEListNode, tIKEListNode *)
    {
        if ((((UINT4) i4NatIKESessionInterfaceNum ==
              pIKEListNode->pIKEHashNode->u4IfNum)
             && (u4NatIKESessionLocalIp ==
                 pIKEListNode->pIKEHashNode->u4LocIpAddr)
             && (u4NatIKESessionOutsideIp ==
                 (pIKEListNode->pIKEHashNode->u4OutIpAddr)) &&
             (MEMCMP
              (pNatIKESessionInitCookie->pu1_OctetList,
               pIKEListNode->pIKEHashNode->au1InitCookie,
               NAT_IKE_COOKIE_LENGTH)) == NAT_ZERO)
/*UT_FIX -END*/
            )
        {

            *pi4RetValNatIKESessionLastUseTime =
                (INT4) pIKEListNode->pIKEHashNode->u4TimeStamp;
            /*UT_FIX -START */
            NAT_DBG1 (NAT_DBG_LLR, "NatIKESessionLatTimeUse = %d\n",
                      *pi4RetValNatIKESessionLastUseTime);
            /*UT_FIX -END */
            return SNMP_SUCCESS;
        }
    }
    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNatIKESessionEntryStatus
 Input       :  The Indices
                NatIKESessionInterfaceNum
                NatIKESessionLocalIp
                NatIKESessionOutsideIp
                NatIKESessionInitCookie

                The Object 
                retValNatIKESessionEntryStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatIKESessionEntryStatus (INT4 i4NatIKESessionInterfaceNum,
                                UINT4 u4NatIKESessionLocalIp,
                                UINT4 u4NatIKESessionOutsideIp,
                                tSNMP_OCTET_STRING_TYPE
                                * pNatIKESessionInitCookie,
                                INT4 *pi4RetValNatIKESessionEntryStatus)
{
    tIKEListNode       *pIKEListNode = NULL;

    NAT_DBG1 (NAT_DBG_LLR, "NatIKESessionInterfaceNum = %d\n",
              i4NatIKESessionInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatIKESessionLocalIp = %x\n",
              u4NatIKESessionLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIKESessionOutsideIp = %x\n",
              u4NatIKESessionOutsideIp);
    TMO_SLL_Scan (&gNatIKEList, pIKEListNode, tIKEListNode *)
    {
        if (((UINT4) i4NatIKESessionInterfaceNum ==
             pIKEListNode->pIKEHashNode->u4IfNum)
            && (u4NatIKESessionLocalIp ==
                pIKEListNode->pIKEHashNode->u4LocIpAddr)
            && ((u4NatIKESessionOutsideIp ==
                 (pIKEListNode->pIKEHashNode->u4OutIpAddr)) &&
                (MEMCMP
                 (pNatIKESessionInitCookie->pu1_OctetList,
                  pIKEListNode->pIKEHashNode->au1InitCookie,
                  NAT_IKE_COOKIE_LENGTH))) == NAT_ZERO)
/*UT_FIX -END*/

        {
            *pi4RetValNatIKESessionEntryStatus = NAT_STATUS_ACTIVE;
            NAT_DBG1 (NAT_DBG_LLR, "NatIKESessionEntryStatus= %d\n",
                      *pi4RetValNatIKESessionEntryStatus);
            return SNMP_SUCCESS;
        }
    }
    NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetNatIKESessionEntryStatus
 Input       :  The Indices
                NatIKESessionInterfaceNum
                NatIKESessionLocalIp
                NatIKESessionOutsideIp
                NatIKESessionInitCookie

                The Object 
                setValNatIKESessionEntryStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatIKESessionEntryStatus (INT4 i4NatIKESessionInterfaceNum,
                                UINT4 u4NatIKESessionLocalIp,
                                UINT4 u4NatIKESessionOutsideIp,
                                tSNMP_OCTET_STRING_TYPE
                                * pNatIKESessionInitCookie,
                                INT4 i4SetValNatIKESessionEntryStatus)
{
    INT1                i1RetStatus = NAT_ZERO;
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    NAT_DBG1 (NAT_DBG_LLR, "NatIKESessionInterfaceNum= %d\n",
              i4NatIKESessionInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatIKESessionLocalIp = %x\n",
              u4NatIKESessionLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIKESessionOutsideIp = %x\n",
              u4NatIKESessionOutsideIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIKESessionEntryStatus= %d\n",
              i4SetValNatIKESessionEntryStatus);

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (SecUtilValidateIfIndex ((UINT2) i4NatIKESessionInterfaceNum) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    switch (i4SetValNatIKESessionEntryStatus)
    {

        case NAT_STATUS_DESTROY:
            /* UT_FIX -START */
            i1RetStatus =
                (INT1) (natDeleteIKESessionRow
                        (i4NatIKESessionInterfaceNum, u4NatIKESessionLocalIp,
                         u4NatIKESessionOutsideIp,
                         pNatIKESessionInitCookie->pu1_OctetList));
            /* UT_FIX -END */
            break;

        default:
            MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT",
                     "\n Unknown Row Status Option \n");
            i1RetStatus = SNMP_FAILURE;
            break;

    }
    if (i1RetStatus == SNMP_SUCCESS)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatIKESessionEntryStatus,
                              u4SeqNum, TRUE, NatLock, NatUnLock, NAT_FOUR,
                              i1RetStatus);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %s %i",
                          i4NatIKESessionInterfaceNum, u4NatIKESessionLocalIp,
                          u4NatIKESessionOutsideIp, pNatIKESessionInitCookie,
                          i4SetValNatIKESessionEntryStatus));

    }

    return i1RetStatus;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2NatIKESessionEntryStatus
 Input       :  The Indices
                NatIKESessionInterfaceNum
                NatIKESessionLocalIp
                NatIKESessionOutsideIp
                NatIKESessionInitCookie

                The Object 
                testValNatIKESessionEntryStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatIKESessionEntryStatus (UINT4 *pu4ErrorCode,
                                   INT4 i4NatIKESessionInterfaceNum,
                                   UINT4 u4NatIKESessionLocalIp,
                                   UINT4 u4NatIKESessionOutsideIp,
                                   tSNMP_OCTET_STRING_TYPE
                                   * pNatIKESessionInitCookie,
                                   INT4 i4TestValNatIKESessionEntryStatus)
{
    UNUSED_PARAM (pNatIKESessionInitCookie);
    NAT_DBG1 (NAT_DBG_LLR, "NatIKESessionInterfaceNum = %d\n",
              i4NatIKESessionInterfaceNum);
    NAT_DBG1 (NAT_DBG_LLR, "NatIKESessionLocalIp = %x\n",
              u4NatIKESessionLocalIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIKESessionOutsideIp = %x\n",
              u4NatIKESessionOutsideIp);
    NAT_DBG1 (NAT_DBG_LLR, "NatIKESessionEntryStatus= %d\n",
              i4TestValNatIKESessionEntryStatus);
    if ((i4NatIKESessionInterfaceNum > NAT_ZERO)
        && (i4NatIKESessionInterfaceNum <= NAT_MAX_NUM_IF)
        && (NatValidateIpAddress (u4NatIKESessionLocalIp, NAT_LOCAL)
            != NAT_ZERO)
        && (NatValidateIpAddress (u4NatIKESessionOutsideIp, NAT_LOCAL)
            != NAT_ZERO) == NAT_SUCCESS)
    {
        switch (i4TestValNatIKESessionEntryStatus)
        {

            case NAT_STATUS_DESTROY:
                MOD_TRC_ARG3 (gu4NatTrc, MGMT_TRC, "NAT",
                              "The row of IKE Session Table "
                              "of  index %d %x, %x \n",
                              i4NatIKESessionInterfaceNum,
                              u4NatIKESessionLocalIp, u4NatIKESessionOutsideIp);
                NAT_TRC (NAT_TRC_ON, "Exiting with SUCCESS: Entry found \n");
                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                return SNMP_SUCCESS;
            case NAT_STATUS_ACTIVE:

            case NAT_STATUS_NOT_IN_SERVICE:

            case NAT_STATUS_CREATE_AND_GO:

            case NAT_STATUS_CREATE_AND_WAIT:

            default:
                break;
        }
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    MOD_TRC_ARG3 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of IKE Session Table of  index %d, %x, %x \n",
                  i4NatIKESessionInterfaceNum, u4NatIKESessionLocalIp,
                  u4NatIKESessionOutsideIp);
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : NatPortTrigInfoTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceNatPortTrigInfoTable
 Input       :  The Indices
                NatPortTrigInfoInBoundPortRange
                NatPortTrigInfoOutBoundPortRange
                NatPortTrigInfoProtocol
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceNatPortTrigInfoTable (tSNMP_OCTET_STRING_TYPE
                                              *
                                              pNatPortTrigInfoInBoundPortRange,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pNatPortTrigInfoOutBoundPortRange,
                                              INT4 i4NatPortTrigInfoProtocol)
{
    INT4                i4ArrayIndex = NAT_ZERO;
    tPortRange          aInPortRange[NAT_MAX_PORT_ALLOWED];
    tPortRange          aOutPortRange[NAT_MAX_PORT_ALLOWED];
    UINT2               u2MaxPortVal = NAT_ZERO;

    UINT1               au1BaseInBoundPort[NAT_MAX_PORT_STRING_LEN]
        = { NAT_ZERO };
    UINT1               au1BaseOutBoundPort[NAT_MAX_PORT_STRING_LEN]
        = { NAT_ZERO };

    MEMSET (au1BaseInBoundPort, NAT_ZERO, NAT_MAX_PORT_STRING_LEN);
    MEMCPY (au1BaseInBoundPort, pNatPortTrigInfoInBoundPortRange->pu1_OctetList,
            pNatPortTrigInfoInBoundPortRange->i4_Length);

    MEMSET (au1BaseOutBoundPort, NAT_ZERO, NAT_MAX_PORT_STRING_LEN);
    MEMCPY (au1BaseOutBoundPort,
            pNatPortTrigInfoOutBoundPortRange->pu1_OctetList,
            pNatPortTrigInfoOutBoundPortRange->i4_Length);

    MEMSET (&(aInPortRange), NAT_ZERO,
            (sizeof (tPortRange)) * (NAT_MAX_PORT_ALLOWED));
    MEMSET (&(aOutPortRange), NAT_ZERO,
            (sizeof (tPortRange)) * (NAT_MAX_PORT_ALLOWED));

    /* Validating Protocol No */
    if ((i4NatPortTrigInfoProtocol != NAT_TCP) && (i4NatPortTrigInfoProtocol
                                                   != NAT_UDP)
        && (i4NatPortTrigInfoProtocol != NAT_PROTO_ANY))
    {
        return (SNMP_FAILURE);
    }

    /* Converting Port range string to port range struct type
       and validating port no for in and out bound ports */
    if (NatPortString2PortRange (&(aInPortRange[NAT_INDEX_0]),
                                 au1BaseInBoundPort,
                                 &u2MaxPortVal) == NAT_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    if (NatPortString2PortRange (&(aOutPortRange[NAT_INDEX_0]),
                                 au1BaseOutBoundPort,
                                 &u2MaxPortVal) == NAT_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    /* Validating the Index in Port Trigger Table */
    i4ArrayIndex =
        (INT4) (NatUtilGetGlobalArrayIndex (au1BaseInBoundPort,
                                            au1BaseOutBoundPort,
                                            (UINT2) i4NatPortTrigInfoProtocol));

    if (i4ArrayIndex == NAT_INVALID_ARRAY_INDEX)
    {
        /* table entry not found */
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexNatPortTrigInfoTable
 Input       :  The Indices
                NatPortTrigInfoInBoundPortRange
                NatPortTrigInfoOutBoundPortRange
                NatPortTrigInfoProtocol
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexNatPortTrigInfoTable (tSNMP_OCTET_STRING_TYPE
                                      * pNatPortTrigInfoInBoundPortRange,
                                      tSNMP_OCTET_STRING_TYPE
                                      * pNatPortTrigInfoOutBoundPortRange,
                                      INT4 *pi4NatPortTrigInfoProtocol)
{
    UINT1               u1StartIndex = NAT_ZERO;

    /* Scanning for the First index */
    while (u1StartIndex < NAT_MAX_APP_ALLOWED)
    {
        if ((gaTrigInfo[u1StartIndex].u1Status == CREATE_AND_WAIT) ||
            (gaTrigInfo[u1StartIndex].u1Status == ACTIVE))
        {
            break;
        }
        u1StartIndex++;
    }

    if (u1StartIndex == NAT_MAX_APP_ALLOWED)
    {
        return (SNMP_FAILURE);
    }

    pNatPortTrigInfoInBoundPortRange->i4_Length
        = (INT4) STRLEN (gaTrigInfo[u1StartIndex].au1InPortString);

    MEMCPY (pNatPortTrigInfoInBoundPortRange->pu1_OctetList,
            gaTrigInfo[u1StartIndex].au1InPortString,
            pNatPortTrigInfoInBoundPortRange->i4_Length);

    pNatPortTrigInfoOutBoundPortRange->i4_Length
        = (INT4) STRLEN (gaTrigInfo[u1StartIndex].au1OutPortString);

    MEMCPY (pNatPortTrigInfoOutBoundPortRange->pu1_OctetList,
            gaTrigInfo[u1StartIndex].au1OutPortString,
            pNatPortTrigInfoOutBoundPortRange->i4_Length);

    *pi4NatPortTrigInfoProtocol = gaTrigInfo[u1StartIndex].u2Protocol;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexNatPortTrigInfoTable
 Input       :  The Indices
                NatPortTrigInfoInBoundPortRange
                nextNatPortTrigInfoInBoundPortRange
                NatPortTrigInfoOutBoundPortRange
                nextNatPortTrigInfoOutBoundPortRange
                NatPortTrigInfoProtocol
                nextNatPortTrigInfoProtocol
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexNatPortTrigInfoTable (tSNMP_OCTET_STRING_TYPE
                                     * pNatPortTrigInfoInBoundPortRange,
                                     tSNMP_OCTET_STRING_TYPE
                                     * pNextNatPortTrigInfoInBoundPortRange,
                                     tSNMP_OCTET_STRING_TYPE
                                     * pNatPortTrigInfoOutBoundPortRange,
                                     tSNMP_OCTET_STRING_TYPE
                                     * pNextNatPortTrigInfoOutBoundPortRange,
                                     INT4 i4NatPortTrigInfoProtocol,
                                     INT4 *pi4NextNatPortTrigInfoProtocol)
{
    INT4                i4ArrayIndex = NAT_ZERO;
    UINT1               au1BaseInBoundPort[NAT_MAX_PORT_STRING_LEN]
        = { NAT_ZERO };
    UINT1               au1BaseOutBoundPort[NAT_MAX_PORT_STRING_LEN]
        = { NAT_ZERO };

    MEMSET (au1BaseInBoundPort, NAT_ZERO, NAT_MAX_PORT_STRING_LEN);
    MEMCPY (au1BaseInBoundPort, pNatPortTrigInfoInBoundPortRange->pu1_OctetList,
            pNatPortTrigInfoInBoundPortRange->i4_Length);

    MEMSET (au1BaseOutBoundPort, NAT_ZERO, NAT_MAX_PORT_STRING_LEN);
    MEMCPY (au1BaseOutBoundPort,
            pNatPortTrigInfoOutBoundPortRange->pu1_OctetList,
            pNatPortTrigInfoOutBoundPortRange->i4_Length);

    /* Getting the Index of the Global Array From Supplied Table Indexes */
    i4ArrayIndex =
        (INT4) (NatUtilGetGlobalArrayIndex (au1BaseInBoundPort,
                                            au1BaseOutBoundPort,
                                            (UINT2) i4NatPortTrigInfoProtocol));

    if (i4ArrayIndex == NAT_INVALID_ARRAY_INDEX)
    {
        /* table entry not found */
        return (SNMP_FAILURE);
    }

    /* Obtaining Array Index for the Next Entry In The Table */
    while (++i4ArrayIndex < NAT_MAX_APP_ALLOWED)
    {
        if ((gaTrigInfo[i4ArrayIndex].u1Status == ACTIVE) ||
            (gaTrigInfo[i4ArrayIndex].u1Status == CREATE_AND_WAIT))
        {
            break;
        }
    }

    /* Check for Boundary Exceed */
    if (i4ArrayIndex == NAT_MAX_APP_ALLOWED)
    {
        return (SNMP_FAILURE);
    }

    /* Next InBound and OutBound Port Range */
    pNextNatPortTrigInfoInBoundPortRange->i4_Length
        = (INT4) STRLEN (gaTrigInfo[i4ArrayIndex].au1InPortString);

    MEMCPY (pNextNatPortTrigInfoInBoundPortRange->pu1_OctetList,
            gaTrigInfo[i4ArrayIndex].au1InPortString,
            pNextNatPortTrigInfoInBoundPortRange->i4_Length);

    pNextNatPortTrigInfoOutBoundPortRange->i4_Length
        = (INT4) STRLEN (gaTrigInfo[i4ArrayIndex].au1OutPortString);

    MEMCPY (pNextNatPortTrigInfoOutBoundPortRange->pu1_OctetList,
            gaTrigInfo[i4ArrayIndex].au1OutPortString,
            pNextNatPortTrigInfoOutBoundPortRange->i4_Length);

    /* Next Port No */
    *pi4NextNatPortTrigInfoProtocol = gaTrigInfo[i4ArrayIndex].u2Protocol;

    return (SNMP_SUCCESS);

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetNatPortTrigInfoAppName
 Input       :  The Indices
                NatPortTrigInfoInBoundPortRange
                NatPortTrigInfoOutBoundPortRange
                NatPortTrigInfoProtocol

                The Object 
                retValNatPortTrigInfoAppName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatPortTrigInfoAppName (tSNMP_OCTET_STRING_TYPE
                              * pNatPortTrigInfoInBoundPortRange,
                              tSNMP_OCTET_STRING_TYPE
                              * pNatPortTrigInfoOutBoundPortRange,
                              INT4 i4NatPortTrigInfoProtocol,
                              tSNMP_OCTET_STRING_TYPE
                              * pRetValNatPortTrigInfoAppName)
{

    INT4                i4ArrayIndex = NAT_ZERO;
    UINT1               au1InBoundPort[NAT_MAX_PORT_STRING_LEN] = { NAT_ZERO };
    UINT1               au1OutBoundPort[NAT_MAX_PORT_STRING_LEN] = { NAT_ZERO };

    MEMSET (au1InBoundPort, NAT_ZERO, NAT_MAX_PORT_STRING_LEN);
    MEMCPY (au1InBoundPort, pNatPortTrigInfoInBoundPortRange->pu1_OctetList,
            pNatPortTrigInfoInBoundPortRange->i4_Length);

    MEMSET (au1OutBoundPort, NAT_ZERO, NAT_MAX_PORT_STRING_LEN);
    MEMCPY (au1OutBoundPort, pNatPortTrigInfoOutBoundPortRange->pu1_OctetList,
            pNatPortTrigInfoOutBoundPortRange->i4_Length);

    /* Getting the Index of the Global Array From Supplied Table Indexes */
    i4ArrayIndex =
        (INT4) (NatUtilGetGlobalArrayIndex (au1InBoundPort,
                                            au1OutBoundPort,
                                            (UINT2) i4NatPortTrigInfoProtocol));

    if (i4ArrayIndex == NAT_INVALID_ARRAY_INDEX)
    {
        /* table entry not found */
        return (SNMP_FAILURE);
    }

    /* Extracting Application Name */
    pRetValNatPortTrigInfoAppName->i4_Length
        = (INT4) STRLEN (&(gaTrigInfo[i4ArrayIndex].au1AppName[NAT_INDEX_0]));

    MEMCPY (pRetValNatPortTrigInfoAppName->pu1_OctetList,
            &(gaTrigInfo[i4ArrayIndex].au1AppName[NAT_INDEX_0]),
            pRetValNatPortTrigInfoAppName->i4_Length);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetNatPortTrigInfoEntryStatus
 Input       :  The Indices
                NatPortTrigInfoInBoundPortRange
                NatPortTrigInfoOutBoundPortRange
                NatPortTrigInfoProtocol

                The Object 
                retValNatPortTrigInfoEntryStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatPortTrigInfoEntryStatus (tSNMP_OCTET_STRING_TYPE
                                  * pNatPortTrigInfoInBoundPortRange,
                                  tSNMP_OCTET_STRING_TYPE
                                  * pNatPortTrigInfoOutBoundPortRange,
                                  INT4 i4NatPortTrigInfoProtocol,
                                  INT4 *pi4RetValNatPortTrigInfoEntryStatus)
{
    INT4                i4ArrayIndex = NAT_ZERO;
    UINT1               au1InBoundPort[NAT_MAX_PORT_STRING_LEN] = { NAT_ZERO };
    UINT1               au1OutBoundPort[NAT_MAX_PORT_STRING_LEN] = { NAT_ZERO };

    MEMSET (au1InBoundPort, NAT_ZERO, NAT_MAX_PORT_STRING_LEN);
    MEMCPY (au1InBoundPort, pNatPortTrigInfoInBoundPortRange->pu1_OctetList,
            pNatPortTrigInfoInBoundPortRange->i4_Length);

    MEMSET (au1OutBoundPort, NAT_ZERO, NAT_MAX_PORT_STRING_LEN);
    MEMCPY (au1OutBoundPort, pNatPortTrigInfoOutBoundPortRange->pu1_OctetList,
            pNatPortTrigInfoOutBoundPortRange->i4_Length);

    /* Getting the Index of the Global Array From Supplied Table Indexes */
    i4ArrayIndex =
        (INT4) (NatUtilGetGlobalArrayIndex (au1InBoundPort,
                                            au1OutBoundPort,
                                            (UINT2) i4NatPortTrigInfoProtocol));

    if (i4ArrayIndex == NAT_INVALID_ARRAY_INDEX)
    {
        /* table entry not found */
        return (SNMP_FAILURE);
    }

    /* Extracting Row Status */
    *pi4RetValNatPortTrigInfoEntryStatus = gaTrigInfo[i4ArrayIndex].u1Status;

    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetNatPortTrigInfoAppName
 Input       :  The Indices
                NatPortTrigInfoInBoundPortRange
                NatPortTrigInfoOutBoundPortRange
                NatPortTrigInfoProtocol

                The Object 
                setValNatPortTrigInfoAppName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatPortTrigInfoAppName (tSNMP_OCTET_STRING_TYPE
                              * pNatPortTrigInfoInBoundPortRange,
                              tSNMP_OCTET_STRING_TYPE
                              * pNatPortTrigInfoOutBoundPortRange,
                              INT4 i4NatPortTrigInfoProtocol,
                              tSNMP_OCTET_STRING_TYPE
                              * pSetValNatPortTrigInfoAppName)
{
    INT4                i4ArrayIndex = NAT_ZERO;
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT1               au1InBoundPort[NAT_MAX_PORT_STRING_LEN] = { NAT_ZERO };
    UINT1               au1OutBoundPort[NAT_MAX_PORT_STRING_LEN] = { NAT_ZERO };

    RM_GET_SEQ_NUM (&u4SeqNum);
    MEMSET (au1InBoundPort, NAT_ZERO, NAT_MAX_PORT_STRING_LEN);
    MEMCPY (au1InBoundPort, pNatPortTrigInfoInBoundPortRange->pu1_OctetList,
            pNatPortTrigInfoInBoundPortRange->i4_Length);

    MEMSET (au1OutBoundPort, NAT_ZERO, NAT_MAX_PORT_STRING_LEN);
    MEMCPY (au1OutBoundPort, pNatPortTrigInfoOutBoundPortRange->pu1_OctetList,
            pNatPortTrigInfoOutBoundPortRange->i4_Length);

    /* Getting the Index of the Global Array From Supplied Table Indexes */
    i4ArrayIndex =
        (INT4) (NatUtilGetGlobalArrayIndex (au1InBoundPort,
                                            au1OutBoundPort,
                                            (UINT2) i4NatPortTrigInfoProtocol));

    if (i4ArrayIndex == NAT_INVALID_ARRAY_INDEX)
    {
        /* table entry not found */
        return (SNMP_FAILURE);
    }

    /* Setting the Application Name Field in table */
    MEMCPY (&(gaTrigInfo[i4ArrayIndex].au1AppName[NAT_INDEX_0]),
            pSetValNatPortTrigInfoAppName->pu1_OctetList,
            pSetValNatPortTrigInfoAppName->i4_Length);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatPortTrigInfoAppName, u4SeqNum,
                          TRUE, NatLock, NatUnLock, NAT_THREE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %s %i %s",
                      pNatPortTrigInfoInBoundPortRange,
                      pNatPortTrigInfoOutBoundPortRange,
                      i4NatPortTrigInfoProtocol,
                      pSetValNatPortTrigInfoAppName));

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetNatPortTrigInfoEntryStatus
 Input       :  The Indices
                NatPortTrigInfoInBoundPortRange
                NatPortTrigInfoOutBoundPortRange
                NatPortTrigInfoProtocol

                The Object 
                setValNatPortTrigInfoEntryStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatPortTrigInfoEntryStatus (tSNMP_OCTET_STRING_TYPE
                                  * pNatPortTrigInfoInBoundPortRange,
                                  tSNMP_OCTET_STRING_TYPE
                                  * pNatPortTrigInfoOutBoundPortRange,
                                  INT4 i4NatPortTrigInfoProtocol,
                                  INT4 i4SetValNatPortTrigInfoEntryStatus)
{
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    INT4                i4ArrayIndex = NAT_INVALID_ARRAY_INDEX;
    INT1                i1RetStatus = SNMP_FAILURE;
    UINT1               au1InBoundPort[NAT_MAX_PORT_STRING_LEN] = { NAT_ZERO };
    UINT1               au1OutBoundPort[NAT_MAX_PORT_STRING_LEN] = { NAT_ZERO };
    tPortRange          aInPortRange[NAT_MAX_PORT_ALLOWED];
    tPortRange          aOutPortRange[NAT_MAX_PORT_ALLOWED];
    UINT2               u2MaxPortVal = NAT_ZERO;

    RM_GET_SEQ_NUM (&u4SeqNum);

    MEMSET (au1InBoundPort, NAT_ZERO, NAT_MAX_PORT_STRING_LEN);
    MEMCPY (au1InBoundPort, pNatPortTrigInfoInBoundPortRange->pu1_OctetList,
            pNatPortTrigInfoInBoundPortRange->i4_Length);

    MEMSET (au1OutBoundPort, NAT_ZERO, NAT_MAX_PORT_STRING_LEN);
    MEMCPY (au1OutBoundPort, pNatPortTrigInfoOutBoundPortRange->pu1_OctetList,
            pNatPortTrigInfoOutBoundPortRange->i4_Length);

    /* For New Entry no search in the table */
    if (i4SetValNatPortTrigInfoEntryStatus != CREATE_AND_WAIT)
    {
        /* Getting the Index of the Global Array From Supplied Table Indexes */
        i4ArrayIndex =
            (INT4) NatUtilGetGlobalArrayIndex (au1InBoundPort,
                                               au1OutBoundPort,
                                               (UINT2)
                                               i4NatPortTrigInfoProtocol);
        if ((i4ArrayIndex < NAT_ZERO) || (i4ArrayIndex >= NAT_MAX_APP_ALLOWED))
        {
            return SNMP_FAILURE;
        }

    }

    switch (i4SetValNatPortTrigInfoEntryStatus)
    {
        case CREATE_AND_WAIT:
            /* After Creating Indexes in the table row status create n wait */

            /* Searching for Next Free Slot in Table */
            i4ArrayIndex = NatGetFreeTrigInfoEntry ();
            if ((i4ArrayIndex < NAT_ZERO) ||
                (i4ArrayIndex > NAT_MAX_APP_ALLOWED))
            {
                return SNMP_FAILURE;
            }

            STRNCPY (gaTrigInfo[i4ArrayIndex].au1OutPortString,
                     au1OutBoundPort, NAT_MAX_PORT_STRING_LEN - NAT_ONE);
            STRNCPY (gaTrigInfo[i4ArrayIndex].au1InPortString,
                     au1InBoundPort, NAT_MAX_PORT_STRING_LEN - NAT_ONE);

            MEMSET ((aInPortRange), NAT_ZERO,
                    (sizeof (tPortRange)) * (NAT_MAX_PORT_ALLOWED));
            MEMSET ((aOutPortRange), NAT_ZERO,
                    (sizeof (tPortRange)) * (NAT_MAX_PORT_ALLOWED));

            /* Converting Port range string to port range struct type
               and validating port no for in and out bound ports */
            if (NatPortString2PortRange
                ((aInPortRange), au1InBoundPort, &u2MaxPortVal) == NAT_FAILURE)
            {
                return (i1RetStatus);
            }

            if (NatPortString2PortRange
                ((aOutPortRange), au1OutBoundPort,
                 &u2MaxPortVal) == NAT_FAILURE)
            {
                return (i1RetStatus);
            }

            MEMCPY (gaTrigInfo[i4ArrayIndex].aOutPortRange, aOutPortRange,
                    (sizeof (tPortRange) * NAT_MAX_PORT_ALLOWED));
            MEMCPY (gaTrigInfo[i4ArrayIndex].aInPortRange, aInPortRange,
                    (sizeof (tPortRange) * NAT_MAX_PORT_ALLOWED));

            gaTrigInfo[i4ArrayIndex].u2Protocol =
                (UINT2) i4NatPortTrigInfoProtocol;

            (gu4NatNextFreeTranslatedLocPort =
             (gu4NatNextFreeTranslatedLocPort > u2MaxPortVal) ?
             gu4NatNextFreeTranslatedLocPort : (UINT4) (u2MaxPortVal +
                                                        NAT_ONE));

            gaTrigInfo[i4ArrayIndex].u1Status = NOT_IN_SERVICE;
            i1RetStatus = SNMP_SUCCESS;
            break;
        case ACTIVE:
            /* After Filling Application Name Row Status Active */
            gaTrigInfo[i4ArrayIndex].u1Status
                = (UINT1) i4SetValNatPortTrigInfoEntryStatus;
            i1RetStatus = SNMP_SUCCESS;
            break;
        case DESTROY:
            /* Deleting Entry from table */
            NatFreeTrigInfoEntry (gaTrigInfo[i4ArrayIndex].au1AppName);
            i1RetStatus = SNMP_SUCCESS;
            break;
        case NOT_IN_SERVICE:
            gaTrigInfo[i4ArrayIndex].u1Status
                = (UINT1) i4SetValNatPortTrigInfoEntryStatus;
            i1RetStatus = SNMP_SUCCESS;
            break;
        default:
            break;
    }

    if (i1RetStatus == SNMP_SUCCESS)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatPortTrigInfoEntryStatus,
                              u4SeqNum, TRUE, NatLock, NatUnLock, NAT_THREE,
                              SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %s %i %i",
                          pNatPortTrigInfoInBoundPortRange,
                          pNatPortTrigInfoOutBoundPortRange,
                          i4NatPortTrigInfoProtocol,
                          i4SetValNatPortTrigInfoEntryStatus));

    }

    return (i1RetStatus);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2NatPortTrigInfoAppName
 Input       :  The Indices
                NatPortTrigInfoInBoundPortRange
                NatPortTrigInfoOutBoundPortRange
                NatPortTrigInfoProtocol

                The Object 
                testValNatPortTrigInfoAppName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatPortTrigInfoAppName (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE
                                 * pNatPortTrigInfoInBoundPortRange,
                                 tSNMP_OCTET_STRING_TYPE
                                 * pNatPortTrigInfoOutBoundPortRange,
                                 INT4 i4NatPortTrigInfoProtocol,
                                 tSNMP_OCTET_STRING_TYPE
                                 * pTestValNatPortTrigInfoAppName)
{

    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4ArrayIndex = NAT_ZERO;
    INT4                i4RetValNatPortTrigInfoEntryStatus = NAT_ZERO;
    UINT1               au1InBoundPort[NAT_MAX_PORT_STRING_LEN] = { NAT_ZERO };
    UINT1               au1OutBoundPort[NAT_MAX_PORT_STRING_LEN] = { NAT_ZERO };
    UINT1               au1ApplicationName[NAT_MAX_APP_NAME_LEN] = { NAT_ZERO };

    if (OSIX_FAILURE ==
        SNMPCheckForNVTChars (pTestValNatPortTrigInfoAppName->pu1_OctetList,
                              pTestValNatPortTrigInfoAppName->i4_Length))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((pTestValNatPortTrigInfoAppName->i4_Length <= NAT_ZERO)
        || (pTestValNatPortTrigInfoAppName->i4_Length >= NAT_MAX_APP_NAME_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    MEMSET (au1InBoundPort, NAT_ZERO, NAT_MAX_PORT_STRING_LEN);
    MEMCPY (au1InBoundPort, pNatPortTrigInfoInBoundPortRange->pu1_OctetList,
            pNatPortTrigInfoInBoundPortRange->i4_Length);

    MEMSET (au1OutBoundPort, NAT_ZERO, NAT_MAX_PORT_STRING_LEN);
    MEMCPY (au1OutBoundPort, pNatPortTrigInfoOutBoundPortRange->pu1_OctetList,
            pNatPortTrigInfoOutBoundPortRange->i4_Length);

    MEMSET (au1ApplicationName, NAT_ZERO, NAT_MAX_APP_NAME_LEN);
    MEMCPY (au1ApplicationName, pTestValNatPortTrigInfoAppName->pu1_OctetList,
            pTestValNatPortTrigInfoAppName->i4_Length);

    /* Getting the Index of the Global Array From Supplied Table Indexes */
    i4ArrayIndex =
        (INT4) NatUtilGetGlobalArrayIndex (au1InBoundPort,
                                           au1OutBoundPort,
                                           (UINT2) i4NatPortTrigInfoProtocol);

    if (i4ArrayIndex == NAT_INVALID_ARRAY_INDEX)
    {
        /* table entry not found */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (i1RetVal);
    }

    /* Validating The Length of Application Length */
    if ((pTestValNatPortTrigInfoAppName->i4_Length
         > (NAT_MAX_APP_NAME_LEN - NAT_ONE))
        || (pTestValNatPortTrigInfoAppName->i4_Length < NAT_MIN_APP_NAME_LEN))
    {
        if (nmhTestv2NatPortTrigInfoEntryStatus
            (pu4ErrorCode,
             pNatPortTrigInfoInBoundPortRange,
             pNatPortTrigInfoOutBoundPortRange,
             i4NatPortTrigInfoProtocol, DESTROY) == SNMP_FAILURE)
        {
            return (SNMP_FAILURE);
        }
        /* Removing the entry */
        if (nmhSetNatPortTrigInfoEntryStatus
            (pNatPortTrigInfoInBoundPortRange,
             pNatPortTrigInfoOutBoundPortRange, i4NatPortTrigInfoProtocol,
             DESTROY) == SNMP_FAILURE)
        {
            return (SNMP_FAILURE);
        }

        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return (i1RetVal);
    }

    /* Not Allowing Already Present Application Name and Cleaning up
       already entered stuffs */
    for (i4ArrayIndex = NAT_ZERO;
         i4ArrayIndex < NAT_MAX_APP_ALLOWED; i4ArrayIndex++)
    {
        i1RetVal = nmhGetNatPortTrigInfoEntryStatus
            (pNatPortTrigInfoInBoundPortRange,
             pNatPortTrigInfoOutBoundPortRange,
             i4NatPortTrigInfoProtocol, &i4RetValNatPortTrigInfoEntryStatus);
        if (SNMP_FAILURE == i1RetVal)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
        if (NOT_IN_SERVICE == i4RetValNatPortTrigInfoEntryStatus)
        {
            return SNMP_SUCCESS;
        }
        if (ACTIVE == i4RetValNatPortTrigInfoEntryStatus)
        {
            if ((MEMCMP
                 (au1ApplicationName, gaTrigInfo[i4ArrayIndex].au1AppName,
                  NAT_MAX_APP_NAME_LEN) == NAT_ZERO))
            {
                return SNMP_SUCCESS;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }

        if ((MEMCMP (au1ApplicationName, gaTrigInfo[i4ArrayIndex].au1AppName,
                     NAT_MAX_APP_NAME_LEN) == NAT_ZERO))
        {                        /* Cleaning the indexes for this invalid application name */
            for (i4ArrayIndex = NAT_ZERO; i4ArrayIndex < NAT_MAX_APP_ALLOWED;
                 i4ArrayIndex++)
            {
                if (gaTrigInfo[i4ArrayIndex].au1AppName[NAT_INDEX_0] ==
                    NAT_ZERO)
                {
                    MEMSET (&(gaTrigInfo[i4ArrayIndex]), NAT_ZERO,
                            sizeof (tTriggerInfo));
                }
            }
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    i1RetVal = SNMP_SUCCESS;

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2NatPortTrigInfoEntryStatus
 Input       :  The Indices
                NatPortTrigInfoInBoundPortRange
                NatPortTrigInfoOutBoundPortRange
                NatPortTrigInfoProtocol

                The Object 
                testValNatPortTrigInfoEntryStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatPortTrigInfoEntryStatus (UINT4 *pu4ErrorCode,
                                     tSNMP_OCTET_STRING_TYPE
                                     * pNatPortTrigInfoInBoundPortRange,
                                     tSNMP_OCTET_STRING_TYPE
                                     * pNatPortTrigInfoOutBoundPortRange,
                                     INT4 i4NatPortTrigInfoProtocol,
                                     INT4 i4TestValNatPortTrigInfoEntryStatus)
{
    INT1                i1RetVal = NAT_ZERO;
    INT4                i4ArrayIndex = NAT_ZERO;
    UINT1               au1InBoundPort[NAT_MAX_PORT_STRING_LEN] = { NAT_ZERO };
    UINT1               au1OutBoundPort[NAT_MAX_PORT_STRING_LEN] = { NAT_ZERO };
    /* PORTTRIGGER FIX  */
    UINT2               u2MaxPortVal = NAT_ZERO;
    tPortRange          aInPortRange[NAT_MAX_PORT_ALLOWED];
    tPortRange          aOutPortRange[NAT_MAX_PORT_ALLOWED];
    /* END */

    MEMSET (au1InBoundPort, NAT_ZERO, NAT_MAX_PORT_STRING_LEN);
    MEMCPY (au1InBoundPort, pNatPortTrigInfoInBoundPortRange->pu1_OctetList,
            pNatPortTrigInfoInBoundPortRange->i4_Length);

    MEMSET (au1OutBoundPort, NAT_ZERO, NAT_MAX_PORT_STRING_LEN);
    MEMCPY (au1OutBoundPort, pNatPortTrigInfoOutBoundPortRange->pu1_OctetList,
            pNatPortTrigInfoOutBoundPortRange->i4_Length);

    i1RetVal = nmhValidateIndexInstanceNatPortTrigInfoTable
        (pNatPortTrigInfoInBoundPortRange,
         pNatPortTrigInfoOutBoundPortRange, i4NatPortTrigInfoProtocol);

    /* Validating Row Status Value */
    switch (i4TestValNatPortTrigInfoEntryStatus)
    {
            /* CREATE AND WAIT is invalid for an Existing Entry */
        case CREATE_AND_WAIT:
            if (i1RetVal == SNMP_SUCCESS)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (SNMP_FAILURE);
            }

            MEMSET ((aInPortRange), NAT_ZERO,
                    (sizeof (tPortRange)) * (NAT_MAX_PORT_ALLOWED));
            MEMSET ((aOutPortRange), NAT_ZERO,
                    (sizeof (tPortRange)) * (NAT_MAX_PORT_ALLOWED));

            /* Converting Port range string to port range struct type
               and validating port no for in and out bound ports */
            if (NatPortString2PortRange
                ((aInPortRange), au1InBoundPort, &u2MaxPortVal) == NAT_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (SNMP_FAILURE);
            }

            if (NatPortString2PortRange
                ((aOutPortRange), au1OutBoundPort, &u2MaxPortVal)
                == NAT_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (SNMP_FAILURE);
            }

            /* Walk through the list to check overflow */
            i4ArrayIndex = NatGetFreeTrigInfoEntry ();

            if (i4ArrayIndex == NAT_MAX_APP_ALLOWED)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return (SNMP_FAILURE);
            }
            break;
            /* ACTIVE, DESTROY and NOT_IN_SERVICE is invalid for 
               non existing entries */
        case ACTIVE:
        case DESTROY:
        case NOT_IN_SERVICE:
            if (i1RetVal == SNMP_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return (SNMP_FAILURE);
            }
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);
    }

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    return (SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : NatPolicyTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceNatPolicyTable
 Input       :  The Indices
                NatPolicyType
                NatPolicyId
                pNatPolicyAclName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceNatPolicyTable (INT4 i4NatPolicyType,
                                        INT4 i4NatPolicyId,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pNatPolicyAclName)
{
    UINT1               au1AclName[NAT_MAX_ACL_NAME_LEN] = { NAT_ZERO };

    MEMSET (au1AclName, NAT_ZERO, NAT_MAX_ACL_NAME_LEN);
    /* 1. Validate if the NatPolicy Type is either static or dynamic. */
    if ((NAT_STATIC_POLICY != i4NatPolicyType) &&
        (NAT_DYNAMIC_POLICY != i4NatPolicyType))
    {
        return SNMP_FAILURE;
    }

    /* Validate if the NatPolicyId falls in the range (1..65535) */
    if ((NAT_MIN_POLICY_ID > i4NatPolicyId) ||
        (NAT_MAX_POLICY_ID < i4NatPolicyId))
    {
        return SNMP_FAILURE;
    }

    /* There must exists a valid filter in the system to configure the
     * Policy Entry. 
     */
    if (pNatPolicyAclName->i4_Length <= (NAT_MAX_ACL_NAME_LEN - NAT_ONE))
    {

        MEMCPY (au1AclName, pNatPolicyAclName->pu1_OctetList,
                MEM_MAX_BYTES (pNatPolicyAclName->i4_Length,
                               (INT4) sizeof (au1AclName)));
        au1AclName[pNatPolicyAclName->i4_Length] = '\0';
        if (NAT_FAILURE ==
            NatPolicyValidateFilter (i4NatPolicyType, au1AclName))
        {
            return SNMP_FAILURE;

        }
    }
    else
    {
        MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT",
                 "Filter Name Length exceeds the allowed limit");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexNatPolicyTable
 Input       :  The Indices
                NatPolicyType
                NatPolicyId
                pNatPolicyAclName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexNatPolicyTable (INT4 *pi4NatPolicyType,
                                INT4 *pi4NatPolicyId,
                                tSNMP_OCTET_STRING_TYPE * pNatPolicyAclName)
{
    tNatPolicyNode     *pNatPolicyNode = NULL;

    /* Search the Static Policy List First. */
    pNatPolicyNode = TMO_SLL_First (&gStaticPolicyNatList);
    if (NULL == pNatPolicyNode)
    {
        /* If entries are not there in Static Policy List. 
         * get the first entry from Dynamic Policy List. 
         */
        pNatPolicyNode = TMO_SLL_First (&gDynamicPolicyNatList);
        if (NULL == pNatPolicyNode)
        {
            return SNMP_FAILURE;
        }
        *pi4NatPolicyType = NAT_DYNAMIC_POLICY;
    }
    else
    {
        *pi4NatPolicyType = NAT_STATIC_POLICY;
    }

    /* Populate the indices from the first node fetched. */
    *pi4NatPolicyId = pNatPolicyNode->i4PolicyId;
    pNatPolicyAclName->i4_Length =
        (INT4) STRLEN ((INT1 *) pNatPolicyNode->au1AclName);
    MEMCPY (pNatPolicyAclName->pu1_OctetList, pNatPolicyNode->au1AclName,
            pNatPolicyAclName->i4_Length);

    pNatPolicyAclName->pu1_OctetList[pNatPolicyAclName->i4_Length] = '\0';
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexNatPolicyTable
 Input       :  The Indices
                NatPolicyType
                nextNatPolicyType
                NatPolicyId
                nextNatPolicyId
                pNatPolicyAclName
                pNextNatPolicyAclName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexNatPolicyTable (INT4 i4NatPolicyType,
                               INT4 *pi4NextNatPolicyType,
                               INT4 i4NatPolicyId,
                               INT4 *pi4NextNatPolicyId,
                               tSNMP_OCTET_STRING_TYPE * pNatPolicyAclName,
                               tSNMP_OCTET_STRING_TYPE * pNextNatPolicyAclName)
{
    tNatPolicyNode     *pNatPolicyNode = NULL;
    INT1                i1Found = NAT_FALSE;

    /* If the current index is of type static policy, then scan the static 
     * policy table to fetch the next entry.
     */
    if ((NAT_STATIC_POLICY == i4NatPolicyType) || (NAT_ZERO == i4NatPolicyType))
    {
        TMO_SLL_Scan (&gStaticPolicyNatList, pNatPolicyNode, tNatPolicyNode *)
        {
            if (((i4NatPolicyId == pNatPolicyNode->i4PolicyId) &&
                 (NAT_ZERO == pNatPolicyAclName->i4_Length)) ||
                ((NAT_ZERO == i4NatPolicyId) && (NAT_ZERO
                                                 ==
                                                 pNatPolicyAclName->i4_Length)))
            {
                *pi4NextNatPolicyType = NAT_STATIC_POLICY;
                i1Found = NAT_TRUE;
                break;
            }
            else if ((i4NatPolicyId == pNatPolicyNode->i4PolicyId) &&
                     (NAT_ZERO == (STRNCMP (pNatPolicyAclName->pu1_OctetList,
                                            pNatPolicyNode->au1AclName,
                                            STRLEN (pNatPolicyNode->
                                                    au1AclName)))))
            {
                pNatPolicyNode = TMO_SLL_Next (&gStaticPolicyNatList,
                                               &(pNatPolicyNode->
                                                 NatPolicyNode));
                if (NULL == pNatPolicyNode)
                {
                    /* Case 1 : If there is no next entry available in 
                     * the static policy list, get the first entry from the 
                     * dynamic policy list. 
                     */
                    pNatPolicyNode = TMO_SLL_First (&gDynamicPolicyNatList);
                    if (NULL == pNatPolicyNode)
                    {
                        return SNMP_FAILURE;
                    }
                    *pi4NextNatPolicyType = NAT_DYNAMIC_POLICY;
                    i1Found = NAT_TRUE;
                    break;
                }
                else
                {
                    /* Case 2 : There is a next entry available in 
                     * the static policy list. Populate the indices of the next 
                     * available node. 
                     */
                    *pi4NextNatPolicyType = NAT_STATIC_POLICY;
                    i1Found = NAT_TRUE;
                    break;
                }
            }
            else
            {
                continue;
            }
        }                        /* End of static policy NAT List scan */
    }
    else                        /* If the current type is of type dynamic. */
    {
        TMO_SLL_Scan (&gDynamicPolicyNatList, pNatPolicyNode, tNatPolicyNode *)
        {
            if (((i4NatPolicyId == pNatPolicyNode->i4PolicyId) &&
                 (NAT_ZERO == pNatPolicyAclName->i4_Length)) ||
                ((NAT_ZERO == i4NatPolicyId)
                 && (NAT_ZERO == pNatPolicyAclName->i4_Length)))
            {
                *pi4NextNatPolicyType = NAT_DYNAMIC_POLICY;
                i1Found = NAT_TRUE;
                break;
            }
            else if ((i4NatPolicyId == pNatPolicyNode->i4PolicyId) &&
                     (NAT_ZERO == (STRNCMP (pNatPolicyAclName->pu1_OctetList,
                                            pNatPolicyNode->au1AclName,
                                            STRLEN (pNatPolicyNode->
                                                    au1AclName)))))
            {
                pNatPolicyNode = TMO_SLL_Next (&gDynamicPolicyNatList,
                                               &(pNatPolicyNode->
                                                 NatPolicyNode));
                if (NULL == pNatPolicyNode)
                {
                    /* Case 3: No available next entry in the dynamic policy
                     * list. Return SNMP_FAILURE.
                     */
                    return SNMP_FAILURE;
                }
                /* Case 4 : A next entry available in the dynamic policy NAT 
                 * list. 
                 */
                *pi4NextNatPolicyType = NAT_DYNAMIC_POLICY;
                i1Found = NAT_TRUE;
                break;
            }
        }                        /* End of dynamic policy NAT List scan */
    }

    /* Get next for (0,0,0) 
     * The static policy linked list is NULL. Fetch the first entry 
     * from the dynamic policy linked list. 
     */
    if ((NAT_ZERO == i4NatPolicyType)
        && (NAT_ZERO == TMO_SLL_Count (&gStaticPolicyNatList)))
    {
        pNatPolicyNode = TMO_SLL_First (&gDynamicPolicyNatList);
        if (NULL == pNatPolicyNode)
        {
            return SNMP_FAILURE;
        }
        *pi4NextNatPolicyType = NAT_DYNAMIC_POLICY;
        i1Found = NAT_TRUE;

    }
    /* Populate the next indices in the output variables. */

    if (NAT_TRUE == i1Found)
    {
        *pi4NextNatPolicyId = pNatPolicyNode->i4PolicyId;
        pNextNatPolicyAclName->i4_Length =
            (INT4) STRLEN ((INT1 *) pNatPolicyNode->au1AclName);
        MEMCPY (pNextNatPolicyAclName->pu1_OctetList,
                pNatPolicyNode->au1AclName, pNextNatPolicyAclName->i4_Length);
        pNextNatPolicyAclName->pu1_OctetList[pNextNatPolicyAclName->i4_Length] =
            '\0';
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetNatPolicyTranslatedIp
 Input       :  The Indices
                NatPolicyType
                NatPolicyId
                pNatPolicyAclName

                The Object 
                retValNatPolicyTranslatedIp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatPolicyTranslatedIp (INT4 i4NatPolicyType,
                             INT4 i4NatPolicyId,
                             tSNMP_OCTET_STRING_TYPE * pNatPolicyAclName,
                             UINT4 *pu4RetValNatPolicyTranslatedIp)
{
    tTMO_SLL           *pNatPolicyList = NULL;
    tNatPolicyNode     *pNatPolicyNode = NULL;

    if (NAT_STATIC_POLICY == i4NatPolicyType)
    {
        pNatPolicyList = &gStaticPolicyNatList;
    }
    else
    {
        pNatPolicyList = &gDynamicPolicyNatList;
    }

    TMO_SLL_Scan (pNatPolicyList, pNatPolicyNode, tNatPolicyNode *)
    {
        /* There exists a node with the matching Policy Id and the filter ID. 
         * Populate the translated IP mapping for the same in the 
         * pu4RetValNatPolicyTranslatedIp.
         */
        if ((pNatPolicyNode->i4PolicyId == i4NatPolicyId) &&
            (NAT_ZERO == STRNCMP (pNatPolicyAclName->pu1_OctetList,
                                  pNatPolicyNode->au1AclName,
                                  STRLEN (pNatPolicyNode->au1AclName))))
        {
            *pu4RetValNatPolicyTranslatedIp =
                pNatPolicyNode->u4TranslatedLocIpAddr;
            return SNMP_SUCCESS;
        }
    }

    /* No entry available in the list or no matching entry available. */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNatPolicyEntryStatus
 Input       :  The Indices
                NatPolicyType
                NatPolicyId
                pNatPolicyAclName

                The Object 
                retValNatPolicyEntryStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatPolicyEntryStatus (INT4 i4NatPolicyType,
                            INT4 i4NatPolicyId,
                            tSNMP_OCTET_STRING_TYPE * pNatPolicyAclName,
                            INT4 *pi4RetValNatPolicyEntryStatus)
{
    tNatPolicyNode     *pNatPolicyNode = NULL;
    tTMO_SLL           *pNatPolicyList = NULL;

    if (NAT_STATIC_POLICY == i4NatPolicyType)
    {
        /* For Static Policy Type refer the Static Policy NAT list. */
        pNatPolicyList = &gStaticPolicyNatList;
    }
    else
    {
        /* For dynamic Policy Type refer the dynamic Policy NAT list. */
        pNatPolicyList = &gDynamicPolicyNatList;
    }

    TMO_SLL_Scan (pNatPolicyList, pNatPolicyNode, tNatPolicyNode *)
    {
        /* There exists a node with the matching Policy Id and the filter ID. 
         * Populate the translated IP mapping for the same in the 
         * pu4RetValNatPolicyTranslatedIp.
         */
        if ((pNatPolicyNode->i4PolicyId == i4NatPolicyId) &&
            (NAT_ZERO == STRNCMP (pNatPolicyAclName->pu1_OctetList,
                                  pNatPolicyNode->au1AclName,
                                  STRLEN (pNatPolicyNode->au1AclName))))
        {
            *pi4RetValNatPolicyEntryStatus = pNatPolicyNode->i4RowStatus;
            return SNMP_SUCCESS;
        }
    }                            /* End of scanning the NatPolicyList */

    /* No entry available in the list or no matching entry available. */
    return SNMP_FAILURE;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetNatPolicyTranslatedIp
 Input       :  The Indices
                NatPolicyType
                NatPolicyId
                pNatPolicyAclName

                The Object 
                setValNatPolicyTranslatedIp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatPolicyTranslatedIp (INT4 i4NatPolicyType,
                             INT4 i4NatPolicyId,
                             tSNMP_OCTET_STRING_TYPE * pNatPolicyAclName,
                             UINT4 u4SetValNatPolicyTranslatedIp)
{
    UINT4               u4SeqNum = NAT_ZERO;
    tTMO_SLL           *pNatPolicyList = NULL;
    tNatPolicyNode     *pNatPolicyNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    MEMSET (&SnmpNotifyInfo, NAT_ZERO, sizeof (tSnmpNotifyInfo));

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (NAT_STATIC_POLICY == i4NatPolicyType)
    {
        /* For Static Policy Type refer the Static Policy NAT list. */
        pNatPolicyList = &gStaticPolicyNatList;
    }
    else
    {
        /* For dynamic Policy Type refer the dynamic Policy NAT list. */
        pNatPolicyList = &gDynamicPolicyNatList;
    }

    TMO_SLL_Scan (pNatPolicyList, pNatPolicyNode, tNatPolicyNode *)
    {
        /* There exists a node with the matching Policy Id and the filter ID. 
         * Fill the translated IP information.
         */
        if ((pNatPolicyNode->i4PolicyId == i4NatPolicyId) &&
            (NAT_ZERO == STRNCMP (pNatPolicyAclName->pu1_OctetList,
                                  pNatPolicyNode->au1AclName,
                                  STRLEN (pNatPolicyNode->au1AclName))))
        {
            pNatPolicyNode->u4TranslatedLocIpAddr =
                u4SetValNatPolicyTranslatedIp;

            /* Notify to MSR */
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatPolicyTranslatedIp,
                                  u4SeqNum, FALSE, NatLock, NatUnLock,
                                  NAT_THREE, SNMP_SUCCESS);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %p", i4NatPolicyType,
                              i4NatPolicyId, pNatPolicyAclName,
                              u4SetValNatPolicyTranslatedIp));

            return SNMP_SUCCESS;
        }
    }                            /* End of scanning the NatPolicyList */

    /* No entry available in the list or no matching entry available. */
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetNatPolicyEntryStatus
 Input       :  The Indices
                NatPolicyType
                NatPolicyId
                pNatPolicyAclName

                The Object 
                setValNatPolicyEntryStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetNatPolicyEntryStatus (INT4 i4NatPolicyType,
                            INT4 i4NatPolicyId,
                            tSNMP_OCTET_STRING_TYPE * pNatPolicyAclName,
                            INT4 i4SetValNatPolicyEntryStatus)
{
    UINT4               u4SeqNum = NAT_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT1               au1AclName[NAT_MAX_ACL_NAME_LEN] = { NAT_ZERO };

    MEMSET (au1AclName, NAT_ZERO, NAT_MAX_ACL_NAME_LEN);
    MEMSET (&SnmpNotifyInfo, NAT_ZERO, sizeof (tSnmpNotifyInfo));

    RM_GET_SEQ_NUM (&u4SeqNum);
    MEMCPY (au1AclName, pNatPolicyAclName->pu1_OctetList,
            MEM_MAX_BYTES (pNatPolicyAclName->i4_Length,
                           (INT4) sizeof (au1AclName)));
    au1AclName[pNatPolicyAclName->i4_Length] = '\0';
    switch (i4SetValNatPolicyEntryStatus)
    {
            /* Intentional fallthrough */
        case NAT_STATUS_CREATE_AND_GO:
        case NAT_STATUS_CREATE_AND_WAIT:
            if (NAT_SUCCESS != NatPolicyEntryCreate
                (i4NatPolicyType,
                 i4NatPolicyId, au1AclName, i4SetValNatPolicyEntryStatus))
            {
                return SNMP_FAILURE;
            }
            break;

            /* Intentional fallthrough */
        case NAT_STATUS_NOT_IN_SERVICE:
        case NAT_STATUS_ACTIVE:
            if (NAT_SUCCESS != NatPolicyEntryUpdate
                (i4NatPolicyType,
                 i4NatPolicyId, au1AclName, i4SetValNatPolicyEntryStatus))
            {
                return SNMP_FAILURE;
            }
            break;

        case NAT_STATUS_DESTROY:
            if (NAT_SUCCESS != NatPolicyEntryDelete (i4NatPolicyType,
                                                     i4NatPolicyId, au1AclName))
            {
                return SNMP_FAILURE;
            }
            break;

        default:
            return SNMP_FAILURE;
    }

    /* Notify to MSR */

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, NatPolicyEntryStatus, u4SeqNum, TRUE,
                          NatLock, NatUnLock, NAT_THREE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %i", i4NatPolicyType,
                      i4NatPolicyId, pNatPolicyAclName,
                      i4SetValNatPolicyEntryStatus));

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2NatPolicyTranslatedIp
 Input       :  The Indices
                NatPolicyType
                NatPolicyId
                pNatPolicyAclName

                The Object 
                testValNatPolicyTranslatedIp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatPolicyTranslatedIp (UINT4 *pu4ErrorCode,
                                INT4 i4NatPolicyType,
                                INT4 i4NatPolicyId,
                                tSNMP_OCTET_STRING_TYPE * pNatPolicyAclName,
                                UINT4 u4TestValNatPolicyTranslatedIp)
{
    tNatPolicyNode     *pNatPolicyNode = NULL;

    UNUSED_PARAM (i4NatPolicyType);
#ifndef TRACE_WANTED
    UNUSED_PARAM (pNatPolicyAclName);
#endif
    /* Validate the IP address u4TestValNatPolicyTranslatedIp */
    if ((NatValidateIpAddress (u4TestValNatPolicyTranslatedIp,
                               NAT_GLOBAL) == NAT_FAILURE)
        || (u4TestValNatPolicyTranslatedIp == NAT_ZERO))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                      "The illegal Global Address in Policy  Table "
                      "of  index %d and %s \n",
                      i4NatPolicyId, pNatPolicyAclName->pu1_OctetList);
        CLI_SET_ERR (CLI_NAT_ERR_IP_INVALID);
        return SNMP_FAILURE;
    }

    /* Search if the translated IP address exists already in the policy NAT 
     * table. 
     */
    TMO_SLL_Scan (&gStaticPolicyNatList, pNatPolicyNode, tNatPolicyNode *)
    {
        if ((u4TestValNatPolicyTranslatedIp ==
             pNatPolicyNode->u4TranslatedLocIpAddr) &&
            (i4NatPolicyId != pNatPolicyNode->i4PolicyId))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_NAT_ERR_IP_CONFLICT);
            return SNMP_FAILURE;
        }
    }                            /* End of SLL scan of static policy List. */

    TMO_SLL_Scan (&gDynamicPolicyNatList, pNatPolicyNode, tNatPolicyNode *)
    {
        if ((u4TestValNatPolicyTranslatedIp ==
             pNatPolicyNode->u4TranslatedLocIpAddr) &&
            (i4NatPolicyId != pNatPolicyNode->i4PolicyId))

        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_NAT_ERR_IP_CONFLICT);
            return SNMP_FAILURE;
        }
    }                            /* End of SLL scan of static policy List. */

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2NatPolicyEntryStatus
 Input       :  The Indices
                NatPolicyType
                NatPolicyId
                pNatPolicyAclName

                The Object 
                testValNatPolicyEntryStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2NatPolicyEntryStatus (UINT4 *pu4ErrorCode,
                               INT4 i4NatPolicyType,
                               INT4 i4NatPolicyId,
                               tSNMP_OCTET_STRING_TYPE * pNatPolicyAclName,
                               INT4 i4TestValNatPolicyEntryStatus)
{
    INT1                i1RetStatus = SNMP_SUCCESS;
    UINT1               au1AclName[NAT_MAX_ACL_NAME_LEN];

    if (pNatPolicyAclName->i4_Length <= (NAT_MAX_ACL_NAME_LEN - NAT_ONE))
    {
        MEMSET (au1AclName, NAT_ZERO, NAT_MAX_ACL_NAME_LEN);
        MEMCPY (au1AclName, pNatPolicyAclName->pu1_OctetList,
                MEM_MAX_BYTES (pNatPolicyAclName->i4_Length,
                               (INT4) sizeof (au1AclName)));
        au1AclName[pNatPolicyAclName->i4_Length] = '\0';
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_NAT_ERR_INVALID_POLICY);
        return SNMP_FAILURE;
    }

    switch (i4TestValNatPolicyEntryStatus)
    {
            /* To set create and wait/create and go, no similar entry
             * should be existing previously. 
             * Intentional fallthrough.
             */
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:
            if (SNMP_FAILURE ==
                nmhValidateIndexInstanceNatPolicyTable (i4NatPolicyType,
                                                        i4NatPolicyId,
                                                        pNatPolicyAclName))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                CLI_SET_ERR (CLI_NAT_ERR_INVALID_POLICY);
                return SNMP_FAILURE;
            }

            if (NAT_SUCCESS == NatPolicyGetEntry (i4NatPolicyId, au1AclName))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_NAT_ERR_EXISTING_ENTRY);
                return SNMP_FAILURE;
            }

            break;

            /* To set the row status to be active/destroy/not-in-service, 
             * an entry should be existing already. 
             * Intentional fallthrough.
             */
        case ACTIVE:
        case NOT_IN_SERVICE:
        case DESTROY:
            if (NAT_FALSE == NatPolicyIsValid (i4NatPolicyType,
                                               i4NatPolicyId, au1AclName))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_NAT_ERR_INCONSISTENT_ENTRY);
                i1RetStatus = SNMP_FAILURE;
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            i1RetStatus = SNMP_FAILURE;
            break;

    }                            /* End of switch */

    return (i1RetStatus);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2NatPolicyTable
 Input       :  The Indices
                NatPolicyType
                NatPolicyId
                NatPolicyAclName
                
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2NatPolicyTable (UINT4 *pu4ErrorCode,
                        tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : NatRsvdPortTrigInfoTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceNatRsvdPortTrigInfoTable
 Input       :  The Indices
                NatRsvdPortTrigInfoAppIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceNatRsvdPortTrigInfoTable (INT4
                                                  i4NatRsvdPortTrigInfoAppIndex)
{
    /* Verify the range of the App Index. */

    if ((i4NatRsvdPortTrigInfoAppIndex < NAT_ZERO) ||
        (i4NatRsvdPortTrigInfoAppIndex >= NAT_MAX_APP_ALLOWED))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexNatRsvdPortTrigInfoTable
 Input       :  The Indices
                NatRsvdPortTrigInfoAppIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexNatRsvdPortTrigInfoTable (INT4 *pi4NatRsvdPortTrigInfoAppIndex)
{
    UINT1               u1AppCount = NAT_ZERO;

    for (u1AppCount = NAT_ZERO; u1AppCount < NAT_MAX_APP_ALLOWED; u1AppCount++)
    {
        if (gaRsvdTrigInfo[u1AppCount].u1Status == NAT_TRIG_ENTRY_AVAIL)
        {
            *pi4NatRsvdPortTrigInfoAppIndex = u1AppCount;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexNatRsvdPortTrigInfoTable
 Input       :  The Indices
                NatRsvdPortTrigInfoAppIndex
                nextNatRsvdPortTrigInfoAppIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexNatRsvdPortTrigInfoTable (INT4 i4NatRsvdPortTrigInfoAppIndex,
                                         INT4
                                         *pi4NextNatRsvdPortTrigInfoAppIndex)
{
    UINT1               u1AppCount = NAT_ZERO;

    if (i4NatRsvdPortTrigInfoAppIndex > NAT_MAX_NUM_IF)
    {
        NAT_TRC (NAT_TRC_ON, "No Such Instance\n");
        return SNMP_FAILURE;
    }

    for (u1AppCount = (UINT1) (i4NatRsvdPortTrigInfoAppIndex + NAT_ONE);
         u1AppCount < NAT_MAX_APP_ALLOWED; u1AppCount++)
    {
        if (gaRsvdTrigInfo[u1AppCount].u1Status == NAT_TRIG_ENTRY_AVAIL)
        {
            *pi4NextNatRsvdPortTrigInfoAppIndex = u1AppCount;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetNatRsvdPortTrigInfoLocalIp
 Input       :  The Indices
                NatRsvdPortTrigInfoAppIndex

                The Object 
                retValNatRsvdPortTrigInfoLocalIp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatRsvdPortTrigInfoLocalIp (INT4 i4NatRsvdPortTrigInfoAppIndex,
                                  UINT4 *pu4RetValNatRsvdPortTrigInfoLocalIp)
{
    if (gaRsvdTrigInfo[i4NatRsvdPortTrigInfoAppIndex].u1Status ==
        NAT_TRIG_ENTRY_AVAIL)
    {
        *pu4RetValNatRsvdPortTrigInfoLocalIp =
            gaRsvdTrigInfo[i4NatRsvdPortTrigInfoAppIndex].u4LocalIpAddr;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNatRsvdPortTrigInfoRemoteIp
 Input       :  The Indices
                NatRsvdPortTrigInfoAppIndex

                The Object 
                retValNatRsvdPortTrigInfoRemoteIp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatRsvdPortTrigInfoRemoteIp (INT4 i4NatRsvdPortTrigInfoAppIndex,
                                   UINT4 *pu4RetValNatRsvdPortTrigInfoRemoteIp)
{
    if (gaRsvdTrigInfo[i4NatRsvdPortTrigInfoAppIndex].u1Status ==
        NAT_TRIG_ENTRY_AVAIL)
    {
        *pu4RetValNatRsvdPortTrigInfoRemoteIp =
            gaRsvdTrigInfo[i4NatRsvdPortTrigInfoAppIndex].u4RemoteIpAddr;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNatRsvdPortTrigInfoStartTime
 Input       :  The Indices
                NatRsvdPortTrigInfoAppIndex

                The Object 
                retValNatRsvdPortTrigInfoStartTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatRsvdPortTrigInfoStartTime (INT4 i4NatRsvdPortTrigInfoAppIndex,
                                    UINT4
                                    *pu4RetValNatRsvdPortTrigInfoStartTime)
{
    if (gaRsvdTrigInfo[i4NatRsvdPortTrigInfoAppIndex].u1Status ==
        NAT_TRIG_ENTRY_AVAIL)
    {
        *pu4RetValNatRsvdPortTrigInfoStartTime =
            gaRsvdTrigInfo[i4NatRsvdPortTrigInfoAppIndex].u4TimeStamp;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNatRsvdPortTrigInfoAppName
 Input       :  The Indices
                NatRsvdPortTrigInfoAppIndex

                The Object 
                retValNatRsvdPortTrigInfoAppName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatRsvdPortTrigInfoAppName (INT4 i4NatRsvdPortTrigInfoAppIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValNatRsvdPortTrigInfoAppName)
{
    /* Return Failure if entry is not available. */
    if (gaRsvdTrigInfo[i4NatRsvdPortTrigInfoAppIndex].u1Status ==
        NAT_TRIG_ENTRY_FREE)
    {
        return SNMP_FAILURE;
    }

    pRetValNatRsvdPortTrigInfoAppName->i4_Length =
        (INT4) STRLEN (gaRsvdTrigInfo[i4NatRsvdPortTrigInfoAppIndex].pTrigInfo->
                       au1AppName);

    MEMCPY (pRetValNatRsvdPortTrigInfoAppName->pu1_OctetList,
            gaRsvdTrigInfo[i4NatRsvdPortTrigInfoAppIndex].pTrigInfo->au1AppName,
            pRetValNatRsvdPortTrigInfoAppName->i4_Length);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNatRsvdPortTrigInfoInBoundPortRange
 Input       :  The Indices
                NatRsvdPortTrigInfoAppIndex

                The Object 
                retValNatRsvdPortTrigInfoInBoundPortRange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetNatRsvdPortTrigInfoInBoundPortRange
    (INT4 i4NatRsvdPortTrigInfoAppIndex,
     tSNMP_OCTET_STRING_TYPE * pRetValNatRsvdPortTrigInfoInBoundPortRange)
{
    /* Return Failure if entry is not available. */
    if (gaRsvdTrigInfo[i4NatRsvdPortTrigInfoAppIndex].u1Status ==
        NAT_TRIG_ENTRY_FREE)
    {
        return SNMP_FAILURE;
    }

    pRetValNatRsvdPortTrigInfoInBoundPortRange->i4_Length =
        (INT4) STRLEN (gaRsvdTrigInfo[i4NatRsvdPortTrigInfoAppIndex].pTrigInfo->
                       au1InPortString);

    MEMCPY (pRetValNatRsvdPortTrigInfoInBoundPortRange->pu1_OctetList,
            gaRsvdTrigInfo[i4NatRsvdPortTrigInfoAppIndex].pTrigInfo->
            au1InPortString,
            pRetValNatRsvdPortTrigInfoInBoundPortRange->i4_Length);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNatRsvdPortTrigInfoOutBoundPortRange
 Input       :  The Indices
                NatRsvdPortTrigInfoAppIndex

                The Object 
                retValNatRsvdPortTrigInfoOutBoundPortRange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetNatRsvdPortTrigInfoOutBoundPortRange
    (INT4 i4NatRsvdPortTrigInfoAppIndex,
     tSNMP_OCTET_STRING_TYPE * pRetValNatRsvdPortTrigInfoOutBoundPortRange)
{
    /* Return Failure if entry is not available. */
    if (gaRsvdTrigInfo[i4NatRsvdPortTrigInfoAppIndex].u1Status ==
        NAT_TRIG_ENTRY_FREE)
    {
        return SNMP_FAILURE;
    }

    pRetValNatRsvdPortTrigInfoOutBoundPortRange->i4_Length =
        (INT4) STRLEN (gaRsvdTrigInfo[i4NatRsvdPortTrigInfoAppIndex].pTrigInfo->
                       au1OutPortString);

    MEMCPY (pRetValNatRsvdPortTrigInfoOutBoundPortRange->pu1_OctetList,
            gaRsvdTrigInfo[i4NatRsvdPortTrigInfoAppIndex].pTrigInfo->
            au1OutPortString,
            pRetValNatRsvdPortTrigInfoOutBoundPortRange->i4_Length);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNatRsvdPortTrigInfoProtocol
 Input       :  The Indices
                NatRsvdPortTrigInfoAppIndex

                The Object 
                retValNatRsvdPortTrigInfoProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNatRsvdPortTrigInfoProtocol (INT4 i4NatRsvdPortTrigInfoAppIndex,
                                   INT4 *pi4RetValNatRsvdPortTrigInfoProtocol)
{
    /* Return Failure if entry is not available. */
    if (gaRsvdTrigInfo[i4NatRsvdPortTrigInfoAppIndex].u1Status ==
        NAT_TRIG_ENTRY_FREE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValNatRsvdPortTrigInfoProtocol =
        gaRsvdTrigInfo[i4NatRsvdPortTrigInfoAppIndex].pTrigInfo->u2Protocol;
    return SNMP_SUCCESS;

}
#endif    /* _FSNATLW_C */                   /* END OF THE FILE FSNATLW.C */
