
/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: natnpapi.c,v 1.2 2014/10/16 13:04:16 siva Exp $
 *
 * Description: This file contains function for invoking NP calls of NAT module.
 ******************************************************************************/

#ifndef __NAT_NPAPI_C__
#define __NAT_NPAPI_C__

#include "nputil.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : NatFsNpIpv4ArpAdd                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4ArpAdd
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4ArpAdd
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
NatFsNpIpv4ArpAdd (tNpVlanId u2VlanId, UINT4 u4IfIndex, UINT4 u4IpAddr,
                   UINT1 *pMacAddr, UINT1 *pu1IfName, INT1 i1State,
                   UINT4 *pu4TblFull)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4ArpAdd *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_ARP_ADD,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4ArpAdd;

    pEntry->u2VlanId = u2VlanId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->pMacAddr = pMacAddr;
    pEntry->pu1IfName = pu1IfName;
    pEntry->i1State = i1State;
    pEntry->pu4TblFull = pu4TblFull;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : NatFsNpIpv4ArpDel                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4ArpDel
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4ArpDel
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
NatFsNpIpv4ArpDel (UINT4 u4IpAddr, UINT1 *pu1IfName, INT1 i1State)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4ArpDel *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_ARP_DEL,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4ArpDel;

    pEntry->u4IpAddr = u4IpAddr;
    pEntry->pu1IfName = pu1IfName;
    pEntry->i1State = i1State;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
#endif /* __NAT_NPAPI_C__ */
