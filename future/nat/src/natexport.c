
/********************************************************************
 * Copyright (C) Future Sotware,2002
 *
 * $Id: natexport.c,v 1.21 2016/03/09 11:44:27 siva Exp $
 *
 * Description: This file is added to define all functions which are
 *              exported to other modules (called by other modules)
 *              like IP etc.
 ***********************************************************************/
#ifndef _NATEXPORT_C
#define _NATEXPORT_C

#include "natinc.h"
#include "fsnatlw.h"
 /*************************************************************************
  * Function Name : NatHandleInterfaceIndication
  *
  * Description   : Handles indications to NAT on Creation, Enabling, 
  *                 Disabling or Deletion of Interface,
  * Inputs        : u4Interface - Interface number on which the indication is 
  *                 received.
  *                 u4IpAddress - The IP address of the interface.
  *                 u4Status - Indicates whether the IP address have to be 
  *                 added or deleted from Nat Interface Info.
  *
  *              CREATE_AND_WAIT - interface has been created, but there is 
  *                                 no IP for the interface
  *              CREATE_AND_GO - Interface has been created with IP assigned
  *              NOT_IN_SERVICE - The interface became down
  *              ACTIVE - The Interface became active, with IP assigned to it
  *              DESTROY - The interface just got destroyed
  *              
  * Output(s)     : None
  * Return        : NAT_SUCCESS or NAT_FAILURE
  *************************************************************************/
PUBLIC INT1
NatHandleInterfaceIndication (UINT4 u4Interface, UINT4 u4IpAddress,
                              UINT4 u4Status)
{
    tInterfaceInfo     *pIfNode = NULL;
    tTranslatedLocIpAddrNode *pGlobalNode = NULL;
    UINT4               u4RowStatus = NAT_ZERO;
    UINT1               u1IfNwType = NAT_ZERO;
    tCfaIfInfo          IfInfo;

    MEMSET (&IfInfo, NAT_ZERO, sizeof (tCfaIfInfo));

    if ((u4Interface == NAT_ZERO) || (u4Interface > NAT_MAX_NUM_IF))
    {
        /* Invalid interface index */
        return NAT_FAILURE;
    }

    /* Getting the type of interface */
    if (SecUtilGetIfNwType (u4Interface, &u1IfNwType) == CFA_FAILURE)
    {
        return NAT_FAILURE;
    }
    if (SecUtilGetIfInfo (u4Interface, &IfInfo) == OSIX_FAILURE)
    {
        return NAT_FAILURE;
    }

    pIfNode = gapNatIfTable[u4Interface];

    switch (u4Status)
    {
        case ACTIVE:

            if (pIfNode == NULL)
            {
                /* No indication received while CREATE_AND_GO or 
                 * CREATE_AND_WAIT of interface or the interface has already 
                 * been DESTROYed */
                return NAT_FAILURE;
            }

            /* Delete the dynamic entries only if the IP Address is changed/ Interface is made down.
             * Always the first node in the TranslatedLocIpList 
             * is the interface Ip, so check with
             * the first node for the change in IP address */
            pGlobalNode = (tTranslatedLocIpAddrNode *)
                TMO_SLL_First
                (&(gapNatIfTable[u4Interface]->TranslatedLocIpList));

            if (pGlobalNode != NULL)
            {
                if ((pGlobalNode->u4TranslatedLocIpAddr != NAT_ZERO) &&
                    ((pGlobalNode->u4TranslatedLocIpAddr != u4IpAddress) ||
                     (IfInfo.u1IfOperStatus == NAT_IFOPER_STAUS_DOWN)))
                {
                    if (gapNatIfTable[u4Interface]->u1NatEnable == NAT_ENABLE)
                    {
                        /* remove all the translation through the interface */
                        NatDeleteDynamicEntry (u4Interface);
                    }
                    /* Interface is made down so delete interface IP 
                     * from default global pool. */
                    if (NatConfigureGlobalIp (u4Interface,
                                              pGlobalNode->
                                              u4TranslatedLocIpAddr,
                                              NAT_DELETE) == NAT_FAILURE)
                    {
                        return NAT_FAILURE;
                    }
                }
            }
            /* If the interface to be conigured does not have an IP Address, 
             * disable NAT on the interface */

            if (u4IpAddress == NAT_ZERO)
            {
                gapNatIfTable[u4Interface]->u1NatEnable = NAT_DISABLE;
            }
            else
            {

		/*While CREATE_AND_WAIT we enabled NAT by default
 		 *Enabling it again not required*/
                if (pIfNode->u1NatEnable == NAT_ENABLE)
                {
                    /* if we receive indication that the interface is made active 
                     * add the given interface IP to global pool. This will 
                     * facilitate connections from LAN-WAN using this IP,if no 
                     * pool is configured.*/
                    if (NatConfigureGlobalIp
                        (u4Interface, u4IpAddress, NAT_CREATE) == NAT_FAILURE)
                    {
                        return NAT_FAILURE;
                    }
                }
                /* Configure all the virtual servers with newly acquired IP */
                NatAutoVirtualServerConfig (u4Interface, u4IpAddress);
            }
            break;

        case NOT_IN_SERVICE:

            break;

        case CREATE_AND_GO:

            /* Nat interface entry is created with state as active
             * with NAT, NAPT and Two way NAT enabled */
            if (NatInterfaceEntryCreate (u4Interface, NAT_ENABLE_ALL) ==
                NAT_FAILURE)
            {
                return NAT_FAILURE;
            }

            if (NatConfigureGlobalIp (u4Interface, u4IpAddress, NAT_CREATE) ==
                NAT_FAILURE)
            {
                return NAT_FAILURE;
            }
            break;

        case CREATE_AND_WAIT:

            /* Nat interface entry is created with state as active
             * with NAT, NAPT and Two way NAT enabled */
            if ((u1IfNwType == CFA_NETWORK_TYPE_WAN) &&
                (IfInfo.u1WanType == CFA_WAN_TYPE_PUBLIC))
            {
                u4RowStatus = NAT_ENABLE_ALL;
            }
            else
            {
                u4RowStatus = NOT_IN_SERVICE;
            }
            if (NatInterfaceEntryCreate (u4Interface, u4RowStatus)
                == NAT_FAILURE)
            {
                return NAT_FAILURE;
            }

            break;

        case DESTROY:
            /* Destroy the interface entry after freeing all the memory 
             * allocated */
            if (pIfNode != NULL)
            {
                if (gapNatIfTable[u4Interface]->u1NatEnable == NAT_ENABLE)
                {
                    /* remove all the translation through the interface */
                    NatDeleteDynamicEntry (u4Interface);
                }
                /* Interface is made down so delete interface IP
                 * from default global pool. */
                if (NatConfigureGlobalIp (u4Interface, u4IpAddress, NAT_DELETE)
                    == NAT_FAILURE)
                {
                    return NAT_FAILURE;
                }
                /* 
                 * Destroy the Nat Interface Entry with the deletion of Wan 
                 * Interface 
                 */
                NatDeInitForIf (u4Interface);
            }
            else
            {
                return NAT_FAILURE;
            }
            break;
        default:
            break;
    }
    return NAT_SUCCESS;
}

/***************************************************************************
 * Function Name : NatAutoVirtualServerConfig                              *
 * Description   : Adds/Deletes the IP address given as input into the Nat *
 *                 Interface table for the given Interface.                *
 * Inputs        : u4IfIndex   - Interface Index                           *
 *                 u4IpAddress - The Gloabl IP address which has to be     *
 *                 activated for NAPT or deleted.                          *
 * Output(s)     : None                                                    *
 * Return        : SUCCESS or FAILURE                                      *
 **************************************************************************/
PUBLIC INT1
NatAutoVirtualServerConfig (UINT4 u4IfIndex, UINT4 u4IpAddress)
{
    tInterfaceInfo     *pIfNode = NULL;
    tStaticNaptEntry   *pStaticNaptEntry = NULL;

    pIfNode = gapNatIfTable[u4IfIndex];

    if (pIfNode != NULL)
    {
        TMO_SLL_Scan (&pIfNode->StaticNaptList, pStaticNaptEntry,
                      tStaticNaptEntry *)
        {
            pStaticNaptEntry->u4TranslatedLocIpAddr = u4IpAddress;
        }
    }

    return SUCCESS;
}

 /*************************************************************************
  * Function Name : NatAutoIpAddressConfig
  *
  * Description   : Adds/Deletes the IP address given as input into the Nat
  *                 Interface
  *                 table for the given Interface.
  * Inputs -  u4Interface - Interafce number on which the IP addr has to be
  *                         made active or deleted.
  *           u4IpAddress - The Gloabl IP address which has to be activated
  *                         for  NAPT or deleted.
  *           u4Status - Indicates whether the IP address have to be added or
  *                      deleted from Nat Interface Info.
  *
  * Output(s) - None
  * Return   - SUCCESS or FAILURE
  *************************************************************************/
PUBLIC INT1
NatAutoIpAddressConfig (UINT4 u4Interface, UINT4 u4IpAddress, UINT4 u4Status)
{
    UINT4               u4ErrCode = NAT_ZERO;
    UINT4               u4WanInterfaceIndex = NAT_ZERO;

    u4WanInterfaceIndex = u4Interface;

    if ((u4WanInterfaceIndex == NAT_ZERO) ||
        (u4WanInterfaceIndex > NAT_MAX_NUM_IF))
    {
        /* WAN IF is not created VPND ADD */
        return FAILURE;
    }

    /* number is present or not and Nat Flag is enabled or not. */
    if (u4Status == NAT_PPP_NAPT_IPADDR_ACTIVE)
    {
        /* Create a Trans IP address entry in the Interface Table. */
        if (nmhTestv2NatGlobalAddressEntryStatus (&u4ErrCode,
                                                  (INT4) u4Interface,
                                                  u4IpAddress,
                                                  NAT_STATUS_CREATE_AND_WAIT)
            != SNMP_SUCCESS)
        {
            return FAILURE;
        }

        /* Here checking for the interface, is it is active WAN interface
         * index then only set the Nat Status as enable.
         */
        /*
         * NAT_INIT_MASK = 0xffffffff
         */
        /* Interface is WAN interface ADD default global pool
         * (first in transkated IP address list)
         * as WAN interface address for single host, this will
         * facilitate connections from LAN-WAN using this global
         * pool if no pool is configured.
         */

        if (nmhSetNatGlobalAddressEntryStatus ((INT4) u4Interface,
                                               u4IpAddress,
                                               NAT_STATUS_CREATE_AND_WAIT) !=
            SNMP_SUCCESS)
        {
            /* It   should not come here.  */
        }

        nmhSetNatGlobalAddressMask ((INT4) u4Interface, u4IpAddress,
                                    NAT_INIT_MASK);
        nmhSetNatGlobalAddressEntryStatus ((INT4) u4Interface, u4IpAddress,
                                           NAT_STATUS_ACTIVE);

        /* VPND ADD - Update Static Entries After Got IP Address
         * from DHCP Server */
        {
            tStaticNaptEntry   *pStaticNaptEntryNode = NULL;
            TMO_SLL_Scan (&(gapNatIfTable[u4Interface]->StaticNaptList),
                          pStaticNaptEntryNode, tStaticNaptEntry *)
            {
                pStaticNaptEntryNode->i4RowStatus = NAT_STATUS_ACTIVE;
                pStaticNaptEntryNode->u4TranslatedLocIpAddr = u4IpAddress;
            }
        }

    }
    else if (u4Status == NAT_PPP_NAPT_IPADDR_DESTROY)
    {
        if (gapNatIfTable[u4Interface] != NULL)
        {
            /*
             * Destroy (Remove) the Trans IP address in the Nat Interface Table.
             */
            /* Interface is WAN interface, delete default global pool.
             */

            nmhSetNatGlobalAddressEntryStatus ((INT4) u4Interface,
                                               u4IpAddress, NAT_STATUS_DESTROY);
            /* Replaced by corresponding low level */
            NatDeleteDynamicEntry (u4Interface);
        }

    }
    else
        return FAILURE;
    return SUCCESS;
}
#endif /* _NATEXPORT_C */
