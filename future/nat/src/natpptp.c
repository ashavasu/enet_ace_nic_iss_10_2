/********************************************************************
 * Copyright (C) Future Sotware,2002
 * 
 *  $Id: natpptp.c,v 1.4 2013/10/25 10:52:27 siva Exp $  
 *  
 *  Description:This file contains functions required for
 *             modifying the pptp payload if required.
 *
 ********************************************************************/
#include "natinc.h"
PRIVATE INT4
     
     
     
     PptpAlgProcessCallRequest
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo));
PRIVATE INT4        PptpAlgProcessCallReply
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo));
PRIVATE INT4        PptpAlgPrcsOutboundCallReq
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo));
PRIVATE INT4        PptpAlgPrcsInboundCallReq
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo));
PRIVATE INT4        PptpAlgPrcsOutboundCallReply
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo));
PRIVATE INT4        PptpAlgPrcsInboundCallReply
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo));
PRIVATE INT4        PptpAlgProcessCallDiscNotify
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo));
PRIVATE INT4        PptpAlgProcessSetLinkInfo
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo));
PRIVATE INT4        PptpAlgPrcsOutboundDataPkt
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo,
           UINT2 u2CallId));
PRIVATE INT4        PptpAlgPrcsInboundDataPkt
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo,
           UINT2 u2CallId));
PRIVATE INT4 NatGetTransInfo ARG_LIST ((tHeaderInfo * pHeaderInfo));
PRIVATE INT4 NatCreateTransInfo ARG_LIST ((tHeaderInfo * pHeaderInfo));

VOID NatFwlGREDynamicRuleHandler ARG_LIST ((tHeaderInfo * pHeaderInfo,
                                            UINT2 u2Flag));

/* Globals manipulated by PPTP Alg and NAT */
/* By setting its value to TRUE, this Flag can be used to indicate the Nat 
 * module that a new session entry needs to be created in the NAT module.
 * Otherwise the session info is only fetched from the Nat Table */

/* By setting its value to TRUE, this Flag can be used to indicate the Nat 
 * module that the Inside Port need not be modified to a new Port */
UINT4               gu4NatPptpOutCreateFlag = FALSE;

/**************************************************************************
* Name           :  NatProcessPPTPCntrlPkt 
* Description    : Processes the PPTP control packets 
* 
*
* Input (s)   : pBuf - pointer to the packet.
*               pHeaderInfo - pointer to the structure containing information
*                             about the IP and the TCP header.
*
* Output (s)  : The translated PPTP Control packets.
*
* Returns     : NAT_SUCCESS if the packet is to be forwarded.
*               NAT_FAILURE if the packet is to be dropped.
*
* ****************************************************************************/

PUBLIC INT4
NatProcessPPTPCntrlPkt (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo)
{
    UINT4               u4PktLen = NAT_ZERO;
    UINT4               u4PptpOffset = NAT_ZERO;
    tPptpCntrlPkt       PptpCntrlPkt;

    /* 
     * The following info are present in pHeaderInfo structure passed from 
     * the NAT module -
     * 1. Src + Dest IP address
     * 2. IP header length
     * 3. Total IP pkt len
     * 4. IP pkt type
     * 5. Src + Dest Port
     * 6. TCP Header Len
     */
    NAT_TRC (NAT_TRC_ON, "\n Inside NatProcessPptpControlPkt\n");
    u4PktLen = pHeaderInfo->u2TotLen;
    u4PptpOffset =
        (UINT4) (pHeaderInfo->u1IpHeadLen + pHeaderInfo->u1TransportHeadLen);
    if (u4PktLen > u4PptpOffset)
    {
        if ((u4PktLen <
             (UINT4) (pHeaderInfo->u1IpHeadLen +
                      pHeaderInfo->u1TransportHeadLen + NAT_PPTP_MIN_LEN)))
        {
            NAT_DBG (NAT_DBG_PPTP, "\n PPTP Cntrl Pkt length not proper\n");
            return NAT_FAILURE;
        }
    }
    else
    {
        /* the packet does not need a PPTP translation , but it should be
         *  forwarded after IP and TCP translation */
        return NAT_SUCCESS;
    }

    /* Validating the Control packets */
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &PptpCntrlPkt,
                               u4PptpOffset, sizeof (tPptpCntrlPkt));
    PptpCntrlPkt.u2Length = OSIX_NTOHS (PptpCntrlPkt.u2Length);
    PptpCntrlPkt.u2PptpMessageType =
        OSIX_NTOHS (PptpCntrlPkt.u2PptpMessageType);
    PptpCntrlPkt.u4MagicCookie = OSIX_NTOHL (PptpCntrlPkt.u4MagicCookie);
    PptpCntrlPkt.u2CntrlMsgType = OSIX_NTOHS (PptpCntrlPkt.u2CntrlMsgType);
    PptpCntrlPkt.u2Reserved = OSIX_NTOHS (PptpCntrlPkt.u2Reserved);

    if (PptpCntrlPkt.u2Length != (u4PktLen - (UINT4) (pHeaderInfo->u1IpHeadLen +
                                                      pHeaderInfo->
                                                      u1TransportHeadLen)))
    {
        return NAT_FAILURE;
    }
    if (PptpCntrlPkt.u4MagicCookie != PPTP_MAGIC_COOKIE)
    {
        return NAT_FAILURE;
    }
    /* validation of PPTP control packets ends here */

    switch (PptpCntrlPkt.u2CntrlMsgType)
    {
        case PPTP_OUT_CALL_REQUEST:
            /*fallthrough */
        case PPTP_IN_CALL_REQUEST:
            if (PptpAlgProcessCallRequest (pBuf, pHeaderInfo) == NAT_FAILURE)
            {
                return NAT_FAILURE;
            }
            /* Punch Firewall hole using the dynamic filters and acls */
            break;

        case PPTP_OUT_CALL_REPLY:
            /*fallthrough */
        case PPTP_IN_CALL_REPLY:
            if (PptpAlgProcessCallReply (pBuf, pHeaderInfo) == NAT_FAILURE)
            {
                return NAT_FAILURE;
            }
            break;

        case PPTP_CALL_DISC_NOTIFY:
            if (PptpAlgProcessCallDiscNotify (pBuf, pHeaderInfo) == NAT_FAILURE)
            {
                return NAT_FAILURE;
            }
            /* Delete dynamic Filters and Acls created for 
             * this control connection */
            break;

        case PPTP_CALL_SET_LINK_INFO:
            if (PptpAlgProcessSetLinkInfo (pBuf, pHeaderInfo) == NAT_FAILURE)
            {
                return NAT_FAILURE;
            }
            break;

        case PPTP_STOP_CONTROL_REQUEST:
            /* Delete dynamic Filters and Acls created for 
             * this control connection */
            break;
        default:
            /* The packet should be forwarded without PPTP translation */
            break;
    }
    /*NatModify TCP header - done by parent module */
    /*NatModify IP header - done by parent module */

    NAT_TRC (NAT_TRC_ON, "\n Exiting NatProcessPptpControlPkt \n");
    return NAT_SUCCESS;
}

/**************************************************************************
* Name           :  NatProcessPPTPDataPkt 
* Description    : Processes the PPTP control packets 
* 
*
* Input (s)   : pBuf - pointer to the packet.
*               pHeaderInfo - pointer to the structure containing information
*                             about the IP and the TCP header.
*
* Output (s)  : The translated PPTP Control packets.
*
* Returns     : NAT_SUCCESS if the packet is to be forwarded.
*               NAT_FAILURE if the packet is to be dropped.
*
* ****************************************************************************/

PUBLIC INT4
NatProcessPPTPDataPkt (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo)
{
    UINT4               u4PktLen = NAT_ZERO;
    UINT4               u4GreOffset = NAT_ZERO;
    UINT4               u4AckSeqBytes = NAT_ZERO;
    tPptpDataPkt        PptpDataPkt;
    NAT_TRC (NAT_TRC_ON, "\n Inside NatProcessPptpDataPkt\n");
    /*
     * assuming that the following IP info are available 
     * 1.Src + Dest IP address
     * 2. IP hdr Len
     * 3. Total IP Pkt Len
     * 4. IP Pkt type
     * */

    /*  Validating the PPTP Data Pkt */
    u4PktLen = CRU_BUF_Get_ChainValidByteCount (pBuf);
    u4GreOffset = pHeaderInfo->u1IpHeadLen;
    if (u4PktLen < (UINT4) (pHeaderInfo->u1IpHeadLen + NAT_GRE_MIN_LEN))
    {
        NAT_DBG (NAT_DBG_PPTP, "\n PPTP Data Pkt length not proper\n");
        return NAT_FAILURE;
    }

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &PptpDataPkt,
                               u4GreOffset, NAT_GRE_MIN_LEN);
    PptpDataPkt.u2FlagsVer = OSIX_NTOHS (PptpDataPkt.u2FlagsVer);
    PptpDataPkt.u2ProtoType = OSIX_NTOHS (PptpDataPkt.u2ProtoType);
    PptpDataPkt.u2KeyLen = OSIX_NTOHS (PptpDataPkt.u2KeyLen);
    PptpDataPkt.u2KeyCallId = OSIX_NTOHS (PptpDataPkt.u2KeyCallId);
    /*
     * Validation of the following fields in the GRE header-
     * 1. Falgs field - it must be zero
     * 2. Version field - it must be 1
     * 3. Protocol type - it must be 0x880B for PPP
     */
    if ((PPTPDATAHDR_INVALID_FLAGS (PptpDataPkt.u2FlagsVer)) ||
        (PPTPDATAHDR_INVALID_VER (PptpDataPkt.u2FlagsVer)) ||
        (PptpDataPkt.u2ProtoType != GRE_PPTP_PROTO_TYPE))
    {
        NAT_DBG (NAT_DBG_PPTP, "\n PPTP Data Pkt Hdr Validation failure\n");
        return NAT_FAILURE;
    }
    /* Validating the length field */
    if (PPTPDATA_HDR_SEQ_PRESENT (PptpDataPkt.u2FlagsVer))
    {
        u4AckSeqBytes = NAT_SEQ_NUM_LEN;
    }
    if (PPTPDATA_HDR_ACK_PRESENT (PptpDataPkt.u2FlagsVer))
    {
        u4AckSeqBytes += NAT_ACK_NUM_LEN;
    }
    if (u4PktLen !=
        (UINT4) ((UINT4) pHeaderInfo->u1IpHeadLen + NAT_GRE_MIN_LEN +
                 u4AckSeqBytes + (UINT4) PptpDataPkt.u2KeyLen))
    {
        NAT_DBG (NAT_DBG_PPTP, "\n PPTP Data Pkt length not proper\n");
        return NAT_FAILURE;
    }

    if (pHeaderInfo->u4Direction == NAT_OUTBOUND)
    {
        if (PptpAlgPrcsOutboundDataPkt
            (pBuf, pHeaderInfo, PptpDataPkt.u2KeyCallId) == NAT_FAILURE)
        {
            return NAT_FAILURE;
        }
    }
    else
    {
        if (PptpAlgPrcsInboundDataPkt
            (pBuf, pHeaderInfo, PptpDataPkt.u2KeyCallId) == NAT_FAILURE)
        {
            return NAT_FAILURE;
        }
    }

    NAT_TRC (NAT_TRC_ON, "\n Exiting NatProcessPptpDataPkt \n");
    return NAT_SUCCESS;

}

/**************************************************************************
* Name           :  PptpAlgProcessCallRequest
* Description    : Processes the PPTP control packet of control msg type 
*                  Incoming Call Request or Outgoing Call Request.
* 
*
* Input (s)   : pBuf - pointer to the packet.
*               pHeaderInfo - pointer to the structure containing information
*                             about the IP header.
*
* Output (s)  : The translated PPTP Control packets.
*
* Returns     : NAT_SUCCESS if the packet is to be forwarded.
*               NAT_FAILURE if the packet is to be dropped.
*
* ****************************************************************************/

PRIVATE INT4
PptpAlgProcessCallRequest (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo
                           * pHeaderInfo)
{

    if (pHeaderInfo->u4Direction == NAT_OUTBOUND)
    {
        if (PptpAlgPrcsOutboundCallReq (pBuf, pHeaderInfo) == NAT_FAILURE)
        {
            return NAT_FAILURE;
        }
    }
    else
    {
        if (PptpAlgPrcsInboundCallReq (pBuf, pHeaderInfo) == NAT_FAILURE)
        {
            return NAT_FAILURE;
        }
    }
    return NAT_SUCCESS;

}

/**************************************************************************
* Name           :  PptpAlgPrcsOutboundCallReq
* Description    : Processes the PPTP control packet of control msg type 
*                  Incoming Call Request or Outgoing Call Request in the 
*                  Outbound direction (from Inside Network to Outside
*                   Network).
* 
*
* Input (s)   : pBuf - pointer to the packet.
*               pHeaderInfo - pointer to the structure containing information
*                             about the IP header.
*
* Output (s)  : The translated PPTP Control packets.
*
* Returns     : NAT_SUCCESS if the packet is to be forwarded.
*               NAT_FAILURE if the packet is to be dropped.
*
* ****************************************************************************/

PRIVATE INT4
PptpAlgPrcsOutboundCallReq (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo
                            * pHeaderInfo)
{
    tHeaderInfo         PptpHdrInfo;
    UINT4               u4SrcIpAddr = NAT_ZERO;
    UINT2               u2OldCallId = NAT_ZERO;
    UINT2               u2NewCallId = NAT_ZERO;
    UINT4               u4PptpOffset = NAT_ZERO;
    UINT4               u4TcpCsumOffset = NAT_ZERO;
    UINT2               u2TcpCksum = NAT_ZERO;

    u4PptpOffset =
        (UINT4) (pHeaderInfo->u1IpHeadLen + pHeaderInfo->u1TransportHeadLen);
    u4TcpCsumOffset = (UINT4) pHeaderInfo->u1IpHeadLen + NAT_TCP_CKSUM_OFFSET;

    /* 
     * The Src IP address cannot be taken from the pHeaderInfo structure 
     * because the pHeaderInfo structure contains the Translated Src Ip 
     * address, not the original IP address 
     */
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u4SrcIpAddr,
                               NAT_IP_SRC_OFFSET, NAT_IP_ADDR_LEN);

    u4SrcIpAddr = OSIX_NTOHL (u4SrcIpAddr);
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2OldCallId,
                               u4PptpOffset + NAT_PPTP_CALLID_OFFSET,
                               NAT_PPTP_CALLID_LEN);

    PptpHdrInfo.u4IfNum = pHeaderInfo->u4IfNum;
    PptpHdrInfo.i4Delta = NAT_ZERO;
    PptpHdrInfo.pDynamicEntry = NULL;
    if (pHeaderInfo->u2OutPort == NAT_PPTP_PORT)
    {
        /*
         * A request from a PAC to PNS . PNS is on the Global
         * Network. The PAC Call Id present in the request packet 
         * needs to be changed if NAPT is enabled.
         */
        NAT_DBG (NAT_DBG_PPTP,
                 "\nPPTP Cntrl Pkt Type -Outbound Request- PAC to PNS \n");
        PptpHdrInfo.u2InPort = OSIX_NTOHS (u2OldCallId);
        PptpHdrInfo.u2OutPort = NAT_ZERO;
        PptpHdrInfo.u4InIpAddr = u4SrcIpAddr;
        PptpHdrInfo.u4OutIpAddr = pHeaderInfo->u4OutIpAddr;
        PptpHdrInfo.u4Direction = NAT_OUTBOUND;
        PptpHdrInfo.u1PktType = NAT_PPTP;

        if (NatGetTransInfo (&PptpHdrInfo) == NAT_FAILURE)
        {
            /*
             *  Session info is not present in the NAT table 
             *  So creating an entry in the Nat Table.
             */
            if (NatCreateTransInfo (&PptpHdrInfo) == NAT_FAILURE)
            {
                NAT_DBG (NAT_DBG_PPTP, "\nPPTPCntrl-Outbound-Req-PAC to PNS:\
                         Failure in Creating session entry for PAC\n");
                return NAT_FAILURE;
            }

        }
        /* Copy the new PAC Call Id and adjust the checksum */
        u2NewCallId = OSIX_HTONS (PptpHdrInfo.u2InPort);
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2NewCallId,
                                   u4PptpOffset + NAT_PPTP_CALLID_OFFSET,
                                   NAT_PPTP_CALLID_LEN);
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2TcpCksum, u4TcpCsumOffset,
                                   NAT_WORD_LEN);
        NatChecksumAdjust ((UINT1 *) &u2TcpCksum, (UINT1 *) &u2OldCallId,
                           NAT_PORT_LEN, (UINT1 *) &u2NewCallId, NAT_PORT_LEN);
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2TcpCksum, u4TcpCsumOffset,
                                   NAT_WORD_LEN);

    }
    else if (pHeaderInfo->u2InPort == NAT_PPTP_PORT)
    {
        /*
         * PNS is in the Inside Network. The request is from the
         * PNS to the PAC. So we need not modify the PNS CallID,
         * but we store the session info to be used by PPTP Data
         * packets
         */
        NAT_DBG (NAT_DBG_PPTP,
                 "\nPPTP Cntrl Pkt Type -Outbound Request- PNS to PAC \n");
        PptpHdrInfo.u2InPort = OSIX_NTOHS (u2OldCallId);
        PptpHdrInfo.u2OutPort = NAT_ZERO;
        PptpHdrInfo.u4InIpAddr = u4SrcIpAddr;
        PptpHdrInfo.u4OutIpAddr = pHeaderInfo->u4OutIpAddr;
        PptpHdrInfo.u4Direction = NAT_OUTBOUND;

        if (NatGetTransInfo (&PptpHdrInfo) == NAT_FAILURE)
        {
            /*
             * Create an entry without modifying the 
             * Inside Port (here PNS Call ID ) even if NAPT 
             * is enabled.
             */
            gu4NatPptpOutCreateFlag = TRUE;
            if (NatCreateTransInfo (&PptpHdrInfo) == NAT_FAILURE)
            {
                NAT_DBG (NAT_DBG_PPTP, "\nPPTPCntrl-Outbound-Req-PNS to PAC: \
                         Failure in Creating session entry for PNS\n");
                return NAT_FAILURE;
            }
            gu4NatPptpOutCreateFlag = FALSE;
        }

    }
    return NAT_SUCCESS;
}

/**************************************************************************
* Name           :  PptpAlgPrcsInboundCallReq
* Description    : Processes the PPTP control packet of control msg
*                  type Incoming Call Request or Outgoing Call 
*                  Request in Inbound direction (from Global Network
*                  to Local Network).
* 
*
* Input (s)   : pBuf - pointer to the packet.
*               pHeaderInfo - pointer to the structure containing information
*                             about the IP header.
*
* Output (s)  : The translated PPTP Control packets.
*
* Returns     : NAT_SUCCESS if the packet is to be forwarded.
*               NAT_FAILURE if the packet is to be dropped.
*
* ****************************************************************************/
PRIVATE INT4
PptpAlgPrcsInboundCallReq (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo
                           * pHeaderInfo)
{
    UINT4               u4DestIpAddr = NAT_ZERO;
    tHeaderInfo         PptpHdrInfo;
    UINT2               u2CallId = NAT_ZERO;
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u4DestIpAddr,
                               NAT_IP_DST_OFFSET, NAT_IP_ADDR_LEN);
    u4DestIpAddr = OSIX_NTOHL (u4DestIpAddr);
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2CallId,
                               pHeaderInfo->u1IpHeadLen +
                               (UINT4) pHeaderInfo->u1TransportHeadLen +
                               NAT_PPTP_CALLID_OFFSET, NAT_PPTP_CALLID_LEN);
    u2CallId = OSIX_NTOHS (u2CallId);

    PptpHdrInfo.u4IfNum = pHeaderInfo->u4IfNum;
    PptpHdrInfo.i4Delta = NAT_ZERO;
    PptpHdrInfo.pDynamicEntry = NULL;
    PptpHdrInfo.u1PktType = NAT_PPTP;
    if (pHeaderInfo->u2OutPort == NAT_PPTP_PORT)
    {
        /* A request from  PNS to PAC */
        NAT_DBG (NAT_DBG_PPTP,
                 "\nPPTP Cntrl Pkt Type -Inbound Request- PNS to PAC\n");
        PptpHdrInfo.u2InPort = NAT_ZERO;
        PptpHdrInfo.u2OutPort = u2CallId;
        PptpHdrInfo.u4InIpAddr = u4DestIpAddr;
        PptpHdrInfo.u4OutIpAddr = pHeaderInfo->u4OutIpAddr;
        PptpHdrInfo.u4Direction = NAT_INBOUND;

        if (NatGetTransInfo (&PptpHdrInfo) == NAT_FAILURE)
        {
            /*
             * Session info not present in the Nat Database so create 
             * an entry in the Nat Database . The session info of the 
             * PNS is to be created even if Two-Way-Nat is not enabled. 
             * Therefore the direction is taken as Outbound direction. 
             */
            PptpHdrInfo.u4Direction = NAT_OUTBOUND;
            gu4NatPptpOutCreateFlag = TRUE;
            if (NatCreateTransInfo (&PptpHdrInfo) == NAT_FAILURE)
            {
                NAT_DBG (NAT_DBG_PPTP, "\nPPTPCntrl-Inbound-Req-PNS to PAC:\
                         Failure in Creating session entry for PNS\n");
                return NAT_FAILURE;
            }
            gu4NatPptpOutCreateFlag = FALSE;
        }
    }
    else if (pHeaderInfo->u2InPort == NAT_PPTP_PORT)
    {
        /* A request from PAC to PNS */
        NAT_DBG (NAT_DBG_PPTP,
                 "\nPPTP Cntrl Pkt Type -Inbound Request- PAC to PNS\n");
        PptpHdrInfo.u2InPort = NAT_ZERO;
        PptpHdrInfo.u2OutPort = u2CallId;
        PptpHdrInfo.u4InIpAddr = u4DestIpAddr;
        PptpHdrInfo.u4OutIpAddr = pHeaderInfo->u4OutIpAddr;
        PptpHdrInfo.u4Direction = NAT_INBOUND;

        if (NatGetTransInfo (&PptpHdrInfo) == NAT_FAILURE)
        {
            if (NatCreateTransInfo (&PptpHdrInfo) == NAT_FAILURE)
            {
                NAT_DBG (NAT_DBG_PPTP, "\nPPTPCntrl-Inbound-Req-PAC to PNS:\
                         Failure in Creating session entry for PAC\n");
                return NAT_FAILURE;
            }
        }
    }
    return NAT_SUCCESS;
}

/**************************************************************************
* Name           :  PptpAlgProcessCallReply
* Description    : Processes the PPTP control packet of control msg
*                 type Incoming Call Reply or Outgoing Call Reply.
* 
*
* Input (s)   : pBuf - pointer to the packet.
*               pHeaderInfo - pointer to the structure containing information
*                             about the IP header.
*
* Output (s)  : The translated PPTP Control packets.
*
* Returns     : NAT_SUCCESS if the packet is to be forwarded.
*               NAT_FAILURE if the packet is to be dropped.
*
* ****************************************************************************/

PRIVATE INT4
PptpAlgProcessCallReply (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo
                         * pHeaderInfo)
{
    if (pHeaderInfo->u4Direction == NAT_OUTBOUND)
    {
        if (PptpAlgPrcsOutboundCallReply (pBuf, pHeaderInfo) == NAT_FAILURE)
        {
            return NAT_FAILURE;
        }
    }
    else
    {
        if (PptpAlgPrcsInboundCallReply (pBuf, pHeaderInfo) == NAT_FAILURE)
        {
            return NAT_FAILURE;
        }
    }
    return NAT_SUCCESS;
}

/**************************************************************************
* Name           :  PptpAlgPrcsOutboundCallReply
* Description    : Processes the PPTP control packet of control msg type
*                  Incoming Call Reply or Outgoing Call Reply in the 
*                  Outbound direction.
* 
*
* Input (s)   : pBuf - pointer to the packet.
*               pHeaderInfo - pointer to the structure containing information
*                             about the IP header.
*
* Output (s)  : The translated PPTP Control packets.
*
* Returns     : NAT_SUCCESS if the packet is to be forwarded.
*               NAT_FAILURE if the packet is to be dropped.
*
* ****************************************************************************/

PRIVATE INT4
PptpAlgPrcsOutboundCallReply (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo
                              * pHeaderInfo)
{
    UINT4               u4SrcIpAddr = NAT_ZERO;
    tHeaderInfo         PptpHdrInfo;
    UINT2               u2CallId = NAT_ZERO;
    UINT2               u2NewCallId = NAT_ZERO;
    UINT2               u2PeerCallId = NAT_ZERO;
    UINT2               u2TcpCksum = NAT_ZERO;
    UINT4               u4PptpOffset = NAT_ZERO;
    UINT4               u4TcpCsumOffset = NAT_ZERO;
    u4TcpCsumOffset = (UINT4) pHeaderInfo->u1IpHeadLen + NAT_TCP_CKSUM_OFFSET;
    u4PptpOffset =
        (UINT4) (pHeaderInfo->u1IpHeadLen + pHeaderInfo->u1TransportHeadLen);
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u4SrcIpAddr, NAT_IP_SRC_OFFSET,
                               NAT_IP_ADDR_LEN);
    u4SrcIpAddr = OSIX_NTOHL (u4SrcIpAddr);

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2CallId,
                               u4PptpOffset + NAT_PPTP_CALLID_OFFSET,
                               NAT_PPTP_CALLID_LEN);
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2PeerCallId,
                               u4PptpOffset + NAT_PPTP_PEERCALLID_OFFSET,
                               NAT_PPTP_CALLID_LEN);

    PptpHdrInfo.u4IfNum = pHeaderInfo->u4IfNum;
    PptpHdrInfo.i4Delta = NAT_ZERO;
    PptpHdrInfo.pDynamicEntry = NULL;
    PptpHdrInfo.u1PktType = NAT_PPTP;
    if (pHeaderInfo->u2OutPort == NAT_PPTP_PORT)
    {
        /* A reply from a PAC to PNS 
         * So the Call Id field is the PAC Call Id
         *     the PeerCall ID  is the PNS Call ID
         *  Check the session entry for the PNS Call Id . If the Entry is not
         *  present then drop the pkt.
         *  else get/create an entry for the PAC Call Id.
         *
         */
        /* Getting the session info for the PNS */
        NAT_DBG (NAT_DBG_PPTP,
                 "\nPPTP Cntrl Pkt Type -Outbound Reply- PAC to PNS \n");

        PptpHdrInfo.u2InPort = NAT_ZERO;
        PptpHdrInfo.u2OutPort = OSIX_NTOHS (u2PeerCallId);
        PptpHdrInfo.u4InIpAddr = u4SrcIpAddr;
        PptpHdrInfo.u4OutIpAddr = pHeaderInfo->u4OutIpAddr;
        PptpHdrInfo.u4Direction = NAT_OUTBOUND;
        if (NatGetTransInfo (&PptpHdrInfo) == NAT_FAILURE)
        {
            NAT_DBG (NAT_DBG_PPTP, "\nPPTPCntrl-Outbound-Reply-PAC to PNS: \
                     Failure in getting session entry for PNS\n");
            return NAT_FAILURE;
        }

        /* Getting the session info for the PAC */
        PptpHdrInfo.u2InPort = OSIX_NTOHS (u2CallId);
        PptpHdrInfo.u2OutPort = NAT_ZERO;
        if (NatGetTransInfo (&PptpHdrInfo) == NAT_FAILURE)
        {
            if (NatCreateTransInfo (&PptpHdrInfo) == NAT_FAILURE)
            {
                NAT_DBG (NAT_DBG_PPTP, "\nPPTPCntrl-Outbound-Reply-PAC to PNS:\
                         Failure in creating session entry for PAC\n");
                return NAT_FAILURE;
            }
            u2NewCallId = OSIX_HTONS (PptpHdrInfo.u2InPort);
            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2NewCallId,
                                       u4PptpOffset + NAT_PPTP_CALLID_OFFSET,
                                       NAT_PPTP_CALLID_LEN);
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2TcpCksum,
                                       u4TcpCsumOffset, NAT_WORD_LEN);
            NatChecksumAdjust ((UINT1 *) &u2TcpCksum, (UINT1 *) &u2CallId,
                               NAT_PORT_LEN, (UINT1 *) &u2NewCallId,
                               NAT_PORT_LEN);
            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2TcpCksum,
                                       u4TcpCsumOffset, NAT_WORD_LEN);
        }
    }
    else if (pHeaderInfo->u2InPort == NAT_PPTP_PORT)
    {
        /*from PNS to PAC - Outbound */
        /*
         * CallId - it is the PNS Call ID
         * PeerCall Id - it is the PAC call Id
         *  Get the PAC Call Id. If it is not present then return  NAT_FAILURE.
         */
        NAT_DBG (NAT_DBG_PPTP,
                 "\nPPTP Cntrl Pkt Type -Outbound Reply- PNS to PAC \n");
        PptpHdrInfo.u2InPort = NAT_ZERO;
        PptpHdrInfo.u2OutPort = OSIX_NTOHS (u2PeerCallId);
        PptpHdrInfo.u4InIpAddr = u4SrcIpAddr;
        PptpHdrInfo.u4OutIpAddr = pHeaderInfo->u4OutIpAddr;
        PptpHdrInfo.u4Direction = NAT_OUTBOUND;

        if (NatGetTransInfo (&PptpHdrInfo) == NAT_FAILURE)
        {
            NAT_DBG (NAT_DBG_PPTP, "\nPPTPCntrl-Outbound-Reply-PNS to PAC:\
                     Failure in getting session entry for PAC\n");
            return NAT_FAILURE;
        }
        /* Get/Create entry for PNS Call Id */
        PptpHdrInfo.u4InIpAddr = u4SrcIpAddr;
        PptpHdrInfo.u2InPort = OSIX_NTOHS (u2CallId);
        PptpHdrInfo.u2OutPort = NAT_ZERO;
        if (NatGetTransInfo (&PptpHdrInfo) == NAT_FAILURE)
        {
            gu4NatPptpOutCreateFlag = TRUE;
            if (NatCreateTransInfo (&PptpHdrInfo) == NAT_FAILURE)
            {
                NAT_DBG (NAT_DBG_PPTP, "\nPPTPCntrl-Outbound-Reply-PNS to PAC:\
                         Failure in creating session entry for PNS\n");
                return NAT_FAILURE;
            }
            gu4NatPptpOutCreateFlag = FALSE;
        }

    }
    return NAT_SUCCESS;
}

/**************************************************************************
* Name           :  PptpAlgPrcsInboundCallReply
* Description    : Processes the PPTP control packet of control 
*                   msg type*                 
*                   Incoming Call Reply or Outgoing Call Reply in  
*                  Inbound direction.
* 
*
* Input (s)   : pBuf - pointer to the packet.
*               pHeaderInfo - pointer to the structure containing information
*                             about the IP header.
*
* Output (s)  : The translated PPTP Control packets.
*
* Returns     : NAT_SUCCESS if the packet is to be forwarded.
*               NAT_FAILURE if the packet is to be dropped.
*
*****************************************************************************/

PRIVATE INT4
PptpAlgPrcsInboundCallReply (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo
                             * pHeaderInfo)
{
    tHeaderInfo         PptpHdrInfo;
    UINT4               u4DestIpAddr = NAT_ZERO;
    UINT2               u2CallId = NAT_ZERO;
    UINT2               u2OldPeerCallId = NAT_ZERO;
    UINT2               u2NewPeerCallId = NAT_ZERO;
    UINT2               u2TcpCksum = NAT_ZERO;
    UINT4               u4TcpCsumOffset = NAT_ZERO;
    UINT4               u4PptpOffset = NAT_ZERO;
    u4TcpCsumOffset = (UINT4) pHeaderInfo->u1IpHeadLen + NAT_TCP_CKSUM_OFFSET;
    u4PptpOffset =
        (UINT4) (pHeaderInfo->u1IpHeadLen + pHeaderInfo->u1TransportHeadLen);
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u4DestIpAddr, NAT_IP_DST_OFFSET,
                               NAT_IP_ADDR_LEN);
    u4DestIpAddr = OSIX_NTOHL (u4DestIpAddr);

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2CallId,
                               u4PptpOffset + NAT_PPTP_CALLID_OFFSET,
                               NAT_PPTP_CALLID_LEN);
    u2CallId = OSIX_NTOHS (u2CallId);
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2OldPeerCallId,
                               u4PptpOffset + NAT_PPTP_PEERCALLID_OFFSET,
                               NAT_PPTP_CALLID_LEN);

    PptpHdrInfo.u4IfNum = pHeaderInfo->u4IfNum;
    PptpHdrInfo.i4Delta = NAT_ZERO;
    PptpHdrInfo.pDynamicEntry = NULL;
    PptpHdrInfo.u1PktType = NAT_PPTP;
    if (pHeaderInfo->u2OutPort == NAT_PPTP_PORT)
    {
        /* A reply from a PNS to PAC
         * So the Call Id field is the PNS Call Id
         *     the PeerCall ID  is the PAC Call ID
         *  Check the session entry for the PAC Call Id . If the Entry is not
         *  present then drop the pkt.
         *  else get/create an entry for the PNS Call Id.
         *
         */
        NAT_DBG (NAT_DBG_PPTP,
                 "\nPPTP Cntrl Pkt Type -Inbound Reply- PNS to PAC \n");
        /* Getting the session info for the PAC */
        PptpHdrInfo.u2InPort = OSIX_NTOHS (u2OldPeerCallId);
        PptpHdrInfo.u2OutPort = NAT_ZERO;
        PptpHdrInfo.u4InIpAddr = u4DestIpAddr;
        PptpHdrInfo.u4OutIpAddr = pHeaderInfo->u4OutIpAddr;
        PptpHdrInfo.u4Direction = NAT_INBOUND;
        if (NatGetTransInfo (&PptpHdrInfo) == NAT_FAILURE)
        {
            NAT_DBG (NAT_DBG_PPTP, "\nPPTPCntrl-Inbound-Reply-PNS to PAC:\
                     Failure in getting session entry for PAC\n");
            return NAT_FAILURE;
        }
        u2NewPeerCallId = OSIX_HTONS (PptpHdrInfo.u2InPort);

        /* Getting the session info for the PNS */
        PptpHdrInfo.u2InPort = NAT_ZERO;
        PptpHdrInfo.u2OutPort = u2CallId;

        if (NatGetTransInfo (&PptpHdrInfo) == NAT_FAILURE)
        {

            /* Session info not present in the Nat Database so create an
             * entry in the Nat Database . The session info of the PNS is to 
             * be created even if Two-Way-Nat is not enabled. Therefore the
             * direction is taken as Outbound direction.  */

            PptpHdrInfo.u4Direction = NAT_OUTBOUND;
            gu4NatPptpOutCreateFlag = TRUE;
            if (NatCreateTransInfo (&PptpHdrInfo) == NAT_FAILURE)
            {
                NAT_DBG (NAT_DBG_PPTP, "\nPPTPCntrl-Inbound-Reply-PNS to PAC:\
                         Failure in creating session entry for PNS\n");
                return NAT_FAILURE;
            }
            gu4NatPptpOutCreateFlag = FALSE;
        }
        /* Copy the modified PAC Call Id to the packet and 
         *  adjust the TCP Checksum */
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2NewPeerCallId,
                                   u4PptpOffset + NAT_PPTP_PEERCALLID_OFFSET,
                                   NAT_PPTP_CALLID_LEN);
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2TcpCksum, u4TcpCsumOffset,
                                   NAT_WORD_LEN);
        NatChecksumAdjust ((UINT1 *) &u2TcpCksum, (UINT1 *) &u2OldPeerCallId,
                           NAT_PORT_LEN, (UINT1 *) &u2NewPeerCallId,
                           NAT_PORT_LEN);
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2TcpCksum, u4TcpCsumOffset,
                                   NAT_WORD_LEN);
    }

    else if (pHeaderInfo->u2InPort == NAT_PPTP_PORT)
    {
        /*from PAC to PNS - Inbound */
        /*
         * CallId - it is the PAC Call ID
         * PeerCall Id - it is the PNS call Id
         */
        NAT_DBG (NAT_DBG_PPTP,
                 "\nPPTP Cntrl Pkt Type -Inbound Reply- PAC to PNS \n");
        PptpHdrInfo.u2InPort = u2OldPeerCallId;
        PptpHdrInfo.u2OutPort = NAT_ZERO;
        PptpHdrInfo.u4InIpAddr = u4DestIpAddr;
        PptpHdrInfo.u4OutIpAddr = pHeaderInfo->u4OutIpAddr;
        PptpHdrInfo.u4Direction = NAT_INBOUND;
        if (NatGetTransInfo (&PptpHdrInfo) == NAT_FAILURE)
        {
            NAT_DBG (NAT_DBG_PPTP, "\nPPTPCntrl-Inbound-Reply-PAC to PNS:\
                     Failure in getting session entry for PNS\n");
            return NAT_FAILURE;
        }
        /* PNS Call ID (PeerCall Id will not change ) */
        /* PAC Call Id */
        PptpHdrInfo.u2InPort = NAT_ZERO;
        PptpHdrInfo.u2OutPort = u2CallId;
        PptpHdrInfo.u4InIpAddr = u4DestIpAddr;

        if (NatGetTransInfo (&PptpHdrInfo) == NAT_FAILURE)
        {
            if (NatCreateTransInfo (&PptpHdrInfo) == NAT_FAILURE)
            {
                NAT_DBG (NAT_DBG_PPTP, "\nPPTPCntrl-Inbound-Reply-PAC to PNS:\
                         Failure in creating session entry for PAC\n");
                return NAT_FAILURE;
            }
        }

    }
    return NAT_SUCCESS;
}

/**************************************************************************
* Name           :  PptpAlgProcessCallDiscNotify
* Description    : Processes the PPTP control packet of control msg type 
*                  Call Disconnect Notify   
* Input (s)   : pBuf - pointer to the packet.
*               pHeaderInfo - pointer to the structure containing information
*                             about the IP header.
* Output (s)  : The translated PPTP Control packet.
*
* Returns     : NAT_SUCCESS if the packet is to be forwarded.
*               NAT_FAILURE if the packet is to be dropped.
*
* ****************************************************************************/

PRIVATE INT4
PptpAlgProcessCallDiscNotify (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo
                              * pHeaderInfo)
{
    /*
     * This function is called only when the pkt is Outbound and the src of this
     * packet is a PAC 
     */
    UINT4               u4SrcIpAddr = NAT_ZERO;
    tHeaderInfo         PptpHdrInfo;
    UINT2               u2OldCallId = NAT_ZERO;
    UINT2               u2NewCallId = NAT_ZERO;
    UINT4               u4TcpCsumOffset = NAT_ZERO;
    UINT2               u2TcpCksum = NAT_ZERO;
    if ((pHeaderInfo->u4Direction != NAT_OUTBOUND) || (pHeaderInfo->u2OutPort !=
                                                       NAT_PPTP_PORT))
    {
        return NAT_SUCCESS;
    }
    /* Get the PAC Call ID from the Nat Database and copy 
     * it in the PPTP packet.
     */

    u4TcpCsumOffset = (UINT4) pHeaderInfo->u1IpHeadLen + NAT_TCP_CKSUM_OFFSET;
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u4SrcIpAddr,
                               NAT_IP_SRC_OFFSET, NAT_IP_ADDR_LEN);
    PptpHdrInfo.u4InIpAddr = OSIX_NTOHL (u4SrcIpAddr);

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2OldCallId,
                               pHeaderInfo->u1IpHeadLen +
                               (UINT4) pHeaderInfo->u1TransportHeadLen +
                               NAT_PPTP_CALLID_OFFSET, NAT_PPTP_CALLID_LEN);
    PptpHdrInfo.u2InPort = OSIX_NTOHS (u2OldCallId);
    PptpHdrInfo.u4OutIpAddr = pHeaderInfo->u4OutIpAddr;
    PptpHdrInfo.u2OutPort = NAT_ZERO;
    PptpHdrInfo.u4Direction = NAT_OUTBOUND;
    PptpHdrInfo.u4IfNum = pHeaderInfo->u4IfNum;
    PptpHdrInfo.u1PktType = pHeaderInfo->u1PktType;
    PptpHdrInfo.pDynamicEntry = NULL;

    if (NatGetTransInfo (&PptpHdrInfo) == NAT_FAILURE)
    {
        NAT_DBG (NAT_DBG_PPTP,
                 "\n PPTP Cntrl Pkt-CallDiscNotify: Failure in getting\
                 session entry for PAC \n");
        return NAT_FAILURE;
    }
    /* Copy the new PAC Call Id to the Packet and adjust the
     * PAC Call Id.  */
    u2NewCallId = OSIX_HTONS (PptpHdrInfo.u2InPort);
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2NewCallId,
                               pHeaderInfo->u1IpHeadLen +
                               (UINT4) pHeaderInfo->u1TransportHeadLen +
                               NAT_PPTP_CALLID_OFFSET, NAT_PPTP_CALLID_LEN);
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2TcpCksum, u4TcpCsumOffset,
                               NAT_WORD_LEN);
    NatChecksumAdjust ((UINT1 *) &u2TcpCksum, (UINT1 *) &u2OldCallId,
                       NAT_PORT_LEN, (UINT1 *) &u2NewCallId, NAT_PORT_LEN);
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2TcpCksum, u4TcpCsumOffset,
                               NAT_WORD_LEN);
    PptpHdrInfo.u2InPort = OSIX_NTOHS (u2OldCallId);
    /* NatDeleteTransInfo(PptpHdrInfo); */
    return NAT_SUCCESS;
}

/**************************************************************************
* Name           :  PptpAlgProcessSetLinkInfo
* Description    : Processes the PPTP control packet of control msg type 
*                  Set-Link-Info 
* Input (s)   : pBuf - pointer to the packet.
*               pHeaderInfo - pointer to the structure containing information
*                             about the IP header.
* Output (s)  : The translated PPTP Control packet.
*
* Returns     : NAT_SUCCESS if the packet is to be forwarded.
*               NAT_FAILURE if the packet is to be dropped.
*
* ****************************************************************************/

PRIVATE INT4
PptpAlgProcessSetLinkInfo (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo
                           * pHeaderInfo)
{
    UINT2               u2OldCallId = NAT_ZERO;
    UINT2               u2NewCallId = NAT_ZERO;
    UINT2               u2TcpCksum = NAT_ZERO;
    UINT4               u4TcpCsumOffset = NAT_ZERO;
    tHeaderInfo         PptpHdrInfo;
    UINT4               u4DestIpAddr = NAT_ZERO;
    if ((pHeaderInfo->u4Direction != NAT_INBOUND) || (pHeaderInfo->u2OutPort !=
                                                      NAT_PPTP_PORT))
    {
        return NAT_SUCCESS;
    }

    u4TcpCsumOffset = (UINT4) pHeaderInfo->u1IpHeadLen + NAT_TCP_CKSUM_OFFSET;
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u4DestIpAddr,
                               NAT_IP_DST_OFFSET, NAT_IP_ADDR_LEN);
    PptpHdrInfo.u4InIpAddr = OSIX_NTOHL (u4DestIpAddr);

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2OldCallId,
                               pHeaderInfo->u1IpHeadLen +
                               (UINT4) pHeaderInfo->u1TransportHeadLen +
                               NAT_PPTP_CALLID_OFFSET, NAT_PPTP_CALLID_LEN);
    /* u2Call ID is the Peer's (PAC) Call ID in the pkt */
    PptpHdrInfo.u2InPort = OSIX_NTOHS (u2OldCallId);
    PptpHdrInfo.u4OutIpAddr = pHeaderInfo->u4OutIpAddr;
    PptpHdrInfo.u2OutPort = NAT_ZERO;
    PptpHdrInfo.u4Direction = NAT_INBOUND;
    PptpHdrInfo.u4IfNum = pHeaderInfo->u4IfNum;
    PptpHdrInfo.u1PktType = pHeaderInfo->u1PktType;
    PptpHdrInfo.pDynamicEntry = NULL;

    if (NatGetTransInfo (&PptpHdrInfo) == NAT_FAILURE)
    {
        NAT_DBG (NAT_DBG_PPTP,
                 "\n PPTP Cntrl Pkt-SetLinkInfo: Failure in getting\
                 session entry for PAC \n");
        return NAT_FAILURE;
    }
    u2NewCallId = OSIX_HTONS (PptpHdrInfo.u2InPort);
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2NewCallId,
                               pHeaderInfo->u1IpHeadLen +
                               (UINT4) pHeaderInfo->u1TransportHeadLen +
                               NAT_PPTP_CALLID_OFFSET, NAT_PPTP_CALLID_LEN);
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2TcpCksum, u4TcpCsumOffset,
                               NAT_WORD_LEN);
    NatChecksumAdjust ((UINT1 *) &u2TcpCksum, (UINT1 *) &u2OldCallId,
                       NAT_PORT_LEN, (UINT1 *) &u2NewCallId, NAT_PORT_LEN);
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2TcpCksum, u4TcpCsumOffset,
                               NAT_WORD_LEN);
    return NAT_SUCCESS;
}

/**************************************************************************
* Name           :  PptpAlgPrcsOutboundDataPkt
* Description    : Processes the PPTP data (GRE) packets which move in OutBound
*                  direction 
* 
*
* Input (s)   : pBuf - pointer to the packet.
*               pHeaderInfo - pointer to the structure containing information
*                             about the IP header.
*
* Output (s)  : The translated PPTP Data packets.
*
* Returns     : NAT_SUCCESS if the packet is to be forwarded.
*               NAT_FAILURE if the packet is to be dropped.
*
* ****************************************************************************/

PRIVATE INT4
PptpAlgPrcsOutboundDataPkt (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo
                            * pHeaderInfo, UINT2 u2CallId)
{
    /* 
     * The Outbound PPTP data packets contain the Call Id of the
     * Peer on the Global Network. So the Call Id need not be translated .
     * But the session info is fetched to get the Translated IP address.
     */
    UINT4               u4SrcIpAddr = NAT_ZERO;
    tHeaderInfo         PptpHdrInfo;
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u4SrcIpAddr,
                               NAT_IP_SRC_OFFSET, NAT_IP_ADDR_LEN);
    PptpHdrInfo.u4InIpAddr = OSIX_NTOHL (u4SrcIpAddr);
    PptpHdrInfo.u2OutPort = u2CallId;

    PptpHdrInfo.u4OutIpAddr = pHeaderInfo->u4OutIpAddr;
    PptpHdrInfo.u2InPort = NAT_ZERO;

    PptpHdrInfo.u4IfNum = pHeaderInfo->u4IfNum;
    PptpHdrInfo.u4Direction = pHeaderInfo->u4Direction;
    PptpHdrInfo.u1PktType = pHeaderInfo->u1PktType;
    PptpHdrInfo.pDynamicEntry = NULL;
    if (NatGetTransInfo (&PptpHdrInfo) == NAT_FAILURE)
    {
        NAT_DBG (NAT_DBG_PPTP, "\n PPTP Data Pkt-Outbound: Failure in getting\
                 session entry \n");
        return NAT_FAILURE;
    }
    /* The IP address is sent to the parent module */
    pHeaderInfo->u4InIpAddr = PptpHdrInfo.u4InIpAddr;
    return NAT_SUCCESS;
}

/**************************************************************************
* Name           :  PptpAlgPrcsInboundDataPkt
* Description    : Processes the PPTP data packets which move in InBound
*                  direction 
* 
*
* Input (s)   : pBuf - pointer to the packet.
*               pHeaderInfo - pointer to the structure containing information
*                             about the IP header.
*
* Output (s)  : The translated PPTP Data packets.
*
* Returns     : NAT_SUCCESS if the packet is to be forwarded.
*               NAT_FAILURE if the packet is to be dropped.
*
* ****************************************************************************/

PRIVATE INT4
PptpAlgPrcsInboundDataPkt (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo
                           * pHeaderInfo, UINT2 u2CallId)
{

    /* Get the Session info and modify the Call ID (PAC or PNS 
     * Call ID -unknown). */
    UINT4               u4DestIpAddr = NAT_ZERO;
    tHeaderInfo         PptpHdrInfo;

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u4DestIpAddr,
                               NAT_IP_DST_OFFSET, NAT_IP_ADDR_LEN);
    MEMSET (&PptpHdrInfo, NAT_ZERO, sizeof (tHeaderInfo));
    PptpHdrInfo.u4InIpAddr = OSIX_NTOHL (u4DestIpAddr);

    PptpHdrInfo.u4OutIpAddr = pHeaderInfo->u4OutIpAddr;
    PptpHdrInfo.u2InPort = u2CallId;
    PptpHdrInfo.u2OutPort = NAT_ZERO;
    PptpHdrInfo.u4IfNum = pHeaderInfo->u4IfNum;
    PptpHdrInfo.u4Direction = pHeaderInfo->u4Direction;
    PptpHdrInfo.u1PktType = pHeaderInfo->u1PktType;
    PptpHdrInfo.pDynamicEntry = NULL;

    if (NatGetTransInfo (&PptpHdrInfo) == NAT_FAILURE)
    {
        NAT_DBG (NAT_DBG_PPTP, "\n PPTP Data Pkt-Outbound: Failure in getting\
                 session entry \n");
        return NAT_FAILURE;
    }
    pHeaderInfo->u4InIpAddr = PptpHdrInfo.u4InIpAddr;
    pHeaderInfo->pDynamicEntry = PptpHdrInfo.pDynamicEntry;
    u2CallId = OSIX_HTONS (PptpHdrInfo.u2InPort);
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2CallId,
                               (UINT4) pHeaderInfo->u1IpHeadLen +
                               NAT_GRE_CALLID_OFFSET, NAT_PPTP_CALLID_LEN);
    return NAT_SUCCESS;
}

/**************************************************************************
* Name           :  NatGetTransInfo
* Description    : Gets the PPTP session translation info from the NAT 
*                  database
*
* Input (s)   : pHeaderInfo - pointer to the structure containing information
*                             about the IP header.
*
* Output (s)  : The session translation info of the given PPTP session
*
* Returns     : NAT_SUCCESS if the session info is present
*               NAT_FAILURE if the session info is not present
*
* ****************************************************************************/
PRIVATE INT4
NatGetTransInfo (tHeaderInfo * pHeaderInfo)
{
    INT4                i4Status = NAT_ZERO;
    /* Only get the info, don't create it */
    i4Status = (INT4) NatGetDynamicEntry (pHeaderInfo, NAT_SEARCH);
    return (i4Status);
}

/**************************************************************************
* Name           :  NatCreateTransInfo
* Description    : Creates the PPTP session translation info  in the NAT 
*                  database
*
* Input (s)   : pHeaderInfo - pointer to the structure containing information
*                             about the IP header.
*
* Output (s)  : The session translation info  is created
*
* Returns     : NAT_SUCCESS if the session info is present
*               NAT_FAILURE if the session info is not present
*
* ****************************************************************************/
PRIVATE INT4
NatCreateTransInfo (tHeaderInfo * pHeaderInfo)
{
    return ((INT4) NatGetDynamicEntry (pHeaderInfo, NAT_CREATE));
}

/****************************************************************************/
/*                 End of the file -- natpptp.c                             */
/****************************************************************************/
