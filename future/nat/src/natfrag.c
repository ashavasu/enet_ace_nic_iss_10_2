/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: natfrag.c,v 1.13 2014/02/14 13:57:30 siva Exp $
 *
 * Description:Consists of  fragment reassembly functions   
 *             Not needed when running as a standalone      
 *             gateway.                                     
 *
 *******************************************************************/
#ifndef _NATFRAG_C
#define _NATFRAG_C
#include "natinc.h"
#include "natfrag.h"
#include "natmemac.h"
#include "natfragext.h"
#include "natdebug.h"
#include "ip.h"

/*************   P R O T O T Y P E    D E C L A R A T I O N S  ************/
PRIVATE tNatFragmentStream *NatFindFragStream
PROTO ((tNatFragmentStream * pNatFragmentStreamKey));
PRIVATE tNatFragmentStream *NatCreatFragStream
PROTO ((tNatFragmentStream * pNatFragmentStreamKey));
PRIVATE tNatIpFrag *NatNewFrag
PROTO ((UINT2 u2StartOffset, UINT2 u2EndOffset, tCRU_BUF_CHAIN_HEADER * pBuf));
PRIVATE VOID NatFreeFrag PROTO ((tNatIpFrag * pFrag));
PRIVATE INT4        NatFragTblCmpFunc (tNatFragRBElem * pRBElem1,
                                       tNatFragRBElem * pRBElem2);
PRIVATE UINT4       NatFragTmrListInit (VOID);
PRIVATE UINT4       NatMakeFragKey (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Dir,
                                    tNatFragmentStream * pNatFragmentStream);
PRIVATE VOID        NatFreeReasm (tNatFragmentStream * pReasm);
PRIVATE VOID        NatFragHandleReasmTimerExpiry (VOID *pArg);

PRIVATE tCRU_BUF_CHAIN_HEADER *NatMakeDupBufChain (tCRU_BUF_CHAIN_HEADER *
                                                   pOrigBuf);
PRIVATE UINT4
      NatAddFragToLinkList (tTMO_DLL * pFragList, tCRU_BUF_CHAIN_HEADER * pBuf);

/*****************external function declaration********/
PUBLIC UINT2        IpCalcCheckSum (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Size,
                                    UINT4 u4Offset);
PUBLIC VOID         ip_put_hdr (tIP_BUF_CHAIN_HEADER * pBuf, t_IP * pIp,
                                INT1 i1flag);
PUBLIC INT1         nmhGetFsIpPathMtu (UINT4 u4FsIpPmtuDestination,
                                       INT4 i4FsIpPmtuTos,
                                       INT4 *pi4RetValFsIpPathMtu);
PUBLIC UINT4        IpifGetIfMTU (UINT2 u2Port);
PUBLIC VOID         ip_construct_hdr_for_frags (t_IP * pIp_org,
                                                t_IP * pIp_frag);

/**************  V A R I A B L E   D E C L A R A T I O N S  ***************/

PRIVATE tNatFragRBTree pReasmRBTree;    /* Reassembly TREE */

/*****************************************************************************/
/* Function     : NatTmrInitTimerDesc                                        */
/*                                                                           */
/* Description  : Initializes the timer descriptor structure.                */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/

PUBLIC VOID
NatTmrInitTimerDesc (VOID)
{

    gaNatTimerDesc[NAT_FRG_REASM_TIMER].TmrExpFn =
        NatFragHandleReasmTimerExpiry;
    gaNatTimerDesc[NAT_FRG_REASM_TIMER].i2Offset =
        (INT2) (NAT_TMR_FUNC_OFFSET (tNatFragmentStream, FrgReasmTimer));

}

/*****************************************************************************/
/* Function     : NatTmrSetTimer                                             */
/*                                                                           */
/* Description  : Sets the timer id in the pTimer struct. Sets no_ticks in   */
/*                pTimer->appl timer. starts the timer.                      */
/*                                                                           */
/* Input        : pTimer       : pointer to timer structure                  */
/*                u1TimerId   : the id ofthe timer to be started             */
/*                i4NrTicks   : the value to which the timer is to be set    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : NAT_SUCCESS                                                */
/*                NAT_FAILURE                                                */
/*****************************************************************************/
PUBLIC UINT4
NatTmrSetTimer (tNatTimer * pTimer, UINT1 u1TimerId, INT4 i4NrTicks)
{
    pTimer->u1TimerId = u1TimerId;
    if (TmrStartTimer (gNatTimerListId, &(pTimer->timerNode),
                       (UINT4) i4NrTicks) != TMR_SUCCESS)
    {
        NAT_TRC (OS_RESOURCE_TRC, "Tmr Link Failure\n");
        return ((INT4) NAT_FAILURE);
    }
    return NAT_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : NatFragInit
 *
 * Input(s)           : None. Maximum no of fragment streams
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Action :
 *
 * Initialization routine for NAT fragment handling. Creates
 * the reassembly Tree.
 *
+-------------------------------------------------------------------*/

UINT4
NatFragInit ()
{
    UINT4               u4Status = NAT_FAILURE;

    NAT_FRAG_RB_TREE = RBTreeCreate (NAT_MAX_NO_OF_FRAGMENT_STREAM,
                                     NatFragTblCmpFunc);
    if (NAT_FRAG_RB_TREE == NULL)
    {
        NAT_TRC (NAT_TRC_ON, "\n Creation of NAT_FRAG_RB_TREE failed.");
        MOD_TRC (gu4NatTrc, ALL_FAILURE_TRC, "NAT",
                 "\n Fragmentation RB Tree failed   \n");
        return NAT_FAILURE;
    }
    NAT_TRC (NAT_TRC_ON, "\n Creation of NAT_FRAG_RB_TREE was success.");

    NatTmrInitTimerDesc ();

    u4Status = NatFragTmrListInit ();
    return (u4Status);
}

/***************************************************************/

/*  Function Name   : NatFragTmrListInit                       */

/*  Description     : Creates the timer list                   */

/*  Input(s)        : none                                     */

/*  Output(s)       : none                                     */

/*  <OPTIONAL Fields>:                                         */

/*  Global Variables Referred : gNatTimerListId                */

/*  Global variables Modified : none                           */

/*  Returns         : NAT_SUCCESS or NAT_FAILURE               */

/***************************************************************/

PRIVATE UINT4
NatFragTmrListInit (VOID)
{
    UINT4               u4Status = NAT_ZERO;
    u4Status = TmrCreateTimerList ((const UINT1 *) CFA_TASK_NAME,
                                   NAT_FRAG_TIMER_EXPIRY_EVENT, NULL,
                                   &gNatTimerListId);
    if (u4Status == TMR_SUCCESS)
    {
        u4Status = NAT_SUCCESS;
        MOD_TRC (gu4NatTrc, OS_RESOURCE_TRC, "NAT",
                 "\n Timer for NAT Success \n");

    }
    else
    {
        u4Status = NAT_FAILURE;
        MOD_TRC (gu4NatTrc, OS_RESOURCE_TRC, "NAT",
                 "\n Timer for NAT Fail.... \n");
    }
    return (u4Status);

}

/***************************************************************/

/*  Function Name   : NatFragDeInit                            */

/*  Description     : De-allocates all the resources used      */

/*  Input(s)        : none                                     */

/*  Output(s)       : none                                     */

/*  <OPTIONAL Fields>:                                         */

/*  Global Variables Referred : gNatTimerListId, gNatFragList  */
/*                       NAT_FRAG_RB_TREE                      */

/*  Global variables Modified : none                           */

/*  Returns         : NAT_SUCCESS or NAT_FAILURE               */

/***************************************************************/

INT4
NatFragDeInit ()
{

    tNatFragmentStream  NatFragStream;
    tNatFragmentStream *pReasm = NULL;

    MEMSET (&NatFragStream, NAT_ZERO, sizeof (tNatFragmentStream));
    pReasm = &NatFragStream;

    while ((pReasm = RBTreeGetNext
            (NAT_FRAG_RB_TREE, (tNatFragRBElem *) pReasm, NULL)) != NULL)
    {
        NatFreeReasm (pReasm);
    }
    RBTreeDelete (NAT_FRAG_RB_TREE);

    if (TmrDeleteTimerList (gNatTimerListId) != TMR_SUCCESS)
    {
        MOD_TRC (gu4NatTrc, INIT_SHUT_TRC, "NAT",
                 "\n Unable to delete the Frag timer list\n");
    }

    if (NatFreeOutFragList (&gNatFragList) != NAT_SUCCESS)
    {
        MOD_TRC (gu4NatTrc, INIT_SHUT_TRC, "NAT",
                 "\n Unable to free the buffers of the DLL, FragList to be sent "
                 "out\n");
    }

    return (SUCCESS);
}

/****************************************************************************/
/* Function Name : NatFragTblCmpFunc                                        */
/* Description   : user compare function for NAT fragment reassembly RBTree */
/* Input(s)      : Two RBTree Nodes be compared                             */
/* Output(s)     : None                                                     */
/* Returns       : 1/(-1)/0                                                 */
/****************************************************************************/
INT4
NatFragTblCmpFunc (tNatFragRBElem * pRBElem1, tNatFragRBElem * pRBElem2)
{
    tNatFragmentStream *pNatFragmentStream1 = NULL;
    tNatFragmentStream *pNatFragmentStream2 = NULL;

    pNatFragmentStream1 = (tNatFragmentStream *) pRBElem1;
    pNatFragmentStream2 = (tNatFragmentStream *) pRBElem2;

    NAT_TRC5 (NAT_TRC_ON,
              "\n Inside NatFragTblCmpFunc:  direction  %d protocol  %d, IP ID "
              " = %d,  src IP = %u dst IP =%u",
              pNatFragmentStream1->u1Direction, pNatFragmentStream1->u1Proto,
              pNatFragmentStream1->u2Id, pNatFragmentStream1->u4Src,
              pNatFragmentStream1->u4Dest);
    if (pNatFragmentStream1->u1Direction < pNatFragmentStream2->u1Direction)
    {
        /* return -1; */
        return NAT_RB_LESSER;
    }
    if (pNatFragmentStream1->u1Direction > pNatFragmentStream2->u1Direction)
    {
        /* return 1; */
        return NAT_RB_GREATER;
    }

    if (pNatFragmentStream1->u1Proto < pNatFragmentStream2->u1Proto)
    {
        return NAT_RB_LESSER;
    }
    if (pNatFragmentStream1->u1Proto > pNatFragmentStream2->u1Proto)
    {
        return NAT_RB_GREATER;
    }

    if (pNatFragmentStream1->u2Id < pNatFragmentStream2->u2Id)
    {
        return NAT_RB_LESSER;
    }
    if (pNatFragmentStream1->u2Id > pNatFragmentStream2->u2Id)
    {
        return NAT_RB_GREATER;
    }

    if (pNatFragmentStream1->u4Src < pNatFragmentStream2->u4Src)
    {
        return NAT_RB_LESSER;
    }

    if (pNatFragmentStream1->u4Src > pNatFragmentStream2->u4Src)
    {
        return NAT_RB_GREATER;
    }

    if (pNatFragmentStream1->u4Dest < pNatFragmentStream2->u4Dest)
    {
        return NAT_RB_LESSER;
    }
    if (pNatFragmentStream1->u4Dest > pNatFragmentStream2->u4Dest)
    {
        return NAT_RB_GREATER;
    }
    return NAT_RB_EQUAL;
}

/* -------------------------------------------------------------------+
 * Function           : NATReassemble
 *
 * Input(s)           :  pBuf, u2Port and direction
 *
 * Output(s)          : pBuf if complete datagram.
 *
 * Returns            : NULL - If buffer is fragment
 *            Buffer pointer - pointer to complete packet.
 *
 * Action :
 * Processes IP datagram fragments.
 * If incoming datagram is complete, returns the same.
 * else if it is a fragment and completes a datagram, returns pointer
 *      completed datagram
 * else put the fragment in proper place for reassembly and returns NULL.
+-------------------------------------------------------------------*/

PUBLIC tCRU_BUF_CHAIN_HEADER *
NatFragReassemble (tCRU_BUF_CHAIN_HEADER * pCRUBuf, UINT4 u4Port,
                   UINT4 u4Direction)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tNatFragmentStream *pReasm = NULL;
    tNatFragmentStream  NatFragmentStreamKey;
    tNatIpFrag         *pLastFrag = NULL;
    tNatIpFrag         *pNextFrag = NULL;
    tNatIpFrag         *pTmpFrag = NULL;

    UINT2               u2Place = NAT_VAL_256;
    UINT2               u2Offset = NAT_ZERO;
    UINT2               u2Cksum = NAT_ZERO;
    UINT2               u2TotalLen = NAT_ZERO;
    UINT2               u2FlgOffs = NAT_ZERO;
    UINT1               u1HdrLen = NAT_ZERO;
    /* 1 if not last fragment, 0 otherwise */
    INT1                i1MoreFlag = NAT_ZERO;
    /* Index of first byte in fragment  */
    UINT2               u2StartOffset = NAT_ZERO;
    /* Index of first byte beyond fragment */
    UINT2               u2EndOffset = NAT_ZERO;
    UINT4               u4BufLen = NAT_ZERO;

    u4Port = NAT_ZERO;            /* may be used in the future for logging */
    UNUSED_PARAM (u4Port);

    NAT_TRC (NAT_TRC_ON, "\n NatFragReassemble  has been Called");

    if (pCRUBuf == NULL)
    {
        NAT_TRC (NAT_TRC_ON, "CRU Buffer passed in is a NULL pointer\n");
        NAT_DBG (NAT_DBG_MEM, "CRU Buffer passed in is a NULL pointer\n");
        return NULL;
    }

    /* duplicates the incoming buffer */
    pBuf = NatMakeDupBufChain (pCRUBuf);
    if (pBuf == NULL)
    {
        NAT_TRC (NAT_TRC_ON,
                 "CRU DUP Failed, Not able to allocate Buf Chain\n");
        return NULL;
    }
    else if (pBuf == pCRUBuf)
    {
        NAT_TRC (NAT_TRC_ON, "CRU DUP allocation returns the same pointer\n");
        return NULL;
    }
    NAT_COPY_FROM_BUF (pBuf, &u1HdrLen,
                       NAT_IP_HDRLEN_AND_VER_OFFSET,
                       NAT_IP_HDRLEN_AND_VER_FIELD_LEN);

    /* Header length is in 32 bit word */
    u1HdrLen = (UINT1) ((u1HdrLen & NAT_IP_HDRLEN_MASK)
                        << NAT_IP_4_BYTE_WORD_TO_NO_OF_BYTES);

    NAT_COPY_FROM_BUF (pBuf, &u2FlgOffs,
                       NAT_FRAG_BIT_OFFSET, NAT_IP_FRAG_OFFSET_LEN);
    u2FlgOffs = CRU_NTOHS (u2FlgOffs);

    NAT_COPY_FROM_BUF (pBuf, &u2TotalLen, NAT_FRAG_TOTAL_LEN_OFFSET,
                       NAT_TWO_BYTE_LEN);
    u2TotalLen = CRU_NTOHS (u2TotalLen);

    /* Convert to bytes */
    u2StartOffset = (UINT2) (NAT_OFFSET_IN_BYTES (u2FlgOffs));
    u2EndOffset = (UINT2) (u2StartOffset + u2TotalLen - u1HdrLen);
    i1MoreFlag = (u2FlgOffs & NAT_MORE_FRAG_BIT) ? TRUE : FALSE;

    NAT_TRC6 (NAT_TRC_ON,
              "\n IP Header Len in bytes %d, Flag and offset %d, Total len = %d"
              ", strt off = %d end off =%d MF = %d", u1HdrLen, u2FlgOffs,
              u2TotalLen, u2StartOffset, u2EndOffset, i1MoreFlag);

    if ((u2StartOffset == NAT_ZERO_OFFSET) && (i1MoreFlag == FALSE))
        /* Complete pkt received */
    {
        /*Already a Complete Packet */
        CRU_BUF_Release_MsgBufChain (pCRUBuf, FALSE);
        return pBuf;
    }
    u4BufLen = NAT_GET_BUF_LENGTH (pBuf);
    if (u4BufLen > u2TotalLen)
    {
        pBuf = CRU_BUF_Delete_BufChainAtEnd (pBuf, (u4BufLen - u2TotalLen));
        if (pBuf == NULL)
        {
            return NULL;
        }
    }

    NatMakeFragKey (pBuf, u4Direction, &NatFragmentStreamKey);

    /* Remove IP header portion from all fragments except first */
    if (u2StartOffset != NAT_ZERO_OFFSET)
    {
        NAT_BUF_MOVE_VALID_OFFSET (pBuf, u1HdrLen);
    }

    if ((pReasm = NatFindFragStream (&NatFragmentStreamKey)) == NULL)
    {
        /* First fragment; create new reassembly descriptor */
        if ((pReasm = NatCreatFragStream (&NatFragmentStreamKey)) == NULL)
        {
            /* No space for descriptor, drop fragment */
            NAT_RELEASE_BUF (pBuf, FALSE);
            NAT_TRC (NAT_TRC_ON, "Removing Buffer was successful\n");
            return NULL;
        }
        NAT_TRC (NAT_TRC_ON,
                 "\n First fragment arrived : node is  created  \n");
        if (NatTmrSetTimer (&(pReasm->FrgReasmTimer), NAT_FRG_REASM_TIMER,
                            (INT4) (NAT_FRAG_REASS_TICKS)) == NAT_FAILURE)
        {
            NAT_RELEASE_BUF (pBuf, FALSE);
            NatFreeReasm (pReasm);    /* freeing the frag stream */
            NAT_TRC3 (NAT_TRC_ON,
                      "Reassem Tmr failed to start for the IP packet "
                      "with id : %d src %u dest %u\n", pReasm->u2Id,
                      pReasm->u4Src, pReasm->u4Dest);
            return NULL;
        }
        NAT_TRC (NAT_TRC_ON, "\n Reassembly Timer Started\n");

    }
    else
    {
        if (pReasm->u2NoOfFrgs == NAT_MAX_NO_OF_FRAGMENTS_PER_FRAME)
        {
            NatFreeReasm (pReasm);    /* freeing the frag stream */
            NAT_RELEASE_BUF (pBuf, FALSE);    /* freeing the new frag recvd */
            NAT_TRC (NAT_TRC_ON, "Removing Buffer was successful\n");
            NAT_TRC (NAT_TRC_ON,
                     "Exceeding max no fragments, Cleaning the resources\n");
            return NULL;
        }
        /* Keep restarting timer as long as we keep getting fragments */

        if ((pReasm->u2Len != NAT_ZERO_LEN) && (u2StartOffset > pReasm->u2Len))
        {
            /*
             *  IP has received a segment whose Offset is more than the size of
             *  the datagram. Hence discard the datagram silently.
             */
            NAT_TRC (NAT_TRC_ON,
                     "Offset greater that datagram size ???, discarding it\n");
            NAT_RELEASE_BUF (pBuf, FALSE);
            NAT_TRC (NAT_TRC_ON, "Removing Buffer was successful\n");

            return NULL;
        }
    }

    /*
     * If this is the last fragment, we now know how long the
     * entire datagram is; record it
     */

    if (i1MoreFlag == FALSE)
    {
        pReasm->u2Len = u2EndOffset;
    }

    /*
     * Set pNextFrag to the first fragment which begins after us,
     * and pLastFrag to the last fragment which begins before us.
     */
    pLastFrag = NULL;

    TMO_DLL_Scan (&(pReasm->FragList), pNextFrag, tNatIpFrag *)
    {
        if (pNextFrag->u2StartOffset > u2StartOffset)
        {
            /* Current fragment should be placed in first in the list\n") */
            break;                /* Next frag offset greater than ours */
        }
        pLastFrag = pNextFrag;
        /* Previous Frag exists */
    }

    /* Check for overlap with preceeding fragment */
    if ((pLastFrag != NULL) && (u2StartOffset < pLastFrag->u2EndOffset))
    {
        /* Strip overlap from new fragment */

        u2Offset = (UINT2) (pLastFrag->u2EndOffset - u2StartOffset);

        if (NAT_GET_BUF_LENGTH (pBuf) <= u2Offset)
        {
            NAT_RELEASE_BUF (pBuf, FALSE);
            NAT_TRC (NAT_TRC_ON, "Removing Buffer was successful\n");

            return NULL;        /* Nothing left */
        }

        NAT_BUF_MOVE_VALID_OFFSET (pBuf, u2Offset);

        u2StartOffset = (UINT2) (u2StartOffset + u2Offset);
    }

    /* Look for overlap with succeeding segments */
    for (; pNextFrag != NULL; pNextFrag = pTmpFrag)
    {

        pTmpFrag = (tNatIpFrag *) TMO_DLL_Next (&pReasm->FragList,
                                                &pNextFrag->Link);

        if (pNextFrag->u2StartOffset >= u2EndOffset)
        {
            break;                /* Past our end */
        }

        /*
         * Trim the front of this entry; if nothing is
         * left, remove it.
         */
        u2Offset = (UINT2) (u2EndOffset - pNextFrag->u2StartOffset);

        NAT_BUF_MOVE_VALID_OFFSET (pNextFrag->pBuf, u2Offset);

        if (NAT_GET_BUF_LENGTH (pNextFrag->pBuf) == NAT_ZERO)
        {
            /* superseded; delete from list */

            TMO_DLL_Delete (&pReasm->FragList, &pNextFrag->Link);
            NatFreeFrag (pNextFrag);
        }
        else
        {
            pNextFrag->u2StartOffset = u2EndOffset;
            break;
        }
    }
    /*
     * Lastfrag now points, as before, to the fragment before us;
     * pNextFrag points at the next fragment. Check to see if we can
     * join to either or both fragments.
     */
    if ((pLastFrag != NULL) && (pNextFrag != NULL))
    {
        u2Place = NAT_FRAG_INSERT;
    }

    if ((pLastFrag != NULL) && (pLastFrag->u2EndOffset == u2StartOffset))
    {
        u2Place |= NAT_FRAG_APPEND;
    }

    if ((pNextFrag != NULL) && (pNextFrag->u2StartOffset == u2EndOffset))
    {
        u2Place |= NAT_FRAG_PREPEND;
    }

    switch (u2Place)
    {
        case NAT_FRAG_INSERT:    /*Insert new desc between pLastFrag & pNextFrag */

            pTmpFrag = NatNewFrag (u2StartOffset, u2EndOffset, pBuf);
            if (pTmpFrag == NULL)
            {
                MOD_TRC (gu4NatTrc, ALL_FAILURE_TRC, "NAT",
                         "\n NatNewFrag returned failure:  \n");
                NAT_RELEASE_BUF (pBuf, FALSE);
                return NULL;
            }
            TMO_DLL_Insert (&pReasm->FragList, &pLastFrag->Link,
                            &pTmpFrag->Link);
            break;

        case NAT_FRAG_APPEND:    /* Append to pLastFrag */
            if (pLastFrag != NULL)
            {
                NAT_CONCAT_BUFS (pLastFrag->pBuf, pBuf);
                pLastFrag->u2EndOffset = u2EndOffset;    /* Extend forward */
            }
            break;

        case NAT_FRAG_PREPEND:    /* Prepend to pNextFrag */
            if (pNextFrag != NULL)
            {
                NAT_CONCAT_BUFS (pBuf, pNextFrag->pBuf);
                pNextFrag->pBuf = pBuf;
                pNextFrag->u2StartOffset = u2StartOffset;    /* Extend backward */
            }
            break;

        case (NAT_FRAG_APPEND | NAT_FRAG_PREPEND):
            /* Consolidate by appending this fragment and pNextFrag
             * to pLastFrag and removing the pNextFrag descriptor
             */
            if ((pLastFrag != NULL) && (pNextFrag != NULL))
            {
                NAT_CONCAT_BUFS (pLastFrag->pBuf, pBuf);
                NAT_CONCAT_BUFS (pLastFrag->pBuf, pNextFrag->pBuf);
                pNextFrag->pBuf = NULL;
                pLastFrag->u2EndOffset = pNextFrag->u2EndOffset;

                /* Finally unlink and delete the now unneeded pNextFrag */
                TMO_DLL_Delete (&(pReasm->FragList), &pNextFrag->Link);
                NatFreeFrag (pNextFrag);
            }
            break;
        default:
            NAT_RELEASE_BUF (pBuf, FALSE);
            return NULL;

    }
    pTmpFrag = (tNatIpFrag *) TMO_DLL_First (&(pReasm->FragList));

    pReasm->u2NoOfFrgs++;        /* Increment the no of frags recvd */

    if (pTmpFrag == NULL)
    {
        return NULL;
    }

    if ((pTmpFrag->u2StartOffset == NAT_ZERO_OFFSET) &&
        (pReasm->u2Len != NAT_ZERO_LEN) && (pTmpFrag->u2EndOffset
                                            == pReasm->u2Len))
    {
        /*
         * We've got a complete datagram, so extract it from the
         * reassembly buffer and pass it on.
         */
        pBuf = pTmpFrag->pBuf;
        pTmpFrag->pBuf = NULL;

        NAT_COPY_FROM_BUF (pBuf, &u1HdrLen, NAT_IP_HDRLEN_AND_VER_OFFSET,
                           NAT_IP_HDRLEN_AND_VER_FIELD_LEN);

        /* Header length is in 32 bit word */
        u1HdrLen = (UINT1) ((u1HdrLen & NAT_IP_HDRLEN_MASK)
                            << NAT_IP_4_BYTE_WORD_TO_NO_OF_BYTES);

        u2TotalLen = (UINT2) (pReasm->u2Len + u1HdrLen);
        u2TotalLen = CRU_HTONS (u2TotalLen);
        NAT_COPY_TO_BUF (pBuf, &u2TotalLen, NAT_FRAG_TOTAL_LEN_OFFSET,
                         NAT_TWO_BYTE_LEN);

        /*
           u2FlgOffs = i2OrgDf;     Since we have done the reassembling in NAT 
           we cant decide on the DF flag to be set to 0
           DF flag would be the same as given by the 
           IP stack 
         */
        u2FlgOffs = NAT_ZERO;

        u2FlgOffs = CRU_HTONS (u2FlgOffs);
        NAT_COPY_TO_BUF (pBuf, &u2FlgOffs, NAT_FRAG_BIT_OFFSET,
                         NAT_IP_FRAG_OFFSET_LEN);

        u2Cksum = NAT_ZERO;
        NAT_COPY_TO_BUF (pBuf, &u2Cksum, NAT_OFFSET_FOR_CHKSUM,
                         NAT_IP_HDR_CHKSUM_LEN);
#ifndef LNXIP4_WANTED
        u2Cksum = IpCalcCheckSum (pBuf, u1HdrLen, NAT_ZERO);
#endif
        u2Cksum = CRU_HTONS (u2Cksum);
        NAT_COPY_TO_BUF (pBuf, &u2Cksum, NAT_OFFSET_FOR_CHKSUM,
                         NAT_IP_HDR_CHKSUM_LEN);

        NatFreeReasm (pReasm);
        /*if (u4Port == NAT_ZERO)
           {
           return pBuf;
           } */
        return pBuf;
    }
    else
    {
        /* Frag Processing Done, still more frags */
        return NULL;
    }
}

/*-------------------------------------------------------------------+
 *
 * Function           : NatMakeFragKey
 *
 * Input(s)           : the fragment buffer and the direction
 *
 * Output(s)          : pointer to the RB Tree key
 *
 * Returns            : NAT_SUCCESS if key is made successfully;
 *                      NAT_FRAG_BIT_OFFSET if pBuf or pNatFragmentStream
 *                      is NULL
 * Action             :  Creates a reassembly RB Tree node Key
 *                       from the fragment buffer and the direction
+-------------------------------------------------------------------*/

PRIVATE UINT4
NatMakeFragKey (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Dir, tNatFragmentStream
                * pNatFragmentStream)
{
    if ((pBuf == NULL) || (pNatFragmentStream == NULL))
    {
        if (pBuf == NULL)
        {
            NAT_TRC (NAT_TRC_ON, "CRU Buffer passed in is a NULL pointer\n");
            NAT_DBG (NAT_DBG_MEM, "CRU Buffer passed in is a NULL pointer\n"
                     "in NatMakeFragKey\n");
        }
        if (pNatFragmentStream == NULL)
        {
            NAT_TRC (NAT_TRC_ON,
                     "pNatFragmentStream passed in is a NULL pointer\n");
            NAT_DBG (NAT_DBG_MEM,
                     "pNatFragmentStream passed in is a NULL pointer\n"
                     "in NatMakeFragKey\n");
        }
        return NAT_FAILURE;
    }
    pNatFragmentStream->u1Direction = (UINT1) u4Dir;
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &pNatFragmentStream->u1Proto,
                               NAT_PROT_OFFSET_IN_IPHEADER,
                               NAT_IP_PROT_FIELD_LEN);
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &pNatFragmentStream->u2Id,
                               NAT_IP_ID_OFFSET_IN_IPHEADER,
                               NAT_IP_ID_FIELD_LEN);
    pNatFragmentStream->u2Id = CRU_NTOHS (pNatFragmentStream->u2Id);
    pNatFragmentStream->u4Src = NatGetSrcIpAddr (pBuf);
    pNatFragmentStream->u4Dest = NatGetDestIpAddr (pBuf);

    return NAT_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : NatFindFragStream
 *
 * Input(s)           : Info about the fragment saved in the key
 *
 * Output(s)          : None
 *
 * Returns            : pointer to the fragment stream (i.e. RB node)- if found
 *                      NULL    - if not found.
 *
 * Action :
 * searches the reassembly RB Tree for a datagram undergoing reassembly 
 * matching the fragment stream.
 *
+-------------------------------------------------------------------*/
PRIVATE tNatFragmentStream *
NatFindFragStream (tNatFragmentStream * pNatFragmentStreamKey)
{
    tNatFragmentStream *pNatFragmentStream = NULL;

    pNatFragmentStream = RBTreeGet (NAT_FRAG_RB_TREE,
                                    (tNatFragRBElem *) pNatFragmentStreamKey);
    return pNatFragmentStream;
}

/*-------------------------------------------------------------------+
 *
 * Function           : NatCreatFragStream 
 *
 * Input(s)           : Info about the fragment saved in the key
 *
 * Output(s)          : None
 *
 * Returns            : pointer to reassembly structure if NAT_SUCCESS;
 *                      NULL if failure
 * Action :
 * Creates a reassembly descriptor, puts at head of reassembly list
 * The procedure also initializes the fragment list for this reassembly.
 *
+-------------------------------------------------------------------*/
PRIVATE tNatFragmentStream *
NatCreatFragStream (tNatFragmentStream * pReasmKey)
{
    tNatFragmentStream *pReasm = NULL;
    UINT4               u4Ret = RB_FAILURE;
    UINT4               u4Count = NAT_ZERO;

    RBTreeCount (NAT_FRAG_RB_TREE, &u4Count);

    if (u4Count == NAT_MAX_NO_OF_FRAGMENT_STREAM)
    {
        NAT_TRC (NAT_TRC_ON, "Fragment RB Tree Nodes Exhausted\n");
        return NULL;
    }

    if (NAT_FRAG_ALLOCATE_MEM_BLOCK
        (NAT_FRAG_FRAG_LIST_POOL_ID, pReasm, tNatFragmentStream) != NULL)
    {
        pReasm->u4Src = pReasmKey->u4Src;
        pReasm->u4Dest = pReasmKey->u4Dest;
        pReasm->u2Id = pReasmKey->u2Id;
        pReasm->u1Proto = pReasmKey->u1Proto;
        pReasm->u2Len = NAT_ZERO_LEN;
        pReasm->u1Direction = pReasmKey->u1Direction;
        pReasm->u2NoOfFrgs = NAT_ZERO;

        /* Initialize the fragment list for this reassembly process */
        TMO_DLL_Init (&(pReasm->FragList));
        u4Ret = RBTreeAdd (NAT_FRAG_RB_TREE, (tNatFragRBElem *) pReasm);
        if (u4Ret == RB_FAILURE)
        {
            NAT_RELEASE_MEM_BLOCK (NAT_FRAG_FRAG_LIST_POOL_ID, pReasm);
            return NULL;
        }
        NAT_TRC (NAT_TRC_ON, "\n RB Tree node allocation was successful");
        return pReasm;
    }
    else
    {
        NAT_TRC (NAT_TRC_ON, "\n RB Tree node allocation failed");
        MOD_TRC (gu4NatTrc, ALL_FAILURE_TRC, "NAT",
                 "\nRB Tree node allocation failed  \n");
        return NULL;
    }
}

/*-------------------------------------------------------------------+
 *
 * Function           : NatFreeReasm
 *
 * Input(s)           : pReasm
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action :
 * Free all resources associated with a reassembly descriptor.
 *
+-------------------------------------------------------------------*/
PRIVATE VOID
NatFreeReasm (tNatFragmentStream * pReasmKey)
{
    tNatIpFrag         *pFrag = NULL;
    tNatFragmentStream *pReasm = NULL;

    if ((pReasm = RBTreeRem (NAT_FRAG_RB_TREE, (tNatFragRBElem *) pReasmKey))
        == NULL)
    {
        NAT_TRC (NAT_TRC_ON,
                 "ALERT: ##### Removing RB Tree Node failed #####\n");
    }

    if (pReasm != NULL)
    {
        NAT_TRC (NAT_TRC_ON, "Removing RB Tree Node is successful\n");
        TmrStopTimer (gNatTimerListId, &(pReasm->FrgReasmTimer.timerNode));
        NAT_TRC (NAT_TRC_ON, "\nReassembly Timer is stopped   \n");

        /* Free any fragments on list, starting at beginning */
        while ((pFrag = (tNatIpFrag *) TMO_DLL_First (&pReasm->FragList)))
        {
            if (pFrag->pBuf != NULL)
            {
                NAT_RELEASE_BUF (pFrag->pBuf, FALSE);
            }
            /* Should have deleted from reassembly list before freeing it. */
            TMO_DLL_Delete (&pReasm->FragList, &pFrag->Link);
            NAT_RELEASE_MEM_BLOCK (NAT_FRAG_FRAGS_POOL_ID, pFrag);
        }
        NAT_TRC (NAT_TRC_ON, "Removing Reassembly list was successful\n");
        NAT_RELEASE_MEM_BLOCK (NAT_FRAG_FRAG_LIST_POOL_ID, pReasm);

    }
}

/*-------------------------------------------------------------------+
 *
 * Function           : NatNewFrag
 *
 * Input(s)           : u2StartOffset, u2EndOffset, pBuf
 *
 * Output(s)          : None
 *
 * Returns            : pointer to new fragment if NAT_SUCCESS;
 *                      NULL if failure
 *
 * Action :
 * Create a fragment.
 *
+-------------------------------------------------------------------*/
PRIVATE tNatIpFrag *
NatNewFrag (UINT2 u2StartOffset, UINT2 u2EndOffset, tIP_BUF_CHAIN_HEADER * pBuf)
{
    tNatIpFrag         *pFrag = NULL;

    if (pBuf == NULL)
    {
        NAT_TRC (NAT_TRC_ON, "CRU Buffer passed in is a NULL pointer\n");
        NAT_DBG (NAT_DBG_MEM, "CRU Buffer passed in is a NULL pointer\n"
                 "in NatNewFrag\n");
        return NULL;
    }

    if (NAT_FRAG_ALLOCATE_MEM_BLOCK (NAT_FRAG_FRAGS_POOL_ID, pFrag, tNatIpFrag)
        == NULL)
    {
        /* Drop fragment */
        NAT_RELEASE_BUF (pBuf, FALSE);
        NAT_TRC (NAT_TRC_ON,
                 "\n Error : Mem allocation failed for the tNatIpFrag to save"
                 " the buffer, hence fragment is freed ");
        MOD_TRC (gu4NatTrc, ALL_FAILURE_TRC, "NAT",
                 "\n  insufficient memory: Mem allocation failed for the "
                 "tNatIpFrag to save the buffer, hence fragment is freed\n");
        return NULL;
    }
    pFrag->pBuf = pBuf;
    pFrag->u2StartOffset = u2StartOffset;
    pFrag->u2EndOffset = u2EndOffset;
    TMO_DLL_Init_Node (&pFrag->Link);
    return pFrag;
}

/*-------------------------------------------------------------------+
 *
 * Function           : NatFreeFrag 
 *
 * Input(s)           : pFrag
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action :
 * Delete a fragment, return next one on queue.
 *
+-------------------------------------------------------------------*/
PRIVATE VOID
NatFreeFrag (tNatIpFrag * pFrag)
{
    if (pFrag == NULL)
    {
        NAT_TRC (NAT_TRC_ON,
                 "Fragment structure passed in is a NULL pointer\n");
        NAT_DBG (NAT_DBG_MEM,
                 "fragment structure passed in is a NULL pointer\n");
        return;
    }
    if (pFrag->pBuf != NULL)
    {
        NAT_RELEASE_BUF (pFrag->pBuf, FALSE);
    }
    NAT_RELEASE_MEM_BLOCK (NAT_FRAG_FRAGS_POOL_ID, pFrag);

}

/*****************************************************************************/
/* Function     : NatTmrHandleExpiry                                         */
/*                                                                           */
/* Description  : This procedure is invoked when the event indicating a timer*/
/*                expiry occurs. This procedure finds the expired timers and */
/*                invokes the corresponding timer routines.                  */
/*                NOTE:                                                      */
/*                -----                                                      */
/*                This function would be for the whole NAT when we migrate   */
/*                to the new timer approach.                                 */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
NatTmrHandleExpiry (void)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    UINT1               u1TimerId = NAT_ZERO;
    INT2                i2Offset = NAT_ZERO;

    NAT_TRC (OS_RESOURCE_TRC, "Tmr Expiry To Be Handled\n");
    while ((pExpiredTimers = TmrGetNextExpiredTimer (gNatTimerListId)) != NULL)
    {
        NAT_TRC1 (OS_RESOURCE_TRC,
                  "Tmr Blk %x To Be Processed\n", pExpiredTimers);

        u1TimerId = ((tNatTimer *) pExpiredTimers)->u1TimerId;
        i2Offset = gaNatTimerDesc[u1TimerId].i2Offset;

        NAT_TRC2 (OS_RESOURCE_TRC,
                  "TmrId %d Offset %d \n", u1TimerId, i2Offset);

        if (i2Offset == NAT_NEGATIVE_OFFSET)
        {
            /* The timer function does not take any parameter */
            (*(gaNatTimerDesc[u1TimerId].TmrExpFn)) (NULL);
        }
        else
        {
            (*(gaNatTimerDesc[u1TimerId].TmrExpFn))
                ((UINT1 *) pExpiredTimers - i2Offset);
        }
    }
    NAT_TRC (OS_RESOURCE_TRC, "Tmr Expiry Processing Over\n");
}

/*-------------------------------------------------------------------+
 * Function           : NatFragHandleReasmTimerExpiry
 *
 * Input(s)           : pArg-  the RB Tree node whose reassembly timer 
 *                                 has fired
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action             :time out handler when assembling the fragments
 *
+-------------------------------------------------------------------*/
PRIVATE VOID
NatFragHandleReasmTimerExpiry (VOID *pArg)
{
    tNatFragmentStream *pReasm = pArg;

    NAT_TRC (NAT_TRC_ON, "Inside Nat_Frag_handle_reasm_timer_expiry\n");

    NAT_TRC4 (NAT_TRC_ON, "\n inside %s for the IP"
              "D =%d, Src %u  and Dst %u",
              "NatFragHandleReasmTimerExpiry",
              pReasm->u2Id, pReasm->u4Src, pReasm->u4Dest);

    NatFreeReasm (pReasm);
}

/*-------------------------------------------------------------------+
 * Function           : NatMakeDupBufChain
 *
 * Input(s)           : pOrigBuf ; the buffer to be duplicated
 *
 * Output(s)          : None
 *
 * Returns            : duplicate buffer or NULL
 *
 * Action             : duplicates the buffer. on error returns NULL
 *
+-------------------------------------------------------------------*/

PRIVATE tCRU_BUF_CHAIN_HEADER *
NatMakeDupBufChain (tCRU_BUF_CHAIN_HEADER * pOrigBuf)
{
    tCRU_BUF_CHAIN_HEADER *pDupBuf = NULL;
    UINT4               u4TotalDataLen = NAT_ZERO;
    UINT4               u4DataOffset = NAT_ZERO_OFFSET;
    tLinearBuf         *pLinearBuf = NULL;

    if (pOrigBuf == NULL)
    {
        NAT_TRC (NAT_TRC_ON, "CRU Buffer passed in is a NULL pointer\n"
                 "in NatMakeDupBufChain\n");
        NAT_DBG (NAT_DBG_MEM, "CRU Buffer passed in is a NULL pointer\n"
                 "in NatMakeDupBufChain\n");
        return NULL;
    }

    u4TotalDataLen = CRU_BUF_Get_ChainValidByteCount (pOrigBuf);
    if ((pDupBuf =
         CRU_BUF_Allocate_MsgBufChain (u4TotalDataLen, u4DataOffset)) == NULL)
    {
        return NULL;
    }
    if (NAT_MEM_ALLOCATE
        (NAT_LINEAR_BUF_POOL_ID, pLinearBuf, tLinearBuf) == NULL)
    {
        CRU_BUF_Release_MsgBufChain (pDupBuf, FALSE);
        return NULL;
    }
    if (CRU_BUF_Copy_FromBufChain
        (pOrigBuf, (UINT1 *) pLinearBuf, u4DataOffset,
         u4TotalDataLen) == CRU_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pDupBuf, FALSE);
        NatMemReleaseMemBlock (NAT_LINEAR_BUF_POOL_ID, (UINT1 *) pLinearBuf);
        return NULL;
    }
    else
    {
        if (CRU_BUF_Copy_OverBufChain
            (pDupBuf, (UINT1 *) pLinearBuf, u4DataOffset,
             u4TotalDataLen) == CRU_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pDupBuf, FALSE);
            NatMemReleaseMemBlock (NAT_LINEAR_BUF_POOL_ID,
                                   (UINT1 *) pLinearBuf);
            return NULL;
        }
    }

    MEMCPY (&pDupBuf->ModuleData, &pOrigBuf->ModuleData, sizeof (tMODULE_DATA));

    NatMemReleaseMemBlock (NAT_LINEAR_BUF_POOL_ID, (UINT1 *) pLinearBuf);
    return pDupBuf;
}

/*-------------------------------------------------------------------+
 * Function           : NatIsFragReq
 *
 * Input(s)           : pBuf,u2IfIndex
 *
 * Output(s)          : MTU size for fragmentation or 
                        0 when no fragmentation is required
 *
 * Returns            : NAT_FAILURE or NAT_SUCCESS
 *
+-------------------------------------------------------------------*/

UINT4
NatIsFragReq (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2IfIndex, UINT4 *pu4MTU)
{
    UINT2               u2PktSize = NAT_ZERO;
    UINT4               u4MTU = NAT_ZERO;

    *pu4MTU = NAT_ZERO_MTU;
    if (pBuf == NULL)
    {
        NAT_TRC (NAT_TRC_ON, "CRU Buffer passed in is a NULL pointer\n"
                 "in NatIsFragReq\n");
        NAT_DBG (NAT_DBG_MEM, "CRU Buffer passed in is a NULL pointer\n"
                 "in NatIsFragReq\n");
        return NAT_FAILURE;
    }
#ifdef FLOWMGR_WANTED
    {
        tFlowData          *pFlowData;
        UNUSED_PARAM (u2IfIndex);

        /* the MTU is the dynamic MTU based on PMTUD if enabled */
        pFlowData = (tFlowData *) & (pBuf->ModuleData.FlowData[NAT_ZERO]);
        u4MTU = pFlowData->u2Mtu;

    }

#else
    {
        t_IP_SEND_PARMS    *pIPParms = NULL;
        UINT4               u4FsIpPmtuDst = NAT_ZERO;
#ifndef LNXIP4_WANTED
        INT4                i4PathMtu = NAT_ZERO;
#endif
        INT4                i4FsIpPmtuTos = NAT_ZERO;

        UNUSED_PARAM (u4FsIpPmtuDst);
        UNUSED_PARAM (i4FsIpPmtuTos);
        pIPParms = (t_IP_SEND_PARMS *) CRU_BUF_Get_ModuleData (pBuf);
        u4FsIpPmtuDst = pIPParms->u4Dest;
        i4FsIpPmtuTos = pIPParms->u1Tos;

#ifndef LNXIP4_WANTED
        if (nmhGetFsIpPathMtu (u4FsIpPmtuDst, i4FsIpPmtuTos,
                               &i4PathMtu) == SNMP_FAILURE)
        {
            if (SecUtilGetIfMtu (u2IfIndex, &u4MTU) == OSIX_FAILURE)
            {
                return NAT_FAILURE;
            }
            if (u4MTU == NAT_ZERO_MTU)
            {
                return NAT_FAILURE;
            }
        }
        else
        {
            NAT_TRC (NAT_TRC_ON, "Getting MTU size from PMTU \n");
            u4MTU = (UINT4) i4PathMtu;
        }
#else
        UNUSED_PARAM (u2IfIndex);
#endif
    }
#endif

    u2PktSize = (UINT2) CRU_BUF_Get_ChainValidByteCount (pBuf);
    if (u2PktSize > u4MTU)
    {
        *pu4MTU = u4MTU;
    }
    return NAT_SUCCESS;
}

/***************************************************************/

/*  Function Name   : NatFragment                              */

/*  Description     : Fragments the buffer (pBuf), using the 
                      MTU value passed and returns the 
                      fragments in a DLL                       */

/*                   The pBuf buffer is freed on error         */

/*  Input(s)        : pBuf, u2IfIndex, u4MTU                   */

/*  Output(s)       : pFragList                                */

/*  Returns         : NAT_SUCCESS or NAT_FAILURE               */

/***************************************************************/

UINT4
NatFragment (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2IfIndex, UINT4 u4MTU,
             tTMO_DLL * pFragList)
{
    t_IP                IpHdrForFrag;
    t_IP                IpHdr;
    t_IP               *pIp = NULL;
    tCRU_BUF_CHAIN_HEADER *pTmpBuf = NULL;
    tCRU_BUF_CHAIN_HEADER *pFragData = NULL;
    tCfaModuleData      ModuleData;
    /* Temp to store original more flag */
    INT2                i2OrgMFlg = NAT_ZERO;
    UINT2               u2Offset = NAT_ZERO;
    /* Size of this fragment's data */
    UINT2               u2FragDataSize = NAT_ZERO;
    INT2                i2IpHdrLen = NAT_ZERO;
    UINT2               u2Len = NAT_ZERO;
    UINT2               u2FragLen = NAT_ZERO;
    UINT2               u2FlgOffs = NAT_ZERO;
    UINT2               u2OrgLen = NAT_ZERO;    /* added for trace and debug */
    INT2                i2OrgDf = NAT_ZERO;
    INT1                i1FirstFrag = TRUE;
    INT1                i1LastFrag = FALSE;
    UINT2               u2StartOffset = NAT_ZERO;
    UINT2               u2EndOffset = NAT_ZERO;
    INT1                i1MoreFlag = NAT_ZERO;

    UNUSED_PARAM (u2IfIndex);

    NAT_TRC (NAT_TRC_ON, "NatFragment has been called\n");

    if (pBuf == NULL)
    {
        NAT_TRC (NAT_TRC_ON, "CRU Buffer passed in is a NULL pointer\n"
                 "in NatFragment\n");
        NAT_DBG (NAT_DBG_MEM, "CRU Buffer passed in is a NULL pointer\n"
                 "in NatFragment\n");
        return NAT_FAILURE;
    }

    if (pFragList == NULL)
    {
        NAT_TRC (NAT_TRC_ON, "Fragmnet DLL passed in is a NULL pointer\n"
                 "in NatFragment\n");
        NAT_DBG (NAT_DBG_MEM, "Fragment DLL passed in is a NULL pointer\n"
                 "in NatFragment\n");
        NAT_RELEASE_BUF (pBuf, FALSE);
        return NAT_FAILURE;
    }

    pIp = &IpHdr;

#ifndef LNXIP4_WANTED
    if (IpExtractHdr (pIp, pBuf) != IP_SUCCESS)
    {
        NAT_DBG (NAT_DBG_PKT_FLOW, "\n Malformed IP pkt\n");
        NAT_RELEASE_BUF (pBuf, FALSE);
        return NAT_FAILURE;
    }
#endif

    TMO_DLL_Init (pFragList);    /* This list wud have the fragments */

    MEMSET (&ModuleData, NAT_ZERO, sizeof (tCfaModuleData));
    MEMCPY (&ModuleData, GET_MODULE_DATA_PTR (pBuf), sizeof (tCfaModuleData));

    u2OrgLen = pIp->u2Len;
    i2IpHdrLen = (INT2) (IP_HDR_LEN + u2OrgLen);
    u2Len = (UINT2) (pIp->u2Len - i2IpHdrLen);    /* Data length only */
    u2Offset = (UINT2) (NAT_OFFSET_IN_BYTES (pIp->u2Fl_offs));
    i2OrgDf = pIp->u2Fl_offs & NAT_DF_AND_0_OFFSET;
    /* The DF flag would have been reset if assembled in NAT.If not assembled
     * in NAT, and if the packet is getting fragmented, then it is 
     * because of the NAT translation (delta increase in size). Here the
     * DF flag would be preserved.

     * Actually an ICMP error has to be thrown to the source of the packet
     * instructing it to send the packet of size lesser than the
     * delta increase due to the NAT translation if DF is set to 1 */
    i2OrgMFlg = pIp->u2Fl_offs & IP_MF;    /* Save original MF flag */

    while (u2Len > NAT_ZERO)    /* As long as there's data left */
    {
        pIp->u2Fl_offs = (UINT2) (NAT_OFFSET_IN_IP_HDR_FORMAT (u2Offset));

        if ((UINT2) (u2Len + i2IpHdrLen) <= u4MTU)
        {
            /* Last fragment; send all that remains */
            u2FragDataSize = u2Len;
            pIp->u2Fl_offs |= (UINT2) i2OrgMFlg;    /* Pass original MF flag */
            pIp->u2Fl_offs |= (UINT2) i2OrgDf;
            i1LastFrag = TRUE;
        }
        else
        {
            /* More to come, so send multpIple of 8 bytes */
            u2FragDataSize =
                (UINT2) (((INT2) u4MTU - i2IpHdrLen) & (NAT_FRAG_DATA_OFFSET));
            pIp->u2Fl_offs |= IP_MF;
            pIp->u2Fl_offs |= (UINT2) i2OrgDf;
        }

        pIp->u2Len = (UINT2) (u2FragDataSize + i2IpHdrLen);

        /* Put IP header back on */
        if (i1FirstFrag == FALSE)
        {
            pTmpBuf =
                NAT_ALLOCATE_BUF ((UINT4) (IP_HDR_LEN + pIp->u2Olen), NAT_ZERO);
            if (pTmpBuf == NULL)
            {
                NatFreeOutFragList (pFragList);
                NAT_RELEASE_BUF (pBuf, FALSE);
                return NAT_FAILURE;
            }
            MEMCPY (GET_MODULE_DATA_PTR (pTmpBuf), &ModuleData,
                    sizeof (tCfaModuleData));
#ifndef LNXIP4_WANTED
            ip_put_hdr (pTmpBuf, pIp, TRUE);
#endif
            /*
             * Extract data from original buffer and link it with our
             * header.
             */
            pFragData = pBuf;

            if (i1LastFrag == FALSE)
            {
                if (NAT_IP_FRAGMENT_BUF (pFragData, u2FragDataSize, &pBuf) !=
                    CRU_SUCCESS)
                {
                    NAT_RELEASE_BUF (pTmpBuf, FALSE);
                    NatFreeOutFragList (pFragList);
                    NAT_RELEASE_BUF (pFragData, FALSE);
                    return (NAT_FAILURE);
                }
            }
            NAT_CONCAT_BUFS (pTmpBuf, pFragData);
        }
        else
        {
            /*
             * Extract IP header  and data from front end and send it.
             */
            pTmpBuf = pBuf;
            if (NAT_IP_FRAGMENT_BUF
                (pTmpBuf, (UINT4) (u2FragDataSize + (UINT2) i2IpHdrLen),
                 &pBuf) != CRU_SUCCESS)
            {
                NAT_RELEASE_BUF (pTmpBuf, FALSE);
                NatFreeOutFragList (pFragList);
                return (NAT_FAILURE);
            }

#ifndef LNXIP4_WANTED
            ip_put_hdr (pTmpBuf, pIp, TRUE);
#endif
        }

        /* Now the datagram to send is in pTmpBuf; Add frag specific info */
        u2FragLen = CRU_HTONS (pIp->u2Len);
        NAT_COPY_TO_BUF (pTmpBuf, &u2FragLen, NAT_FRAG_TOTAL_LEN_OFFSET,
                         NAT_TWO_BYTE_LEN);
        u2FlgOffs = CRU_HTONS (pIp->u2Fl_offs);
        NAT_COPY_TO_BUF (pTmpBuf, &u2FlgOffs, NAT_FRAG_BIT_OFFSET,
                         NAT_TWO_BYTE_LEN);
        /* For trace printing */

        /* Convert to bytes */
        u2StartOffset = (UINT2) (NAT_OFFSET_IN_BYTES (pIp->u2Fl_offs));
        u2EndOffset = (UINT2) (u2StartOffset + pIp->u2Len - pIp->u1Hlen);
        i1MoreFlag = (pIp->u2Fl_offs & NAT_MORE_FRAG_BIT) ? TRUE : FALSE;

        NAT_TRC6 (NAT_TRC_ON,
                  "\n IP Header Len in bytes %d, Flag and offset %d, Total len "
                  "= %d, strt off = %d, end off =%d MF = %d", pIp->u1Hlen,
                  pIp->u2Fl_offs, pIp->u2Len, u2StartOffset, u2EndOffset,
                  i1MoreFlag);

        /* add the fragments in list */
        if (NatAddFragToLinkList (pFragList, pTmpBuf) != NAT_SUCCESS)
        {
            /* Free all our extra data */
            if (pTmpBuf != pBuf)
            {
                NAT_RELEASE_BUF (pTmpBuf, FALSE);
            }
            NAT_RELEASE_BUF (pBuf, FALSE);
            NatFreeOutFragList (pFragList);
            return NAT_FAILURE;
        }

        /* Each time offset gets updated */
        u2Offset = (UINT2) (u2Offset + u2FragDataSize);
        u2Len = (UINT2) (u2Len - u2FragDataSize);

        if (i1FirstFrag == TRUE)
        {
            i1FirstFrag = FALSE;
            /* If there are no options dont need to copy */
            if (pIp->u2Olen != NAT_ZERO_LEN)
            {
                /*
                 * After the first fragment, should remove those
                 * options that aren't supposed to be copied on fragmentation
                 */
#ifndef LNXIP4_WANTED
                ip_construct_hdr_for_frags (pIp, &IpHdrForFrag);
#endif
                pIp = &(IpHdrForFrag);
                i2IpHdrLen = (INT2) (IP_HDR_LEN + pIp->u2Olen);
            }
        }                        /* first frag */
    }                            /* while length */
    return NAT_SUCCESS;
}

/***************************************************************/

/*  Function Name   : NatAddFragToLinkList                     */

/*  Description     : adds the fragment into the DLL           */

/*  Input(s)        : pFragList and fragment (pBuf)            */

/*  Output(s)       : none                                     */

/*  Returns         : NAT_SUCCESS or NAT_FAILURE               */

/***************************************************************/

PRIVATE UINT4
NatAddFragToLinkList (tTMO_DLL * pFragList, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tNatIpFrag         *pFrag = NULL;

    if ((pFragList == NULL) || (pBuf == NULL))
    {
        NAT_DBG (NAT_DBG_MEM, "\n the CRU buffer or Frag DLL "
                 "in NatAddFragToLinkList is a NULL pointer\n");
        return NAT_FAILURE;
    }

    pFrag = NatNewFrag (NAT_ZERO, NAT_ZERO, pBuf);
    if (pFrag == NULL)
    {
        MOD_TRC (gu4NatTrc, ALL_FAILURE_TRC, "NAT",
                 "\n NatNewFrag returned failure:  \n");
        return NAT_FAILURE;
    }
    TMO_DLL_Add (pFragList, (tTMO_DLL_NODE *) & pFrag->Link);

    return NAT_SUCCESS;
}

/***************************************************************/

/*  Function Name   : NatFreeOutFragList                       */

/*  Description     : frees the DLL and the associated memory  */

/*  Input(s)        : pFragList                                */

/*  Output(s)       : none                                     */

/*  Returns         : NAT_SUCCESS or NAT_FAILURE               */

/***************************************************************/

PUBLIC UINT4
NatFreeOutFragList (tTMO_DLL * pFragList)
{
    UINT4               u4Status = NAT_SUCCESS;
    tNatIpFrag         *pFrag = NULL;

    if (NULL != pFragList)
    {
        while ((pFrag = (tNatIpFrag *) TMO_DLL_First (pFragList)))
        {
            if (pFrag->pBuf != NULL)
            {
                NAT_RELEASE_BUF (pFrag->pBuf, FALSE);
            }
            /* Should have deleted from reassembly list before freeing it. */
            TMO_DLL_Delete (pFragList, &pFrag->Link);
            NAT_RELEASE_MEM_BLOCK (NAT_FRAG_FRAGS_POOL_ID, pFrag);
        }
        TMO_DLL_Init (pFragList);
    }
    else
    {
        u4Status = NAT_FAILURE;
    }
    return u4Status;
}

/***************************************************************/

/*  Function Name   : NatGetFragFrmQueToSend                   */

/*  Description     : Removes the 1st fragment from the DLL 
                      Queue and returns the fragment           */

/*  Input(s)        : pFragList                                */

/*  Output(s)       : none                                     */

/*  Returns         : the fragment or NULL                     */

/***************************************************************/

PUBLIC tCRU_BUF_CHAIN_HEADER *
NatGetFragFrmQueToSend (tTMO_DLL * pFragList)
{
    tNatIpFrag         *pFrag = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;

    pFrag = (tNatIpFrag *) TMO_DLL_First (pFragList);
    if (pFrag == NULL)
    {
        return NULL;
    }
    else
    {
        pBuf = pFrag->pBuf;
        TMO_DLL_Delete (pFragList, (tTMO_DLL_NODE *) & pFrag->Link);
        NAT_RELEASE_MEM_BLOCK (NAT_FRAG_FRAGS_POOL_ID, pFrag);
        return (pBuf);
    }
}
#endif /* _NATFRAG_C */
