/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: natmemac.c,v 1.5 2013/10/25 10:52:27 siva Exp $
 *
 * Description:This file contains functions for allocating and deallocating
 *             memory pools.
 *
 *******************************************************************/
#include "natinc.h"

#define NAT_MEMPOOL

/***************************************************************************
* Function Name    :  NatMemReleaseMemBlock
* Description    :  This function releases mem blocks back to either mempool
*             or system memory.
*
* Input (s)    :  1.PoolId - The ID of the pool to which the mem block
*            should be returned.
*             2.pu1Block - Pointer to the mem block
*
* Output (s)    :  None
* Returns      :  None.
*
****************************************************************************/
PUBLIC VOID
NatMemReleaseMemBlock (tMemPoolId PoolId, UINT1 *pu1Block)
{
    UINT4               u4MemStatus = NAT_ZERO;

    u4MemStatus = MemReleaseMemBlock (PoolId, pu1Block);

    /* To be verified */

    if (u4MemStatus == MEM_FAILURE)
    {
        MEM_FREE (pu1Block);
    }
}
