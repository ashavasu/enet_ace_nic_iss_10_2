
/********************************************************************
 * Copyright (C) Future Sotware,2002
 * 
 *  $Id: nath323.c,v 1.6 2013/10/25 10:50:23 siva Exp $ 
 *  
 *  Description:This file contains functions required for
 *             modifying the h323 payload if required.
 *
 ********************************************************************/
#ifndef _NATH323_C
#define _NATH323_C
#include "natinc.h"

/* H245 Application definition record */
tNatAppDefn         gNatH245Appln = {
    "h245",                        /* Application Name */
    NAT_ZERO,                    /* Id */
    NAT_ZERO,                    /* UseCount */
    NatProcessH245Pkt,            /* pkt_out */
};

/****************************************************************************
* Function Name :  NatTranslateH225PktIn
* Description   : This function translates Inbound H225 pkt 
*                 
* Input (s)  : 1. pBuf - Inbound Pkt CRU buffer pointer
*            : 2. pHeaderInfo - Pkt Header Info
* Output (s) : The changed pkt payload
* Returns    : SUCCESS/FAILURE
*****************************************************************************/

PRIVATE INT4
NatTranslateH225PktIn (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo)
{
    tHeaderInfo         NewHeaderInfo;
    UINT4               u4DataOffset = NAT_ZERO;
    UINT4               u4DataLen = NAT_ZERO;
    UINT4               u4SrcIpAddr = NAT_ZERO;
    UINT4               u4TmpIpAddr = NAT_ZERO;
    UINT4               u4IpAddr = NAT_ZERO;
    UINT4               u4TransIpAddr = NAT_ZERO;
    UINT4               u4LocIpAddr = NAT_ZERO;
    INT4                i4Status = NAT_ZERO;
    UINT4               u4CksumOffset = NAT_ZERO;
    INT4                i4H245Registered = NAT_FALSE;
    UINT2               u2H245Port = NAT_ZERO;
    UINT2               u2TransportCksum = NAT_ZERO;
    UINT1              *pDataBuf = NULL;
    tLinearBuf         *pLinearBuf = NULL;
    UINT1              *pDataEnd = NULL;

    NAT_TRC2 (NAT_TRC_ON, "\n H225: Translating In Pkt, Port"
              "#(In/Out): %u/%u \n", pHeaderInfo->u2InPort,
              pHeaderInfo->u2OutPort);

    MEMSET (&NewHeaderInfo, NAT_ZERO, sizeof (tHeaderInfo));
    u4DataOffset = NAT_ZERO;

    /* get data offset */
    u4DataOffset = (UINT4) (pHeaderInfo->u1IpHeadLen);

    /* Toatal length of the pkt */
    u4DataLen = CRU_BUF_Get_ChainValidByteCount (pBuf);

    /* Transport checksum offset */
    u4CksumOffset = u4DataOffset + NAT_TCP_CKSUM_OFFSET;
    /* payload length */
    u4DataLen -= u4DataOffset;

    /* allocate memory to copy data portion to linear buffer */
    /* pLinearBuf = (UINT1 *) NAT_MALLOC (u4DataLen * sizeof (UINT1)); */
    if (NAT_MEM_ALLOCATE
        (NAT_LINEAR_BUF_POOL_ID, pLinearBuf, tLinearBuf) == NULL)
    {
        NAT_TRC (NAT_TRC_ON, "\n H.225: Memory alloc failure");
        return (NAT_FAILURE);
    }

    /* Copy data from CRU buffer to linear buffer */
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pLinearBuf, u4DataOffset,
                               u4DataLen);

    /* initialise search variables */
    pDataEnd = (UINT1 *) pLinearBuf + u4DataLen;
    pDataBuf = (UINT1 *) pLinearBuf;

    u4SrcIpAddr = OSIX_HTONL (pHeaderInfo->pDynamicEntry->u4OutIpAddr);
    u4TmpIpAddr =
        OSIX_HTONL (pHeaderInfo->pDynamicEntry->u4TranslatedLocIpAddr);
    while (((pDataBuf + (NAT_IP_ADDR_LEN + NAT_WORD_LEN)) < pDataEnd)
           && (u4TmpIpAddr != u4SrcIpAddr))
    {
        pDataBuf++;
        MEMCPY (&u4TmpIpAddr, pDataBuf, NAT_IP_ADDR_LEN);
    }

    if (u4TmpIpAddr == u4SrcIpAddr)
    {
        MEMCPY (&u2H245Port, (pDataBuf + NAT_IP_ADDR_LEN), NAT_WORD_LEN);

        u2H245Port = OSIX_NTOHS (u2H245Port);

        NAT_TRC1 (NAT_TRC_ON, "\n Call From Private Host, Port#: %u",
                  u2H245Port);
        NewHeaderInfo.u4IfNum = pHeaderInfo->u4IfNum;
        NewHeaderInfo.u4OutIpAddr = OSIX_NTOHL (u4TmpIpAddr);
        NewHeaderInfo.u2OutPort = u2H245Port;
        NewHeaderInfo.u4InIpAddr = pHeaderInfo->u4InIpAddr;
        NewHeaderInfo.u4Direction = NAT_OUTBOUND;
        NewHeaderInfo.u1PktType = NAT_TCP;
        u4IpAddr = OSIX_NTOHL (u4TmpIpAddr);

        if (NatScanPartialLinksList (&NewHeaderInfo, &u4IpAddr,
                                     &u2H245Port) != NAT_SUCCESS)
        {
            NatAddPartialLinksList (&NewHeaderInfo, u4IpAddr,
                                    u2H245Port, NAT_NON_PERSISTENT);
        }

        if ((gu4NatEnable == NAT_DISABLE) ||
            (NatCheckIfNatEnable (NewHeaderInfo.u4IfNum) == NAT_DISABLE))
        {
            u2H245Port = NewHeaderInfo.u2OutPort;
        }

        i4Status = NatRegisterAppln (&gNatH245Appln, NAT_TCP,
                                     u2H245Port, NAT_PORT_NEG_APPLN_H323);
        if (i4Status != NAT_SUCCESS)
        {
            return NAT_FAILURE;
        }
        if (i4H245Registered != NAT_TRUE)
        {
            i4H245Registered = NAT_TRUE;
        }
    }

    /* call from public host */
    pDataBuf = (UINT1 *) pLinearBuf;
    u4TransIpAddr = OSIX_HTONL
        (pHeaderInfo->pDynamicEntry->u4TranslatedLocIpAddr);

    while (((pDataBuf + (NAT_IP_ADDR_LEN + NAT_PORT_LEN)) < pDataEnd)
           && (u4TmpIpAddr != u4TransIpAddr))
    {
        pDataBuf++;
        MEMCPY (&u4TmpIpAddr, pDataBuf, NAT_IP_ADDR_LEN);
    }

    if (u4TmpIpAddr == u4TransIpAddr)
    {
        NAT_TRC1 (NAT_TRC_ON, "\n Call From Public Host, Addr#: %x",
                  u4TransIpAddr);

        u4LocIpAddr = OSIX_HTONL (pHeaderInfo->pDynamicEntry->u4LocIpAddr);

        MEMCPY (pDataBuf, &u4LocIpAddr, NAT_IP_ADDR_LEN);
    }

    /* Copy data to CRU buffer from linear buffer */
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) pLinearBuf, u4DataOffset,
                               u4DataLen);

    /* Recalculating transport checksum */
    if ((u4TmpIpAddr == u4TransIpAddr) || (u4TmpIpAddr == u4SrcIpAddr))
    {
        u2TransportCksum = NAT_ZERO;
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2TransportCksum,
                                   u4CksumOffset, NAT_WORD_LEN);
        u2TransportCksum = NatTransportCksumAdjust (pBuf, pHeaderInfo);
        if (u2TransportCksum == NAT_FAILURE)
        {
            NatMemReleaseMemBlock (NAT_LINEAR_BUF_POOL_ID,
                                   (UINT1 *) pLinearBuf);
            return NAT_FAILURE;
        }

        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2TransportCksum,
                                   u4CksumOffset, NAT_WORD_LEN);
    }

    NatMemReleaseMemBlock (NAT_LINEAR_BUF_POOL_ID, (UINT1 *) pLinearBuf);

    NAT_TRC (NAT_TRC_ON, "\n H225: Inbound Pkt Translation success");

    return (NAT_SUCCESS);
}

/****************************************************************************
* Function Name :  NatTranslateH225PktOut
* Description   : This function translates outbound H225 pkt
*                 
* Input (s)  : 1. pBuf - Incoming Pkt CRU buffer pointer
*            : 2. pHeaderInfo - Pkt Header Info
* Output (s) : The changed pkt payload
* Returns    : SUCCESS/FAILURE
*****************************************************************************/

PRIVATE INT4
NatTranslateH225PktOut (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo)
{
    tHeaderInfo         NewHeaderInfo;
    UINT4               u4DataOffset = NAT_ZERO;
    UINT4               u4DataLen = NAT_ZERO;
    UINT4               u4LocIpAddr = NAT_ZERO;
    UINT4               u4DataIpAddr = NAT_ZERO;
    UINT4               u4TmpIpAddr = NAT_ZERO;
    UINT4               u4TransIpAddr = NAT_ZERO;
    UINT4               u4ConnectionStatus = NAT_ZERO;
    INT4                i4H245Registered = NAT_FALSE;
    UINT4               u4Status = NAT_ZERO;
    UINT2               u2TransPort = NAT_ZERO;
    UINT2               u2LocPort = NAT_ZERO;
    UINT2               u2DataPort = NAT_ZERO;
    UINT2               u2TmpPort = NAT_ZERO;
    UINT2               u2TransportCksum = NAT_ZERO;
    UINT4               u4CksumOffset = NAT_ZERO;
    UINT1              *pDataBuf = NULL;
    tLinearBuf         *pLinearBuf = NULL;
    UINT1              *pDataEnd = NULL;
    INT1                i1ChangeOver = NAT_FALSE;

    NAT_TRC2 (NAT_TRC_ON, "\n H225: Translating Out Pkt, Port"
              "#(In/Out): %u/%u \n", pHeaderInfo->u2InPort,
              pHeaderInfo->u2OutPort);

    /* get data offset */
    u4DataOffset = (UINT4) (pHeaderInfo->u1IpHeadLen);

    /* Toatal length of the pkt */
    u4DataLen = CRU_BUF_Get_ChainValidByteCount (pBuf);

    /* payload length */
    u4DataLen -= u4DataOffset;

    /* allocate memory to copy data portion to linear buffer */
    /* pLinearBuf = (UINT1 *) NAT_MALLOC (u4DataLen * sizeof (UINT1)); */
    if (NAT_MEM_ALLOCATE
        (NAT_LINEAR_BUF_POOL_ID, pLinearBuf, tLinearBuf) == NULL)
    {
        NAT_TRC (NAT_TRC_ON, "\n H.225: Memory alloc failure");
        return (NAT_FAILURE);
    }

    /* Copy data from CRU buffer to linear buffer */
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pLinearBuf, u4DataOffset,
                               u4DataLen);
    u4CksumOffset = NAT_TCP_CKSUM_OFFSET + (UINT4) pHeaderInfo->u1IpHeadLen;
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2TransportCksum,
                               u4CksumOffset, NAT_WORD_LEN);
    /* initialise search variables */
    pDataEnd = (UINT1 *) pLinearBuf + u4DataLen;
    pDataBuf = (UINT1 *) pLinearBuf;

    u4LocIpAddr = OSIX_HTONL (pHeaderInfo->pDynamicEntry->u4LocIpAddr);
    u2LocPort = OSIX_HTONS (pHeaderInfo->pDynamicEntry->u2LocPort);

    while ((pDataBuf + (NAT_IP_ADDR_LEN + NAT_ONE)) < pDataEnd)
    {

        MEMCPY (&u4DataIpAddr, pDataBuf, NAT_IP_ADDR_LEN);

        if (u4DataIpAddr == u4LocIpAddr)
        {

            MEMCPY (&u2DataPort, (pDataBuf + NAT_IP_ADDR_LEN), NAT_WORD_LEN);

            /* call from private host */
            if (u2DataPort == u2LocPort)
            {
                NAT_TRC1 (NAT_TRC_ON, "\n Call From Private Host, Port#: %u",
                          OSIX_NTOHS (u2DataPort));

                u2TmpPort = OSIX_HTONS (pHeaderInfo->u2InPort);
                u4TmpIpAddr = OSIX_HTONL (pHeaderInfo->u4InIpAddr);

                /* change msg IP address and port */
                MEMCPY (pDataBuf, &u4TmpIpAddr, NAT_IP_ADDR_LEN);
                pDataBuf = (pDataBuf + NAT_IP_ADDR_LEN);
                MEMCPY (pDataBuf, &u2TmpPort, NAT_WORD_LEN);

                /* set change complete flag */
                i1ChangeOver = NAT_TRUE;
            }
            else if (i1ChangeOver == NAT_FALSE)
            {
                u4DataIpAddr = OSIX_NTOHL (u4DataIpAddr);
                u2DataPort = OSIX_NTOHS (u2DataPort);

                NAT_TRC1 (NAT_TRC_ON, "\n Call From Private Host, Port#: %u",
                          u2DataPort);

                NewHeaderInfo.u4IfNum = pHeaderInfo->u4IfNum;
                NewHeaderInfo.u4OutIpAddr = pHeaderInfo->u4OutIpAddr;
                NewHeaderInfo.u1PktType = NAT_TCP;
                NewHeaderInfo.u4Direction = NAT_INBOUND;

                NewHeaderInfo.u4InIpAddr = u4DataIpAddr;
                NewHeaderInfo.u2InPort = u2DataPort;

                u4ConnectionStatus = NatSearchListAndTable (&NewHeaderInfo,
                                                            &u4TransIpAddr,
                                                            &u2TransPort);

                if (NAT_FAILURE == u4ConnectionStatus)
                {
                    u4TransIpAddr =
                        pHeaderInfo->pDynamicEntry->u4TranslatedLocIpAddr;

                    /* If Entry already exists with NewHeaderInfo then 
                     * do not allocate new port. */
                    if (NatScanPartialLinksList (&NewHeaderInfo, &u4TransIpAddr,
                                                 &u2TransPort) != NAT_SUCCESS)
                    {
                        u2TransPort =
                            (UINT2) (NatGetFreeGlobalPort (pHeaderInfo->u4IfNum,
                                                           u2DataPort));
                    }

                    NewHeaderInfo.u4InIpAddr = u4TransIpAddr;
                    NewHeaderInfo.u2InPort = u2TransPort;

                    NatAddPartialLinksList (&NewHeaderInfo, u4DataIpAddr,
                                            u2DataPort, NAT_NON_PERSISTENT);

                    NAT_TRC2 (NAT_TRC_ON,
                              "\n H245: New entry added to Tmp list. Port"
                              "#(In/Out): %u/%u \n",
                              NewHeaderInfo.u2InPort, NewHeaderInfo.u2OutPort);
                }

                if (i4H245Registered == NAT_TRUE)
                {
                    u4Status = (UINT4) NatUnRegisterAppln (&gNatH245Appln);
                    if (u4Status == NAT_SUCCESS)
                    {
                        i4H245Registered = NAT_FALSE;
                    }
                    u4Status =
                        (UINT4) NatRegisterAppln (&gNatH245Appln, NAT_TCP,
                                                  u2DataPort,
                                                  NAT_PORT_NEG_APPLN_H323);
                    i4H245Registered = NAT_TRUE;
                }
                else
                {
                    u4Status =
                        (UINT4) NatRegisterAppln (&gNatH245Appln, NAT_TCP,
                                                  u2DataPort,
                                                  NAT_PORT_NEG_APPLN_H323);
                    i4H245Registered = NAT_TRUE;
                }

                NAT_TRC1 (NAT_TRC_ON,
                          "\n H.245 applicatn registered at Port#: %u",
                          u2DataPort);

                u2DataPort = OSIX_HTONS (u2DataPort);
                u4DataIpAddr = OSIX_HTONL (u4DataIpAddr);

                u2TmpPort = OSIX_HTONS (u2TransPort);
                u4TmpIpAddr = OSIX_HTONL (u4TransIpAddr);

                /* change msg IP address and port */
                MEMCPY (pDataBuf, &u4TmpIpAddr, NAT_IP_ADDR_LEN);
                pDataBuf = (pDataBuf + NAT_IP_ADDR_LEN);
                MEMCPY (pDataBuf, &u2TmpPort, NAT_WORD_LEN);

            }
        }
        pDataBuf++;
    }

    /* Copy data to CRU buffer from linear buffer */
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) pLinearBuf, u4DataOffset,
                               u4DataLen);

    /* Recalculating transport checksum */
    u2TransportCksum = NAT_ZERO;
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2TransportCksum,
                               u4CksumOffset, NAT_WORD_LEN);
    u2TransportCksum = NatTransportCksumAdjust (pBuf, pHeaderInfo);
    if (u2TransportCksum == NAT_FAILURE)
    {
        NatMemReleaseMemBlock (NAT_LINEAR_BUF_POOL_ID, (UINT1 *) pLinearBuf);
        return NAT_FAILURE;
    }

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2TransportCksum,
                               u4CksumOffset, NAT_WORD_LEN);

    NatMemReleaseMemBlock (NAT_LINEAR_BUF_POOL_ID, (UINT1 *) pLinearBuf);
    NAT_TRC (NAT_TRC_ON, "\n H225: Outbound Pkt Translation success");

    return (NAT_SUCCESS);
}

/****************************************************************************
* Function Name :  NatTranslateH245PktInOut
* Description   : This function translates outbound/Inbound H245 pkt
*                 
* Input (s)  : 1. pBuf - Outbound Pkt CRU buffer pointer
*            : 2. pHeaderInfo - Pkt Header Info
* Output (s) : The changed pkt payload
* ReturnsderInfo-    : SUCCESS/FAILURE
*****************************************************************************/

PRIVATE INT4
NatTranslateH245PktInOut (tCRU_BUF_CHAIN_HEADER * pBuf,
                          tHeaderInfo * pHeaderInfo)
{
    tHeaderInfo         NewHeaderInfo;
    UINT4               u4DataOffset = NAT_ZERO;
    UINT4               u4DataLen = NAT_ZERO;
    UINT4               u4LocIpAddr = NAT_ZERO;
    UINT4               u4DataIpAddr = NAT_ZERO;
    UINT4               u4TmpIpAddr = NAT_ZERO;
    UINT4               u4TransIpAddr = NAT_ZERO;
    UINT4               u4ConnectionStatus = NAT_ZERO;
    UINT2               u2TransPort = NAT_ZERO;
    UINT2               u2LocPort = NAT_ZERO;
    UINT2               u2DataPort = NAT_ZERO;
    UINT2               u2TmpPort = NAT_ZERO;
    UINT2               u2TransportCksum = NAT_ZERO;
    UINT4               u4CksumOffset = NAT_ZERO;
    UINT1               u1PktChanged = NAT_FALSE;
    UINT1              *pDataBuf = NULL;
    tLinearBuf         *pLinearBuf = NULL;
    UINT1              *pDataEnd = NULL;

    NAT_TRC2 (NAT_TRC_ON, "\n H245: Translating Out Pkt, Port"
              "#(In/Out): %u/%u \n", pHeaderInfo->u2InPort,
              pHeaderInfo->u2OutPort);
    MEMSET (&NewHeaderInfo, NAT_ZERO, sizeof (tHeaderInfo));

    /* get data offset */
    u4DataOffset = (UINT4) (pHeaderInfo->u1IpHeadLen);

    /* Toatal length of the pkt */
    u4DataLen = CRU_BUF_Get_ChainValidByteCount (pBuf);

    /* payload length */
    u4DataLen -= u4DataOffset;

    /* allocate memory to copy data portion to linear buffer */
    /* pLinearBuf = (UINT1 *) NdAT_MALLOC (u4DataLen * sizeof (UINT1)); */
    if (NAT_MEM_ALLOCATE
        (NAT_LINEAR_BUF_POOL_ID, pLinearBuf, tLinearBuf) == NULL)
    {
        NAT_TRC (NAT_TRC_ON, "\n H.245: Memory alloc failure");
        return (NAT_FAILURE);
    }

    /* Copy data from CRU buffer to linear buffer */
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pLinearBuf, u4DataOffset,
                               u4DataLen);

    u4CksumOffset = NAT_TCP_CKSUM_OFFSET + (UINT4) pHeaderInfo->u1IpHeadLen;
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2TransportCksum,
                               u4CksumOffset, NAT_WORD_LEN);
    /* initialise search variables */
    pDataEnd = (UINT1 *) pLinearBuf + u4DataLen;
    pDataBuf = (UINT1 *) pLinearBuf + pHeaderInfo->u1TransportHeadLen;

    u4LocIpAddr = OSIX_HTONL (pHeaderInfo->pDynamicEntry->u4LocIpAddr);
    if (u2LocPort == NAT_ZERO)
    {
        u2LocPort = OSIX_HTONS (pHeaderInfo->pDynamicEntry->u2LocPort);
    }
    while ((pDataBuf + NAT_FIVE) < pDataEnd)
    {
        MEMCPY (&u4DataIpAddr, pDataBuf, NAT_IP_ADDR_LEN);

        if (pHeaderInfo->u4Direction == NAT_OUTBOUND)
        {
            if (u4DataIpAddr == u4LocIpAddr)
            {
                MEMCPY (&u2DataPort, (pDataBuf + NAT_IP_ADDR_LEN),
                        NAT_WORD_LEN);
                u4DataIpAddr = OSIX_NTOHL (u4DataIpAddr);
                u2DataPort = OSIX_NTOHS (u2DataPort);

                if (u2DataPort != NAT_H323_DATA_NEG_PORT)
                {
                    NewHeaderInfo.u4IfNum = pHeaderInfo->u4IfNum;
                    NewHeaderInfo.u4OutIpAddr = pHeaderInfo->u4OutIpAddr;
                    NewHeaderInfo.u1PktType = NAT_UDP;
                    NewHeaderInfo.u4Direction = NAT_INBOUND;

                    NewHeaderInfo.u4InIpAddr = u4DataIpAddr;
                    NewHeaderInfo.u2InPort = u2DataPort;

                    u4ConnectionStatus = NatSearchListAndTable (&NewHeaderInfo,
                                                                &u4TransIpAddr,
                                                                &u2TransPort);

                    if (NAT_FAILURE == u4ConnectionStatus)
                    {
                        /* If Entry already exists with NewHeaderInfo then 
                         * do not allocate new port. */
                        if (NatScanPartialLinksList
                            (&NewHeaderInfo, &u4TransIpAddr,
                             &u2TransPort) != NAT_SUCCESS)
                        {
                            u2TransPort =
                                (UINT2) (NatGetFreeGlobalPort
                                         (pHeaderInfo->u4IfNum, u2DataPort));
                        }
                        u4TransIpAddr = pHeaderInfo->u4InIpAddr;

                        NewHeaderInfo.u4InIpAddr = u4TransIpAddr;

                        NewHeaderInfo.u2InPort = u2TransPort;

                        NAT_TRC2 (NAT_TRC_ON, "\n H245: New entry added to"
                                  " Tmp list. Port #(In/Out): %u/%u \n",
                                  pHeaderInfo->u2InPort,
                                  pHeaderInfo->u2OutPort);

                        NatAddPartialLinksList (&NewHeaderInfo,
                                                u4DataIpAddr, u2DataPort,
                                                NAT_NON_PERSISTENT);
                    }
                    u2TmpPort = OSIX_HTONS (u2TransPort);
                    u4TmpIpAddr = OSIX_HTONL (u4TransIpAddr);
                }
                else
                {
                    /* If Port is 1503 then no need to create partial link */
                    u2TmpPort = OSIX_HTONS (u2DataPort);
                    u4TmpIpAddr =
                        OSIX_HTONL (pHeaderInfo->pDynamicEntry->
                                    u4TranslatedLocIpAddr);
                }

                u4DataIpAddr = OSIX_HTONL (u4DataIpAddr);
                u2DataPort = OSIX_HTONS (u2DataPort);
                /* change msg IP address and port */
                MEMCPY (pDataBuf, &u4TmpIpAddr, NAT_IP_ADDR_LEN);
                pDataBuf = (pDataBuf + NAT_IP_ADDR_LEN);
                MEMCPY (pDataBuf, &u2TmpPort, NAT_WORD_LEN);
                u1PktChanged = NAT_TRUE;
                pDataBuf++;
            }
        }
        /* Parsing InBound packet to create punch hole in Firewall
         * for OutBound dynamic connection
         */
        else                    /* NAT_INBOUND parsing */
        {
            if (u4DataIpAddr == OSIX_HTONL (pHeaderInfo->u4OutIpAddr))
            {
                MEMCPY (&u2DataPort, (pDataBuf + NAT_IP_ADDR_LEN),
                        NAT_WORD_LEN);
                u4DataIpAddr = OSIX_NTOHL (u4DataIpAddr);
                u2DataPort = OSIX_NTOHS (u2DataPort);
                if (u2DataPort != pHeaderInfo->u2OutPort)
                {
                    NewHeaderInfo.u4IfNum = pHeaderInfo->u4IfNum;
                    NewHeaderInfo.u4OutIpAddr = pHeaderInfo->u4OutIpAddr;
                    NewHeaderInfo.u2OutPort = u2DataPort;
                    NewHeaderInfo.u1PktType = NAT_UDP;
                    NewHeaderInfo.u4Direction = NAT_OUTBOUND;
                    NewHeaderInfo.u4InIpAddr = pHeaderInfo->u4InIpAddr;

                    if (NatScanPartialLinksList (&NewHeaderInfo, &u4DataIpAddr,
                                                 &u2DataPort) != NAT_SUCCESS)
                    {
                        NatAddPartialLinksList (&NewHeaderInfo,
                                                u4DataIpAddr, u2DataPort,
                                                NAT_NON_PERSISTENT);
                    }
                }

                pDataBuf = (pDataBuf + NAT_IP_ADDR_LEN);
                pDataBuf = (pDataBuf + NAT_WORD_LEN);
            }
        }
        pDataBuf++;
    }

    /* Copy data to CRU buffer from linear buffer */
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) pLinearBuf, u4DataOffset,
                               u4DataLen);

    /* Recalculating transport checksum */
    if (u1PktChanged == NAT_TRUE)
    {
        u2TransportCksum = NAT_ZERO;
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2TransportCksum,
                                   u4CksumOffset, NAT_WORD_LEN);
        u2TransportCksum = NatTransportCksumAdjust (pBuf, pHeaderInfo);
        if (u2TransportCksum == NAT_FAILURE)
        {
            NatMemReleaseMemBlock (NAT_LINEAR_BUF_POOL_ID,
                                   (UINT1 *) pLinearBuf);
            return NAT_FAILURE;
        }

        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2TransportCksum,
                                   u4CksumOffset, NAT_WORD_LEN);
    }

    /* free the temp buffer */
    NatMemReleaseMemBlock (NAT_LINEAR_BUF_POOL_ID, (UINT1 *) pLinearBuf);

    NAT_TRC (NAT_TRC_ON, "\n H245: Outbound Pkt Translation success");

    return (NAT_SUCCESS);
}

/****************************************************************************
* Function Name :  NatProcessH225Pkt
* Description   : This function translates Inbound/Outbound H225 pkt
*                 
* Input (s)  : 1. pBuf - Inbound/Outbound Pkt CRU buffer pointer
*            : 2. pHeaderInfo - Pkt Header Info
* Output (s) : The changed pkt payload
* Returns    : SUCCESS/FAILURE
*****************************************************************************/

PUBLIC INT4
NatProcessH225Pkt (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo)
{
    INT4                i4Status = NAT_ZERO;

    if (pHeaderInfo->u4Direction == NAT_OUTBOUND)
    {
        i4Status = NatTranslateH225PktOut (pBuf, pHeaderInfo);
    }
    else
    {
        i4Status = NatTranslateH225PktIn (pBuf, pHeaderInfo);
    }
    return (i4Status);
}

/****************************************************************************
* Function Name :  NatProcessH245Pkt
* Description   : This function changes  Inbound/Outbound H245 pkt
*                 
* Input (s)  : 1. pBuf - Incoming Pkt CRU buffer pointer
*            : 2. pHeaderInfo - Pkt Header Info
* Output (s) : The changed pkt payload
* Returns    : SUCCESS/FAILURE
*****************************************************************************/

PUBLIC INT4
NatProcessH245Pkt (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo)
{
    INT4                i4Status = NAT_ZERO;

    i4Status = NatTranslateH245PktInOut (pBuf, pHeaderInfo);

    return (i4Status);
}
#endif /* _NATH323_C */
