/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: nattimer.c,v 1.7 2013/10/25 10:52:27 siva Exp $
 *
 * Description:This file contains callback routines of timer
 *             sub module for NAT.
 *
 *******************************************************************/
#include "natinc.h"
#include "natwincs.h"

PRIVATE tTimerListId NatTimerListId;
/*********************************************************************
*  Function Name : NatInitTimer
*  Description    : This function initializes the timer list.
*  
*  Parameter(s)    : VOID
*
*  Return Values : VOID
*********************************************************************/
PUBLIC UINT4
NatInitTimer (VOID)
{
    UINT4               u4Status = TMR_FAILURE;
    MOD_TRC (gu4NatTrc, OS_RESOURCE_TRC, "NAT",
             "\n Timer for NAT initialised \n");
    /* The Timer List is denoted by NatTimerListId */
    u4Status =
        TmrCreateTimerList ((const UINT1 *) CFA_TASK_NAME,
                            NAT_TIMER_EXPIRY_EVENT, NULL, &NatTimerListId);
    if (u4Status == TMR_SUCCESS)
    {
        /*flag indicating that the timer is not yet started */
        gu4NatTmrEnable = NAT_DISABLE;
        u4Status = NAT_SUCCESS;
        MOD_TRC (gu4NatTrc, OS_RESOURCE_TRC, "NAT",
                 "\n Timer for NAT Success \n");

    }
    else
    {
        u4Status = NAT_FAILURE;
        MOD_TRC (gu4NatTrc, OS_RESOURCE_TRC, "NAT",
                 "\n Timer for NAT Fail.... \n");
    }
    return (u4Status);
}

/*********************************************************************
*  Function Name : NatDeInitTimer
*  Description    :
*     This function deinitializes the timer list.
*  Parameter(s)    : VOID
*  Return Values : VOID
*********************************************************************/
PUBLIC UINT4
NatDeInitTimer (VOID)
{
    UINT4               u4Status = NAT_ZERO;

    u4Status = NAT_FAILURE;
    MOD_TRC (gu4NatTrc, OS_RESOURCE_TRC, "NAT",
             "\n Timer for NAT is removed\n");
    u4Status = TmrDeleteTimerList (NatTimerListId);

    if (u4Status == TMR_SUCCESS)
    {
        u4Status = NAT_SUCCESS;
    }
    else
    {
        u4Status = NAT_FAILURE;
    }
    return (u4Status);
}

/*********************************************************************
*  Function Name : NatStartTimer
*  Description   : This procedure adds the global Timer node gu4NatAppTimer
*                  to the timer list.
*  Parameter(s)    : NONE
*
*  Return Values : VOID
*********************************************************************/
PUBLIC UINT4
NatStartTimer (VOID)
{

    UINT4               u4Status = NAT_ZERO;

    u4Status = NAT_SUCCESS;
    /* gu4NatIdleTimeOut is the Timeout period */
    u4Status =
        TmrStartTimer (NatTimerListId, &gu4NatAppTimer, gu4NatIdleTimeOut);
    if (u4Status == TMR_FAILURE)
    {
        u4Status = NAT_FAILURE;
    }
    MOD_TRC_ARG1 (gu4NatTrc, OS_RESOURCE_TRC, "NAT",
                  "\n Timer for NAT    of duration %d has been started \n",
                  gu4NatIdleTimeOut);
    return (u4Status);
}

/*********************************************************************
*  Function Name : NatStopTimer
*  Description   : This procedure stops the timer gu4NatIdleTimeOut if it is
*                  ACTIVE.
*  Parameter(s)  : VOID
*
*  Return Values : VOID
*********************************************************************/
VOID
NatStopTimer ()
{
    UINT4               u4Status = NAT_ZERO;

    u4Status = TMR_FAILURE;
    /*
     * Get the Expired Timer.
     * Then stop the Timer
     */
    TmrGetNextExpiredTimer (NatTimerListId);
    u4Status = TmrStopTimer (NatTimerListId, &gu4NatAppTimer);
    if (u4Status == TMR_SUCCESS)
    {
        gu4NatTmrEnable = NAT_DISABLE;
        MOD_TRC (gu4NatTrc, OS_RESOURCE_TRC, "NAT",
                 "\n Timer for NAT    is stopped  \n");
    }
    else
    {
        MOD_TRC (gu4NatTrc, ALL_FAILURE_TRC, "NAT",
                 "\n Unable to stop the Timer for NAT    \n");
    }
    return;
}

/*********************************************************************
*  Function Name : NatProcessTimeOut
*
*  Description   : This procedure takes appropriate action on the expiry
*                  of NAT_TIMER.
*  Parameter(s)    : NONE
*
*  Return Values : VOID
*********************************************************************/

PUBLIC VOID
NatProcessTimeOut (VOID)
{

    UINT4               u4Status = TMR_FAILURE;

    NatLock ();
    NatDeleteAllExpiredEntries ();
    NatUnLock ();
    /*
     * We are getting the expired Timer entry from the TimerList
     * and use the same entry to restart the timer 
     */
    if (NAT_ENABLE == gu4NatEnable)
    {
        TmrGetNextExpiredTimer (NatTimerListId);
        u4Status =
            TmrStartTimer (NatTimerListId, &gu4NatAppTimer, gu4NatIdleTimeOut);
        if (u4Status == TMR_FAILURE)
        {
            u4Status = NAT_FAILURE;
            MOD_TRC (gu4NatTrc, ALL_FAILURE_TRC, "NAT",
                     "\n Unable to start Timer \n");
        }
    }

    MOD_TRC_ARG1 (gu4NatTrc, OS_RESOURCE_TRC, "NAT",
                  "\n Timer for NAT    of duration %d has been started \n",
                  gu4NatIdleTimeOut);
}
