/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: natipsec.c,v 1.9 2014/01/25 13:52:26 siva Exp $
 *
 *  DESCRIPTION           :    This file contains functions for parsing
 *                            the IPSec packets and modifying the IPSec payload.
 *
 *******************************************************************/

/*****************************************************************************/
/*  FILE NAME             :   natipsec.c                                     */
/*  PRINCIPAL AUTHOR      :   FutureSoft                                     */
/*  SUBSYSTEM NAME        :    LR                                            */
/*  MODULE NAME           :    NAT                                           */
/*  LANGUAGE              :    C                                             */
/*  TARGET ENVIRONMENT    :   Linux                                          */
/*  DATE OF FIRST RELEASE :   dd/mm/yy                                       */
/*  AUTHOR                :   FutureSoft                                     */
/*  DESCRIPTION           :    This file contains functions for parsing      */
/*                          the IPSec packets and modifying the IPSec payload*/
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Version               :      1.1                                         */
/*  Date(DD/MM/YYYY)      :      14/08/03                                    */
/*  Modified by           :      FutureSoft                                  */
/*  Description of change :      addressed the code review comments          */
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Version               :      1.2                                         */
/*  Date(DD/MM/YYYY)      :      03/09/03                                    */
/*  Modified by           :      FutureSoft                                  */
/*  Description of change :      code after unit test changes                */
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Version               :      1.3                                         */
/*  Date(DD/MM/YYYY)      :      20/09/03                                    */
/*  Modified by           :      FutureSoft                                  */
/*  Description of change :      revision history updation                   */
/*****************************************************************************/
#ifndef NAT_IPSEC_C
#define NAT_IPSEC_C
#include "natinc.h"
tTMO_SLL            gNatIPSecListNode;    /* Pointer to IPSec session List */
tTMO_SLL            gNatIPSecPendListNode;    /* Pointer to IPSec Pending List */

tTMO_HASH_TABLE    *gpNatIPSecInHashList;    /* Pointer to IPSec In Hash Table */
tTMO_HASH_TABLE    *gpNatIPSecOutHashList;    /* Pointer to IPSec Out Hash Table */
tTMO_HASH_TABLE    *gpNatIPSecPendHashList;    /*Pointer to IPSec 
                                               Pending Hash Table */

/*****************************************************************************/
/*  Function Name   : natProcessIPSec                                        */
/*  Description     : NAT is maintaining a separate table for IPSec.This     */
/*                    function will create a separate dynamic entry in       */
/*                    the IPSec table and maintains all the IPSec entries    */
/*                    corresponding to different IPSec sessions flowing      */
/*                    across the NAT router.                                 */
/*  Input(s)        :  pBuf - Received buffer                                */
/*                      pHeaderInfo - This contains the header information   */
/*  Output(s)       :   pBuf - Modified  packet                              */
/*  Global Variables Referred :  None                                        */
/*  Global variables Modified :  None                                        */
/*  Exceptions or Operating System Error Handling :    None                  */
/*  Use of Recursion :  None                                                 */
/*  Returns          :  NAT_SUCCESS / NAT_FAILURE                            */
/*****************************************************************************/
UINT4
natProcessIPSec (tCRU_BUF_CHAIN_HEADER * pbuf, tHeaderInfo * pHeaderInfo)
{
    UINT4               u4Status = NAT_FAILURE;
    tIPSecHeaderInfo    IPSecHeaderInfo;

    NAT_TRC (NAT_TRC_ON, "\n Entering natProcessIPSec \n");

    natInitIPSecHeaderInfo (pbuf, pHeaderInfo, &IPSecHeaderInfo);
    if (pHeaderInfo->u4Direction == NAT_OUTBOUND)
    {
        u4Status = natProcessIPSecOutboundPkt (pbuf, &IPSecHeaderInfo);
    }
    else
    {
        u4Status = natProcessIPSecInboundPkt (pbuf, &IPSecHeaderInfo);
    }
    if (u4Status == NAT_SUCCESS)
    {
        pHeaderInfo->u4InIpAddr = IPSecHeaderInfo.u4InIpAddr;
        NatIpHeaderModify (pbuf, pHeaderInfo);
        NAT_TRC (NAT_TRC_ON, "\n Packet translated\n");
        NAT_TRC (NAT_TRC_ON, "\n Exiting natProcessIPSec \n");
        return NAT_SUCCESS;
    }
    else
    {
        NAT_TRC (NAT_TRC_ON, "\n Packet translation failed\n");
        NAT_TRC (NAT_TRC_ON, "\n Exiting natProcessIPSec \n");
        return NAT_FAILURE;
    }
}

/*****************************************************************************/
/*  Function Name   : natInitIPSecHeaderInfo                                 */
/*  Description     : This function gets the SPI information from the packet */
/*                    and IP information from the pHeaderInfo and stores     */
/*                    them in the data structure pointed by pIPSecHeaderInfo.*/
/*  Input(s)        :  pBuf - Pointer to the IPSec Packet                    */
/*                     pHeaderInfo - This contains the header information    */
/*                     pIpSecHeaderInfo -  Contains IPSec header information */
/*  Output(s)       :   pIPSecheaderInfo - Modified IPSec  header Info       */
/*  Global Variables Referred :  None                                        */
/*  Global variables Modified :  None                                        */
/*  Exceptions or Operating System Error Handling :    None                  */
/*  Use of Recursion :  None                                                 */
/*  Returns          :  None                                                 */
/*****************************************************************************/
VOID
natInitIPSecHeaderInfo (tCRU_BUF_CHAIN_HEADER * pbuf, tHeaderInfo * pHeaderInfo,
                        tIPSecHeaderInfo * pIPSecHeaderInfo)
{
    /*UT_FIX-START */
    /*NAT_UT_IPSEC_FUNC_FLT_8-START */
    UINT4               u4SPI = NAT_ZERO;
    /*NAT_UT_IPSEC_FUNC_FLT_8-END */
    /*UT_FIX-END */
    /*
       Fill the following fields InsideIpAddress, OutsideIpAddress, Direction and
       Interface number of the pIPSecHeaderInfo structure from the pHeaderInfo 
       structure Fill the SPI of pIPSecHeaderInfo structure from the data buffer 
       pbuf
     */
    NAT_TRC (NAT_TRC_ON, "\n Entering natInitIPSecHeaderInfo\n");

    pIPSecHeaderInfo->u4InIpAddr = pHeaderInfo->u4InIpAddr;
    pIPSecHeaderInfo->u4OutIpAddr = pHeaderInfo->u4OutIpAddr;
    pIPSecHeaderInfo->u4IfNum = pHeaderInfo->u4IfNum;
    pIPSecHeaderInfo->u4Direction = pHeaderInfo->u4Direction;
    /*UT_FIX-START */
    /*NAT_UT_IPSEC_FUNC_FLT_8-START */
    CRU_BUF_Copy_FromBufChain (pbuf, (UINT1 *) &u4SPI,
                               pHeaderInfo->u1IpHeadLen, NAT_IPSEC_SPI_LENGTH);
    pIPSecHeaderInfo->u4SPI = OSIX_NTOHL (u4SPI);
    NAT_TRC3 (NAT_TRC_ON, "\n"
              "Inside IP address : %x\n    Outside IP address : %x"
              "SPI : %x \n", pIPSecHeaderInfo->u4InIpAddr,
              pIPSecHeaderInfo->u4OutIpAddr, pIPSecHeaderInfo->u4SPI);
    /*NAT_UT_IPSEC_FUNC_FLT_8-END */
    /*UT_FIX-END */
    NAT_TRC (NAT_TRC_ON, "\n Exiting natInitIPSecHeaderInfo \n");
}

/*****************************************************************************/
/*  Function Name   : natProcessIPSecOutboundPkt                             */
/*  Description     : This function processes the IPSec ESP Outbound packet  */
/*  Input(s)        :  pBuf - Pointer to the IPSec Packet                    */
/*                     pHeaderInfo - This contains the header information    */
/*                     pIpSecHeaderInfo -  Contains IPSec header information */
/*  Output(s)       :   pIPSecheaderInfo - Modified  packet                  */
/*  Global Variables Referred :  gpNatIPSecOutHashList                       */
/*  Global variables Modified :  pIPSecOutHashNode                           */
/*  Exceptions or Operating System Error Handling :    None                  */
/*  Use of Recursion :  None                                                 */
/*  Returns          :  NAT_SUCCESS / NAT_FAILURE                            */
/*****************************************************************************/
UINT4
natProcessIPSecOutboundPkt (tCRU_BUF_CHAIN_HEADER * pbuf,
                            tIPSecHeaderInfo * pIPSecHeaderInfo)
{
    UINT4               u4Status = NAT_FAILURE;
    UINT4               u4HashKey = NAT_ZERO;
    UINT4               u4CurrTime = NAT_ZERO;
    tIPSecOutHashNode  *pIPSecOutHashNode = NULL;

    NAT_TRC (NAT_TRC_ON, "\n Entering natProcessIPSecOutboundPkt  \n");

    /* Form IPSec Outbound Hash key */
    u4HashKey = natFormIPSecHashKey (pIPSecHeaderInfo->u4InIpAddr,
                                     pIPSecHeaderInfo->u4OutIpAddr,
                                     NAT_OUTBOUND);
    /*Scan till the node end and check the following */
    TMO_HASH_Scan_Bucket (gpNatIPSecOutHashList, u4HashKey, pIPSecOutHashNode,
                          tIPSecOutHashNode *)
    {

        /* Match the SPI, Outside IP and Inside IP in the IPSec Header Info with  
           SPI Inside,Outside IP and Inside IP of IPSec Outbound Hash Node 
           respectively */
        if (((pIPSecHeaderInfo->u4SPI == pIPSecOutHashNode->u4SPIInside) &&
             (pIPSecHeaderInfo->u4OutIpAddr == pIPSecOutHashNode->u4OutIpAddr)
             && (pIPSecHeaderInfo->u4InIpAddr ==
                 pIPSecOutHashNode->u4LocIpAddr)) == NAT_SUCCESS)
        {
            /*
               Update the  Inside IP of  IPSec Header Info with the translated IP from
               IPSec Outbound Hash Node
               Get the current time and Update the time stamp value of
               IPSec Outbound Hash Node
             */
            pIPSecHeaderInfo->u4InIpAddr =
                pIPSecOutHashNode->u4TranslatedIpAddr;
            NAT_GET_SYS_TIME (&u4CurrTime);
            pIPSecOutHashNode->u4TimeStamp = u4CurrTime;
            /*UT_FIX-START */
            /*NAT_UT_IPSEC_MISC_FLT_6-START */
            NAT_TRC5 (NAT_TRC_ON,
                      "Inside IP : %x\n    Translated IP : %x"
                      "Outside IP : %x\n    SPI : %x\n    Time stamp : %d\n",
                      pIPSecOutHashNode->u4LocIpAddr,
                      pIPSecHeaderInfo->u4InIpAddr,
                      pIPSecHeaderInfo->u4OutIpAddr, pIPSecHeaderInfo->u4SPI,
                      pIPSecOutHashNode->u4TimeStamp);
            NAT_TRC (NAT_TRC_ON, "\n Exiting natProcessIPSecOutboundPkt  \n");
            /*NAT_UT_IPSEC_MISC_FLT_6-END */
            /*UT_FIX-END */
            return NAT_SUCCESS;
        }
    }                            /* End of Scan */
    NAT_FL_UTIL_UPDATE_FLOWTYPE_FLOWDATA (pbuf);
    u4Status = natSearchAndCreateIPSecOutboundPkt (pIPSecHeaderInfo);
    NAT_TRC (NAT_TRC_ON, "\n Exiting natProcessIPSecOutboundPkt  \n");
    return u4Status;

}

/*****************************************************************************/
/*  Function Name   : natProcessIPSecInboundPkt                              */
/*  Description     : This function processes the IPSec ESP Inbound packet   */
/*  Input(s)        :  pIpSecHeaderInfo -  Contains IPSec header information */
/*  Output(s)       :   pIPSecheaderInfo - Modified  packet                  */
/*  Global Variables Referred :  gpNatIPSecInHashList                        */
/*  Global variables Modified :  None                                        */
/*  Exceptions or Operating System Error Handling :    None                  */
/*  Use of Recursion :  None                                                 */
/*  Returns          :  NAT_SUCCESS / NAT_FAILURE                            */
/*****************************************************************************/
UINT4
natProcessIPSecInboundPkt (tCRU_BUF_CHAIN_HEADER * pbuf,
                           tIPSecHeaderInfo * pIPSecHeaderInfo)
{
    UINT4               u4Status = NAT_FAILURE;
    UINT4               u4HashKey = NAT_ZERO;
    tIPSecInHashNode   *pIPSecInHashNode = NULL;

    NAT_TRC (NAT_TRC_ON, "\n Entering natProcessIPSecInboundPkt  \n");

    u4HashKey = natFormIPSecHashKey (pIPSecHeaderInfo->u4OutIpAddr,
                                     pIPSecHeaderInfo->u4SPI, NAT_INBOUND);
    /*Scan till the node end and check the following) */
    TMO_HASH_Scan_Bucket (gpNatIPSecInHashList, u4HashKey, pIPSecInHashNode,
                          tIPSecInHashNode *)
    {

        /* Match the SPI and Outside IP in the IPSec Header Info with SPI 
         * Outside and Outside IP of IPSec Inbound Hash Node respectively */
        if (((pIPSecHeaderInfo->u4SPI == pIPSecInHashNode->u4SPIOutside) &&
             (pIPSecHeaderInfo->u4OutIpAddr == pIPSecInHashNode->u4OutIpAddr))
            == NAT_SUCCESS)
        {
            /*
               Update the Inside IP of IPSec Header Info  with the Local IP from
               IPSec Inbound Hash Node
             */
            pIPSecHeaderInfo->u4InIpAddr = pIPSecInHashNode->u4LocIpAddr;
            /*UT_FIX-START */
            /*NAT_UT_IPSEC_MISC_FLT_7-START */
            NAT_TRC3 (NAT_TRC_ON,
                      "Local IP : %x\n    Outside IP : %x"
                      "SPI : %x  \n", pIPSecHeaderInfo->u4InIpAddr,
                      pIPSecHeaderInfo->u4OutIpAddr, pIPSecHeaderInfo->u4SPI);
            NAT_TRC (NAT_TRC_ON, "\n Exiting natProcessIPSecInboundPkt  \n");
            /*NAT_UT_IPSEC_MISC_FLT_7-END */
            /*UT_FIX-END */
            return NAT_SUCCESS;
        }
    }                            /* End of the scan */
    NAT_FL_UTIL_UPDATE_FLOWTYPE_FLOWDATA (pbuf);
    u4Status = natSearchAndCreateIPSecInboundPkt (pIPSecHeaderInfo);

    NAT_TRC (NAT_TRC_ON, "\n Exiting natProcessIPSecInboundPkt  \n");
    return u4Status;

}

/*****************************************************************************/
/*  Function Name   : natFormIPSecHashKey                                    */
/*  Description     : This function forms the hash key for the IPSec hash    */
/*                     tables depending on the type of Hash table            */
/*  Input(s)        :  u4value1 -This represents Local IP when Outbound,     */
/*                     Outside IP  when Inbound or pending                   */
/*                     u4value2 - This represents Outside IP when Outbound,  */
/*                     SPI outside when Inbound and NULL when pending.       */
/*                     u4type -The u4Type represent whether the Hash Table is*/
/*                     Outbound, Inbound or Pending                          */
/*  Output(s)       :   None                                                 */
/*  Global Variables Referred :  None                                        */
/*  Global variables Modified :  None                                        */
/*  Exceptions or Operating System Error Handling :    None                  */
/*  Use of Recursion :  None                                                 */
/*  Returns          :  u4HashKey                                            */
/*****************************************************************************/
UINT4
natFormIPSecHashKey (UINT4 u4value1, UINT4 u4value2, UINT4 u4type)
{
    UINT4               u4HashKey = NAT_ZERO;

    NAT_TRC (NAT_TRC_ON, "\n Entering natFormIPSecHashKey \n");

    switch (u4type)
    {

        case NAT_OUTBOUND:
        case NAT_INBOUND:
            /* mask  least 5 bits */
            u4value1 = u4value1 & NAT_ENCH_FIVE_BIT_MASK;
            /* mask  least 5 bits */
            u4value2 = u4value2 & NAT_ENCH_FIVE_BIT_MASK;
            /* shift the bits towards left */
            u4value1 = u4value1 << NAT_FIVE;
            u4HashKey = u4value1 | u4value2;
            u4HashKey = u4HashKey & NAT_ENCH_HASH_MASK;
            break;
        case NAT_PENDING:
            u4HashKey = u4value1 & NAT_ENCH_HASH_MASK;
            break;
        default:
            break;
    }
    NAT_TRC1 (NAT_TRC_ON, "\n Hash key:%d \n", u4HashKey);
    NAT_TRC (NAT_TRC_ON, "\n Exiting natFormIPSecHashKey \n");
    return u4HashKey;

}

/*****************************************************************************/
/*  Function Name   : natSearchAndCreateIPSecOutboundPkt                     */
/*  Description     : This function searches pending hash table to move entry*/
/*                    from the pending hash table to Outbound and Inbound    */
/*                    hash table or create a new entry in the pending hash   */
/*                    table when the IPSec ESP packet is Outbound.           */
/*  Input(s)        :  pIPSecHeaderInfo- IPSec header Information data struct*/
/*  Output(s)       :   pIPSecheaderInfo - modified                          */
/*  Global Variables Referred :  gpNatIPSecPendHashList                      */
/*                               gi4NatIPSecMaxRetry                         */
/*  Global variables Modified :  gpNatIPSecPendHashList                      */
/*  Exceptions or Operating System Error Handling :    None                  */
/*  Use of Recursion :  None                                                 */
/*  Returns          :  NAT_SUCCESS / NAT_FAILURE                            */
/*****************************************************************************/
UINT4
natSearchAndCreateIPSecOutboundPkt (tIPSecHeaderInfo * pIPSecHeaderInfo)
{

    UINT4               u4Status = NAT_FAILURE;
    UINT4               u4HashKey = NAT_ZERO;
    UINT4               u4CurrTime = NAT_ZERO;
    tIPSecPendHashNode *pIPSecPendHashNode = NULL;

    NAT_TRC (NAT_TRC_ON, "\n Entering natSearchAndCreateIPSecOutboundPkt \n");

    /* Form Pending Hash Key */
    u4HashKey = natFormIPSecHashKey (pIPSecHeaderInfo->u4OutIpAddr, NAT_ZERO,
                                     NAT_PENDING);
    /*Scan till the node end and check the following */
    TMO_HASH_Scan_Bucket (gpNatIPSecPendHashList, u4HashKey, pIPSecPendHashNode,
                          tIPSecPendHashNode *)
    {
        if (pIPSecPendHashNode->u4OutIpAddr == pIPSecHeaderInfo->u4OutIpAddr)
        {
            if ((pIPSecPendHashNode->u4SPIOutside != NAT_ZERO) &&
                (pIPSecPendHashNode->u4LocIpAddr
                 == pIPSecHeaderInfo->u4InIpAddr))
            {
                u4Status =
                    natCreateIPSecNode (pIPSecHeaderInfo, pIPSecPendHashNode);
                NAT_TRC (NAT_TRC_ON,
                         "\n Exiting" "natSearchAndCreateIPSecOutboundPkt \n");
                return u4Status;
            }
            else if ((pIPSecPendHashNode->u4SPIInside != NAT_ZERO))
            {
                if (pIPSecPendHashNode->u4SPIInside != pIPSecHeaderInfo->u4SPI)
                {
                    NAT_TRC (NAT_TRC_ON, "\n Inside SPI do not Match \n");
                    NAT_TRC (NAT_TRC_ON, "\n Exiting"
                             "natSearchAndCreateIPSecOutboundPkt\n");
                    return NAT_FAILURE;
                }
                if (pIPSecPendHashNode->u4LocIpAddr !=
                    pIPSecHeaderInfo->u4InIpAddr)
                {
                    NAT_TRC (NAT_TRC_ON, "\n Local IP do not Match \n");
                    NAT_TRC (NAT_TRC_ON, "\n Exiting"
                             "natSearchAndCreateIPSecOutboundPkt \n");
                    return NAT_FAILURE;
                }
                if (pIPSecPendHashNode->u2NoOfRetry >
                    (UINT2) gi4NatIPSecMaxRetry)
                {
                    natDeleteIPSecPendingEntry (pIPSecHeaderInfo,
                                                pIPSecPendHashNode);
                    /*UT_FIX-START */
                    NAT_TRC (NAT_TRC_ON,
                             "Number of Retries exceeded Max count \n");
                    NAT_TRC (NAT_TRC_ON,
                             "Exiting natSearchAndCreateIPSecOutboundPkt \n");
                    /*UT_FIX-END */
                    return NAT_FAILURE;
                }
                pIPSecPendHashNode->u2NoOfRetry++;
                pIPSecHeaderInfo->u4InIpAddr =
                    pIPSecPendHashNode->u4TranslatedIpAddr;
                NAT_GET_SYS_TIME (&u4CurrTime);
                pIPSecPendHashNode->u4TimeStamp = u4CurrTime;
                /*UT_FIX-START */
                NAT_TRC (NAT_TRC_ON, "Entry found in Pending Hash Table\n");
                NAT_TRC5 (NAT_TRC_ON, "\n"
                          "Inside IP : %x\n              Translated IP : %x"
                          "Outside IP : %x\n              Inside SPI : %x"
                          "Time Stamp : %d\n",
                          pIPSecHeaderInfo->u4InIpAddr,
                          pIPSecPendHashNode->u4LocIpAddr,
                          pIPSecHeaderInfo->u4OutIpAddr,
                          pIPSecHeaderInfo->u4SPI,
                          pIPSecPendHashNode->u4TimeStamp);
                NAT_TRC (NAT_TRC_ON,
                         "Exiting natSearchAndCreateIPSecOutboundPkt \n");
                /*UT_FIX-END */
                return NAT_SUCCESS;
            }                    /* End of the Inside SPI check */
        }                        /* End of Outside IP match */
    }                            /* End of the Scan */
    u4Status = natCreateIPSecPendNodeOutboundPkt (pIPSecHeaderInfo, u4HashKey);
    NAT_TRC (NAT_TRC_ON, "\n Exiting natSearchAndCreateIPSecOutboundPkt \n");
    return u4Status;
}

/*****************************************************************************/
/*  Function Name   : natCreateIPSecNode                                     */
/*  Description     : This function moves the entry from the pending hash    */
/*                    table to the Inbound and the Outbound hash table.      */
/*  Input(s)        :  pIPSecHeaderInfo- IPSec header Information data struct*/
/*                      pIPSecPendHashNode - IPSec Pend Hash Node data struct*/
/*  Output(s)       :   pIPSecheaderInfo - modified                          */
/*  Global Variables Referred :  gpNatIPSecInHashList                        */
/*                               gpNatIPSecOutHashList                       */
/*                               gNatIPSecListNode                           */
/*                               NAT_IPSEC_IN_HASH_POOL_ID                   */
/*                               NAT_IPSEC_OUT_HASH_POOL_ID                  */
/*                               NAT_IPSEC_LIST_POOL_ID                      */
/*  Global variables Modified :  gpNatIPSecInHashList                        */
/*                               gpNatIPSecOutHashList                       */
/*                               gNatIPSecListNode                           */
/*  Exceptions or Operating System Error Handling :    None                  */
/*  Use of Recursion :  None                                                 */
/*  Returns          :  NAT_SUCCESS / NAT_FAILURE                            */
/*****************************************************************************/
UINT4
natCreateIPSecNode (tIPSecHeaderInfo * pIPSecHeaderInfo,
                    tIPSecPendHashNode * pIPSecPendHashNode)
{

    UINT4               u4HashKey = NAT_ZERO;
    tIPSecInHashNode   *pIPSecInboundHashNode = NULL;
    tIPSecOutHashNode  *pIPSecOutboundHashNode = NULL;
    tIPSecListNode     *pIPSecListNode = NULL;

    NAT_TRC (NAT_TRC_ON, "\n Entering natCreateIPSecNode \n");

    /*Allocate memory for the IPSec Inbound Hash Node */
    if (NAT_MEM_ALLOCATE (NAT_IPSEC_IN_HASH_POOL_ID,
                          pIPSecInboundHashNode, tIPSecInHashNode) == NULL)
    {
        NAT_TRC (NAT_TRC_ON, "\n Memory Allocation Failed for Inbound"
                 "Hash Node\n");
        NAT_TRC (NAT_TRC_ON, "\n Exiting natCreateIPSecNode \n");
        return NAT_FAILURE;
    }
    /*Allocate memory for the IPSec Outbound Hash Node */
    if (NAT_MEM_ALLOCATE (NAT_IPSEC_OUT_HASH_POOL_ID,
                          pIPSecOutboundHashNode, tIPSecOutHashNode) == NULL)
    {
        /* Release the memory allocated for the Inbound Hash Node */
        NatMemReleaseMemBlock (NAT_IPSEC_IN_HASH_POOL_ID,
                               (UINT1 *) pIPSecInboundHashNode);
        NAT_TRC (NAT_TRC_ON, "\n Memory Allocation Failed for Outbound"
                 "Hash Node\n");
        NAT_TRC (NAT_TRC_ON, "\n Exiting natCreateIPSecNode \n");
        return NAT_FAILURE;
    }
    /* Allocate memory for the IPSec Session List Node */
    if (NAT_MEM_ALLOCATE (NAT_IPSEC_LIST_POOL_ID,
                          pIPSecListNode, tIPSecListNode) == NULL)
    {
        /*Release the memory allocated for the Inbound and Outbound Hash Node
         */
        NatMemReleaseMemBlock (NAT_IPSEC_IN_HASH_POOL_ID,
                               (UINT1 *) pIPSecInboundHashNode);
        NatMemReleaseMemBlock (NAT_IPSEC_OUT_HASH_POOL_ID,
                               (UINT1 *) pIPSecOutboundHashNode);
        NAT_TRC (NAT_TRC_ON, "\n Memory Allocation Failed for Session"
                 "List Node  \n");
        NAT_TRC (NAT_TRC_ON, "\n Exiting natCreateIPSecNode \n");
        return NAT_FAILURE;
    }
    /*
       Fill Inbound and the Outbound Hash Nodes with the information from
       the IPSec Pending Hash Node and IPSec Header Info
       Fill the time stamp for the Outbound Hash Node
       Fill the Inbound hash node pointer in the Outbound Hash Node */
    natFillIPSecInAndOutHashNode (pIPSecHeaderInfo, pIPSecInboundHashNode,
                                  pIPSecOutboundHashNode, pIPSecPendHashNode);

    /* Form Inbound Hash key and Add to the Inbound Hash table */
    /*UT_FIX -START */
    /*NAT_UT_IPSEC_FUNC_FLT_3 -START */
    u4HashKey = natFormIPSecHashKey (pIPSecInboundHashNode->u4OutIpAddr,
                                     pIPSecInboundHashNode->u4SPIOutside,
                                     NAT_INBOUND);
    /*NAT_UT_IPSEC_FUNC_FLT_3 -END */
    /*UT_FIX -END */
    TMO_HASH_Add_Node (gpNatIPSecInHashList,
                       &(pIPSecInboundHashNode->IPSecInHash), u4HashKey, NULL);

    /* Form Outbound Hash key and Add to the Outbound Hash table */
    /*UT_FIX -START */
    /*NAT_UT_IPSEC_FUNC_FLT_4 -START */
    u4HashKey = natFormIPSecHashKey (pIPSecOutboundHashNode->u4LocIpAddr,
                                     pIPSecOutboundHashNode->u4OutIpAddr,
                                     NAT_OUTBOUND);
    /*NAT_UT_IPSEC_FUNC_FLT_4 -END */
    /*UT_FIX -END */
    TMO_HASH_Add_Node (gpNatIPSecOutHashList,
                       &(pIPSecOutboundHashNode->IPSecOutHash), u4HashKey,
                       NULL);

    /* Add the Outbound Hash Node in the IPSec Session List */
    pIPSecListNode->pIPSecOutHashNode = pIPSecOutboundHashNode;
    TMO_SLL_Add (&gNatIPSecListNode, &(pIPSecListNode->IPSecList));
    if (pIPSecHeaderInfo->u4Direction == NAT_OUTBOUND)
    {
        pIPSecHeaderInfo->u4InIpAddr = pIPSecPendHashNode->u4TranslatedIpAddr;
    }
    else
    {
        pIPSecHeaderInfo->u4InIpAddr = pIPSecPendHashNode->u4LocIpAddr;
    }
    /* Delete the Pending Node from the IPSec Pending List and Pending Hash
       table Node */
    gapNatIfTable[pIPSecHeaderInfo->u4IfNum]->u4NumOfActiveSessions++;
    NAT_TRC (NAT_TRC_ON, "\n NAT IPSec In and Outbound Hash"
             "Table Entry created \n");
    /*UT_FIX -START */
    NAT_TRC6 (NAT_TRC_ON, "\n"
              "Inside IP address : %x\n    Outside IP address : %x"
              "Translated IP address : %x\n    SPI : %x"
              "Direction : %d\n    Time stamp : %d \n",
              pIPSecPendHashNode->u4LocIpAddr,
              pIPSecHeaderInfo->u4OutIpAddr,
              pIPSecPendHashNode->u4TranslatedIpAddr,
              pIPSecHeaderInfo->u4SPI, pIPSecHeaderInfo->u4Direction,
              pIPSecOutboundHashNode->u4TimeStamp);
    natDeleteIPSecPendingEntry (pIPSecHeaderInfo, pIPSecPendHashNode);
    /*UT_FIX -END */
    NAT_TRC (NAT_TRC_ON, "\n Exiting natCreateIPSecNode \n");
    return NAT_SUCCESS;

}

/*****************************************************************************/
/*  Function Name   : natFillIPSecInAndOutHashNode                           */
/*  Description     : This function fills the Inbound and Outbound Hash Node */
/*                    when the packet is Inbound or Outbound.                */
/*  Input(s)        :  pIPSecHeaderInfo- IPSec header Information data struct*/
/*                      pIPSecOutHashNode - IPSec Out Hash Node data struct  */
/*                      pIPSecInHashNode - IPSec In Hash Node data struct    */
/*  Output(s)       :   None                                                 */
/*  Global Variables Referred :  None                                        */
/*  Global variables Modified :  None                                        */
/*  Exceptions or Operating System Error Handling :    None                  */
/*  Use of Recursion :  None                                                 */
/*  Returns          :  None                                                 */
/*****************************************************************************/
VOID
natFillIPSecInAndOutHashNode (tIPSecHeaderInfo * pIPSecHeaderInfo,
                              tIPSecInHashNode * pIPSecInboundHashNode,
                              tIPSecOutHashNode * pIPSecOutboundHashNode,
                              tIPSecPendHashNode * pIPSecPendHashNode)
{
    UINT4               u4CurrTime = NAT_ZERO;

    NAT_TRC (NAT_TRC_ON, "\n Entering natFillIPSecInAndOutHashNode \n");

    pIPSecInboundHashNode->u4LocIpAddr = pIPSecPendHashNode->u4LocIpAddr;
    pIPSecInboundHashNode->u4OutIpAddr = pIPSecPendHashNode->u4OutIpAddr;
    pIPSecInboundHashNode->u4TranslatedIpAddr =
        pIPSecPendHashNode->u4TranslatedIpAddr;
    if (pIPSecHeaderInfo->u4Direction == NAT_OUTBOUND)
    {
        pIPSecInboundHashNode->u4SPIOutside = pIPSecPendHashNode->u4SPIOutside;
        pIPSecOutboundHashNode->u4SPIInside = pIPSecHeaderInfo->u4SPI;
    }
    else
    {
        pIPSecInboundHashNode->u4SPIOutside = pIPSecHeaderInfo->u4SPI;
        pIPSecOutboundHashNode->u4SPIInside = pIPSecPendHashNode->u4SPIInside;
    }
    pIPSecOutboundHashNode->u4LocIpAddr = pIPSecPendHashNode->u4LocIpAddr;
    pIPSecOutboundHashNode->u4OutIpAddr = pIPSecPendHashNode->u4OutIpAddr;
    pIPSecOutboundHashNode->u4TranslatedIpAddr =
        pIPSecPendHashNode->u4TranslatedIpAddr;
    pIPSecOutboundHashNode->u4IfNum = pIPSecPendHashNode->u4IfNum;
    /*UT_FIX -START */
    /*NAT_UT_IPSEC_FUNC_FLT_13 -START */
    NAT_GET_SYS_TIME (&u4CurrTime);
    pIPSecOutboundHashNode->u4TimeStamp = u4CurrTime;
    /*NAT_UT_IPSEC_FUNC_FLT_13 -END */
    /*UT_FIX -END */
    pIPSecOutboundHashNode->pIPSecInHashNode = pIPSecInboundHashNode;
    NAT_TRC (NAT_TRC_ON, "\n Exiting natFillIPSecInAndOutHashNode \n");
}

/*****************************************************************************/
/*  Function Name   : natDeleteIPSecPendingEntry                             */
/*  Description     : This function deletes the pending entry from the IPSec */
/*                    Pending List and Pending Hash table                    */
/*  Input(s)        :  pIPSecHeaderInfo- IPSec header Information data struct*/
/*                      pIPSecPendHashNode - IPSec Pend Hash Node data struct*/
/*  Output(s)       :   None                                                 */
/*  Global Variables Referred :  gpNatIPSecPendHashList                      */
/*                               NAT_IPSEC_PEND_HASH_POOL_ID                 */
/*                               NAT_IPSEC_PEND_LIST_POOL_ID                 */
/*                               gNatIPSecPendListNode                       */
/*  Global Variables Modified :  gpNatIPSecPendHashList                      */
/*                               gNatIPSecPendListNode                       */
/*  Exceptions or Operating System Error Handling :    None                  */
/*  Use of Recursion :  None                                                 */
/*  Returns          :  None                                                 */
/*****************************************************************************/
VOID
natDeleteIPSecPendingEntry (tIPSecHeaderInfo * pIPSecHeaderInfo,
                            tIPSecPendHashNode * pIPSecPendHashNode)
{
    UINT4               u4HashKey = NAT_ZERO;
    tIPSecPendListNode *pDeleteNode = NULL;
    tIPSecPendListNode *pNextNode = NULL;

    NAT_TRC (NAT_TRC_ON, "\n Entering natDeleteIPSecPendingEntry \n");

    /* Form Hash key for the Pending Hash Table  */
    u4HashKey = natFormIPSecHashKey (pIPSecHeaderInfo->u4OutIpAddr, NAT_ZERO,
                                     NAT_PENDING);
    /* Delete the Hash Node Entry from the Pending Hash Table */
    TMO_HASH_Delete_Node (gpNatIPSecPendHashList,
                          &(pIPSecPendHashNode->IPSecPendHash), u4HashKey);
    /* Release the Hash Node memory */
    NatMemReleaseMemBlock (NAT_IPSEC_PEND_HASH_POOL_ID,
                           (UINT1 *) pIPSecPendHashNode);

    /* Delete Pending Node from the pending List and Release the memory */
    pDeleteNode = (tIPSecPendListNode *) TMO_SLL_First (&gNatIPSecPendListNode);

    while (pDeleteNode != NULL)
    {

        /* Get Node when the Pending Hash Node matches in the Pending List */
        if (pDeleteNode->pIPSecPendHashNode == pIPSecPendHashNode)
        {

            /* Get the Next Node from the Pending List */
            pNextNode =
                (tIPSecPendListNode *) TMO_SLL_Next (&gNatIPSecPendListNode,
                                                     &(pDeleteNode->
                                                       IPSecPendList));

            /* Delete the Current Node from the Pending List */
            TMO_SLL_Delete (&gNatIPSecPendListNode,
                            &(pDeleteNode->IPSecPendList));

            /* Release the Current Node memory */
            NatMemReleaseMemBlock (NAT_IPSEC_PEND_LIST_POOL_ID,
                                   (UINT1 *) pDeleteNode);
            /* Assign the Next node as the current Node */
            pDeleteNode = pNextNode;
            break;
        }
        else
        {
            pDeleteNode =
                (tIPSecPendListNode *) TMO_SLL_Next (&gNatIPSecPendListNode,
                                                     &(pDeleteNode->
                                                       IPSecPendList));
        }
    }                            /* End of while */

    NAT_TRC (NAT_TRC_ON, "\n Exiting natDeleteIPSecPendingEntry \n");
}

/*****************************************************************************/
/*  Function Name   : natCreateIPSecPendNodeOutboundPkt                      */
/*  Description     : This function creates an IPSec pending hash node in the*/
/*                    IPSec pending hash table when the packet is outbound   */
/*  Input(s)        :  pIPSecHeaderInfo- IPSec header Information data struct*/
/*                     u4HashKey - Hash Key Information                      */
/*  Output(s)       :   None                                                 */
/*  Global variables Referred :  gpNatIPSecPendHashList                      */
/*                               gNatIPSecPendListNode                       */
/*                               NAT_IPSEC_PEND_HASH_POOL_ID                 */
/*                               NAT_IPSEC_PEND_LIST_POOL_ID                 */
/*  Global Variables Modified :  gpNatIPSecPendHashList                      */
/*                               gNatIPSecPendListNode                       */
/*  Exceptions or Operating System Error Handling :    None                  */
/*  Use of Recursion :  None                                                 */
/*  Returns          :  None                                                 */
/*****************************************************************************/
UINT4
natCreateIPSecPendNodeOutboundPkt (tIPSecHeaderInfo * pIPSecHeaderInfo,
                                   UINT4 u4HashKey)
{
    tGlobalInfo         GlobalInfo;
    tIPSecPendHashNode *pPendHashNode = NULL;
    tIPSecPendListNode *pPendListNode = NULL;

    MEMSET (&GlobalInfo, NAT_ZERO, sizeof (tGlobalInfo));
    NAT_TRC (NAT_TRC_ON, "\n Entering natCreateIPSecPendNodeOutboundPkt \n");

    /* Get the Free Global IP address for translation */
    NatGetNextFreeGlobalIpPort (pIPSecHeaderInfo->u4InIpAddr,
                                NAT_ZERO,
                                pIPSecHeaderInfo->u4OutIpAddr,
                                NAT_ZERO,
                                pIPSecHeaderInfo->u4IfNum, &GlobalInfo);
    if ((GlobalInfo.u4TranslatedLocIpAddr == NAT_ZERO)
        || (GlobalInfo.u2TranslatedLocPort != NAT_ZERO))
    {
        NAT_TRC (NAT_TRC_ON,
                 "\n No Free Global IP Address or port is translated"
                 "and the packet is dropped  \n");
        NAT_TRC (NAT_TRC_ON, "\n Exiting natCreateIPSecPendNodeOutboundPkt\n");
        return NAT_FAILURE;
    }
    /* Allocate memory for the Pending Hash Node */
    if (NAT_MEM_ALLOCATE (NAT_IPSEC_PEND_HASH_POOL_ID,
                          pPendHashNode, tIPSecPendHashNode) == NULL)
    {
        NAT_TRC (NAT_TRC_ON, "\n Memory Allocation Failed for Pending"
                 "Hash Node \n");
        NAT_TRC (NAT_TRC_ON, "\n Exiting natCreateIPSecPendNodeOutboundPkt \n");
        return NAT_FAILURE;
    }
    /* Allocate memory for the pending List Node */
    if (NAT_MEM_ALLOCATE (NAT_IPSEC_PEND_LIST_POOL_ID,
                          pPendListNode, tIPSecPendListNode) == NULL)
    {

        /* Release memory of the Pending Hash Node  */
        NatMemReleaseMemBlock (NAT_IPSEC_PEND_HASH_POOL_ID,
                               (UINT1 *) pPendHashNode);
        NAT_TRC (NAT_TRC_ON, "\n Memory Allocation Failed for Pending"
                 "List Node\n");
        NAT_TRC (NAT_TRC_ON, "\n Exiting natCreateIPSecPendNodeOutboundPkt\n");
        return NAT_FAILURE;
    }
    natFillIPSecPendingNode (pPendHashNode, pIPSecHeaderInfo,
                             GlobalInfo.u4TranslatedLocIpAddr);

    /* Add the hash node information in the pending list */
    TMO_HASH_Add_Node (gpNatIPSecPendHashList, &(pPendHashNode->IPSecPendHash),
                       u4HashKey, NULL);

    /* Assign the Pending Hash Node to the pending List Node */
    pPendListNode->pIPSecPendHashNode = pPendHashNode;

    /*Add the hash List Node to the pending List */
    TMO_SLL_Add (&gNatIPSecPendListNode, &(pPendListNode->IPSecPendList));
    pIPSecHeaderInfo->u4InIpAddr = pPendHashNode->u4TranslatedIpAddr;

    /* Check whether Timer is started or not , if not do it now */
    if (gu4NatTmrEnable == NAT_DISABLE)
    {
        gu4NatTmrEnable = NAT_ENABLE;
        /* START TIMER */
        NatStartTimer ();
        NAT_TRC (NAT_TRC_ON, "\n NAT Timer Started \n");
    }
    NAT_TRC (NAT_TRC_ON, "\n NAT IPSec Pending  Entry created \n");
    /*UT_FIX -START */
    /* NAT_UT_IPSEC_MISC_FLT_1 -START */
    NAT_TRC5 (NAT_TRC_ON, "\n"
              "Inside IP address : %x\n    Translated IP address : %x"
              "Outside IP address : %x\n    SPI : %x"
              "Time stamp : %d\n",
              pPendHashNode->u4LocIpAddr, pIPSecHeaderInfo->u4InIpAddr,
              pIPSecHeaderInfo->u4OutIpAddr, pIPSecHeaderInfo->u4SPI,
              pPendHashNode->u4TimeStamp);
    /* NAT_UT_IPSEC_MISC_FLT_1 -END */
    /*UT_FIX -END */
    NAT_TRC (NAT_TRC_ON, "\n Exiting natCreateIPSecPendNodeOutboundPkt \n");
    return NAT_SUCCESS;

}

/*****************************************************************************/
/*  Function Name   : natFillIPSecPendingNode                                */
/*  Description     : This function fills the pending node when the packet is*/
/*                    Inbound or Outbound                                    */
/*  Input(s)        :  pIPSecHeaderInfo- IPSec header Information data struct*/
/*                     pIPSecPendHashNode - IPSec Pend Hash Node Data Struct */
/*                     u4IpAddr - Translated IP address (Outbound) or        */
/*                     Local address (Inbound)                               */
/*  Output(s)       :   pIPSecPendHashNode                                   */
/*  Global variables Referred :  None                                        */
/*  Global Variables Modified :  None                                        */
/*  Exceptions or Operating System Error Handling :    None                  */
/*  Use of Recursion :  None                                                 */
/*  Returns          :  None                                                 */
/*****************************************************************************/
VOID
natFillIPSecPendingNode (tIPSecPendHashNode * pPendHashNode, tIPSecHeaderInfo
                         * pIPSecHeaderInfo, UINT4 u4IpAddr)
{
    UINT4               u4CurrTime = NAT_ZERO;

    NAT_TRC (NAT_TRC_ON, "\n Entering natFillIPSecPendingNode \n");

    /*
       Fill the Pending Hash Node with information from the pIPSecHeaderInfo
       Translated IP or Local IP
       Fill the current time stamp.
     */
    pPendHashNode->u4OutIpAddr = pIPSecHeaderInfo->u4OutIpAddr;
    if (pIPSecHeaderInfo->u4Direction == NAT_OUTBOUND)
    {
        pPendHashNode->u4TranslatedIpAddr = u4IpAddr;
        pPendHashNode->u4LocIpAddr = pIPSecHeaderInfo->u4InIpAddr;
        pPendHashNode->u4SPIInside = pIPSecHeaderInfo->u4SPI;
        pPendHashNode->u4SPIOutside = NAT_ZERO;
    }
    else
    {
        pPendHashNode->u4TranslatedIpAddr = pIPSecHeaderInfo->u4InIpAddr;
        pPendHashNode->u4LocIpAddr = u4IpAddr;
        pPendHashNode->u4SPIInside = NAT_ZERO;
        pPendHashNode->u4SPIOutside = pIPSecHeaderInfo->u4SPI;
    }
    pPendHashNode->u2NoOfRetry = NAT_ONE;
    pPendHashNode->u4IfNum = pIPSecHeaderInfo->u4IfNum;
    /*UT_FIX -START */
    /*NAT_UT_IPSEC_FUNC_FLT_12 -START */
    NAT_GET_SYS_TIME (&u4CurrTime);
    pPendHashNode->u4TimeStamp = u4CurrTime;
    /*NAT_UT_IPSEC_FUNC_FLT_12 -END */
    /*UT_FIX -END */
    NAT_TRC (NAT_TRC_ON, "\n Exiting natFillIPSecPendingNode \n");
}

/*****************************************************************************/
/*  Function Name   : natSearchAndCreateIPSecInboundPkt                      */
/*  Description     : This function searches pending hash table to move entry*/
/*                    from the pending hash table to Outbound and Inbound    */
/*                    hash table or create a new entry in the pending hash   */
/*                     table when the IPSec ESP packet is Inbound.           */
/*  Input(s)        :  pIPSecHeaderInfo- IPSec header Information data struct*/
/*  Output(s)       :   pIPSecHeaderInfo - modified                          */
/*  Global variables Referred :  gpNatIPSecPendHashList                      */
/*  Global Variables Modified :  pIPSecPendHashNode                          */
/*  Exceptions or Operating System Error Handling :    None                  */
/*  Use of Recursion :  None                                                 */
/*  Returns          :  NAT_SUCCESS / NAT_FAILURE                            */
/*****************************************************************************/
UINT4
natSearchAndCreateIPSecInboundPkt (tIPSecHeaderInfo * pIPSecHeaderInfo)
{
    UINT4               u4HashKey = NAT_ZERO;
    UINT4               u4Status = NAT_FAILURE;
    UINT4               u4CurrTime = NAT_ZERO;
    tIPSecPendHashNode *pIPSecPendHashNode = NULL;

    NAT_TRC (NAT_TRC_ON, "\n Entering natSearchAndCreateIPSecInboundPkt\n");

    /* Form Pending Hash Key */
    u4HashKey = natFormIPSecHashKey (pIPSecHeaderInfo->u4OutIpAddr, NAT_ZERO,
                                     NAT_PENDING);
    /*Scan till the node end and check the following */
    TMO_HASH_Scan_Bucket (gpNatIPSecPendHashList, u4HashKey, pIPSecPendHashNode,
                          tIPSecPendHashNode *)
    {

        if (pIPSecPendHashNode->u4OutIpAddr == pIPSecHeaderInfo->u4OutIpAddr)
        {
            if (pIPSecPendHashNode->u4SPIInside != NAT_ZERO)
            {
                u4Status = natCreateIPSecNode (pIPSecHeaderInfo,
                                               pIPSecPendHashNode);
                NAT_TRC (NAT_TRC_ON, "\n Exiting"
                         "natSearchAndCreateIPSecInboundPkt \n");
                return u4Status;
            }
            if (pIPSecPendHashNode->u4SPIOutside != pIPSecHeaderInfo->u4SPI)
            {
                NAT_TRC (NAT_TRC_ON, "\n SPI do not match \n");
                NAT_TRC (NAT_TRC_ON, "\n Exiting"
                         "natSearchAndCreateIPSecInboundPkt \n");
                return NAT_FAILURE;
            }
            if (pIPSecPendHashNode->u2NoOfRetry > (UINT2) gi4NatIPSecMaxRetry)
            {
                natDeleteIPSecPendingEntry (pIPSecHeaderInfo,
                                            pIPSecPendHashNode);
                /*UT_FIX -START */
                NAT_TRC (NAT_TRC_ON, "Number of Retries exceeded Max count \n");
                NAT_TRC (NAT_TRC_ON,
                         "Exiting natSearchAndCreateIPSecInboundPkt \n");
                /*UT_FIX -END */
                return NAT_FAILURE;
            }
            pIPSecPendHashNode->u2NoOfRetry++;
            NAT_GET_SYS_TIME (&u4CurrTime);
            pIPSecPendHashNode->u4TimeStamp = u4CurrTime;
            pIPSecHeaderInfo->u4InIpAddr = pIPSecPendHashNode->u4LocIpAddr;
            /*UT_FIX -START */
            NAT_TRC (NAT_TRC_ON, "Entry found in Pending Hash Table\n");
            NAT_TRC4 (NAT_TRC_ON, "\n"
                      "Local IP : %x\n             Outside IP : %x"
                      "Inside SPI : %x\n             Time Stamp : %d\n",
                      pIPSecHeaderInfo->u4InIpAddr,
                      pIPSecHeaderInfo->u4OutIpAddr,
                      pIPSecHeaderInfo->u4SPI, pIPSecPendHashNode->u4TimeStamp);
            NAT_TRC (NAT_TRC_ON,
                     "Exiting natSearchAndCreateIPSecInboundPkt \n");
            /*UT_FIX -END */
            return NAT_SUCCESS;
        }                        /*End of the Outside IP check */
    }                            /*End of scan */
    u4Status = natCreateIPSecPendNodeInboundPkt (pIPSecHeaderInfo, u4HashKey);

    NAT_TRC (NAT_TRC_ON, "\n Exiting natSearchAndCreateIPSecInboundPkt  \n");
    return u4Status;
}

/*****************************************************************************/
/*  Function Name   : natCreateIPSecPendNodeInboundPkt                       */
/*  Description     : This function creates an IPSec pending hash node in the*/
/*                    IPSec pending hash table when the packet is Inbound.   */
/*  Input(s)        :  pIPSecHeaderInfo- IPSec header Information data struct*/
/*                     u4HashKey - Hash Key Information                      */
/*  Output(s)       :   None                                                 */
/*  Global variables Referred :  gpNatIPSecPendHashList                      */
/*                               gNatIPSecPendListNode                       */
/*                               NAT_IPSEC_PEND_HASH_POOL_ID                 */
/*                               NAT_IPSEC_PEND_LIST_POOL_ID                 */
/*  Global Variables Modified :  gpNatIPSecPendHashList                      */
/*                               gNatIPSecPendListNode                       */
/*  Exceptions or Operating System Error Handling :    None                  */
/*  Use of Recursion :  None                                                 */
/*  Returns          :  NAT_SUCCESS / NAT_FAILURE                            */
/*****************************************************************************/
UINT4
natCreateIPSecPendNodeInboundPkt (tIPSecHeaderInfo * pIPSecHeaderInfo,
                                  UINT4 u4HashKey)
{
    UINT4               u4InIpAddr = NAT_ZERO;
    UINT2               u2InPort = NAT_ZERO;
    tIPSecPendListNode *pPendListNode = NULL;
    tIPSecPendHashNode *pPendHashNode = NULL;

    NAT_TRC (NAT_TRC_ON, "\n Entering natCreateIPSecPendNodeInboundPkt \n");

    /* Check whether TWO way NAT is enabled for the interface */
    if (gapNatIfTable[pIPSecHeaderInfo->u4IfNum]->u1TwoWayNatEnable
        == NAT_ENABLE)
    {
        /* Get the Local IP address */
        u4InIpAddr = pIPSecHeaderInfo->u4InIpAddr;

        if (NAT_FAILURE == NatPolicyGetStaticEntry
            (&u4InIpAddr,
             pIPSecHeaderInfo->u4InIpAddr,
             pIPSecHeaderInfo->u4OutIpAddr, NAT_PROTO_ANY, NAT_INBOUND))
        {

            if (NatGetStaticNaptEntry (&u4InIpAddr, &u2InPort,
                                       pIPSecHeaderInfo->u4Direction,
                                       pIPSecHeaderInfo->u4IfNum,
                                       NAT_PROTO_ANY) == NULL)
            {
                u4InIpAddr = NatSearchStaticTable (pIPSecHeaderInfo->u4InIpAddr,
                                                   pIPSecHeaderInfo->
                                                   u4Direction,
                                                   pIPSecHeaderInfo->u4IfNum);
                if (u4InIpAddr == NAT_ZERO)
                {
                    NAT_TRC (NAT_TRC_ON, "\n No Local IP address \n");
                    NAT_TRC (NAT_TRC_ON, "\n Exiting"
                             "natCreateIPSecPendNodeInboundPkt\n");
                    return NAT_FAILURE;
                }                /* End of Static table search */
            }
            else
            {
                if (u2InPort != NAT_ZERO)
                {
                    NAT_TRC (NAT_TRC_ON, "\n Port Translated \n");
                    NAT_TRC (NAT_TRC_ON, "\n Exiting"
                             "natCreateIPSecPendNodeInboundPkt \n");
                    return NAT_FAILURE;
                }
            }                    /*End of Static NAPT Search */
        }
        else
        {
            if (u4InIpAddr == NAT_ZERO)
            {
                NAT_TRC (NAT_TRC_ON, "\n No Local IP address \n");
                NAT_TRC (NAT_TRC_ON, "\n Exiting"
                         "natCreateIPSecPendNodeInboundPkt\n");
                return NAT_FAILURE;
            }                    /* End of Static policy table search */

        }
    }
    else
    {
        NAT_TRC (NAT_TRC_ON, "\n Two way NAT disabled \n");
        NAT_TRC (NAT_TRC_ON, "\n Exiting"
                 "natCreateIPSecPendNodeInboundPkt \n");
        return NAT_FAILURE;
    }                            /* End of Two-Way NAT check */

    /* Allocate memory for the Pending Hash Node  */
    if (NAT_MEM_ALLOCATE (NAT_IPSEC_PEND_HASH_POOL_ID,
                          pPendHashNode, tIPSecPendHashNode) == NULL)
    {
        NAT_TRC (NAT_TRC_ON, "\n Memory Allocation Failed for "
                 "Pending Hash Node  \n");
        NAT_TRC (NAT_TRC_ON, "\n Exiting natCreateIPSecPendNodeInboundPkt\n");
        return NAT_FAILURE;
    }
    /* Allocate memory for the pending List Node */
    if (NAT_MEM_ALLOCATE (NAT_IPSEC_PEND_LIST_POOL_ID,
                          pPendListNode, tIPSecPendListNode) == NULL)
    {

        /* Release memory of the Pending Hash Node */
        NatMemReleaseMemBlock (NAT_IPSEC_PEND_HASH_POOL_ID,
                               (UINT1 *) pPendHashNode);
        NAT_TRC (NAT_TRC_ON,
                 "\n Memory Allocation Failed for Pending" "List Node\n");
        NAT_TRC (NAT_TRC_ON, "\n Exiting natCreateIPSecPendNodeInboundPkt \n");
        return NAT_FAILURE;
    }
    natFillIPSecPendingNode (pPendHashNode, pIPSecHeaderInfo, u4InIpAddr);

    /* Add the hash node information in the pending list */
    TMO_HASH_Add_Node (gpNatIPSecPendHashList,
                       &(pPendHashNode->IPSecPendHash), u4HashKey, NULL);

    /* Assign the Pending Hash Node to the pending List Node */
    pPendListNode->pIPSecPendHashNode = pPendHashNode;

    /*Add the hash List Node to the pending List */
    TMO_SLL_Add (&gNatIPSecPendListNode, &(pPendListNode->IPSecPendList));
    pIPSecHeaderInfo->u4InIpAddr = pPendHashNode->u4LocIpAddr;

    /* Check whether Timer is started or not , if not do it now */
    if (gu4NatTmrEnable == NAT_DISABLE)
    {
        gu4NatTmrEnable = NAT_ENABLE;
        /* START TIMER */
        NatStartTimer ();
        NAT_TRC (NAT_TRC_ON, "\n NAT Timer Started \n");
    }
    NAT_TRC (NAT_TRC_ON, "\n NAT IPSec Pending  Entry created \n");
    /*UT_FIX -START */
    /*NAT_UT_IPSEC_MISC_FLT_2 -START */
    NAT_TRC5 (NAT_TRC_ON, "\n"
              "Translated IP address : %x\n    Local IP address : %x"
              "Outside IP address : %x\n    SPI : %x"
              "Time stamp : %d \n",
              pPendHashNode->u4TranslatedIpAddr,
              pIPSecHeaderInfo->u4InIpAddr, pIPSecHeaderInfo->u4OutIpAddr,
              pIPSecHeaderInfo->u4SPI, pPendHashNode->u4TimeStamp);
    /*NAT_UT_IPSEC_MISC_FLT_2 -END */
    /*UT_FIX -END */
    NAT_TRC (NAT_TRC_ON, "\n Exiting natCreateIPSecPendNodeInboundPkt \n");
    return NAT_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : natDeleteIPSecEntries                                  */
/*  Description     : This function deletes IPSec sessions from the IPSec    */
/*                    Session List, IPSec Pending List table, Inbound and    */
/*                    Outbound Hash Tables when the time stampis more than   */
/*                    the timeout values.                                    */
/*  Input(s)        :  None                                                  */
/*  Output(s)       :   None                                                 */
/*  Global variables Referred :  gNatIPSecListNode                           */
/*                               gNatIPSecPendListNode                       */
/*                               NAT_IPSEC_LIST_POOL_ID                      */
/*                               NAT_IPSEC_LIST_POOL_ID                      */
/*  Global Variables Modified :  gNatIPSecListNode                           */
/*                               gNatIPSecPendListNode                       */
/*  Exceptions or Operating System Error Handling :    None                  */
/*  Use of Recursion :  None                                                 */
/*  Returns          :  None                                                 */
/*****************************************************************************/
VOID
natDeleteIPSecEntries (VOID)
{
    UINT4               u4CurrTime = NAT_ZERO;

    tIPSecListNode     *pDeleteNode = NULL;
    tIPSecListNode     *pNextNode = NULL;
    tIPSecPendListNode *pDeletePendNode = NULL;
    tIPSecPendListNode *pNextPendNode = NULL;

    NAT_TRC (NAT_TRC_ON, "\n Entering natDeleteIPSecEntries \n");

    /*Get the First Node of the IPSec session List */
    pDeleteNode = (tIPSecListNode *) TMO_SLL_First (&gNatIPSecListNode);
    while (pDeleteNode != NULL)
    {
#ifdef FLOWMGR_WANTED
        /* Update the timestamp using the entries in the hardware */
        if ((NAT_FL_UTIL_GET_IPSEC_TIMESTAMP
             (pDeleteNode->pIPSecOutHashNode->u4LocIpAddr,
              pDeleteNode->pIPSecOutHashNode->u4OutIpAddr,
              pDeleteNode->pIPSecOutHashNode->u4SPIInside,
              NAT_IPSEC_ESP) == OSIX_SUCCESS)
            ||
            (NAT_FL_UTIL_GET_IPSEC_TIMESTAMP
             (pDeleteNode->pIPSecOutHashNode->u4OutIpAddr,
              pDeleteNode->pIPSecOutHashNode->u4TranslatedIpAddr,
              pDeleteNode->pIPSecOutHashNode->pIPSecInHashNode->u4SPIOutside,
              NAT_IPSEC_ESP) == OSIX_SUCCESS))
        {
            NAT_GET_SYS_TIME (&pDeleteNode->pIPSecOutHashNode->u4TimeStamp);
        }
#endif /* FLOWMGR_WANTED */
        NAT_GET_SYS_TIME (&u4CurrTime);
        /* Current time  - Session timestamp >= IPSec timeout value */
        if (u4CurrTime - pDeleteNode->pIPSecOutHashNode->u4TimeStamp >=
            (UINT4) gi4NatIPSecTimeOut)
        {
            /*
               Get the next node in the IPSec List
               Delete the Hash Node from the IPSec In and Outbound Hash Table
               Delete the Node from the IPSec List
               Release the memory of the IPSec List Node
               Assign the next node of the IPSec List as the current Node
               Increment number of closed sessions for the interface
               Decrement number of active sessions for the interface
             */
            /*UT_FIX -START */
            /*NAT_UT_IPSEC_FUNC_FLT_9 -START */
            gapNatIfTable[pDeleteNode->pIPSecOutHashNode->u4IfNum]->
                u4NumOfSessionsClosed++;
            gapNatIfTable[pDeleteNode->pIPSecOutHashNode->u4IfNum]->
                u4NumOfActiveSessions--;
            /*NAT_UT_IPSEC_FUNC_FLT_9 -END */
            /*UT_FIX -END */
            pNextNode = (tIPSecListNode *) TMO_SLL_Next (&gNatIPSecListNode,
                                                         &(pDeleteNode->
                                                           IPSecList));
            natDeleteIPSecInOutHashNode (pDeleteNode->pIPSecOutHashNode);

            TMO_SLL_Delete (&gNatIPSecListNode, &(pDeleteNode->IPSecList));
            NatMemReleaseMemBlock (NAT_IPSEC_LIST_POOL_ID,
                                   (UINT1 *) pDeleteNode);
            pDeleteNode = pNextNode;
        }
        else
        {
            pDeleteNode = (tIPSecListNode *) TMO_SLL_Next (&gNatIPSecListNode,
                                                           &(pDeleteNode->
                                                             IPSecList));
        }
    }                            /* End of while */

    /* Get the First Node of the IPSec Pending List  */
    pDeletePendNode = (tIPSecPendListNode *)
        TMO_SLL_First (&gNatIPSecPendListNode);
    while (pDeletePendNode != NULL)
    {
        /* Current time  - Session timestamp greater than IPSec Pending 
           timeout value */
        if (u4CurrTime - pDeletePendNode->pIPSecPendHashNode->u4TimeStamp >=
            (UINT4) gi4NatIPSecPendingTimeOut)
        {

            /*
               Get the next node in the IPSec Pending List
               Delete the Hash Node from the IPSec Pending Hash Table
               Delete the Node from the IPSec Pending List
               Release the memory of the IPSec Pending List Node
               Assign the next node of the IPSec Pending List as the current Node
             */
            pNextPendNode = (tIPSecPendListNode *) TMO_SLL_Next
                (&gNatIPSecPendListNode, &(pDeletePendNode->IPSecPendList));
            natDeleteIPSecPendHashNode (pDeletePendNode->pIPSecPendHashNode);
            TMO_SLL_Delete (&gNatIPSecPendListNode,
                            &(pDeletePendNode->IPSecPendList));
            NatMemReleaseMemBlock (NAT_IPSEC_PEND_LIST_POOL_ID, (UINT1 *)
                                   pDeletePendNode);
            pDeletePendNode = pNextPendNode;
        }
        else
        {
            pDeletePendNode = (tIPSecPendListNode *)
                TMO_SLL_Next (&gNatIPSecPendListNode,
                              &(pDeletePendNode->IPSecPendList));
        }
    }                            /* End of while */

    NAT_TRC (NAT_TRC_ON, "\n Exiting natDeleteIPSecEntries \n");
}

/*****************************************************************************/
/*  Function Name   : natDeleteIPSecInOutHashNode                            */
/*  Description     : This function deletes the IPSec Entry from the Inbound */
/*                    and Outbound Hash table                                */
/*                    Outbound Hash Tables when the time stampis more than   */
/*                    the timeout values.                                    */
/*  Input(s)        :  pIPSecOutHashNode - Outbound Hash Node to be deleted  */
/*  Output(s)       :   None                                                 */
/*  Global variables Referred :  gpNatIPSecInHashList                        */
/*                               gpNatIPSecOutHashList                       */
/*                               NAT_IPSEC_OUT_HASH_POOL_ID                  */
/*                               NAT_IPSEC_IN_HASH_POOL_ID                   */
/*  Global Variables Modified :  gpNatIPSecInHashList                        */
/*                               gpNatIPSecOutHashList                       */
/*  Exceptions or Operating System Error Handling :    None                  */
/*  Use of Recursion :  None                                                 */
/*  Returns          :  None                                                 */
/*****************************************************************************/
VOID
natDeleteIPSecInOutHashNode (tIPSecOutHashNode * pIPSecOutHashNode)
{
    UINT4               u4InKey = NAT_ZERO;
    UINT4               u4OutKey = NAT_ZERO;

    NAT_TRC (NAT_TRC_ON, "\n Entering natDeleteIPSecInOutHashNode \n");

    /* Delete both the IPSEC entries from hardware */
    NAT_FL_UTIL_DEL_IPSEC_HW_ENTRIES (pIPSecOutHashNode->u4LocIpAddr,
                                      pIPSecOutHashNode->u4OutIpAddr,
                                      pIPSecOutHashNode->u4SPIInside,
                                      NAT_IPSEC_ESP);

    NAT_FL_UTIL_DEL_IPSEC_HW_ENTRIES (pIPSecOutHashNode->u4OutIpAddr,
                                      pIPSecOutHashNode->u4TranslatedIpAddr,
                                      pIPSecOutHashNode->pIPSecInHashNode->
                                      u4SPIOutside, NAT_IPSEC_ESP);

    /* Form the hash key for the Inbound hash Table */
    u4InKey = natFormIPSecHashKey (pIPSecOutHashNode->pIPSecInHashNode->
                                   u4OutIpAddr,
                                   pIPSecOutHashNode->pIPSecInHashNode->
                                   u4SPIOutside, NAT_INBOUND);
    /* Form the hash key for the Outbound hash Table */
    u4OutKey = natFormIPSecHashKey (pIPSecOutHashNode->u4LocIpAddr,
                                    pIPSecOutHashNode->u4OutIpAddr,
                                    NAT_OUTBOUND);
    /* Delete the Inbound Hash Node */
    TMO_HASH_Delete_Node (gpNatIPSecInHashList,
                          &(pIPSecOutHashNode->IPSecOutHash), u4InKey);

    /* Release the memory */
    NatMemReleaseMemBlock (NAT_IPSEC_IN_HASH_POOL_ID,
                           (UINT1 *) pIPSecOutHashNode->pIPSecInHashNode);
    /* Delete the Outbound Hash Node */
    TMO_HASH_Delete_Node (gpNatIPSecOutHashList,
                          &(pIPSecOutHashNode->IPSecOutHash), u4OutKey);

    /* Release the memory */
    NatMemReleaseMemBlock (NAT_IPSEC_OUT_HASH_POOL_ID,
                           (UINT1 *) pIPSecOutHashNode);

    NAT_TRC (NAT_TRC_ON, "\n Exiting natDeleteIPSecInOutHashNode \n");
}

/*****************************************************************************/
/*  Function Name   : natDeleteIPSecPendHashNode                             */
/*  Description     : This function deletes IPSec Pending hash Node.         */
/*  Input(s)        :  pIPSecPendHashNode- IPSec Pend Hash Node to be deleted*/
/*  Output(s)       :   None                                                 */
/*  Global variables Referred :  gpNatIPSecPendHashList                      */
/*                               NAT_IPSEC_PEND_HASH_POOL_ID                 */
/*  Global Variables Modified :  gpNatIPSecPendHashList                      */
/*                               NAT_IPSEC_PEND_HASH_POOL_ID                 */
/*  Exceptions or Operating System Error Handling :    None                  */
/*  Use of Recursion :  None                                                 */
/*  Returns          :  None                                                 */
/*****************************************************************************/
VOID
natDeleteIPSecPendHashNode (tIPSecPendHashNode * pIPSecPendHashNode)
{
    UINT4               u4HashKey = NAT_ZERO;

    NAT_TRC (NAT_TRC_ON, "\n Entering natDeleteIPSecPendHashNode \n");

    /* Form the hash key for the pending hash Table */
    u4HashKey = natFormIPSecHashKey (pIPSecPendHashNode->u4OutIpAddr, NAT_ZERO,
                                     NAT_PENDING);
    /* Delete the Pending Hash Node */
    TMO_HASH_Delete_Node (gpNatIPSecPendHashList,
                          &(pIPSecPendHashNode->IPSecPendHash), u4HashKey);

    /* Release the memory */
    NatMemReleaseMemBlock (NAT_IPSEC_PEND_HASH_POOL_ID, (UINT1 *)
                           pIPSecPendHashNode);

    NAT_TRC (NAT_TRC_ON, "\n Exiting natDeleteIPSecPendHashNode \n");
}

/*****************************************************************************/
/*  Function Name   : natDeleteIPSecPendingRow                               */
/*  Description     : This function deletes IPSec Pending hash Node.         */
/*                    called by nmhSetNatIPSecPendingEntryStatus             */
/*  Input(s)        :  i4NatIPSecPendingInterfaceNum                         */
/*                     u4NatIPSecPendingLocalIp                              */
/*                     u4NatIPSecPendingOutsideIp                            */
/*                     u4NatIPSecPendingSPIInside                            */
/*                     u4NatIPSecPendingSPIOutside                           */
/*  Output(s)       :   None                                                 */
/*  Global variables Referred :  gNatIPSecPendListNode                       */
/*                               NAT_IPSEC_PEND_LIST_POOL_ID                 */
/*  Global Variables Modified :  gNatIPSecPendListNode                       */
/*                               NAT_IPSEC_PEND_LIST_POOL_ID                 */
/*  Exceptions or Operating System Error Handling :    None                  */
/*  Use of Recursion :  None                                                 */
/*  Returns          :  NAT_SUCCESS / NAT_FAILURE                            */
/*****************************************************************************/
UINT4
natDeleteIPSecPendingRow (INT4 i4NatIPSecPendingInterfaceNum,
                          UINT4 u4NatIPSecPendingLocalIp,
                          UINT4 u4NatIPSecPendingOutsideIp,
                          UINT4 u4NatIPSecPendingSPIInside,
                          UINT4 u4NatIPSecPendingSPIOutside)
{
    tIPSecPendListNode *pIPSecPendListNode = NULL;

    NAT_TRC (NAT_TRC_ON, "\n Entering natDeleteIPSecPendingRow \n");

    TMO_SLL_Scan (&gNatIPSecPendListNode, pIPSecPendListNode,
                  tIPSecPendListNode *)
    {
        if ((pIPSecPendListNode->pIPSecPendHashNode->u4IfNum
             == (UINT4) i4NatIPSecPendingInterfaceNum) &&
            (pIPSecPendListNode->pIPSecPendHashNode->u4LocIpAddr
             == u4NatIPSecPendingLocalIp) &&
            (pIPSecPendListNode->pIPSecPendHashNode->u4OutIpAddr
             == u4NatIPSecPendingOutsideIp) &&
            (pIPSecPendListNode->pIPSecPendHashNode->u4SPIInside
             == u4NatIPSecPendingSPIInside) &&
            (pIPSecPendListNode->pIPSecPendHashNode->u4SPIOutside
             == u4NatIPSecPendingSPIOutside))
        {
            natDeleteIPSecPendHashNode (pIPSecPendListNode->pIPSecPendHashNode);
            TMO_SLL_Delete (&gNatIPSecPendListNode,
                            &(pIPSecPendListNode->IPSecPendList));
            NatMemReleaseMemBlock (NAT_IPSEC_PEND_LIST_POOL_ID,
                                   (UINT1 *) pIPSecPendListNode);
            NAT_TRC (NAT_TRC_ON, "\n Exiting natDeleteIPSecPendingRow \n");
            return SNMP_SUCCESS;
        }
    }

    NAT_TRC (NAT_TRC_ON, "\n Exiting natDeleteIPSecPendingRow \n");
    return SNMP_FAILURE;
}

/*****************************************************************************/
/*  Function Name   : natDeleteIPSecSessionRow                               */
/*  Description     : This function deletes IPSec Session hash Node.         */
/*                    called by nmhSetNatIPSecSessionEntryStatus             */
/*  Input(s)        :  i4NatIPSecSessionInterfaceNum                  */
/*                     u4NatIPSecSessionLocalIp                              */
/*                     u4NatIPSecSessionOutsideIp                            */
/*                     u4NatIPSecSessionSPIInside                            */
/*                     u4NatIPSecSessionSPIOutside                           */
/*  Output(s)       :   None                                                 */
/*  Global variables Referred :  gNatIPSecListNode                           */
/*                               NAT_IPSEC_LIST_POOL_ID                      */
/*  Global Variables Modified :  gNatIPSecListNode                           */
/*                               NAT_IPSEC_LIST_POOL_ID                      */
/*  Exceptions or Operating System Error Handling :    None                  */
/*  Use of Recursion :  None                                                 */
/*  Returns          :  NAT_SUCCESS / NAT_FAILURE                            */
/*****************************************************************************/
UINT4
natDeleteIPSecSessionRow (INT4 i4NatIPSecSessionInterfaceNum,
                          UINT4 u4NatIPSecSessionLocalIp,
                          UINT4 u4NatIPSecSessionOutsideIp,
                          UINT4 u4NatIPSecSessionSPIInside,
                          UINT4 u4NatIPSecSessionSPIOutside)
{
    tIPSecListNode     *pNatIPSecListNode = NULL;
    NAT_TRC (NAT_TRC_ON, "\n Entering natDeleteIPSecSessionRow \n");

    TMO_SLL_Scan (&gNatIPSecListNode, pNatIPSecListNode, tIPSecListNode *)
    {
        if ((pNatIPSecListNode->pIPSecOutHashNode->u4IfNum
             == (UINT4) i4NatIPSecSessionInterfaceNum) &&
            (pNatIPSecListNode->pIPSecOutHashNode->u4LocIpAddr
             == u4NatIPSecSessionLocalIp) &&
            (pNatIPSecListNode->pIPSecOutHashNode->u4OutIpAddr
             == u4NatIPSecSessionOutsideIp) &&
            (pNatIPSecListNode->pIPSecOutHashNode->u4SPIInside
             == u4NatIPSecSessionSPIInside) &&
            (pNatIPSecListNode->pIPSecOutHashNode->pIPSecInHashNode->
             u4SPIOutside == u4NatIPSecSessionSPIOutside))
        {
            natDeleteIPSecInOutHashNode (pNatIPSecListNode->pIPSecOutHashNode);
            TMO_SLL_Delete (&gNatIPSecListNode,
                            &(pNatIPSecListNode->IPSecList));
            NatMemReleaseMemBlock (NAT_IPSEC_LIST_POOL_ID,
                                   (UINT1 *) pNatIPSecListNode);
            NAT_TRC (NAT_TRC_ON, "\n Exiting natDeleteIPSecSessionRow \n");
            return SNMP_SUCCESS;
        }
    }
    NAT_TRC (NAT_TRC_ON, "\n Exiting natDeleteIPSecSessionRow \n");
    return SNMP_FAILURE;
}

#endif /* NAT_IPSEC_C */
