/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: natike.c,v 1.13 2015/07/16 10:50:36 siva Exp $
 *
 *  DESCRIPTION           :    This file contains functions for parsing      
 *                            the IKE packets and modifying the IKE payload. 
 *
 *******************************************************************/

/*****************************************************************************/
/*  FILE NAME             :   natike.c                                       */
/*  PRINCIPAL AUTHOR      :   FutureSoft                                     */
/*  SUBSYSTEM NAME        :    LR                                            */
/*  MODULE NAME           :    NAT                                           */
/*  LANGUAGE              :    C                                             */
/*  TARGET ENVIRONMENT    :   Linux                                          */
/*  DATE OF FIRST RELEASE :   dd/mm/yy                                       */
/*  AUTHOR                :   FutureSoft                                     */
/*  DESCRIPTION           :    This file contains functions for parsing      */
/*                            the IKE packets and modifying the IKE payload. */
/*****************************************************************************/
/*  Date(DD/MM/YYYY)      :      14/08/03                                    */
/*  Modified by           :      FutureSoft                                  */
/*  Description of change :      addressed the code review comments          */
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Version               :      1.2                                         */
/*  Date(DD/MM/YYYY)      :      03/09/03                                    */
/*  Modified by           :      FutureSoft                                  */
/*  Description of change :      code after unit test changes                */
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Version               :      1.3                                         */
/*  Date(DD/MM/YYYY)      :      20/09/03                                    */
/*  Modified by           :      FutureSoft                                  */
/*  Description of change :      changes for RFC 6330 and revision history   */
/*                               updation                                    */
/*****************************************************************************/
#ifndef _NATIKE_C
#define _NATIKE_C
#include "natinc.h"
tTMO_SLL            gNatIKEList;    /* Pointer to IKE session List */
tTMO_HASH_TABLE    *gpNatIKEHashList;    /* Pointer to IKE Hash Table */

/*****************************************************************************/
/*  Function Name   : natProcessIKE                                          */
/*  Description     : NAT is maintaining a separate table for IKE.This       */
/*                    function will create a separate dynamic entry in       */
/*                    the IKE table and maintains all the IKE entries        */
/*                    corresponding to different IKE sessions flowing        */
/*                    across the NAT router.                                 */
/*  Input(s)        :  pBuf - Received buffer                                */
/*                      pHeaderInfo - This contains the header information   */
/*  Output(s)       :   pBuf - Modified  packet                              */
/*  Global Variables Referred :  None                                        */
/*  Global variables Modified :  None                                        */
/*  Exceptions or Operating System Error Handling :    None                  */
/*  Use of Recursion :  None                                                 */
/*  Returns          :  NAT_SUCCESS / NAT_FAILURE                            */
/*****************************************************************************/
UINT4
natProcessIKE (tCRU_BUF_CHAIN_HEADER * pbuf, tHeaderInfo * pHeaderInfo)
{
    UINT4               u4Status = NAT_FAILURE;
    tIKEHeaderInfo      IKEHeaderInfo;
#ifndef LNXIP4_WANTED
    UINT4               u4TransStatus = NAT_FAILURE;
#endif /* !LNXIP4_WANTED */

    /*
     *The function  searches for the session entry in the IKE session
     * table.If the entry is not there then a new session is created.
     */
    NAT_TRC (NAT_TRC_ON, "\n Entering NatProcessIke \n");

    NAT_FL_UTIL_UPDATE_FLOWTYPE_FLOWDATA (pbuf);

    natInitIKEHeaderInfo (pbuf, pHeaderInfo, &IKEHeaderInfo);

    if (pHeaderInfo->u4Direction == NAT_OUTBOUND)
    {
        /* Process the Outbound Packet */
        /*printk ("\n %s,%s,%d \n",__FILE__,__FUNCTION__,__LINE__); */
        u4Status = natProcessIKEOutboundPkt (&IKEHeaderInfo);
    }
    else
    {
        /* Process the Inbound Packet */
        /* printk ("\n %s,%s,%d \n",__FILE__,__FUNCTION__,__LINE__); */
        u4Status = natProcessIKEInboundPkt (&IKEHeaderInfo);
    }

    if (u4Status == NAT_SUCCESS)
    {
        pHeaderInfo->u4InIpAddr = IKEHeaderInfo.u4InIpAddr;
        /* printk ("\n %s,%s,%d \n",__FILE__,__FUNCTION__,__LINE__); */
        NatIpHeaderModify (pbuf, pHeaderInfo);
        NAT_TRC (NAT_TRC_ON, "\n Packet translated\n");
        NAT_TRC (NAT_TRC_ON, "\n Exiting NatProcessIke \n");
        return NAT_SUCCESS;
    }
    else
    {
#ifndef LNXIP4_WANTED
        if (SecUtilIpIfIsOurAddress (pHeaderInfo->u4InIpAddr) == TRUE)
        {
            u4TransStatus = NatGetDynamicEntry (pHeaderInfo, NAT_SEARCH);
            if (u4TransStatus == NAT_SUCCESS)
            {
                /* If the packet is destined to us and dynamic entry exists,
                 * then the packet requires translation.So return NAT_FAILURE.*/
                return (NAT_FAILURE);
            }
            else
            {
                /* Since the packet is destined to us and the entry is not there in
                 * the translation table ,this packet doesn't require nat 
                 * translation. Hence return NAT_SUCCESS */
                return (NAT_SUCCESS);
            }
        }
        else
#endif /* !LNXIP4_WANTED */
        {
            /* Since the packet is not destined to us , allow the packet to get 
             * translated.Hence returning NAT_FAILURE.*/
            return NAT_FAILURE;
        }
    }
}

/*****************************************************************************/
/*  Function Name   : natInitIKEHeaderInfo                                   */
/*  Description     : This function gets IKE initiator cookie information    */
/*                    from the packet and  IP information from the         */
/*                    pHeaderInfo and stores them in the data structure      */
/*                    pointed by pIKEHeaderInfo.                             */
/*  Input(s)        :  pBuf - Received buffer                                */
/*                      pHeaderInfo - This contains the header information   */
/*                      pIKEHeaderInfo - pointer to IKE header information.  */
/*  Output(s)       : pIKEHeaderInfo - Filled IKE header Info data structure */
/*  Global Variables Referred :  None                                        */
/*  Global variables Modified :  None                                        */
/*  Exceptions or Operating System Error Handling :    None                  */
/*  Use of Recursion :  None                                                 */
/*  Returns          :  None                                                 */
/*****************************************************************************/
VOID
natInitIKEHeaderInfo (tCRU_BUF_CHAIN_HEADER * pbuf, tHeaderInfo * pHeaderInfo,
                      tIKEHeaderInfo * pIKEHeaderInfo)
{
    NAT_TRC (NAT_TRC_ON, "\n Entering NatInitIkeHeaderInfo  \n");

    /*
       Fill the InsideIpAddress, OutsideIpAddress, Direction
       and Interface number of pIKEHeaderInfo structure from the
       pHeaderInfo structure
       Fill the initiator cookie from  data buffer (pBuf)
     */
    pIKEHeaderInfo->u4InIpAddr = pHeaderInfo->u4InIpAddr;
    pIKEHeaderInfo->u4OutIpAddr = pHeaderInfo->u4OutIpAddr;
    pIKEHeaderInfo->u2InPort = pHeaderInfo->u2InPort;
    pIKEHeaderInfo->u2OutPort = pHeaderInfo->u2OutPort;
    pIKEHeaderInfo->u4IfNum = pHeaderInfo->u4IfNum;
    pIKEHeaderInfo->u4Direction = pHeaderInfo->u4Direction;
    /*UT_FIX-START */
    /* NAT_UT_IKE_FUNC_FLT_16 -START */
    CRU_BUF_Copy_FromBufChain (pbuf, pIKEHeaderInfo->au1InitCookie,
                               (UINT4) (pHeaderInfo->u1IpHeadLen +
                                        NAT_IKE_COOKIE_LENGTH),
                               NAT_IKE_COOKIE_LENGTH);
    /* NAT_UT_IKE_FUNC_FLT_16 -END */
    NAT_TRC2 (NAT_TRC_ON, "\n"
              "Inside IP : %x\n    Outside IP : %x\n",
              pIKEHeaderInfo->u4InIpAddr, pIKEHeaderInfo->u4OutIpAddr);
    /*UT_FIX-END */
    NAT_TRC (NAT_TRC_ON, "\n Exiting NatInitIkeHeaderInfo  \n");
}

/*****************************************************************************/
/*  Function Name   : natProcessIKEOutboundPkt                               */
/*  Description     : This function processes the IKE outbound packet.       */
/*  Input(s)        : pIKEHeaderInfo - pointer to IKE header information     */
/*  Output(s)       : pIKEHeaderInfo - Modified IKE header Info              */
/*                                                       data structure      */
/*  Global Variables Referred : gpNatIKEHashList                             */
/*  Global variables Modified :  pIKEHashNode                                */
/*  Exceptions or Operating System Error Handling :    None                  */
/*  Use of Recursion :  None                                                 */
/*  Returns          :  NAT_SUCCESS / NAT_FAILURE                            */
/*****************************************************************************/
UINT4
natProcessIKEOutboundPkt (tIKEHeaderInfo * pIKEHeaderInfo)
{
    UINT4               u4HashKey = NAT_ZERO;
    UINT4               u4CurrTime = NAT_ZERO;
    UINT4               u4Status = NAT_FAILURE;
    tIKEHashNode       *pIKEHashNode = NULL;

    NAT_TRC (NAT_TRC_ON, "\n Entering natProcessIKEOutboundPkt  \n");

    /* Form IKE Hash key */
    u4HashKey = natFormIKEHashKey (pIKEHeaderInfo->au1InitCookie);

    /* Scan till the node end and check the following */
    TMO_HASH_Scan_Bucket (gpNatIKEHashList, u4HashKey, pIKEHashNode,
                          tIKEHashNode *)
    {

        /* Match the Initiator cookie, Outside IP and Inside IP in the IKE Header
           Info with the IKE Hash Node */
        if ((MEMCMP (pIKEHeaderInfo->au1InitCookie, pIKEHashNode->au1InitCookie,
                     NAT_IKE_COOKIE_LENGTH) == NAT_ZERO)
            && (pIKEHeaderInfo->u4OutIpAddr == pIKEHashNode->u4OutIpAddr)
            && (pIKEHeaderInfo->u4InIpAddr == pIKEHashNode->u4LocIpAddr) ==
            NAT_SUCCESS)
        {
            /*
               Update the Inside IP of IKE Header Info  with the translated IP from
               IKE Hash Node
               Get the current time and Update the time stamp value of
               IKE Hash Node
             */
            pIKEHeaderInfo->u4InIpAddr = pIKEHashNode->u4TranslatedIpAddr;
            NAT_GET_SYS_TIME (&u4CurrTime);
            pIKEHashNode->u4TimeStamp = u4CurrTime;
            /*UT_FIX-START */
            NAT_TRC4 (NAT_TRC_ON, "\n"
                      "Inside IP : %x\n        Translated IP : %x "
                      "Outside IP : %x "
                      "Time stamp: %d \n", pIKEHashNode->u4LocIpAddr,
                      pIKEHeaderInfo->u4InIpAddr,
                      pIKEHeaderInfo->u4OutIpAddr, pIKEHashNode->u4TimeStamp);
            /*UT_FIX-END */
            NAT_TRC (NAT_TRC_ON, "\n Exiting natProcessIKEOutboundPkt  \n");
            return NAT_SUCCESS;
        }
    }
    u4Status = natCreateOutboundIKENode (pIKEHeaderInfo, u4HashKey);
    NAT_TRC (NAT_TRC_ON, "\n Exiting natProcessIKEOutboundPkt  \n");
    return u4Status;
}

/*****************************************************************************/
/*  Function Name   : natProcessIKEInboundPkt                                */
/*  Description     : This function processes the IKE Inbound packet.        */
/*  Input(s)        : pIKEHeaderInfo - pointer to IKE header information     */
/*  Output(s)       : pIKEHeaderInfo- Modified IKE header Info data structure*/
/*  Global Variables Referred : gpNatIKEHashList                             */
/*  Global variables Modified :  pIKEHashNode                                */
/*  Exceptions or Operating System Error Handling :    None                  */
/*  Use of Recursion :  None                                                 */
/*  Returns          :  NAT_SUCCESS / NAT_FAILURE                            */
/*****************************************************************************/
UINT4
natProcessIKEInboundPkt (tIKEHeaderInfo * pIKEHeaderInfo)
{
    UINT4               u4HashKey = NAT_ZERO;
    UINT4               u4CurrTime = NAT_ZERO;
    UINT4               u4Status = NAT_FAILURE;
    tIKEHashNode       *pIKEHashNode = NULL;

    NAT_TRC (NAT_TRC_ON, "\n Entering natProcessIKEInboundPkt \n");

    /* Form IKE Hash key */
    u4HashKey = natFormIKEHashKey (pIKEHeaderInfo->au1InitCookie);

    /*Scan till the node end and check the following */
    TMO_HASH_Scan_Bucket (gpNatIKEHashList, u4HashKey, pIKEHashNode,
                          tIKEHashNode *)
    {
        /* Match the Initiator cookie, Outside IP and Inside IP in the IKE Header Info
         * with the Initiator cookie, Outside IP and  Translated IP fields
         * of the IKE Hash Node respectively
         */
        if ((MEMCMP (pIKEHeaderInfo->au1InitCookie, pIKEHashNode->au1InitCookie,
                     NAT_IKE_COOKIE_LENGTH) == NAT_ZERO)
            && (pIKEHeaderInfo->u4OutIpAddr == pIKEHashNode->u4OutIpAddr)
            && (pIKEHeaderInfo->u4InIpAddr ==
                pIKEHashNode->u4TranslatedIpAddr) == NAT_SUCCESS)
        {
            /*
               Update the Inside IP of IKE Header Info  with the Local IP from
               IKE Hash Node
               Get the current time and Update the time stamp value of
               IKE Hash Node
             */
            pIKEHeaderInfo->u4InIpAddr = pIKEHashNode->u4LocIpAddr;
            NAT_GET_SYS_TIME (&u4CurrTime);
            pIKEHashNode->u4TimeStamp = u4CurrTime;
            /*UT_FIX-START */
            /*NAT_UT_IKE_MISC_FLT_15 -START */
            NAT_TRC4 (NAT_TRC_ON, "\n"
                      "Translated IP : %x\n        Local IP : %x"
                      "Outside IP : %x"
                      "Time stamp : %d \n",
                      pIKEHashNode->u4TranslatedIpAddr,
                      pIKEHeaderInfo->u4InIpAddr,
                      pIKEHeaderInfo->u4OutIpAddr, pIKEHashNode->u4TimeStamp);
            /*NAT_UT_IKE_MISC_FLT_15 -END */
            NAT_TRC (NAT_TRC_ON, "\n Exiting natProcessIKEInboundPkt  \n");
            return NAT_SUCCESS;
        }
    }
    u4Status = natCreateInboundIKENode (pIKEHeaderInfo, u4HashKey);
    NAT_TRC (NAT_TRC_ON, "\n Exiting natProcessIKEInboundPkt  \n");
    return u4Status;

}

/*****************************************************************************/
/*  Function Name   : natFormIKEHashKey                                      */
/*  Description     : This function forms the hash key for the IKE hash table*/
/*  Input(s)        : u1InitCookie - Initiator cookie of the session.        */
/*  Output(s)       : None                                                   */
/*  Global Variables Referred : None                                         */
/*  Global variables Modified :  None                                        */
/*  Exceptions or Operating System Error Handling :    None                  */
/*  Use of Recursion :  None                                                 */
/*  Returns          :  u4HashKey                                            */
/*****************************************************************************/
UINT4
natFormIKEHashKey (UINT1 *u1InitCookie)
{
    UINT4               u4HashKey = NAT_ZERO;
    NAT_TRC (NAT_TRC_ON, "\n Entering natFormIKEHashKey \n");

    /* Take the last 4 bytes and do 10 bit Mask operation */
    /* RFC -6330 -START */
    MEMCPY ((void *) &u4HashKey, u1InitCookie + NAT_FOUR, NAT_FOUR);
    u4HashKey = OSIX_HTONL (u4HashKey);
    u4HashKey = u4HashKey & NAT_ENCH_HASH_MASK;
    /* RFC -6330 -END */
    NAT_TRC1 (NAT_TRC_ON, "\n Hash key:%d \n", u4HashKey);
    NAT_TRC (NAT_TRC_ON, "\n Exiting natFormIKEHashKey  \n");
    return u4HashKey;
}

/*****************************************************************************/
/*  Function Name   : natCreateOutboundIKENode                               */
/*  Description     : This function creates an IKE hash node in IKE hash     */
/*                    tables using the pIKEHeaderInfo and hash key when the  */
/*                    packet is outbound                                     */
/*  Input(s)        : pIkeHeaderInfo - IKE header Information data structure */
/*  Output(s)       : None                                                   */
/*  Global Variables Referred : NAT_IKE_HASH_POOL_ID                         */
/*                              NAT_IKE_LIST_POOL_ID                         */
/*                              gpNatIKEHashList                             */
/*                              gu4NatTmrEnable                              */
/*                              gNatIKEList                                  */
/*  Global variables Modified :  gpNatIKEHashList                            */
/*                              gu4NatTmrEnable                              */
/*                              gNatIKEList                                  */
/*  Exceptions or Operating System Error Handling :    None                  */
/*  Use of Recursion :  None                                                 */
/*  Returns          :  NAT_SUCCESS / NAT_FAILURE                            */
/*****************************************************************************/
UINT4
natCreateOutboundIKENode (tIKEHeaderInfo * pIKEHeaderInfo, UINT4 u4HashKey)
{
    tGlobalInfo         GlobalInfo;
    tIKEHashNode       *pIKEHashNode = NULL;
    tIKEListNode       *pIKEList = NULL;

    MEMSET (&GlobalInfo, NAT_ZERO, sizeof (tGlobalInfo));
    NAT_TRC (NAT_TRC_ON, "\n Entering natCreateOutboundIKENode \n");

    NatGetNextFreeGlobalIpPort (pIKEHeaderInfo->u4InIpAddr,
                                pIKEHeaderInfo->u2InPort,
                                pIKEHeaderInfo->u4OutIpAddr,
                                NAT_ZERO, pIKEHeaderInfo->u4IfNum, &GlobalInfo);
    /*UT_FIX-START */
    /*NAT_UT_IKE_FUNC_FLT_10-START */
    if (((GlobalInfo.u2TranslatedLocPort != NAT_IKE_STD_PORT)||(GlobalInfo.u2TranslatedLocPort != NATT_IKE_STD_PORT)) &&
        (GlobalInfo.u2TranslatedLocPort != NAT_ZERO))
    {
        /* Give Message that IKE port translation is not enabled and drop 
           the Packet */
        NAT_TRC (NAT_TRC_ON, "\n IKE port translation is not enabled and"
                 "Packet is dropped  \n");
        NAT_TRC (NAT_TRC_ON, "\n Exiting natCreateOutboundIKENode  \n");
        return NAT_FAILURE;
    }
    /*NAT_UT_IKE_FUNC_FLT_10-END */
    /*UT_FIX-END */
    if (GlobalInfo.u4TranslatedLocIpAddr == NAT_ZERO)
    {
        /* Give Message that no free global IP address and drop the Packet */
        NAT_TRC (NAT_TRC_ON, "\n No Free Global IP Address and packet is"
                 "dropped  \n");
        NAT_TRC (NAT_TRC_ON, "\n Exiting natCreateOutboundIKENode  \n");
        return NAT_FAILURE;
    }
    /*
       Allocate memory for the IKE Hash Node
       Get the Translated IP address
     */
    if (NAT_MEM_ALLOCATE (NAT_IKE_HASH_POOL_ID,
                          pIKEHashNode, tIKEHashNode) == NULL)
    {
        NAT_TRC (NAT_TRC_ON,
                 "\n Memory Allocation for IKE Hash Node Failed \n");
        NAT_TRC (NAT_TRC_ON, "\n Exiting natCreateOutboundIKENode  \n");
        return NAT_FAILURE;
    }

    /* Allocate memory for the IKE Session List */
    if (NAT_MEM_ALLOCATE (NAT_IKE_LIST_POOL_ID, pIKEList, tIKEListNode) == NULL)
    {

        /* Release memory allocated for the IKE Hash Node */
        NatMemReleaseMemBlock (NAT_IKE_HASH_POOL_ID, (UINT1 *) pIKEHashNode);
        NAT_TRC (NAT_TRC_ON, "\n Memory Allocation for IKE Session List"
                 "Failed \n");
        NAT_TRC (NAT_TRC_ON, "\n Exiting natCreateOutboundIKENode  \n");
        return NAT_FAILURE;
    }
    /* Fill the Hash Node Local IP address, Outside IP address, 
       initiator cookie and Interface Number with the Inside IP address, 
       Outside IP address, Initiator cookie
       and the Interface number from the pIKEHeaderInfo respectively
       Fill the translated IP address of Hash Node with the free global IP address
       Get the current time and Update the time stamp information
     */
    natFillIKEHashNode (pIKEHashNode, pIKEHeaderInfo,
                        GlobalInfo.u4TranslatedLocIpAddr);

    /*
       Add the Hash Node to the Hash Table using the Hash key
       Add the Hash Node to the IKE Session List
       Assign Inside IP of pIKEHeaderInfo with the translated IP address
       Increment the number of active sessions for the interface
     */
    TMO_HASH_Add_Node (gpNatIKEHashList, &(pIKEHashNode->IKEHash),
                       u4HashKey, NULL);
    pIKEList->pIKEHashNode = pIKEHashNode;
    TMO_SLL_Add (&gNatIKEList, &(pIKEList->IKEList));
    pIKEHeaderInfo->u4InIpAddr = GlobalInfo.u4TranslatedLocIpAddr;
    gapNatIfTable[pIKEHeaderInfo->u4IfNum]->u4NumOfActiveSessions++;

    /* Check whether Timer is started or not  else do it now */
    if (gu4NatTmrEnable == NAT_DISABLE)
    {
        gu4NatTmrEnable = NAT_ENABLE;
        /* START TIMER */
        NatStartTimer ();
        NAT_TRC (NAT_TRC_ON, "\n NAT Timer Started \n");
    }
    NAT_TRC (NAT_TRC_ON, "\n NAT Outbound IKE Entry created \n");
    /*UT_FIX-START */
    NAT_TRC4 (NAT_TRC_ON, "\n"
              "Inside IP : %x\n    Translated IP : %x"
              "Outside IP : %x"
              "Time stamp : %d \n",
              pIKEHashNode->u4LocIpAddr, pIKEHeaderInfo->u4InIpAddr,
              pIKEHeaderInfo->u4OutIpAddr, pIKEHashNode->u4TimeStamp);
    NAT_TRC (NAT_TRC_ON, "\n Exiting natCreateOutboundIKENode  \n");
    /*UT_FIX-END */
    return NAT_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : natCreateInboundIKENode                                */
/*  Description     : This function creates an IKE hash node in the IKE hash */
/*                    table using pIKEHeaderInfo and hash key information    */
/*                    when the packet is Inbound.                            */
/*  Input(s)        : u4HashKey - Hash Key Information                       */
/*                    pIkeHeaderInfo - IKE Header Information                */
/*  Output(s)       : None                                                   */
/*  Global Variables Referred : NAT_IKE_HASH_POOL_ID                         */
/*                              NAT_IKE_LIST_POOL_ID                         */
/*                              gu4NatTmrEnable                              */
/*                              gNatIKEList                                  */
/*  Global variables Modified :  gpNatIKEHashList                            */
/*                              gu4NatTmrEnable                              */
/*                              gNatIKEList                                  */
/*  Exceptions or Operating System Error Handling :    None                  */
/*  Use of Recursion :  None                                                 */
/*  Returns          :  NAT_SUCCESS / NAT_FAILURE                            */
/*****************************************************************************/
UINT4
natCreateInboundIKENode (tIKEHeaderInfo * pIKEHeaderInfo, UINT4 u4HashKey)
{
    UINT2               u2InPort = NAT_IKE_STD_PORT;
    UINT4               u4InIpAddr = NAT_ZERO;
    tIKEHashNode       *pIKEHashNode = NULL;
    tIKEListNode       *pIKEList = NULL;

    NAT_TRC (NAT_TRC_ON, "\n Entering natCreateInboundIKENode \n");

    /* Check whether TWO way NAT is enabled for the interface */
    if (gapNatIfTable[pIKEHeaderInfo->u4IfNum]->u1TwoWayNatEnable == NAT_ENABLE)
    {
        u4InIpAddr = pIKEHeaderInfo->u4InIpAddr;

        /* Search the policy entry */
        if (NAT_FAILURE == NatPolicyGetStaticEntry (&u4InIpAddr,
                                                    pIKEHeaderInfo->u4InIpAddr,
                                                    pIKEHeaderInfo->u4OutIpAddr,
                                                    NAT_PROTO_ANY, NAT_INBOUND))
        {
            if (NatGetStaticNaptEntry (&u4InIpAddr, &u2InPort,
                                       pIKEHeaderInfo->u4Direction,
                                       pIKEHeaderInfo->u4IfNum,
                                       NAT_PROTO_ANY) == NULL)
            {
                u4InIpAddr = NatSearchStaticTable (pIKEHeaderInfo->u4InIpAddr,
                                                   pIKEHeaderInfo->u4Direction,
                                                   pIKEHeaderInfo->u4IfNum);
                if (u4InIpAddr == NAT_ZERO)
                {
                    NAT_TRC (NAT_TRC_ON, "\n No Local IP address \n");
                    NAT_TRC (NAT_TRC_ON,
                             "\n Exiting natCreateInboundIKENode  \n");
                    return NAT_FAILURE;
                }                /* End of Static table search */
            }
            else
            {
                if (u2InPort != NAT_IKE_STD_PORT)
                {
                    /* Give Message that port translation is not enabled and 
                       drop the Packet          */
                    NAT_TRC (NAT_TRC_ON,
                             "\n IKE port translation is not enabled and"
                             "Packet is dropped \n");
                    NAT_TRC (NAT_TRC_ON,
                             "\n Exiting natCreateInboundIKENode  \n");
                    return NAT_FAILURE;
                }
            }                    /*End of Static NAPT Search */
        }
        else
        {
            if (NAT_ZERO == u4InIpAddr)
            {
                NAT_TRC (NAT_TRC_ON, "\n No Local IP address \n");
                NAT_TRC (NAT_TRC_ON, "\n Exiting natCreateInboundIKENode\n");
                return NAT_FAILURE;
            }
        }
        /*
           Allocate memory for the IKE Hash Node
           Get the Local IP address
         */
        if (NAT_MEM_ALLOCATE (NAT_IKE_HASH_POOL_ID,
                              pIKEHashNode, tIKEHashNode) == NULL)
        {
            NAT_TRC (NAT_TRC_ON, "\n Memory Allocation Failed  for"
                     "IKE Hash Node\n");
            NAT_TRC (NAT_TRC_ON, "\n Exiting natCreateInboundIKENode  \n");
            return NAT_FAILURE;
        }

        /* Allocate memory for the IKE Session List */
        if (NAT_MEM_ALLOCATE (NAT_IKE_LIST_POOL_ID,
                              pIKEList, tIKEListNode) == NULL)
        {
            /* Release memory allocated for the IKE Hash Node */
            NatMemReleaseMemBlock (NAT_IKE_HASH_POOL_ID,
                                   (UINT1 *) pIKEHashNode);
            /*UT_FIX-START */
            NAT_TRC (NAT_TRC_ON, "\n Memory Allocation Failed  for IKE Session"
                     "List \n");
            /*UT_FIX-END */
            NAT_TRC (NAT_TRC_ON, "\n Exiting natCreateInboundIKENode  \n");
            return NAT_FAILURE;
        }
        /* Fill the Hash Node Translated IP address, Outside IP address, initiator
           cookie and Interface Number with the Inside IP address, Outside IP address,
           Initiator cookie and the Interface number from the pIKEHeaderInfo 
           respectively
           Fill the Local IP address of Hash Node with the Local IP address
           Get the current time and Update the time stamp information
         */
        natFillIKEHashNode (pIKEHashNode, pIKEHeaderInfo, u4InIpAddr);

        /*
           Add the Hash Node to the Hash Table using the Hash key
           Add the Hash Node to the IKE Session List
           Assign Inside IP of pIKEHeaderInfo with the Local IP address
           Increment the number of active sessions for the interface
         */
        TMO_HASH_Add_Node (gpNatIKEHashList, &(pIKEHashNode->IKEHash),
                           u4HashKey, NULL);
        pIKEList->pIKEHashNode = pIKEHashNode;
        TMO_SLL_Add (&gNatIKEList, &(pIKEList->IKEList));
        pIKEHeaderInfo->u4InIpAddr = u4InIpAddr;
        gapNatIfTable[pIKEHeaderInfo->u4IfNum]->u4NumOfActiveSessions++;

        /* Check whether Timer is started or not  else do it now */
        if (gu4NatTmrEnable == NAT_DISABLE)
        {
            gu4NatTmrEnable = NAT_ENABLE;
            /* START TIMER */
            NatStartTimer ();
            NAT_TRC (NAT_TRC_ON, "\n NAT Timer Started \n");
        }
    }
    else
    {
        /*UT_FIX-START */
        /* Two-way NAT Not Enabled */
        NAT_TRC (NAT_TRC_ON, "\n Two-way NAT Not Enabled \n");
        /*UT_FIX-END */
        NAT_TRC (NAT_TRC_ON, "\n Exiting natCreateInboundIKENode  \n");
        return NAT_FAILURE;
    }
    NAT_TRC (NAT_TRC_ON, "\n NAT Inbound IKE Entry created \n");
    /*UT_FIX-START */
    /*NAT_UT_IKE_MISC_FLT_14 -START */
    NAT_TRC4 (NAT_TRC_ON, "\n"
              "Translated IP : %x\n    Local IP : %x"
              "Outside IP : %x"
              "Time stamp : %d \n",
              pIKEHashNode->u4TranslatedIpAddr, pIKEHeaderInfo->u4InIpAddr,
              pIKEHeaderInfo->u4OutIpAddr, pIKEHashNode->u4TimeStamp);
    /*NAT_UT_IKE_MISC_FLT_14 -END */
    /*UT_FIX-END */
    NAT_TRC (NAT_TRC_ON, "\n Exiting natCreateInboundIKENode  \n");
    return NAT_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : natFillIKEHashNode                                     */
/*  Description     : This function Fills the Hash Node when the packet is   */
/*                    Inbound or Outbound.                                   */
/*  Input(s)        : *pIKEHashNode - Data structure for IKE Hash Node       */
/*                    pIkeHeaderInfo - IKE Header Information                */
/*  Output(s)       : IKEHashNode - Modified  IKE Hash Node.                 */
/*  Global Variables Referred :  None                                        */
/*  Global variables Modified :  None                                        */
/*  Exceptions or Operating System Error Handling :    None                  */
/*  Use of Recursion :  None                                                 */
/*  Returns          :  None                                                 */
/*****************************************************************************/
VOID
natFillIKEHashNode (tIKEHashNode * pIKEHashNode,
                    tIKEHeaderInfo * pIKEHeaderInfo, UINT4 u4IpAddr)
{

    UINT4               u4CurrTime = NAT_ZERO;

    NAT_TRC (NAT_TRC_ON, "\n Entering natFillIKEHashNode \n");

    /* Fill the Hash Node with HeaderInfo details */

    pIKEHashNode->u4OutIpAddr = pIKEHeaderInfo->u4OutIpAddr;
    /*UT_FIX-START */
    /* NAT_UT_IKE_FUNC_FLT_21 -START */
    NAT_GET_SYS_TIME (&u4CurrTime);
    pIKEHashNode->u4TimeStamp = u4CurrTime;
    /* NAT_UT_IKE_FUNC_FLT_21 -END */
    /*UT_FIX-END */
    /*UT_FIX-START */
    MEMCPY (pIKEHashNode->au1InitCookie, pIKEHeaderInfo->au1InitCookie,
            NAT_IKE_COOKIE_LENGTH);
    /*UT_FIX-END */
    pIKEHashNode->u4IfNum = pIKEHeaderInfo->u4IfNum;

    if (pIKEHeaderInfo->u4Direction == NAT_OUTBOUND)
    {
        pIKEHashNode->u4TranslatedIpAddr = u4IpAddr;
        pIKEHashNode->u4LocIpAddr = pIKEHeaderInfo->u4InIpAddr;
    }
    else
    {
        pIKEHashNode->u4TranslatedIpAddr = pIKEHeaderInfo->u4InIpAddr;
        pIKEHashNode->u4LocIpAddr = u4IpAddr;
    }
    NAT_TRC (NAT_TRC_ON, "\n Exiting natFillIKEHashNode \n");
}

/*****************************************************************************/
/*  Function Name   : natDeleteIKEEntries                                    */
/*  Description     : This function deletes IKE sessions from IKE List and   */
/*                    IKE hash table when the IKE timeout value expires      */
/*  Input(s)        :  None                                                  */
/*  Output(s)       : None                                                   */
/*  Global Variables Referred : NAT_IKE_LIST_POOL_ID                         */
/*                              gNatIKEList                                  */
/*  Global variables Modified : gNatIKEList                                  */
/*  Exceptions or Operating System Error Handling :    None                  */
/*  Use of Recursion :  None                                                 */
/*  Returns          :  None                                                 */
/*****************************************************************************/
VOID
natDeleteIKEEntries ()
{
    UINT4               u4CurrTime = NAT_ZERO;
    tIKEListNode       *pDeleteNode = NULL;
    tIKEListNode       *pNextNode = NULL;

    NAT_TRC (NAT_TRC_ON, "\n Entering natDeleteIKEEntries \n");

    NAT_GET_SYS_TIME (&u4CurrTime);

    /* Get the First Node of the IKE session List */
    pDeleteNode = (tIKEListNode *) TMO_SLL_First (&gNatIKEList);

    while (pDeleteNode != NULL)
    {

        /*Current time  - Session timestamp >= IKE timeout value */
        if (u4CurrTime - pDeleteNode->pIKEHashNode->u4TimeStamp >=
            (UINT4) gi4NatIKETimeOut)
        {
            /*
               Get the next node in the IKE List
               Delete the Hash Node from the IKE Hash Table
               Delete the Node from the IKE List
               Release the memory of the IKE List Node
               Assign the next node of the IKE List as the current Node
               Increment number of closed sessions for the interface
               Decrement number of active sessions for the interface
             */
            /*UT_FIX-START */
            /*NAT_UT_IKE_FUNC_FLT_19 -START */
            gapNatIfTable[pDeleteNode->pIKEHashNode->u4IfNum]->
                u4NumOfSessionsClosed++;
            gapNatIfTable[pDeleteNode->pIKEHashNode->u4IfNum]->
                u4NumOfActiveSessions--;
            /*NAT_UT_IKE_FUNC_FLT_19 -END */
            /*UT_FIX-END */
            pNextNode = (tIKEListNode *) TMO_SLL_Next (&gNatIKEList,
                                                       &(pDeleteNode->IKEList));
            natDeleteIKEHashNode (pDeleteNode->pIKEHashNode);
            TMO_SLL_Delete (&gNatIKEList, &(pDeleteNode->IKEList));
            NatMemReleaseMemBlock (NAT_IKE_LIST_POOL_ID, (UINT1 *) pDeleteNode);
            pDeleteNode = pNextNode;
        }
        else
        {
            pDeleteNode = (tIKEListNode *) TMO_SLL_Next (&gNatIKEList,
                                                         &(pDeleteNode->
                                                           IKEList));
        }
    }                            /* End of while */
    NAT_TRC (NAT_TRC_ON, "\n Exiting natDeleteIKEEntries \n");
}

/*****************************************************************************/
/*  Function Name   : natDeleteIKEHashNode                                   */
/*  Description     : This function deletes IKE Node from the IKE hash table */
/*                    and releases the memory.                               */
/*  Input(s)        : *pIKEHashNode - IKE Hash Node data structure.          */
/*  Output(s)       : None                                                   */
/*  Global Variables Referred : NAT_IKE_HASH_POOL_ID                         */
/*                              gpNatIKEHashList                             */
/*  Global variables Modified :  gpNatIKEHashList                            */
/*  Exceptions or Operating System Error Handling :    None                  */
/*  Use of Recursion :  None                                                 */
/*  Returns          :  None                                                 */
/*****************************************************************************/
VOID
natDeleteIKEHashNode (tIKEHashNode * pIKEHashNode)
{

    UINT4               u4HashKey = NAT_ZERO;

    NAT_TRC (NAT_TRC_ON, "\n Entering natDeleteIKEHashNode \n");
    /* Form the hash key for the IKE hash Table */
    u4HashKey = natFormIKEHashKey (pIKEHashNode->au1InitCookie);

    /* Delete the Hash Node */
    TMO_HASH_Delete_Node (gpNatIKEHashList, &(pIKEHashNode->IKEHash),
                          u4HashKey);

    /* Release the memory of the Hash Node */
    NatMemReleaseMemBlock (NAT_IKE_HASH_POOL_ID, (UINT1 *) pIKEHashNode);
    NAT_TRC (NAT_TRC_ON, "\n Exiting natDeleteIKEHashNode \n");
}

/*****************************************************************************/
/*  Function Name   : natDeleteIKESessionRow                                 */
/*  Description     : This function deletes IKE Session hash Node.           */
/*                    called by nmhSetNatIKESessionEntryStatus               */
/*  Input(s)        :  i4NatIKESessionInterfaceNum                  */
/*                     u4NatIKESessionLocalIp                                */
/*                     u4NatIKESessionOutsideIp                              */
/*                     pNatIKESessionInitCookie                              */
/*  Output(s)       :   None                                                 */
/*  Global variables Referred :  gNatIKEList                                 */
/*                               NAT_IKE_LIST_POOL_ID                        */
/*  Global Variables Modified :  gNatIKEList                                 */
/*                               NAT_IKE_LIST_POOL_ID                        */
/*  Exceptions or Operating System Error Handling :    None                  */
/*  Use of Recursion :  None                                                 */
/*  Returns          :  NAT_SUCCESS / NAT_FAILURE                            */
/*****************************************************************************/
UINT4
natDeleteIKESessionRow (INT4 i4NatIKESessionInterfaceNum,
                        UINT4 u4NatIKESessionLocalIp,
                        UINT4 u4NatIKESessionOutsideIp,
                        UINT1 *pNatIKESessionInitCookie)
{
    tIKEListNode       *pNatIKEList = NULL;
    NAT_TRC (NAT_TRC_ON, "\n Entering natDeleteIKESessionRow \n");

    TMO_SLL_Scan (&gNatIKEList, pNatIKEList, tIKEListNode *)
    {
        if ((pNatIKEList->pIKEHashNode->u4IfNum
             == (UINT4) i4NatIKESessionInterfaceNum) &&
            (pNatIKEList->pIKEHashNode->u4LocIpAddr
             == u4NatIKESessionLocalIp) &&
            (pNatIKEList->pIKEHashNode->u4OutIpAddr
             == u4NatIKESessionOutsideIp) && (
                                                                /*UT_FIX -START */
                                                                MEMCMP
                                                                (pNatIKESessionInitCookie,
                                                                 pNatIKEList->
                                                                 pIKEHashNode->
                                                                 au1InitCookie,
                                                                 NAT_IKE_COOKIE_LENGTH)
                                                                == NAT_ZERO)
            /*UT_FIX -END */
            )
        {
            natDeleteIKEHashNode (pNatIKEList->pIKEHashNode);
            TMO_SLL_Delete (&gNatIKEList, &(pNatIKEList->IKEList));
            NatMemReleaseMemBlock (NAT_IKE_LIST_POOL_ID, (UINT1 *) pNatIKEList);
            NAT_TRC (NAT_TRC_ON, "\n Exiting natDeleteIKESessionRow \n");
            return SNMP_SUCCESS;
        }
    }
    NAT_TRC (NAT_TRC_ON, "\n Exiting natDeleteIKESessionRow \n");
    return SNMP_FAILURE;
}

/* ******************** END_OF_FILE **************** */
#endif /* _NATIKE_C */
