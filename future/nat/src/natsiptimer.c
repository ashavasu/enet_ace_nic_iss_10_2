/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: natsiptimer.c,v 1.5 2014/07/02 10:23:27 siva Exp $
 *
 * Description:This file contains the NAT SIPALG timer related 
 *             function definitions
 *******************************************************************/

#include "natsiptimer.h"

tTimerListId        NatSipAlgTimerListId;

#ifdef SECURITY_KERNEL_WANTED

tTMO_SLL            gSipAlgDataList;
UINT4               gu4SipAlgTaskletData;
struct tasklet_struct gSipAlgTasklet;
#endif

/************************************************************************/
/*  Function Name : NatSipAlgInitTimer                                  */
/*  Description   : This function Initializes the Timer List.           */
/*  Parameter(s)  : VOID                                                */
/*  Return Values : NAT_SUCCESS/NAT_FAILURE                           */
/************************************************************************/
PUBLIC INT4
NatSipAlgInitTimer (VOID)
{
    UINT1               au1TaskName[NAT_TASK_NAME_LEN] = { NAT_ZERO };
#ifdef SECURITY_KERNEL_WANTED
    UINT4               u4Event = NAT_ZERO;
#endif
    STRCPY (au1TaskName, (const UINT1 *) CFA_TASK_NAME);

    NatSipAlgTimerListId = (tTimerListId) NAT_SIPALG_TIMER_LIST_ID;

    /* Creating TimerList & Registering callback function with the list */
#ifndef SECURITY_KERNEL_WANTED
    if (TmrCreateTimerList (au1TaskName,
                            (UINT4) NAT_SIPALG_TIMER_EXPIRY_EVENT,
                            NULL, &NatSipAlgTimerListId) != TMR_SUCCESS)
    {
#else
    if (TmrCreateTimerList (au1TaskName,
                            u4Event, NatSipAlgTmrExpiryCallBkFun,
                            &NatSipAlgTimerListId) != TMR_SUCCESS)
    {
#endif
        MOD_TRC (gu4NatTrc, OS_RESOURCE_TRC, "NAT",
                 "\n NatSipAlg Timer Init Failed \n");
        return (NAT_FAILURE);
    }
#ifdef SECURITY_KERNEL_WANTED
    TMO_SLL_Init (&gSipAlgDataList);
#endif
    return (NAT_SUCCESS);
}

/************************************************************************/
/*  Function Name : NatSipAlgDeInitTimer                                */
/*  Description   :                                                     */
/*                : This function De-Initializes the Timer List.        */
/*  Parameter(s)  : VOID                                                */
/*  Return Values : NAT_SUCCESS/NAT_FAILURE                           */
/************************************************************************/

PUBLIC INT4
NatSipAlgDeInitTimer (VOID)
{

    if (TmrDeleteTimerList (NatSipAlgTimerListId) != TMR_SUCCESS)
    {
        MOD_TRC (gu4NatTrc, OS_RESOURCE_TRC, "NAT",
                 "\n NatSipAlg Timer DeInit Failed \n");
        return (NAT_FAILURE);
    }

    return (NAT_SUCCESS);
}

/*************************************************************************/
/*  Function Name : NatSipAlgStartTimer                                  */
/*  Description   : This function  starts the timer for the given value. */
/*  Parameter(s)  : pTimer - Pointer to the timer to be started          */
/*                : TimeOut -  Time out value                            */
/*  Return Values : NAT_SUCCESS/NAT_FAILURE                            */
/*************************************************************************/

INT4
NatSipAlgStartTimer (tNatSipAlgTimer * pTimer, UINT4 u4TimeOut)
{
    if (pTimer->u1TimerStatus == NAT_ENABLE)
    {
        MOD_TRC (gu4NatTrc, OS_RESOURCE_TRC, "NAT",
                 "\n NatSipAlg Timer Already Activated \n");
        return (NAT_FAILURE);
    }
    /* Starting the Timer only if it is not started already */
    if (TmrStartTimer
        (NatSipAlgTimerListId, &pTimer->TimerNode,
         (SYS_NUM_OF_TIME_UNITS_IN_A_SEC * u4TimeOut)) != TMR_SUCCESS)
    {
        MOD_TRC (gu4NatTrc, OS_RESOURCE_TRC, "NAT",
                 "\n NatSipAlg Start Timer Failed \n");
        return (NAT_FAILURE);
    }

    pTimer->u1TimerStatus = NAT_ENABLE;

    return (NAT_SUCCESS);
}

/*************************************************************************/
/*  Function Name : NatSipAlgStopTimer                                   */
/*  Description   :                                                      */
/*                : This procedure stops the timer 'pTimer' .            */
/*  Parameter(s)  :                                                      */
/*                : pTimer  -  pointer to the Timer block                */
/*  Return Values : VOID                                                 */
/*************************************************************************/

INT4
NatSipAlgStopTimer (tNatSipAlgTimer * pTimer)
{
    if (pTimer->u1TimerStatus == NAT_DISABLE)
    {
        MOD_TRC (gu4NatTrc, OS_RESOURCE_TRC, "NAT",
                 "\n NatSipAlg Timer Already DeActivated \n");
        return (NAT_FAILURE);
    }

    if (TmrStopTimer (NatSipAlgTimerListId, &pTimer->TimerNode) != TMR_SUCCESS)
    {
        MOD_TRC (gu4NatTrc, OS_RESOURCE_TRC, "NAT",
                 "\n NatSipAlg Stop Timer Failed \n");
        return (NAT_FAILURE);
    }

    pTimer->u1TimerStatus = NAT_DISABLE;

    return (NAT_SUCCESS);
}

/*************************************************************************/
/*  Function Name : NatSipAlgRestartTimer                                */
/*  Description   :                                                      */
/*                : This procedure restarts the timer 'pTimer' .         */
/*  Parameter(s)  :                                                      */
/*                : pTimer  -  pointer to the Timer block                */
/*  Return Values : VOID                                                 */
/*************************************************************************/

INT4
NatSipAlgReStartTimer (tNatSipAlgTimer * pTimer, UINT4 u4TimeOut)
{
    if (pTimer->u1TimerStatus != NAT_DISABLE)
    {
        if (TmrStopTimer (NatSipAlgTimerListId, &pTimer->TimerNode) !=
            TMR_SUCCESS)
        {
            MOD_TRC (gu4NatTrc, OS_RESOURCE_TRC, "NAT",
                     "\n NatSipAlg Stop Timer Failed \n");
        }

        pTimer->u1TimerStatus = NAT_DISABLE;
    }

    if (TmrStartTimer (NatSipAlgTimerListId, &pTimer->TimerNode,
                       (SYS_NUM_OF_TIME_UNITS_IN_A_SEC * u4TimeOut)) !=
        TMR_SUCCESS)
    {
        MOD_TRC (gu4NatTrc, OS_RESOURCE_TRC, "NAT",
                 "\n NatSipAlg Start Timer Failed \n");
        return (NAT_FAILURE);
    }

    pTimer->u1TimerStatus = NAT_ENABLE;

    return (NAT_SUCCESS);
}

#ifndef SECURITY_KERNEL_WANTED
/******************************************************************************/
/*                                                                            */
/*  Function Name : NatSipAlgProcessTimeOut                                   */
/*  Description   :                                                           */
/*                : This procedure calls the appropriate procedures           */
/*                  to handle the timer expiry condition.                     */
/*  Parameter(s)  : None                                                      */
/*  Return Values : VOID                                                      */
/******************************************************************************/

PUBLIC VOID
NatSipAlgProcessTimeOut (tTimerListId TimerListId)
{

    tTmrAppTimer       *pTimer = NULL;

    pTimer = TmrGetNextExpiredTimer (NatSipAlgTimerListId);

    UNUSED_PARAM (TimerListId);
    /* Searching in the NatSipAlg timer list for identifying the 
       Timer whose expiry has been reached */
    while (pTimer != NULL)
    {
        pTimer = TmrGetNextExpiredTimer (NatSipAlgTimerListId);
    }
    return;
}
#else
/******************************************************************************/
/*                                                                            */
/*  Function Name : NatSipAlgProcessTimeOut                                   */
/*  Description   :                                                           */
/*                : This procedure calls the appropriate procedures           */
/*                  to handle the timer expiry condition.                     */
/*  Parameter(s)  : None                                                      */
/*  Return Values : VOID                                                      */
/******************************************************************************/

PUBLIC VOID
NatSipAlgProcessTimeOut (unsigned long ptr)
{
    tSipAlgTaskletData *pData = NULL;
    tNatSipAlgTimer    *pSipAlgTimer = NULL;

    UNUSED_PARAM (ptr);
    do
    {
        pData = (tSipAlgTaskletData *) TMO_SLL_First (&gSipAlgDataList);
        TMO_SLL_Delete (&gSipAlgDataList, (tTMO_SLL_NODE *) pData);
        pSipAlgTimer = (tNatSipAlgTimer *) pData->u4Arg;

        if (pSipAlgTimer != NULL)
        {
            pSipAlgTimer->u1TimerStatus = NAT_DISABLE;
        }
        MemReleaseMemBlock (NAT_SIPALG_TASKLET_POOL_ID, (UINT1 *) pData);
    }
    while (TMO_SLL_Count (&gSipAlgDataList) != NAT_ZERO);

    return;
}
#endif

#ifdef SECURITY_KERNEL_WANTED
/* Call back function called by kernel timer. This function 
 * should return as early as possible */
VOID
NatSipAlgTmrExpiryCallBkFun (UINT4 u4Arg)
{
    UINT4               u4QueCount = NAT_ZERO;
    tSipAlgTaskletData *pSipAlgTaskletData = NULL;

    if (MemAllocateMemBlock (NAT_SIPALG_TASKLET_POOL_ID,
                             (UINT1 **) &pSipAlgTaskletData) == MEM_FAILURE)
    {

        MOD_TRC (gu4NatTrc, OS_RESOURCE_TRC, "NAT",
                 "\n NatSipAlg Tasklet mem allocation Failure \n");
        return;
    }

    pSipAlgTaskletData->u4Arg = u4Arg;
    u4QueCount = TMO_SLL_Count (&gSipAlgDataList);
    TMO_SLL_Add (&gSipAlgDataList, (tTMO_SLL_NODE *) pSipAlgTaskletData);

    if (u4QueCount == NAT_ZERO)
    {
        /* Initialize the Tasklet as the earlier is expired */
        tasklet_init (&gSipAlgTasklet, (VOID *) NatSipAlgProcessTimeOut,
                      (UINT4) &gu4SipAlgTaskletData);
        tasklet_schedule (&gSipAlgTasklet);
    }

}
#endif
