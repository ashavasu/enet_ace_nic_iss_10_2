
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: natkstub.c,v 1.3 2015/01/19 11:34:58 siva Exp $
 *
 * Description: Stub Functions called when NAT module operates in 
 *              Kernel mode
 *******************************************************************/

#ifndef _NATKSTUB_C_
#define _NATKSTUB_C_

#include "natinc.h"
#include "natwincs.h"

/************************************************************************
 *  Function Name   : SNMPRegisterMib
 *  Description     : Function to register a protocol to agent db
 *  Input           : MibID - Protocol oid
 *                    pMibData - protocol db pointer
 *  Output          : None
 *  Returns         : Success or Failure
 ************************************************************************/
INT4
SNMPRegisterMib (tSNMP_OID_TYPE * MibID, tMibData * pMibData,
                 tSnmpMsrBool SnmpMsrTgr)
{
    UNUSED_PARAM (MibID);
    UNUSED_PARAM (pMibData);
    UNUSED_PARAM (SnmpMsrTgr);
    return 1;
}

/************************************************************************
 *  Function Name   : SNMPDelSysorEntry
 *  Description     : Function Delete a protocol from sysor table
 *  Input           : pMibOid - Protocol Oid pointer
 *                    pu1MibName - Protocol Name Pointer
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
SNMPDelSysorEntry (tSNMP_OID_TYPE * pMibOid, const UINT1 *pu1MibName)
{
    UNUSED_PARAM (pMibOid);
    UNUSED_PARAM (pu1MibName);
    return;
}

/************************************************************************
 *  Function Name   : SNMPAddSysorEntry
 *  Description     : Function add a protocol to sysor table
 *  Input           : pMibOid - Protocol Oid pointer
 *                    pu1MibName - Protocol Name Pointer
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
SNMPAddSysorEntry (tSNMP_OID_TYPE * pMibOid, const UINT1 *pu1MibName)
{
    UNUSED_PARAM (pMibOid);
    UNUSED_PARAM (pu1MibName);
    return;
}

/************************************************************************
 *  Function Name   : SNMPUnRegisterMib
 *  Description     : Function to unregister a protocol from agent db
 *  Input           : MibID - Protocol oid
 *                    pMibData - protocol db pointer
 *  Output          : None
 *  Returns         : Success or Failure
 ************************************************************************/

INT4
SNMPUnRegisterMib (tSNMP_OID_TYPE * MibID, tMibData * pMibData)
{
    UNUSED_PARAM (MibID);
    UNUSED_PARAM (pMibData);
    return 1;

}

/*-------------------------------------------------------------------+
 * Function           : NatFragInit
 *
 * Input(s)           : None. Maximum no of fragment streams
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Action :
 *
 * Initialization routine for NAT fragment handling. Creates
 * the reassembly Tree.
 *
+-------------------------------------------------------------------*/

UINT4
NatFragInit ()
{
    return NAT_SUCCESS;
}

/***************************************************************/

/*  Function Name   : NatFragDeInit                            */

/*  Description     : De-allocates all the resources used      */

/*  Input(s)        : none                                     */

/*  Output(s)       : none                                     */

/*  <OPTIONAL Fields>:                                         */

/*  Global Variables Referred : gNatTimerListId, gNatFragList  */
/*                       NAT_FRAG_RB_TREE                      */

/*  Global variables Modified : none                           */

/*  Returns         : NAT_SUCCESS or NAT_FAILURE               */

/***************************************************************/

INT4
NatFragDeInit ()
{
    return NAT_SUCCESS;
}

/* -------------------------------------------------------------------+
 * Function           : NATReassemble
 *
 * Input(s)           :  pBuf, u2Port and direction
 *
 * Output(s)          : pBuf if complete datagram.
 *
 * Returns            : NULL - If buffer is fragment
 *            Buffer pointer - pointer to complete packet.
 *
 * Action :
 * Processes IP datagram fragments.
 * If incoming datagram is complete, returns the same.
 * else if it is a fragment and completes a datagram, returns pointer
 *      completed datagram
 * else put the fragment in proper place for reassembly and returns NULL.
+-------------------------------------------------------------------*/

PUBLIC tCRU_BUF_CHAIN_HEADER *
NatFragReassemble (tCRU_BUF_CHAIN_HEADER * pCRUBuf, UINT4 u4Port,
                   UINT4 u4Direction)
{
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u4Direction);
    return pCRUBuf;
}

/*****************************************************************************/
/* Function Name      : CfaLock                                              */
/*                                                                           */
/* Description        : This function is used to take the CFA  protocol      */
/*                      SEMa4 to avoid simultaneous access to                */
/*                      CFA database                                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS or CFA_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
CfaLock (VOID)
{
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaUnLock                                            */
/*                                                                           */
/* Description        : This function is used to give the CFA                */
/*                      SEMa4 to avoid simultaneous access to                */
/*                      CFA database                                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS or CFA_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
CfaUnlock (VOID)
{
    return SNMP_SUCCESS;
}
#endif
