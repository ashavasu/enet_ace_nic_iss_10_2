/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: natcom.c,v 1.13 2016/03/09 11:44:27 siva Exp $
 *
 * Description:This file contains common functions used across
 *             all sub modules in NAT.
 *
 *******************************************************************/
#ifndef _NATCOM_C
#define _NATCOM_C
#include "natinc.h"

/* various global list used */

/* This is list containing the local hosts */
tTMO_SLL            gNatLocIpAddrList;
tTMO_SLL            gNatFreePortList;    /* This is list containing the free port  */
    /* obtained due deletion of variousi       */
    /*  connections */

UINT4               gu4NatNextFreeTranslatedLocPort;
/* Contains the value of global port 
 * which can be assigned to a new connection. */

/*******************************************************************************          FUNCTIONS PROTOTYPE USED BY THE NAT MODULE
*******************************************************************************/

PRIVATE UINT2 NatSearchFreePortList ARG_LIST ((UINT4 u4IfNum));
PRIVATE tTranslatedLocIpAddrNode *NatNextGlobalIpNode
ARG_LIST ((tInterfaceInfo * pNode, UINT4 u4TranslatedLocIpAddr));

/***************************************************************************
* Function Name  :  NatAddFreeGipList
* Description    :  This function adds the freed Global Ip in the Nat Free
*                   Global Ip list of an interface.
*
* Input (s)    :  1. u4TranslatedLocIpAddr - The IP that has to inserted in
*            the List
*             2. u4IfNum - The interface number in whose list,
*            u4TranslatedLocIpAddr has to be added
*
* Output (s)    :  None
* Returns      :  None
*
****************************************************************************/

PUBLIC VOID
NatAddFreeGipList (UINT4 u4TranslatedLocIpAddr, UINT4 u4IfNum)
{
    tNatFreeGipListNode *pNatFreeGipListNode = NULL;
    tTranslatedLocIpAddrNode *pTranslatedLocalIpAddrNode = NULL;
    tInterfaceInfo     *pNode = NULL;

    NAT_DBG (NAT_DBG_ENTRY, "\n Entering NatAddFreeGipList \n");
    /*
     * the IP address is added to FreeGipList only when the IP address is not
     * present in the Static and policy tables and it is present within 
     * the range of IP addresses  defined in the Global IP list for an Interface
     */

    pNode = gapNatIfTable[u4IfNum];
    if (pNode == NULL)
    {
        return;
    }

    if (((NatSearchStaticTable (u4TranslatedLocIpAddr, NAT_INBOUND, u4IfNum)) ==
         NAT_ZERO) && (NAT_FAILURE ==
                       NatApiSearchPolicyTable (u4TranslatedLocIpAddr)))
    {
        TMO_SLL_Scan (&gapNatIfTable[u4IfNum]->NatFreeGipList,
                      pNatFreeGipListNode, tNatFreeGipListNode *)
        {
            if (pNatFreeGipListNode->u4TranslatedLocIpAddr ==
                u4TranslatedLocIpAddr)
            {
                break;
            }
        }
        if (pNatFreeGipListNode == NULL)
        {
            /*It is not already present in the FreeGipList */
            TMO_SLL_Scan (&(gapNatIfTable[u4IfNum]->TranslatedLocIpList),
                          pTranslatedLocalIpAddrNode,
                          tTranslatedLocIpAddrNode *)
            {
                /* 
                 * If the IP exitsts in the Global IP list then allocate a node
                 * in the FreeGipList
                 */
                if ((u4TranslatedLocIpAddr & pTranslatedLocalIpAddrNode->
                     u4Mask) ==
                    (pTranslatedLocalIpAddrNode->
                     u4TranslatedLocIpAddr & pTranslatedLocalIpAddrNode->
                     u4Mask))
                {
                    NAT_MEM_ALLOCATE (NAT_FREE_GLOBAL_LIST_POOL_ID,
                                      pNatFreeGipListNode, tNatFreeGipListNode);
                    if (pNatFreeGipListNode != NULL)
                    {
                        pNatFreeGipListNode->u4TranslatedLocIpAddr =
                            u4TranslatedLocIpAddr;
                        TMO_SLL_Add (&gapNatIfTable[u4IfNum]->NatFreeGipList,
                                     &(pNatFreeGipListNode->
                                       NatFreeGipListNode));
                        break;
                    }
                    else
                        break;

                }
            }
        }
    }
    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatAddFreeGipList \n");
}

/**************************************************************************
* Function Name  :  NatSearchGlobalIpAddrTable
* Description    :  This function searches the global address list of an
*                   interface for the presence of the IP address given
*
* Input (s)    :  1. u4TranslatedLocIpAddr - The IP that has to searched in
*            the Global list of interface u4IfNum.
*             2. u4IfNum - The interface number in whose global
*            list, u4TranslatedLocIpAddr has to be searched
*
* Output (s)    :  None
* Returns      :  NAT_SUCCESS or NAT_FAILURE
*
***************************************************************************/

PUBLIC UINT4
NatSearchGlobalIpAddrTable (UINT4 u4TranslatedLocIpAddr, UINT4 u4IfNum)
{
    tTranslatedLocIpAddrNode *pTranslatedLocIpAddrNode = NULL;
    tInterfaceInfo     *pNode = NULL;
    UINT4               u4Status = NAT_ZERO;

    u4Status = NAT_FAILURE;

    NAT_DBG (NAT_DBG_ENTRY, "\n Entering NatSearchGlobalIpAddrTable \n");
    pNode = gapNatIfTable[u4IfNum];

    if (pNode == NULL)
    {
        NAT_TRC (NAT_TRC_ON, "\n Interface not initialised \n");
        return (NAT_FORWARD);
    }

    /* DSL_ADD -- If there is no entry in the global address table 
     * then the packets will be forwarded without any translations */
    pTranslatedLocIpAddrNode = (tTranslatedLocIpAddrNode *)
        TMO_SLL_First (&(pNode->TranslatedLocIpList));
    if (pTranslatedLocIpAddrNode == NULL)
    {
        u4Status = NAT_FORWARD;
    }
    /* DSL_ADD -E- */
    else if (pNode->i4RowStatus == NAT_STATUS_ACTIVE)
    {
        TMO_SLL_Scan (&(pNode->TranslatedLocIpList), pTranslatedLocIpAddrNode,
                      tTranslatedLocIpAddrNode *)
        {
            if ((pTranslatedLocIpAddrNode->i4RowStatus == NAT_STATUS_ACTIVE)
                &&
                ((pTranslatedLocIpAddrNode->
                  u4TranslatedLocIpAddr & pTranslatedLocIpAddrNode->u4Mask) ==
                 (u4TranslatedLocIpAddr & pTranslatedLocIpAddrNode->u4Mask)))
            {

                u4Status = NAT_SUCCESS;
                NAT_DBG1 (NAT_DBG_PKT_FLOW,
                          "\n%d GIP exists in Global Address Table \n",
                          u4TranslatedLocIpAddr);
                break;
            }
        }
    }
    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatSearchGlobalIpAddrTable \n");
    return (u4Status);

}

/**************************************************************************
* Function Name  :  NatSearchLocIpAddrTable
* Description    :  This function searches the Local Address Table 
*                   for the presence of the given local IP.
*
* Input (s)    :  u4LocIpAddr - This is the IP address that has to
*             be searched for.
*
* Output (s)    :  None
* Returns      :  NAT_SUCCESS or NAT_FAILURE
*
***************************************************************************/

/*
 * The u4LocIpAddr is searched in the Local Address Table and if present
 * NAT_SUCCESS is returned.
 */
PUBLIC UINT4
NatSearchLocIpAddrTable (UINT4 u4LocIpAddr)
{
    tLocIpAddrNode     *pLocIpAddrNode = NULL;
    UINT4               u4Status = NAT_ZERO;

    u4Status = NAT_FAILURE;

    NAT_DBG (NAT_DBG_ENTRY, "\n Entering NatSearchLocIpAddrTable \n");
    pLocIpAddrNode = (tLocIpAddrNode *) TMO_SLL_First (&gNatLocIpAddrList);
    if (pLocIpAddrNode != NULL)
    {
        TMO_SLL_Scan (&gNatLocIpAddrList, pLocIpAddrNode, tLocIpAddrNode *)
        {
            if ((pLocIpAddrNode->i4RowStatus == NAT_STATUS_ACTIVE)
                && (u4LocIpAddr >= pLocIpAddrNode->u4LocIpNum) &&
                (pLocIpAddrNode->u4LocIpNum & pLocIpAddrNode->u4Mask) ==
                (u4LocIpAddr & pLocIpAddrNode->u4Mask))
            {
                u4Status = NAT_SUCCESS;

                NAT_DBG1 (NAT_DBG_PKT_FLOW,
                          "\n %d LIP exists in the Local Address Table  \n",
                          u4LocIpAddr);
                break;
            }
        }
    }
    else
    {
        u4Status = NAT_SUCCESS;
    }

    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatSearchLocIpAddrTable \n");
    return (u4Status);

}

/**************************************************************************
* Function Name    :  NatSearchFreePortList
* Description    :  This function gets first free port available in the
*                  gNatFreePortList and deletes it from the list.
*
* Input (s)    :  None
*
* Output (s)    :  None
* Returns      :  Free Port Number or NULL
*
***************************************************************************/

/*
 * If the list is not empty the first global free port number is returned
 * else 0.
 */

PRIVATE UINT2
NatSearchFreePortList (UINT4 u4IfNum)
{
    UINT2               u2Port = NAT_ZERO;
    tFreePortNode      *pFreePortNode = NULL;

    u2Port = NAT_ZERO;
    pFreePortNode = NULL;

    NAT_DBG (NAT_DBG_ENTRY, "\n Entering NatSearchFreePortList \n");
    pFreePortNode = (tFreePortNode *) TMO_SLL_Get (&gNatFreePortList);
    if (pFreePortNode != NULL)
    {
        /* Port is present in the list so delete that node from the List */
        u2Port = pFreePortNode->u2Port;
        NatMemReleaseMemBlock (NAT_FREE_PORT_POOL_ID, (UINT1 *) pFreePortNode);
        pFreePortNode = NULL;
        NAT_DBG1 (NAT_DBG_PKT_FLOW, "\n Free Global Port - %d \n", u2Port);
        if (NatSearchTransPortInNaptTable (u4IfNum, u2Port) == NAT_SUCCESS)
        {
            u2Port = NAT_ZERO;
        }
    }
    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatSearchFreePortList \n");
    return (u2Port);
}

/**************************************************************************
* Function Name    :  NatUpdateHeaderInfo
* Description    :  This function updates the pHeaderInfo with new data
*                  from the IP header in the packet.
*
* Input (s)    :  1. pHeaderInfo - Contains the IP and TCP/UDP header
*            information.
*             2. pDynamicNode - Contains the pointer to the
*            corresponding session entry in the Dynamic Table.
*
* Output (s)    :  Updates pHeaderInfo
* Returns      :  None
*
***************************************************************************/

PUBLIC VOID
NatUpdateHeaderInfo (tHeaderInfo * pHeaderInfo, tDynamicEntry * pDynamicNode)
{

    NAT_DBG (NAT_DBG_ENTRY, "\n Entering NatUpdateHeaderInfo \n");
    if (pHeaderInfo->u4Direction == NAT_OUTBOUND)
    {
        pHeaderInfo->u4InIpAddr = pDynamicNode->u4TranslatedLocIpAddr;
        pHeaderInfo->u2InPort = pDynamicNode->u2TranslatedLocPort;
    }
    else
    {
        pHeaderInfo->u4InIpAddr = pDynamicNode->u4LocIpAddr;
        pHeaderInfo->u2InPort = pDynamicNode->u2LocPort;
    }
    pHeaderInfo->pDynamicEntry = pDynamicNode;

    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatUpdateHeaderInfo \n");
}

/***************************************************************************
* Function Name    :  NatGetGlobalIpForDns
* Description    :  This function gets the new Global IP for a local host
*                   when a dns query comes for it.
*
* Input (s)    :  u4IfNum - The interface number on which the packet
*             has arrived.
*
* Output (s)    :  None
* Returns      :  Global IP Address or 0           *
****************************************************************************/

/*
 * A new Global IP address is assigned to each new Dns response coming for a 
 * local host. If the  range end of the current dns Global IP in the
 * Interface Info Table for u4IfNum  has been reached then the next node
 * in the Global IP List of that Interface is taken .Then the current dns
 * Global IP and DnsRangeEnd Global IP is updated. If the Global IP list is
 * exhausted then the first global nodes's IP is taken and searched in the
 * DNS Table for its presence. If present then the next Global IP is taken
 * and same procedure is repeated. If once again the global List is exhausted
 * then 0 is returned.
 */
PUBLIC UINT4
NatGetGlobalIpForDns (UINT4 u4IfNum)
{

    tTranslatedLocIpAddrNode *pTranslatedLocIpAddrNode = NULL;
    tNatFreeGipListNode *pNatFreeGipListNode = NULL;
    UINT4               u4IpAddr = NAT_ZERO;
    tInterfaceInfo     *pIfNode = NULL;
    UINT4               u4TranslatedLocIpAddr = NAT_ZERO;

    u4IpAddr = NAT_ZERO;

    NAT_DBG (NAT_DBG_ENTRY, "\n Entering NatGetGlobalIpForDns \n");

    /* Get the information for u4IfNum */
    pIfNode = gapNatIfTable[u4IfNum];

    pNatFreeGipListNode =
        (tNatFreeGipListNode *) TMO_SLL_Get (&(pIfNode->NatFreeGipList));
    if (pNatFreeGipListNode != NULL)
    {
        u4TranslatedLocIpAddr = pNatFreeGipListNode->u4TranslatedLocIpAddr;
        NatMemReleaseMemBlock (NAT_FREE_GLOBAL_LIST_POOL_ID,
                               (UINT1 *) pNatFreeGipListNode);
        return (u4TranslatedLocIpAddr);
    }
    /* The current IP is obtained and incremented and checked against range end */
    u4TranslatedLocIpAddr = pIfNode->u4CurrDnsTranslatedLocIpInUse;
    if (pIfNode->u4CurrDnsTranslatedLocIpInUse ==
        pIfNode->u4CurrDnsTranslatedLocIpRangeEnd + NAT_ONE)
    {

/* If range end has been reached, the Global Ip in the next node is extracted */
        pTranslatedLocIpAddrNode =
            NatNextGlobalIpNode (pIfNode, u4TranslatedLocIpAddr - NAT_ONE);
        /*
         * If the Global List reaches its end, then the Global IP in the first
         * node is used and checked in the DNS Table for its presence. If
         * present the next Global IP is used and same procedure is repeated.
         */
        if (pTranslatedLocIpAddrNode == NULL)
        {
            TMO_SLL_Scan (&(pIfNode->TranslatedLocIpList),
                          pTranslatedLocIpAddrNode, tTranslatedLocIpAddrNode *)
            {
                if (pTranslatedLocIpAddrNode->i4RowStatus == NAT_STATUS_ACTIVE)
                {
                    pIfNode->u4CurrDnsTranslatedLocIpInUse =
                        pTranslatedLocIpAddrNode->u4TranslatedLocIpAddr;
                    while (TRUE)
                    {
                        u4IpAddr =
                            NatLookUpDnsTable (pIfNode->
                                               u4CurrDnsTranslatedLocIpInUse);
                        if (u4IpAddr != NAT_ZERO)
                        {
                            pIfNode->u4CurrDnsTranslatedLocIpInUse++;
                            if (pIfNode->u4CurrDnsTranslatedLocIpInUse ==
                                pIfNode->u4CurrDnsTranslatedLocIpRangeEnd +
                                NAT_ONE)
                            {
                                pIfNode->u4CurrDnsTranslatedLocIpInUse--;
                                break;
                            }
                        }
                        else
                        {
                            pIfNode->u4CurrDnsTranslatedLocIpRangeEnd =
                                pTranslatedLocIpAddrNode->
                                u4TranslatedLocIpAddr |
                                ~(pTranslatedLocIpAddrNode->u4Mask);
                            break;
                        }
                    }
                    if (u4IpAddr == NAT_ZERO)
                    {
                        u4TranslatedLocIpAddr =
                            pIfNode->u4CurrDnsTranslatedLocIpInUse;
                        break;
                    }
                }
            }
        }
        else
        {
            pIfNode->u4CurrDnsTranslatedLocIpInUse =
                pTranslatedLocIpAddrNode->u4TranslatedLocIpAddr;
            pIfNode->u4CurrDnsTranslatedLocIpRangeEnd =
                pTranslatedLocIpAddrNode->
                u4TranslatedLocIpAddr | ~(pTranslatedLocIpAddrNode->u4Mask);
            u4TranslatedLocIpAddr = pIfNode->u4CurrDnsTranslatedLocIpInUse;
        }

    }
    pIfNode->u4CurrDnsTranslatedLocIpInUse++;
    NAT_DBG1 (NAT_DBG_EXIT, "\n Exiting Global IP %d for DNS \n",
              u4TranslatedLocIpAddr);
    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatGetGlobalIpForDns \n");

    return (u4TranslatedLocIpAddr);
}

/***************************************************************************
* Function Name    :  NatNextGlobalIpNode
* Description    :  This function gets the Global IP from the next node
*                   if available.
*
* Input (s):  1. pIfNode - The interface infomration.
*             2. u4TranslatedLocIpAddr - The IP address to be searched.
*             3. pTranslatedLocIpNode - Contains the next node.
*
* Output (s)    :  pTranslatedLocIpNode
* Returns      :  Global IP Address node or NULL 
*
****************************************************************************/

PRIVATE tTranslatedLocIpAddrNode *
NatNextGlobalIpNode (tInterfaceInfo * pIfNode, UINT4 u4TranslatedLocIpAddr)
{
    tTranslatedLocIpAddrNode *pTranslatedLocIpNode = NULL;

    /*
     * This function gets the Global IP from the next node if available in the
     * Linked List of Global IP addresses in the corresponding Interface table.
     */
    pTranslatedLocIpNode = NULL;

    NAT_DBG (NAT_DBG_ENTRY, "\n Entering NatNextGlobalIp \n");

    TMO_SLL_Scan (&(pIfNode->TranslatedLocIpList), pTranslatedLocIpNode,
                  tTranslatedLocIpAddrNode *)
    {
        if ((pTranslatedLocIpNode->
             u4TranslatedLocIpAddr & pTranslatedLocIpNode->u4Mask) ==
            (u4TranslatedLocIpAddr & pTranslatedLocIpNode->u4Mask))
        {
            break;
        }
    }
    if (pTranslatedLocIpNode != NULL)
    {
        /*Scan the consecutive nodes to find if the RowStatus is Active */
        while ((pTranslatedLocIpNode = (tTranslatedLocIpAddrNode *)
                TMO_SLL_Next (&(pIfNode->TranslatedLocIpList),
                              &(pTranslatedLocIpNode->
                                TranslatedLocIpAddrNode))) != NULL)
        {
            if (pTranslatedLocIpNode->i4RowStatus == NAT_STATUS_ACTIVE)
            {
                break;
            }
        }
    }
    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatNextGlobalIp \n");
    return (pTranslatedLocIpNode);
}

/***************************************************************************
* Function Name    :  NatGetNextFreeGlobalIpPort
* Description    :  This function gets the next free global IP.And if NAPT
*             is enabled on the given interface, a next free global
*             port is also given.
*
* Input (s)    :  u4IpAddr - This is the IP address of local host which
*             needs a Global IP address.
*             u4IfNum - This gives the Interface number on which the
*             packet is being transmitted.
*
* Output (s)    :  None
* Returns      :  The Global Structure containing Global IP address
*             and port.
*
****************************************************************************/

/*
 * This function is used for assigning a global IP to a local host which
 *  initiates a new connection.
 *  If a static mapping is present for that local host then it is used.
 *  If NAPT is enabled, a global port is also assigned. If port reaches 65533
 *  the next global IP in the list is used with a global Port.
 *  If only NAT is enabled, the NatOnlyNat Hash Table is searched for the
 *  mapping of Local IP address.If not present there NAT Free Global List is
 *  searched. If that is null the next address of u4CurrGlobalIpAddr is used.
 *  If all the global IP are in use then NULL is returned.
 */
PUBLIC UINT4
NatGetNextFreeGlobalIpPort (UINT4 u4InIpAddr, UINT2 u2InPort, UINT4 u4OutIpAddr,
                            UINT2 u2OutPort, UINT4 u4IfNum,
                            tGlobalInfo * pGlobalInfo)
{
    tTranslatedLocIpAddrNode *pTranslatedLocIpNode = NULL;
    tNatFreeGipListNode *pNatFreeGipListNode = NULL;
    tNatOnlyNode       *pNatOnlyNode = NULL;
    tTMO_SLL            pIfGlobalList;
    tInterfaceInfo     *pIfNode = NULL;
    UINT4               u4TranslatedLocIpAddr = NAT_ZERO;
#ifdef UPNP_WANTED
    CHR1               *pu1value = NULL;
#endif

    if ((gu4NatEnable == NAT_DISABLE) ||
        (NatCheckIfNatEnable (u4IfNum) == NAT_DISABLE))
    {
        pGlobalInfo->u4TranslatedLocIpAddr = u4InIpAddr;
        pGlobalInfo->u2TranslatedLocPort = u2InPort;
    }

    u4TranslatedLocIpAddr = NAT_ZERO;
    pIfNode = gapNatIfTable[u4IfNum];
    pIfGlobalList = pIfNode->TranslatedLocIpList;
    UNUSED_PARAM (pIfGlobalList);
    NAT_TRC (NAT_TRC_ON, "\n Entering NatGetNextFreeGlobalIpPort.");

    /* NAT order of search : 1. Search the static policy NAT table for the 
     *                          translated IP address. 
     */
    if (NAT_SUCCESS == NatPolicyGetStaticEntry (&u4TranslatedLocIpAddr,
                                                u4InIpAddr, u4OutIpAddr,
                                                u2OutPort, NAT_OUTBOUND))
    {
        if (NAT_ZERO != u4TranslatedLocIpAddr)
        {
            pGlobalInfo->u4TranslatedLocIpAddr = u4TranslatedLocIpAddr;
            /* Mapping received from static policy NAT table, 
             * hence port should not be altered. 
             */
            pGlobalInfo->u2TranslatedLocPort = NAT_ZERO;
            return NAT_SUCCESS;
        }

    }

    if (NatGetStaticNaptEntry (&u4InIpAddr, &u2InPort,
                               NAT_OUTBOUND, u4IfNum, NAT_PROTO_ANY) == NULL)
    {
        /* NAT order of search : 2. Search the static NAT table for the 
         *                          translated IP address. 
         */
        u4TranslatedLocIpAddr = NatSearchStaticTable (u4InIpAddr, NAT_OUTBOUND,
                                                      u4IfNum);
        if (u4TranslatedLocIpAddr == NAT_ZERO)
        {
            /* NAT order of search : 3. Search the static NAPT table for the 
             *                          translated IP address and port. 
             */
            if (u4IfNum > NAT_MAX_NUM_IF)
            {
                return NAT_FAILURE;
            }

            if (gapNatIfTable[u4IfNum]->u1NaptEnable == NAT_ENABLE)
            {

                /* NAPT is enabled */
                /* Global Port Allocation */
                /* For IKE no need of port translation */
                /* Port Zero Indicates IPSec Traffic */
                if ((u2InPort != IKE_UDP_PORT) && (u2InPort != NAT_ZERO))
                {
                    pGlobalInfo->u2TranslatedLocPort =
                        NatGetFreeGlobalPort (u4IfNum, u2InPort);
	            if(pGlobalInfo->u2TranslatedLocPort == NAT_ZERO)
                    {
                        /*NAPT is enabled and we have reached MAX no of translation*/
                        NAT_TRC (NAT_TRC_ON,"\nNAPT is enabled and we have reached MAX no of translation\n");
                        return NAT_FAILURE;
                    }
                }
                else
                {
                    pGlobalInfo->u2TranslatedLocPort = u2InPort;
                }
                /* Global IP Allocation */
                /*
                 * If the Global Port < 0xffff then the currently used Global IP
                 * address is taken
                 */
		if ((pGlobalInfo->u2TranslatedLocPort < NAT_MAX_PORT_NUMBER)&&
                    (pGlobalInfo->u2TranslatedLocPort < 
                                (gu4NatInitTranslatedLocPort + MAX_NAT_FREE_PORT_NODE)))
                {
                    u4TranslatedLocIpAddr =
                        pGlobalInfo->u4TranslatedLocIpAddr =
                        pIfNode->u4CurrTranslatedLocIpAddr;

                }
                /*If the Global port reaches the max limit and u4TranslatedLocIpAddr 
                 * and u4TranslatedLocIpRangeEnd are same then we will not allow further translations
                 * since we have reched Max no of port translation*/
                if(((pGlobalInfo->u2TranslatedLocPort >= NAT_MAX_PORT_NUMBER)||
                    (pGlobalInfo->u2TranslatedLocPort >= 
                    (gu4NatInitTranslatedLocPort + MAX_NAT_FREE_PORT_NODE)))&&
                        ((pIfNode->u4CurrTranslatedLocIpAddr) >= 
                                        (pIfNode->u4TranslatedLocIpRangeEnd)))
                {
                    /*Go to the next Global IP node */
                    pTranslatedLocIpNode =
                        NatNextGlobalIpNode (pIfNode,
                                             u4TranslatedLocIpAddr
                                             - NAT_ONE);
                    if (pTranslatedLocIpNode == NULL)
                    {
                        NAT_TRC (NAT_TRC_ON, "\n Global Port And Ip Range Exhausted Returning .....\n");
                        return NAT_FAILURE;
                    }                        
                }
                /*
                 * If the Global Port reaches the Max Limit i.e . 
                 * 0xffff then we get a new Global IP (Trans IP ).
                 */
		if (((pGlobalInfo->u2TranslatedLocPort >= NAT_MAX_PORT_NUMBER)||
                    (pGlobalInfo->u2TranslatedLocPort >= 
                            (gu4NatInitTranslatedLocPort + MAX_NAT_FREE_PORT_NODE))) ||
		     (pGlobalInfo->u4TranslatedLocIpAddr >
                      (pIfNode->u4TranslatedLocIpRangeEnd)))
                {
                    /*
                     * If gu4NatNextFreeTranslatedLocPort > 
                     * NAT_MAX_PORT_NUMBER then
                     * get the next Global IP from the CurrentTransIP pointer
                     * present in the Nat Interface Table.
                     * If Trans IP taken that way
                     * is greater than the Trans IP range End address 
                     * then we go to 
                     * the next node in the GlobalIP list of the same Interface.
                     * We take the Ip addr from the New Global Ip node 
                     * and update
                     * the CurrentTransIP pointer and Trans IPRangeEndPointer
                     * 
                     */
                    u4TranslatedLocIpAddr = pIfNode->u4CurrTranslatedLocIpAddr;
                    gu4NatNextFreeTranslatedLocPort =
                        gu4NatInitTranslatedLocPort;
                    pGlobalInfo->u2TranslatedLocPort =
                        (UINT2) gu4NatInitTranslatedLocPort;
                    if (u4TranslatedLocIpAddr >
                        (pIfNode->u4TranslatedLocIpRangeEnd))
                    {
                        /*Go to the next Global IP node */
                        pTranslatedLocIpNode =
                            NatNextGlobalIpNode (pIfNode,
                                                 u4TranslatedLocIpAddr
                                                 - NAT_ONE);
                        if (pTranslatedLocIpNode == NULL)
                        {
                            /*
                             * Unable to get a Global IP node from the consecutive 
                             * nodes in the Global IP list (this may be because of
                             * absence of consecutive nodes or because the
                             * RowStatus 
                             * may not be Active).
                             * In this case search for first active node in
                             * the list 
                             * from the start of the Global IP List
                             */
                            TMO_SLL_Scan (&(pIfNode->TranslatedLocIpList),
                                          pTranslatedLocIpNode,
                                          tTranslatedLocIpAddrNode *)
                            {
                                if (pTranslatedLocIpNode->i4RowStatus ==
                                    NAT_STATUS_ACTIVE)
                                {
                                    /* Taken the first active node from list. 
                                     * Break */
                                    break;
                                }
                            }
                        }
                        if (pTranslatedLocIpNode != NULL)
                        {
                            NatGetSubnetRangeIpAddress (pTranslatedLocIpNode->
                                                        u4TranslatedLocIpAddr,
                                                        pTranslatedLocIpNode->
                                                        u4Mask,
                                                        &u4TranslatedLocIpAddr,
                                                        &pIfNode->
                                                        u4TranslatedLocIpRangeEnd);
                            pIfNode->u4CurrTranslatedLocIpAddr =
                                u4TranslatedLocIpAddr;
#ifdef UPNP_WANTED
                            CLI_CONVERT_IPADDR_TO_STR (pu1value,
                                                       (pIfNode->
                                                        u4CurrTranslatedLocIpAddr));
                            SET_UPNP_NAT_STATE_VAR_VAL
                                (NAT_VARIABLE_TYPE_EXT_IPADD, pu1value);
                            SEND_NOTIFY_TO_CONTROL_POINTS ();
#endif /* UPNP_WANTED */
                        }
                        else
                        {
                            u4TranslatedLocIpAddr = NAT_ZERO;
                            pGlobalInfo->u4TranslatedLocIpAddr = NAT_ZERO;
                        }
                    }
                    else
                    {
                        /*
                         * u4TranslatedLocIpAddr is a valid address.
                         * So update the
                         * CurrIPaddr pointer to the next IP addr.
                         */
                        pIfNode->u4CurrTranslatedLocIpAddr += NAT_ONE;
#ifdef UPNP_WANTED
                        CLI_CONVERT_IPADDR_TO_STR (pu1value,
                                                   (pIfNode->
                                                    u4CurrTranslatedLocIpAddr));
                        SET_UPNP_NAT_STATE_VAR_VAL (NAT_VARIABLE_TYPE_EXT_IPADD,
                                                    pu1value);
                        SEND_NOTIFY_TO_CONTROL_POINTS ();
#endif /* UPNP_WANTED */
                    }
                }
            }
            else
            {
                /* Only NAT (Dynamic IP address allocation) is enabled */
                /*
                 * Search the NatPolicyTable followed by NatOnlyNat Table.
                 * If mapping is not found then Get a
                 * new Global IP addr as was done above (first search 
                 * FreeGlobal IP list .If not found get the Addr from Nat 
                 * Interface Table.
                 */
                /* NAT order of search : 4. Search the dynamic policy NAT 
                 * table for the translated IP address. 
                 */
                if (NAT_SUCCESS ==
                    NatPolicyGetDynamicEntry (&u4TranslatedLocIpAddr,
                                              u4InIpAddr, u4OutIpAddr,
                                              u2OutPort))
                {
                    if (NAT_ZERO != u4TranslatedLocIpAddr)
                    {
                        pGlobalInfo->u4TranslatedLocIpAddr =
                            u4TranslatedLocIpAddr;
                        pGlobalInfo->u2TranslatedLocPort = NAT_ZERO;
                        return NAT_SUCCESS;
                    }

                }

                /* NAT order of search : 5. Search the dynamic NAT 
                 * table for the translated IP address. 
                 */
                pNatOnlyNode = NatSearchNatOnlyTable (u4InIpAddr, u4IfNum);
                if (pNatOnlyNode != NULL)
                {
                    u4TranslatedLocIpAddr = pNatOnlyNode->u4TranslatedLocIpAddr;
                    pNatOnlyNode->u4NoOfConnections++;
                }
                if (u4TranslatedLocIpAddr == NAT_ZERO)
                {
                    /* NAT order of search : 6. Search the Free global IP  
                     * list for the translated IP address. 
                     */
                    pNatFreeGipListNode =
                        (tNatFreeGipListNode *)
                        TMO_SLL_Get (&(pIfNode->NatFreeGipList));
                    if (pNatFreeGipListNode == NULL)
                    {
                        /* No free global IPs in the Nat Global IP List */
                        u4TranslatedLocIpAddr =
                            pIfNode->u4CurrTranslatedLocIpAddr;
                        if (((pIfNode->u4TranslatedLocIpRangeEnd)) <
                            u4TranslatedLocIpAddr)
                        {
                            pTranslatedLocIpNode =
                                NatNextGlobalIpNode (pIfNode,
                                                     u4TranslatedLocIpAddr -
                                                     NAT_ONE);
                            if (pTranslatedLocIpNode != NULL)
                            {

                                NatGetSubnetRangeIpAddress
                                    (pTranslatedLocIpNode->
                                     u4TranslatedLocIpAddr,
                                     pTranslatedLocIpNode->u4Mask,
                                     &u4TranslatedLocIpAddr,
                                     &pIfNode->u4TranslatedLocIpRangeEnd);
                                pIfNode->u4CurrTranslatedLocIpAddr =
                                    u4TranslatedLocIpAddr + NAT_ONE;
#ifdef UPNP_WANTED
                                CLI_CONVERT_IPADDR_TO_STR (pu1value,
                                                           (pIfNode->
                                                            u4CurrTranslatedLocIpAddr));
                                SET_UPNP_NAT_STATE_VAR_VAL
                                    (NAT_VARIABLE_TYPE_EXT_IPADD, pu1value);
                                SEND_NOTIFY_TO_CONTROL_POINTS ();
#endif /* UPNP_WANTED */
                            }
                            else
                            {
                                pGlobalInfo->u4TranslatedLocIpAddr =
                                    u4TranslatedLocIpAddr = NAT_ZERO;
                            }
                        }
                        else
                        {
                            if (pIfNode->u4CurrTranslatedLocIpAddr != NAT_ZERO)
                            {
                                pIfNode->u4CurrTranslatedLocIpAddr++;
#ifdef UPNP_WANTED
                                CLI_CONVERT_IPADDR_TO_STR (pu1value,
                                                           (pIfNode->
                                                            u4CurrTranslatedLocIpAddr));
                                SET_UPNP_NAT_STATE_VAR_VAL
                                    (NAT_VARIABLE_TYPE_EXT_IPADD, pu1value);
                                SEND_NOTIFY_TO_CONTROL_POINTS ();
#endif /* UPNP_WANTED */

                            }
                        }
                    }
                    else
                    {
                        /* there are free global IP in the NAT list */
                        u4TranslatedLocIpAddr =
                            pNatFreeGipListNode->u4TranslatedLocIpAddr;
                        NatMemReleaseMemBlock (NAT_FREE_GLOBAL_LIST_POOL_ID,
                                               (UINT1 *) pNatFreeGipListNode);
                    }
                    if (u4TranslatedLocIpAddr != NAT_ZERO)
                    {
                        /*Update the NatOnly Nat Table with the new mapping */
                        NatAddNatOnlyTable (u4InIpAddr, u4TranslatedLocIpAddr,
                                            u4IfNum, NAT_ZERO);
                        /* Update the Dns IP addr use Pointer */
                        pIfNode->u4CurrDnsTranslatedLocIpInUse =
                            u4TranslatedLocIpAddr + NAT_ONE;
                        pIfNode->u4CurrDnsTranslatedLocIpRangeEnd =
                            pIfNode->u4TranslatedLocIpRangeEnd;
                    }
                }
            }                    /* end of Dynamic NAT part */

        }
        else
        {
            /*Got an entry from Static Table so Port should not be translated */
            pGlobalInfo->u2TranslatedLocPort = NAT_ZERO;
        }
    }
    else
    {
        u4TranslatedLocIpAddr = u4InIpAddr;
        pGlobalInfo->u2TranslatedLocPort = u2InPort;
    }

    if (u4TranslatedLocIpAddr != NAT_ZERO)
    {
        pGlobalInfo->u4TranslatedLocIpAddr = u4TranslatedLocIpAddr;
        if (pIfNode->u1NaptEnable == NAT_DISABLE)
        {
            pGlobalInfo->u2TranslatedLocPort = NAT_ZERO;
        }
        NAT_TRC2 (NAT_TRC_ON, "\n Global IP and Port Assigned are %u, %d \n",
                  pGlobalInfo->u4TranslatedLocIpAddr,
                  pGlobalInfo->u2TranslatedLocPort);
    }

    NAT_TRC (NAT_TRC_ON, "\n Exiting NatGetNextFreeGlobalIpPort \n");

    return (NAT_SUCCESS);
}

/***************************************************************************
* Function Name    :  NatValidateIpAddress
* Description    :  This function validates the IP address given as input.
*
* Input (s)    :  u4IpAddr -  The IP address (in host byte order)which 
*                    needs to be validated.
*                 u4IpAddrType - The type of IP address in the contest of
*                    NAT- whether it is a Local IP or a Global IP.
*
* Output (s)    :  None
* Returns      :  NAT_SUCCESS if it is a valid packet .
*                 NAT_FAILURE if it is an invalid packet.
*
****************************************************************************/

PUBLIC UINT4
NatValidateIpAddress (UINT4 u4IpAddr, UINT4 u4IpAddrType)
{
    /*
     * This function validates the IP address given as input.
     * The IP address is an invalid IP address if -
     * 1)it is above class C IP address.
     * 2)it is a loopback address.
     * 3)broadcast address (default bcast address according to the Class).
     * RFC -1812 Section 4.2.2.11 referred.
     */
    if (u4IpAddr > NAT_CLASS_C_MAX)
    {
        /*It is a class D or Class E IP address */
        return NAT_FAILURE;
    }

    /*0x7f000000 */
    if ((u4IpAddr & NAT_LOOP_BACK_ADDR_MASK) == NAT_LOOPBACK_IPADDR)
    {
        /* It is a loopback IP address ( 127.X.X.X) */
        return NAT_FAILURE;
    }

    if (u4IpAddr == NAT_INVLD_IP_ADDR_1)    /* 0x00000000 */
    {
        /* It is an obselete form of limited broadcast address */
        return NAT_FAILURE;
    }

    /*
     * Checking for Class A broadcast address and invalid 
     * address(like 10.0.0.0) if the IP address is the Global IP address
     */
    if (NAT_IS_IPADDR_CLASS_A (u4IpAddr) == TRUE)
    {
        /* It is a Class A IP address */
        if (((u4IpAddr & NAT_CLASS_A_HOST_MASK) == NAT_CLASS_A_HOST_MASK) ||
            ((u4IpAddrType == NAT_GLOBAL)
             && ((u4IpAddr & NAT_CLASS_A_HOST_MASK) == NAT_INVLD_IP_ADDR_1)))
        {
            /*
             * It is net-prefixed broadcast IP address 
             * (RFC 1812-Sec 4.2.2.11)
             */
            return NAT_FAILURE;
        }
    }

    /*
     * Checking for Class B broadcast address and invalid
     * address(like 134.5.0.0) if the IP address is the Global IP address
     */
    if (NAT_IS_IPADDR_CLASS_B (u4IpAddr) == TRUE)
    {
        /* It is a Class B IP address */
        if (((u4IpAddr & NAT_CLASS_B_HOST_MASK) == NAT_CLASS_B_HOST_MASK) ||
            ((u4IpAddrType == NAT_GLOBAL)
             && ((u4IpAddr & NAT_CLASS_B_HOST_MASK) == NAT_INVLD_IP_ADDR_1)))
        {
            /* It is net-prefixed broadcast IP address */
            return NAT_FAILURE;
        }
    }
    /*
     * Checking for Class C broadcast address and invalid
     * address(like 193.5.6.0) if the IP address is the Global IP address
     */
    if (NAT_IS_IPADDR_CLASS_C (u4IpAddr) == TRUE)
    {
        /* It is a Class C IP address */
        if (((u4IpAddr & NAT_EXTRACT_MSB8_BITS) == NAT_EXTRACT_MSB8_BITS) ||
            ((u4IpAddrType == NAT_GLOBAL)
             && ((u4IpAddr & NAT_EXTRACT_MSB8_BITS) == NAT_INVLD_IP_ADDR_1)))
        {
            /* It is net-prefixed broadcast IP address */
            return NAT_FAILURE;
        }
    }
    /* if all the above checks fail then the packet is a valid packet */
    return NAT_SUCCESS;
}

/***************************************************************************
* Function Name    :  NatGetFreeGlobalPortGroup
* Description    :  This function returns the contiguous ports starting with 
*                  the required parity.
*
* Input (s)    : pu2TransBasePort- Starting port number of the contiguous 
*                                  ports.
*                u4NumOfPort     - Number of contiguous ports. 
*
*                  eBaseParity - The parity of the base port
*                  
*                  u1BindingType - type of port pool (Signal or Media)  
*                  This is unused now and would be used when 
*                  port pools are defined
*
* Output (s)    : pu2TransBasePort.
* Returns      :  NAT_SUCCESS or NAT_FAILURE
****************************************************************************/

PUBLIC INT4
NatGetFreeGlobalPortGroup (UINT2 *pu2TransBasePort, UINT4 u4NumOfPort,
                           eParity eBaseParity, eBindType eBindingType)
{
    UINT4               u4FreeNodes = NAT_ZERO;
    UINT4               u4ReqPorts = NAT_ZERO;
    UINT4               u4PortFound = NAT_FALSE;
    UINT2               u2StartPort = NATIPC_PORT_NUM_ZERO;
    tFreePortNode      *pStartDelNode = NULL;
    tFreePortNode      *pCurrFreeNode = NULL;
    tFreePortNode      *pNextFreeNode = NULL;
    tTMO_SLL_NODE      *pNext = NULL;
    tTMO_SLL_NODE      *pCurr = NULL;

    UNUSED_PARAM (eBindingType);    /* This would be used when there has 
                                     * to be a difference in the range of 
                                     * ports for Media and Signal i.e two port 
                                     * pools 1 for media and other for signal */

    /* get number of free nodes present */
    u4FreeNodes = TMO_SLL_Count (&gNatFreePortList);

    if (u4FreeNodes >= u4NumOfPort)
    {
        /* enters when the available ports are >= total ports requested */
        pCurrFreeNode = (tFreePortNode *) TMO_SLL_First (&gNatFreePortList);
        if (pCurrFreeNode == NULL)
        {
            return NAT_FAILURE;
        }
        pNextFreeNode = NULL;
        pStartDelNode = pCurrFreeNode;

        /* the base port of N contiguous ports */
        u2StartPort = pCurrFreeNode->u2Port;
        u4ReqPorts = u4NumOfPort - NAT_ONE;

        TMO_SLL_Scan (&gNatFreePortList, pNextFreeNode, tFreePortNode *)
        {

            /* chk for continuity */
            if (((pNextFreeNode->u2Port) == (pCurrFreeNode->u2Port + NAT_ONE)) && ((eBaseParity == PARITY_ANY)    /* dont care about the parity */
                                                                                   ||
                                                                                   (eBaseParity
                                                                                    ==
                                                                                    NAT_BASE_PORT_PARITY
                                                                                    (u2StartPort))))
                /* if requested parity matches with the free port available */
            {
                u4ReqPorts--;
                if (u4ReqPorts == NAT_ZERO)
                {
                    /* N contiguous ports allocated with 
                       baseport's parity as requested */
                    u4PortFound = NAT_TRUE;
                    break;
                }
            }
            else
            {
                u4ReqPorts = u4NumOfPort - NAT_ONE;
                pStartDelNode = pNextFreeNode;
                u2StartPort = pNextFreeNode->u2Port;
            }
            pCurrFreeNode = pNextFreeNode;
        }

        if ((pNextFreeNode != NULL) && (pStartDelNode != NULL))
        {
            *pu2TransBasePort = pStartDelNode->u2Port;
            for (pCurr = &(pStartDelNode->NextFreePort),
                 pNext = TMO_SLL_Next
                 (&gNatFreePortList, &(pStartDelNode->NextFreePort));
                 pCurr != &(pNextFreeNode->NextFreePort);
                 pCurr = pNext, pNext = TMO_SLL_Next (&gNatFreePortList, pNext))
            {

                TMO_SLL_Delete (&gNatFreePortList, pCurr);
                NatMemReleaseMemBlock (NAT_FREE_PORT_POOL_ID, (UINT1 *) pCurr);
            }
            TMO_SLL_Delete (&gNatFreePortList, pCurr);
            NatMemReleaseMemBlock (NAT_FREE_PORT_POOL_ID, (UINT1 *) pCurr);
        }

    }

    /* No need, exists already, just add node to list */
    if ((u4FreeNodes < u4NumOfPort) || (u4PortFound == NAT_FALSE))
    {
        if (eBaseParity != PARITY_ANY)
        {
            /* if the required parity matches with that of global variable, then
               the N ports can be directly obtained starting from the global variable */

            if (NAT_BASE_PORT_PARITY (gu4NatNextFreeTranslatedLocPort) !=
                eBaseParity)
            {
                /* Since the parity and port number specified don't match,
                   the port number is  released to the SLL */

                if (NatReleaseGlobalPort
                    ((UINT2) gu4NatNextFreeTranslatedLocPort) == NAT_FAILURE)
                {
                    return NAT_FAILURE;
                }

                /* Move to the next number to get the required parity */
                gu4NatNextFreeTranslatedLocPort++;
            }
        }
        if (gu4NatNextFreeTranslatedLocPort + u4NumOfPort > NAT_PORT_LIMIT)
        {
            /* If the port number getting allocated exceeds 2 byte max: error */
            NAT_DBG1 (NAT_DBG_PKT_FLOW,
                      "\n %u NAT port getting allocated exceeds 2^16-1 = 65535 \n",
                      gu4NatNextFreeTranslatedLocPort + u4NumOfPort);
            return NAT_FAILURE;
        }

        *pu2TransBasePort = (UINT2) gu4NatNextFreeTranslatedLocPort;

        gu4NatNextFreeTranslatedLocPort += u4NumOfPort;
    }
    return NAT_SUCCESS;
}

/**************************************************************************
* Function Name  :  NatReleaseGlobalPort
* Description    :  This function adds the released global port into 
*                   the Free global port list 
*
* Input (s)    :  u2PortNum - The released global port number
*
* Output (s)    :  None
* Returns      :  None
*
***************************************************************************/

PUBLIC INT4
NatReleaseGlobalPort (UINT2 u2PortNum)
{
    tFreePortNode      *pFreePortNode = NULL;
    tFreePortNode      *pCurrFreeNode = NULL;
    tFreePortNode      *pNextFreeNode = NULL;

    pFreePortNode = NULL;

    if (u2PortNum == NAT_SIP_DEFAULT_ALG_PORT)
    {
        return (NAT_SUCCESS);
    }
    if (u2PortNum >= gu4NatInitTranslatedLocPort)
    {
        if (NAT_MEM_ALLOCATE (NAT_FREE_PORT_POOL_ID,
                              pFreePortNode, tFreePortNode) == NULL)
        {
            return NAT_FAILURE;
        }
        pFreePortNode->u2Port = u2PortNum;

        /* scan the free port list, add free port(node) in inc. order */
        pCurrFreeNode = NULL;
        pNextFreeNode = NULL;

        TMO_SLL_Scan (&gNatFreePortList, pNextFreeNode, tFreePortNode *)
        {
            if (pNextFreeNode->u2Port == u2PortNum)
            {
                NatMemReleaseMemBlock (NAT_FREE_PORT_POOL_ID,
                                       (UINT1 *) pFreePortNode);
                return NAT_SUCCESS;
            }
            if (pNextFreeNode->u2Port > u2PortNum)
            {
                /* port value falls some where in middle of freed ports */
                break;
            }
            pCurrFreeNode = pNextFreeNode;
        }

        if (pNextFreeNode != NULL)
        {
            TMO_SLL_Insert (&gNatFreePortList,
                            &(pCurrFreeNode->NextFreePort),
                            &(pFreePortNode->NextFreePort));
        }
        else
        {
            /* port is the largest of all freed ports, add last */
            TMO_SLL_Add (&gNatFreePortList, &(pFreePortNode->NextFreePort));
        }
    }
    else
    {
        /* It is a privilaged Port 
         * Release it to privilaged array */
        if (gu2FreePortIndex == NAT_ZERO)
        {
            /* it is an impossible condition */
            return NAT_FAILURE;
        }
        else
        {
            gu2FreePortIndex--;
            gau2NatGlobalFreePorts[gu2FreePortIndex] = u2PortNum;
        }
    }
    return NAT_SUCCESS;
}

/**************************************************************************
* Function Name  :  NatSearchString
* Description    :  This function  searches for a given string in a 
*                   given linear data buffer.
*
* Input (s)    :  u2PortNum - The released global port number
*
* Output (s)    :  None
* Returns      :  None
*
***************************************************************************/

PUBLIC INT4
NatSearchString (UINT1 *pData, UINT4 u4DataLen, CONST CHR1 * pStr)
{
    UINT4               u4DataCount = NAT_ZERO;
    UINT4               u4DataStrCount = NAT_ZERO;
    UINT4               u4StrCount = NAT_ZERO;
    UINT4               u4StrLen = NAT_ZERO;

    u4StrLen = STRLEN (pStr);
    for (u4DataCount = NAT_ZERO; u4DataCount < u4DataLen - u4StrLen;
         u4DataCount++)
    {
        for (u4DataStrCount = u4DataCount, u4StrCount = NAT_ZERO;
             u4DataStrCount
             < (u4DataLen - u4StrLen) + NAT_ONE; u4DataStrCount++, u4StrCount++)
        {
            if ((*(pData + u4DataStrCount) != *(pStr + u4StrCount)) &&
                (*(pData + u4DataStrCount) !=
                 (*(pStr + u4StrCount) - ('a' - 'A'))))
            {
                break;
            }
            if (u4StrCount == (u4StrLen - NAT_ONE))
            {
                return ((INT4) u4DataStrCount + NAT_ONE);
            }
        }
    }
    return (NAT_NOT_FOUND);
}

/***************************************************************************
* Function Name    :  NatGetFreeGlobalPort
* Description      :  This function gets the next free global Port.
*
* Input (s)        : u4IfNum - Interface Number
*                    u2LocPort - local port
*
* Output (s)       :  None
* Returns          :  Free Gloabl Port.
****************************************************************************/

PUBLIC UINT2
NatGetFreeGlobalPort (UINT4 u4IfNum, UINT2 u2LocPort)
{
    UINT2               u2TranslatedLocPort = NAT_ZERO;

    u2TranslatedLocPort = NAT_ZERO;

    if ((gapNatIfTable[u4IfNum]->u1NaptEnable == NAT_ENABLE) &&
        (gapNatIfTable[u4IfNum]->u1NatEnable == NAT_ENABLE) &&
        (gu4NatEnable == NAT_ENABLE))
    {

        /* Search the FreePortList for free ports */
        u2TranslatedLocPort = NatSearchFreePortList (u4IfNum);

	if ((u2TranslatedLocPort == NAT_ZERO) && ( gu4NatNextFreeTranslatedLocPort <
                         (gu4NatInitTranslatedLocPort + MAX_NAT_FREE_PORT_NODE)))
        {
            /* Take the next Port from the Translated Port Counter */
            u2TranslatedLocPort = (UINT2) (gu4NatNextFreeTranslatedLocPort);
           if(gu4NatNextFreeTranslatedLocPort < NAT_MAX_PORT_NUMBER )
           {	
            gu4NatNextFreeTranslatedLocPort++;
	   }
            if (NatSearchTransPortInNaptTable (u4IfNum,
                                               u2TranslatedLocPort)
                == NAT_SUCCESS)
            {
                u2TranslatedLocPort = (UINT2) (gu4NatNextFreeTranslatedLocPort);
	        if(gu4NatNextFreeTranslatedLocPort < NAT_MAX_PORT_NUMBER)
                {
                gu4NatNextFreeTranslatedLocPort++;
	        }
            }
        }
    }
    else
    {
        u2TranslatedLocPort = u2LocPort;
    }

    return (u2TranslatedLocPort);
}

/***************************************************************************
* Function Name    :  NatGetPrivilagedGlobalPort
* Description      :  This function gets the next free privilaged (512- 1023)
*                     global Port.
*
* Input (s)        :  None
*
* Output (s)       :  None
* Returns          :  Free Gloabl Port.
****************************************************************************/

UINT2
NatGetPrivilagedGlobalPort (VOID)
{
    UINT2               u2GlobalPort = NAT_ZERO;

    /* If there are no free privilaged ports, return 0 */
    if (gu2FreePortIndex <= NAT_MAX_FREE_PRIV_PORTS)
    {

        u2GlobalPort = gau2NatGlobalFreePorts[gu2FreePortIndex];
        gu2FreePortIndex++;
    }

    return u2GlobalPort;
}

/***************************************************************************
* Function Name    :  NatGetSubnetRangeIpAddress
* Description      :  This function gives the start and end ip in the subnet
*                     for Ip address and mask provided.
*
* Input (s)        :  u4IpAddr -  The IP address. 
*                     u4Mask   -  Mask for subnet.
*
* Output (s)       :  pu4StartIp - Start IP address of subnet.
*                     pu4EndIp   - End IP address of subnet.
* Returns          :  NAT_SUCCESS/NAT_FAILURE.
*
****************************************************************************/

PUBLIC UINT4
NatGetSubnetRangeIpAddress (UINT4 u4IpAddress, UINT4 u4Mask, UINT4 *pu4StartIp,
                            UINT4 *pu4EndIp)
{

    UINT4               u4Network = NAT_ZERO;
    UINT4               u4BroadCastAddr = NAT_ZERO;

    if ((pu4StartIp == NULL) || (pu4EndIp == NULL))
    {
        NAT_TRC (NAT_TRC_ON, "\r\n NatGetSubnetRangeIpAddress: Start IP or "
                 "End IP is NULL !\r\n");
        return NAT_FAILURE;
    }

    /* Get netowrk of the subnet */
    u4Network = u4IpAddress & u4Mask;

    /* Calculate the braoadcast address for subnet. 
     * Start IP address for the subnet is network address plus 1.
     * End IP address for the subnet is braodcast address minus 1.
     */

    u4BroadCastAddr = u4Network | (~u4Mask & NAT_INIT_MASK);

    /* Increment the Network IP address
     * so that we don't end up using that
     * for translations.
     */
    if (u4Network == u4IpAddress)
    {
        *pu4StartIp = u4IpAddress + NAT_ONE;
    }
    else
    {
        *pu4StartIp = u4IpAddress;
    }

    *pu4EndIp = u4BroadCastAddr - NAT_ONE;

    /* If mask is  255.255.255.255 or 255.255.255.254
     * then Start IP address of subnet is same as netowrk
     * and End IP address of subnet is braodcast address.
     */

    if ((u4Mask == NAT_INIT_MASK - NAT_ONE) || (u4Mask == NAT_INIT_MASK))
    {
        *pu4StartIp = u4Network;
        *pu4EndIp = u4BroadCastAddr;
    }

    NAT_TRC5 (NAT_TRC_ON, "Ip Addr %x, Mask %x, BroadCastAddr %x,"
              " Start Ip %x, End Ip %x\n", u4IpAddress, u4Mask, u4BroadCastAddr,
              *pu4StartIp, *pu4EndIp);
    return NAT_SUCCESS;

}
#endif /* _NATCOM_C */
