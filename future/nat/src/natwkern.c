
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: natwkern.c,v 1.10 2014/07/02 10:23:27 siva Exp $
 *
 * Description: Functions called when NAT module operates in Kernel mode
 *******************************************************************/
#include "natwincs.h"
#define IOCTL_SUCCESS 0
extern tTMO_SLL     gNatPartialLinksList;

extern UINT4        gu4CopyModIdx;
extern INT4         gi4CliWebSetError;

/* -------------------------------------------------------------
 *
 * Function: NatNmhIoctl
 *
 * -------------------------------------------------------------
 */
int
NatNmhIoctl (unsigned long p)
{
    tOsixKernUserInfo   OsixKerUseInfo;
    int                 rc = 0;
    int                 nmhiocnr;
    union unNatNmh      lv;
    UINT4               count = 0;

    tNatPartialLinkNode *pNatPartialLinkNode;

    OsixKerUseInfo.pDest = &nmhiocnr;
    OsixKerUseInfo.pSrc = (int *) p;

    if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo, 4, 0) ==
        OSIX_FAILURE)
    {
        return -EFAULT;
    }

    MEMSET (&lv, 0, sizeof (union unNatNmh));

    switch (nmhiocnr)
    {

        case NMH_GET_NAT_ENABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatEnable;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatEnable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatEnable), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatEnable.rval =
                nmhGetNatEnable (lv.nmhGetNatEnable.pi4RetValNatEnable);

            NatUnLock ();
            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatEnable;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatEnable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatEnable)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_TRC_FLAG:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatTrcFlag;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatTrcFlag *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatTrcFlag), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatTrcFlag.rval =
                nmhGetNatTrcFlag (lv.nmhGetNatTrcFlag.pi4RetValNatTrcFlag);

            NatUnLock ();
            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatTrcFlag;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatTrcFlag *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatTrcFlag)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_TYPICAL_NUMBER_OF_ENTRIES:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatTypicalNumberOfEntries;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatTypicalNumberOfEntries *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatTypicalNumberOfEntries),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatTypicalNumberOfEntries.rval =
                nmhGetNatTypicalNumberOfEntries (lv.
                                                 nmhGetNatTypicalNumberOfEntries.
                                                 pi4RetValNatTypicalNumberOfEntries);
            NatUnLock ();
            lv.nmhGetNatTypicalNumberOfEntries.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatTypicalNumberOfEntries;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatTypicalNumberOfEntries *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatTypicalNumberOfEntries)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_TRANSLATED_LOCAL_PORT_START:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatTranslatedLocalPortStart;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatTranslatedLocalPortStart *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatTranslatedLocalPortStart),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatTranslatedLocalPortStart.rval =
                nmhGetNatTranslatedLocalPortStart (lv.
                                                   nmhGetNatTranslatedLocalPortStart.
                                                   pi4RetValNatTranslatedLocalPortStart);
            NatUnLock ();
            lv.nmhGetNatTranslatedLocalPortStart.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatTranslatedLocalPortStart;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatTranslatedLocalPortStart *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatTranslatedLocalPortStart)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_IDLE_TIME_OUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatIdleTimeOut;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatIdleTimeOut *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIdleTimeOut), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatIdleTimeOut.rval =
                nmhGetNatIdleTimeOut (lv.nmhGetNatIdleTimeOut.
                                      pi4RetValNatIdleTimeOut);
            NatUnLock ();
            lv.nmhGetNatIdleTimeOut.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatIdleTimeOut;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatIdleTimeOut *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIdleTimeOut)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_TCP_TIME_OUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatTcpTimeOut;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatTcpTimeOut *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatTcpTimeOut), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatTcpTimeOut.rval =
                nmhGetNatTcpTimeOut (lv.nmhGetNatTcpTimeOut.
                                     pi4RetValNatTcpTimeOut);
            NatUnLock ();
            lv.nmhGetNatTcpTimeOut.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhGetNatTcpTimeOut;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatTcpTimeOut *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatTcpTimeOut)) == OSIX_FAILURE)
                /* Copy NPAPI return value(s) back to user */
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_UDP_TIME_OUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatUdpTimeOut;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatUdpTimeOut *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatUdpTimeOut), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatUdpTimeOut.rval =
                nmhGetNatUdpTimeOut (lv.nmhGetNatUdpTimeOut.
                                     pi4RetValNatUdpTimeOut);
            NatUnLock ();
            lv.nmhGetNatUdpTimeOut.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatUdpTimeOut;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatUdpTimeOut *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatUdpTimeOut)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_STAT_DYNAMIC_ALLOC_FAILURE_COUNT:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatStatDynamicAllocFailureCount;
            OsixKerUseInfo.pSrc =
                (tNatwnmhGetNatStatDynamicAllocFailureCount *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatStatDynamicAllocFailureCount),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatStatDynamicAllocFailureCount.rval =
                nmhGetNatStatDynamicAllocFailureCount (lv.
                                                       nmhGetNatStatDynamicAllocFailureCount.
                                                       pu4RetValNatStatDynamicAllocFailureCount);
            NatUnLock ();
            lv.nmhGetNatStatDynamicAllocFailureCount.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatStatDynamicAllocFailureCount;
            OsixKerUseInfo.pDest =
                (tNatwnmhGetNatStatDynamicAllocFailureCount *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatStatDynamicAllocFailureCount)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_STAT_TOTAL_NUMBER_OF_TRANSLATIONS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatStatTotalNumberOfTranslations;
            OsixKerUseInfo.pSrc =
                (tNatwnmhGetNatStatTotalNumberOfTranslations *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatStatTotalNumberOfTranslations),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatStatTotalNumberOfTranslations.rval =
                nmhGetNatStatTotalNumberOfTranslations (lv.
                                                        nmhGetNatStatTotalNumberOfTranslations.
                                                        pu4RetValNatStatTotalNumberOfTranslations);
            NatUnLock ();
            lv.nmhGetNatStatTotalNumberOfTranslations.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatStatTotalNumberOfTranslations;
            OsixKerUseInfo.pDest =
                (tNatwnmhGetNatStatTotalNumberOfTranslations *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatStatTotalNumberOfTranslations)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_STAT_TOTAL_NUMBER_OF_ACTIVE_SESSIONS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatStatTotalNumberOfActiveSessions;
            OsixKerUseInfo.pSrc =
                (tNatwnmhGetNatStatTotalNumberOfActiveSessions *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatStatTotalNumberOfActiveSessions),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatStatTotalNumberOfActiveSessions.rval =
                nmhGetNatStatTotalNumberOfActiveSessions (lv.
                                                          nmhGetNatStatTotalNumberOfActiveSessions.
                                                          pu4RetValNatStatTotalNumberOfActiveSessions);
            NatUnLock ();
            lv.nmhGetNatStatTotalNumberOfActiveSessions.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatStatTotalNumberOfActiveSessions;
            OsixKerUseInfo.pDest =
                (tNatwnmhGetNatStatTotalNumberOfActiveSessions *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatStatTotalNumberOfActiveSessions)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_STAT_TOTAL_NUMBER_OF_PKTS_DROPPED:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatStatTotalNumberOfPktsDropped;
            OsixKerUseInfo.pSrc =
                (tNatwnmhGetNatStatTotalNumberOfPktsDropped *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatStatTotalNumberOfPktsDropped),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatStatTotalNumberOfPktsDropped.rval =
                nmhGetNatStatTotalNumberOfPktsDropped (lv.
                                                       nmhGetNatStatTotalNumberOfPktsDropped.
                                                       pu4RetValNatStatTotalNumberOfPktsDropped);
            NatUnLock ();
            lv.nmhGetNatStatTotalNumberOfPktsDropped.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatStatTotalNumberOfPktsDropped;
            OsixKerUseInfo.pDest =
                (tNatwnmhGetNatStatTotalNumberOfPktsDropped *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatStatTotalNumberOfPktsDropped)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_STAT_TOTAL_NUMBER_OF_SESSIONS_CLOSED:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatStatTotalNumberOfSessionsClosed;
            OsixKerUseInfo.pSrc =
                (tNatwnmhGetNatStatTotalNumberOfSessionsClosed *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatStatTotalNumberOfSessionsClosed),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatStatTotalNumberOfSessionsClosed.rval =
                nmhGetNatStatTotalNumberOfSessionsClosed (lv.
                                                          nmhGetNatStatTotalNumberOfSessionsClosed.
                                                          pu4RetValNatStatTotalNumberOfSessionsClosed);
            NatUnLock ();
            lv.nmhGetNatStatTotalNumberOfSessionsClosed.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatStatTotalNumberOfSessionsClosed;
            OsixKerUseInfo.pDest =
                (tNatwnmhGetNatStatTotalNumberOfSessionsClosed *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatStatTotalNumberOfSessionsClosed)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_I_K_E_PORT_TRANSLATION:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatIKEPortTranslation;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatIKEPortTranslation *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIKEPortTranslation), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatIKEPortTranslation.rval =
                nmhGetNatIKEPortTranslation (lv.nmhGetNatIKEPortTranslation.
                                             pi4RetValNatIKEPortTranslation);
            NatUnLock ();
            lv.nmhGetNatIKEPortTranslation.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatIKEPortTranslation;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatIKEPortTranslation *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIKEPortTranslation)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_I_K_E_TIMEOUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatIKETimeout;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatIKETimeout *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIKETimeout), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatIKETimeout.rval =
                nmhGetNatIKETimeout (lv.nmhGetNatIKETimeout.
                                     pi4RetValNatIKETimeout);
            NatUnLock ();
            lv.nmhGetNatIKETimeout.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatIKETimeout;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatIKETimeout *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIKETimeout)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_I_P_SEC_TIMEOUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatIPSecTimeout;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatIPSecTimeout *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIPSecTimeout), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatIPSecTimeout.rval =
                nmhGetNatIPSecTimeout (lv.nmhGetNatIPSecTimeout.
                                       pi4RetValNatIPSecTimeout);
            NatUnLock ();
            lv.nmhGetNatIPSecTimeout.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatIPSecTimeout;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatIPSecTimeout *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIPSecTimeout)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_I_P_SEC_PENDING_TIMEOUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatIPSecPendingTimeout;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatIPSecPendingTimeout *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIPSecPendingTimeout), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatIPSecPendingTimeout.rval =
                nmhGetNatIPSecPendingTimeout (lv.nmhGetNatIPSecPendingTimeout.
                                              pi4RetValNatIPSecPendingTimeout);
            NatUnLock ();
            lv.nmhGetNatIPSecPendingTimeout.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatIPSecPendingTimeout;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatIPSecPendingTimeout *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIPSecPendingTimeout)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_I_P_SEC_MAX_RETRY:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatIPSecMaxRetry;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatIPSecMaxRetry *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIPSecMaxRetry), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatIPSecMaxRetry.rval =
                nmhGetNatIPSecMaxRetry (lv.nmhGetNatIPSecMaxRetry.
                                        pi4RetValNatIPSecMaxRetry);
            NatUnLock ();
            lv.nmhGetNatIPSecMaxRetry.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatIPSecMaxRetry;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatIPSecMaxRetry *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIPSecMaxRetry)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_SIP_ALG_PORT:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetSipAlgPort;
            OsixKerUseInfo.pSrc = (tNatwnmhGetSipAlgPort *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetSipAlgPort), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetSipAlgPort.rval =
                nmhGetSipAlgPort (lv.nmhGetSipAlgPort.pi4RetValSipAlgPort);

            NatUnLock ();
            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetSipAlgPort;
            OsixKerUseInfo.pDest = (tNatwnmhGetSipAlgPort *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetSipAlgPort)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_GET_NAT_SIP_ALG_PARTIAL_ENTRY_TIME_OUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatSipAlgPartialEntryTimeOut;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatSipAlgPartialEntryTimeOut *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatSipAlgPartialEntryTimeOut),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatSipAlgPartialEntryTimeOut.rval =
                nmhGetNatSipAlgPartialEntryTimeOut (lv.
                                                    nmhGetNatSipAlgPartialEntryTimeOut.
                                                    pi4RetValNatSipAlgPartialEntryTimeOut);

            NatUnLock ();
            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatSipAlgPartialEntryTimeOut;
            OsixKerUseInfo.pDest =
                (tNatwnmhGetNatSipAlgPartialEntryTimeOut *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatSipAlgPartialEntryTimeOut)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_SET_NAT_ENABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatEnable;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatEnable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatEnable), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();

            lv.nmhSetNatEnable.rval =
                nmhSetNatEnable (lv.nmhSetNatEnable.i4SetValNatEnable);
            NatUnLock ();

            lv.nmhSetNatEnable.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatEnable;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatEnable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatEnable)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_TYPICAL_NUMBER_OF_ENTRIES:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatTypicalNumberOfEntries;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatTypicalNumberOfEntries *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatTypicalNumberOfEntries),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhSetNatTypicalNumberOfEntries.rval =
                nmhSetNatTypicalNumberOfEntries (lv.
                                                 nmhSetNatTypicalNumberOfEntries.
                                                 i4SetValNatTypicalNumberOfEntries);
            NatUnLock ();
            lv.nmhSetNatTypicalNumberOfEntries.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatTypicalNumberOfEntries;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatTypicalNumberOfEntries *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatTypicalNumberOfEntries)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_TRANSLATED_LOCAL_PORT_START:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatTranslatedLocalPortStart;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatTranslatedLocalPortStart *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatTranslatedLocalPortStart),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhSetNatTranslatedLocalPortStart.rval =
                nmhSetNatTranslatedLocalPortStart (lv.
                                                   nmhSetNatTranslatedLocalPortStart.
                                                   i4SetValNatTranslatedLocalPortStart);
            NatUnLock ();
            lv.nmhSetNatTranslatedLocalPortStart.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatTranslatedLocalPortStart;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatTranslatedLocalPortStart *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatTranslatedLocalPortStart)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_IDLE_TIME_OUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatIdleTimeOut;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatIdleTimeOut *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatIdleTimeOut), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhSetNatIdleTimeOut.rval =
                nmhSetNatIdleTimeOut (lv.nmhSetNatIdleTimeOut.
                                      i4SetValNatIdleTimeOut);
            NatUnLock ();
            lv.nmhSetNatIdleTimeOut.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatIdleTimeOut;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatIdleTimeOut *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatIdleTimeOut)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_TCP_TIME_OUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatTcpTimeOut;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatTcpTimeOut *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatTcpTimeOut), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhSetNatTcpTimeOut.rval =
                nmhSetNatTcpTimeOut (lv.nmhSetNatTcpTimeOut.
                                     i4SetValNatTcpTimeOut);
            NatUnLock ();
            lv.nmhSetNatTcpTimeOut.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatTcpTimeOut;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatTcpTimeOut *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatTcpTimeOut)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_UDP_TIME_OUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatUdpTimeOut;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatUdpTimeOut *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatUdpTimeOut), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhSetNatUdpTimeOut.rval =
                nmhSetNatUdpTimeOut (lv.nmhSetNatUdpTimeOut.
                                     i4SetValNatUdpTimeOut);
            NatUnLock ();
            lv.nmhSetNatUdpTimeOut.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatUdpTimeOut;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatUdpTimeOut *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatUdpTimeOut)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_TRC_FLAG:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatTrcFlag;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatTrcFlag *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatTrcFlag), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhSetNatTrcFlag.rval =
                nmhSetNatTrcFlag (lv.nmhSetNatTrcFlag.i4SetValNatTrcFlag);
            NatUnLock ();
            lv.nmhSetNatTrcFlag.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatTrcFlag;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatTrcFlag *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatTrcFlag)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_I_K_E_PORT_TRANSLATION:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatIKEPortTranslation;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatIKEPortTranslation *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatIKEPortTranslation), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhSetNatIKEPortTranslation.rval =
                nmhSetNatIKEPortTranslation (lv.nmhSetNatIKEPortTranslation.
                                             i4SetValNatIKEPortTranslation);
            NatUnLock ();
            lv.nmhSetNatIKEPortTranslation.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatIKEPortTranslation;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatIKEPortTranslation *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatIKEPortTranslation)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_I_K_E_TIMEOUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatIKETimeout;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatIKETimeout *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatIKETimeout), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhSetNatIKETimeout.rval =
                nmhSetNatIKETimeout (lv.nmhSetNatIKETimeout.
                                     i4SetValNatIKETimeout);
            lv.nmhSetNatIKETimeout.cliWebSetError = gi4CliWebSetError;
            NatUnLock ();

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatIKETimeout;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatIKETimeout *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatIKETimeout)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_I_P_SEC_TIMEOUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatIPSecTimeout;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatIPSecTimeout *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatIPSecTimeout), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhSetNatIPSecTimeout.rval =
                nmhSetNatIPSecTimeout (lv.nmhSetNatIPSecTimeout.
                                       i4SetValNatIPSecTimeout);
            NatUnLock ();
            lv.nmhSetNatIPSecTimeout.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatIPSecTimeout;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatIPSecTimeout *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatIPSecTimeout)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_I_P_SEC_PENDING_TIMEOUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatIPSecPendingTimeout;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatIPSecPendingTimeout *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatIPSecPendingTimeout), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhSetNatIPSecPendingTimeout.rval =
                nmhSetNatIPSecPendingTimeout (lv.nmhSetNatIPSecPendingTimeout.
                                              i4SetValNatIPSecPendingTimeout);
            NatUnLock ();
            lv.nmhSetNatIPSecPendingTimeout.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatIPSecPendingTimeout;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatIPSecPendingTimeout *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatIPSecPendingTimeout)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_I_P_SEC_MAX_RETRY:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatIPSecMaxRetry;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatIPSecMaxRetry *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatIPSecMaxRetry), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhSetNatIPSecMaxRetry.rval =
                nmhSetNatIPSecMaxRetry (lv.nmhSetNatIPSecMaxRetry.
                                        i4SetValNatIPSecMaxRetry);
            NatUnLock ();
            lv.nmhSetNatIPSecMaxRetry.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatIPSecMaxRetry;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatIPSecMaxRetry *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatIPSecMaxRetry)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_SIP_ALG_PORT:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetSipAlgPort;
            OsixKerUseInfo.pSrc = (tNatwnmhSetSipAlgPort *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetSipAlgPort), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhSetSipAlgPort.rval =
                nmhSetSipAlgPort (lv.nmhSetSipAlgPort.i4SetValSipAlgPort);

            NatUnLock ();
            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetSipAlgPort;
            OsixKerUseInfo.pDest = (tNatwnmhSetSipAlgPort *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetSipAlgPort)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_SET_NAT_SIP_ALG_PARTIAL_ENTRY_TIME_OUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatSipAlgPartialEntryTimeOut;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatSipAlgPartialEntryTimeOut *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatSipAlgPartialEntryTimeOut),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhSetNatSipAlgPartialEntryTimeOut.rval =
                nmhSetNatSipAlgPartialEntryTimeOut (lv.
                                                    nmhSetNatSipAlgPartialEntryTimeOut.
                                                    i4SetValNatSipAlgPartialEntryTimeOut);

            NatUnLock ();
            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatSipAlgPartialEntryTimeOut;
            OsixKerUseInfo.pDest =
                (tNatwnmhSetNatSipAlgPartialEntryTimeOut *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatSipAlgPartialEntryTimeOut)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_TESTV2_NAT_ENABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatEnable;
            OsixKerUseInfo.pSrc = (tNatwnmhTestv2NatEnable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatEnable), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            NatLock ();
            /* Invoke NPAPI */
            lv.nmhTestv2NatEnable.rval =
                nmhTestv2NatEnable (lv.nmhTestv2NatEnable.pu4ErrorCode,
                                    lv.nmhTestv2NatEnable.i4TestValNatEnable);
            NatUnLock ();
            lv.nmhTestv2NatEnable.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatEnable;
            OsixKerUseInfo.pDest = (tNatwnmhTestv2NatEnable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatEnable)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_TYPICAL_NUMBER_OF_ENTRIES:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatTypicalNumberOfEntries;
            OsixKerUseInfo.pSrc = (tNatwnmhTestv2NatTypicalNumberOfEntries *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatTypicalNumberOfEntries),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhTestv2NatTypicalNumberOfEntries.rval =
                nmhTestv2NatTypicalNumberOfEntries (lv.
                                                    nmhTestv2NatTypicalNumberOfEntries.
                                                    pu4ErrorCode,
                                                    lv.
                                                    nmhTestv2NatTypicalNumberOfEntries.
                                                    i4TestValNatTypicalNumberOfEntries);
            NatUnLock ();
            lv.nmhTestv2NatTypicalNumberOfEntries.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatTypicalNumberOfEntries;
            OsixKerUseInfo.pDest =
                (tNatwnmhTestv2NatTypicalNumberOfEntries *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatTypicalNumberOfEntries)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_TRANSLATED_LOCAL_PORT_START:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatTranslatedLocalPortStart;
            OsixKerUseInfo.pSrc =
                (tNatwnmhTestv2NatTranslatedLocalPortStart *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatTranslatedLocalPortStart),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhTestv2NatTranslatedLocalPortStart.rval =
                nmhTestv2NatTranslatedLocalPortStart (lv.
                                                      nmhTestv2NatTranslatedLocalPortStart.
                                                      pu4ErrorCode,
                                                      lv.
                                                      nmhTestv2NatTranslatedLocalPortStart.
                                                      i4TestValNatTranslatedLocalPortStart);
            NatUnLock ();
            lv.nmhTestv2NatTranslatedLocalPortStart.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatTranslatedLocalPortStart;
            OsixKerUseInfo.pDest =
                (tNatwnmhTestv2NatTranslatedLocalPortStart *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatTranslatedLocalPortStart)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_IDLE_TIME_OUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatIdleTimeOut;
            OsixKerUseInfo.pSrc = (tNatwnmhTestv2NatIdleTimeOut *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatIdleTimeOut), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhTestv2NatIdleTimeOut.rval =
                nmhTestv2NatIdleTimeOut (lv.nmhTestv2NatIdleTimeOut.
                                         pu4ErrorCode,
                                         lv.nmhTestv2NatIdleTimeOut.
                                         i4TestValNatIdleTimeOut);
            NatUnLock ();
            lv.nmhTestv2NatIdleTimeOut.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatIdleTimeOut;
            OsixKerUseInfo.pDest = (tNatwnmhTestv2NatIdleTimeOut *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatIdleTimeOut)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_TCP_TIME_OUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatTcpTimeOut;
            OsixKerUseInfo.pSrc = (tNatwnmhTestv2NatTcpTimeOut *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatTcpTimeOut), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhTestv2NatTcpTimeOut.rval =
                nmhTestv2NatTcpTimeOut (lv.nmhTestv2NatTcpTimeOut.pu4ErrorCode,
                                        lv.nmhTestv2NatTcpTimeOut.
                                        i4TestValNatTcpTimeOut);
            NatUnLock ();
            lv.nmhTestv2NatTcpTimeOut.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatTcpTimeOut;
            OsixKerUseInfo.pDest = (tNatwnmhTestv2NatTcpTimeOut *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatTcpTimeOut)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_UDP_TIME_OUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatUdpTimeOut;
            OsixKerUseInfo.pSrc = (tNatwnmhTestv2NatUdpTimeOut *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatUdpTimeOut), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhTestv2NatUdpTimeOut.rval =
                nmhTestv2NatUdpTimeOut (lv.nmhTestv2NatUdpTimeOut.pu4ErrorCode,
                                        lv.nmhTestv2NatUdpTimeOut.
                                        i4TestValNatUdpTimeOut);
            NatUnLock ();
            lv.nmhTestv2NatUdpTimeOut.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatUdpTimeOut;
            OsixKerUseInfo.pDest = (tNatwnmhTestv2NatUdpTimeOut *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatUdpTimeOut)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_TRC_FLAG:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatTrcFlag;
            OsixKerUseInfo.pSrc = (tNatwnmhTestv2NatTrcFlag *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatTrcFlag), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            NatLock ();
            /* Invoke NPAPI */
            lv.nmhTestv2NatTrcFlag.rval =
                nmhTestv2NatTrcFlag (lv.nmhTestv2NatTrcFlag.pu4ErrorCode,
                                     lv.nmhTestv2NatTrcFlag.
                                     i4TestValNatTrcFlag);
            NatUnLock ();
            lv.nmhTestv2NatTrcFlag.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatTrcFlag;
            OsixKerUseInfo.pDest = (tNatwnmhTestv2NatTrcFlag *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatTrcFlag)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_I_K_E_PORT_TRANSLATION:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatIKEPortTranslation;
            OsixKerUseInfo.pSrc = (tNatwnmhTestv2NatIKEPortTranslation *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatIKEPortTranslation),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhTestv2NatIKEPortTranslation.rval =
                nmhTestv2NatIKEPortTranslation (lv.
                                                nmhTestv2NatIKEPortTranslation.
                                                pu4ErrorCode,
                                                lv.
                                                nmhTestv2NatIKEPortTranslation.
                                                i4TestValNatIKEPortTranslation);
            NatUnLock ();
            lv.nmhTestv2NatIKEPortTranslation.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatIKEPortTranslation;
            OsixKerUseInfo.pDest = (tNatwnmhTestv2NatIKEPortTranslation *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatIKEPortTranslation)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_I_K_E_TIMEOUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatIKETimeout;
            OsixKerUseInfo.pSrc = (tNatwnmhTestv2NatIKETimeout *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatIKETimeout), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhTestv2NatIKETimeout.rval =
                nmhTestv2NatIKETimeout (lv.nmhTestv2NatIKETimeout.pu4ErrorCode,
                                        lv.nmhTestv2NatIKETimeout.
                                        i4TestValNatIKETimeout);
            NatUnLock ();
            lv.nmhTestv2NatIKETimeout.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatIKETimeout;
            OsixKerUseInfo.pDest = (tNatwnmhTestv2NatIKETimeout *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatIKETimeout)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_I_P_SEC_TIMEOUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatIPSecTimeout;
            OsixKerUseInfo.pSrc = (tNatwnmhTestv2NatIPSecTimeout *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatIPSecTimeout), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhTestv2NatIPSecTimeout.rval =
                nmhTestv2NatIPSecTimeout (lv.nmhTestv2NatIPSecTimeout.
                                          pu4ErrorCode,
                                          lv.nmhTestv2NatIPSecTimeout.
                                          i4TestValNatIPSecTimeout);
            NatUnLock ();
            lv.nmhTestv2NatIPSecTimeout.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatIPSecTimeout;
            OsixKerUseInfo.pDest = (tNatwnmhTestv2NatIPSecTimeout *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatIPSecTimeout)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_I_P_SEC_PENDING_TIMEOUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatIPSecPendingTimeout;
            OsixKerUseInfo.pSrc = (tNatwnmhTestv2NatIPSecPendingTimeout *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatIPSecPendingTimeout),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhTestv2NatIPSecPendingTimeout.rval =
                nmhTestv2NatIPSecPendingTimeout (lv.
                                                 nmhTestv2NatIPSecPendingTimeout.
                                                 pu4ErrorCode,
                                                 lv.
                                                 nmhTestv2NatIPSecPendingTimeout.
                                                 i4TestValNatIPSecPendingTimeout);
            NatUnLock ();
            lv.nmhTestv2NatIPSecPendingTimeout.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatIPSecPendingTimeout;
            OsixKerUseInfo.pDest = (tNatwnmhTestv2NatIPSecPendingTimeout *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatIPSecPendingTimeout)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_I_P_SEC_MAX_RETRY:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatIPSecMaxRetry;
            OsixKerUseInfo.pSrc = (tNatwnmhTestv2NatIPSecMaxRetry *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatIPSecMaxRetry), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhTestv2NatIPSecMaxRetry.rval =
                nmhTestv2NatIPSecMaxRetry (lv.nmhTestv2NatIPSecMaxRetry.
                                           pu4ErrorCode,
                                           lv.nmhTestv2NatIPSecMaxRetry.
                                           i4TestValNatIPSecMaxRetry);
            NatUnLock ();
            lv.nmhTestv2NatIPSecMaxRetry.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatIPSecMaxRetry;
            OsixKerUseInfo.pDest = (tNatwnmhTestv2NatIPSecMaxRetry *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatIPSecMaxRetry)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_SIP_ALG_PORT:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2SipAlgPort;
            OsixKerUseInfo.pSrc = (tNatwnmhTestv2SipAlgPort *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2SipAlgPort), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhTestv2SipAlgPort.rval =
                nmhTestv2SipAlgPort (lv.nmhTestv2SipAlgPort.pu4ErrorCode,
                                     lv.nmhTestv2SipAlgPort.
                                     i4TestValSipAlgPort);

            NatUnLock ();
            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2SipAlgPort;
            OsixKerUseInfo.pDest = (tNatwnmhTestv2SipAlgPort *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2SipAlgPort)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_SIP_ALG_PARTIAL_ENTRY_TIME_OUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatSipAlgPartialEntryTimeOut;
            OsixKerUseInfo.pSrc =
                (tNatwnmhTestv2NatSipAlgPartialEntryTimeOut *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatSipAlgPartialEntryTimeOut),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhTestv2NatSipAlgPartialEntryTimeOut.rval =
                nmhTestv2NatSipAlgPartialEntryTimeOut (lv.
                                                       nmhTestv2NatSipAlgPartialEntryTimeOut.
                                                       pu4ErrorCode,
                                                       lv.
                                                       nmhTestv2NatSipAlgPartialEntryTimeOut.
                                                       i4TestValNatSipAlgPartialEntryTimeOut);

            NatUnLock ();
            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatSipAlgPartialEntryTimeOut;
            OsixKerUseInfo.pDest =
                (tNatwnmhTestv2NatSipAlgPartialEntryTimeOut *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatSipAlgPartialEntryTimeOut)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_VALIDATE_INDEX_INSTANCE_NAT_DYNAMIC_TRANS_TABLE:
        {
            OsixKerUseInfo.pDest =
                &lv.nmhValidateIndexInstanceNatDynamicTransTable;
            OsixKerUseInfo.pSrc =
                (tNatwnmhValidateIndexInstanceNatDynamicTransTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhValidateIndexInstanceNatDynamicTransTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhValidateIndexInstanceNatDynamicTransTable.rval =
                nmhValidateIndexInstanceNatDynamicTransTable (lv.
                                                              nmhValidateIndexInstanceNatDynamicTransTable.
                                                              i4NatDynamicTransInterfaceNum,
                                                              lv.
                                                              nmhValidateIndexInstanceNatDynamicTransTable.
                                                              u4NatDynamicTransLocalIp,
                                                              lv.
                                                              nmhValidateIndexInstanceNatDynamicTransTable.
                                                              i4NatDynamicTransLocalPort,
                                                              lv.
                                                              nmhValidateIndexInstanceNatDynamicTransTable.
                                                              u4NatDynamicTransOutsideIp,
                                                              lv.
                                                              nmhValidateIndexInstanceNatDynamicTransTable.
                                                              i4NatDynamicTransOutsidePort);
            NatUnLock ();
            lv.nmhValidateIndexInstanceNatDynamicTransTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc =
                &lv.nmhValidateIndexInstanceNatDynamicTransTable;
            OsixKerUseInfo.pDest =
                (tNatwnmhValidateIndexInstanceNatDynamicTransTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhValidateIndexInstanceNatDynamicTransTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FIRST_INDEX_NAT_DYNAMIC_TRANS_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFirstIndexNatDynamicTransTable;
            OsixKerUseInfo.pSrc =
                (tNatwnmhGetFirstIndexNatDynamicTransTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetFirstIndexNatDynamicTransTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetFirstIndexNatDynamicTransTable.rval =
                nmhGetFirstIndexNatDynamicTransTable (lv.
                                                      nmhGetFirstIndexNatDynamicTransTable.
                                                      pi4NatDynamicTransInterfaceNum,
                                                      lv.
                                                      nmhGetFirstIndexNatDynamicTransTable.
                                                      pu4NatDynamicTransLocalIp,
                                                      lv.
                                                      nmhGetFirstIndexNatDynamicTransTable.
                                                      pi4NatDynamicTransLocalPort,
                                                      lv.
                                                      nmhGetFirstIndexNatDynamicTransTable.
                                                      pu4NatDynamicTransOutsideIp,
                                                      lv.
                                                      nmhGetFirstIndexNatDynamicTransTable.
                                                      pi4NatDynamicTransOutsidePort);
            NatUnLock ();
            lv.nmhGetFirstIndexNatDynamicTransTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetFirstIndexNatDynamicTransTable;
            OsixKerUseInfo.pDest =
                (tNatwnmhGetFirstIndexNatDynamicTransTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetFirstIndexNatDynamicTransTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NEXT_INDEX_NAT_DYNAMIC_TRANS_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNextIndexNatDynamicTransTable;
            OsixKerUseInfo.pSrc =
                (tNatwnmhGetNextIndexNatDynamicTransTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNextIndexNatDynamicTransTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            NatLock ();
            /* Invoke NPAPI */
            lv.nmhGetNextIndexNatDynamicTransTable.rval =
                nmhGetNextIndexNatDynamicTransTable (lv.
                                                     nmhGetNextIndexNatDynamicTransTable.
                                                     i4NatDynamicTransInterfaceNum,
                                                     lv.
                                                     nmhGetNextIndexNatDynamicTransTable.
                                                     pi4NextNatDynamicTransInterfaceNum,
                                                     lv.
                                                     nmhGetNextIndexNatDynamicTransTable.
                                                     u4NatDynamicTransLocalIp,
                                                     lv.
                                                     nmhGetNextIndexNatDynamicTransTable.
                                                     pu4NextNatDynamicTransLocalIp,
                                                     lv.
                                                     nmhGetNextIndexNatDynamicTransTable.
                                                     i4NatDynamicTransLocalPort,
                                                     lv.
                                                     nmhGetNextIndexNatDynamicTransTable.
                                                     pi4NextNatDynamicTransLocalPort,
                                                     lv.
                                                     nmhGetNextIndexNatDynamicTransTable.
                                                     u4NatDynamicTransOutsideIp,
                                                     lv.
                                                     nmhGetNextIndexNatDynamicTransTable.
                                                     pu4NextNatDynamicTransOutsideIp,
                                                     lv.
                                                     nmhGetNextIndexNatDynamicTransTable.
                                                     i4NatDynamicTransOutsidePort,
                                                     lv.
                                                     nmhGetNextIndexNatDynamicTransTable.
                                                     pi4NextNatDynamicTransOutsidePort);
            NatUnLock ();
            lv.nmhGetNextIndexNatDynamicTransTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNextIndexNatDynamicTransTable;
            OsixKerUseInfo.pDest =
                (tNatwnmhGetNextIndexNatDynamicTransTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNextIndexNatDynamicTransTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_DYNAMIC_TRANS_TRANSLATED_LOCAL_IP:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatDynamicTransTranslatedLocalIp;
            OsixKerUseInfo.pSrc =
                (tNatwnmhGetNatDynamicTransTranslatedLocalIp *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatDynamicTransTranslatedLocalIp),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            NatLock ();

            /* Invoke NPAPI */
            lv.nmhGetNatDynamicTransTranslatedLocalIp.rval =
                nmhGetNatDynamicTransTranslatedLocalIp (lv.
                                                        nmhGetNatDynamicTransTranslatedLocalIp.
                                                        i4NatDynamicTransInterfaceNum,
                                                        lv.
                                                        nmhGetNatDynamicTransTranslatedLocalIp.
                                                        u4NatDynamicTransLocalIp,
                                                        lv.
                                                        nmhGetNatDynamicTransTranslatedLocalIp.
                                                        i4NatDynamicTransLocalPort,
                                                        lv.
                                                        nmhGetNatDynamicTransTranslatedLocalIp.
                                                        u4NatDynamicTransOutsideIp,
                                                        lv.
                                                        nmhGetNatDynamicTransTranslatedLocalIp.
                                                        i4NatDynamicTransOutsidePort,
                                                        lv.
                                                        nmhGetNatDynamicTransTranslatedLocalIp.
                                                        pu4RetValNatDynamicTransTranslatedLocalIp);
            NatUnLock ();
            lv.nmhGetNatDynamicTransTranslatedLocalIp.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatDynamicTransTranslatedLocalIp;
            OsixKerUseInfo.pDest =
                (tNatwnmhGetNatDynamicTransTranslatedLocalIp *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatDynamicTransTranslatedLocalIp)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_DYNAMIC_TRANS_TRANSLATED_LOCAL_PORT:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatDynamicTransTranslatedLocalPort;
            OsixKerUseInfo.pSrc =
                (tNatwnmhGetNatDynamicTransTranslatedLocalPort *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatDynamicTransTranslatedLocalPort),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatDynamicTransTranslatedLocalPort.rval =
                nmhGetNatDynamicTransTranslatedLocalPort (lv.
                                                          nmhGetNatDynamicTransTranslatedLocalPort.
                                                          i4NatDynamicTransInterfaceNum,
                                                          lv.
                                                          nmhGetNatDynamicTransTranslatedLocalPort.
                                                          u4NatDynamicTransLocalIp,
                                                          lv.
                                                          nmhGetNatDynamicTransTranslatedLocalPort.
                                                          i4NatDynamicTransLocalPort,
                                                          lv.
                                                          nmhGetNatDynamicTransTranslatedLocalPort.
                                                          u4NatDynamicTransOutsideIp,
                                                          lv.
                                                          nmhGetNatDynamicTransTranslatedLocalPort.
                                                          i4NatDynamicTransOutsidePort,
                                                          lv.
                                                          nmhGetNatDynamicTransTranslatedLocalPort.
                                                          pi4RetValNatDynamicTransTranslatedLocalPort);
            NatUnLock ();
            lv.nmhGetNatDynamicTransTranslatedLocalPort.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatDynamicTransTranslatedLocalPort;
            OsixKerUseInfo.pDest =
                (tNatwnmhGetNatDynamicTransTranslatedLocalPort *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatDynamicTransTranslatedLocalPort)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_DYNAMIC_TRANS_LAST_USE_TIME:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatDynamicTransLastUseTime;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatDynamicTransLastUseTime *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatDynamicTransLastUseTime),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatDynamicTransLastUseTime.rval =
                nmhGetNatDynamicTransLastUseTime (lv.
                                                  nmhGetNatDynamicTransLastUseTime.
                                                  i4NatDynamicTransInterfaceNum,
                                                  lv.
                                                  nmhGetNatDynamicTransLastUseTime.
                                                  u4NatDynamicTransLocalIp,
                                                  lv.
                                                  nmhGetNatDynamicTransLastUseTime.
                                                  i4NatDynamicTransLocalPort,
                                                  lv.
                                                  nmhGetNatDynamicTransLastUseTime.
                                                  u4NatDynamicTransOutsideIp,
                                                  lv.
                                                  nmhGetNatDynamicTransLastUseTime.
                                                  i4NatDynamicTransOutsidePort,
                                                  lv.
                                                  nmhGetNatDynamicTransLastUseTime.
                                                  pi4RetValNatDynamicTransLastUseTime);
            NatUnLock ();
            lv.nmhGetNatDynamicTransLastUseTime.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatDynamicTransLastUseTime;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatDynamicTransLastUseTime *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatDynamicTransLastUseTime)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_VALIDATE_INDEX_INSTANCE_NAT_GLOBAL_ADDRESS_TABLE:
        {
            OsixKerUseInfo.pDest =
                &lv.nmhValidateIndexInstanceNatGlobalAddressTable;
            OsixKerUseInfo.pSrc =
                (tNatwnmhValidateIndexInstanceNatGlobalAddressTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhValidateIndexInstanceNatGlobalAddressTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            NatLock ();
            /* Invoke NPAPI */
            lv.nmhValidateIndexInstanceNatGlobalAddressTable.rval =
                nmhValidateIndexInstanceNatGlobalAddressTable (lv.
                                                               nmhValidateIndexInstanceNatGlobalAddressTable.
                                                               i4NatGlobalAddressInterfaceNum,
                                                               lv.
                                                               nmhValidateIndexInstanceNatGlobalAddressTable.
                                                               u4NatGlobalAddressTranslatedLocalIp);
            NatUnLock ();
            lv.nmhValidateIndexInstanceNatGlobalAddressTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc =
                &lv.nmhValidateIndexInstanceNatGlobalAddressTable;
            OsixKerUseInfo.pDest =
                (tNatwnmhValidateIndexInstanceNatGlobalAddressTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhValidateIndexInstanceNatGlobalAddressTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FIRST_INDEX_NAT_GLOBAL_ADDRESS_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFirstIndexNatGlobalAddressTable;
            OsixKerUseInfo.pSrc =
                (tNatwnmhGetFirstIndexNatGlobalAddressTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetFirstIndexNatGlobalAddressTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetFirstIndexNatGlobalAddressTable.rval =
                nmhGetFirstIndexNatGlobalAddressTable (lv.
                                                       nmhGetFirstIndexNatGlobalAddressTable.
                                                       pi4NatGlobalAddressInterfaceNum,
                                                       lv.
                                                       nmhGetFirstIndexNatGlobalAddressTable.
                                                       pu4NatGlobalAddressTranslatedLocalIp);
            NatUnLock ();
            lv.nmhGetFirstIndexNatGlobalAddressTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetFirstIndexNatGlobalAddressTable;
            OsixKerUseInfo.pDest =
                (tNatwnmhGetFirstIndexNatGlobalAddressTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetFirstIndexNatGlobalAddressTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NEXT_INDEX_NAT_GLOBAL_ADDRESS_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNextIndexNatGlobalAddressTable;
            OsixKerUseInfo.pSrc =
                (tNatwnmhGetNextIndexNatGlobalAddressTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNextIndexNatGlobalAddressTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNextIndexNatGlobalAddressTable.rval =
                nmhGetNextIndexNatGlobalAddressTable (lv.
                                                      nmhGetNextIndexNatGlobalAddressTable.
                                                      i4NatGlobalAddressInterfaceNum,
                                                      lv.
                                                      nmhGetNextIndexNatGlobalAddressTable.
                                                      pi4NextNatGlobalAddressInterfaceNum,
                                                      lv.
                                                      nmhGetNextIndexNatGlobalAddressTable.
                                                      u4NatGlobalAddressTranslatedLocalIp,
                                                      lv.
                                                      nmhGetNextIndexNatGlobalAddressTable.
                                                      pu4NextNatGlobalAddressTranslatedLocalIp);
            NatUnLock ();
            lv.nmhGetNextIndexNatGlobalAddressTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNextIndexNatGlobalAddressTable;
            OsixKerUseInfo.pDest =
                (tNatwnmhGetNextIndexNatGlobalAddressTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNextIndexNatGlobalAddressTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_GLOBAL_ADDRESS_MASK:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatGlobalAddressMask;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatGlobalAddressMask *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatGlobalAddressMask), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatGlobalAddressMask.rval =
                nmhGetNatGlobalAddressMask (lv.nmhGetNatGlobalAddressMask.
                                            i4NatGlobalAddressInterfaceNum,
                                            lv.nmhGetNatGlobalAddressMask.
                                            u4NatGlobalAddressTranslatedLocalIp,
                                            lv.nmhGetNatGlobalAddressMask.
                                            pu4RetValNatGlobalAddressMask);
            NatUnLock ();
            lv.nmhGetNatGlobalAddressMask.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatGlobalAddressMask;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatGlobalAddressMask *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatGlobalAddressMask)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_GLOBAL_ADDRESS_ENTRY_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatGlobalAddressEntryStatus;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatGlobalAddressEntryStatus *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatGlobalAddressEntryStatus),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatGlobalAddressEntryStatus.rval =
                nmhGetNatGlobalAddressEntryStatus (lv.
                                                   nmhGetNatGlobalAddressEntryStatus.
                                                   i4NatGlobalAddressInterfaceNum,
                                                   lv.
                                                   nmhGetNatGlobalAddressEntryStatus.
                                                   u4NatGlobalAddressTranslatedLocalIp,
                                                   lv.
                                                   nmhGetNatGlobalAddressEntryStatus.
                                                   pi4RetValNatGlobalAddressEntryStatus);
            NatUnLock ();
            lv.nmhGetNatGlobalAddressEntryStatus.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatGlobalAddressEntryStatus;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatGlobalAddressEntryStatus *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatGlobalAddressEntryStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_GLOBAL_ADDRESS_MASK:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatGlobalAddressMask;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatGlobalAddressMask *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatGlobalAddressMask), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhSetNatGlobalAddressMask.rval =
                nmhSetNatGlobalAddressMask (lv.nmhSetNatGlobalAddressMask.
                                            i4NatGlobalAddressInterfaceNum,
                                            lv.nmhSetNatGlobalAddressMask.
                                            u4NatGlobalAddressTranslatedLocalIp,
                                            lv.nmhSetNatGlobalAddressMask.
                                            u4SetValNatGlobalAddressMask);
            NatUnLock ();

            lv.nmhSetNatGlobalAddressMask.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatGlobalAddressMask;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatGlobalAddressMask *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatGlobalAddressMask)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_GLOBAL_ADDRESS_ENTRY_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatGlobalAddressEntryStatus;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatGlobalAddressEntryStatus *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatGlobalAddressEntryStatus),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhSetNatGlobalAddressEntryStatus.rval =
                nmhSetNatGlobalAddressEntryStatus (lv.
                                                   nmhSetNatGlobalAddressEntryStatus.
                                                   i4NatGlobalAddressInterfaceNum,
                                                   lv.
                                                   nmhSetNatGlobalAddressEntryStatus.
                                                   u4NatGlobalAddressTranslatedLocalIp,
                                                   lv.
                                                   nmhSetNatGlobalAddressEntryStatus.
                                                   i4SetValNatGlobalAddressEntryStatus);
            NatUnLock ();
            lv.nmhSetNatGlobalAddressEntryStatus.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatGlobalAddressEntryStatus;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatGlobalAddressEntryStatus *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatGlobalAddressEntryStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_GLOBAL_ADDRESS_MASK:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatGlobalAddressMask;
            OsixKerUseInfo.pSrc = (tNatwnmhTestv2NatGlobalAddressMask *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatGlobalAddressMask),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhTestv2NatGlobalAddressMask.rval =
                nmhTestv2NatGlobalAddressMask (lv.nmhTestv2NatGlobalAddressMask.
                                               pu4ErrorCode,
                                               lv.nmhTestv2NatGlobalAddressMask.
                                               i4NatGlobalAddressInterfaceNum,
                                               lv.nmhTestv2NatGlobalAddressMask.
                                               u4NatGlobalAddressTranslatedLocalIp,
                                               lv.nmhTestv2NatGlobalAddressMask.
                                               u4TestValNatGlobalAddressMask);
            NatUnLock ();
            lv.nmhTestv2NatGlobalAddressMask.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatGlobalAddressMask;
            OsixKerUseInfo.pDest = (tNatwnmhTestv2NatGlobalAddressMask *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatGlobalAddressMask)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_GLOBAL_ADDRESS_ENTRY_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatGlobalAddressEntryStatus;
            OsixKerUseInfo.pSrc =
                (tNatwnmhTestv2NatGlobalAddressEntryStatus *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatGlobalAddressEntryStatus),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhTestv2NatGlobalAddressEntryStatus.rval =
                nmhTestv2NatGlobalAddressEntryStatus (lv.
                                                      nmhTestv2NatGlobalAddressEntryStatus.
                                                      pu4ErrorCode,
                                                      lv.
                                                      nmhTestv2NatGlobalAddressEntryStatus.
                                                      i4NatGlobalAddressInterfaceNum,
                                                      lv.
                                                      nmhTestv2NatGlobalAddressEntryStatus.
                                                      u4NatGlobalAddressTranslatedLocalIp,
                                                      lv.
                                                      nmhTestv2NatGlobalAddressEntryStatus.
                                                      i4TestValNatGlobalAddressEntryStatus);
            NatUnLock ();
            lv.nmhTestv2NatGlobalAddressEntryStatus.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatGlobalAddressEntryStatus;
            OsixKerUseInfo.pDest =
                (tNatwnmhTestv2NatGlobalAddressEntryStatus *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatGlobalAddressEntryStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_VALIDATE_INDEX_INSTANCE_NAT_LOCAL_ADDRESS_TABLE:
        {
            OsixKerUseInfo.pDest =
                &lv.nmhValidateIndexInstanceNatLocalAddressTable;
            OsixKerUseInfo.pSrc =
                (tNatwnmhValidateIndexInstanceNatLocalAddressTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhValidateIndexInstanceNatLocalAddressTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            NatLock ();
            /* Invoke NPAPI */
            lv.nmhValidateIndexInstanceNatLocalAddressTable.rval =
                nmhValidateIndexInstanceNatLocalAddressTable (lv.
                                                              nmhValidateIndexInstanceNatLocalAddressTable.
                                                              i4NatLocalAddressInterfaceNumber,
                                                              lv.
                                                              nmhValidateIndexInstanceNatLocalAddressTable.
                                                              u4NatLocalAddressLocalIp);
            NatUnLock ();
            lv.nmhValidateIndexInstanceNatLocalAddressTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc =
                &lv.nmhValidateIndexInstanceNatLocalAddressTable;
            OsixKerUseInfo.pDest =
                (tNatwnmhValidateIndexInstanceNatLocalAddressTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhValidateIndexInstanceNatLocalAddressTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FIRST_INDEX_NAT_LOCAL_ADDRESS_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFirstIndexNatLocalAddressTable;
            OsixKerUseInfo.pSrc =
                (tNatwnmhGetFirstIndexNatLocalAddressTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetFirstIndexNatLocalAddressTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetFirstIndexNatLocalAddressTable.rval =
                nmhGetFirstIndexNatLocalAddressTable (lv.
                                                      nmhGetFirstIndexNatLocalAddressTable.
                                                      pi4NatLocalAddressInterfaceNumber,
                                                      lv.
                                                      nmhGetFirstIndexNatLocalAddressTable.
                                                      pu4NatLocalAddressLocalIp);
            NatUnLock ();
            lv.nmhGetFirstIndexNatLocalAddressTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetFirstIndexNatLocalAddressTable;
            OsixKerUseInfo.pDest =
                (tNatwnmhGetFirstIndexNatLocalAddressTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetFirstIndexNatLocalAddressTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NEXT_INDEX_NAT_LOCAL_ADDRESS_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNextIndexNatLocalAddressTable;
            OsixKerUseInfo.pSrc =
                (tNatwnmhGetNextIndexNatLocalAddressTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNextIndexNatLocalAddressTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNextIndexNatLocalAddressTable.rval =
                nmhGetNextIndexNatLocalAddressTable (lv.
                                                     nmhGetNextIndexNatLocalAddressTable.
                                                     i4NatLocalAddressInterfaceNumber,
                                                     lv.
                                                     nmhGetNextIndexNatLocalAddressTable.
                                                     pi4NextNatLocalAddressInterfaceNumber,
                                                     lv.
                                                     nmhGetNextIndexNatLocalAddressTable.
                                                     u4NatLocalAddressLocalIp,
                                                     lv.
                                                     nmhGetNextIndexNatLocalAddressTable.
                                                     pu4NextNatLocalAddressLocalIp);
            NatUnLock ();
            lv.nmhGetNextIndexNatLocalAddressTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNextIndexNatLocalAddressTable;
            OsixKerUseInfo.pDest =
                (tNatwnmhGetNextIndexNatLocalAddressTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNextIndexNatLocalAddressTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_LOCAL_ADDRESS_MASK:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatLocalAddressMask;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatLocalAddressMask *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatLocalAddressMask), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatLocalAddressMask.rval =
                nmhGetNatLocalAddressMask (lv.nmhGetNatLocalAddressMask.
                                           i4NatLocalAddressInterfaceNumber,
                                           lv.nmhGetNatLocalAddressMask.
                                           u4NatLocalAddressLocalIp,
                                           lv.nmhGetNatLocalAddressMask.
                                           pu4RetValNatLocalAddressMask);
            NatUnLock ();
            lv.nmhGetNatLocalAddressMask.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatLocalAddressMask;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatLocalAddressMask *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatLocalAddressMask)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_LOCAL_ADDRESS_ENTRY_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatLocalAddressEntryStatus;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatLocalAddressEntryStatus *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatLocalAddressEntryStatus),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatLocalAddressEntryStatus.rval =
                nmhGetNatLocalAddressEntryStatus (lv.
                                                  nmhGetNatLocalAddressEntryStatus.
                                                  i4NatLocalAddressInterfaceNumber,
                                                  lv.
                                                  nmhGetNatLocalAddressEntryStatus.
                                                  u4NatLocalAddressLocalIp,
                                                  lv.
                                                  nmhGetNatLocalAddressEntryStatus.
                                                  pi4RetValNatLocalAddressEntryStatus);
            NatUnLock ();
            lv.nmhGetNatLocalAddressEntryStatus.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatLocalAddressEntryStatus;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatLocalAddressEntryStatus *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatLocalAddressEntryStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_LOCAL_ADDRESS_MASK:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatLocalAddressMask;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatLocalAddressMask *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatLocalAddressMask), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            NatLock ();
            /* Invoke NPAPI */
            lv.nmhSetNatLocalAddressMask.rval =
                nmhSetNatLocalAddressMask (lv.nmhSetNatLocalAddressMask.
                                           i4NatLocalAddressInterfaceNumber,
                                           lv.nmhSetNatLocalAddressMask.
                                           u4NatLocalAddressLocalIp,
                                           lv.nmhSetNatLocalAddressMask.
                                           u4SetValNatLocalAddressMask);
            NatUnLock ();
            lv.nmhSetNatLocalAddressMask.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatLocalAddressMask;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatLocalAddressMask *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatLocalAddressMask)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_LOCAL_ADDRESS_ENTRY_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatLocalAddressMask;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatLocalAddressEntryStatus *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatLocalAddressEntryStatus),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            NatLock ();
            /* Invoke NPAPI */
            lv.nmhSetNatLocalAddressEntryStatus.rval =
                nmhSetNatLocalAddressEntryStatus (lv.
                                                  nmhSetNatLocalAddressEntryStatus.
                                                  i4NatLocalAddressInterfaceNumber,
                                                  lv.
                                                  nmhSetNatLocalAddressEntryStatus.
                                                  u4NatLocalAddressLocalIp,
                                                  lv.
                                                  nmhSetNatLocalAddressEntryStatus.
                                                  i4SetValNatLocalAddressEntryStatus);
            NatUnLock ();
            lv.nmhSetNatLocalAddressEntryStatus.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatLocalAddressMask;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatLocalAddressEntryStatus *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatLocalAddressEntryStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_LOCAL_ADDRESS_MASK:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatLocalAddressMask;
            OsixKerUseInfo.pSrc = (tNatwnmhTestv2NatLocalAddressMask *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatLocalAddressMask), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            NatLock ();
            /* Invoke NPAPI */
            lv.nmhTestv2NatLocalAddressMask.rval =
                nmhTestv2NatLocalAddressMask (lv.nmhTestv2NatLocalAddressMask.
                                              pu4ErrorCode,
                                              lv.nmhTestv2NatLocalAddressMask.
                                              i4NatLocalAddressInterfaceNumber,
                                              lv.nmhTestv2NatLocalAddressMask.
                                              u4NatLocalAddressLocalIp,
                                              lv.nmhTestv2NatLocalAddressMask.
                                              u4TestValNatLocalAddressMask);
            NatUnLock ();
            lv.nmhTestv2NatLocalAddressMask.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatLocalAddressMask;
            OsixKerUseInfo.pDest = (tNatwnmhTestv2NatLocalAddressMask *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatLocalAddressMask)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_LOCAL_ADDRESS_ENTRY_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatLocalAddressEntryStatus;
            OsixKerUseInfo.pSrc =
                (tNatwnmhTestv2NatLocalAddressEntryStatus *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatLocalAddressEntryStatus),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhTestv2NatLocalAddressEntryStatus.rval =
                nmhTestv2NatLocalAddressEntryStatus (lv.
                                                     nmhTestv2NatLocalAddressEntryStatus.
                                                     pu4ErrorCode,
                                                     lv.
                                                     nmhTestv2NatLocalAddressEntryStatus.
                                                     i4NatLocalAddressInterfaceNumber,
                                                     lv.
                                                     nmhTestv2NatLocalAddressEntryStatus.
                                                     u4NatLocalAddressLocalIp,
                                                     lv.
                                                     nmhTestv2NatLocalAddressEntryStatus.
                                                     i4TestValNatLocalAddressEntryStatus);
            NatUnLock ();
            lv.nmhTestv2NatLocalAddressEntryStatus.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatLocalAddressEntryStatus;
            OsixKerUseInfo.pDest =
                (tNatwnmhTestv2NatLocalAddressEntryStatus *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatLocalAddressEntryStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_VALIDATE_INDEX_INSTANCE_NAT_STATIC_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhValidateIndexInstanceNatStaticTable;
            OsixKerUseInfo.pSrc =
                (tNatwnmhValidateIndexInstanceNatStaticTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhValidateIndexInstanceNatStaticTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhValidateIndexInstanceNatStaticTable.rval =
                nmhValidateIndexInstanceNatStaticTable (lv.
                                                        nmhValidateIndexInstanceNatStaticTable.
                                                        i4NatStaticInterfaceNum,
                                                        lv.
                                                        nmhValidateIndexInstanceNatStaticTable.
                                                        u4NatStaticLocalIp);
            NatUnLock ();
            lv.nmhValidateIndexInstanceNatStaticTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhValidateIndexInstanceNatStaticTable;
            OsixKerUseInfo.pDest =
                (tNatwnmhValidateIndexInstanceNatStaticTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhValidateIndexInstanceNatStaticTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FIRST_INDEX_NAT_STATIC_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFirstIndexNatStaticTable;
            OsixKerUseInfo.pSrc = (tNatwnmhGetFirstIndexNatStaticTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetFirstIndexNatStaticTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetFirstIndexNatStaticTable.rval =
                nmhGetFirstIndexNatStaticTable (lv.
                                                nmhGetFirstIndexNatStaticTable.
                                                pi4NatStaticInterfaceNum,
                                                lv.
                                                nmhGetFirstIndexNatStaticTable.
                                                pu4NatStaticLocalIp);
            NatUnLock ();
            lv.nmhGetFirstIndexNatStaticTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetFirstIndexNatStaticTable;
            OsixKerUseInfo.pDest = (tNatwnmhGetFirstIndexNatStaticTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetFirstIndexNatStaticTable)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NEXT_INDEX_NAT_STATIC_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNextIndexNatStaticTable;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNextIndexNatStaticTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNextIndexNatStaticTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNextIndexNatStaticTable.rval =
                nmhGetNextIndexNatStaticTable (lv.nmhGetNextIndexNatStaticTable.
                                               i4NatStaticInterfaceNum,
                                               lv.nmhGetNextIndexNatStaticTable.
                                               pi4NextNatStaticInterfaceNum,
                                               lv.nmhGetNextIndexNatStaticTable.
                                               u4NatStaticLocalIp,
                                               lv.nmhGetNextIndexNatStaticTable.
                                               pu4NextNatStaticLocalIp);
            NatUnLock ();
            lv.nmhGetNextIndexNatStaticTable.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNextIndexNatStaticTable;
            OsixKerUseInfo.pDest = (tNatwnmhGetNextIndexNatStaticTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNextIndexNatStaticTable)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_STATIC_TRANSLATED_LOCAL_IP:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatStaticTranslatedLocalIp;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatStaticTranslatedLocalIp *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatStaticTranslatedLocalIp),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatStaticTranslatedLocalIp.rval =
                nmhGetNatStaticTranslatedLocalIp (lv.
                                                  nmhGetNatStaticTranslatedLocalIp.
                                                  i4NatStaticInterfaceNum,
                                                  lv.
                                                  nmhGetNatStaticTranslatedLocalIp.
                                                  u4NatStaticLocalIp,
                                                  lv.
                                                  nmhGetNatStaticTranslatedLocalIp.
                                                  pu4RetValNatStaticTranslatedLocalIp);
            NatUnLock ();
            lv.nmhGetNatStaticTranslatedLocalIp.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatStaticTranslatedLocalIp;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatStaticTranslatedLocalIp *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatStaticTranslatedLocalIp)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_STATIC_ENTRY_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatStaticEntryStatus;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatStaticEntryStatus *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatStaticEntryStatus), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatStaticEntryStatus.rval =
                nmhGetNatStaticEntryStatus (lv.nmhGetNatStaticEntryStatus.
                                            i4NatStaticInterfaceNum,
                                            lv.nmhGetNatStaticEntryStatus.
                                            u4NatStaticLocalIp,
                                            lv.nmhGetNatStaticEntryStatus.
                                            pi4RetValNatStaticEntryStatus);
            NatUnLock ();
            lv.nmhGetNatStaticEntryStatus.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatStaticEntryStatus;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatStaticEntryStatus *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatStaticEntryStatus)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_STATIC_TRANSLATED_LOCAL_IP:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatStaticTranslatedLocalIp;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatStaticTranslatedLocalIp *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatStaticTranslatedLocalIp),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhSetNatStaticTranslatedLocalIp.rval =
                nmhSetNatStaticTranslatedLocalIp (lv.
                                                  nmhSetNatStaticTranslatedLocalIp.
                                                  i4NatStaticInterfaceNum,
                                                  lv.
                                                  nmhSetNatStaticTranslatedLocalIp.
                                                  u4NatStaticLocalIp,
                                                  lv.
                                                  nmhSetNatStaticTranslatedLocalIp.
                                                  u4SetValNatStaticTranslatedLocalIp);
            NatUnLock ();
            lv.nmhSetNatStaticTranslatedLocalIp.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatStaticTranslatedLocalIp;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatStaticTranslatedLocalIp *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatStaticTranslatedLocalIp)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_STATIC_ENTRY_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatStaticEntryStatus;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatStaticEntryStatus *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatStaticEntryStatus), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhSetNatStaticEntryStatus.rval =
                nmhSetNatStaticEntryStatus (lv.nmhSetNatStaticEntryStatus.
                                            i4NatStaticInterfaceNum,
                                            lv.nmhSetNatStaticEntryStatus.
                                            u4NatStaticLocalIp,
                                            lv.nmhSetNatStaticEntryStatus.
                                            i4SetValNatStaticEntryStatus);
            NatUnLock ();
            lv.nmhSetNatStaticEntryStatus.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatStaticEntryStatus;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatStaticEntryStatus *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatStaticEntryStatus)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_STATIC_TRANSLATED_LOCAL_IP:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatStaticTranslatedLocalIp;
            OsixKerUseInfo.pSrc =
                (tNatwnmhTestv2NatStaticTranslatedLocalIp *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatStaticTranslatedLocalIp),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            NatLock ();
            /* Invoke NPAPI */
            lv.nmhTestv2NatStaticTranslatedLocalIp.rval =
                nmhTestv2NatStaticTranslatedLocalIp (lv.
                                                     nmhTestv2NatStaticTranslatedLocalIp.
                                                     pu4ErrorCode,
                                                     lv.
                                                     nmhTestv2NatStaticTranslatedLocalIp.
                                                     i4NatStaticInterfaceNum,
                                                     lv.
                                                     nmhTestv2NatStaticTranslatedLocalIp.
                                                     u4NatStaticLocalIp,
                                                     lv.
                                                     nmhTestv2NatStaticTranslatedLocalIp.
                                                     u4TestValNatStaticTranslatedLocalIp);
            NatUnLock ();
            lv.nmhTestv2NatStaticTranslatedLocalIp.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatStaticTranslatedLocalIp;
            OsixKerUseInfo.pDest =
                (tNatwnmhTestv2NatStaticTranslatedLocalIp *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatStaticTranslatedLocalIp)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_STATIC_ENTRY_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatStaticEntryStatus;
            OsixKerUseInfo.pSrc = (tNatwnmhTestv2NatStaticEntryStatus *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatStaticEntryStatus),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            NatLock ();
            /* Invoke NPAPI */
            lv.nmhTestv2NatStaticEntryStatus.rval =
                nmhTestv2NatStaticEntryStatus (lv.nmhTestv2NatStaticEntryStatus.
                                               pu4ErrorCode,
                                               lv.nmhTestv2NatStaticEntryStatus.
                                               i4NatStaticInterfaceNum,
                                               lv.nmhTestv2NatStaticEntryStatus.
                                               u4NatStaticLocalIp,
                                               lv.nmhTestv2NatStaticEntryStatus.
                                               i4TestValNatStaticEntryStatus);
            NatUnLock ();
            lv.nmhTestv2NatStaticEntryStatus.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatStaticEntryStatus;
            OsixKerUseInfo.pDest = (tNatwnmhTestv2NatStaticEntryStatus *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatStaticEntryStatus)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_VALIDATE_INDEX_INSTANCE_NAT_STATIC_NAPT_TABLE:
        {
            OsixKerUseInfo.pDest =
                &lv.nmhValidateIndexInstanceNatStaticNaptTable;
            OsixKerUseInfo.pSrc =
                (tNatwnmhValidateIndexInstanceNatStaticNaptTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhValidateIndexInstanceNatStaticNaptTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            NatLock ();
            /* Invoke NPAPI */
            lv.nmhValidateIndexInstanceNatStaticNaptTable.rval =
                nmhValidateIndexInstanceNatStaticNaptTable (lv.
                                                            nmhValidateIndexInstanceNatStaticNaptTable.
                                                            i4NatStaticNaptInterfaceNum,
                                                            lv.
                                                            nmhValidateIndexInstanceNatStaticNaptTable.
                                                            u4NatStaticNaptLocalIp,
                                                            lv.
                                                            nmhValidateIndexInstanceNatStaticNaptTable.
                                                            i4NatStaticNaptStartLocalPort,
                                                            lv.
                                                            nmhValidateIndexInstanceNatStaticNaptTable.
                                                            i4NatStaticNaptEndLocalPort,
                                                            lv.
                                                            nmhValidateIndexInstanceNatStaticNaptTable.
                                                            i4NatStaticNaptProtocolNumber);
            NatUnLock ();
            lv.nmhValidateIndexInstanceNatStaticNaptTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc =
                &lv.nmhValidateIndexInstanceNatStaticNaptTable;
            OsixKerUseInfo.pDest =
                (tNatwnmhValidateIndexInstanceNatStaticNaptTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhValidateIndexInstanceNatStaticNaptTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FIRST_INDEX_NAT_STATIC_NAPT_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFirstIndexNatStaticNaptTable;
            OsixKerUseInfo.pSrc = (tNatwnmhGetFirstIndexNatStaticNaptTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetFirstIndexNatStaticNaptTable), 0) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetFirstIndexNatStaticNaptTable.rval =
                nmhGetFirstIndexNatStaticNaptTable (lv.
                                                    nmhGetFirstIndexNatStaticNaptTable.
                                                    pi4NatStaticNaptInterfaceNum,
                                                    lv.
                                                    nmhGetFirstIndexNatStaticNaptTable.
                                                    pu4NatStaticNaptLocalIp,
                                                    lv.
                                                    nmhGetFirstIndexNatStaticNaptTable.
                                                    pi4NatStaticNaptStartLocalPort,
                                                    lv.
                                                    nmhGetFirstIndexNatStaticNaptTable.
                                                    pi4NatStaticNaptEndLocalPort,
                                                    lv.
                                                    nmhGetFirstIndexNatStaticNaptTable.
                                                    pi4NatStaticNaptProtocolNumber);
            NatUnLock ();
            lv.nmhGetFirstIndexNatStaticNaptTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetFirstIndexNatStaticNaptTable;
            OsixKerUseInfo.pDest =
                (tNatwnmhGetFirstIndexNatStaticNaptTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetFirstIndexNatStaticNaptTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NEXT_INDEX_NAT_STATIC_NAPT_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNextIndexNatStaticNaptTable;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNextIndexNatStaticNaptTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNextIndexNatStaticNaptTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNextIndexNatStaticNaptTable.rval =
                nmhGetNextIndexNatStaticNaptTable (lv.
                                                   nmhGetNextIndexNatStaticNaptTable.
                                                   i4NatStaticNaptInterfaceNum,
                                                   lv.
                                                   nmhGetNextIndexNatStaticNaptTable.
                                                   pi4NextNatStaticNaptInterfaceNum,
                                                   lv.
                                                   nmhGetNextIndexNatStaticNaptTable.
                                                   u4NatStaticNaptLocalIp,
                                                   lv.
                                                   nmhGetNextIndexNatStaticNaptTable.
                                                   pu4NextNatStaticNaptLocalIp,
                                                   lv.
                                                   nmhGetNextIndexNatStaticNaptTable.
                                                   i4NatStaticNaptStartLocalPort,
                                                   lv.
                                                   nmhGetNextIndexNatStaticNaptTable.
                                                   pi4NextNatStaticNaptStartLocalPort,
                                                   lv.
                                                   nmhGetNextIndexNatStaticNaptTable.
                                                   i4NatStaticNaptEndLocalPort,
                                                   lv.
                                                   nmhGetNextIndexNatStaticNaptTable.
                                                   pi4NextNatStaticNaptEndLocalPort,
                                                   lv.
                                                   nmhGetNextIndexNatStaticNaptTable.
                                                   i4NatStaticNaptProtocolNumber,
                                                   lv.
                                                   nmhGetNextIndexNatStaticNaptTable.
                                                   pi4NextNatStaticNaptProtocolNumber);
            NatUnLock ();
            lv.nmhGetNextIndexNatStaticNaptTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNextIndexNatStaticNaptTable;
            OsixKerUseInfo.pDest = (tNatwnmhGetNextIndexNatStaticNaptTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNextIndexNatStaticNaptTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_STATIC_NAPT_TRANSLATED_LOCAL_IP:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatStaticNaptTranslatedLocalIp;
            OsixKerUseInfo.pSrc =
                (tNatwnmhGetNatStaticNaptTranslatedLocalIp *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatStaticNaptTranslatedLocalIp),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatStaticNaptTranslatedLocalIp.rval =
                nmhGetNatStaticNaptTranslatedLocalIp (lv.
                                                      nmhGetNatStaticNaptTranslatedLocalIp.
                                                      i4NatStaticNaptInterfaceNum,
                                                      lv.
                                                      nmhGetNatStaticNaptTranslatedLocalIp.
                                                      u4NatStaticNaptLocalIp,
                                                      lv.
                                                      nmhGetNatStaticNaptTranslatedLocalIp.
                                                      i4NatStaticNaptStartLocalPort,
                                                      lv.
                                                      nmhGetNatStaticNaptTranslatedLocalIp.
                                                      i4NatStaticNaptEndLocalPort,
                                                      lv.
                                                      nmhGetNatStaticNaptTranslatedLocalIp.
                                                      i4NatStaticNaptProtocolNumber,
                                                      lv.
                                                      nmhGetNatStaticNaptTranslatedLocalIp.
                                                      pu4RetValNatStaticNaptTranslatedLocalIp);
            NatUnLock ();
            lv.nmhGetNatStaticNaptTranslatedLocalIp.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tNatwnmhGetNatStaticNaptTranslatedLocalIp *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetNatStaticNaptTranslatedLocalIp;
            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatStaticNaptTranslatedLocalIp)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_STATIC_NAPT_TRANSLATED_LOCAL_PORT:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatStaticNaptTranslatedLocalPort;
            OsixKerUseInfo.pSrc =
                (tNatwnmhGetNatStaticNaptTranslatedLocalPort *) p;
            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatStaticNaptTranslatedLocalPort),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatStaticNaptTranslatedLocalPort.rval =
                nmhGetNatStaticNaptTranslatedLocalPort (lv.
                                                        nmhGetNatStaticNaptTranslatedLocalPort.
                                                        i4NatStaticNaptInterfaceNum,
                                                        lv.
                                                        nmhGetNatStaticNaptTranslatedLocalPort.
                                                        u4NatStaticNaptLocalIp,
                                                        lv.
                                                        nmhGetNatStaticNaptTranslatedLocalPort.
                                                        i4NatStaticNaptStartLocalPort,
                                                        lv.
                                                        nmhGetNatStaticNaptTranslatedLocalPort.
                                                        i4NatStaticNaptEndLocalPort,
                                                        lv.
                                                        nmhGetNatStaticNaptTranslatedLocalPort.
                                                        i4NatStaticNaptProtocolNumber,
                                                        lv.
                                                        nmhGetNatStaticNaptTranslatedLocalPort.
                                                        pi4RetValNatStaticNaptTranslatedLocalPort);
            NatUnLock ();
            lv.nmhGetNatStaticNaptTranslatedLocalPort.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tNatwnmhGetNatStaticNaptTranslatedLocalPort *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetNatStaticNaptTranslatedLocalPort;
            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatStaticNaptTranslatedLocalPort)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_STATIC_NAPT_DESCRIPTION:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatStaticNaptDescription;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatStaticNaptDescription *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatStaticNaptDescription),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatStaticNaptDescription.rval =
                nmhGetNatStaticNaptDescription (lv.
                                                nmhGetNatStaticNaptDescription.
                                                i4NatStaticNaptInterfaceNum,
                                                lv.
                                                nmhGetNatStaticNaptDescription.
                                                u4NatStaticNaptLocalIp,
                                                lv.
                                                nmhGetNatStaticNaptDescription.
                                                i4NatStaticNaptStartLocalPort,
                                                lv.
                                                nmhGetNatStaticNaptDescription.
                                                i4NatStaticNaptEndLocalPort,
                                                lv.
                                                nmhGetNatStaticNaptDescription.
                                                i4NatStaticNaptProtocolNumber,
                                                lv.
                                                nmhGetNatStaticNaptDescription.
                                                pRetValNatStaticNaptDescription);
            NatUnLock ();
            lv.nmhGetNatStaticNaptDescription.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatStaticNaptDescription;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatStaticNaptDescription *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatStaticNaptDescription)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_STATIC_NAPT_LEASE_DURATION:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatStaticNaptLeaseDuration;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatStaticNaptLeaseDuration *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatStaticNaptLeaseDuration),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatStaticNaptLeaseDuration.rval =
                nmhGetNatStaticNaptLeaseDuration (lv.
                                                  nmhGetNatStaticNaptLeaseDuration.
                                                  i4NatStaticNaptInterfaceNum,
                                                  lv.
                                                  nmhGetNatStaticNaptLeaseDuration.
                                                  u4NatStaticNaptLocalIp,
                                                  lv.
                                                  nmhGetNatStaticNaptLeaseDuration.
                                                  i4NatStaticNaptStartLocalPort,
                                                  lv.
                                                  nmhGetNatStaticNaptLeaseDuration.
                                                  i4NatStaticNaptEndLocalPort,
                                                  lv.
                                                  nmhGetNatStaticNaptLeaseDuration.
                                                  i4NatStaticNaptProtocolNumber,
                                                  lv.
                                                  nmhGetNatStaticNaptLeaseDuration.
                                                  pi4RetValNatStaticNaptLeaseDuration);
            NatUnLock ();
            lv.nmhGetNatStaticNaptLeaseDuration.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatStaticNaptLeaseDuration;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatStaticNaptLeaseDuration *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatStaticNaptLeaseDuration)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_STATIC_NAPT_ENTRY_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatStaticNaptEntryStatus;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatStaticNaptEntryStatus *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatStaticNaptEntryStatus),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatStaticNaptEntryStatus.rval =
                nmhGetNatStaticNaptEntryStatus (lv.
                                                nmhGetNatStaticNaptEntryStatus.
                                                i4NatStaticNaptInterfaceNum,
                                                lv.
                                                nmhGetNatStaticNaptEntryStatus.
                                                u4NatStaticNaptLocalIp,
                                                lv.
                                                nmhGetNatStaticNaptEntryStatus.
                                                i4NatStaticNaptStartLocalPort,
                                                lv.
                                                nmhGetNatStaticNaptEntryStatus.
                                                i4NatStaticNaptEndLocalPort,
                                                lv.
                                                nmhGetNatStaticNaptEntryStatus.
                                                i4NatStaticNaptProtocolNumber,
                                                lv.
                                                nmhGetNatStaticNaptEntryStatus.
                                                pi4RetValNatStaticNaptEntryStatus);
            NatUnLock ();
            lv.nmhGetNatStaticNaptEntryStatus.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatStaticNaptEntryStatus;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatStaticNaptEntryStatus *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatStaticNaptEntryStatus)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_STATIC_NAPT_TRANSLATED_LOCAL_IP:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatStaticNaptTranslatedLocalIp;
            OsixKerUseInfo.pSrc =
                (tNatwnmhSetNatStaticNaptTranslatedLocalIp *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatStaticNaptTranslatedLocalIp),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhSetNatStaticNaptTranslatedLocalIp.rval =
                nmhSetNatStaticNaptTranslatedLocalIp (lv.
                                                      nmhSetNatStaticNaptTranslatedLocalIp.
                                                      i4NatStaticNaptInterfaceNum,
                                                      lv.
                                                      nmhSetNatStaticNaptTranslatedLocalIp.
                                                      u4NatStaticNaptLocalIp,
                                                      lv.
                                                      nmhSetNatStaticNaptTranslatedLocalIp.
                                                      i4NatStaticNaptStartLocalPort,
                                                      lv.
                                                      nmhSetNatStaticNaptTranslatedLocalIp.
                                                      i4NatStaticNaptEndLocalPort,
                                                      lv.
                                                      nmhSetNatStaticNaptTranslatedLocalIp.
                                                      i4NatStaticNaptProtocolNumber,
                                                      lv.
                                                      nmhSetNatStaticNaptTranslatedLocalIp.
                                                      u4SetValNatStaticNaptTranslatedLocalIp);
            NatUnLock ();
            lv.nmhSetNatStaticNaptTranslatedLocalIp.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatStaticNaptTranslatedLocalIp;
            OsixKerUseInfo.pDest =
                (tNatwnmhSetNatStaticNaptTranslatedLocalIp *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatStaticNaptTranslatedLocalIp)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_STATIC_NAPT_TRANSLATED_LOCAL_PORT:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatStaticNaptTranslatedLocalPort;
            OsixKerUseInfo.pSrc =
                (tNatwnmhSetNatStaticNaptTranslatedLocalPort *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatStaticNaptTranslatedLocalPort),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhSetNatStaticNaptTranslatedLocalPort.rval =
                nmhSetNatStaticNaptTranslatedLocalPort (lv.
                                                        nmhSetNatStaticNaptTranslatedLocalPort.
                                                        i4NatStaticNaptInterfaceNum,
                                                        lv.
                                                        nmhSetNatStaticNaptTranslatedLocalPort.
                                                        u4NatStaticNaptLocalIp,
                                                        lv.
                                                        nmhSetNatStaticNaptTranslatedLocalPort.
                                                        i4NatStaticNaptStartLocalPort,
                                                        lv.
                                                        nmhSetNatStaticNaptTranslatedLocalPort.
                                                        i4NatStaticNaptEndLocalPort,
                                                        lv.
                                                        nmhSetNatStaticNaptTranslatedLocalPort.
                                                        i4NatStaticNaptProtocolNumber,
                                                        lv.
                                                        nmhSetNatStaticNaptTranslatedLocalPort.
                                                        i4SetValNatStaticNaptTranslatedLocalPort);
            NatUnLock ();
            lv.nmhSetNatStaticNaptTranslatedLocalPort.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatStaticNaptTranslatedLocalPort;
            OsixKerUseInfo.pDest =
                (tNatwnmhSetNatStaticNaptTranslatedLocalPort *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatStaticNaptTranslatedLocalPort)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_STATIC_NAPT_DESCRIPTION:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatStaticNaptDescription;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatStaticNaptDescription *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatStaticNaptDescription),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhSetNatStaticNaptDescription.rval =
                nmhSetNatStaticNaptDescription (lv.
                                                nmhSetNatStaticNaptDescription.
                                                i4NatStaticNaptInterfaceNum,
                                                lv.
                                                nmhSetNatStaticNaptDescription.
                                                u4NatStaticNaptLocalIp,
                                                lv.
                                                nmhSetNatStaticNaptDescription.
                                                i4NatStaticNaptStartLocalPort,
                                                lv.
                                                nmhSetNatStaticNaptDescription.
                                                i4NatStaticNaptEndLocalPort,
                                                lv.
                                                nmhSetNatStaticNaptDescription.
                                                i4NatStaticNaptProtocolNumber,
                                                lv.
                                                nmhSetNatStaticNaptDescription.
                                                pSetValNatStaticNaptDescription);
            NatUnLock ();
            lv.nmhSetNatStaticNaptDescription.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatStaticNaptDescription;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatStaticNaptDescription *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatStaticNaptDescription)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_STATIC_NAPT_LEASE_DURATION:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatStaticNaptLeaseDuration;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatStaticNaptLeaseDuration *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatStaticNaptLeaseDuration),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhSetNatStaticNaptLeaseDuration.rval =
                nmhSetNatStaticNaptLeaseDuration (lv.
                                                  nmhSetNatStaticNaptLeaseDuration.
                                                  i4NatStaticNaptInterfaceNum,
                                                  lv.
                                                  nmhSetNatStaticNaptLeaseDuration.
                                                  u4NatStaticNaptLocalIp,
                                                  lv.
                                                  nmhSetNatStaticNaptLeaseDuration.
                                                  i4NatStaticNaptStartLocalPort,
                                                  lv.
                                                  nmhSetNatStaticNaptLeaseDuration.
                                                  i4NatStaticNaptEndLocalPort,
                                                  lv.
                                                  nmhSetNatStaticNaptLeaseDuration.
                                                  i4NatStaticNaptProtocolNumber,
                                                  lv.
                                                  nmhSetNatStaticNaptLeaseDuration.
                                                  i4SetValNatStaticNaptLeaseDuration);
            NatUnLock ();
            lv.nmhSetNatStaticNaptLeaseDuration.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatStaticNaptLeaseDuration;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatStaticNaptLeaseDuration *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatStaticNaptLeaseDuration)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_STATIC_NAPT_ENTRY_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatStaticNaptEntryStatus;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatStaticNaptEntryStatus *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatStaticNaptEntryStatus),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhSetNatStaticNaptEntryStatus.rval =
                nmhSetNatStaticNaptEntryStatus (lv.
                                                nmhSetNatStaticNaptEntryStatus.
                                                i4NatStaticNaptInterfaceNum,
                                                lv.
                                                nmhSetNatStaticNaptEntryStatus.
                                                u4NatStaticNaptLocalIp,
                                                lv.
                                                nmhSetNatStaticNaptEntryStatus.
                                                i4NatStaticNaptStartLocalPort,
                                                lv.
                                                nmhSetNatStaticNaptEntryStatus.
                                                i4NatStaticNaptEndLocalPort,
                                                lv.
                                                nmhSetNatStaticNaptEntryStatus.
                                                i4NatStaticNaptProtocolNumber,
                                                lv.
                                                nmhSetNatStaticNaptEntryStatus.
                                                i4SetValNatStaticNaptEntryStatus);
            NatUnLock ();
            lv.nmhSetNatStaticNaptEntryStatus.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatStaticNaptEntryStatus;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatStaticNaptEntryStatus *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatStaticNaptEntryStatus)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_STATIC_NAPT_TRANSLATED_LOCAL_IP:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatStaticNaptTranslatedLocalIp;
            OsixKerUseInfo.pSrc =
                (tNatwnmhTestv2NatStaticNaptTranslatedLocalIp *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatStaticNaptTranslatedLocalIp),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhTestv2NatStaticNaptTranslatedLocalIp.rval =
                nmhTestv2NatStaticNaptTranslatedLocalIp (lv.
                                                         nmhTestv2NatStaticNaptTranslatedLocalIp.
                                                         pu4ErrorCode,
                                                         lv.
                                                         nmhTestv2NatStaticNaptTranslatedLocalIp.
                                                         i4NatStaticNaptInterfaceNum,
                                                         lv.
                                                         nmhTestv2NatStaticNaptTranslatedLocalIp.
                                                         u4NatStaticNaptLocalIp,
                                                         lv.
                                                         nmhTestv2NatStaticNaptTranslatedLocalIp.
                                                         i4NatStaticNaptStartLocalPort,
                                                         lv.
                                                         nmhTestv2NatStaticNaptTranslatedLocalIp.
                                                         i4NatStaticNaptEndLocalPort,
                                                         lv.
                                                         nmhTestv2NatStaticNaptTranslatedLocalIp.
                                                         i4NatStaticNaptProtocolNumber,
                                                         lv.
                                                         nmhTestv2NatStaticNaptTranslatedLocalIp.
                                                         u4TestValNatStaticNaptTranslatedLocalIp);
            NatUnLock ();
            lv.nmhTestv2NatStaticNaptTranslatedLocalIp.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatStaticNaptTranslatedLocalIp;
            OsixKerUseInfo.pDest =
                (tNatwnmhTestv2NatStaticNaptTranslatedLocalIp *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatStaticNaptTranslatedLocalIp)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_STATIC_NAPT_TRANSLATED_LOCAL_PORT:
        {
            OsixKerUseInfo.pDest =
                &lv.nmhTestv2NatStaticNaptTranslatedLocalPort;
            OsixKerUseInfo.pSrc =
                (tNatwnmhTestv2NatStaticNaptTranslatedLocalPort *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatStaticNaptTranslatedLocalPort),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhTestv2NatStaticNaptTranslatedLocalPort.rval =
                nmhTestv2NatStaticNaptTranslatedLocalPort (lv.
                                                           nmhTestv2NatStaticNaptTranslatedLocalPort.
                                                           pu4ErrorCode,
                                                           lv.
                                                           nmhTestv2NatStaticNaptTranslatedLocalPort.
                                                           i4NatStaticNaptInterfaceNum,
                                                           lv.
                                                           nmhTestv2NatStaticNaptTranslatedLocalPort.
                                                           u4NatStaticNaptLocalIp,
                                                           lv.
                                                           nmhTestv2NatStaticNaptTranslatedLocalPort.
                                                           i4NatStaticNaptStartLocalPort,
                                                           lv.
                                                           nmhTestv2NatStaticNaptTranslatedLocalPort.
                                                           i4NatStaticNaptEndLocalPort,
                                                           lv.
                                                           nmhTestv2NatStaticNaptTranslatedLocalPort.
                                                           i4NatStaticNaptProtocolNumber,
                                                           lv.
                                                           nmhTestv2NatStaticNaptTranslatedLocalPort.
                                                           i4TestValNatStaticNaptTranslatedLocalPort);
            NatUnLock ();
            lv.nmhTestv2NatStaticNaptTranslatedLocalPort.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatStaticNaptTranslatedLocalPort;
            OsixKerUseInfo.pDest =
                (tNatwnmhTestv2NatStaticNaptTranslatedLocalPort *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatStaticNaptTranslatedLocalPort)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_STATIC_NAPT_DESCRIPTION:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatStaticNaptDescription;
            OsixKerUseInfo.pSrc = (tNatwnmhTestv2NatStaticNaptDescription *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatStaticNaptDescription),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhTestv2NatStaticNaptDescription.rval =
                nmhTestv2NatStaticNaptDescription (lv.
                                                   nmhTestv2NatStaticNaptDescription.
                                                   pu4ErrorCode,
                                                   lv.
                                                   nmhTestv2NatStaticNaptDescription.
                                                   i4NatStaticNaptInterfaceNum,
                                                   lv.
                                                   nmhTestv2NatStaticNaptDescription.
                                                   u4NatStaticNaptLocalIp,
                                                   lv.
                                                   nmhTestv2NatStaticNaptDescription.
                                                   i4NatStaticNaptStartLocalPort,
                                                   lv.
                                                   nmhTestv2NatStaticNaptDescription.
                                                   i4NatStaticNaptEndLocalPort,
                                                   lv.
                                                   nmhTestv2NatStaticNaptDescription.
                                                   i4NatStaticNaptProtocolNumber,
                                                   lv.
                                                   nmhTestv2NatStaticNaptDescription.
                                                   pTestValNatStaticNaptDescription);
            NatUnLock ();
            lv.nmhTestv2NatStaticNaptDescription.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatStaticNaptDescription;
            OsixKerUseInfo.pDest = (tNatwnmhTestv2NatStaticNaptDescription *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatStaticNaptDescription)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_STATIC_NAPT_LEASE_DURATION:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatStaticNaptLeaseDuration;
            OsixKerUseInfo.pSrc =
                (tNatwnmhTestv2NatStaticNaptLeaseDuration *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatStaticNaptLeaseDuration),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhTestv2NatStaticNaptLeaseDuration.rval =
                nmhTestv2NatStaticNaptLeaseDuration (lv.
                                                     nmhTestv2NatStaticNaptLeaseDuration.
                                                     pu4ErrorCode,
                                                     lv.
                                                     nmhTestv2NatStaticNaptLeaseDuration.
                                                     i4NatStaticNaptInterfaceNum,
                                                     lv.
                                                     nmhTestv2NatStaticNaptLeaseDuration.
                                                     u4NatStaticNaptLocalIp,
                                                     lv.
                                                     nmhTestv2NatStaticNaptLeaseDuration.
                                                     i4NatStaticNaptStartLocalPort,
                                                     lv.
                                                     nmhTestv2NatStaticNaptLeaseDuration.
                                                     i4NatStaticNaptEndLocalPort,
                                                     lv.
                                                     nmhTestv2NatStaticNaptLeaseDuration.
                                                     i4NatStaticNaptProtocolNumber,
                                                     lv.
                                                     nmhTestv2NatStaticNaptLeaseDuration.
                                                     i4TestValNatStaticNaptLeaseDuration);
            NatUnLock ();
            lv.nmhTestv2NatStaticNaptLeaseDuration.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatStaticNaptLeaseDuration;
            OsixKerUseInfo.pDest =
                (tNatwnmhTestv2NatStaticNaptLeaseDuration *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatStaticNaptLeaseDuration)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }
            break;
        }

        case NMH_TESTV2_NAT_STATIC_NAPT_ENTRY_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatStaticNaptEntryStatus;
            OsixKerUseInfo.pSrc = (tNatwnmhTestv2NatStaticNaptEntryStatus *) p;
            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatStaticNaptEntryStatus),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhTestv2NatStaticNaptEntryStatus.rval =
                nmhTestv2NatStaticNaptEntryStatus (lv.
                                                   nmhTestv2NatStaticNaptEntryStatus.
                                                   pu4ErrorCode,
                                                   lv.
                                                   nmhTestv2NatStaticNaptEntryStatus.
                                                   i4NatStaticNaptInterfaceNum,
                                                   lv.
                                                   nmhTestv2NatStaticNaptEntryStatus.
                                                   u4NatStaticNaptLocalIp,
                                                   lv.
                                                   nmhTestv2NatStaticNaptEntryStatus.
                                                   i4NatStaticNaptStartLocalPort,
                                                   lv.
                                                   nmhTestv2NatStaticNaptEntryStatus.
                                                   i4NatStaticNaptEndLocalPort,
                                                   lv.
                                                   nmhTestv2NatStaticNaptEntryStatus.
                                                   i4NatStaticNaptProtocolNumber,
                                                   lv.
                                                   nmhTestv2NatStaticNaptEntryStatus.
                                                   i4TestValNatStaticNaptEntryStatus);
            NatUnLock ();
            lv.nmhTestv2NatStaticNaptEntryStatus.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNatwnmhTestv2NatStaticNaptEntryStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatStaticNaptEntryStatus;
            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatStaticNaptEntryStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_VALIDATE_INDEX_INSTANCE_NAT_IF_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhValidateIndexInstanceNatIfTable;
            OsixKerUseInfo.pSrc = (tNatwnmhValidateIndexInstanceNatIfTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhValidateIndexInstanceNatIfTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }
            break;
        }

        case NMH_GET_FIRST_INDEX_NAT_IF_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFirstIndexNatIfTable;
            OsixKerUseInfo.pSrc = (tNatwnmhGetFirstIndexNatIfTable *) p;
            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetFirstIndexNatIfTable), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetFirstIndexNatIfTable.rval =
                nmhGetFirstIndexNatIfTable (lv.nmhGetFirstIndexNatIfTable.
                                            pi4NatIfInterfaceNumber);
            NatUnLock ();
            lv.nmhGetFirstIndexNatIfTable.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetFirstIndexNatIfTable;
            OsixKerUseInfo.pDest = (tNatwnmhGetFirstIndexNatIfTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof ((tNatwnmhGetFirstIndexNatIfTable *) p)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NEXT_INDEX_NAT_IF_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNextIndexNatIfTable;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNextIndexNatIfTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNextIndexNatIfTable), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNextIndexNatIfTable.rval =
                nmhGetNextIndexNatIfTable (lv.nmhGetNextIndexNatIfTable.
                                           i4NatIfInterfaceNumber,
                                           lv.nmhGetNextIndexNatIfTable.
                                           pi4NextNatIfInterfaceNumber);
            NatUnLock ();
            lv.nmhGetNextIndexNatIfTable.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNextIndexNatIfTable;
            OsixKerUseInfo.pDest = (tNatwnmhGetNextIndexNatIfTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNextIndexNatIfTable)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_IF_NAT:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatIfNat;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatIfNat *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIfNat), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatIfNat.rval =
                nmhGetNatIfNat (lv.nmhGetNatIfNat.i4NatIfInterfaceNumber,
                                lv.nmhGetNatIfNat.pi4RetValNatIfNat);
            NatUnLock ();
            lv.nmhGetNatIfNat.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatIfNat;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatIfNat *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIfNat)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_IF_NAPT:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatIfNapt;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatIfNapt *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIfNapt), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatIfNapt.rval =
                nmhGetNatIfNapt (lv.nmhGetNatIfNapt.i4NatIfInterfaceNumber,
                                 lv.nmhGetNatIfNapt.pi4RetValNatIfNapt);
            NatUnLock ();
            lv.nmhGetNatIfNapt.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatIfNapt;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatIfNapt *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIfNapt)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_IF_TWO_WAY_NAT:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatIfTwoWayNat;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatIfTwoWayNat *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIfTwoWayNat), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatIfTwoWayNat.rval =
                nmhGetNatIfTwoWayNat (lv.nmhGetNatIfTwoWayNat.
                                      i4NatIfInterfaceNumber,
                                      lv.nmhGetNatIfTwoWayNat.
                                      pi4RetValNatIfTwoWayNat);
            NatUnLock ();
            lv.nmhGetNatIfTwoWayNat.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatIfTwoWayNat;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatIfTwoWayNat *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIfTwoWayNat)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_IF_ENTRY_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatIfEntryStatus;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatIfEntryStatus *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIfEntryStatus), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatIfEntryStatus.rval =
                nmhGetNatIfEntryStatus (lv.nmhGetNatIfEntryStatus.
                                        i4NatIfInterfaceNumber,
                                        lv.nmhGetNatIfEntryStatus.
                                        pi4RetValNatIfEntryStatus);
            NatUnLock ();
            lv.nmhGetNatIfEntryStatus.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatIfEntryStatus;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatIfEntryStatus *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIfEntryStatus)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_IF_NAT:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatIfNat;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatIfNat *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatIfNat), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhSetNatIfNat.rval =
                nmhSetNatIfNat (lv.nmhSetNatIfNat.i4NatIfInterfaceNumber,
                                lv.nmhSetNatIfNat.i4SetValNatIfNat);
            NatUnLock ();
            lv.nmhSetNatIfNat.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatIfNat;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatIfNat *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatIfNat)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_IF_NAPT:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatIfNapt;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatIfNapt *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatIfNapt), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhSetNatIfNapt.rval =
                nmhSetNatIfNapt (lv.nmhSetNatIfNapt.i4NatIfInterfaceNumber,
                                 lv.nmhSetNatIfNapt.i4SetValNatIfNapt);
            NatUnLock ();
            lv.nmhSetNatIfNapt.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatIfNapt;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatIfNapt *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatIfNapt)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_IF_TWO_WAY_NAT:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatIfTwoWayNat;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatIfTwoWayNat *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatIfTwoWayNat), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhSetNatIfTwoWayNat.rval =
                nmhSetNatIfTwoWayNat (lv.nmhSetNatIfTwoWayNat.
                                      i4NatIfInterfaceNumber,
                                      lv.nmhSetNatIfTwoWayNat.
                                      i4SetValNatIfTwoWayNat);
            NatUnLock ();
            lv.nmhSetNatIfTwoWayNat.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatIfTwoWayNat;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatIfTwoWayNat *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatIfTwoWayNat)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_IF_ENTRY_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatIfEntryStatus;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatIfEntryStatus *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatIfEntryStatus), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhSetNatIfEntryStatus.rval =
                nmhSetNatIfEntryStatus (lv.nmhSetNatIfEntryStatus.
                                        i4NatIfInterfaceNumber,
                                        lv.nmhSetNatIfEntryStatus.
                                        i4SetValNatIfEntryStatus);
            NatUnLock ();
            lv.nmhSetNatIfEntryStatus.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatIfEntryStatus;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatIfEntryStatus *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatIfEntryStatus)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_IF_NAT:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatIfNat;
            OsixKerUseInfo.pSrc = (tNatwnmhTestv2NatIfNat *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatIfNat), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhTestv2NatIfNat.rval =
                nmhTestv2NatIfNat (lv.nmhTestv2NatIfNat.pu4ErrorCode,
                                   lv.nmhTestv2NatIfNat.i4NatIfInterfaceNumber,
                                   lv.nmhTestv2NatIfNat.i4TestValNatIfNat);
            NatUnLock ();
            lv.nmhTestv2NatIfNat.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatIfNat;
            OsixKerUseInfo.pDest = (tNatwnmhTestv2NatIfNat *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatIfNat)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_IF_NAPT:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatIfNapt;
            OsixKerUseInfo.pSrc = (tNatwnmhTestv2NatIfNapt *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatIfNapt), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhTestv2NatIfNapt.rval =
                nmhTestv2NatIfNapt (lv.nmhTestv2NatIfNapt.pu4ErrorCode,
                                    lv.nmhTestv2NatIfNapt.
                                    i4NatIfInterfaceNumber,
                                    lv.nmhTestv2NatIfNapt.i4TestValNatIfNapt);
            NatUnLock ();
            lv.nmhTestv2NatIfNapt.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatIfNapt;
            OsixKerUseInfo.pDest = (tNatwnmhTestv2NatIfNapt *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatIfNapt)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_IF_TWO_WAY_NAT:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatIfTwoWayNat;
            OsixKerUseInfo.pSrc = (tNatwnmhTestv2NatIfTwoWayNat *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatIfTwoWayNat), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhTestv2NatIfTwoWayNat.rval =
                nmhTestv2NatIfTwoWayNat (lv.nmhTestv2NatIfTwoWayNat.
                                         pu4ErrorCode,
                                         lv.nmhTestv2NatIfTwoWayNat.
                                         i4NatIfInterfaceNumber,
                                         lv.nmhTestv2NatIfTwoWayNat.
                                         i4TestValNatIfTwoWayNat);
            NatUnLock ();
            lv.nmhTestv2NatIfTwoWayNat.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatIfTwoWayNat;
            OsixKerUseInfo.pDest = (tNatwnmhTestv2NatIfTwoWayNat *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatIfTwoWayNat)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_IF_ENTRY_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatIfEntryStatus;
            OsixKerUseInfo.pSrc = (tNatwnmhTestv2NatIfEntryStatus *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatIfEntryStatus), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhTestv2NatIfEntryStatus.rval =
                nmhTestv2NatIfEntryStatus (lv.nmhTestv2NatIfEntryStatus.
                                           pu4ErrorCode,
                                           lv.nmhTestv2NatIfEntryStatus.
                                           i4NatIfInterfaceNumber,
                                           lv.nmhTestv2NatIfEntryStatus.
                                           i4TestValNatIfEntryStatus);
            NatUnLock ();
            lv.nmhTestv2NatIfEntryStatus.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatIfEntryStatus;
            OsixKerUseInfo.pDest = (tNatwnmhTestv2NatIfEntryStatus *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatIfEntryStatus)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_VALIDATE_INDEX_INSTANCE_NAT_I_P_SEC_SESSION_TABLE:
        {
            OsixKerUseInfo.pDest =
                &lv.nmhValidateIndexInstanceNatIPSecSessionTable;
            OsixKerUseInfo.pSrc =
                (tNatwnmhValidateIndexInstanceNatIPSecSessionTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhValidateIndexInstanceNatIPSecSessionTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhValidateIndexInstanceNatIPSecSessionTable.rval =
                nmhValidateIndexInstanceNatIPSecSessionTable (lv.
                                                              nmhValidateIndexInstanceNatIPSecSessionTable.
                                                              i4NatIPSecSessionInterfaceNum,
                                                              lv.
                                                              nmhValidateIndexInstanceNatIPSecSessionTable.
                                                              u4NatIPSecSessionLocalIp,
                                                              lv.
                                                              nmhValidateIndexInstanceNatIPSecSessionTable.
                                                              u4NatIPSecSessionOutsideIp,
                                                              lv.
                                                              nmhValidateIndexInstanceNatIPSecSessionTable.
                                                              i4NatIPSecSessionSPIInside,
                                                              lv.
                                                              nmhValidateIndexInstanceNatIPSecSessionTable.
                                                              i4NatIPSecSessionSPIOutside);
            NatUnLock ();
            lv.nmhValidateIndexInstanceNatIPSecSessionTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc =
                &lv.nmhValidateIndexInstanceNatIPSecSessionTable;
            OsixKerUseInfo.pDest =
                (tNatwnmhValidateIndexInstanceNatIPSecSessionTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhValidateIndexInstanceNatIPSecSessionTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FIRST_INDEX_NAT_I_P_SEC_SESSION_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFirstIndexNatIPSecSessionTable;
            OsixKerUseInfo.pSrc =
                (tNatwnmhGetFirstIndexNatIPSecSessionTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetFirstIndexNatIPSecSessionTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetFirstIndexNatIPSecSessionTable.rval =
                nmhGetFirstIndexNatIPSecSessionTable (lv.
                                                      nmhGetFirstIndexNatIPSecSessionTable.
                                                      pi4NatIPSecSessionInterfaceNum,
                                                      lv.
                                                      nmhGetFirstIndexNatIPSecSessionTable.
                                                      pu4NatIPSecSessionLocalIp,
                                                      lv.
                                                      nmhGetFirstIndexNatIPSecSessionTable.
                                                      pu4NatIPSecSessionOutsideIp,
                                                      lv.
                                                      nmhGetFirstIndexNatIPSecSessionTable.
                                                      pi4NatIPSecSessionSPIInside,
                                                      lv.
                                                      nmhGetFirstIndexNatIPSecSessionTable.
                                                      pi4NatIPSecSessionSPIOutside);
            NatUnLock ();
            lv.nmhGetFirstIndexNatIPSecSessionTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetFirstIndexNatIPSecSessionTable;
            OsixKerUseInfo.pDest =
                (tNatwnmhGetFirstIndexNatIPSecSessionTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetFirstIndexNatIPSecSessionTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NEXT_INDEX_NAT_I_P_SEC_SESSION_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFirstIndexNatIPSecSessionTable;
            OsixKerUseInfo.pSrc =
                (tNatwnmhGetNextIndexNatIPSecSessionTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNextIndexNatIPSecSessionTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNextIndexNatIPSecSessionTable.rval =
                nmhGetNextIndexNatIPSecSessionTable (lv.
                                                     nmhGetNextIndexNatIPSecSessionTable.
                                                     i4NatIPSecSessionInterfaceNum,
                                                     lv.
                                                     nmhGetNextIndexNatIPSecSessionTable.
                                                     pi4NextNatIPSecSessionInterfaceNum,
                                                     lv.
                                                     nmhGetNextIndexNatIPSecSessionTable.
                                                     u4NatIPSecSessionLocalIp,
                                                     lv.
                                                     nmhGetNextIndexNatIPSecSessionTable.
                                                     pu4NextNatIPSecSessionLocalIp,
                                                     lv.
                                                     nmhGetNextIndexNatIPSecSessionTable.
                                                     u4NatIPSecSessionOutsideIp,
                                                     lv.
                                                     nmhGetNextIndexNatIPSecSessionTable.
                                                     pu4NextNatIPSecSessionOutsideIp,
                                                     lv.
                                                     nmhGetNextIndexNatIPSecSessionTable.
                                                     i4NatIPSecSessionSPIInside,
                                                     lv.
                                                     nmhGetNextIndexNatIPSecSessionTable.
                                                     pi4NextNatIPSecSessionSPIInside,
                                                     lv.
                                                     nmhGetNextIndexNatIPSecSessionTable.
                                                     i4NatIPSecSessionSPIOutside,
                                                     lv.
                                                     nmhGetNextIndexNatIPSecSessionTable.
                                                     pi4NextNatIPSecSessionSPIOutside);
            NatUnLock ();
            lv.nmhGetNextIndexNatIPSecSessionTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetFirstIndexNatIPSecSessionTable;
            OsixKerUseInfo.pDest =
                (tNatwnmhGetNextIndexNatIPSecSessionTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNextIndexNatIPSecSessionTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_I_P_SEC_SESSION_TRANSLATED_LOCAL_IP:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatIPSecSessionTranslatedLocalIp;
            OsixKerUseInfo.pSrc =
                (tNatwnmhGetNatIPSecSessionTranslatedLocalIp *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIPSecSessionTranslatedLocalIp),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatIPSecSessionTranslatedLocalIp.rval =
                nmhGetNatIPSecSessionTranslatedLocalIp (lv.
                                                        nmhGetNatIPSecSessionTranslatedLocalIp.
                                                        i4NatIPSecSessionInterfaceNum,
                                                        lv.
                                                        nmhGetNatIPSecSessionTranslatedLocalIp.
                                                        u4NatIPSecSessionLocalIp,
                                                        lv.
                                                        nmhGetNatIPSecSessionTranslatedLocalIp.
                                                        u4NatIPSecSessionOutsideIp,
                                                        lv.
                                                        nmhGetNatIPSecSessionTranslatedLocalIp.
                                                        i4NatIPSecSessionSPIInside,
                                                        lv.
                                                        nmhGetNatIPSecSessionTranslatedLocalIp.
                                                        i4NatIPSecSessionSPIOutside,
                                                        lv.
                                                        nmhGetNatIPSecSessionTranslatedLocalIp.
                                                        pu4RetValNatIPSecSessionTranslatedLocalIp);
            NatUnLock ();
            lv.nmhGetNatIPSecSessionTranslatedLocalIp.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatIPSecSessionTranslatedLocalIp;
            OsixKerUseInfo.pDest =
                (tNatwnmhGetNatIPSecSessionTranslatedLocalIp *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIPSecSessionTranslatedLocalIp)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_I_P_SEC_SESSION_LAST_USE_TIME:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatIPSecSessionLastUseTime;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatIPSecSessionLastUseTime *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIPSecSessionLastUseTime),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatIPSecSessionLastUseTime.rval =
                nmhGetNatIPSecSessionLastUseTime (lv.
                                                  nmhGetNatIPSecSessionLastUseTime.
                                                  i4NatIPSecSessionInterfaceNum,
                                                  lv.
                                                  nmhGetNatIPSecSessionLastUseTime.
                                                  u4NatIPSecSessionLocalIp,
                                                  lv.
                                                  nmhGetNatIPSecSessionLastUseTime.
                                                  u4NatIPSecSessionOutsideIp,
                                                  lv.
                                                  nmhGetNatIPSecSessionLastUseTime.
                                                  i4NatIPSecSessionSPIInside,
                                                  lv.
                                                  nmhGetNatIPSecSessionLastUseTime.
                                                  i4NatIPSecSessionSPIOutside,
                                                  lv.
                                                  nmhGetNatIPSecSessionLastUseTime.
                                                  pi4RetValNatIPSecSessionLastUseTime);
            NatUnLock ();
            lv.nmhGetNatIPSecSessionLastUseTime.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatIPSecSessionLastUseTime;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatIPSecSessionLastUseTime *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIPSecSessionLastUseTime)) ==
                OSIX_FAILURE)

                break;
        }

        case NMH_GET_NAT_I_P_SEC_SESSION_ENTRY_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatIPSecSessionEntryStatus;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatIPSecSessionEntryStatus *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIPSecSessionEntryStatus),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatIPSecSessionEntryStatus.rval =
                nmhGetNatIPSecSessionEntryStatus (lv.
                                                  nmhGetNatIPSecSessionEntryStatus.
                                                  i4NatIPSecSessionInterfaceNum,
                                                  lv.
                                                  nmhGetNatIPSecSessionEntryStatus.
                                                  u4NatIPSecSessionLocalIp,
                                                  lv.
                                                  nmhGetNatIPSecSessionEntryStatus.
                                                  u4NatIPSecSessionOutsideIp,
                                                  lv.
                                                  nmhGetNatIPSecSessionEntryStatus.
                                                  i4NatIPSecSessionSPIInside,
                                                  lv.
                                                  nmhGetNatIPSecSessionEntryStatus.
                                                  i4NatIPSecSessionSPIOutside,
                                                  lv.
                                                  nmhGetNatIPSecSessionEntryStatus.
                                                  pi4RetValNatIPSecSessionEntryStatus);
            NatUnLock ();
            lv.nmhGetNatIPSecSessionEntryStatus.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNatwnmhGetNatIPSecSessionEntryStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetNatIPSecSessionEntryStatus;
            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIPSecSessionEntryStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_I_P_SEC_SESSION_ENTRY_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatIPSecSessionEntryStatus;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatIPSecSessionEntryStatus *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatIPSecSessionEntryStatus),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhSetNatIPSecSessionEntryStatus.rval =
                nmhSetNatIPSecSessionEntryStatus (lv.
                                                  nmhSetNatIPSecSessionEntryStatus.
                                                  i4NatIPSecSessionInterfaceNum,
                                                  lv.
                                                  nmhSetNatIPSecSessionEntryStatus.
                                                  u4NatIPSecSessionLocalIp,
                                                  lv.
                                                  nmhSetNatIPSecSessionEntryStatus.
                                                  u4NatIPSecSessionOutsideIp,
                                                  lv.
                                                  nmhSetNatIPSecSessionEntryStatus.
                                                  i4NatIPSecSessionSPIInside,
                                                  lv.
                                                  nmhSetNatIPSecSessionEntryStatus.
                                                  i4NatIPSecSessionSPIOutside,
                                                  lv.
                                                  nmhSetNatIPSecSessionEntryStatus.
                                                  i4SetValNatIPSecSessionEntryStatus);
            NatUnLock ();
            lv.nmhSetNatIPSecSessionEntryStatus.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatIPSecSessionEntryStatus;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatIPSecSessionEntryStatus *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatIPSecSessionEntryStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_I_P_SEC_SESSION_ENTRY_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatIPSecSessionEntryStatus;
            OsixKerUseInfo.pSrc =
                (tNatwnmhTestv2NatIPSecSessionEntryStatus *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatIPSecSessionEntryStatus),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhTestv2NatIPSecSessionEntryStatus.rval =
                nmhTestv2NatIPSecSessionEntryStatus (lv.
                                                     nmhTestv2NatIPSecSessionEntryStatus.
                                                     pu4ErrorCode,
                                                     lv.
                                                     nmhTestv2NatIPSecSessionEntryStatus.
                                                     i4NatIPSecSessionInterfaceNum,
                                                     lv.
                                                     nmhTestv2NatIPSecSessionEntryStatus.
                                                     u4NatIPSecSessionLocalIp,
                                                     lv.
                                                     nmhTestv2NatIPSecSessionEntryStatus.
                                                     u4NatIPSecSessionOutsideIp,
                                                     lv.
                                                     nmhTestv2NatIPSecSessionEntryStatus.
                                                     i4NatIPSecSessionSPIInside,
                                                     lv.
                                                     nmhTestv2NatIPSecSessionEntryStatus.
                                                     i4NatIPSecSessionSPIOutside,
                                                     lv.
                                                     nmhTestv2NatIPSecSessionEntryStatus.
                                                     i4TestValNatIPSecSessionEntryStatus);
            NatUnLock ();
            lv.nmhTestv2NatIPSecSessionEntryStatus.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatIPSecSessionEntryStatus;
            OsixKerUseInfo.pDest =
                (tNatwnmhTestv2NatIPSecSessionEntryStatus *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatIPSecSessionEntryStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_VALIDATE_INDEX_INSTANCE_NAT_I_P_SEC_PENDING_TABLE:
        {
            OsixKerUseInfo.pDest =
                &lv.nmhValidateIndexInstanceNatIPSecPendingTable;
            OsixKerUseInfo.pSrc =
                (tNatwnmhValidateIndexInstanceNatIPSecPendingTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhValidateIndexInstanceNatIPSecPendingTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhValidateIndexInstanceNatIPSecPendingTable.rval =
                nmhValidateIndexInstanceNatIPSecPendingTable (lv.
                                                              nmhValidateIndexInstanceNatIPSecPendingTable.
                                                              i4NatIPSecPendingInterfaceNum,
                                                              lv.
                                                              nmhValidateIndexInstanceNatIPSecPendingTable.
                                                              u4NatIPSecPendingLocalIp,
                                                              lv.
                                                              nmhValidateIndexInstanceNatIPSecPendingTable.
                                                              u4NatIPSecPendingOutsideIp,
                                                              lv.
                                                              nmhValidateIndexInstanceNatIPSecPendingTable.
                                                              i4NatIPSecPendingSPIInside,
                                                              lv.
                                                              nmhValidateIndexInstanceNatIPSecPendingTable.
                                                              i4NatIPSecPendingSPIOutside);
            NatUnLock ();
            lv.nmhValidateIndexInstanceNatIPSecPendingTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc =
                &lv.nmhValidateIndexInstanceNatIPSecPendingTable;
            OsixKerUseInfo.pDest =
                (tNatwnmhValidateIndexInstanceNatIPSecPendingTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhValidateIndexInstanceNatIPSecPendingTable)) ==
                OSIX_FAILURE)

                break;
        }

        case NMH_GET_FIRST_INDEX_NAT_I_P_SEC_PENDING_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFirstIndexNatIPSecPendingTable;
            OsixKerUseInfo.pSrc =
                (tNatwnmhGetFirstIndexNatIPSecPendingTable *) p;
            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetFirstIndexNatIPSecPendingTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetFirstIndexNatIPSecPendingTable.rval =
                nmhGetFirstIndexNatIPSecPendingTable (lv.
                                                      nmhGetFirstIndexNatIPSecPendingTable.
                                                      pi4NatIPSecPendingInterfaceNum,
                                                      lv.
                                                      nmhGetFirstIndexNatIPSecPendingTable.
                                                      pu4NatIPSecPendingLocalIp,
                                                      lv.
                                                      nmhGetFirstIndexNatIPSecPendingTable.
                                                      pu4NatIPSecPendingOutsideIp,
                                                      lv.
                                                      nmhGetFirstIndexNatIPSecPendingTable.
                                                      pi4NatIPSecPendingSPIInside,
                                                      lv.
                                                      nmhGetFirstIndexNatIPSecPendingTable.
                                                      pi4NatIPSecPendingSPIOutside);
            NatUnLock ();
            lv.nmhGetFirstIndexNatIPSecPendingTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tNatwnmhGetFirstIndexNatIPSecPendingTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFirstIndexNatIPSecPendingTable;
            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetFirstIndexNatIPSecPendingTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NEXT_INDEX_NAT_I_P_SEC_PENDING_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNextIndexNatIPSecPendingTable;
            OsixKerUseInfo.pSrc =
                (tNatwnmhGetNextIndexNatIPSecPendingTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNextIndexNatIPSecPendingTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNextIndexNatIPSecPendingTable.rval =
                nmhGetNextIndexNatIPSecPendingTable (lv.
                                                     nmhGetNextIndexNatIPSecPendingTable.
                                                     i4NatIPSecPendingInterfaceNum,
                                                     lv.
                                                     nmhGetNextIndexNatIPSecPendingTable.
                                                     pi4NextNatIPSecPendingInterfaceNum,
                                                     lv.
                                                     nmhGetNextIndexNatIPSecPendingTable.
                                                     u4NatIPSecPendingLocalIp,
                                                     lv.
                                                     nmhGetNextIndexNatIPSecPendingTable.
                                                     pu4NextNatIPSecPendingLocalIp,
                                                     lv.
                                                     nmhGetNextIndexNatIPSecPendingTable.
                                                     u4NatIPSecPendingOutsideIp,
                                                     lv.
                                                     nmhGetNextIndexNatIPSecPendingTable.
                                                     pu4NextNatIPSecPendingOutsideIp,
                                                     lv.
                                                     nmhGetNextIndexNatIPSecPendingTable.
                                                     i4NatIPSecPendingSPIInside,
                                                     lv.
                                                     nmhGetNextIndexNatIPSecPendingTable.
                                                     pi4NextNatIPSecPendingSPIInside,
                                                     lv.
                                                     nmhGetNextIndexNatIPSecPendingTable.
                                                     i4NatIPSecPendingSPIOutside,
                                                     lv.
                                                     nmhGetNextIndexNatIPSecPendingTable.
                                                     pi4NextNatIPSecPendingSPIOutside);
            NatUnLock ();
            lv.nmhGetNextIndexNatIPSecPendingTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNextIndexNatIPSecPendingTable;
            OsixKerUseInfo.pDest =
                (tNatwnmhGetNextIndexNatIPSecPendingTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNextIndexNatIPSecPendingTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_I_P_SEC_PENDING_TRANSLATED_LOCAL_IP:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatIPSecPendingTranslatedLocalIp;
            OsixKerUseInfo.pSrc =
                (tNatwnmhGetNatIPSecPendingTranslatedLocalIp *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIPSecPendingTranslatedLocalIp),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatIPSecPendingTranslatedLocalIp.rval =
                nmhGetNatIPSecPendingTranslatedLocalIp (lv.
                                                        nmhGetNatIPSecPendingTranslatedLocalIp.
                                                        i4NatIPSecPendingInterfaceNum,
                                                        lv.
                                                        nmhGetNatIPSecPendingTranslatedLocalIp.
                                                        u4NatIPSecPendingLocalIp,
                                                        lv.
                                                        nmhGetNatIPSecPendingTranslatedLocalIp.
                                                        u4NatIPSecPendingOutsideIp,
                                                        lv.
                                                        nmhGetNatIPSecPendingTranslatedLocalIp.
                                                        i4NatIPSecPendingSPIInside,
                                                        lv.
                                                        nmhGetNatIPSecPendingTranslatedLocalIp.
                                                        i4NatIPSecPendingSPIOutside,
                                                        lv.
                                                        nmhGetNatIPSecPendingTranslatedLocalIp.
                                                        pu4RetValNatIPSecPendingTranslatedLocalIp);
            NatUnLock ();
            lv.nmhGetNatIPSecPendingTranslatedLocalIp.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatIPSecPendingTranslatedLocalIp;
            OsixKerUseInfo.pDest =
                (tNatwnmhGetNatIPSecPendingTranslatedLocalIp *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIPSecPendingTranslatedLocalIp)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_I_P_SEC_PENDING_LAST_USE_TIME:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatIPSecPendingLastUseTime;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatIPSecPendingLastUseTime *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIPSecPendingLastUseTime),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatIPSecPendingLastUseTime.rval =
                nmhGetNatIPSecPendingLastUseTime (lv.
                                                  nmhGetNatIPSecPendingLastUseTime.
                                                  i4NatIPSecPendingInterfaceNum,
                                                  lv.
                                                  nmhGetNatIPSecPendingLastUseTime.
                                                  u4NatIPSecPendingLocalIp,
                                                  lv.
                                                  nmhGetNatIPSecPendingLastUseTime.
                                                  u4NatIPSecPendingOutsideIp,
                                                  lv.
                                                  nmhGetNatIPSecPendingLastUseTime.
                                                  i4NatIPSecPendingSPIInside,
                                                  lv.
                                                  nmhGetNatIPSecPendingLastUseTime.
                                                  i4NatIPSecPendingSPIOutside,
                                                  lv.
                                                  nmhGetNatIPSecPendingLastUseTime.
                                                  pi4RetValNatIPSecPendingLastUseTime);
            NatUnLock ();
            lv.nmhGetNatIPSecPendingLastUseTime.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatIPSecPendingLastUseTime;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatIPSecPendingLastUseTime *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIPSecPendingLastUseTime)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_I_P_SEC_PENDING_NO_OF_RETRY:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatIPSecPendingNoOfRetry;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatIPSecPendingNoOfRetry *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIPSecPendingNoOfRetry),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatIPSecPendingNoOfRetry.rval =
                nmhGetNatIPSecPendingNoOfRetry (lv.
                                                nmhGetNatIPSecPendingNoOfRetry.
                                                i4NatIPSecPendingInterfaceNum,
                                                lv.
                                                nmhGetNatIPSecPendingNoOfRetry.
                                                u4NatIPSecPendingLocalIp,
                                                lv.
                                                nmhGetNatIPSecPendingNoOfRetry.
                                                u4NatIPSecPendingOutsideIp,
                                                lv.
                                                nmhGetNatIPSecPendingNoOfRetry.
                                                i4NatIPSecPendingSPIInside,
                                                lv.
                                                nmhGetNatIPSecPendingNoOfRetry.
                                                i4NatIPSecPendingSPIOutside,
                                                lv.
                                                nmhGetNatIPSecPendingNoOfRetry.
                                                pi4RetValNatIPSecPendingNoOfRetry);
            NatUnLock ();
            lv.nmhGetNatIPSecPendingNoOfRetry.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatIPSecPendingNoOfRetry;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatIPSecPendingNoOfRetry *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIPSecPendingNoOfRetry)) == OSIX_FAILURE)

                break;
        }

        case NMH_GET_NAT_I_P_SEC_PENDING_ENTRY_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatIPSecPendingEntryStatus;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatIPSecPendingEntryStatus *) p;
            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIPSecPendingEntryStatus),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatIPSecPendingEntryStatus.rval =
                nmhGetNatIPSecPendingEntryStatus (lv.
                                                  nmhGetNatIPSecPendingEntryStatus.
                                                  i4NatIPSecPendingInterfaceNum,
                                                  lv.
                                                  nmhGetNatIPSecPendingEntryStatus.
                                                  u4NatIPSecPendingLocalIp,
                                                  lv.
                                                  nmhGetNatIPSecPendingEntryStatus.
                                                  u4NatIPSecPendingOutsideIp,
                                                  lv.
                                                  nmhGetNatIPSecPendingEntryStatus.
                                                  i4NatIPSecPendingSPIInside,
                                                  lv.
                                                  nmhGetNatIPSecPendingEntryStatus.
                                                  i4NatIPSecPendingSPIOutside,
                                                  lv.
                                                  nmhGetNatIPSecPendingEntryStatus.
                                                  pi4RetValNatIPSecPendingEntryStatus);
            NatUnLock ();
            lv.nmhGetNatIPSecPendingEntryStatus.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNatwnmhGetNatIPSecPendingEntryStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetNatIPSecPendingEntryStatus;
            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIPSecPendingEntryStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_I_P_SEC_PENDING_ENTRY_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatIPSecPendingEntryStatus;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatIPSecPendingEntryStatus *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatIPSecPendingEntryStatus),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhSetNatIPSecPendingEntryStatus.rval =
                nmhSetNatIPSecPendingEntryStatus (lv.
                                                  nmhSetNatIPSecPendingEntryStatus.
                                                  i4NatIPSecPendingInterfaceNum,
                                                  lv.
                                                  nmhSetNatIPSecPendingEntryStatus.
                                                  u4NatIPSecPendingLocalIp,
                                                  lv.
                                                  nmhSetNatIPSecPendingEntryStatus.
                                                  u4NatIPSecPendingOutsideIp,
                                                  lv.
                                                  nmhSetNatIPSecPendingEntryStatus.
                                                  i4NatIPSecPendingSPIInside,
                                                  lv.
                                                  nmhSetNatIPSecPendingEntryStatus.
                                                  i4NatIPSecPendingSPIOutside,
                                                  lv.
                                                  nmhSetNatIPSecPendingEntryStatus.
                                                  i4SetValNatIPSecPendingEntryStatus);
            NatUnLock ();
            lv.nmhSetNatIPSecPendingEntryStatus.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatIPSecPendingEntryStatus;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatIPSecPendingEntryStatus *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatIPSecPendingEntryStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_I_P_SEC_PENDING_ENTRY_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatIPSecPendingEntryStatus;
            OsixKerUseInfo.pSrc =
                (tNatwnmhTestv2NatIPSecPendingEntryStatus *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatIPSecPendingEntryStatus),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhTestv2NatIPSecPendingEntryStatus.rval =
                nmhTestv2NatIPSecPendingEntryStatus (lv.
                                                     nmhTestv2NatIPSecPendingEntryStatus.
                                                     pu4ErrorCode,
                                                     lv.
                                                     nmhTestv2NatIPSecPendingEntryStatus.
                                                     i4NatIPSecPendingInterfaceNum,
                                                     lv.
                                                     nmhTestv2NatIPSecPendingEntryStatus.
                                                     u4NatIPSecPendingLocalIp,
                                                     lv.
                                                     nmhTestv2NatIPSecPendingEntryStatus.
                                                     u4NatIPSecPendingOutsideIp,
                                                     lv.
                                                     nmhTestv2NatIPSecPendingEntryStatus.
                                                     i4NatIPSecPendingSPIInside,
                                                     lv.
                                                     nmhTestv2NatIPSecPendingEntryStatus.
                                                     i4NatIPSecPendingSPIOutside,
                                                     lv.
                                                     nmhTestv2NatIPSecPendingEntryStatus.
                                                     i4TestValNatIPSecPendingEntryStatus);
            NatUnLock ();
            lv.nmhTestv2NatIPSecPendingEntryStatus.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatIPSecPendingEntryStatus;
            OsixKerUseInfo.pDest =
                (tNatwnmhTestv2NatIPSecPendingEntryStatus *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatIPSecPendingEntryStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_VALIDATE_INDEX_INSTANCE_NAT_I_K_E_SESSION_TABLE:
        {
            OsixKerUseInfo.pDest =
                &lv.nmhValidateIndexInstanceNatIKESessionTable;
            OsixKerUseInfo.pSrc =
                (tNatwnmhValidateIndexInstanceNatIKESessionTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhValidateIndexInstanceNatIKESessionTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhValidateIndexInstanceNatIKESessionTable.rval =
                nmhValidateIndexInstanceNatIKESessionTable (lv.
                                                            nmhValidateIndexInstanceNatIKESessionTable.
                                                            i4NatIKESessionInterfaceNum,
                                                            lv.
                                                            nmhValidateIndexInstanceNatIKESessionTable.
                                                            u4NatIKESessionLocalIp,
                                                            lv.
                                                            nmhValidateIndexInstanceNatIKESessionTable.
                                                            u4NatIKESessionOutsideIp,
                                                            lv.
                                                            nmhValidateIndexInstanceNatIKESessionTable.
                                                            pNatIKESessionInitCookie);
            NatUnLock ();
            lv.nmhValidateIndexInstanceNatIKESessionTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc =
                &lv.nmhValidateIndexInstanceNatIKESessionTable;
            OsixKerUseInfo.pDest =
                (tNatwnmhValidateIndexInstanceNatIKESessionTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhValidateIndexInstanceNatIKESessionTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FIRST_INDEX_NAT_I_K_E_SESSION_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFirstIndexNatIKESessionTable;
            OsixKerUseInfo.pSrc = (tNatwnmhGetFirstIndexNatIKESessionTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetFirstIndexNatIKESessionTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetFirstIndexNatIKESessionTable.rval =
                nmhGetFirstIndexNatIKESessionTable (lv.
                                                    nmhGetFirstIndexNatIKESessionTable.
                                                    pi4NatIKESessionInterfaceNum,
                                                    lv.
                                                    nmhGetFirstIndexNatIKESessionTable.
                                                    pu4NatIKESessionLocalIp,
                                                    lv.
                                                    nmhGetFirstIndexNatIKESessionTable.
                                                    pu4NatIKESessionOutsideIp,
                                                    lv.
                                                    nmhGetFirstIndexNatIKESessionTable.
                                                    pNatIKESessionInitCookie);
            NatUnLock ();
            lv.nmhGetFirstIndexNatIKESessionTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetFirstIndexNatIKESessionTable;
            OsixKerUseInfo.pDest =
                (tNatwnmhGetFirstIndexNatIKESessionTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetFirstIndexNatIKESessionTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NEXT_INDEX_NAT_I_K_E_SESSION_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNextIndexNatIKESessionTable;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNextIndexNatIKESessionTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNextIndexNatIKESessionTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNextIndexNatIKESessionTable.rval =
                nmhGetNextIndexNatIKESessionTable (lv.
                                                   nmhGetNextIndexNatIKESessionTable.
                                                   i4NatIKESessionInterfaceNum,
                                                   lv.
                                                   nmhGetNextIndexNatIKESessionTable.
                                                   pi4NextNatIKESessionInterfaceNum,
                                                   lv.
                                                   nmhGetNextIndexNatIKESessionTable.
                                                   u4NatIKESessionLocalIp,
                                                   lv.
                                                   nmhGetNextIndexNatIKESessionTable.
                                                   pu4NextNatIKESessionLocalIp,
                                                   lv.
                                                   nmhGetNextIndexNatIKESessionTable.
                                                   u4NatIKESessionOutsideIp,
                                                   lv.
                                                   nmhGetNextIndexNatIKESessionTable.
                                                   pu4NextNatIKESessionOutsideIp,
                                                   lv.
                                                   nmhGetNextIndexNatIKESessionTable.
                                                   pNatIKESessionInitCookie,
                                                   lv.
                                                   nmhGetNextIndexNatIKESessionTable.
                                                   pNextNatIKESessionInitCookie);
            NatUnLock ();
            lv.nmhGetNextIndexNatIKESessionTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNextIndexNatIKESessionTable;
            OsixKerUseInfo.pDest = (tNatwnmhGetNextIndexNatIKESessionTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNextIndexNatIKESessionTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_I_K_E_SESSION_TRANSLATED_LOCAL_IP:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatIKESessionTranslatedLocalIp;
            OsixKerUseInfo.pSrc =
                (tNatwnmhGetNatIKESessionTranslatedLocalIp *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIKESessionTranslatedLocalIp),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatIKESessionTranslatedLocalIp.rval =
                nmhGetNatIKESessionTranslatedLocalIp (lv.
                                                      nmhGetNatIKESessionTranslatedLocalIp.
                                                      i4NatIKESessionInterfaceNum,
                                                      lv.
                                                      nmhGetNatIKESessionTranslatedLocalIp.
                                                      u4NatIKESessionLocalIp,
                                                      lv.
                                                      nmhGetNatIKESessionTranslatedLocalIp.
                                                      u4NatIKESessionOutsideIp,
                                                      lv.
                                                      nmhGetNatIKESessionTranslatedLocalIp.
                                                      pNatIKESessionInitCookie,
                                                      lv.
                                                      nmhGetNatIKESessionTranslatedLocalIp.
                                                      pu4RetValNatIKESessionTranslatedLocalIp);
            NatUnLock ();
            lv.nmhGetNatIKESessionTranslatedLocalIp.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatIKESessionTranslatedLocalIp;
            OsixKerUseInfo.pDest =
                (tNatwnmhGetNatIKESessionTranslatedLocalIp *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIKESessionTranslatedLocalIp)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_I_K_E_SESSION_LAST_USE_TIME:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatIKESessionLastUseTime;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatIKESessionLastUseTime *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIKESessionLastUseTime),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatIKESessionLastUseTime.rval =
                nmhGetNatIKESessionLastUseTime (lv.
                                                nmhGetNatIKESessionLastUseTime.
                                                i4NatIKESessionInterfaceNum,
                                                lv.
                                                nmhGetNatIKESessionLastUseTime.
                                                u4NatIKESessionLocalIp,
                                                lv.
                                                nmhGetNatIKESessionLastUseTime.
                                                u4NatIKESessionOutsideIp,
                                                lv.
                                                nmhGetNatIKESessionLastUseTime.
                                                pNatIKESessionInitCookie,
                                                lv.
                                                nmhGetNatIKESessionLastUseTime.
                                                pi4RetValNatIKESessionLastUseTime);
            NatUnLock ();
            lv.nmhGetNatIKESessionLastUseTime.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatIKESessionLastUseTime;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatIKESessionLastUseTime *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIKESessionLastUseTime)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_I_K_E_SESSION_ENTRY_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatIKESessionEntryStatus;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatIKESessionEntryStatus *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIKESessionEntryStatus),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatIKESessionEntryStatus.rval =
                nmhGetNatIKESessionEntryStatus (lv.
                                                nmhGetNatIKESessionEntryStatus.
                                                i4NatIKESessionInterfaceNum,
                                                lv.
                                                nmhGetNatIKESessionEntryStatus.
                                                u4NatIKESessionLocalIp,
                                                lv.
                                                nmhGetNatIKESessionEntryStatus.
                                                u4NatIKESessionOutsideIp,
                                                lv.
                                                nmhGetNatIKESessionEntryStatus.
                                                pNatIKESessionInitCookie,
                                                lv.
                                                nmhGetNatIKESessionEntryStatus.
                                                pi4RetValNatIKESessionEntryStatus);
            NatUnLock ();
            lv.nmhGetNatIKESessionEntryStatus.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatIKESessionEntryStatus;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatIKESessionEntryStatus *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatIKESessionEntryStatus)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_I_K_E_SESSION_ENTRY_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatIKESessionEntryStatus;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatIKESessionEntryStatus *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatIKESessionEntryStatus),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhSetNatIKESessionEntryStatus.rval =
                nmhSetNatIKESessionEntryStatus (lv.
                                                nmhSetNatIKESessionEntryStatus.
                                                i4NatIKESessionInterfaceNum,
                                                lv.
                                                nmhSetNatIKESessionEntryStatus.
                                                u4NatIKESessionLocalIp,
                                                lv.
                                                nmhSetNatIKESessionEntryStatus.
                                                u4NatIKESessionOutsideIp,
                                                lv.
                                                nmhSetNatIKESessionEntryStatus.
                                                pNatIKESessionInitCookie,
                                                lv.
                                                nmhSetNatIKESessionEntryStatus.
                                                i4SetValNatIKESessionEntryStatus);
            NatUnLock ();
            lv.nmhSetNatIKESessionEntryStatus.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatIKESessionEntryStatus;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatIKESessionEntryStatus *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatIKESessionEntryStatus)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_I_K_E_SESSION_ENTRY_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatIKESessionEntryStatus;
            OsixKerUseInfo.pSrc = (tNatwnmhTestv2NatIKESessionEntryStatus *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatIKESessionEntryStatus),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhTestv2NatIKESessionEntryStatus.rval =
                nmhTestv2NatIKESessionEntryStatus (lv.
                                                   nmhTestv2NatIKESessionEntryStatus.
                                                   pu4ErrorCode,
                                                   lv.
                                                   nmhTestv2NatIKESessionEntryStatus.
                                                   i4NatIKESessionInterfaceNum,
                                                   lv.
                                                   nmhTestv2NatIKESessionEntryStatus.
                                                   u4NatIKESessionLocalIp,
                                                   lv.
                                                   nmhTestv2NatIKESessionEntryStatus.
                                                   u4NatIKESessionOutsideIp,
                                                   lv.
                                                   nmhTestv2NatIKESessionEntryStatus.
                                                   pNatIKESessionInitCookie,
                                                   lv.
                                                   nmhTestv2NatIKESessionEntryStatus.
                                                   i4TestValNatIKESessionEntryStatus);
            NatUnLock ();
            lv.nmhTestv2NatIKESessionEntryStatus.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatIKESessionEntryStatus;
            OsixKerUseInfo.pDest = (tNatwnmhTestv2NatIKESessionEntryStatus *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatIKESessionEntryStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_VALIDATE_INDEX_INSTANCE_NAT_PORT_TRIG_INFO_TABLE:
        {
            OsixKerUseInfo.pDest =
                &lv.nmhValidateIndexInstanceNatPortTrigInfoTable;
            OsixKerUseInfo.pSrc =
                (tNatwnmhValidateIndexInstanceNatPortTrigInfoTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhValidateIndexInstanceNatPortTrigInfoTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhValidateIndexInstanceNatPortTrigInfoTable.rval =
                nmhValidateIndexInstanceNatPortTrigInfoTable (lv.
                                                              nmhValidateIndexInstanceNatPortTrigInfoTable.
                                                              pNatPortTrigInfoInBoundPortRange,
                                                              lv.
                                                              nmhValidateIndexInstanceNatPortTrigInfoTable.
                                                              pNatPortTrigInfoOutBoundPortRange,
                                                              lv.
                                                              nmhValidateIndexInstanceNatPortTrigInfoTable.
                                                              i4NatPortTrigInfoProtocol);
            NatUnLock ();
            lv.nmhValidateIndexInstanceNatPortTrigInfoTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc =
                &lv.nmhValidateIndexInstanceNatPortTrigInfoTable;
            OsixKerUseInfo.pDest =
                (tNatwnmhValidateIndexInstanceNatPortTrigInfoTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhValidateIndexInstanceNatPortTrigInfoTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FIRST_INDEX_NAT_PORT_TRIG_INFO_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFirstIndexNatPortTrigInfoTable;
            OsixKerUseInfo.pSrc =
                (tNatwnmhGetFirstIndexNatPortTrigInfoTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetFirstIndexNatPortTrigInfoTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetFirstIndexNatPortTrigInfoTable.rval =
                nmhGetFirstIndexNatPortTrigInfoTable (lv.
                                                      nmhGetFirstIndexNatPortTrigInfoTable.
                                                      pNatPortTrigInfoInBoundPortRange,
                                                      lv.
                                                      nmhGetFirstIndexNatPortTrigInfoTable.
                                                      pNatPortTrigInfoOutBoundPortRange,
                                                      lv.
                                                      nmhGetFirstIndexNatPortTrigInfoTable.
                                                      pi4NatPortTrigInfoProtocol);

            NatUnLock ();
            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetFirstIndexNatPortTrigInfoTable;
            OsixKerUseInfo.pDest =
                (tNatwnmhGetFirstIndexNatPortTrigInfoTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetFirstIndexNatPortTrigInfoTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            break;
        }

        case NMH_GET_NEXT_INDEX_NAT_PORT_TRIG_INFO_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNextIndexNatPortTrigInfoTable;
            OsixKerUseInfo.pSrc =
                (tNatwnmhGetNextIndexNatPortTrigInfoTable *) p;
            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNextIndexNatPortTrigInfoTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNextIndexNatPortTrigInfoTable.rval =
                nmhGetNextIndexNatPortTrigInfoTable (lv.
                                                     nmhGetNextIndexNatPortTrigInfoTable.
                                                     pNatPortTrigInfoInBoundPortRange,
                                                     lv.
                                                     nmhGetNextIndexNatPortTrigInfoTable.
                                                     pNextNatPortTrigInfoInBoundPortRange,
                                                     lv.
                                                     nmhGetNextIndexNatPortTrigInfoTable.
                                                     pNatPortTrigInfoOutBoundPortRange,
                                                     lv.
                                                     nmhGetNextIndexNatPortTrigInfoTable.
                                                     pNextNatPortTrigInfoOutBoundPortRange,
                                                     lv.
                                                     nmhGetNextIndexNatPortTrigInfoTable.
                                                     i4NatPortTrigInfoProtocol,
                                                     lv.
                                                     nmhGetNextIndexNatPortTrigInfoTable.
                                                     pi4NextNatPortTrigInfoProtocol);
            NatUnLock ();
            lv.nmhGetNextIndexNatPortTrigInfoTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tNatwnmhGetNextIndexNatPortTrigInfoTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetNextIndexNatPortTrigInfoTable;
            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNextIndexNatPortTrigInfoTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_PORT_TRIG_INFO_APP_NAME:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatPortTrigInfoAppName;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatPortTrigInfoAppName *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatPortTrigInfoAppName), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetNatPortTrigInfoAppName.rval =
                nmhGetNatPortTrigInfoAppName (lv.nmhGetNatPortTrigInfoAppName.
                                              pNatPortTrigInfoInBoundPortRange,
                                              lv.nmhGetNatPortTrigInfoAppName.
                                              pNatPortTrigInfoOutBoundPortRange,
                                              lv.nmhGetNatPortTrigInfoAppName.
                                              i4NatPortTrigInfoProtocol,
                                              lv.nmhGetNatPortTrigInfoAppName.
                                              pRetValNatPortTrigInfoAppName);
            NatUnLock ();
            lv.nmhGetNatPortTrigInfoAppName.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatPortTrigInfoAppName;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatPortTrigInfoAppName *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatPortTrigInfoAppName)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_PORT_TRIG_INFO_ENTRY_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatPortTrigInfoEntryStatus;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatPortTrigInfoEntryStatus *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatPortTrigInfoEntryStatus),
                 0) == OSIX_FAILURE)

                break;
        }

        case NMH_SET_NAT_PORT_TRIG_INFO_APP_NAME:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatPortTrigInfoAppName;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatPortTrigInfoAppName *) p;
            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatPortTrigInfoAppName), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhSetNatPortTrigInfoAppName.rval =
                nmhSetNatPortTrigInfoAppName (lv.nmhSetNatPortTrigInfoAppName.
                                              pNatPortTrigInfoInBoundPortRange,
                                              lv.nmhSetNatPortTrigInfoAppName.
                                              pNatPortTrigInfoOutBoundPortRange,
                                              lv.nmhSetNatPortTrigInfoAppName.
                                              i4NatPortTrigInfoProtocol,
                                              lv.nmhSetNatPortTrigInfoAppName.
                                              pSetValNatPortTrigInfoAppName);
            NatUnLock ();
            lv.nmhSetNatPortTrigInfoAppName.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatPortTrigInfoAppName;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatPortTrigInfoAppName *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatPortTrigInfoAppName)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_PORT_TRIG_INFO_ENTRY_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatPortTrigInfoEntryStatus;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatPortTrigInfoEntryStatus *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatPortTrigInfoEntryStatus),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhSetNatPortTrigInfoEntryStatus.rval =
                nmhSetNatPortTrigInfoEntryStatus (lv.
                                                  nmhSetNatPortTrigInfoEntryStatus.
                                                  pNatPortTrigInfoInBoundPortRange,
                                                  lv.
                                                  nmhSetNatPortTrigInfoEntryStatus.
                                                  pNatPortTrigInfoOutBoundPortRange,
                                                  lv.
                                                  nmhSetNatPortTrigInfoEntryStatus.
                                                  i4NatPortTrigInfoProtocol,
                                                  lv.
                                                  nmhSetNatPortTrigInfoEntryStatus.
                                                  i4SetValNatPortTrigInfoEntryStatus);
            NatUnLock ();
            lv.nmhSetNatPortTrigInfoEntryStatus.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatPortTrigInfoEntryStatus;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatPortTrigInfoEntryStatus *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatPortTrigInfoEntryStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_PORT_TRIG_INFO_APP_NAME:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatPortTrigInfoAppName;
            OsixKerUseInfo.pSrc = (tNatwnmhTestv2NatPortTrigInfoAppName *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatPortTrigInfoAppName),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhTestv2NatPortTrigInfoAppName.rval =
                nmhTestv2NatPortTrigInfoAppName (lv.
                                                 nmhTestv2NatPortTrigInfoAppName.
                                                 pu4ErrorCode,
                                                 lv.
                                                 nmhTestv2NatPortTrigInfoAppName.
                                                 pNatPortTrigInfoInBoundPortRange,
                                                 lv.
                                                 nmhTestv2NatPortTrigInfoAppName.
                                                 pNatPortTrigInfoOutBoundPortRange,
                                                 lv.
                                                 nmhTestv2NatPortTrigInfoAppName.
                                                 i4NatPortTrigInfoProtocol,
                                                 lv.
                                                 nmhTestv2NatPortTrigInfoAppName.
                                                 pTestValNatPortTrigInfoAppName);
            NatUnLock ();
            lv.nmhTestv2NatPortTrigInfoAppName.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatPortTrigInfoAppName;
            OsixKerUseInfo.pDest = (tNatwnmhTestv2NatPortTrigInfoAppName *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatPortTrigInfoAppName)) == OSIX_FAILURE)

                break;
        }

        case NMH_TESTV2_NAT_PORT_TRIG_INFO_ENTRY_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatPortTrigInfoEntryStatus;
            OsixKerUseInfo.pSrc =
                (tNatwnmhTestv2NatPortTrigInfoEntryStatus *) p;
            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatPortTrigInfoEntryStatus),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhTestv2NatPortTrigInfoEntryStatus.rval =
                nmhTestv2NatPortTrigInfoEntryStatus (lv.
                                                     nmhTestv2NatPortTrigInfoEntryStatus.
                                                     pu4ErrorCode,
                                                     lv.
                                                     nmhTestv2NatPortTrigInfoEntryStatus.
                                                     pNatPortTrigInfoInBoundPortRange,
                                                     lv.
                                                     nmhTestv2NatPortTrigInfoEntryStatus.
                                                     pNatPortTrigInfoOutBoundPortRange,
                                                     lv.
                                                     nmhTestv2NatPortTrigInfoEntryStatus.
                                                     i4NatPortTrigInfoProtocol,
                                                     lv.
                                                     nmhTestv2NatPortTrigInfoEntryStatus.
                                                     i4TestValNatPortTrigInfoEntryStatus);
            NatUnLock ();
            lv.nmhTestv2NatPortTrigInfoEntryStatus.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tNatwnmhTestv2NatPortTrigInfoEntryStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatPortTrigInfoEntryStatus;
            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatPortTrigInfoEntryStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_VALIDATE_INDEX_INSTANCE_NAT_POLICY_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhValidateIndexInstanceNatPolicyTable;
            OsixKerUseInfo.pSrc =
                (tNatwnmhValidateIndexInstanceNatPolicyTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhValidateIndexInstanceNatPolicyTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhValidateIndexInstanceNatPolicyTable.rval =
                nmhValidateIndexInstanceNatPolicyTable (lv.
                                                        nmhValidateIndexInstanceNatPolicyTable.
                                                        i4NatPolicyType,
                                                        lv.
                                                        nmhValidateIndexInstanceNatPolicyTable.
                                                        i4NatPolicyId,
                                                        lv.
                                                        nmhValidateIndexInstanceNatPolicyTable.
                                                        pNatPolicyAclName);
            NatUnLock ();
            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhValidateIndexInstanceNatPolicyTable;
            OsixKerUseInfo.pDest =
                (tNatwnmhValidateIndexInstanceNatPolicyTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhValidateIndexInstanceNatPolicyTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FIRST_INDEX_NAT_POLICY_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFirstIndexNatPolicyTable;
            OsixKerUseInfo.pSrc = (tNatwnmhGetFirstIndexNatPolicyTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetFirstIndexNatPolicyTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.nmhGetFirstIndexNatPolicyTable.rval =
                nmhGetFirstIndexNatPolicyTable (lv.
                                                nmhGetFirstIndexNatPolicyTable.
                                                pi4NatPolicyType,
                                                lv.
                                                nmhGetFirstIndexNatPolicyTable.
                                                pi4NatPolicyId,
                                                lv.
                                                nmhGetFirstIndexNatPolicyTable.
                                                pNatPolicyAclName);
            NatUnLock ();
            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetFirstIndexNatPolicyTable;
            OsixKerUseInfo.pDest = (tNatwnmhGetFirstIndexNatPolicyTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetFirstIndexNatPolicyTable)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NEXT_INDEX_NAT_POLICY_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNextIndexNatPolicyTable;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNextIndexNatPolicyTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNextIndexNatPolicyTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            NatLock ();
            /* Invoke NPAPI */
            lv.nmhGetNextIndexNatPolicyTable.rval =
                nmhGetNextIndexNatPolicyTable (lv.
                                               nmhGetNextIndexNatPolicyTable.
                                               i4NatPolicyType,
                                               lv.
                                               nmhGetNextIndexNatPolicyTable.
                                               pi4NextNatPolicyType,
                                               lv.
                                               nmhGetNextIndexNatPolicyTable.
                                               i4NatPolicyId,
                                               lv.
                                               nmhGetNextIndexNatPolicyTable.
                                               pi4NextNatPolicyId,
                                               lv.
                                               nmhGetNextIndexNatPolicyTable.
                                               pNatPolicyAclName,
                                               lv.
                                               nmhGetNextIndexNatPolicyTable.
                                               pNextNatPolicyAclName);
            NatUnLock ();
            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNextIndexNatPolicyTable;
            OsixKerUseInfo.pDest = (tNatwnmhGetNextIndexNatPolicyTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNextIndexNatPolicyTable)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_POLICY_TRANSLATED_IP:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatPolicyTranslatedIp;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatPolicyTranslatedIp *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatPolicyTranslatedIp), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            NatLock ();
            /* Invoke NPAPI */
            lv.nmhGetNatPolicyTranslatedIp.rval =
                nmhGetNatPolicyTranslatedIp (lv.nmhGetNatPolicyTranslatedIp.
                                             i4NatPolicyType,
                                             lv.nmhGetNatPolicyTranslatedIp.
                                             i4NatPolicyId,
                                             lv.nmhGetNatPolicyTranslatedIp.
                                             pNatPolicyAclName,
                                             lv.nmhGetNatPolicyTranslatedIp.
                                             pu4RetValNatPolicyTranslatedIp);
            NatUnLock ();
            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatPolicyTranslatedIp;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatPolicyTranslatedIp *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatPolicyTranslatedIp)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_POLICY_ENTRY_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatPolicyEntryStatus;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatPolicyEntryStatus *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatPolicyEntryStatus), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            NatLock ();
            /* Invoke NPAPI */
            lv.nmhGetNatPolicyEntryStatus.rval =
                nmhGetNatPolicyEntryStatus (lv.nmhGetNatPolicyEntryStatus.
                                            i4NatPolicyType,
                                            lv.nmhGetNatPolicyEntryStatus.
                                            i4NatPolicyId,
                                            lv.nmhGetNatPolicyEntryStatus.
                                            pNatPolicyAclName,
                                            lv.nmhGetNatPolicyEntryStatus.
                                            pi4RetValNatPolicyEntryStatus);
            NatUnLock ();
            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNatPolicyEntryStatus;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatPolicyEntryStatus *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatPolicyEntryStatus)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_POLICY_TRANSLATED_IP:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatPolicyTranslatedIp;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatPolicyTranslatedIp *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatPolicyTranslatedIp), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            NatLock ();
            /* Invoke NPAPI */
            lv.nmhSetNatPolicyTranslatedIp.rval =
                nmhSetNatPolicyTranslatedIp (lv.nmhSetNatPolicyTranslatedIp.
                                             i4NatPolicyType,
                                             lv.nmhSetNatPolicyTranslatedIp.
                                             i4NatPolicyId,
                                             lv.nmhSetNatPolicyTranslatedIp.
                                             pNatPolicyAclName,
                                             lv.nmhSetNatPolicyTranslatedIp.
                                             u4SetValNatPolicyTranslatedIp);
            NatUnLock ();
            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatPolicyTranslatedIp;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatPolicyTranslatedIp *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatPolicyTranslatedIp)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_NAT_POLICY_ENTRY_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetNatPolicyEntryStatus;
            OsixKerUseInfo.pSrc = (tNatwnmhSetNatPolicyEntryStatus *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatPolicyEntryStatus), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            NatLock ();
            /* Invoke NPAPI */
            lv.nmhSetNatPolicyEntryStatus.rval =
                nmhSetNatPolicyEntryStatus (lv.nmhSetNatPolicyEntryStatus.
                                            i4NatPolicyType,
                                            lv.nmhSetNatPolicyEntryStatus.
                                            i4NatPolicyId,
                                            lv.nmhSetNatPolicyEntryStatus.
                                            pNatPolicyAclName,
                                            lv.nmhSetNatPolicyEntryStatus.
                                            i4SetValNatPolicyEntryStatus);
            NatUnLock ();
            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhSetNatPolicyEntryStatus;
            OsixKerUseInfo.pDest = (tNatwnmhSetNatPolicyEntryStatus *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhSetNatPolicyEntryStatus)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_POLICY_TRANSLATED_IP:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatPolicyTranslatedIp;
            OsixKerUseInfo.pSrc = (tNatwnmhTestv2NatPolicyTranslatedIp *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatPolicyTranslatedIp),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            NatLock ();
            /* Invoke NPAPI */
            lv.nmhTestv2NatPolicyTranslatedIp.rval =
                nmhTestv2NatPolicyTranslatedIp (lv.
                                                nmhTestv2NatPolicyTranslatedIp.
                                                pu4ErrorCode,
                                                lv.
                                                nmhTestv2NatPolicyTranslatedIp.
                                                i4NatPolicyType,
                                                lv.
                                                nmhTestv2NatPolicyTranslatedIp.
                                                i4NatPolicyId,
                                                lv.
                                                nmhTestv2NatPolicyTranslatedIp.
                                                pNatPolicyAclName,
                                                lv.
                                                nmhTestv2NatPolicyTranslatedIp.
                                                u4TestValNatPolicyTranslatedIp);
            NatUnLock ();
            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatPolicyTranslatedIp;
            OsixKerUseInfo.pDest = (tNatwnmhTestv2NatPolicyTranslatedIp *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatPolicyTranslatedIp)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_NAT_POLICY_ENTRY_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2NatPolicyEntryStatus;
            OsixKerUseInfo.pSrc = (tNatwnmhTestv2NatPolicyEntryStatus *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatPolicyEntryStatus),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            NatLock ();
            /* Invoke NPAPI */
            lv.nmhTestv2NatPolicyEntryStatus.rval =
                nmhTestv2NatPolicyEntryStatus (lv.
                                               nmhTestv2NatPolicyEntryStatus.
                                               pu4ErrorCode,
                                               lv.
                                               nmhTestv2NatPolicyEntryStatus.
                                               i4NatPolicyType,
                                               lv.
                                               nmhTestv2NatPolicyEntryStatus.
                                               i4NatPolicyId,
                                               lv.
                                               nmhTestv2NatPolicyEntryStatus.
                                               pNatPolicyAclName,
                                               lv.
                                               nmhTestv2NatPolicyEntryStatus.
                                               i4TestValNatPolicyEntryStatus);
            NatUnLock ();
            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhTestv2NatPolicyEntryStatus;
            OsixKerUseInfo.pDest = (tNatwnmhTestv2NatPolicyEntryStatus *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhTestv2NatPolicyEntryStatus)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_NAT_POLICY_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2NatPolicyTable;
            OsixKerUseInfo.pSrc = (tNatwnmhDepv2NatPolicyTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatPolicyTable), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            NatLock ();
            /* Invoke NPAPI */
            lv.nmhDepv2NatPolicyTable.rval =
                nmhDepv2NatPolicyTable (lv.nmhDepv2NatPolicyTable.
                                        pu4ErrorCode,
                                        lv.nmhDepv2NatPolicyTable.
                                        pSnmpIndexList,
                                        lv.nmhDepv2NatPolicyTable.pSnmpVarBind);
            NatUnLock ();
            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhDepv2NatPolicyTable;
            OsixKerUseInfo.pDest = (tNatwnmhDepv2NatPolicyTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatPolicyTable)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NAT_SEARCH_STATIC_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.NatSearchStaticTable;
            OsixKerUseInfo.pSrc = (tNatwNatSearchStaticTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwNatSearchStaticTable), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.NatSearchStaticTable.rval =
                NatSearchStaticTable (lv.NatSearchStaticTable.u4IpAddr,
                                      lv.NatSearchStaticTable.u4Direction,
                                      lv.NatSearchStaticTable.u4IfNum);

            NatUnLock ();
            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.NatSearchStaticTable;
            OsixKerUseInfo.pDest = (tNatwNatSearchStaticTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwNatSearchStaticTable)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NAT_SEARCH_GLOBAL_IP_ADDR_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.NatSearchGlobalIpAddrTable;
            OsixKerUseInfo.pSrc = (tNatwNatSearchGlobalIpAddrTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwNatSearchGlobalIpAddrTable), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            NatLock ();
            /* Invoke NPAPI */
            lv.NatSearchGlobalIpAddrTable.rval =
                NatSearchGlobalIpAddrTable (lv.NatSearchGlobalIpAddrTable.
                                            u4GlobalIpAddr,
                                            lv.NatSearchGlobalIpAddrTable.
                                            u4IfNum);

            NatUnLock ();
            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.NatSearchGlobalIpAddrTable;
            OsixKerUseInfo.pDest = (tNatwNatSearchGlobalIpAddrTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwNatSearchGlobalIpAddrTable)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NAT_HANDLE_INTERFACE_INDICATION:
        {
            OsixKerUseInfo.pDest = &lv.NatHandleInterfaceIndication;
            OsixKerUseInfo.pSrc = (tNatwNatHandleInterfaceIndication *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwNatHandleInterfaceIndication), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.NatHandleInterfaceIndication.rval =
                NatHandleInterfaceIndication (lv.NatHandleInterfaceIndication.
                                              u4Interface,
                                              lv.NatHandleInterfaceIndication.
                                              u4IpAddress,
                                              lv.NatHandleInterfaceIndication.
                                              u4Status);

            NatUnLock ();
            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.NatHandleInterfaceIndication;
            OsixKerUseInfo.pDest = (tNatwNatHandleInterfaceIndication *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwNatHandleInterfaceIndication)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NAT_CLI_SET_DEBUG_LEVEL:
        {
            OsixKerUseInfo.pDest = &lv.NatCliSetDebugLevel;
            OsixKerUseInfo.pSrc = (tNatwNatCliSetDebugLevel *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwNatCliSetDebugLevel), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            NatLock ();
            lv.NatCliSetDebugLevel.rval =
                NatSetDebugLevel (lv.NatCliSetDebugLevel.u4TraceLevel);

            NatUnLock ();
            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.NatCliSetDebugLevel;
            OsixKerUseInfo.pDest = (tNatwNatCliSetDebugLevel *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwNatCliSetDebugLevel)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NAT_CLI_RESET_DEBUG_LEVEL:
        {
            OsixKerUseInfo.pDest = &lv.NatCliResetDebugLevel;
            OsixKerUseInfo.pSrc = (tNatwNatCliResetDebugLevel *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwNatCliResetDebugLevel), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NAT_API_HANDLE_CLEAN_ALL_BINDS:
        {
            OsixKerUseInfo.pDest = &lv.NatApiHandleCleanAllBinds;
            OsixKerUseInfo.pSrc = (tNatwNatApiHandleCleanAllBinds *) p;
            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwNatApiHandleCleanAllBinds), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            NatLock ();
            /* Invoke NPAPI */
            lv.NatApiHandleCleanAllBinds.rval = NatApiHandleCleanAllBinds ();
            lv.NatApiHandleCleanAllBinds.cliWebSetError = gi4CliWebSetError;

            NatUnLock ();
            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.NatApiHandleCleanAllBinds;
            OsixKerUseInfo.pDest = (tNatwNatApiHandleCleanAllBinds *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwNatApiHandleCleanAllBinds)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NAT_SIP_SHOW_PARTIAL_ENTRIES:
        {
            OsixKerUseInfo.pDest = &lv.NatSipShowPartialLinks;
            OsixKerUseInfo.pSrc = (tNatSipShowPartialLinks *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatSipShowPartialLinks), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.NatSipShowPartialLinks.u1EntryFound = OSIX_FALSE;
            NatLock ();
            TMO_SLL_Scan (&gNatPartialLinksList, pNatPartialLinkNode,
                          tNatPartialLinkNode *)
            {
                if (pNatPartialLinkNode != NULL)
                {
                    if ((pNatPartialLinkNode->u1AppCallStatus != NAT_ON_HOLD) &&
                        (pNatPartialLinkNode->u1AppCallStatus !=
                         NAT_NOT_ON_HOLD))
                    {
                        continue;
                    }

                    count++;
                    if (count > lv.NatSipShowPartialLinks.u4PrintCount)
                    {
                        memcpy (&lv.NatSipShowPartialLinks.NatPartialLinkNode,
                                pNatPartialLinkNode,
                                sizeof (tNatSipShowPartialLinks));
                        lv.NatSipShowPartialLinks.u1EntryFound = OSIX_TRUE;
                        break;
                    }
                }
            }

            NatUnLock ();
            if (lv.NatSipShowPartialLinks.u1EntryFound == OSIX_FALSE)
            {
                memset (&lv.NatSipShowPartialLinks, 0,
                        sizeof (tNatSipShowPartialLinks));
            }

            OsixKerUseInfo.pSrc = &lv.NatSipShowPartialLinks;
            OsixKerUseInfo.pDest = (tNatSipShowPartialLinks *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatSipShowPartialLinks)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            rc = IOCTL_SUCCESS;

            break;
        }

        case NMH_DEPV2_NAT_ENABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2NatEnable;
            OsixKerUseInfo.pSrc = (tNatwnmhDepv2NatEnable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatEnable), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhDepv2NatEnable.rval =
                nmhDepv2NatEnable (lv.nmhDepv2NatEnable.pu4ErrorCode,
                                   lv.nmhDepv2NatEnable.pSnmpIndexList,
                                   lv.nmhDepv2NatEnable.pSnmpVarBind);

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhDepv2NatEnable;
            OsixKerUseInfo.pDest = (tNatwnmhDepv2NatEnable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatEnable)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_NAT_TYPICAL_NUMBER_OF_ENTRIES:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2NatTypicalNumberOfEntries;
            OsixKerUseInfo.pSrc = (tNatwnmhDepv2NatTypicalNumberOfEntries *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatTypicalNumberOfEntries),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhDepv2NatTypicalNumberOfEntries.rval =
                nmhDepv2NatTypicalNumberOfEntries (lv.
                                                   nmhDepv2NatTypicalNumberOfEntries.
                                                   pu4ErrorCode,
                                                   lv.
                                                   nmhDepv2NatTypicalNumberOfEntries.
                                                   pSnmpIndexList,
                                                   lv.
                                                   nmhDepv2NatTypicalNumberOfEntries.
                                                   pSnmpVarBind);

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhDepv2NatTypicalNumberOfEntries;
            OsixKerUseInfo.pDest = (tNatwnmhDepv2NatTypicalNumberOfEntries *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatTypicalNumberOfEntries)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_NAT_TRANSLATED_LOCAL_PORT_START:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2NatTranslatedLocalPortStart;
            OsixKerUseInfo.pSrc =
                (tNatwnmhDepv2NatTranslatedLocalPortStart *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatTranslatedLocalPortStart),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhDepv2NatTranslatedLocalPortStart.rval =
                nmhDepv2NatTranslatedLocalPortStart (lv.
                                                     nmhDepv2NatTranslatedLocalPortStart.
                                                     pu4ErrorCode,
                                                     lv.
                                                     nmhDepv2NatTranslatedLocalPortStart.
                                                     pSnmpIndexList,
                                                     lv.
                                                     nmhDepv2NatTranslatedLocalPortStart.
                                                     pSnmpVarBind);

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhDepv2NatTranslatedLocalPortStart;
            OsixKerUseInfo.pDest =
                (tNatwnmhDepv2NatTranslatedLocalPortStart *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatTranslatedLocalPortStart)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_NAT_IDLE_TIME_OUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2NatIdleTimeOut;
            OsixKerUseInfo.pSrc = (tNatwnmhDepv2NatIdleTimeOut *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatIdleTimeOut), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhDepv2NatIdleTimeOut.rval =
                nmhDepv2NatIdleTimeOut (lv.nmhDepv2NatIdleTimeOut.pu4ErrorCode,
                                        lv.nmhDepv2NatIdleTimeOut.
                                        pSnmpIndexList,
                                        lv.nmhDepv2NatIdleTimeOut.pSnmpVarBind);

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhDepv2NatIdleTimeOut;
            OsixKerUseInfo.pDest = (tNatwnmhDepv2NatIdleTimeOut *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatIdleTimeOut)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_NAT_TCP_TIME_OUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2NatTcpTimeOut;
            OsixKerUseInfo.pSrc = (tNatwnmhDepv2NatTcpTimeOut *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatTcpTimeOut), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhDepv2NatTcpTimeOut.rval =
                nmhDepv2NatTcpTimeOut (lv.nmhDepv2NatTcpTimeOut.pu4ErrorCode,
                                       lv.nmhDepv2NatTcpTimeOut.pSnmpIndexList,
                                       lv.nmhDepv2NatTcpTimeOut.pSnmpVarBind);

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhDepv2NatTcpTimeOut;
            OsixKerUseInfo.pDest = (tNatwnmhDepv2NatTcpTimeOut *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatTcpTimeOut)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_NAT_UDP_TIME_OUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2NatTcpTimeOut;
            OsixKerUseInfo.pSrc = (tNatwnmhDepv2NatUdpTimeOut *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatUdpTimeOut), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhDepv2NatUdpTimeOut.rval =
                nmhDepv2NatUdpTimeOut (lv.nmhDepv2NatUdpTimeOut.pu4ErrorCode,
                                       lv.nmhDepv2NatUdpTimeOut.pSnmpIndexList,
                                       lv.nmhDepv2NatUdpTimeOut.pSnmpVarBind);

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhDepv2NatTcpTimeOut;
            OsixKerUseInfo.pDest = (tNatwnmhDepv2NatUdpTimeOut *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatUdpTimeOut)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_NAT_TRC_FLAG:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2NatTrcFlag;
            OsixKerUseInfo.pSrc = (tNatwnmhDepv2NatTrcFlag *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatTrcFlag), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhDepv2NatTrcFlag.rval =
                nmhDepv2NatTrcFlag (lv.nmhDepv2NatTrcFlag.pu4ErrorCode,
                                    lv.nmhDepv2NatTrcFlag.pSnmpIndexList,
                                    lv.nmhDepv2NatTrcFlag.pSnmpVarBind);

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhDepv2NatTrcFlag;
            OsixKerUseInfo.pDest = (tNatwnmhDepv2NatTrcFlag *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatTrcFlag)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_NAT_I_K_E_PORT_TRANSLATION:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2NatIKEPortTranslation;
            OsixKerUseInfo.pSrc = (tNatwnmhDepv2NatIKEPortTranslation *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatIKEPortTranslation),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhDepv2NatIKEPortTranslation.rval =
                nmhDepv2NatIKEPortTranslation (lv.nmhDepv2NatIKEPortTranslation.
                                               pu4ErrorCode,
                                               lv.nmhDepv2NatIKEPortTranslation.
                                               pSnmpIndexList,
                                               lv.nmhDepv2NatIKEPortTranslation.
                                               pSnmpVarBind);

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhDepv2NatIKEPortTranslation;
            OsixKerUseInfo.pDest = (tNatwnmhDepv2NatIKEPortTranslation *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatIKEPortTranslation)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_NAT_I_K_E_TIMEOUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2NatIKETimeout;
            OsixKerUseInfo.pSrc = (tNatwnmhDepv2NatIKETimeout *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatIKETimeout), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhDepv2NatIKETimeout.rval =
                nmhDepv2NatIKETimeout (lv.nmhDepv2NatIKETimeout.pu4ErrorCode,
                                       lv.nmhDepv2NatIKETimeout.pSnmpIndexList,
                                       lv.nmhDepv2NatIKETimeout.pSnmpVarBind);

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhDepv2NatIKETimeout;
            OsixKerUseInfo.pDest = (tNatwnmhDepv2NatIKETimeout *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatIKETimeout)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_NAT_I_P_SEC_TIMEOUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2NatIPSecTimeout;
            OsixKerUseInfo.pSrc = (tNatwnmhDepv2NatIPSecTimeout *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatIPSecTimeout), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhDepv2NatIPSecTimeout.rval =
                nmhDepv2NatIPSecTimeout (lv.nmhDepv2NatIPSecTimeout.
                                         pu4ErrorCode,
                                         lv.nmhDepv2NatIPSecTimeout.
                                         pSnmpIndexList,
                                         lv.nmhDepv2NatIPSecTimeout.
                                         pSnmpVarBind);

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhDepv2NatIPSecTimeout;
            OsixKerUseInfo.pDest = (tNatwnmhDepv2NatIPSecTimeout *) p;
            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatIPSecTimeout)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_NAT_I_P_SEC_PENDING_TIMEOUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2NatIPSecPendingTimeout;
            OsixKerUseInfo.pSrc = (tNatwnmhDepv2NatIPSecPendingTimeout *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatIPSecPendingTimeout),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhDepv2NatIPSecPendingTimeout.rval =
                nmhDepv2NatIPSecPendingTimeout (lv.
                                                nmhDepv2NatIPSecPendingTimeout.
                                                pu4ErrorCode,
                                                lv.
                                                nmhDepv2NatIPSecPendingTimeout.
                                                pSnmpIndexList,
                                                lv.
                                                nmhDepv2NatIPSecPendingTimeout.
                                                pSnmpVarBind);

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhDepv2NatIPSecPendingTimeout;
            OsixKerUseInfo.pDest = (tNatwnmhDepv2NatIPSecPendingTimeout *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatIPSecPendingTimeout)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_NAT_I_P_SEC_MAX_RETRY:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2NatIPSecMaxRetry;
            OsixKerUseInfo.pSrc = (tNatwnmhDepv2NatIPSecMaxRetry *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatIPSecMaxRetry), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhDepv2NatIPSecMaxRetry.rval =
                nmhDepv2NatIPSecMaxRetry (lv.nmhDepv2NatIPSecMaxRetry.
                                          pu4ErrorCode,
                                          lv.nmhDepv2NatIPSecMaxRetry.
                                          pSnmpIndexList,
                                          lv.nmhDepv2NatIPSecMaxRetry.
                                          pSnmpVarBind);

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhDepv2NatIPSecMaxRetry;
            OsixKerUseInfo.pDest = (tNatwnmhDepv2NatIPSecMaxRetry *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatIPSecMaxRetry)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_SIP_ALG_PORT:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2SipAlgPort;
            OsixKerUseInfo.pSrc = (tNatwnmhDepv2SipAlgPort *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2SipAlgPort), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhDepv2SipAlgPort.rval =
                nmhDepv2SipAlgPort (lv.nmhDepv2SipAlgPort.pu4ErrorCode,
                                    lv.nmhDepv2SipAlgPort.pSnmpIndexList,
                                    lv.nmhDepv2SipAlgPort.pSnmpVarBind);

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhDepv2SipAlgPort;
            OsixKerUseInfo.pDest = (tNatwnmhDepv2SipAlgPort *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2SipAlgPort)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_NAT_SIP_ALG_PARTIAL_ENTRY_TIME_OUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2NatSipAlgPartialEntryTimeOut;
            OsixKerUseInfo.pSrc =
                (tNatwnmhDepv2NatSipAlgPartialEntryTimeOut *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatSipAlgPartialEntryTimeOut),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhDepv2NatSipAlgPartialEntryTimeOut.rval =
                nmhDepv2NatSipAlgPartialEntryTimeOut (lv.
                                                      nmhDepv2NatSipAlgPartialEntryTimeOut.
                                                      pu4ErrorCode,
                                                      lv.
                                                      nmhDepv2NatSipAlgPartialEntryTimeOut.
                                                      pSnmpIndexList,
                                                      lv.
                                                      nmhDepv2NatSipAlgPartialEntryTimeOut.
                                                      pSnmpVarBind);

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhDepv2NatSipAlgPartialEntryTimeOut;
            OsixKerUseInfo.pDest =
                (tNatwnmhDepv2NatSipAlgPartialEntryTimeOut *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatSipAlgPartialEntryTimeOut)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_NAT_GLOBAL_ADDRESS_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2NatGlobalAddressTable;
            OsixKerUseInfo.pSrc = (tNatwnmhDepv2NatGlobalAddressTable *) p;
            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatGlobalAddressTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhDepv2NatGlobalAddressTable.rval =
                nmhDepv2NatGlobalAddressTable (lv.nmhDepv2NatGlobalAddressTable.
                                               pu4ErrorCode,
                                               lv.nmhDepv2NatGlobalAddressTable.
                                               pSnmpIndexList,
                                               lv.nmhDepv2NatGlobalAddressTable.
                                               pSnmpVarBind);

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNatwnmhDepv2NatGlobalAddressTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhDepv2NatGlobalAddressTable;
            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatGlobalAddressTable)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_NAT_LOCAL_ADDRESS_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2NatLocalAddressTable;
            OsixKerUseInfo.pSrc = (tNatwnmhDepv2NatLocalAddressTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatLocalAddressTable), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhDepv2NatLocalAddressTable.rval =
                nmhDepv2NatLocalAddressTable (lv.nmhDepv2NatLocalAddressTable.
                                              pu4ErrorCode,
                                              lv.nmhDepv2NatLocalAddressTable.
                                              pSnmpIndexList,
                                              lv.nmhDepv2NatLocalAddressTable.
                                              pSnmpVarBind);

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhDepv2NatLocalAddressTable;
            OsixKerUseInfo.pDest = (tNatwnmhDepv2NatLocalAddressTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatLocalAddressTable)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_NAT_STATIC_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2NatStaticTable;
            OsixKerUseInfo.pSrc = (tNatwnmhDepv2NatStaticTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatStaticTable), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhDepv2NatStaticTable.rval =
                nmhDepv2NatStaticTable (lv.nmhDepv2NatStaticTable.pu4ErrorCode,
                                        lv.nmhDepv2NatStaticTable.
                                        pSnmpIndexList,
                                        lv.nmhDepv2NatStaticTable.pSnmpVarBind);

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhDepv2NatStaticTable;
            OsixKerUseInfo.pDest = (tNatwnmhDepv2NatStaticTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatStaticTable)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_NAT_STATIC_NAPT_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2NatStaticNaptTable;
            OsixKerUseInfo.pSrc = (tNatwnmhDepv2NatStaticNaptTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatStaticNaptTable), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhDepv2NatStaticNaptTable.rval =
                nmhDepv2NatStaticNaptTable (lv.nmhDepv2NatStaticNaptTable.
                                            pu4ErrorCode,
                                            lv.nmhDepv2NatStaticNaptTable.
                                            pSnmpIndexList,
                                            lv.nmhDepv2NatStaticNaptTable.
                                            pSnmpVarBind);

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhDepv2NatStaticNaptTable;
            OsixKerUseInfo.pDest = (tNatwnmhDepv2NatStaticNaptTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatStaticNaptTable)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_NAT_IF_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2NatIfTable;
            OsixKerUseInfo.pSrc = (tNatwnmhDepv2NatIfTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatIfTable), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhDepv2NatIfTable.rval =
                nmhDepv2NatIfTable (lv.nmhDepv2NatIfTable.pu4ErrorCode,
                                    lv.nmhDepv2NatIfTable.pSnmpIndexList,
                                    lv.nmhDepv2NatIfTable.pSnmpVarBind);

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhDepv2NatIfTable;
            OsixKerUseInfo.pDest = (tNatwnmhDepv2NatIfTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatIfTable)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_NAT_I_P_SEC_SESSION_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2NatIPSecSessionTable;
            OsixKerUseInfo.pSrc = (tNatwnmhDepv2NatIPSecSessionTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatIPSecSessionTable), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhDepv2NatIPSecSessionTable.rval =
                nmhDepv2NatIPSecSessionTable (lv.nmhDepv2NatIPSecSessionTable.
                                              pu4ErrorCode,
                                              lv.nmhDepv2NatIPSecSessionTable.
                                              pSnmpIndexList,
                                              lv.nmhDepv2NatIPSecSessionTable.
                                              pSnmpVarBind);

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhDepv2NatIPSecSessionTable;
            OsixKerUseInfo.pDest = (tNatwnmhDepv2NatIPSecSessionTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatIPSecSessionTable)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_NAT_I_P_SEC_PENDING_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2NatIPSecPendingTable;
            OsixKerUseInfo.pSrc = (tNatwnmhDepv2NatIPSecPendingTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatIPSecPendingTable), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhDepv2NatIPSecPendingTable.rval =
                nmhDepv2NatIPSecPendingTable (lv.nmhDepv2NatIPSecPendingTable.
                                              pu4ErrorCode,
                                              lv.nmhDepv2NatIPSecPendingTable.
                                              pSnmpIndexList,
                                              lv.nmhDepv2NatIPSecPendingTable.
                                              pSnmpVarBind);

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhDepv2NatIPSecPendingTable;
            OsixKerUseInfo.pDest = (tNatwnmhDepv2NatIPSecPendingTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatIPSecPendingTable)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_NAT_I_K_E_SESSION_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2NatIKESessionTable;
            OsixKerUseInfo.pSrc = (tNatwnmhDepv2NatIKESessionTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatIKESessionTable), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhDepv2NatIKESessionTable.rval =
                nmhDepv2NatIKESessionTable (lv.nmhDepv2NatIKESessionTable.
                                            pu4ErrorCode,
                                            lv.nmhDepv2NatIKESessionTable.
                                            pSnmpIndexList,
                                            lv.nmhDepv2NatIKESessionTable.
                                            pSnmpVarBind);

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhDepv2NatIKESessionTable;
            OsixKerUseInfo.pDest = (tNatwnmhDepv2NatIKESessionTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatIKESessionTable)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_NAT_PORT_TRIG_INFO_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2NatPortTrigInfoTable;
            OsixKerUseInfo.pSrc = (tNatwnmhDepv2NatPortTrigInfoTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatPortTrigInfoTable), 0) == OSIX_FAILURE)

            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhDepv2NatPortTrigInfoTable.rval =
                nmhDepv2NatPortTrigInfoTable (lv.nmhDepv2NatPortTrigInfoTable.
                                              pu4ErrorCode,
                                              lv.nmhDepv2NatPortTrigInfoTable.
                                              pSnmpIndexList,
                                              lv.nmhDepv2NatPortTrigInfoTable.
                                              pSnmpVarBind);

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhDepv2NatPortTrigInfoTable;
            OsixKerUseInfo.pDest = (tNatwnmhDepv2NatPortTrigInfoTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhDepv2NatPortTrigInfoTable)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_VALIDATE_INDEX_INSTANCE_NAT_RSVD_PORT_TRIG_INFO_TABLE:
        {

            OsixKerUseInfo.pDest =
                &lv.nmhValidateIndexInstanceNatRsvdPortTrigInfoTable;
            OsixKerUseInfo.pSrc =
                (tNatwnmhValidateIndexInstanceNatRsvdPortTrigInfoTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhValidateIndexInstanceNatRsvdPortTrigInfoTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhValidateIndexInstanceNatRsvdPortTrigInfoTable.rval =
                nmhValidateIndexInstanceNatRsvdPortTrigInfoTable (lv.
                                                                  nmhValidateIndexInstanceNatRsvdPortTrigInfoTable.
                                                                  i4NatRsvdPortTrigInfoAppIndex);

            OsixKerUseInfo.pSrc =
                &lv.nmhValidateIndexInstanceNatRsvdPortTrigInfoTable;
            OsixKerUseInfo.pDest =
                (tNatwnmhValidateIndexInstanceNatRsvdPortTrigInfoTable *) p;
            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhValidateIndexInstanceNatRsvdPortTrigInfoTable))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FIRST_INDEX_NAT_RSVD_PORT_TRIG_INFO_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFirstIndexNatRsvdPortTrigInfoTable;
            OsixKerUseInfo.pSrc =
                (tNatwnmhGetFirstIndexNatRsvdPortTrigInfoTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetFirstIndexNatRsvdPortTrigInfoTable), 0) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFirstIndexNatRsvdPortTrigInfoTable.rval =
                nmhGetFirstIndexNatRsvdPortTrigInfoTable (lv.
                                                          nmhGetFirstIndexNatRsvdPortTrigInfoTable.
                                                          pi4NatRsvdPortTrigInfoAppIndex);

            OsixKerUseInfo.pSrc = &lv.nmhGetFirstIndexNatRsvdPortTrigInfoTable;
            OsixKerUseInfo.pDest =
                (tNatwnmhGetFirstIndexNatRsvdPortTrigInfoTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetFirstIndexNatRsvdPortTrigInfoTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NEXT_INDEX_NAT_RSVD_PORT_TRIG_INFO_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNextIndexNatRsvdPortTrigInfoTable;
            OsixKerUseInfo.pSrc =
                (tNatwnmhGetNextIndexNatRsvdPortTrigInfoTable *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNextIndexNatRsvdPortTrigInfoTable), 0) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhGetNextIndexNatRsvdPortTrigInfoTable.rval =
                nmhGetNextIndexNatRsvdPortTrigInfoTable (lv.
                                                         nmhGetNextIndexNatRsvdPortTrigInfoTable.
                                                         i4NatRsvdPortTrigInfoAppIndex,
                                                         lv.
                                                         nmhGetNextIndexNatRsvdPortTrigInfoTable.
                                                         pi4NextNatRsvdPortTrigInfoAppIndex);

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pSrc = &lv.nmhGetNextIndexNatRsvdPortTrigInfoTable;
            OsixKerUseInfo.pDest =
                (tNatwnmhGetNextIndexNatRsvdPortTrigInfoTable *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNextIndexNatRsvdPortTrigInfoTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_RSVD_PORT_TRIG_INFO_LOCAL_IP:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatRsvdPortTrigInfoLocalIp;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatRsvdPortTrigInfoLocalIp *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatRsvdPortTrigInfoLocalIp), 0) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetNatRsvdPortTrigInfoLocalIp.rval =
                nmhGetNatRsvdPortTrigInfoLocalIp (lv.
                                                  nmhGetNatRsvdPortTrigInfoLocalIp.
                                                  i4NatRsvdPortTrigInfoAppIndex,
                                                  lv.
                                                  nmhGetNatRsvdPortTrigInfoLocalIp.
                                                  pu4RetValNatRsvdPortTrigInfoLocalIp);

            OsixKerUseInfo.pSrc = &lv.nmhGetNatRsvdPortTrigInfoLocalIp;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatRsvdPortTrigInfoLocalIp *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatRsvdPortTrigInfoLocalIp)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_RSVD_PORT_TRIG_INFO_REMOTE_IP:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatRsvdPortTrigInfoRemoteIp;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatRsvdPortTrigInfoRemoteIp *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatRsvdPortTrigInfoRemoteIp), 0) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetNatRsvdPortTrigInfoRemoteIp.rval =
                nmhGetNatRsvdPortTrigInfoRemoteIp (lv.
                                                   nmhGetNatRsvdPortTrigInfoRemoteIp.
                                                   i4NatRsvdPortTrigInfoAppIndex,
                                                   lv.
                                                   nmhGetNatRsvdPortTrigInfoRemoteIp.
                                                   pu4RetValNatRsvdPortTrigInfoRemoteIp);

            OsixKerUseInfo.pSrc = &lv.nmhGetNatRsvdPortTrigInfoRemoteIp;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatRsvdPortTrigInfoRemoteIp *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatRsvdPortTrigInfoRemoteIp)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_RSVD_PORT_TRIG_INFO_START_TIME:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatRsvdPortTrigInfoStartTime;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatRsvdPortTrigInfoStartTime *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatRsvdPortTrigInfoStartTime), 0) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetNatRsvdPortTrigInfoStartTime.rval =
                nmhGetNatRsvdPortTrigInfoStartTime (lv.
                                                    nmhGetNatRsvdPortTrigInfoStartTime.
                                                    i4NatRsvdPortTrigInfoAppIndex,
                                                    lv.
                                                    nmhGetNatRsvdPortTrigInfoStartTime.
                                                    pu4RetValNatRsvdPortTrigInfoStartTime);

            OsixKerUseInfo.pSrc = &lv.nmhGetNatRsvdPortTrigInfoStartTime;
            OsixKerUseInfo.pDest =
                (tNatwnmhGetNatRsvdPortTrigInfoStartTime *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatRsvdPortTrigInfoStartTime)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_RSVD_PORT_TRIG_INFO_APP_NAME:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatRsvdPortTrigInfoAppName;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatRsvdPortTrigInfoAppName *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatRsvdPortTrigInfoAppName), 0) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetNatRsvdPortTrigInfoAppName.rval =
                nmhGetNatRsvdPortTrigInfoAppName (lv.
                                                  nmhGetNatRsvdPortTrigInfoAppName.
                                                  i4NatRsvdPortTrigInfoAppIndex,
                                                  lv.
                                                  nmhGetNatRsvdPortTrigInfoAppName.
                                                  pRetValNatRsvdPortTrigInfoAppName);

            OsixKerUseInfo.pSrc = &lv.nmhGetNatRsvdPortTrigInfoAppName;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatRsvdPortTrigInfoAppName *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatRsvdPortTrigInfoAppName)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_RSVD_PORT_TRIG_INFO_IN_BOUND_PORT_RANGE:
        {
            OsixKerUseInfo.pDest =
                &lv.nmhGetNatRsvdPortTrigInfoInBoundPortRange;
            OsixKerUseInfo.pSrc =
                (tNatwnmhGetNatRsvdPortTrigInfoInBoundPortRange *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatRsvdPortTrigInfoInBoundPortRange), 0) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetNatRsvdPortTrigInfoInBoundPortRange.rval =
                nmhGetNatRsvdPortTrigInfoInBoundPortRange (lv.
                                                           nmhGetNatRsvdPortTrigInfoInBoundPortRange.
                                                           i4NatRsvdPortTrigInfoAppIndex,
                                                           lv.
                                                           nmhGetNatRsvdPortTrigInfoInBoundPortRange.
                                                           pRetValNatRsvdPortTrigInfoInBoundPortRange);

            OsixKerUseInfo.pSrc = &lv.nmhGetNatRsvdPortTrigInfoInBoundPortRange;
            OsixKerUseInfo.pDest =
                (tNatwnmhGetNatRsvdPortTrigInfoInBoundPortRange *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatRsvdPortTrigInfoInBoundPortRange)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_RSVD_PORT_TRIG_INFO_OUT_BOUND_PORT_RANGE:
        {
            OsixKerUseInfo.pDest =
                &lv.nmhGetNatRsvdPortTrigInfoOutBoundPortRange;
            OsixKerUseInfo.pSrc =
                (tNatwnmhGetNatRsvdPortTrigInfoOutBoundPortRange *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatRsvdPortTrigInfoOutBoundPortRange), 0) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetNatRsvdPortTrigInfoOutBoundPortRange.rval =
                nmhGetNatRsvdPortTrigInfoOutBoundPortRange (lv.
                                                            nmhGetNatRsvdPortTrigInfoOutBoundPortRange.
                                                            i4NatRsvdPortTrigInfoAppIndex,
                                                            lv.
                                                            nmhGetNatRsvdPortTrigInfoOutBoundPortRange.
                                                            pRetValNatRsvdPortTrigInfoOutBoundPortRange);

            OsixKerUseInfo.pSrc =
                &lv.nmhGetNatRsvdPortTrigInfoOutBoundPortRange;
            OsixKerUseInfo.pDest =
                (tNatwnmhGetNatRsvdPortTrigInfoOutBoundPortRange *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatRsvdPortTrigInfoOutBoundPortRange)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NAT_RSVD_PORT_TRIG_INFO_PROTOCOL:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNatRsvdPortTrigInfoProtocol;
            OsixKerUseInfo.pSrc = (tNatwnmhGetNatRsvdPortTrigInfoProtocol *) p;

            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatRsvdPortTrigInfoProtocol), 0) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetNatRsvdPortTrigInfoProtocol.rval =
                nmhGetNatRsvdPortTrigInfoProtocol (lv.
                                                   nmhGetNatRsvdPortTrigInfoProtocol.
                                                   i4NatRsvdPortTrigInfoAppIndex,
                                                   lv.
                                                   nmhGetNatRsvdPortTrigInfoProtocol.
                                                   pi4RetValNatRsvdPortTrigInfoProtocol);

            OsixKerUseInfo.pSrc = &lv.nmhGetNatRsvdPortTrigInfoProtocol;
            OsixKerUseInfo.pDest = (tNatwnmhGetNatRsvdPortTrigInfoProtocol *) p;

            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNatwnmhGetNatRsvdPortTrigInfoProtocol)) ==
                OSIX_FAILURE)

            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NAT_API_SEARCH_POLICY_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.NatApiSearchPolicyTable;
            OsixKerUseInfo.pSrc = (tNatwNatApiSearchPolicyTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNatwNatApiSearchPolicyTable),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.NatApiSearchPolicyTable.rval =
                NatApiSearchPolicyTable (lv.NatApiSearchPolicyTable.
                                         u4IpAddress);

            OsixKerUseInfo.pSrc = &lv.NatApiSearchPolicyTable;
            OsixKerUseInfo.pDest = (tNatwNatApiSearchPolicyTable *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNatwNatApiSearchPolicyTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        default:
            rc = -EFAULT;
            break;
    }                            /* switch */

    return (rc);
}
