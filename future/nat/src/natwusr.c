/********************************************************************
 * Copyright (C) Future Software,1997-98,2001
 *
 * $Id: natwusr.c,v 1.11 2014/11/23 09:17:19 siva Exp $
 *
 * Description: Functions called when NAT module operates in Kernel mode
 *******************************************************************/
#include "natwincs.h"
# include  "fsnatlw.h"
#include "fsnatwr.h"
#include "fsnatcli.h"
#ifdef UPNP_WANTED
#include "arupnp.h"
#endif
#include "rmgr.h"
#include "natinc.h"
/* -------------------------------------------------------------
 *
 * Function: nmhGetNatEnable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatEnable (INT4 *pi4RetValNatEnable)
{
    int                 rc;
    tNatwnmhGetNatEnable lv;

    lv.cmd = NMH_GET_NAT_ENABLE;
    lv.pi4RetValNatEnable = pi4RetValNatEnable;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatTypicalNumberOfEntries
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatTypicalNumberOfEntries (INT4 *pi4RetValNatTypicalNumberOfEntries)
{
    int                 rc;
    tNatwnmhGetNatTypicalNumberOfEntries lv;

    lv.cmd = NMH_GET_NAT_TYPICAL_NUMBER_OF_ENTRIES;
    lv.pi4RetValNatTypicalNumberOfEntries = pi4RetValNatTypicalNumberOfEntries;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatTranslatedLocalPortStart
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatTranslatedLocalPortStart (INT4 *pi4RetValNatTranslatedLocalPortStart)
{
    int                 rc;
    tNatwnmhGetNatTranslatedLocalPortStart lv;

    lv.cmd = NMH_GET_NAT_TRANSLATED_LOCAL_PORT_START;
    lv.pi4RetValNatTranslatedLocalPortStart =
        pi4RetValNatTranslatedLocalPortStart;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatIdleTimeOut
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatIdleTimeOut (INT4 *pi4RetValNatIdleTimeOut)
{
    int                 rc;
    tNatwnmhGetNatIdleTimeOut lv;

    lv.cmd = NMH_GET_NAT_IDLE_TIME_OUT;
    lv.pi4RetValNatIdleTimeOut = pi4RetValNatIdleTimeOut;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatTcpTimeOut
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatTcpTimeOut (INT4 *pi4RetValNatTcpTimeOut)
{
    int                 rc;
    tNatwnmhGetNatTcpTimeOut lv;

    lv.cmd = NMH_GET_NAT_TCP_TIME_OUT;
    lv.pi4RetValNatTcpTimeOut = pi4RetValNatTcpTimeOut;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatUdpTimeOut
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatUdpTimeOut (INT4 *pi4RetValNatUdpTimeOut)
{
    int                 rc;
    tNatwnmhGetNatUdpTimeOut lv;

    lv.cmd = NMH_GET_NAT_UDP_TIME_OUT;
    lv.pi4RetValNatUdpTimeOut = pi4RetValNatUdpTimeOut;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatTrcFlag
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatTrcFlag (INT4 *pi4RetValNatTrcFlag)
{
    int                 rc;
    tNatwnmhGetNatTrcFlag lv;

    lv.cmd = NMH_GET_NAT_TRC_FLAG;
    lv.pi4RetValNatTrcFlag = pi4RetValNatTrcFlag;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatStatDynamicAllocFailureCount
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatStatDynamicAllocFailureCount (UINT4
                                       *pu4RetValNatStatDynamicAllocFailureCount)
{
    int                 rc;
    tNatwnmhGetNatStatDynamicAllocFailureCount lv;

    lv.cmd = NMH_GET_NAT_STAT_DYNAMIC_ALLOC_FAILURE_COUNT;
    lv.pu4RetValNatStatDynamicAllocFailureCount =
        pu4RetValNatStatDynamicAllocFailureCount;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatStatTotalNumberOfTranslations
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatStatTotalNumberOfTranslations (UINT4
                                        *pu4RetValNatStatTotalNumberOfTranslations)
{
    int                 rc;
    tNatwnmhGetNatStatTotalNumberOfTranslations lv;

    lv.cmd = NMH_GET_NAT_STAT_TOTAL_NUMBER_OF_TRANSLATIONS;
    lv.pu4RetValNatStatTotalNumberOfTranslations =
        pu4RetValNatStatTotalNumberOfTranslations;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatStatTotalNumberOfActiveSessions
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatStatTotalNumberOfActiveSessions (UINT4
                                          *pu4RetValNatStatTotalNumberOfActiveSessions)
{
    int                 rc;
    tNatwnmhGetNatStatTotalNumberOfActiveSessions lv;

    lv.cmd = NMH_GET_NAT_STAT_TOTAL_NUMBER_OF_ACTIVE_SESSIONS;
    lv.pu4RetValNatStatTotalNumberOfActiveSessions =
        pu4RetValNatStatTotalNumberOfActiveSessions;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatStatTotalNumberOfPktsDropped
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatStatTotalNumberOfPktsDropped (UINT4
                                       *pu4RetValNatStatTotalNumberOfPktsDropped)
{
    int                 rc;
    tNatwnmhGetNatStatTotalNumberOfPktsDropped lv;

    lv.cmd = NMH_GET_NAT_STAT_TOTAL_NUMBER_OF_PKTS_DROPPED;
    lv.pu4RetValNatStatTotalNumberOfPktsDropped =
        pu4RetValNatStatTotalNumberOfPktsDropped;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatStatTotalNumberOfSessionsClosed
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatStatTotalNumberOfSessionsClosed (UINT4
                                          *pu4RetValNatStatTotalNumberOfSessionsClosed)
{
    int                 rc;
    tNatwnmhGetNatStatTotalNumberOfSessionsClosed lv;

    lv.cmd = NMH_GET_NAT_STAT_TOTAL_NUMBER_OF_SESSIONS_CLOSED;
    lv.pu4RetValNatStatTotalNumberOfSessionsClosed =
        pu4RetValNatStatTotalNumberOfSessionsClosed;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatIKEPortTranslation
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatIKEPortTranslation (INT4 *pi4RetValNatIKEPortTranslation)
{
    int                 rc;
    tNatwnmhGetNatIKEPortTranslation lv;

    lv.cmd = NMH_GET_NAT_I_K_E_PORT_TRANSLATION;
    lv.pi4RetValNatIKEPortTranslation = pi4RetValNatIKEPortTranslation;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatIKETimeout
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatIKETimeout (INT4 *pi4RetValNatIKETimeout)
{
    int                 rc;
    tNatwnmhGetNatIKETimeout lv;

    lv.cmd = NMH_GET_NAT_I_K_E_TIMEOUT;
    lv.pi4RetValNatIKETimeout = pi4RetValNatIKETimeout;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatIPSecTimeout
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatIPSecTimeout (INT4 *pi4RetValNatIPSecTimeout)
{
    int                 rc;
    tNatwnmhGetNatIPSecTimeout lv;

    lv.cmd = NMH_GET_NAT_I_P_SEC_TIMEOUT;
    lv.pi4RetValNatIPSecTimeout = pi4RetValNatIPSecTimeout;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatIPSecPendingTimeout
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatIPSecPendingTimeout (INT4 *pi4RetValNatIPSecPendingTimeout)
{
    int                 rc;
    tNatwnmhGetNatIPSecPendingTimeout lv;

    lv.cmd = NMH_GET_NAT_I_P_SEC_PENDING_TIMEOUT;
    lv.pi4RetValNatIPSecPendingTimeout = pi4RetValNatIPSecPendingTimeout;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatIPSecMaxRetry
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatIPSecMaxRetry (INT4 *pi4RetValNatIPSecMaxRetry)
{
    int                 rc;
    tNatwnmhGetNatIPSecMaxRetry lv;

    lv.cmd = NMH_GET_NAT_I_P_SEC_MAX_RETRY;
    lv.pi4RetValNatIPSecMaxRetry = pi4RetValNatIPSecMaxRetry;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetSipAlgPort
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetSipAlgPort (INT4 *pi4RetValSipAlgPort)
{
    int                 rc;
    tNatwnmhGetSipAlgPort lv;

    lv.cmd = NMH_GET_SIP_ALG_PORT;
    lv.pi4RetValSipAlgPort = pi4RetValSipAlgPort;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatSipAlgPartialEntryTimeOut
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatSipAlgPartialEntryTimeOut (INT4 *pi4RetValNatSipAlgPartialEntryTimeOut)
{
    int                 rc;
    tNatwnmhGetNatSipAlgPartialEntryTimeOut lv;

    lv.cmd = NMH_GET_NAT_SIP_ALG_PARTIAL_ENTRY_TIME_OUT;
    lv.pi4RetValNatSipAlgPartialEntryTimeOut =
        pi4RetValNatSipAlgPartialEntryTimeOut;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatEnable
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatEnable (INT4 i4SetValNatEnable)
{
    int                 rc;
    tNatwnmhSetNatEnable lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_ENABLE;
    lv.i4SetValNatEnable = i4SetValNatEnable;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatEnable;
        SnmpNotifyInfo.u4OidLen = sizeof (NatEnable) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;
        SnmpNotifyInfo.u4Indices = NAT_ZERO;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValNatEnable));
    }
#ifdef NPAPI_WANTED
    /* Set the security throughput meter limit according to the 
     * status of NAT module */
    FsSecNpHwSetRateLimit (ISS_SEC_NAT_MODULE, i4SetValNatEnable);
#endif

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatTypicalNumberOfEntries
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatTypicalNumberOfEntries (INT4 i4SetValNatTypicalNumberOfEntries)
{
    int                 rc;
    tNatwnmhSetNatTypicalNumberOfEntries lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_TYPICAL_NUMBER_OF_ENTRIES;
    lv.i4SetValNatTypicalNumberOfEntries = i4SetValNatTypicalNumberOfEntries;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatTypicalNumberOfEntries;
        SnmpNotifyInfo.u4OidLen =
            sizeof (NatTypicalNumberOfEntries) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;
        SnmpNotifyInfo.u4Indices = NAT_ZERO;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                          i4SetValNatTypicalNumberOfEntries));
    }

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatTranslatedLocalPortStart
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatTranslatedLocalPortStart (INT4 i4SetValNatTranslatedLocalPortStart)
{
    int                 rc;
    tNatwnmhSetNatTranslatedLocalPortStart lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_TRANSLATED_LOCAL_PORT_START;
    lv.i4SetValNatTranslatedLocalPortStart =
        i4SetValNatTranslatedLocalPortStart;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatTranslatedLocalPortStart;
        SnmpNotifyInfo.u4OidLen =
            sizeof (NatTranslatedLocalPortStart) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;
        SnmpNotifyInfo.u4Indices = NAT_ZERO;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                          i4SetValNatTranslatedLocalPortStart));
    }
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatIdleTimeOut
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatIdleTimeOut (INT4 i4SetValNatIdleTimeOut)
{
    int                 rc;
    tNatwnmhSetNatIdleTimeOut lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_IDLE_TIME_OUT;
    lv.i4SetValNatIdleTimeOut = i4SetValNatIdleTimeOut;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatIdleTimeOut;
        SnmpNotifyInfo.u4OidLen = sizeof (NatIdleTimeOut) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;;
        SnmpNotifyInfo.u4Indices = NAT_ZERO;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValNatIdleTimeOut));
    }

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatTcpTimeOut
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatTcpTimeOut (INT4 i4SetValNatTcpTimeOut)
{
    int                 rc;
    tNatwnmhSetNatTcpTimeOut lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_TCP_TIME_OUT;
    lv.i4SetValNatTcpTimeOut = i4SetValNatTcpTimeOut;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatTcpTimeOut;
        SnmpNotifyInfo.u4OidLen = sizeof (NatTcpTimeOut) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;;
        SnmpNotifyInfo.u4Indices = NAT_ZERO;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValNatTcpTimeOut));
    }

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatUdpTimeOut
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatUdpTimeOut (INT4 i4SetValNatUdpTimeOut)
{
    int                 rc;
    tNatwnmhSetNatUdpTimeOut lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_UDP_TIME_OUT;
    lv.i4SetValNatUdpTimeOut = i4SetValNatUdpTimeOut;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatUdpTimeOut;
        SnmpNotifyInfo.u4OidLen = sizeof (NatUdpTimeOut) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;;
        SnmpNotifyInfo.u4Indices = NAT_ZERO;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValNatUdpTimeOut));
    }

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatTrcFlag
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatTrcFlag (INT4 i4SetValNatTrcFlag)
{
    int                 rc;
    tNatwnmhSetNatTrcFlag lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_TRC_FLAG;
    lv.i4SetValNatTrcFlag = i4SetValNatTrcFlag;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatTrcFlag;
        SnmpNotifyInfo.u4OidLen = sizeof (NatTrcFlag) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;;
        SnmpNotifyInfo.u4Indices = NAT_ZERO;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValNatTrcFlag));
    }

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatIKEPortTranslation
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatIKEPortTranslation (INT4 i4SetValNatIKEPortTranslation)
{
    int                 rc;
    tNatwnmhSetNatIKEPortTranslation lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_I_K_E_PORT_TRANSLATION;
    lv.i4SetValNatIKEPortTranslation = i4SetValNatIKEPortTranslation;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatIKEPortTranslation;
        SnmpNotifyInfo.u4OidLen =
            sizeof (NatIKEPortTranslation) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;;
        SnmpNotifyInfo.u4Indices = NAT_ZERO;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValNatIKEPortTranslation));
    }

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatIKETimeout
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatIKETimeout (INT4 i4SetValNatIKETimeout)
{
    int                 rc;
    tNatwnmhSetNatIKETimeout lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_I_K_E_TIMEOUT;
    lv.i4SetValNatIKETimeout = i4SetValNatIKETimeout;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatIKETimeout;
        SnmpNotifyInfo.u4OidLen = sizeof (NatIKETimeout) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;
        SnmpNotifyInfo.u4Indices = NAT_ZERO;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValNatIKETimeout));
    }

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatIPSecTimeout
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatIPSecTimeout (INT4 i4SetValNatIPSecTimeout)
{
    int                 rc;
    tNatwnmhSetNatIPSecTimeout lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_I_P_SEC_TIMEOUT;
    lv.i4SetValNatIPSecTimeout = i4SetValNatIPSecTimeout;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatIPSecTimeout;
        SnmpNotifyInfo.u4OidLen = sizeof (NatIPSecTimeout) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;
        SnmpNotifyInfo.u4Indices = NAT_ZERO;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValNatIPSecTimeout));
    }

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatIPSecPendingTimeout
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatIPSecPendingTimeout (INT4 i4SetValNatIPSecPendingTimeout)
{
    int                 rc;
    tNatwnmhSetNatIPSecPendingTimeout lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_I_P_SEC_PENDING_TIMEOUT;
    lv.i4SetValNatIPSecPendingTimeout = i4SetValNatIPSecPendingTimeout;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatIPSecPendingTimeout;
        SnmpNotifyInfo.u4OidLen =
            sizeof (NatIPSecPendingTimeout) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;
        SnmpNotifyInfo.u4Indices = NAT_ZERO;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                          i4SetValNatIPSecPendingTimeout));
    }

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatIPSecMaxRetry
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatIPSecMaxRetry (INT4 i4SetValNatIPSecMaxRetry)
{
    int                 rc;
    tNatwnmhSetNatIPSecMaxRetry lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_I_P_SEC_MAX_RETRY;
    lv.i4SetValNatIPSecMaxRetry = i4SetValNatIPSecMaxRetry;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatIPSecMaxRetry;
        SnmpNotifyInfo.u4OidLen = sizeof (NatIPSecMaxRetry) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;
        SnmpNotifyInfo.u4Indices = NAT_ZERO;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValNatIPSecMaxRetry));
    }

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetSipAlgPort
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetSipAlgPort (INT4 i4SetValSipAlgPort)
{
    int                 rc;
    tNatwnmhSetSipAlgPort lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_SIP_ALG_PORT;
    lv.i4SetValSipAlgPort = i4SetValSipAlgPort;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = SipAlgPort;
        SnmpNotifyInfo.u4OidLen = sizeof (SipAlgPort) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;
        SnmpNotifyInfo.u4Indices = NAT_ZERO;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValSipAlgPort));
    }

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatSipAlgPartialEntryTimeOut
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatSipAlgPartialEntryTimeOut (INT4 i4SetValNatSipAlgPartialEntryTimeOut)
{
    int                 rc;
    tNatwnmhSetNatSipAlgPartialEntryTimeOut lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_SIP_ALG_PARTIAL_ENTRY_TIME_OUT;
    lv.i4SetValNatSipAlgPartialEntryTimeOut =
        i4SetValNatSipAlgPartialEntryTimeOut;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatSipAlgPartialEntryTimeOut;
        SnmpNotifyInfo.u4OidLen =
            sizeof (NatSipAlgPartialEntryTimeOut) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;
        SnmpNotifyInfo.u4Indices = NAT_ZERO;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                          i4SetValNatSipAlgPartialEntryTimeOut));
    }

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatEnable
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatEnable (UINT4 *pu4ErrorCode, INT4 i4TestValNatEnable)
{
    int                 rc;
    tNatwnmhTestv2NatEnable lv;

    lv.cmd = NMH_TESTV2_NAT_ENABLE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValNatEnable = i4TestValNatEnable;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatTypicalNumberOfEntries
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatTypicalNumberOfEntries (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValNatTypicalNumberOfEntries)
{
    int                 rc;
    tNatwnmhTestv2NatTypicalNumberOfEntries lv;

    lv.cmd = NMH_TESTV2_NAT_TYPICAL_NUMBER_OF_ENTRIES;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValNatTypicalNumberOfEntries = i4TestValNatTypicalNumberOfEntries;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatTranslatedLocalPortStart
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatTranslatedLocalPortStart (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValNatTranslatedLocalPortStart)
{
    int                 rc;
    tNatwnmhTestv2NatTranslatedLocalPortStart lv;

    lv.cmd = NMH_TESTV2_NAT_TRANSLATED_LOCAL_PORT_START;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValNatTranslatedLocalPortStart =
        i4TestValNatTranslatedLocalPortStart;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatIdleTimeOut
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatIdleTimeOut (UINT4 *pu4ErrorCode, INT4 i4TestValNatIdleTimeOut)
{
    int                 rc;
    tNatwnmhTestv2NatIdleTimeOut lv;

    lv.cmd = NMH_TESTV2_NAT_IDLE_TIME_OUT;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValNatIdleTimeOut = i4TestValNatIdleTimeOut;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatTcpTimeOut
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatTcpTimeOut (UINT4 *pu4ErrorCode, INT4 i4TestValNatTcpTimeOut)
{
    int                 rc;
    tNatwnmhTestv2NatTcpTimeOut lv;

    lv.cmd = NMH_TESTV2_NAT_TCP_TIME_OUT;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValNatTcpTimeOut = i4TestValNatTcpTimeOut;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatUdpTimeOut
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatUdpTimeOut (UINT4 *pu4ErrorCode, INT4 i4TestValNatUdpTimeOut)
{
    int                 rc;
    tNatwnmhTestv2NatUdpTimeOut lv;

    lv.cmd = NMH_TESTV2_NAT_UDP_TIME_OUT;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValNatUdpTimeOut = i4TestValNatUdpTimeOut;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatTrcFlag
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatTrcFlag (UINT4 *pu4ErrorCode, INT4 i4TestValNatTrcFlag)
{
    int                 rc;
    tNatwnmhTestv2NatTrcFlag lv;

    lv.cmd = NMH_TESTV2_NAT_TRC_FLAG;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValNatTrcFlag = i4TestValNatTrcFlag;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatIKEPortTranslation
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatIKEPortTranslation (UINT4 *pu4ErrorCode,
                                INT4 i4TestValNatIKEPortTranslation)
{
    int                 rc;
    tNatwnmhTestv2NatIKEPortTranslation lv;

    lv.cmd = NMH_TESTV2_NAT_I_K_E_PORT_TRANSLATION;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValNatIKEPortTranslation = i4TestValNatIKEPortTranslation;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatIKETimeout
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatIKETimeout (UINT4 *pu4ErrorCode, INT4 i4TestValNatIKETimeout)
{
    int                 rc;
    tNatwnmhTestv2NatIKETimeout lv;

    lv.cmd = NMH_TESTV2_NAT_I_K_E_TIMEOUT;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValNatIKETimeout = i4TestValNatIKETimeout;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatIPSecTimeout
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatIPSecTimeout (UINT4 *pu4ErrorCode, INT4 i4TestValNatIPSecTimeout)
{
    int                 rc;
    tNatwnmhTestv2NatIPSecTimeout lv;

    lv.cmd = NMH_TESTV2_NAT_I_P_SEC_TIMEOUT;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValNatIPSecTimeout = i4TestValNatIPSecTimeout;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatIPSecPendingTimeout
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatIPSecPendingTimeout (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValNatIPSecPendingTimeout)
{
    int                 rc;
    tNatwnmhTestv2NatIPSecPendingTimeout lv;

    lv.cmd = NMH_TESTV2_NAT_I_P_SEC_PENDING_TIMEOUT;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValNatIPSecPendingTimeout = i4TestValNatIPSecPendingTimeout;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatIPSecMaxRetry
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatIPSecMaxRetry (UINT4 *pu4ErrorCode, INT4 i4TestValNatIPSecMaxRetry)
{
    int                 rc;
    tNatwnmhTestv2NatIPSecMaxRetry lv;

    lv.cmd = NMH_TESTV2_NAT_I_P_SEC_MAX_RETRY;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValNatIPSecMaxRetry = i4TestValNatIPSecMaxRetry;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2SipAlgPort
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2SipAlgPort (UINT4 *pu4ErrorCode, INT4 i4TestValSipAlgPort)
{
    int                 rc;
    tNatwnmhTestv2SipAlgPort lv;

    lv.cmd = NMH_TESTV2_SIP_ALG_PORT;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValSipAlgPort = i4TestValSipAlgPort;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatSipAlgPartialEntryTimeOut
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatSipAlgPartialEntryTimeOut (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValNatSipAlgPartialEntryTimeOut)
{
    int                 rc;
    tNatwnmhTestv2NatSipAlgPartialEntryTimeOut lv;

    lv.cmd = NMH_TESTV2_NAT_SIP_ALG_PARTIAL_ENTRY_TIME_OUT;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValNatSipAlgPartialEntryTimeOut =
        i4TestValNatSipAlgPartialEntryTimeOut;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhValidateIndexInstanceNatDynamicTransTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhValidateIndexInstanceNatDynamicTransTable (INT4
                                              i4NatDynamicTransInterfaceNum,
                                              UINT4 u4NatDynamicTransLocalIp,
                                              INT4 i4NatDynamicTransLocalPort,
                                              UINT4 u4NatDynamicTransOutsideIp,
                                              INT4 i4NatDynamicTransOutsidePort)
{
    int                 rc;
    tNatwnmhValidateIndexInstanceNatDynamicTransTable lv;

    lv.cmd = NMH_VALIDATE_INDEX_INSTANCE_NAT_DYNAMIC_TRANS_TABLE;
    lv.i4NatDynamicTransInterfaceNum = i4NatDynamicTransInterfaceNum;
    lv.u4NatDynamicTransLocalIp = u4NatDynamicTransLocalIp;
    lv.i4NatDynamicTransLocalPort = i4NatDynamicTransLocalPort;
    lv.u4NatDynamicTransOutsideIp = u4NatDynamicTransOutsideIp;
    lv.i4NatDynamicTransOutsidePort = i4NatDynamicTransOutsidePort;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFirstIndexNatDynamicTransTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFirstIndexNatDynamicTransTable (INT4 *pi4NatDynamicTransInterfaceNum,
                                      UINT4 *pu4NatDynamicTransLocalIp,
                                      INT4 *pi4NatDynamicTransLocalPort,
                                      UINT4 *pu4NatDynamicTransOutsideIp,
                                      INT4 *pi4NatDynamicTransOutsidePort)
{
    int                 rc;
    tNatwnmhGetFirstIndexNatDynamicTransTable lv;

    lv.cmd = NMH_GET_FIRST_INDEX_NAT_DYNAMIC_TRANS_TABLE;
    lv.pi4NatDynamicTransInterfaceNum = pi4NatDynamicTransInterfaceNum;
    lv.pu4NatDynamicTransLocalIp = pu4NatDynamicTransLocalIp;
    lv.pi4NatDynamicTransLocalPort = pi4NatDynamicTransLocalPort;
    lv.pu4NatDynamicTransOutsideIp = pu4NatDynamicTransOutsideIp;
    lv.pi4NatDynamicTransOutsidePort = pi4NatDynamicTransOutsidePort;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNextIndexNatDynamicTransTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNextIndexNatDynamicTransTable (INT4 i4NatDynamicTransInterfaceNum,
                                     INT4 *pi4NextNatDynamicTransInterfaceNum,
                                     UINT4 u4NatDynamicTransLocalIp,
                                     UINT4 *pu4NextNatDynamicTransLocalIp,
                                     INT4 i4NatDynamicTransLocalPort,
                                     INT4 *pi4NextNatDynamicTransLocalPort,
                                     UINT4 u4NatDynamicTransOutsideIp,
                                     UINT4 *pu4NextNatDynamicTransOutsideIp,
                                     INT4 i4NatDynamicTransOutsidePort,
                                     INT4 *pi4NextNatDynamicTransOutsidePort)
{
    int                 rc;
    tNatwnmhGetNextIndexNatDynamicTransTable lv;

    lv.cmd = NMH_GET_NEXT_INDEX_NAT_DYNAMIC_TRANS_TABLE;
    lv.i4NatDynamicTransInterfaceNum = i4NatDynamicTransInterfaceNum;
    lv.pi4NextNatDynamicTransInterfaceNum = pi4NextNatDynamicTransInterfaceNum;
    lv.u4NatDynamicTransLocalIp = u4NatDynamicTransLocalIp;
    lv.pu4NextNatDynamicTransLocalIp = pu4NextNatDynamicTransLocalIp;
    lv.i4NatDynamicTransLocalPort = i4NatDynamicTransLocalPort;
    lv.pi4NextNatDynamicTransLocalPort = pi4NextNatDynamicTransLocalPort;
    lv.u4NatDynamicTransOutsideIp = u4NatDynamicTransOutsideIp;
    lv.pu4NextNatDynamicTransOutsideIp = pu4NextNatDynamicTransOutsideIp;
    lv.i4NatDynamicTransOutsidePort = i4NatDynamicTransOutsidePort;
    lv.pi4NextNatDynamicTransOutsidePort = pi4NextNatDynamicTransOutsidePort;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatDynamicTransTranslatedLocalIp
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatDynamicTransTranslatedLocalIp (INT4 i4NatDynamicTransInterfaceNum,
                                        UINT4 u4NatDynamicTransLocalIp,
                                        INT4 i4NatDynamicTransLocalPort,
                                        UINT4 u4NatDynamicTransOutsideIp,
                                        INT4 i4NatDynamicTransOutsidePort,
                                        UINT4
                                        *pu4RetValNatDynamicTransTranslatedLocalIp)
{
    int                 rc;
    tNatwnmhGetNatDynamicTransTranslatedLocalIp lv;

    lv.cmd = NMH_GET_NAT_DYNAMIC_TRANS_TRANSLATED_LOCAL_IP;
    lv.i4NatDynamicTransInterfaceNum = i4NatDynamicTransInterfaceNum;
    lv.u4NatDynamicTransLocalIp = u4NatDynamicTransLocalIp;
    lv.i4NatDynamicTransLocalPort = i4NatDynamicTransLocalPort;
    lv.u4NatDynamicTransOutsideIp = u4NatDynamicTransOutsideIp;
    lv.i4NatDynamicTransOutsidePort = i4NatDynamicTransOutsidePort;
    lv.pu4RetValNatDynamicTransTranslatedLocalIp =
        pu4RetValNatDynamicTransTranslatedLocalIp;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatDynamicTransTranslatedLocalPort
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatDynamicTransTranslatedLocalPort (INT4 i4NatDynamicTransInterfaceNum,
                                          UINT4 u4NatDynamicTransLocalIp,
                                          INT4 i4NatDynamicTransLocalPort,
                                          UINT4 u4NatDynamicTransOutsideIp,
                                          INT4 i4NatDynamicTransOutsidePort,
                                          INT4
                                          *pi4RetValNatDynamicTransTranslatedLocalPort)
{
    int                 rc;
    tNatwnmhGetNatDynamicTransTranslatedLocalPort lv;

    lv.cmd = NMH_GET_NAT_DYNAMIC_TRANS_TRANSLATED_LOCAL_PORT;
    lv.i4NatDynamicTransInterfaceNum = i4NatDynamicTransInterfaceNum;
    lv.u4NatDynamicTransLocalIp = u4NatDynamicTransLocalIp;
    lv.i4NatDynamicTransLocalPort = i4NatDynamicTransLocalPort;
    lv.u4NatDynamicTransOutsideIp = u4NatDynamicTransOutsideIp;
    lv.i4NatDynamicTransOutsidePort = i4NatDynamicTransOutsidePort;
    lv.pi4RetValNatDynamicTransTranslatedLocalPort =
        pi4RetValNatDynamicTransTranslatedLocalPort;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatDynamicTransLastUseTime
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatDynamicTransLastUseTime (INT4 i4NatDynamicTransInterfaceNum,
                                  UINT4 u4NatDynamicTransLocalIp,
                                  INT4 i4NatDynamicTransLocalPort,
                                  UINT4 u4NatDynamicTransOutsideIp,
                                  INT4 i4NatDynamicTransOutsidePort,
                                  INT4 *pi4RetValNatDynamicTransLastUseTime)
{
    int                 rc;
    tNatwnmhGetNatDynamicTransLastUseTime lv;

    lv.cmd = NMH_GET_NAT_DYNAMIC_TRANS_LAST_USE_TIME;
    lv.i4NatDynamicTransInterfaceNum = i4NatDynamicTransInterfaceNum;
    lv.u4NatDynamicTransLocalIp = u4NatDynamicTransLocalIp;
    lv.i4NatDynamicTransLocalPort = i4NatDynamicTransLocalPort;
    lv.u4NatDynamicTransOutsideIp = u4NatDynamicTransOutsideIp;
    lv.i4NatDynamicTransOutsidePort = i4NatDynamicTransOutsidePort;
    lv.pi4RetValNatDynamicTransLastUseTime =
        pi4RetValNatDynamicTransLastUseTime;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhValidateIndexInstanceNatGlobalAddressTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhValidateIndexInstanceNatGlobalAddressTable (INT4
                                               i4NatGlobalAddressInterfaceNum,
                                               UINT4
                                               u4NatGlobalAddressTranslatedLocalIp)
{
    int                 rc;
    tNatwnmhValidateIndexInstanceNatGlobalAddressTable lv;

    lv.cmd = NMH_VALIDATE_INDEX_INSTANCE_NAT_GLOBAL_ADDRESS_TABLE;
    lv.i4NatGlobalAddressInterfaceNum = i4NatGlobalAddressInterfaceNum;
    lv.u4NatGlobalAddressTranslatedLocalIp =
        u4NatGlobalAddressTranslatedLocalIp;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFirstIndexNatGlobalAddressTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFirstIndexNatGlobalAddressTable (INT4 *pi4NatGlobalAddressInterfaceNum,
                                       UINT4
                                       *pu4NatGlobalAddressTranslatedLocalIp)
{
    int                 rc;
    tNatwnmhGetFirstIndexNatGlobalAddressTable lv;

    lv.cmd = NMH_GET_FIRST_INDEX_NAT_GLOBAL_ADDRESS_TABLE;
    lv.pi4NatGlobalAddressInterfaceNum = pi4NatGlobalAddressInterfaceNum;
    lv.pu4NatGlobalAddressTranslatedLocalIp =
        pu4NatGlobalAddressTranslatedLocalIp;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNextIndexNatGlobalAddressTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNextIndexNatGlobalAddressTable (INT4 i4NatGlobalAddressInterfaceNum,
                                      INT4 *pi4NextNatGlobalAddressInterfaceNum,
                                      UINT4 u4NatGlobalAddressTranslatedLocalIp,
                                      UINT4
                                      *pu4NextNatGlobalAddressTranslatedLocalIp)
{
    int                 rc;
    tNatwnmhGetNextIndexNatGlobalAddressTable lv;

    lv.cmd = NMH_GET_NEXT_INDEX_NAT_GLOBAL_ADDRESS_TABLE;
    lv.i4NatGlobalAddressInterfaceNum = i4NatGlobalAddressInterfaceNum;
    lv.pi4NextNatGlobalAddressInterfaceNum =
        pi4NextNatGlobalAddressInterfaceNum;
    lv.u4NatGlobalAddressTranslatedLocalIp =
        u4NatGlobalAddressTranslatedLocalIp;
    lv.pu4NextNatGlobalAddressTranslatedLocalIp =
        pu4NextNatGlobalAddressTranslatedLocalIp;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatGlobalAddressMask
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatGlobalAddressMask (INT4 i4NatGlobalAddressInterfaceNum,
                            UINT4 u4NatGlobalAddressTranslatedLocalIp,
                            UINT4 *pu4RetValNatGlobalAddressMask)
{
    int                 rc;
    tNatwnmhGetNatGlobalAddressMask lv;

    lv.cmd = NMH_GET_NAT_GLOBAL_ADDRESS_MASK;
    lv.i4NatGlobalAddressInterfaceNum = i4NatGlobalAddressInterfaceNum;
    lv.u4NatGlobalAddressTranslatedLocalIp =
        u4NatGlobalAddressTranslatedLocalIp;
    lv.pu4RetValNatGlobalAddressMask = pu4RetValNatGlobalAddressMask;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatGlobalAddressEntryStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatGlobalAddressEntryStatus (INT4 i4NatGlobalAddressInterfaceNum,
                                   UINT4 u4NatGlobalAddressTranslatedLocalIp,
                                   INT4 *pi4RetValNatGlobalAddressEntryStatus)
{
    int                 rc;
    tNatwnmhGetNatGlobalAddressEntryStatus lv;

    lv.cmd = NMH_GET_NAT_GLOBAL_ADDRESS_ENTRY_STATUS;
    lv.i4NatGlobalAddressInterfaceNum = i4NatGlobalAddressInterfaceNum;
    lv.u4NatGlobalAddressTranslatedLocalIp =
        u4NatGlobalAddressTranslatedLocalIp;
    lv.pi4RetValNatGlobalAddressEntryStatus =
        pi4RetValNatGlobalAddressEntryStatus;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatGlobalAddressMask
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatGlobalAddressMask (INT4 i4NatGlobalAddressInterfaceNum,
                            UINT4 u4NatGlobalAddressTranslatedLocalIp,
                            UINT4 u4SetValNatGlobalAddressMask)
{
    int                 rc;
    tNatwnmhSetNatGlobalAddressMask lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_GLOBAL_ADDRESS_MASK;
    lv.i4NatGlobalAddressInterfaceNum = i4NatGlobalAddressInterfaceNum;
    lv.u4NatGlobalAddressTranslatedLocalIp =
        u4NatGlobalAddressTranslatedLocalIp;
    lv.u4SetValNatGlobalAddressMask = u4SetValNatGlobalAddressMask;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatGlobalAddressMask;
        SnmpNotifyInfo.u4OidLen =
            sizeof (NatGlobalAddressMask) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;
        SnmpNotifyInfo.u4Indices = 2;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p",
                          i4NatGlobalAddressInterfaceNum,
                          u4NatGlobalAddressTranslatedLocalIp,
                          u4SetValNatGlobalAddressMask));
    }
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatGlobalAddressEntryStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatGlobalAddressEntryStatus (INT4 i4NatGlobalAddressInterfaceNum,
                                   UINT4 u4NatGlobalAddressTranslatedLocalIp,
                                   INT4 i4SetValNatGlobalAddressEntryStatus)
{
    int                 rc;
    tNatwnmhSetNatGlobalAddressEntryStatus lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_GLOBAL_ADDRESS_ENTRY_STATUS;
    lv.i4NatGlobalAddressInterfaceNum = i4NatGlobalAddressInterfaceNum;
    lv.u4NatGlobalAddressTranslatedLocalIp =
        u4NatGlobalAddressTranslatedLocalIp;
    lv.i4SetValNatGlobalAddressEntryStatus =
        i4SetValNatGlobalAddressEntryStatus;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatGlobalAddressEntryStatus;
        SnmpNotifyInfo.u4OidLen =
            sizeof (NatGlobalAddressEntryStatus) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = TRUE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;
        SnmpNotifyInfo.u4Indices = 2;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                          i4NatGlobalAddressInterfaceNum,
                          u4NatGlobalAddressTranslatedLocalIp,
                          i4SetValNatGlobalAddressEntryStatus));
    }

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatGlobalAddressMask
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatGlobalAddressMask (UINT4 *pu4ErrorCode,
                               INT4 i4NatGlobalAddressInterfaceNum,
                               UINT4 u4NatGlobalAddressTranslatedLocalIp,
                               UINT4 u4TestValNatGlobalAddressMask)
{
    int                 rc;
    tNatwnmhTestv2NatGlobalAddressMask lv;

    lv.cmd = NMH_TESTV2_NAT_GLOBAL_ADDRESS_MASK;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4NatGlobalAddressInterfaceNum = i4NatGlobalAddressInterfaceNum;
    lv.u4NatGlobalAddressTranslatedLocalIp =
        u4NatGlobalAddressTranslatedLocalIp;
    lv.u4TestValNatGlobalAddressMask = u4TestValNatGlobalAddressMask;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatGlobalAddressEntryStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatGlobalAddressEntryStatus (UINT4 *pu4ErrorCode,
                                      INT4 i4NatGlobalAddressInterfaceNum,
                                      UINT4 u4NatGlobalAddressTranslatedLocalIp,
                                      INT4 i4TestValNatGlobalAddressEntryStatus)
{
    int                 rc;
    tNatwnmhTestv2NatGlobalAddressEntryStatus lv;

    lv.cmd = NMH_TESTV2_NAT_GLOBAL_ADDRESS_ENTRY_STATUS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4NatGlobalAddressInterfaceNum = i4NatGlobalAddressInterfaceNum;
    lv.u4NatGlobalAddressTranslatedLocalIp =
        u4NatGlobalAddressTranslatedLocalIp;
    lv.i4TestValNatGlobalAddressEntryStatus =
        i4TestValNatGlobalAddressEntryStatus;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhValidateIndexInstanceNatLocalAddressTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhValidateIndexInstanceNatLocalAddressTable (INT4
                                              i4NatLocalAddressInterfaceNumber,
                                              UINT4 u4NatLocalAddressLocalIp)
{
    int                 rc;
    tNatwnmhValidateIndexInstanceNatLocalAddressTable lv;

    lv.cmd = NMH_VALIDATE_INDEX_INSTANCE_NAT_LOCAL_ADDRESS_TABLE;
    lv.i4NatLocalAddressInterfaceNumber = i4NatLocalAddressInterfaceNumber;
    lv.u4NatLocalAddressLocalIp = u4NatLocalAddressLocalIp;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFirstIndexNatLocalAddressTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFirstIndexNatLocalAddressTable (INT4 *pi4NatLocalAddressInterfaceNumber,
                                      UINT4 *pu4NatLocalAddressLocalIp)
{
    int                 rc;
    tNatwnmhGetFirstIndexNatLocalAddressTable lv;

    lv.cmd = NMH_GET_FIRST_INDEX_NAT_LOCAL_ADDRESS_TABLE;
    lv.pi4NatLocalAddressInterfaceNumber = pi4NatLocalAddressInterfaceNumber;
    lv.pu4NatLocalAddressLocalIp = pu4NatLocalAddressLocalIp;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNextIndexNatLocalAddressTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNextIndexNatLocalAddressTable (INT4 i4NatLocalAddressInterfaceNumber,
                                     INT4
                                     *pi4NextNatLocalAddressInterfaceNumber,
                                     UINT4 u4NatLocalAddressLocalIp,
                                     UINT4 *pu4NextNatLocalAddressLocalIp)
{
    int                 rc;
    tNatwnmhGetNextIndexNatLocalAddressTable lv;

    lv.cmd = NMH_GET_NEXT_INDEX_NAT_LOCAL_ADDRESS_TABLE;
    lv.i4NatLocalAddressInterfaceNumber = i4NatLocalAddressInterfaceNumber;
    lv.pi4NextNatLocalAddressInterfaceNumber =
        pi4NextNatLocalAddressInterfaceNumber;
    lv.u4NatLocalAddressLocalIp = u4NatLocalAddressLocalIp;
    lv.pu4NextNatLocalAddressLocalIp = pu4NextNatLocalAddressLocalIp;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatLocalAddressMask
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatLocalAddressMask (INT4 i4NatLocalAddressInterfaceNumber,
                           UINT4 u4NatLocalAddressLocalIp,
                           UINT4 *pu4RetValNatLocalAddressMask)
{
    int                 rc;
    tNatwnmhGetNatLocalAddressMask lv;

    lv.cmd = NMH_GET_NAT_LOCAL_ADDRESS_MASK;
    lv.i4NatLocalAddressInterfaceNumber = i4NatLocalAddressInterfaceNumber;
    lv.u4NatLocalAddressLocalIp = u4NatLocalAddressLocalIp;
    lv.pu4RetValNatLocalAddressMask = pu4RetValNatLocalAddressMask;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatLocalAddressEntryStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatLocalAddressEntryStatus (INT4 i4NatLocalAddressInterfaceNumber,
                                  UINT4 u4NatLocalAddressLocalIp,
                                  INT4 *pi4RetValNatLocalAddressEntryStatus)
{
    int                 rc;
    tNatwnmhGetNatLocalAddressEntryStatus lv;

    lv.cmd = NMH_GET_NAT_LOCAL_ADDRESS_ENTRY_STATUS;
    lv.i4NatLocalAddressInterfaceNumber = i4NatLocalAddressInterfaceNumber;
    lv.u4NatLocalAddressLocalIp = u4NatLocalAddressLocalIp;
    lv.pi4RetValNatLocalAddressEntryStatus =
        pi4RetValNatLocalAddressEntryStatus;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatLocalAddressMask
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatLocalAddressMask (INT4 i4NatLocalAddressInterfaceNumber,
                           UINT4 u4NatLocalAddressLocalIp,
                           UINT4 u4SetValNatLocalAddressMask)
{
    int                 rc;
    tNatwnmhSetNatLocalAddressMask lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_LOCAL_ADDRESS_MASK;
    lv.i4NatLocalAddressInterfaceNumber = i4NatLocalAddressInterfaceNumber;
    lv.u4NatLocalAddressLocalIp = u4NatLocalAddressLocalIp;
    lv.u4SetValNatLocalAddressMask = u4SetValNatLocalAddressMask;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatLocalAddressMask;
        SnmpNotifyInfo.u4OidLen = sizeof (NatLocalAddressMask) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;
        SnmpNotifyInfo.u4Indices = 2;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p",
                          i4NatLocalAddressInterfaceNumber,
                          u4NatLocalAddressLocalIp,
                          u4SetValNatLocalAddressMask));
    }

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatLocalAddressEntryStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatLocalAddressEntryStatus (INT4 i4NatLocalAddressInterfaceNumber,
                                  UINT4 u4NatLocalAddressLocalIp,
                                  INT4 i4SetValNatLocalAddressEntryStatus)
{
    int                 rc;
    tNatwnmhSetNatLocalAddressEntryStatus lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_LOCAL_ADDRESS_ENTRY_STATUS;
    lv.i4NatLocalAddressInterfaceNumber = i4NatLocalAddressInterfaceNumber;
    lv.u4NatLocalAddressLocalIp = u4NatLocalAddressLocalIp;
    lv.i4SetValNatLocalAddressEntryStatus = i4SetValNatLocalAddressEntryStatus;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatLocalAddressEntryStatus;
        SnmpNotifyInfo.u4OidLen =
            sizeof (NatLocalAddressEntryStatus) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = TRUE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;
        SnmpNotifyInfo.u4Indices = 2;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                          i4NatLocalAddressInterfaceNumber,
                          u4NatLocalAddressLocalIp,
                          i4SetValNatLocalAddressEntryStatus));
    }

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatLocalAddressMask
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatLocalAddressMask (UINT4 *pu4ErrorCode,
                              INT4 i4NatLocalAddressInterfaceNumber,
                              UINT4 u4NatLocalAddressLocalIp,
                              UINT4 u4TestValNatLocalAddressMask)
{
    int                 rc;
    tNatwnmhTestv2NatLocalAddressMask lv;

    lv.cmd = NMH_TESTV2_NAT_LOCAL_ADDRESS_MASK;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4NatLocalAddressInterfaceNumber = i4NatLocalAddressInterfaceNumber;
    lv.u4NatLocalAddressLocalIp = u4NatLocalAddressLocalIp;
    lv.u4TestValNatLocalAddressMask = u4TestValNatLocalAddressMask;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatLocalAddressEntryStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatLocalAddressEntryStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4NatLocalAddressInterfaceNumber,
                                     UINT4 u4NatLocalAddressLocalIp,
                                     INT4 i4TestValNatLocalAddressEntryStatus)
{
    int                 rc;
    tNatwnmhTestv2NatLocalAddressEntryStatus lv;

    lv.cmd = NMH_TESTV2_NAT_LOCAL_ADDRESS_ENTRY_STATUS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4NatLocalAddressInterfaceNumber = i4NatLocalAddressInterfaceNumber;
    lv.u4NatLocalAddressLocalIp = u4NatLocalAddressLocalIp;
    lv.i4TestValNatLocalAddressEntryStatus =
        i4TestValNatLocalAddressEntryStatus;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhValidateIndexInstanceNatStaticTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhValidateIndexInstanceNatStaticTable (INT4 i4NatStaticInterfaceNum,
                                        UINT4 u4NatStaticLocalIp)
{
    int                 rc;
    tNatwnmhValidateIndexInstanceNatStaticTable lv;

    lv.cmd = NMH_VALIDATE_INDEX_INSTANCE_NAT_STATIC_TABLE;
    lv.i4NatStaticInterfaceNum = i4NatStaticInterfaceNum;
    lv.u4NatStaticLocalIp = u4NatStaticLocalIp;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFirstIndexNatStaticTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFirstIndexNatStaticTable (INT4 *pi4NatStaticInterfaceNum,
                                UINT4 *pu4NatStaticLocalIp)
{
    int                 rc;
    tNatwnmhGetFirstIndexNatStaticTable lv;

    lv.cmd = NMH_GET_FIRST_INDEX_NAT_STATIC_TABLE;
    lv.pi4NatStaticInterfaceNum = pi4NatStaticInterfaceNum;
    lv.pu4NatStaticLocalIp = pu4NatStaticLocalIp;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNextIndexNatStaticTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNextIndexNatStaticTable (INT4 i4NatStaticInterfaceNum,
                               INT4 *pi4NextNatStaticInterfaceNum,
                               UINT4 u4NatStaticLocalIp,
                               UINT4 *pu4NextNatStaticLocalIp)
{
    int                 rc;
    tNatwnmhGetNextIndexNatStaticTable lv;

    lv.cmd = NMH_GET_NEXT_INDEX_NAT_STATIC_TABLE;
    lv.i4NatStaticInterfaceNum = i4NatStaticInterfaceNum;
    lv.pi4NextNatStaticInterfaceNum = pi4NextNatStaticInterfaceNum;
    lv.u4NatStaticLocalIp = u4NatStaticLocalIp;
    lv.pu4NextNatStaticLocalIp = pu4NextNatStaticLocalIp;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatStaticTranslatedLocalIp
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatStaticTranslatedLocalIp (INT4 i4NatStaticInterfaceNum,
                                  UINT4 u4NatStaticLocalIp,
                                  UINT4 *pu4RetValNatStaticTranslatedLocalIp)
{
    int                 rc;
    tNatwnmhGetNatStaticTranslatedLocalIp lv;

    lv.cmd = NMH_GET_NAT_STATIC_TRANSLATED_LOCAL_IP;
    lv.i4NatStaticInterfaceNum = i4NatStaticInterfaceNum;
    lv.u4NatStaticLocalIp = u4NatStaticLocalIp;
    lv.pu4RetValNatStaticTranslatedLocalIp =
        pu4RetValNatStaticTranslatedLocalIp;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatStaticEntryStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatStaticEntryStatus (INT4 i4NatStaticInterfaceNum,
                            UINT4 u4NatStaticLocalIp,
                            INT4 *pi4RetValNatStaticEntryStatus)
{
    int                 rc;
    tNatwnmhGetNatStaticEntryStatus lv;

    lv.cmd = NMH_GET_NAT_STATIC_ENTRY_STATUS;
    lv.i4NatStaticInterfaceNum = i4NatStaticInterfaceNum;
    lv.u4NatStaticLocalIp = u4NatStaticLocalIp;
    lv.pi4RetValNatStaticEntryStatus = pi4RetValNatStaticEntryStatus;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatStaticTranslatedLocalIp
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatStaticTranslatedLocalIp (INT4 i4NatStaticInterfaceNum,
                                  UINT4 u4NatStaticLocalIp,
                                  UINT4 u4SetValNatStaticTranslatedLocalIp)
{
    int                 rc;
    tNatwnmhSetNatStaticTranslatedLocalIp lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_STATIC_TRANSLATED_LOCAL_IP;
    lv.i4NatStaticInterfaceNum = i4NatStaticInterfaceNum;
    lv.u4NatStaticLocalIp = u4NatStaticLocalIp;
    lv.u4SetValNatStaticTranslatedLocalIp = u4SetValNatStaticTranslatedLocalIp;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatStaticTranslatedLocalIp;
        SnmpNotifyInfo.u4OidLen =
            sizeof (NatStaticTranslatedLocalIp) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;
        SnmpNotifyInfo.u4Indices = 2;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p",
                          i4NatStaticInterfaceNum,
                          u4NatStaticLocalIp,
                          u4SetValNatStaticTranslatedLocalIp));
    }

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatStaticEntryStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatStaticEntryStatus (INT4 i4NatStaticInterfaceNum,
                            UINT4 u4NatStaticLocalIp,
                            INT4 i4SetValNatStaticEntryStatus)
{
    int                 rc;
    tNatwnmhSetNatStaticEntryStatus lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_STATIC_ENTRY_STATUS;
    lv.i4NatStaticInterfaceNum = i4NatStaticInterfaceNum;
    lv.u4NatStaticLocalIp = u4NatStaticLocalIp;
    lv.i4SetValNatStaticEntryStatus = i4SetValNatStaticEntryStatus;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatStaticEntryStatus;
        SnmpNotifyInfo.u4OidLen =
            sizeof (NatStaticEntryStatus) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = TRUE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;
        SnmpNotifyInfo.u4Indices = 2;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                          i4NatStaticInterfaceNum,
                          u4NatStaticLocalIp, i4SetValNatStaticEntryStatus));
    }

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatStaticTranslatedLocalIp
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatStaticTranslatedLocalIp (UINT4 *pu4ErrorCode,
                                     INT4 i4NatStaticInterfaceNum,
                                     UINT4 u4NatStaticLocalIp,
                                     UINT4 u4TestValNatStaticTranslatedLocalIp)
{
    int                 rc;
    tNatwnmhTestv2NatStaticTranslatedLocalIp lv;

    lv.cmd = NMH_TESTV2_NAT_STATIC_TRANSLATED_LOCAL_IP;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4NatStaticInterfaceNum = i4NatStaticInterfaceNum;
    lv.u4NatStaticLocalIp = u4NatStaticLocalIp;
    lv.u4TestValNatStaticTranslatedLocalIp =
        u4TestValNatStaticTranslatedLocalIp;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatStaticEntryStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatStaticEntryStatus (UINT4 *pu4ErrorCode,
                               INT4 i4NatStaticInterfaceNum,
                               UINT4 u4NatStaticLocalIp,
                               INT4 i4TestValNatStaticEntryStatus)
{
    int                 rc;
    tNatwnmhTestv2NatStaticEntryStatus lv;

    lv.cmd = NMH_TESTV2_NAT_STATIC_ENTRY_STATUS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4NatStaticInterfaceNum = i4NatStaticInterfaceNum;
    lv.u4NatStaticLocalIp = u4NatStaticLocalIp;
    lv.i4TestValNatStaticEntryStatus = i4TestValNatStaticEntryStatus;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhValidateIndexInstanceNatStaticNaptTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhValidateIndexInstanceNatStaticNaptTable (INT4 i4NatStaticNaptInterfaceNum,
                                            UINT4 u4NatStaticNaptLocalIp,
                                            INT4 i4NatStaticNaptStartLocalPort,
                                            INT4 i4NatStaticNaptEndLocalPort,
                                            INT4 i4NatStaticNaptProtocolNumber)
{
    int                 rc;
    tNatwnmhValidateIndexInstanceNatStaticNaptTable lv;

    lv.cmd = NMH_VALIDATE_INDEX_INSTANCE_NAT_STATIC_NAPT_TABLE;
    lv.i4NatStaticNaptInterfaceNum = i4NatStaticNaptInterfaceNum;
    lv.u4NatStaticNaptLocalIp = u4NatStaticNaptLocalIp;
    lv.i4NatStaticNaptStartLocalPort = i4NatStaticNaptStartLocalPort;
    lv.i4NatStaticNaptEndLocalPort = i4NatStaticNaptEndLocalPort;
    lv.i4NatStaticNaptProtocolNumber = i4NatStaticNaptProtocolNumber;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFirstIndexNatStaticNaptTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFirstIndexNatStaticNaptTable (INT4 *pi4NatStaticNaptInterfaceNum,
                                    UINT4 *pu4NatStaticNaptLocalIp,
                                    INT4 *pi4NatStaticNaptStartLocalPort,
                                    INT4 *pi4NatStaticNaptEndLocalPort,
                                    INT4 *pi4NatStaticNaptProtocolNumber)
{
    int                 rc;
    tNatwnmhGetFirstIndexNatStaticNaptTable lv;

    lv.cmd = NMH_GET_FIRST_INDEX_NAT_STATIC_NAPT_TABLE;
    lv.pi4NatStaticNaptInterfaceNum = pi4NatStaticNaptInterfaceNum;
    lv.pu4NatStaticNaptLocalIp = pu4NatStaticNaptLocalIp;
    lv.pi4NatStaticNaptStartLocalPort = pi4NatStaticNaptStartLocalPort;
    lv.pi4NatStaticNaptEndLocalPort = pi4NatStaticNaptEndLocalPort;
    lv.pi4NatStaticNaptProtocolNumber = pi4NatStaticNaptProtocolNumber;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNextIndexNatStaticNaptTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNextIndexNatStaticNaptTable (INT4 i4NatStaticNaptInterfaceNum,
                                   INT4 *pi4NextNatStaticNaptInterfaceNum,
                                   UINT4 u4NatStaticNaptLocalIp,
                                   UINT4 *pu4NextNatStaticNaptLocalIp,
                                   INT4 i4NatStaticNaptStartLocalPort,
                                   INT4 *pi4NextNatStaticNaptStartLocalPort,
                                   INT4 i4NatStaticNaptEndLocalPort,
                                   INT4 *pi4NextNatStaticNaptEndLocalPort,
                                   INT4 i4NatStaticNaptProtocolNumber,
                                   INT4 *pi4NextNatStaticNaptProtocolNumber)
{
    int                 rc;
    tNatwnmhGetNextIndexNatStaticNaptTable lv;

    lv.cmd = NMH_GET_NEXT_INDEX_NAT_STATIC_NAPT_TABLE;
    lv.i4NatStaticNaptInterfaceNum = i4NatStaticNaptInterfaceNum;
    lv.pi4NextNatStaticNaptInterfaceNum = pi4NextNatStaticNaptInterfaceNum;
    lv.u4NatStaticNaptLocalIp = u4NatStaticNaptLocalIp;
    lv.pu4NextNatStaticNaptLocalIp = pu4NextNatStaticNaptLocalIp;
    lv.i4NatStaticNaptStartLocalPort = i4NatStaticNaptStartLocalPort;
    lv.pi4NextNatStaticNaptStartLocalPort = pi4NextNatStaticNaptStartLocalPort;
    lv.i4NatStaticNaptEndLocalPort = i4NatStaticNaptEndLocalPort;
    lv.pi4NextNatStaticNaptEndLocalPort = pi4NextNatStaticNaptEndLocalPort;
    lv.i4NatStaticNaptProtocolNumber = i4NatStaticNaptProtocolNumber;
    lv.pi4NextNatStaticNaptProtocolNumber = pi4NextNatStaticNaptProtocolNumber;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatStaticNaptTranslatedLocalIp
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatStaticNaptTranslatedLocalIp (INT4 i4NatStaticNaptInterfaceNum,
                                      UINT4 u4NatStaticNaptLocalIp,
                                      INT4 i4NatStaticNaptStartLocalPort,
                                      INT4 i4NatStaticNaptEndLocalPort,
                                      INT4 i4NatStaticNaptProtocolNumber,
                                      UINT4
                                      *pu4RetValNatStaticNaptTranslatedLocalIp)
{
    int                 rc;
    tNatwnmhGetNatStaticNaptTranslatedLocalIp lv;

    lv.cmd = NMH_GET_NAT_STATIC_NAPT_TRANSLATED_LOCAL_IP;
    lv.i4NatStaticNaptInterfaceNum = i4NatStaticNaptInterfaceNum;
    lv.u4NatStaticNaptLocalIp = u4NatStaticNaptLocalIp;
    lv.i4NatStaticNaptStartLocalPort = i4NatStaticNaptStartLocalPort;
    lv.i4NatStaticNaptEndLocalPort = i4NatStaticNaptEndLocalPort;
    lv.i4NatStaticNaptProtocolNumber = i4NatStaticNaptProtocolNumber;
    lv.pu4RetValNatStaticNaptTranslatedLocalIp =
        pu4RetValNatStaticNaptTranslatedLocalIp;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatStaticNaptTranslatedLocalPort
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatStaticNaptTranslatedLocalPort (INT4 i4NatStaticNaptInterfaceNum,
                                        UINT4 u4NatStaticNaptLocalIp,
                                        INT4 i4NatStaticNaptStartLocalPort,
                                        INT4 i4NatStaticNaptEndLocalPort,
                                        INT4 i4NatStaticNaptProtocolNumber,
                                        INT4
                                        *pi4RetValNatStaticNaptTranslatedLocalPort)
{
    int                 rc;
    tNatwnmhGetNatStaticNaptTranslatedLocalPort lv;

    lv.cmd = NMH_GET_NAT_STATIC_NAPT_TRANSLATED_LOCAL_PORT;
    lv.i4NatStaticNaptInterfaceNum = i4NatStaticNaptInterfaceNum;
    lv.u4NatStaticNaptLocalIp = u4NatStaticNaptLocalIp;
    lv.i4NatStaticNaptStartLocalPort = i4NatStaticNaptStartLocalPort;
    lv.i4NatStaticNaptEndLocalPort = i4NatStaticNaptEndLocalPort;
    lv.i4NatStaticNaptProtocolNumber = i4NatStaticNaptProtocolNumber;
    lv.pi4RetValNatStaticNaptTranslatedLocalPort =
        pi4RetValNatStaticNaptTranslatedLocalPort;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatStaticNaptDescription
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatStaticNaptDescription (INT4 i4NatStaticNaptInterfaceNum,
                                UINT4 u4NatStaticNaptLocalIp,
                                INT4 i4NatStaticNaptStartLocalPort,
                                INT4 i4NatStaticNaptEndLocalPort,
                                INT4 i4NatStaticNaptProtocolNumber,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValNatStaticNaptDescription)
{
    int                 rc;
    tNatwnmhGetNatStaticNaptDescription lv;

    lv.cmd = NMH_GET_NAT_STATIC_NAPT_DESCRIPTION;
    lv.i4NatStaticNaptInterfaceNum = i4NatStaticNaptInterfaceNum;
    lv.u4NatStaticNaptLocalIp = u4NatStaticNaptLocalIp;
    lv.i4NatStaticNaptStartLocalPort = i4NatStaticNaptStartLocalPort;
    lv.i4NatStaticNaptEndLocalPort = i4NatStaticNaptEndLocalPort;
    lv.i4NatStaticNaptProtocolNumber = i4NatStaticNaptProtocolNumber;
    lv.pRetValNatStaticNaptDescription = pRetValNatStaticNaptDescription;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatStaticNaptLeaseDuration
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatStaticNaptLeaseDuration (INT4 i4NatStaticNaptInterfaceNum,
                                  UINT4 u4NatStaticNaptLocalIp,
                                  INT4 i4NatStaticNaptStartLocalPort,
                                  INT4 i4NatStaticNaptEndLocalPort,
                                  INT4 i4NatStaticNaptProtocolNumber,
                                  INT4 *pi4RetValNatStaticNaptLeaseDuration)
{
    int                 rc;
    tNatwnmhGetNatStaticNaptLeaseDuration lv;

    lv.cmd = NMH_GET_NAT_STATIC_NAPT_LEASE_DURATION;
    lv.i4NatStaticNaptInterfaceNum = i4NatStaticNaptInterfaceNum;
    lv.u4NatStaticNaptLocalIp = u4NatStaticNaptLocalIp;
    lv.i4NatStaticNaptStartLocalPort = i4NatStaticNaptStartLocalPort;
    lv.i4NatStaticNaptEndLocalPort = i4NatStaticNaptEndLocalPort;
    lv.i4NatStaticNaptProtocolNumber = i4NatStaticNaptProtocolNumber;
    lv.pi4RetValNatStaticNaptLeaseDuration =
        pi4RetValNatStaticNaptLeaseDuration;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatStaticNaptEntryStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatStaticNaptEntryStatus (INT4 i4NatStaticNaptInterfaceNum,
                                UINT4 u4NatStaticNaptLocalIp,
                                INT4 i4NatStaticNaptStartLocalPort,
                                INT4 i4NatStaticNaptEndLocalPort,
                                INT4 i4NatStaticNaptProtocolNumber,
                                INT4 *pi4RetValNatStaticNaptEntryStatus)
{
    int                 rc;
    tNatwnmhGetNatStaticNaptEntryStatus lv;

    lv.cmd = NMH_GET_NAT_STATIC_NAPT_ENTRY_STATUS;
    lv.i4NatStaticNaptInterfaceNum = i4NatStaticNaptInterfaceNum;
    lv.u4NatStaticNaptLocalIp = u4NatStaticNaptLocalIp;
    lv.i4NatStaticNaptStartLocalPort = i4NatStaticNaptStartLocalPort;
    lv.i4NatStaticNaptEndLocalPort = i4NatStaticNaptEndLocalPort;
    lv.i4NatStaticNaptProtocolNumber = i4NatStaticNaptProtocolNumber;
    lv.pi4RetValNatStaticNaptEntryStatus = pi4RetValNatStaticNaptEntryStatus;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatStaticNaptTranslatedLocalIp
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatStaticNaptTranslatedLocalIp (INT4 i4NatStaticNaptInterfaceNum,
                                      UINT4 u4NatStaticNaptLocalIp,
                                      INT4 i4NatStaticNaptStartLocalPort,
                                      INT4 i4NatStaticNaptEndLocalPort,
                                      INT4 i4NatStaticNaptProtocolNumber,
                                      UINT4
                                      u4SetValNatStaticNaptTranslatedLocalIp)
{
    int                 rc;
    tNatwnmhSetNatStaticNaptTranslatedLocalIp lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_STATIC_NAPT_TRANSLATED_LOCAL_IP;
    lv.i4NatStaticNaptInterfaceNum = i4NatStaticNaptInterfaceNum;
    lv.u4NatStaticNaptLocalIp = u4NatStaticNaptLocalIp;
    lv.i4NatStaticNaptStartLocalPort = i4NatStaticNaptStartLocalPort;
    lv.i4NatStaticNaptEndLocalPort = i4NatStaticNaptEndLocalPort;
    lv.i4NatStaticNaptProtocolNumber = i4NatStaticNaptProtocolNumber;
    lv.u4SetValNatStaticNaptTranslatedLocalIp =
        u4SetValNatStaticNaptTranslatedLocalIp;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatStaticNaptTranslatedLocalIp;
        SnmpNotifyInfo.u4OidLen =
            sizeof (NatStaticNaptTranslatedLocalIp) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;
        SnmpNotifyInfo.u4Indices = 5;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i %i %p",
                          i4NatStaticNaptInterfaceNum,
                          u4NatStaticNaptLocalIp,
                          i4NatStaticNaptStartLocalPort,
                          i4NatStaticNaptEndLocalPort,
                          i4NatStaticNaptProtocolNumber,
                          u4SetValNatStaticNaptTranslatedLocalIp));
    }

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatStaticNaptTranslatedLocalPort
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatStaticNaptTranslatedLocalPort (INT4 i4NatStaticNaptInterfaceNum,
                                        UINT4 u4NatStaticNaptLocalIp,
                                        INT4 i4NatStaticNaptStartLocalPort,
                                        INT4 i4NatStaticNaptEndLocalPort,
                                        INT4 i4NatStaticNaptProtocolNumber,
                                        INT4
                                        i4SetValNatStaticNaptTranslatedLocalPort)
{
    int                 rc;
    tNatwnmhSetNatStaticNaptTranslatedLocalPort lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_STATIC_NAPT_TRANSLATED_LOCAL_PORT;
    lv.i4NatStaticNaptInterfaceNum = i4NatStaticNaptInterfaceNum;
    lv.u4NatStaticNaptLocalIp = u4NatStaticNaptLocalIp;
    lv.i4NatStaticNaptStartLocalPort = i4NatStaticNaptStartLocalPort;
    lv.i4NatStaticNaptEndLocalPort = i4NatStaticNaptEndLocalPort;
    lv.i4NatStaticNaptProtocolNumber = i4NatStaticNaptProtocolNumber;
    lv.i4SetValNatStaticNaptTranslatedLocalPort =
        i4SetValNatStaticNaptTranslatedLocalPort;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatStaticNaptTranslatedLocalPort;
        SnmpNotifyInfo.u4OidLen =
            sizeof (NatStaticNaptTranslatedLocalPort) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;
        SnmpNotifyInfo.u4Indices = 5;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i %i %i",
                          i4NatStaticNaptInterfaceNum,
                          u4NatStaticNaptLocalIp,
                          i4NatStaticNaptStartLocalPort,
                          i4NatStaticNaptEndLocalPort,
                          i4NatStaticNaptProtocolNumber,
                          i4SetValNatStaticNaptTranslatedLocalPort));
    }

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatStaticNaptDescription
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatStaticNaptDescription (INT4 i4NatStaticNaptInterfaceNum,
                                UINT4 u4NatStaticNaptLocalIp,
                                INT4 i4NatStaticNaptStartLocalPort,
                                INT4 i4NatStaticNaptEndLocalPort,
                                INT4 i4NatStaticNaptProtocolNumber,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValNatStaticNaptDescription)
{
    int                 rc;
    tNatwnmhSetNatStaticNaptDescription lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_STATIC_NAPT_DESCRIPTION;
    lv.i4NatStaticNaptInterfaceNum = i4NatStaticNaptInterfaceNum;
    lv.u4NatStaticNaptLocalIp = u4NatStaticNaptLocalIp;
    lv.i4NatStaticNaptStartLocalPort = i4NatStaticNaptStartLocalPort;
    lv.i4NatStaticNaptEndLocalPort = i4NatStaticNaptEndLocalPort;
    lv.i4NatStaticNaptProtocolNumber = i4NatStaticNaptProtocolNumber;
    lv.pSetValNatStaticNaptDescription = pSetValNatStaticNaptDescription;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatStaticNaptDescription;
        SnmpNotifyInfo.u4OidLen =
            sizeof (NatStaticNaptDescription) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;
        SnmpNotifyInfo.u4Indices = 5;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i %i %s",
                          i4NatStaticNaptInterfaceNum,
                          u4NatStaticNaptLocalIp,
                          i4NatStaticNaptStartLocalPort,
                          i4NatStaticNaptEndLocalPort,
                          i4NatStaticNaptProtocolNumber,
                          pSetValNatStaticNaptDescription));
    }

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatStaticNaptLeaseDuration
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatStaticNaptLeaseDuration (INT4 i4NatStaticNaptInterfaceNum,
                                  UINT4 u4NatStaticNaptLocalIp,
                                  INT4 i4NatStaticNaptStartLocalPort,
                                  INT4 i4NatStaticNaptEndLocalPort,
                                  INT4 i4NatStaticNaptProtocolNumber,
                                  INT4 i4SetValNatStaticNaptLeaseDuration)
{
    int                 rc;
    tNatwnmhSetNatStaticNaptLeaseDuration lv;

    lv.cmd = NMH_SET_NAT_STATIC_NAPT_LEASE_DURATION;
    lv.i4NatStaticNaptInterfaceNum = i4NatStaticNaptInterfaceNum;
    lv.u4NatStaticNaptLocalIp = u4NatStaticNaptLocalIp;
    lv.i4NatStaticNaptStartLocalPort = i4NatStaticNaptStartLocalPort;
    lv.i4NatStaticNaptEndLocalPort = i4NatStaticNaptEndLocalPort;
    lv.i4NatStaticNaptProtocolNumber = i4NatStaticNaptProtocolNumber;
    lv.i4SetValNatStaticNaptLeaseDuration = i4SetValNatStaticNaptLeaseDuration;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatStaticNaptEntryStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatStaticNaptEntryStatus (INT4 i4NatStaticNaptInterfaceNum,
                                UINT4 u4NatStaticNaptLocalIp,
                                INT4 i4NatStaticNaptStartLocalPort,
                                INT4 i4NatStaticNaptEndLocalPort,
                                INT4 i4NatStaticNaptProtocolNumber,
                                INT4 i4SetValNatStaticNaptEntryStatus)
{
    int                 rc;
    tNatwnmhSetNatStaticNaptEntryStatus lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_STATIC_NAPT_ENTRY_STATUS;
    lv.i4NatStaticNaptInterfaceNum = i4NatStaticNaptInterfaceNum;
    lv.u4NatStaticNaptLocalIp = u4NatStaticNaptLocalIp;
    lv.i4NatStaticNaptStartLocalPort = i4NatStaticNaptStartLocalPort;
    lv.i4NatStaticNaptEndLocalPort = i4NatStaticNaptEndLocalPort;
    lv.i4NatStaticNaptProtocolNumber = i4NatStaticNaptProtocolNumber;
    lv.i4SetValNatStaticNaptEntryStatus = i4SetValNatStaticNaptEntryStatus;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatStaticNaptEntryStatus;
        SnmpNotifyInfo.u4OidLen =
            sizeof (NatStaticNaptEntryStatus) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = TRUE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;
        SnmpNotifyInfo.u4Indices = 5;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i %i %i",
                          i4NatStaticNaptInterfaceNum,
                          u4NatStaticNaptLocalIp,
                          i4NatStaticNaptStartLocalPort,
                          i4NatStaticNaptEndLocalPort,
                          i4NatStaticNaptProtocolNumber,
                          i4SetValNatStaticNaptEntryStatus));
    }

#ifdef UPNP_WANTED
    UPnPEvntNotifyPortMapping (i4NatStaticNaptInterfaceNum);
#endif
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatStaticNaptTranslatedLocalIp
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatStaticNaptTranslatedLocalIp (UINT4 *pu4ErrorCode,
                                         INT4 i4NatStaticNaptInterfaceNum,
                                         UINT4 u4NatStaticNaptLocalIp,
                                         INT4 i4NatStaticNaptStartLocalPort,
                                         INT4 i4NatStaticNaptEndLocalPort,
                                         INT4 i4NatStaticNaptProtocolNumber,
                                         UINT4
                                         u4TestValNatStaticNaptTranslatedLocalIp)
{
    int                 rc;
    tNatwnmhTestv2NatStaticNaptTranslatedLocalIp lv;

    lv.cmd = NMH_TESTV2_NAT_STATIC_NAPT_TRANSLATED_LOCAL_IP;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4NatStaticNaptInterfaceNum = i4NatStaticNaptInterfaceNum;
    lv.u4NatStaticNaptLocalIp = u4NatStaticNaptLocalIp;
    lv.i4NatStaticNaptStartLocalPort = i4NatStaticNaptStartLocalPort;
    lv.i4NatStaticNaptEndLocalPort = i4NatStaticNaptEndLocalPort;
    lv.i4NatStaticNaptProtocolNumber = i4NatStaticNaptProtocolNumber;
    lv.u4TestValNatStaticNaptTranslatedLocalIp =
        u4TestValNatStaticNaptTranslatedLocalIp;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatStaticNaptTranslatedLocalPort
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatStaticNaptTranslatedLocalPort (UINT4 *pu4ErrorCode,
                                           INT4 i4NatStaticNaptInterfaceNum,
                                           UINT4 u4NatStaticNaptLocalIp,
                                           INT4 i4NatStaticNaptStartLocalPort,
                                           INT4 i4NatStaticNaptEndLocalPort,
                                           INT4 i4NatStaticNaptProtocolNumber,
                                           INT4
                                           i4TestValNatStaticNaptTranslatedLocalPort)
{
    int                 rc;
    tNatwnmhTestv2NatStaticNaptTranslatedLocalPort lv;

    lv.cmd = NMH_TESTV2_NAT_STATIC_NAPT_TRANSLATED_LOCAL_PORT;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4NatStaticNaptInterfaceNum = i4NatStaticNaptInterfaceNum;
    lv.u4NatStaticNaptLocalIp = u4NatStaticNaptLocalIp;
    lv.i4NatStaticNaptStartLocalPort = i4NatStaticNaptStartLocalPort;
    lv.i4NatStaticNaptEndLocalPort = i4NatStaticNaptEndLocalPort;
    lv.i4NatStaticNaptProtocolNumber = i4NatStaticNaptProtocolNumber;
    lv.i4TestValNatStaticNaptTranslatedLocalPort =
        i4TestValNatStaticNaptTranslatedLocalPort;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatStaticNaptDescription
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatStaticNaptDescription (UINT4 *pu4ErrorCode,
                                   INT4 i4NatStaticNaptInterfaceNum,
                                   UINT4 u4NatStaticNaptLocalIp,
                                   INT4 i4NatStaticNaptStartLocalPort,
                                   INT4 i4NatStaticNaptEndLocalPort,
                                   INT4 i4NatStaticNaptProtocolNumber,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValNatStaticNaptDescription)
{
    int                 rc;
    tNatwnmhTestv2NatStaticNaptDescription lv;

    lv.cmd = NMH_TESTV2_NAT_STATIC_NAPT_DESCRIPTION;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4NatStaticNaptInterfaceNum = i4NatStaticNaptInterfaceNum;
    lv.u4NatStaticNaptLocalIp = u4NatStaticNaptLocalIp;
    lv.i4NatStaticNaptStartLocalPort = i4NatStaticNaptStartLocalPort;
    lv.i4NatStaticNaptEndLocalPort = i4NatStaticNaptEndLocalPort;
    lv.i4NatStaticNaptProtocolNumber = i4NatStaticNaptProtocolNumber;
    lv.pTestValNatStaticNaptDescription = pTestValNatStaticNaptDescription;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatStaticNaptLeaseDuration
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatStaticNaptLeaseDuration (UINT4 *pu4ErrorCode,
                                     INT4 i4NatStaticNaptInterfaceNum,
                                     UINT4 u4NatStaticNaptLocalIp,
                                     INT4 i4NatStaticNaptStartLocalPort,
                                     INT4 i4NatStaticNaptEndLocalPort,
                                     INT4 i4NatStaticNaptProtocolNumber,
                                     INT4 i4TestValNatStaticNaptLeaseDuration)
{
    int                 rc;
    tNatwnmhTestv2NatStaticNaptLeaseDuration lv;

    lv.cmd = NMH_TESTV2_NAT_STATIC_NAPT_LEASE_DURATION;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4NatStaticNaptInterfaceNum = i4NatStaticNaptInterfaceNum;
    lv.u4NatStaticNaptLocalIp = u4NatStaticNaptLocalIp;
    lv.i4NatStaticNaptStartLocalPort = i4NatStaticNaptStartLocalPort;
    lv.i4NatStaticNaptEndLocalPort = i4NatStaticNaptEndLocalPort;
    lv.i4NatStaticNaptProtocolNumber = i4NatStaticNaptProtocolNumber;
    lv.i4TestValNatStaticNaptLeaseDuration =
        i4TestValNatStaticNaptLeaseDuration;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatStaticNaptEntryStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatStaticNaptEntryStatus (UINT4 *pu4ErrorCode,
                                   INT4 i4NatStaticNaptInterfaceNum,
                                   UINT4 u4NatStaticNaptLocalIp,
                                   INT4 i4NatStaticNaptStartLocalPort,
                                   INT4 i4NatStaticNaptEndLocalPort,
                                   INT4 i4NatStaticNaptProtocolNumber,
                                   INT4 i4TestValNatStaticNaptEntryStatus)
{
    int                 rc;
    tNatwnmhTestv2NatStaticNaptEntryStatus lv;

    lv.cmd = NMH_TESTV2_NAT_STATIC_NAPT_ENTRY_STATUS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4NatStaticNaptInterfaceNum = i4NatStaticNaptInterfaceNum;
    lv.u4NatStaticNaptLocalIp = u4NatStaticNaptLocalIp;
    lv.i4NatStaticNaptStartLocalPort = i4NatStaticNaptStartLocalPort;
    lv.i4NatStaticNaptEndLocalPort = i4NatStaticNaptEndLocalPort;
    lv.i4NatStaticNaptProtocolNumber = i4NatStaticNaptProtocolNumber;
    lv.i4TestValNatStaticNaptEntryStatus = i4TestValNatStaticNaptEntryStatus;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhValidateIndexInstanceNatIfTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhValidateIndexInstanceNatIfTable (INT4 i4NatIfInterfaceNumber)
{
    int                 rc;
    tNatwnmhValidateIndexInstanceNatIfTable lv;

    lv.cmd = NMH_VALIDATE_INDEX_INSTANCE_NAT_IF_TABLE;
    lv.i4NatIfInterfaceNumber = i4NatIfInterfaceNumber;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFirstIndexNatIfTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFirstIndexNatIfTable (INT4 *pi4NatIfInterfaceNumber)
{
    int                 rc;
    tNatwnmhGetFirstIndexNatIfTable lv;

    lv.cmd = NMH_GET_FIRST_INDEX_NAT_IF_TABLE;
    lv.pi4NatIfInterfaceNumber = pi4NatIfInterfaceNumber;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : SNMP_SUCCESS);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNextIndexNatIfTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNextIndexNatIfTable (INT4 i4NatIfInterfaceNumber,
                           INT4 *pi4NextNatIfInterfaceNumber)
{
    int                 rc;
    tNatwnmhGetNextIndexNatIfTable lv;

    lv.cmd = NMH_GET_NEXT_INDEX_NAT_IF_TABLE;
    lv.i4NatIfInterfaceNumber = i4NatIfInterfaceNumber;
    lv.pi4NextNatIfInterfaceNumber = pi4NextNatIfInterfaceNumber;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatIfNat
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatIfNat (INT4 i4NatIfInterfaceNumber, INT4 *pi4RetValNatIfNat)
{
    int                 rc;
    tNatwnmhGetNatIfNat lv;

    lv.cmd = NMH_GET_NAT_IF_NAT;
    lv.i4NatIfInterfaceNumber = i4NatIfInterfaceNumber;
    lv.pi4RetValNatIfNat = pi4RetValNatIfNat;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatIfNapt
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatIfNapt (INT4 i4NatIfInterfaceNumber, INT4 *pi4RetValNatIfNapt)
{
    int                 rc;
    tNatwnmhGetNatIfNapt lv;

    lv.cmd = NMH_GET_NAT_IF_NAPT;
    lv.i4NatIfInterfaceNumber = i4NatIfInterfaceNumber;
    lv.pi4RetValNatIfNapt = pi4RetValNatIfNapt;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatIfTwoWayNat
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatIfTwoWayNat (INT4 i4NatIfInterfaceNumber,
                      INT4 *pi4RetValNatIfTwoWayNat)
{
    int                 rc;
    tNatwnmhGetNatIfTwoWayNat lv;

    lv.cmd = NMH_GET_NAT_IF_TWO_WAY_NAT;
    lv.i4NatIfInterfaceNumber = i4NatIfInterfaceNumber;
    lv.pi4RetValNatIfTwoWayNat = pi4RetValNatIfTwoWayNat;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatIfEntryStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatIfEntryStatus (INT4 i4NatIfInterfaceNumber,
                        INT4 *pi4RetValNatIfEntryStatus)
{
    int                 rc;
    tNatwnmhGetNatIfEntryStatus lv;

    lv.cmd = NMH_GET_NAT_IF_ENTRY_STATUS;
    lv.i4NatIfInterfaceNumber = i4NatIfInterfaceNumber;
    lv.pi4RetValNatIfEntryStatus = pi4RetValNatIfEntryStatus;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatIfNat
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatIfNat (INT4 i4NatIfInterfaceNumber, INT4 i4SetValNatIfNat)
{
    int                 rc;
    tNatwnmhSetNatIfNat lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_IF_NAT;
    lv.i4NatIfInterfaceNumber = i4NatIfInterfaceNumber;
    lv.i4SetValNatIfNat = i4SetValNatIfNat;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatIfNat;
        SnmpNotifyInfo.u4OidLen = sizeof (NatIfNat) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          i4NatIfInterfaceNumber, i4SetValNatIfNat));
    }

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatIfNapt
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatIfNapt (INT4 i4NatIfInterfaceNumber, INT4 i4SetValNatIfNapt)
{
    int                 rc;
    tNatwnmhSetNatIfNapt lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_IF_NAPT;
    lv.i4NatIfInterfaceNumber = i4NatIfInterfaceNumber;
    lv.i4SetValNatIfNapt = i4SetValNatIfNapt;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatIfNapt;
        SnmpNotifyInfo.u4OidLen = sizeof (NatIfNapt) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          i4NatIfInterfaceNumber, i4SetValNatIfNapt));
    }

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatIfTwoWayNat
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatIfTwoWayNat (INT4 i4NatIfInterfaceNumber, INT4 i4SetValNatIfTwoWayNat)
{
    int                 rc;
    tNatwnmhSetNatIfTwoWayNat lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_IF_TWO_WAY_NAT;
    lv.i4NatIfInterfaceNumber = i4NatIfInterfaceNumber;
    lv.i4SetValNatIfTwoWayNat = i4SetValNatIfTwoWayNat;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatIfTwoWayNat;
        SnmpNotifyInfo.u4OidLen = sizeof (NatIfTwoWayNat) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          i4NatIfInterfaceNumber, i4SetValNatIfTwoWayNat));
    }

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatIfEntryStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatIfEntryStatus (INT4 i4NatIfInterfaceNumber,
                        INT4 i4SetValNatIfEntryStatus)
{
    int                 rc;
    tNatwnmhSetNatIfEntryStatus lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_IF_ENTRY_STATUS;
    lv.i4NatIfInterfaceNumber = i4NatIfInterfaceNumber;
    lv.i4SetValNatIfEntryStatus = i4SetValNatIfEntryStatus;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatIfEntryStatus;
        SnmpNotifyInfo.u4OidLen = sizeof (NatIfEntryStatus) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = TRUE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          i4NatIfInterfaceNumber, i4SetValNatIfEntryStatus));
    }

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatIfNat
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatIfNat (UINT4 *pu4ErrorCode,
                   INT4 i4NatIfInterfaceNumber, INT4 i4TestValNatIfNat)
{
    int                 rc;
    tNatwnmhTestv2NatIfNat lv;

    lv.cmd = NMH_TESTV2_NAT_IF_NAT;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4NatIfInterfaceNumber = i4NatIfInterfaceNumber;
    lv.i4TestValNatIfNat = i4TestValNatIfNat;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatIfNapt
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatIfNapt (UINT4 *pu4ErrorCode,
                    INT4 i4NatIfInterfaceNumber, INT4 i4TestValNatIfNapt)
{
    int                 rc;
    tNatwnmhTestv2NatIfNapt lv;

    lv.cmd = NMH_TESTV2_NAT_IF_NAPT;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4NatIfInterfaceNumber = i4NatIfInterfaceNumber;
    lv.i4TestValNatIfNapt = i4TestValNatIfNapt;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatIfTwoWayNat
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatIfTwoWayNat (UINT4 *pu4ErrorCode,
                         INT4 i4NatIfInterfaceNumber,
                         INT4 i4TestValNatIfTwoWayNat)
{
    int                 rc;
    tNatwnmhTestv2NatIfTwoWayNat lv;

    lv.cmd = NMH_TESTV2_NAT_IF_TWO_WAY_NAT;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4NatIfInterfaceNumber = i4NatIfInterfaceNumber;
    lv.i4TestValNatIfTwoWayNat = i4TestValNatIfTwoWayNat;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatIfEntryStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatIfEntryStatus (UINT4 *pu4ErrorCode,
                           INT4 i4NatIfInterfaceNumber,
                           INT4 i4TestValNatIfEntryStatus)
{
    int                 rc;
    tNatwnmhTestv2NatIfEntryStatus lv;

    lv.cmd = NMH_TESTV2_NAT_IF_ENTRY_STATUS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4NatIfInterfaceNumber = i4NatIfInterfaceNumber;
    lv.i4TestValNatIfEntryStatus = i4TestValNatIfEntryStatus;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhValidateIndexInstanceNatIPSecSessionTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhValidateIndexInstanceNatIPSecSessionTable (INT4
                                              i4NatIPSecSessionInterfaceNum,
                                              UINT4 u4NatIPSecSessionLocalIp,
                                              UINT4 u4NatIPSecSessionOutsideIp,
                                              INT4 i4NatIPSecSessionSPIInside,
                                              INT4 i4NatIPSecSessionSPIOutside)
{
    int                 rc;
    tNatwnmhValidateIndexInstanceNatIPSecSessionTable lv;

    lv.cmd = NMH_VALIDATE_INDEX_INSTANCE_NAT_I_P_SEC_SESSION_TABLE;
    lv.i4NatIPSecSessionInterfaceNum = i4NatIPSecSessionInterfaceNum;
    lv.u4NatIPSecSessionLocalIp = u4NatIPSecSessionLocalIp;
    lv.u4NatIPSecSessionOutsideIp = u4NatIPSecSessionOutsideIp;
    lv.i4NatIPSecSessionSPIInside = i4NatIPSecSessionSPIInside;
    lv.i4NatIPSecSessionSPIOutside = i4NatIPSecSessionSPIOutside;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFirstIndexNatIPSecSessionTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFirstIndexNatIPSecSessionTable (INT4 *pi4NatIPSecSessionInterfaceNum,
                                      UINT4 *pu4NatIPSecSessionLocalIp,
                                      UINT4 *pu4NatIPSecSessionOutsideIp,
                                      INT4 *pi4NatIPSecSessionSPIInside,
                                      INT4 *pi4NatIPSecSessionSPIOutside)
{
    int                 rc;
    tNatwnmhGetFirstIndexNatIPSecSessionTable lv;

    lv.cmd = NMH_GET_FIRST_INDEX_NAT_I_P_SEC_SESSION_TABLE;
    lv.pi4NatIPSecSessionInterfaceNum = pi4NatIPSecSessionInterfaceNum;
    lv.pu4NatIPSecSessionLocalIp = pu4NatIPSecSessionLocalIp;
    lv.pu4NatIPSecSessionOutsideIp = pu4NatIPSecSessionOutsideIp;
    lv.pi4NatIPSecSessionSPIInside = pi4NatIPSecSessionSPIInside;
    lv.pi4NatIPSecSessionSPIOutside = pi4NatIPSecSessionSPIOutside;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNextIndexNatIPSecSessionTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNextIndexNatIPSecSessionTable (INT4 i4NatIPSecSessionInterfaceNum,
                                     INT4 *pi4NextNatIPSecSessionInterfaceNum,
                                     UINT4 u4NatIPSecSessionLocalIp,
                                     UINT4 *pu4NextNatIPSecSessionLocalIp,
                                     UINT4 u4NatIPSecSessionOutsideIp,
                                     UINT4 *pu4NextNatIPSecSessionOutsideIp,
                                     INT4 i4NatIPSecSessionSPIInside,
                                     INT4 *pi4NextNatIPSecSessionSPIInside,
                                     INT4 i4NatIPSecSessionSPIOutside,
                                     INT4 *pi4NextNatIPSecSessionSPIOutside)
{
    int                 rc;
    tNatwnmhGetNextIndexNatIPSecSessionTable lv;

    lv.cmd = NMH_GET_NEXT_INDEX_NAT_I_P_SEC_SESSION_TABLE;
    lv.i4NatIPSecSessionInterfaceNum = i4NatIPSecSessionInterfaceNum;
    lv.pi4NextNatIPSecSessionInterfaceNum = pi4NextNatIPSecSessionInterfaceNum;
    lv.u4NatIPSecSessionLocalIp = u4NatIPSecSessionLocalIp;
    lv.pu4NextNatIPSecSessionLocalIp = pu4NextNatIPSecSessionLocalIp;
    lv.u4NatIPSecSessionOutsideIp = u4NatIPSecSessionOutsideIp;
    lv.pu4NextNatIPSecSessionOutsideIp = pu4NextNatIPSecSessionOutsideIp;
    lv.i4NatIPSecSessionSPIInside = i4NatIPSecSessionSPIInside;
    lv.pi4NextNatIPSecSessionSPIInside = pi4NextNatIPSecSessionSPIInside;
    lv.i4NatIPSecSessionSPIOutside = i4NatIPSecSessionSPIOutside;
    lv.pi4NextNatIPSecSessionSPIOutside = pi4NextNatIPSecSessionSPIOutside;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatIPSecSessionTranslatedLocalIp
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatIPSecSessionTranslatedLocalIp (INT4 i4NatIPSecSessionInterfaceNum,
                                        UINT4 u4NatIPSecSessionLocalIp,
                                        UINT4 u4NatIPSecSessionOutsideIp,
                                        INT4 i4NatIPSecSessionSPIInside,
                                        INT4 i4NatIPSecSessionSPIOutside,
                                        UINT4
                                        *pu4RetValNatIPSecSessionTranslatedLocalIp)
{
    int                 rc;
    tNatwnmhGetNatIPSecSessionTranslatedLocalIp lv;

    lv.cmd = NMH_GET_NAT_I_P_SEC_SESSION_TRANSLATED_LOCAL_IP;
    lv.i4NatIPSecSessionInterfaceNum = i4NatIPSecSessionInterfaceNum;
    lv.u4NatIPSecSessionLocalIp = u4NatIPSecSessionLocalIp;
    lv.u4NatIPSecSessionOutsideIp = u4NatIPSecSessionOutsideIp;
    lv.i4NatIPSecSessionSPIInside = i4NatIPSecSessionSPIInside;
    lv.i4NatIPSecSessionSPIOutside = i4NatIPSecSessionSPIOutside;
    lv.pu4RetValNatIPSecSessionTranslatedLocalIp =
        pu4RetValNatIPSecSessionTranslatedLocalIp;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatIPSecSessionLastUseTime
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatIPSecSessionLastUseTime (INT4 i4NatIPSecSessionInterfaceNum,
                                  UINT4 u4NatIPSecSessionLocalIp,
                                  UINT4 u4NatIPSecSessionOutsideIp,
                                  INT4 i4NatIPSecSessionSPIInside,
                                  INT4 i4NatIPSecSessionSPIOutside,
                                  INT4 *pi4RetValNatIPSecSessionLastUseTime)
{
    int                 rc;
    tNatwnmhGetNatIPSecSessionLastUseTime lv;

    lv.cmd = NMH_GET_NAT_I_P_SEC_SESSION_LAST_USE_TIME;
    lv.i4NatIPSecSessionInterfaceNum = i4NatIPSecSessionInterfaceNum;
    lv.u4NatIPSecSessionLocalIp = u4NatIPSecSessionLocalIp;
    lv.u4NatIPSecSessionOutsideIp = u4NatIPSecSessionOutsideIp;
    lv.i4NatIPSecSessionSPIInside = i4NatIPSecSessionSPIInside;
    lv.i4NatIPSecSessionSPIOutside = i4NatIPSecSessionSPIOutside;
    lv.pi4RetValNatIPSecSessionLastUseTime =
        pi4RetValNatIPSecSessionLastUseTime;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatIPSecSessionEntryStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatIPSecSessionEntryStatus (INT4 i4NatIPSecSessionInterfaceNum,
                                  UINT4 u4NatIPSecSessionLocalIp,
                                  UINT4 u4NatIPSecSessionOutsideIp,
                                  INT4 i4NatIPSecSessionSPIInside,
                                  INT4 i4NatIPSecSessionSPIOutside,
                                  INT4 *pi4RetValNatIPSecSessionEntryStatus)
{
    int                 rc;
    tNatwnmhGetNatIPSecSessionEntryStatus lv;

    lv.cmd = NMH_GET_NAT_I_P_SEC_SESSION_ENTRY_STATUS;
    lv.i4NatIPSecSessionInterfaceNum = i4NatIPSecSessionInterfaceNum;
    lv.u4NatIPSecSessionLocalIp = u4NatIPSecSessionLocalIp;
    lv.u4NatIPSecSessionOutsideIp = u4NatIPSecSessionOutsideIp;
    lv.i4NatIPSecSessionSPIInside = i4NatIPSecSessionSPIInside;
    lv.i4NatIPSecSessionSPIOutside = i4NatIPSecSessionSPIOutside;
    lv.pi4RetValNatIPSecSessionEntryStatus =
        pi4RetValNatIPSecSessionEntryStatus;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatIPSecSessionEntryStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatIPSecSessionEntryStatus (INT4 i4NatIPSecSessionInterfaceNum,
                                  UINT4 u4NatIPSecSessionLocalIp,
                                  UINT4 u4NatIPSecSessionOutsideIp,
                                  INT4 i4NatIPSecSessionSPIInside,
                                  INT4 i4NatIPSecSessionSPIOutside,
                                  INT4 i4SetValNatIPSecSessionEntryStatus)
{
    int                 rc;
    tNatwnmhSetNatIPSecSessionEntryStatus lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_I_P_SEC_SESSION_ENTRY_STATUS;
    lv.i4NatIPSecSessionInterfaceNum = i4NatIPSecSessionInterfaceNum;
    lv.u4NatIPSecSessionLocalIp = u4NatIPSecSessionLocalIp;
    lv.u4NatIPSecSessionOutsideIp = u4NatIPSecSessionOutsideIp;
    lv.i4NatIPSecSessionSPIInside = i4NatIPSecSessionSPIInside;
    lv.i4NatIPSecSessionSPIOutside = i4NatIPSecSessionSPIOutside;
    lv.i4SetValNatIPSecSessionEntryStatus = i4SetValNatIPSecSessionEntryStatus;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatIPSecSessionEntryStatus;
        SnmpNotifyInfo.u4OidLen =
            sizeof (NatIPSecSessionEntryStatus) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = TRUE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;
        SnmpNotifyInfo.u4Indices = 5;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i %i",
                          i4NatIPSecSessionInterfaceNum,
                          u4NatIPSecSessionLocalIp,
                          u4NatIPSecSessionOutsideIp,
                          i4NatIPSecSessionSPIInside,
                          i4NatIPSecSessionSPIOutside,
                          i4SetValNatIPSecSessionEntryStatus));
    }

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatIPSecSessionEntryStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatIPSecSessionEntryStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4NatIPSecSessionInterfaceNum,
                                     UINT4 u4NatIPSecSessionLocalIp,
                                     UINT4 u4NatIPSecSessionOutsideIp,
                                     INT4 i4NatIPSecSessionSPIInside,
                                     INT4 i4NatIPSecSessionSPIOutside,
                                     INT4 i4TestValNatIPSecSessionEntryStatus)
{
    int                 rc;
    tNatwnmhTestv2NatIPSecSessionEntryStatus lv;

    lv.cmd = NMH_TESTV2_NAT_I_P_SEC_SESSION_ENTRY_STATUS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4NatIPSecSessionInterfaceNum = i4NatIPSecSessionInterfaceNum;
    lv.u4NatIPSecSessionLocalIp = u4NatIPSecSessionLocalIp;
    lv.u4NatIPSecSessionOutsideIp = u4NatIPSecSessionOutsideIp;
    lv.i4NatIPSecSessionSPIInside = i4NatIPSecSessionSPIInside;
    lv.i4NatIPSecSessionSPIOutside = i4NatIPSecSessionSPIOutside;
    lv.i4TestValNatIPSecSessionEntryStatus =
        i4TestValNatIPSecSessionEntryStatus;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhValidateIndexInstanceNatIPSecPendingTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhValidateIndexInstanceNatIPSecPendingTable (INT4
                                              i4NatIPSecPendingInterfaceNum,
                                              UINT4 u4NatIPSecPendingLocalIp,
                                              UINT4 u4NatIPSecPendingOutsideIp,
                                              INT4 i4NatIPSecPendingSPIInside,
                                              INT4 i4NatIPSecPendingSPIOutside)
{
    int                 rc;
    tNatwnmhValidateIndexInstanceNatIPSecPendingTable lv;

    lv.cmd = NMH_VALIDATE_INDEX_INSTANCE_NAT_I_P_SEC_PENDING_TABLE;
    lv.i4NatIPSecPendingInterfaceNum = i4NatIPSecPendingInterfaceNum;
    lv.u4NatIPSecPendingLocalIp = u4NatIPSecPendingLocalIp;
    lv.u4NatIPSecPendingOutsideIp = u4NatIPSecPendingOutsideIp;
    lv.i4NatIPSecPendingSPIInside = i4NatIPSecPendingSPIInside;
    lv.i4NatIPSecPendingSPIOutside = i4NatIPSecPendingSPIOutside;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFirstIndexNatIPSecPendingTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFirstIndexNatIPSecPendingTable (INT4 *pi4NatIPSecPendingInterfaceNum,
                                      UINT4 *pu4NatIPSecPendingLocalIp,
                                      UINT4 *pu4NatIPSecPendingOutsideIp,
                                      INT4 *pi4NatIPSecPendingSPIInside,
                                      INT4 *pi4NatIPSecPendingSPIOutside)
{
    int                 rc;
    tNatwnmhGetFirstIndexNatIPSecPendingTable lv;

    lv.cmd = NMH_GET_FIRST_INDEX_NAT_I_P_SEC_PENDING_TABLE;
    lv.pi4NatIPSecPendingInterfaceNum = pi4NatIPSecPendingInterfaceNum;
    lv.pu4NatIPSecPendingLocalIp = pu4NatIPSecPendingLocalIp;
    lv.pu4NatIPSecPendingOutsideIp = pu4NatIPSecPendingOutsideIp;
    lv.pi4NatIPSecPendingSPIInside = pi4NatIPSecPendingSPIInside;
    lv.pi4NatIPSecPendingSPIOutside = pi4NatIPSecPendingSPIOutside;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNextIndexNatIPSecPendingTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNextIndexNatIPSecPendingTable (INT4 i4NatIPSecPendingInterfaceNum,
                                     INT4 *pi4NextNatIPSecPendingInterfaceNum,
                                     UINT4 u4NatIPSecPendingLocalIp,
                                     UINT4 *pu4NextNatIPSecPendingLocalIp,
                                     UINT4 u4NatIPSecPendingOutsideIp,
                                     UINT4 *pu4NextNatIPSecPendingOutsideIp,
                                     INT4 i4NatIPSecPendingSPIInside,
                                     INT4 *pi4NextNatIPSecPendingSPIInside,
                                     INT4 i4NatIPSecPendingSPIOutside,
                                     INT4 *pi4NextNatIPSecPendingSPIOutside)
{
    int                 rc;
    tNatwnmhGetNextIndexNatIPSecPendingTable lv;

    lv.cmd = NMH_GET_NEXT_INDEX_NAT_I_P_SEC_PENDING_TABLE;
    lv.i4NatIPSecPendingInterfaceNum = i4NatIPSecPendingInterfaceNum;
    lv.pi4NextNatIPSecPendingInterfaceNum = pi4NextNatIPSecPendingInterfaceNum;
    lv.u4NatIPSecPendingLocalIp = u4NatIPSecPendingLocalIp;
    lv.pu4NextNatIPSecPendingLocalIp = pu4NextNatIPSecPendingLocalIp;
    lv.u4NatIPSecPendingOutsideIp = u4NatIPSecPendingOutsideIp;
    lv.pu4NextNatIPSecPendingOutsideIp = pu4NextNatIPSecPendingOutsideIp;
    lv.i4NatIPSecPendingSPIInside = i4NatIPSecPendingSPIInside;
    lv.pi4NextNatIPSecPendingSPIInside = pi4NextNatIPSecPendingSPIInside;
    lv.i4NatIPSecPendingSPIOutside = i4NatIPSecPendingSPIOutside;
    lv.pi4NextNatIPSecPendingSPIOutside = pi4NextNatIPSecPendingSPIOutside;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatIPSecPendingTranslatedLocalIp
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatIPSecPendingTranslatedLocalIp (INT4 i4NatIPSecPendingInterfaceNum,
                                        UINT4 u4NatIPSecPendingLocalIp,
                                        UINT4 u4NatIPSecPendingOutsideIp,
                                        INT4 i4NatIPSecPendingSPIInside,
                                        INT4 i4NatIPSecPendingSPIOutside,
                                        UINT4
                                        *pu4RetValNatIPSecPendingTranslatedLocalIp)
{
    int                 rc;
    tNatwnmhGetNatIPSecPendingTranslatedLocalIp lv;

    lv.cmd = NMH_GET_NAT_I_P_SEC_PENDING_TRANSLATED_LOCAL_IP;
    lv.i4NatIPSecPendingInterfaceNum = i4NatIPSecPendingInterfaceNum;
    lv.u4NatIPSecPendingLocalIp = u4NatIPSecPendingLocalIp;
    lv.u4NatIPSecPendingOutsideIp = u4NatIPSecPendingOutsideIp;
    lv.i4NatIPSecPendingSPIInside = i4NatIPSecPendingSPIInside;
    lv.i4NatIPSecPendingSPIOutside = i4NatIPSecPendingSPIOutside;
    lv.pu4RetValNatIPSecPendingTranslatedLocalIp =
        pu4RetValNatIPSecPendingTranslatedLocalIp;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatIPSecPendingLastUseTime
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatIPSecPendingLastUseTime (INT4 i4NatIPSecPendingInterfaceNum,
                                  UINT4 u4NatIPSecPendingLocalIp,
                                  UINT4 u4NatIPSecPendingOutsideIp,
                                  INT4 i4NatIPSecPendingSPIInside,
                                  INT4 i4NatIPSecPendingSPIOutside,
                                  INT4 *pi4RetValNatIPSecPendingLastUseTime)
{
    int                 rc;
    tNatwnmhGetNatIPSecPendingLastUseTime lv;

    lv.cmd = NMH_GET_NAT_I_P_SEC_PENDING_LAST_USE_TIME;
    lv.i4NatIPSecPendingInterfaceNum = i4NatIPSecPendingInterfaceNum;
    lv.u4NatIPSecPendingLocalIp = u4NatIPSecPendingLocalIp;
    lv.u4NatIPSecPendingOutsideIp = u4NatIPSecPendingOutsideIp;
    lv.i4NatIPSecPendingSPIInside = i4NatIPSecPendingSPIInside;
    lv.i4NatIPSecPendingSPIOutside = i4NatIPSecPendingSPIOutside;
    lv.pi4RetValNatIPSecPendingLastUseTime =
        pi4RetValNatIPSecPendingLastUseTime;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatIPSecPendingNoOfRetry
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatIPSecPendingNoOfRetry (INT4 i4NatIPSecPendingInterfaceNum,
                                UINT4 u4NatIPSecPendingLocalIp,
                                UINT4 u4NatIPSecPendingOutsideIp,
                                INT4 i4NatIPSecPendingSPIInside,
                                INT4 i4NatIPSecPendingSPIOutside,
                                INT4 *pi4RetValNatIPSecPendingNoOfRetry)
{
    int                 rc;
    tNatwnmhGetNatIPSecPendingNoOfRetry lv;

    lv.cmd = NMH_GET_NAT_I_P_SEC_PENDING_NO_OF_RETRY;
    lv.i4NatIPSecPendingInterfaceNum = i4NatIPSecPendingInterfaceNum;
    lv.u4NatIPSecPendingLocalIp = u4NatIPSecPendingLocalIp;
    lv.u4NatIPSecPendingOutsideIp = u4NatIPSecPendingOutsideIp;
    lv.i4NatIPSecPendingSPIInside = i4NatIPSecPendingSPIInside;
    lv.i4NatIPSecPendingSPIOutside = i4NatIPSecPendingSPIOutside;
    lv.pi4RetValNatIPSecPendingNoOfRetry = pi4RetValNatIPSecPendingNoOfRetry;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatIPSecPendingEntryStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatIPSecPendingEntryStatus (INT4 i4NatIPSecPendingInterfaceNum,
                                  UINT4 u4NatIPSecPendingLocalIp,
                                  UINT4 u4NatIPSecPendingOutsideIp,
                                  INT4 i4NatIPSecPendingSPIInside,
                                  INT4 i4NatIPSecPendingSPIOutside,
                                  INT4 *pi4RetValNatIPSecPendingEntryStatus)
{
    int                 rc;
    tNatwnmhGetNatIPSecPendingEntryStatus lv;

    lv.cmd = NMH_GET_NAT_I_P_SEC_PENDING_ENTRY_STATUS;
    lv.i4NatIPSecPendingInterfaceNum = i4NatIPSecPendingInterfaceNum;
    lv.u4NatIPSecPendingLocalIp = u4NatIPSecPendingLocalIp;
    lv.u4NatIPSecPendingOutsideIp = u4NatIPSecPendingOutsideIp;
    lv.i4NatIPSecPendingSPIInside = i4NatIPSecPendingSPIInside;
    lv.i4NatIPSecPendingSPIOutside = i4NatIPSecPendingSPIOutside;
    lv.pi4RetValNatIPSecPendingEntryStatus =
        pi4RetValNatIPSecPendingEntryStatus;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatIPSecPendingEntryStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatIPSecPendingEntryStatus (INT4 i4NatIPSecPendingInterfaceNum,
                                  UINT4 u4NatIPSecPendingLocalIp,
                                  UINT4 u4NatIPSecPendingOutsideIp,
                                  INT4 i4NatIPSecPendingSPIInside,
                                  INT4 i4NatIPSecPendingSPIOutside,
                                  INT4 i4SetValNatIPSecPendingEntryStatus)
{
    int                 rc;
    tNatwnmhSetNatIPSecPendingEntryStatus lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_I_P_SEC_PENDING_ENTRY_STATUS;
    lv.i4NatIPSecPendingInterfaceNum = i4NatIPSecPendingInterfaceNum;
    lv.u4NatIPSecPendingLocalIp = u4NatIPSecPendingLocalIp;
    lv.u4NatIPSecPendingOutsideIp = u4NatIPSecPendingOutsideIp;
    lv.i4NatIPSecPendingSPIInside = i4NatIPSecPendingSPIInside;
    lv.i4NatIPSecPendingSPIOutside = i4NatIPSecPendingSPIOutside;
    lv.i4SetValNatIPSecPendingEntryStatus = i4SetValNatIPSecPendingEntryStatus;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatIPSecPendingEntryStatus;
        SnmpNotifyInfo.u4OidLen =
            sizeof (NatIPSecPendingEntryStatus) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = TRUE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;
        SnmpNotifyInfo.u4Indices = 5;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%p %p %i %i %i",
                          u4NatIPSecPendingLocalIp,
                          u4NatIPSecPendingOutsideIp,
                          i4NatIPSecPendingSPIInside,
                          i4NatIPSecPendingSPIOutside,
                          i4SetValNatIPSecPendingEntryStatus));
    }

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatIPSecPendingEntryStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatIPSecPendingEntryStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4NatIPSecPendingInterfaceNum,
                                     UINT4 u4NatIPSecPendingLocalIp,
                                     UINT4 u4NatIPSecPendingOutsideIp,
                                     INT4 i4NatIPSecPendingSPIInside,
                                     INT4 i4NatIPSecPendingSPIOutside,
                                     INT4 i4TestValNatIPSecPendingEntryStatus)
{
    int                 rc;
    tNatwnmhTestv2NatIPSecPendingEntryStatus lv;

    lv.cmd = NMH_TESTV2_NAT_I_P_SEC_PENDING_ENTRY_STATUS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4NatIPSecPendingInterfaceNum = i4NatIPSecPendingInterfaceNum;
    lv.u4NatIPSecPendingLocalIp = u4NatIPSecPendingLocalIp;
    lv.u4NatIPSecPendingOutsideIp = u4NatIPSecPendingOutsideIp;
    lv.i4NatIPSecPendingSPIInside = i4NatIPSecPendingSPIInside;
    lv.i4NatIPSecPendingSPIOutside = i4NatIPSecPendingSPIOutside;
    lv.i4TestValNatIPSecPendingEntryStatus =
        i4TestValNatIPSecPendingEntryStatus;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhValidateIndexInstanceNatIKESessionTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhValidateIndexInstanceNatIKESessionTable (INT4 i4NatIKESessionInterfaceNum,
                                            UINT4 u4NatIKESessionLocalIp,
                                            UINT4 u4NatIKESessionOutsideIp,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pNatIKESessionInitCookie)
{
    int                 rc;
    tNatwnmhValidateIndexInstanceNatIKESessionTable lv;

    lv.cmd = NMH_VALIDATE_INDEX_INSTANCE_NAT_I_K_E_SESSION_TABLE;
    lv.i4NatIKESessionInterfaceNum = i4NatIKESessionInterfaceNum;
    lv.u4NatIKESessionLocalIp = u4NatIKESessionLocalIp;
    lv.u4NatIKESessionOutsideIp = u4NatIKESessionOutsideIp;
    lv.pNatIKESessionInitCookie = pNatIKESessionInitCookie;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFirstIndexNatIKESessionTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFirstIndexNatIKESessionTable (INT4 *pi4NatIKESessionInterfaceNum,
                                    UINT4 *pu4NatIKESessionLocalIp,
                                    UINT4 *pu4NatIKESessionOutsideIp,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNatIKESessionInitCookie)
{
    int                 rc;
    tNatwnmhGetFirstIndexNatIKESessionTable lv;

    lv.cmd = NMH_GET_FIRST_INDEX_NAT_I_K_E_SESSION_TABLE;
    lv.pi4NatIKESessionInterfaceNum = pi4NatIKESessionInterfaceNum;
    lv.pu4NatIKESessionLocalIp = pu4NatIKESessionLocalIp;
    lv.pu4NatIKESessionOutsideIp = pu4NatIKESessionOutsideIp;
    lv.pNatIKESessionInitCookie = pNatIKESessionInitCookie;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNextIndexNatIKESessionTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNextIndexNatIKESessionTable (INT4 i4NatIKESessionInterfaceNum,
                                   INT4 *pi4NextNatIKESessionInterfaceNum,
                                   UINT4 u4NatIKESessionLocalIp,
                                   UINT4 *pu4NextNatIKESessionLocalIp,
                                   UINT4 u4NatIKESessionOutsideIp,
                                   UINT4 *pu4NextNatIKESessionOutsideIp,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNatIKESessionInitCookie,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextNatIKESessionInitCookie)
{
    int                 rc;
    tNatwnmhGetNextIndexNatIKESessionTable lv;

    lv.cmd = NMH_GET_NEXT_INDEX_NAT_I_K_E_SESSION_TABLE;
    lv.i4NatIKESessionInterfaceNum = i4NatIKESessionInterfaceNum;
    lv.pi4NextNatIKESessionInterfaceNum = pi4NextNatIKESessionInterfaceNum;
    lv.u4NatIKESessionLocalIp = u4NatIKESessionLocalIp;
    lv.pu4NextNatIKESessionLocalIp = pu4NextNatIKESessionLocalIp;
    lv.u4NatIKESessionOutsideIp = u4NatIKESessionOutsideIp;
    lv.pu4NextNatIKESessionOutsideIp = pu4NextNatIKESessionOutsideIp;
    lv.pNatIKESessionInitCookie = pNatIKESessionInitCookie;
    lv.pNextNatIKESessionInitCookie = pNextNatIKESessionInitCookie;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatIKESessionTranslatedLocalIp
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatIKESessionTranslatedLocalIp (INT4 i4NatIKESessionInterfaceNum,
                                      UINT4 u4NatIKESessionLocalIp,
                                      UINT4 u4NatIKESessionOutsideIp,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pNatIKESessionInitCookie,
                                      UINT4
                                      *pu4RetValNatIKESessionTranslatedLocalIp)
{
    int                 rc;
    tNatwnmhGetNatIKESessionTranslatedLocalIp lv;

    lv.cmd = NMH_GET_NAT_I_K_E_SESSION_TRANSLATED_LOCAL_IP;
    lv.i4NatIKESessionInterfaceNum = i4NatIKESessionInterfaceNum;
    lv.u4NatIKESessionLocalIp = u4NatIKESessionLocalIp;
    lv.u4NatIKESessionOutsideIp = u4NatIKESessionOutsideIp;
    lv.pNatIKESessionInitCookie = pNatIKESessionInitCookie;
    lv.pu4RetValNatIKESessionTranslatedLocalIp =
        pu4RetValNatIKESessionTranslatedLocalIp;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatIKESessionLastUseTime
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatIKESessionLastUseTime (INT4 i4NatIKESessionInterfaceNum,
                                UINT4 u4NatIKESessionLocalIp,
                                UINT4 u4NatIKESessionOutsideIp,
                                tSNMP_OCTET_STRING_TYPE *
                                pNatIKESessionInitCookie,
                                INT4 *pi4RetValNatIKESessionLastUseTime)
{
    int                 rc;
    tNatwnmhGetNatIKESessionLastUseTime lv;

    lv.cmd = NMH_GET_NAT_I_K_E_SESSION_LAST_USE_TIME;
    lv.i4NatIKESessionInterfaceNum = i4NatIKESessionInterfaceNum;
    lv.u4NatIKESessionLocalIp = u4NatIKESessionLocalIp;
    lv.u4NatIKESessionOutsideIp = u4NatIKESessionOutsideIp;
    lv.pNatIKESessionInitCookie = pNatIKESessionInitCookie;
    lv.pi4RetValNatIKESessionLastUseTime = pi4RetValNatIKESessionLastUseTime;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatIKESessionEntryStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatIKESessionEntryStatus (INT4 i4NatIKESessionInterfaceNum,
                                UINT4 u4NatIKESessionLocalIp,
                                UINT4 u4NatIKESessionOutsideIp,
                                tSNMP_OCTET_STRING_TYPE *
                                pNatIKESessionInitCookie,
                                INT4 *pi4RetValNatIKESessionEntryStatus)
{
    int                 rc;
    tNatwnmhGetNatIKESessionEntryStatus lv;

    lv.cmd = NMH_GET_NAT_I_K_E_SESSION_ENTRY_STATUS;
    lv.i4NatIKESessionInterfaceNum = i4NatIKESessionInterfaceNum;
    lv.u4NatIKESessionLocalIp = u4NatIKESessionLocalIp;
    lv.u4NatIKESessionOutsideIp = u4NatIKESessionOutsideIp;
    lv.pNatIKESessionInitCookie = pNatIKESessionInitCookie;
    lv.pi4RetValNatIKESessionEntryStatus = pi4RetValNatIKESessionEntryStatus;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatIKESessionEntryStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatIKESessionEntryStatus (INT4 i4NatIKESessionInterfaceNum,
                                UINT4 u4NatIKESessionLocalIp,
                                UINT4 u4NatIKESessionOutsideIp,
                                tSNMP_OCTET_STRING_TYPE *
                                pNatIKESessionInitCookie,
                                INT4 i4SetValNatIKESessionEntryStatus)
{
    int                 rc;
    tNatwnmhSetNatIKESessionEntryStatus lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_I_K_E_SESSION_ENTRY_STATUS;
    lv.i4NatIKESessionInterfaceNum = i4NatIKESessionInterfaceNum;
    lv.u4NatIKESessionLocalIp = u4NatIKESessionLocalIp;
    lv.u4NatIKESessionOutsideIp = u4NatIKESessionOutsideIp;
    lv.pNatIKESessionInitCookie = pNatIKESessionInitCookie;
    lv.i4SetValNatIKESessionEntryStatus = i4SetValNatIKESessionEntryStatus;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatIKESessionEntryStatus;
        SnmpNotifyInfo.u4OidLen =
            sizeof (NatIKESessionEntryStatus) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = TRUE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;
        SnmpNotifyInfo.u4Indices = 4;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %s %i",
                          i4NatIKESessionInterfaceNum,
                          u4NatIKESessionLocalIp,
                          u4NatIKESessionOutsideIp,
                          pNatIKESessionInitCookie,
                          i4SetValNatIKESessionEntryStatus));
    }

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatIKESessionEntryStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatIKESessionEntryStatus (UINT4 *pu4ErrorCode,
                                   INT4 i4NatIKESessionInterfaceNum,
                                   UINT4 u4NatIKESessionLocalIp,
                                   UINT4 u4NatIKESessionOutsideIp,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNatIKESessionInitCookie,
                                   INT4 i4TestValNatIKESessionEntryStatus)
{
    int                 rc;
    tNatwnmhTestv2NatIKESessionEntryStatus lv;

    lv.cmd = NMH_TESTV2_NAT_I_K_E_SESSION_ENTRY_STATUS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4NatIKESessionInterfaceNum = i4NatIKESessionInterfaceNum;
    lv.u4NatIKESessionLocalIp = u4NatIKESessionLocalIp;
    lv.u4NatIKESessionOutsideIp = u4NatIKESessionOutsideIp;
    lv.pNatIKESessionInitCookie = pNatIKESessionInitCookie;
    lv.i4TestValNatIKESessionEntryStatus = i4TestValNatIKESessionEntryStatus;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhValidateIndexInstanceNatPortTrigInfoTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhValidateIndexInstanceNatPortTrigInfoTable (tSNMP_OCTET_STRING_TYPE *
                                              pNatPortTrigInfoInBoundPortRange,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pNatPortTrigInfoOutBoundPortRange,
                                              INT4 i4NatPortTrigInfoProtocol)
{
    int                 rc;
    tNatwnmhValidateIndexInstanceNatPortTrigInfoTable lv;

    lv.cmd = NMH_VALIDATE_INDEX_INSTANCE_NAT_PORT_TRIG_INFO_TABLE;
    lv.pNatPortTrigInfoInBoundPortRange = pNatPortTrigInfoInBoundPortRange;
    lv.pNatPortTrigInfoOutBoundPortRange = pNatPortTrigInfoOutBoundPortRange;
    lv.i4NatPortTrigInfoProtocol = i4NatPortTrigInfoProtocol;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFirstIndexNatPortTrigInfoTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFirstIndexNatPortTrigInfoTable (tSNMP_OCTET_STRING_TYPE *
                                      pNatPortTrigInfoInBoundPortRange,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pNatPortTrigInfoOutBoundPortRange,
                                      INT4 *pi4NatPortTrigInfoProtocol)
{
    int                 rc;
    tNatwnmhGetFirstIndexNatPortTrigInfoTable lv;

    lv.cmd = NMH_GET_FIRST_INDEX_NAT_PORT_TRIG_INFO_TABLE;
    lv.pNatPortTrigInfoInBoundPortRange = pNatPortTrigInfoInBoundPortRange;
    lv.pNatPortTrigInfoOutBoundPortRange = pNatPortTrigInfoOutBoundPortRange;
    lv.pi4NatPortTrigInfoProtocol = pi4NatPortTrigInfoProtocol;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNextIndexNatPortTrigInfoTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNextIndexNatPortTrigInfoTable (tSNMP_OCTET_STRING_TYPE *
                                     pNatPortTrigInfoInBoundPortRange,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextNatPortTrigInfoInBoundPortRange,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNatPortTrigInfoOutBoundPortRange,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextNatPortTrigInfoOutBoundPortRange,
                                     INT4 i4NatPortTrigInfoProtocol,
                                     INT4 *pi4NextNatPortTrigInfoProtocol)
{
    int                 rc;
    tNatwnmhGetNextIndexNatPortTrigInfoTable lv;

    lv.cmd = NMH_GET_NEXT_INDEX_NAT_PORT_TRIG_INFO_TABLE;
    lv.pNatPortTrigInfoInBoundPortRange = pNatPortTrigInfoInBoundPortRange;
    lv.pNextNatPortTrigInfoInBoundPortRange =
        pNextNatPortTrigInfoInBoundPortRange;
    lv.pNatPortTrigInfoOutBoundPortRange = pNatPortTrigInfoOutBoundPortRange;
    lv.pNextNatPortTrigInfoOutBoundPortRange =
        pNextNatPortTrigInfoOutBoundPortRange;
    lv.i4NatPortTrigInfoProtocol = i4NatPortTrigInfoProtocol;
    lv.pi4NextNatPortTrigInfoProtocol = pi4NextNatPortTrigInfoProtocol;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatPortTrigInfoAppName
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatPortTrigInfoAppName (tSNMP_OCTET_STRING_TYPE *
                              pNatPortTrigInfoInBoundPortRange,
                              tSNMP_OCTET_STRING_TYPE *
                              pNatPortTrigInfoOutBoundPortRange,
                              INT4 i4NatPortTrigInfoProtocol,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValNatPortTrigInfoAppName)
{
    int                 rc;
    tNatwnmhGetNatPortTrigInfoAppName lv;

    lv.cmd = NMH_GET_NAT_PORT_TRIG_INFO_APP_NAME;
    lv.pNatPortTrigInfoInBoundPortRange = pNatPortTrigInfoInBoundPortRange;
    lv.pNatPortTrigInfoOutBoundPortRange = pNatPortTrigInfoOutBoundPortRange;
    lv.i4NatPortTrigInfoProtocol = i4NatPortTrigInfoProtocol;
    lv.pRetValNatPortTrigInfoAppName = pRetValNatPortTrigInfoAppName;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatPortTrigInfoEntryStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatPortTrigInfoEntryStatus (tSNMP_OCTET_STRING_TYPE *
                                  pNatPortTrigInfoInBoundPortRange,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pNatPortTrigInfoOutBoundPortRange,
                                  INT4 i4NatPortTrigInfoProtocol,
                                  INT4 *pi4RetValNatPortTrigInfoEntryStatus)
{
    int                 rc;
    tNatwnmhGetNatPortTrigInfoEntryStatus lv;

    lv.cmd = NMH_GET_NAT_PORT_TRIG_INFO_ENTRY_STATUS;
    lv.pNatPortTrigInfoInBoundPortRange = pNatPortTrigInfoInBoundPortRange;
    lv.pNatPortTrigInfoOutBoundPortRange = pNatPortTrigInfoOutBoundPortRange;
    lv.i4NatPortTrigInfoProtocol = i4NatPortTrigInfoProtocol;
    lv.pi4RetValNatPortTrigInfoEntryStatus =
        pi4RetValNatPortTrigInfoEntryStatus;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatPortTrigInfoAppName
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatPortTrigInfoAppName (tSNMP_OCTET_STRING_TYPE *
                              pNatPortTrigInfoInBoundPortRange,
                              tSNMP_OCTET_STRING_TYPE *
                              pNatPortTrigInfoOutBoundPortRange,
                              INT4 i4NatPortTrigInfoProtocol,
                              tSNMP_OCTET_STRING_TYPE *
                              pSetValNatPortTrigInfoAppName)
{
    int                 rc;
    tNatwnmhSetNatPortTrigInfoAppName lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_PORT_TRIG_INFO_APP_NAME;
    lv.pNatPortTrigInfoInBoundPortRange = pNatPortTrigInfoInBoundPortRange;
    lv.pNatPortTrigInfoOutBoundPortRange = pNatPortTrigInfoOutBoundPortRange;
    lv.i4NatPortTrigInfoProtocol = i4NatPortTrigInfoProtocol;
    lv.pSetValNatPortTrigInfoAppName = pSetValNatPortTrigInfoAppName;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatPortTrigInfoAppName;
        SnmpNotifyInfo.u4OidLen =
            sizeof (NatPortTrigInfoAppName) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = TRUE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;
        SnmpNotifyInfo.u4Indices = 3;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %s %i %s",
                          pNatPortTrigInfoInBoundPortRange,
                          pNatPortTrigInfoOutBoundPortRange,
                          i4NatPortTrigInfoProtocol,
                          pSetValNatPortTrigInfoAppName));
    }

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatPortTrigInfoEntryStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatPortTrigInfoEntryStatus (tSNMP_OCTET_STRING_TYPE *
                                  pNatPortTrigInfoInBoundPortRange,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pNatPortTrigInfoOutBoundPortRange,
                                  INT4 i4NatPortTrigInfoProtocol,
                                  INT4 i4SetValNatPortTrigInfoEntryStatus)
{
    int                 rc;
    tNatwnmhSetNatPortTrigInfoEntryStatus lv;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    RM_GET_SEQ_NUM (&u4SeqNum);

    lv.cmd = NMH_SET_NAT_PORT_TRIG_INFO_ENTRY_STATUS;
    lv.pNatPortTrigInfoInBoundPortRange = pNatPortTrigInfoInBoundPortRange;
    lv.pNatPortTrigInfoOutBoundPortRange = pNatPortTrigInfoOutBoundPortRange;
    lv.i4NatPortTrigInfoProtocol = i4NatPortTrigInfoProtocol;
    lv.i4SetValNatPortTrigInfoEntryStatus = i4SetValNatPortTrigInfoEntryStatus;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    if ((rc >= NAT_ZERO) && (lv.rval == SNMP_SUCCESS))
    {
        SnmpNotifyInfo.pu4ObjectId = NatPortTrigInfoEntryStatus;
        SnmpNotifyInfo.u4OidLen =
            sizeof (NatPortTrigInfoEntryStatus) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = TRUE;
        SnmpNotifyInfo.pLockPointer = NatLock;
        SnmpNotifyInfo.pUnLockPointer = NatUnLock;
        SnmpNotifyInfo.u4Indices = 3;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %s %i %i",
                          pNatPortTrigInfoInBoundPortRange,
                          pNatPortTrigInfoOutBoundPortRange,
                          i4NatPortTrigInfoProtocol,
                          i4SetValNatPortTrigInfoEntryStatus));
    }

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatPortTrigInfoAppName
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatPortTrigInfoAppName (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pNatPortTrigInfoInBoundPortRange,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pNatPortTrigInfoOutBoundPortRange,
                                 INT4 i4NatPortTrigInfoProtocol,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValNatPortTrigInfoAppName)
{
    int                 rc;
    tNatwnmhTestv2NatPortTrigInfoAppName lv;

    lv.cmd = NMH_TESTV2_NAT_PORT_TRIG_INFO_APP_NAME;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pNatPortTrigInfoInBoundPortRange = pNatPortTrigInfoInBoundPortRange;
    lv.pNatPortTrigInfoOutBoundPortRange = pNatPortTrigInfoOutBoundPortRange;
    lv.i4NatPortTrigInfoProtocol = i4NatPortTrigInfoProtocol;
    lv.pTestValNatPortTrigInfoAppName = pTestValNatPortTrigInfoAppName;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatPortTrigInfoEntryStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatPortTrigInfoEntryStatus (UINT4 *pu4ErrorCode,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNatPortTrigInfoInBoundPortRange,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNatPortTrigInfoOutBoundPortRange,
                                     INT4 i4NatPortTrigInfoProtocol,
                                     INT4 i4TestValNatPortTrigInfoEntryStatus)
{
    int                 rc;
    tNatwnmhTestv2NatPortTrigInfoEntryStatus lv;

    lv.cmd = NMH_TESTV2_NAT_PORT_TRIG_INFO_ENTRY_STATUS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pNatPortTrigInfoInBoundPortRange = pNatPortTrigInfoInBoundPortRange;
    lv.pNatPortTrigInfoOutBoundPortRange = pNatPortTrigInfoOutBoundPortRange;
    lv.i4NatPortTrigInfoProtocol = i4NatPortTrigInfoProtocol;
    lv.i4TestValNatPortTrigInfoEntryStatus =
        i4TestValNatPortTrigInfoEntryStatus;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: NatSearchStaticTable
 *
 * -------------------------------------------------------------
 */
UINT4
NatSearchStaticTable (UINT4 u4IpAddr, UINT4 u4Direction, UINT4 u4IfNum)
{
    int                 rc;
    tNatwNatSearchStaticTable lv;

    lv.cmd = NAT_SEARCH_STATIC_TABLE;
    lv.u4IpAddr = u4IpAddr;
    lv.u4Direction = u4Direction;
    lv.u4IfNum = u4IfNum;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (UINT4) 0 : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: NatSearchGlobalIpAddrTable
 *
 * -------------------------------------------------------------
 */
UINT4
NatSearchGlobalIpAddrTable (UINT4 u4GlobalIpAddr, UINT4 u4IfNum)
{
    int                 rc;
    tNatwNatSearchGlobalIpAddrTable lv;

    lv.cmd = NAT_SEARCH_GLOBAL_IP_ADDR_TABLE;
    lv.u4GlobalIpAddr = u4GlobalIpAddr;
    lv.u4IfNum = u4IfNum;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (UINT4) NAT_FAILURE : lv.rval);
}

    /* -------------------------------------------------------------
     *
     * Function: NatHandleInterfaceIndication
     *
     * -------------------------------------------------------------
     */
INT1
NatHandleInterfaceIndication (UINT4 u4Interface,
                              UINT4 u4IpAddress, UINT4 u4Status)
{
    int                 rc;
    tNatwNatHandleInterfaceIndication lv;

    lv.cmd = NAT_HANDLE_INTERFACE_INDICATION;
    lv.u4Interface = u4Interface;
    lv.u4IpAddress = u4IpAddress;
    lv.u4Status = u4Status;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) NAT_FAILURE : lv.rval);
}

INT4
NatSetDebugLevel (UINT4 u4TraceLevel)
{
    int                 rc;
    tNatwNatCliSetDebugLevel lv;

    lv.cmd = NAT_CLI_SET_DEBUG_LEVEL;
    lv.u4TraceLevel = u4TraceLevel;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) NAT_FAILURE : lv.rval);
}

INT4
NatResetDebugLevel (UINT4 u4TraceLevel)
{
    int                 rc;
    tNatwNatCliResetDebugLevel lv;

    lv.cmd = NAT_CLI_RESET_DEBUG_LEVEL;
    lv.u4TraceLevel = u4TraceLevel;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) NAT_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: NatApiHandleCleanAllBinds
 *
 * -------------------------------------------------------------
 */
UINT4
NatApiHandleCleanAllBinds (VOID)
{
    int                 rc;
    tNatwNatApiHandleCleanAllBinds lv;

    lv.cmd = NAT_API_HANDLE_CLEAN_ALL_BINDS;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < NAT_ZERO ? (UINT4) NAT_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: sipAlgNatNotificationCbkFunc
 *
 * -------------------------------------------------------------
 */
void
sipAlgNatNotificationCbkFunc (tEventNotificationInfo * pEventNotificationInfo)
{
    int                 rc;
    tNatwsipAlgNatNotificationCbkFunc lv;

    lv.cmd = SIP_ALG_NAT_NOTIFICATION_CBK_FUNC;
    lv.pEventNotificationInfo = pEventNotificationInfo;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    if (rc < NAT_ZERO)
    {
        perror ("-E- sipAlgNatNotificationCbkFunc ");
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: sipAlgNatDisableNotification
 *
 * -------------------------------------------------------------
 */
void
sipAlgNatDisableNotification (VOID)
{
    int                 rc;
    tNatwsipAlgNatDisableNotification lv;

    lv.cmd = SIP_ALG_NAT_DISABLE_NOTIFICATION;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    if (rc < NAT_ZERO)
    {
        perror ("-E- sipAlgNatDisableNotification ");
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: sipAlgSipPortChangeNotification
 *
 * -------------------------------------------------------------
 */
void
sipAlgSipPortChangeNotification (UINT2 dPort)
{
    int                 rc;
    tNatwsipAlgSipPortChangeNotification lv;

    lv.cmd = SIP_ALG_SIP_PORT_CHANGE_NOTIFICATION;
    lv.dPort = dPort;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    if (rc < NAT_ZERO)
    {
        perror ("-E- sipAlgSipPortChangeNotification ");
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2NatEnable
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2NatEnable (UINT4 *pu4ErrorCode,
                   tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tNatwnmhDepv2NatEnable lv;

    lv.cmd = NMH_DEPV2_NAT_ENABLE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2NatTypicalNumberOfEntries
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2NatTypicalNumberOfEntries (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tNatwnmhDepv2NatTypicalNumberOfEntries lv;

    lv.cmd = NMH_DEPV2_NAT_TYPICAL_NUMBER_OF_ENTRIES;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2NatTranslatedLocalPortStart
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2NatTranslatedLocalPortStart (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tNatwnmhDepv2NatTranslatedLocalPortStart lv;

    lv.cmd = NMH_DEPV2_NAT_TRANSLATED_LOCAL_PORT_START;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2NatIdleTimeOut
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2NatIdleTimeOut (UINT4 *pu4ErrorCode,
                        tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tNatwnmhDepv2NatIdleTimeOut lv;

    lv.cmd = NMH_DEPV2_NAT_IDLE_TIME_OUT;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2NatTcpTimeOut
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2NatTcpTimeOut (UINT4 *pu4ErrorCode,
                       tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tNatwnmhDepv2NatTcpTimeOut lv;

    lv.cmd = NMH_DEPV2_NAT_TCP_TIME_OUT;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2NatUdpTimeOut
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2NatUdpTimeOut (UINT4 *pu4ErrorCode,
                       tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tNatwnmhDepv2NatUdpTimeOut lv;

    lv.cmd = NMH_DEPV2_NAT_UDP_TIME_OUT;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2NatTrcFlag
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2NatTrcFlag (UINT4 *pu4ErrorCode,
                    tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tNatwnmhDepv2NatTrcFlag lv;

    lv.cmd = NMH_DEPV2_NAT_TRC_FLAG;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2NatIKEPortTranslation
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2NatIKEPortTranslation (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tNatwnmhDepv2NatIKEPortTranslation lv;

    lv.cmd = NMH_DEPV2_NAT_I_K_E_PORT_TRANSLATION;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2NatIKETimeout
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2NatIKETimeout (UINT4 *pu4ErrorCode,
                       tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tNatwnmhDepv2NatIKETimeout lv;

    lv.cmd = NMH_DEPV2_NAT_I_K_E_TIMEOUT;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2NatIPSecTimeout
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2NatIPSecTimeout (UINT4 *pu4ErrorCode,
                         tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tNatwnmhDepv2NatIPSecTimeout lv;

    lv.cmd = NMH_DEPV2_NAT_I_P_SEC_TIMEOUT;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2NatIPSecPendingTimeout
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2NatIPSecPendingTimeout (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tNatwnmhDepv2NatIPSecPendingTimeout lv;

    lv.cmd = NMH_DEPV2_NAT_I_P_SEC_PENDING_TIMEOUT;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2NatIPSecMaxRetry
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2NatIPSecMaxRetry (UINT4 *pu4ErrorCode,
                          tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tNatwnmhDepv2NatIPSecMaxRetry lv;

    lv.cmd = NMH_DEPV2_NAT_I_P_SEC_MAX_RETRY;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2SipAlgPort
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2SipAlgPort (UINT4 *pu4ErrorCode,
                    tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tNatwnmhDepv2SipAlgPort lv;

    lv.cmd = NMH_DEPV2_SIP_ALG_PORT;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2NatSipAlgPartialEntryTimeOut
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2NatSipAlgPartialEntryTimeOut (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tNatwnmhDepv2NatSipAlgPartialEntryTimeOut lv;

    lv.cmd = NMH_DEPV2_NAT_SIP_ALG_PARTIAL_ENTRY_TIME_OUT;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2NatGlobalAddressTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2NatGlobalAddressTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tNatwnmhDepv2NatGlobalAddressTable lv;

    lv.cmd = NMH_DEPV2_NAT_GLOBAL_ADDRESS_TABLE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2NatLocalAddressTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2NatLocalAddressTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tNatwnmhDepv2NatLocalAddressTable lv;

    lv.cmd = NMH_DEPV2_NAT_LOCAL_ADDRESS_TABLE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2NatStaticTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2NatStaticTable (UINT4 *pu4ErrorCode,
                        tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tNatwnmhDepv2NatStaticTable lv;

    lv.cmd = NMH_DEPV2_NAT_STATIC_TABLE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2NatStaticNaptTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2NatStaticNaptTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tNatwnmhDepv2NatStaticNaptTable lv;

    lv.cmd = NMH_DEPV2_NAT_STATIC_NAPT_TABLE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2NatIfTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2NatIfTable (UINT4 *pu4ErrorCode,
                    tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tNatwnmhDepv2NatIfTable lv;

    lv.cmd = NMH_DEPV2_NAT_IF_TABLE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2NatIPSecSessionTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2NatIPSecSessionTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tNatwnmhDepv2NatIPSecSessionTable lv;

    lv.cmd = NMH_DEPV2_NAT_I_P_SEC_SESSION_TABLE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2NatIPSecPendingTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2NatIPSecPendingTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tNatwnmhDepv2NatIPSecPendingTable lv;

    lv.cmd = NMH_DEPV2_NAT_I_P_SEC_PENDING_TABLE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2NatIKESessionTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2NatIKESessionTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tNatwnmhDepv2NatIKESessionTable lv;

    lv.cmd = NMH_DEPV2_NAT_I_K_E_SESSION_TABLE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2NatPortTrigInfoTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2NatPortTrigInfoTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tNatwnmhDepv2NatPortTrigInfoTable lv;

    lv.cmd = NMH_DEPV2_NAT_PORT_TRIG_INFO_TABLE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhValidateIndexInstanceNatPolicyTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhValidateIndexInstanceNatPolicyTable (INT4 i4NatPolicyType,
                                        INT4 i4NatPolicyId,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pNatPolicyAclName)
{
    int                 rc;
    tNatwnmhValidateIndexInstanceNatPolicyTable lv;

    lv.cmd = NMH_VALIDATE_INDEX_INSTANCE_NAT_POLICY_TABLE;
    lv.i4NatPolicyType = i4NatPolicyType;
    lv.i4NatPolicyId = i4NatPolicyId;
    lv.pNatPolicyAclName = pNatPolicyAclName;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFirstIndexNatPolicyTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFirstIndexNatPolicyTable (INT4 *pi4NatPolicyType,
                                INT4 *pi4NatPolicyId,
                                tSNMP_OCTET_STRING_TYPE * pNatPolicyAclName)
{
    int                 rc;
    tNatwnmhGetFirstIndexNatPolicyTable lv;

    lv.cmd = NMH_GET_FIRST_INDEX_NAT_POLICY_TABLE;
    lv.pi4NatPolicyType = pi4NatPolicyType;
    lv.pi4NatPolicyId = pi4NatPolicyId;
    lv.pNatPolicyAclName = pNatPolicyAclName;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNextIndexNatPolicyTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNextIndexNatPolicyTable (INT4 i4NatPolicyType,
                               INT4 *pi4NextNatPolicyType,
                               INT4 i4NatPolicyId,
                               INT4 *pi4NextNatPolicyId,
                               tSNMP_OCTET_STRING_TYPE * pNatPolicyAclName,
                               tSNMP_OCTET_STRING_TYPE * pNextNatPolicyAclName)
{
    int                 rc;
    tNatwnmhGetNextIndexNatPolicyTable lv;

    lv.cmd = NMH_GET_NEXT_INDEX_NAT_POLICY_TABLE;
    lv.i4NatPolicyType = i4NatPolicyType;
    lv.pi4NextNatPolicyType = pi4NextNatPolicyType;
    lv.i4NatPolicyId = i4NatPolicyId;
    lv.pi4NextNatPolicyId = pi4NextNatPolicyId;
    lv.pNatPolicyAclName = pNatPolicyAclName;
    lv.pNextNatPolicyAclName = pNextNatPolicyAclName;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatPolicyTranslatedIp
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatPolicyTranslatedIp (INT4 i4NatPolicyType,
                             INT4 i4NatPolicyId,
                             tSNMP_OCTET_STRING_TYPE * pNatPolicyAclName,
                             UINT4 *pu4RetValNatPolicyTranslatedIp)
{
    int                 rc;
    tNatwnmhGetNatPolicyTranslatedIp lv;

    lv.cmd = NMH_GET_NAT_POLICY_TRANSLATED_IP;
    lv.i4NatPolicyType = i4NatPolicyType;
    lv.i4NatPolicyId = i4NatPolicyId;
    lv.pNatPolicyAclName = pNatPolicyAclName;
    lv.pu4RetValNatPolicyTranslatedIp = pu4RetValNatPolicyTranslatedIp;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatPolicyEntryStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatPolicyEntryStatus (INT4 i4NatPolicyType,
                            INT4 i4NatPolicyId,
                            tSNMP_OCTET_STRING_TYPE * pNatPolicyAclName,
                            INT4 *pi4RetValNatPolicyEntryStatus)
{
    int                 rc;
    tNatwnmhGetNatPolicyEntryStatus lv;

    lv.cmd = NMH_GET_NAT_POLICY_ENTRY_STATUS;
    lv.i4NatPolicyType = i4NatPolicyType;
    lv.i4NatPolicyId = i4NatPolicyId;
    lv.pNatPolicyAclName = pNatPolicyAclName;
    lv.pi4RetValNatPolicyEntryStatus = pi4RetValNatPolicyEntryStatus;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatPolicyTranslatedIp
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatPolicyTranslatedIp (INT4 i4NatPolicyType,
                             INT4 i4NatPolicyId,
                             tSNMP_OCTET_STRING_TYPE * pNatPolicyAclName,
                             UINT4 u4SetValNatPolicyTranslatedIp)
{
    int                 rc;
    tNatwnmhSetNatPolicyTranslatedIp lv;

    lv.cmd = NMH_SET_NAT_POLICY_TRANSLATED_IP;
    lv.i4NatPolicyType = i4NatPolicyType;
    lv.i4NatPolicyId = i4NatPolicyId;
    lv.pNatPolicyAclName = pNatPolicyAclName;
    lv.u4SetValNatPolicyTranslatedIp = u4SetValNatPolicyTranslatedIp;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetNatPolicyEntryStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetNatPolicyEntryStatus (INT4 i4NatPolicyType,
                            INT4 i4NatPolicyId,
                            tSNMP_OCTET_STRING_TYPE * pNatPolicyAclName,
                            INT4 i4SetValNatPolicyEntryStatus)
{
    int                 rc;
    tNatwnmhSetNatPolicyEntryStatus lv;

    lv.cmd = NMH_SET_NAT_POLICY_ENTRY_STATUS;
    lv.i4NatPolicyType = i4NatPolicyType;
    lv.i4NatPolicyId = i4NatPolicyId;
    lv.pNatPolicyAclName = pNatPolicyAclName;
    lv.i4SetValNatPolicyEntryStatus = i4SetValNatPolicyEntryStatus;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatPolicyTranslatedIp
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatPolicyTranslatedIp (UINT4 *pu4ErrorCode,
                                INT4 i4NatPolicyType,
                                INT4 i4NatPolicyId,
                                tSNMP_OCTET_STRING_TYPE * pNatPolicyAclName,
                                UINT4 u4TestValNatPolicyTranslatedIp)
{
    int                 rc;
    tNatwnmhTestv2NatPolicyTranslatedIp lv;

    lv.cmd = NMH_TESTV2_NAT_POLICY_TRANSLATED_IP;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4NatPolicyType = i4NatPolicyType;
    lv.i4NatPolicyId = i4NatPolicyId;
    lv.pNatPolicyAclName = pNatPolicyAclName;
    lv.u4TestValNatPolicyTranslatedIp = u4TestValNatPolicyTranslatedIp;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2NatPolicyEntryStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2NatPolicyEntryStatus (UINT4 *pu4ErrorCode,
                               INT4 i4NatPolicyType,
                               INT4 i4NatPolicyId,
                               tSNMP_OCTET_STRING_TYPE * pNatPolicyAclName,
                               INT4 i4TestValNatPolicyEntryStatus)
{
    int                 rc;
    tNatwnmhTestv2NatPolicyEntryStatus lv;

    lv.cmd = NMH_TESTV2_NAT_POLICY_ENTRY_STATUS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4NatPolicyType = i4NatPolicyType;
    lv.i4NatPolicyId = i4NatPolicyId;
    lv.pNatPolicyAclName = pNatPolicyAclName;
    lv.i4TestValNatPolicyEntryStatus = i4TestValNatPolicyEntryStatus;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2NatPolicyTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2NatPolicyTable (UINT4 *pu4ErrorCode,
                        tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tNatwnmhDepv2NatPolicyTable lv;

    lv.cmd = NMH_DEPV2_NAT_POLICY_TABLE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhValidateIndexInstanceNatRsvdPortTrigInfoTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhValidateIndexInstanceNatRsvdPortTrigInfoTable (INT4
                                                  i4NatRsvdPortTrigInfoAppIndex)
{
    int                 rc;
    tNatwnmhValidateIndexInstanceNatRsvdPortTrigInfoTable lv;

    lv.cmd = NMH_VALIDATE_INDEX_INSTANCE_NAT_RSVD_PORT_TRIG_INFO_TABLE;
    lv.i4NatRsvdPortTrigInfoAppIndex = i4NatRsvdPortTrigInfoAppIndex;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFirstIndexNatRsvdPortTrigInfoTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFirstIndexNatRsvdPortTrigInfoTable (INT4 *pi4NatRsvdPortTrigInfoAppIndex)
{
    int                 rc;
    tNatwnmhGetFirstIndexNatRsvdPortTrigInfoTable lv;

    lv.cmd = NMH_GET_FIRST_INDEX_NAT_RSVD_PORT_TRIG_INFO_TABLE;
    lv.pi4NatRsvdPortTrigInfoAppIndex = pi4NatRsvdPortTrigInfoAppIndex;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNextIndexNatRsvdPortTrigInfoTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNextIndexNatRsvdPortTrigInfoTable (INT4 i4NatRsvdPortTrigInfoAppIndex,
                                         INT4
                                         *pi4NextNatRsvdPortTrigInfoAppIndex)
{
    int                 rc;
    tNatwnmhGetNextIndexNatRsvdPortTrigInfoTable lv;

    lv.cmd = NMH_GET_NEXT_INDEX_NAT_RSVD_PORT_TRIG_INFO_TABLE;
    lv.i4NatRsvdPortTrigInfoAppIndex = i4NatRsvdPortTrigInfoAppIndex;
    lv.pi4NextNatRsvdPortTrigInfoAppIndex = pi4NextNatRsvdPortTrigInfoAppIndex;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatRsvdPortTrigInfoLocalIp
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatRsvdPortTrigInfoLocalIp (INT4 i4NatRsvdPortTrigInfoAppIndex,
                                  UINT4 *pu4RetValNatRsvdPortTrigInfoLocalIp)
{
    int                 rc;
    tNatwnmhGetNatRsvdPortTrigInfoLocalIp lv;

    lv.cmd = NMH_GET_NAT_RSVD_PORT_TRIG_INFO_LOCAL_IP;
    lv.i4NatRsvdPortTrigInfoAppIndex = i4NatRsvdPortTrigInfoAppIndex;
    lv.pu4RetValNatRsvdPortTrigInfoLocalIp =
        pu4RetValNatRsvdPortTrigInfoLocalIp;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatRsvdPortTrigInfoRemoteIp
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatRsvdPortTrigInfoRemoteIp (INT4 i4NatRsvdPortTrigInfoAppIndex,
                                   UINT4 *pu4RetValNatRsvdPortTrigInfoRemoteIp)
{
    int                 rc;
    tNatwnmhGetNatRsvdPortTrigInfoRemoteIp lv;

    lv.cmd = NMH_GET_NAT_RSVD_PORT_TRIG_INFO_REMOTE_IP;
    lv.i4NatRsvdPortTrigInfoAppIndex = i4NatRsvdPortTrigInfoAppIndex;
    lv.pu4RetValNatRsvdPortTrigInfoRemoteIp =
        pu4RetValNatRsvdPortTrigInfoRemoteIp;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatRsvdPortTrigInfoStartTime
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatRsvdPortTrigInfoStartTime (INT4 i4NatRsvdPortTrigInfoAppIndex,
                                    UINT4
                                    *pu4RetValNatRsvdPortTrigInfoStartTime)
{
    int                 rc;
    tNatwnmhGetNatRsvdPortTrigInfoStartTime lv;

    lv.cmd = NMH_GET_NAT_RSVD_PORT_TRIG_INFO_START_TIME;
    lv.i4NatRsvdPortTrigInfoAppIndex = i4NatRsvdPortTrigInfoAppIndex;
    lv.pu4RetValNatRsvdPortTrigInfoStartTime =
        pu4RetValNatRsvdPortTrigInfoStartTime;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatRsvdPortTrigInfoAppName
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatRsvdPortTrigInfoAppName (INT4 i4NatRsvdPortTrigInfoAppIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValNatRsvdPortTrigInfoAppName)
{
    int                 rc;
    tNatwnmhGetNatRsvdPortTrigInfoAppName lv;

    lv.cmd = NMH_GET_NAT_RSVD_PORT_TRIG_INFO_APP_NAME;
    lv.i4NatRsvdPortTrigInfoAppIndex = i4NatRsvdPortTrigInfoAppIndex;
    lv.pRetValNatRsvdPortTrigInfoAppName = pRetValNatRsvdPortTrigInfoAppName;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatRsvdPortTrigInfoInBoundPortRange
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatRsvdPortTrigInfoInBoundPortRange (INT4 i4NatRsvdPortTrigInfoAppIndex,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pRetValNatRsvdPortTrigInfoInBoundPortRange)
{
    int                 rc;
    tNatwnmhGetNatRsvdPortTrigInfoInBoundPortRange lv;

    lv.cmd = NMH_GET_NAT_RSVD_PORT_TRIG_INFO_IN_BOUND_PORT_RANGE;
    lv.i4NatRsvdPortTrigInfoAppIndex = i4NatRsvdPortTrigInfoAppIndex;
    lv.pRetValNatRsvdPortTrigInfoInBoundPortRange =
        pRetValNatRsvdPortTrigInfoInBoundPortRange;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatRsvdPortTrigInfoOutBoundPortRange
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatRsvdPortTrigInfoOutBoundPortRange (INT4 i4NatRsvdPortTrigInfoAppIndex,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pRetValNatRsvdPortTrigInfoOutBoundPortRange)
{
    int                 rc;
    tNatwnmhGetNatRsvdPortTrigInfoOutBoundPortRange lv;

    lv.cmd = NMH_GET_NAT_RSVD_PORT_TRIG_INFO_OUT_BOUND_PORT_RANGE;
    lv.i4NatRsvdPortTrigInfoAppIndex = i4NatRsvdPortTrigInfoAppIndex;
    lv.pRetValNatRsvdPortTrigInfoOutBoundPortRange =
        pRetValNatRsvdPortTrigInfoOutBoundPortRange;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNatRsvdPortTrigInfoProtocol
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNatRsvdPortTrigInfoProtocol (INT4 i4NatRsvdPortTrigInfoAppIndex,
                                   INT4 *pi4RetValNatRsvdPortTrigInfoProtocol)
{
    int                 rc;
    tNatwnmhGetNatRsvdPortTrigInfoProtocol lv;

    lv.cmd = NMH_GET_NAT_RSVD_PORT_TRIG_INFO_PROTOCOL;
    lv.i4NatRsvdPortTrigInfoAppIndex = i4NatRsvdPortTrigInfoAppIndex;
    lv.pi4RetValNatRsvdPortTrigInfoProtocol =
        pi4RetValNatRsvdPortTrigInfoProtocol;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: NatApiSearchPolicyTable
 *
 * -------------------------------------------------------------
 */
INT1
NatApiSearchPolicyTable (UINT4 u4IpAddress)
{
    int                 rc;
    tNatwNatApiSearchPolicyTable lv;

    lv.cmd = NAT_API_SEARCH_POLICY_TABLE;
    lv.u4IpAddress = u4IpAddress;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    return (rc < NAT_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
}
