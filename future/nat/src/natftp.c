/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: natftp.c,v 1.11 2014/01/31 13:10:31 siva Exp $
 *
 * Description:This file contains functions required for
 *             modifying the ftp payload if required.
 *
 *******************************************************************/
#ifndef _NATFTP_C
#define _NATFTP_C
#include "natinc.h"

/******************************************************************************
*                 FUNCTIONS PROTOTYPES
*******************************************************************************/
INT4                NatFtpSubsIpAddr
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IpAddr, UINT4 u4OldIpAddr,
           UINT4 u4Offset, UINT4 *pu4TranslatedLocIpLen,
           tHeaderInfo * pHeaderInfo));

INT4                NatFtpSubsPort
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2TranslatedLocPort,
           UINT2 u2OldPort, UINT4 u4WriteOffset, UINT1 u1IpHeadLen,
           INT4 *pi4Delta));

PRIVATE UINT4 NatPayloadIpPortModify ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf,
                                                tHeaderInfo * pHeaderInfo,
                                                UINT4 u4Offset, UINT1 u1Cmd));

PRIVATE VOID NatFtpGetPayloadIpPort ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf,
                                               CHR1
                                               u1IpAddr[NAT_IPADDR_LEN_MAX],
                                               INT1 u1Port[NAT_PORT_LEN_MAX],
                                               UINT4 *u4Offset, UINT1 u1Cmd,
                                               tGlobalInfo * pPayload));

PRIVATE UINT4       NatCheckPortPasvCmd
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo,
           UINT4 u4ReadOffset));

PRIVATE INT4 NatFtpSubsIpPortString ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf,
                                               UINT4 u4GlobalIpAddr,
                                               UINT4 u4TranslatedLocIpAddr,
                                               UINT2 u2GlobalPort,
                                               UINT2 u2TranslatedLocPort,
                                               UINT4 u4WriteOffset,
                                               INT4 *pi4Delta));

/***************************************************************************
* Function Name  :  NatFtpSubsIpAddr
* Description    :  This function substitute the local IP address with
*                   Global IP address in the packet( payload).
*
* Input (s)      :  1. pBuf - Contains the IP and TCP header along with the
*                             FTP payload.
*
* Output (s)     :  pBuf modified with the Global IP address
* Returns        :  i4Delta - change in size of the old and new IP address.
*
****************************************************************************/
INT4
NatFtpSubsIpAddr (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IpAddr,
                  UINT4 u4OldIpAddr, UINT4 u4Offset,
                  UINT4 *pu4TranslatedLocIpLen, tHeaderInfo * pHeaderInfo)
{

    /*
     * Temporary buffers used to store and manipulate the IP address to be
     * translated .The IP address are stored in the ascii of each digit
     * i.e 10.0.0.1 is stored as "0x31 0x30 0x2e 0x30 0x2e 0x30 0x2e 0x31".
     */
    /*new IP ad dr */
    INT1                ai1IpAddr[NAT_IPADDR_LEN_MAX] = { NAT_ZERO };
    INT1                ai1TempIpAddr[NAT_IPADDR_LEN_MAX] = { NAT_ZERO };
    INT1                ai1TempIpAddr1[NAT_IPADDR_LEN_MAX] = { NAT_ZERO };
    /*old IP ad dr */
    INT1                ai1OldIpAddr[NAT_IPADDR_LEN_MAX] = { NAT_ZERO };
    UINT1               u1WordLength = NAT_ZERO;
    UINT2               u2Chksum = NAT_ZERO;
    INT4                i4OldLen = NAT_ZERO;
    INT4                i4NewLen = NAT_ZERO;
    INT4                i4Delta = NAT_ZERO;
    UINT4               u4Index = NAT_ZERO;
    UINT4               u4CkSumOffset = NAT_ZERO;
    UINT1               u1IpHeadLen = NAT_ZERO;
    tCRU_BUF_CHAIN_HEADER *pRestBuf = NULL;
    tUtlInAddr          Addr = { NAT_ZERO };
    UINT1               u1TcpCksumNeeded = NAT_ZERO;

    NAT_DBG (NAT_DBG_ENTRY, "\n Entering NatFtpSubsIpAddr \n");
    /*
     * This function substitutes the local IP address with the Global IP address
     * in their ascii form. While this is being done the number of bytes for the
     * new IP address may differ with that of the old IP addr. So everything is
     * taken care - length and the chksum
     */

    u4Index = NAT_ZERO;
    u4CkSumOffset = NAT_ZERO;
    u1IpHeadLen = pHeaderInfo->u1IpHeadLen;
    u1TcpCksumNeeded = NAT_FAILURE;
    /*
     * Convert the integer equivalent of an IP address to corresponding
     * ascii value.
     */
    Addr.u4Addr = OSIX_HTONL (u4IpAddr);    /*Host to Network byte order */
    STRNCPY (ai1IpAddr, INET_NTOA (Addr), NAT_IPADDR_LEN_MAX - NAT_ONE);

    /* Substitute '.' with ',' ( in ascii value) as that is the way it is stored
     * in the FTP packet at the payload part.
     * i.e 10.0.0.1 is stored as "0x31 0x30 0x2c 0x30 0x2c 0x30 0x2c 0x31".
     */
    u1WordLength = (UINT1) (STRLEN (ai1IpAddr));
    u4Index = NAT_ZERO;

    /* Substitute '.' with ',' ( in ascii value) for the new IP address */
    while (u1WordLength != NAT_ZERO)
    {
        /*    if (u4Index > NAT_IPADDR_LEN_MAX)
           { */
        if (ai1IpAddr[u4Index] == NAT_PAYLOAD_2E)
        {
            ai1IpAddr[u4Index] = NAT_PAYLOAD_2C;
        }
        u1WordLength--;
        u4Index++;
        /*} */
    }
    ai1IpAddr[u4Index] = '\0';

    Addr.u4Addr = OSIX_HTONL (u4OldIpAddr);
    STRNCPY (ai1OldIpAddr, INET_NTOA (Addr), NAT_IPADDR_LEN_MAX - NAT_ONE);
    u1WordLength = (UINT1) (STRLEN (ai1OldIpAddr));
    u4Index = NAT_ZERO;
    /* Substitute '.' with ',' ( in ascii value) for the old IP address */
    while (u1WordLength != NAT_ZERO)
    {
        /* if (u4Index > NAT_IPADDR_LEN_MAX)
           { */
        if (ai1OldIpAddr[u4Index] == NAT_PAYLOAD_2E)
        {
            ai1OldIpAddr[u4Index] = NAT_PAYLOAD_2C;
        }
        u1WordLength--;
        u4Index++;
        /* } */
    }

    ai1OldIpAddr[u4Index] = '\0';
    i4NewLen = (INT4) STRLEN (ai1IpAddr);
    i4OldLen = (INT4) STRLEN (ai1OldIpAddr);
    /*
     * The following lines in the if block ensures that the new IP address is
     * copied at the offset which is even. If the write-offset is not even then
     * we put a null character so that the IP address to be written starts from
     * the even offset
     */
    if ((u4Offset % MODULO_DIVISOR_2) != NAT_ZERO)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &ai1TempIpAddr[NAT_INDEX_0],
                                   u4Offset - NAT_ONE, NAT_ONE_BYTE);
        ai1TempIpAddr[NAT_INDEX_1] = '\0';
        SNPRINTF ((CHR1 *) ai1TempIpAddr1, sizeof (ai1TempIpAddr1), "%s",
                  ai1TempIpAddr);
        SNPRINTF ((CHR1 *) ai1TempIpAddr1 + STRLEN (ai1TempIpAddr1),
                  sizeof (ai1TempIpAddr1) - STRLEN (ai1TempIpAddr1), "%s",
                  ai1OldIpAddr);
        SNPRINTF ((CHR1 *) ai1OldIpAddr, sizeof (ai1OldIpAddr), "%s",
                  ai1TempIpAddr1);
        SNPRINTF ((CHR1 *) ai1TempIpAddr + STRLEN (ai1TempIpAddr),
                  sizeof (ai1TempIpAddr) - STRLEN (ai1TempIpAddr), "%s",
                  ai1IpAddr);
        STRNCPY (ai1IpAddr, ai1TempIpAddr, STRLEN (ai1TempIpAddr));

        i4NewLen = (INT4) STRNLEN (ai1IpAddr, NAT_IPADDR_LEN_MAX);
        i4OldLen = (INT4) STRNLEN (ai1OldIpAddr, NAT_IPADDR_LEN_MAX);
        ai1IpAddr[i4NewLen] = '\0';
    }
    u4CkSumOffset = NAT_TCP_CKSUM_OFFSET + (UINT4) u1IpHeadLen;
    /*
     * The length of the new IP address (in ascii form including the comas)
     * should not be greater than 15 (for 255.255.255.255 in ascii 15 bytes are
     * required).If it is greater than 15 then switch on the TcpCksum flag to 
     * indicate that the TCP Cksum has to be recalculated down the processing
     * path .
     * If the length is < 15 then the Cksum has just to be adjusted.
     */
    if (i4NewLen < NAT_IPADDR_LEN_MAX_VPN)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2Chksum, u4CkSumOffset,
                                   NAT_WORD_LEN);
        /*
         * Adjust the new IP addr part by stuffing the bytes with '0'.This
         * stuffing is done only when one of the two IP addresses - the old
         * and the new is odd (and the other is even).
         */
        NatDataAdjust ((UINT1 *) ai1IpAddr, i4NewLen, i4OldLen);
        i4NewLen = (INT4) STRLEN (ai1IpAddr);
        /* Adjust the TCP Cksum accordingly */
        NatChecksumAdjust ((UINT1 *) &u2Chksum, (UINT1 *) ai1OldIpAddr,
                           i4OldLen, (UINT1 *) ai1IpAddr, i4NewLen);
    }
    else
    {
        u1TcpCksumNeeded = NAT_SUCCESS;
        u2Chksum = NAT_ZERO;
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2Chksum, u4CkSumOffset,
                                   NAT_WORD_LEN);
    }

    /* One is added to actual length of global IP address to consider comma
     * in between IP addr and Port in PASV/PORT command,in case of odd offset
     * no need to add one, as one extra byte is considered when preparing
     * string to replace old string.
     */

    if (u4Offset % MODULO_DIVISOR_2 != NAT_ZERO)
    {
        *pu4TranslatedLocIpLen = (UINT4) i4NewLen;
    }
    else
    {
        *pu4TranslatedLocIpLen = (UINT4) i4NewLen + NAT_ONE;
    }

    i4Delta = i4NewLen - i4OldLen;
    /*
     * if there is a change in size, Cut the buffer at offset. Delete the old
     * IP address length number of bytes from the start of the second buffer(
     * this is done by moving the Offset pointer that many number of bytes). Add
     * the new Global IP address (in ascii) at the beginning of the second
     * buffer. Concatenate the two buffer.
     */
    if (i4Delta != NAT_ZERO)
    {
        /* BUFFER_CHANGE starts */

        /* If the offset is odd then fragment the BufChain from even offset 
         * by taking one more byte before exact offset
         */
        if (u4Offset % MODULO_DIVISOR_2 != NAT_ZERO)
        {
            if (CRU_BUF_Fragment_BufChain (pBuf, u4Offset
                                           - NAT_ONE, &pRestBuf) == CRU_FAILURE)
            {
                return NAT_FAILURE;
            }
        }
        else
        {
            if (CRU_BUF_Fragment_BufChain (pBuf, u4Offset, &pRestBuf)
                == CRU_FAILURE)
            {
                return NAT_FAILURE;
            }
        }
        /* BUFFER_CHANGE ends */
        CRU_BUF_Move_ValidOffset (pRestBuf, STRLEN (ai1OldIpAddr));
        CRU_BUF_Prepend_BufChain (pRestBuf, (UINT1 *) ai1IpAddr,
                                  STRLEN (ai1IpAddr));
        CRU_BUF_Concat_MsgBufChains (pBuf, pRestBuf);
    }
    else
    {
        /* If size is same, copy over the buffer chain */
        /* If the offset is odd then copy from even offset
         * by taking one more byte before exact offset
         */
        if (u4Offset % MODULO_DIVISOR_2 != NAT_ZERO)
        {
            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) ai1IpAddr,
                                       u4Offset - NAT_ONE, STRLEN (ai1IpAddr));
        }
        else
        {
            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) ai1IpAddr, u4Offset,
                                       STRLEN (ai1IpAddr));
        }
    }

    pHeaderInfo->i4Delta = i4Delta;
    if (u1TcpCksumNeeded == NAT_SUCCESS)
    {
        u2Chksum = NatTransportCksumAdjust (pBuf, pHeaderInfo);
        if (u2Chksum == NAT_FAILURE)
        {
            return NAT_FAILURE;
        }
    }
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2Chksum, u4CkSumOffset,
                               NAT_WORD_LEN);
    NAT_DBG2 (NAT_DBG_FTP,
              "\n FTP payload modification for LIP %d is GIP %d \n",
              u4OldIpAddr, u4IpAddr);

    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatFtpSubsIpAddr \n");
    return NAT_SUCCESS;

}

/***************************************************************************
* Function Name    :  NatIntToAscii
* Description      :  This function converts an integer value to corresponding
*                     ascii notation.
*
* Input (s)        :  1. u2Port - The integer number to be converted.
*                     2. ai1Port[] - Corresponding ascii equivalent.
*
* Output (s)       :  ai1Port - Contains the converted ascii equivalent.
* Returns          :  None
*
****************************************************************************/

PUBLIC VOID
NatIntToAscii (UINT2 u2OldPort, INT1 ai1Port[])
{
    UINT4               u4Count = NAT_ZERO;
    UINT4               u4Len = NAT_ZERO;
    UINT1               u1TempVar = NAT_ZERO;
    UINT2               u2Port = NAT_ZERO;

    u4Count = NAT_ZERO;
    u2Port = u2OldPort;

    NAT_DBG (NAT_DBG_ENTRY, "\n Entering NatIntToAscii \n");
    do
    {
        /* 10 is used to extract each digit */
        ai1Port[u4Count++] = (INT1) (u2Port % MODULO_DIVISOR_10 + '0');
    }
    while ((u2Port /= (UINT2) NAT_TEN) > NAT_ZERO);
    ai1Port[u4Count] = NAT_STRING_END;
    /*
     * the integer is stored in ascii form in reverse order .So we reverse that
     */

    u4Len = STRLEN ((INT1 *) ai1Port) - NAT_ONE;
    for (u4Count = NAT_ZERO; u4Count < u4Len; u4Count++)
    {
        u1TempVar = (UINT1) ai1Port[u4Count];
        ai1Port[u4Count] = ai1Port[u4Len];
        ai1Port[u4Len] = (INT1) u1TempVar;
        u4Len--;
    }
    NAT_DBG2 (NAT_DBG_FTP, "\n ASCII mapping of port %d is %s \n", u2OldPort,
              ai1Port);

    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatIntToAscii \n");
}

/***************************************************************************
* Function Name    :  NatFtpSubsPort
* Description    :  This function is used for substituting the Local Port
*             with Global port if NAPT is enabled.
*
* Input (s)  :1. pBuf - Contains the IP and TCP header along with FTP
*                       payload.
*             2. u2TranslatedLocPort - The port value to be substituted.
*             3. u2OldPort - The port value which has to be substituted.
*             4. u4WriteOffset - The offset at which substitution should
*            be done.
*
*
* Output (s)    :  pBuf modified with Global Port
* Returns      :  i4Delta - change in size between u2TranslatedLocPort and 
*               u2OldPort
*
****************************************************************************/

INT4
NatFtpSubsPort (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2TranslatedLocPort,
                UINT2 u2OldPort, UINT4 u4WriteOffset, UINT1 u1IpHeadLen,
                INT4 *pi4Delta)
{

    tCRU_BUF_CHAIN_HEADER *pRestBuf = NULL;
    /*
     * the following arrays are used to store the ports in their ascii value
     */
    INT1                ai1Str[NAT_PORT_LEN_MAX] = { NAT_ZERO };
    INT1                ai1OldPort[NAT_PORT_LEN_MAX] = { NAT_ZERO };
    INT1                ai1TempStr[NAT_PORT_LEN_MAX] = { NAT_ZERO };
    INT1                ai1TempStr1[NAT_PORT_LEN_MAX] = { NAT_ZERO };

    INT4                i4OldLen = NAT_ZERO;
    INT4                i4NewLen = NAT_ZERO;
    UINT2               u2Chksum = NAT_ZERO;
    UINT4               u4CkSumOffset = NAT_ZERO;
    UINT2               u2TempData = NAT_ZERO;

    *pi4Delta = NAT_ZERO;

    NAT_DBG (NAT_DBG_ENTRY, "\n Entering NatFtpSubsPort \n");
    NAT_DBG2 (NAT_DBG_FTP, "\n FTP payload old and new Port are %d, %d \n",
              u2OldPort, u2TranslatedLocPort);

    /*
     * Convert Port number from integer to ascii and in FTP port notation 
     * Checksum is adjusted accordingly .And if there is any change in the
     * length then the processing is done accordingly
     */
    u2TempData = (UINT2) (u2TranslatedLocPort / NAT_FTP_PORT_DIV_FACTOR);
    NatIntToAscii (u2TempData, ai1TempStr);
    SNPRINTF ((CHR1 *) ai1Str, sizeof (ai1Str), "%s", ai1TempStr);
    SNPRINTF ((CHR1 *) ai1Str + STRLEN (ai1Str),
              sizeof (ai1Str) - STRLEN (ai1Str), "%s", ",");
    u2TempData = (UINT2) (u2TranslatedLocPort % NAT_FTP_PORT_DIV_FACTOR);
    NatIntToAscii (u2TempData, ai1TempStr);
    SNPRINTF ((CHR1 *) ai1Str + STRLEN (ai1Str),
              sizeof (ai1Str) - STRLEN (ai1Str), "%s", ai1TempStr);
    u2TempData = (UINT2) (u2OldPort / NAT_FTP_PORT_DIV_FACTOR);
    NatIntToAscii (u2TempData, ai1TempStr);
    STRNCPY (ai1OldPort, ai1TempStr, (sizeof (ai1OldPort) - NAT_ONE));
    SNPRINTF ((CHR1 *) ai1OldPort + STRLEN (ai1OldPort),
              sizeof (ai1OldPort) - STRLEN (ai1OldPort), "%s", ",");
    u2TempData = (UINT2) (u2OldPort % NAT_FTP_PORT_DIV_FACTOR);
    NatIntToAscii (u2TempData, ai1TempStr);
    SNPRINTF ((CHR1 *) ai1OldPort + STRLEN (ai1OldPort),
              sizeof (ai1OldPort) - STRLEN (ai1OldPort), "%s", ai1TempStr);

    i4NewLen = (INT4) STRNLEN (ai1Str, NAT_PORT_LEN_MAX);
    i4OldLen = (INT4) STRNLEN (ai1OldPort, NAT_PORT_LEN_MAX);
    /*
     * The Length of  the old Port and the new Port should
     * be either both even or both odd. If one of them is even while the other
     * odd then byte stuffing (with '0') is done in the new Port .
     */
    if ((i4NewLen % MODULO_DIVISOR_2) != (i4OldLen % MODULO_DIVISOR_2))
    {
        STRNCPY (ai1TempStr, "0", sizeof (ai1TempStr));
        SNPRINTF ((CHR1 *) ai1TempStr + STRLEN (ai1TempStr),
                  sizeof (ai1TempStr) - STRLEN (ai1TempStr), "%s", ai1Str);
        STRNCPY (ai1Str, ai1TempStr, (sizeof (ai1Str) - NAT_ONE));
    }
    if ((u4WriteOffset % MODULO_DIVISOR_2) != NAT_ZERO)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &ai1TempStr[NAT_ZERO],
                                   u4WriteOffset - NAT_ONE, NAT_ONE_BYTE);
        ai1TempStr[NAT_INDEX_1] = '\0';
        STRNCPY (ai1TempStr1, ai1TempStr, (sizeof (ai1TempStr1) - NAT_ONE));
        SNPRINTF ((CHR1 *) ai1TempStr1 + STRLEN (ai1TempStr1),
                  sizeof (ai1TempStr1) - STRLEN (ai1TempStr1), "%s",
                  ai1OldPort);
        STRNCPY (ai1OldPort, ai1TempStr1, sizeof (ai1OldPort) - NAT_ONE);
        SNPRINTF ((CHR1 *) ai1TempStr + STRLEN (ai1TempStr),
                  sizeof (ai1TempStr) - STRLEN (ai1TempStr), "%s", ai1Str);
        STRNCPY (ai1Str, ai1TempStr, (sizeof (ai1Str) - NAT_ONE));

    }
    i4NewLen = (INT4) STRNLEN (ai1Str, NAT_PORT_LEN_MAX);
    i4OldLen = (INT4) STRNLEN (ai1OldPort, NAT_PORT_LEN_MAX);
    u4CkSumOffset = NAT_TCP_CKSUM_OFFSET + (UINT4) u1IpHeadLen;
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2Chksum, u4CkSumOffset,
                               NAT_WORD_LEN);

    i4NewLen = (INT4) STRNLEN (ai1Str, NAT_PORT_LEN_MAX);
    NatChecksumAdjust ((UINT1 *) &u2Chksum, (UINT1 *) ai1OldPort, i4OldLen,
                       (UINT1 *) ai1Str, i4NewLen);
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2Chksum, u4CkSumOffset,
                               NAT_WORD_LEN);
    *pi4Delta = i4NewLen - i4OldLen;

    /*
     * if there is a change in size, fragment the buffer at offset. Delete
     * the old port length number of bytes from the start of the second
     * buffer. Add (prepend) the new port number (in ascii) at the beginning
     * of the second buffer. Concatenate the two buffers.
     */
    if (*pi4Delta != NAT_ZERO)
    {
        /*Fragment from the even offset */
        if ((u4WriteOffset % MODULO_DIVISOR_2) != NAT_ZERO)
        {
            /* BUFFER_CHANGE starts */
            if (CRU_BUF_Fragment_BufChain (pBuf,
                                           u4WriteOffset - NAT_ONE,
                                           &pRestBuf) == CRU_FAILURE)
            {
                return NAT_FAILURE;
            }
            /* BUFFER_CHANGE ends */
            CRU_BUF_Move_ValidOffset (pRestBuf, STRLEN (ai1OldPort));
        }
        else
        {
            /* BUFFER_CHANGE starts */
            if (CRU_BUF_Fragment_BufChain (pBuf, u4WriteOffset, &pRestBuf) ==
                CRU_FAILURE)
            {
                return NAT_FAILURE;
            }
            /* BUFFER_CHANGE ends */
            CRU_BUF_Move_ValidOffset (pRestBuf, STRLEN (ai1OldPort));
        }
        CRU_BUF_Prepend_BufChain (pRestBuf, (UINT1 *) ai1Str, STRLEN (ai1Str));
        CRU_BUF_Concat_MsgBufChains (pBuf, pRestBuf);
    }
    else
    {

        /* if the size is same, just copy over the buffer */
        if ((u4WriteOffset % MODULO_DIVISOR_2) != NAT_ZERO)
        {
            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) ai1Str,
                                       u4WriteOffset - NAT_ONE,
                                       STRLEN (ai1Str));
        }
        else
        {
            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) ai1Str, u4WriteOffset,
                                       STRLEN (ai1Str));
        }
    }

    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatFtpSubsPort \n");
    return NAT_SUCCESS;            /*BUFFER_CHANGE */
}

/***************************************************************************
*
* Function Name  :  NatFtpSubsIpPortString
*
* Description    :  This function is used for substituting the Local Ip, Port 
*                    Global Ip, and Port in the FTP Payload.
*
* Input (s)      :  1. pBuf - Contains the IP and TCP header along with FTP
*                       payload.
*                   2. u4GlobalIpAddr - Global Ip address to be substituted.
*                   3. u4TranslatedLocIpAddr - Ip address which has to be 
*                                               substituted.
*                   4. u2GlobalPort - Port value to be substituted.
*                   5. u2TranslatedLocPort - Port value which has to be 
*                                             substituted.
*                   6. u4WriteOffset - Offset at which substitution should
*                                       be done.
*                   7. pi4Delta - Difference in the old length to new length
*                                  is stored in the pointer which will be 
*                                  used to adjust the TCP Seq/Ack no 
*                                  accordingly.
*
* Output (s)     :  pBuf modified with Global Port
*
* Returns        :  NAT_SUCCESS - if the Ip, Port substitution is successfull.
*                   NAT_FAILURE - if Ip, Port substitution is failed.
*
****************************************************************************/

PRIVATE INT4
NatFtpSubsIpPortString (tCRU_BUF_CHAIN_HEADER * pBuf,
                        UINT4 u4GlobalIpAddr,
                        UINT4 u4TranslatedLocIpAddr,
                        UINT2 u2GlobalPort,
                        UINT2 u2TranslatedLocPort,
                        UINT4 u4WriteOffset, INT4 *pi4Delta)
{
    tCRU_BUF_CHAIN_HEADER *pRestBuf = NULL;
    UINT1              *pu1TempIpAddr = NULL;
    UINT1              *pu1TempPort = NULL;
    UINT4               u4OrigStrLen = NAT_ZERO;
    UINT4               u4TransStrLen = NAT_ZERO;
    INT1                ai1OrigStr[NAT_IPADDR_PORT_LEN_MAX] = { NAT_ZERO };
    INT1                ai1TransStr[NAT_IPADDR_PORT_LEN_MAX] = { NAT_ZERO };
    INT1                i1Chr = NAT_ZERO;

    NAT_DBG (NAT_DBG_ENTRY, "\n Entering NatFtpSubsIpPortString \n");
    NAT_DBG2 (NAT_DBG_FTP, "\n FTP payload orig and trans IP are %x, %x \n",
              u4TranslatedLocIpAddr, u4GlobalIpAddr);
    NAT_DBG2 (NAT_DBG_FTP, "\n FTP payload orig and trans port are %x, %x \n",
              u2TranslatedLocPort, u2GlobalPort);

    if ((NULL == pBuf) || (NULL == pi4Delta))
    {
        NAT_DBG (NAT_DBG_EXIT, "\n pBuf or pi4Delta is NULL Exiting\
                 NatFtpSubsIpPortString\n");
        return NAT_FAILURE;
    }

    MEMSET (ai1OrigStr, NAT_ZERO, sizeof (ai1OrigStr));
    MEMSET (ai1TransStr, NAT_ZERO, sizeof (ai1TransStr));

    u4TranslatedLocIpAddr = OSIX_HTONL (u4TranslatedLocIpAddr);
    pu1TempIpAddr = (UINT1 *) &u4TranslatedLocIpAddr;

    u2TranslatedLocPort = OSIX_HTONS (u2TranslatedLocPort);
    pu1TempPort = (UINT1 *) &u2TranslatedLocPort;

    if ((u4WriteOffset % MODULO_DIVISOR_2) != NAT_ZERO)
    {
        /* Copy the Even Offset character to the local buffer and 
         * prepend it to the new buffer. */
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &i1Chr,
                                   u4WriteOffset - NAT_ONE, NAT_ONE_BYTE);

        SNPRINTF ((CHR1 *) ai1OrigStr, NAT_IPADDR_PORT_LEN_MAX,
                  "%c%d,%d,%d,%d,%d,%d", i1Chr, *pu1TempIpAddr,
                  *(pu1TempIpAddr + NAT_ONE),
                  *(pu1TempIpAddr + NAT_TWO),
                  *(pu1TempIpAddr + NAT_THREE),
                  *pu1TempPort, *(pu1TempPort + NAT_ONE));
    }
    else
    {
        SNPRINTF ((CHR1 *) ai1OrigStr, NAT_IPADDR_PORT_LEN_MAX,
                  "%d,%d,%d,%d,%d,%d", *pu1TempIpAddr,
                  *(pu1TempIpAddr + NAT_ONE),
                  *(pu1TempIpAddr + NAT_TWO),
                  *(pu1TempIpAddr + NAT_THREE), *pu1TempPort,
                  *(pu1TempPort + NAT_ONE));
    }

    u4GlobalIpAddr = OSIX_HTONL (u4GlobalIpAddr);
    pu1TempIpAddr = (UINT1 *) &u4GlobalIpAddr;

    u2GlobalPort = OSIX_HTONS (u2GlobalPort);
    pu1TempPort = (UINT1 *) &u2GlobalPort;

    if ((u4WriteOffset % MODULO_DIVISOR_2) != NAT_ZERO)
    {
        /* Using the Same even offset character from the above i1Chr */
        SNPRINTF ((CHR1 *) ai1TransStr, NAT_IPADDR_PORT_LEN_MAX,
                  "%c%d,%d,%d,%d,%d,%d", i1Chr, *pu1TempIpAddr,
                  *(pu1TempIpAddr + NAT_ONE),
                  *(pu1TempIpAddr + NAT_TWO),
                  *(pu1TempIpAddr + NAT_THREE),
                  *pu1TempPort, *(pu1TempPort + NAT_ONE));
    }
    else
    {
        SNPRINTF ((CHR1 *) ai1TransStr, NAT_IPADDR_PORT_LEN_MAX,
                  "%d,%d,%d,%d,%d,%d", *pu1TempIpAddr,
                  *(pu1TempIpAddr + NAT_ONE),
                  *(pu1TempIpAddr + NAT_TWO),
                  *(pu1TempIpAddr + NAT_THREE), *pu1TempPort,
                  *(pu1TempPort + NAT_ONE));
    }

    u4OrigStrLen = STRLEN (ai1OrigStr);
    u4TransStrLen = STRLEN (ai1TransStr);

    /*
     * If there is a change in size, fragment the buffer at offset. Delete
     * the old Ipadr, port length number of bytes from the start of the 
     * second buffer. Add (prepend) the new Ipaddr, port number (in ascii) 
     * at the beginning of the second buffer. Concatenate the two buffers.
     */

    /* BUFFER_CHANGE starts */
    if (CRU_BUF_Fragment_BufChain (pBuf, ((u4WriteOffset % MODULO_DIVISOR_2) ? u4WriteOffset - NAT_ONE :    /* Odd offset */
                                          u4WriteOffset),    /* Even offset */
                                   &pRestBuf) == CRU_FAILURE)
    {
        NAT_DBG (NAT_DBG_EXIT, "\n Fragment BufChain Failed Exiting\
                 NatFtpSubsIpPortString\n");
        return NAT_FAILURE;
    }

    CRU_BUF_Move_ValidOffset (pRestBuf, u4OrigStrLen);

    CRU_BUF_Prepend_BufChain (pRestBuf, (UINT1 *) ai1TransStr, u4TransStrLen);
    CRU_BUF_Concat_MsgBufChains (pBuf, pRestBuf);
    /* BUFFER_CHANGE ends */

    /* Delta is difference of New String Len -  Old String Len */
    *pi4Delta = (INT4) (u4TransStrLen - u4OrigStrLen);

    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatFtpSubsIpPortString \n");
    return NAT_SUCCESS;

}

/***************************************************************************
* Function Name    :  NatPayloadIpPortModify
* Description    :  This function is used for modifying the IP and port number
*                    with Global IP and port.
*
* Input (s)    :  1. pBuf - Contains the IP and TCP header along with FTP
*            payload
*             2. pHeaderInfo - Contains the extracted information of IP
*            and TCP header.
*             3. u4Offset - Gives the start of the IP address
*             4. u1Cmd - Whether the packet contains "PORT" command or
*            "227" PASV reply code.
*
* Output (s)    :  Modified pBuf with Global IP and port.
* Returns      :  NAT_SUCCESS or NAT_FAILURE
*
****************************************************************************/

PRIVATE UINT4
NatPayloadIpPortModify (tCRU_BUF_CHAIN_HEADER * pBuf,
                        tHeaderInfo * pHeaderInfo, UINT4 u4Offset, UINT1 u1Cmd)
{
    CHR1                ai1IpAddr[NAT_IPADDR_LEN_MAX] = { NAT_ZERO };
    INT1                ai1Port[NAT_PORT_LEN_MAX] = { NAT_ZERO };
    UINT4               u4WriteOffset = NAT_ZERO;
    tGlobalInfo         PaylaodData;
    tGlobalInfo         GlobalInfo;
    INT4                i4Delta = NAT_ZERO;
    UINT4               u4Status = NAT_ZERO;
    UINT2               u2Chksum = NAT_ZERO;
    tHeaderInfo         NewHeaderInfo;

    i4Delta = NAT_ZERO;
    u4Status = NAT_SUCCESS;

    NAT_TRC (NAT_TRC_ON, "\n Inside NatPayloadIpPortModify \n");
    /*
     * this function substitutes the IP addr and port in the FTP header with the
     * Global IP and Port (if NAPT) for Outbound packets.There are two cases
     * 1)For Port Command - Here the port number (on the Client side to which
     * the Server initiates connection) is sent in the control packet from the
     * client.So the port number has to be translated along with the IP addr
     * in the pkt payload.This happens when the client is in the Inside 
     * Network and Ftp Server in the Outside Network.
     * 2)For PASV Command - Here the port number (on the Server side to which
     * the Client initiates connection) is sent in the control packet  from the
     * Server.So this port number has to be translated if the Server lies in the
     * Inside Network and the Client in the Outside Network.
     */

    /* Extract the IP address and Port */

    MEMSET (&PaylaodData, NAT_ZERO, sizeof (tGlobalInfo));
    NatFtpGetPayloadIpPort (pBuf, ai1IpAddr, ai1Port, &u4Offset, u1Cmd,
                            &PaylaodData);
    MEMSET (&NewHeaderInfo, NAT_ZERO, sizeof (tHeaderInfo));
    if (PaylaodData.u4TranslatedLocIpAddr == NAT_ZERO)
    {
        NAT_DBG (NAT_DBG_FTP, "\n Failed to retrieve IP address"
                 " and port from PORT/PASVcommand\n");
        return (NAT_FAILURE);
    }
    u4WriteOffset = u4Offset;

    if (pHeaderInfo->u4Direction == NAT_OUTBOUND)
    {
        /* check if the IP addr in FTP header is a valid IP addr to be
         * translated. */
        u4Status = NatSearchLocIpAddrTable (PaylaodData.u4TranslatedLocIpAddr);
        if (u4Status == NAT_SUCCESS)
        {
            /*Add a new entry if it is a PORT command */
            NewHeaderInfo.u4IfNum = pHeaderInfo->u4IfNum;
            NewHeaderInfo.u4InIpAddr = PaylaodData.u4TranslatedLocIpAddr;
            NewHeaderInfo.u2InPort = PaylaodData.u2TranslatedLocPort;
            NewHeaderInfo.u4OutIpAddr = pHeaderInfo->u4OutIpAddr;
            NewHeaderInfo.u1PktType = NAT_TCP;
            NewHeaderInfo.u4Direction = NAT_INBOUND;
            /* Avoiding retransmission of PORT/PASV command alloacting 
             * new ports */
            if (NatScanPartialLinksList (&NewHeaderInfo,
                                         &GlobalInfo.u4TranslatedLocIpAddr,
                                         &GlobalInfo.u2TranslatedLocPort) !=
                NAT_SUCCESS)
            {
                NAT_TRC (NAT_TRC_ON, "\nNo Previous Partial link Added for "
                         " this entry Adding new one\n");

                NatGetNextFreeGlobalIpPort (PaylaodData.
                                            u4TranslatedLocIpAddr,
                                            PaylaodData.u2TranslatedLocPort,
                                            NewHeaderInfo.u4OutIpAddr,
                                            NewHeaderInfo.u2OutPort,
                                            pHeaderInfo->u4IfNum, &GlobalInfo);
            }
            if (GlobalInfo.u4TranslatedLocIpAddr != NAT_ZERO)
            {
                NewHeaderInfo.u4InIpAddr = GlobalInfo.u4TranslatedLocIpAddr;
                if (GlobalInfo.u2TranslatedLocPort != NAT_ZERO)
                {
                    NewHeaderInfo.u2InPort = GlobalInfo.u2TranslatedLocPort;
                }
                else
                {
                    NewHeaderInfo.u2InPort = PaylaodData.u2TranslatedLocPort;
                }
                NatAddPartialLinksList (&NewHeaderInfo,
                                        PaylaodData.u4TranslatedLocIpAddr,
                                        PaylaodData.u2TranslatedLocPort,
                                        NAT_NON_PERSISTENT);

            }
            else
            {
                u4Status = NAT_FAILURE;
            }
            NewHeaderInfo.u2OutPort = pHeaderInfo->u2OutPort;

            if (u4Status == NAT_SUCCESS)
            {
                /* BUFFER_CHANGE starts */
                /* If NAPT is disabled New Header In-port is same as Translated 
                 * local port because port translation will not done. */

                if (gapNatIfTable[pHeaderInfo->u4IfNum]->u1NaptEnable !=
                    NAT_ENABLE)
                {
                    NewHeaderInfo.u2InPort = PaylaodData.u2TranslatedLocPort;
                }

                u4Status = (UINT4) NatFtpSubsIpPortString
                    (pBuf,
                     NewHeaderInfo.u4InIpAddr,
                     PaylaodData.u4TranslatedLocIpAddr,
                     NewHeaderInfo.u2InPort,
                     PaylaodData.u2TranslatedLocPort, u4WriteOffset, &i4Delta);

                if (u4Status == NAT_SUCCESS)
                {
                    pHeaderInfo->i4Delta = i4Delta;
                }

                u2Chksum = NAT_ZERO;
                /* BUFFER_CHANGE starts */
                CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2Chksum,
                                           (NAT_TCP_CKSUM_OFFSET +
                                            (UINT4) pHeaderInfo->u1IpHeadLen),
                                           NAT_WORD_LEN);

                /* Recalculate the checksum of the TCP header */
                u2Chksum = NatTransportCksumAdjust (pBuf, pHeaderInfo);
                if (u2Chksum == NAT_FAILURE)
                {
                    return NAT_FAILURE;
                }

                CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2Chksum,
                                           (NAT_TCP_CKSUM_OFFSET +
                                            (UINT4) pHeaderInfo->u1IpHeadLen),
                                           NAT_WORD_LEN);
                /* BUFFER_CHANGE ends */

            }
            else
            {
                u4Status = NAT_FAILURE;

            }
        }
        else
        {
            /*
             * This means that the IP Address in the PORT command is not a
             * Local IP Address.
             */
            u4Status = NAT_SUCCESS;
        }

    }
    else                        /* NAT_INBOUND parsing */
    {
        /*Add a new entry if it is a PORT command */
        NewHeaderInfo.u4IfNum = pHeaderInfo->u4IfNum;
        NewHeaderInfo.u4InIpAddr = pHeaderInfo->u4InIpAddr;
        NewHeaderInfo.u4OutIpAddr = PaylaodData.u4TranslatedLocIpAddr;
        NewHeaderInfo.u2OutPort = PaylaodData.u2TranslatedLocPort;
        NewHeaderInfo.u4Direction = NAT_OUTBOUND;
        NewHeaderInfo.u1PktType = NAT_TCP;

        NatAddPartialLinksList (&NewHeaderInfo,
                                PaylaodData.u4TranslatedLocIpAddr,
                                PaylaodData.u2TranslatedLocPort,
                                NAT_NON_PERSISTENT);
    }

    NAT_TRC1 (NAT_TRC_ON, "\n Change in size of FTP payload is %d \n",
              pHeaderInfo->i4Delta);
    NAT_TRC (NAT_TRC_ON, "\n Exiting NatPayloadIpPortModify \n");
    return (u4Status);
}

/***************************************************************************
* Function Name    :  NatFtpGetPayloadIpPort
* Description    :  This function extracts the IP address and Port number
*             embedded in the packet.
*
* Input (s)    :  1. pBuf - Contains the IP and TCP headers along with FTP
*             payload.
*             2. ai1IpAddr - Contains the extracted IP address.
*             3. ai1Port - Contains the extracted port.
*             4. u4Offset - Gives the start of the IP address.
*             5. u1Cmd - Whether "PORT" or "227"
*             6. pGlobalInfo - Contains the extracted IP address and
*            port number.
*
* Output (s)    :  returns the extracted IP address and Port number in
*             pGlobalInfo
* Returns      :  None
*
****************************************************************************/

PRIVATE VOID
NatFtpGetPayloadIpPort (tCRU_BUF_CHAIN_HEADER * pBuf,
                        CHR1 ai1IpAddr[NAT_IPADDR_LEN_MAX],
                        INT1 ai1Port[NAT_PORT_LEN_MAX], UINT4 *u4Offset,
                        UINT1 u1Cmd, tGlobalInfo * pGlobalInfo)
{

    UINT1               u1Byte = NAT_ZERO;
    UINT1               u1Index = NAT_ZERO;
    UINT1               u1CommaCount = NAT_ZERO;
    UINT2               u2Port = NAT_ZERO;
    UINT4               u4PktSize = NAT_ZERO;
    UINT4               u4TempOffset = NAT_ZERO;

    NAT_DBG (NAT_DBG_ENTRY, "\n Entering NatFtpGetPayloadIpPort \n");

    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
    u4TempOffset = *u4Offset;

    if (u1Cmd == NAT_FTP_PASV)
    {
        /* Search for the '(' after end of 227 string */
        while (u4TempOffset < u4PktSize)
        {
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1Byte,
                                       u4TempOffset++, NAT_ONE_BYTE);
            if (u1Byte == '(')
            {
                *u4Offset = u4TempOffset;
                break;
            }
        }
        if (u4TempOffset >= u4PktSize)
        {
            NAT_DBG (NAT_DBG_FTP, "\n No \'(\' in PASV reply\n");
            return;
        }
    }

    /* extract IP address and Port number */

    /*
     * The IP address and port number (in ascii) in the payload is separated
     * by commas. Hence the IP address in nothing but the value separated by
     * four commas. for example
     * 10.0.0.1 is represented by 31 30, 30, 30, 31,
     * Then each byte is extracted until the fourth comma is reached and
     * stored in ai1IpAddress. Similarly port is also read.
     * The FTP payload is delimited by 0x0d 0x0a.
     */
    while (*u4Offset < u4PktSize)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1Byte, (*u4Offset)++,
                                   NAT_ONE_BYTE);
        if (u1Byte == NAT_PAYLOAD_0D)
        {
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1Byte, (*u4Offset)++,
                                       NAT_ONE_BYTE);
            if (u1Byte == NAT_PAYLOAD_0A)
            {
                /*The end of FTP payload is reached */
                switch (u1Cmd)
                {
                    case NAT_FTP_PORT:
                        if (u1Index < NAT_PORT_LEN_MAX)
                        {
                            ai1Port[u1Index] = NAT_STRING_END;
                        }
                        break;
                    case NAT_FTP_PASV:
                    default:
                        if (u1Index < NAT_PORT_LEN_MAX)
                        {
                            ai1Port[--u1Index] = NAT_STRING_END;
                        }
                        break;
                }
                break;
            }
        }
        else
        {
            /* IP addr is read */
            if (u1CommaCount < NAT_FTP_IP_COMMA_COUNT)
            {
                if ((u1Byte == ',')
                    && (u1CommaCount < (NAT_FTP_IP_COMMA_COUNT - NAT_ONE)))
                {
                    if (u1Index < NAT_IPADDR_LEN_MAX)
                    {
                        ai1IpAddr[u1Index++] = '.';
                        u1CommaCount++;
                    }
                }
                else
                {
                    if ((u1Byte == ',')
                        && (u1CommaCount++ ==
                            (NAT_FTP_IP_COMMA_COUNT - NAT_ONE)))
                    {
                        if (u1Index < NAT_IPADDR_LEN_MAX)
                        {
                            ai1IpAddr[u1Index] = NAT_STRING_END;
                            u1Index = NAT_ZERO;
                        }
                    }
                    else
                    {
                        if (u1Index < NAT_IPADDR_LEN_MAX)
                        {
                            ai1IpAddr[u1Index++] = (INT1) u1Byte;
                        }
                    }
                }
            }
            /* Port is read */
            else
            {
                if (u1Byte == ',')
                {
                    if (u1Index < NAT_PORT_LEN_MAX)
                    {
                        ai1Port[u1Index++] = NAT_STRING_END;
                        u2Port =
                            (UINT2) (NAT_ATOI (ai1Port) *
                                     NAT_FTP_PORT_DIV_FACTOR);
                        u1Index = NAT_ZERO;
                    }
                }
                else
                {
                    if (u1Index < NAT_PORT_LEN_MAX)
                    {
                        ai1Port[u1Index++] = (INT1) u1Byte;
                    }
                }
            }
        }
    }                            /* End of While Loop */

    /* Reached end of packet but didn't find 0xd 0xa then end the port
     * string with NAT_STRING_END
     */

    if (*u4Offset >= u4PktSize)
    {
        /*The end of FTP payload is reached */

        switch (u1Cmd)
        {
            case NAT_FTP_PORT:
            case NAT_FTP_PASV:
                if (u1Index < NAT_PORT_LEN_MAX)
                {
                    ai1Port[u1Index] = NAT_STRING_END;
                }
                break;
            default:
                if ((u1Index < NAT_PORT_LEN_MAX) && (u1Index != NAT_ZERO))
                {
                    ai1Port[--u1Index] = NAT_STRING_END;
                }
                break;
        }
    }

    /* conversion of ascii to integer of the port value */
    u2Port = (UINT2) (u2Port + (UINT2) NAT_ATOI (ai1Port));
    pGlobalInfo->u4TranslatedLocIpAddr = OSIX_HTONL (INET_ADDR (ai1IpAddr));
    pGlobalInfo->u2TranslatedLocPort = u2Port;
    *u4Offset = u4TempOffset;

    NAT_DBG2 (NAT_DBG_FTP, "\n Extracted IP address and Port are %u %u \n",
              pGlobalInfo->u4TranslatedLocIpAddr,
              pGlobalInfo->u2TranslatedLocPort);

    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatFtpGetPayloadIpPort \n");
}

/***************************************************************************
* Function Name    :  NatCheckPortPasvCmd
* Description    :  This function checks for the presence of PORT command or
*             reply code in the payload. If present the modification
*             is done.
*
* Input (s)    :  1. pBuf - Contains IP and TCP header information along
*             with FTP payload.
*             2. pHeaderInfo - Contains header information of IP and TCP
*             3. u4ReadOffset - Start of FTP payload.
*
* Output (s)    :  Modified pBuf
* Returns      :  NAT_SUCCESS or NAT_FAILURE
*
****************************************************************************/

PRIVATE UINT4
NatCheckPortPasvCmd (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo,
                     UINT4 u4ReadOffset)
{
    INT1                ai1PortCmd[NAT_FTP_PORT_LEN] = { NAT_ZERO };
    INT1                ai1PasvCmd[NAT_FTP_PASV_REPLY_LEN] = { NAT_ZERO };
    UINT4               u4Status = NAT_ZERO;

    /*
     * this function finds out whether the control packet contains info for PORT
     * mode or for the PASV mode .Accordingly it calls the appropriate function
     */
    u4Status = NAT_SUCCESS;

    NAT_TRC (NAT_TRC_ON, "\n Inside NatCheckPortPasvCmd \n");
    /* check for the word "PORT" in the FTP payload */
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) ai1PortCmd, u4ReadOffset,
                               NAT_FTP_PORT_LEN);
    ai1PortCmd[NAT_INDEX_4] = '\0';
    if ((STRCMP (ai1PortCmd, "PORT")) == NAT_STRCMP_SUCCESS)
    {

        NAT_DBG (NAT_DBG_FTP, "\n PORT command met \n");

        u4Status =
            NatPayloadIpPortModify (pBuf, pHeaderInfo,
                                    u4ReadOffset + NAT_FIVE, NAT_FTP_PORT);

    }
    else
    {

        /* if "PORT" is not found, then search for "227" PASV reply code */
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) ai1PasvCmd, u4ReadOffset,
                                   NAT_FTP_PASV_REPLY_LEN);

        ai1PasvCmd[NAT_INDEX_3] = '\0';
        if ((STRCMP (ai1PasvCmd, "227")) == NAT_STRCMP_SUCCESS)
        {

            NAT_DBG (NAT_DBG_FTP, "\n PASV reply code met \n");
            u4Status =
                NatPayloadIpPortModify (pBuf, pHeaderInfo, u4ReadOffset,
                                        NAT_FTP_PASV);

        }
    }

    NAT_TRC (NAT_TRC_ON, "\n Exiting NatCheckPortPasvCmd \n");
    return (u4Status);

}

/***************************************************************************
* Function Name    :  NatProcessFtp
* Description    :  This function processes the FTP control packet. If payload
*             modification is requied, then the modification is done
*             and the history field in the dynamic table is updated.
*
* Input (s)    :  1. pBuf - Contains IP and TCP header information along
*            with FTP payload.
*             2. pHeaderInfo - Contains header information of IP and TCP
*
* Output (s)    :  Modified pBuf
* Returns      :  NAT_SUCCESS or NAT_FAILURE
*
****************************************************************************/

PUBLIC UINT4
NatProcessFtp (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo)
{
    UINT4               u4FtpReadOffset = NAT_ZERO;
    UINT4               u4Status = NAT_ZERO;

    NAT_TRC (NAT_TRC_ON, "\n Inside NatProcessFtp \n");

    /* Calculate the start offset of the FTP payload */
    u4FtpReadOffset =
        (UINT4) (pHeaderInfo->u1IpHeadLen + pHeaderInfo->u1TransportHeadLen);

    /*
     * Check for PORT command or PASV reply code (227). If present modify the
     *  payload.
     */

    u4Status = NatCheckPortPasvCmd (pBuf, pHeaderInfo, u4FtpReadOffset);

    NAT_TRC (NAT_TRC_ON, "\n Exiting NatProcessFtp \n");
    return (u4Status);
}
#endif /* _NATFTP_C */
