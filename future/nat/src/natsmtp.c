/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: natsmtp.c,v 1.7 2014/01/25 13:52:26 siva Exp $
 *
 * Description:This file contains callback routines of SMTP ALG.
 *              It also contains functions for parsing
 *             the smtp packets and modifying the smtp payload.
 *
 *******************************************************************/
#include "natinc.h"

/*****************************************************************************
     PROTOTYPES OF SMTP_ALG FUNCTIONS
*****************************************************************************/
PRIVATE INT4        NatChangeSmtpPayload
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo,
           UINT4 u4StartOffset, UINT4 u4EndOffset));
PRIVATE UINT4       NatGetSmtpAddr
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Type, tAddrInfo * pAddrInfo,
           UINT4 u4Offset));

/*****************************************************************************
              SMTP_ALG FUNCTIONS
*****************************************************************************/

/********************************************************************************  Function Name    : NatChangeSmtpPayload
*  Description        : This function changes the payload of the HTTP packet.
*             This replaces the Ip address present in the SMTP
*             packet between StartOffset and EndOffset with the
*             Global IP address.
*
*  Input(s)        : pBuf - SMTP packet that has to be changed.
*             u4TranslatedLocIpAddress - This address has to be substituted*             for the IP address present in the SMTP packet.
*             u4StartOffset - Starting offset of the IP address
*             present in the packet.
*             u4EndOffset - End offset of the IP address.
*  Output(s)        : pBuf - Modified SMTP packet.
*  Returns        : Change in size of the packet due to substitution.
*******************************************************************************/
PRIVATE INT4
NatChangeSmtpPayload (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo,
                      UINT4 u4BeginOffset, UINT4 u4EndOffset)
{
    INT1                ai1NewFrmAddr[NAT_IPADDR_LEN_MAX] = { NAT_ZERO };
    INT1                ai1TempNewFrmAddr[NAT_IPADDR_LEN_MAX] = { NAT_ZERO };
    INT1                ai1OldData[NAT_IPADDR_LEN_MAX] = { NAT_ZERO };

    INT4                i4OldLen = NAT_ZERO;
    INT4                i4NewLen = NAT_ZERO;
    UINT2               u2Chksum = NAT_ZERO;
    UINT4               u4Size = NAT_ZERO;
    UINT4               u4CkSumOffset = NAT_ZERO;
    tUtlInAddr          addr = { NAT_ZERO };
    tCRU_BUF_CHAIN_HEADER *pTempBuf = NULL;
    UINT4               u4TranslatedLocIpaddr = NAT_ZERO;
    UINT1               u1IpHeadLen = NAT_ZERO;
    UINT1               u1TcpCksumNeeded = NAT_FAILURE;

    NAT_TRC (NAT_TRC_ON, "\n Inside NatChangeSmtpPayload \n");

    /*
     * converts the IP address from integer to Dotted string format.
     */
    u4TranslatedLocIpaddr = pHeaderInfo->u4InIpAddr;
    u1IpHeadLen = pHeaderInfo->u1IpHeadLen;
    addr.u4Addr = OSIX_HTONL (u4TranslatedLocIpaddr);
    STRNCPY (ai1NewFrmAddr, INET_NTOA (addr), NAT_IPADDR_LEN_MAX - NAT_ONE);
    *(ai1NewFrmAddr + STRLEN (ai1NewFrmAddr)) = '\0';
    u4Size = u4EndOffset - u4BeginOffset + NAT_ONE;
    i4NewLen = (INT4) STRLEN (ai1NewFrmAddr);

    /*
     *  Fragmenting the buffer,  then deleting the start of the second fragment,
     *  then prepending the new buffer to the second fragment, then
     *  concatenating the first and second buffer is equivalent to substitution.
     */
    if ((u4BeginOffset % MODULO_DIVISOR_2) != NAT_ZERO)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) ai1OldData,
                                   u4BeginOffset - NAT_ONE, u4Size + NAT_ONE);
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *)
                                   &ai1TempNewFrmAddr[NAT_ZERO],
                                   u4BeginOffset - NAT_ONE, NAT_ONE_BYTE);
        /*BUFFER_CHANGE starts */
        if (CRU_BUF_Fragment_BufChain (pBuf, u4BeginOffset -
                                       NAT_ONE, &pTempBuf) == CRU_FAILURE)
        {
            return NAT_FAILURE;
        }
        /*BUFFER_CHANGE ends */
        ai1TempNewFrmAddr[NAT_INDEX_1] = '\0';
        SNPRINTF ((CHR1 *) ai1TempNewFrmAddr + STRLEN (ai1TempNewFrmAddr),
                  sizeof (ai1TempNewFrmAddr) - STRLEN (ai1TempNewFrmAddr), "%s",
                  ai1NewFrmAddr);
        STRNCPY (ai1NewFrmAddr, ai1TempNewFrmAddr,
                 sizeof (ai1NewFrmAddr) - NAT_ONE);
    }
    else
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) ai1OldData, u4BeginOffset,
                                   u4Size);
        /*BUFFER_CHANGE starts */
        if (CRU_BUF_Fragment_BufChain (pBuf, u4BeginOffset, &pTempBuf) ==
            CRU_FAILURE)
        {
            return NAT_FAILURE;
        }
        /*BUFFER_CHANGE ends */
    }

    i4OldLen = (INT4) STRNLEN (ai1OldData, NAT_IPADDR_LEN_MAX);
    i4NewLen = (INT4) STRNLEN (ai1NewFrmAddr, NAT_IPADDR_LEN_MAX);

    if (i4NewLen < NAT_IPADDR_LEN_MAX - NAT_ONE)
    {
        NatDataAdjust ((UINT1 *) ai1NewFrmAddr, i4NewLen, i4OldLen);
        i4NewLen = (INT4) STRLEN (ai1NewFrmAddr);
        u4CkSumOffset = NAT_TCP_CKSUM_OFFSET + (UINT4) u1IpHeadLen;
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2Chksum,
                                   u4CkSumOffset, NAT_TWO_BYTES);
        NatChecksumAdjust ((UINT1 *) &u2Chksum, (UINT1 *) ai1OldData, i4OldLen,
                           (UINT1 *) ai1NewFrmAddr, i4NewLen);
    }
    else
    {
        u1TcpCksumNeeded = NAT_SUCCESS;
        u2Chksum = NAT_ZERO;
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2Chksum,
                                   u4CkSumOffset, NAT_TWO_BYTES);
    }
    CRU_BUF_Move_ValidOffset (pTempBuf, (UINT4) i4OldLen);
    CRU_BUF_Prepend_BufChain (pTempBuf, (UINT1 *) ai1NewFrmAddr,
                              (UINT4) i4NewLen);
    CRU_BUF_Concat_MsgBufChains (pBuf, pTempBuf);
    pHeaderInfo->i4Delta = i4NewLen - i4OldLen;
    if (u1TcpCksumNeeded == NAT_SUCCESS)
    {
        u2Chksum = NatTransportCksumAdjust (pBuf, pHeaderInfo);
    }
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2Chksum,
                               u4CkSumOffset, NAT_TWO_BYTES);
    NAT_TRC (NAT_TRC_ON, "\n Exiting NatChangeSmtpPayload \n");
    return NAT_SUCCESS;            /* BUFFER_CHANGE */

}

/********************************************************************************  Function Name    : NatGetSmtpAddr
*  Description        : This function is used to extract the IP address
*             from the SMTP header fields, indicated by u4Type.
*
*  Input(s)        : pBuf - SMTP packet that is scanned for the IP address
*             in the SMTP header filed, indicateed by u4Type.
*             u4Type - This gives the name of the Headerfields.
*             pAddrInfo - Pointer to the AddressInfo structure.
*  Output(s)        : pAddrInfo - This structure is filled with the
*             IP address present in the header field indicated by
*             u4Type. This is valid if return value is NAT_SUCCESS.
*  Returns        : NAT_SUCCESS or NAT_FAILURE
*******************************************************************************/

PRIVATE UINT4
NatGetSmtpAddr (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Type,
                tAddrInfo * pAddrInfo, UINT4 u4Offset)
{
    UINT4               u4Addr = NAT_ZERO;
    UINT4               u4Index = NAT_ZERO;
    UINT4               u4PktSize = NAT_ZERO;
    CHR1                au1AddrStr[NAT_IPADDR_LEN_MAX] = { NAT_ZERO };
    UINT1               u1CurrentByte = NAT_ZERO;
    UINT1               u1NextByte = NAT_ZERO;
    INT1                ai1String[NAT_SMTP_FIELD_LIMIT] = { NAT_ZERO };
    UINT2               u2EndDelimiter = NAT_ZERO;
    UINT4               u4Flag = NAT_ZERO;

    NAT_DBG (NAT_DBG_ENTRY, "\n Inside NatGetSmtpAddr \n");
    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1CurrentByte,
                               u4Offset, NAT_ONE_BYTE);
    u4Offset++;
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1NextByte,
                               u4Offset, NAT_ONE_BYTE);
    u4Offset++;

    /*
     *  Scan till the end of the header (0x0d0a0d0a) and check the header field
     *  given by u4Type contains any IP address if so return that IP address.
     */

    while (u4Offset <= u4PktSize)
    {

        if ((u1CurrentByte == NAT_PAYLOAD_0D) && (u1NextByte == NAT_PAYLOAD_0A))
        {
            if (u4Type == NAT_MAILFROM)
            {
                CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &ai1String,
                                           u4Offset, NAT_NINE_BYTES);
                ai1String[NAT_INDEX_9] = '\0';
                if (STRCMP (ai1String, "Mailfrom:") == NAT_ZERO)
                {
                    u4Flag = NAT_TRUE;
                }
                else
                {
                    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2EndDelimiter,
                                               u4Offset, NAT_TWO_BYTES);
                }
                u4Offset += NAT_NINE;

            }
            if (u4Type == NAT_CC)
            {
                CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &ai1String,
                                           u4Offset, NAT_THREE_BYTES);
                ai1String[NAT_INDEX_3] = '\0';
                if (STRCMP (ai1String, "Cc:") == NAT_ZERO)
                {
                    u4Flag = NAT_TRUE;
                }
                else
                {
                    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2EndDelimiter,
                                               u4Offset, NAT_TWO_BYTES);
                }
                u4Offset += NAT_THREE;
            }
            if (u4Type == NAT_REPLYTO)
            {
                CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &ai1String,
                                           u4Offset, NAT_EIGHT_BYTES);
                ai1String[NAT_INDEX_8] = '\0';
                if (STRCMP (ai1String, "Replyto:") == NAT_ZERO)
                {
                    u4Flag = NAT_TRUE;
                }
                else
                {
                    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2EndDelimiter,
                                               u4Offset, NAT_TWO_BYTES);
                }
                u4Offset += NAT_EIGHT;
            }
            if (u4Type == NAT_FROM)
            {
                CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) ai1String, u4Offset,
                                           NAT_FIVE_BYTES);
                ai1String[NAT_INDEX_5] = '\0';
                if (STRCMP (ai1String, "From:") == NAT_ZERO)
                {
                    u4Flag = NAT_TRUE;
                }
                else
                {
                    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2EndDelimiter,
                                               u4Offset, NAT_TWO_BYTES);
                }
                u4Offset += NAT_FIVE;
            }
            if (u4Type == NAT_RECV)
            {
                CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &ai1String,
                                           u4Offset, NAT_FIVE_BYTES);
                ai1String[NAT_INDEX_5] = '\0';
                if (STRCMP (ai1String, "Recv:") == NAT_ZERO)
                {
                    u4Flag = NAT_TRUE;
                }
                else
                {
                    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2EndDelimiter,
                                               u4Offset, NAT_TWO_BYTES);
                }
                u4Offset += NAT_FIVE;
            }

            /*
             *  If the field indicated by u4Type is there in the packet thenu4Flag
             *  will be set then this flag is checked if it is then that field is
             *  scanned for presence of any IP address.If present,then AddressInfo
             *  structure is filled appropriately.
             */

            if (u4Flag == NAT_TRUE)
            {
                CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1CurrentByte,
                                           u4Offset, NAT_ONE_BYTE);
                u4Offset++;
                while (u4Offset <= u4PktSize)
                {
                    if (u1CurrentByte == '@')
                    {
                        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1NextByte,
                                                   u4Offset, NAT_ONE_BYTE);
                        u4Offset++;
                        /*
                         *  Normally IP address is present between the square braces,         *  that's why
                         *  characters between the square braces are extracted.
                         */
                        if (u1NextByte == '[')
                        {
                            pAddrInfo->u4StartOffset = u4Offset;
                            u4Index = NAT_ZERO;
                            CRU_BUF_Copy_FromBufChain (pBuf,
                                                       (UINT1 *) &u1CurrentByte,
                                                       u4Offset, NAT_ONE_BYTE);
                            u4Offset++;
                            while ((u1CurrentByte != ']') &&
                                   (u4Index < NAT_IPADDR_LEN_MAX))
                            {
                                au1AddrStr[u4Index] = (CHR1) u1CurrentByte;
                                CRU_BUF_Copy_FromBufChain (pBuf,
                                                           (UINT1 *)
                                                           &u1CurrentByte,
                                                           u4Offset,
                                                           NAT_ONE_BYTE);
                                u4Index++;
                                u4Offset++;
                            }
                            pAddrInfo->u4EndOffset = u4Offset - NAT_TWO;
                            CRU_BUF_Copy_FromBufChain (pBuf,
                                                       (UINT1 *) &u1CurrentByte,
                                                       u4Offset, NAT_ONE_BYTE);
                            u4Offset++;
                            CRU_BUF_Copy_FromBufChain (pBuf,
                                                       (UINT1 *) &u1NextByte,
                                                       u4Offset, NAT_ONE_BYTE);
                            u4Offset++;
                        }
                    }
                    else
                    {
                        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1NextByte,
                                                   u4Offset++, NAT_ONE_BYTE);
                    }

                    if ((u1CurrentByte == NAT_PAYLOAD_0D) &&
                        (u1NextByte == NAT_PAYLOAD_0A))
                    {
                        break;
                    }
                    else
                    {
                        u1CurrentByte = u1NextByte;
                        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1NextByte,
                                                   u4Offset, NAT_ONE_BYTE);
                        u4Offset++;
                    }
                }
            }
            else
            {
                if (u2EndDelimiter == NAT_IP_DELIMITER)
                {
                    break;
                }
            }
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2EndDelimiter,
                                       u4Offset, NAT_TWO_BYTES);
            u4Offset += NAT_TWO;
            u2EndDelimiter = OSIX_NTOHS (u2EndDelimiter);
            if ((u1CurrentByte == NAT_PAYLOAD_0D) &&
                (u1NextByte == NAT_PAYLOAD_0A) &&
                (u2EndDelimiter == NAT_IP_DELIMITER))
            {
                break;
            }

        }
        u1CurrentByte = u1NextByte;
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1NextByte, u4Offset,
                                   NAT_ONE_BYTE);
        u4Offset++;

        /*CRU_BUF_Copy_FromBufChain( pBuf,(UINT1 *)&u1CurrentByte, u4Offset, 1 );
           u4Offset++;
           CRU_BUF_Copy_FromBufChain(pBuf, (UINT1 *)&u1NextByte, u4Offset, 1);
           u4Offset++; */
    }
    if (u4Index != NAT_ZERO)
    {
        au1AddrStr[u4Index] = '\0';
        u4Addr = INET_ADDR (au1AddrStr);
        pAddrInfo->u4IpAddr = u4Addr;
        NAT_DBG (NAT_DBG_ENTRY, "\n Exiting NatGetSmtpAddr \n");
        return (NAT_SUCCESS);
    }
    else
    {
        NAT_DBG (NAT_DBG_ENTRY, "\n Exiting NatGetSmtpAddr \n");
        return (NAT_FAILURE);
    }

}

/********************************************************************************  Function Name    : NatProcessSmtp
*  Description        : This function performs the Smtp ALG functionality of
*             NAT. This function searches for IP address present
*             in From, MailFrom and Recv field ofthe Smtp header and
*             translates those IP addresses, if they are present.
*
*  Input(s)        : pBuf - Pointer to the Smtp packet.
*
*             pHeaderInfo - Pointer to the structure containing
*             NAT/NAPT entry details corresponding to the session
*             in which the current packet is exchanged.
*
*  Output(s)        : pBuf - Modified smtp packet.
*  Returns        : NAT_SUCCESS or NAT_FAILURE.
*******************************************************************************/
PUBLIC UINT4
NatProcessSmtp (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo)
{
    UINT4               u4SearchStatus = NAT_FAILURE;
    UINT4               u4Offset = NAT_ZERO;

    tAddrInfo           FromInfo;
    tAddrInfo           MailFromInfo;
    tAddrInfo           RecvInfo;

    FromInfo.u4IpAddr = NAT_ZERO;
    MailFromInfo.u4IpAddr = NAT_ZERO;
    RecvInfo.u4IpAddr = NAT_ZERO;
    /*
     *  Get the Ip address from the MailFrom field if it is present. If so, then
     *  extract the MailFrom address and search the LocalIP address table and
     *  replace that IP address with the corresponding Global IP address present
     *  in the mapping in Local IP address table
     */

    NAT_TRC (NAT_TRC_ON, "\n Inside NatProcessSmtp \n");
    u4Offset =
        (UINT4) pHeaderInfo->u1IpHeadLen +
        (UINT4) pHeaderInfo->u1TransportHeadLen;
    if (pHeaderInfo->u4Direction == NAT_OUTBOUND)
    {
        u4SearchStatus =
            NatGetSmtpAddr (pBuf, NAT_MAILFROM, &MailFromInfo, u4Offset);
        if (u4SearchStatus == NAT_SUCCESS)
        {
            /* BUFFER_CHANGE starts */
            if (NatChangeSmtpPayload (pBuf, pHeaderInfo,
                                      MailFromInfo.u4StartOffset,
                                      MailFromInfo.u4EndOffset) == NAT_FAILURE)
            {
                return NAT_FAILURE;
            }
            /* BUFFER_CHANGE ends */
            NAT_DBG (NAT_DBG_SMTP, "\n Mail From Address Scan Success \n");
        }
        else
        {
            NAT_DBG (NAT_DBG_SMTP, "\n Mail From Address Is not there \n");
        }
    }

    /* similar processing for From field */
    if (pHeaderInfo->u4Direction == NAT_OUTBOUND)
    {
        NatGetSmtpAddr (pBuf, NAT_FROM, &FromInfo, u4Offset);
        if (FromInfo.u4IpAddr != NAT_ZERO)
        {
            /* BUFFER_CHANGE starts */
            if (NatChangeSmtpPayload (pBuf, pHeaderInfo,
                                      FromInfo.u4StartOffset,
                                      FromInfo.u4EndOffset) == NAT_FAILURE)
            {
                return NAT_FAILURE;
            }
            /* BUFFER_CHANGE ends */
        }
    }

    /* similar processing for Recv field */

    if (pHeaderInfo->u4Direction == NAT_INBOUND)
    {
        NatGetSmtpAddr (pBuf, NAT_RECV, &RecvInfo, u4Offset);
        if (RecvInfo.u4IpAddr != NAT_ZERO)
        {
            /* BUFFER_CHANGE starts */
            if (NatChangeSmtpPayload (pBuf, pHeaderInfo,
                                      RecvInfo.u4StartOffset,
                                      RecvInfo.u4EndOffset) == NAT_FAILURE)
            {
                return NAT_FAILURE;
            }
            /* BUFFER_CHANGE ends */
        }
    }

    NAT_TRC (NAT_TRC_ON, "\n Exiting NatProcessSmtp \n");
    return NAT_SUCCESS;
}
