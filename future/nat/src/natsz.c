/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: natsz.c,v 1.9 2013/11/29 11:04:14 siva Exp $
 *
 * Description:This file contains callback routines of SMTP ALG.
 *              It also contains functions for parsing
 *             the smtp packets and modifying the smtp payload.
 *
 *******************************************************************/
#define _NATSZ_C
#include "natinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
NatSizingMemCreateMemPools ()
{
    INT4                i4RetVal = NAT_ZERO;
    INT4                i4SizingId = NAT_ZERO;

    for (i4SizingId = NAT_ZERO; i4SizingId < NAT_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            (INT4) MemCreateMemPool (gaFsNATSizingParams[i4SizingId].
                                     u4StructSize,
                                     gaFsNATSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(gaNATMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            NatSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
NatSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, gaFsNATSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, gaNATMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
NatSizingMemDeleteMemPools ()
{
    INT4                i4SizingId = NAT_ZERO;

    for (i4SizingId = NAT_ZERO; i4SizingId < NAT_MAX_SIZING_ID; i4SizingId++)
    {
        if (gaNATMemPoolIds[i4SizingId] != NAT_ZERO)
        {
            MemDeleteMemPool (gaNATMemPoolIds[i4SizingId]);
            gaNATMemPoolIds[i4SizingId] = NAT_ZERO;
        }
    }
    return;
}
