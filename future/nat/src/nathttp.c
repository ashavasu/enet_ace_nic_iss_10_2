/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: nathttp.c,v 1.9 2014/12/16 10:52:44 siva Exp $
 *
 * Description:This file contains callback routines of HTTP
 *             module. It also contains functions for parsing
 *             the http packets and modifying the http payload.
 *
 *******************************************************************/
#ifndef _NATHTTP_C
#define _NATHTTP_C
#include "natinc.h"

/*****************************************************************************
     PROTOTYPES OF HTTP_ALG FUNCTIONS
*****************************************************************************/
PRIVATE UINT4       NatGetHttpFrmField
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4BeginOffset,
           UINT4 *pu4EndOffset));
PRIVATE UINT4       NatGetHttpHostField
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4BeginOffset,
           UINT4 *pu4EndOffset));
PRIVATE INT4        NatChangeHttpPayload
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo,
           UINT4 u4BeginOffset, UINT4 u4EndOffset));

/*****************************************************************************
             HTTP_ALG FUNCTIONS
*****************************************************************************/
/********************************************************************************  Function Name    : NatGetHttpFrmField
*  Description        : This functions extracts the IP address from the
*                    'from' field of HTTP header, if it is present.
*
*  Input(s)        : pBuf - Pointer to the Http packet.
*  Output(s)        : None
*  Returns        : NULL or FromField IP address.
*******************************************************************************/

UINT4
NatGetHttpFrmField (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4BeginOffset,
                    UINT4 *pu4EndOffset)
{
    CHR1                ai1FrmIPaddr[NAT_IPADDR_LEN_MAX] = { NAT_ZERO };
    INT1                i1ScanChar = NAT_ZERO;
    INT1                i1HeaderEndScanChar = NAT_ZERO;
    INT1                i1NextScanChar = NAT_ZERO;
    INT1                i1HeaderEndNextScanChar = NAT_ZERO;
    UINT4               u4Index = NAT_ZERO;
    UINT4               u4CurrentOffset = NAT_ONE;
    UINT4               u4PktSize = NAT_ZERO;
    UINT4               u4IpAddr = 0;
    INT1                i2ScanChar = NAT_ZERO;



    /*
     * Get byte by byte and check whether we have the "from" field present
     * in the HTTP header, if it is there then that field is checked for
     * the presence of IP address. If IP addr is there then the IP address is
     * extracted and returned
     */

    NAT_DBG (NAT_DBG_ENTRY, "\n Inside NatGetHttpFrmField \n");
    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
    ai1FrmIPaddr[NAT_INDEX_0] = NAT_ZERO;
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &i1ScanChar, u4CurrentOffset,
                               NAT_ONE_BYTE);
    u4CurrentOffset++;
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &i1NextScanChar,
                               u4CurrentOffset, NAT_ONE_BYTE);
    u4CurrentOffset++;
    while (u4CurrentOffset <= u4PktSize)
    {

        /* 
         * check for the end of 'any' fields present in http header. If it is
         * the end of any field then the next few four bytes are scanned for
         * 'from' field.
         */

        if ((i1ScanChar == NAT_PAYLOAD_0D)
            && (i1NextScanChar == NAT_PAYLOAD_0A))
        {
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &i1ScanChar,
                                       u4CurrentOffset, NAT_ONE_BYTE);
            u4CurrentOffset++;
            if ((i1ScanChar == 'F') || (i1ScanChar == NAT_PAYLOAD_0D))
            {
                CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &i1NextScanChar,
                                           u4CurrentOffset, NAT_ONE_BYTE);
                u4CurrentOffset++;
                if ((i1NextScanChar == 'r') ||
                    (i1NextScanChar == NAT_PAYLOAD_0A))
                {
                    if ((i1ScanChar == NAT_PAYLOAD_0D)
                        && (i1NextScanChar == NAT_PAYLOAD_0A))
                    {
                        break;
                    }
                    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &i1ScanChar,
                                               u4CurrentOffset, NAT_ONE_BYTE);
                    u4CurrentOffset++;
                    if (i1ScanChar == 'o')
                    {
                        CRU_BUF_Copy_FromBufChain (pBuf,
                                                   (UINT1 *) &i1NextScanChar,
                                                   u4CurrentOffset,
                                                   NAT_ONE_BYTE);
                        u4CurrentOffset++;
                        if (i1NextScanChar == 'm')
                        {
                            CRU_BUF_Copy_FromBufChain (pBuf,
                                                       (UINT1 *) &i1ScanChar,
                                                       u4CurrentOffset,
                                                       NAT_ONE_BYTE);
                            u4CurrentOffset++;
                            if (i1ScanChar == ':')
                            {
                                CRU_BUF_Copy_FromBufChain (pBuf,
                                                           (UINT1 *)
                                                           &i1ScanChar,
                                                           u4CurrentOffset,
                                                           NAT_ONE_BYTE);
                                u4CurrentOffset++;
                                while ((i1ScanChar != '@') && (i1ScanChar != ' ') && (u4CurrentOffset <= u4PktSize))
                                {
                                    CRU_BUF_Copy_FromBufChain (pBuf,
                                                               (UINT1 *)
                                                               &i1ScanChar,
                                                               u4CurrentOffset,
                                                               NAT_ONE_BYTE);
                                    u4CurrentOffset++;
                                }
                                CRU_BUF_Copy_FromBufChain (pBuf,
                                                           (UINT1 *)
                                                           &i1NextScanChar,
                                                           u4CurrentOffset,
                                                           NAT_ONE_BYTE);
                                u4CurrentOffset++;
                                if ((i1ScanChar == '@') && (i1NextScanChar
                                                            == '['))
                                {

                                    /* fill up the string until ']' character appears in the string;
                                     * Normally IP address is embedded between the square brackets, so
                                     * those characters are read from the packet and filled up in 
                                     * ai1FrmIPaddr. BeginOffset and EndOffset of the IP address present
                                     * in the packets are recorded in the global variables
                                     */

                                    CRU_BUF_Copy_FromBufChain (pBuf,
                                                               (UINT1 *)
                                                               &i1ScanChar,
                                                               u4CurrentOffset,
                                                               NAT_ONE_BYTE);
                                    *pu4BeginOffset = u4CurrentOffset;
                                    u4CurrentOffset++;
                                    while ((i1ScanChar != ']') && (u4Index <= (NAT_IPADDR_LEN_MAX-1)) &&
                                           (u4CurrentOffset <= u4PktSize))
                                    {
                                        ai1FrmIPaddr[u4Index] = i1ScanChar;
                                        u4Index++;
                                        CRU_BUF_Copy_FromBufChain
                                            (pBuf,
                                             (UINT1 *)
                                             &i1ScanChar,
                                             u4CurrentOffset, NAT_ONE_BYTE);
                                        u4CurrentOffset++;
                                    }
                                    *pu4EndOffset = (u4CurrentOffset - NAT_TWO);
                                    ai1FrmIPaddr[u4Index] = '\0';
                                    break;
                                }
                                else if (i1ScanChar == ' ')
                                {
                                    *pu4BeginOffset = u4CurrentOffset-1;
                                    do
                                    {

                                        if ((u4Index <= (NAT_IPADDR_LEN_MAX-1)) && (u4CurrentOffset <= u4PktSize))
                                        {
                                            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &i1ScanChar, 
                                                                       u4CurrentOffset-1, NAT_ONE_BYTE);

                                             if ((i1ScanChar) == NAT_PAYLOAD_0D)
                                               {
                                                 CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &i2ScanChar,
                                                                            u4CurrentOffset, NAT_ONE_BYTE);
                                                 if ((i2ScanChar) == NAT_PAYLOAD_0A)
                                                  {
                                                    break;
                                                  }
                                                  else
                                                  {
                                                   u4CurrentOffset--;
                                                  }
                                                }
                                            ai1FrmIPaddr[u4Index++] = i1ScanChar;
                                            i1NextScanChar = i1ScanChar;
                                            u4CurrentOffset++;
                                            }
                                        else
                                        {
                                            break;
                                        }

                                    }while ((i1ScanChar != NAT_PAYLOAD_0A) || (i1ScanChar != NAT_PAYLOAD_0D));
                                    

                                      *pu4EndOffset = (u4CurrentOffset - NAT_TWO);
                                       ai1FrmIPaddr[u4Index] = '\0';
                                       break;
                                }
                            }
                        }
                    }
                }
            }

            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &i1HeaderEndScanChar,
                                       u4CurrentOffset, NAT_ONE_BYTE);
            u4CurrentOffset++;
            CRU_BUF_Copy_FromBufChain (pBuf,
                                       (UINT1 *) &i1HeaderEndNextScanChar,
                                       u4CurrentOffset, NAT_ONE_BYTE);
            u4CurrentOffset++;

            /* HTTP headers are terminated by 0d0a0d0a, that's why next two
             * characters are scanned for 0d0a because previous two characters
             * are 0d0a. if so, come out of the loop
             */

            if ((i1HeaderEndScanChar == NAT_PAYLOAD_0D)
                && (i1HeaderEndNextScanChar == NAT_PAYLOAD_0A))
            {
                break;
            }
            else
            {
                i1ScanChar = i1HeaderEndScanChar;
                i1NextScanChar = i1HeaderEndNextScanChar;
            }
        }
        else
        {
            i1ScanChar = i1NextScanChar;
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &i1NextScanChar,
                                       u4CurrentOffset, NAT_ONE_BYTE);
            u4CurrentOffset++;
        }
    }
    
    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatGetHttpFrmField \n");

   
    u4IpAddr = OSIX_NTOHL (INET_ADDR (ai1FrmIPaddr));

                                           

    if (u4IpAddr == 0xffffffff)
    {
        NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatGetHttpFrmField \n");
        return NAT_ZERO;
    }
    else
    {
        NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatGetHttpFrmField \n");
        return (OSIX_NTOHL (INET_ADDR (ai1FrmIPaddr)));
    }                            /* This function converts the IP
                                 * address from dotted notations
                                 * to binary data in network
                                 * byte order.
                                 */

}

/********************************************************************************  Function Name    : NatGetHttpHostField
*  Description        : This functions extracts the IP address from the
*             'Host' field of HTTP header, if it is present.
*
*  Input(s)        : pBuf - Pointer to the Http packet.
*  Output(s)        : None
*  Returns        : NULL or HostField IP address.
*******************************************************************************/

UINT4
NatGetHttpHostField (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4BeginOffset,
                     UINT4 *pu4EndOffset)
{
    CHR1                ai1HostIPaddr[NAT_IPADDR_LEN_MAX] = { NAT_ZERO };
    INT1                i1ScanChar = NAT_ZERO;
    INT1                i1HeaderEndScanChar = NAT_ZERO;
    INT1                i1NextScanChar = NAT_ZERO;
    INT1                i1HeaderEndNextScanChar = NAT_ZERO;
    UINT4               u4Index = NAT_ZERO;
    UINT4               u4CurrentOffset = NAT_ONE;
    UINT4               u4PktSize = NAT_ZERO;

    /* Get byte by byte and check whether we have the "Host" filed present
     * in the HTTP header, if it is there then that field is checked for
     * the presence of IP address, if it is there then that IP address is
     * extracted and returned.
     */

    NAT_DBG (NAT_DBG_ENTRY, "\n Inside NatGetHttpHostField \n");
    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
    ai1HostIPaddr[NAT_INDEX_0] = NAT_ZERO;
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &i1ScanChar, u4CurrentOffset,
                               NAT_ONE_BYTE);
    u4CurrentOffset++;
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &i1NextScanChar,
                               u4CurrentOffset, NAT_ONE_BYTE);
    u4CurrentOffset++;
    while (u4CurrentOffset <= u4PktSize)
    {

        /* check for the end of any fields present in http header. If it is
         * the end of any field then the next few four bytes are scanned for
         * 'host' field.
         */

        if ((i1ScanChar == NAT_PAYLOAD_0D)
            && (i1NextScanChar == NAT_PAYLOAD_0A))
        {
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &i1ScanChar,
                                       u4CurrentOffset, NAT_ONE_BYTE);
            u4CurrentOffset++;
            if ((i1ScanChar == 'H') || (i1ScanChar == NAT_PAYLOAD_0D))
            {
                CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &i1NextScanChar,
                                           u4CurrentOffset, NAT_ONE_BYTE);
                u4CurrentOffset++;
                if ((i1NextScanChar == 'o')
                    || (i1NextScanChar == NAT_PAYLOAD_0A))
                {
                    if ((i1ScanChar == NAT_PAYLOAD_0D)
                        && (i1NextScanChar == NAT_PAYLOAD_0A))
                    {
                        break;
                    }
                    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &i1ScanChar,
                                               u4CurrentOffset, NAT_ONE_BYTE);
                    u4CurrentOffset++;
                    if (i1ScanChar == 's')
                    {
                        CRU_BUF_Copy_FromBufChain (pBuf,
                                                   (UINT1 *) &i1NextScanChar,
                                                   u4CurrentOffset,
                                                   NAT_ONE_BYTE);
                        u4CurrentOffset++;
                        if (i1NextScanChar == 't')
                        {
                            CRU_BUF_Copy_FromBufChain (pBuf,
                                                       (UINT1 *) &i1ScanChar,
                                                       u4CurrentOffset,
                                                       NAT_ONE_BYTE);
                            if (i1ScanChar == ':')
                            {
                                CRU_BUF_Copy_FromBufChain (pBuf,
                                                           (UINT1 *)
                                                           &i1ScanChar,
                                                           u4CurrentOffset,
                                                           NAT_ONE_BYTE);
                                u4CurrentOffset += NAT_TWO;    /* This is to bypass the space
                                                             * following the ':'
                                                             */

                                CRU_BUF_Copy_FromBufChain (pBuf,
                                                           (UINT1 *)
                                                           &i1ScanChar,
                                                           u4CurrentOffset,
                                                           NAT_ONE_BYTE);
                                *pu4BeginOffset = u4CurrentOffset;
                                u4CurrentOffset++;
                                CRU_BUF_Copy_FromBufChain (pBuf,
                                                           (UINT1 *)
                                                           &i1NextScanChar,
                                                           u4CurrentOffset,
                                                           NAT_ONE_BYTE);
                                u4CurrentOffset++;

                                while (u4CurrentOffset <= u4PktSize)
                                {

                                    if ((i1ScanChar ==
                                         NAT_PAYLOAD_0D /*0x0d */ )
                                        && (i1NextScanChar ==
                                            NAT_PAYLOAD_0A /*0x0a */ ))
                                    {
                                        *pu4EndOffset
                                            = u4CurrentOffset - NAT_THREE;
                                        /* '\0' is used to find out the improper IP
                                           address at the end of this function */
                                        ai1HostIPaddr[u4Index] = '\0';
                                        break;
                                    }
                                    else
                                    {
                                        /* 'a' is used to find out the improper IP
                                         *  address at the end of this function.
                                         */
                                        if ((i1ScanChar > ASCII_HEX_39))
                                        {
                                            ai1HostIPaddr[u4Index] = 'a';
                                            break;
                                        }
                                        if (i1ScanChar < ASCII_HEX_30)
                                        {
                                            if (i1ScanChar != '.')
                                            {
                                                ai1HostIPaddr[u4Index] = 'a';
                                                break;
                                            }
                                        }
                                        ai1HostIPaddr[u4Index++] = i1ScanChar;
                                        i1ScanChar = i1NextScanChar;
                                        CRU_BUF_Copy_FromBufChain
                                            (pBuf,
                                             (UINT1 *)
                                             &i1NextScanChar,
                                             u4CurrentOffset, NAT_ONE_BYTE);
                                        u4CurrentOffset++;
                                        if ((i1ScanChar == NAT_PAYLOAD_0A)
                                            &&
                                            (i1NextScanChar == NAT_PAYLOAD_0A))
                                        {
                                            ai1HostIPaddr[u4Index] = '\0';
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            }

            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &i1HeaderEndScanChar,
                                       u4CurrentOffset, NAT_ONE_BYTE);
            u4CurrentOffset++;
            CRU_BUF_Copy_FromBufChain (pBuf,
                                       (UINT1 *) &i1HeaderEndNextScanChar,
                                       u4CurrentOffset, NAT_ONE_BYTE);
            u4CurrentOffset++;
            if ((i1HeaderEndScanChar == NAT_PAYLOAD_0D)
                && (i1HeaderEndNextScanChar == NAT_PAYLOAD_0A /*0x0a */ ))
            {
                break;
            }
            else
            {
                i1ScanChar = i1HeaderEndScanChar;
                i1NextScanChar = i1HeaderEndNextScanChar;
            }
        }
        else
        {
            i1ScanChar = i1NextScanChar;
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &i1NextScanChar,
                                       u4CurrentOffset, NAT_ONE_BYTE);
            u4CurrentOffset++;
        }

    }
    if ((ai1HostIPaddr[NAT_INDEX_0] == NAT_ZERO)
        || (ai1HostIPaddr[u4Index] == 'a'))
    {
        NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatGetHttpHostField \n");
        return NAT_ZERO;
    }
    else
    {
        NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatGetHttpHostField \n");
        return (OSIX_NTOHL (INET_ADDR (ai1HostIPaddr)));
    }

}

/********************************************************************************  Function Name    : NatChangeHttpPayload
*  Description        : This function changes the payload of the HTTP packet.
*             Begin and End offset of the IP address that has to
*             be changed is taken from the global variable values
*             and the IP address that is there in that offset is
*             replaced with the IP address passed as a parameter
*             to this function.
*
*  Input(s)        : pBuf - Pointer to the Http packet.
*             u4GlobalIPAddr - This is the IP address to be inserted
*             in the HTTP packet in place of the local IP address
*             present in the packet.
*  Output(s)        : pBuf - Modified Http packet.
*  Returns        : i4Delta - change in size of the Http packet due to
*             the IP address substitution.
*******************************************************************************/

PRIVATE INT4
NatChangeHttpPayload (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo,
                      UINT4 u4BeginOffset, UINT4 u4EndOffset)
{
    tUtlInAddr          addr;
    INT1                ai1NewFrmAddr[NAT_IPADDR_LEN_MAX + NAT_ONE] =
        { NAT_ZERO };
    INT1                ai1NewFrmAddr1[NAT_IPADDR_LEN_MAX] = { NAT_ZERO };
    INT1                ai1TempNewFrmAddr[NAT_IPADDR_LEN_MAX + NAT_ONE]
        = { NAT_ZERO };
    INT1                ai1OldData[NAT_IPADDR_LEN_MAX] = { NAT_ZERO };
    INT4                i4Delta = NAT_ZERO;
    INT4                i4OldLen = NAT_ZERO;
    INT4                i4NewLen = NAT_ZERO;
    UINT2               u2Chksum = NAT_ZERO;
    UINT4               u4Size = NAT_ZERO;
    UINT4               u4CkSumOffset = NAT_ZERO;
    UINT4               u4GlobalIPAddr = NAT_ZERO;
    UINT1               u1IpHeadLen = NAT_ZERO;
    UINT1               u1TcpCksumNeeded = NAT_ZERO;
    tCRU_BUF_CHAIN_HEADER *pTempBuf = NULL;

    i4Delta = NAT_ZERO;
    u4CkSumOffset = NAT_ZERO;
    u4GlobalIPAddr = pHeaderInfo->u4InIpAddr;
    u1IpHeadLen = pHeaderInfo->u1IpHeadLen;
    u1TcpCksumNeeded = NAT_FAILURE;
    /*
     * u4GlobalIPAddr is in network byte order, this is converted to dotted
     * notation form and inserted in the packet at FromFieldBeginOffset.
     */

    NAT_TRC (NAT_TRC_ON, "\n Inside NatChangeHttpPayload \n");
    addr.u4Addr = OSIX_HTONL (u4GlobalIPAddr);
    STRNCPY (ai1NewFrmAddr, INET_NTOA (addr), NAT_IPADDR_LEN_MAX - NAT_ONE);
    *(ai1NewFrmAddr + STRLEN (ai1NewFrmAddr)) = '\0';
    u4Size = u4EndOffset - u4BeginOffset + NAT_ONE;
    i4NewLen = (INT4) STRLEN (ai1NewFrmAddr);
    STRNCPY (ai1NewFrmAddr1, ai1NewFrmAddr, sizeof (ai1NewFrmAddr1) - NAT_ONE);

    if ((u4BeginOffset % MODULO_DIVISOR_2) != NAT_ZERO)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) ai1OldData,
                                   u4BeginOffset - NAT_ONE, u4Size + NAT_ONE);
        ai1OldData[u4Size + NAT_ONE] = '\0';
        CRU_BUF_Copy_FromBufChain (pBuf,
                                   (UINT1 *) &ai1TempNewFrmAddr[NAT_INDEX_0],
                                   u4BeginOffset - NAT_ONE, NAT_ONE_BYTE);
        ai1TempNewFrmAddr[NAT_INDEX_1] = '\0';
        STRNCAT (ai1TempNewFrmAddr, ai1NewFrmAddr,
                 (NAT_IPADDR_LEN_MAX - STRLEN (ai1TempNewFrmAddr) - NAT_ONE));
        STRNCPY (ai1NewFrmAddr, ai1TempNewFrmAddr,
                 sizeof (ai1NewFrmAddr) - NAT_ONE);
        /* BUFFER_CHANGE starts */
        if (CRU_BUF_Fragment_BufChain (pBuf,
                                       u4BeginOffset - NAT_ONE,
                                       &pTempBuf) == CRU_FAILURE)
        {
            return NAT_FAILURE;
        }
        /* BUFFER_CHANGE ends */
        CRU_BUF_Move_ValidOffset (pTempBuf, u4Size + NAT_ONE);
    }
    else
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) ai1OldData, u4BeginOffset,
                                   u4Size);
        ai1OldData[u4Size] = '\0';
        /* BUFFER_CHANGE starts */
        if (CRU_BUF_Fragment_BufChain (pBuf, u4BeginOffset, &pTempBuf)
            == CRU_FAILURE)
        {
            return NAT_FAILURE;
        }
        /* BUFFER_CHANGE ends */
        CRU_BUF_Move_ValidOffset (pTempBuf, u4Size);
    }
    i4OldLen = (INT4) STRNLEN (ai1OldData, NAT_IPADDR_LEN_MAX);
    i4NewLen = (INT4) STRNLEN (ai1NewFrmAddr, (NAT_IPADDR_LEN_MAX + NAT_ONE));
    u4CkSumOffset = NAT_TCP_CKSUM_OFFSET + (UINT4) u1IpHeadLen;
    if (i4NewLen < NAT_IPADDR_LEN_MAX - NAT_ONE)
    {
        NatDataAdjust ((UINT1 *) ai1NewFrmAddr, i4NewLen, i4OldLen);
        i4NewLen = (INT4) STRLEN (ai1NewFrmAddr);
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2Chksum, u4CkSumOffset,
                                   NAT_TWO_BYTES);

        NatChecksumAdjust ((UINT1 *) &u2Chksum, (UINT1 *) ai1OldData, i4OldLen,
                           (UINT1 *) ai1NewFrmAddr, i4NewLen);
    }
    else
    {
        u1TcpCksumNeeded = NAT_SUCCESS;
        u2Chksum = NAT_ZERO;
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2Chksum, u4CkSumOffset,
                                   NAT_TWO_BYTES);
    }
    CRU_BUF_Prepend_BufChain (pTempBuf, (UINT1 *) ai1NewFrmAddr,
                              STRNLEN (ai1NewFrmAddr,
                                       (NAT_IPADDR_LEN_MAX + NAT_ONE)));
    CRU_BUF_Concat_MsgBufChains (pBuf, pTempBuf);
    pHeaderInfo->i4Delta += i4NewLen - i4OldLen;
    if (u1TcpCksumNeeded == NAT_SUCCESS)
    {
        u2Chksum = NatTransportCksumAdjust (pBuf, pHeaderInfo);
    }
    /*if (i4Delta != NAT_ZERO)
       {
       return NAT_FAILURE;
       } */
    UNUSED_PARAM (i4Delta);
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2Chksum, u4CkSumOffset,
                               NAT_TWO_BYTES);
    NAT_TRC (NAT_TRC_ON, "\n Exiting NatChangeHttpPayload \n");
    return NAT_SUCCESS;

}

/********************************************************************************  Function Name    : NatProcessHttp
*  Description        : This function performs the Http ALG functionality of
*             NAT. This function searches for IP address present
*             in From and Host field of the Http header and
*             translates those IP addresses, if they are present.
*  Input(s)        : pBuf - Pointer to the Http packet.
*             pHeaderInfo - Pointer to the structure containing
*             NAT/NAPT entry details corresponding to the session
*             in which the current packet is exchanged.
*  Output(s)        : pBuf - Modified Http packet.
*  Returns        : NAT_SUCCESS always
*******************************************************************************/

PUBLIC UINT4
NatProcessHttp (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo)
{
    UINT4               u4HostIPAddr = NAT_ZERO;
    UINT4               u4GlobalIPAddr = NAT_ZERO;
    UINT4               u4LocalIPAddr = NAT_ZERO;
    UINT4               u4BeginOffset = NAT_ZERO;
    UINT4               u4EndOffset = NAT_ZERO;
    UINT4               u4FromIPAddr = NAT_ZERO;

    /*  This portion of the code extracts the IP address present in the from
     *  field of the HTTP header, if it is present. It then checks whether that
     *  IP address is Local IP address table, if IP is there then it searches
     *  the static table using that 'from field' IP address and gets the
     *  corresponding GLobal IP address mapping and changes the From field IP
     *  address in the header with the global IP address obtained from the
     *  static table .
     */

    NAT_TRC (NAT_TRC_ON, "\n Inside NatProcessHttp \n");

    if (pHeaderInfo->u4Direction == NAT_OUTBOUND)
    {
        u4FromIPAddr = NatGetHttpFrmField (pBuf, &u4BeginOffset, &u4EndOffset);
        NAT_TRC1 (NAT_TRC_ON, "\nFrom Field IP address is %x", u4FromIPAddr);
        if (u4FromIPAddr != NAT_ZERO)
        {
            u4GlobalIPAddr = pHeaderInfo->u4InIpAddr;
            NAT_DBG1 (NAT_DBG_HTTP, "\n GlobalIP address is %x",
                      u4GlobalIPAddr);
            NAT_TRC1 (NAT_TRC_ON, "\n GlobalIP address is %x", u4GlobalIPAddr);
            if (u4GlobalIPAddr != NAT_ZERO)
            {
                /* BUFFER_CHANGE starts */
                if (NatChangeHttpPayload (pBuf, pHeaderInfo,
                                          u4BeginOffset, u4EndOffset)
                    == NAT_FAILURE)
                {
                    return NAT_FAILURE;
                }
                /* BUFFER_CHANGE ends */
            }
        }
    }

    /*  This portion of the code checks for any IP address in Host field of
     *  the HTTP header, in the inbound packets. If it is there , we will
     *  check whether there is any static mapping existing for that global ip
     *  address in our static table. if so, then the corresponding local IP
     *  address is extracted from the static table and the host field IP
     *  address is changed.
     */

    if (pHeaderInfo->u4Direction == NAT_INBOUND)
    {
        u4HostIPAddr = NatGetHttpHostField (pBuf, &u4BeginOffset, &u4EndOffset);
        NAT_TRC1 (NAT_TRC_ON, "\nHost Field IP address is %x", u4HostIPAddr);
        if (u4HostIPAddr != NAT_ZERO)
        {
            u4LocalIPAddr = pHeaderInfo->u4InIpAddr;
            NAT_DBG1 (NAT_DBG_HTTP, "\n LocalIP address is %x", u4LocalIPAddr);
            NAT_TRC1 (NAT_TRC_ON, "\n LocalIP address is %x", u4LocalIPAddr);
            if (u4LocalIPAddr != NAT_ZERO)
            {
                /* BUFFER_CHANGE starts */
                if (NatChangeHttpPayload (pBuf, pHeaderInfo, u4BeginOffset,
                                          u4EndOffset) == NAT_FAILURE)
                {
                    return NAT_FAILURE;
                }
                /* BUFFER_CHANGE ends */
            }
        }
    }

    NAT_TRC (NAT_TRC_ON, "\n Exiting NatProcessHttp \n");
    return NAT_SUCCESS;
}
#endif /* _NATHTTP_C */
