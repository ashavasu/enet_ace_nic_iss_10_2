/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: natmain.c,v 1.29 2015/07/16 10:50:36 siva Exp $
 *
 * Description:This file contains main NAT module functions
 *         and other functions needed for simple NAT translation
 *
 *******************************************************************/
#ifndef _NATMAIN_C_
#define _NATMAIN_C_
#include "sipalg_inc.h"
#include "natinc.h"
#include "natglobal.h"
#include "fsnatlw.h"
UINT4               gu4NatTrc = NAT_ZERO;
UINT4               gu4NatTestTrc = NAT_ZERO;
tInterfaceInfo     *gapNatIfTable[NAT_MAX_NUM_IF + NAT_ONE];
/* H323 */
tNatAppDefn        *gapNatNegPortApplnList[NAT_MAX_PORT_NEG_APPLN];
tTriggerInfo        gaTrigInfo[NAT_MAX_APP_ALLOWED];
tRsvdTrigInfo       gaRsvdTrigInfo[NAT_MAX_APP_ALLOWED];

UINT4               gu4NatEnable;
UINT4               gu4NatTmrEnable;
tTmrAppTimer        gu4NatAppTimer;
UINT4               gu4NatIdleTimeOut;
UINT4               gu4NatTcpTimeOut;
UINT4               gu4NatUdpTimeOut;
UINT4               gu4NatStatDynamicAllocFailureCount;
UINT4               gu4NatTypicalNumOfEntries;


INT4                gi4NatIKEPortTranslation;
INT4                gi4NatIKETimeOut;
INT4                gi4NatIPSecTimeOut;
INT4                gi4NatIPSecPendingTimeOut;
INT4                gi4NatIPSecMaxRetry;
/* NAT_Enh End */

INT4                gi4NatSipAlgPartialEntryTimer;
PUBLIC INT4         NatSipAlgInitTimer (VOID);

/******************************************************************************
*        FUNCTIONS USED BY THE NAT MODULE
******************************************************************************/

PRIVATE UINT4       NatTranslatePkt
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Direction, UINT4 u4IfNum));
PRIVATE UINT4       NatProtocolHandler
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo));
PRIVATE UINT4       NatCheckInbound
ARG_LIST ((UINT4 u4IfNum, tCRU_BUF_CHAIN_HEADER * pBuf));
PRIVATE UINT4       NatCheckOutbound
ARG_LIST ((UINT4 u4IfNum, tCRU_BUF_CHAIN_HEADER * pBuf));
PRIVATE UINT4 NatCheckForIpMulticast ARG_LIST ((tHeaderInfo * pHeaderInfo));
PRIVATE UINT4       NatCheckIfFragmentedPkt
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pu1FirstFrag));

/***************************************************************************
* Function Name    :  NatGetIpHeaderLength                 *
* Description    :  This function gets the IP header length from the    *
*             IP header in the packet.                *
*                                      *
* Input (s)    :  pBuf - This stores the full IP packet along      *
*            with TCP/UDP header and payload.          *
*                                      *
* Output (s)    :  None                         *
* Returns      :  Length of the IP header.                *
*                                      *
****************************************************************************/

/*
 * This function gets the IP header length by moving the offset to the
 * corresponding value i.e. starting of the header. One byte is extracted
 * and the 4-bit header length field is obtained by masking the byte extracted
 * with 0x0f and returned.
 */

PUBLIC UINT4
NatGetIpHeaderLength (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               u1IpHeaderLen = NAT_ZERO;

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1IpHeaderLen,
                               NAT_IP_LEN_OFFSET, NAT_HEADER_LEN);
    u1IpHeaderLen = u1IpHeaderLen & NAT_HEADER_LEN_MASK;

    return (((UINT4) u1IpHeaderLen) * NAT_FOUR_BYTES);
}

/***************************************************************************
* Function Name    :  NatGetSrcIpAddr                    *
* Description    :  This function gets the source IP address from the    *
*             IP header in the packet.                *
*                                      *
* Input (s)    :  pBuf - This stores the full IP packet along      *
*            with TCP/UDP header and payload.          *
*                                      *
* Output (s)    :  None                         *
* Returns      :  The IP address of the source host who has sent this  *
*             packet.                        *
*                                      *
****************************************************************************/

/*
 * This function is used for extracting the source IP addressin the IP header
 * and returning the same.  The offset at which the byte should be extracted
 * is given by NAT_SRC_IP_OFFSET which is equal to 12.
 */

PUBLIC UINT4
NatGetSrcIpAddr (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT4               u4SrcIpAddr = NAT_ZERO;

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u4SrcIpAddr, NAT_SRC_IP_OFFSET,
                               NAT_IP_ADDR_LEN);
    return (OSIX_NTOHL (u4SrcIpAddr));
}

/***************************************************************************
* Function Name    :  NatGetDestIpAddr                    *
* Description    :  This function gets the destination IP address from   *
*             the IP header in the packet.             *
*                                      *
* Input (s)    :  pBuf - This stores the full IP packet along      *
*            with TCP/UDP header and payload.          *
*                                      *
* Output (s)    :  None                         *
* Returns      :  The IP address of the host to which the packet is    *
*             being sent.                      *
*                                      *
****************************************************************************/

/*
 * This function is used for extracting the destinaion IP addressin the IP
 * header and returning the same.  The offset at which the byte should be
 * extracted is given by NAT_DEST_IP_OFFSET which is equal to 16.
 */
PUBLIC UINT4
NatGetDestIpAddr (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT4               u4DestIpAddr = NAT_ZERO;

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u4DestIpAddr,
                               NAT_DEST_IP_OFFSET, NAT_IP_ADDR_LEN);
    return (OSIX_NTOHL (u4DestIpAddr));
}

/***************************************************************************
* Function Name    :  NatGetSrcPort                    *
* Description    :  This function gets the source port from the TCP    *
*             header in the packet stored in pBuf.         *
*                                      *
* Input (s)    :  pBuf - This stores the TCP/UDP header information    *
*             along with IP header and payload.            *
*                                      *
* Output (s)    :  None                         *
* Returns      :  Source Port Number                   *
*                                      *
***************************************************************************/

/*
 * This function is used for extracting the source port number in the TCP/UDP
 * header and returning the same.  The offset at which the byte should be
 * extracted is calculated by obtaining the IP header length. As the source
 * port is the first field in the TCP/UDP header, the IP header length is
 * used as the offset.
 */

PUBLIC UINT2
NatGetSrcPort (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT2               u2SrcPort = NAT_ZERO;
    UINT4               u4SrcPortOffset = NAT_ZERO;

    u4SrcPortOffset = NatGetIpHeaderLength (pBuf);
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2SrcPort, u4SrcPortOffset,
                               NAT_PORT_LEN);
    return (OSIX_NTOHS (u2SrcPort));
}

/***************************************************************************
* Function Name    :  NatGetDestPort                    *
* Description    :  This function gets the destination port number from  *
*             IP header in the packet.                *
*                                      *
* Input (s)    :  pBuf - This stores the TCP/UDP header along with    *
*            IP header and payload.                *
*                                      *
* Output (s)    :  None                         *
* Returns      :  The destination port which is being contacted.    *
*                                      *
***************************************************************************/

/*
 * This function is used for extracting the destination port number in the
 * TCP/UDP header and returning the same.  The offset at which the byte should
 * be extracted is calculated by obtaining the IP header length. As the
 * destination port is the second field (2 bytes) in the TCP/UDP header,
 * the IP header length + 2 is used as the offset.
 */

PUBLIC UINT2
NatGetDestPort (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT2               u2DestPort = NAT_ZERO;
    UINT4               u4DestPortOffset = NAT_ZERO;

    u4DestPortOffset = NatGetIpHeaderLength (pBuf) + NAT_TWO;
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2DestPort, u4DestPortOffset,
                               NAT_PORT_LEN);
    return (OSIX_NTOHS (u2DestPort));
}

/***************************************************************************
* Function Name    :  NatGetTransportHeaderLength              *
* Description    :  This function gets the TCP or UDP header length    *
*             from their respective header depending on the packet *
*             type.                        *
*                                      *
* Input (s)    :  1.  pBuf - This stores the TCP/UDP header along    *
*                with IP header and payload.        *
*             2.  u1PktType - Identifies the type of transport    *
*             protocol i.e. whether it is TCP or UDP       *
*                                      *
* Output (s)    :  None                         *
* Returns      :  Length of the TCP/UDP header depending on u1PktType. *
*                                      *
****************************************************************************/

/*
 * This function  obtains the Transport header length from the TCP/UDP header
 * based on the value of u1PktType. u1PktType value can be NAT_TCP, NAT_UDP or
 * NAT_ICMP. The length is extracted from the corresponding headers. As the
 * length field in TCP header is only 4 bits, the byte extracted is anded
 * with 0x0f.
 */

PUBLIC UINT4
NatGetTransportHeaderLength (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1PktType)
{
    UINT4               u4HeaderLenOffset = NAT_ZERO;
    UINT1               u1Length = NAT_ZERO;

    if (NAT_TCP == u1PktType)
    {
        u4HeaderLenOffset = NAT_TCP_LENGTH_OFFSET + NatGetIpHeaderLength (pBuf);
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1Length,
                                   u4HeaderLenOffset, NAT_HEADER_LEN);
        u1Length = (UINT1) ((u1Length >> NAT_FOUR) & NAT_HEADER_LEN_MASK);
        u1Length = (UINT1) (u1Length * NAT_FOUR_BYTES);
    }
    else
    {
        u1Length = NAT_UDP_HEADER_LENGTH;
    }

    return (u1Length);
}

/***************************************************************************
* Function Name    :  NatCheckForIpMulticast                *
* Description    :  This function checks if the packet is a IP Multicast *
*                   packet                        *
*                                      *
* Input (s)    :  pHeaderInfo : Contains information on IP header    *
*                                      *
* Output (s)    :  None                         *
*                                      *
* Returns      :  NAT_TRUE or NAT_FALSE.                *
*                                      *
****************************************************************************/

PRIVATE UINT4
NatCheckForIpMulticast (tHeaderInfo * pHeaderInfo)
{

    if ((pHeaderInfo->u4Direction == NAT_OUTBOUND)
        && (pHeaderInfo->u4OutIpAddr >= NAT_MULTICAST_START_ADDR)
        && (pHeaderInfo->u4OutIpAddr < NAT_MULTICAST_END_ADDR))
    {
        return (NAT_TRUE);
    }
    else
    {
        return (NAT_FALSE);
    }
}

/***************************************************************************
* Function Name    :  NatCheckIfFragmentedPkt                              *
* Description    :  This function checks if the packet is a IP fragmented  *
*                   packet                                                 *
*                                                                          *
* Input (s)    :  1. pBuf - This stores the full IP packet along           *
*                   with TCP/UDP header and payload.                       *
*                                                                          *
* Output (s)    :  *pu1FirstFrag                                           *
*                                                                          *
* Returns       : NAT_TRUE (if fragmented ) or                             *
*                 NAT_FALSE (if not fragmented).                           *
*                                                                          *
****************************************************************************/

PRIVATE UINT4
NatCheckIfFragmentedPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pu1FirstFrag)
{
    UINT4               u4Status = NAT_FALSE;
    UINT2               u2FragFlag = NAT_ZERO;
    UINT2               u2FragOffset = NAT_ZERO;
    UINT2               u2MoreFragBit = NAT_ZERO;

    *pu1FirstFrag = NAT_FALSE;
    NAT_TRC (NAT_TRC_ON, "\n NatCheckIfFragmentedPkt  has been Called");

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2FragFlag,
                               NAT_FRAG_BIT_OFFSET, NAT_WORD_LEN);
    u2FragFlag = OSIX_NTOHS (u2FragFlag);
    u2MoreFragBit = u2FragFlag & NAT_MORE_FRAG_BIT;
    u2FragOffset = u2FragFlag & NAT_FRAG_OFFSET;

    if ((u2MoreFragBit == NAT_MORE_FRAG_BIT) && (u2FragOffset == NAT_ZERO))
    {
        *pu1FirstFrag = NAT_TRUE;
        u4Status = NAT_TRUE;
    }
    else if ((u2MoreFragBit == NAT_MORE_FRAG_BIT)
             || ((u2MoreFragBit == NAT_ZERO) && (u2FragOffset != NAT_ZERO)))
    {
        u4Status = NAT_TRUE;
    }
    return (u4Status);
}

/***************************************************************************
* Function Name:  NatTranslateInBoundPkt                 *
* Description  :  This function translates the Inbound packet if Global 
*                 Nat is enabled and if the packet needs translation.     
*                                     
* Input (s)    :  1. pBuf - This stores the full IP packet along    *
*                 with TCP/UDP header and payload.          
*                 2. u4InIf - This gives the incoming interface number *
*              
* Output (s)   :  Either
*                  modified IP Packet with all references to Global IP  *
*                  and port translated to local IP and port 
*                 Or 
*                  unmodified IP Packet which is to be just forwarded.
*                 and  Variable pi1FrgReass  showing if the fragments 
*                 are reassembled or not, when the fragments are received.
*
* Returns      :  SUCCESS if the packet is to be forwarded to the Inside
*                 Network irrespctive of whether the packet "needs 
*                 translation" or not.
*                 FAILURE if the packet is to be dropped by the calling 
*                 module.This happens when the packet needs to be 
*                 translated but NAT is unable to do it. 
*              
****************************************************************************/

PUBLIC INT4
NatTranslateInBoundPkt (tCRU_BUF_CHAIN_HEADER ** ppBuf, UINT4 u4InIf,
                        INT1 *pi1FrgReass)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4Direction = NAT_ZERO;
    INT4                i4NatStatus = SUCCESS;
    UINT4               u4Status = NAT_FAILURE;
    UINT1               u1PktType = NAT_ZERO;
    UINT2               u2OutPort = NAT_ZERO;
    UINT4               u4DestIp = NAT_ZERO;
    UINT4               u4LocIp = NAT_ZERO;
    UINT4               u4SrcIp = NAT_ZERO;
    UINT4               u4TransIp = NAT_ZERO;
    UINT2               u2SrcPort = NAT_ZERO;
    UINT2               u2DestPort = NAT_ZERO;
    UINT2               u2InPort = NAT_ZERO;
    UINT1               u1NatIfStatus = NAT_ZERO;
    UINT1               u1FirstFrag = NAT_ZERO;

    pBuf = *ppBuf;
    /* Checks for inbound or outbound direction */
    if ((gu4NatEnable == NAT_DISABLE) ||
        ((u1NatIfStatus =
          (UINT1) (NatCheckIfNatEnable (u4InIf))) == NAT_DISABLE))
    {
        return SUCCESS;
    }
    u4Status = NatCheckIfFragmentedPkt (pBuf, &u1FirstFrag);

    /* check for 1st Fragment is not required as all the fragments are 
       defragmented and given to the NAT as a non-fragmented packet */
    if (u4Status == NAT_TRUE)
    {
        *pi1FrgReass = FAILURE;
        if ((*ppBuf = NatFragReassemble (pBuf, u4InIf, NAT_INBOUND)) == NULL)
        {
            *ppBuf = pBuf;
            return FAILURE;
        }
        pBuf = *ppBuf;
        *pi1FrgReass = SUCCESS;
    }
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1PktType,
                               NAT_PROT_OFFSET_IN_IPHEADER,
                               NAT_IP_PROT_FIELD_LEN);

    /* By passing dns packets for nat disabled interface */
    if ((u1PktType == NAT_TCP) || (u1PktType == NAT_UDP))
    {
        u2DestPort = NatGetDestPort (pBuf);
        u2SrcPort = NatGetSrcPort (pBuf);

        if (((gu4NatEnable == NAT_DISABLE) ||
             (u1NatIfStatus == NAT_DISABLE)) &&
            ((u2DestPort == NAT_DNS_PORT) || (u2SrcPort == NAT_DNS_PORT)))
        {
            return SUCCESS;
        }
    }

    if (u1PktType == NAT_UDP)
    {
        u2OutPort = NatGetSrcPort (pBuf);
        u2InPort = NatGetDestPort (pBuf);
        /* DHCP Client-->Server and DHCP Server-->Client packets will be 
         * processed without any translations */
        if ((u2OutPort == NAT_DHCP_SERV) || (u2OutPort == NAT_DHCP_CLNT))
        {
            NAT_DBG (NAT_DBG_PKT_FLOW, "DHCP Packets Bypassed\n");
            return SUCCESS;
        }
        /* Bypass RIP Packets */
        if ((u2OutPort == NAT_RIP_PORT) && (u2InPort == NAT_RIP_PORT))
        {
            NAT_DBG (NAT_DBG_PKT_FLOW, "RIP control Packets Bypassed\n");
            return SUCCESS;
        }
    }
    else if (u1PktType == NAT_TCP)
    {
        u2OutPort = NatGetDestPort (pBuf);
        u4DestIp = NatGetDestIpAddr (pBuf);
#ifndef LNXIP4_WANTED
        if (SecUtilIpIfIsOurAddress (u4DestIp) == TRUE)
        {
            switch (u2OutPort)
            {
                case NAT_PPTP_TCP_PORT:
                    if (NULL == NatGetStaticNaptEntry (&u4DestIp,
                                                       &u2OutPort,
                                                       NAT_INBOUND,
                                                       u4InIf, u1PktType))
                    {
                        NAT_DBG (NAT_DBG_PKT_FLOW,
                                 "Packet to Server running in CAS, "
                                 "so Bypassed\n");
                        return (SUCCESS);
                    }
                    break;
                default:
                    break;
            }
        }
#endif
    }
    else if (u1PktType == NAT_OSPF_PROT)
    {
        /* ByPass OSPF Packets */
        NAT_DBG (NAT_DBG_PKT_FLOW, "OSPF Packets Bypassed\n");
        return SUCCESS;
    }
    else if (u1PktType == NAT_IGMP_PROT)
    {
        /* ByPass IGMP Packets */
        NAT_DBG (NAT_DBG_PKT_FLOW, "IGMP Packets Bypassed\n");
        return SUCCESS;
    }
    else if (((gu4NatEnable == NAT_DISABLE) ||
              (u1NatIfStatus == NAT_DISABLE)) && (u1PktType != NAT_PPTP))
    {
        /* 
         * ByPass Packets like ICMP when either global or interface nat 
         * status is disabled 
         */
        NAT_DBG (NAT_DBG_PKT_FLOW, "Unknown Packets Bypassed\n");
        return SUCCESS;
    }

    /* ByPass all the multicast packet, if there is no virtual server
     * configured, for the Destination IP and Port.
     * Address is Mulicast, i.e between 224.x.x.x and 239.x.x.x,
     * if the first four bits are 1110 */
    u4DestIp = NatGetDestIpAddr (pBuf);
    if ((u4DestIp & NAT_MULTICAST_END_ADDR /*0xf0000000 */ )
        == NAT_MULTICAST_START_ADDR /*0xe0000000 */ )
    {
        u2OutPort = NatGetDestPort (pBuf);
        if (NULL == NatGetStaticNaptEntry (&u4DestIp, &u2OutPort,
                                           NAT_INBOUND, u4InIf, u1PktType))
        {
            /* ByPass Multicast Packets */
            NAT_DBG (NAT_DBG_PKT_FLOW, "Multicast Data Packets Bypassed\n");
            return SUCCESS;
        }
    }
    /*
     * Check if the Inbound packet needs translation.If the below function
     * returns NAT_SUCCESS then the packet needs to be translated and
     * sent to the Inside Network .Otherwise the packet should be forwarded
     * without transaltion to the Inside Network
     */

    if (u4InIf > (NAT_MAX_NUM_IF))
    {
        return FAILURE;
    }
    u4Status = NatCheckInbound (u4InIf, pBuf);
    if (u4Status == NAT_SUCCESS)
    {
        /*Its a valid Inbound packet which needs to be translated */
        u4Direction = NAT_INBOUND;
        NAT_TRC (NAT_TRC_ON, "\n Packet is an INBOUND packet.");

        /*Its not a fragmented packet so Translate the packet */
        /* Collect the packet information before translating */
        u4LocIp = NatGetDestIpAddr (pBuf);
        u4Status = NatTranslatePkt (pBuf, u4Direction, u4InIf);

        /* Mark that the packet is an inbound packet. */
        if (u4Status == NAT_SUCCESS)
        {
            /* Collect the packet information after translating */
            if ((u1PktType == NAT_TCP) || (u1PktType == NAT_UDP))
            {
                u2SrcPort = NatGetSrcPort (pBuf);
                u2DestPort = NatGetDestPort (pBuf);
            }

            u4TransIp = NatGetDestIpAddr (pBuf);
            if (u4LocIp != u4TransIp)
            {
                u4SrcIp = NatGetSrcIpAddr (pBuf);
                /* Update the tuple information in flow manager */
                FL_NAT_UPDATE_FLOWDATA_NAT_TUPLE (pBuf);
            }

            i4NatStatus = SUCCESS;
            NAT_DBG (NAT_DBG_PKT_FLOW, " \n Inbound packet successfully "
                     "translated \n");
            NAT_TRC (NAT_TRC_ON,
                     "\n Inbound Packet Successfully translated \n");
            gapNatIfTable[u4InIf]->u4NumOfTranslation++;
            NAT_TRC1 (NAT_TRC_ON, "\n Number of Translation %d \n",
                      gapNatIfTable[u4InIf]->u4NumOfTranslation);
        }

        if (u4Status == NAT_FAILURE)
        {
#ifndef LNXIP4_WANTED
            u4DestIp = NatGetDestIpAddr (pBuf);
            if (SecUtilIpIfIsOurAddress (u4DestIp) == TRUE)
            {
                if ((u1PktType == NAT_TCP) || (u1PktType == NAT_UDP))
                {
                    NAT_DBG (NAT_DBG_PKT_FLOW,
                             "Packet to Server running in ISS, "
                             "so Bypassed\n");
                    return (SUCCESS);
                }
            }
#endif
            /* when Nat-Translation fails somewhere. */
            gapNatIfTable[u4InIf]->u4NumOfPktsDropped++;
            i4NatStatus = FAILURE;

            NAT_DBG (NAT_DBG_PKT_FLOW, " \n Inbound Packet Dropped -"
                     " NAT Translations failed\n");
            NAT_TRC1 (NAT_TRC_ON, "\n Inbound Packet Dropped %d \n",
                      gapNatIfTable[u4InIf]->u4NumOfPktsDropped);

        }
    }
    else
    {
        if (u4Status == NAT_FORWARD)
        {
            NAT_DBG (NAT_DBG_PKT_FLOW, "\n Inbound Packet forwarded "
                     " without translation \n");
            i4NatStatus = SUCCESS;
        }
        else
        {
            NAT_DBG (NAT_DBG_PKT_FLOW, "\n Inbound Packet Dropped ");
            i4NatStatus = FAILURE;
        }
        /*
         * The Packet coming in this interface need not be translated but
         * should be forwarded.
         */
        NAT_TRC (NAT_TRC_ON, "\n Inbound Packet Not Translated.");

    }

    NAT_TRC (NAT_TRC_ON, "\n Exiting NatProcessInBoundPkt \n");
    MOD_TRC (gu4NatTrc, DATA_PATH_TRC, "NAT", "\n Exiting FutureNAT module \n");
    if (u4SrcIp == NAT_ZERO)
    {
        return (i4NatStatus);
    }
    /* Return whether translation has been success or not */
    return (i4NatStatus);

}

/***************************************************************************
* Function Name  :  NatTranslateOutBoundPkt         
* Description :  This function translates the outbound packet if Global 
*                Nat is enabled and if the packet needs translation.     
* 
* Input (s)   :  1. pBuf - This stores the full IP packet along    
*                 with TCP/UDP header and payload.          
*                 2. u4OutIf - This gives the outgoing interface
*             
* Output (s)  :Either
*               modified IP packet with all references to Local IP   
*               and port(if NAPT enable) modified to Global IP and  
*               port.The flag pi1FrgReass is set to SUCCESS when
*               re-assembling has been done.              
*              Or 
*               unmodified IP Packet which is to be just forwarded.
*               
* Returns     :SUCCESS- if the packet is to be forwarded to the Outside
*              Network irrespective of whether the packet "needs 
*              translation" or not.
*              FAILURE- if the packet is to be dropped by the calling 
*              module.This happens when the packet needs to be 
*              translated but NAT is unable to do it. 
*              FAILURE - when all the fragments are not received
*
****************************************************************************/

PUBLIC INT4
NatTranslateOutBoundPkt (tCRU_BUF_CHAIN_HEADER ** ppBuf, UINT4 u4OutIf,
                         INT1 *pi1FrgReass)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4Direction = NAT_ZERO;
    INT4                i4NatStatus = FAILURE;
    UINT4               u4Status = NAT_FAILURE;
    UINT1               u1PktType = NAT_ZERO;
    UINT4               u4SrcIp = NAT_ZERO;
    UINT4               u4LocIp = NAT_ZERO;
    UINT4               u4TransIp = NAT_ZERO;
    UINT4               u4DestIp = NAT_ZERO;
    UINT2               u2SrcPort = NAT_ZERO;
    UINT2               u2DestPort = NAT_ZERO;
    UINT1               u1NatIfStatus = NAT_ZERO;
    UINT1               u1Dummy = NAT_ZERO;

    UNUSED_PARAM (u4SrcIp);
    pBuf = *ppBuf;

    if ((gu4NatEnable == NAT_DISABLE) ||
        ((u1NatIfStatus =
          (UINT1) (NatCheckIfNatEnable (u4OutIf))) == NAT_DISABLE))
    {
        return SUCCESS;
    }

    u4Status = NatCheckIfFragmentedPkt (pBuf, &u1Dummy);
    if (u4Status == NAT_TRUE)
    {
        *pi1FrgReass = FAILURE;
        if ((*ppBuf = NatFragReassemble (pBuf, u4OutIf, NAT_OUTBOUND)) == NULL)
        {
            *ppBuf = pBuf;
            return FAILURE;
        }
        pBuf = *ppBuf;
        *pi1FrgReass = SUCCESS;
    }

    /* DHCP Client-->Server and DHCP Server-->Client packets will be 
     * processed without any translations */

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1PktType,
                               NAT_PROT_OFFSET_IN_IPHEADER,
                               NAT_IP_PROT_FIELD_LEN);
    /* By passing dns packets for nat disabled interface */
    if ((u1PktType == NAT_TCP) || (u1PktType == NAT_UDP))
    {
        u2DestPort = NatGetDestPort (pBuf);
        u2SrcPort = NatGetSrcPort (pBuf);

        if (((gu4NatEnable == NAT_DISABLE) ||
             (u1NatIfStatus == NAT_DISABLE)) &&
            ((u2DestPort == NAT_DNS_PORT) || (u2SrcPort == NAT_DNS_PORT)))
        {
            return SUCCESS;
        }
    }

    if (u1PktType == NAT_UDP)
    {
        u2SrcPort = NatGetSrcPort (pBuf);
        u2DestPort = NatGetDestPort (pBuf);
        if ((u2DestPort == NAT_DHCP_SERV) || (u2DestPort == NAT_DHCP_CLNT))
        {
            i4NatStatus = SUCCESS;
            return (i4NatStatus);
        }
        /* Bypass RIP Packets */
        if ((u2SrcPort == NAT_RIP_PORT) && (u2DestPort == NAT_RIP_PORT))
        {
            NAT_DBG (NAT_DBG_PKT_FLOW, "\nRIP control Packets Bypassed");
            return SUCCESS;
        }
    }
    else if (u1PktType == NAT_OSPF_PROT)
    {
        /* ByPass OSPF Packets */
        NAT_DBG (NAT_DBG_PKT_FLOW, "\nOSPF Packets Bypassed");
        return SUCCESS;
    }
    else if (u1PktType == NAT_IGMP_PROT)
    {
        /* ByPass IGMP Packets */
        NAT_DBG (NAT_DBG_PKT_FLOW, "\n IGMP Packets Bypassed");
        return SUCCESS;
    }
    else if (((gu4NatEnable == NAT_DISABLE) ||
              (u1NatIfStatus == NAT_DISABLE)) && (u1PktType != NAT_PPTP))
    {
        /* 
         * ByPass Packets like ICMP when either global or interface nat 
         * status is disabled 
         */
        NAT_DBG (NAT_DBG_PKT_FLOW, "\nUnknown Packets Bypassed");
        return SUCCESS;
    }

    u2SrcPort = NatGetSrcPort (pBuf);
    u4SrcIp = NatGetSrcIpAddr (pBuf);
    /* ByPass PPTP Response From Our PPTP Server */

#ifndef LNXIP4_WANTED
    if (((u2SrcPort == NAT_PPTP_TCP_PORT) ||
         (NULL == NatGetStaticNaptEntry (&u4SrcIp,
                                         &u2SrcPort,
                                         NAT_OUTBOUND,
                                         u4OutIf, u1PktType))) &&
        (SecUtilIpIfIsOurAddress (u4SrcIp) == TRUE))
    {
        NAT_DBG (NAT_DBG_PKT_FLOW,
                 "\nPacket from server running in ISS. So bypassed.\n");
        return SUCCESS;
    }
#endif

    /* 
     * Check if the  outbound packet needs to be translated
     * or not.If the function below returns NAT_FAILURE then the packet can
     * be forwarded without translation.
     */
    if (u4OutIf > (NAT_MAX_NUM_IF))
    {
        return FAILURE;
    }
    u4Status = NatCheckOutbound (u4OutIf, pBuf);
    if (u4Status == NAT_SUCCESS)
    {
        u4Direction = NAT_OUTBOUND;
        NAT_TRC (NAT_TRC_ON, "\n Packet is an OUTBOUND packet.");
        /* Collect the packet information before translating */
        u4LocIp = NatGetSrcIpAddr (pBuf);
        u4Status = NatTranslatePkt (pBuf, u4Direction, u4OutIf);
        if (u4Status == NAT_SUCCESS)
        {
            /* Collect the packet information before translating */
            if ((u1PktType == NAT_TCP) || (u1PktType == NAT_UDP))
            {
                u2DestPort = NatGetDestPort (pBuf);
                u2SrcPort = NatGetSrcPort (pBuf);
            }

            u4TransIp = NatGetSrcIpAddr (pBuf);
            if (u4LocIp != u4TransIp)
            {
                u4DestIp = NatGetDestIpAddr (pBuf);
                /* Update the tuple information in flow manager */
                FL_NAT_UPDATE_FLOWDATA_NAT_TUPLE (pBuf);
            }
            i4NatStatus = SUCCESS;
            NAT_TRC (NAT_TRC_ON, "\n Outbound packet successfully Translated.");

            gapNatIfTable[u4OutIf]->u4NumOfTranslation++;
            NAT_DBG (NAT_DBG_PKT_FLOW, "\n Outbound packet "
                     "successfully Translated.");
            NAT_TRC1 (NAT_TRC_ON, "\n Number of Translation %d \n",
                      gapNatIfTable[u4OutIf]->u4NumOfTranslation);
        }
        if (u4Status == NAT_FAILURE)
        {
            /*
             * the packet could not be translated because Nat is 
             * unable to Translate the pkt
             */
            gapNatIfTable[u4OutIf]->u4NumOfPktsDropped++;
            i4NatStatus = FAILURE;
            NAT_DBG (NAT_DBG_PKT_FLOW, "\n Outbound Packet Dropped -"
                     " Nat translations failed");
            NAT_TRC1 (NAT_TRC_ON, "\n Outbound Packet Dropped %d ",
                      gapNatIfTable[u4OutIf]->u4NumOfPktsDropped);

        }
    }
    else
    {
        /*
         * The packet should not be translated but should be forwarded.
         */
        NAT_TRC (NAT_TRC_ON, "\n Outbound Packet Not Translated. ");
    }

    if (u4DestIp == NAT_ZERO)
    {
        return (i4NatStatus);
    }
    /* Returns whether packet is successfully translated or not */
    return (i4NatStatus);
}

/***************************************************************************
* Function Name:  NatIsNfs   
* Description  :  This function checks whether the arrived packet is   
*                 an NFS packet by checking the port number.       
*                                      
* Input (s)    :  pHeaderInfo - Contains IP and TCP/UDP header     
*                               information.                     
*                              
* Output (s)   :  None       
* Returns      :  NAT_TRUE or NAT_FALSE
*                     
****************************************************************************/

/*
 * This function checks both the source and destination port to see whether
 * it matches any of the NFS protocol port numbers i.e. 2049(NFS- Network File
 * System),111(RPC- Remote Procedure Call) or 635(PM- Linux Mountd).If it is
 * so then NAT_TRUE is  returned.
 */
PUBLIC UINT4
NatIsNfs (tHeaderInfo * pHeaderInfo)
{
    UINT4               u4Status = NAT_FALSE;

    if (pHeaderInfo->u4Direction == NAT_OUTBOUND)
    {
        if ((pHeaderInfo->u2OutPort == NAT_NFS) ||
            (pHeaderInfo->u2OutPort == NAT_RPC) ||
            (pHeaderInfo->u2OutPort == NAT_PM))
        {
            u4Status = NAT_TRUE;
        }
    }
    else
    {
        if ((pHeaderInfo->u2InPort == NAT_NFS) ||
            (pHeaderInfo->u2InPort == NAT_RPC) ||
            (pHeaderInfo->u2InPort == NAT_PM))
        {
            u4Status = NAT_TRUE;
        }
    }

    return (u4Status);
}

/***************************************************************************
* Function Name    :  NatInitHeaderInfo                    
* Description    :  This function gets the IP and TCP/UDP header    
*                   information from the packet and stores them in the    
*                   structure pointed by pHeaderInfo.                       
*                                      
* Input (s)    :  1. pBuf - This stores the full IP packet along    
*                    with TCP/UDP header and payload.          
*                 2. pHeaderInfo - This contains the header information  
*                    from packet.                    
*                                      
* Output (s)    :  Modified pHeaderInfo                   
* Returns      :  None
*                                      
****************************************************************************/

PUBLIC VOID
NatInitHeaderInfo (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo,
                   UINT4 u4Direction, UINT4 u4IfNum)
{
    UINT4               u4SrcIpAddr = NAT_ZERO;
    UINT4               u4DestIpAddr = NAT_ZERO;
    UINT4               u4SynByteOffset = NAT_ZERO;
    UINT2               u2SrcPort = NAT_ZERO;
    UINT2               u2DestPort = NAT_ZERO;
    UINT1               u1SynByte = NAT_ZERO;

    pHeaderInfo->u4IfNum = u4IfNum;
    pHeaderInfo->u4Direction = u4Direction;
    pHeaderInfo->i4Delta = NAT_ZERO;
    pHeaderInfo->u1SynBit = NAT_ZERO;
    pHeaderInfo->u1IpHeadLen = (UINT1) (NatGetIpHeaderLength (pBuf));

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &(pHeaderInfo->u2TotLen),
                               NAT_PKTLEN_OFFSET_IN_IPHEADER, NAT_IP_TOTAL_LEN);
    pHeaderInfo->u2TotLen = OSIX_NTOHS (pHeaderInfo->u2TotLen);

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &(pHeaderInfo->u1PktType),
                               NAT_PROT_OFFSET_IN_IPHEADER,
                               NAT_IP_PROT_FIELD_LEN);
    u4SrcIpAddr = NatGetSrcIpAddr (pBuf);
    u4DestIpAddr = NatGetDestIpAddr (pBuf);
    if (pHeaderInfo->u1PktType == NAT_TCP)
    {
        pHeaderInfo->u1TransportHeadLen =
            (UINT1) (NatGetTransportHeaderLength (pBuf, NAT_TCP));
        u4SynByteOffset =
            (UINT4) pHeaderInfo->u1IpHeadLen + NAT_TCP_CODE_BYTE_OFFSET;
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1SynByte, u4SynByteOffset,
                                   NAT_ONE_BYTE);
        u1SynByte &= NAT_TCP_SYN_BIT_MASK;
        /*Check if Syn Bit is set or not */
        if (u1SynByte == NAT_TCP_SYN_BIT)
        {
            pHeaderInfo->u1SynBit = NAT_TRUE;
        }
    }
    else
    {
        if (pHeaderInfo->u1PktType == NAT_UDP)
        {
            pHeaderInfo->u1TransportHeadLen =
                (UINT1) (NatGetTransportHeaderLength (pBuf, NAT_UDP));
        }
        else
        {
            pHeaderInfo->u1TransportHeadLen = NAT_ZERO;
        }
    }

    if ((pHeaderInfo->u1PktType == NAT_UDP)
        || (pHeaderInfo->u1PktType == NAT_TCP))
    {
        u2SrcPort = NatGetSrcPort (pBuf);
        u2DestPort = NatGetDestPort (pBuf);
    }

    if (NAT_OUTBOUND == u4Direction)
    {
        pHeaderInfo->u4InIpAddr = u4SrcIpAddr;
        pHeaderInfo->u4OutIpAddr = u4DestIpAddr;
        pHeaderInfo->u2InPort = u2SrcPort;
        pHeaderInfo->u2OutPort = u2DestPort;
    }
    else
    {
        pHeaderInfo->u4InIpAddr = u4DestIpAddr;
        pHeaderInfo->u4OutIpAddr = u4SrcIpAddr;
        pHeaderInfo->u2InPort = u2DestPort;
        pHeaderInfo->u2OutPort = u2SrcPort;
    }
    pHeaderInfo->pDynamicEntry = NULL;
    NAT_DBG4 (NAT_DBG_PKT_FLOW, "\n ***Header Information***\n  Source IP  \
              %x\n Destination IP %x \n Source Port %d \n \
              Destination Port %d \n ", u4SrcIpAddr, u4DestIpAddr, u2SrcPort, u2DestPort);
}

PUBLIC VOID
NatFragmentPktInitHeaderInfo (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo *
                              pHeaderInfo, UINT4 u4Direction, UINT4 u4IfNum)
{
    UINT4               u4SrcIpAddr = NAT_ZERO;
    UINT4               u4DestIpAddr = NAT_ZERO;
    UINT2               u2SrcPort = NAT_ZERO;
    UINT2               u2DestPort = NAT_ZERO;

    MEMSET (pHeaderInfo, NAT_ZERO, sizeof (tHeaderInfo));

    pHeaderInfo->u4IfNum = u4IfNum;
    pHeaderInfo->u4Direction = u4Direction;
    pHeaderInfo->i4Delta = NAT_ZERO;
    pHeaderInfo->u1SynBit = NAT_ZERO;
    pHeaderInfo->u1IpHeadLen = (UINT1) (NatGetIpHeaderLength (pBuf));

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &(pHeaderInfo->u2TotLen),
                               NAT_PKTLEN_OFFSET_IN_IPHEADER, NAT_IP_TOTAL_LEN);

    pHeaderInfo->u2TotLen = OSIX_NTOHS (pHeaderInfo->u2TotLen);

    u4SrcIpAddr = NatGetSrcIpAddr (pBuf);

    u4DestIpAddr = NatGetDestIpAddr (pBuf);

    pHeaderInfo->u1TransportHeadLen = NAT_ZERO;

    if (NAT_OUTBOUND == u4Direction)
    {
        pHeaderInfo->u4InIpAddr = u4SrcIpAddr;
        pHeaderInfo->u4OutIpAddr = u4DestIpAddr;
        pHeaderInfo->u2InPort = u2SrcPort;
        pHeaderInfo->u2OutPort = u2DestPort;
    }
    else
    {
        pHeaderInfo->u4InIpAddr = u4DestIpAddr;
        pHeaderInfo->u4OutIpAddr = u4SrcIpAddr;
        pHeaderInfo->u2InPort = u2DestPort;
        pHeaderInfo->u2OutPort = u2SrcPort;
    }

    pHeaderInfo->pDynamicEntry = NULL;

    NAT_DBG4 (NAT_DBG_PKT_FLOW, "\n ***Header Information***\n  Source IP   \
              %x\n Destination IP %x \n Source Port %d \n \
              Destination Port %d \n ", u4SrcIpAddr, u4DestIpAddr, u2SrcPort, u2DestPort);
}

/***************************************************************************
* Function Name    :  NatTranslatePkt                    
* Description    :  This function modifies the various IP addresses    
*                   present in the payload and various headers,      
*                   depending upon the direction, availability of Global 
*                   addresses, connection information.
*                   
*                                      
* Input (s)    :  1. pBuf - This stores the full IP packet along    
*            with TCP/UDP header and payload.          
*             2. u4Direction - Whether it is Inbound or Outbound   
*             3. u4IfNum - The interface number on which the    
*            packet has come or going depending upon the    
*            direction.                    
*                                      
* Output (s)    :  Modified pBuf.                    
* Returns      :  NAT_SUCCESS if the packet is successfully translated
*                 NAT_FAILURE if the packet could not be translated (may be due
*                 to unavalability of sufficient info or memory . 
*                                      
****************************************************************************/

PRIVATE UINT4
NatTranslatePkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Direction, UINT4 u4IfNum)
{
    tHeaderInfo         HeaderInfo;
    UINT4               u4TransStatus = NAT_SUCCESS;
    UINT4               u4NfsStatus = NAT_ZERO;

    NAT_TRC (NAT_TRC_ON, "\n Inside NatTranslatePkt ");
    MEMSET (&HeaderInfo, NAT_ZERO, sizeof (tHeaderInfo));
    /*
     * All the info from the packet is stored in the HeaderInfo structure.
     * For ICMP, IP _MCAST and NFS pkts we call the respective functions to
     * process the pkt.
     * For other pkts  we get the translated info and then-
     *   translate the payload and the headers if the payload 
     *   of the pkt invloves IP address
     *   else just translate the headers (IP,TCP/UDP)
     */

    /* Update the HeaderInfo Structure */
    NatInitHeaderInfo (pBuf, &HeaderInfo, u4Direction, u4IfNum);
    MOD_PKT_DUMP ((UINT4) gu4NatTrc, (UINT4) (DUMP_TRC), "NAT", pBuf,
                  (UINT4) (HeaderInfo.u1IpHeadLen +
                           HeaderInfo.u1TransportHeadLen),
                  "\n Packet Dump before Translation \n");

    if ((gu4NatEnable == NAT_ENABLE) &&
        (NatCheckIfNatEnable (HeaderInfo.u4IfNum) == NAT_ENABLE))
    {
        u4TransStatus = NatPortTrigInfoLookUp (pBuf, &HeaderInfo);

        if ((u4TransStatus == NAT_FAILURE) || (u4TransStatus == NAT_SUCCESS))
        {
            return (u4TransStatus);
        }
    }

    if (HeaderInfo.u1PktType == NAT_UDP)
    {
        /* ByPass L2TP Requests/Responses To Router */
#ifndef LNXIP4_WANTED
        if ((SecUtilIpIfIsOurAddress (HeaderInfo.u4InIpAddr) == TRUE) &&
            ((HeaderInfo.u2InPort == NAT_L2TP_UDP_PORT) ||
             (HeaderInfo.u2OutPort == NAT_L2TP_UDP_PORT)))
        {
            return NAT_SUCCESS;
        }
#endif

        if (((HeaderInfo.u2OutPort == IKE_UDP_PORT) &&
            (HeaderInfo.u2InPort == IKE_UDP_PORT))||
            ((HeaderInfo.u2OutPort == IKE_NATT_UDP_PORT)&&
            (HeaderInfo.u2InPort == IKE_NATT_UDP_PORT)))
        {
            if (u4Direction == NAT_INBOUND)
            {
                u4TransStatus = natProcessIKE (pBuf, &HeaderInfo);
                if (u4TransStatus == NAT_FAILURE)
                {
#ifndef LNXIP4_WANTED
                    if (SecUtilIpIfIsOurAddress (HeaderInfo.u4InIpAddr) == TRUE)
                    {
                        return (NAT_SUCCESS);
                    }
#endif
                }
                else
                {
                    return (NAT_SUCCESS);
                }
            }
            else if (u4Direction == NAT_OUTBOUND)
            {
#ifndef LNXIP4_WANTED
                if (SecUtilIpIfIsOurAddress (HeaderInfo.u4InIpAddr) == TRUE)

                {
                    return (NAT_SUCCESS);
                }
#endif
            }
        }
    }

    if (HeaderInfo.u1PktType == NAT_TCP)
    {
        if (HeaderInfo.u2InPort == KERBEROS_TCP_PORT)
        {
            return NAT_FORWARD;
        }

    }
    /* DSL_ADD -E- */

    /* Check whether it is an ICMP packet */
    if (HeaderInfo.u1PktType == NAT_ICMP)
    {
        u4TransStatus = (UINT4) NatProcessIcmp (pBuf, u4Direction, u4IfNum);
    }
    else if (HeaderInfo.u1PktType == NAT_PPTP)
    {
        NatProcessPPTPDataPkt (pBuf, &HeaderInfo);
        NatIpHeaderModify (pBuf, &HeaderInfo);
        u4TransStatus = NAT_SUCCESS;
    }
    /* Check for IPSec ESP packet */
    else if (HeaderInfo.u1PktType == NAT_IPSEC_ESP)
    {
        u4TransStatus = natProcessIPSec (pBuf, &HeaderInfo);
    }
    /* Check for IPSec AH packet */
    else if (HeaderInfo.u1PktType == NAT_IPSEC_AH)
    {
        u4TransStatus = NAT_FAILURE;
    }
    /* Check for IKE packet */
    else if ((((HeaderInfo.u2InPort == NAT_IKE_STD_PORT) &&
             (HeaderInfo.u2OutPort == NAT_IKE_STD_PORT))||
             ((HeaderInfo.u2InPort == IKE_NATT_UDP_PORT)&&
             (HeaderInfo.u2OutPort == IKE_NATT_UDP_PORT))) &&
             (HeaderInfo.u1PktType == NAT_UDP) &&
             (gi4NatIKEPortTranslation == NAT_DISABLE) &&
             (u4Direction == NAT_OUTBOUND))
    {
        u4TransStatus = natProcessIKE (pBuf, &HeaderInfo);
    }
/* NAT_Enh End */
    else
    {
        /* Check for Multicast packet */
        if (NatCheckForIpMulticast (&HeaderInfo) == NAT_TRUE)
        {
            NAT_FL_UTIL_UPDATE_FLOWTYPE_FLOWDATA (pBuf);
            u4TransStatus = NatProcessIpMulticast (&HeaderInfo);
        }
        else
        {

            /* Check whether it is an NFS packet */
            u4NfsStatus = NatSearchNfsTable (&HeaderInfo);

            if (u4NfsStatus != NAT_FORWARD)
            {
                u4TransStatus = u4NfsStatus;
            }
            else
            {

                /*
                 * Search for the connection in the dynamic session table.
                 * If present  update the pHeaderInfo, else create a new
                 * session  entry and update the pHeaderInfo.
                 */
                u4TransStatus = NatGetDynamicEntry (&HeaderInfo, NAT_CREATE);

                if (u4TransStatus != NAT_FAILURE)
                {
                    NAT_TRC (NAT_TRC_ON, "\n Table search Success.");

                    /* Check for payload modification -ALGs are called */
                    u4TransStatus = NatProtocolHandler (pBuf, &HeaderInfo);

                }
                else
                {
                    NAT_TRC (NAT_TRC_ON, "\n Table search Failure.");
                }
            }
        }
        if (u4TransStatus == NAT_SUCCESS)
        {
            /* Header Translation */
            if (HeaderInfo.u1PktType == NAT_TCP)
            {
                NatTcpHeaderModify (pBuf, &HeaderInfo);
            }
            if (HeaderInfo.u1PktType == NAT_UDP)
            {
                NatUdpHeaderModify (pBuf, &HeaderInfo);
            }
            NatIpHeaderModify (pBuf, &HeaderInfo);

            NAT_TRC (NAT_TRC_ON,
                     "\n TCP/UDP and IP header modified successfully \n");
            NAT_DBG4 (NAT_DBG_PKT_FLOW,
                      "\n ***Modified Header Information***\n  "
                      "Inside IP    %x\n Outside IP %x \n "
                      "Inside Port %d \n Outside Port %d \n ",
                      HeaderInfo.u4InIpAddr, HeaderInfo.u4OutIpAddr,
                      HeaderInfo.u2InPort, HeaderInfo.u2OutPort);
            NAT_TRC4 (NAT_TRC_ON,
                      "\n ***Modified Header Information***\n  "
                      "Inside IP  %u\n    Outside IP %u \n "
                      "Inside Port %d \n Outside Port %d \n ",
                      HeaderInfo.u4InIpAddr, HeaderInfo.u4OutIpAddr,
                      HeaderInfo.u2InPort, HeaderInfo.u2OutPort);
        }
        else
        {
            NAT_TRC (NAT_TRC_ON, "\n Protocol Failure.");

        }

    }                            /* End of NAT is ICMP check */

    MOD_PKT_DUMP ((UINT4) gu4NatTrc, (UINT4) (DUMP_TRC), "NAT", pBuf,
                  (UINT4) (HeaderInfo.u1IpHeadLen +
                           HeaderInfo.u1TransportHeadLen),
                  "\n Packet Dump after Translation \n");
    NAT_TRC (NAT_TRC_ON, "\n Exiting NatTranslatePkt\n");

    /* Returns the status of translation - NAT_SUCCESS or NAT_FAILURE */
    return (u4TransStatus);
}

/***************************************************************************
* Function Name    :  NatCheckIfNatEnable                    *
* Description    :  This function checks whether NAT is enabled on the   *
*                   given interface                    *
*                                      *
* Input (s)    :  u4IfNum - Interface number to be checked for NAT    *
*                enable                        *
*                                      *
* Output (s)    :  None                         *
* Returns      :  NAT_ENABLE or NAT_DISABLE                *
*                                      *
****************************************************************************/

PUBLIC UINT4
NatCheckIfNatEnable (UINT4 u4IfNum)
{
    UINT4               u4EnableStatus = NAT_DISABLE;

    if (gu4NatEnable != NAT_ENABLE)
    {
        return u4EnableStatus;
    }

    if (u4IfNum <= NAT_MAX_NUM_IF)
    {
        if ((gapNatIfTable[u4IfNum] != NULL)
            && (gapNatIfTable[u4IfNum]->u1NatEnable == NAT_ENABLE)
            && (gapNatIfTable[u4IfNum]->i4RowStatus == NAT_STATUS_ACTIVE))
        {
            u4EnableStatus = NAT_ENABLE;
        }
    }
    return (u4EnableStatus);

}

/***************************************************************************
* Function Name    :  NatCheckIfNaptEnable                                 *
* Description      :  This function checks whether NAPT is enabled on the  *
*                     given interface                                      *
*                                                                          *
* Input (s)        :  u4IfNum - Interface number to be checked for NAPT    *
*                     enable                                               *
*                                                                          *
* Output (s)       :  None                                                 *
* Returns          :  NAT_ENABLE or NAT_DISABLE                            *
*                                                                          *
****************************************************************************/

PUBLIC UINT4
NatCheckIfNaptEnable (UINT4 u4IfNum)
{
    UINT4               u4EnableStatus = NAT_DISABLE;

    if (u4IfNum <= NAT_MAX_NUM_IF)
    {
        if ((gapNatIfTable[u4IfNum] != NULL)
            && (gapNatIfTable[u4IfNum]->u1NaptEnable == NAT_ENABLE))
        {
            u4EnableStatus = NAT_ENABLE;
        }
    }
    return (u4EnableStatus);
}

/***************************************************************************
* Function Name    :  NatCheckInbound
* Description    :  This function checks whether the packet arrived is an
*                   inbound packet.
*
* Input (s)    :  u4IfNum - The outside interface number on which the
*              packet is received.
*             pBuf - This stores the full IP packet along
*            with TCP/UDP header and payload.
*
* Output (s)    :  None
* Returns      :  NAT_SUCCESS or NAT_FAILURE
*
****************************************************************************/

/*
 * For a packet to be inbound, NAT should be enabled on the incoming interface
 * and the destination address should be in the Static Table or the 
 * Global Address Table.
 */
PRIVATE UINT4
NatCheckInbound (UINT4 u4IfNum, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT4               u4IpAddr = NAT_ZERO;
    UINT4               u4TranslatedLocIpAddr = NAT_ZERO;
    UINT4               u4SearchStatus = NAT_FAILURE;
    UINT4               u4IfStatus = NAT_ZERO;

    u4IfStatus = NatCheckIfNatEnable (u4IfNum);

    if ((gu4NatEnable == NAT_ENABLE)
        && (u4IfStatus == NAT_ENABLE)
        && (gapNatIfTable[u4IfNum]->i4RowStatus == NAT_STATUS_ACTIVE))
    {
        u4TranslatedLocIpAddr = NatGetDestIpAddr (pBuf);
        if ((u4TranslatedLocIpAddr >= NAT_MULTICAST_START_ADDR)
            && (u4TranslatedLocIpAddr < NAT_MULTICAST_END_ADDR))
        {
            return NAT_FORWARD;
        }

        /* Search if an entry is available in the static policy NAT table. */
        if (NAT_SUCCESS == NatApiSearchPolicyTable (u4TranslatedLocIpAddr))
        {
            return NAT_SUCCESS;
        }
        u4IpAddr =
            NatSearchStaticTable (u4TranslatedLocIpAddr, NAT_INBOUND, u4IfNum);
        if (u4IpAddr == NAT_ZERO)
        {
            u4SearchStatus =
                NatSearchGlobalIpAddrTable (u4TranslatedLocIpAddr, u4IfNum);
        }
        else
        {
            u4SearchStatus = NAT_SUCCESS;
        }
    }
    else
    {
        u4SearchStatus = NAT_SUCCESS;
    }

    return (u4SearchStatus);
}

/***************************************************************************
* Function Name    :  NatCheckOutbound
* Description    :  This function checks whether the packet arrived is an
*             Outbound packet.
*
* Input (s)    :  u4IfNum - The outside interface number on which the
*              packet is transmitted.
*             pBuf - This stores the full IP packet along
*            with TCP/UDP header and payload.
*
* Output (s)    :  None
* Returns      :  NAT_SUCCESS or NAT_FAILURE
*
****************************************************************************/

/*
 * For a packet to be outbound, NAT should be enabled on the outgoing interface
 * and the source address should be in the Static Table or the Local Address
 * Table.
 */

PRIVATE UINT4
NatCheckOutbound (UINT4 u4IfNum, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT4               u4IpAddr = NAT_ZERO;
    UINT4               u4TransIpAddr = NAT_ZERO;
    UINT4               u4SearchStatus = NAT_FAILURE;
    UINT4               u4IfStatus = NAT_ZERO;

    u4IfStatus = NatCheckIfNatEnable (u4IfNum);

    if ((gu4NatEnable == NAT_ENABLE) &&
        (u4IfStatus == NAT_ENABLE) &&
        (gapNatIfTable[u4IfNum]->i4RowStatus == NAT_STATUS_ACTIVE))
    {
        u4IpAddr = NatGetSrcIpAddr (pBuf);
        /* searching the StaticTable also */
        u4TransIpAddr = NatSearchStaticTable (u4IpAddr, NAT_OUTBOUND, u4IfNum);
        if (u4TransIpAddr != NAT_ZERO)
        {
            return NAT_SUCCESS;
        }
        /*search if the src IP address lies in the local Ip address range */
        u4SearchStatus = NatSearchLocIpAddrTable (u4IpAddr);

    }
    else
    {
        u4SearchStatus = NAT_SUCCESS;
    }

    return (u4SearchStatus);
}

/***************************************************************************
* Function Name    :  NatProtocolHandler                   
* Description    :  This function checks whether the  IP address is    
*             embedded in the payload and whether modification is  
*             required. If it is required, then corresponding    
*             changes to the payload are done.            
*                                      
* Input (s)    :  1. pBuf - This stores the full IP packet along    
*            with TCP/UDP header and payload.          
*             2. pHeaderInfo - Contains information on IP and    
*            TCP/UDP headers.                  
*                                      
* Output (s)    :  Modified pBuf                    *
* Returns      :  NAT_SUCCESS or NAT_FAILURE.              *
*                                      *
****************************************************************************/

PRIVATE UINT4
NatProtocolHandler (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo)
{
    tNatAppDefn        *pApplnRec = NULL;
    tHeaderInfo         NewHeaderInfo;
    UINT4               u4TranslateStatus = NAT_SUCCESS;
    UINT4               u4IpAddr = NAT_ZERO;
    UINT2               u2Port = NAT_ZERO;
    UINT2               u2TotalLength = NAT_ZERO;
    UINT2               u2AlgPort = NAT_ZERO;
    UINT2               u2AlgPort_source = NAT_ZERO;
    /* variable for sipalg port */
    UINT2               u2SipAlgPort = NAT_SIP_TCPUDP_PORT;

    UINT1               u1DataFlag = NAT_TRUE;
    UINT1               u1Alg = NAT_TRUE;
    /*
     * this function handles the protocol specific packets -
     * Packets which need payload translations
     * Packets like TFTP which need special treatment
     */

    u2SipAlgPort = gSipSignalPort.u2UdpListenPort;

    NAT_TRC (NAT_TRC_ON, "\n Inside NatProtocolHandler \n");

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2TotalLength,
                               NAT_TOT_LEN_OFFSET, NAT_TOL_LEN_FIELD);
    u2TotalLength = OSIX_NTOHS (u2TotalLength);
    /*
     * Check if the pkt contains data part (other than IP & TCP/UDP
     * headers).If it contains data part, only then go for payload 
     * translation.
     */

    if (u2TotalLength ==
        (pHeaderInfo->u1IpHeadLen + pHeaderInfo->u1TransportHeadLen))
    {
        u1DataFlag = NAT_FALSE;
    }

    /* Get the port to which ALG has to be applied */
    if (pHeaderInfo->pDynamicEntry->u1Direction == NAT_INBOUND)
    {
        /* Server is present inside the LAN network */
        u2AlgPort = pHeaderInfo->pDynamicEntry->u2LocPort;
        u2AlgPort_source = pHeaderInfo->pDynamicEntry->u2OutPort;
    }
    else if (pHeaderInfo->pDynamicEntry->u1Direction == NAT_OUTBOUND)
    {
        /* Server is present in the WAN network */
        u2AlgPort = pHeaderInfo->pDynamicEntry->u2OutPort;
        u2AlgPort_source = pHeaderInfo->pDynamicEntry->u2LocPort;
    }
    else
    {
        return NAT_FAILURE;
    }

    if ((u2AlgPort == u2SipAlgPort) || (u2AlgPort_source == u2SipAlgPort))
    {
        u4TranslateStatus = (UINT4) NatProcessSIPPkt (pBuf, pHeaderInfo);
    }
    else
    {

        switch (u2AlgPort)
        {

            case NAT_FTP_CTRL_PORT:
                NAT_TRC (NAT_TRC_ON, "\nFTP Packet ");

                if ((pHeaderInfo->u1PktType == NAT_TCP)
                    && (u1DataFlag == NAT_TRUE))
                {
                    u4TranslateStatus = NatProcessFtp (pBuf, pHeaderInfo);
                }
                break;
            case NAT_DNS_PORT:
                NAT_TRC (NAT_TRC_ON, "\nDNS Packet ");
                if (u1DataFlag == NAT_TRUE)
                {
                    u4TranslateStatus =
                        NatProcessDnsTcpOrUdp (pBuf, pHeaderInfo);
                }
                break;
            case NAT_TFTP_PORT:
                NAT_DBG (NAT_DBG_PKT_FLOW, "\n TFTP Packet");
                if ((pHeaderInfo->u1PktType == NAT_UDP)
                    && (u1DataFlag == NAT_TRUE))
                {
                    MEMCPY (&NewHeaderInfo, pHeaderInfo, sizeof (tHeaderInfo));
                    if (pHeaderInfo->pDynamicEntry->u1Direction == NAT_INBOUND)
                    {
                        NewHeaderInfo.u4Direction = NAT_OUTBOUND;
                        NewHeaderInfo.u2InPort = NAT_ZERO;

                        /* If Entry already exists with NewHeaderInfo then 
                         * do not call partiallinks to add.  */
                        if (NatScanPartialLinksList (&NewHeaderInfo, &u4IpAddr,
                                                     &u2Port) != NAT_SUCCESS)
                        {
                            u4TranslateStatus =
                                NatAddPartialLinksList (&NewHeaderInfo,
                                                        pHeaderInfo->
                                                        u4OutIpAddr,
                                                        pHeaderInfo->u2OutPort,
                                                        NAT_NON_PERSISTENT);
                        }
                    }
                    else
                    {
                        NewHeaderInfo.u4Direction = NAT_INBOUND;
                        NewHeaderInfo.u2OutPort = NAT_ZERO;

                        /* If Entry already exists with NewHeaderInfo then 
                         * do not call partiallinks to add.  */
                        if (NatScanPartialLinksList (&NewHeaderInfo, &u4IpAddr,
                                                     &u2Port) != NAT_SUCCESS)
                        {
                            u4TranslateStatus =
                                NatAddPartialLinksList (&NewHeaderInfo,
                                                        pHeaderInfo->
                                                        pDynamicEntry->
                                                        u4LocIpAddr,
                                                        pHeaderInfo->
                                                        pDynamicEntry->
                                                        u2LocPort,
                                                        NAT_NON_PERSISTENT);
                        }
                    }
                }
                break;

            case NAT_PPTP_PORT:
                /* PPTP Control Pkt */
                NAT_DBG (NAT_DBG_PKT_FLOW, "\n PPTP Packet");
                u4TranslateStatus =
                    (UINT4) NatProcessPPTPCntrlPkt (pBuf, pHeaderInfo);
                break;

                /* H323 */
            case NAT_H225_PORT:
                /* H225 Pkt */
                u4TranslateStatus =
                    (UINT4) NatProcessH225Pkt (pBuf, pHeaderInfo);
                break;

                /*  Real_Audio */
            case NAT_RTSP_PORT:
                /*fall through */
            case NAT_PNA_PORT:
                u4TranslateStatus =
                    (UINT4) NatProcessSmediaPkt (pBuf, pHeaderInfo);
                break;

                /* CUSEEME */
            case NAT_CUSME_PORT:
                if ((pHeaderInfo->u1PktType == NAT_UDP)
                    && (u1DataFlag == NAT_TRUE))
                {
                    u4TranslateStatus =
                        (UINT4) NatProcessCuSeeMePkt (pBuf, pHeaderInfo);
                }
                break;

            default:
                if (pHeaderInfo->u1PktType == NAT_TCP)
                {
                    if (pHeaderInfo->pDynamicEntry->pAppRec != NULL)
                    {
                        if (u1DataFlag == NAT_TRUE)
                        {
                            u4TranslateStatus =
                                (UINT4) pHeaderInfo->pDynamicEntry->pAppRec->
                                pProcessPkt (pBuf, pHeaderInfo);
                        }
                    }
                    else
                    {
                        pApplnRec =
                            NatCheckPortNegAppln ((UINT4) pHeaderInfo->
                                                  u1PktType, u2AlgPort);
                        if (pApplnRec != NULL)
                        {
                            /* Assign the application record to the Link */
                            pHeaderInfo->pDynamicEntry->pAppRec = pApplnRec;

                            /* call the application call back function for 
                             * payload translation */
                            if ((pApplnRec->pProcessPkt != NULL)
                                && (u1DataFlag == NAT_TRUE))
                            {
                                u4TranslateStatus =
                                    (UINT4) pApplnRec->pProcessPkt (pBuf,
                                                                    pHeaderInfo);
                            }
                        }
                        else
                        {
                            u1Alg = NAT_FALSE;
                        }
                    }
                }
                else if (pHeaderInfo->u1PktType == NAT_UDP)
                {
                    u1Alg = NAT_FALSE;
                }

                break;
        }
    }
    if (u1Alg != NAT_FALSE)
    {
        /* Update the flow as control flow */
        NAT_FL_UTIL_UPDATE_FLOWTYPE_FLOWDATA (pBuf);
    }

    NAT_TRC (NAT_TRC_ON, "\n Exiting NatProtocolHandler \n");

    return (u4TranslateStatus);
}

/***************************************************************************
* Function Name    :  NatIfInitFields
* Description    :  This function initialises fields in the Interface
*                   Node
*
* Input (s)    :  u4IfNum - Interface number whose information should be
*                 updated.
*
* Output (s)    :  None
* Returns      :  None
*
****************************************************************************/

PUBLIC VOID
NatIfInitFields (UINT4 u4IfNum)
{
    tTranslatedLocIpAddrNode *pGlobalNode = NULL;
#ifdef UPNP_WANTED
    CHR1               *pu1value = NULL;
#endif

    /*
     * Initialise the first and the last address to be used for translation .
     */

    TMO_SLL_Scan (&(gapNatIfTable[u4IfNum]->TranslatedLocIpList), pGlobalNode,
                  tTranslatedLocIpAddrNode *)
    {
        if (pGlobalNode->i4RowStatus == NAT_STATUS_ACTIVE)
        {
            /*
             * pointer to the Trans IP address which is going to be 
             * used the next
             * time a Global IP is needed.
             */
            NatGetSubnetRangeIpAddress (pGlobalNode->u4TranslatedLocIpAddr,
                                        pGlobalNode->u4Mask,
                                        &(gapNatIfTable[u4IfNum]->
                                          u4CurrTranslatedLocIpAddr),
                                        &(gapNatIfTable[u4IfNum]->
                                          u4TranslatedLocIpRangeEnd));
#ifdef UPNP_WANTED
            CLI_CONVERT_IPADDR_TO_STR (pu1value,
                                       (gapNatIfTable[u4IfNum]->
                                        u4CurrTranslatedLocIpAddr));
            SET_UPNP_NAT_STATE_VAR_VAL (NAT_VARIABLE_TYPE_EXT_IPADD, pu1value);
            SEND_NOTIFY_TO_CONTROL_POINTS ();
            NAT_TRC1 (NAT_TRC_ON, "ExternalIPAddress :%lu",
                      (gapNatIfTable[u4IfNum]->u4CurrTranslatedLocIpAddr));
#endif /* UPNP_WANTED */

            /*
             * pointer to the last IP address in the range of IP addresses 
             * which can be used for translation.
             */

            NAT_TRC1 (NAT_TRC_ON, "\r\n Set the u4TranslatedLocIpRangeEnd = %x",
                      gapNatIfTable[u4IfNum]->u4TranslatedLocIpRangeEnd);
            /*
             * pointer to the Trans IP address which is  to be used for
             * translating the  IP address in the DNS response.
             */
            gapNatIfTable[u4IfNum]->u4CurrDnsTranslatedLocIpInUse =
                gapNatIfTable[u4IfNum]->u4CurrTranslatedLocIpAddr;
            /*
             * pointer to the last Trans IP address in the range of Global IP
             * addresses which can be used for  translating the  IP address
             * in the DNS response.
             */
            gapNatIfTable[u4IfNum]->u4CurrDnsTranslatedLocIpRangeEnd =
                gapNatIfTable[u4IfNum]->u4TranslatedLocIpRangeEnd;
            break;
        }
    }

    /*Initialise all the linked lists */
    TMO_SLL_Init (&(gapNatIfTable[u4IfNum]->NatFreeGipList));
    gapNatIfTable[u4IfNum]->u4NumOfTranslation = NAT_ZERO;
    gapNatIfTable[u4IfNum]->u4NumOfActiveSessions = NAT_ZERO;
    gapNatIfTable[u4IfNum]->u4NumOfPktsDropped = NAT_ZERO;
    gapNatIfTable[u4IfNum]->u4NumOfSessionsClosed = NAT_ZERO;
}

/***************************************************************************
* Function Name    :  NatInit
* Description    :  IP (or the Parent Module ) should call init for 
*                   allocating memory to configure the MIB and the 
*                   tables used by it.
*
* Input (s)    :  None
*
* Output (s)    :  None
* Returns      :  SUCCESS or FAILURE
*
****************************************************************************/

/* The Parent Module should call init for allocating memory to configure 
 * the MIB */
PUBLIC INT4
NatInit (INT1 *i1pDummy)
{
    INT4                i4Status = NAT_ZERO;
    UINT4               u4Status = NAT_ZERO;
    UINT4               u4Counter = NAT_ZERO;
    UINT4               u4Index = NAT_ZERO;

    UNUSED_PARAM (i1pDummy);
    /*
     * All the tables and lists are initalised 
     * Blocks of memory are alloted for tables which are needed for configuring
     * NAT- Interface table, Local IP address list, Static mapping
     * list and Global IP list.
     * By default the Global Nat switch is disabled.
     * The Timer for Nat is initialised.
     */

    i4Status = SUCCESS;
    gu4NatTypicalNumOfEntries = NAT_TYPICAL_NUM_ENTRIES;
    gu4NatEnable = NAT_DISABLE;

    gu4NatIdleTimeOut = NAT_IDLE_TIME_OUT;

    if (OsixCreateSem ((CONST UINT1 *) NAT_PROTOCOL_LOCK, NAT_ONE,
                       NAT_ZERO, &gNatSemId) != OSIX_SUCCESS)
    {
        NAT_TRC (NAT_TRC_ON, "Semaphore Creation Failed\n");
        return (NAT_FAILURE);
    }
    NatLock ();
    nmhSetNatEnable (NAT_ENABLE);
    NatUnLock ();
    NAT_TRC (NAT_TRC_ON, "\n NatInit has been Called");

    TMO_SLL_Init (&gNatLocIpAddrList);

    gu4NatInitTranslatedLocPort = NAT_INIT_PORT_NUMBER;
    if (NatSizingMemCreateMemPools () != OSIX_FAILURE)
    {
        for (u4Counter = NAT_ONE; u4Counter <= NAT_MAX_NUM_IF; u4Counter++)
        {
            gapNatIfTable[u4Counter] = NULL;
        }
        u4Status = NatInitTimer ();
        if (u4Status == NAT_FAILURE)
        {
            i4Status = NAT_FAILURE;
            MOD_TRC (gu4NatTrc, ALL_FAILURE_TRC, "NAT",
                     "\n Timer not initialised \n");
        }
    }
    else
    {
        NAT_TRC (NAT_TRC_ON, "\n Error : LLR Mem Pool Creation failure");
        MOD_TRC (gu4NatTrc, ALL_FAILURE_TRC, "NAT",
                 "\n Memory Pools not created - insufficient memory \n");
        return NAT_FAILURE;
    }
    NatSipAlgInitTimer ();

    /* Initialize all the Lists and Hash tables */
    NatInitListsAndTables ();

    MEMSET (gaTrigInfo, NAT_ZERO, NAT_MAX_APP_ALLOWED);
    MEMSET (gaRsvdTrigInfo, NAT_ZERO, NAT_MAX_APP_ALLOWED);

    RegisterFSNAT ();

    for (u4Counter = NAT_MAX_PRIVILAGED_PORT;
         u4Counter > NAT_MIN_PRIVILAGED_PORT; u4Counter--, u4Index++)
    {
        gau2NatGlobalFreePorts[u4Index] = (UINT2) (u4Counter);
    }
    gu2FreePortIndex = NAT_ZERO;
    if (NatFragInit () == NAT_FAILURE)
    {
        NAT_TRC (NAT_TRC_ON, "\n NatFragInit initialization failed \n");
        return FAILURE;
    }

    NAT_TRC (NAT_TRC_ON, "\n Exiting NatInit \n");
    NAT_INIT_COMPLETE (OSIX_SUCCESS);
    NatLock ();
    nmhSetNatEnable (NAT_ENABLE);
    NatUnLock ();
    return (i4Status);
}

/***************************************************************************
* Function Name    :  NatDeInit
* Description    :  This function deinitialises memory.
*             tables
*
* Input (s)    :  NONE
*
* Output (s)    :  None
* Returns      :  NAT_SUCCESS or NAT_FAILURE
*
****************************************************************************/
PUBLIC INT1
NatDeInit (VOID)
{
    NAT_DBG (NAT_DBG_PKT_FLOW, "\n Entering NatDeInit \n");
    nmhSetNatEnable (NAT_DISABLE);
    NatDeleteHashTables ();
    NatPolicyDeInitList ();
    NatSizingMemDeleteMemPools ();
    if (NatDeInitTimer () == NAT_FAILURE)
    {
        MOD_TRC (gu4NatTrc, ALL_FAILURE_TRC, "NAT",
                 "\n Timer Not deleted For NAT \n");

    }
    NatFragDeInit ();
    NAT_DBG (NAT_DBG_PKT_FLOW, "\n Exiting NatDeInit \n");
    return (SUCCESS);
}

/***************************************************************************
* Function Name    :  NatDeInitForIf
* Description    :  This will be called if NAT is disabled in an interface
*             to deallocate all the memory allocated for that i/f.
*
* Input (s)    :  u4IfNum - Interface Number
*
* Output (s)    :  None
* Returns      :  None
*
****************************************************************************/

PUBLIC VOID
NatDeInitForIf (UINT4 u4IfNum)
{
    tInterfaceInfo     *pNode = NULL;
    tStaticNaptEntry   *pStaticNaptNode = NULL;
    tNatFreeGipListNode *pNatFreeGipListNode = NULL;
    tTMO_SLL_NODE      *pNextNode = NULL;
    INT4                i4IfIndex = NAT_ZERO;
    INT4                i4GlobalFlag = NAT_ZERO;
    INT4                i4StaticFlag = NAT_ZERO;
    UINT4               u4GlobalAddrTranslatedLocIp = NAT_ZERO;
    INT4                i4StaticIndex = NAT_ZERO;
    UINT4               u4StaticLocalIp = NAT_ZERO;
    INT1                i1RetVal = NAT_ZERO;

    pNode = gapNatIfTable[u4IfNum];
    if (pNode == NULL)
    {
        return;
    }

    /*Release all the Static mapping for the given interface */

    i1RetVal =
        nmhGetFirstIndexNatStaticTable (&i4StaticIndex, &u4StaticLocalIp);
    if (u4IfNum == (UINT4) i4StaticIndex)
    {
        nmhSetNatStaticEntryStatus (i4StaticIndex, u4StaticLocalIp, DESTROY);
        i4StaticFlag = NAT_ONE;

    }
    do
    {
        if (i4StaticFlag == NAT_ZERO)
        {
            if (u4IfNum == (UINT4) i4StaticIndex)
            {
                nmhSetNatStaticEntryStatus (i4StaticIndex, u4StaticLocalIp,
                                            DESTROY);

            }
        }

    }
    while (nmhGetNextIndexNatStaticTable (i4StaticIndex,
                                          &i4StaticIndex,
                                          u4StaticLocalIp,
                                          &u4StaticLocalIp) == SNMP_SUCCESS);

    /* Delete the Virtual servers */
    pStaticNaptNode = (tStaticNaptEntry *)
        TMO_SLL_First (&(pNode->StaticNaptList));
    while (pStaticNaptNode != NULL)
    {
        pNextNode = TMO_SLL_Next (&(pNode->StaticNaptList),
                                  &(pStaticNaptNode->StaticNaptTable));
        TMO_SLL_Delete (&(pNode->StaticNaptList),
                        &(pStaticNaptNode->StaticNaptTable));
        NatMemReleaseMemBlock (NAT_STATIC_NAPT_POOL_ID,
                               (UINT1 *) pStaticNaptNode);
        pStaticNaptNode = (tStaticNaptEntry *) pNextNode;

    }

    /* Delete the free global IP list */
    i1RetVal = nmhGetFirstIndexNatGlobalAddressTable (&i4IfIndex,
                                                      &u4GlobalAddrTranslatedLocIp);
    UNUSED_PARAM (i1RetVal);
    if (u4IfNum == (UINT4) i4IfIndex)
    {
        nmhSetNatGlobalAddressEntryStatus (i4IfIndex,
                                           u4GlobalAddrTranslatedLocIp,
                                           DESTROY);
        i4GlobalFlag = NAT_ONE;

    }
    do
    {
        if (i4GlobalFlag == NAT_ZERO)
        {
            if (u4IfNum == (UINT4) i4IfIndex)
            {
                nmhSetNatGlobalAddressEntryStatus (i4IfIndex,
                                                   u4GlobalAddrTranslatedLocIp,
                                                   DESTROY);
                break;
            }
        }
    }
    while (nmhGetNextIndexNatGlobalAddressTable (i4IfIndex,
                                                 &i4IfIndex,
                                                 u4GlobalAddrTranslatedLocIp,
                                                 &u4GlobalAddrTranslatedLocIp)
           == SNMP_SUCCESS);

    pNatFreeGipListNode = (tNatFreeGipListNode *)
        TMO_SLL_First (&(pNode->NatFreeGipList));
    while (pNatFreeGipListNode != NULL)
    {
        pNextNode = TMO_SLL_Next (&(pNode->NatFreeGipList),
                                  &(pNatFreeGipListNode->NatFreeGipListNode));
        TMO_SLL_Delete (&(pNode->NatFreeGipList),
                        &(pNatFreeGipListNode->NatFreeGipListNode));
        NatMemReleaseMemBlock (NAT_FREE_GLOBAL_LIST_POOL_ID,
                               (UINT1 *) pNatFreeGipListNode);
        pNatFreeGipListNode = (tNatFreeGipListNode *) pNextNode;
    }

    nmhSetNatIfEntryStatus ((INT4) u4IfNum, DESTROY);
}

/*****************************************************************************/
/* Function Name      : NatLock                                              */
/*                                                                           */
/* Description        : This function is used to take the NAT  protocol      */
/*                      SEMa4 to avoid simultaneous access to                */
/*                      NAT database                                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : NAT_SUCCESS or NAT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
NatLock (VOID)
{
    if (OsixSemTake (gNatSemId) != OSIX_SUCCESS)
    {
        return (NAT_FAILURE);
    }
    return (NAT_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : NatUnLock                                            */
/*                                                                           */
/* Description        : This function is used to give the NAT                */
/*                      SEMa4 to avoid simultaneous access to                */
/*                      NAT database                                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : NAT_SUCCESS or NAT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
NatUnLock (VOID)
{
    if (OsixSemGive (gNatSemId) != OSIX_SUCCESS)
    {
        return (NAT_FAILURE);
    }
    return (NAT_SUCCESS);
}
#endif /* _NATMAIN_C_ */
