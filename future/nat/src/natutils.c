/********************************************************************
 * Copyright (C) Future Sotware,2002
 *
 *  $Id: natutils.c,v 1.24 2014/11/05 13:36:07 siva Exp $
 *
 *  Description:This file contains the NAT utility functions.  
 *
 ********************************************************************/

#ifndef __NAT_UTILS_C__
#define __NAT_UTILS_C__

#include "natinc.h"
#include "npapi.h"
#include "ipnp.h"
#include "fsnatlw.h"

#ifdef NPAPI_WANTED
extern INT4         ArpGetNextArpEntry
PROTO ((INT4 *pi4CfaIfIndex, UINT4 *pu4IpAddr, UINT1 *pMacAddr));
#endif

/**************************************************************************
 * Function Name :  cli_get_app_name                                       *
 *                                                                         *
 * Description   :  This function the is used to get the application name  *
 *                  corresponding to the given port number                 *
 *                                                                         *
 * Input (s)     :  u2Port - The port number                               *
 *                  pu1App - The string in which the application name      *
 *                           is stored                                     *
 *                                                                         *
 * Output (s)    :  None                                                   *
 *                                                                         *
 * Returns       :  None                                                   *
 ***************************************************************************/
VOID
cli_get_app_name (UINT2 u2Port, UINT1 *pu1App)
{
    CHR1               *pu1String = NULL;
    switch (u2Port)
    {
        case NAT_AUTH_APP_PORT:
            pu1String = "auth";
            break;
        case NAT_DNS_APP_PORT:
            pu1String = "dns";
            break;
        case NAT_FTP_APP_PORT:
            pu1String = "ftp";
            break;
        case NAT_POP3_APP_PORT:
            pu1String = "pop3";
            break;
        case NAT_PPTP_APP_PORT:
            pu1String = "pptp";
            break;
        case NAT_SMTP_APP_PORT:
            pu1String = "smtp";
            break;
        case NAT_TELNET_APP_PORT:
            pu1String = "telnet";
            break;
        case NAT_HTTP_APP_PORT:
            pu1String = "http";
            break;
        case NAT_NNTP_APP_PORT:
            pu1String = "nntp";
            break;
        case NAT_SNMP_APP_PORT:
            pu1String = "snmp";
            break;
        default:
            pu1String = "other";
            break;
    }
    CLI_STRCPY (pu1App, pu1String);
}

/**************************************************************************
 * Function Name :  cli_check_valid_ip_address                             *
 *                                                                         *
 * Description   :  This function is used to check whether a given         *
 *                  ip address is valid or not                             *
 *                                                                         *
 * Input (s)     :  u4Mask   - The mask                                    *
 *                  u4IpAddr - The IP Address                              *
 *                                                                         *
 * Output (s)    :  None                                                   *
 *                                                                         *
 * Returns       :  CLI_FAILURE  /  CLI_SUCCESS                            *
 ***************************************************************************/
UINT4
cli_check_valid_ip_address (UINT4 u4IpAddr, UINT4 u4Mask)
{
    if (u4IpAddr > NAT_CLASS_C_MAX)
    {
        /*It is a class D or Class E IP address */
        return CLI_FAILURE;
    }

    if ((u4IpAddr & NAT_LOOP_BACK_ADDR_MASK /*0xff000000 */ )
        == NAT_LOOPBACK_IPADDR)    /*0x7f000000 */
    {
        /* It is a loopback IP address ( 127.X.X.X) */
        return CLI_FAILURE;
    }
    if (u4IpAddr == NAT_INVLD_IP_ADDR_ANY)    /* 0x00000000 */
    {
        /* It is an obselete form of limited broadcast address */
        return CLI_FAILURE;
    }
    /* Check for address to be start of subnet */
    if ((u4IpAddr & u4Mask) != u4IpAddr)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/**************************************************************************
 * Function Name :  NatGetWanInterfaceIpAddr                              *
 * Description   :  This function the is used to get the WAN Interface    *
 *                  IpAddress  according to IfIndex                       *
 * Input (s)     :  i4WanIf  - WAN Interface Index                        *
 * Output (s)    :  None                                                  *
 * Returns       :  i4WanIf - WAN Interface Index                         *
 **************************************************************************/
UINT4
NatGetWanInterfaceIpAddr (UINT4 u4WanIf)
{
    UINT4               u4WanIfIpAddr = NAT_ZERO;
    INT1                i1RetVal = NAT_ZERO;

    i1RetVal = CfaGetIfIpAddr ((INT4) u4WanIf, &u4WanIfIpAddr);
    UNUSED_PARAM (i1RetVal);
    return (u4WanIfIpAddr);
}

VOID
NatConvertPortListToArray (tPortList PortList,
                           UINT4 *pu4IfPortArray, UINT1 *u1NumPorts)
{
    UINT2               u2Port = NAT_ZERO;
    UINT2               u2BytIndex = NAT_ZERO;
    UINT2               u2BitIndex = NAT_ZERO;
    UINT1               u1PortFlag = NAT_ZERO;
    UINT1               u1Counter = NAT_ZERO;
#define NAT_VLAN_PORTS_PER_BYTE  8
#define NAT_VLAN_BIT8            0x80
    for (u2BytIndex = NAT_ZERO; u2BytIndex < BRG_PORT_LIST_SIZE; u2BytIndex++)
    {
        u1PortFlag = PortList[u2BytIndex];
        for (u2BitIndex = NAT_ZERO; ((u2BitIndex < NAT_VLAN_PORTS_PER_BYTE)
                                     && (u1PortFlag != NAT_ZERO)); u2BitIndex++)
        {
            if ((u1PortFlag & NAT_VLAN_BIT8) != NAT_ZERO)
            {
                u2Port = (UINT2) ((u2BytIndex * NAT_VLAN_PORTS_PER_BYTE) +
                                  u2BitIndex + NAT_ONE);
                pu4IfPortArray[u1Counter] = u2Port;
                u1Counter++;
            }
            u1PortFlag = (UINT1) (u1PortFlag << NAT_ONE);
        }
    }
    /* update number of ports */
    *u1NumPorts = u1Counter;
}

/***************************************************************************
 * Function Name :  NatUtilTestUniqueExtPortProtoComb                      *
 *                                                                         *
 * Description   :  This function Validates the Uniqueness of the Protocol *
 *                  No and  External Port Value Combination.               *
 *                                                                         *
 * Input (s)     :  Local IP Address,Local Potr,External Port,Protocol No  *
 *                                                                         *
 * Output (s)    :  None.                                                  *
 *                                                                         *
 * Returns       :  NAT_SUCCESS/NAT_FAILURE                                *
 ***************************************************************************/

INT4
NatUtilTestUniqueExtPortProtoComb (UINT4 u4InterfaceNum, UINT4 u4LocalIpAddr,
                                   INT4 i4StartPort, INT4 i4EndPort,
                                   INT4 i4ProtoNo, INT4 i4ExtPort)
{
    UINT4               u4IfNum = NAT_ZERO;
    UINT4               u4LocIpAddr = NAT_ZERO;
    INT4                i4StartLocPort = NAT_ZERO;
    INT4                i4EndLocPort = NAT_ZERO;
    INT4                i4ProtocolNo = NAT_ZERO;
    INT4                i4TranslatedLocPort = NAT_ZERO;

    UNUSED_PARAM (u4LocalIpAddr);
    UNUSED_PARAM (i4StartPort);
    UNUSED_PARAM (i4EndPort);

    if (u4InterfaceNum == NAT_ZERO)
    {
        return NAT_FAILURE;
    }

    u4IfNum = u4InterfaceNum;

    if (nmhGetFirstIndexNatStaticNaptTable ((INT4 *) &u4IfNum,
                                            &u4LocIpAddr,
                                            &i4StartLocPort,
                                            &i4EndLocPort,
                                            &i4ProtocolNo) == SNMP_FAILURE)
    {

        NAT_TRC (NAT_TRC_ON, "First Entry\n");
        return NAT_SUCCESS;
    }

    do
    {

        if ((nmhGetNatStaticNaptTranslatedLocalPort ((INT4) u4IfNum,
                                                     u4LocIpAddr,
                                                     i4StartLocPort,
                                                     i4EndLocPort,
                                                     i4ProtocolNo,
                                                     &i4TranslatedLocPort))
            == (INT1) SNMP_FAILURE)
        {
            return NAT_FAILURE;
        }
        /* For Supporting Any(255) in PortRange Command */
        if (((i4ProtocolNo == i4ProtoNo) ||
             (i4ProtocolNo == NAT_PROTO_ANY))
            && (i4TranslatedLocPort == i4ExtPort))

        {
            NAT_TRC (NAT_TRC_ON, "External Port - Protocol "
                     "Combination Existing\n");
            return NAT_FAILURE;
        }

    }
    while ((nmhGetNextIndexNatStaticNaptTable ((INT4) u4IfNum,
                                               (INT4 *) &u4IfNum,
                                               u4LocIpAddr,
                                               &u4LocIpAddr,
                                               i4StartLocPort,
                                               &i4StartLocPort,
                                               i4EndLocPort,
                                               &i4EndLocPort,
                                               i4ProtocolNo,
                                               &i4ProtocolNo)) ==
           (INT1) SNMP_SUCCESS);

    return NAT_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : NatUtilIsIkeNattPassthroughTraffic
 *
 *    Description          : Checks whether the received packet already has 
 *                           NAT translation in its database or not
 *
 *    Input(s)             : Cru BUf
 *
 *    Output(s)            : None.
 *
 *    Returns              : OSIX_SUCCESS / OSIX_FAILURE
 *
 *****************************************************************************/
PUBLIC UINT4
NatUtilIsIkeNattPassthroughTraffic (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tHeaderInfo         HeaderInfo;
    UINT4               u4TransStatus = NAT_ZERO;

    MEMSET (&HeaderInfo, NAT_ZERO, sizeof (tHeaderInfo));
    if (gu4NatEnable == NAT_DISABLE)
    {
        return OSIX_FAILURE;
    }

    NatInitHeaderInfo (pBuf, &HeaderInfo, NAT_INBOUND, ZERO);
    u4TransStatus = NatGetDynamicEntry (&HeaderInfo, NAT_FALSE);
    if (u4TransStatus == NAT_SUCCESS)
    {
        return (OSIX_SUCCESS);
    }
    return (OSIX_FAILURE);
}

/*****************************************************************************
 *
 *    Function Name        : NatUtilIsVPNPassthroughTraffic
 *
 *    Description          : Checks whether the given initiator cookie is 
 *                           present in NAT database or not
 *
 *    Input(s)             : pu1InitiatorCookie - Pointer to initiator cookie
 *
 *    Output(s)            : None.
 *
 *    Returns              : OSIX_SUCCESS / OSIX_FAILURE
 *
 *****************************************************************************/
PUBLIC UINT4
NatUtilIsVPNPassthroughTraffic (UINT1 *pu1InitiatorCookie)
{
    UINT4               u4HashKey = NAT_ZERO;
    tIKEHashNode       *pIKEHashNode = NULL;

    /* If NAT is disabled then return FAILURE */
    if (gu4NatEnable == NAT_DISABLE)
    {
        return (OSIX_FAILURE);
    }

    u4HashKey = natFormIKEHashKey (pu1InitiatorCookie);
    TMO_HASH_Scan_Bucket (gpNatIKEHashList, u4HashKey, pIKEHashNode,
                          tIKEHashNode *)
    {
        if ((MEMCMP (pu1InitiatorCookie, pIKEHashNode->au1InitCookie,
                     NAT_IKE_COOKIE_LENGTH) == NAT_ZERO))
        {
            return (OSIX_SUCCESS);
        }
    }
    return (OSIX_FAILURE);
}

/***************************************************************************
 *
 * Function Name : NatUtilAddPartialList
 *
 * Description   : This Function is used to Add Entry in Partial List
 *                 for a request
 * 
 * Inputs        : u4SrcIpAddr  - Source IP Address
 *                 u4DestIpAddr - Destination IP Address
 *                 u2SrcPort    - Source Port
 *                 u2DestPort   - Destination Port
 *                 u2ProtocolId - Protocol Identifier
 *                 u2Direction  - Direction of flow
 *                 i4IfNum      - Interface Id
 *                 u1CallOnHold - Call hold option
 * 
 * Output        : None
 *
 * Returns       : NAT_SUCCESS/NAT_FAILURE
 *
 * ************************************************************************/

INT4
NatUtilAddPartialList (UINT4 u4SrcIpAddr, UINT4 u4DestIpAddr,
                       UINT2 u2SrcPort, UINT2 u2DestPort,
                       UINT4 u4TransIpAddr, INT4 *pi4TransPort,
                       UINT2 u2ProtocolId, UINT2 u2Direction,
                       INT4 i4IfNum, UINT1 u1CallOnHold)
{
    UINT4               u4CurrTime = NAT_ZERO;
    INT4                i4RetVal = NAT_SUCCESS;
    INT4                i4RetVal1 = NAT_SUCCESS;
    INT4                i4RetVal2 = NAT_SUCCESS;
    tNatSipInfo         NatSipInfo;
    tNatPartialLinkNode *pInPartialNode = NULL;
    tNatPartialLinkNode *pOutPartialNode = NULL;

    UNUSED_PARAM (u4SrcIpAddr);
    UNUSED_PARAM (u2SrcPort);

    NAT_GET_SYS_TIME (&u4CurrTime);

    MEMSET (&NatSipInfo, NAT_ZERO, sizeof (tNatSipInfo));
    NatSipInfo.i4IfNum = i4IfNum;

    NatSipInfo.u4TransIp = u4TransIpAddr;
    NatSipInfo.u2TransPort = (UINT2) *pi4TransPort;

    NatSipInfo.u4RemoteIp = u4DestIpAddr;
    NatSipInfo.u2RemotePort = u2DestPort;

    NatSipInfo.u4LocalIp = u4SrcIpAddr;
    NatSipInfo.u2LocalPort = u2SrcPort;

    NatSipInfo.u1Direction = (UINT1) u2Direction;
    NatSipInfo.u1Protocol = (UINT1) u2ProtocolId;
    NatSipInfo.u1CallHold = u1CallOnHold;

    if (NatUtilScanPartialLinksList (&NatSipInfo, &pInPartialNode,
                                     &pOutPartialNode) == NAT_FAILURE)
    {
        if (NatSipInfo.u1Direction == NAT_ANY)
        {
            NatSipInfo.u1Direction = NAT_INBOUND;
            /* Add for inbound direction */
            i4RetVal1 = (INT4) NatUtilAddToPartialList (&NatSipInfo,
                                                        NAT_SIP_PERSISTENT);

            NatSipInfo.u1Direction = NAT_OUTBOUND;

            /* Add for outbound direction */
            i4RetVal2 = (INT4) NatUtilAddToPartialList (&NatSipInfo,
                                                        NAT_SIP_PERSISTENT);
        }
        else
        {
            i4RetVal = (INT4)
                NatUtilAddToPartialList (&NatSipInfo, NAT_SIP_PERSISTENT);
        }
    }
    else
    {
        if (pInPartialNode != NULL)
        {
            *pi4TransPort = pInPartialNode->u2TranslatedLocPort;
            i4RetVal = NATIPC_BINDING_ALREADY_EXISTS;
        }
        else if (pOutPartialNode != NULL)
        {
            *pi4TransPort = pOutPartialNode->u2TranslatedLocPort;
            i4RetVal = NATIPC_PINHOLE_ALREADY_EXISTS;
        }
        else
        {
            *pi4TransPort = NAT_ZERO;
        }
    }

    if ((i4RetVal1 == NATIPC_BINDING_ALREADY_EXISTS) ||
        (i4RetVal2 == NATIPC_BINDING_ALREADY_EXISTS) ||
        (i4RetVal == NATIPC_BINDING_ALREADY_EXISTS))
    {
        i4RetVal = NATIPC_BINDING_ALREADY_EXISTS;
    }

    return (i4RetVal);
}

/***************************************************************************
 *
 * Function Name : NatUtilDelPartialList
 *
 * Description   : This Function is used to Delete Entry in Partial List
 *                 for a request
 * 
 * Inputs        : u4SrcIpAddr   - Source IP Address
 *                 u2SrcPort     - Source Port
 *                 u4DestIpAddr  - Destination IP Address
 *                 u2DestPort    - Destination Port
 *                 u1ProtocolId  - Protocol identifier
 *                 u1Direction   - Direction of flow
 *                 i4IfNum       - Interface Id
 *                 u1CallHoldOpt - Call hold option
 * 
 * Output        : None
 *
 * Returns       : NAT_SUCCESS/NAT_FAILURE
 *
 * ************************************************************************/

INT4
NatUtilDelPartialList (UINT4 u4SrcIpAddr, UINT2 u2SrcPort,
                       UINT4 u4DestIpAddr, UINT2 u2DestPort,
                       UINT4 u4TransIpAddr, UINT2 u2TransPort,
                       UINT1 u1ProtocolId, UINT1 u1Direction,
                       INT4 i4IfNum, UINT1 u1CallHoldOpt)
{
    tNatPartialLinkNode *pNatPartialLinkNode = NULL;
    tNatPartialLinkNode *pNextNode = NULL;
#ifdef FIREWALL_WANTED
    tPartialInfo        PartialCmnInfo;
#endif
    UINT1               u1RetVal = NAT_FAILURE;

    UNUSED_PARAM (i4IfNum);

#ifdef FIREWALL_WANTED
    PartialCmnInfo.LocalIP.v4Addr = u4SrcIpAddr;
    PartialCmnInfo.u2LocalPort = u2SrcPort;

    PartialCmnInfo.RemoteIP.v4Addr = u4DestIpAddr;
    PartialCmnInfo.u2RemotePort = u2DestPort;

    PartialCmnInfo.u1Direction = u1Direction;
    PartialCmnInfo.u1Protocol = u1ProtocolId;
    PartialCmnInfo.u1PersistFlag = NAT_SIP_PERSISTENT;
    PartialCmnInfo.u1AppCallStatus = u1CallHoldOpt;
#else
    UNUSED_PARAM (u1CallHoldOpt);
#endif

    pNatPartialLinkNode =
        (tNatPartialLinkNode *) TMO_SLL_First (&gNatPartialLinksList);

    while (pNatPartialLinkNode)
    {
        pNextNode = (tNatPartialLinkNode *) TMO_SLL_Next (&gNatPartialLinksList,
                                                          (tTMO_SLL_NODE
                                                           *) (VOID *)
                                                          pNatPartialLinkNode);

        if ((pNatPartialLinkNode->u1PktType == u1ProtocolId) &&
            (pNatPartialLinkNode->u1Direction == u1Direction) &&
            (pNatPartialLinkNode->u1PersistFlag == NAT_SIP_PERSISTENT))
        {
            if ((u4SrcIpAddr != NAT_ZERO) &&
                (pNatPartialLinkNode->u4LocIpAddr != u4SrcIpAddr))
            {
                pNatPartialLinkNode = pNextNode;
                continue;
            }

            if ((u2SrcPort != NAT_ZERO) &&
                (pNatPartialLinkNode->u2LocPort != u2SrcPort))
            {
                pNatPartialLinkNode = pNextNode;
                continue;
            }

            if ((u4DestIpAddr != NAT_ZERO) &&
                (pNatPartialLinkNode->u4OutIpAddr != u4DestIpAddr))
            {
                pNatPartialLinkNode = pNextNode;
                continue;
            }

            if ((u2DestPort != NAT_ZERO) &&
                (pNatPartialLinkNode->u2OutPort != u2DestPort))
            {
                pNatPartialLinkNode = pNextNode;
                continue;
            }

            if ((u2TransPort != NAT_ZERO) &&
                (pNatPartialLinkNode->u2TranslatedLocPort != u2TransPort))
            {
                pNatPartialLinkNode = pNextNode;
                continue;
            }

            if ((u4TransIpAddr != NAT_ZERO) &&
                (pNatPartialLinkNode->u4TranslatedLocIpAddr != u4TransIpAddr))
            {
                pNatPartialLinkNode = pNextNode;
                continue;
            }

            TMO_SLL_Delete (&gNatPartialLinksList,
                            (tTMO_SLL_NODE *) (VOID *) pNatPartialLinkNode);
            NatMemReleaseMemBlock (NAT_PARTIAL_LINKS_LIST_POOL_ID,
                                   (UINT1 *) pNatPartialLinkNode);

            u1RetVal = NAT_SUCCESS;
            /* Fix done */
            break;
        }

        pNatPartialLinkNode = pNextNode;
    }

#ifdef FIREWALL_WANTED
    /* Deleting firewall pinhole */
    FwlClosePinholeEntry (&PartialCmnInfo);
#endif

    if (u1RetVal != NAT_SUCCESS)
    {
        return (NATIPC_PINHOLE_NOT_EXISTS);
    }

    return (u1RetVal);
}

/***************************************************************************
 * Function Name : NatUtilScanPartialLinksList                             *
 *                                                                         *
 * Description   : This Function searches the partial list for a           * 
 *                 particular entry.                                       *   
 *                                                                         *
 * Input (s)     : pNatSipInfo   - Pointer to tNatSipInfo                  *
 *                 pInPartialNode  - Pointer to Partial Node for inbound   *        
 *                                   direction                             *        
 *                 pOutPartialNode - Pointer to Partial Node for outbound  *
 *                                   direction                             *
 * Output (s)    : None.                                                   *
 *                                                                         *
 * Returns       : NAT_SUCCESS/NAT_FAILURE                                 *
 *                                                                         *
 ***************************************************************************/

INT4
NatUtilScanPartialLinksList (tNatSipInfo * pNatSipInfo,
                             tNatPartialLinkNode ** pNatInPartialLink,
                             tNatPartialLinkNode ** pNatOutPartialLink)
{
    tNatPartialLinkNode *pNatPartialLinkNode = NULL;

    pNatPartialLinkNode = NULL;

    if ((pNatSipInfo->u1Protocol == NAT_TCP) ||
        (pNatSipInfo->u1Protocol == NAT_UDP))
    {
        TMO_SLL_Scan (&gNatPartialLinksList, pNatPartialLinkNode,
                      tNatPartialLinkNode *)
        {
            if (((pNatSipInfo->u1Direction == NAT_INBOUND) &&
                 (NatUtilVerifyInTuple (pNatSipInfo, pNatPartialLinkNode)
                  == NAT_SUCCESS)) ||
                ((pNatSipInfo->u1Direction == NAT_OUTBOUND) &&
                 (NatUtilVerifyOutTuple (pNatSipInfo, pNatPartialLinkNode)
                  == NAT_SUCCESS)))
            {
                *pNatInPartialLink = NULL;
                *pNatOutPartialLink = NULL;

                if (pNatSipInfo->u1Direction == NAT_INBOUND)
                {
                    *pNatInPartialLink = pNatPartialLinkNode;
                }
                else
                {
                    *pNatOutPartialLink = pNatPartialLinkNode;
                }
                break;
            }
            else
            {
                if (pNatSipInfo->u1Direction == NAT_ANY)
                {
                    if (NatUtilVerifyInTuple (pNatSipInfo, pNatPartialLinkNode)
                        == NAT_SUCCESS)
                    {
                        *pNatInPartialLink = pNatPartialLinkNode;
                    }
                    else if (NatUtilVerifyOutTuple (pNatSipInfo,
                                                    pNatPartialLinkNode) ==
                             NAT_SUCCESS)
                    {
                        *pNatOutPartialLink = pNatPartialLinkNode;
                    }
                }
            }
        }

    }

    if (pNatPartialLinkNode != NULL)
    {
        return NAT_SUCCESS;
    }

    return NAT_FAILURE;

}

/***************************************************************************
 * Function Name : NatUtilVerifyInTuple                                    *
 *                                                                         *
 * Description   : This Function matches the partial node tuple for out    *
 *                 direction.                                              *
 *                                                                         *
 * Input (s)     :  pNatSipInfo   - Pointer to tNatSipInfo                 *
 *                  pNatPartialLinkNode  - Pointer to Partial Node         *
 *                                                                         *
 * Output (s)    :  None.                                                  *
 *                                                                         *
 * Returns       :  NAT_SUCCESS/ NAT_FAILURE                               *
 *                                                                         *
 ***************************************************************************/

INT4
NatUtilVerifyInTuple (tNatSipInfo * pNatSipInfo, tNatPartialLinkNode
                      * pNatPartialLinkNode)
{
    if ((pNatSipInfo == NULL) || (pNatPartialLinkNode == NULL))
    {
        return (NAT_FAILURE);
    }

    if ((pNatPartialLinkNode->u4LocIpAddr ==
         pNatSipInfo->u4LocalIp) &&
        (pNatPartialLinkNode->u2LocPort ==
         pNatSipInfo->u2LocalPort) &&
        (pNatPartialLinkNode->u4TranslatedLocIpAddr ==
         pNatSipInfo->u4TransIp) &&
        (pNatPartialLinkNode->u1Direction ==
         pNatSipInfo->u1Direction) &&
        (pNatPartialLinkNode->u1PktType == pNatSipInfo->u1Protocol))
    {
        return (NAT_SUCCESS);
    }

    return (NAT_FAILURE);

}

/***************************************************************************
 * Function Name : NatUtilVerifyOutTuple                                   *
 *                                                                         *
 * Description   : This Function matches the partial node tuple for out    *
 *                 direction.                                              *
 *                                                                         *
 * Input (s)     :  pNatSipInfo   - Pointer to tNatSipInfo                 *
 *                  pNatPartialLinkNode  - Pointer to Partial Node         *
 *                                                                         *
 * Output (s)    :  None.                                                  *
 *                                                                         *
 * Returns       :  NAT_SUCCESS/ NAT_FAILURE                               *
 *                                                                         *
 ***************************************************************************/

INT4
NatUtilVerifyOutTuple (tNatSipInfo * pNatSipInfo, tNatPartialLinkNode
                       * pNatPartialLinkNode)
{
    if ((pNatSipInfo == NULL) || (pNatPartialLinkNode == NULL))
    {
        return (NAT_FAILURE);
    }

    if ((pNatPartialLinkNode->u1Direction ==
         pNatSipInfo->u1Direction) &&
        (pNatPartialLinkNode->u1PktType == pNatSipInfo->u1Protocol))
    {

        if ((pNatSipInfo->u4RemoteIp != NAT_ZERO) &&
            (pNatPartialLinkNode->u4OutIpAddr != pNatSipInfo->u4RemoteIp))
        {
            return (NAT_FAILURE);
        }

        if ((pNatSipInfo->u2RemotePort != NAT_ZERO) &&
            (pNatPartialLinkNode->u2OutPort != pNatSipInfo->u2RemotePort))
        {
            return (NAT_FAILURE);
        }

        if ((pNatSipInfo->u4LocalIp != NAT_ZERO)
            && (pNatPartialLinkNode->u4LocIpAddr != pNatSipInfo->u4LocalIp))
        {
            return (NAT_FAILURE);
        }

        if ((pNatSipInfo->u2LocalPort != NAT_ZERO)
            && (pNatPartialLinkNode->u2LocPort != pNatSipInfo->u2LocalPort))
        {
            return (NAT_FAILURE);
        }

    }
    else
    {
        return (NAT_FAILURE);
    }

    return (NAT_SUCCESS);

}

/****************************************************************************
 *
 * Function Name : NatUtilAddToPartialList
 *
 * Description   : Adds entry in partial list for SIP
 *
 * Inputs        : pNatSipInfo -> Pointer to tNatSipInfo
 *                 u1PersistFlag -> Persistant Flag
 *
 * Output        : None 
 *
 * Returns       : NAT_SUCCESS/NAT_FAILURE
 *
 * **************************************************************************/

UINT4
NatUtilAddToPartialList (tNatSipInfo * pNatSipInfo, UINT1 u1PersistFlag)
{
    tNatPartialLinkNode *pNatPartialLinkNode = NULL;
    UINT4               u4CurrTime = NAT_ZERO;
#ifdef FIREWALL_WANTED
    tPartialInfo        sPartialInfo;
#endif

    NAT_GET_SYS_TIME (&u4CurrTime);

    if (pNatPartialLinkNode == NULL)
    {
        if (NAT_MEM_ALLOCATE (NAT_PARTIAL_LINKS_LIST_POOL_ID,
                              pNatPartialLinkNode, tNatPartialLinkNode) == NULL)
        {
            return NAT_FAILURE;
        }

        MEMSET (pNatPartialLinkNode, NAT_ZERO, sizeof (tNatPartialLinkNode));

        /* Adding partial link for INBOUND direction */
        pNatPartialLinkNode->u4TranslatedLocIpAddr = pNatSipInfo->u4TransIp;
        pNatPartialLinkNode->u2TranslatedLocPort = pNatSipInfo->u2TransPort;
        pNatPartialLinkNode->u4OutIpAddr = pNatSipInfo->u4RemoteIp;
        pNatPartialLinkNode->u4LocIpAddr = pNatSipInfo->u4LocalIp;
        pNatPartialLinkNode->u2OutPort = pNatSipInfo->u2RemotePort;
        pNatPartialLinkNode->u2LocPort = pNatSipInfo->u2LocalPort;
        pNatPartialLinkNode->u4TimeStamp = u4CurrTime;
        pNatPartialLinkNode->u1Direction = pNatSipInfo->u1Direction;
        pNatPartialLinkNode->u1PktType = pNatSipInfo->u1Protocol;
        pNatPartialLinkNode->u1PersistFlag = u1PersistFlag;
        pNatPartialLinkNode->u1AppCallStatus = pNatSipInfo->u1CallHold;

#ifdef FIREWALL_WANTED
        MEMSET (&sPartialInfo, NAT_ZERO, sizeof (tPartialInfo));
        sPartialInfo.RemoteIP.v4Addr = pNatPartialLinkNode->u4OutIpAddr;
        sPartialInfo.RemoteIP.u4AddrType = IP_VERSION_4;
        sPartialInfo.u2RemotePort = pNatPartialLinkNode->u2OutPort;
        sPartialInfo.LocalIP.v4Addr = pNatPartialLinkNode->u4LocIpAddr;
        sPartialInfo.LocalIP.u4AddrType = IP_VERSION_4;
        sPartialInfo.u2LocalPort = pNatPartialLinkNode->u2LocPort;
        sPartialInfo.u1Protocol = pNatPartialLinkNode->u1PktType;
        sPartialInfo.u1PersistFlag = pNatPartialLinkNode->u1PersistFlag;
        sPartialInfo.u2InterfaceNum = (UINT2) pNatSipInfo->i4IfNum;
        sPartialInfo.u1Direction = pNatSipInfo->u1Direction;

        /* Entry in Partial Info for Call Hold / Unhold */
        sPartialInfo.u1AppCallStatus = pNatSipInfo->u1CallHold;

        FwlAddPartialLink (&sPartialInfo);
#endif
        /* For SIP RTP PORT */
        TMO_SLL_Add (&gNatPartialLinksList,
                     (tTMO_SLL_NODE *) (VOID *) pNatPartialLinkNode);

    }

    return NAT_SUCCESS;
}

/***************************************************************************
 * Function Name :  NatUtilUpdateCallHoldInfo                              *
 *                                                                         *
 * Description   : This Function Updates the call hold flags in different  *
 *                 tables.                                                 *
 *                                                                         *
 * Input (s)     :  pNatSipInfo   - Pointer to tNatSipInfo                 *
 *                  pInPartialNode  - Pointer to Partial Node for inbound  *  
 *                  direction                                              *        
 *                  pOutPartialNode - Pointer to Partial Node for outbound *
 *                  direction                                              *
 * Output (s)    :  None.                                                  *
 *                                                                         *
 * Returns       :  NAT_SUCCESS/ NAT_FAILURE                               *
 *                                                                         *
 ***************************************************************************/

INT4
NatUtilUpdateCallHoldInfo (tNatSipInfo * pNatSipInfo,
                           tNatPartialLinkNode * pInPartialNode,
                           tNatPartialLinkNode * pOutPartialNode)
{

    if ((pNatSipInfo == NULL) || ((pOutPartialNode == NULL) &&
                                  (pInPartialNode == NULL)))
    {
        return (NAT_FAILURE);
    }

    if (pOutPartialNode != NULL)
    {
        if (pOutPartialNode->u1AppCallStatus == pNatSipInfo->u1CallHold)
        {
            if (pInPartialNode == NULL)
            {
                return (NATIPC_PINHOLE_ALREADY_EXISTS);
            }
        }

        /* Update the Hold Status for the Partial Entry */
        pOutPartialNode->u1AppCallStatus = pNatSipInfo->u1CallHold;

#ifdef FIREWALL_WANTED
        /* Updating Firewall Call Status */
        NatUtilFwlAddPartialLink (pOutPartialNode->u4LocIpAddr,
                                  pOutPartialNode->u2LocPort,
                                  pOutPartialNode->u4OutIpAddr,
                                  pOutPartialNode->u2OutPort, pOutPartialNode->
                                  u1PktType, pNatSipInfo->u1Direction,
                                  pNatSipInfo->u1CallHold);
#endif
    }

    if (pInPartialNode != NULL)
    {
        if (pInPartialNode->u1AppCallStatus == pNatSipInfo->u1CallHold)
        {
            if (pOutPartialNode == NULL)
            {
                return (NATIPC_PINHOLE_ALREADY_EXISTS);
            }
        }

        /* Update the Hold Status for the Partial Entry */
        pInPartialNode->u1AppCallStatus = pNatSipInfo->u1CallHold;

#ifdef FIREWALL_WANTED
        /* Updating Firewall Call Status */
        NatUtilFwlAddPartialLink (pInPartialNode->u4LocIpAddr, pInPartialNode->
                                  u2LocPort, pInPartialNode->u4OutIpAddr,
                                  pInPartialNode->u2OutPort, pInPartialNode->
                                  u1PktType, pNatSipInfo->u1Direction,
                                  pNatSipInfo->u1CallHold);
#endif
    }

    return (NAT_SUCCESS);
}

#ifdef FIREWALL_WANTED
INT4
NatUtilFwlAddPartialLink (UINT4 u4LocalIP, UINT2 u2LocPort, UINT4 u4RemoteIP,
                          UINT2 u2RemotePort, UINT1 u1Protocol,
                          UINT1 u1Direction, UINT1 u1AppCallStatus)
{
    tPartialInfo        sPartialInfo;

    MEMSET (&sPartialInfo, NAT_ZERO, sizeof (tPartialInfo));
    sPartialInfo.RemoteIP.v4Addr = u4RemoteIP;
    sPartialInfo.RemoteIP.u4AddrType = IP_VERSION_4;
    sPartialInfo.u2RemotePort = u2RemotePort;
    sPartialInfo.LocalIP.v4Addr = u4LocalIP;
    sPartialInfo.LocalIP.u4AddrType = IP_VERSION_4;
    sPartialInfo.u2LocalPort = u2LocPort;
    sPartialInfo.u1Protocol = u1Protocol;
    sPartialInfo.u1PersistFlag = NAT_SIP_PERSISTENT;
    sPartialInfo.u2InterfaceNum = NAT_ZERO;
    sPartialInfo.u1Direction = u1Direction;

    /* Entry in Partial Info for Call Hold / Unhold */
    sPartialInfo.u1AppCallStatus = u1AppCallStatus;

    FwlAddPartialLink (&sPartialInfo);

    return (NAT_SUCCESS);

}
#endif

/*****************************************************************************
 *
 *    Function Name        : SipClearAllInterfaceBindings
 *
 *    Description          : This function is for cleaning all the bindings
 *
 *    Input(s)             : i4IfNum - Interface Index
 *
 *    Output(s)            : None.
 *
 *    Returns              : NAT_SUCCESS/NAT_FAILURE.
 *
 *****************************************************************************/
INT4
SipClearAllInterfaceBindings (INT4 i4IfNum)
{
    tStaticNaptEntry   *pStaticNaptEntryNode = NULL;
    tStaticNaptEntry   *pNextStaticNapt = NULL;
    tInterfaceInfo     *pIfNode = NULL;
    tNatPartialLinkNode *pNatPartialLinkNode = NULL;
    tNatPartialLinkNode *pNatPartialNextNode = NULL;

    /* Clearing Port Map Entries */
    pIfNode = gapNatIfTable[i4IfNum];

    if (pIfNode != NULL)
    {
        /* Scanning the StaticNAPT List */
        pStaticNaptEntryNode =
            (tStaticNaptEntry *) TMO_SLL_First (&pIfNode->StaticNaptList);

        while (pStaticNaptEntryNode != NULL)
        {
            if ((pStaticNaptEntryNode->u1AppCallStatus == NAT_ON_HOLD) ||
                (pStaticNaptEntryNode->u1AppCallStatus == NAT_NOT_ON_HOLD))
            {
                pNextStaticNapt = (tStaticNaptEntry *) TMO_SLL_Next
                    (&pIfNode->StaticNaptList,
                     (tTMO_SLL_NODE *) (VOID *) pStaticNaptEntryNode);

                /* Cleaning virtual server entry */
                TMO_SLL_Delete (&pIfNode->StaticNaptList,
                                (tTMO_SLL_NODE *) (VOID *)
                                pStaticNaptEntryNode);

                NatMemReleaseMemBlock (NAT_STATIC_NAPT_POOL_ID,
                                       (UINT1 *) pStaticNaptEntryNode);
            }
            else
            {
                pNextStaticNapt = (tStaticNaptEntry *) TMO_SLL_Next
                    (&pIfNode->StaticNaptList,
                     (tTMO_SLL_NODE *) (VOID *) pStaticNaptEntryNode);

            }

            pStaticNaptEntryNode = pNextStaticNapt;

        }

        /* Scan the partial list and delete all SIP entries */
        pNatPartialLinkNode =
            (tNatPartialLinkNode *) TMO_SLL_First (&gNatPartialLinksList);

        while (pNatPartialLinkNode != NULL)
        {
            pNatPartialNextNode =
                (tNatPartialLinkNode *) TMO_SLL_Next (&gNatPartialLinksList,
                                                      (tTMO_SLL_NODE *) (VOID *)
                                                      pNatPartialLinkNode);

            if ((pNatPartialLinkNode->u1AppCallStatus != NAT_ON_HOLD) &&
                (pNatPartialLinkNode->u1AppCallStatus != NAT_NOT_ON_HOLD))
            {
                pNatPartialLinkNode = pNatPartialNextNode;
                continue;
            }

            TMO_SLL_Delete (&gNatPartialLinksList,
                            (tTMO_SLL_NODE *) (VOID *) pNatPartialLinkNode);
            NatMemReleaseMemBlock (NAT_PARTIAL_LINKS_LIST_POOL_ID,
                                   (UINT1 *) pNatPartialLinkNode);
            pNatPartialLinkNode = pNatPartialNextNode;
        }
    }

#ifdef FIREWALL_WANTED
    FwlCleanAllAppEntry ();
#endif

    return (NAT_SUCCESS);
}

/*******************************************************************************
 *                                                                              
 * Function     : NatUpdatePartialList                                          
 *                                                                              
 * Description  : This funtion is used to update the OUT partial list with      
 *                NAT IP/Port when data flows from LAN to WAN before 200 OK     
 *                                                                              
 * Input        : pNatWanUaHashNode -Hash node having OUT IP/Port and N pinholes
 *                u4Direction - Direction of data flow                          
 *                u4NatIpAdd - NAT IP                                           
 *                u2NatBasePort - NAT Base Port                                 
 *                u1PktType - Packet type      
 *            
 *
 * 
 * Output       : None                                           
 * 
 * Return       : NAT_SUCCESS/NAT_FAILURE  
 * 
 * 
 * 
 *****************************************************************************/
UINT4
NatUpdatePartialList (tNatWanUaHashNode * pNatWanUaHashNode, UINT4 u4Direction,
                      UINT4 u4NatIpAdd, UINT2 u2NatBasePort, UINT1 u1PktType)
{

    tNatPartialLinkNode *pNatPartialLinkNode = NULL;
    UINT4               u4OutIpAdd = pNatWanUaHashNode->u4OutIpAdd;
    UINT2               u2OutBasePort = pNatWanUaHashNode->u2OutBasePort;
    UINT2               u2TotalBindings = pNatWanUaHashNode->u2TotalBindings;
    UINT2               u2PortOffset;
    UINT2               u2UpdateCount = NAT_ZERO;

    TMO_SLL_Scan (&gNatPartialLinksList, pNatPartialLinkNode,
                  tNatPartialLinkNode *)
    {
        if ((pNatPartialLinkNode->u1Direction == u4Direction) &&
            (pNatPartialLinkNode->u1PktType == u1PktType))
        {
            if (pNatPartialLinkNode->u4OutIpAddr != u4OutIpAdd)
            {
                /* OUT IP should match with that of partial entry */
                continue;
            }

            /* Find if OUT port falls in the range stored in the hash node */
            if ((pNatPartialLinkNode->u2OutPort >= u2OutBasePort) &&
                (pNatPartialLinkNode->u2OutPort <
                 u2OutBasePort + u2TotalBindings))
            {
                /* offset is the diff between base port and partial outport */
                u2PortOffset = (UINT2) (pNatPartialLinkNode->u2OutPort -
                                        u2OutBasePort);
                pNatPartialLinkNode->u4TranslatedLocIpAddr = u4NatIpAdd;
                /* Offset is added to the NAT base port */
                pNatPartialLinkNode->u2TranslatedLocPort =
                    (UINT2) (u2NatBasePort + u2PortOffset);
                u2UpdateCount++;
                if (u2UpdateCount == u2TotalBindings)
                {
                    /* Count when equals to total bindings updated,return */
                    return NAT_SUCCESS;
                }
            }
        }
    }
    NAT_TRC2 (NAT_TRC_ON, "Partial list getting updated with wrong count %d "
              "instead of bindings %d \n ", u2UpdateCount, u2TotalBindings);
    NAT_DBG2 (NAT_DBG_ALL, "Partial list getting updated with wrong count %d "
              "instead of bindings %d \n ", u2UpdateCount, u2TotalBindings);

    return NAT_FAILURE;
}

/**************************************************************************
* Function Name :  NatSetDebugLevel                                       *
*                                                                         *
* Description   :  This function sets the given debug level               *
*                                                                         *
* Input (s)     :  u4TraceLevel - Debug trace level                       *
*                                                                         *
* Output (s)    :  None                                                   *
* Returns       :  OSIX_SUCCESS/OSIX_FAILURE                              *
**************************************************************************/
INT4
NatSetDebugLevel (UINT4 u4TraceLevel)
{
    gu4NatTrc |= u4TraceLevel;

    return OSIX_SUCCESS;
}

/**************************************************************************
* Function Name :  NatResetDebugLevel                                     *
*                                                                         *
* Description   :  This function resets the given debug level             *
*                                                                         *
* Input (s)     :  u4TraceLevel - Debug trace level                       *
*                                                                         *
* Output (s)    :  None                                                   *
* Returns       :  OSIX_SUCCESS/OSIX_FAILURE                              *
**************************************************************************/
INT4
NatResetDebugLevel (UINT4 u4TraceLevel)
{
    gu4NatTrc &= (~(u4TraceLevel));

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : NatAllocateMemory
 *
 *    Description        : This function allocates memory for per interface
 *                         nat configuration table .
 *
 *    Input(s)           : INT4 i4IfNum - interface identifier
 *
 *    Output(s)          : None.
 *
 *    Returns            : tInterfaceInfo
 *
 *****************************************************************************/

tInterfaceInfo     *
NatAllocateMemory (INT4 i4IfNum)
{
    if (gapNatIfTable[i4IfNum] == NULL)
    {
        if (NAT_MEM_ALLOCATE
            (NAT_INTERFACE_INFO_POOL_ID,
             gapNatIfTable[i4IfNum], tInterfaceInfo) == NULL)
        {
            return NULL;
        }
        NATIFTABLE_INITIALIZE (gapNatIfTable[i4IfNum], NAT_STATUS_INVLD);
    }
    return gapNatIfTable[i4IfNum];
}

/******************************************************************************/
/*  Function Name              : FsNatDisableOnIntf                           */
/*  Description                : Disable NAT on a given IP interface          */
/*  Input(s)                   : i4Intf - Ip interface                        */
/*  Output(s)                  : None                                         */
/*  Global Variables Referred  : None                                         */
/*  Global variables Modified  : None                                         */
/*  Exceptions                 : None                                         */
/*  Use of Recursion           : None                                         */
/*  Returns                    : None                                         */
/******************************************************************************/
VOID
FsNatDisableOnIntf (INT4 i4Intf)
{
    /* This function deletes ARP entries from the hardware */
#ifdef NPAPI_WANTED
    UINT1               au1NxtMacAddr[CFA_ENET_ADDR_LEN];
    INT4                i4CfaNxtIfIndex = NAT_ONE;
    UINT4               u4NxtIpAddress = NAT_ZERO;
    UINT2               u2VlanId = NAT_ZERO;
    UINT1               bu1TblFull = FNP_FALSE;

    if (SEC_KERN != gi4SysOperMode)
    {
        if ((CfaGetVlanId ((UINT4) i4Intf, &u2VlanId) == CFA_FAILURE) ||
            (u2VlanId == NAT_ZERO))
        {
            return;
        }

        while (ArpGetNextArpEntry
               (&i4CfaNxtIfIndex, &u4NxtIpAddress, au1NxtMacAddr)
               != NAT_INVALID)
        {
            if (i4CfaNxtIfIndex == i4Intf)
            {
                NatFsNpIpv4ArpAdd (u2VlanId, i4CfaNxtIfIndex, u4NxtIpAddress,
                                   au1NxtMacAddr, NULL, NAT_ZERO, (VOID *)(&bu1TblFull));
                if (bu1TblFull == FNP_TRUE)
                {
                    break;
                }
            }
            else if (i4CfaNxtIfIndex > i4Intf)
            {
                break;
            }
        }
    }

#else
    UNUSED_PARAM (i4Intf);
#endif

    return;

}

/******************************************************************************/
/*  Function Name              : FsNatEnableOnIntf                            */
/*  Description                : Enables Nat on the given IP Interface        */
/*  Input(s)                   : i4Intf - IP interface                        */
/*  Output(s)                  : None                                         */
/*  Global Variables Referred  : None                                         */
/*  Global variables Modified  : None                                         */
/*  Exceptions                 : None                                         */
/*  Use of Recursion           : None                                         */
/*  Returns                    : None                                         */
/******************************************************************************/

INT4
FsNatEnableOnIntf (INT4 i4Intf)
{
    /* This function deletes ARP entries from the hardware */
#ifdef NPAPI_WANTED
    UINT1               au1NxtMacAddr[CFA_ENET_ADDR_LEN];
    INT4                i4CfaNxtIfIndex = NAT_ONE;
    UINT4               u4NxtIpAddress = NAT_ZERO;
    UINT2               u2VlanId = NAT_ZERO;
    UINT1               bu1TblFull = FNP_FALSE;

    if (SEC_KERN != gi4SysOperMode)
    {
        if ((CfaGetVlanId ((UINT4) i4Intf, &u2VlanId) == CFA_FAILURE) ||
            (u2VlanId == NAT_ZERO))
        {
            return NAT_FAILURE;
        }

        while (ArpGetNextArpEntry
               (&i4CfaNxtIfIndex, &u4NxtIpAddress, au1NxtMacAddr)
               != NAT_INVALID)
        {
            if (i4CfaNxtIfIndex == i4Intf)
            {
                NatFsNpIpv4ArpDel (u4NxtIpAddress, NULL, NAT_ZERO);
                if (bu1TblFull == FNP_TRUE)
                {
                    break;
                }
            }
            else if (i4CfaNxtIfIndex > i4Intf)
            {
                break;
            }
        }
    }
#else
    UNUSED_PARAM (i4Intf);
#endif
    return (NAT_SUCCESS);
}

/***************************************************************************
 * Function Name    :  NatUtilGetGlobalNatStatus
 * Description      :  This function returns the global NAT status
 *
 * Input (s)        :  None.
 *
 * Output (s)       :  None.
 * Returns          :  NAT_ENABLE/NAT_DISABLE.
 ***************************************************************************/
INT4
NatUtilGetGlobalNatStatus ()
{
    return ((INT4) gu4NatEnable);
}

#endif /* __NAT_UTILS_C__ */
