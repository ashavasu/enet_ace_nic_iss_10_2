/********************************************************************
*  Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
*  $Id: ntpolicy.c,v 1.7 2014/02/14 13:57:30 siva Exp $
*
*  Description:  This file contains access routines  for performing
*                well defined operations in the nat policy table.
*
********************************************************************/
#ifndef NTPOLICY_C_
#define NTPOLICY_C_

#include "natinc.h"

/* NAT Policy Initialization */
/*****************************************************************************/
/* Function Name      : NatPolicyInitLists                                   */
/* Description        : This function is used to initialize the static and   */
/*                      dynamic linked list.                                 */
/*                      This function is called from NatInitListsAndTables   */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : gStaticPolicyNatList                                 */
/*                      gDynamicPolicyNatList                                */
/* Global Variables                                                          */
/* Modified           : gStaticPolicyNatList                                 */
/*                      gDynamicPolicyNatList                                */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
NatPolicyInitLists (VOID)
{
    /* gStaticPolicyNatList maintains the translated IP information for the 
     * traffic (source IP<==> destination IP), as identified by the L3 
     * filter ID. 
     */
    TMO_SLL_Init (&gStaticPolicyNatList);

    /* gDynamicPolicyNatList  maintains the translated IP information for the 
     * traffic (source net<==> destination net), as identified by the L3 
     * filter ID. 
     */
    TMO_SLL_Init (&gDynamicPolicyNatList);
    return;
}

/*****************************************************************************/
/* Function Name      : NatPolicyDeInitList                                  */
/* Description        : This function is used to release all the memory      */
/*                      associated with the policy NAT database.             */
/*                      This function is invoked from NatDeInit.             */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : gStaticPolicyNatList                                 */
/*                      gDynamicPolicyNatList                                */
/* Global Variables                                                          */
/* Modified           : gStaticPolicyNatList                                 */
/*                      gDynamicPolicyNatList                                */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
NatPolicyDeInitList (VOID)
{
    tNatPolicyNode     *pNatPolicyNode = NULL;
    tNatPolicyNode     *pNextNatPolicyNode = NULL;

    /* Scan the Static policy List and free the memory associated with the list.
     */
    pNatPolicyNode = TMO_SLL_First (&gStaticPolicyNatList);
    while (pNatPolicyNode != NULL)
    {
        pNextNatPolicyNode = TMO_SLL_Next (&gStaticPolicyNatList,
                                           &(pNatPolicyNode->NatPolicyNode));
        TMO_SLL_Delete (&gStaticPolicyNatList,
                        &(pNatPolicyNode->NatPolicyNode));
        MemReleaseMemBlock (NAT_POLICY_LIST_POOL_ID, (UINT1 *) pNatPolicyNode);
        pNatPolicyNode = pNextNatPolicyNode;
    }                            /* End of scan. */

    /* Scan the Dynamic policy List and free the memory associated with 
     * the list. 
     */
    pNatPolicyNode = TMO_SLL_First (&gDynamicPolicyNatList);
    while (pNatPolicyNode != NULL)
    {
        pNextNatPolicyNode = TMO_SLL_Next (&gDynamicPolicyNatList,
                                           &(pNatPolicyNode->NatPolicyNode));
        TMO_SLL_Delete (&gDynamicPolicyNatList,
                        &(pNatPolicyNode->NatPolicyNode));
        MemReleaseMemBlock (NAT_POLICY_LIST_POOL_ID, (UINT1 *) pNatPolicyNode);
        pNatPolicyNode = pNextNatPolicyNode;
    }                            /* End of scan. */
    return;
}

/*****************************************************************************/
/*  Function Name   : NatPolicyEntryCreate                                   */
/*  Description     : This function creates the policy entry in the Policy   */
/*                    NAT database.                                          */
/*  Input(s)        : i4PolicyType - Policy Type (static or dynamic)         */
/*                    i4PolicyId   - Policy Entry identifier.                */
/*                    pAclName   - Access list Name.                         */
/*                    i4RowStatus   - Row status value.                      */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  <OPTIONAL Fields>           : None                                       */
/*  Global Variables Referred   : gStaticPolicyNatList,gDynamicPolicyNatList */
/*  Global variables Modified   : None                                       */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  NAT_FAILURE on failure                    */
/*                                 NAT_SUCCESS on success                    */
/*****************************************************************************/
INT4
NatPolicyEntryCreate (INT4 i4PolicyType, INT4 i4PolicyId, UINT1 *pAclName,
                      INT4 i4RowStatus)
{
    tTMO_SLL           *pNatPolicyList = NULL;
    tNatPolicyNode     *pNatPolicyNode = NULL;
    tNatPolicyNode     *pNewPolicyNode = NULL;
    tNatPolicyNode     *pPrevPolicyNode = NULL;

    if (NAT_STATIC_POLICY == i4PolicyType)
    {
        pNatPolicyList = &gStaticPolicyNatList;
    }
    else
    {
        pNatPolicyList = &gDynamicPolicyNatList;
    }

    /* Allocate memory for the policy NAT entry. */

    if (NULL == NAT_MEM_ALLOCATE (NAT_POLICY_LIST_POOL_ID,
                                  pNewPolicyNode, tNatPolicyNode))
    {
        return NAT_FAILURE;
    }

    MEMSET (pNewPolicyNode->au1AclName, NAT_ZERO,
            sizeof (pNewPolicyNode->au1AclName));
    STRNCPY (pNewPolicyNode->au1AclName, pAclName,
             sizeof (pNewPolicyNode->au1AclName) - NAT_ONE);
    pNewPolicyNode->i4PolicyId = i4PolicyId;

    if (NAT_STATUS_CREATE_AND_WAIT == i4RowStatus)
    {
        /* Marking to NOT_READY since the translated IP information is not
         * available for using this entry. 
         */
        pNewPolicyNode->i4RowStatus = NAT_STATUS_NOT_READY;
    }
    else
    {
        /* Marking to ACTIVE if the row status is CREATE_AND_GO. 
         */
        pNewPolicyNode->i4RowStatus = NAT_STATUS_ACTIVE;
    }

    TMO_SLL_Scan (pNatPolicyList, pNatPolicyNode, tNatPolicyNode *)
    {
        if (pNatPolicyNode->i4PolicyId > i4PolicyId)
        {
            break;
        }
        else
        {
            pPrevPolicyNode = pNatPolicyNode;
        }
    }                            /* End of SLL_Scan */

    if (NULL == pNatPolicyNode)
    {
        TMO_SLL_Add (pNatPolicyList, &(pNewPolicyNode->NatPolicyNode));

    }
    else
    {
        TMO_SLL_Insert (pNatPolicyList,
                        &(pPrevPolicyNode->NatPolicyNode),
                        &(pNewPolicyNode->NatPolicyNode));
        return NAT_SUCCESS;
    }

    return NAT_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : NatPolicyEntryUpdate                                   */
/*  Description     : This function does the following.                      */
/*                    1. Search the static or dynamic policy list based on   */
/*                       type.                                               */
/*                    2. If there exists a matching entry for the (policyID, */
/*                       pAclName) pair, update the row status.              */
/*  Input(s)        : i4PolicyType - Static or dynamic                       */
/*                    i4PolicyId   - Policy Identifier                       */
/*                    pAclName   - Filter Identifier.                        */
/*                    i4RowStatus  - Row Status value.                       */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  <OPTIONAL Fields>           : None                                       */
/*  Global Variables Referred   : gStaticPolicyNatList,gDynamicPolicyNatList */
/*  Global variables Modified   : None                                       */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  NAT_FAILURE on failure                    */
/*                                 NAT_SUCCESS on success                    */
/*****************************************************************************/
INT4
NatPolicyEntryUpdate (INT4 i4PolicyType, INT4 i4PolicyId, UINT1 *pAclName,
                      INT4 i4RowStatus)
{
    tTMO_SLL           *pNatPolicyList = NULL;
    tNatPolicyNode     *pNatPolicyNode = NULL;

    if (NAT_STATIC_POLICY == i4PolicyType)
    {
        /* For Static Policy Type refer the Static Policy NAT list. */
        pNatPolicyList = &gStaticPolicyNatList;
    }
    else
    {
        /* For dynamic Policy Type refer the dynamic Policy NAT list. */
        pNatPolicyList = &gDynamicPolicyNatList;
    }

    TMO_SLL_Scan (pNatPolicyList, pNatPolicyNode, tNatPolicyNode *)
    {
        /* There exists a node with the matching Policy Id and the filter ID. 
         */
        if ((pNatPolicyNode->i4PolicyId == i4PolicyId) &&
            (NAT_ZERO == STRCMP (pNatPolicyNode->au1AclName, pAclName)))
        {
            /* Update the row status. */
            pNatPolicyNode->i4RowStatus = i4RowStatus;
            return NAT_SUCCESS;
        }
    }                            /* End of scanning the NatPolicyList */

    /* No entry available in the list or no matching entry available. */
    return NAT_FAILURE;
}

/*****************************************************************************/
/*  Function Name   : NatPolicyEntryDelete                                   */
/*  Description     : This function deletes the policy entry from the Policy */
/*                    NAT database.                                          */
/*  Input(s)        : i4PolicyType - Policy Type (static or dynamic)         */
/*                    i4PolicyId   - Policy Entry identifier.                */
/*                    pAclName   - Access list identifier.                   */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  <OPTIONAL Fields>           : None                                       */
/*  Global Variables Referred   : gStaticPolicyNatList,gDynamicPolicyNatList */
/*  Global variables Modified   : None                                       */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  NAT_FAILURE on failure                    */
/*                                 NAT_SUCCESS on success                    */
/*****************************************************************************/
INT4
NatPolicyEntryDelete (INT4 i4PolicyType, INT4 i4PolicyId, UINT1 *pAclName)
{
    tTMO_SLL           *pNatPolicyList = NULL;
    tNatPolicyNode     *pNatPolicyNode = NULL;

    if (NAT_STATIC_POLICY == i4PolicyType)
    {
        /* For Static Policy Type refer the Static Policy NAT list. */
        pNatPolicyList = &gStaticPolicyNatList;
    }
    else
    {
        /* For dynamic Policy Type refer the dynamic Policy NAT list. */
        pNatPolicyList = &gDynamicPolicyNatList;
    }

    TMO_SLL_Scan (pNatPolicyList, pNatPolicyNode, tNatPolicyNode *)
    {
        if ((pNatPolicyNode->i4PolicyId == i4PolicyId) &&
            (NAT_ZERO == STRCMP (pNatPolicyNode->au1AclName, pAclName)))
        {
            TMO_SLL_Delete (pNatPolicyList, &(pNatPolicyNode->NatPolicyNode));
            MemReleaseMemBlock (NAT_POLICY_LIST_POOL_ID,
                                (UINT1 *) pNatPolicyNode);
            return NAT_SUCCESS;
        }

    }                            /* End of SLL_Scan */

    return NAT_FAILURE;
}

/* Validation routines for Policy NAT table */

/*****************************************************************************/
/*  Function Name   : NatPolicyIsValid                                       */
/*  Description     : This function does the following.                      */
/*                    1. Search the static or dynamic policy list based on   */
/*                       type.                                               */
/*                    2. If there exists a matching entry for the (policyID, */
/*                       pAclName) pair, return SUCCESS.                     */
/*  Input(s)        : i4PolicyType - Static or dynamic                       */
/*                    i4PolicyId   - Policy Identifier                       */
/*                    pAclName   - Filter Identifier.                        */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  <OPTIONAL Fields>           : None                                       */
/*  Global Variables Referred   : gStaticPolicyNatList,gDynamicPolicyNatList */
/*  Global variables Modified   : None                                       */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  NAT_FALSE on failure                      */
/*                                 NAT_TRUE on success                       */
/*****************************************************************************/
INT4
NatPolicyIsValid (INT4 i4PolicyType, INT4 i4PolicyId, UINT1 *pAclName)
{
    tNatPolicyNode     *pNatPolicyNode = NULL;
    tTMO_SLL           *pNatPolicyList = NULL;

    if (NAT_STATIC_POLICY == i4PolicyType)
    {
        /* For Static Policy Type refer the Static Policy NAT list. */
        pNatPolicyList = &gStaticPolicyNatList;
    }
    else
    {
        /* For dynamic Policy Type refer the Dynamic Policy NAT list. */
        pNatPolicyList = &gDynamicPolicyNatList;
    }

    TMO_SLL_Scan (pNatPolicyList, pNatPolicyNode, tNatPolicyNode *)
    {
        /* There exists a node with the matching Policy Id or the filter ID. 
         * Return FAILURE;
         */
        if ((pNatPolicyNode->i4PolicyId == i4PolicyId) &&
            (NAT_ZERO == STRCMP (pNatPolicyNode->au1AclName, pAclName)))
        {
            return NAT_TRUE;
        }
    }                            /* End of scanning the NatPolicyList */

    /* No entry available in the list or no matching entry available. */
    return NAT_FALSE;
}

/*****************************************************************************/
/*  Function Name   : NatPolicyValidateFilter                                */
/*  Description     : 1. Verifies if the filter number exists.               */
/*                    2. Verifies if the filter id corresponds to the permit */
/*                       access list.                                        */
/*                    3. Verifies if the source and destination IP address   */
/*                       masks are full for statc policy NAT type.           */
/*                    4. If either one of the above condtions fail, return   */
/*                       NAT_FAILURE                                         */
/*  Input(s)        : i4NatPolicyType -Static or Dynamic policy NAT.         */
/*                    pAclName - Access list identifier.                     */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   : None                                       */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  NAT_FAILURE on failure                    */
/*                                 NAT_SUCCESS on success                    */
/*****************************************************************************/
INT4
NatPolicyValidateFilter (INT4 i4NatPolicyType, UINT1 *pAclName)
{
    INT4                i4RetStatus = OSIX_FAILURE;

#ifndef FIREWALL_WANTED
    UNUSED_PARAM (pAclName);
    UNUSED_PARAM (i4NatPolicyType);
    UNUSED_PARAM (i4RetStatus);
#endif
    /* There must exists a valid filter in the system to configure the
     * Policy Entry.
     */
#ifdef FIREWALL_WANTED
    i4RetStatus = FwlUtilGetAclAction (i4NatPolicyType, pAclName);
    if (OSIX_FAILURE == i4RetStatus)
    {
        return NAT_FAILURE;
    }
#endif
    return NAT_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : NatPolicyGetEntry                                      */
/*  Description     : This function does the following.                      */
/*                    1. Search the static or dynamic policy list based on   */
/*                       type.                                               */
/*                    2. If there exists already a matching policyID or      */
/*                       pAclName, return SUCCESS.                           */
/*  Input(s)        :                                                        */
/*                    i4PolicyId   - Policy Identifier                       */
/*                    pAclName   - Filter Identifier.                        */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  <OPTIONAL Fields>           : None                                       */
/*  Global Variables Referred   : gStaticPolicyNatList,gDynamicPolicyNatList */
/*  Global variables Modified   : None                                       */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  NAT_FAILURE on failure                    */
/*                                 NAT_SUCCESS on success                    */
/*****************************************************************************/
INT4
NatPolicyGetEntry (INT4 i4PolicyId, UINT1 *pAclName)
{
    tNatPolicyNode     *pNatPolicyNode = NULL;

    TMO_SLL_Scan (&gStaticPolicyNatList, pNatPolicyNode, tNatPolicyNode *)
    {
        /* There exists a node with the matching Policy Id or the filter ID. 
         * Return FAILURE;
         */
        if ((pNatPolicyNode->i4PolicyId == i4PolicyId) ||
            (NAT_ZERO == STRCMP (pNatPolicyNode->au1AclName, pAclName)))
        {
            return NAT_SUCCESS;
        }
    }                            /* End of scanning the NatPolicyList */

    TMO_SLL_Scan (&gDynamicPolicyNatList, pNatPolicyNode, tNatPolicyNode *)
    {
        /* There exists a node with the matching Policy Id and the filter ID. 
         * Return FAILURE;
         */
        if ((pNatPolicyNode->i4PolicyId == i4PolicyId) ||
            (NAT_ZERO == STRCMP (pNatPolicyNode->au1AclName, pAclName)))
        {
            return NAT_SUCCESS;
        }
    }                            /* End of scanning the NatPolicyList */

    /* No entry available in the list or no matching entry available. */
    return NAT_FAILURE;
}

/* Search Operations in Policy NAT Table. */

/*****************************************************************************/
/*  Function Name   : NatPolicyGetStaticEntry                                */
/*  Description     : This function searches the Static Policy NAT table for */
/*                    the presence of local IP address or Global IP address  */
/*                    depending on the direction. If it is present, the      */
/*                    corresponding mapping  is returned.                    */
/*  Input(s)        : u4InIpAddr - The LAN side IP address.                  */
/*                    u4OutIpAddr - The WAN side IP address                  */
/*                    u4Direction -  Tells the direction of the packet.      */
/*                    NAT_INBOUND/NAT_OUTBOUND.                              */
/*  Output(s)       : pu4IpAddr - Local IP Address for NAT_INBOUND direction */
/*                    pu4IpAddr - Global IP Address for NAT_OUTBOUND directin*/
/*                  :                                                        */
/*  <OPTIONAL Fields>           : None                                       */
/*  Global Variables Referred   : gStaticPolicyNatList.                      */
/*  Global variables Modified   : None                                       */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  NAT_SUCCESS if a mapping is found.        */
/*                                 NAT_FAILURE if a match is not found.      */
/*****************************************************************************/
INT4
NatPolicyGetStaticEntry (UINT4 *pu4IpAddr,
                         UINT4 u4InIpAddr,
                         UINT4 u4OutIpAddr, UINT4 u4OutPort, UINT4 u4Direction)
{
    tNatPolicyNode     *pNatPolicyNode = NULL;
#ifdef FIREWALL_WANTED
    tFwlPacketInfo      FwlPacketNode;
    INT4                i4RetStatus = OSIX_FAILURE;
    INT4                i4NatPolicyType = NAT_STATIC_POLICY;
    INT4                i4Found = NAT_FALSE;
#endif

#ifdef FIREWALL_WANTED
    MEMSET (&FwlPacketNode, NAT_ZERO, sizeof (tFwlPacketInfo));
#endif

#ifndef FIREWALL_WANTED
    UNUSED_PARAM (pu4IpAddr);
    UNUSED_PARAM (u4OutIpAddr);
    UNUSED_PARAM (u4OutPort);
#endif
    /* Scan the entire static policy List. */
    TMO_SLL_Scan (&gStaticPolicyNatList, pNatPolicyNode, tNatPolicyNode *)
    {
        /* Search only the Static policy nodes, for the access list match. 
         */
        if (NAT_STATUS_ACTIVE != pNatPolicyNode->i4RowStatus)
        {
            continue;
        }

        switch (u4Direction)
        {
                /* For inbound search, 
                 * u4InIpAddr will be the translated IP address. 
                 * u4OutIpAddr  will be the WAN side IP address. 
                 * Need to update the local IP address information in the
                 * output variable pu4IpAddr.
                 */
            case NAT_INBOUND:
                /* If the translated IP address of this policy node is not 
                 * matching the inside IP address skip. 
                 * since this policy node will not provide the local IP address
                 * mapping. 
                 */
                if (pNatPolicyNode->u4TranslatedLocIpAddr != u4InIpAddr)
                {
                    continue;
                }

                /* A matching translated IP address is found. Then invoke
                 * FwlUtilAclLookup to get the access list 
                 * information, from which the local IP address can be
                 * identified. 
                 */
#ifdef FIREWALL_WANTED
                FwlPacketNode.u4Direction = u4Direction;
                FwlPacketNode.u4SrcIpAddr = u4InIpAddr;
                FwlPacketNode.u4DestIpAddr = u4OutIpAddr;
                FwlPacketNode.u4DestPort = u4OutPort;
                i4RetStatus = FwlUtilAclLookup (&FwlPacketNode,
                                                pNatPolicyNode->au1AclName,
                                                i4NatPolicyType);
                if (OSIX_FAILURE == i4RetStatus)
                {
                    continue;
                }
                if (NAT_TRUE == FwlPacketNode.u4PortMatch)
                {
                    *pu4IpAddr = FwlPacketNode.u4OutIpAddr;
                    return NAT_SUCCESS;
                }
                else
                {
                    i4Found = NAT_TRUE;
                    *pu4IpAddr = FwlPacketNode.u4OutIpAddr;
                }
#endif
                break;

                /* For outbound search, 
                 * u4OutIpAddr will be the WAN side IP address. 
                 * u4InIpAddr  will be the local IP address. 
                 * Need to update the translated IP address information in the
                 * output variable pu4IpAddr.
                 */
            case NAT_OUTBOUND:
                /* FwlUtilAclLookup is invoked to get the access 
                 * list information, from which the translated IP address can be
                 * identified. 
                 */
#ifdef FIREWALL_WANTED
                FwlPacketNode.u4Direction = u4Direction;
                FwlPacketNode.u4SrcIpAddr = u4InIpAddr;
                FwlPacketNode.u4DestIpAddr = u4OutIpAddr;
                FwlPacketNode.u4DestPort = u4OutPort;
                i4RetStatus = FwlUtilAclLookup (&FwlPacketNode,
                                                pNatPolicyNode->au1AclName,
                                                i4NatPolicyType);
                if (OSIX_FAILURE == i4RetStatus)
                {
                    continue;
                }
                if (NAT_TRUE == FwlPacketNode.u4PortMatch)
                {
                    *pu4IpAddr = pNatPolicyNode->u4TranslatedLocIpAddr;
                    return NAT_SUCCESS;
                }
                else
                {
                    i4Found = NAT_TRUE;
                    *pu4IpAddr = pNatPolicyNode->u4TranslatedLocIpAddr;
                }
#endif
                break;

            default:
                break;
        }                        /* End of switch */
    }                            /* End of scan */

#ifdef FIREWALL_WANTED
    if (NAT_FALSE == i4Found)
    {
        /* No matching entry exists. Return NAT_FAILURE. */
        return NAT_FAILURE;
    }
#endif
    return NAT_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : NatPolicyGetDynamicEntry                               */
/*  Description     : This function searches the dynamic Policy NAT table for*/
/*                    the presence of global IP address corresponding to the */
/*                    (source IP, destination IP, destination port *) tuple. */
/*                    If there exists a mapping, the global IP address is    */
/*                    updated in the output variable.                        */
/*                    corresponding mapping  is returned.                    */
/*  Input(s)        : u4SrcIpAddr - The source IP address.                   */
/*                    (LAN side IP address)                                  */
/*                    u4DestIpAddr - The destination IP address              */
/*                    (WAN side IP address)                                  */
/*  Output(s)       : pu4IpAddr - Global IP Address                          */
/*                  :                                                        */
/*  <OPTIONAL Fields>           : None                                       */
/*  Global Variables Referred   : gDynamicPolicyNatList.                     */
/*  Global variables Modified   : None                                       */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  NAT_SUCCESS if a mapping is found.        */
/*                                 NAT_FAILURE if a match is not found.      */
/*****************************************************************************/
INT4
NatPolicyGetDynamicEntry (UINT4 *pu4TransIpAddr,
                          UINT4 u4SrcIpAddr,
                          UINT4 u4DestIpAddr, UINT4 u4DestPort)
{
    tNatPolicyNode     *pNatPolicyNode = NULL;
#ifdef FIREWALL_WANTED
    tFwlPacketInfo      FwlPacketNode;
    INT4                i4RetStatus = OSIX_FAILURE;
    UINT4               u4Direction = NAT_OUTBOUND;
    INT4                i4NatPolicyType = NAT_DYNAMIC_POLICY;
    INT4                i4Found = NAT_FALSE;
#endif

#ifndef FIREWALL_WANTED
    UNUSED_PARAM (pu4TransIpAddr);
    UNUSED_PARAM (u4SrcIpAddr);
    UNUSED_PARAM (u4DestIpAddr);
    UNUSED_PARAM (u4DestPort);
#endif
#ifdef FIREWALL_WANTED
    MEMSET (&FwlPacketNode, NAT_ZERO, sizeof (tFwlPacketInfo));
#endif
    /* Scan the entire dynamic policy List. */
    TMO_SLL_Scan (&gDynamicPolicyNatList, pNatPolicyNode, tNatPolicyNode *)
    {
        /* Search only the active dynamic policy nodes, 
         * for a matching access list.
         */
        if (NAT_STATUS_ACTIVE != pNatPolicyNode->i4RowStatus)
        {
            continue;
        }

        /* FwlUtilAclLookup is invoked to get the access
         * list information, from which the translated IP address can be
         * identified.
         */
#ifdef FIREWALL_WANTED
        FwlPacketNode.u4Direction = u4Direction;
        FwlPacketNode.u4SrcIpAddr = u4SrcIpAddr;
        FwlPacketNode.u4DestIpAddr = u4DestIpAddr;
        FwlPacketNode.u4DestPort = u4DestPort;
        i4RetStatus = FwlUtilAclLookup (&FwlPacketNode,
                                        pNatPolicyNode->au1AclName,
                                        i4NatPolicyType);
        if (OSIX_FAILURE == i4RetStatus)
        {
            continue;
        }
        if (NAT_TRUE == FwlPacketNode.u4PortMatch)
        {
            *pu4TransIpAddr = pNatPolicyNode->u4TranslatedLocIpAddr;
            return NAT_SUCCESS;
        }
        else
        {
            i4Found = NAT_TRUE;
            *pu4TransIpAddr = pNatPolicyNode->u4TranslatedLocIpAddr;
        }
#endif
    }
#ifdef FIREWALL_WANTED
    if (NAT_FALSE == i4Found)
    {
        /* No matching entry exists. Return NAT_FAILURE. */
        return NAT_FAILURE;
    }
#endif
    return NAT_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : NatPolicySearchStaticTable.                            */
/*  Description     : This function searches the NAT policy table (both      */
/*                    static to find if the IP address passed as             */
/*                    input is available as the translated IP in the table.  */
/*  Input(s)        : u4IpAddress - Input IP address information.            */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  <OPTIONAL Fields>           : None                                       */
/*  Global Variables Referred   : gStaticPolicyNatList,gDynamicPolicyNatList */
/*  Global variables Modified   : None                                       */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  NAT_SUCCESS if the search succeeds.       */
/*                                 NAT_FAILURE if the search fails.          */
/*****************************************************************************/
INT1
NatPolicySearchStaticTable (UINT4 u4IpAddress)
{
    tNatPolicyNode     *pNatPolicyNode = NULL;

    /* Search the static policy table for the matching entry of 
     * the translated IP address.
     */
    TMO_SLL_Scan (&gStaticPolicyNatList, pNatPolicyNode, tNatPolicyNode *)
    {
        if (pNatPolicyNode->u4TranslatedLocIpAddr == u4IpAddress)
        {
            return NAT_SUCCESS;
        }
    }                            /* End of scan - Static policy NAT list. */
    return NAT_FAILURE;
}

/*****************************************************************************/
/*  Function Name   : NatPolicySearchDynamicTable.                           */
/*  Description     : This function searches the NAT policy table            */
/*                    (dynamic) to find if the IP address passed as          */
/*                    input is available as the translated IP in the table.  */
/*  Input(s)        : u4IpAddress - Input IP address information.            */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  <OPTIONAL Fields>           : None                                       */
/*  Global Variables Referred   : gDynamicPolicyNatList                      */
/*  Global variables Modified   : None                                       */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  NAT_SUCCESS if the search succeeds.       */
/*                                 NAT_FAILURE if the search fails.          */
/*****************************************************************************/
INT1
NatPolicySearchDynamicTable (UINT4 u4IpAddress)
{
    tNatPolicyNode     *pNatPolicyNode = NULL;

    /* Search the dynamic policy table for the matching entry of 
     * the translated IP address.
     */
    TMO_SLL_Scan (&gDynamicPolicyNatList, pNatPolicyNode, tNatPolicyNode *)
    {
        if (pNatPolicyNode->u4TranslatedLocIpAddr == u4IpAddress)
        {
            return NAT_SUCCESS;
        }
    }                            /* End of scan - dynamic policy NAT list. */

    return NAT_FAILURE;
}

#endif /* NATPOLICY_C_ */
