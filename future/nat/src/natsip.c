/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: natsip.c,v 1.7 2014/01/31 13:10:32 siva Exp $
 *
 * Description: This file contains the supporting functions for NAT 
 *              SIPALG
 ********************************************************************/

#include "natinc.h"
#include "natsip.h"
#include "sipalg_inc.h"

#if ((defined(SDF_TIMESTAMP)) || (defined(SDF_PASSTHROUGH)))
PUBLIC int          snprintf (char *str, size_t size, const char *format, ...);
#endif

/*********************************************************************
* Function Name :  NatProcessSIPPkt
* Description   : This function translates Inbound/Outbound SIP pkt
*                 
* Input (s)  : 1. pBuf - Inbound/Outbound Pkt CRU buffer pointer
*            : 2. pHeaderInfo - Pkt Header Info
* Output (s) : The changed pkt payload 

* Returns    : SUCCESS/FAILURE
***********************************************************************/

PUBLIC INT4
NatProcessSIPPkt (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo)
{

    UINT4               u4DataOffset = NAT_ZERO;
    UINT4               u4OldDataLen = NAT_ZERO;
    UINT4               u4NewDataLen = NAT_ZERO;
    UINT1              *pPktBuf = NULL;
    UINT4               u4CksumOffset = NAT_ZERO;
    UINT2               u2TransportCksum = NAT_ZERO;
    UINT2               u2NewTransportLen = NAT_ZERO;
    UINT2               u2TotLen = NAT_ZERO;
    UINT2               u2OldTotLen = NAT_ZERO;
    UINT2               u2NewLen = NAT_ZERO;
    UINT2               u2NewTotLen = NAT_ZERO;
    INT4                i4Retval = NAT_FAILURE;
    UINT1              *pSipTransBuf = NULL;
    UINT2               u2NewTransLen = NAT_ZERO;
    en_SipBoolean       u4IfAllocatedMemory = SipFalse;

    NAT_TRC (NAT_TRC_ON, "\n SIPALG: Translating Pkt");

    /* Get data offset - the place where SIP message begins */
    u4DataOffset =
        (UINT4) (pHeaderInfo->u1IpHeadLen + pHeaderInfo->u1TransportHeadLen);

    /* Toatal length of the IP pkt (IP + Transport + SIP) */
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2TotLen, NAT_TOT_LEN_OFFSET,
                               NAT_TOL_LEN_FIELD);
    u2OldTotLen = OSIX_NTOHS (u2TotLen);

    if (pHeaderInfo->u1PktType == NAT_TCP)
    {
        /* There is no payload in TCP Packet */
        if ((u2OldTotLen - u4DataOffset) == NAT_ZERO)
        {
            NAT_TRC (NAT_TRC_ON,
                     "\n SIPALG: TCP control packet having no data");
            return NAT_SUCCESS;
        }
    }

    /* SIP Payload length */
    u4OldDataLen = u2OldTotLen - u4DataOffset;

/* memory leak fix start */
#ifndef SDF_PASSTHROUGH
    pPktBuf = CRU_BUF_Get_DataPtr_IfLinear (pBuf, u4DataOffset, u4OldDataLen);

    if (pPktBuf == NULL)
    {

        pPktBuf = (UINT1 *) NAT_MALLOC ((u4OldDataLen
                                         + NAT_ONE) * sizeof (UINT1));

        if (pPktBuf == NULL)
        {
            NAT_TRC (NAT_TRC_ON, "\n SIPALG: Memory alloc failure");
            return (NAT_FAILURE);
        }

        u4IfAllocatedMemory = SipTrue;
        /* Copy data from CRU buffer to linear buffer */
        CRU_BUF_Copy_FromBufChain (pBuf, pPktBuf, u4DataOffset, u4OldDataLen);
        pPktBuf[u4OldDataLen] = '\0';
    }
#else
    pPktBuf = (UINT1 *) NAT_MALLOC ((u4OldDataLen + NAT_ONE) * sizeof (UINT1));

    if (pPktBuf == NULL)
    {
        NAT_TRC (NAT_TRC_ON, "\n SIPALG: Memory alloc failure");
        return (NAT_FAILURE);
    }

    u4IfAllocatedMemory = SipTrue;
    /* Copy data from CRU buffer to linear buffer */
    CRU_BUF_Copy_FromBufChain (pBuf, pPktBuf, u4DataOffset, u4OldDataLen);
    pPktBuf[u4OldDataLen] = '\0';

/* memory leak fix end */
    if (PASSTHROUGH_ENABLE == SipGetAlgPassThrough ())
    {
        INT1               *pi1Temp1 = strstr (pPktBuf, "UNKNOWN");
        if (NULL != pi1Temp1)
        {
            struct timeval      tp;
            INT2                i2HdrLen = strlen ("UNKNOWNXXX: ");
            i2HdrLen += NAT_SEVENTEEN;
            /*points to ALG time => UNKNOWNXXX: "B2Btime16char" "ALGtime16" */
#ifdef SECURITY_KERNEL_WANTED
            ArOsixLkGetTimeOfDay (&tp);
#else
            gettimeofday (&tp, NAT_ZERO);
#endif
            INT4                i4Msec = tp.tv_sec;
            INT4                i4Usec = tp.tv_usec;

            INT1                ai1RecvSecs[NAT_SEVENTEEN];
            memset (ai1RecvSecs, '0', NAT_16_BYTES);
            ai1RecvSecs[NAT_INDEX_16] = '\0';

            INT1                pi1Tempstr[NAT_ELEVEN];
            sprintf (pi1Tempstr, "%lu", i4Msec);
            INT2                i2Len = strlen (pi1Tempstr);

            INT1                pi1Tempchar = ai1RecvSecs[NAT_TEN];
            /* UNKNOWNINV: 0000000000 -> UNKNOWNINV: 0000133845 
               keep 0's at left rather than blank */
            snprintf (ai1RecvSecs + NAT_TEN - i2Len, i2Len
                      + NAT_ONE, "%lu", i4Msec);
            ai1RecvSecs[NAT_INDEX_10] = pi1Tempchar;
            INT1                ai1Tempstr1[NAT_SEVEN];
            sprintf (ai1Tempstr1, "%lu", i4Usec);
            i2Len = strlen (ai1Tempstr1);

            pi1Tempchar = ai1RecvSecs[NAT_SIXTEEN];
            snprintf (ai1RecvSecs + NAT_SIXTEEN - i2Len, i2Len
                      + NAT_ONE, "%lu", i4Usec);
            ai1RecvSecs[NAT_INDEX_16] = pi1Tempchar;

            pi1Tempchar = pi1Temp1[i2HdrLen + NAT_SIXTEEN];
            snprintf (pi1Temp1 + i2HdrLen, NAT_SIXTEEN
                      + NAT_ONE, "%s", ai1RecvSecs);
            pi1Temp1[i2HdrLen + NAT_SIXTEEN] = pi1Tempchar;
        }
    }
#endif

#ifdef SDF_PASSTHROUGH
    if (PASSTHROUGH_ENABLE == SipGetAlgPassThrough ())
    {
        struct timeval      tp;
        INT2                i2Index = NAT_ZERO;
        INT1               *pi1Temp = NULL;

#ifdef SECURITY_KERNEL_WANTED
        ArOsixLkGetTimeOfDay (&tp);
#else
        gettimeofday (&tp, NAT_ZERO);
#endif
        INT4                i4Mcurrentsec = tp.tv_sec;
        INT4                i4Ucurrentsec = tp.tv_usec;

        pi1Temp = strstr (pPktBuf, "UNKNOWNINV: ");
        if (pi1Temp)
        {
            i2Index = NAT_ONE;
        }
        else
        {
            pi1Temp = strstr (pPktBuf, "UNKNOWN2IN: ");
            if (pi1Temp)
            {
                i2Index = NAT_TWO;
            }
            else
            {
                pi1Temp = strstr (pPktBuf, "UNKNOWNACK: ");
                if (pi1Temp)
                {
                    i2Index = NAT_THREE;
                }
                else
                {
                    pi1Temp = strstr (pPktBuf, "UNKNOWNBYE: ");
                    if (pi1Temp)
                    {
                        i2Index = NAT_FOUR;
                    }
                    else
                    {
                        pi1Temp = strstr (pPktBuf, "UNKNOWN2BY: ");
                        if (pi1Temp)
                        {
                            i2Index = NAT_FIVE;
                        }
                        else
                        {
                            pi1Temp = strstr (pPktBuf, "UNKNOWNREG: ");
                            if (pi1Temp)
                            {
                                i2Index = NAT_SIX;
                            }
                            else
                            {
                                pi1Temp = strstr (pPktBuf, "UNKNOWN2RE: ");
                                if (pi1Temp)
                                {
                                    i2Index = NAT_SEVEN;
                                }
                            }
                        }
                    }
                }
            }
        }
        INT2                i2HdrLen = strlen ("UNKNOWNXXX: ");

        /*point to ALG timestamp rather b2b timestamp */
        i2HdrLen += NAT_SEVENTEEN;
        if (NULL != pi1Temp)
        {
            INT1                ai1RecvtimeS[NAT_INDEX_11];
            INT2                i2Indx;
            for (i2Indx = NAT_ZERO; i2Indx <= NAT_NINE; i2Indx++)
            {
                ai1RecvtimeS[i2Indx] = pi1Temp[i2HdrLen + i2Indx];
            }
            ai1RecvtimeS[i2Indx] = '\0';

            INT4                i4RecvtimeSecinMsg = sipAlgatoi (ai1RecvtimeS);

            pi1Temp += i2HdrLen + i2Indx;
            INT1                i1RecvtimeU[NAT_SEVEN];
            INT2                i2Indxb;
            for (i2Indxb = NAT_ZERO; i2Indxb <= NAT_SIX; i2Indxb++)
            {
                i1RecvtimeU[i2Indxb] = pi1Temp[i2Indxb];
            }
            i1RecvtimeU[i2Indxb] = '\0';

            INT4                i4RecvTimeUsecinMsg = sipAlgatoi (i1RecvtimeU);
            /*if current time Usecs is less than that in msg then remove 
               one sec from current time and add 1000000 
               usecs to currenttime */
            if (i4Ucurrentsec < i4RecvTimeUsecinMsg)
            {
                i4Mcurrentsec -= NAT_ONE;
                i4Ucurrentsec += NAT_ADD_1_SEC;
            }
            INT4                i4DelaySec, i4DelayUsec;

            i4DelaySec = i4Mcurrentsec - i4RecvtimeSecinMsg;
            i4DelayUsec = i4Ucurrentsec - i4RecvTimeUsecinMsg;

            switch (i2Index)
            {
                case NAT_ONE:
                    st_Invite->i2NumInvite++;
                    st_Invite->AvgSec =
                        ((st_Invite->AvgSec) * (st_Invite->i2NumInvite
                                                - NAT_ONE) +
                         i4DelaySec) / (FLT4) st_Invite->i2NumInvite;
                    st_Invite->AvgUsec =
                        ((st_Invite->AvgUsec) * (st_Invite->i2NumInvite
                                                 - NAT_ONE) +
                         i4DelayUsec) / (FLT4) st_Invite->i2NumInvite;

                    if (st_Invite->i4PeakSec < i4DelaySec)
                    {
                        st_Invite->i4PeakSec = i4DelaySec;
                        st_Invite->i4PeakUsec = i4DelayUsec;
                        st_Invite->i2NumInvPeak = NAT_ONE;
                    }
                    else if (st_Invite->i4PeakSec == i4DelaySec)
                    {
                        if (st_Invite->i4PeakUsec < i4DelayUsec)
                        {
                            st_Invite->i4PeakSec = i4DelaySec;
                            st_Invite->i4PeakUsec = i4DelayUsec;
                            st_Invite->i2NumInvPeak = NAT_ONE;
                        }
                        /*same as  old peak */

                        else if (st_Invite->i4PeakUsec == i4DelayUsec)
                        {
                            st_Invite->i2NumInvPeak++;
                        }
                    }

                    if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                         + i4DelayUsec / NAT_CONVERT_MILLI_SEC) <= NAT_FIVE)
                    {
                        st_Invite->ai2Band[NAT_INDEX_0]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_10_MILLI_SEC)
                    {
                        st_Invite->ai2Band[NAT_INDEX_1]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_20_MILLI_SEC)
                    {
                        st_Invite->ai2Band[NAT_INDEX_2]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_30_MILLI_SEC)
                    {
                        st_Invite->ai2Band[NAT_INDEX_3]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_40_MILLI_SEC)
                    {
                        st_Invite->ai2Band[NAT_INDEX_4]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_50_MILLI_SEC)
                    {
                        st_Invite->ai2Band[NAT_INDEX_5]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_60_MILLI_SEC)
                    {
                        st_Invite->ai2Band[NAT_INDEX_6]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_70_MILLI_SEC)
                    {
                        st_Invite->ai2Band[NAT_INDEX_7]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_80_MILLI_SEC)
                    {
                        st_Invite->ai2Band[NAT_INDEX_8]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_90_MILLI_SEC)
                    {
                        st_Invite->ai2Band[NAT_INDEX_9]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_100_MILLI_SEC)
                    {
                        st_Invite->ai2Band[NAT_INDEX_10]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_110_MILLI_SEC)
                    {
                        st_Invite->ai2Band[NAT_INDEX_11]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_120_MILLI_SEC)
                    {
                        st_Invite->ai2Band[NAT_INDEX_12]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_130_MILLI_SEC)
                    {
                        st_Invite->ai2Band[NAT_INDEX_13]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_140_MILLI_SEC)
                    {
                        st_Invite->ai2Band[NAT_INDEX_14]++;
                    }
                    else
                    {
                        st_Invite->ai2Band[NAT_INDEX_15]++;
                    }            /*greater than 140 */

                    break;
                case NAT_TWO:
                    st_200INV->i2NumInvite++;
                    st_200INV->AvgSec =
                        ((st_200INV->AvgSec) * (st_200INV->i2NumInvite
                                                - NAT_ONE) +
                         i4DelaySec) / (FLT4) st_200INV->i2NumInvite;
                    st_200INV->AvgUsec =
                        ((st_200INV->AvgUsec) * (st_200INV->i2NumInvite
                                                 - NAT_ONE) +
                         i4DelayUsec) / (FLT4) st_200INV->i2NumInvite;

                    if (st_200INV->i4PeakSec < i4DelaySec)
                    {
                        st_200INV->i4PeakSec = i4DelaySec;
                        st_200INV->i4PeakUsec = i4DelayUsec;
                        st_200INV->i2NumInvPeak = NAT_ONE;
                    }
                    else if (st_200INV->i4PeakSec == i4DelaySec)
                    {
                        if (st_200INV->i4PeakUsec < i4DelayUsec)
                        {
                            st_200INV->i4PeakSec = i4DelaySec;
                            st_200INV->i4PeakUsec = i4DelayUsec;
                            st_200INV->i2NumInvPeak = NAT_ONE;
                        }
                        /*same as  old peak */
                        else if (st_200INV->i4PeakUsec == i4DelayUsec)
                        {
                            st_200INV->i2NumInvPeak++;
                        }
                    }

                    if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                         + i4DelayUsec / NAT_CONVERT_MILLI_SEC) <= NAT_FIVE)
                    {
                        st_200INV->ai2Band[NAT_INDEX_0]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_10_MILLI_SEC)
                    {
                        st_200INV->ai2Band[NAT_INDEX_1]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_20_MILLI_SEC)
                    {
                        st_200INV->ai2Band[NAT_INDEX_2]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_30_MILLI_SEC)
                    {
                        st_200INV->ai2Band[NAT_INDEX_3]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_40_MILLI_SEC)
                    {
                        st_200INV->ai2Band[NAT_INDEX_4]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_50_MILLI_SEC)
                    {
                        st_200INV->ai2Band[NAT_INDEX_5]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_60_MILLI_SEC)
                    {
                        st_200INV->ai2Band[NAT_INDEX_6]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_70_MILLI_SEC)
                    {
                        st_200INV->ai2Band[NAT_INDEX_7]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_80_MILLI_SEC)
                    {
                        st_200INV->ai2Band[NAT_INDEX_8]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_90_MILLI_SEC)
                    {
                        st_200INV->ai2Band[NAT_INDEX_9]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_100_MILLI_SEC)
                    {
                        st_200INV->ai2Band[NAT_INDEX_10]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_110_MILLI_SEC)
                    {
                        st_200INV->ai2Band[NAT_INDEX_11]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_120_MILLI_SEC)
                    {
                        st_200INV->ai2Band[NAT_INDEX_12]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_130_MILLI_SEC)
                    {
                        st_200INV->ai2Band[NAT_INDEX_13]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_140_MILLI_SEC)
                    {
                        st_200INV->ai2Band[NAT_INDEX_14]++;
                    }
                    else
                    {
                        st_200INV->ai2Band[NAT_INDEX_15]++;
                    }            /*greater than 140 */

                    break;
                case NAT_THREE:
                    st_ACK->i2NumInvite++;
                    st_ACK->AvgSec =
                        ((st_ACK->AvgSec) * (st_ACK->i2NumInvite
                                             - NAT_ONE) +
                         i4DelaySec) / (FLT4) st_ACK->i2NumInvite;
                    st_ACK->AvgUsec =
                        ((st_ACK->AvgUsec) * (st_ACK->i2NumInvite
                                              - NAT_ONE) +
                         i4DelayUsec) / (FLT4) st_ACK->i2NumInvite;

                    if (st_ACK->i4PeakSec < i4DelaySec)
                    {
                        st_ACK->i4PeakSec = i4DelaySec;
                        st_ACK->i4PeakUsec = i4DelayUsec;
                        st_ACK->i2NumInvPeak = NAT_ONE;
                    }
                    else if (st_ACK->i4PeakSec == i4DelaySec)
                    {
                        if (st_ACK->i4PeakUsec < i4DelayUsec)
                        {
                            st_ACK->i4PeakSec = i4DelaySec;
                            st_ACK->i4PeakUsec = i4DelayUsec;
                            st_ACK->i2NumInvPeak = NAT_ONE;
                        }
                        /*same as  old peak */

                        else if (st_ACK->i4PeakUsec == i4DelayUsec)
                        {
                            st_ACK->i2NumInvPeak++;
                        }
                    }

                    if ((i4DelaySec * NAT_CONVERT_MILLI_SEC +
                         i4DelayUsec / NAT_CONVERT_MILLI_SEC) <= NAT_FIVE)
                    {
                        st_ACK->ai2Band[NAT_INDEX_0]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_10_MILLI_SEC)
                    {
                        st_ACK->ai2Band[NAT_INDEX_1]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_20_MILLI_SEC)
                    {
                        st_ACK->ai2Band[NAT_INDEX_2]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_30_MILLI_SEC)
                    {
                        st_ACK->ai2Band[NAT_INDEX_3]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_40_MILLI_SEC)
                    {
                        st_ACK->ai2Band[NAT_INDEX_4]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_50_MILLI_SEC)
                    {
                        st_ACK->ai2Band[NAT_INDEX_5]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_60_MILLI_SEC)
                    {
                        st_ACK->ai2Band[NAT_INDEX_6]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_70_MILLI_SEC)
                    {
                        st_ACK->ai2Band[NAT_INDEX_7]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_80_MILLI_SEC)
                    {
                        st_ACK->ai2Band[NAT_INDEX_8]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_90_MILLI_SEC)
                    {
                        st_ACK->ai2Band[NAT_INDEX_9]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_100_MILLI_SEC)
                    {
                        st_ACK->ai2Band[NAT_INDEX_10]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_110_MILLI_SEC)
                    {
                        st_ACK->ai2Band[NAT_INDEX_11]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_120_MILLI_SEC)
                    {
                        st_ACK->ai2Band[NAT_INDEX_12]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_130_MILLI_SEC)
                    {
                        st_ACK->ai2Band[NAT_INDEX_13]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_140_MILLI_SEC)
                    {
                        st_ACK->ai2Band[NAT_INDEX_14]++;
                    }
                    else
                    {
                        st_ACK->ai2Band[NAT_INDEX_15]++;
                    }            /*greater than 140 */
                    break;
                case NAT_FOUR:
                    st_BYE->i2NumInvite++;
                    st_BYE->AvgSec =
                        ((st_BYE->AvgSec) * (st_BYE->i2NumInvite
                                             - NAT_ONE) +
                         i4DelaySec) / (FLT4) st_BYE->i2NumInvite;
                    st_BYE->AvgUsec =
                        ((st_BYE->AvgUsec) * (st_BYE->i2NumInvite
                                              - NAT_ONE) +
                         i4DelayUsec) / (FLT4) st_BYE->i2NumInvite;

                    if (st_BYE->i4PeakSec < i4DelaySec)
                    {
                        st_BYE->i4PeakSec = i4DelaySec;
                        st_BYE->i4PeakUsec = i4DelayUsec;
                        st_BYE->i2NumInvPeak = NAT_ONE;
                    }
                    else if (st_BYE->i4PeakSec == i4DelaySec)
                    {
                        if (st_BYE->i4PeakUsec < i4DelayUsec)
                        {
                            st_BYE->i4PeakSec = i4DelaySec;
                            st_BYE->i4PeakUsec = i4DelayUsec;
                            st_BYE->i2NumInvPeak = NAT_ONE;
                        }        /*same as  old peak */

                        else if (st_BYE->i4PeakUsec == i4DelayUsec)
                        {
                            st_BYE->i2NumInvPeak++;
                        }
                    }

                    if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                         + i4DelayUsec / NAT_CONVERT_MILLI_SEC) <= NAT_FIVE)
                    {
                        st_BYE->ai2Band[NAT_INDEX_0]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_10_MILLI_SEC)
                    {
                        st_BYE->ai2Band[NAT_INDEX_1]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_20_MILLI_SEC)
                    {
                        st_BYE->ai2Band[NAT_INDEX_2]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_30_MILLI_SEC)
                    {
                        st_BYE->ai2Band[NAT_INDEX_3]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_40_MILLI_SEC)
                    {
                        st_BYE->ai2Band[NAT_INDEX_4]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_50_MILLI_SEC)
                    {
                        st_BYE->ai2Band[NAT_INDEX_5]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_60_MILLI_SEC)
                    {
                        st_BYE->ai2Band[NAT_INDEX_6]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_70_MILLI_SEC)
                    {
                        st_BYE->ai2Band[NAT_INDEX_7]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_80_MILLI_SEC)
                    {
                        st_BYE->ai2Band[NAT_INDEX_8]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_90_MILLI_SEC)
                    {
                        st_BYE->ai2Band[NAT_INDEX_9]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_100_MILLI_SEC)
                    {
                        st_BYE->ai2Band[NAT_INDEX_10]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_110_MILLI_SEC)
                    {
                        st_BYE->ai2Band[NAT_INDEX_11]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_120_MILLI_SEC)
                    {
                        st_BYE->ai2Band[NAT_INDEX_12]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_130_MILLI_SEC)
                    {
                        st_BYE->ai2Band[NAT_INDEX_13]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_140_MILLI_SEC)
                    {
                        st_BYE->ai2Band[NAT_INDEX_14]++;
                    }
                    else
                    {
                        st_BYE->ai2Band[NAT_INDEX_15]++;
                    }            /*greater than 140 */

                    break;
                case NAT_FIVE:
                    st_200BYE->i2NumInvite++;
                    st_200BYE->AvgSec =
                        ((st_200BYE->AvgSec) * (st_200BYE->i2NumInvite
                                                - NAT_ONE) +
                         i4DelaySec) / (FLT4) st_200BYE->i2NumInvite;
                    st_200BYE->AvgUsec =
                        ((st_200BYE->AvgUsec) * (st_200BYE->i2NumInvite
                                                 - NAT_ONE) +
                         i4DelayUsec) / (FLT4) st_200BYE->i2NumInvite;

                    if (st_200BYE->i4PeakSec < i4DelaySec)
                    {
                        st_200BYE->i4PeakSec = i4DelaySec;
                        st_200BYE->i4PeakUsec = i4DelayUsec;
                        st_200BYE->i2NumInvPeak = NAT_ONE;
                    }
                    else if (st_200BYE->i4PeakSec == i4DelaySec)
                    {
                        if (st_200BYE->i4PeakUsec < i4DelayUsec)
                        {
                            st_200BYE->i4PeakSec = i4DelaySec;
                            st_200BYE->i4PeakUsec = i4DelayUsec;
                            st_200BYE->i2NumInvPeak = NAT_ONE;
                        }
                        /*same as  old peak */

                        else if (st_200BYE->i4PeakUsec == i4DelayUsec)
                        {
                            st_200BYE->i2NumInvPeak++;
                        }
                    }

                    if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                         + i4DelayUsec / NAT_CONVERT_MILLI_SEC) <= NAT_FIVE)
                    {
                        st_200BYE->ai2Band[NAT_INDEX_0]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_10_MILLI_SEC)
                    {
                        st_200BYE->ai2Band[NAT_INDEX_1]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_20_MILLI_SEC)
                    {
                        st_200BYE->ai2Band[NAT_INDEX_2]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_30_MILLI_SEC)
                    {
                        st_200BYE->ai2Band[NAT_INDEX_3]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_40_MILLI_SEC)
                    {
                        st_200BYE->ai2Band[NAT_INDEX_4]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_50_MILLI_SEC)
                    {
                        st_200BYE->ai2Band[NAT_INDEX_5]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_60_MILLI_SEC)
                    {
                        st_200BYE->ai2Band[NAT_INDEX_6]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_70_MILLI_SEC)
                    {
                        st_200BYE->ai2Band[NAT_INDEX_7]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_80_MILLI_SEC)
                    {
                        st_200BYE->ai2Band[NAT_INDEX_8]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_90_MILLI_SEC)
                    {
                        st_200BYE->ai2Band[NAT_INDEX_9]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_100_MILLI_SEC)
                    {
                        st_200BYE->ai2Band[NAT_INDEX_10]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_110_MILLI_SEC)
                    {
                        st_200BYE->ai2Band[NAT_INDEX_11]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_120_MILLI_SEC)
                    {
                        st_200BYE->ai2Band[NAT_INDEX_12]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_130_MILLI_SEC)
                    {
                        st_200BYE->ai2Band[NAT_INDEX_13]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_140_MILLI_SEC)
                    {
                        st_200BYE->ai2Band[NAT_INDEX_14]++;
                    }
                    else
                    {
                        st_200BYE->ai2Band[NAT_INDEX_15]++;
                    }            /*greater than 140 */

                    break;
                case NAT_SIX:
                    st_REG->i2NumInvite++;
                    st_REG->AvgSec =
                        ((st_REG->AvgSec) * (st_REG->i2NumInvite
                                             - NAT_ONE) +
                         i4DelaySec) / (FLT4) st_REG->i2NumInvite;
                    st_REG->AvgUsec =
                        ((st_REG->AvgUsec) * (st_REG->i2NumInvite
                                              - NAT_ONE) +
                         i4DelayUsec) / (FLT4) st_REG->i2NumInvite;

                    if (st_REG->i4PeakSec < i4DelaySec)
                    {
                        st_REG->i4PeakSec = i4DelaySec;
                        st_REG->i4PeakUsec = i4DelayUsec;
                        st_REG->i2NumInvPeak = NAT_ONE;
                    }
                    else if (st_REG->i4PeakSec == i4DelaySec)
                    {
                        if (st_REG->i4PeakUsec < i4DelayUsec)
                        {
                            st_REG->i4PeakSec = i4DelaySec;
                            st_REG->i4PeakUsec = i4DelayUsec;
                            st_REG->i2NumInvPeak = NAT_ONE;
                        }
                        /*same as  old peak */

                        else if (st_REG->i4PeakUsec == i4DelayUsec)
                        {
                            st_REG->i2NumInvPeak++;
                        }
                    }

                    if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                         + i4DelayUsec / NAT_CONVERT_MILLI_SEC) <= NAT_FIVE)
                    {
                        st_REG->ai2Band[NAT_INDEX_0]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_10_MILLI_SEC)
                    {
                        st_REG->ai2Band[NAT_INDEX_1]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_20_MILLI_SEC)
                    {
                        st_REG->ai2Band[NAT_INDEX_2]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_30_MILLI_SEC)
                    {
                        st_REG->ai2Band[NAT_INDEX_3]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_40_MILLI_SEC)
                    {
                        st_REG->ai2Band[NAT_INDEX_4]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_50_MILLI_SEC)
                    {
                        st_REG->ai2Band[NAT_INDEX_5]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_60_MILLI_SEC)
                    {
                        st_REG->ai2Band[NAT_INDEX_6]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT__70_MILLI_SEC)
                    {
                        st_REG->ai2Band[NAT_INDEX_7]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_80_MILLI_SEC)
                    {
                        st_REG->ai2Band[NAT_INDEX_8]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_90_MILLI_SEC)
                    {
                        st_REG->ai2Band[NAT_INDEX_9]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_100_MILLI_SEC)
                    {
                        st_REG->ai2Band[NAT_INDEX_10]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_110_MILLI_SEC)
                    {
                        st_REG->ai2Band[NAT_INDEX_11]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_120_MILLI_SEC)
                    {
                        st_REG->ai2Band[NAT_INDEX_12]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_130_MILLI_SEC)
                    {
                        st_REG->ai2Band[NAT_INDEX_13]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_140_MILLI_SEC)
                    {
                        st_REG->ai2Band[NAT_INDEX_14]++;
                    }
                    else
                    {
                        st_REG->ai2Band[NAT_INDEX_15]++;
                    }            /*greater than 140 */

                    break;
                case NAT_SEVEN:
                    st_200REG->i2NumInvite++;
                    st_200REG->AvgSec =
                        ((st_200REG->AvgSec) * (st_200REG->i2NumInvite
                                                - NAT_ONE) +
                         i4DelaySec) / (FLT4) st_200REG->i2NumInvite;
                    st_200REG->AvgUsec =
                        ((st_200REG->AvgUsec) * (st_200REG->i2NumInvite
                                                 - NAT_ONE) +
                         i4DelayUsec) / (FLT4) st_200REG->i2NumInvite;

                    if (st_200REG->i4PeakSec < i4DelaySec)
                    {
                        st_200REG->i4PeakSec = i4DelaySec;
                        st_200REG->i4PeakUsec = i4DelayUsec;
                        st_200REG->i2NumInvPeak = NAT_ONE;
                    }
                    else if (st_200REG->i4PeakSec == i4DelaySec)
                    {
                        if (st_200REG->i4PeakUsec < i4DelayUsec)
                        {
                            st_200REG->i4PeakSec = i4DelaySec;
                            st_200REG->i4PeakUsec = i4DelayUsec;
                            st_200REG->i2NumInvPeak = NAT_ONE;
                        }
                        /*same as  old peak */

                        else if (st_200REG->i4PeakUsec == i4DelayUsec)
                        {
                            st_200REG->i2NumInvPeak++;
                        }
                    }

                    if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                         + i4DelayUsec / NAT_CONVERT_MILLI_SEC) <= NAT_FIVE)
                    {
                        st_200REG->ai2Band[NAT_INDEX_0]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_10_MILLI_SEC)
                    {
                        st_200REG->ai2Band[NAT_INDEX_1]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_20_MILLI_SEC)
                    {
                        st_200REG->ai2Band[NAT_INDEX_2]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_30_MILLI_SEC)
                    {
                        st_200REG->ai2Band[NAT_INDEX_3]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_40_MILLI_SEC)
                    {
                        st_200REG->ai2Band[NAT_INDEX_4]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_50_MILLI_SEC)
                    {
                        st_200REG->ai2Band[NAT_INDEX_5]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_60_MILLI_SEC)
                    {
                        st_200REG->ai2Band[NAT_INDEX_6]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_70_MILLI_SEC)
                    {
                        st_200REG->ai2Band[NAT_INDEX_7]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_80_MILLI_SEC)
                    {
                        st_200REG->ai2Band[NAT_INDEX_8]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_90_MILLI_SEC)
                    {
                        st_200REG->ai2Band[NAT_INDEX_9]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_100_MILLI_SEC)
                    {
                        st_200REG->ai2Band[NAT_INDEX_10]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_110_MILLI_SEC)
                    {
                        st_200REG->ai2Band[NAT_INDEX_11]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_120_MILLI_SEC)
                    {
                        st_200REG->ai2Band[NAT_INDEX_12]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_130_MILLI_SEC)
                    {
                        st_200REG->ai2Band[NAT_INDEX_13]++;
                    }
                    else if ((i4DelaySec * NAT_CONVERT_MILLI_SEC
                              + i4DelayUsec / NAT_CONVERT_MILLI_SEC)
                             <= NAT_140_MILLI_SEC)
                    {
                        st_200REG->ai2Band[NAT_INDEX_14]++;
                    }
                    else
                    {
                        st_200REG->ai2Band[NAT_INDEX_15]++;
                    }            /*greater than 140 */

                    break;
            }
        }
    }
#endif
    if (i4Retval == NAT_FAILURE)
    {
        NAT_TRC (NAT_TRC_ON, "\n SIPALG: Translation failed");
        if (u4IfAllocatedMemory == SipTrue)
        {
            NAT_FREE (pPktBuf);
        }
        return (NAT_FAILURE);
    }

    /* if pSipTransBuf is null, then further processing
       is not reqd. return from this function */
#if 0
    if (pSipTransBuf != NULL)
    {
        /* After the IP address and transport length, delete the rest
           of the information from CRU buffer. The new translated SIP
           buffer will be appended at the end of CRU buffer  */
        if (NULL == CRU_BUF_Delete_BufChainAtEnd (pBuf, u4OldDataLen))
        {
            NAT_TRC (NAT_TRC_ON, "\n SIPALG: Translation failed");
            if (u4IfAllocatedMemory == SipTrue)
            {
                NAT_FREE (pPktBuf);
            }
            NAT_FREE (pSipTransBuf);
            return (NAT_FAILURE);
        }

        /* Get the length of modified SIP message */
        /*      u4NewDataLen = sip_strlen ((const SIP_S8bit *) pSipTransBuf);
         */
        /* Copy data to CRU buffer from linear buffer */
        CRU_BUF_Copy_OverBufChain (pBuf, pSipTransBuf,
                                   u4DataOffset, u4NewDataLen);

        /* Updated transport length */
        u2NewTransportLen =
            (UINT2) (u4NewDataLen + pHeaderInfo->u1TransportHeadLen);

        /* As delta is set, this will in turn update the length
           of IP packet */
        pHeaderInfo->i4Delta =
            (u2NewTransportLen + pHeaderInfo->u1IpHeadLen) - u2OldTotLen;

        /* Checksum offset in transport packet */
        if (pHeaderInfo->u1PktType == NAT_UDP)
        {
            u4CksumOffset =
                NAT_UDP_CKSUM_OFFSET + (UINT4) pHeaderInfo->u1IpHeadLen;
        }
        else
            u4CksumOffset =
                NAT_TCP_CKSUM_OFFSET + (UINT4) pHeaderInfo->u1IpHeadLen;
        /* Calculate the new IP header length */
        u2NewTotLen = (UINT2) (u2NewTransportLen + pHeaderInfo->u1IpHeadLen);

        if (pHeaderInfo->u1PktType == NAT_UDP)
        {
            /* If IP length is changed, update the same in CRU buffer */
            if (u2OldTotLen != u2NewTotLen)
            {
                u2NewLen = OSIX_HTONS (u2NewTotLen);

                /* Modified IP length in IP header */
                CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2NewLen,
                                           NAT_TOT_LEN_OFFSET,
                                           NAT_TOL_LEN_FIELD);

            }
            /* This is to modify the UDP header length */
            u2NewTransLen = OSIX_HTONS (u2NewTransportLen);

            /* Update the UDP length in the UDP header */
            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2NewTransLen,
                                       (UINT4) (pHeaderInfo->u1IpHeadLen
                                                + NAT_FOUR), NAT_WORD_LEN);
        }
        /* Recalculating transport checksum */
        u2TransportCksum = NAT_ZERO;
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2TransportCksum,
                                   u4CksumOffset, NAT_WORD_LEN);
        u2TransportCksum = NatTransportCksumAdjust (pBuf, pHeaderInfo);
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2TransportCksum,
                                   u4CksumOffset, NAT_WORD_LEN);
        if (pHeaderInfo->u1PktType == NAT_UDP)
        {
            /* Reset the IP length to the original IP length value */
            u2TotLen = OSIX_HTONS (u2OldTotLen);
            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2TotLen,
                                       NAT_TOT_LEN_OFFSET, NAT_TOL_LEN_FIELD);
        }

        NAT_FREE (pSipTransBuf);
    }
    if (u4IfAllocatedMemory == SipTrue)
    {
        NAT_FREE (pPktBuf);
    }

#endif
    UNUSED_PARAM (pSipTransBuf);
    UNUSED_PARAM (u2NewTotLen);
    UNUSED_PARAM (u2NewLen);
    UNUSED_PARAM (u2NewTransportLen);
    UNUSED_PARAM (u2TransportCksum);
    UNUSED_PARAM (u4CksumOffset);
    UNUSED_PARAM (u4NewDataLen);
    UNUSED_PARAM (u2NewTransLen);
    NAT_TRC (NAT_TRC_ON, "\n SIPALG: Pkt Translation success");
    return (NAT_SUCCESS);
}
