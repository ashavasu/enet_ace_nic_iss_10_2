/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: natipmul.c,v 1.4 2014/01/25 13:52:26 siva Exp $
 *
 * Description:This file contains function(s) for processing IP Multicast
 * packets. 
 *
 *******************************************************************/
#include "natinc.h"

/***************************************************************************
* Function Name  :  NatProcessIpMulticast
* Description    :  This function gets and stores the Translation info for
*                   the IP multicast packets 
*                  
*
* Input (s)    :  Pointer to HeaderInfo 
*
* Output (s)    :  Pointer to HeaderInfo containing the full Translation info
* Returns      :  NAT_SUCCESS or NAT_FAILURE
*
****************************************************************************/
PUBLIC UINT4
NatProcessIpMulticast (tHeaderInfo * pHeaderInfo)
{
    UINT4               u4Status = NAT_ZERO;
    UINT4               u4IpAddr = NAT_ZERO;
    tNatOnlyNode       *pNatOnlyNode = NULL;
    tGlobalInfo         GlobalInfo;

    MEMSET (&GlobalInfo, NAT_ZERO, sizeof (tGlobalInfo));
    /*
     * this function searches for a mapping for the IP address (only IP
     * address). It searches first ths Static Table then the NatOnlyNat Table.
     * If not present it allocates a new IP address .Store the IP address
     * in NatOnlyNat Table.
     */
    u4Status = NAT_SUCCESS;
    u4IpAddr = NatSearchStaticTable (pHeaderInfo->u4InIpAddr,
                                     pHeaderInfo->u4Direction,
                                     pHeaderInfo->u4IfNum);

    if (u4IpAddr == NAT_ZERO)
    {
        pNatOnlyNode = NatSearchNatOnlyTable (pHeaderInfo->u4InIpAddr,
                                              pHeaderInfo->u4IfNum);
        if (pNatOnlyNode == NULL)
        {
            /*Get a new Global IP address */
            NatGetNextFreeGlobalIpPort (pHeaderInfo->u4InIpAddr, NAT_ZERO,
                                        pHeaderInfo->u4OutIpAddr,
                                        pHeaderInfo->u2OutPort,
                                        pHeaderInfo->u4IfNum, &GlobalInfo);

            u4IpAddr = GlobalInfo.u4TranslatedLocIpAddr;
        }
        else
        {
            u4IpAddr = pNatOnlyNode->u4TranslatedLocIpAddr;
        }
        if (u4IpAddr != NAT_ZERO)
        {
            /*Add the IP address in NatOnlyNat Table with MCAST flag set. */
            NatAddNatOnlyTableForIpMul (pHeaderInfo->u4InIpAddr, u4IpAddr,
                                        pHeaderInfo->u4IfNum);
        }
        else
        {
            u4Status = NAT_FAILURE;
        }
    }
    if (u4IpAddr != NAT_ZERO)
    {
        pHeaderInfo->u4InIpAddr = u4IpAddr;
    }
    return (u4Status);
}

/***************************************************************************
* Function Name  :  NatCheckIfAddMcastEntry
* Description    :  This function checks whether a multicast entry has to be
*                   created or not
* *
*
* Input (s)    :  Pointer to HeaderInfo
*
* Output (s)    :  Pointer to HeaderInfo containing the full Translation info
* Returns      :  NAT_SUCCESS or NAT_FAILURE
*
****************************************************************************/
UINT4
NatCheckIfAddMcastEntry (UINT4 u4IfNum)
{
    UINT4               u4Iface = NAT_ZERO;
    UINT4               u4IfStatus = NAT_ZERO;

    u4IfStatus = NatCheckIfNatEnable (u4IfNum);
    if (u4IfStatus == NAT_ENABLE)
    {
        /* NAT is enabled on the interface, pkt is received
         * from outside network, Return SUCCESS so that
         * Entry can be created in the HW
         */
        return NAT_SUCCESS;
    }

    for (u4Iface = NAT_ZERO; u4Iface <= NAT_MAX_NUM_IF + NAT_ONE; u4Iface++)
    {
        u4IfStatus = NatCheckIfNatEnable (u4Iface);
        if (u4IfStatus == NAT_ENABLE)
        {
            /* NAT is enabled on some other interface, pkt is
             * received from inside network, Right now return
             * FAILURE and set a global variable so that Mcast
             * data is forwarded in control plane itself
             */
            /* In future we can check if there are any listners
             * in the outside network, if YES we can return FAILURE
             * else we can return SUCCESS so that entry can be
             * created in HW
             */
            return NAT_FAILURE;
        }
    }

    return NAT_SUCCESS;
}
