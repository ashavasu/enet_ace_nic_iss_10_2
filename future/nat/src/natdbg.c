/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: natdbg.c,v 1.4 2011/06/14 11:31:31 siva Exp $
 *
 * Description:This file contains NAT module functions needed for debugging
 *             the NAT module.
 *
 *******************************************************************/
/*
 * The NatDbgMalloc, NatDbgFree, NatDbgPrintUnFreed are three functions that
 * can be used to debug memory related problems.
 * The following data structure is used by these functions.
 * This data structure and the three functions will be defined only when
 * NAT_MALLOC_CHECK_NEEDED is defined.
 * NAT_MALLOC_CHECK_NEEDED can be defined in gensys.h
 */
#ifndef _NATDBG_C
#define _NATDBG_C

#include "natinc.h"

UINT4               u4NatDbgUnFreedByteCount = 0;
UINT4               u4NatDbgFreedByteCount = 0;
UINT4               u4NatDbgMallocCallCount = 0;
tNatDbgMallocNode  *pNatDbgMallocHead;

INT1                FileData[] =
    { 0x45, 0, 0, 0x2c, 0x18, 0x5e, 0, 0, 0x40, 0x06,
    0xec, 0x63, 0x46, 0, 0, 0x0a, 0x0f, 0x01,
    0x01, 0x0a, 0, 0x17, 0x17, 0x73, 0x59, 0x27,
    0xdd, 0xdd, 0x09, 0x3b, 0x39, 0x76, 0x60, 0x12,
    0x37, 0xff, 0x5c, 0x80, 0, 0, 0x02, 0x04,
    0x02, 0, 0x44, 0x55
};

/********************************************************************
 * DEBUG MALLOC FACILITY FROM-NatDbg.
 * Use this for faster debugging of memory related problems!!!
 *
 * NatDbgMalloc has to be input with filename, lineno of the place
 * from where malloc is needed.
 * NatDbgMalloc maintains a list of all mallocs done.
 *
 * NatDbgFree is a simple function.
 *
 * NatDbgPrintUnFreed prints all the unfreed blocks.
 * It will print the filename, lineno of the malloc call.
 *
 *******************************************************************/

PUBLIC UINT4
NatDbgMalloc (const INT1 *pi1FileName, const INT4 i4LineNo,
              tMemPoolId MemPoolId, UINT1 **AddrPtr)
{
    UINT1              *pu1Block;
    UINT4               u4Size;
    tNatDbgMallocNode  *pNode;
    INT1                i1Status;

    i1Status = MemAllocateMemBlock (MemPoolId, &pu1Block);
    if (i1Status == MEM_FAILURE)
    {
        i1Status = NatDynamicAlloc (MemPoolId, &pu1Block);
    }
    if (i1Status == MEM_FAILURE)
    {
        NAT_DBG (NAT_DBG_PKT_FLOW, "\nMALLOC FAILED");
        return NAT_FAILURE;
    }
    u4Size = NatGetPoolUnitSizeFromPoolId (MemPoolId);
    pNode = (tNatDbgMallocNode *) NAT_MALLOC (sizeof (tNatDbgMallocNode));
    if (pNode == NULL)
    {
        NAT_DBG (NAT_DBG_PKT_FLOW, "\nMalloc failed in NatDbgMalloc\n");
        NAT_FREE (pu1Block);
        return NAT_FAILURE;
    }
    pNode->pu1Block = pu1Block;
    pNode->u4Size = u4Size;
    STRCPY (pNode->i1FileName, pi1FileName);
    pNode->i4LineNo = i4LineNo;
    pNode->pPrev = NULL;
    pNode->pNext = NULL;

    if (pNatDbgMallocHead == NULL)
    {
        pNatDbgMallocHead = pNode;
    }
    else
    {
        pNode->pNext = pNatDbgMallocHead;
        pNatDbgMallocHead->pPrev = pNode;
        pNatDbgMallocHead = pNode;
    }

    u4NatDbgUnFreedByteCount += u4Size;
    u4NatDbgMallocCallCount++;
    *AddrPtr = pu1Block;
    return NAT_SUCCESS;
}

PUBLIC VOID
NatDbgFree (tMemPoolId MemPoolId, UINT1 *pu1Block)
{
    tNatDbgMallocNode  *pRover, *pNode;

    for (pRover = pNatDbgMallocHead; pRover != NULL; pRover = pRover->pNext)
    {
        if (pRover->pu1Block == pu1Block)
        {
            break;
        }
    }
    if (pRover == NULL)
    {
        NAT_DBG (NAT_DBG_PKT_FLOW, "\nILLEGAL FREE");
        return;
    }
    if (MemReleaseMemBlock (MemPoolId, pu1Block) == MEM_FAILURE)
    {
        NAT_FREE (pu1Block);
    }

    /*NAT_DBG1 (NAT_DBG_PKT_FLOW, "\nFREEING : %d",pRover->u4Size); */
    u4NatDbgUnFreedByteCount -= pRover->u4Size;
    u4NatDbgFreedByteCount += pRover->u4Size;

    pNode = pRover;
    if (pNode->pPrev == NULL)
    {
        if ((pNatDbgMallocHead = pNode->pNext) != NULL)
        {
            pNode->pNext->pPrev = NULL;
        }
        NAT_FREE (pNode);
        return;
    }

    if (pNode->pNext == NULL)
    {
        pNode->pPrev->pNext = NULL;
        NAT_FREE (pNode);
        return;
    }

    pNode->pPrev->pNext = pNode->pNext;
    pNode->pNext->pPrev = pNode->pPrev;
    NAT_FREE (pNode);
}

PUBLIC VOID
NatDbgPrintUnFreed (VOID)
{
    tNatDbgMallocNode  *pRover;

    for (pRover = pNatDbgMallocHead; pRover != NULL; pRover = pRover->pNext)
    {
        NAT_DBG3 (NAT_DBG_PKT_FLOW, "\nMallocSize = %d File = %s Line %d",
                  pRover->u4Size, pRover->i1FileName, pRover->i4LineNo);
    }
}

PUBLIC UINT4
NatGetPoolUnitSizeFromPoolId (tMemPoolId MemPoolId)
{

    if (MemPoolId == NAT_SIP_HASH_POOL_ID)
    {
        NAT_DBG (NAT_DBG_PKT_FLOW, "Malloc For Structure :tNatWanUaHashNode\n");
        return (sizeof (tNatWanUaHashNode));
    }

    if (MemPoolId == NAT_DYNAMIC_POOL_ID)
    {
        NAT_DBG (NAT_DBG_PKT_FLOW, "Malloc For Structure :tDynamicEntry\n");
        return (sizeof (tDynamicEntry));
    }

    if (MemPoolId == NAT_HASH_POOL_ID)
    {
        NAT_DBG (NAT_DBG_PKT_FLOW, "Malloc For Structure :tLocOutHashNode\n");
        return (sizeof (tLocOutHashNode));
    }

    if (MemPoolId == NAT_GLOBAL_HASH_POOL_ID)
    {
        NAT_DBG (NAT_DBG_PKT_FLOW, "Malloc For Structure :tGlobalHashNode\n");
        return (sizeof (tGlobalHashNode));
    }

    if (MemPoolId == NAT_INTERFACE_INFO_POOL_ID)
    {
        NAT_DBG (NAT_DBG_PKT_FLOW, "Malloc For Structure :tInterfaceInfo\n");
        return (sizeof (tInterfaceInfo));
    }

    if (MemPoolId == NAT_STATIC_POOL_ID)
    {
        NAT_DBG (NAT_DBG_PKT_FLOW, "Malloc For Structure :tStaticEntryNode\n");
        return (sizeof (tStaticEntryNode));
    }

    if (MemPoolId == NAT_LOCAL_ADDR_POOL_ID)
    {
        NAT_DBG (NAT_DBG_PKT_FLOW, "Malloc For Structure :tLocIpAddrNode\n");
        return (sizeof (tLocIpAddrNode));
    }

    if (MemPoolId == NAT_DNS_LIST_POOL_ID)
    {
        NAT_DBG (NAT_DBG_PKT_FLOW, "Malloc For Structure :tDnsEntryNode\n");
        return (sizeof (tDnsEntryNode));
    }

    if (MemPoolId == NAT_ARR_DYNAMIC_NODE_POOL_ID)
    {
        NAT_DBG (NAT_DBG_PKT_FLOW,
                 "Malloc For Structure :tIidOidArrayDynamicListNode\n");
        return (sizeof (tIidOidArrayDynamicListNode));
    }

    if (MemPoolId == NAT_IID_LIST_POOL_ID)
    {
        NAT_DBG (NAT_DBG_PKT_FLOW, "Malloc For Structure :tIidListNode\n");
        return (sizeof (tIidListNode));
    }

    if (MemPoolId == NAT_FREE_GLOBAL_LIST_POOL_ID)
    {
        NAT_DBG (NAT_DBG_PKT_FLOW,
                 "Malloc For Structure :tNatFreeGipListNode\n");
        return (sizeof (tNatFreeGipListNode));
    }

    if (MemPoolId == NAT_GLOBAL_ADDR_POOL_ID)
    {
        NAT_DBG (NAT_DBG_PKT_FLOW,
                 "Malloc For Structure :tTranslatedLocIpAddrNode\n");
        return (sizeof (tTranslatedLocIpAddrNode));
    }

    if (MemPoolId == NAT_TCP_STACK_POOL_ID)
    {
        NAT_DBG (NAT_DBG_PKT_FLOW, "Malloc For Structure :tTcpDelNode\n");
        return (sizeof (tTcpDelNode));
    }

    if (MemPoolId == NAT_FREE_PORT_POOL_ID)
    {
        NAT_DBG (NAT_DBG_PKT_FLOW, "Malloc For Structure :tFreePortNode\n");
        return (sizeof (tFreePortNode));
    }

    if (MemPoolId == NAT_TFTP_FTP_LIST_POOL_ID)
    {
        NAT_DBG (NAT_DBG_PKT_FLOW,
                 "Malloc For Structure :tNatPartialLinkNode\n");
        return (sizeof (tNatPartialLinkNode));
    }

    return 0;
}

PUBLIC VOID
NatInitiateSessions (UINT2 u2totalSess, UINT1 u1Type)
{
    UINT2               telSession;
    UINT2               u2SrcPort;
    UINT2               u2NewSrcPort;
    UINT4               u4SrcPortOffset;
    UINT2               u2Chksum;
    UINT4               u4IpAddr;
    tCRU_BUF_CHAIN_HEADER *pBuf;

    u2SrcPort = 1024;
    pBuf = (tCRU_BUF_CHAIN_HEADER *) FileData;
    CRU_BMC_APPEND_STRING (pBuf, (UINT1 *) FileData, 46);

    u4SrcPortOffset = NatGetIpHeaderLength (pBuf);
    switch (u1Type)
    {
        case 1:
            u4IpAddr = 0x0a01010f;
            break;
        case 2:
            u4IpAddr = 0x0a00000a;
            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4IpAddr,
                                       NAT_IP_SRC_OFFSET, 4);
            u4IpAddr = 0x0a000046;
            break;
        default:
            return;
    }

    for (telSession = 0; telSession < u2totalSess; telSession++)
    {
        u2NewSrcPort = u2SrcPort + 1;
        u2SrcPort = OSIX_NTOHS (u2SrcPort);
        u2NewSrcPort = OSIX_NTOHS (u2NewSrcPort);

        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2NewSrcPort,
                                   u4SrcPortOffset, NAT_PORT_LEN);
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4IpAddr,
                                   NAT_IP_DST_OFFSET, 4);
        NatChecksumAdjust ((UINT1 *) &u2Chksum, (UINT1 *) &u2SrcPort,
                           sizeof (UINT2), (UINT1 *) &u2NewSrcPort,
                           sizeof (UINT2));
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2Chksum,
                                   NAT_TCP_CKSUM_OFFSET +
                                   NatGetIpHeaderLength (pBuf), NAT_WORD_LEN);

        if (u1Type == 1)
            NatTranslateInBoundPkt (pBuf, 1);
        else
            NatTranslateOutBoundPkt (pBuf, 0);

        u2SrcPort = OSIX_NTOHS (u2SrcPort);
        u2SrcPort++;

    }

}

PUBLIC VOID
NatDeleteSessions (UINT2 u2Port, UINT2 u2totalSess, UINT1 u1Type)
{
    UINT2               telSession;
    UINT2               u2SrcPort;
    UINT2               u2NewSrcPort;
    UINT4               u4SrcPortOffset;
    UINT2               u2Chksum;
    UINT4               u4IpAddr;
    UINT1               u1FinBit;
    tCRU_BUF_CHAIN_HEADER *pBuf;

    u2SrcPort = u2Port;
    pBuf = (tCRU_BUF_CHAIN_HEADER *) CRU_BUF_Allocate_MsgBufChain (300, 30);
    CRU_BMC_APPEND_STRING (pBuf, (UINT1 *) FileData, 46);

    switch (u1Type)
    {
        case 1:
            u4IpAddr = 0x0a01010f;
            break;
        case 2:
            u4IpAddr = 0x0a00000a;
            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4IpAddr,
                                       NAT_IP_SRC_OFFSET, 4);
            u4IpAddr = 0x0a000046;
            break;
        default:
            return;
    }

    u4SrcPortOffset = NatGetIpHeaderLength (pBuf);
    u1FinBit = 0x01;
    CRU_BUF_Copy_OverBufChain (pBuf, &u1FinBit,
                               u4SrcPortOffset + NAT_TCP_CODE_BYTE_OFFSET, 1);
    if (u1Type == 1)
        NatTranslateInBoundPkt (pBuf, 1);
    else
        NatTranslateOutBoundPkt (pBuf, 0);

    for (telSession = 0; telSession < u2totalSess; telSession++)
    {
        u2NewSrcPort = u2SrcPort + 1;
        u2SrcPort = OSIX_NTOHS (u2SrcPort);
        u2NewSrcPort = OSIX_NTOHS (u2NewSrcPort);

        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2NewSrcPort,
                                   u4SrcPortOffset, NAT_PORT_LEN);
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4IpAddr,
                                   NAT_IP_DST_OFFSET, 4);
        CRU_BUF_Copy_OverBufChain (pBuf, &u1FinBit,
                                   u4SrcPortOffset + NAT_TCP_CODE_BYTE_OFFSET,
                                   1);
        NatChecksumAdjust ((UINT1 *) &u2Chksum, (UINT1 *) &u2SrcPort,
                           sizeof (UINT2), (UINT1 *) &u2NewSrcPort,
                           sizeof (UINT2));
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2Chksum,
                                   NAT_TCP_CKSUM_OFFSET +
                                   NatGetIpHeaderLength (pBuf), NAT_WORD_LEN);

        if (u1Type == 1)
            NatTranslateInBoundPkt (pBuf, 1);
        else
            NatTranslateOutBoundPkt (pBuf, 0);

        u2SrcPort = OSIX_NTOHS (u2SrcPort);
        u2SrcPort++;

    }

}

PUBLIC VOID
NatInitiateFromDifferentHosts (UINT2 u2totalSess, UINT1 u1Type)
{
    UINT2               telSession;
    UINT4               u4SrcIpAddr;
    UINT4               u4DestIpAddr;
    UINT4               u4NewSrcIpAddr;
    UINT4               u4NewDestIpAddr;
    tCRU_BUF_CHAIN_HEADER *pBuf;

    pBuf = (tCRU_BUF_CHAIN_HEADER *) CRU_BUF_Allocate_MsgBufChain (300, 30);
    CRU_BMC_APPEND_STRING (pBuf, (UINT1 *) FileData, 46);

    switch (u1Type)
    {
        case 1:
            u4SrcIpAddr = 0x4600000a;
            u4DestIpAddr = 0x0f01010a;
            break;
        case 2:
            u4SrcIpAddr = 0x0a00000a;
            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4SrcIpAddr,
                                       NAT_IP_SRC_OFFSET, 4);
            u4DestIpAddr = 0x4600000a;
            break;
        default:
            return;
    }

    for (telSession = 0; telSession < u2totalSess; telSession++)
    {
        u4NewSrcIpAddr = u4SrcIpAddr + 1;
        u4NewDestIpAddr = u4DestIpAddr + 1;

        u4SrcIpAddr = OSIX_NTOHS (u4SrcIpAddr);
        u4NewSrcIpAddr = OSIX_NTOHS (u4NewSrcIpAddr);
        u4DestIpAddr = OSIX_NTOHS (u4DestIpAddr);
        u4NewDestIpAddr = OSIX_NTOHS (u4NewDestIpAddr);

        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4NewSrcIpAddr,
                                   NAT_IP_SRC_OFFSET, 4);
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4NewDestIpAddr,
                                   NAT_IP_DST_OFFSET, 4);

        if (u1Type == 1)
            NatTranslateInBoundPkt (pBuf, 1);
        else
            NatTranslateOutBoundPkt (pBuf, 0);

        u4SrcIpAddr = OSIX_NTOHS (u4SrcIpAddr);
        u4DestIpAddr = OSIX_NTOHS (u4DestIpAddr);
        u4SrcIpAddr++;
        u4DestIpAddr++;

    }

}

PUBLIC VOID
NatDeleteFromDifferentHosts (UINT2 u2Port, UINT2 u2totalSess, UINT1 u1Type)
{
    UINT2               telSession;
    UINT4               u4SrcIpAddr, u4SrcPortOffset;
    UINT4               u4DestIpAddr;
    UINT4               u4NewSrcIpAddr;
    UINT4               u4NewDestIpAddr;
    UINT1               u1FinBit;
    tCRU_BUF_CHAIN_HEADER *pBuf;

    pBuf = (tCRU_BUF_CHAIN_HEADER *) CRU_BUF_Allocate_MsgBufChain (300, 30);
    CRU_BMC_APPEND_STRING (pBuf, (UINT1 *) FileData, 46);
    u4SrcPortOffset = NatGetIpHeaderLength (pBuf);

    switch (u1Type)
    {
        case 1:
            u4SrcIpAddr = 0x4600000a;
            u4DestIpAddr = 0x0f01010a;
            break;
        case 2:
            u4SrcIpAddr = 0x0a00000a;
            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4SrcIpAddr,
                                       NAT_IP_SRC_OFFSET, 4);
            u4DestIpAddr = 0x4600000a;
            break;
        default:
            return;
    }
    u1FinBit = 0x01;
    CRU_BUF_Copy_OverBufChain (pBuf, &u1FinBit,
                               u4SrcPortOffset + NAT_TCP_CODE_BYTE_OFFSET, 1);

    if (u1Type == 1)
        NatTranslateInBoundPkt (pBuf, 1);
    else
        NatTranslateOutBoundPkt (pBuf, 0);

    for (telSession = 0; telSession < u2totalSess; telSession++)
    {
        u4NewSrcIpAddr = u4SrcIpAddr + 1;
        u4NewDestIpAddr = u4DestIpAddr + 1;

        u4SrcIpAddr = OSIX_NTOHS (u4SrcIpAddr);
        u4NewSrcIpAddr = OSIX_NTOHS (u4NewSrcIpAddr);
        u4DestIpAddr = OSIX_NTOHS (u4DestIpAddr);
        u4NewDestIpAddr = OSIX_NTOHS (u4NewDestIpAddr);

        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4NewSrcIpAddr,
                                   NAT_IP_SRC_OFFSET, 4);
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4NewDestIpAddr,
                                   NAT_IP_DST_OFFSET, 4);

        if (u1Type == 1)
            NatTranslateInBoundPkt (pBuf, 1);
        else
            NatTranslateOutBoundPkt (pBuf, 0);

        u4SrcIpAddr = OSIX_NTOHS (u4SrcIpAddr);
        u4DestIpAddr = OSIX_NTOHS (u4DestIpAddr);
        u4SrcIpAddr++;
        u4DestIpAddr++;

    }

}
#endif /* _NATDBG_C */
